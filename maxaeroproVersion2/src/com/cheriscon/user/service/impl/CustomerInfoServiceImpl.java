package com.cheriscon.user.service.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.user.service.ICustomerInfoService;

/**
 * @remark 用户信息业务逻辑实现类
 * @author yangpinggui
 * @date 2013-01-07
 * 
 */
@Service
public class CustomerInfoServiceImpl 
		implements ICustomerInfoService {

	@SuppressWarnings("unchecked")
	public Map<String, Object> ResendEmail(String autelId) {
		String jsonData = HttpUtil.getReqeuestContent("rest.customer.resendEmail", "autelId=" + autelId);
		Map<String, Object> dataMap = JsonUtil.getMapFromJson(jsonData);
		return dataMap;
	}
	
	public boolean addCustomer(CustomerInfo customerInfo) throws Exception {
		String result = HttpUtil.postRequestContent("rest.customerInfo.register","customerInfo.", customerInfo);
		if("success".equals(result)||"\"success\"".equals(result)){
			return true;
		}
		return false;
	}


	public boolean updateCustomer(CustomerInfo customerInfo) throws Exception {

		String result = HttpUtil.postRequestContent("rest.customerInfo.update","customerInfo.", customerInfo);
		if("success".equals(result)||"\"success\"".equals(result)){
			return true;
		}
		return false;
	}


	@SuppressWarnings("unused")
	public boolean updateCustomerByCode(CustomerInfo customerInfoEdit)
			throws Exception {

		
		String jsonData = HttpUtil.postRequestContent(
				"rest.customer.updateByCode", "customerInfoEdit.", customerInfoEdit);
		
		return true;
	}
	
	public boolean updateCustPwdForSealer(CustomerInfo customerInfoEdit) {
		String jsonData = HttpUtil.postRequestContent(
				"rest.sealer.updateCustPwdForSealer", "customerInfoEdit.", customerInfoEdit);
		return true;
	}

//	@Override
//	public void updateCustomer(String code, String lastLoginTime)
//			throws Exception {
//		return true;
//	}

	@Override
	public void logout(String code)
			{
		HttpUtil.getReqeuestContent("rest.customerInfo.logout", "code="+code);
	}

//	@Override
//	public void updateCustomerPwd(String code, String password)
//			throws Exception {
//		String sql = "update DT_CustomerInfo set userPwd=? where code= ?";
//		this.daoSupport.execute(sql, password, code);
//	}

	@Override
	public CustomerInfo getCustomerInfoByAutelId(String autelId)
			throws Exception {
		CustomerInfo customerInfo = (CustomerInfo) HttpUtil.getReqeuestContentObject("rest.customerInfo.get.by.autelid", 
				"autelId="+autelId, CustomerInfo.class);
		return customerInfo;
	}

	@Override
	public CustomerInfo getCustomerInfoByAutelIdAndPassword(String autelId,
			String password,String loginIp) {
		CustomerInfo customerInfo = (CustomerInfo) HttpUtil.getReqeuestContentObject("rest.customerInfo.login", 
				"autelId="+autelId+"&password="+password+"&loginIp="+loginIp, CustomerInfo.class);
		return customerInfo;
	}
//
//	@Override
//	public CustomerInfo getCustomerInfoByAutelIdAndActCode(String autelId,
//			String actCode) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from DT_CustomerInfo where autelId='")
//				.append(autelId).append("' and actCode='").append(actCode)
//				.append("'");
//		return this.daoSupport.queryForObject(sql.toString(),
//				CustomerInfo.class);
//	}
//
//	public CustomerInfo getCustomerInfoBySecondEmailAndActCode(
//			String secondEmail, String actCode) {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from DT_CustomerInfo where secondEmail='")
//				.append(secondEmail).append("' and actCode='").append(actCode)
//				.append("'");
//
//		List<CustomerInfo> list = this.daoSupport.queryForList(sql.toString(),
//				CustomerInfo.class);
//		if (list == null || list.size() == 0) {
//			return null;
//		} else {
//			return list.get(0);
//		}
//
//	}

	/**
	 * 根据code查找用户信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Override
	public CustomerInfo getCustomerByCode(String code) throws Exception {
		return (CustomerInfo)HttpUtil.getReqeuestContentObject("rest.customerInfo.get.by.code", 
				"code="+code, CustomerInfo.class);
	}
//
//	/**
//	 * 根据code删除用户信息
//	 * 
//	 * @param code
//	 * @return
//	 * @throws Exception
//	 * @author pengdongan
//	 */

//	public int delCustomer(String code) throws Exception {
//		String sql = "delete from DT_CustomerInfo where code= ?";
//		this.daoSupport.execute(sql, code);
//		return 0;
//	}
//
//	/**
//	 * 根据codes批量删除用户信息
//	 * 
//	 * @param code
//	 * @return
//	 * @throws Exception
//	 * @author pengdongan
//	 */

//	public int delCustomers(String codes) throws Exception {
//		String sql = "delete from DT_CustomerInfo where code in(" + codes + ")";
//		this.daoSupport.execute(sql);
//		return 0;
//	}

	@Override
	public CustomerInfo getCustomerInfoBySerialNo(String serialNo)
			throws Exception {
		return (CustomerInfo)HttpUtil.getReqeuestContentObject("rest.customerInfo.get.by.serialNo", 
				"serialNo="+serialNo, CustomerInfo.class);
	}
	public CustomerInfo getCustomerInfoBySerialNo(String serialNo,String password){
		return (CustomerInfo)HttpUtil.getReqeuestContentObject("rest.customerInfo.get.by.serialNo", 
				"serialNo="+serialNo+"&password="+password, CustomerInfo.class);
	}

//	@Override
//	public CustomerInfo getCustomerAutelIdBySerialNo(String serialNo)
//			throws Exception {
//		StringBuffer querySql = new StringBuffer();
//		querySql.append("select b.autelId,b.code from DT_ProductInfo a,")
//				.append(" DT_CustomerInfo b,DT_CustomerProInfo d ")
//				.append(" where a.code = d.proCode and b.code = d.customerCode ")
//				.append(" and  a.serialNo='").append(serialNo).append("'");
//
//		return this.daoSupport.queryForObject(querySql.toString(),
//				new ParameterizedBeanPropertyRowMapper<CustomerInfo>() {
//
//					@Override
//					public CustomerInfo mapRow(ResultSet rs, int arg1)
//							throws SQLException {
//						CustomerInfo customerInfo = new CustomerInfo();
//						customerInfo.setAutelId(rs.getString("autelId"));
//						customerInfo.setCode(rs.getString("code"));
//						return customerInfo;
//					}
//				});
//	}
//
//	@Override
//	public CustomerInfo findCustomerInfoByAutelId(String secondEmail,
//			String firstName, String lastName) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from DT_CustomerInfo where secondEmail='")
//				.append(secondEmail).append("' and firstName='")
//				.append(firstName).append("' and lastName='").append(lastName)
//				.append("'");
//		return this.daoSupport.queryForObject(sql.toString(),
//				CustomerInfo.class);
//	}
//
	@Override
	public List<CustomerInfo> getCustomerInfoListBySecondEmail(
			String secondEmail) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from DT_CustomerInfo where secondEmail='")
//				.append(secondEmail).append("'");
//		return this.daoSupport.queryForList(sql.toString(), CustomerInfo.class);
		return null;
	}
//
//	@Override
//	public int getCustomerInfoNumByAutelId(String autelId) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select count(id) from DT_CustomerInfo where autelId=?");
//
//		return this.daoSupport.queryForInt(sql.toString(), autelId);
//	}
//
//	public List<CustomerInfo> updatePassWordList() throws Exception {
//		String sql = "select id,code,userPwd from  DT_CustomerInfo where sourceType=22 and id>294194";
//		return this.daoSupport.queryForList(sql.toString(), CustomerInfo.class);
//	}
//
//	public CustomerInfo getCustBySerialNo(String serialNo) throws Exception {
//		String sql = "select a.* from DT_CustomerInfo a,DT_customerProInfo b ,DT_productInfo c where a.code=b.customerCode and b.procode=c.code and c.serialNo like ? limit 0,1";
//		return this.daoSupport.queryForObject(sql, CustomerInfo.class, serialNo
//				+ "%");
//	}
//
//	public CustomerInfo getCustBySerialNo2(String serialNo) throws Exception {
//		String sql = "select a.* from DT_CustomerInfo a,DT_customerProInfo b ,DT_productInfo c where a.code=b.customerCode and b.procode=c.code and c.serialNo = ? limit 0,1";
//		return this.daoSupport
//				.queryForObject(sql, CustomerInfo.class, serialNo);
//	}
//
	public CustomerInfo getCustBySerialNo3(String serialNo) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select a.*,c.proDate as proDate,c.regTime as proRegTime,d.warrantyMonth as  warrantyMonth,");
//		sql.append(" (select distinct validdate from DT_ProductSoftwareValidStatus where proCode=c.code ) as expTime");
//		sql.append(" from DT_CustomerInfo a,DT_customerProInfo b ,DT_productInfo c ,DT_SaleContract d");
//		sql.append(" where ");
//		sql.append(" a.code=b.customerCode  ");
//		sql.append(" and b.procode=c.code  ");
//		sql.append(" and c.saleContractCode=d.code ");
//		sql.append(" and c.serialNo=? limit 0,1");
//		return this.daoSupport.queryForObject(sql.toString(),
//				CustomerInfo.class, serialNo);
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.rga.getCustBySerialNo3", "serialNo=" + serialNo);
		return (CustomerInfo) JsonUtil.getDTO(jsonData, CustomerInfo.class);
	}
//
//	public CustomerInfo getCustBySerialNo(String serialNo, String password)
//			throws Exception {
//		String sql = "select a.* from DT_CustomerInfo a,DT_customerProInfo b ,DT_productInfo c where a.code=b.customerCode and b.procode=c.code and c.serialNo like ? and c.regPwd=?";
//		return this.daoSupport.queryForObject(sql, CustomerInfo.class, serialNo
//				+ "%", password);
//	}

	public void updateBbsUsername(String code, String username)
			throws Exception {
	}

	public String updateCustomerPassword(String autelId, String oldPassword,
			int id, String password) {
		String jsonData = HttpUtil.getReqeuestContent(
				"rest.customer.updatePassword", "autelId=" + autelId + 
				"&oldPassword=" + oldPassword + "&customerId=" + id + 
				"&password=" + password);
		Map map = (Map) JsonUtil.getDTO(jsonData, Map.class);
		jsonData = JSONArray.fromObject(map).toString();
		return jsonData;
	}


	@Override
	public void loginAfter(String userCode, String loginIp) {
		HttpUtil.getReqeuestContent("rest.customerInfo.loginAfter", "userCode="+userCode+"&loginIp="+loginIp);
	}

	@Override
	public String forgotPasswordForMail(String autelId) {
		return HttpUtil.getReqeuestContent("rest.customerInfo.forgotPasswordForMail", "autelId="+autelId);
	}

	@Override
	public String reSetPasswordByActCode(String actCode, String password,String autelId) {
		
		/*return HttpUtil.getReqeuestContent("rest.customerInfo.reSetPasswordByActCode",
				"actCode="+actCode+"&password="+password+"&autelId="+autelId);*/
		// add by gaoyaxiong 20160824 因为get请求会将已经转化后的特殊字符进行再次转化，而导致结果不正确。
		// 故需要采用post提交请求
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("actCode", actCode);
		map.put("password", password);
		map.put("autelId", autelId);
		
		return HttpUtil.postRequestContent("rest.customerInfo.reSetPasswordByActCode",map);
	}
	
	public CustomerInfo getByFacebookId(String id) {
		String jsonData = HttpUtil.getReqeuestContent("rest.customerInfo.getbyFacebookId", "facebookId=" + id);
		return (CustomerInfo) JsonUtil.getDTO(jsonData, CustomerInfo.class);
	}

	public String updateAutelId(String oldId, String newId) {
		String jsonData = HttpUtil.getReqeuestContent("rest.customerinfo.updateAutelId", "oldId=" + oldId + "&newId=" + newId);
		return jsonData;
	}
}
