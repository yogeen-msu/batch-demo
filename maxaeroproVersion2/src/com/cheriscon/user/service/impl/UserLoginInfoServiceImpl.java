package com.cheriscon.user.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

//import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.UserLoginInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.user.service.IUserLoginInfoService;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.cop.sdk.database.BaseSupport;


/**
 * @remark 用户登录信息service实现
 * @author yangpinggui
 * @date 2013-01-07
 * 
 */
@Service
public class UserLoginInfoServiceImpl  implements IUserLoginInfoService
{

//	@Transactional
	public boolean addUserLoginInfo(UserLoginInfo userLoginInfo)
			throws Exception
	{
		HttpUtil.postRequestContent("rest.userlogininfo.save", "userLoginInfo.", userLoginInfo);
		
//		userLoginInfo.setCode(DBUtils.generateCode(DBConstant.USERLOGININFO_CODE));
//		this.daoSupport.insert("DT_UserLoginInfo", userLoginInfo);
		return true;
	}

	public List<UserLoginInfo> queryUserLoginInfoByCode(String usercode) {
		return null;
	}

	@Override
	public UserLoginInfo getLastLogin(String userCode) {
		return (UserLoginInfo) HttpUtil.getReqeuestContentObject("rest.customerInfo.getLastLogin", "userCode="+userCode, UserLoginInfo.class);
	}
}
