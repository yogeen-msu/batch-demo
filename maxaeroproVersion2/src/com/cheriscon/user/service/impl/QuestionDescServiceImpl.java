package com.cheriscon.user.service.impl;


//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//
import org.springframework.stereotype.Service;

import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.user.model.QuestionDesc;
import com.cheriscon.user.service.IQuestionDescService;
//import org.springframework.transaction.annotation.Transactional;

//import com.cheriscon.backstage.member.service.IQuestionDescService;
//import com.cheriscon.backstage.system.service.ILanguageService;
//import com.cheriscon.common.model.Language;
//import com.cheriscon.common.model.QuestionDesc;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.database.Page;

/**
 * 问题描述业务逻辑实现类
 * @author pengdongan
 * @date 2013-01-24
 */
@Service
public class QuestionDescServiceImpl implements IQuestionDescService
{
//	
//	@Resource
//	private ILanguageService languageService;
//	
//	/**
//	 * 查询所有问题描述对象
//	 * @param codes
//	 * @return
//	 * @throws Exception
//	 */
//	@Override
//	public List<QuestionDesc> listAll() throws Exception {
//		String sql = "select * from DT_QuestionDesc order by id desc";
//		
//		return this.daoSupport.queryForList(sql, QuestionDesc.class);
//	}
	/**
	 * 按语言查询所有问题描述对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<QuestionDesc> listQuestionDescByLanguageCode(String languageCode) throws Exception {
		List<QuestionDesc> descs = (List<QuestionDesc>) HttpUtil.getReqeuestContentList("rest.question.desc.list.by.languagecode",
				"languageCode="+languageCode, QuestionDesc.class);
		return descs;
	}
//
//	/**
//	 * 根据客诉类型查询问题描述对象
//	 * @param codes
//	 * @return
//	 * @throws Exception
//	 */
//	@Override
//	public List<QuestionDesc> queryByQuestionCode(String questionCode) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select a.*,b.name languageName");
//		sql.append(" from DT_QuestionDesc a");
//		sql.append(" left join DT_Language b on a.languageCode=b.code");
//		sql.append(" where a.questionCode = ? order by a.id desc");
//		
//		return this.daoSupport.queryForList(sql.toString(), QuestionDesc.class, questionCode);
//	}
//	
//	
//	/**
//	 * 添加问题描述
//	 * @param questionDesc
//	 * 问题描述
//	 * @throws Exception
//	 */
//	@Transactional
//	public int insertQuestionDesc(QuestionDesc questionDesc) throws Exception {
//		this.daoSupport.insert("DT_QuestionDesc", questionDesc);
//		return 0;
//	}
//	
//	/**
//	 * 更新问题描述
//	 * @param questionDesc
//	 * 问题描述
//	 * @throws Exception
//	 */
//	@Transactional
//	public int updateQuestionDesc(QuestionDesc questionDesc) throws Exception {
//		this.daoSupport.update("DT_QuestionDesc", questionDesc, "questionCode='"+questionDesc.getQuestionCode()+"'");
//		return 0;
//	}
//
//	@Override
//	@Transactional
//	public void updateQuestionDescs(
//			List<QuestionDesc> questionDescs, List<QuestionDesc> delQuestionDescs)
//			throws Exception {
//		//删除
//		if(null != delQuestionDescs && delQuestionDescs.size() > 0){
//			for(QuestionDesc questionDesc : delQuestionDescs){
//				this.delQuestionDesc(questionDesc.getId());
//			}
//		}
//		//添加
//		if(null != questionDescs && questionDescs.size() > 0){
//			for(QuestionDesc questionDesc : questionDescs){
//				this.insertQuestionDesc(questionDesc);
//			}
//		}
//	}
//
//	public QuestionDesc getQuestionDescByCode(int id) throws Exception {
//		String sql = "select * from DT_QuestionDesc where id = ?";
//		
//		return (QuestionDesc) this.daoSupport.queryForObject(sql, QuestionDesc.class, id);
//	}
//
//	@Transactional
//	public int delQuestionDesc(int id) throws Exception {
//		String sql = "delete from DT_QuestionDesc where id= ?";
//		this.daoSupport.execute(sql,id);
//		return 0;
//	}
//
//	@Transactional
//	public int delQuestionDescByQuestionCode(String questionCode) throws Exception {
//		String sql = "delete from DT_QuestionDesc where questionCode= ?";
//		this.daoSupport.execute(sql,questionCode);
//		return 0;
//	}
//
//
//
//	/**
//	  * 问题描述分页显示方法
//	  * @param QuestionDesc
//	  * @param pageNo
//	  * @param pageSize
//	  * @return
//	  * @throws Exception
//	  */ 
//	@Override
//	public Page pageQuestionDescPage(QuestionDesc questionDesc, int pageNo,
//			int pageSize) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select a.*,c.questionDesc questionDesc");
//		sql.append(" from DT_QuestionInfo a");
//		sql.append(" left join DT_QuestionDesc c on a.code=c.questionCode");
//		sql.append(" where 1=1");
//		
////		if (null != questionDesc) {
////			if (StringUtils.isNotBlank(questionDesc.getName())) {
////				sql.append(" and c.name like '%");
////				sql.append(questionDesc.getName().trim());
////				sql.append("%'");
////			}
////		}		
////
////		
////		if (StringUtils.isNotBlank(questionDesc.getAdminLanguageCode())) {
////			sql.append(" and c.languageCode = '");
////			sql.append(questionDesc.getAdminLanguageCode().trim());
////			sql.append("'");
////		}
//		
//		
//		sql.append(" order by a.id desc");
//		
//		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
//				pageSize, QuestionDesc.class);
//		return page;
//	}
//
//
//	@Override
//	public QuestionDesc getQuestionDescByCode(
//			String code) throws Exception {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	@Override
//	public QuestionDesc getQuestionDesc(
//			String questionCode, String languageCode) throws Exception 
//	{
//		String sql = "select * from DT_QuestionDesc where questionCode=? and languageCode =? ";
//		
//		return this.daoSupport.queryForObject(sql, QuestionDesc.class, questionCode,languageCode);
//	}
//
//	/**
//	  * <p>Title: getAvailableLanguage</p>
//	  * <p>Description: </p>
//	  * @param unitCode
//	  * @return
//	  * @throws Exception
//	  */ 
//	@Override
//	public List<Language> getAvailableLanguage(String questionCode)
//			throws Exception {
//		List<Language> list = new ArrayList<Language>();
//		List<Language> languages = languageService.queryLanguage();
//		if(null == languages || languages.size() == 0){
//			return list;
//		}
//		List<QuestionDesc> questionDescs = this.queryByQuestionCode(questionCode);
//		if(null == questionDescs || questionDescs.size() == 0){
//			return languages;
//		}
//		Map<String , QuestionDesc> map = this.convertListToMap(questionDescs);
//		for(Language language : languages){
//			if(!map.containsKey(language.getCode()))
//				list.add(language);
//		}
//		return list;
//	}
//	
//	private Map<String , QuestionDesc> convertListToMap(List<QuestionDesc> questionDescs) throws Exception{
//		Map<String , QuestionDesc> map = new HashMap<String , QuestionDesc>();
//		for(QuestionDesc questionDesc : questionDescs){
//			map.put(questionDesc.getLanguageCode(), questionDesc);
//		}
//		return map;
//	}

@Override
public QuestionDesc getQuestionDescByAutelId(String autelId) {
	return (QuestionDesc) HttpUtil.getReqeuestContentObject("rest.question.desc.getQuestionDescByAutelId", 
			"autelId="+autelId, QuestionDesc.class);
}

}
