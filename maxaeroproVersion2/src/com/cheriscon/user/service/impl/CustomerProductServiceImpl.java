package com.cheriscon.user.service.impl;

//import java.util.List;

//import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.user.service.ICustomerProductService;
//import org.springframework.transaction.annotation.Transactional;

//import com.cheriscon.backstage.member.service.ICustomerProductService;
//import com.cheriscon.backstage.member.vo.CustomerProductVo;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.database.Page;

/**
 * @remark 客户信息service实现
 * @author pengdongan
 * @date 2013-01-10
 * 
 */
@Service
public class CustomerProductServiceImpl implements ICustomerProductService {

//	@Override
//	public List<CustomerProductVo> listCustomerPro(
//			CustomerProductVo customerProductVo) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select a.id,a.code,a.autelId,a.firstName,a.middleName,a.lastName,a.name,");
//		sql.append("d.code saleContractCode,e.code sealerCode,e.autelId sealerAutelId,f.code areaCfgCode,f.name areaCfgName,");
//		sql.append("c.code proCode,c.serialNo proSerialNo,c.proDate proDate,c.regTime proRegTime,c.regStatus proRegStatus,c.noRegReason proNoRegReason");
//		sql.append(" from DT_CustomerInfo a");
//		sql.append(" left join DT_CustomerProInfo b on a.code=b.customerCode");
//		sql.append(" left join DT_ProductInfo c on b.proCode=c.code");
//		sql.append(" left join DT_SaleContract d on c.saleContractCode=d.code");
//		sql.append(" left join DT_SealerInfo e on d.sealerCode=e.code");
//		sql.append(" left join DT_AreaConfig f on d.areaCfgCode=f.code");
//		sql.append(" where 1=1");
//
//		if (null != customerProductVo) {
//			if (StringUtils.isNotBlank(customerProductVo.getAutelId())) {
//				sql.append(" and a.autelId like '%");
//				sql.append(customerProductVo.getAutelId().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getProSerialNo())) {
//				sql.append(" and c.serialNo like '%");
//				sql.append(customerProductVo.getProSerialNo().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getSealerAutelId())) {
//				sql.append(" and e.autelId like '%");
//				sql.append(customerProductVo.getSealerAutelId().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getAreaCfgName())) {
//				sql.append(" and f.name like '%");
//				sql.append(customerProductVo.getAreaCfgName().trim());
//				sql.append("%'");
//			}
//			if (customerProductVo.getProRegStatus() != null) {
//				sql.append(" and c.regStatus = ");
//				sql.append(customerProductVo.getProRegStatus().toString());
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getBeginDate())) {
//				sql.append(" and c.regTime >= '");
//				sql.append(customerProductVo.getBeginDate().trim());
//				sql.append("'");
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getEndDate())) {
//				sql.append(" and c.regTime < cast('");
//				sql.append(customerProductVo.getEndDate().trim());
//				sql.append("' as datetime)+1");
//			}
//		}
//		return this.daoSupport.queryForList(sql.toString(),
//				CustomerProductVo.class);
//	}
//
//	/**
//	 * 解绑客户注册产品
//	 * 
//	 * @param code
//	 * @return int
//	 * @throws Exception
//	 */
//	@Transactional
//	public int noRegPro(String code, String proNoRegReason, String createUser)
//			throws Exception {
//		String sql = "update DT_ProductInfo set regStatus=2,noRegReason=? ,noRegDate=date_format(NOW(),'%Y-%m-%d %H:%i:%s'),"
//				+ " createUser=? where code= ?";
//		this.daoSupport.execute(sql, proNoRegReason, createUser, code);
//		String delCustPro = "delete from dt_customerproinfo where proCode=?";
//		this.daoSupport.execute(delCustPro, code);
//		return 0;
//	}
//
//	/**
//	 * 客户注册产品分页显示方法
//	 * 
//	 * @param CustomerProductVo
//	 * @param pageNo
//	 * @param pageSize
//	 * @return
//	 * @throws Exception
//	 */
	@Override
	public Page pageCustomerProductVoPage(CustomerProductVo customerProductVo,
			int pageNo, int pageSize) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select * from (");
//
//		sql.append("select a.id,a.code,a.autelId,a.firstName,a.middleName,a.lastName,a.name,a.mobilePhone,a.mobilePhoneAC,a.mobilePhoneCC,a.daytimePhoneCC,a.daytimePhoneAC,a.daytimePhone,");
//		sql.append("d.code saleContractCode,e.code sealerCode,e.autelId sealerAutelId,f.code areaCfgCode,f.name areaCfgName,");
//		sql.append("c.code proCode,c.serialNo proSerialNo,c.proDate proDate,c.regTime proRegTime,c.regStatus proRegStatus,c.noRegReason proNoRegReason");
//		sql.append(" from DT_CustomerInfo a");
//		sql.append(" left join DT_CustomerProInfo b on a.code=b.customerCode");
//		sql.append(" left join DT_ProductInfo c on b.proCode=c.code");
//		sql.append(" left join DT_SaleContract d on c.saleContractCode=d.code");
//		sql.append(" left join DT_SealerInfo e on d.sealerCode=e.code");
//		sql.append(" left join DT_AreaConfig f on d.areaCfgCode=f.code");
//		sql.append(" where 1=1");
//
//		if (null != customerProductVo) {
//			if (StringUtils.isNotBlank(customerProductVo.getCode())) {
//				sql.append(" and a.code= '");
//				sql.append(customerProductVo.getCode().trim());
//				sql.append("'");
//			}
//
//			if (StringUtils.isNotBlank(customerProductVo.getAutelId())) {
//				sql.append(" and a.autelId like '%");
//				sql.append(customerProductVo.getAutelId().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getCity())) {
//				sql.append(" and a.city like '%");
//				sql.append(customerProductVo.getCity().trim());
//				sql.append("%'");
//			}
//
//			if (StringUtils.isNotBlank(customerProductVo.getProvince())) {
//				sql.append(" and a.province like '%");
//				sql.append(customerProductVo.getProvince().trim());
//				sql.append("%'");
//			}
//
//			if (StringUtils.isNotBlank(customerProductVo.getProSerialNo())) {
//				sql.append(" and c.serialNo like '%");
//				sql.append(customerProductVo.getProSerialNo().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getSealerAutelId())) {
//				sql.append(" and e.autelId like '%");
//				sql.append(customerProductVo.getSealerAutelId().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getAreaCfgName())) {
//				sql.append(" and f.name like '%");
//				sql.append(customerProductVo.getAreaCfgName().trim());
//				sql.append("%'");
//			}
//			if (customerProductVo.getProRegStatus() != null) {
//				sql.append(" and c.regStatus = ");
//				sql.append(customerProductVo.getProRegStatus().toString());
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getBeginDate())) {
//				sql.append(" and c.regTime >= '");
//				sql.append(customerProductVo.getBeginDate().trim());
//				sql.append("'");
//			}
//			if (StringUtils.isNotBlank(customerProductVo.getEndDate())) {
//				sql.append(" and c.regTime < cast('");
//				sql.append(customerProductVo.getEndDate().trim());
//				sql.append("' as datetime)+1");
//			}
//		}
//
//		sql.append(") m");
//
//		sql.append(" order by m.id desc");
//
//		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
//				pageSize, CustomerProductVo.class);
//		return page;
//		Map<String, Object> dataMap = new HashMap<String, Object>();
//		dataMap.put("customerProductVo", customerProductVo);
//		dataMap.put("page", pageNo);
//		dataMap.put("pageSize", pageSize);
//		Page page = HttpUtil.postRequestContentPage("rest.sealer.regProduct.pageCustomerProductVoPage", dataMap, CustomerProductVo.class);
		String jsonData = HttpUtil.postRequestContent("rest.sealer.regProduct.pageCustomerProductVoPage", "customerProductVo.", customerProductVo);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", CustomerProductVo.class);
		Page page = (Page) JsonUtil.getDTO(jsonData, Page.class, map);
		return page;
	}
//
	public int queryProductArea(String areaCode, String userCode){
//		StringBuffer sql = new StringBuffer();
//		sql.append("select count(1) from dt_customerproinfo a , ");
//		sql.append(" DT_productinfo b, ");
//		sql.append(" dt_saleContract c where a.proCode=b.code ");
//		sql.append(" and b.saleContractCode=c.code and  ");
//		sql.append(" c.areaCfgCode='" + areaCode + "' ");
//		sql.append(" and a.customerCode='" + userCode + "'");
//
//		return this.daoSupport.queryForInt(sql.toString(), null);
		int count = 0;
		try{
			String result = HttpUtil.getReqeuestContent("rest.user.queryProductArea", "areaCode="+areaCode+"&customerInfoCode="+userCode);
			count = Integer.valueOf(result);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	public CustomerProductVo getCustomerPro(String serialNo) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select a.code as proCode,b.warrantyMonth as Warrantymonth,a.serialNo as proSerialNo,date_format(a.proDate, '%m/%d/%Y') as proDate,date_format(a.regTime, '%m/%d/%Y') as proRegTime,");
//		sql.append(" c.autelId as sealerAutelId,d.name as language,a.regPwd as regPwd, s.name as productTypeName,");
//		sql.append(" (select distinct date_format(d.validDate, '%m/%d/%Y') from DT_ProductSoftwareValidStatus d where d.proCode=a.code) as validDate");
//		sql.append(" from dt_productinfo a,DT_SaleContract b,DT_SealerInfo c,dt_productforsealer s,DT_LanguageConfig d ");
//		sql.append(" where a.saleContractCode=b.code and b.sealerCode=c.code and b.languageCfgCode=d.code and a.proTypeCode=s.code");
//		sql.append(" and a.serialNo=?");
//		return this.daoSupport.queryForObject(sql.toString(),
//				CustomerProductVo.class, new String[] { serialNo });
		String jsonData = HttpUtil.getReqeuestContent("rest.customerProduct.getCustomerPro", "serialNo=" + serialNo);
		CustomerProductVo customerProductVo = (CustomerProductVo) JsonUtil.getDTO(jsonData, CustomerProductVo.class);
		return customerProductVo;
	}
//
//	@Override
//	public String getCustomerproinfo(String proCode) throws Exception {
//		String sql = "select a.customerCode as code from dt_customerproinfo a where proCode = '"
//				+ proCode + "' ";
//		return this.daoSupport.queryForString(sql);
//	}
}
