/**
 * 
 */
package com.cheriscon.user.service.impl;

//import java.util.Date;

import java.util.List;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.RGAInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.distributor.vo.RGAFrontVo;
import com.cheriscon.front.usercenter.distributor.vo.RGAInfoVo;
import com.cheriscon.user.service.IRGAInfoService;
//import com.cheriscon.backstage.member.service.IRGAInfoService;
//import com.cheriscon.common.constant.DBConstant;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.framework.util.DateUtil;
//import com.cheriscon.framework.util.StringUtil;

/**
 * @author A13045
 *
 */
@Service
public class RGAInfoServiceImpl  implements IRGAInfoService {
//
//
	@Override
	public Page pageRGAQuery(String auth,RGAInfo info, int pageNo, int pageSize) throws Exception {
		return null;
//		StringBuffer sql=new StringBuffer("");
//		sql.append("select a.code as code,a.createUser as createUser ,d.name as createUserName,");
//		sql.append("date_format(a.createDate, '%m/%d/%Y') as createDate,");
//		sql.append("a.serialNo as serialNo,a.status as status,DATEDIFF(NOW() ,createDate) as dayNum");
//		/*sql.append(",b.name as proTypeName");*/
//		sql.append(" from DT_RGAInfo a,DT_SealerDataAuthDeails c,dt_sealerInfo d " );
//		/*sql.append(" where a.proTypeCode=b.code ");*/
//		sql.append(" where a.createUser=d.autelId");
//		sql.append(" and a.createUser=c.sealerCode");
//		sql.append(" and c.authCode like'").append(auth).append("%'");
//		if(null != info ){
//			if(!StringUtil.isEmpty(info.getProTypeCode()) && !info.getProTypeCode().equals("-1")){
//				sql.append(" and a.proTypeCode like'%").append(info.getProTypeCode().trim()).append("%'");
//			}
//			if(!StringUtil.isEmpty(info.getSerialNo())){
//				sql.append(" and a.serialNo like '%").append(info.getSerialNo().trim()).append("%'");
//			}
//			if(!StringUtil.isEmpty(info.getCode())){
//				sql.append(" and a.code like '%").append(info.getCode().trim()).append("%'");
//			}
//		}
//		sql.append(" order by a.id desc");
//		
//		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}
	
	@SuppressWarnings("unchecked")
	public Page pageRGAQuery(RGAInfoVo rgaInfoVo) {
		String pageJson = HttpUtil.postRequestContent("rest.sealer.rga.pageRGAQuery", "rgaInfoVo.", rgaInfoVo);
		JSONObject jsonObject = new JSONObject();
		jsonObject = JSONObject.fromObject(pageJson);
		String result = jsonObject.getString("result");
//		{"currentPageNo":1,"pageSize":10,,"totalCount":3400,"totalPageCount":340}
		List<RGAFrontVo> list = JsonUtil.getDTOList(result, RGAFrontVo.class);
		String pageSize = jsonObject.getString("pageSize");
		String totalCount = jsonObject.getString("totalCount");
		String currentPageNo = jsonObject.getString("currentPageNo");
		Page page = new Page();
		page.setData(list);
		page.setPageSize(Integer.parseInt(pageSize));
		page.setTotalCount(Integer.parseInt(totalCount));
		page.setCurrentPageNo(Integer.parseInt(currentPageNo));
//		Page page = (Page) JsonUtil.getDTO(pageJson, Page.class);
		return page;
	}
	
	@Override
	public void saveRGAInfo(RGAInfo info) throws Exception {
//		info.setCode(getMaxNum());
//		this.daoSupport.insert("DT_RGAInfo", info);
		HttpUtil.postRequestContent("rest.sealer.rga.saveRGAInfo", "rgaInfo.", info);
	}
//
//	public String getMaxNum() throws Exception{
//		String sql="select * from dt_rgainfo order by id desc limit 0,1";
//		RGAInfo info=this.daoSupport.queryForObject(sql, RGAInfo.class);
//		String DateStr=DateUtil.toString(new Date(), "yyyyMMdd");
//		String RGACODE="";
//		if(info==null){
//			RGACODE= "RGA"+DateStr+"-"+"00001";
//		}else{
//			
//			String code=info.getCode();
//			int startLenth=code.indexOf("-");
//			String yyyymmdd=code.substring(3, startLenth);
//			if(DateStr.equals(yyyymmdd)){
//				String lastCode=code.substring(startLenth+1);
//				lastCode=String.valueOf(Integer.valueOf(lastCode)+1);
//				RGACODE="RGA"+DateStr+"-"+this.addZeroForNum(lastCode, 5);
//			}else{
//				RGACODE="RGA"+DateStr+"-"+"00001";
//			}
//			
//		}
//		return RGACODE;
//	}
//	
//	public  String addZeroForNum(String str, int strLength) {
//		int strLen = str.length();
//		if (strLen < strLength) {
//			while (strLen < strLength) {
//			StringBuffer sb = new StringBuffer();
//			sb.append("0").append(str);
//			str = sb.toString();
//			strLen = str.length();
//			}
//		}
//
//			return str;
//		}
//	
//	
	@Override
	public void updateRGAInfo(RGAInfo info)
			throws Exception {
//		this.daoSupport.update("DT_RGAInfo", info, "id="+info.getId()+"");
		HttpUtil.postRequestContent("rest.sealer.rga.updateRGAInfo", "rgaInfo.", info);
	}
//
//	@Override
//	public void deleteRGAInfo(String code) throws Exception {
//		String sql = "delete from DT_RGAInfo where code= ?";
//		this.daoSupport.execute(sql,code);
//
//	}
//
	public RGAInfo getRGAInfoByCode(String code) {
//		String sql="select a.*,d.name as createUserName from DT_RGAInfo a,dt_sealerInfo d where  a.createUser=d.autelId and a.code=?";
//		return this.daoSupport.queryForObject(sql, RGAInfo.class, code);
		return (RGAInfo)HttpUtil.getReqeuestContentObject("rest.rga.getRGAInfoByCode", "code="+code, RGAInfo.class);
	}
}
