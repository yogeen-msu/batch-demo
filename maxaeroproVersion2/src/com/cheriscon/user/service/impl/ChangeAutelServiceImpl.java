package com.cheriscon.user.service.impl;
//package com.cheriscon.front.user.service.impl;
//
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.user.service.IChangeAutelService;
//import com.cheriscon.common.constant.DBConstant;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.front.user.service.IChangeAutelService;
@Service
public class ChangeAutelServiceImpl 
//extends BaseSupport<CustomerChangeAutel>
implements IChangeAutelService {
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> activateAutelId(String autelId) {
		String jsonData = HttpUtil.getReqeuestContent("rest.customer.activateAutelId", "autelId=" + autelId);
		Map<String, Object> dataMap = JsonUtil.getMapFromJson(jsonData);
		return dataMap;
	}

//	@Override
//	public boolean addCustomerChangeAutel(CustomerChangeAutel changeAutel)
//			throws Exception {
//		changeAutel.setCode(DBUtils.generateCode(DBConstant.CUSTOMERINFO_CHANGE_CODE));
//		this.daoSupport.insert("DT_CustomerChangeAutel", changeAutel);
//		return false;
//	}
	
	public  CustomerChangeAutel getCustomerChangeAutel(String customerCode) throws Exception{
//		StringBuffer sql=new StringBuffer();
//		sql.append("select * from DT_CustomerChangeAutel where customerCode='");
//		sql.append(customerCode.trim());
//		sql.append("'");
//		return this.daoSupport.queryForObject(sql.toString(), CustomerChangeAutel.class, null);
		String jsonData = HttpUtil.getReqeuestContent(
				"rest.customer.ChangeAutel", "customerCode=" + customerCode);
		return (CustomerChangeAutel) JsonUtil.getDTO(jsonData, CustomerChangeAutel.class);
	}
//	@Override
	public int getCustomerInfoNumByIdAndSrouce(String autelId) throws Exception{
//		StringBuffer sql=new StringBuffer();
//		sql.append("select count(id) from DT_CustomerInfo a where a.autelId=?");
//		sql.append(" and  (EXISTS(select 1  from DT_CustomerProInfo b where a.code=b.customerCode )");
//		sql.append(" or a.sourceType!=").append(FrontConstant.CUSTOMER_SOURCE_TYPE_ENT).append(")");
//		return this.daoSupport.queryForInt(sql.toString(), new Object[]{autelId});
		return 0;
	}
//	@Override
//	public boolean updateCustomerChange(CustomerChangeAutel changeAutel) throws Exception {
		
//		this.daoSupport.update("DT_CustomerChangeAutel", changeAutel, "id='"+changeAutel.getId()+"'");
//		return true;
//	}


}
