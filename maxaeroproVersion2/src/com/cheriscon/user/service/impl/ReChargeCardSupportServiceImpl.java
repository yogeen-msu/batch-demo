package com.cheriscon.user.service.impl;

import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.user.service.IReChargeCardSupportService;


//import java.util.Date;

//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

//import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
//import com.cheriscon.backstage.member.service.IReChargeCardErrorLogService;
//import com.cheriscon.backstage.member.service.IReChargeCardSupportService;
//import com.cheriscon.common.constant.DBConstant;
//import com.cheriscon.common.model.ReChargeCardErrorLog;
import com.cheriscon.common.model.ReChargeCardSupport;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.common.utils.SessionUtil;
//import com.cheriscon.cop.resource.model.AdminUser;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.framework.util.DateUtil;
//import com.cheriscon.framework.util.StringUtil;

@Service
public class ReChargeCardSupportServiceImpl  implements
IReChargeCardSupportService {
//
//	@Resource
//	private IAdminUserManager adminUserManager;
//	
//	@Override
//	public Page pageReChargeCardSupport(ReChargeCardSupport support,
//			int pageNo, int pageSize) throws Exception {
//		StringBuffer sql=new StringBuffer("select * from DT_ReChargeCardSupport where 1=1 ");
//		if(null!=support){
//			if(!StringUtil.isEmpty(support.getCardSN())){
//				sql.append(" and cardSN like '%"+support.getCardSN().trim()+"%'");
//			}
//			if(!StringUtil.isEmpty(support.getCardPwd())){
//				sql.append(" and cardPwd like '%"+support.getCardPwd().trim()+"%'");
//			}
//		}
//		
//		sql.append(" order by status,createTime desc");
//		
//		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, ReChargeCardSupport.class);
//	}

	@Override
	public boolean saveCardSupport(ReChargeCardSupport support)  {
		String result = HttpUtil.postRequestContent("rest.user.saveCardSupport", "support.", support);
		if("success".equals(result)){
			return true;
		}
		return false;
	}

//
//	@Override
//	public ReChargeCardSupport getErrorLogByCode(String code) throws Exception {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void updateCardSupportStatus(String code) throws Exception {
//		StringBuffer sql=new StringBuffer();
//		AdminUser user =adminUserManager.getCurrentUser();
//		String userName=user.getUsername();
//		String updateTime=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss");
//		sql.append("update DT_ReChargeCardSupport set status=?,updateUser=?,updateTime=? where code=?");
//		this.daoSupport.execute(sql.toString(), new Object[]{"1",userName,updateTime,code});
//		
//	}

}
