package com.cheriscon.user.service;

import com.cheriscon.common.model.RGAInfo;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.distributor.vo.RGAInfoVo;
//import com.cheriscon.framework.database.Page;

public interface IRGAInfoService {

	
	//分页展示
	public Page pageRGAQuery(String auth,RGAInfo info, int pageNo,int pageSize) throws Exception;
	
	public Page pageRGAQuery(RGAInfoVo rgaInfoVo) throws Exception;
//	
	public void saveRGAInfo(RGAInfo info) throws Exception;
//	
	public void updateRGAInfo(RGAInfo info) throws Exception;
//	
//	public void deleteRGAInfo(String code) throws Exception;
	
	public RGAInfo getRGAInfoByCode(String code) ;
}
