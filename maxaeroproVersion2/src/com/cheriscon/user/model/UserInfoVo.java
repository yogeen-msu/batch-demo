package com.cheriscon.user.model;

import java.io.Serializable;

/**
 * 用户信息（包括个人用户、经销商用户）vo
 * @author yangpinggui 2013-1-16
 *
 */
public class UserInfoVo implements  Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3261486843766516244L;

	private int id;
	
	/**
	 * 用户类型
	 */
	private int userType;
	
	/**
	 * 用户code
	 */
	private String code;
	
	/**
	 * 用户账号（邮箱）
	 */
	private String autelId;
	
	/**
	 * 用户密码
	 */
	private String userPwd;
	
	/**
	 * 验证码
	 */
	private String imageCode;
	
	/**
	 * 用户旧密码
	 */
	private String oldPassword;
	
	/**
	 * 第一个名字
	 */
	private String firstName;			
	
	/**
	 * 中间名字
	 */
	private String middleName;			
	
	/**
	 * 最后名字
	 */
	private String lastName;			
	
	/**
	 * 固定电话
	 */
	private String daytimePhone;		
	
	/**
	 * 固定电话国家代码
	 */
	private String daytimePhoneCC;		
	
	/**
	 * 固定电话区域代码
	 */
	private String daytimePhoneAC;		
	
	/**
	 * 固定电话分机号
	 */
	private String daytimePhoneExtCode;	
	
	/**
	 * 移动电话
	 */
	private String mobilePhone;			
	
	/**
	 * 移动电话国家代码
	 */
	private String mobilePhoneCC;		
	
	/**
	 * 移动电话区域代码
	 */
	private String mobilePhoneAC;	
	
	/**
	 * 用户姓名
	 */
	private String comUsername;
			
	/**
	 * 语言code
	 */
	private String languageCode;		
	
	/**
	 * 国家
	 */
	private String country;				
	
	/**
	 * 城市
	 */
	private String city;				
	
	/**
	 * 地址
	 */
	private String address;				
	
	/**
	 * 公司
	 */
	private String company;				
	
	/**
	 * 邮编
	 */
	private String zipCode;				
	
	/**
	 * 安全问题code
	 */
	private String questionCode;		
	
	/**
	 * 	安全问题答案
	 */
	private String answer;			
				
	
	/**
	 * 是否允许发邮件
	 */
	private Integer isAllowSendEmail;
	
	/**
	 * 用户第一邮箱
	 */
	private String email;
	
	/**
	 * 用户第二邮箱
	 */
	private String secondEmail;
	
	/**
	 * 发送邮件时的唯一识别码
	 */
	private String actCode;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDaytimePhone() {
		return daytimePhone;
	}

	public void setDaytimePhone(String daytimePhone) {
		this.daytimePhone = daytimePhone;
	}

	public String getDaytimePhoneCC() {
		return daytimePhoneCC;
	}

	public void setDaytimePhoneCC(String daytimePhoneCC) {
		this.daytimePhoneCC = daytimePhoneCC;
	}

	public String getDaytimePhoneAC() {
		return daytimePhoneAC;
	}

	public void setDaytimePhoneAC(String daytimePhoneAC) {
		this.daytimePhoneAC = daytimePhoneAC;
	}

	public String getDaytimePhoneExtCode() {
		return daytimePhoneExtCode;
	}

	public void setDaytimePhoneExtCode(String daytimePhoneExtCode) {
		this.daytimePhoneExtCode = daytimePhoneExtCode;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMobilePhoneCC() {
		return mobilePhoneCC;
	}

	public void setMobilePhoneCC(String mobilePhoneCC) {
		this.mobilePhoneCC = mobilePhoneCC;
	}

	public String getMobilePhoneAC() {
		return mobilePhoneAC;
	}

	public void setMobilePhoneAC(String mobilePhoneAC) {
		this.mobilePhoneAC = mobilePhoneAC;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getIsAllowSendEmail() {
		return isAllowSendEmail;
	}

	public void setIsAllowSendEmail(Integer isAllowSendEmail) {
		this.isAllowSendEmail = isAllowSendEmail;
	}

	public String getComUsername() {
		return comUsername;
	}

	public void setComUsername(String comUsername) {
		this.comUsername = comUsername;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImageCode() {
		return imageCode;
	}

	public void setImageCode(String imageCode) {
		this.imageCode = imageCode;
	}

	public String getSecondEmail() {
		return secondEmail;
	}

	public void setSecondEmail(String secondEmail) {
		this.secondEmail = secondEmail;
	}

	public String getActCode() {
		return actCode;
	}

	public void setActCode(String actCode) {
		this.actCode = actCode;
	}
	
	
	
}
