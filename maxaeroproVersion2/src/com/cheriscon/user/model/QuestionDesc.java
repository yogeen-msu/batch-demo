package com.cheriscon.user.model;

/**
 * 用户账号安全问题
 * 
 * @author pengdongan
 */
public class QuestionDesc implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6980131290522439420L;
	private Integer id;
	private String questionCode;
	private String languageCode;
	private String questionDesc;
	
	

 	/**  非表字段  begin */
	
	private String languageName;					//语言类型名称

 	/**  非表字段  end */

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getQuestionDesc() {
		return this.questionDesc;
	}

	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

 	/**  非表字段  begin */

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

 	/**  非表字段  end */

	
	

}