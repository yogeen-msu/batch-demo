package com.cheriscon.user.model;

import java.io.Serializable;

/**
 * 客诉信息vo
 * @author yangpinggui 2013-1-16
 *
 */
public class ReCustomerComplaintInfoVO implements  Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8302477219708718745L;
	
	private int id;
	
	/**
	 * 客诉code
	 */
	private String cusComCode;
	
	/**
	 * 客诉回复code
	 */
	private String code;
	
	/**
	 * 回复内容
	 */
	private String reContent;
	
	/**
	 * 用户类型
	 */
	private int userType;
	
	/**
	 * 客诉状态
	 */
	private int acceptState;
	
	/**
	 * 附件
	 */
	private String attachment;
	
	/**
	 * 用户账号
	 */
	private String autelId;
	
	/**
	 *  客诉评价分数
	 */
	private Integer complaintScore;   
	


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCusComCode() {
		return cusComCode;
	}

	public void setCusComCode(String cusComCode) {
		this.cusComCode = cusComCode;
	}

	public String getReContent() {
		return reContent;
	}

	public void setReContent(String reContent) {
		this.reContent = reContent;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public int getAcceptState() {
		return acceptState;
	}

	public void setAcceptState(int acceptState) {
		this.acceptState = acceptState;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getComplaintScore() {
		return complaintScore;
	}

	public void setComplaintScore(Integer complaintScore) {
		this.complaintScore = complaintScore;
	}


	
	
	
	
	

}
