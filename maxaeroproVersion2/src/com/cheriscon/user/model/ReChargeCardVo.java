package com.cheriscon.user.model;

import java.io.Serializable;

/**
 * 给客户充值vo
 * @author yangpinggui 2013-1-16
 *
 */
public class ReChargeCardVo implements  Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8472935852249202277L;
	
	/**
	 * 产品序列号
	 */
	private String proSerialNo;
	
	/**
	 * 产品code
	 */
	private String proCode;
	
	/**
	 * 产品型号code
	 */
	private String proTypeCode;
	
	/**
	 * 充值卡序列号
	 */
	private String reChargeCardSerialNo;
	
	/**
	 * 充值卡密码
	 */
	private String reChargeCardPassword;
	
	/**
	 * 续租产品
	 */
	private String proName;
	
	/**
	 * 续租时间
	 */
	private int reChargeTime;
	
	/**
	 * 会员名称
	 */
	private String memberName;
	
	/**
	 * 会员类型
	 */
	private int memberType;
	
	/**
	 * 最小销售单位code
	 */
	private String minSaleUnitCode;
	
	/**
	 * 区域配置Code
	 */
	private String areaCfgCode;
	
	/**
	 * 标准销售配置Code
	 */
	private String saleCfgCode;

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getReChargeCardSerialNo() {
		return reChargeCardSerialNo;
	}

	public void setReChargeCardSerialNo(String reChargeCardSerialNo) {
		this.reChargeCardSerialNo = reChargeCardSerialNo;
	}

	public String getReChargeCardPassword() {
		return reChargeCardPassword;
	}

	public void setReChargeCardPassword(String reChargeCardPassword) {
		this.reChargeCardPassword = reChargeCardPassword;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public int getReChargeTime() {
		return reChargeTime;
	}

	public void setReChargeTime(int reChargeTime) {
		this.reChargeTime = reChargeTime;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public int getMemberType() {
		return memberType;
	}

	public void setMemberType(int memberType) {
		this.memberType = memberType;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public String getSaleCfgCode() {
		return saleCfgCode;
	}

	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}
	
	
	
	
	
}
