package com.cheriscon.user.model;
/**
 * 用户消息vo
 * @author yangpinggui 2013-3-6
 *
 */
public class UserMessageVo 
{
	private Integer userType; //用户类型  1：个人用户 2：经销商用户
	private String userCode;  //用户code
	private String messageCode; //消息code
	
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	
	
}
