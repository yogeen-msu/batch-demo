package com.cheriscon.user.model;

public class SystemErrorLog {
	
	private Integer id;
	private String userCode;
	private String operateIp;
	private String operateTime;
	private String errorMsg;
	private String type;
	public Integer getId() {
		return id;
	}
	public String getUserCode() {
		return userCode;
	}
	public String getOperateIp() {
		return operateIp;
	}
	public String getOperateTime() {
		return operateTime;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public String getType() {
		return type;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public void setOperateIp(String operateIp) {
		this.operateIp = operateIp;
	}
	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public void setType(String type) {
		this.type = type;
	}

}
