package com.cheriscon.front.filter;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cheriscon.framework.util.FileUtil;

public class GoogleLoginFilter implements Filter {

	private static String serverUrl;
	private static final String CLIENT_ID = "312310724639-igs03e629ctk8mmreqb96gd6bsa2310g.apps.googleusercontent.com";
	private static final String CLIENT_SECRET = "K61Q3rCM3Y1Obr7_kRhYx-9e";
	
	public void destroy() {
		
	}

	@SuppressWarnings("unused")
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		
		ServletContext sc = request.getServletContext();
		WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(sc);
		
		HttpSession session = request.getSession();
		String code = request.getParameter("code");
		String token = getAuthByCode(code);
	}

	public void init(FilterConfig arg0) throws ServletException {
		InputStream input = FileUtil.getResourceAsStream("cop.properties");
		Properties properties = new Properties();
		try {
			properties.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		serverUrl = properties.getProperty("serverName");
	}

	private String getAuthByCode(String code) throws ClientProtocolException, IOException {
		String url = "https://accounts.google.com/o/oauth2/token";
//		String url = "https://www.googleapis.com/oauth2/v4/token";
		HttpPost post = new HttpPost(url);
		post.addHeader("Content-Type", "application/x-www-form-urlencoded");
//		post.addHeader("content-type", "application/json");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("code", code);
		jsonObject.put("client_id", CLIENT_ID);
		jsonObject.put("client_secret", CLIENT_SECRET);
		jsonObject.put("redirect_uri", serverUrl + "/oauth2callback.do");
		jsonObject.put("scope", "email");
		jsonObject.put("grant_type", "authorization_code");
		StringEntity entity = new StringEntity(jsonObject.toString(), "UTF-8");
//		entity.setContentType("application/json");
		entity.setContentType("application/x-www-form-urlencoded");
		post.setEntity(entity);
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse response = httpClient.execute(post);
		String result = "";
		System.out.println("response entity = " + EntityUtils.toString(response.getEntity(), "UTF-8"));
		System.out.println("response = " + response);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			result = EntityUtils.toString(response.getEntity(), "UTF-8");
		}
		return result;
	}
	
}
