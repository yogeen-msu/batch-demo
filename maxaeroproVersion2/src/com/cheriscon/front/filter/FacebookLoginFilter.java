package com.cheriscon.front.filter;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.UserLoginInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.user.service.ICustomerInfoService;
import com.cheriscon.user.service.IUserLoginInfoService;

public class FacebookLoginFilter implements Filter {

	private ICustomerInfoService customerInfoService;
	
	private static final String appId = "180924732273214";
	private static final String secret = "f03e853735c60df323537a259de433e5";
	private static String appUrl;
	
	private Log log = LogFactory.getLog(FacebookLoginFilter.class);
	
	public void destroy() {  
		
	}

	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		
		ServletContext sc = request.getServletContext();
		WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(sc);
		customerInfoService = (ICustomerInfoService) wac.getBean("customerInfoServiceImpl");
		IMyAccountService myAccountService = (IMyAccountService) wac.getBean("myAccountServiceImpl");
//		ILanguageService languageService = (ILanguageService) wac.getBean("languageServiceImpl");
		IUserLoginInfoService userLoginInfoService = (IUserLoginInfoService) wac.getBean("userLoginInfoServiceImpl");
		
		HttpSession session = request.getSession();
		session.setAttribute("appId", appId);
		session.setAttribute("secret", secret);
		session.setAttribute("appUrl", appUrl);
		String code = request.getParameter("code");
		CustomerInfo customerInfo = null;
		if (code != null) {
			String accessToken = (String) session.getAttribute("accessToken");
			String expires = (String) session.getAttribute("expires");
            if ((null == accessToken || "".equals(accessToken.trim())) || null == expires) {  
            	String result = getAuthByCode(code);
                log.info("facebook登录的时候读取授权URL后的返回内容：\n" + result);  
  
                String[] pairs = result.split("&");  
                String[] keyValue;  
                for (String pair : pairs) {  
                    keyValue = pair.split("=");  
                    if (keyValue.length != 2) {  
                        log.error("facebook访问授权URL获取keyValue对出错,原因：\n",  
                                new RuntimeException("Unexpected auth response"));  
                    } else {  
                        //获取accessToken  
                        if (keyValue[0].equals("access_token")) {  
                            accessToken = keyValue[1];  
                            if (accessToken != null) {  
                                //将accessToken放入session，避免重复获取  
                                session.setAttribute("accessToken",accessToken);  
                                JSONObject json = getJsonByAccessToken(accessToken);  
                                customerInfo = translationFacebookAccount(json);  
                                SessionUtil.setLoginUserInfo(request, customerInfo);
                                
//                                Language language = languageService.getByCode(customerInfo.getLanguageCode());
                                Integer proCount = myAccountService.queryProductCount(customerInfo.getCode());
                                request.getSession().setAttribute("proCount", proCount);
                                request.getSession().setAttribute("userType", "1");
                                
                                UserLoginInfo userLoginInfo = new UserLoginInfo();
                                userLoginInfo.setUserCode(customerInfo.getCode());
                                userLoginInfo.setLoginIp(request.getRemoteAddr());
                                userLoginInfo.setLoginTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
                                userLoginInfo.setUserType(1);
                                try {
									userLoginInfoService.addUserLoginInfo(userLoginInfo);
								} catch (Exception e) {
									e.printStackTrace();
								}
                                
                            } else {  
                                log.info("facebook 获取accessToken失败：\naccessToken=" + accessToken);  
                            }  
                        }  
                          
                        //获取expires  
                        if (keyValue[0].equals("expires")) {  
                            expires = Integer.valueOf(keyValue[1]) + "";  
                            //将expires放入session，避免重复获取  
                            session.setAttribute("expires", expires);  
                        }  
                    }  
                }  
            }  
            session.setAttribute("facebookAccount", customerInfo);
            response.sendRedirect(FreeMarkerPaser.getBundleValue("autelproweb.url")+"/myAccount.html?userType=1&operationType=5");
        } else {  
        	arg2.doFilter(request, response);  
        }  
    }  
  
	private String getAuthByCode(String code) throws ParseException, IOException {
		String url = "https://graph.facebook.com/oauth/access_token?client_id="  
              + appId + "&redirect_uri=" + appUrl + "&client_secret="  
              + secret + "&code=" + code;
		HttpGet httpGet = new HttpGet(url);
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse response = httpClient.execute(httpGet);
		return EntityUtils.toString(response.getEntity(), "UTF-8");
	}
  
    /** 
     * 根据获得的访问标记accessToken访问获取用户个人信息的json数据 
     *  
     * @param accessToken 
     *            访问标记 
     * @return 
     */  
    @SuppressWarnings("static-access")
	private JSONObject getJsonByAccessToken(String accessToken) {
    	HttpGet httpGet = new HttpGet("https://graph.facebook.com/me?access_token=" + accessToken);
    	CloseableHttpClient client = HttpClientBuilder.create().build();
    	HttpResponse httpResponse;
    	String response = "";
		try {
			httpResponse = client.execute(httpGet);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				response = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	JSONObject json = new JSONObject();
    	log.info("根据访问标记accessToken访问获取用户个人信息的json数据:\n" + response); 
    	return json.fromObject(response);
    }  
  
    /** 
     * 根据用户个人信息的json数据，进行我们需要的本地化处理，比如：存入数据库等等 
     *  
     * @param json 
     *            用户个人信息的json数据 
     * @return 
     */  
    private CustomerInfo translationFacebookAccount(JSONObject jsonObject) {
    	String id = jsonObject.getString("id");
    	CustomerInfo customerInfo = customerInfoService.getByFacebookId(id);
    	if (customerInfo == null) {
    		customerInfo = new CustomerInfo();
    		String name = jsonObject.getString("name");
    		if (jsonObject.containsKey("email")) {
    			String email = jsonObject.getString("email");
    			customerInfo.setAutelId(email);
    		} else {
    			customerInfo.setAutelId(id);
    		}
    		customerInfo.setName(name);
    		customerInfo.setFacebookId(id);
    		
//    	customerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword("111111"));
    		customerInfo.setRegTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
    		customerInfo.setLanguageCode("lag201304221104540500");
    		customerInfo.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
    		customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
    		customerInfo.setLastLoginTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
    		customerInfo.setSourceType(FrontConstant.CUSTOMER_SOURCE_TYPE_PRO);
//    	customerInfo.setSendActiveTime(DateUtil.toString(new Date(), 
//				  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
    		try {
    			customerInfoService.addCustomer(customerInfo);
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    	return customerInfo;
    }
    
	public void init(FilterConfig arg0) throws ServletException {
		appUrl = FreeMarkerPaser.getBundleValue("autelproweb.url")+"/facebookLoginFilter.do";
	}
	
	
}
