package com.cheriscon.front.constant;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.cheriscon.framework.util.FileUtil;

/**
 * 产品网站前台常量类
 * @author yangpinggui
 * @version 创建时间：2013-1-7
 */
public class FrontConstant 
{
	
	/**
	 * 查询所有 
	 */
	public static final int QUERY_ALL = -1;
	
	/**
	 * 个人用户
	 */
	public static final int CUSTOMER_USER_TYPE = 1;
	
	/**
	 * 经销商用户
	 */
	public static final int DISTRIBUTOR_USER_TYPE = 2;
	
	/**
	 * 客诉状态-打开
	 */
	public static final int CUSTOMER_COMPLAINT_OPEN_STATE = 1;
	
	/**
	 * 客诉状态-挂起
	 */
	public static final int CUSTOMER_COMPLAINT_GUAQI_STATE = 2;
	
	/**
	 * 客诉状态-等待回复
	 */
	public static final int CUSTOMER_COMPLAINT_WAIT_STATE = 3;
	
	/**
	 * 客诉状态-关闭
	 */
	public static final int CUSTOMER_COMPLAINT_CLOSE_STATE = 4;
	
	/**
	 * 投诉有效
	 */
	public static final int COMPLAINT_YES_STATE = 1;
	
	/**
	 * 投诉无效
	 */
	public static final int COMPLAINT_NO_STATE = 2;
	
	/**
	 * 客诉受理状态-未受理
	 */
	public static final int COMPLAINT_NOACCEPT_STATE = 0;
	
	/**
	 * 客诉受理状态-已受理
	 */
	public static final int COMPLAINT_ACCEPTED_STATE = 1;
	
	/**
	 * 添加操作
	 */
	public static final int ADD_OPERATION_TYPE = 1;
	
	/**
	 * 修改操作
	 */
	public static final int UPDATE_OPERATION_TYPE = 2;
	
	/**
	 * 删除操作
	 */
	public static final int DELETE_OPERATION_TYPE =3;
	
	/**
	 * 查询操作
	 */
	public static final int QUERY_OPERATION_TYPE = 4;
	
	/**
	 * 用户状态 0 未激活
	 */
	public static final int USER_STATE_NOT_ACTIVE = 0;
	

	/**
	 * 用户状态 1 已激活
	 */
	public static final int USER_STATE_YES_ACTIVE = 1;
	
	/**
	 * 查询最近三个月消息
	 */
	public static final int LATELY_THREE_MONTH = 1;
	
	/**
	 * 查询三个月前消息
	 */
	public static final int BEFORE_THREE_MONTH = 2;
	
	/**
	 * 保存登录用户验证码字段
	 */
	public static final String SESSION_IMAGE_CODE_NAME = "valid_code_customer";
	
	/**
	 * 充值卡未使用
	 */
	public static final int CARD_SERIAL_IS_NO_USE = 1;
	
	/**
	 * 充值卡已使用
	 */
	public static final int CARD_SERIAL_IS_YES_USE = 2;
	
	/**
	 * 订单状态-已取消
	 */
	public static final int ORDER_STATE_NO_DONE = 0;
	
	/**
	 * 订单状态-有效
	 */
	public static final int ORDER_STATE_YES_DONE = 1;
	

	/**
	 * 订单支付状态-未支付
	 */
	public static final int ORDER_PAY_STATE_NO = 0;
	
	/**
	 * 订单支付状态-已支付
	 */
	public static final int ORDER_PAY_STATE_YES = 1;
	
	
	/**
	 * 订单支付状态-已退款
	 */
	public static final int ORDER_PAY_STATE_BACK = 2;
	
	
	/**
	 * 订单支付状态-已回款
	 */
	public static final int ORDER_PAY_STATE_REBACK = 3;
	
	/**
	 * 时间格式
	 */
	public static final String TIMEFORMAT_YYYY_MM_DD_HH_MM_SS ="yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 时间格式
	 */
	public static final String TIMEFORMAT_YYYY_MM_DD ="yyyy-MM-dd";
	
	/**
	 * 时间拆分符
	 */
	public static final String DATE_CHARACTER_SPLIT_STRING = "-";
	
	/**
	 * 充值卡未激活
	 */
	public static final int CHARGE_CARE_IS_NO_ACTIVE = 1;
	
	/**
	 * 充值卡已激活
	 */
	public static final int CHARGE_CARE_IS_YES_ACTIVE = 2;
	
	/**
	 * 充值卡未注册
	 */
	public static final int CHARGE_CARE_IS_NO_REG = 0;
	
	/**
	 * 充值卡已注册
	 */
	public static final int CHARGE_CARE_IS_YES_REG = 1;
	
	/**
	 * 用户消息未读
	 */
	public static final int USER_MESSAGE_IS_NO_READ = 0;
	
	/**
	 * 用户消息已读
	 */
	public static final int USER_MESSAGE_IS_YES_READ = 1;
	
	/**
	 * 用户所有消息包括弹出与未弹出
	 */
	public static final int USER_MESSAGE_IS_ALL_DIALOG = -1;
	
	/**
	 * 用户弹出消息
	 */
	public static final int USER_MESSAGE_IS_YES_DIALOG = 1;
	
	/**
	 * 活动已启用
	 */
	public static final int MARKET_PROMOTION_IS_START = 1;
	/**
	 * 活动已终止
	 */
	public static final int MARKET_PROMOTION_IS_END = 2;
	
	/**
	 * 活动类型为限时购买
	 */
	public static final int MARKET_PROMOTION_TYPE_IS_CONFINE = 1;
	
	/**
	 * 活动类型为越早越便宜
	 */
	public static final int MARKET_PROMOTION_TYPE_IS_CHEAP = 2;
	/**
	 * 活动类型为套餐
	 */
	public static final int MARKET_PROMOTION_TYPE_IS_SETMEAL = 3;
	/**
	 * 活动类型为过期续租
	 */
	public static final int MARKET_PROMOTION_TYPE_IS_PAST = 4;
	
	/**
	 * 消费类型为续租
	 */
	public static final int USER_IS_SOFT_RENT = 1;
	/**
	 * 消费类型为购买
	 */
	public static final int USER_IS_SOFT_BUY = 0;
	
	/**
	 * 产品问题
	 */
	public static final int CUSTOMER_COMPLAINT_PRODUCT_PROBLEM = 1;
	
	/**
	 * 软件问题
	 */
	public static final int CUSTOMER_COMPLAINT_SOFTWARE_PROBLEM = 2;
	
	/**
	 * 硬件问题
	 */
	public static final int CUSTOMER_COMPLAINT_HARDWARE_PROBLEM = 3;
	
	/**
	 * 其它问题
	 */
	public static final int CUSTOMER_COMPLAINT_OTHER_PROBLEM = 4;
	
	/**产品注册状态 ： 未注册  */
	public static final int PRODUCT_REGSTATUS_NO	= 0;
	/**产品注册状态 ： 已注册  */
	public static final int PRODUCT_REGSTATUS_YES	= 1;
	/**产品注册状态 ： 已解绑  */
	public static final int PRODUCT_REGSTATUS_UNBINDED	= 2;
	
	/**
	 * 客诉回复人类型 - 普通用户
	 */
	public static final int RECUSTOMER_COMPLAINT_CUSTOMER_USER = 1;
	
	/**
	 * 客诉回复人类型 - 系统客服人员
	 */
	public static final int RECUSTOMER_COMPLAINT_SYSTEM_USER = 2;
	
	/**
	 * MAXIFLASH PRO  type :类型 1 蓝牙盒子 2 J23534盒子
	 */
	public static final int PRODUCTFORPRO_TYPE_BLUETOOTHBOX = 1;
	
	/**
	 * MAXIFLASH PRO type :类型 1 蓝牙盒子 2 J23534盒子
	 */
	public static final int PRODUCTFORPRO_TYPE_J23534BOX = 2;
	
	/**
	 * MAXIFLASH PRO  regStatus :绑定状态 0 为未绑定 1 为绑定
	 */
	public static final int PRODUCTFORPRO_STATUS_BOUND_NO = 0;
	
	/**
	 * MAXIFLASH PRO regStatus :绑定状态 0 为未绑定 1 为绑定
	 */
	public static final int PRODUCTFORPRO_STATUS_BOUND_YES = 1;
	
	/**
	 * 支付验证结果 --错误
	 */
	public static final int PAY_FALSE=0;
	/**
	 * 支付验证结果 --正确
	 */
	public static final int PAY_TRUE=1;
	
	/**
	 * 版本发布状态-已发布
	 */
	public static final int SOFTWARE_VERSION_RELEASE_IS_STATE=1;
	
	/**
	 * 版本发布状态-未发布
	 */
	public static final int SOFTWARE_VERSION_RELEASE_NO_STATE=0;
	
	/**
	 * session 用户类型标识
	 */
	public static final String SESSION_USER_TYPE_FLAG = "userType";
	
	/**
	 * 产品网站注册用户
	 * */
	public static int CUSTOMER_SOURCE_TYPE_PRO=0;
	/**
	 * 企业网站注册用户
	 * */
	public static int CUSTOMER_SOURCE_TYPE_ENT=1;
	/**
	 * MaciDas注册用户
	 * */
	public static int CUSTOMER_SOURCE_TYPE_DAS=2;
	
	/**
	 * 客诉来源-客户自己添加
	 */
	public static final int COMPLAINT_SOURCE_CUST = 0;
	/**
	 * 客诉来源-经销商添加
	 */
	public static final int COMPLAINT_SOURCE_SEALER = 1;
	/**
	 * 后台请求的sessionid
	 */
	public static final String SERVER_SESSION="_server_session";
	
	
	public static String getProperValue(String code){
		String retValue="";
		Properties prop = new Properties(); 
		InputStream in=FileUtil.getResourceAsStream("config.properties");
		 try { 
	            prop.load(in);
	            retValue= prop.getProperty(code).trim(); 
		 }catch(Exception e){
			 e.printStackTrace(); 
		 }
		 return retValue;
	}
	
	public static String getPropertValue(String key) throws IOException{
		InputStream in = FileUtil.getResourceAsStream("config.properties");
		Properties properties = new Properties();
		properties.load(in);
		return properties.getProperty(key)==null?"":properties.getProperty(key);
	}
	
	public static String getComplaintStatus(String countryCode,Integer status){
		String retValue="";
		Properties prop = new Properties(); 
		InputStream in=null;
		if(countryCode.toUpperCase().equals("CN")){
			   in = FileUtil.getResourceAsStream("backstage_zh_CN.properties");
			 
			 
		}else{
			 in =  FileUtil.getResourceAsStream("backstage_en_US.properties"); 
		}
		 try { 
	            prop.load(in); 
	            if(status.equals(CUSTOMER_COMPLAINT_OPEN_STATE)){
					retValue= prop.getProperty("customercomplaint.complaintstate.open2").trim(); 
				}if(status.equals(CUSTOMER_COMPLAINT_GUAQI_STATE)){
					retValue= prop.getProperty("customercomplaint.complaintstate.guaqi2").trim(); 
				}if(status.equals(CUSTOMER_COMPLAINT_WAIT_STATE)){
					retValue= prop.getProperty("customercomplaint.complaintstate.waitreply2").trim(); 
				}if(status.equals(CUSTOMER_COMPLAINT_CLOSE_STATE)){
					retValue= prop.getProperty("customercomplaint.complaintstate.close2").trim(); 
				}
	        } catch (IOException e) { 
	            e.printStackTrace(); 
	        } 
			
		return retValue;
	}
	
	public static String getCMS(){
		String retValue="";
		Properties prop = new Properties(); 
		InputStream in = FileUtil.getResourceAsStream("backstage_en_US.properties");
		try { 
            prop.load(in);
            retValue=prop.getProperty("autelcms.path").trim(); 
		} catch (IOException e) { 
            e.printStackTrace(); 
        } 
		return retValue;
		
	}
	
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Real-IP");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return ip;
		}
		ip = request.getHeader("X-Forwarded-For");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {

			// 多次反向代理后会有多个IP值，第一个为真实IP。
			int index = ip.indexOf(',');
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		} else {
			return request.getRemoteAddr();
		}
	}
	
}
