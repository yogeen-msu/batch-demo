package com.cheriscon.front.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.FileUtil;

public class FreshDeskSSOServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8693048803872432675L;
	
//	private final String freshDeskUrl = "https://elliotsspcompany.freshdesk.com/login/sso";
//	private static final String key = "9e163029c2849199df7abd9904e4da64";//测试地址
	private static final String key = "10145f12c8ae16556588a167da95e950";//正式环境

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		CustomerInfo customerInfo = (CustomerInfo) SessionUtil.getLoginUserInfo(req);
		if (customerInfo != null) {
			try {
				String url = getSSOUrl(customerInfo);
				resp.sendRedirect(url);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		} 
		else {
			String server = getPropertiesValue("serverName");
			req.getSession().setAttribute("forwardUrl", getPropertiesValue("freshdesk.notlogin.url") + "/support/login");
			resp.sendRedirect(server + "/login.html");
		}
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		CustomerInfo customerInfo =  new CustomerInfo();
		customerInfo.setAutelId("gaoyaxiong0603@gmail.com");
		customerInfo.setLastName("gao");
		System.out.println(new FreshDeskSSOServlet().getSSOUrl(customerInfo));
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doPost(req, resp);
	}
	
	private static String getHash(String params) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("MD5");
		digest.update(params.getBytes(), 0, params.length());
		return "" + new BigInteger(1, digest.digest()).toString(16);
	}
	
	private String getSSOUrl(CustomerInfo customerInfo) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String name = customerInfo.getLastName();
		String email = customerInfo.getAutelId();
		long timeInSeconds = System.currentTimeMillis()/1000; 
//		String hash = getHash(name + email + this.key);
//		String hash = getHash(name + email + timeInSeconds);
		String hash = ""; 
		try {
			hash = getHMACHash(name, email, timeInSeconds);
		} catch (Exception e) {
			e.printStackTrace();
		}
		name = URLEncoder.encode(name, "UTF-8");
		String url = getPropertiesValue("freshdesk.notlogin.url") + "/login/sso" + "?name=" + name + "&email=" + email + "&timestamp=" + timeInSeconds + "&hash=" + hash;
		url += "&redirect_to="+getPropertiesValue("freshdesk.discussions.url");
		return url;
//		return freshDeskUrl + "?name=" + name + "&email=" + email + "&timestamp=" + timeInSeconds + "&hash=" + hash;
//		return freshDeskUrl + "?email=" + email + "&timestamp=" + timeInSeconds + "&hash=" + hash;
	}
	
	 public static String hashToHexString(byte[] byteData) {
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	            String hex = Integer.toHexString(0xff & byteData[i]);
	            // NB! E.g.: Integer.toHexString(0x0C) will return "C", not "0C"            
	            if (hex.length() == 1) {
	                hexString.append('0');
	            }
	            hexString.append(hex);
	        }
	        return hexString.toString();
	 }

    private static String getHMACHash(String name,String email,long timeInMillis) throws Exception {
        byte[] keyBytes = key.getBytes();
        String movingFact = name +key+ email + timeInMillis;
        byte[] text = movingFact.getBytes();
        
        String hexString = "";
        Mac hmacMD5;
        try {
          hmacMD5 = Mac.getInstance("HmacMD5");
          SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
          hmacMD5.init(macKey);
          byte[] hash =  hmacMD5.doFinal(text);
          hexString = hashToHexString(hash);

        } catch (Exception nsae) {
        	nsae.printStackTrace();
        }
        return hexString;
    }
    
    private static String getHMACHash(String email,long timeInMillis) throws Exception {
        byte[] keyBytes = key.getBytes();
        String movingFact = email + timeInMillis;
        byte[] text = movingFact.getBytes();
        
        String hexString = "";
        Mac hmacMD5;
        try {
          hmacMD5 = Mac.getInstance("HmacMD5");
          SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
          hmacMD5.init(macKey);
          byte[] hash =  hmacMD5.doFinal(text);
          hexString = hashToHexString(hash);

        } catch (Exception nsae) {
        	nsae.printStackTrace();
        }
        return hexString;
    }

    public void sendUrl(HttpServletResponse response) throws NoSuchAlgorithmException, InvalidKeyException, IOException {

        String hash;
        String url = null;
        String name = "abc";
        String email = "auteltestreceive@163.com";
        long timeInSeconds = System.currentTimeMillis()/1000; 
        
        try {       
            hash = getHMACHash(name,email,timeInSeconds);
            url = getPropertiesValue("freshdesk.notlogin.url") + "/login/sso" + "?name="+name+"&email="+email+"&timestamp="+timeInSeconds+"&hash=" + hash;
            url += "&redirect_to="+getPropertiesValue("freshdesk.discussions.url");
        } catch (Exception e) {
            e.printStackTrace();
        }   
        response.sendRedirect(url);
    }
    
//    public static void main(String[] args) {
//    	String hash;
//        String url = null;
//        String name = "gao";
//        String email = "2424487422@qq.com";
//        long timeInSeconds = System.currentTimeMillis()/1000; 
//        
//        try {       
//            hash = getHMACHash(name,email,timeInSeconds);
//            url = getPropertiesValue("freshdesk.notlogin.url") + "/login/sso" + "?name="+name+"&email="+email+"&timestamp="+timeInSeconds+"&hash=" + hash;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }  
//        System.out.println(url);
//	}
    
    private static String getPropertiesValue(String key) {
    	InputStream in = FileUtil.getResourceAsStream("cop.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return props.getProperty(key);
    }
	    
}
