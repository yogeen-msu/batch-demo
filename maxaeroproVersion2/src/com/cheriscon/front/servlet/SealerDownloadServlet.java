package com.cheriscon.front.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cheriscon.front.constant.FrontConstant;

@WebServlet(asyncSupported = true, urlPatterns = { "/sealerDownload" })
public class SealerDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SealerDownloadServlet() {  
        super();  
    }  

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//获得文件完整路径
		String fileUrl = FrontConstant.getProperValue("sealer.download.url")
				+ request.getParameter("fileUrl");
		// 获得请求文件名
		String fullName = fileUrl.substring(fileUrl.lastIndexOf("/")+1);

		// 设置文件MIME类型
		response.setContentType(getServletContext().getMimeType(fullName));
		// 设置Content-Disposition
		response.setHeader("Content-Disposition", "attachment;filename="
				+ fullName);
		// 读取目标文件，通过response将目标文件写到客户端
		// 读取文件
		URL url = new URL(fileUrl);
		URLConnection conn = url.openConnection();
        InputStream in;
		try {
			in = conn.getInputStream();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		OutputStream out = response.getOutputStream();
		
		// 写文件
		int b;
		while ((b = in.read()) != -1) {
			out.write(b);
		}

		in.close();
		out.close();
	}
}
