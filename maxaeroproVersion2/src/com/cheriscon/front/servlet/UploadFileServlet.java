package com.cheriscon.front.servlet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;

import com.cheriscon.cop.sdk.context.CopSetting;

public class UploadFileServlet extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6938590675531865132L;
	private static final long FILE_MAX_SIZE = 1024 * 1024 * 20;
 
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
//		doPost(req, resp);
	}
    
    @SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        
        RequestContext req = new ServletRequestContext(request);
        if (FileUpload.isMultipartContent(req)) {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload fileUpload = new ServletFileUpload(factory);
            fileUpload.setHeaderEncoding("UTF-8");
            fileUpload.setFileSizeMax(FILE_MAX_SIZE);
            List<FileItem> items = new ArrayList<FileItem>();
            try {
                items = fileUpload.parseRequest(request);
            } catch (FileUploadException e) {
                e.printStackTrace();
            }
            Iterator<FileItem> it = items.iterator();
            while (it.hasNext()) {
                FileItem fileItem = it.next();
                if (fileItem.isFormField()) {
                    System.out.println(fileItem.getFieldName()
                            + " "
                            + fileItem.getName()
                            + " "
                            + new String(fileItem.getString().getBytes(
                                    "US-ASCII"), "UTF-8"));
                } else {
                    System.out.println(fileItem.getFieldName() + " "
                            + fileItem.getName() + " " + fileItem.isInMemory()
                            + " " + fileItem.getContentType() + " "
                            + fileItem.getSize());
                    if (fileItem.getName() != null && fileItem.getSize() != 0) {
//                        File fullFile = new File(fileItem.getName());
                        File newFile = new File(CopSetting.IMG_SERVER_PATH + "/attachment/complaintinfo/"
//                                + fullFile.getName());
                        		+ fileItem.getName());
                        if (!newFile.getParentFile().exists()) {
                        	newFile.getParentFile().mkdirs();
                        }
                        try {
                            fileItem.write(newFile);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("no file choosen or empty file");
                    }
                }
            }
        }
    }
	
}
