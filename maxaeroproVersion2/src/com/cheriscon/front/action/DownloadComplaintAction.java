package com.cheriscon.front.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.stereotype.Controller;

import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

@Controller
@Namespace("/")
public class DownloadComplaintAction {
	
	private String downloadPath;
 
	@Action("DownloadComplaintAction")
	public void execute() throws UnsupportedEncodingException {
		HttpServletResponse response = ThreadContextHolder.getHttpResponse();
		if (downloadPath.indexOf("fs:")!=-1) {
			downloadPath = downloadPath.replace("fs:", "");
		}
		
		String filePath = CopSetting.IMG_SERVER_PATH + new String(downloadPath.getBytes("ISO-8859-1"), "UTF-8");
		
		
		// path是指欲下载的文件的路径。
		File file = new File(filePath);
		
		if (!file.exists()) {
			try {
				response.getWriter().write("not exits");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		
		// 取得文件名。
		String filename = file.getName();

		
		try {
			// 以流的形式下载文件。
			InputStream fis = new BufferedInputStream(new FileInputStream(filePath));
			byte[] buffer;
			buffer = new byte[fis.available()];
			fis.read(buffer);
			fis.close();
			
			// 清空response
			if (!response.isCommitted()) {
				response.reset();
			}
			
			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes()));
			response.addHeader("Content-Length", "" + file.length());
			OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
			response.setContentType("application/octet-stream");
			outputStream.write(buffer);
			outputStream.flush();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}
	
	
}
