package com.cheriscon.front.action;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.action.WWAction;

public class DownloadAction{

    private String downloadPath;
    private String fileName;
    private InputStream inputStream;
    protected List msgs = new ArrayList();
   
    public String execute(){
		try 
		{
			String filePath = CopSetting.IMG_SERVER_PATH + downloadPath;
			File file = new File(filePath);
			this.setFileName(file.getName());
			if(file.exists())
			{
				inputStream=new BufferedInputStream(new FileInputStream(filePath));
			}
			else
			{
				this.msgs.add("该文件不存在！");
				return "error";
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
            return "success";
    }
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDownloadPath() {
		return downloadPath;
	}
	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public List getMsgs() {
		return msgs;
	}
	public void setMsgs(List msgs) {
		this.msgs = msgs;
	}
 
}
