package com.cheriscon.front.action.product;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.tools.ant.filters.StringInputStream;

import com.cheriscon.common.utils.StrEncrypt;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;

@ParentPackage("json-default")
@Namespace("/back/product")
public class RegProductForNAction extends WWAction{
	
	private String serialNo;//c产品序列号
	private String regPwd;//产品注册密码
	private String mobilePhone; //手机号
	
	@Resource
	private IRegProductService  regProductService;
	
	@Action("reg")
	public String reg(){
//		解密
		serialNo = StrEncrypt.decrypt(serialNo);
		regPwd = StrEncrypt.decrypt(regPwd);
		mobilePhone = StrEncrypt.decrypt(mobilePhone);
		if(serialNo==null){
			String qstr = getRequest().getQueryString();
			if(getRequest().getMethod().toLowerCase().equals("post")){
				try {
					qstr = IOUtils.toString(getRequest().getInputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(StringUtils.isNotEmpty(qstr)){
				qstr=StrEncrypt.decrypt(getRequest().getQueryString());
				String []params = qstr.split("&");
				if(params!=null){
					Map<String, String> parammMAP = new HashMap<String, String>();
					for(String param:params){
						String [] paramarry = param.split("=");
						if(paramarry!=null&&paramarry.length==2){
							parammMAP.put(paramarry[0], paramarry[1]);
						}
					}
					serialNo = parammMAP.get("serialNo");
					regPwd = parammMAP.get("regPwd");
					mobilePhone = parammMAP.get("mobilePhone");
				}
			}
			
		}
		if(serialNo==null){
			renderText("{\"total\":0,\"success\":0}");
			return null;
		}
		String res = regProductService.regProductForCN(serialNo, regPwd, mobilePhone);
		renderText(res);
		return null;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getRegPwd() {
		return regPwd;
	}

	public void setRegPwd(String regPwd) {
		this.regPwd = regPwd;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	
}
