package com.cheriscon.front.action.product;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.backstage.system.service.IProductChangeNoLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.common.model.ProductChangeNoLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;
import com.cheriscon.front.usercenter.customer.vo.RegProductAccountInfoSecVo;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;
import com.cheriscon.user.service.ICustomerProductService;
//import org.bouncycastle.ocsp.Req;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.cheriscon.backstage.member.service.ICustomerProductService;
//import com.cheriscon.backstage.member.vo.CustomerProductVo;
//import com.cheriscon.backstage.system.service.IProductChangeNoLogService;
//import com.cheriscon.common.model.CustomerInfo;
//import com.cheriscon.common.model.ProductChangeNoLog;
//import com.cheriscon.common.model.SealerInfo;
//import com.cheriscon.common.utils.DesModule;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.front.user.vo.SystemErrorLog;

/**
 * 注册产品
 * @author caozhiyong
 * @version 2013-3-5
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class RegProductAction extends WWAction {

	private static final long serialVersionUID = 1L;
	
	private String proRegPwd;//注册密码
	private String proSerialNo;//产品序列号
	private String proTypeCode;//产品类型code
	private String sealCode;//经销商编码
	private String imagenum;
	private String autelId; 
	private String proCode; //产品编码
	
	private String oldProductSN; //更换产品序列号-老序列号
	private String newProductSN; //更换产品序列号-新序列号
	private String customerCode; //更换产品序列号-用户code
	private String jsonData;
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getOldProductSN() {
		return oldProductSN;
	}

	public String getNewProductSN() {
		return newProductSN;
	}

	public void setOldProductSN(String oldProductSN) {
		this.oldProductSN = oldProductSN;
	}

	public void setNewProductSN(String newProductSN) {
		this.newProductSN = newProductSN;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	private String regResult="false";
	
	@Resource
	private IRegProductService regProductService;
	@Resource
	private IProductInfoService productInfoService; 
	@Resource
	private ISystemErrorLogService systemErrorService;
	@Resource
	private ICustomerProductService customerProductService;
	@Resource 
	private IProductChangeNoLogService productChangeNoLogService;
	
	private RegProductAcountInfoVO regProductInfoVO=new RegProductAcountInfoVO();
	
	public RegProductAcountInfoVO getRegProductInfoVO() {
		return regProductInfoVO;
	}

	public void setRegProductInfoVO(RegProductAcountInfoVO regProductInfoVO) {
		this.regProductInfoVO = regProductInfoVO;
	}

//	@Action(value = "checkProduct", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "regResult"}) })
//	public String checkProduct(){
//		
//		String code = (String) ThreadContextHolder.getSessionContext()
//				.getAttribute(FrontConstant.SESSION_IMAGE_CODE_NAME);
//		
//		if(code!=null){
//			if (!imagenum.toUpperCase().equals(code.toUpperCase())) {
//				regResult="imagenum"; 
//			}else{
//			ProductInfo info = regProductService.queryProduct(proSerialNo);
//			if(info==null){
//				regResult="notFound";
//			}else{
//				if(!info.getRegPwd().equals(proRegPwd)){
//					regResult="passwordError";
//				}else{
//					 int num=regProductService.checkProduct(autelId,proSerialNo);
//					  if(num==0){
//						  regResult="productNotForYou";
//					  }
//				}
//			}
//			}
//	 }else{
//		 regResult="imagenum"; 
//	 }
//		Map<String,String> dataMap = new HashMap<String, String>();
//		dataMap.put("regResult", regResult);
//		regResult=JSONArray.fromObject(dataMap).toString();
//		return SUCCESS;
//		
//	}
	
	
	/**
	 *	注册产品
	 * @return
	 */
	@Action(value = "changeProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String changeProduct(){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
//		regProductInfoVO.setProRegPwd(proRegPwd.trim());//注册密码
//		regProductInfoVO.setProSerialNo(proSerialNo.trim());//产品序列号
//		regProductInfoVO.setProTypeCode(proTypeCode);//产品类型code
//		regProductInfoVO.setSealCode(sealCode);//经销商编码
//		
//		List<ProductInfo> list = regProductService.queryProductInfo(regProductInfoVO);
//		if (list!=null && list.size()==1) {
//			ProductInfo info=list.get(0);
//			regProductInfoVO.setProCode(info.getCode());
//			boolean flag=regProductService.changeCustProInfo(customerInfo.getCode(), regProductInfoVO);
//			if(flag==true){
//				 regResult="true";
//			}else{
//				regResult="false";
//			}
//		}
		regResult = regProductService.changeProduct(proRegPwd, proSerialNo, proTypeCode, customerInfo.getCode());
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("regResult", regResult);
		regResult=JSONArray.fromObject(dataMap).toString();
		return SUCCESS;
	}
	
	/**
	 *	注册产品
	 * @return
	 */
	@Action(value = "regProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String regProduct() throws Exception{
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		String returnMsg="";
		try{
			/*20160315 hlh String code = (String) ThreadContextHolder.getSessionContext()
			.getAttribute(FrontConstant.SESSION_IMAGE_CODE_NAME);*/
	        
	        CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
			if(customerInfo==null){
				regResult="notLogin";
			}else{
		        /*hlh 20160315 if(code!=null && imagenum!=null){
					if (!imagenum.toUpperCase().equals(code.toUpperCase())) {
						regResult="imagenum"; 
					}else {*/
						proSerialNo =  proSerialNo.trim();
//						regResult = regProductService.regProduct(proRegPwd.trim(),proSerialNo, proTypeCode, customerInfo.getCode());
						regResult = regProductService.regProduct("",proSerialNo, proTypeCode, customerInfo.getCode());
						if(regResult.startsWith("regConfirm")){
							returnMsg=FreeMarkerPaser.getBundleValue("add.product.msg.title");
							returnMsg=returnMsg.replace("{0}", regResult.substring(11));
							returnMsg=returnMsg.replace("{1}", proSerialNo);
							returnMsg=returnMsg.replace("{2}", customerInfo.getAutelId());
						}
			/*	20160315 hlh	}					
			}else{
				regResult="imagenum";
			}*/
			}
		}catch(Exception ex){
			
			regResult="SystemError";
			systemErrorService.saveLog("产品注册", ex, 
					"com.cheriscon.front.usercenter.customer.action.RegProductAction");
		}
		
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("regResult", regResult);
		dataMap.put("returnMsg", returnMsg);
		dataMap.put("serialNo", proSerialNo);
		regResult=JSONArray.fromObject(dataMap).toString();

		return SUCCESS;
	}

	
//	/**
//	 * 更换产品序列号
//	 * ***/
	@Action(value = "changeProductSerialNo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String changeProductSerialNo(){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		ProductInfo newProduct = regProductService.queryProductInfo(newProductSN); //判断新产品是否正确
		if(null==newProduct){
			regResult="NewProNotFind"; //新产品序列号已经存在
		}else{
			
			  if(newProduct.getRegPwd().equals(proRegPwd)){
				if(newProduct.getRegStatus()==FrontConstant.CHARGE_CARE_IS_YES_REG){
					regResult="HadReged"; //已经注册
				}else{
					ProductInfo oldProduct=regProductService.queryProductInfo(oldProductSN);
					if(!newProduct.getProTypeCode().equals(oldProduct.getProTypeCode())){
						regResult="ProTypeError"; //产品类型错误
					}else if(!newProduct.getSealerAutelId().equals(oldProduct.getSealerAutelId())){
						regResult="NotForYou"; //产品序列号不属于经销商
					}else{
						/**开始执行更换操作
						 * 1.将产品的信息写入中间表，包括原用户code，有效期等
						 * ***/
//						ProductChangeNoLog log=new ProductChangeNoLog();
						Object obj=SessionUtil.getLoginUserInfo(request);
						String createUser="";
						if(obj.getClass().equals(CustomerInfo.class)){
							createUser=((CustomerInfo)obj).getCode();
						}
						if(obj.getClass().equals(SealerInfo.class)){
							createUser=((SealerInfo)obj).getCode();
						}
//						log.setCreateUser(createUser);
//						log.setCreateIp(request.getRemoteAddr());
//						log.setCreateDate(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
//						log.setCustomerCode(customerCode);
//						log.setOldProCode(oldProduct.getCode());
//						log.setNewProCode(newProduct.getCode());
//						log.setValidDate(oldProduct.getValidDate()); //有效期
//						log.setRegDate(oldProduct.getRegTime()); //注册日期
						
						RegProductAccountInfoSecVo regProductAccountInfoSecVo = new RegProductAccountInfoSecVo();
						regProductAccountInfoSecVo.setCreateUser(createUser);
						regProductAccountInfoSecVo.setCreateIp(request.getRemoteAddr());
						regProductAccountInfoSecVo.setCreateDate(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
						regProductAccountInfoSecVo.setCustomerCode(customerCode);
						regProductAccountInfoSecVo.setOldProCode(oldProduct.getCode());
						regProductAccountInfoSecVo.setNewProCode(newProduct.getCode());
						regProductAccountInfoSecVo.setValidDate(oldProduct.getValidDate());
						regProductAccountInfoSecVo.setRegDate(oldProduct.getRegTime());
						
						
//						try {
							//先将原来的关系记录日志后删除
//							productChangeNoLogService.saveLog(log);
							//绑定新的关系
						/*	CustomerProInfo tempInfo=new CustomerProInfo();
							tempInfo.setCustomerCode(customerCode);
							tempInfo.setProCode(newProduct.getCode());
							boolean result=regProductService.insertCustProInfo(tempInfo);*/
							
//							regProductInfoVO.setProCode(newProduct.getCode());
//							regProductInfoVO.setProRegPwd(newProduct.getRegPwd());
//							regProductInfoVO.setProSerialNo(newProduct.getSerialNo());
//							regProductInfoVO.setProTypeCode(newProduct.getProTypeCode());
//							boolean result = regProductService.addProductSoftwareValidStatus(customerCode,oldProduct.getValidDate(),oldProduct.getRegTime(),oldProduct.getProDate(),regProductInfoVO);	
							
							regProductAccountInfoSecVo.setProCode(newProduct.getCode());
							regProductAccountInfoSecVo.setProRegPwd(newProduct.getRegPwd());
							regProductAccountInfoSecVo.setProSerialNo(newProduct.getSerialNo());
							regProductAccountInfoSecVo.setProTypeCode(newProduct.getProTypeCode());
							regProductAccountInfoSecVo.setCustomerCode(customerCode);
							regProductAccountInfoSecVo.setValidDate(oldProduct.getValidDate());
							regProductAccountInfoSecVo.setRegTime(oldProduct.getRegTime());
							regProductAccountInfoSecVo.setProDate(oldProduct.getProDate());
						try {
							regResult = regProductService.addProductSoftwareValidStatusSec(regProductAccountInfoSecVo);
							
//							ProductAssociatesDate proDate = productDateService.getProductDateInfo(newProduct.getCode());
							
//							if(proDate==null){
//								proDate=new ProductAssociatesDate();
//								proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
//								proDate.setProCode(newProduct.getCode());
//								result=productDateService.insertProductDate(proDate);
//							}else{
//								result=productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), newProduct.getCode());
//							}
//							if(result){
//								regResult="SUCCESS";
//							}else{
//								regResult="FAIL";
//							}
						} catch (Exception e) {
							regResult="FAIL";
							e.printStackTrace();
						}
					}
				}
				
			}else{
				regResult="RegPwdError"; //注册密码错误
			}
		}
		
		return SUCCESS;
	}
	
	/**
	 * 修改产品备注
	 */
	@Action(value = "updateProductRemark", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String updateProductRemark(){
		regResult=productInfoService.updateProductRemark(regProductInfoVO.getProCode(), regProductInfoVO.getRemark());
		return SUCCESS;
	}
	
	/*
	 * 移除产品注册
	 **/
	@Action(value = "removeProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String removeProduct(){
		
		if(proCode!=null){
			regResult = regProductService.removeProductInfo(proCode);
		}else{
			regResult="false";
		}
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("regResult", regResult);
		regResult=JSONArray.fromObject(dataMap).toString();
		return SUCCESS;
	}
//	
//	
//	/**
//	 * 查询产品信息
//	 * **/
	@Action(value = "queryProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"}) })
	public String queryProduct(){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		
		CustomerProductVo customerProductVoSel=new CustomerProductVo();
    	customerProductVoSel.setAutelId(request.getParameter("autelID"));
    	customerProductVoSel.setProSerialNo(request.getParameter("productSN"));
    	customerProductVoSel.setCity(request.getParameter("city"));
    	customerProductVoSel.setProvince(request.getParameter("province"));
    	String sealerAutelId="";
    	Object object=SessionUtil.getLoginUserInfo(request);
    	if(object!=null && object.getClass().equals(SealerInfo.class)){
    		sealerAutelId=((SealerInfo)SessionUtil.getLoginUserInfo(request)).getAutelId();
    	}
    	customerProductVoSel.setSealerAutelId(sealerAutelId);
    	
		try {
			Page dataPage;
			dataPage = customerProductService.pageCustomerProductVoPage(customerProductVoSel, 1, 15);
			long totalCount=dataPage.getTotalCount();
			String productSerialNo="";
			String customerCode="";
			if(totalCount==1){
//			    List<CustomerProductVo> list=(List<CustomerProductVo>) dataPage.getResult();
//			    productSerialNo=list.get(0).getProSerialNo();
//			    customerCode=list.get(0).getCode();
//				CustomerProductVo[] cpv = new CustomerProductVo[1];
				CustomerProductVo[] cpv = (CustomerProductVo[]) dataPage.getResult();
				productSerialNo = cpv[0].getProSerialNo();
			    customerCode = cpv[0].getCode();
			 }
		    Map<String,String> map=new HashMap<String,String>();	
		    map.put("totalCount", String.valueOf(totalCount));
		    map.put("productSerialNo", productSerialNo);
		    map.put("customerCode", customerCode);
		    
			this.jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
	   
		return SUCCESS;
	}
//	
//	
//	public int  compareDate(String s1,String s2){
//		java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd");
//		java.util.Calendar c1=java.util.Calendar.getInstance();
//		java.util.Calendar c2=java.util.Calendar.getInstance();
//		try {
//			c1.setTime(df.parse(s1));
//			c1.add(Calendar.MONTH, 3);
//			c2.setTime(df.parse(s2));
//			int result=c1.compareTo(c2);
//			System.out.println(result);
//			return  result;
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return 0;
//	}
//	
	

	public String getRegResult() {
		return regResult;
	}

	public void setRegResult(String regResult) {
		this.regResult = regResult;
	}

//	public IRegProductService getRegProductService() {
//		return regProductService;
//	}
//
//	public void setRegProductService(IRegProductService regProductService) {
//		this.regProductService = regProductService;
//	}
//
	public String getProRegPwd() {
		return proRegPwd;
	}
	public void setProRegPwd(String proRegPwd) {
		this.proRegPwd = proRegPwd;
	}
	public String getProSerialNo() {
		return proSerialNo;
	}
	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}
	public String getProTypeCode() {
		return proTypeCode;
	}
	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
	public String getSealCode() {
		return sealCode;
	}
	public void setSealCode(String sealCode) {
		this.sealCode = sealCode;
	}


	public String getImagenum() {
		return imagenum;
	}


	public void setImagenum(String imagenum) {
		this.imagenum = imagenum;
	}
	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	
}
