package com.cheriscon.front.action.user;

//import java.io.BufferedReader;
//import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
//import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
//import java.util.Properties;
//
import javax.annotation.Resource;
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
//
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
//
//import org.apache.commons.httpclient.HttpClient;
//import org.apache.commons.httpclient.HttpException;
//import org.apache.commons.httpclient.HttpStatus;
//import org.apache.commons.httpclient.NameValuePair;
//import org.apache.commons.httpclient.methods.PostMethod;
//import org.apache.http.Header;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.xml.sax.SAXException;
//
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import urn.ebay.apis.eBLBaseComponents.CountryCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.ItemCategoryType;
import urn.ebay.apis.eBLBaseComponents.LandingPageType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsItemType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.SetExpressCheckoutRequestDetailsType;
//
import com.cheriscon.backstage.system.component.plugin.rate.RatePlugin;
import com.cheriscon.backstage.system.service.IPaymentCfgService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.PayErrorLog;
//import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.common.model.PayLog;
import com.cheriscon.common.model.PaymentCfg;
//import com.cheriscon.common.utils.JsonHelper;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
//import com.cheriscon.framework.util.DateUtil;
//import com.cheriscon.framework.util.FileUtil;
//import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.alipay.config.AlipayConfig;
import com.cheriscon.front.usercenter.alipay.util.AlipayNotify;
import com.cheriscon.front.usercenter.alipay.util.AlipaySubmit;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
//import com.cheriscon.front.usercenter.customer.service.IPayService;
//import com.cheriscon.front.usercenter.customer.model.PayPal;
import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.sdk.exceptions.OAuthException;
import paypal.payflow.*;

//import java.math.BigDecimal;
//import java.net.URL;
//import java.net.URLEncoder;
/**
 * 支付
 * @author caozhiyong
 * @version 2013-3-29
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class PayMentAction extends WWAction{

	private static final long serialVersionUID = 1L;
	
	
	private String orderNo;//订单编号
	private String money;//金额
	private String payType;//支付方式
	
	private String partner;//支付宝合作身份者ID
	private String seller_email;//支付宝卖家商户账号
	private String key;//支付宝商户的私钥
	private String result;
	
	
	@Resource 
	private IPaymentCfgService service;
//	@Resource 
//	private IPayService payService ;
	@Resource
	private IOrderInfoService orderInfoService;
//	@Resource
//	private IOrderInfoBackService orderInfoBackService;
	
	@Action(value = "payMent", results = { @Result( name = SUCCESS, type = "json", params = {
			"root", "result" }) })
	public String payMent(){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		HttpServletResponse response = ThreadContextHolder.getHttpResponse();
		try{
//			支付宝方式已停用
		if (payType.equals("alipayDirectPlugin")) {//使用支付宝支付
			alipayDirect(request);
		}else if(payType.equals("paypal")){ //使用paypal支付
			paypay(request,response,LandingPageType.LOGIN);
			//paypayNew(request,response,LandingPageType.LOGIN);
		}else{//使用信用卡支付
//			信用卡 方式 是在页面判断支付方式后直接跳转creditPay.html 不再经过次方法
			paypayForBilling(request, response, LandingPageType.BILLING);
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
//
//	@Action(value = "paypayNew")
//	public void paypayNew(HttpServletRequest request,HttpServletResponse response,LandingPageType type){
//
//		HttpSession session = request.getSession();
//        PayPal pp = new PayPal();
//        /*
//        '------------------------------------
//        ' The returnURL is the location where buyers return to when a
//        ' payment has been succesfully authorized.
//        '
//        ' This is set to the value entered on the Integration Assistant
//        '------------------------------------
//        */
//        
//        String returnURL = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/payPalReturn.html";
//        if(pp.getUserActionFlag().equals("true"))
//        	returnURL = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/payPalReturn.html";
//       
//        /*
//        '------------------------------------
//        ' The cancelURL is the location buyers are sent to when they hit the
//        ' cancel button during authorization of payment during the PayPal flow
//        '
//        ' This is set to the value entered on the Integration Assistant
//        '------------------------------------
//        */
//        String cancelURL = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/myOrders-1-1.html";
//      
//        OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderNo); //根据订单编号获取价格
//        
//        session.setAttribute("L_PAYMENTREQUEST_0_NUMBER0", orderInfo.getCode()); //Item ID
//        session.setAttribute("L_PAYMENTREQUEST_0_NAME0", orderInfo.getCode());   //Item Name
//        session.setAttribute("PAYMENTREQUEST_0_ITEMAMT", orderInfo.getOrderMoney());   //Price
//        session.setAttribute("currencyCodeType", "USD");  //货币 Currency Code
//        session.setAttribute("PAYMENTREQUEST_0_AMT", orderInfo.getOrderMoney()); // Total Amount
//        
//       /* for (String key : request.getParameterMap().keySet()) {
//			   session.setAttribute(key, request.getParameterMap().get(key)[0]);
//		}*/
//
//        //Redirect to check out page for check out mark
//        if(!isSet(request.getParameter("Confirm")) && isSet(request.getParameter("checkout"))){
//
//    		if(isSet(request.getParameter("checkout")) || isSet(session.getAttribute("checkout"))) {
//    			session.setAttribute("checkout", request.getParameter("checkout"));
//    		}
//    			
//	    	
//	    	session.setAttribute("EXPRESS_MARK", "ECMark");
//	    	
//	    	request.setAttribute("PAYMENTREQUEST_0_AMT", request.getParameter("PAYMENTREQUEST_0_AMT"));
//	    	RequestDispatcher dispatcher = request.getRequestDispatcher("checkout.jsp");
//	    	if (dispatcher != null){
//	    		try {
//					dispatcher.forward(request, response);
//				} catch (ServletException e) {
//					
//					e.printStackTrace();
//				} catch (IOException e) {
//					
//					e.printStackTrace();
//				}
//	    	}
//	    	
//        }
//        else{
//        	Map<String, String> nvp;
//        	        	
//        	if(isSet(session.getAttribute("EXPRESS_MARK")) && session.getAttribute("EXPRESS_MARK").equals("ECMark")){
//	        	if(isSet(request.getParameter("shipping_method"))) {
//	        		BigDecimal new_shipping = new BigDecimal(request.getParameter("shipping_method")); //need to change this value, just for testing
//	        		BigDecimal shippingamt = new BigDecimal(session.getAttribute("PAYMENTREQUEST_0_SHIPPINGAMT").toString());
//	        		BigDecimal paymentAmount = new BigDecimal((String)session.getAttribute("PAYMENTREQUEST_0_AMT"));
//	        		if(shippingamt.compareTo(new BigDecimal(0)) > 0){
//	        			paymentAmount = paymentAmount.add(new_shipping).subtract(shippingamt) ;
//	        		}
//	        		session.setAttribute("PAYMENTREQUEST_0_AMT",paymentAmount.toString().replace(".00", ""));
//	        		session.setAttribute("PAYMENTREQUEST_0_SHIPPINGAMT",new_shipping.toString());	
//	        		session.setAttribute("shippingAmt",new_shipping.toString());
//	        	}
//	        	returnURL = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/Return?page=return";
//	        	 nvp = pp.callMarkExpressCheckout(request, returnURL, cancelURL);        	
//	        	
//        	} else {
//        	
//        		 nvp = pp.callShortcutExpressCheckout (request, returnURL, cancelURL);
//        	}
//	        
//	        
//			String strAck = nvp.get("ACK").toString().toUpperCase();
//	        if(strAck !=null && (strAck.equals("SUCCESS") || strAck.equals("SUCCESSWITHWARNING") ))
//	        {
//	            session.setAttribute("token", nvp.get("TOKEN").toString());
//	            //' Redirect to paypal.com
//	           // pp.redirectURL(response, nvp.get("TOKEN").toString(),(isSet(session.getAttribute("EXPRESS_MARK")) && session.getAttribute("EXPRESS_MARK").equals("ECMark") || (pp.getUserActionFlag().equalsIgnoreCase("true"))) );
//	            
//	            Map<String, String> resultMap = new HashMap<String, String>();
//	            String sbHtml=pp.getURL(response, nvp.get("TOKEN").toString(), true);
//				resultMap.put("paypalURL", sbHtml);
//				resultMap.put("html", "");
//				result = JSONArray.fromObject(resultMap).toString();
//	            
//	        }
//	        else
//	        {
//	            // Display a user friendly Error on the page using any of the following error information returned by PayPal
//	            String ErrorCode = nvp.get("L_ERRORCODE0").toString();
//	            String ErrorShortMsg = nvp.get("L_SHORTMESSAGE0").toString();
//	            String ErrorLongMsg = nvp.get("L_LONGMESSAGE0").toString();
//	            String ErrorSeverityCode = nvp.get("L_SEVERITYCODE0").toString();
//	            
//	            String errorString = "SetExpressCheckout API call failed. "+
//	           		"<br>Detailed Error Message: " + ErrorLongMsg +
//			        "<br>Short Error Message: " + ErrorShortMsg +
//			        "<br>Error Code: " + ErrorCode +
//			        "<br>Error Severity Code: " + ErrorSeverityCode;
//	            request.setAttribute("error", errorString);
//	        	RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
//	        	if (dispatcher != null){
//	        		try {
//						dispatcher.forward(request, response);
//					} catch (ServletException e) {
//						
//						e.printStackTrace();
//					} catch (IOException e) {
//						
//						e.printStackTrace();
//					}
//	        	}
//	            
//	        }
//        }
//	}
//	
//	private boolean isSet(Object value){
//		return (value !=null && value.toString().length()!=0);
//	}
//	@Action(value = "payForBilling")	
//	public String payForBilling(){
//		
//		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//        SDKProperties.setHostAddress("payflowpro.paypal.com");
//        SDKProperties.setHostPort(443);
//        SDKProperties.setTimeOut(45);
//
//        UserInfo user = new UserInfo("autelusinc", "autelusinc", "PayPal", "0116Autel*");
//        PayflowConnectionData connection = new PayflowConnectionData();
//        Invoice inv = new Invoice();
//
//    	OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderNo); //根据订单编号获取价格
//		money=orderInfo.getOrderMoney();
//        Currency amt = new Currency(new Double(money), "USD");
//        inv.setAmt(amt);
//        HttpSession session = request.getSession();
//        session.setAttribute("orderNo", orderNo);
//
//        AuthorizationTransaction trans = new AuthorizationTransaction(user, connection, inv, null, PayflowUtility.getRequestId());
//
//        trans.setCreateSecureToken("Y");
//        trans.setSecureTokenId(PayflowUtility.getRequestId());
//
//        Response resp = trans.submitTransaction();
//
//        if (resp != null) {
//            TransactionResponse trxnResponse = resp.getTransactionResponse();
//
//            if (trxnResponse != null) {
//                
//                Map<String, String> resultMap = new HashMap<String, String>();
//				resultMap.put("paypalURL", "");
//				resultMap.put("SECURETOKEN", trxnResponse.getSecureToken());
//				resultMap.put("SECURETOKENID", trxnResponse.getSecureTokenId());
//				resultMap.put("html", "");
//				result = JSONArray.fromObject(resultMap).toString();
//				
//				
//				  HttpClient httpClient = new HttpClient();
//				  String url = "https://payflowlink.paypal.com/";
//				  PostMethod postMethod = new PostMethod(url);
//				//   填入各个表单域的值
//				    NameValuePair[] data = { 
//				    new NameValuePair("SECURETOKEN", trxnResponse.getSecureToken()),
//				    new NameValuePair("SECURETOKENID", trxnResponse.getSecureTokenId()),
//				    new NameValuePair("MODE", "LIVE"),
//				  };
//				  postMethod.setRequestBody(data);
//				  int statusCode = 0;
//				  try {
//				   statusCode = httpClient.executeMethod(postMethod);
//				  } catch (HttpException e) {
//				   
//				   e.printStackTrace();
//				  } catch (IOException e) {
//				   
//				   e.printStackTrace();
//				  }
//				  if (statusCode == HttpStatus.SC_MOVED_PERMANENTLY || statusCode == HttpStatus.SC_MOVED_TEMPORARILY)
//				  {
//				   //   从头中取出转向的地址
//				   Header locationHeader = (Header) postMethod.getResponseHeader("location");
//				   String location = null;
//				   if (locationHeader != null) {
//				    location = locationHeader.getValue();
//				    System.out.println("The page was redirected to:" + location);
//				   }
//				   else {
//				    System.err.println("Location field value is null.");
//				   }
//				  
//				  }
//				  else
//				  {
//				         System.out.println(postMethod.getStatusLine());
//				         String str = "";
//				         try {
//				               str = postMethod.getResponseBodyAsString();
//				         } catch (IOException e) {
//				               
//				               e.printStackTrace();
//				         }         
//				         System.out.println(str);
//				  }
//				  postMethod.releaseConnection();
//				        
//				
//
//            Context transCtx = resp.getContext();
//            if (transCtx != null && transCtx.getErrorCount() > 0) {
//                System.out.println("\nTransaction Errors = " + transCtx.toString());
//            }
//        }
//        }
//		return SUCCESS;
//	}
	
	
	public void paypayForBilling(HttpServletRequest request,HttpServletResponse response,LandingPageType type)
	{
		 System.out.println("------------------------------------------------------");
	        System.out.println("Executing Sample from File: DOSecureTokenAuth.java");
	        System.out.println("------------------------------------------------------");

	
	        SDKProperties.setHostAddress("payflowpro.paypal.com");
	        SDKProperties.setHostPort(443);
	        SDKProperties.setTimeOut(45);

	        UserInfo user = new UserInfo("autelusinc", "autelusinc", "PayPal", "0116Autel*");

	        // Create the Payflow Connection data object with the required connection details.
	        PayflowConnectionData connection = new PayflowConnectionData();

	        // Create a new Invoice data object with the Amount, Billing Address etc. details.
	        Invoice inv = new Invoice();

	    	OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderNo); //根据订单编号获取价格
			money=orderInfo.getOrderMoney();
	        // Set Amount.
	        Currency amt = new Currency(new Double(money), "USD");
	        inv.setAmt(amt);
	        inv.setPoNum(orderNo);
	        inv.setInvNum(orderNo);
	        HttpSession session = request.getSession();
	        session.setAttribute("orderNo", orderNo);

	     /*   AuthorizationTransaction trans = new AuthorizationTransaction(user, connection, inv, null, PayflowUtility.getRequestId());*/
             
	        SaleTransaction  trans = new SaleTransaction (user, connection, inv, null, PayflowUtility.getRequestId());
	        // Set the flag to create a Secure Token.
	        trans.setCreateSecureToken("Y");
	        // The Secure Token Id must be a unique id up to 36 characters.  Using the RequestID object to
	        // generate a random id, but any means to create an id can be used.
	        trans.setSecureTokenId(PayflowUtility.getRequestId());

	        // Submit the Transaction
//	        Response resp = trans.submitTransaction();
//
//	        // Display the transaction response parameters.
//	        if (resp != null) {
//	            // Get the Transaction Response parameters.
//	            TransactionResponse trxnResponse = resp.getTransactionResponse();
//
//	            if (trxnResponse != null) {
//
//	                System.out.println("RESULT = " + trxnResponse.getResult());
//	                System.out.println("RESPMSG = " + trxnResponse.getRespMsg());
//	                System.out.println("SECURETOKEN = " + trxnResponse.getSecureToken());
//	                System.out.println("SECURETOKENID = " + trxnResponse.getSecureTokenId());
//	                // If value is true, then the Request ID has not been changed and the original response
//	                // of the original transaction is returned.
//	                System.out.println("DUPLICATE = " + trxnResponse.getDuplicate());
//	                
	                Map<String, String> resultMap = new HashMap<String, String>();
					resultMap.put("paypalURL", "");
//					resultMap.put("SECURETOKEN", trxnResponse.getSecureToken());
//					resultMap.put("SECURETOKENID", trxnResponse.getSecureTokenId());
//					resultMap.put("html", "");
//					result = JSONArray.fromObject(resultMap).toString();
					resultMap.put("SECURETOKEN", "SECURETOKEN");
					resultMap.put("SECURETOKENID", "SECURETOKENID");
					resultMap.put("html", "");
					result = JSONArray.fromObject(resultMap).toString();
//	                
//	            }
//
//	            // Display the response.
//	            System.out.println("\n" + PayflowUtility.getStatus(resp));
//
//	            // Get the Transaction Context and check for any contained SDK specific errors (optional code).
//	            Context transCtx = resp.getContext();
//	            if (transCtx != null && transCtx.getErrorCount() > 0) {
//	                System.out.println("\nTransaction Errors = " + transCtx.toString());
//	            }
//	        }

	}
	
	/**
	 * 使用支付宝支付
	 * @param request
	 */
	private void alipayDirect(HttpServletRequest request) {
		/**
		 *从数据库读取支付宝参数 
		 */
		Map<String,String> map=service.getConfigParams("alipayDirectPlugin");
		key=map.get("key").toString();
		seller_email=map.get("seller_email").toString();
		partner=map.get("partner").toString();
		
		//给基本配置类的变量赋值
		AlipayConfig.partner=partner;
		AlipayConfig.key=key;
		
		//支付类型（1、立即支付）
		String payment_type = "1";
		
		//必填，不能修改
		//服务器异步通知页面路径
		StringBuffer url = new StringBuffer();
		url.append("http://");
		url.append(request.getServerName());
		url.append(":");
		url.append(request.getServerPort());
		url.append(request.getContextPath());
		
		String notify_url = url+"/front/usercenter/customer/notifyUrl.do";
		//String notify_url = url+"/notify_url.html";
		//String notify_url = url+"/notify_url.html";
		//需http://格式的完整路径，不能加?id=123这类自定义参数

		//页面跳转同步通知页面路径
		String return_url =  url+"/return_url.html";
		//需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

		//防钓鱼时间戳
		String anti_phishing_key="";
		//若要使用请调用类文件submit中的query_timestamp函数

		
		//客户端的IP地址
		String exter_invoke_ip = request.getRemoteAddr();
		//非局域网的外网IP地址，如：221.0.0.1
		OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderNo); //根据订单编号获取价格
		money=orderInfo.getOrderMoney();
		
		
		PaymentCfg paymentCfg=service.getByPluginId(new RatePlugin().getId());
		String biref=paymentCfg.getConfig();
		if(biref.startsWith("\"")){
			biref = biref.substring(1, biref.length()-1);
		}
		JSONObject json = JSONObject.fromObject(biref);
		
		money=String.valueOf(Double.parseDouble(money)*Double.parseDouble(json.getString("rate")));
		
		//把请求参数打包成数组
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", "create_direct_pay_by_user");
		sParaTemp.put("partner", AlipayConfig.partner);
		sParaTemp.put("_input_charset", AlipayConfig.input_charset);
		sParaTemp.put("payment_type", payment_type);
		sParaTemp.put("notify_url", notify_url);
		sParaTemp.put("return_url", return_url);
		sParaTemp.put("seller_email", seller_email);
		sParaTemp.put("out_trade_no", orderNo);
		sParaTemp.put("subject", orderNo);
		sParaTemp.put("total_fee", money);
		sParaTemp.put("anti_phishing_key", anti_phishing_key);
		sParaTemp.put("exter_invoke_ip", exter_invoke_ip);
		
		
		//建立请求
		String sHtmlText = AlipaySubmit.buildRequest(sParaTemp,"get","确认");
		
		Map<String, String> resultMap = new HashMap<String, String>();
		resultMap.put("html", sHtmlText);
		result = JSONArray.fromObject(resultMap).toString();
		
	}
	
	/**
	 * paypal支付
	 * @param request
	 * @param response
	 */
	public void paypay(HttpServletRequest request,HttpServletResponse response,LandingPageType type)
	{
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);
		HttpSession session = request.getSession();
	    session.setAttribute("url", request.getRequestURI());
	    session.setAttribute("relatedUrl", "<ul><li><a href='GetExpressCheckout.html'>SetExpressCheckout</a></li><li><a href='GetExpressCheckout.html'>GetExpressCheckout</a></li><li><a href='GetExpressCheckout.html'>DoExpressCheckout</a></li></ul>");

	    response.setContentType("text/html");
	    try {
	    	PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(getClass()
	    		  .getResourceAsStream("/sdk_config.properties"));

	        SetExpressCheckoutRequestType setExpressCheckoutReq = new SetExpressCheckoutRequestType();
	        SetExpressCheckoutRequestDetailsType details = new SetExpressCheckoutRequestDetailsType();

	        details.setLandingPage(type);//PayPal默认支付方式
	        StringBuffer url = new StringBuffer();
	        url.append("http://");
	        url.append(request.getServerName());
	        url.append(":");
	        url.append(request.getServerPort());
	        url.append(request.getContextPath());

	        String returnURL = url.toString() + "/GetExpressCheckout.html";
	        String cancelURL = url.toString() + "/myOrders-1-1.html";

	        details.setReturnURL(returnURL + "?currencyCodeType=USD" );

	        details.setCancelURL(cancelURL);
	        details.setBuyerEmail(customerInfo.getAutelId().toString());

	        request.getSession().setAttribute("paymentType", "Sale");

	        double itemTotal = 0D;
	        double orderTotal = 0D;

	    	OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderNo); //根据订单编号获取价格
			money=orderInfo.getOrderMoney();
	        
	        String amountItems = money;
	        String qtyItems = "1";
	        String names = orderNo;

	        List lineItems = new ArrayList();

	        PaymentDetailsItemType item = new PaymentDetailsItemType();
	        BasicAmountType amt = new BasicAmountType();
	        amt.setCurrencyID(CurrencyCodeType.fromValue("USD"));

	        amt.setValue(amountItems);
	        item.setQuantity(new Integer(qtyItems));
	        item.setName(names);
	        item.setAmount(amt);
	        item.setItemCategory(ItemCategoryType.fromValue("Physical"));   //Digital

	        item.setDescription(orderNo);
	        lineItems.add(item);

	        itemTotal = itemTotal + Double.parseDouble(qtyItems) * Double.parseDouble(amountItems);
	        orderTotal = orderTotal + itemTotal;

	        List payDetails = new ArrayList();
	        PaymentDetailsType paydtl = new PaymentDetailsType();
	        paydtl.setPaymentAction(PaymentActionCodeType.fromValue("Sale"));

	      
	        BasicAmountType shippingTotal = new BasicAmountType();
	        shippingTotal.setValue("0.00");

	        shippingTotal.setCurrencyID(CurrencyCodeType.fromValue("USD"));

	        orderTotal = orderTotal + Double.parseDouble("0.00");

	        paydtl.setShippingTotal(shippingTotal);
	
	        BasicAmountType itemsTotal = new BasicAmountType();
	        itemsTotal.setValue(Double.toString(itemTotal));
	        itemsTotal.setCurrencyID(CurrencyCodeType.fromValue("USD"));

	        paydtl.setOrderTotal(new BasicAmountType(CurrencyCodeType.fromValue("USD"), Double.toString(orderTotal)));

	        paydtl.setPaymentDetailsItem(lineItems);

	        paydtl.setItemTotal(itemsTotal);
	        paydtl.setNotifyURL(request.getParameter("notifyURL"));
	        payDetails.add(paydtl);
	        details.setPaymentDetails(payDetails);

	        details.setReqConfirmShipping("No");
	        details.setAddressOverride("");
	        AddressType shipToAddress = new AddressType();
	        shipToAddress.setName("");
	        shipToAddress.setStreet1("");
	        shipToAddress.setStreet2("");
	        shipToAddress.setCityName("");
	        shipToAddress.setStateOrProvince("");
	        shipToAddress.setPostalCode("");
	        shipToAddress.setCountry(CountryCodeType.fromValue("US"));
	        details.setAddress(shipToAddress);

	        details.setNoShipping("0");

	        details.setBrandName("");
	        details.setCustom("");
	        details.setCppHeaderImage("");
	        details.setCppHeaderBorderColor("");
	        details.setCppHeaderBackColor("");
	        details.setCppPayflowColor("");
	        details.setAllowNote("0");
	        
	        setExpressCheckoutReq.setSetExpressCheckoutRequestDetails(details);
	        SetExpressCheckoutReq expressCheckoutReq = new SetExpressCheckoutReq();
	        expressCheckoutReq.setSetExpressCheckoutRequest(setExpressCheckoutReq);
	        
	        SetExpressCheckoutResponseType setExpressCheckoutResponse = service.setExpressCheckout(expressCheckoutReq);

	        
	        session.setAttribute("orderNo", orderNo);
			if (setExpressCheckoutResponse != null) {
				session.setAttribute("lastReq", service.getLastRequest());
				session.setAttribute("lastResp", service.getLastResponse());
				if (setExpressCheckoutResponse.getAck().toString()
						.equalsIgnoreCase("SUCCESS")) {
					Map map = new LinkedHashMap();
					map.put("Ack", setExpressCheckoutResponse.getAck());
					map.put("Token", setExpressCheckoutResponse.getToken());

					session.setAttribute("map", map);

					String sbHtml = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token="
							+ setExpressCheckoutResponse.getToken();
					Map<String, String> resultMap = new HashMap<String, String>();
					resultMap.put("paypalURL", sbHtml);
					resultMap.put("html", "");
					result = JSONArray.fromObject(resultMap).toString();

				} else {
					session.setAttribute("Error",
							setExpressCheckoutResponse.getErrors());

					response.sendRedirect(ServletActionContext
							.getServletContext().getContextPath()
							+ "/Error.jsp");
				}
			}
		
	    }catch (FileNotFoundException e) {
	    	insertErrorLog(orderNo,"请求失败：FileNotFoundException");
			e.printStackTrace();
		} catch (SAXException e) {
			insertErrorLog(orderNo,"请求失败：SAXException");
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			insertErrorLog(orderNo,"请求失败：ParserConfigurationException");
			e.printStackTrace();
		} catch (SSLConfigurationException e) {
			insertErrorLog(orderNo,"请求失败：SSLConfigurationException");
			e.printStackTrace();
		} catch (InvalidCredentialException e) {
			insertErrorLog(orderNo,"请求失败：InvalidCredentialException");
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			insertErrorLog(orderNo,"请求失败：UnsupportedEncodingException");
			e.printStackTrace();
		} catch (HttpErrorException e) {
			insertErrorLog(orderNo,"请求失败：HttpErrorException");
			e.printStackTrace();
		} catch (InvalidResponseDataException e) {
			insertErrorLog(orderNo,"请求失败：InvalidResponseDataException");
			e.printStackTrace();
		} catch (ClientActionRequiredException e) {
			insertErrorLog(orderNo,"请求失败：ClientActionRequiredException");
			e.printStackTrace();
		} catch (MissingCredentialException e) {
			insertErrorLog(orderNo,"请求失败：MissingCredentialException");
			e.printStackTrace();
		} catch (OAuthException e) {
			insertErrorLog(orderNo,"请求失败：OAuthException");
			e.printStackTrace();
		} catch (IOException e) {
			insertErrorLog(orderNo,"请求失败：IOException");
			e.printStackTrace();
		} catch (InterruptedException e) {
			insertErrorLog(orderNo,"请求失败：InterruptedException");
			e.printStackTrace();
		}catch (Exception e) {
			insertErrorLog(orderNo,"请求失败：Exception");
			e.printStackTrace();
		}

	}
	//记录操作失败日志
	public void insertErrorLog(String orderNo,String remark){
		    PayErrorLog errorLog =new PayErrorLog();
		    String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		    HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			String ip=FrontConstant.getIpAddr(request);
			errorLog.setIp(ip);
		    errorLog.setErrorDate(date);
		    errorLog.setRemark(remark);
		    errorLog.setOrderNo(orderNo);
		    orderInfoService.payErrorLog(errorLog);
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
//	public IPaymentCfgService getService() {
//		return service;
//	}
//	public void setService(IPaymentCfgService service) {
//		this.service = service;
//	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	@Action(value = "notifyUrl", results = { @Result( name = SUCCESS, location="../../../../../themes/autel/notify_url.jsp")})
	public String notifyUrl(){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		HttpServletResponse response=ThreadContextHolder.getHttpResponse();
		
		//获取支付宝POST过来反馈信息
		
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
			params.put(name, valueStr);
		}
		try {
			
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
			
			//商户订单号
			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");

			//支付宝交易号
			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");

			//交易状态
			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");

			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
			boolean flag=false;
			if(AlipayNotify.verify(params)){//验证成功
				//////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代码
				
				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				
				if(trade_status.equals("TRADE_FINISHED")){
					//判断该笔订单是否在商户网站中已经做过处理
						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
						//如果有做过处理，不执行商户的业务程序
					//注意：
					//该种交易状态只在两种情况下出现
					//1、开通了普通即时到账，买家付款成功后。
					//2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。
				} else if (trade_status.equals("TRADE_SUCCESS")){
					//判断该笔订单是否在商户网站中已经做过处理
						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
						//如果有做过处理，不执行商户的业务程序
					flag=true;
					orderInfoService.updateOrderPayState(out_trade_no,trade_no,trade_status);
//					orderInfoBackService.processState(out_trade_no); //更新有效期
					
					//注意：
					//该种交易状态只在一种情况下出现——开通了高级即时到账，买家付款成功后。
				}
				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
				result = "success";
				response.getWriter().write("success");
				//out.println("success");	//请不要修改或删除
				System.out.println("success");
			}else{//验证失败
				System.out.println("fail");
				response.getWriter().write("fail");
				//out.println("fail");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
		
	}
	
//	public String getPartner() {
//		return partner;
//	}
//	public void setPartner(String partner) {
//		this.partner = partner;
//	}
//	public String getSeller_email() {
//		return seller_email;
//	}
//	public void setSeller_email(String seller_email) {
//		this.seller_email = seller_email;
//	}
//	public String getKey() {
//		return key;
//	}
//	public void setKey(String key) {
//		this.key = key;
//	}
//
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
//
//	public IPayService getPayService() {
//		return payService;
//	}
//
//	public void setPayService(IPayService payService) {
//		this.payService = payService;
//	}
//
//	public IOrderInfoService getOrderInfoService() {
//		return orderInfoService;
//	}
//
//	public void setOrderInfoService(IOrderInfoService orderInfoService) {
//		this.orderInfoService = orderInfoService;
//	}
	
	
	

}
