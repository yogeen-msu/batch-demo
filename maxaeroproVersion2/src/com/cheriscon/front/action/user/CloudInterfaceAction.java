package com.cheriscon.front.action.user;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.user.service.ICustomerInfoService;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/cloud")
public class CloudInterfaceAction extends WWAction{
	

	@Resource
	private ICustomerInfoService customerInfoService; 
	
	@Action(value = "login")
	public void login() {
		String serialNo=getRequest().getParameter("serialNo");
		String password=getRequest().getParameter("password");
		if(null != serialNo){
			try {
				CustomerInfo customer=null;
					if(StringUtils.isEmpty(password)){
						customer = customerInfoService.getCustomerInfoBySerialNo(serialNo);
					}else{
						customer = customerInfoService.getCustomerInfoBySerialNo(serialNo, password);
					}
					if(customer!=null){
						getResponse().setContentType("text/json;charset=UTF-8");
						getResponse().setCharacterEncoding("UTF-8");
						getResponse().getWriter().write(JSONObject.fromObject(customer).toString());
					}
					
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		
		}
	}
	
}
