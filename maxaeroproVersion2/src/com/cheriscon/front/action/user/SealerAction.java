package com.cheriscon.front.action.user;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ISalesQueryInfoService;
import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.service.IVehicleService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.CustomerProRemark;
import com.cheriscon.common.model.SalesQueryInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.Vehicle;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;
import com.cheriscon.front.usercenter.distributor.service.ICustomerProRemarkService;
import com.cheriscon.user.service.ICustomerInfoService;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/front/sealer")
public class SealerAction extends WWAction {

	@Resource
	private IVehicleService vehicleService;
	@Resource
	private ICustomerProRemarkService customerProRemarkService;
	@Resource
	private ISealerInfoService sealerInfoService;
	@Resource
	private ISealerAuthService sealerAuthService;
	@Resource
	private ISealerDataAuthDeailsService sealerDataAuthDeailsService;
	@Resource
	private ISalesQueryInfoService salesQueryInfoService;
	@Resource
	private ICustomerInfoService customerInfoService;

	private String jsonData;
	private Vehicle vehicle;
	private CustomerProRemark customerProRemark;
	private SealerInfo sealer;
	private SealerAuthVO sealerAuth;
	private String sealerAuthDesc;
	private SalesQueryInfo salesQueryInfo;
	private String serialNo;

	// 客诉获取车型列表
	@Action(value = "getVehicleJson", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getVehicleJson() throws Exception {
		List<Vehicle> listJson = vehicleService.getVehicleList(vehicle);
		jsonData = JSONArray.fromObject(listJson).toString();

		return SUCCESS;
	}

	@Action(value = "addCustomerProRemark", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String addCustomerProRemark() throws Exception {
		jsonData = "SUCCESS";
		try {
			HttpServletRequest request = ThreadContextHolder.getHttpRequest();
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			customerProRemark.setCreateDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			customerProRemark.setCreateUser(sealerInfo.getAutelId());
			customerProRemarkService.addCustomerProRemark(customerProRemark);
		} catch (Exception e) {
			jsonData = "FAIL";
		}
		return SUCCESS;
	}

	@Action(value = "updateSealerStatus", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateSealerStatus() throws Exception {
		jsonData = "SUCCESS";
		try {
			SealerInfo temp = sealerInfoService.getSealerByAutelId(sealer.getAutelId());
			temp.setStatus(0);
			sealerInfoService.updateSealer(temp);
		} catch (Exception e) {
			jsonData = "FAIL";
		}
		return SUCCESS;
	}

	@Action(value = "updateSealerAuthInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateSealerAuthInfo() throws Exception {
		SealerInfo info = sealerInfoService.getSealerByAutelId(sealerAuth.getAutelId());
		if (info == null) {
			jsonData = "FAIL";
		}
		try {
			sealerAuthService.saveAuthDetail(info.getCode(), sealerAuthDesc);
			jsonData = "SUCCESS";
		} catch (Exception e) {
			jsonData = "FAIL";
		}
		return SUCCESS;
	}

	@Action(value = "addSealerInfo", results = { @Result(name = SUCCESS, type = "json", params = {"root", "jsonData" })})
	public String addSealerInfo() throws Exception {
		try {
			jsonData = "SUCCESS";
			SealerInfo temp = sealerInfoService.getSealerByAutelId(sealer.getAutelId());
			if (temp == null) {
				//插入经销商信息表
				HttpServletRequest request = ThreadContextHolder.getHttpRequest();
				SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
				sealer.setAddress(sealerInfo.getAddress() + "_frontSealerInfoAutelId_" + sealerInfo.getAutelId());
				sealer.setAreaName(sealerInfo.getAreaName());
				sealer.setCity(sealerInfo.getCity());
				sealer.setCompany(sealerInfo.getCompany());
				sealer.setCountry(sealerInfo.getCountry());
				sealer.setLanguageCode(sealerInfo.getLanguageCode());
				sealer.setRegTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
				sealer.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(sealer.getUserPwd(),null));
				sealer.setIsAllowSendEmail(sealerInfo.getIsAllowSendEmail());
				sealer.setLastLoginTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
//				sealer.setCode(DBUtils.generateCode(DBConstant.SEALERINFO_CODE));
				sealer.setStatus(1);
				sealerInfoService.insertSealer2(sealer);
//				//插入左边导航权限
//				sealerInfoService.insertSealerAuth("queryCustomer", sealer.getCode());
//				sealerInfoService.insertSealerAuth("sealerComplaint", sealer.getCode());
//				sealerInfoService.insertSealerAuth("updateSealer", sealer.getCode());
//				sealerInfoService.insertSealerAuth("salesList", sealer.getCode());
//				sealerInfoService.insertSealerAuth("rgamanagement", sealer.getCode());
				
//				//插入数据角色权限
//				
//				SealerDataAuthDeails auth = new SealerDataAuthDeails();
//				if (sealerInfo.getAutelId().equals("C-0063")) {
//					auth.setAuthCode("0063,0001");
//				}
//				if (sealerInfo.getAutelId().equals("C-0759")) {
//					auth.setAuthCode("0064,0001");
//				}
//				
//				auth.setSealerCode(sealer.getAutelId());
//				sealerDataAuthDeailsService.insertAuth(auth);
			} else {
				jsonData = "EXIST";
			}
		} catch (Exception e) {
			jsonData = "FAIL";
		}
		
		return SUCCESS;
	}
	
	@Action(value = "updateSalesQueryInfo", results = { @Result(name = SUCCESS, type = "json", params = {"root", "jsonData" }) })
    public String updateSalesQueryInfo() throws Exception{
		jsonData="SUCCESS";
		try{
			HttpServletRequest request = ThreadContextHolder.getHttpRequest();
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			
			if (null != salesQueryInfo && "2".equals(salesQueryInfo.getStatus())) {
				salesQueryInfo.setCloseDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
				salesQueryInfo.setCloseUser(sealerInfo.getAutelId());
			}
			salesQueryInfo.setCustomerName(salesQueryInfo.getFirstName() + " " + salesQueryInfo.getLastName());
			salesQueryInfoService.updateSalesQuery(salesQueryInfo);
		} catch(Exception e) {
			jsonData = "FAIL";
		}
		return SUCCESS;
	}
	
	@Action(value = "addSalesQueryInfo", results = { @Result(name = SUCCESS, type = "json", params = {"root", "jsonData" })})
    public String addSalesQueryInfo() throws Exception{
		jsonData = "SUCCESS";
		try {
			HttpServletRequest request = ThreadContextHolder.getHttpRequest();
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			salesQueryInfo.setCustomerName(salesQueryInfo.getFirstName() + " " + salesQueryInfo.getLastName());
			salesQueryInfo.setCreateDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			salesQueryInfo.setCreateUser(sealerInfo.getAutelId());
			salesQueryInfoService.saveSalesQuery(salesQueryInfo);
		} catch(Exception e) {
			jsonData = "FAIL";
		}
		return SUCCESS;
	}
	
	//客诉获取车型列表
	@Action(value = "getCustomerInfoBySerialNo", results = { @Result(name = SUCCESS, type = "json", params = {
				"root", "jsonData" }) })
	public String getCustomerInfoBySerialNo() throws Exception{
			
		CustomerInfo customerInfo = customerInfoService.getCustBySerialNo3(serialNo);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date proDate = sdf.parse(customerInfo.getProDate());
		Date period =DateUtil.toDate(customerInfo.getProRegTime(), "yyyy-MM-dd");
		
		Date expDate = DateUtil.toDate(customerInfo.getExpTime(), "yyyy-MM-dd");
		customerInfo.setExpTime(new SimpleDateFormat("MM/dd/yyyy").format(expDate));
		customerInfo.setRegTime(new SimpleDateFormat("MM/dd/yyyy").format(period));
		
		int Warrantymonth = customerInfo.getWarrantymonth() == null ? 0:Integer.parseInt(customerInfo.getWarrantymonth());
		
		if (DateUtil.compareDate(DateUtil.toDate(customerInfo.getProDate(), "yyyy-MM-dd"), DateUtil.toDate(customerInfo.getProRegTime(), "yyyy-MM-dd"))) {
			period.setMonth(period.getMonth()+Warrantymonth);
		} else {
			period = DateUtil.toDate(customerInfo.getProDate(), "yyyy-MM-dd");
			period.setMonth(period.getMonth() + Warrantymonth+3);
		}
		customerInfo.setWarrantyDate(new SimpleDateFormat("MM/dd/yyyy").format(period));
		customerInfo.setProDate(new SimpleDateFormat("MM/dd/yyyy").format(proDate));
		
		jsonData = JSONArray.fromObject(customerInfo).toString();	
		return SUCCESS;
		
	}
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public CustomerProRemark getCustomerProRemark() {
		return customerProRemark;
	}

	public void setCustomerProRemark(CustomerProRemark customerProRemark) {
		this.customerProRemark = customerProRemark;
	}

	public SealerInfo getSealer() {
		return sealer;
	}

	public void setSealer(SealerInfo sealer) {
		this.sealer = sealer;
	}

	public SealerAuthVO getSealerAuth() {
		return sealerAuth;
	}

	public void setSealerAuth(SealerAuthVO sealerAuth) {
		this.sealerAuth = sealerAuth;
	}

	public String getSealerAuthDesc() {
		return sealerAuthDesc;
	}

	public void setSealerAuthDesc(String sealerAuthDesc) {
		this.sealerAuthDesc = sealerAuthDesc;
	}

	public SalesQueryInfo getSalesQueryInfo() {
		return salesQueryInfo;
	}

	public void setSalesQueryInfo(SalesQueryInfo salesQueryInfo) {
		this.salesQueryInfo = salesQueryInfo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

}
