package com.cheriscon.front.action.user;

import java.math.BigDecimal;
import java.util.ArrayList;
//import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
//import org.aspectj.weaver.ast.Var;

//import com.cheriscon.app.base.core.model.Smtp;
//import com.cheriscon.app.base.core.service.ISmtpManager;
//import com.cheriscon.backstage.content.constant.CTConsatnt;
//import com.cheriscon.backstage.content.service.IEmailTemplateService;
//import com.cheriscon.backstage.system.service.ILanguageService;
//import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerInfo;
//import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ShoppingCart;
//import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.SessionUtil;
//import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
//import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
//import com.cheriscon.framework.jms.EmailModel;
//import com.cheriscon.framework.jms.EmailProducer;
//import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
//import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
//import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
//import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
//import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.OneOrderInfoVo;
import com.cheriscon.front.usercenter.customer.vo.OrderInfoVO;

/**
 * 订单模块处理类
 * @author caozhiyong
 * @version 2013-1-16
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class OrderInfoAction extends WWAction{

	private static final long serialVersionUID = 1L;
	
	@Resource
	private IOrderInfoService service;
//	
//	private String minSaleUnitList;
//	
//	private String allmoney;//总金额
//	
//	private String discountallmoney;//优惠金额
//	
//	private OrderInfoVO orderInfoVO=new OrderInfoVO();
//	
	private String orderInfoCode;//订单编号
	
	private String subResult="false";//订单结果
	
	private String orderResult;
//	
//	@Resource 
//	private IShoppingCartService shoppingCartService;
//	
//	@Resource
//	private IMinSaleUnitDetailService minSaleUnitDetailService;
//	
//	@Resource
//	private ISmtpManager smtpManager;
//	@Resource
//	private ILanguageService languageService;
//	@Resource
//	private IEmailTemplateService emailTemplateService; 
//	@Resource
//	private EmailProducer emailProducer;
	/**
	 * 生成订单
	 * @return
	 */
	@Action(value = "submintOrderInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "orderResult"}) })
	public String submintOrderInfo(){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		orderResult=service.submitOrderInfo(customerInfo.getCode());
		return "success";
		
	}

	/**
	 * 取消订单
	 * @return
	 */
	@Action(value = "removeOrderInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "orderResult"}) })
	public String removeOrderInfo(){
		boolean removeResult=service.removeOrder(orderInfoCode);
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("result", String.valueOf(removeResult));
		orderResult=JSONArray.fromObject(dataMap).toString();
		return SUCCESS; 
	}
	
//	public String getMinSaleUnitList() {
//		return minSaleUnitList;
//	}
//
//	public void setMinSaleUnitList(String minSaleUnitList) {
//		this.minSaleUnitList = minSaleUnitList;
//	}
//
//	public String getAllmoney() {
//		return allmoney;
//	}
//
//	public void setAllmoney(String allmoney) {
//		this.allmoney = allmoney;
//	}
//
//	public String getDiscountallmoney() {
//		return discountallmoney;
//	}
//
//	public void setDiscountallmoney(String discountallmoney) {
//		this.discountallmoney = discountallmoney;
//	}
//
//	public IOrderInfoService getService() {
//		return service;
//	}
//
//	public void setService(IOrderInfoService service) {
//		this.service = service;
//	}
//
//	public OrderInfoVO getOrderInfoVO() {
//		return orderInfoVO;
//	}
//
//	public void setOrderInfoVO(OrderInfoVO orderInfoVO) {
//		this.orderInfoVO = orderInfoVO;
//	}

	public String getOrderInfoCode() {
		return orderInfoCode;
	}

	public void setOrderInfoCode(String orderInfoCode) {
		this.orderInfoCode = orderInfoCode;
	}

//	public String getSubResult() {
//		return subResult;
//	}
//
//	public void setSubResult(String subResult) {
//		this.subResult = subResult;
//	}

	public String getOrderResult() {
		return orderResult;
	}

	public void setOrderResult(String orderResult) {
		this.orderResult = orderResult;
	}
	

}
