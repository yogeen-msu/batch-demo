package com.cheriscon.front.action.user;
//
//import java.io.InputStream;
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
//import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
//import com.cheriscon.backstage.member.service.ICustomerLoginInfoService;
//import com.cheriscon.backstage.member.service.ICustomerProductService;
//import com.cheriscon.backstage.member.service.ICustomerSoftwareService;
//import com.cheriscon.backstage.member.service.IExportExcelService;
//import com.cheriscon.backstage.member.service.ICustomerInfoVoService;
//import com.cheriscon.backstage.member.service.IProUpgradeRecordService;
//import com.cheriscon.backstage.member.service.impl.ExportExcelServiceImpl;
//import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
//import com.cheriscon.backstage.member.vo.CustomerInfoVo;
//import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;
//import com.cheriscon.backstage.member.vo.CustomerProductVo;
//import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
//import com.cheriscon.backstage.member.vo.ProUpgradeRecordVo;
//import com.cheriscon.backstage.system.service.ILanguageService;
//import com.cheriscon.backstage.trade.service.IOrderInfoVoService;
//import com.cheriscon.backstage.trade.service.IRechargeRecordVoService;
//import com.cheriscon.backstage.trade.vo.OrderInfoVo;
//import com.cheriscon.backstage.trade.vo.RechargeRecordVo;
import com.cheriscon.framework.action.WWAction;
//import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

public class ExportExcelAction  extends WWAction{   
//
//       
//
//    /**
//	 * 
//	 */
//	private static final long serialVersionUID = 181389388018056413L;
//	
//	private InputStream excelStream;
//	private String fileName;
//	
//	private CustomerInfoVo customerInfoVoSel;
//	private CustomerProductVo customerProductVoSel;
//	private ProUpgradeRecordVo proUpgradeRecordVoSel;
//	private CustomerLoginInfoVo customerLoginInfoVoSel;
//	private CustomerSoftwareVo customerSoftwareVoSel;
//	private OrderInfoVo orderInfoVoSel;
//	private CustomerComplaintInfoVo customerComplaintInfoVoSel;
//
//
//	@Resource
//	private ICustomerInfoVoService customerInfoVoService;
//	@Resource
//	private ICustomerProductService customerProductService;
//	@Resource
//	private IProUpgradeRecordService proUpgradeRecordService;
//	@Resource
//	private ICustomerLoginInfoService customerLoginInfoService;
//	@Resource
//	private ICustomerSoftwareService customerSoftwareService;
//	@Resource
//	private IOrderInfoVoService orderInfoVoService;
//	@Resource
//	private ICustomerComplaintInfoVoService customerComplaintInfoVoService;
//
//	@Resource
//	private ILanguageService languageService;
//	@Resource
//	private IRechargeRecordVoService reChargeRecordService;
//	
//	//@Resource
//	IExportExcelService exportExcelService = new ExportExcelServiceImpl();
//	
//
//	private List<CustomerInfoVo> customerInfoVos;
//	private List<CustomerProductVo> customerProductVos;
//	private List<ProUpgradeRecordVo> proUpgradeRecordVos;
//	private List<CustomerLoginInfoVo> customerLoginInfoVos;
//	private List<CustomerSoftwareVo> customerSoftwareVos;
//	private List<OrderInfoVo> orderInfoVos;
//	private List<Map> lMaps;
//	private List<RechargeRecordVo> reChargeRecordVos;
//	private RechargeRecordVo reChargeRecordSel;
//
//       
//
//    public String customerInfo(){
//    	try {
//        	customerInfoVos = customerInfoVoService.listCustomer(customerInfoVoSel);
//
//            excelStream = exportExcelService.getCustomerInfoExcelInputStream(customerInfoVos);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		this.fileName="customerInfo.xls";
//		
//        return "success";   
//
//    }
//    
//    public String customerProduct(){
//    	try {
//        	customerProductVos = customerProductService.listCustomerPro(customerProductVoSel);
//
//            excelStream = exportExcelService.getCustomerProductExcelInputStream(customerProductVos);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		this.fileName="customerProduct.xls";
//
//        return "success";   
//
//    }
//    
//    public String proUpgradeRecord(){
//    	try {
//        	proUpgradeRecordVos = proUpgradeRecordService.listSealer(proUpgradeRecordVoSel);
//
//            excelStream = exportExcelService.getProUpgradeRecordExcelInputStream(proUpgradeRecordVos);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		this.fileName="proUpgradeRecord.xls";
//
//        return "success";   
//
//    }
//    
//    public String customerLoginInfo(){
//    	try {
//        	customerLoginInfoVos = customerLoginInfoService.listSealer(customerLoginInfoVoSel);
//
//            excelStream = exportExcelService.getCustomerLoginInfoExcelInputStream(customerLoginInfoVos);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		this.fileName="customerLoginInfo.xls";
//
//        return "success";   
//
//    }
//    
//    public String customerSoftware(){
//    	try {
//			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
//			customerSoftwareVoSel.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
//        	customerSoftwareVos = customerSoftwareService.listSealer(customerSoftwareVoSel);
//
//            excelStream = exportExcelService.getCustomerSoftwareExcelInputStream(customerSoftwareVos);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		this.fileName="customerSoftware.xls";
//
//        return "success";   
//
//    }
//    
//    public String orderInfo(){
//    	try {
//    		orderInfoVos = orderInfoVoService.listOrderToExcel(orderInfoVoSel);
//
//            excelStream = exportExcelService.getOrderInfoExcelInputStream(orderInfoVos);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		this.fileName="orderInfo.xls";
//
//        return "success";   
//
//    }
//    
//    public String rechargeRecord(){
//    	try {
//    		reChargeRecordVos = reChargeRecordService.listReChargeRecordPage(reChargeRecordSel);
//            excelStream = exportExcelService.getRechargeRecordExcelInputStream(reChargeRecordVos);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		this.fileName="rechargeRecord.xls";
//
//        return "success";   
//
//    }
//    
//    
//    
//    
//    public String complaintStat(){
//    	try {
//    		lMaps = customerComplaintInfoVoService.listCustomerComplaintStat(customerComplaintInfoVoSel);
//
//            excelStream = exportExcelService.getComplaintStatExcelInputStream(lMaps);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		this.fileName="complaintStat.xls";
//
//        return "success";   
//
//    }
//
//
//
//	public InputStream getExcelStream() {
//		return excelStream;
//	}
//
//
//
//	public void setExcelStream(InputStream excelStream) {
//		this.excelStream = excelStream;
//	}
//
//
//
//	public CustomerInfoVo getCustomerInfoVoSel() {
//		return customerInfoVoSel;
//	}
//
//
//
//	public void setCustomerInfoVoSel(CustomerInfoVo customerInfoVoSel) {
//		this.customerInfoVoSel = customerInfoVoSel;
//	}
//
//	public CustomerProductVo getCustomerProductVoSel() {
//		return customerProductVoSel;
//	}
//
//	public void setCustomerProductVoSel(CustomerProductVo customerProductVoSel) {
//		this.customerProductVoSel = customerProductVoSel;
//	}
//
//	public ProUpgradeRecordVo getProUpgradeRecordVoSel() {
//		return proUpgradeRecordVoSel;
//	}
//
//	public void setProUpgradeRecordVoSel(ProUpgradeRecordVo proUpgradeRecordVoSel) {
//		this.proUpgradeRecordVoSel = proUpgradeRecordVoSel;
//	}
//
//	public CustomerLoginInfoVo getCustomerLoginInfoVoSel() {
//		return customerLoginInfoVoSel;
//	}
//
//	public void setCustomerLoginInfoVoSel(CustomerLoginInfoVo customerLoginInfoVoSel) {
//		this.customerLoginInfoVoSel = customerLoginInfoVoSel;
//	}
//
//	public CustomerSoftwareVo getCustomerSoftwareVoSel() {
//		return customerSoftwareVoSel;
//	}
//
//	public void setCustomerSoftwareVoSel(CustomerSoftwareVo customerSoftwareVoSel) {
//		this.customerSoftwareVoSel = customerSoftwareVoSel;
//	}
//
//	public OrderInfoVo getOrderInfoVoSel() {
//		return orderInfoVoSel;
//	}
//
//	public void setOrderInfoVoSel(OrderInfoVo orderInfoVoSel) {
//		this.orderInfoVoSel = orderInfoVoSel;
//	}
//
//	public CustomerComplaintInfoVo getCustomerComplaintInfoVoSel() {
//		return customerComplaintInfoVoSel;
//	}
//
//	public void setCustomerComplaintInfoVoSel(
//			CustomerComplaintInfoVo customerComplaintInfoVoSel) {
//		this.customerComplaintInfoVoSel = customerComplaintInfoVoSel;
//	}
//
//	public String getFileName() {
//		return fileName;
//	}
//
//	public void setFileName(String fileName) {
//		this.fileName = fileName;
//	}
//
//	public RechargeRecordVo getReChargeRecordSel() {
//		return reChargeRecordSel;
//	}
//
//	public void setReChargeRecordSel(RechargeRecordVo reChargeRecordSel) {
//		this.reChargeRecordSel = reChargeRecordSel;
//	}
//
//	public List<RechargeRecordVo> getReChargeRecordVos() {
//		return reChargeRecordVos;
//	}
//
//	public void setReChargeRecordVos(List<RechargeRecordVo> reChargeRecordVos) {
//		this.reChargeRecordVos = reChargeRecordVos;
//	}   


}

