package com.cheriscon.front.action.cart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IDeleteShoppingCartService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;

/**
 * 删除购物车数据
 * @author caozhiyong
 * @version 2013-1-16
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class DeleteShoppingCartAction extends WWAction{

	private static final long serialVersionUID = 1L;
//	@Resource
//	private IDeleteShoppingCartService service;
	
	@Resource
	private IShoppingCartService shoppingCartService;
	
	private String minSaleUnitCodeList;
	
	private String proSerial;//产品序列号
	private String proCode;//产品code
	
	private String data="false";
	
	List<String> minSaleUnitCodes=new ArrayList<String>();
	
	/**
	 * 批量删除购物车数据
	 * @return
	 */
//	@Action(value = "batchDelShoppingCart", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "data" }) })
//	public String batchDelShoppingCart(){
//		Map<String,String> map = new HashMap<String, String>();
//		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
//		String usercode=customerInfo.getCode();//用户code
//		String[] code=minSaleUnitCodeList.split(",");
//		boolean delSuccess=true;
//		for (int i = 0; i < code.length; i++) {
//			String[] pro=code[i].split("-");
//			String proCode=pro[1];
//			String minSaleUnitCode=pro[0];
//			delSuccess=service.delshoppingCart(usercode, minSaleUnitCode,proCode);
//		}
//		
//		
//		if(delSuccess){
//			data="true";
//		}
//		map.put("isSuccess", data);
//		data=JSONArray.fromObject(map).toString();
//		return "success";
//	}
	
	/**
	 * 删除购物车单条数据
	 * @return
	 */
	@Action(value = "delShoppingCart", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "data" }) })
	public String delShoppingCart(){
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		String usercode=customerInfo.getCode();//用户code
		boolean isSuccess=shoppingCartService.delShoppingCart(usercode, proSerial,proCode);
		Map<String,String> map = new HashMap<String, String>();
		map.put("isSuccess", String.valueOf(isSuccess));
		data=JSONArray.fromObject(map).toString();
		return "success";
	}
	
	public String getMinSaleUnitCode() {
		return minSaleUnitCodeList;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCodeList = minSaleUnitCode;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

	public String getMinSaleUnitCodeList() {
		return minSaleUnitCodeList;
	}

	public void setMinSaleUnitCodeList(String minSaleUnitCodeList) {
		this.minSaleUnitCodeList = minSaleUnitCodeList;
	}

	public String getProSerial() {
		return proSerial;
	}

	public void setProSerial(String proSerial) {
		this.proSerial = proSerial;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public List<String> getMinSaleUnitCodes() {
		return minSaleUnitCodes;
	}

	public void setMinSaleUnitCodes(List<String> minSaleUnitCodes) {
		this.minSaleUnitCodes = minSaleUnitCodes;
	}
	
}
