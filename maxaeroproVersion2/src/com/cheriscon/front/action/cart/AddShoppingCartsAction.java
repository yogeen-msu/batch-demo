package com.cheriscon.front.action.cart;

import java.math.BigDecimal;
//import java.util.ArrayList;
import java.util.HashMap;
//import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

//import com.cheriscon.backstage.market.service.IMinSaleUnitService;
//import com.cheriscon.backstage.market.service.IMinSalesUnitMemoService;
//import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.common.model.CustomerInfo;
//import com.cheriscon.common.model.MinSaleUnit;
//import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.model.ShoppingCart;
import com.cheriscon.common.utils.SessionUtil;
//import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;

/**
 * 软件批量加入购物车
 * @author caozhiyong
 * @version 2013-1-30
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class AddShoppingCartsAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Resource
	private IShoppingCartService service;
//	@Resource
//	private IMinSaleUnitService minSaleUnitService;
//	@Resource
//	private ISaleConfigService saleConfigService;
//	@Resource
//	private IMinSalesUnitMemoService minSalesUnitMemoService;
	private String addResult;//结果
//	private String shoppingInfos;
//	
	private String year;
	private String money;
	private BigDecimal allMoney=new BigDecimal(0.00);
	private String serialNo;
	private String minSaleUnitCode;
	
	public BigDecimal getAllMoney() {
		return allMoney;
	}

	public void setAllMoney(BigDecimal allMoney) {
		this.allMoney = allMoney;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

//	@Action(value = "addShoppingCart", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "addResult" }) })
//	public String addShoppingCart(){
//		Map<String,String> map = new HashMap<String, String>();
//		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
//		if (shoppingInfos.isEmpty()) {
//			map.put("addResult", "false");
//		}else {
//			String[] shoppingInfo=shoppingInfos.split(",");
//			
//			List<ShoppingCart> infoVos=new ArrayList<ShoppingCart>();
//			
//			for (int i = 0; i < shoppingInfo.length; i++) {
//				String info=shoppingInfo[i];
//				String[] shoppingCarts=info.split("--");
//				
//				ShoppingCart shoppingCart=new ShoppingCart();
//				
//				shoppingCart.setProName(shoppingCarts[0]);//产品名称		
//				shoppingCart.setProSerial(shoppingCarts[1]);//序列号
//				shoppingCart.setProCode(shoppingCarts[2]);//产品code
//				shoppingCart.setAreaCfgCode(shoppingCarts[3]);//区域配置code
//				shoppingCart.setMinSaleUnitCode(shoppingCarts[4]);//最小销售单位code
//				shoppingCart.setYear(Integer.parseInt(shoppingCarts[5]));//续租年限
//				shoppingCart.setCustomerCode(customerInfo.getCode());
//				shoppingCart.setType(Integer.parseInt(shoppingCarts[6]));//消费类型为续租
//				String picPath=CopContext.getContext().getContextPath()+shoppingCarts[7];
//				shoppingCart.setPicPath(picPath);//产品图片路径
//				shoppingCart.setIsSoft(Integer.parseInt(shoppingCarts[8]));
//				shoppingCart.setCfgPrice(shoppingCarts[9]);
//				shoppingCart.setValidData(shoppingCarts[10]);
//				
//				infoVos.add(shoppingCart);
//			}
//			boolean isSuccess=false;
//			try {
//				isSuccess = service.softAddShoppingCarts(infoVos);
//			} catch (Exception e) {
//				System.out.println(e.getMessage());
//				e.printStackTrace();
//			}
//			map.put("addResult", String.valueOf(isSuccess));
//		}
//		addResult=JSONArray.fromObject(map).toString();
//		System.out.println(addResult);
//		return "success";
//	}
//
//	@Action(value = "checkMinSaleUnitIsUpCode", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "addResult" }) })
//	public String checkMinSaleUnitIsUpCode(){
//		Map<String,String> map = new HashMap<String, String>();
//		if (shoppingInfos.isEmpty()) {
//			map.put("addResult", "false");
//		}else {
//			String[] shoppingInfo=shoppingInfos.split(",");
//			List<MinSaleUnit> minSaleUnits=new ArrayList<MinSaleUnit>();
//			for (int i = 0; i < shoppingInfo.length; i++) {
//				String info=shoppingInfo[i];
//				String[] shoppingCarts=info.split("--");
//				
//				String code=shoppingCarts[4];
//				int isSoft=Integer.parseInt(shoppingCarts[8]);
//				try {
//					if (isSoft ==  1) {
//						minSaleUnits.add(minSaleUnitService.getMinSaleUnitByCode(code));
//					}else{
//						minSaleUnits.addAll(saleConfigService.queryMinSaleUnits(code));
//					}
//				} catch (Exception e) {
//					map.put("addResult", "false");
//				}
//			}
//			
//			for (int i = 0; i < minSaleUnits.size(); i++) {
//				
//				MinSaleUnit minSaleUnit=minSaleUnits.get(i);
//				if (minSaleUnit.getUpCode()!=null && minSaleUnit.getUpCode()!="") {
//					
//					for (int j = 0; j < minSaleUnits.size(); j++) {
//
//						MinSaleUnit minSaleUnit2 = minSaleUnits.get(j);
//
//						if (minSaleUnit.getUpCode().equals(
//								minSaleUnit2.getCode())) {
//
//							HttpServletRequest request = ThreadContextHolder
//									.getHttpRequest();
//							CustomerInfo customerInfo = (CustomerInfo) SessionUtil
//									.getLoginUserInfo(request);// 从session中获取当前登录的用户信息
//
//							MinSaleUnitMemo memo = minSalesUnitMemoService
//									.querMinSaleUnitMemo(
//											minSaleUnit2.getCode(),
//											customerInfo.getLanguageCode());
//							MinSaleUnitMemo memo2 = minSalesUnitMemoService
//									.querMinSaleUnitMemo(minSaleUnit.getCode(),
//											customerInfo.getLanguageCode());
//
//							map.put("addResult", "isupcode");
//							map.put("jia", memo.getName());
//							map.put("yi", memo2.getName());
//							
//							addResult=JSONArray.fromObject(map).toString();
//							return SUCCESS;
//						}
//					}
//				}
//			}
//		}
//		map.put("addResult", "true");
//		addResult=JSONArray.fromObject(map).toString();
//		return SUCCESS;
//	}
	@Action(value = "calculate", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "addResult" }) })
	public String calculate() {
		BigDecimal total=new BigDecimal(0.00);
		Map<String,String> map = new HashMap<String, String>();
		BigDecimal num1=new BigDecimal(year);
		num1.setScale(2, BigDecimal.ROUND_HALF_UP);
		
		BigDecimal num2=new BigDecimal(money);
		num2.setScale(2, BigDecimal.ROUND_HALF_UP);
		total=num1.multiply(num2);
		allMoney=allMoney.add(total);
		map.put("addResult", String.valueOf(allMoney));
		addResult=JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	@Action(value = "updateYear", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "addResult" }) })
	public String updateYear() {
		Map<String,String> map = new HashMap<String, String>();
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		try {
			boolean re = service.updateYear(customerInfo.getCode(), serialNo, minSaleUnitCode, year);
			if(re){
				map.put("addResult", "true");
			}else{
				map.put("addResult", "false");
			}
		} catch (Exception e) {
			map.put("addResult", "false");
			e.printStackTrace();
		}
		
		addResult=JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	
//	public IShoppingCartService getService() {
//		return service;
//	}
//
//	public void setService(IShoppingCartService service) {
//		this.service = service;
//	}

	public String getAddResult() {
		return addResult;
	}

	public void setAddResult(String addResult) {
		this.addResult = addResult;
	}
//
//	public String getShoppingInfos() {
//		return shoppingInfos;
//	}
//	public void setShoppingInfos(String shoppingInfos) {
//		this.shoppingInfos = shoppingInfos;
//	}
//
	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	
}
