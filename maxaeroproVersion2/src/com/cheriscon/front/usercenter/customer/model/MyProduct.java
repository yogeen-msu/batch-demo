package com.cheriscon.front.usercenter.customer.model;

import java.io.Serializable;

/**
 * @date:2012-1-5
 * @author caozhiyong
 * 个人客户 我的产品信息
 */
public class MyProduct implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Integer proID;//产品ID
	private String proCode;//产品Code
	private String proSerialNo;//产品序列号
	private String proRegTime;//产品注册日期
	private String proDate;//产品生产日期
	private String saleContractCode;//销售契约code
	private String proRegPwd;//产品注册密码
	private String proTypeCode;//产品型号Code
	private String noRegReason;//产品解绑原因	
	private String proTypeName;//产品名称
	private Integer regStatus;//注册状态
	private String validDate;//产品到期时间
	private String picPath;//产品型号图片路径
	private String warrantymonth; //产品硬件免费保修月
	private String period;//产品硬件保修期
	private String overdueFlag; //是否已过期
	private String renewButton; //是否显示过期续租按钮
	private String isUpgradeFlag; //是否需要升级
	private String remark; //产品说明，美国分公司需求
    private String rgaFlag; //是否是在RGA处理中

	public String getRgaFlag() {
		return rgaFlag;
	}

	public void setRgaFlag(String rgaFlag) {
		this.rgaFlag = rgaFlag;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsUpgradeFlag() {
		return isUpgradeFlag;
	}

	public void setIsUpgradeFlag(String isUpgradeFlag) {
		this.isUpgradeFlag = isUpgradeFlag;
	}

	public String getRenewButton() {
		return renewButton;
	}

	public void setRenewButton(String renewButton) {
		this.renewButton = renewButton;
	}

	public String getOverdueFlag() {
		return overdueFlag;
	}

	public void setOverdueFlag(String overdueFlag) {
		this.overdueFlag = overdueFlag;
	}

	public MyProduct() {
		super();
		
	}

	public MyProduct(Integer proID, String proCode, String proSerialNo,
			String proRegTime, String proDate, String saleContractCode,
			String proRegPwd, String proTypeCode, String noRegReason,
			String proTypeName, Integer regStatus, String validDate,
			String picPath, String period,String overdueFlag,String isUpgradeFlag) {
		super();
		this.proID = proID;
		this.proCode = proCode;
		this.proSerialNo = proSerialNo;
		this.proRegTime = proRegTime;
		this.proDate = proDate;
		this.saleContractCode = saleContractCode;
		this.proRegPwd = proRegPwd;
		this.proTypeCode = proTypeCode;
		this.noRegReason = noRegReason;
		this.proTypeName = proTypeName;
		this.regStatus = regStatus;
		this.validDate = validDate;
		this.picPath = picPath;
		this.period = period;
		this.overdueFlag=overdueFlag;
		this.isUpgradeFlag=isUpgradeFlag;
	}

	public Integer getProID() {
		return proID;
	}

	public void setProID(Integer proID) {
		this.proID = proID;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getProRegTime() {
		return proRegTime;
	}

	public void setProRegTime(String proRegTime) {
		this.proRegTime = proRegTime;
	}

	public String getProDate() {
		return proDate;
	}

	public void setProDate(String proDate) {
		this.proDate = proDate;
	}

	public String getSaleContractCode() {
		return saleContractCode;
	}

	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}

	public String getProRegPwd() {
		return proRegPwd;
	}

	public void setProRegPwd(String proRegPwd) {
		this.proRegPwd = proRegPwd;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getNoRegReason() {
		return noRegReason;
	}

	public void setNoRegReason(String noRegReason) {
		this.noRegReason = noRegReason;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	public Integer getRegStatus() {
		return regStatus;
	}

	public void setRegStatus(Integer regStatus) {
		this.regStatus = regStatus;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}



	public String getPeriod() {
		return period;
	}



	public void setPeriod(String period) {
		this.period = period;
	}

	public String getWarrantymonth() {
		return warrantymonth;
	}

	public void setWarrantymonth(String warrantymonth) {
		this.warrantymonth = warrantymonth;
	}

}
