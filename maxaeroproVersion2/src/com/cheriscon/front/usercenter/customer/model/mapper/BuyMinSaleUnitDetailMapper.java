package com.cheriscon.front.usercenter.customer.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;

/**
 * 最小销售单位详细信息
 * 
 * @author caozhiyong
 * @version2013-1-10
 */
public class BuyMinSaleUnitDetailMapper implements RowMapper {

	@Override
	public MinSaleUnitDetailVO mapRow(ResultSet arg0, int arg1)
			throws SQLException {
		MinSaleUnitDetailVO detailVO = new MinSaleUnitDetailVO();
		detailVO.setAreaCfgCode(arg0.getString("areaCfgCode"));
		detailVO.setLanguageCode(arg0.getString("languageCode"));
		detailVO.setMemo(arg0.getString("memo"));
		detailVO.setMinSaleUnitCode(arg0.getString("minSaleUnitCode"));
		detailVO.setMinSaleUnitName(arg0.getString("name"));
		detailVO.setPrice(arg0.getString("price"));
		detailVO.setPicpath(arg0.getString("picpath"));
		detailVO.setMemoDetail(arg0.getString("memoDetail"));
		return detailVO;
	}

}
