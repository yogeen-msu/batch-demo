package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cheriscon.common.model.OrderInfo;

/**
 * @ClassName: OrderResult
 * @Description: 订单分页对象
 * @author caozhiyong
 * @date 2013-1-25 
 * 
 */
public class MyOrderPageData implements Serializable{
	
	private static final long serialVersionUID = 7451623405180649063L;

	private OrderInfo orderInfo;
	
	private List<OrderProductPageData> details = new ArrayList<OrderProductPageData>(); //订单最小销售单表详细

	public MyOrderPageData() {
		super();
	}

	public MyOrderPageData(OrderInfo orderInfo,
			List<OrderProductPageData> details) {
		super();
		this.orderInfo = orderInfo;
		this.details = details;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public List<OrderProductPageData> getDetails() {
		return details;
	}

	public void setDetails(List<OrderProductPageData> details) {
		this.details = details;
	}

	
}
