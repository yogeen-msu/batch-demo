/**
*/ 
package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cheriscon.common.model.OrderMinSaleUnitDetail;

/**
 * @ClassName: OrderResult
 * @Description: 订单产品分页对象
 * @author caozhiyong
 * @date 2013-1-25 
 * 
 */
public class OrderProductPageData implements Serializable{
	
	private static final long serialVersionUID = 7451623405180649063L;

	private String orderCode;
	private String proName;//产品名称
	private String proSerialNo;//产品序列号
	private String picPath;//产品图片路径
	private List<OrderMinSaleUnitDetail> minSaleUnitDetails=new ArrayList<OrderMinSaleUnitDetail>(); //订单最小销售单表详细

	public OrderProductPageData() {
		super();
	}



	public OrderProductPageData(String orderCode, String proName,
			String proSerialNo, String picPath,
			List<OrderMinSaleUnitDetail> minSaleUnitDetails) {
		super();
		this.orderCode = orderCode;
		this.proName = proName;
		this.proSerialNo = proSerialNo;
		this.picPath = picPath;
		this.minSaleUnitDetails = minSaleUnitDetails;
	}



	public String getPicPath() {
		return picPath;
	}



	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}



	public String getOrderCode() {
		return orderCode;
	}



	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}



	public String getProName() {
		return proName;
	}



	public void setProName(String proName) {
		this.proName = proName;
	}



	public String getProSerialNo() {
		return proSerialNo;
	}



	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}



	public List<OrderMinSaleUnitDetail> getMinSaleUnitDetails() {
		return minSaleUnitDetails;
	}



	public void setMinSaleUnitDetails(
			List<OrderMinSaleUnitDetail> minSaleUnitDetails) {
		this.minSaleUnitDetails = minSaleUnitDetails;
	}
	
}
