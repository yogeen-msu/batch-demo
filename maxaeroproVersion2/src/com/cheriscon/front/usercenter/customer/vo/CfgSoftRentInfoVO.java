package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cheriscon.common.model.MinSaleUnitMemo;

/**
 * 产品出厂配置软件信息
 * @author caozhiyong
 * @version 2013-1-10
 */
public class CfgSoftRentInfoVO  implements Serializable{

	private static final long serialVersionUID = 1L;

	private String proName;//产品名称;
	private String proSerial;//产品序列号
	private String proCode;//产品code
	private List<MinSaleUnitMemo> minSaleUnitsMemo=new ArrayList<MinSaleUnitMemo>();//出厂配置软件
	private String price;//出厂价格
	private String date;//到期时间
	private int rentYears=1;//续租年限
	private String areaCfgCode;//区域配置code 
	private String picPath;//产品图片
	private String discountPrice;//折后价
	private String saleCfgCode;//销售配置code
	private int isValid=0;//是否过期（0未过期，1已过期）
	private String saleCfgName;//销售配置名称
	private String cfgpicpath;
	private String memo;
	
	public CfgSoftRentInfoVO() {
		super();
	}



	public CfgSoftRentInfoVO(String proName, String proSerial, String proCode,
			List<MinSaleUnitMemo> minSaleUnitsMemo, String price, String date,
			int rentYears, String areaCfgCode, String picPath,
			String discountPrice, String saleCfgCode, int isValid,
			String saleCfgName) {
		super();
		this.proName = proName;
		this.proSerial = proSerial;
		this.proCode = proCode;
		this.minSaleUnitsMemo = minSaleUnitsMemo;
		this.price = price;
		this.date = date;
		this.rentYears = rentYears;
		this.areaCfgCode = areaCfgCode;
		this.picPath = picPath;
		this.discountPrice = discountPrice;
		this.saleCfgCode = saleCfgCode;
		this.isValid = isValid;
		this.saleCfgName = saleCfgName;
	}



	public String getSaleCfgName() {
		return saleCfgName;
	}



	public void setSaleCfgName(String saleCfgName) {
		this.saleCfgName = saleCfgName;
	}



	public String getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}


	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getProName() {
		return proName;
	}


	public void setProName(String proName) {
		this.proName = proName;
	}


	public String getProSerial() {
		return proSerial;
	}


	public void setProSerial(String proSerial) {
		this.proSerial = proSerial;
	}


	public List<MinSaleUnitMemo> getMinSaleUnitsMemo() {
		return minSaleUnitsMemo;
	}


	public void setMinSaleUnitsMemo(List<MinSaleUnitMemo> minSaleUnitsMemo) {
		this.minSaleUnitsMemo = minSaleUnitsMemo;
	}


	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public int getRentYears() {
		return rentYears;
	}


	public void setRentYears(int rentYears) {
		this.rentYears = rentYears;
	}


	public String getSaleCfgCode() {
		return saleCfgCode;
	}


	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}

	public int getIsValid() {
		return isValid;
	}

	public void setIsValid(int isValid) {
		this.isValid = isValid;
	}



	public String getCfgpicpath() {
		return cfgpicpath;
	}



	public void setCfgpicpath(String cfgpicpath) {
		this.cfgpicpath = cfgpicpath;
	}



	public String getMemo() {
		return memo;
	}



	public void setMemo(String memo) {
		this.memo = memo;
	}

	
	
	
}
