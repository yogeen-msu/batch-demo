package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;


/**
 * 产品软件有效状态
 * @author caozhiyong
 * @version 2013-1-8
 */
public class ProductSoftwareValidStatusUpMonthVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String code;
	private String proCode;//产品code
	private String minSaleUnitCode;//最小销售单位
	private String validDate;//有效期
	/**
	 * 免费升级月
	 */
	private Integer upMonth ;
	
	public Integer getUpMonth() {
		return upMonth;
	}
	public void setUpMonth(Integer upMonth) {
		this.upMonth = upMonth;
	}
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	
	public String getValidDate() {	
		return validDate;
	}
	
	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
}
