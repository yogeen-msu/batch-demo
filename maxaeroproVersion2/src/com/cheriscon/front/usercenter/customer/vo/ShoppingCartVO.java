package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 购物车详细信息
 * @author caozhiyong
 * @version 2013-1-15
 */
public class ShoppingCartVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;//id
	private String customerCode;//客户code
	private String minSaleUnitCode;//最小销售单位code
	private String areaCfgCode;//区域配置code
	private String proName;//产品名称
	private String proSerial;//产品序列号
	private String proCode;//产品code
	private Integer year;//续租年限
	private Integer type;//消费类型（0、购买，1、续租）
	private String name;//最小销售单位名称
	private String costPrice;//原价
	private String discountPrice;//折后价
	private String sparePrice="0";//节省
	private String validDate;//有效期
	private String picPath;//产品图片路径
	private Integer isSoft;
	
	public ShoppingCartVO() {
		super();
	}

	public ShoppingCartVO(Integer id, String customerCode,
			String minSaleUnitCode, String areaCfgCode, String proName,
			String proSerial, String proCode, Integer year, Integer type,
			String name, String costPrice, String discountPrice,
			String sparePrice, String validDate, String picPath, Integer isSoft) {
		super();
		this.id = id;
		this.customerCode = customerCode;
		this.minSaleUnitCode = minSaleUnitCode;
		this.areaCfgCode = areaCfgCode;
		this.proName = proName;
		this.proSerial = proSerial;
		this.proCode = proCode;
		this.year = year;
		this.type = type;
		this.name = name;
		this.costPrice = costPrice;
		this.discountPrice = discountPrice;
		this.sparePrice = sparePrice;
		this.validDate = validDate;
		this.picPath = picPath;
		this.isSoft = isSoft;
	}

	public Integer getIsSoft() {
		return isSoft;
	}

	public void setIsSoft(Integer isSoft) {
		this.isSoft = isSoft;
	}

	public String getPicPath() {
		return picPath;
	}



	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	public String getAreaCfgCode() {
		return areaCfgCode;
	}
	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public String getProSerial() {
		return proSerial;
	}
	public void setProSerial(String proSerial) {
		this.proSerial = proSerial;
	}
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}
	public String getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}
	public String getSparePrice() {
		return sparePrice;
	}
	public void setSparePrice(String sparePrice) {
		this.sparePrice = sparePrice;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	
	
}
