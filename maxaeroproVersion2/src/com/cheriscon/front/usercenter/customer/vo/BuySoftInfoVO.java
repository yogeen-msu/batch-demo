package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 自购软件信息
 * @author caozhiyong
 * @version 2013-1-10
 */
public class BuySoftInfoVO  implements Serializable{

	private static final long serialVersionUID = 1L;

	private String proName;//产品名称;
	private String proSerial;//产品序列号
	private String proCode;//产品code
	private String picPath;//产品图片
	private List<MinSaleUnitDetailVO> minSaleUnitDetailVOs=new ArrayList<MinSaleUnitDetailVO>();//最小销售单位
	
	public BuySoftInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}


	


	public BuySoftInfoVO(String proName, String proSerial, String proCode,
			String picPath, List<MinSaleUnitDetailVO> minSaleUnitDetailVOs) {
		super();
		this.proName = proName;
		this.proSerial = proSerial;
		this.proCode = proCode;
		this.picPath = picPath;
		this.minSaleUnitDetailVOs = minSaleUnitDetailVOs;
	}





	public String getPicPath() {
		return picPath;
	}





	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}





	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public String getProSerial() {
		return proSerial;
	}

	public void setProSerial(String proSerial) {
		this.proSerial = proSerial;
	}

	public List<MinSaleUnitDetailVO> getMinSaleUnitDetailVOs() {
		return minSaleUnitDetailVOs;
	}

	public void setMinSaleUnitDetailVOs(
			List<MinSaleUnitDetailVO> minSaleUnitDetailVOs) {
		this.minSaleUnitDetailVOs = minSaleUnitDetailVOs;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}


	
	
}
