package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 购物车产品集合
 * @author caozhiyong
 * @version 2013-3-14
 */
public class ShopingCartsProListVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String proName;//产品名称
	private String proSerial;//产品序列号
	private String proCode;//产品code 
	private String picPath;//产品图片路径
	private List<ShopingCartsMinUnitVO> minUnitVOs=new ArrayList<ShopingCartsMinUnitVO>();

	public ShopingCartsProListVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ShopingCartsProListVO(Integer id, String proName, String proSerial,
			String proCode, String picPath,
			List<ShopingCartsMinUnitVO> minUnitVOs) {
		super();
		this.id = id;
		this.proName = proName;
		this.proSerial = proSerial;
		this.proCode = proCode;
		this.picPath = picPath;
		this.minUnitVOs = minUnitVOs;
	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public String getPicPath() {
		return picPath;
	}



	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}



	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public String getProSerial() {
		return proSerial;
	}

	public void setProSerial(String proSerial) {
		this.proSerial = proSerial;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public List<ShopingCartsMinUnitVO> getMinUnitVOs() {
		return minUnitVOs;
	}

	public void setMinUnitVOs(List<ShopingCartsMinUnitVO> minUnitVOs) {
		this.minUnitVOs = minUnitVOs;
	}
	
	
}
