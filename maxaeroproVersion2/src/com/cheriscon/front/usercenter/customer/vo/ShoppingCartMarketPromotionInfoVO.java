package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 最小销售单位参与活动信息
 * @author caozhiyong
 *
 */
public class ShoppingCartMarketPromotionInfoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ShoppingCartVO shoppingCartVO;
	private MarketPromotionDiscountInfoVO marketPromotionDiscountInfoVO;
	
	public ShoppingCartMarketPromotionInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ShoppingCartMarketPromotionInfoVO(ShoppingCartVO shoppingCartVO,
			MarketPromotionDiscountInfoVO marketPromotionDiscountInfoVO) {
		super();
		this.shoppingCartVO = shoppingCartVO;
		this.marketPromotionDiscountInfoVO = marketPromotionDiscountInfoVO;
	}
	public ShoppingCartVO getShoppingCartVO() {
		return shoppingCartVO;
	}
	public void setShoppingCartVO(ShoppingCartVO shoppingCartVO) {
		this.shoppingCartVO = shoppingCartVO;
	}
	public MarketPromotionDiscountInfoVO getMarketPromotionDiscountInfoVO() {
		return marketPromotionDiscountInfoVO;
	}
	public void setMarketPromotionDiscountInfoVO(
			MarketPromotionDiscountInfoVO marketPromotionDiscountInfoVO) {
		this.marketPromotionDiscountInfoVO = marketPromotionDiscountInfoVO;
	}
	
	
}
