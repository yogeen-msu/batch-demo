package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 订单产品信息
 * @author caozhiyong
 * @version 2013-1-17
 */
public class OneOrderInfoVo  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String minSaleUnitCode;//最小销售单位code		
	private String serial;//序列号
	private String price;//原价
	private Integer year=0;//续租年限
	private Integer type;//消费类型(默认为购买)
	private String picPath;//产品图片路径
	private Integer isSoft;//(0、销售配置,1、最小销售单位)
	
	public OneOrderInfoVo() {
		super();
		// TODO Auto-generated constructor stub
	}


	public OneOrderInfoVo(String minSaleUnitCode, String serial, String price,
			Integer year, Integer type, String picPath, Integer isSoft) {
		super();
		this.minSaleUnitCode = minSaleUnitCode;
		this.serial = serial;
		this.price = price;
		this.year = year;
		this.type = type;
		this.picPath = picPath;
		this.isSoft = isSoft;
	}


	public Integer getIsSoft() {
		return isSoft;
	}


	public void setIsSoft(Integer isSoft) {
		this.isSoft = isSoft;
	}


	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
}
