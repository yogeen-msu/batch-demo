package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 订单最小销售单位詳細信息
 * @author caozhiyong
 * @version 2013-1-23
 */
public class OrderMinSaleUnitInfoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 产品名称
	 */
	private String proName;
	/**
	 * 产品序列号
	 */
	private String productSerialNo;
	/**
	 * 软件名称
	 */
	private String softName;
	
	/**
	 * 消费类型
	 */
	private Integer consumeType;
	/**
	 * 最小销售单位价格
	 */
	private String minSaleUnitPrice;
	
	/**
	 * 订单金额
	 */
	private String orderMoney;
	/**
	 * 下单时间
	 */
	private String orderDate;
	/**
	 * 订单状态
	 */
	private Integer orderState;
	/**
	 * 订单支付状态
	 */
	private Integer orderPayState;
	/**
	 * 优惠金额
	 */
	private String discountMoney;
	
	/**
	 * 订单编号
	 */
	private String orderNo;
	
	public OrderMinSaleUnitInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrderMinSaleUnitInfoVO(String proName, String productSerialNo,
			String softName, Integer consumeType, String minSaleUnitPrice,
			String orderMoney, String orderDate, Integer orderState,
			Integer orderPayState, String discountMoney, String orderNo) {
		super();
		this.proName = proName;
		this.productSerialNo = productSerialNo;
		this.softName = softName;
		this.consumeType = consumeType;
		this.minSaleUnitPrice = minSaleUnitPrice;
		this.orderMoney = orderMoney;
		this.orderDate = orderDate;
		this.orderState = orderState;
		this.orderPayState = orderPayState;
		this.discountMoney = discountMoney;
		this.orderNo = orderNo;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public String getProductSerialNo() {
		return productSerialNo;
	}

	public void setProductSerialNo(String productSerialNo) {
		this.productSerialNo = productSerialNo;
	}

	public String getSoftName() {
		return softName;
	}

	public void setSoftName(String softName) {
		this.softName = softName;
	}

	public Integer getConsumeType() {
		return consumeType;
	}

	public void setConsumeType(Integer consumeType) {
		this.consumeType = consumeType;
	}

	public String getMinSaleUnitPrice() {
		return minSaleUnitPrice;
	}

	public void setMinSaleUnitPrice(String minSaleUnitPrice) {
		this.minSaleUnitPrice = minSaleUnitPrice;
	}

	public String getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(String orderMoney) {
		this.orderMoney = orderMoney;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getOrderState() {
		return orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	public Integer getOrderPayState() {
		return orderPayState;
	}

	public void setOrderPayState(Integer orderPayState) {
		this.orderPayState = orderPayState;
	}

	public String getDiscountMoney() {
		return discountMoney;
	}

	public void setDiscountMoney(String discountMoney) {
		this.discountMoney = discountMoney;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	
}
