package com.cheriscon.front.usercenter.customer.vo;

public class RegProductAccountInfoSecVo {

	private String newProductSN;
	private String oldProductSN;
	
	// from RegProductAcountInfoVO
	private String proTypeCode;// 产品型号code
	private String proSerialNo;// 产品序列号
	private String proRegPwd;// 产品注册密码
	private String sealCode;// 经销商编码
	private String proCode;// 产品编码 newProduct.getCode()
	private String remark;// 说明

	private String customerCode; // 更换产品序列号-用户code
	private String validDate; // 有效期
	private String regTime; // 注册时间
	private String proDate; // 产品出厂日期
	
	// from ProductChangeNoLog
	private String code;
	private String oldProCode; //老产品code
	private String newProCode; //新产品code
	private String regDate; //注册日期，主要是为了计算保修期
	private String createDate;
	private String createUser;
	private String createIp;
	
	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getProRegPwd() {
		return proRegPwd;
	}

	public void setProRegPwd(String proRegPwd) {
		this.proRegPwd = proRegPwd;
	}

	public String getSealCode() {
		return sealCode;
	}

	public void setSealCode(String sealCode) {
		this.sealCode = sealCode;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public String getProDate() {
		return proDate;
	}

	public void setProDate(String proDate) {
		this.proDate = proDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOldProCode() {
		return oldProCode;
	}

	public void setOldProCode(String oldProCode) {
		this.oldProCode = oldProCode;
	}

	public String getNewProCode() {
		return newProCode;
	}

	public void setNewProCode(String newProCode) {
		this.newProCode = newProCode;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateIp() {
		return createIp;
	}

	public void setCreateIp(String createIp) {
		this.createIp = createIp;
	}

	public String getNewProductSN() {
		return newProductSN;
	}

	public void setNewProductSN(String newProductSN) {
		this.newProductSN = newProductSN;
	}

	public String getOldProductSN() {
		return oldProductSN;
	}

	public void setOldProductSN(String oldProductSN) {
		this.oldProductSN = oldProductSN;
	}

}
