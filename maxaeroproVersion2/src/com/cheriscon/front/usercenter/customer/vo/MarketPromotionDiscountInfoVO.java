package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 活动折扣信息
 * @author caozhiyong
 * @version 2013-1-28
 */
public class MarketPromotionDiscountInfoVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String promotionName;
	private Integer promotionType;
	private String startTime;
	private String endTime;
	private double discount;
	private String promotionCode;//活动code
	
	public MarketPromotionDiscountInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MarketPromotionDiscountInfoVO(String promotionName,
			Integer promotionType, String startTime, String endTime,
			Integer discount, String promotionCode) {
		super();
		this.promotionName = promotionName;
		this.promotionType = promotionType;
		this.startTime = startTime;
		this.endTime = endTime;
		this.discount = discount;
		this.promotionCode = promotionCode;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	
}
