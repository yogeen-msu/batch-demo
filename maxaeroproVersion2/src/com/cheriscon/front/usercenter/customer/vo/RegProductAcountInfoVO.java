package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 注册产品账号信息
 * @author caozhiyong
 * @version 2013-1-8
 */
public class RegProductAcountInfoVO  implements Serializable{

	private static final long serialVersionUID = 1L;

	private String proTypeCode;//产品型号code
	private String proSerialNo;//产品序列号
	private String proRegPwd;//产品注册密码
	private String AricraftSerialNumber; //飞机序列号
	private String sealCode;//经销商编码
	private String proCode;//产品编码
	private String remark;// 说明
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public RegProductAcountInfoVO() {
		super();
	}
	
	public RegProductAcountInfoVO(String proTypeCode, String proSerialNo,
			String proRegPwd, String sealCode,String proCode,String AricraftSerialNumber) {
		super();
		this.proTypeCode = proTypeCode;
		this.proSerialNo = proSerialNo;
		this.proRegPwd = proRegPwd;
		this.sealCode = sealCode;
		this.proCode = proCode;
		this.AricraftSerialNumber = AricraftSerialNumber;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getProRegPwd() {
		return proRegPwd;
	}

	public void setProRegPwd(String proRegPwd) {
		this.proRegPwd = proRegPwd;
	}

	public String getSealCode() {
		return sealCode;
	}

	public void setSealCode(String sealCode) {
		this.sealCode = sealCode;
	}
	
	public String getAricraftSerialNumber() {
		return AricraftSerialNumber;
	}

	public void setAricraftSerialNumber(String aricraftSerialNumber) {
		AricraftSerialNumber = aricraftSerialNumber;
	}
	
}
