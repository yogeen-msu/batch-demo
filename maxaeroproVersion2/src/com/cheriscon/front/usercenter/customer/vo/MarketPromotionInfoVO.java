package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 最小销售单位参与的活动信息
 * @author caozhiyong
 * @version 2013-1-28
 */
public class MarketPromotionInfoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String customerCode;// 客户code
	private String minSaleUnitCode;// 最小销售单位code
	private String areaCfgCode;// 区域配置code
	private String proSerial;// 产品序列号
	private String proCode;// 产品code
	private Integer year;// 续租年限
	private Integer type;// 消费类型
	/**
	 * 产品名称
	 */
	private String proName;
	/**
	 * 活动名称
	 */
	private String promotionName;
	/**
	 * 活动类型
	 */
	private Integer promotionType;
	/**
	 * 活动开始时间
	 */
	private String startTime;
	/**
	 * 活动结束时间
	 */
	private String endTime;
	/**
	 * 区域配置code
	 */
	private String areaCode;
	
	/**
	 * 最小销售单位名称
	 */
	private String minSaleUnitName;
	/**
	 * 最小销售单位原价
	 */
	private String costPrice;
	/**
	 * 折扣
	 */
	private Integer discount;
	public MarketPromotionInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MarketPromotionInfoVO(Integer id, String customerCode,
			String minSaleUnitCode, String areaCfgCode, String proSerial,
			String proCode, Integer year, Integer type, String proName,
			String promotionName, Integer promotionType, String startTime,
			String endTime, String areaCode, String minSaleUnitName,
			String costPrice, Integer discount) {
		super();
		this.id = id;
		this.customerCode = customerCode;
		this.minSaleUnitCode = minSaleUnitCode;
		this.areaCfgCode = areaCfgCode;
		this.proSerial = proSerial;
		this.proCode = proCode;
		this.year = year;
		this.type = type;
		this.proName = proName;
		this.promotionName = promotionName;
		this.promotionType = promotionType;
		this.startTime = startTime;
		this.endTime = endTime;
		this.areaCode = areaCode;
		this.minSaleUnitName = minSaleUnitName;
		this.costPrice = costPrice;
		this.discount = discount;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	public String getAreaCfgCode() {
		return areaCfgCode;
	}
	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}
	public String getProSerial() {
		return proSerial;
	}
	public void setProSerial(String proSerial) {
		this.proSerial = proSerial;
	}
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public Integer getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getMinSaleUnitName() {
		return minSaleUnitName;
	}
	public void setMinSaleUnitName(String minSaleUnitName) {
		this.minSaleUnitName = minSaleUnitName;
	}
	public String getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}
	public Integer getDiscount() {
		return discount;
	}
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}
	
	
}
