package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.Language;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;

/**
 * 我的支付说明
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Component("myPayDescWidget")
public class MyPayDescWidget extends RequestParamWidget
{

	@Override
	protected void display(Map<String, String> params) 
	{
		Language language = (Language) CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
		
		if(language != null)
		{
			this.putData("languageCode", language.getLanguageCode());
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
