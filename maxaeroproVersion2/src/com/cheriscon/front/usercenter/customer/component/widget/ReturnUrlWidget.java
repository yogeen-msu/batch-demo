package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

//import com.cheriscon.backstage.order.service.IOrderInfoBackService;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.alipay.util.AlipayNotify;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * 支付宝页面跳转同步通知业务处理
 * @author caozhiyong
 * @version 2013-1-24
 */
@Component("returnUrlWidget")
public class ReturnUrlWidget extends AbstractWidget {

	@Resource
	private IOrderInfoService orderInfoService;
//	@Resource
//	private IOrderInfoBackService orderInfoBackService;
	@SuppressWarnings({ "rawtypes", "unused" })
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		//获取支付宝GET过来反馈信息
		Map<String,String> params2 = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			try {
				valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			params2.put(name, valueStr);
		}
		
		try {
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
			//商户订单号

			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");

			//支付宝交易号

			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");

			//交易状态
			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");

			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
			
			//计算得出通知验证结果
			boolean verify_result = AlipayNotify.verify(params2);
			
			if(verify_result){//验证成功
				//////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代码

				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				 if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")){
					//判断该笔订单是否在商户网站中已经做过处理
						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
						//如果有做过处理，不执行商户的业务程序
					orderInfoService.updateOrderPayState(out_trade_no,trade_no,trade_status);
//					try {
//						orderInfoBackService.processState(out_trade_no); //更新有效期
//					} catch (Exception e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} 
				}
				
				//该页面可做页面美工编辑
				this.putData("result","success");
				this.putData("out_trade_no",out_trade_no);
				//out.println("验证成功<br />");
				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

				//////////////////////////////////////////////////////////////////////////////////////////
			}else{
				
				//该页面可做页面美工编辑
				this.putData("result","fail");
				//out.println("验证失败");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

//	public IOrderInfoService getOrderInfoService() {
//		return orderInfoService;
//	}
//
//	public void setOrderInfoService(IOrderInfoService orderInfoService) {
//		this.orderInfoService = orderInfoService;
//	}

}
