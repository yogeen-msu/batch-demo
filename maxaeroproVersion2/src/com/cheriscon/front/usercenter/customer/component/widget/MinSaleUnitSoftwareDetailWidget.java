package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;

/**
 * 功能軟件描述
 * @author caozhiyong
 * @version 2013-4-9
 */
@Component("minSaleUnitSoftwareDetailWidget")
public class MinSaleUnitSoftwareDetailWidget extends AbstractWidget{

	@Resource
	private IMinSaleUnitDetailService service;
	
	@Override
	protected void display(Map<String, String> params) {

		String softwareVersionCode = params.get("softwareVersionCode");
		String name=params.get("name");
		try {
			name = new String(name.getBytes("ISO-8859-1"),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String versionNo=params.get("versionNo");
		String releaseDate2=params.get("releaseDate");
		
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		//从session中获取当前登录的用户信息	
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);	
	
		LanguagePack languagePack=service.querySoftWareVersionDetail(softwareVersionCode, customerInfo);
		
		MinSaleUnitSoftwareVO detailVO=new MinSaleUnitSoftwareVO();
		detailVO.setName(name);
		detailVO.setReleaseDate2(releaseDate2);
		detailVO.setVersionCode(softwareVersionCode);
		if (languagePack!=null) {
			detailVO.setVersionMemo(languagePack.getMemo());
		}
		detailVO.setVersionNo(versionNo);
		this.putData("minSaleUnitSoftwareVO", detailVO);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

//	public IMinSaleUnitDetailService getService() {
//		return service;
//	}
//
//	public void setService(IMinSaleUnitDetailService service) {
//		this.service = service;
//	}

	
}
