//package com.cheriscon.front.usercenter.customer.component.widget;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.stereotype.Component;
//
//import com.cheriscon.app.cms.component.widget.RequestParamWidget;
//import com.cheriscon.common.model.CustomerInfo;
//import com.cheriscon.common.model.OrderMinSaleUnitDetail;
//import com.cheriscon.common.utils.SessionUtil;
//import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
//import com.cheriscon.framework.util.StringUtil;
//import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.usercenter.customer.service.IBuySoftService;
//import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
//import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
//import com.cheriscon.front.usercenter.customer.vo.BuySoftInfoVO;
//import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
//import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
//
///**
// * 软件购买
// * @author caozhiyong
// * @version 2013-1-11
// */
//@Component("buySoft")
//public class BuySoftWidget extends RequestParamWidget{
//
//	@Resource
//	private IBuySoftService iBuySoftService;
//	
//	private BuySoftInfoVO buySoftInfoVO = new BuySoftInfoVO();
//	
//	@Resource
//	private IOrderInfoService orderInfoService;
//	
//	@Resource
//	private IMinSaleUnitDetailService service;
//	
//	@Override
//	protected void display(Map<String, String> params) {
//		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
//		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
//		String serial  = params.get("serial");//序列号
//		String proName = params.get("proName");//产品名称
//		String saleContractCode = params.get("saleContractCode");//销售契约code
//		String proCode = params.get("proCode");//产品code
//		String picPath = params.get("picPath");//产品图片
//		
//		String pageSize = params.get("pagesize");
//		String pageNoStr = params.get("pageno");
//		Integer[] ids = this.parseId();
//		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
//		
//		Page page=iBuySoftService.queryBuySoftForPage(saleContractCode, proCode, pageNo, Integer.parseInt(pageSize),customerInfo.getLanguageCode());
//		@SuppressWarnings("unchecked")
//		List<MinSaleUnitDetailVO> detailVOs=(List<MinSaleUnitDetailVO>)page.getResult();
//		
//		//构造分页标签
//		StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, page.getTotalCount(), Integer.valueOf(pageSize));
//		String page_html=pagerHtmlBuilder.buildPageHtml();
//		long totalPageCount =page.getTotalPageCount();
//		long totalCount = page.getTotalCount();		
//		
//		buySoftInfoVO.setProCode(proCode);
//		buySoftInfoVO.setProName(proName);
//		buySoftInfoVO.setProSerial(serial);
//		buySoftInfoVO.setPicPath(picPath);
//		
//		buySoftInfoVO.getMinSaleUnitDetailVOs().clear();
//		for (int i = 0; i < detailVOs.size(); i++) {
//			if (detailVOs.get(i).getPrice() != null) {
//				OrderMinSaleUnitDetail orderMinSaleUnitDetail=orderInfoService.queryOrderInfoByInfo(buySoftInfoVO.getProSerial(), FrontConstant.USER_IS_SOFT_BUY, detailVOs.get(i).getMinSaleUnitCode(), customerInfo.getCode());
//				if(orderMinSaleUnitDetail == null){
//					buySoftInfoVO.getMinSaleUnitDetailVOs().add(detailVOs.get(i));
//				}
//			}
//		}
//		
//		List<MinSaleUnitDetailVO> list=buySoftInfoVO.getMinSaleUnitDetailVOs();
//		List<MinSaleUnitDetailVO> minSaleUnitDetailVOs=new ArrayList<MinSaleUnitDetailVO>();
//		for (int i = 0; i < list.size(); i++) {
//			MinSaleUnitDetailVO detailVO=list.get(i);
//			
//			//查询最小销售单位是否可参与活动
//			MarketPromotionDiscountInfoVO  info=service.queryDiscount(customerInfo, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_BUY,detailVO.getDate());
//			String discountPrice=detailVO.getPrice();
//			detailVO.setPrice(Float.valueOf(discountPrice).toString());
//			if (info!=null) {
//				//折后价
//				discountPrice=String.valueOf((Float.parseFloat(detailVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
//			}
//			detailVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
//			minSaleUnitDetailVOs.add(detailVO);
//		}
//		if(this.getData("minSaleUnitS")!=null){
//			this.getData("minSaleUnitS").notifyAll();
//		}
//		this.putData("buySoftInfoVOs",buySoftInfoVO);
//		this.putData("minSaleUnitS",minSaleUnitDetailVOs);
//		this.putData("pager", page_html); 
//		this.putData("pagesize", pageSize);
//		this.putData("pageno", pageNo);
//		this.putData("totalcount", totalCount);
//		this.putData("totalpagecount",totalPageCount);
//		
//	}
//
//	@Override
//	protected void config(Map<String, String> params) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	public IBuySoftService getiBuySoftService() {
//		return iBuySoftService;
//	}
//
//	public void setiBuySoftService(IBuySoftService iBuySoftService) {
//		this.iBuySoftService = iBuySoftService;
//	}
//
//	public BuySoftInfoVO getBuySoftInfoVO() {
//		return buySoftInfoVO;
//	}
//
//	public void setBuySoftInfoVO(BuySoftInfoVO buySoftInfoVO) {
//		this.buySoftInfoVO = buySoftInfoVO;
//	}
//
//	
//}
