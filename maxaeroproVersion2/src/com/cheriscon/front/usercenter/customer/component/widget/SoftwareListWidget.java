package com.cheriscon.front.usercenter.customer.component.widget;

//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
//import com.cheriscon.framework.util.DateUtil;
//import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
//import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;

/**
 * 最小销售单位对应的功能软件
 * @author caozhiyong
 */
@Component("softwareListWidget")
public class SoftwareListWidget extends AbstractWidget{

	@Resource
	private IMinSaleUnitDetailService service;
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		
		int type=Integer.valueOf(params.get("type"));
		
		String saleCfgCode = params.get("saleCfgCode");
		String serialNo=params.get("serialNo");
		
		
//		//功能軟件数据集合
		List<MinSaleUnitSoftwareVO>	softwareVOs= service
					.querySoftWareInfoByContract(customerInfo.getLanguageCode(),saleCfgCode,type);
//		
		CfgSoftRentInfoVO rentInfoVO=null;
		MinSaleUnitDetailVO detailVO=null;
		
		
		this.putData("isRenew", "1");
		this.putData("minSaleUnitInfo",detailVO);
		this.putData("cfgSoftRentInfoVO",rentInfoVO);
		this.putData("serialNo",serialNo);
		this.putData("minSaleUnitSoftwareVO", softwareVOs);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}
//
//	public IMinSaleUnitDetailService getService() {
//		return service;
//	}
//
//	public void setService(IMinSaleUnitDetailService service) {
//		this.service = service;
//	}
//
	
}
