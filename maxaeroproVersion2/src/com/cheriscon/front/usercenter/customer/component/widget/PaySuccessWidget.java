package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * 续租卡续租
 * @author chenqichuan
 * @version 2013-10-8
 */
@Component("paySuccessWidget")
public class PaySuccessWidget extends RequestParamWidget{
	private Logger logger = Logger.getLogger(PaySuccessWidget.class);

	@Resource
	private IOrderInfoService orderInfoService;
	
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String orderNo=(String)request.getSession().getAttribute("orderNo");
//		String SECURETOKEN=request.getSession().getAttribute("SECURETOKEN").toString();
//		String SECURETOKENID=request.getSession().getAttribute("SECURETOKENID").toString();
		this.putData("orderNo",orderNo);
		String RESULT = params.get("RESULT");
		String securetoken= params.get("SECURETOKEN");
		String securetokenid=params.get("SECURETOKENID");
		String PPREF=request.getParameter("PPREF")==null?"":request.getParameter("PPREF");
		Map<String,String[]> map=request.getParameterMap();
		 StringBuffer returnmess = new StringBuffer();
		 for(Iterator iter = map.keySet().iterator(); iter.hasNext();){
	       	String name = (String) iter.next();
				String[] values = (String[]) map.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]: valueStr + values[i] + ",";
				}
				if("get".equals(request.getMethod().toLowerCase())){
					try {
						valueStr = new String(valueStr.getBytes("ISO-8859-1"),"UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
				returnmess.append(name + "=" + valueStr + "&");
	       }
//		 String PPREF=request.getParameter("PPREF")==null?"":request.getParameter("PPREF");
//		  PPREF= "testPPREF";
//		 	RESULT = "0";
		 
		 logger.info("信用卡支付成功返回：PPREF="+PPREF+",全部信息："+returnmess.toString());
		try {
			if("0".equals(RESULT))
			{
			    String res=orderInfoService.updateOrderPayStateByBill(securetoken, securetokenid, orderNo, PPREF, "信用卡支付成功"); //更新状态
				if("success".equals(res)){
					 this.putData("result","Y");
				}else{
					this.putData("result","N");
				}
			}else{
				this.putData("result","N");
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.putData("result","N");
		}
		
	}

	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}


	
}
