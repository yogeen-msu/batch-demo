package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import com.cheriscon.backstage.system.service.SealerDownLoadService;
import com.cheriscon.common.vo.SealerDownLoadVo;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;

/**
 * 经销商资料控制器实现类
 * @author gaoyaxiong
 * @version 创建时间：2016-4-13
 */
@Component("sealerDownLoadWidget")
public class SealerDownLoadWidget extends RequestParamWidget {
	@Resource
	private SealerDownLoadService sealerDownLoadService;
	
	@Override
	protected void display(Map<String, String> params) {
		try {
			String fileType = params.get("fileType");
			
			List<SealerDownLoadVo> fileTypeList = this.sealerDownLoadService.querySealerDownLoadFiles();
			
			List<SealerDownLoadVo> sealerDownLoadList = null;
			if (null != fileTypeList && !fileTypeList.isEmpty()) {
				if (StringUtils.isEmpty(fileType)) {
					fileType = fileTypeList.get(0).getType();
				}
				sealerDownLoadList = this.sealerDownLoadService.querySealerDownLoadList(fileType);
			}
			
			this.putData("fileType", fileType);
			this.putData("fileTypeList", fileTypeList);
		    this.putData("sealerDownLoadList", sealerDownLoadList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
