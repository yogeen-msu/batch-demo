package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.common.model.PayLog;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * 续租卡续租
 * @author chenqichuan
 * @version 2013-10-8
 */
@Component("payErrorWidget")
public class PayErrorWidget extends RequestParamWidget{

	private Logger logger = Logger.getLogger(PayErrorWidget.class);
	
	@Resource
	private IOrderInfoService  orderInfoService;
	
	
	//记录操作失败日志
	   public void insertErrorLog(String orderNo,String remark){
		   PayErrorLog errorLog =new PayErrorLog();
		   String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		   HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		   String ip=FrontConstant.getIpAddr(request);
		   errorLog.setIp(ip);
		   errorLog.setErrorDate(date);
		   errorLog.setRemark(remark);
		   errorLog.setOrderNo(orderNo);
		   orderInfoService.payErrorLog(errorLog);
		}

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String orderNo=request.getSession().getAttribute("orderNo").toString();
		Map<String,String[]> map=request.getParameterMap();
		 StringBuffer returnmess = new StringBuffer();
		 for(Iterator iter = map.keySet().iterator(); iter.hasNext();){
	       	String name = (String) iter.next();
				String[] values = (String[]) map.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]: valueStr + values[i] + ",";
				}
				if("get".equals(request.getMethod().toLowerCase())){
					try {
						valueStr = new String(valueStr.getBytes("ISO-8859-1"),"UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
				returnmess.append(name + "=" + valueStr + "&");
	       }
		 String PPREF=params.get("PPREF")==null?null:params.get("PPREF");
		 
		 logger.info("信用卡支付失败返回：PPREF="+PPREF+",全部信息："+returnmess.toString());
		 
		PayLog payLog=new PayLog();
	    payLog.setOut_trade_no(orderNo);
	    
	    payLog.setResult(FrontConstant.PAY_FALSE);
		
	    String ip=FrontConstant.getIpAddr(request);
	    String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
	    payLog.setIp(ip);
	    payLog.setCreateTime(date);
	    
	    payLog.setTrade_no(PPREF);
	    payLog.setTrade_status("信用卡支付失败");
	    
	    try {
	    	orderInfoService.insertPayLog(payLog);
		} catch (Exception e) {
			insertErrorLog(orderNo,"信用卡失败返回-响应失败：插入支付日志错误");
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}


	
}
