package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.springframework.stereotype.Component;

import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;

import com.cheriscon.front.usercenter.customer.model.PayPal;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * 续租卡续租
 * @author chenqichuan
 * @version 2013-10-8
 */
@Component("payPalReturnWidget")
public class PayPalReturnWidget extends RequestParamWidget{

	@Resource
	private IOrderInfoService orderInfoService;
	

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String orderNo="";
		
		HttpSession session = request.getSession(true);
    	String finalPaymentAmount = (String)session.getAttribute("Payment_Amount");
    	if (isSet(request.getParameter("PayerID")))
    		session.setAttribute("payer_id", request.getParameter("PayerID"));
    	String token = "";
    	if (isSet(request.getParameter("token"))){
    		session.setAttribute("TOKEN", request.getParameter("token"));
    		token = request.getParameter("token");
    	}else{
    		token = (String) session.getAttribute("TOKEN");
    	}

    	// Check to see if the Request object contains a variable named 'token'	
    	PayPal pp = new PayPal();   	
    	Map<String, String> result = new HashMap<String, String>();
    	// If the Request object contains the variable 'token' then it means that the user is coming from PayPal site.	
    	if (isSet(token))
    	{
    		/*
    		* Calls the GetExpressCheckoutDetails API call
    		*/
    		HashMap<String, String> getec = pp.getShippingDetails(session, token );
    		String strAck = getec.get("ACK");	 
    		if(strAck !=null && (strAck.equalsIgnoreCase("SUCCESS") || strAck.equalsIgnoreCase("SUCCESSWITHWARNING") ))
    		{
    			result.putAll(getec);
    			/*
    			* The information that is returned by the GetExpressCheckoutDetails call should be integrated by the partner into his Order Review 
    			* page		
    			*/
//    			String email 				= getec.get("EMAIL"); // ' Email address of payer.
//    			String payerId 			= getec.get("PAYERID"); // ' Unique PayPal customer account identification number.
//    			String payerStatus		= getec.get("PAYERSTATUS"); // ' Status of payer. Character length and limitations: 10 single-byte alphabetic characters.
//    			String firstName			= getec.get("FIRSTNAME"); // ' Payer's first name.
//    			String lastName			= getec.get("LASTNAME"); // ' Payer's last name.
//    			String cntryCode			= getec.get("COUNTRYCODE"); // ' Payer's country of residence in the form of ISO standard 3166 two-character country codes.
//    			String shipToName			= getec.get("PAYMENTREQUEST_0_SHIPTONAME"); // ' Person's name associated with this address.
//    			String shipToStreet		= getec.get("PAYMENTREQUEST_0_SHIPTOSTREET"); // ' First street address.
//    			String shipToCity			= getec.get("PAYMENTREQUEST_0_SHIPTOCITY"); // ' Name of city.
//    			String shipToState		= getec.get("PAYMENTREQUEST_0_SHIPTOSTATE"); // ' State or province
//    			String shipToCntryCode	= getec.get("PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"); // ' Country code. 
//    			String shipToZip			= getec.get("PAYMENTREQUEST_0_SHIPTOZIP"); // ' U.S. Zip code or other country-specific postal code.
//    			String addressStatus 		= getec.get("ADDRESSSTATUS"); // ' Status of street address on file with PayPal 
//    			String totalAmt   		= getec.get("PAYMENTREQUEST_0_AMT"); // ' Total Amount to be paid by buyer
//    			String currencyCode       = getec.get("CURRENCYCODE"); // 'Currency being used 
    			orderNo=getec.get("L_PAYMENTREQUEST_0_NAME0");
    			this.putData("orderNo", orderNo);
    		} 
    		else  
    		{
    			//Display a user friendly Error on the page using any of the following error information returned by PayPal
                String ErrorCode = getec.get("L_ERRORCODE0").toString();
                String ErrorShortMsg = getec.get("L_SHORTMESSAGE0").toString();
                String ErrorLongMsg = getec.get("L_LONGMESSAGE0").toString();
                String ErrorSeverityCode = getec.get("L_SEVERITYCODE0").toString();
                
                String errorString = "SetExpressCheckout API call failed. "+

               		"<br>Detailed Error Message: " + ErrorLongMsg +
    		        "<br>Short Error Message: " + ErrorShortMsg +
    		        "<br>Error Code: " + ErrorCode +
    		        "<br>Error Severity Code: " + ErrorSeverityCode;
                request.setAttribute("error", errorString);
            	/*RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
            	if (dispatcher != null){
            		//dispatcher.forward(request, response);
            	}*/
            }
    	}   
    	

	

    	
    	/*
    	* Calls the DoExpressCheckoutPayment API call
    	*/
    	/*if (isSet(request.getParameter("page")) && request.getParameter("page").equals("return")){  */
	    	HashMap doEC = pp.confirmPayment (session,finalPaymentAmount,request.getServerName() );
	    	String strAck = doEC.get("ACK").toString().toUpperCase();
	    	String TRANSACTIONID=doEC.get("PAYMENTINFO_0_TRANSACTIONID")==null?null:doEC.get("PAYMENTINFO_0_TRANSACTIONID").toString();
	    	
	    	if(strAck !=null && (strAck.equalsIgnoreCase("Success") || strAck.equalsIgnoreCase("SuccessWithWarning"))){
//		    	PayLog payLog=new PayLog();
//			    payLog.setOut_trade_no(orderNo);
//			    payLog.setResult(FrontConstant.PAY_TRUE);
//			    String ip=FrontConstant.getIpAddr(request);
//			    String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
//			    payLog.setIp(ip);
//			    payLog.setCreateTime(date);
//			    
//			    payLog.setTrade_no(TRANSACTIONID);
//			    payLog.setTrade_status("PayPal支付成功");
//			    
//			    try {
//					payService.insertPayLog(payLog);
//				} catch (Exception e) {
//					insertErrorLog(orderNo,"PayPal成功返回-响应失败：插入支付日志错误");
//					e.printStackTrace();
//				}
				
			    orderInfoService.updateOrderPayState(orderNo, TRANSACTIONID, "PayPal支付成功"); //更新状态
				this.putData("result","Y");
		    	
		    	session.removeAttribute("TOKEN");
		    	session.removeAttribute("payer_id");
		    	session.removeAttribute("Payment_Amount");
	    	}else{
		    	//Display a user friendly Error on the page using any of the following error information returned by PayPal
	            String ErrorCode = doEC.get("L_ERRORCODE0").toString();
	            String ErrorShortMsg = doEC.get("L_SHORTMESSAGE0").toString();
	            String ErrorLongMsg = doEC.get("L_LONGMESSAGE0").toString();
	            String ErrorSeverityCode = doEC.get("L_SEVERITYCODE0").toString();	            
	            String errorString = "SetExpressCheckout API call failed. "+	
	           		"<br>Detailed Error Message: " + ErrorLongMsg +
			        "<br>Short Error Message: " + ErrorShortMsg +
			        "<br>Error Code: " + ErrorCode +
			        "<br>Error Severity Code: " + ErrorSeverityCode;
	            this.putData("errorString",errorString);
	            this.putData("result","N");
	    	}
	}
	
	private boolean isSet(Object value){
		return (value !=null && value.toString().length()!=0);
	}
	
	//记录操作失败日志
	   public void insertErrorLog(String orderNo,String remark){
		   PayErrorLog errorLog =new PayErrorLog();
		   String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		   HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		   String ip=FrontConstant.getIpAddr(request);
		   errorLog.setIp(ip);
		   errorLog.setErrorDate(date);
		   errorLog.setRemark(remark);
		   errorLog.setOrderNo(orderNo);
		   orderInfoService.payErrorLog(errorLog);
		}
	
	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}


	
}
