package com.cheriscon.front.usercenter.customer.component.widget;

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


//import net.sf.json.JSONObject;

import org.springframework.stereotype.Component;

import com.cheriscon.app.base.core.model.SiteMenu;
import com.cheriscon.app.base.core.service.ISiteMenuManager;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.RequestUtils;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * 底部模板
 * @author caozhiyong
 * @version 2013-4-19
 */
@Component("footerWidget")
public class FooterWidget extends RequestParamWidget{
	
//	@Resource private ISiteMenuManager siteMenuManager;
//	@Resource private ILanguageService languageService;


	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		StringBuffer url = new StringBuffer();
		url.append("http://");
		url.append(request.getServerName());
		url.append(":");
		url.append(request.getServerPort());
		url.append(request.getContextPath());
		
		this.putData("url",url);
		
		Language language = (Language) CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
		Locale locale = null;
		if (language != null) {
			locale = new Locale(language.getLanguageCode(), language.getCountryCode());
		} else {
			locale = CopContext.getDefaultLocal();
		}
		String ip = RequestUtils.getIpAddr(request);
		String showGoogle=request.getSession().getAttribute("showGoogle")==null?null:request.getSession().getAttribute("showGoogle").toString();
		if(showGoogle==null){
			String flag=getCountry(ip);
			this.putData("flag", flag);
			request.getSession().setAttribute("showGoogle",flag);
		}else{
			this.putData("flag", showGoogle);
		}
		this.putData("flag", showGoogle);
        this.putData("locale",locale);
		/*List<SiteMenu> menuList = siteMenuManager.list(0,"1",languageService.getByLocale(locale));
		this.putData("footMenuList",menuList);*/
	}

	
	public String getCountry(String ip){
		 String flag="NO";
		 /*URLConnection webUrlRequest;
		 String code="";
		 JSONObject jsonObj2=new JSONObject();
		 JSONObject jsonObj=new JSONObject();
		try{
			webUrlRequest = webUrlRequest("http://ip.taobao.com/service/getIpInfo.php?ip="+ip);
			webUrlRequest.setConnectTimeout(3000);
			webUrlRequest.setReadTimeout(3000);
			String jsonString = jsonString(webUrlRequest.getInputStream());
			jsonObj = JSONObject.fromObject(jsonString);
			code=jsonObj.getString("code");
		}catch(Exception ex){
			flag="NO";
		}
		if(code.equals("0")){
		    jsonObj2=jsonObj.getJSONObject("data");
		    String ipCountry=jsonObj2.getString("country");
		    if(ipCountry.equals("中国")||ipCountry.equals("未分配或者内网IP")){
		    	flag="NO";
		    }
		}*/
		return flag;
	}
//	public  URLConnection webUrlRequest(String requestUrl) throws IOException {
//		return new URL(requestUrl).openConnection();
//	}
//	
//	public String jsonString(InputStream inputStream) throws IOException {
//		// List<String> list = new ArrayList<String>();
//		BufferedReader bufferReader = null;
//		try {
//			bufferReader = new BufferedReader(
//					new InputStreamReader(inputStream));
//
//			String readline;
//			while ((readline = bufferReader.readLine()) != null) {
//				if (readline.indexOf("code") != -1) {
//					return readline.toString();
//				}
//			}
//		} finally {
//			inputStream.close();
//			bufferReader.close();
//		}
//		return null;
//
//	}
	
	
	@Override
	protected void config(Map<String, String> params) {
	}


	
}
