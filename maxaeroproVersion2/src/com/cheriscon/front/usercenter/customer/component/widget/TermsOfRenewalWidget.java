package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.cheriscon.cop.sdk.widget.RequestParamWidget;

/**
 * 支付
 * @author caozhiyong
 * @version 2013-1-17
 */
@Component("termsOfRenewalWidget")
public class TermsOfRenewalWidget extends RequestParamWidget{

	@Override
	protected void display(Map<String, String> params) {
		
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

	
}
