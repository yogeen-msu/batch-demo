//package com.cheriscon.front.usercenter.customer.component.widget;
//
//import java.util.Date;
//import java.util.Iterator;
//import java.util.Map;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.stereotype.Component;
//
//import com.cheriscon.backstage.order.service.IOrderInfoBackService;
//import com.cheriscon.common.model.PayLog;
//import com.cheriscon.cop.sdk.widget.AbstractWidget;
//import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
//import com.cheriscon.framework.util.DateUtil;
//import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.usercenter.alipay.util.AlipayNotify;
//import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
//import com.cheriscon.front.usercenter.customer.service.IPayService;
//
///**
// * 支付宝服务器异步通知业务处理
// * @author caozhiyong
// * @version 2013-1-24
// */
//@Component("notifyUrlWidget")
//public class NotifyUrlWidget extends AbstractWidget {
//
//
//	@Resource 
//	private IPayService service ;
//	@Resource
//	private IOrderInfoService orderInfoService;
//	@Resource
//	private IOrderInfoBackService orderInfoBackService;
//	@SuppressWarnings({ "rawtypes"})
//	@Override
//	protected void display(Map<String, String> params) {
//		
//		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//		//获取支付宝POST过来反馈信息
//		//Map<String,String> params = new HashMap<String,String>();
//		Map requestParams = request.getParameterMap();
//		
//		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
//			String name = (String) iter.next();
//			String[] values = (String[]) requestParams.get(name);
//			String valueStr = "";
//			for (int i = 0; i < values.length; i++) {
//				valueStr = (i == values.length - 1) ? valueStr + values[i]
//						: valueStr + values[i] + ",";
//			}
//			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
//			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
//			params.put(name, valueStr);
//		}
//		
//		try {
//			
//			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
//			
//			//商户订单号
//			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
//
//			//支付宝交易号
//			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");
//
//			//交易状态
//			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");
//
//			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
//			boolean flag=false;
//			if(AlipayNotify.verify(params)){//验证成功
//				//////////////////////////////////////////////////////////////////////////////////////////
//				//请在这里加上商户的业务逻辑程序代码
//				orderInfoService.updateOrderPayState(out_trade_no);
//				orderInfoBackService.processState(out_trade_no); //更新有效期
//				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
//				
//				if(trade_status.equals("TRADE_FINISHED")){
//					//判断该笔订单是否在商户网站中已经做过处理
//						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
//						//如果有做过处理，不执行商户的业务程序
//					//注意：
//					//该种交易状态只在两种情况下出现
//					//1、开通了普通即时到账，买家付款成功后。
//					//2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。
//				} else if (trade_status.equals("TRADE_SUCCESS")){
//					//判断该笔订单是否在商户网站中已经做过处理
//						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
//						//如果有做过处理，不执行商户的业务程序
//					flag=true;
//					
//					//注意：
//					//该种交易状态只在一种情况下出现——开通了高级即时到账，买家付款成功后。
//				}
//				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
//					
//				//out.println("success");	//请不要修改或删除
//				System.out.println("success");
//				this.putData("result","success");
//				this.putData("out_trade_no",out_trade_no);
//			}else{//验证失败
//				System.out.println("fail");
//				this.putData("result","fail");				
//				//out.println("fail");
//			}
//			PayLog payLog=new PayLog();
//		    payLog.setOut_trade_no(out_trade_no);
//		    if (flag) {
//		    	payLog.setResult(FrontConstant.PAY_TRUE);
//			}else {
//				payLog.setResult(FrontConstant.PAY_FALSE);
//			}
//		    String ip=FrontConstant.getIpAddr(request);
//		    String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
//		    payLog.setIp(ip);
//		    payLog.setCreateTime(date);
//		    payLog.setTrade_no(trade_no);
//		    payLog.setTrade_status(trade_status);
//		    try {
//		    	service.insertPayLog(payLog);
//				
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		
//		
//		
//	}
//
//	@Override
//	protected void config(Map<String, String> params) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	public IPayService getService() {
//		return service;
//	}
//
//	public void setService(IPayService service) {
//		this.service = service;
//	}
//
//}
