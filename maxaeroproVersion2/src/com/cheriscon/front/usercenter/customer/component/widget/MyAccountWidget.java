package com.cheriscon.front.usercenter.customer.component.widget;

//import java.util.List;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.content.service.IMessageService;
//import com.cheriscon.backstage.content.service.IUserMessageService;
//import com.cheriscon.backstage.system.service.IProductToolsService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ShoppingCart;
//import com.cheriscon.common.model.ProductTypeTools;
//import com.cheriscon.common.utils.JsonHelper;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.usercenter.customer.service.IBuySoftRentService;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;
//import com.google.gson.reflect.TypeToken;

/**
 *  个人客户会员-我的账号
 * @date:2013-3-10
 * @author caozhiyong
 */
@Component("myAccount")
public class MyAccountWidget extends RequestParamWidget {

//	public boolean cacheAble()
//	{
//		return false;
//	}
	
	@Resource
	private IOrderInfoService orderInfoService;
	@Resource
	private IShoppingCartService shoppingCartService;
//	@Resource
//	private  IBuySoftRentService buySoftRentService;
//	@Resource
//	private IUserMessageService userMessageService;
//	@Resource
//	private IProductToolsService toolsService;
//	
	@Resource
	private IMessageService messageService;
	
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		//从session中获取当前登录的用户信息	
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);	
	
		//未支付的订单数量
		int orderNumber=orderInfoService.queryOrderIsNotPay(customerInfo.getCode());
		//购物车数量
		int shoppingCarNumber=shoppingCartService.queryShoppingCartsSoftNumber(customerInfo.getCode());
		//过期软件数量
		int ExpiringSoftNumber=0;//buySoftRentService.querySoftIsExpiring(customerInfo.getCode());
		
		int userMessageCount=0;//用户消息数量
		/*try {
			userMessageCount=userMessageService.queryUserMessageCount(FrontConstant.CUSTOMER_USER_TYPE,customerInfo.getLanguageCode(),customerInfo.getCode());
			userMessageCount = messageService.queryMessageCount(customerInfo.getCode(),null,
					customerInfo.getLanguageCode(), FrontConstant.CUSTOMER_USER_TYPE,
					FrontConstant.USER_MESSAGE_IS_YES_DIALOG, null, null);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		int productTypeToolsNumber=0;
		/*try {
			String languageCode = ((CustomerInfo) SessionUtil
					.getLoginUserInfo(request)).getLanguageCode();
			List<ProductTypeTools> list = toolsService
					.queryProductToolList(languageCode);
			String toolName = "";
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {

					Map<String, String> nameMap = JsonHelper.fromJson(
							list.get(i).getToolName(),
							new TypeToken<Map<String, String>>() {
							}.getType());

					for (String lanCode : nameMap.keySet()) {
						if (!languageCode.equals(lanCode)) {
							continue;
						}

						if (!toolName.equals(nameMap.get(lanCode))) {
							productTypeToolsNumber++;
						} 

						toolName = nameMap.get(lanCode);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}*/
		this.putData("orderNumber",orderNumber);
		this.putData("shoppingCarNumber",shoppingCarNumber);
		this.putData("ExpiringSoftNumber",ExpiringSoftNumber);
		this.putData("userType",request.getParameter("userType"));
		this.putData("userCode",customerInfo.getCode());
		this.putData("userMessageCount",userMessageCount);
		this.putData("toolsDownLoadNumber",productTypeToolsNumber);
		
		try
		{
			this.putData("messageList",messageService.queryUserMessageNew(
					FrontConstant.CUSTOMER_USER_TYPE,
					customerInfo.getLanguageCode(),customerInfo.getCode()));
//			this.putData("messageList",null);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

//	public IOrderInfoService getOrderInfoService() {
//		return orderInfoService;
//	}
//
//	public void setOrderInfoService(IOrderInfoService orderInfoService) {
//		this.orderInfoService = orderInfoService;
//	}
//
//	public IShoppingCartService getShoppingCartService() {
//		return shoppingCartService;
//	}
//
//	public void setShoppingCartService(IShoppingCartService shoppingCartService) {
//		this.shoppingCartService = shoppingCartService;
//	}
//
//	public IBuySoftRentService getBuySoftRentService() {
//		return buySoftRentService;
//	}
//
//	public void setBuySoftRentService(IBuySoftRentService buySoftRentService) {
//		this.buySoftRentService = buySoftRentService;
//	}


}
