package com.cheriscon.front.usercenter.customer.component.widget;

//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsMinUnitVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;

/**
 * 我的购物车（加入购物车）
 * @author caozhiyong
 * @version 2013-1-15
 */
@Component("shoppingCartWidget")
public class ShoppingCartWidget extends RequestParamWidget{

	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource 
	private IShoppingCartService service;
	
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;

	@Override
	protected void display(Map<String, String> params) {
		//int way=Integer.parseInt(params.get("way"));
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		String usercode=customerInfo.getCode();//用户code
		String languageCode=customerInfo.getLanguageCode();//用户首选语言code
		
		String isSuccess=params.get("isSuccess");
		
		List<ShopingCartsProListVO> proListVOs=service.queryShoppingCartByCustomerCode(usercode,languageCode);
		int items=0;
		for (int i = 0; i < proListVOs.size(); i++) {
			for (int j = 0; j < proListVOs.get(i).getMinUnitVOs().size(); j++) {
				
				ShopingCartsMinUnitVO minUnitVO=proListVOs.get(i).getMinUnitVOs().get(j);
//				MarketPromotionDiscountInfoVO info=null;
//				if (minUnitVO.getIsSoft() == 1) {
//					// 查询最小销售单位是否可参与活动
//					if (minUnitVO.getType() == 0) {
//						info = minSaleUnitDetailService.queryDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
//								FrontConstant.USER_IS_SOFT_BUY,
//								minUnitVO.getValidDate());
//					}else{
//						info = minSaleUnitDetailService.queryDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
//								FrontConstant.USER_IS_SOFT_RENT,
//								minUnitVO.getValidDate());
//					}
//					
//					
//				} else {
//					// 查询销售配置是否可参与活动
//					info = minSaleUnitDetailService.querySaleCfgDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
//							FrontConstant.USER_IS_SOFT_RENT,
//									minUnitVO.getValidDate()); 
//				}
//				String discountPrice = minUnitVO.getCostPrice();
//				minUnitVO.setCostPrice(Float.valueOf(discountPrice).toString());
//				if (info != null) {
//					// 折后价
//					discountPrice = String.valueOf((Float.parseFloat(minUnitVO.getCostPrice())*1000 * Float.parseFloat(String.valueOf(info.getDiscount()))) / 10/1000);
//				}
//				minUnitVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
//				if(minUnitVO.getValidDate() != null){
//					Date date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy/MM/dd");
//					Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
//					//当前时间大于有效期时间(已经过期)
//					if(currentDate.after(date)){
//						minUnitVO.setIsValid(1);
//					}
//				}
//				
//				items++;
//				proListVOs.get(i).getMinUnitVOs().set(j, minUnitVO);
				
				if (isSuccess!=null&&isSuccess.equals(proListVOs.get(i).getProSerial()+minUnitVO.getMinSaleUnitCode())) {
					this.putData("price",minUnitVO.getDiscountPrice());
				}else if(isSuccess!=null&&isSuccess.equals("true")){
					if (j == proListVOs.get(i).getMinUnitVOs().size()-1) {
						this.putData("price",minUnitVO.getDiscountPrice());
					}
				}
				
			}
		}
		
		this.putData("items",items);
		if (isSuccess!=null) {
			if (isSuccess.equals("true")) {
				this.putData("type",0);//添加购物车成功
			}else {
				this.putData("type",1);//商品在购物车中已存在
			}
		}
		
		this.putData("shoppingCart",proListVOs);
		
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}
	
}
