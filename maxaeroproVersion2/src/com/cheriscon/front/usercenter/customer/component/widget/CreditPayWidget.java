package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import paypal.payflow.Currency;
import paypal.payflow.Invoice;
import paypal.payflow.PayflowConnectionData;
import paypal.payflow.PayflowUtility;
import paypal.payflow.Response;
import paypal.payflow.SDKProperties;
import paypal.payflow.SaleTransaction;
import paypal.payflow.TransactionResponse;
import paypal.payflow.UserInfo;

import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * @author caozhiyong
 * @version 2013-4-3
 */
@Component("creditPayWidget")
public class CreditPayWidget extends AbstractWidget{

	@Resource
	private IOrderInfoService orderInfoService;
	
	@Override
	protected void display(Map<String, String> params) {
		
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		String orderNo=request.getParameter("orderNo");
        SDKProperties.setHostAddress("payflowpro.paypal.com");
        SDKProperties.setHostPort(443);
        SDKProperties.setTimeOut(45);

        UserInfo user = new UserInfo("autelusinc", "autelusinc", "PayPal", "0116Autelus*123");
        PayflowConnectionData connection = new PayflowConnectionData();
        Invoice inv = new Invoice();

    	OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderNo); //根据订单编号获取价格
		String money=orderInfo.getOrderMoney();
        Currency amt = new Currency(new Double(money), "USD");
        inv.setAmt(amt);
        inv.setPoNum(orderNo);
        inv.setInvNum(orderNo);
        HttpSession session = request.getSession();
        session.setAttribute("orderNo", orderNo);

      //  AuthorizationTransaction trans = new AuthorizationTransaction(user, connection, inv, null, PayflowUtility.getRequestId());
       try{
	        SaleTransaction  trans = new SaleTransaction (user, connection, inv, null, PayflowUtility.getRequestId());
	        trans.setCreateSecureToken("Y");
	        trans.setSecureTokenId(PayflowUtility.getRequestId());
	
	        Response resp = trans.submitTransaction();
//	        session.removeAttribute("SECURETOKEN");
//	        session.removeAttribute("SECURETOKENID");
	        if (resp != null) {
	            TransactionResponse trxnResponse = resp.getTransactionResponse();
	            if (trxnResponse != null&&trxnResponse.getResult()==0) {
	            	String securetoken = trxnResponse.getSecureToken();
	            	String securetokenid = trxnResponse.getSecureTokenId();
					boolean res= orderInfoService.savePayPalToken(orderNo, securetoken, securetokenid);
					if(!res){
						this.putData("SECURETOKEN", "error");
					}else{
//						session.setAttribute("SECURETOKEN", securetoken);
//		            	session.setAttribute("SECURETOKENID", securetokenid);
						this.putData("SECURETOKEN", securetoken);
						this.putData("SECURETOKENID", securetokenid);
					}
	            }
	        }
//	        session.setAttribute("SECURETOKEN", "SECURETOKEN");
//        	session.setAttribute("SECURETOKENID", "SECURETOKENID");
//        	this.putData("SECURETOKEN", "SECURETOKEN");
//			this.putData("SECURETOKENID", "SECURETOKENID");
       }catch (Exception e) {
		e.printStackTrace();
		this.putData("SECURETOKEN", "error");
	}
	}
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
