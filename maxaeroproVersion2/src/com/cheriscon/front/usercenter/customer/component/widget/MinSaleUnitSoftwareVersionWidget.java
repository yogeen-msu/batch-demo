package com.cheriscon.front.usercenter.customer.component.widget;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;

/**
 * 最小销售单位对应的功能软件
 * @author caozhiyong
 */
@Component("minSaleUnitSoftwareVersionWidget")
public class MinSaleUnitSoftwareVersionWidget extends AbstractWidget{

	@Resource
	private IMinSaleUnitDetailService service;
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		
		int type=Integer.valueOf(params.get("type"));
		
		String saleCfgCode = params.get("saleCfgCode");
		String serialNo=params.get("serialNo");
		
		int isRenew=Integer.valueOf(params.get("isRenew"));
		//功能軟件数据集合
		List<MinSaleUnitSoftwareVO>	softwareVOs= service
					.querySoftWareInfo(customerInfo.getLanguageCode(),saleCfgCode,type);
		
		CfgSoftRentInfoVO rentInfoVO=null;
		MinSaleUnitDetailVO detailVO=null;
		if (type == 1) {
			rentInfoVO=service.findSaleConfigDetailInfo(saleCfgCode, customerInfo.getLanguageCode(),serialNo);
			
			MarketPromotionDiscountInfoVO info=service.querySaleCfgDiscount(customerInfo, rentInfoVO.getSaleCfgCode(), rentInfoVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,rentInfoVO.getDate());
			String discountPrice=rentInfoVO.getPrice();
			rentInfoVO.setPrice(Float.valueOf(discountPrice).toString());
			if (info!=null) {
				//折后价
				discountPrice=String.valueOf((Float.parseFloat(rentInfoVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
			}
			Date date = DateUtil.toDate(rentInfoVO.getDate().trim(), "yyyy/MM/dd");
			Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
			//当前时间大于有效期时间(已经过期)
			if(currentDate.after(date)){
				rentInfoVO.setIsValid(1);
			}
			rentInfoVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
			
			
			try {
				Date date2 = new SimpleDateFormat("yyyy/MM/dd").parse(rentInfoVO.getDate());
				rentInfoVO.setDate(DateUtil.toString(date2, "MM/dd/yyyy"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			this.putData("minSaleUnitsMemos",rentInfoVO.getMinSaleUnitsMemo());
		}else {
			detailVO=service.findMinSaleUnitDetailInfo(saleCfgCode, customerInfo.getLanguageCode(),serialNo);
			//查询最小销售单位是否可参与活动
			MarketPromotionDiscountInfoVO  info=null;
			if (isRenew == 1) {
				info=service.queryDiscount(customerInfo, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,detailVO.getDate());
			}else {
				info=service.queryDiscount(customerInfo, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_BUY,detailVO.getDate());
			}
					
			String discountPrice=detailVO.getPrice();
			detailVO.setPrice(Float.valueOf(discountPrice).toString());
			if (info!=null) {
				//折后价
				discountPrice=String.valueOf((Float.parseFloat(detailVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
			}
			detailVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
			
			if (detailVO.getDate()!=null) {
				Date date=null;
				if(detailVO.getDate().indexOf("/")!=-1){
					date = DateUtil.toDate(detailVO.getDate().trim(), "yyyy/MM/dd");
				}else{
					date = DateUtil.toDate(detailVO.getDate().trim(), "yyyy-MM-dd");
				}
				Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
				//当前时间大于有效期时间(已经过期)
				if(currentDate.after(date)){
					detailVO.setIsValid(1);
				}
			}
			
		}
		
		this.putData("minSaleUnitInfo",detailVO);
		this.putData("cfgSoftRentInfoVO",rentInfoVO);
		this.putData("isRenew",isRenew);
		this.putData("serialNo",serialNo);
		this.putData("minSaleUnitSoftwareVO", softwareVOs);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}
//
//	public IMinSaleUnitDetailService getService() {
//		return service;
//	}
//
//	public void setService(IMinSaleUnitDetailService service) {
//		this.service = service;
//	}

	
}
