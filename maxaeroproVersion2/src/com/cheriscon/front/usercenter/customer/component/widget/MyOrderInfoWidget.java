package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * 我的订单
 * @author caozhiyong
 * @version 2013-1-23
 */
@Component("myOrderInfoWidget")
public class MyOrderInfoWidget extends RequestParamWidget {
	
//	public boolean cacheAble()
//	{
//		return false;
//	}
	
	@Resource
	private IOrderInfoService service;
	/**
	 * 查询时间  0近一个月订单,1一个月以前订单
	 */
	private Integer orderDate=0;
	/**
	 * 订单状态  0已取消,1有效,2全部
	 */
	private Integer orderState=2;
	
	/**
	 * 登录用户信息
	 */
	private CustomerInfo customerInfo;
	
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);
		String pageSize = params.get("pagesize");

		String pageNoStr = params.get("pageno");

		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		
		
		if(params.get("orderDate")!=null){
			orderDate=Integer.parseInt(params.get("orderDate"));
		}
		if(params.get("orderState")!=null){
			orderState=Integer.parseInt(params.get("orderState"));
		}
		
		try {
			Page page = service.pageOrderInfo(orderDate, orderState, customerInfo,Integer.valueOf(pageSize),pageNo);
			this.putData("orderDate",orderDate);
			this.putData("orderState",orderState);
			this.putData("orderPageDatas",page.getResult());
			
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, page.getTotalCount(), Integer.valueOf(pageSize));
			String page_html=pagerHtmlBuilder.buildPageHtml();
			long totalPageCount =page.getTotalPageCount();
			long totalCount = page.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

//	public IOrderInfoService getService() {
//		return service;
//	}
//
//	public Integer getOrderDate() {
//		return orderDate;
//	}
//
//	public void setOrderDate(Integer orderDate) {
//		this.orderDate = orderDate;
//	}
//
//	public Integer getOrderState() {
//		return orderState;
//	}
//
//	public void setOrderState(Integer orderState) {
//		this.orderState = orderState;
//	}
//
//	public CustomerInfo getCustomerInfo() {
//		return customerInfo;
//	}
//
//	public void setCustomerInfo(CustomerInfo customerInfo) {
//		this.customerInfo = customerInfo;
//	}
//
//	public void setService(IOrderInfoService service) {
//		this.service = service;
//	}
//
	


	
}
