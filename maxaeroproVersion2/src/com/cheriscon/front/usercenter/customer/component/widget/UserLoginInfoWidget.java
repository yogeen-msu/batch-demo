package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.UserLoginInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.user.service.IUserLoginInfoService;

/**
 * 用户登录信息
 * @author caozhiyong
 * @version 创建时间：2013-1-7
 */
@Component("userLoginInfo")
public class UserLoginInfoWidget extends AbstractWidget {

	@Resource
	private IUserLoginInfoService userLoginInfoService;

	@Override
	protected void display(Map<String, String> params) {

		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
	
		//用户登录信息
		try {
			UserLoginInfo userLoginInfo=userLoginInfoService.getLastLogin(customerInfo.getCode());
			if (userLoginInfo!=null) {
				userLoginInfo.setLoginTime(DateUtil.toString(DateUtil.toDate(userLoginInfo.getLoginTime(),"yyyy-MM-dd hh:mm:ss"),"MM/dd/yyyy hh:mm:ss"));
			}
			this.putData("userLoginInfo", userLoginInfo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.putData("userName", customerInfo.getAutelId());//用户AutelID
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

}
