package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;

/**
 * 续租卡续租
 * @author chenqichuan
 * @version 2013-10-8
 */
@Component("cardSupportWidget")
public class CardSupportWidget extends RequestParamWidget{

	@Resource
	private IMyAccountService myAccountService;
	
	private List<MyProduct> listProduct;
	public List<MyProduct> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<MyProduct> listProduct) {
		this.listProduct = listProduct;
	}

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		
		Object obj=SessionUtil.getLoginUserInfo(request);
		String code="";
		String userType="";
		if(obj.getClass().equals(CustomerInfo.class)){
			CustomerInfo customerInfo=(CustomerInfo)obj;
			this.putData("autelId", customerInfo.getAutelId());
			this.putData("name", customerInfo.getName());
			code=customerInfo.getCode();
			userType="1";
		}
		if(obj.getClass().equals(SealerInfo.class)){
			SealerInfo customerInfo=(SealerInfo)obj;
			this.putData("autelId", customerInfo.getEmail());
			this.putData("name", customerInfo.getName());
			code=customerInfo.getCode();
			userType="2";
		}
		this.putData("userType",userType);
		String cardNum=request.getParameter("cardNum");
		String serialNo=request.getParameter("serialNo");
		this.putData("cardNum",cardNum);
		this.putData("serialNo",serialNo);
		
		try {
			 listProduct=myAccountService.queryProductByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.putData("listProduct", listProduct);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}


	
}
