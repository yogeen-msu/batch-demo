package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.cheriscon.front.usercenter.customer.vo.MyOrderPageData;

/**
 * 订单详情
 * @author caozhiyong
 * @version 2013-3-14
 */
@Component("orderDetailInfoWidget")
public class OrderDetailInfoWidget extends RequestParamWidget {
	
	@Resource
	private IOrderInfoService service;
	
	@Override
	protected void display(Map<String, String> params) {
		String orderCode  = params.get("orderCode");//序列号
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		List<MyOrderPageData> datas=new ArrayList<MyOrderPageData>();
		try {
			datas=service.queryOrderDetailInfoByOrderCode(orderCode,customerInfo.getLanguageCode());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.putData("orderDetailInfos",datas);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

	

	
}
