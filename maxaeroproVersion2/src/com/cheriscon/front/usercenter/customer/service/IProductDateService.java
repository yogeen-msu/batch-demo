package com.cheriscon.front.usercenter.customer.service;

import com.cheriscon.common.model.ProductAssociatesDate;
//import com.cheriscon.common.model.ProductInfo;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;

/**
 * 注册产品业务逻辑实现接口
 * @author caozhiyong
 * @version 2013-1-8
 */
public interface IProductDateService {

	
	
	/**
	 * 查询产品关联日期信息
	 * @param regProductInfoVO
	 * @return
	 */
	public ProductAssociatesDate getProductDateInfo(String proCode);
	
	public  boolean updateProductDate(String proDate,String proCode);
	
	public boolean insertProductDate(ProductAssociatesDate proDate);
	
}
