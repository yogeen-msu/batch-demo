package com.cheriscon.front.usercenter.customer.service;

import java.util.List;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.LanguagePack;
//import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;

/**
 * 最小销售单位业务逻辑实现接口
 * @author caozhiyong
 * @version 2013-1-12
 */
public interface IMinSaleUnitDetailService {

	
	 /**
	  * 最小销售单位功能描述
	  * @param minSaleUnitCode
	  * @param languageCode
	  * @return
	  */
	 public String findMinSaleUnitMemo(String minSaleUnitCode,String languageCode);
	 
	 /**
	  * 销售配置详细信息
	  * @param saleCfgCode
	  * @param langugeCode
	  */
	 public CfgSoftRentInfoVO findSaleConfigDetailInfo(String saleCfgCode,String langugeCode,String serialNo);
	 
	 /**
	  * 最小销售单位详细信息
	  * @param minSaleUnitCode
	  * @param langugeCode
	  */
	 public MinSaleUnitDetailVO findMinSaleUnitDetailInfo(String minSaleUnitCode,String langugeCode,String serialNo);
	 
	 /**
	  * 查询最小销售参与活动的折扣信息
	  * @param minSaleUnitCode
	  * @param areaCfgCode
	  * @param type
	  * @return
	  */
	 public MarketPromotionDiscountInfoVO queryDiscount(CustomerInfo customerInfo,String minSaleUnitCode,String areaCfgCode,Integer type,String date);
	 
	 
	 
	 /**
	  * 查询销售配置参与活动的折扣信息
	  * @param saleCfgCode
	  * @param areaCfgCode
	  * @param type
	  * @return
	  */
	 public MarketPromotionDiscountInfoVO querySaleCfgDiscount(CustomerInfo customerInfo,String saleCfgCode,String areaCfgCode,Integer type,String date);
	 
	 /**
	  * 查询最小销售单位对应的功能软件
	  * @param saleCfgCode
	  */
	 public List<MinSaleUnitSoftwareVO> querySoftWareInfo(String languageCode,String saleCfgCode,int type);
	 
	 /**
	  * 查詢功能軟件描述
	  * @param softwareVersionCode
	  * @return
	  */
	 public LanguagePack querySoftWareVersionDetail(String softwareVersionCode,CustomerInfo customerInfo);
	 
	 
	 /**
	  * 根据销售契约查询最小销售单位对应的功能软件
	  * @param contractCode
	  */
	 public List<MinSaleUnitSoftwareVO> querySoftWareInfoByContract(String languageCode,String contractCode,int type);
	 
}
