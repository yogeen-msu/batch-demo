package com.cheriscon.front.usercenter.customer.service.impl;

//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;
//import java.util.Set;


//import org.springframework.beans.BeanUtils;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JsonConfig;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.cheriscon.backstage.order.vo.OrderInfoVo;
//import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderMinSaleUnitDetail;
import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.common.model.PayLog;
import com.cheriscon.common.utils.HttpUtil;
//import com.cheriscon.common.model.OrderInfo;
//import com.cheriscon.common.model.OrderMinSaleUnitDetail;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
//import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.cheriscon.front.usercenter.customer.vo.MyOrderPageData;
import com.cheriscon.front.usercenter.customer.vo.OrderProductPageData;
//import com.cheriscon.front.usercenter.customer.vo.MyOrderPageData;
//import com.cheriscon.front.usercenter.customer.vo.OneOrderInfoVo;
//import com.cheriscon.front.usercenter.customer.vo.OrderInfoVO;
//import com.cheriscon.front.usercenter.customer.vo.OrderProductPageData;

/**
 * 订单业务逻辑实现类
 * @author caozhiyong
 * @version 2013-1-17
 */
@SuppressWarnings("rawtypes")
@Service
public class OrderInfoServiceImpl implements IOrderInfoService {

	public String  submitOrderInfo(String userCode){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userCode", userCode);
		String res = HttpUtil.postRequestContent("rest.orderInfo.submintOrderInfo", map);
		return res;
	}
	
//	@Transactional
//	@Override
//	public boolean submitOrderInfo(OrderInfoVO orderInfoVO) {
//		String code=orderInfoVO.getOrderCode();
//		boolean flag=true;
//		while(flag==true){
//			StringBuffer sql=new StringBuffer("select count(id) from DT_OrderInfo where code='");
//			sql.append(code).append("'");
//			int num=this.daoSupport.queryForInt(sql.toString());
//			if(num>0){
//				code=DBConstant.getRandStr();
//			}else{
//				flag=false;
//			}
//		};
//		orderInfoVO.setOrderCode(code);
//		
//		//插入数据到订单信息表
//		StringBuffer sql=new StringBuffer();
//		sql.append("insert DT_OrderInfo(code,userCode,orderMoney,orderDate,discountMoney,orderState,orderPayState,recConfirmState,processState)");
//		sql.append("values");
//		sql.append("(");sql.append("'");
//		sql.append(orderInfoVO.getOrderCode());
//		sql.append("'");sql.append(",");sql.append("'");
//		sql.append(orderInfoVO.getCustomerCode());
//		sql.append("'");sql.append(",");sql.append("'");
//		sql.append(orderInfoVO.getAllMoney());
//		sql.append("'");sql.append(",");sql.append("'");
//		sql.append(orderInfoVO.getOrderDate());
//		sql.append("'");sql.append(",");sql.append("'");
//		sql.append(orderInfoVO.getDiscountAllMoney());
//		sql.append("'");sql.append(",");sql.append("1");sql.append(",");
//		sql.append("0");sql.append(",");sql.append("0");
//		sql.append(",");sql.append("0");
//		sql.append(")");
//		
//		try {
//			this.daoSupport.execute(sql.toString(), new Object[]{});
//			
//			//插入数据到订单最小销售单位表
//			for (int i = 0; i < orderInfoVO.getOrderInfoVos().size(); i++) {
//				OneOrderInfoVo orderInfo=orderInfoVO.getOrderInfoVos().get(i);
//				
//				StringBuffer sqlToOrderMinSalUnit=new StringBuffer();
//				sqlToOrderMinSalUnit.append("insert DT_OrderMinSaleUnitDetail(code,orderCode,productSerialNo,minSaleUnitCode,consumeType,minSaleUnitPrice,year,picPath,isSoft)");
//				sqlToOrderMinSalUnit.append("values");
//				sqlToOrderMinSalUnit.append("(");
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(DBUtils.generateCode(DBConstant.ORD_MINSALEUNIT_INFO_CODE));
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(",");
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(orderInfoVO.getOrderCode());
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(",");
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(orderInfo.getSerial());
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(",");
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(orderInfo.getMinSaleUnitCode());
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(",");
//				sqlToOrderMinSalUnit.append(orderInfo.getType());
//				sqlToOrderMinSalUnit.append(",");
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(Double.parseDouble(orderInfo.getPrice()));
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(",");
//				sqlToOrderMinSalUnit.append(orderInfo.getYear());
//				sqlToOrderMinSalUnit.append(",");
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(orderInfo.getPicPath());
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(",");
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(orderInfo.getIsSoft());
//				sqlToOrderMinSalUnit.append("'");
//				sqlToOrderMinSalUnit.append(")");
//				
//				//订单最小销售单位表新增数据
//				this.daoSupport.execute(sqlToOrderMinSalUnit.toString(), new Object[]{});
//				
//				/**
//				 *提交订单成功后将数据从购物车移除
//				 */
//				StringBuffer delShoppingCartSql=new StringBuffer();
//				delShoppingCartSql.append("delete from DT_ShoppingCart where ");
//				delShoppingCartSql.append("customerCode=");
//				delShoppingCartSql.append("'");
//				delShoppingCartSql.append(orderInfoVO.getCustomerCode());
//				delShoppingCartSql.append("'");
//				delShoppingCartSql.append(" and minSaleUnitCode=");
//				delShoppingCartSql.append("'");
//				delShoppingCartSql.append(orderInfo.getMinSaleUnitCode());
//				delShoppingCartSql.append("'");				
//				this.daoSupport.execute(delShoppingCartSql.toString(), new Object[]{});
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//		
//		return true;
//	}
//	
//	
//	@Override
//	public boolean updatePayState(String orderCode) {
//		StringBuffer sql=new StringBuffer();
//		sql.append("update DT_OrderInfo set orderPayState=1 where code=");
//		sql.append("'");
//		sql.append(orderCode);
//		sql.append("'");
//		this.daoSupport.execute(sql.toString(), new Object[]{});
//		return true;
//	}

	@Override
	public boolean removeOrder(String orderNo) {
		String result = HttpUtil.getReqeuestContent("rest.orderInfo.removeOrder", "orderNo="+orderNo);
		if("success".equals(result)){
			return true;
		}else{
			return false;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public Page pageOrderInfo(int orderDate, int orderState,
			CustomerInfo customerInfo,int pageSize,int pageNo) throws Exception  {

		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setRootClass(Page.class);
		Map<String, Class> classMap = new HashMap<String, Class>();
		classMap.put("result", MyOrderPageData.class);
		classMap.put("orderInfo", OrderInfo.class);
		classMap.put("details", OrderProductPageData.class);
		classMap.put("minSaleUnitDetails", OrderMinSaleUnitDetail.class);
		jsonConfig.setClassMap(classMap);
		Page page =  (Page)HttpUtil.getReqeuestContentObject("rest.orderInfo.pageOrderInfo", 
				"orderDate="+orderDate+"&orderState="+orderState+"&pageSize="+pageSize+"&pageNo="+pageNo
				+"&userCode="+customerInfo.getCode()+"&languageCode="+customerInfo.getLanguageCode(), jsonConfig);
		if(null == page){
			page = new Page();
		}
		return page;
	}

//	/**
//	 * 
//	* @Title: convertPageData
//	* @Description: 将查询结果转换为可控显示的分页数据
//	* @param    
//	* @return List<MyOrderPageData>    
//	* @throws
//	 */
//	@SuppressWarnings("unchecked")
//	public List<MyOrderPageData> convertPageData(List<OrderInfoVo> orderInfoVos) throws Exception{
//		
//		List<MyOrderPageData> result = new ArrayList<MyOrderPageData>();
//		
//		Map orderMap = new HashMap();//订单
//		Map detailMap = new HashMap();//订单产品
//		Map minSaleUnitDetailMap = new HashMap();//订单产品最小销售单位
//		
//		for(OrderInfoVo v : orderInfoVos){
//			String code = v.getCode();
//			if(!orderMap.containsKey(code)){
//				OrderInfo info = new OrderInfo();
//				BeanUtils.copyProperties(v, info);//对象copy
//				orderMap.put(code, info);
//			}
//			
//			/**
//			 *订单最小销售单位
//			 */
//			String orderMinSaleUnitCode = v.getDetailCode();
//			
//			if (!minSaleUnitDetailMap.containsKey(orderMinSaleUnitCode)) {
//				OrderMinSaleUnitDetail detail = new OrderMinSaleUnitDetail();
//				BeanUtils.copyProperties(v, detail);
//				minSaleUnitDetailMap.put(orderMinSaleUnitCode, detail);
//			}
//			
//			String proSerialNoOrderCode = v.getProductSerialNo()+"-"+v.getOrderCode();//序列号+订单code
//			
//			if (!detailMap.containsKey(proSerialNoOrderCode)) {
//				OrderProductPageData data = new OrderProductPageData();
//				data.setOrderCode(code);
//				data.setProName(v.getProName());
//				data.setProSerialNo(v.getProductSerialNo());
//				data.setPicPath(v.getPicPath());
//				detailMap.put(proSerialNoOrderCode, data);
//			}
//			
//					
//		}	
//		Set keys = orderMap.keySet();
//		for (Object key : keys) {
//			MyOrderPageData myOrderPageData = new MyOrderPageData();
//			OrderInfo orderInfo = (OrderInfo) orderMap.get(key);
//			
//			List<OrderProductPageData> orderProductPageDatas = new ArrayList<OrderProductPageData>();
//			Set prokeys = detailMap.keySet();
//			for (Object prokey : prokeys) {
//				OrderProductPageData data=(OrderProductPageData)detailMap.get(prokey);
//				
//				List<OrderMinSaleUnitDetail> minSaleUnitlList = new ArrayList<OrderMinSaleUnitDetail>();
//				Set minSaleUnitkeys = minSaleUnitDetailMap.keySet();
//				for (Object minSaleUnitkey : minSaleUnitkeys) {
//					OrderMinSaleUnitDetail minSaleUnit=(OrderMinSaleUnitDetail)minSaleUnitDetailMap.get(minSaleUnitkey);
//					if(data.getOrderCode().equals(minSaleUnit.getOrderCode())&& data.getProSerialNo().equals(minSaleUnit.getProductSerialNo())){
//						minSaleUnitlList.add(minSaleUnit);
//					}
//				}
//				data.setMinSaleUnitDetails(minSaleUnitlList);
//				
//				if(data.getOrderCode().equals(orderInfo.getCode())){
//					orderProductPageDatas.add(data);
//				}
//				
//			}
//			myOrderPageData.setOrderInfo(orderInfo);
//			myOrderPageData.setDetails(orderProductPageDatas);
//			result.add(myOrderPageData);
//		}
//		ComparatorUser comparator=new ComparatorUser();
//		Collections.sort(result, comparator);
//		
//		List<MyOrderPageData> result2 = new ArrayList<MyOrderPageData>();
//		
//		for (int i = 0; i < result.size(); i++) {
//			MyOrderPageData data=result.get(i);
//			Date date=new SimpleDateFormat("yyyy-MM-dd").parse(data.getOrderInfo().getOrderDate());
//			
//			data.getOrderInfo().setOrderDate(new SimpleDateFormat("MM/dd/yyyy").format(date)); 
//			result2.add(data);
//		}
//		return result2;
//	}
//
//	/**
//	 * 集合排序
//	 * 
//	 * @author temp
//	 * 
//	 */
//	public class ComparatorUser implements Comparator {
//
//		public int compare(Object arg0, Object arg1) {
//			MyOrderPageData order0 = (MyOrderPageData) arg0;
//			MyOrderPageData order1 = (MyOrderPageData) arg1;
//
//			// 按订单时间降序排序
//			int flag = order1.getOrderInfo().getOrderDate()
//					.compareTo(order0.getOrderInfo().getOrderDate());
//			return flag;
//		}
//	}

	@Override
	public int queryOrderIsNotPay(String customerCode) {
		
		int num=0;
		String res = HttpUtil.getReqeuestContent("rest.orderInfo.queryOrderIsNotPay", "userCode="+customerCode);
		try{
			num = Integer.valueOf(res);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return num;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<MyOrderPageData> queryOrderDetailInfoByOrderCode(String orderCode,String languageCode) throws Exception {
		
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setRootClass(MyOrderPageData.class);
		Map<String, Class> classMap = new HashMap<String, Class>();
		classMap.put("orderInfo", OrderInfo.class);
		classMap.put("details", OrderProductPageData.class);
		classMap.put("minSaleUnitDetails", OrderMinSaleUnitDetail.class);
		jsonConfig.setClassMap(classMap);
		return (List<MyOrderPageData>) HttpUtil.getReqeuestContentList("rest.orderInfo.queryOrderDetailInfoByOrderCode",
				"orderCode="+orderCode+"&languageCode="+languageCode, jsonConfig);
	}


	@Override
	public boolean updateOrderPayState(String orderCode,String tradeNo,String tradeStatus) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("orderCode", orderCode);
		map.put("tradeNo", tradeNo);
		map.put("tradeStatus", tradeStatus);
		String result = HttpUtil.postRequestContent("rest.orderInfo.updateOrderPayState", map);
		if("success".equals(result)){
			return true;
		}
		return false;
	}


	@Override
	public OrderInfo queryOrderInfoByCode(String code) {
		return (OrderInfo) HttpUtil.getReqeuestContentObject("rest.orderInfo.queryOrderInfoByCode", "orderCode="+code, OrderInfo.class);
	}
//	
//	/**
//	 * 修改订单信息
//	 * @param orderInfo
//	 * @throws Exception
//	 */
//	@SuppressWarnings("unchecked")
//	@Transactional
//	public int updateOrderInfo(OrderInfo orderInfo) throws Exception {
//		this.daoSupport.update("DT_OrderInfo", orderInfo, "code='"+orderInfo.getCode()+"'");
//		return 0;
//	}
//
//
//	@Override
//	public OrderMinSaleUnitDetail queryOrderInfoByInfo(String serialNo, int consumeType,
//			String minSaleUnitCode, String userCode) {
//		StringBuffer sql=new StringBuffer();
//		sql.append("select m.* from dt_orderinfo o,DT_OrderMinSaleUnitDetail m where " +
//				"o.code=m.orderCode and o.orderPayState='1'");
//		sql.append(" and m.consumeType=");
//		sql.append("'");
//		sql.append(consumeType);
//		sql.append("'");
//		sql.append(" and m.productSerialNo=");
//		sql.append("'");
//		sql.append(serialNo);
//		sql.append("'");
//		sql.append(" and m.minSaleUnitCode=");
//		sql.append("'");
//		sql.append(minSaleUnitCode);
//		sql.append("'");
//		sql.append(" and o.userCode=");
//		sql.append("'");
//		sql.append(userCode);
//		sql.append("'");
//		
//		OrderMinSaleUnitDetail  orderMinSaleUnitDetail=(OrderMinSaleUnitDetail)this.daoSupport.queryForObject(sql.toString(), OrderMinSaleUnitDetail.class, new Object[]{});
//		return orderMinSaleUnitDetail;
//	}

	public void payErrorLog(PayErrorLog errorLog){
		HttpUtil.postRequestContent("rest.orderInfo.payErrorLog","payErrorLog.", errorLog);
	}

	@Override
	public void insertPayLog(PayLog payLog) {
		HttpUtil.postRequestContent("rest.orderInfo.insertPayLog","payLog.", payLog);
	}

	@Override
	public boolean savePayPalToken(String orderNo, String secureToken,
			String secureTokenId) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("orderNo", orderNo);
		map.put("secureToken", secureToken);
		map.put("secureTokenId", secureTokenId);
		String res = HttpUtil.postRequestContent("rest.orderInfo.savePayPalToken",map);
		if("success".equals(res)){
			return true;
		}
		return false;
	}

	@Override
	public String updateOrderPayStateByBill(String secureToken,
			String secureTokenId, String orderCode, String tradeNo,
			String tradeStatus) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("secureToken", secureToken);
		map.put("secureTokenId", secureTokenId);
		map.put("orderCode", orderCode);
		map.put("tradeNo", tradeNo);
		map.put("tradeStatus", tradeStatus);
		return HttpUtil.postRequestContent("rest.orderInfo.updateOrderPayStateByBill",map);
	}
	
}
