package com.cheriscon.front.usercenter.customer.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.common.utils.HttpUtil;
//import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.usercenter.customer.service.IProductDateService;

@Service
public class ProductDateServiceImpl  implements IProductDateService {

	@Override
	public ProductAssociatesDate getProductDateInfo(String proCode) {
//		String sql="select * from dt_ProductAssociatesDate where proCode='"+proCode+"'";
//		return (ProductAssociatesDate) this.daoSupport.queryForObject(sql, ProductAssociatesDate.class);
	return (ProductAssociatesDate) HttpUtil.getReqeuestContentObject("rest.productDate.get", "proCode="+proCode, ProductAssociatesDate.class);
	}

	public  boolean updateProductDate(String proDate,String proCode) {
//		String sql="update dt_ProductAssociatesDate set associatesDate='"+proDate+"'  where proCode='"+proCode+"'";
//		this.daoSupport.execute(sql, null);
//		return true;
		Map<String, String> map = new HashMap<String, String>();
		map.put("associatesDate", proDate);
		map.put("proCode", proCode);
		String result = HttpUtil.postRequestContent("rest.productDate.update", map);
		if("success".equals(result)){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean insertProductDate(ProductAssociatesDate proDate){
//		this.daoSupport.insert("dt_ProductAssociatesDate", proDate);
//		return true;
		String result = HttpUtil.postRequestContent("rest.productDate.add","proDate.", proDate);
		if("success".equals(result)){
			return true;
		}else{
			return false;
		}
	}
	
}
