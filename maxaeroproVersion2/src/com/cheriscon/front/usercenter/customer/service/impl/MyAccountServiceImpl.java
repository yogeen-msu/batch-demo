package com.cheriscon.front.usercenter.customer.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JsonConfig;

import org.springframework.stereotype.Service;

//import com.cheriscon.backstage.product.service.ISoftwareVersionService;
//import com.cheriscon.backstage.system.service.ISoftListService;
//import com.cheriscon.common.model.SoftList;
//import com.cheriscon.common.model.SoftwareVersion;
//import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;

/**
 * date 2013-1-5
 * 个人客户我的产品信息实现类
 * @author caozhiyong 
 */
@Service
public class MyAccountServiceImpl implements
		IMyAccountService {

//	@Resource
//	private ISoftwareVersionService softwareVersionService;
//	@Resource
//	private ISoftListService softListService;
	
	
	@Override
	public Page queryProducts(String remark,String usercode, int pageNo, int pageSize)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("remark", remark);
		map.put("usercode", usercode);
		map.put("pageNo", pageNo);
		map.put("pageSize", pageSize);
		return HttpUtil.postRequestContentPage("rest.regProduct.queryProducts", map, MyProduct.class);
	}

	
	//比较两个软件版本
//	public String compareVersion(List<SoftwareVersion> versionList,List<SoftList> softList){
//		String isUpgradeFlag="0";
//		if(versionList.size()>softList.size()){
//			isUpgradeFlag="1";
//		}else{
//			for(int i=0;i<versionList.size();i++){
//				SoftwareVersion softVersion=versionList.get(i);
//				for(int j=0;j<softList.size();j++){
//					SoftList soft=softList.get(j);
//					if(soft.getSoftCode().equals(softVersion.getCode()) &&
//							!softVersion.getVersionName().equals(soft.getSoftVersion()))
//					
//					{
//						isUpgradeFlag="1";
//						break;
//					}
//				}
//				
//			}
//			
//			
//		}
//		
//		return isUpgradeFlag ;
//		
//	}
	
	@Override
	public int queryProductCount(String userCode) {
		String result = HttpUtil.getReqeuestContent("rest.regProduct.queryProductCount",
				"customerCode="+userCode);
		try{
			return Integer.valueOf(result);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	@Override
	public List<MyProduct> queryProductByCode(String userCode) throws Exception{
		
		return (List<MyProduct>) HttpUtil.getReqeuestContentList("rest.regProduct.queryProductByUserCode", "usercode="+userCode, MyProduct.class);
	}
	
	/**
	 * 查询客户即将到期产品
	 * @param userCode
	 * @return
	 */
	public List<MyProduct> queryRenewByCode(String userCode) throws Exception{
		
		return (List<MyProduct>)HttpUtil.getReqeuestContentList("rest.user.queryRenewByCode",
				"customerInfoCode="+userCode, MyProduct.class);
	}
	
	public int queryCustomerByProCode(String proCode) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select count(cp.id) from DT_CustomerProInfo cp where cp.proCode=?");
//		return this.daoSupport.queryForInt(sql.toString(), proCode);
		return 0;
	}
	
//	private int  compareDate(String s1,String s2){
//		java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd");
//		java.util.Calendar c1=java.util.Calendar.getInstance();
//		java.util.Calendar c2=java.util.Calendar.getInstance();
//		try {
//			c1.setTime(df.parse(s1));
//			c1.add(Calendar.MONTH, 3);
//			c2.setTime(df.parse(s2));
//			int result=c1.compareTo(c2);
//			System.out.println(result);
//			return  result;
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return 0;
//	}
	
}
