package com.cheriscon.front.usercenter.customer.service.impl;

//import java.util.Date;
//
//import javax.servlet.http.HttpServletRequest;

import java.util.Date;

import org.springframework.stereotype.Service;
//
//import com.cheriscon.common.model.CustomerInfo;
//import com.cheriscon.common.model.SealerInfo;
//import com.cheriscon.common.utils.SessionUtil;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.cop.sdk.utils.DateUtil;
//import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
//import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.user.vo.SystemErrorLog;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;

@Service
public class SystemErrorLogServiceImpl implements ISystemErrorLogService {

	@Override
	public void saveLog(String type,String errorMsg) {
//		SystemErrorLog errorLog=new SystemErrorLog();
//		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//		Object obj=SessionUtil.getLoginUserInfo(request);
//		if(obj==null){
//			errorLog.setUserCode("");
//		}
//		else 
//			{if(obj.getClass().equals(CustomerInfo.class)){
//				errorLog.setUserCode(((CustomerInfo)obj).getCode());
//			}
//			if(obj.getClass().equals(SealerInfo.class)){
//				errorLog.setUserCode(((SealerInfo)obj).getCode());
//			}
//		}
//		String ip=FrontConstant.getIpAddr(request);
//		String createDate=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss");
//		errorLog.setOperateIp(ip);
//		errorLog.setOperateTime(createDate);
//		errorLog.setErrorMsg(errorMsg);
//		errorLog.setType(type);
//		this.daoSupport.insert("DT_SystemErrorLog", errorLog);
		System.out.println(errorMsg);
	}

	@Override
	public void saveLog(String type, Exception ex,String clazz) {
		ex.printStackTrace();
		String errorMsg="";
		StackTraceElement[] st = ex.getStackTrace();
		for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if(clazz.equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
				saveLog("用户注册提交保存信息",errorMsg);
			}
		}
	}

}
