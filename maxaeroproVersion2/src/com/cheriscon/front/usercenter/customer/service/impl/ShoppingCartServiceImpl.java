package com.cheriscon.front.usercenter.customer.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JsonConfig;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.ShoppingCart;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsMinUnitVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;
import com.cheriscon.front.usercenter.customer.vo.ShoppingCartVO;

/**
 * 购物车业务逻辑实现类
 * @author caozhiyong
 * @version 2013-1-15
 */
@SuppressWarnings("rawtypes")
@Service
public class ShoppingCartServiceImpl  implements IShoppingCartService {

	@Override
	public String addShoppingCartIsRent(String customerCode, String serialNo, String minSaleUnitCode,
			String areaCfgCode, int type, int year,String picPath,int isSoft,String languageCode) {
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("customerCode", customerCode);
		param.put("serialNo", serialNo);
		param.put("minSaleUnitCode", minSaleUnitCode);
		param.put("areaCfgCode", areaCfgCode);
		param.put("type", type);
		param.put("year", year);
		param.put("picPath", picPath);
		param.put("isSoft", isSoft);
		param.put("languageCode", languageCode);
		
		return HttpUtil.postRequestContent("rest.shoppingCart.addShoppingCartIsRent", param);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ShopingCartsProListVO> queryShoppingCartByCustomerCode(String customerCode,String languageCode) {
		
//		List<ShoppingCartVO> list = (List<ShoppingCartVO>)HttpUtil.getReqeuestContentList("rest.shoppingCart.queryShoppingCartByCustomerCode", 
//				"customerCode="+customerCode+"&languageCode="+languageCode, ShoppingCartVO.class);
//		return toCartsProListVOs(list);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setRootClass(ShopingCartsProListVO.class);
		Map<String , Class> clazzMap = new HashMap<String, Class>();
		clazzMap.put("minUnitVOs", ShopingCartsMinUnitVO.class);
		jsonConfig.setClassMap(clazzMap);
		return HttpUtil.getReqeuestContentList("rest.shoppingCart.queryShoppingCartByCustomerCode", 
				"customerCode="+customerCode+"&languageCode="+languageCode, jsonConfig);
	}

	/**
	 * 将查询出来的数据构造成购物车数据
	 * @param shoList
	 * @param 
	 * @return
	 */
	private List<ShopingCartsProListVO> toCartsProListVOs(List<ShoppingCartVO> shoList){
		List<ShopingCartsProListVO> list=new ArrayList<ShopingCartsProListVO>();
		a:
		for (int i = 0; i < shoList.size(); i++) {
			ShoppingCartVO vo=shoList.get(i);
			
			for (int j = 0; j < list.size(); j++) {
				ShopingCartsProListVO vListVO=list.get(j);
				if(vo.getProSerial().equals(vListVO.getProSerial())){
					continue a; 
				}
				
			}
			ShopingCartsProListVO proListVO=new ShopingCartsProListVO();
			proListVO.setId(vo.getId());
			proListVO.setProCode(vo.getProCode());
			proListVO.setProName(vo.getProName());
			proListVO.setProSerial(vo.getProSerial());
			proListVO.setPicPath(vo.getPicPath());
			for (int k = 0; k < shoList.size(); k++) {
				ShoppingCartVO vo2=shoList.get(k);
				
				ShopingCartsMinUnitVO minUnitVO=new ShopingCartsMinUnitVO();
				minUnitVO.setAreaCfgCode(vo2.getAreaCfgCode());
				minUnitVO.setCostPrice(vo2.getCostPrice());
				minUnitVO.setCustomerCode(vo2.getCustomerCode());
				minUnitVO.setDiscountPrice(vo2.getDiscountPrice());
				minUnitVO.setMinSaleUnitCode(vo2.getMinSaleUnitCode());
				minUnitVO.setName(vo2.getName());
				minUnitVO.setSparePrice(vo2.getSparePrice());
				minUnitVO.setType(vo2.getType());
				minUnitVO.setYear(vo2.getYear());
				minUnitVO.setIsSoft(vo2.getIsSoft());
				minUnitVO.setValidDate(vo2.getValidDate());
				if(vo.getProSerial().equals(vo2.getProSerial())){
					proListVO.getMinUnitVOs().add(minUnitVO);
					
				}   
			}
			list.add(proListVO);
		}
		return list;
	}
	
//	@Override
//	public boolean softAddShoppingCarts(List<ShoppingCart> infoVos) {
//		for (int i = 0; i < infoVos.size(); i++) {
//			ShoppingCart shoppingCart=infoVos.get(i);
//			StringBuffer sql=new StringBuffer();
//			sql.append("select * from DT_ShoppingCart " +
//					"where customerCode='"+shoppingCart.getCustomerCode()+"'"+ 
//					" and minSaleUnitCode='"+shoppingCart.getMinSaleUnitCode()+"'"+
//					" and proCode='"+shoppingCart.getProCode()+"'"+
//					" and proSerial='"+shoppingCart.getProSerial()+"'"+
//					" and areaCfgCode=");
//			sql.append("'");
//			sql.append(shoppingCart.getAreaCfgCode());
//			sql.append("'");
//		}
//		return true;
//	}

	@Override
	public int queryShoppingCartsSoftNumber(String customerCode) {
		String res = HttpUtil.getReqeuestContent("rest.shoppingCart.queryShoppingCartsSoftNumber", 
				"customerCode="+customerCode);
		int count = 0;
		try{
			count = Integer.valueOf(res);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public boolean updateYear(String userCode, String serialNo,
			String minSaleUnitCode, String year) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("customerCode", userCode);
		map.put("serialNo", serialNo);
		map.put("minSaleUnitCode", minSaleUnitCode);
		map.put("year", year);
		String result = HttpUtil.postRequestContent("rest.shoppingCart.updateYear", map);
		if("success".equals(result)){
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ShoppingCart> querShoppingCarts(String customerCode) {
		String sql="select * from dt_shoppingcart where customerCode=?";
//		return this.daoSupport.queryForList(sql, ShoppingCart.class, customerCode);
		return null;
	}

	@Override
	public boolean delShoppingCart(String usercode, String proSerial,
			String minSaleUnitCode) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("customerCode", usercode);
		map.put("serialNo", proSerial);
		map.put("minSaleUnitCode", minSaleUnitCode);
		String result = HttpUtil.postRequestContent("rest.shoppingCart.delShoppingCart", map);
		if("success".equals(result)){
			return true;
		}
		return false;
	}

}
