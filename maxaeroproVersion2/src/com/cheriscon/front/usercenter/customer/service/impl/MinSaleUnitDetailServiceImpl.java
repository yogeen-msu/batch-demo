package com.cheriscon.front.usercenter.customer.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JsonConfig;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.model.LanguagePack;
//import com.cheriscon.common.model.MarkerPromotionBaseInfo;
//import com.cheriscon.common.model.MarkerPromotionDiscountRule;
//import com.cheriscon.common.model.MarkerPromotionRule;
//import com.cheriscon.common.model.MinSaleUnitMemo;
//import com.cheriscon.common.model.MinSaleUnitSoftwareDetail;
//import com.cheriscon.common.model.SoftwareType;
//import com.cheriscon.common.model.SoftwareVersion;
//import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.usercenter.customer.model.mapper.MinSaleUnitMemoPriceMapper;
//import com.cheriscon.front.usercenter.customer.model.mapper.MinSaleUnitSoftwareDetailMapper;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
//import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitMemoPriceVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;

/**
 * 最小销售单位功能业务逻辑实现类
 * 
 * @author caozhiyong
 * @version 2013-1-12
 */
@SuppressWarnings("rawtypes")
@Service
public class MinSaleUnitDetailServiceImpl implements
		IMinSaleUnitDetailService {

	@Override
	public String findMinSaleUnitMemo(String minSaleUnitCode,String languageCode) {
		String memo = "null";

		StringBuffer numSql = new StringBuffer();
		numSql.append("select * from DT_MinSaleUnitSoftwareDetail where minSaleUnitCode=");
		numSql.append("'");
		numSql.append(minSaleUnitCode);
		numSql.append("'");
		
//		@SuppressWarnings("unchecked")
		// 查询最小销售单位里面包含的软件类型
//		List<MinSaleUnitSoftwareDetail> softwareDetails = (List<MinSaleUnitSoftwareDetail>) this.daoSupport
//				.queryForList(numSql.toString(),
//						new MinSaleUnitSoftwareDetailMapper(), new Object[]{});
		// 判断最小销售单位是否只包含一个软件版本
//		if (softwareDetails.size() > 1) {
//			// 查询最小销售单位的说明
//			StringBuffer sql = new StringBuffer();
//			sql.append("select * from DT_MinSaleUnitMemo where minSaleUnitCode=");
//			sql.append("'");
//			sql.append(minSaleUnitCode);
//			sql.append("'");
//			sql.append("and languageCode=");
//			sql.append("'");
//			sql.append(languageCode);
//			sql.append("'");
//			MinSaleUnitMemo minSaleUnitMemo = (MinSaleUnitMemo) this.daoSupport
//					.queryForObject(sql.toString(), MinSaleUnitMemo.class,
//							new Object[]{});
//			if (minSaleUnitMemo.getMemo()!= null) {
//				memo = minSaleUnitMemo.getMemo().toString();
//			}
//		} else {
			// 查询最新发布的软件版本的语言包说明
//			StringBuffer sql = new StringBuffer();
//		
//			sql.append("select l.memo from DT_MinSaleUnitSoftwareDetail m" +
//					" left join DT_SoftwareVersion s on " +
//					"s.softwareTypeCode=m.softwareTypeCode " +
//					"left join DT_LanguagePack l on s.code=l.softwareVersionCode " +
//					"where m.minSaleUnitCode=");
//			sql.append("'");
//			sql.append(minSaleUnitCode);
//			sql.append("'");
//			sql.append("and s.releaseState=0 order by s.releaseDate asc,l.memo desc limit 0,1");
//			String softMemo = this.daoSupport.queryForString(sql.toString());
//			if (softMemo != null) {
//				memo = softMemo;
//			}
//		}
//		return memo;
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public MarketPromotionDiscountInfoVO queryDiscount(CustomerInfo customerInfo,String minSaleUnitCode, String areaCfgCode,
			Integer type,String date) {
		
		return (MarketPromotionDiscountInfoVO)HttpUtil.getReqeuestContentObject("rest.minSaleUnitDetail.queryDiscount",
				"minSaleUnitCode="+minSaleUnitCode+"&areaCfgCode="+areaCfgCode+"&validDate="+date, MarketPromotionDiscountInfoVO.class);
		
	}

	/**
	 * 根据活动基本信息查询对应的活动折扣
	 * @param type
	 * @param date
	 * @param baseInfos
	 * @return
	 */
//	private MarketPromotionDiscountInfoVO toQueryDiscountByPromotionCode(Integer type, String date,
//			List<MarkerPromotionBaseInfo> baseInfos) {
//		Map<String, Integer> map=compareDate(date);
//		
//		for (int i = 0; i < baseInfos.size(); i++) {
//			MarkerPromotionBaseInfo baseInfo=baseInfos.get(i);
//			MarketPromotionDiscountInfoVO info=new MarketPromotionDiscountInfoVO();
//			
//			if (baseInfo.getPromotionType()==FrontConstant.MARKET_PROMOTION_TYPE_IS_CHEAP) {//活动类型为越早越便宜
//				if (type==1) {
//					if(map.get("after")>0){//过期软件不参与此活动
//						return null;
//					}
//					int num=map.get("before");//获取过期前相差月份
//					if (num>0) {
//						return queryDiscountRule(num, baseInfo, info);
//					}
//				}else {//购买不可参与此活动
//					 return null;
//				}
//				
//			}else if (baseInfo.getPromotionType()==FrontConstant.MARKET_PROMOTION_TYPE_IS_CONFINE) {//限时购买
//				if (type==1) {
//					if(map.get("after")>0){//过期不参与此活动
//						return null;
//					}
//				}
//				
//				//根据活动code查询折扣信息
//				StringBuffer sql=new StringBuffer();
//				sql.append("select * from DT_MarketPromotionRule where marketPromotionCode=");
//				sql.append("'");
//				sql.append(baseInfo.getCode());
//				sql.append("'");
//				sql.append(" and conditionType=");
//				sql.append("'");
//				sql.append(type);
//				sql.append("'");
//				
//				MarkerPromotionRule rule=(MarkerPromotionRule)this.daoSupport.queryForObject(sql.toString(), MarkerPromotionRule.class, new Object[]{});
//				
//				if (rule==null) {
//					return null;
//				}
//				info.setDiscount(rule.getDiscount());
//				info.setPromotionCode(baseInfo.getCode());
//				info.setPromotionName(baseInfo.getPromotionName());
//				info.setPromotionType(baseInfo.getPromotionType());
//				
//				return info;
//				
//			}else if(baseInfo.getPromotionType()==FrontConstant.MARKET_PROMOTION_TYPE_IS_SETMEAL){//套餐
//				if (type==1) {
//					if(map.get("after")>0){//过期不参与此活动
//						return null;
//					}
//				}
//				
//			}else if(baseInfo.getPromotionType()==FrontConstant.MARKET_PROMOTION_TYPE_IS_PAST){//过期续租
//				if (type==1) {
//					if (map.get("after")>0) {//没过期不参与此活动
//						return queryDiscountRule(map.get("after"), baseInfo, info);
//					}
//				}
//			}
//		}
//		return null;
//	}

	/**
	 * 查询规则条件
	 * @param month
	 * @param baseInfo
	 * @param info
	 * @return
	 */
//	private MarketPromotionDiscountInfoVO queryDiscountRule(int month, MarkerPromotionBaseInfo baseInfo,
//			MarketPromotionDiscountInfoVO info) {
//		
//		StringBuffer sql=new StringBuffer();
//		sql.append("select * from  DT_MarketPromotionDiscountRule dr where exists");
//		sql.append("(select code from DT_MarketPromotionRule r where marketPromotionCode=");
//		sql.append("'");
//		sql.append(baseInfo.getCode());
//		sql.append("'");
//		sql.append(" and dr.marketPromotionRuleCode=r.code )");
//		sql.append(" and ");
//		sql.append("'");
//		sql.append(month);
//		sql.append("'");
//		sql.append(" between betweenMonth and endMonth");
//		
//		MarkerPromotionDiscountRule discountRule=(MarkerPromotionDiscountRule)this.daoSupport.queryForObject(sql.toString(), MarkerPromotionDiscountRule.class, new Object[]{});
//		if (discountRule==null) {
//			return null;
//		}
//		info.setDiscount(discountRule.getDiscount());
//		info.setPromotionCode(baseInfo.getCode());
//		info.setPromotionName(baseInfo.getPromotionName());
//		info.setPromotionType(baseInfo.getPromotionType());
//		
//		return info;
//	}

	/**
	 * 时间比较得出相差月份
	 * @param date1
	 * @return
	 */
	public static Map<String, Integer> compareDate(String date1) {
		
		Map<String, Integer> map=new HashMap<String, Integer>();
		
		int  before= 0;//过期前月份
		int  after= 0;//过期后月份
		
		String formatStyle = "yyyy/MM/dd";
		
		if(date1!=null){
			date1=date1.replace("-", "/");
		}
		
		Calendar c = Calendar.getInstance();   
		Date date = c.getTime();   
		SimpleDateFormat simple = new SimpleDateFormat("yyyy/MM/dd");   

		DateFormat df = new SimpleDateFormat(formatStyle);
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		
		Calendar c3 = Calendar.getInstance();
		Calendar c4 = Calendar.getInstance();
		try {
			if(date1!=null){
			c1.setTime(df.parse(date1));
			c2.setTime(df.parse(simple.format(date)));
			
			c3.setTime(df.parse(date1));
			c4.setTime(df.parse(simple.format(date)));
			}
		} catch (Exception e3) {
			System.out.println(e3.getMessage());
			System.out.println("wrong occured");
		}
		while (!c2.after(c1)) { // 循环对比，直到相等，n 就是所要的结果
			before++;
			c2.add(Calendar.MONTH, 1); // 比较月份，月份+1
		}
		while (!c3.after(c4)) { // 循环对比，直到相等，n 就是所要的结果
			after++;
			c3.add(Calendar.MONTH, 1); // 比较月份，月份+1
		}
		
		map.put("before", before);
		map.put("after", after);
		return map;
	}

	@Override
	public MarketPromotionDiscountInfoVO querySaleCfgDiscount(
			CustomerInfo customerInfo, String saleCfgCode,
			String areaCfgCode, Integer type, String date) {
		
		return (MarketPromotionDiscountInfoVO) HttpUtil.getReqeuestContentObject("rest.minSaleUnitDetail.querySaleCfgDiscount",
				"saleCfgCode="+saleCfgCode+"&areaCfgCode="+areaCfgCode+"&type="+type+"&validDate="+date, MarketPromotionDiscountInfoVO.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitSoftwareVO> querySoftWareInfo(String languageCode,String saleCfgCode,int type) {
		return (List<MinSaleUnitSoftwareVO>)HttpUtil.getReqeuestContentList("rest.minSaleUnitDetail.querySoftWareInfo", 
				"languageCode="+languageCode+"&saleCfgCode="+saleCfgCode+"&type="+type, MinSaleUnitSoftwareVO.class);
	}

	@Override
	public LanguagePack querySoftWareVersionDetail(
			String softwareVersionCode,CustomerInfo customerInfo) {
		return (LanguagePack) HttpUtil.getReqeuestContentObject("rest.minSaleUnitDetail.querySoftWareVersionDetail",
				"softwareVersionCode="+softwareVersionCode+"&languageCode="+customerInfo.getLanguageCode(), LanguagePack.class);
	}

	@Override
	public CfgSoftRentInfoVO findSaleConfigDetailInfo(String saleCfgCode,
			String langugeCode,String serialNo) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setRootClass(CfgSoftRentInfoVO.class);
		Map<String, Class> classmap = new HashMap<String, Class>();
		classmap.put("minSaleUnitsMemo", MinSaleUnitMemo.class);
		jsonConfig.setClassMap(classmap);
		return (CfgSoftRentInfoVO)HttpUtil.getReqeuestContentObject("rest.minSaleUnitDetail.findSaleConfigDetailInfo",
				"saleCfgCode="+saleCfgCode+"&languageCode="+langugeCode+"&serialNo="+serialNo, jsonConfig);
	}

	@Override
	public MinSaleUnitDetailVO findMinSaleUnitDetailInfo(
			String minSaleUnitCode, String langugeCode,String serialNo) {
		
		return (MinSaleUnitDetailVO)HttpUtil.getReqeuestContentObject("rest.minSaleUnitDetail.findMinSaleUnitDetailInfo",
				"minSaleUnitCode="+minSaleUnitCode+"&languageCode="+langugeCode+"&serialNo="+serialNo, MinSaleUnitDetailVO.class);
	}

	
	 public List<MinSaleUnitSoftwareVO> querySoftWareInfoByContract(String languageCode,String contractCode,int type){
		 
			return (List<MinSaleUnitSoftwareVO>)HttpUtil.getReqeuestContentList("rest.minSaleUnitDetail.querySoftWareInfoByContract", 
					"languageCode="+languageCode+"&contractCode="+contractCode+"&type="+type, MinSaleUnitSoftwareVO.class);
	 }
	
}
