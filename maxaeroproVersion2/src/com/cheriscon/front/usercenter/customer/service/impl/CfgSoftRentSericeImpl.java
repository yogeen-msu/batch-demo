package com.cheriscon.front.usercenter.customer.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JsonConfig;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.utils.HttpUtil;
//import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
//import com.cheriscon.front.usercenter.customer.model.mapper.MinSaleUnitMemoPriceMapper;
import com.cheriscon.front.usercenter.customer.service.ICfgSoftRentService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
//import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitMemoPriceVO;

/**
 * 产品出厂配置软件接口实现类
 * 
 * @author caozhiyong
 * @version 2013-1-10
 */
@Service
public class CfgSoftRentSericeImpl
		implements ICfgSoftRentService {

	@Override
	public CfgSoftRentInfoVO querySoftRent(String saleContractCode,
			String proCode,CustomerInfo customerInfo) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setRootClass(CfgSoftRentInfoVO.class);
//		minSaleUnitsMemo
		Map<String, Class> classmap = new HashMap<String, Class>();
		classmap.put("minSaleUnitsMemo", MinSaleUnitMemo.class);
		return (CfgSoftRentInfoVO)HttpUtil.getReqeuestContentObject("rest.cfgSoftRend.querySoftRent",
				"saleContractCode="+saleContractCode+"&proCode="+proCode+"&languageCode="+customerInfo.getLanguageCode(), jsonConfig);
	}	
}
