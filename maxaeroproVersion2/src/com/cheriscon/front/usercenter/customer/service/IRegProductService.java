package com.cheriscon.front.usercenter.customer.service;

import java.util.List;

import com.cheriscon.common.model.CustomerProInfo;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.front.usercenter.customer.vo.RegProductAccountInfoSecVo;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;
//import com.cheriscon.framework.database.Page;

/**
 * 注册产品业务逻辑实现接口
 * @author caozhiyong
 * @version 2013-1-8
 */
public interface IRegProductService {

	/**
	 * 查询产品信息
	 * @param regProductInfoVO
	 * @return
	 */
	public List<ProductInfo> queryProductInfo(RegProductAcountInfoVO regProductInfoVO);
	
//	/**
//	 * 注册产品信息
//	 * @param regProductInfoVO
//	 * @return
//	 */
//	public boolean regProductInfo(RegProductAcountInfoVO regProductInfoVO);
	
	
	/**
	 *  注册成功后把产品对应的标准出厂软件配置（最小销售单位）写到产品软件效状态表中
	 * @param usercode
	 * @param regProductInfoVO
	 */
	public boolean addProductSoftwareValidStatus(String usercode,RegProductAcountInfoVO regProductInfoVO);
	
	
	public String addProductSoftwareValidStatusSec(RegProductAccountInfoSecVo regProductAccountInfoSecVo);
//	
//	
//	/**
//	 * 根据产品序列号和autelID判断是否对应
//	 *  @param autelId
//	 *  @param proSerialNo
//	 */
//	public int checkProduct(String autelId,String proSerialNo);
	
	/**
	 * 根据产品序列号和autelID判断是否对应
	 *  @param autelId
	 *  @param proSerialNo
	 */
//	public ProductInfo queryProduct(String proSerialNo);
	
	//根据产品序列号获取产品信息和经销商20140213
	public ProductInfo queryProductAndSealer(String proSerialNo);
	
	public ProductInfo queryProductInfo(String proSerialNo);
//	
//	/**
//	 * 
//	 * 
//	 *  @param code
//	 */
//	public ProductInfo queryProductByCode(String code);
	
	/**
	 * 移除注册产品信息
	 * @param regProductInfoVO
	 * @return
	 */
	public String removeProductInfo(String proCode);
	
	
	/**
	 * 查询产品是否已经关联用户
	 * @param proCode
	 * @return
	 */
	public CustomerProInfo CustomerProductNum(String proCode);
	
	/**
	 * 插入用户与产品关联信息表
	 * @param regProductInfoVO
	 * @return
	 */
	public boolean insertCustProInfo(CustomerProInfo custProInfo);
	
	
//	public boolean changeCustProInfo(String userCode,RegProductAcountInfoVO regProductInfoVO);
//	
//	public boolean addProductSoftwareValidStatus(String usercode,String validDate,String regDate,
//			RegProductAcountInfoVO regProductInfoVO);
//	
//	public boolean addProductSoftwareValidStatus(String usercode,String validDate,String regDate,
//			String proDate,RegProductAcountInfoVO regProductInfoVO);
	
	/**
	 * 改变产品注册用户
	 */
	public String changeProduct(String proRegPwd,String proSerialNo,String proTypeCode,String customerCode);
	/**
	 * 注册产品
	 * @param proRegPwd
	 * @param proSerialNo
	 * @param proTypeCode
	 * @param customerCode
	 * @return
	 */
	public String regProduct(String proRegPwd,String proSerialNo,String proTypeCode,String customerCode);
	
	/**
	 * 为国内提供的特殊接口
	 * 通过 序列号 注册密码 手机号，生成一条注册用户 
	 * 并绑定产品序列号
	 * @param serialNo
	 * @param regPwd
	 * @param mobilePhone
	 * @return
	 */
	public String regProductForCN(String serialNo,String regPwd,String mobilePhone);
	
}
