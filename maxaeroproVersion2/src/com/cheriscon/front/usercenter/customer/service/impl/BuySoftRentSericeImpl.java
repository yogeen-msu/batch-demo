package com.cheriscon.front.usercenter.customer.service.impl;


//import java.text.SimpleDateFormat;
//import java.util.Calendar;

import org.springframework.stereotype.Service;

//import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.framework.database.Page;
//import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.usercenter.customer.model.mapper.MinSaleUnitDetailMapper;
import com.cheriscon.front.usercenter.customer.service.IBuySoftRentService;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;

/**
 * 自购软件接口实现类
 * 
 * @author caozhiyong
 * @version 2013-1-10
 */
@Service
public class BuySoftRentSericeImpl implements IBuySoftRentService {

	@Override
	public Page queryBuySoftRent(String saleContractCode,
			String proCode,int pageNo, int pageSize,String languageCode) {
		return HttpUtil.getReqeuestContentPage("rest.cfgSoftRend.queryBuySoftRent", 
				"saleContractCode="+saleContractCode+"&proCode="+proCode+"&languageCode="+languageCode+"&pageNo="+pageNo+"&pageSize="+pageSize,
				MinSaleUnitDetailVO.class);
	}

//	@Override
//	public int querySoftIsExpiring(String customerCode) {
//		Calendar calendar=Calendar.getInstance();
//		String time=new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
//		StringBuffer s=new StringBuffer();
//		s.append("select count(1) from dt_productsoftwarevalidStatus s ");
//		s.append("left join dt_customerProInfo p on s.proCode=p.proCode left join dt_productinfo pi on pi.code=p.proCode");
//		s.append(" where s.validDate<");
//		s.append("'");
//		s.append(time);
//		s.append("'");
//		s.append(" and  p.customerCode=");
//		s.append("'");
//		s.append(customerCode);
//		s.append("'");
//		s.append(" and  pi.regStatus!=");
//		s.append("'");
//		s.append(FrontConstant.PRODUCT_REGSTATUS_UNBINDED);
//		s.append("'");
//		
//		int num=this.daoSupport.queryForInt(s.toString(), new Object[]{});
//		return num;
//	}

}
