package com.cheriscon.front.usercenter.customer.service.impl;

//import java.text.SimpleDateFormat;
//import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.CustomerProInfo;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.vo.RegProductAccountInfoSecVo;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
//import org.jsoup.helper.StringUtil;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.cheriscon.common.constant.DBConstant;
//import com.cheriscon.common.model.CustomerInfo;
//import com.cheriscon.common.model.ProductAssociatesDate;
//import com.cheriscon.common.model.ProductChangeNoLog;
//import com.cheriscon.common.model.ProductRemoveLog;
//import com.cheriscon.common.model.ProductSoftwareValidStatus;
//import com.cheriscon.common.model.SealerInfo;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.common.utils.SessionUtil;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.cop.sdk.utils.DateUtil;
//import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.usercenter.customer.model.mapper.RegProductInfoMapper;
//import com.cheriscon.front.usercenter.customer.service.IProductDateService;
//import com.cheriscon.front.usercenter.customer.vo.ProductSoftwareValidStatusUpMonthVO;
//import com.cheriscon.front.usercenter.customer.vo.ProductSoftwareValidStatusVO;
//import com.cheriscon.front.usercenter.customer.vo.RegProductDetailInfoVO;

/**
 * 注册产品信息业务实现类
 * 
 * @author caozhiyong
 * @version 2013-1-8
 */
@Service
public class RegProductServiceImpl implements IRegProductService {
//
//	@Resource
//	private IProductDateService productDateService;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductInfo> queryProductInfo(RegProductAcountInfoVO regProductInfoVO) {
		StringBuffer sql = new StringBuffer();

		sql.append("regProductInfoVO.proTypeCode=");// 产品类型code
		sql.append(regProductInfoVO.getProTypeCode());

		sql.append("&regProductInfoVO.proSerialNo=");// 产品序列号
		sql.append(regProductInfoVO.getProSerialNo());

		sql.append("&regProductInfoVO.proRegPwd=");// 产品注册密码
		sql.append(regProductInfoVO.getProRegPwd());
		
		return (List<ProductInfo>)HttpUtil.getReqeuestContentList("rest.regProduct.query", sql.toString(), ProductInfo.class);
	}

//	@Override
//	public boolean regProductInfo(RegProductAcountInfoVO regProductInfoVO) {
//		StringBuffer sql = new StringBuffer();
//		sql.append("update DT_ProductInfo set regStatus=1,regTime=?");
//		sql.append(" where regPwd=");// 产品注册密码
//		sql.append("'");
//		sql.append(regProductInfoVO.getProRegPwd());
//		sql.append("'");
//
//		sql.append(" and proTypeCode =");// 产品类型code
//		sql.append("'");
//		sql.append(regProductInfoVO.getProTypeCode());
//		sql.append("'");
//
//		sql.append(" and serialNo =");// 产品序列号
//		sql.append("'");
//		sql.append(regProductInfoVO.getProSerialNo());
//		sql.append("'");
//
//		try {
//			this.daoSupport.execute(sql.toString(),
//					DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			return false;
//		}
//		return true;
//	}
//
//	public boolean addProductSoftwareValidStatus(String usercode,String validDate,String regDate,
//			RegProductAcountInfoVO regProductInfoVO){
//		try {
//			
//			/**
//			 * 修改产品注册状态以及添加产品注册时间
//			 */
//			if(StringUtil.isBlank(regDate)){
//				regDate=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
//			}
//			
//			StringBuffer sql = new StringBuffer();
//			sql.append("update DT_ProductInfo set regStatus=1,regTime='"+regDate+"'");
//			sql.append(" where regPwd=");// 产品注册密码
//			sql.append("'");
//			sql.append(regProductInfoVO.getProRegPwd());
//			sql.append("'");
//
//			sql.append(" and proTypeCode =");// 产品类型code
//			sql.append("'");
//			sql.append(regProductInfoVO.getProTypeCode());
//			sql.append("'");
//
//			sql.append(" and serialNo =");// 产品序列号
//			sql.append("'");
//			sql.append(regProductInfoVO.getProSerialNo());
//			sql.append("'");
//
//			
//			this.daoSupport.execute(sql.toString(),null);
//			
//			// 查询标准出厂配置对应的最小销售单位
//			StringBuffer querySql = new StringBuffer();
//			querySql.append("select DT_ProductInfo.code as proCode,DT_ProductInfo.regTime as validDate,DT_MinSaleUnitSaleCfgDetail.minSaleUnitCode" +
//					",DT_SaleContract.upMonth from"
//					+ " (DT_MinSaleUnitSaleCfgDetail left join DT_SaleContract on "
//					+ "DT_MinSaleUnitSaleCfgDetail.saleCfgCode =DT_SaleContract.saleCfgCode)"
//					+ "left join DT_ProductInfo on DT_SaleContract.code=DT_ProductInfo.saleContractCode "
//					+ "where 1=1");
//			querySql.append(" and DT_ProductInfo.serialNo=?");// 序列号
//
//			List<ProductSoftwareValidStatusUpMonthVO> proSoftwareValidStatusUpMonthVOs = this.daoSupport
//					.queryForList(querySql.toString(),
//							ProductSoftwareValidStatusUpMonthVO.class, regProductInfoVO.getProSerialNo());
//
//			// 用户产品关联类
//			CustomerProInfo customerProInfo = new CustomerProInfo();
//			customerProInfo.setCustomerCode(usercode);
//			customerProInfo.setProCode(proSoftwareValidStatusUpMonthVOs.get(0)
//					.getProCode());
//			
//
//			// 添加用户产品关联信息数据
//			this.daoSupport.insert(DBUtils.getTableName(CustomerProInfo.class),
//					customerProInfo);
//			
//			
//			
//			for (int i = 0, j = proSoftwareValidStatusUpMonthVOs.size(); i < j; i++) {
//				ProductSoftwareValidStatusVO productSoftwareValidStatusVO = new ProductSoftwareValidStatusVO();
//
//				productSoftwareValidStatusVO.setMinSaleUnitCode(proSoftwareValidStatusUpMonthVOs.get(i).getMinSaleUnitCode());
//				productSoftwareValidStatusVO.setProCode(proSoftwareValidStatusUpMonthVOs.get(i).getProCode());
//				productSoftwareValidStatusVO.setValidDate(validDate);
//				productSoftwareValidStatusVO.setCode(DBUtils
//						.generateCode(DBConstant.PRO_SOFTWARE_VALID_STATUS));
//				// 把产品对应的标准出厂软件配置（最小销售单位）写到产品软件效状态表中
//				this.daoSupport.insert(
//						DBUtils.getTableName(ProductSoftwareValidStatus.class),
//						productSoftwareValidStatusVO);
//			}
//
//			
//				
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			return false;
//		}
//
//		return true;
//	}
//	public boolean addProductSoftwareValidStatus(String usercode,String validDate,String regDate,
//			String proDate,RegProductAcountInfoVO regProductInfoVO){
//		try {
//			
//			/**
//			 * 修改产品注册状态以及添加产品注册时间
//			 */
//			if(StringUtil.isBlank(regDate)){
//				regDate=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
//			}
//			
//			StringBuffer sql = new StringBuffer();
//			sql.append("update DT_ProductInfo set regStatus=1,regTime='"+regDate+"',proDate='"+proDate+"'");
//			sql.append(" where regPwd=");// 产品注册密码
//			sql.append("'");
//			sql.append(regProductInfoVO.getProRegPwd());
//			sql.append("'");
//
//			sql.append(" and proTypeCode =");// 产品类型code
//			sql.append("'");
//			sql.append(regProductInfoVO.getProTypeCode());
//			sql.append("'");
//
//			sql.append(" and serialNo =");// 产品序列号
//			sql.append("'");
//			sql.append(regProductInfoVO.getProSerialNo());
//			sql.append("'");
//
//			
//			this.daoSupport.execute(sql.toString(),null);
//			
//			// 查询标准出厂配置对应的最小销售单位
//			StringBuffer querySql = new StringBuffer();
//			querySql.append("select DT_ProductInfo.code as proCode,DT_ProductInfo.regTime as validDate,DT_MinSaleUnitSaleCfgDetail.minSaleUnitCode" +
//					",DT_SaleContract.upMonth from"
//					+ " (DT_MinSaleUnitSaleCfgDetail left join DT_SaleContract on "
//					+ "DT_MinSaleUnitSaleCfgDetail.saleCfgCode =DT_SaleContract.saleCfgCode)"
//					+ "left join DT_ProductInfo on DT_SaleContract.code=DT_ProductInfo.saleContractCode "
//					+ "where 1=1");
//			querySql.append(" and DT_ProductInfo.serialNo=?");// 序列号
//
//			List<ProductSoftwareValidStatusUpMonthVO> proSoftwareValidStatusUpMonthVOs = this.daoSupport
//					.queryForList(querySql.toString(),
//							ProductSoftwareValidStatusUpMonthVO.class, regProductInfoVO.getProSerialNo());
//
//			// 用户产品关联类
//			CustomerProInfo customerProInfo = new CustomerProInfo();
//			customerProInfo.setCustomerCode(usercode);
//			customerProInfo.setProCode(proSoftwareValidStatusUpMonthVOs.get(0)
//					.getProCode());
//			
//
//			// 添加用户产品关联信息数据
//			this.daoSupport.insert(DBUtils.getTableName(CustomerProInfo.class),
//					customerProInfo);
//			
//			
//			
//			for (int i = 0, j = proSoftwareValidStatusUpMonthVOs.size(); i < j; i++) {
//				ProductSoftwareValidStatusVO productSoftwareValidStatusVO = new ProductSoftwareValidStatusVO();
//
//				productSoftwareValidStatusVO.setMinSaleUnitCode(proSoftwareValidStatusUpMonthVOs.get(i).getMinSaleUnitCode());
//				productSoftwareValidStatusVO.setProCode(proSoftwareValidStatusUpMonthVOs.get(i).getProCode());
//				productSoftwareValidStatusVO.setValidDate(validDate);
//				productSoftwareValidStatusVO.setCode(DBUtils
//						.generateCode(DBConstant.PRO_SOFTWARE_VALID_STATUS));
//				// 把产品对应的标准出厂软件配置（最小销售单位）写到产品软件效状态表中
//				this.daoSupport.insert(
//						DBUtils.getTableName(ProductSoftwareValidStatus.class),
//						productSoftwareValidStatusVO);
//			}
//
//			
//				
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			return false;
//		}
//
//		return true;
//	}
	
	public boolean addProductSoftwareValidStatus(String usercode,
			RegProductAcountInfoVO regProductInfoVO) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("usercode", usercode); 
		map.put("regProductInfoVO.proRegRwd", regProductInfoVO.getProRegPwd()); // 产品注册密码
		map.put("regProductInfoVO.proTypeCode", regProductInfoVO.getProTypeCode()); // 产品类型code
		map.put("regProductInfoVO.proSerialNo", regProductInfoVO.getProSerialNo()); // 产品序列号
		String result = HttpUtil.postRequestContent("rest.regProduct.addProductSoftwareValidStatus",map);
		if("success".equals(result)){
			return true;
		}else{
			return false;
		}
	}
	
	public String addProductSoftwareValidStatusSec(RegProductAccountInfoSecVo regProductAccountInfoSecVo) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("proDate", regProductAccountInfoSecVo.getProDate());
//		from ProductChangeNoLog
//		log.setCreateUser(createUser);
//		log.setCreateIp(request.getRemoteAddr());
//		log.setCreateDate(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
//		log.setCustomerCode(customerCode);
//		log.setOldProCode(oldProduct.getCode());
//		log.setNewProCode(newProduct.getCode());
//		log.setValidDate(oldProduct.getValidDate()); //有效期
//		log.setRegDate(oldProduct.getRegTime()); //注册日期
		dataMap.put("log.createUser", regProductAccountInfoSecVo.getCreateUser());
		dataMap.put("log.createIp", regProductAccountInfoSecVo.getCreateIp());
		dataMap.put("log.createDate", regProductAccountInfoSecVo.getCreateDate());
		dataMap.put("log.customerCode", regProductAccountInfoSecVo.getCustomerCode());
		dataMap.put("log.oldProCode", regProductAccountInfoSecVo.getOldProCode());
		dataMap.put("log.newProCode", regProductAccountInfoSecVo.getNewProCode());
		dataMap.put("log.validDate", regProductAccountInfoSecVo.getValidDate());
		dataMap.put("log.regDate", regProductAccountInfoSecVo.getRegDate());
		
//		from RegProductAcountInfoVO
//		private String proTypeCode;//产品型号code
//		private String proSerialNo;//产品序列号
//		private String proRegPwd;//产品注册密码
//		private String sealCode;//经销商编码
//		private String proCode;//产品编码
//		private String remark;// 说明
		dataMap.put("regProductAcountInfoVo.proTypeCode", regProductAccountInfoSecVo.getProTypeCode());
		dataMap.put("regProductAcountInfoVo.proSerialNo", regProductAccountInfoSecVo.getProSerialNo());
		dataMap.put("regProductAcountInfoVo.proRegPwd", regProductAccountInfoSecVo.getProRegPwd());
		dataMap.put("regProductAcountInfoVo.sealCode", regProductAccountInfoSecVo.getSealCode());
		dataMap.put("regProductAcountInfoVo.proCode", regProductAccountInfoSecVo.getProCode());
		dataMap.put("regProductAcountInfoVo.remark", regProductAccountInfoSecVo.getRemark());
//		String jsonData = HttpUtil.postRequestContent("rest.sealer.changeRegProduct.addProductSoftwareValidStatus", 
//				"regProductAccountInfoSecVo.", regProductAccountInfoSecVo);
		String jsonData = HttpUtil.postRequestContent("rest.sealer.regProduct.changeRegProduct", dataMap);
		return jsonData.substring(1, jsonData.length() - 1);
	}
	
//
//	
//	public int checkProduct(String autelId,String proSerialNo){
//		
//		StringBuffer sql = new StringBuffer();
//		sql.append("select count(1) from DT_CustomerInfo a,DT_CustomerProInfo b,DT_ProductInfo c");
//		sql.append(" where a.code=b.customerCode and b.proCode=c.code and a.autelId='");
//		sql.append(autelId.trim()+"'");
//		sql.append(" and c.serialNo='");
//		sql.append(proSerialNo.trim()+"'");
//		return this.daoSupport.queryForInt(sql.toString(), new Object[]{});
//	}
//	
//	public ProductInfo queryProduct(String proSerialNo){
//		StringBuffer sql=new StringBuffer();
//		sql.append("select d.* from DT_ProductInfo d where serialNo='"+proSerialNo+"'");
//		return (ProductInfo)this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, new Object[]{});
//	}
//	
//	public ProductInfo queryProductByCode(String code){
//		StringBuffer sql=new StringBuffer();
//		sql.append("select d.* from DT_ProductInfo d where code='"+code+"'");
//		return (ProductInfo)this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, new Object[]{});
//	}
	
	public ProductInfo queryProductAndSealer(String proSerialNo){
//		StringBuffer sql=new StringBuffer();
//		sql.append("select d.*,b.autelId as sealerAutelId ,");
//		sql.append(" (select distinct validdate from DT_ProductSoftwareValidStatus where proCode=d.code) as validDate");
//		sql.append(" from dt_productinfo d,DT_SaleContract s,DT_SealerInfo b");
//		sql.append(" where d.saleContractCode=s.code");
//		sql.append(" and s.sealerCode=b.code");
//		sql.append(" and d.serialNo=?");
//		return (ProductInfo)this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, new Object[]{proSerialNo});
		return (ProductInfo) HttpUtil.getReqeuestContentObject("rest.regProduct.queryProductAndSealer", "proSerialNo="+proSerialNo, ProductInfo.class);
	}
	
	public ProductInfo queryProductInfo(String proSerialNo) {
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.regProduct.queryProductAndSealer", "proSerialNo=" + proSerialNo);
		if (jsonData != null && !"".equals(jsonData)) {
			return (ProductInfo) JsonUtil.getDTO(jsonData, ProductInfo.class);
		}
		return null;
	}
	
	/**
	 * 移除产品注册
	 */
	public String removeProductInfo(String proCode){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		Object obj=SessionUtil.getLoginUserInfo(request);
		String ip=FrontConstant.getIpAddr(request);
		String operateUser="";
		if(obj.getClass().equals(CustomerInfo.class)){
			operateUser=((CustomerInfo)obj).getCode();
		}
		String result = HttpUtil.getReqeuestContent("rest.regProduct.removeProductInfo", 
				"proCode="+proCode+"&usercode="+operateUser+"&ip="+ip);
		return result;
	}
	
	
	public CustomerProInfo CustomerProductNum(String proCode){
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from DT_CustomerProInfo where proCode=?");
//		return (CustomerProInfo)this.daoSupport.queryForObject(sql.toString(), CustomerProInfo.class,new Object[]{proCode});
	return (CustomerProInfo) HttpUtil.getReqeuestContentObject("rest.regProduct.customerPro.get",
			"proCode="+proCode, CustomerProInfo.class);
	}
	
	public boolean insertCustProInfo(CustomerProInfo custProInfo){
//		this.daoSupport.insert(DBUtils.getTableName(CustomerProInfo.class),
//				custProInfo);
//		return true;
		String result = HttpUtil.postRequestContent("rest.regProduct.customerPro.add","custProInfo.", custProInfo);
		if("success".equals(result)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public String changeProduct(String proRegPwd, String proSerialNo,
			String proTypeCode, String customerCode) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("proRegPwd", proRegPwd);
		map.put("proSerialNo", proSerialNo);
		map.put("proTypeCode", proTypeCode);
		map.put("customerCode", customerCode);
		return HttpUtil.postRequestContent("rest.regProduct.changeProduct", map);
	}

	@Override
	public String regProduct(String proRegPwd, String proSerialNo,
			String proTypeCode, String customerCode) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("proRegPwd", proRegPwd);
		map.put("proSerialNo", proSerialNo);
		map.put("proTypeCode", proTypeCode);
		map.put("customerCode", customerCode);
		return HttpUtil.postRequestContent("rest.regProduct.regProduct", map);
	}

	@Override
	public String regProductForCN(String serialNo, String regPwd,
			String mobilePhone) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("proSerialNo", serialNo);
		map.put("proRegPwd", regPwd);
		map.put("mobilePhone", mobilePhone);
		return HttpUtil.postRequestContent("rest.regProduct.regProductForCN", map);
	}
	
//	public boolean changeCustProInfo(String userCode,RegProductAcountInfoVO regProductInfoVO){
//		//1.先删除对应关系
//		String delSql="delete from dt_customerProInfo where proCode='"+regProductInfoVO.getProCode()+"'";
//		this.daoSupport.execute(delSql, null);
//		
//		// 2.用户产品关联类
//		CustomerProInfo customerProInfo = new CustomerProInfo();
//		customerProInfo.setCustomerCode(userCode);
//		customerProInfo.setProCode(regProductInfoVO.getProCode());
//		this.daoSupport.insert("DT_CustomerProInfo", customerProInfo);
//		//更新关联日期
//		ProductAssociatesDate proDate=productDateService.getProductDateInfo(regProductInfoVO.getProCode());
//		if(proDate==null){
//			proDate=new ProductAssociatesDate();
//			proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
//			proDate.setProCode(regProductInfoVO.getProCode());
//			productDateService.insertProductDate(proDate);
//		}else{
//			productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), regProductInfoVO.getProCode());
//		}
//		
//		return true;
//	}
}
