package com.cheriscon.front.usercenter.distributor.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.ToCustomerCharge;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.distributor.service.IChangeLanguageService;

@Service
public class ChangeLanguageServiceImpl implements IChangeLanguageService {

	public Page queryToCustomerChangePage(String sealerCode, String serialNo,
			String proTypeCode, int pageNo, int pageSize) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select a.code as proCode ,a.serialNo as serialNo,f.name as proTypeName ,c.name as languageName,a.regStatus as regStatus,date_format(a.regTime,'%m/%d/%Y') as regDate from ");
//		sql.append(" dt_productinfo a,");
//		sql.append(" dt_salecontract b ,");
//		sql.append(" DT_LanguageConfig c ,");
//		sql.append(" dt_sealerinfo d ,");
//		sql.append(" dt_sealerProductChange e,");
//		sql.append(" DT_ProductForSealer f ");
//		sql.append(" where a.saleContractCode=b.code");
//		sql.append(" and b.languageCfgCode=c.code");
//		sql.append(" and b.sealerCode=d.code");
//		sql.append(" and b.proTypeCode=e.proTypeCode");
//		sql.append(" and b.proTypeCode=f.code");
//		if (!StringUtil.isEmpty(sealerCode)) 
//		{
//			sql.append(" and d.code ='").append(sealerCode).append("'");
//		}
//		if (!StringUtil.isEmpty(serialNo)) 
//		{
//			sql.append(" and a.serialNo like'%").append(serialNo.trim()).append("%'");
//		}
//		if (!StringUtil.isEmpty(proTypeCode) && !proTypeCode.equals("0")) 
//		{
//			sql.append(" and a.proTypeCode ='").append(proTypeCode).append("'");
//		}
//		sql.append(" order by a.proTypeCode asc");
//		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, ToCustomerCharge.class);
		return null;
	}
	
	public Page querySealerProductPage(String sealerCode, String serialNo, String proTypeCode, int pageNo, int pageSize) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sealerCode", sealerCode);
		map.put("serialNo", serialNo);
		map.put("proTypeCode", proTypeCode);
		map.put("page", pageNo);
		map.put("pageSize", pageSize);
		Page page = HttpUtil.postRequestContentPage("rest.sealer.proRegPwd.querySealerProductPage", map, ToCustomerCharge.class);
		return page;
//		StringBuffer sql = new StringBuffer();
//		sql.append("select a.code as proCode ,a.serialNo as serialNo,f.name as proTypeName ,c.name as languageName,a.regStatus as regStatus,date_format(a.regTime,'%m/%d/%Y') as regDate,a.regPwd as regPwd from ");
//		sql.append(" dt_productinfo a,");
//		sql.append(" dt_salecontract b ,");
//		sql.append(" DT_LanguageConfig c ,");
//		sql.append(" dt_sealerinfo d ,");
//		sql.append(" DT_ProductForSealer f ");
//		sql.append(" where a.saleContractCode=b.code");
//		sql.append(" and b.languageCfgCode=c.code");
//		sql.append(" and b.sealerCode=d.code");
//		sql.append(" and b.proTypeCode=f.code");
//		if (!StringUtil.isEmpty(sealerCode)) 
//		{
//			sql.append(" and d.code ='").append(sealerCode).append("'");
//		}
//		if (!StringUtil.isEmpty(serialNo)) 
//		{
//			sql.append(" and a.serialNo like'%").append(serialNo.trim()).append("%'");
//		}
//		if (!StringUtil.isEmpty(proTypeCode) && !proTypeCode.equals("0")) 
//		{
//			sql.append(" and a.proTypeCode ='").append(proTypeCode).append("'");
//		}
//		sql.append(" order by a.proTypeCode asc");
//		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, ToCustomerCharge.class);
	}

}
