package com.cheriscon.front.usercenter.distributor.service;

import java.util.Map;

import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.framework.database.Page;

/**
 * 客诉回复业务逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public interface IReCustomerComplaintInfoService 
{
	/**
	 * 新增客诉回复信息
	 * @param reCustomerComplaintInfo
	 * @throws Exception
	 */
	public void addReCustomerComplaintInfo(ReCustomerComplaintInfo reCustomerComplaintInfo) 
			throws Exception;
	
	
	/**
	 * 更新客诉回复信息
	 * @param reCustomerComplaintInfo
	 * @throws Exception
	 */
	public void updateReCustomerComplaintInfo(ReCustomerComplaintInfo reCustomerComplaintInfo) 
			throws Exception;
	
	/**
	 * 根据客诉code查询该客诉code的所有客诉回复信息记录
	 * @param cusCode
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> queryReCustomerComplaintInfoByCode(String cusCode) 
			throws Exception;

	/**
	  * 客诉回复信息分页显示方法
	  * @param customerComplaintCode
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageReCustomerComplaintInfoPage(String customerComplaintCode, int pageNo, int pageSize) throws Exception;
	
	
	/**
	 * 根据客户回复code查询客诉回复对象信息
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public ReCustomerComplaintInfo getReCustomerComplaintInfoByCode(String code) throws Exception;
}
