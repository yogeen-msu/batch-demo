package com.cheriscon.front.usercenter.distributor.service;

import com.cheriscon.framework.database.Page;

/**
 * 我的账户业务逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public interface IMyDistributorAccountService 
{
	/**
	 * 根据经销商code查询经销商账户信息（包含可卖产品名称、产品类型、销售区域）
	 * @param saleCode 经销商code
	 * @param pageNo 页
	 * @param pageSize 大小
	 * @return
	 */
	public Page queryMyDistributorAccountPage(String saleCode, int pageNo, int pageSize)throws Exception;
}
