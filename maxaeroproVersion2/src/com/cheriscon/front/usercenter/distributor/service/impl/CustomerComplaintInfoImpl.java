//package com.cheriscon.front.usercenter.distributor.service.impl;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.Calendar;
//import java.util.List;
//
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.cheriscon.common.constant.DBConstant;
//import com.cheriscon.common.model.CustomerComplaintInfo;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.framework.util.DateUtil;
//import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
//
///**
// * 客诉信息(1:个人用户 2：经销商用户)业务逻辑实现类
// * @author yangpinggui
// * @version 创建时间：2013-1-5
// */
//@Service
//public class CustomerComplaintInfoImpl extends BaseSupport<CustomerComplaintInfo> 
//		implements ICustomerComplaintInfoService
//{
//
//	
//	public  Page queryCustomerComplaintInfoPage2(String authCode, int userType,int timeRange,int complaintState,String userCode, int pageNo, int pageSize) throws Exception{
//StringBuffer querySql = new StringBuffer();
//		
//		querySql.append("select CASE WHEN a.complaintState=4 then  HOUR(TIMEDIFF(a.closeDate,a.complaintDate)) else HOUR(TIMEDIFF(NOW(),a.complaintDate)) end as hoursBetween,a.*  from DT_CustomerComplaintInfo a left join DT_SealerDataAuthDeails b");
//		querySql.append(" on  b.SealerCode=a.acceptor ");
//		querySql.append(" where 1=1 ");
//		
//		Calendar calendar = Calendar.getInstance();
//		if(userType==1){
//		calendar.add(Calendar.MONTH, -1); //一般顾客得到一个月的客诉
//		}else{
//		calendar.add(Calendar.DATE, -7);; //经销商查看7天
//		}
//		String time = DateUtil.toString(calendar.getTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
//		
//		if(complaintState != FrontConstant.QUERY_ALL)
//		{
//			querySql.append(" and complaintState = ").append(complaintState);
//		}
//		
//		if(timeRange == FrontConstant.LATELY_THREE_MONTH)//查询最近3个月消息
//		{
//			querySql.append(" and complaintDate >= '").append(time).append("'");
//		}
//		else if(timeRange == FrontConstant.BEFORE_THREE_MONTH)//查询3个月前消息
//		{
//			querySql.append(" and complaintDate < '").append(time).append("'");
//		}
//		if(userType==1){
//			querySql.append(" and userCode ='")
//				.append(userCode).append("'");
//		}else{
//			if(null != authCode && authCode.equals("0063")){
//				querySql.append(" and b.authCode like '").append(authCode).append("%'");
//			}else if(null != authCode && authCode.equals("0064")){
//				querySql.append(" and b.authCode like '").append(authCode).append("%'");
//			}
//			else{
//				querySql.append(" and a.acceptor = '").append(userCode).append("'");
//			}
//		}
//		
//		querySql.append(" order by complaintDate desc");
//		
//		return this.daoSupport.queryForPage(querySql.toString(), pageNo, pageSize, CustomerComplaintInfo.class);
//	}
//	
//	@Override
//	public Page queryCustomerComplaintInfoPage(int userType,int timeRange,int complaintState,
//			String userCode, int pageNo, int pageSize) throws Exception
//	{
//		StringBuffer querySql = new StringBuffer();
//		
//		querySql.append("select a.* ,CASE WHEN a.complaintState=4 then  HOUR(TIMEDIFF(a.closeDate,a.complaintDate)) else HOUR(TIMEDIFF(NOW(),a.complaintDate)) end as hoursBetween from DT_CustomerComplaintInfo a");
//		querySql.append(" where 1=1 ");
//		
//		if(userType==1){
//			querySql.append(" and userCode ='")
//				.append(userCode).append("'");
//		}else{
//			querySql.append(" and acceptor ='")
//			.append(userCode).append("'");
//		}
//		Calendar calendar = Calendar.getInstance();
//		if(userType==1){
//		calendar.add(Calendar.MONTH, -1); //一般顾客得到一个月的客诉
//		}else{
//		calendar.add(Calendar.DATE, -7);; //经销商查看7天
//		}
//		String time = DateUtil.toString(calendar.getTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
//		
//		if(complaintState != FrontConstant.QUERY_ALL)
//		{
//			querySql.append(" and complaintState = ").append(complaintState);
//		}
//		
//		if(timeRange == FrontConstant.LATELY_THREE_MONTH)//查询最近3个月消息
//		{
//			querySql.append(" and complaintDate >= '").append(time).append("'");
//		}
//		else if(timeRange == FrontConstant.BEFORE_THREE_MONTH)//查询3个月前消息
//		{
//			querySql.append(" and complaintDate < '").append(time).append("'");
//		}
//		
//		querySql.append(" order by complaintDate desc");
//		
//		return this.daoSupport.queryForPage(querySql.toString(), pageNo, 
//								pageSize);
//	}
//
//	
//	@Override
//	public Page queryCustomerComplaintInfoPage3(int userType,int timeRange,int complaintState,
//			String userCode, int pageNo, int pageSize) throws Exception
//	{
//		StringBuffer querySql = new StringBuffer();
//		
//		querySql.append("select a.* ,CASE WHEN a.complaintState=4 then  HOUR(TIMEDIFF(a.closeDate,a.complaintDate)) else HOUR(TIMEDIFF(NOW(),a.complaintDate)) end as hoursBetween from DT_CustomerComplaintInfo a");
//		querySql.append(" where 1=1 ");
//		
//		if(userType==1){
//			querySql.append(" and userCode ='")
//				.append(userCode).append("'");
//		}else{
//			querySql.append(" and acceptor ='")
//			.append(userCode).append("'");
//		}
//		
//		if(complaintState != FrontConstant.QUERY_ALL)
//		{
//			querySql.append(" and complaintState = ").append(complaintState);
//		}
//		
//		querySql.append(" order by complaintDate desc");
//		
//		return this.daoSupport.queryForPage(querySql.toString(), pageNo, 
//								pageSize);
//	}
//
//	
//	
//	@Transactional
//	public void addCustomerComplaintInfo(
//			CustomerComplaintInfo customerComplaintInfo) throws Exception 
//	{
//	//	customerComplaintInfo.setCode(DBUtils.generateCode(DBConstant.CUC_INFO_CODE));
//		String code=DBConstant.getRandStr();
//		boolean flag=true;
//		while(flag==true){
//			StringBuffer sql=new StringBuffer("select count(id) from DT_CustomerComplaintInfo where code='");
//			sql.append(code).append("'");
//			int num=this.daoSupport.queryForInt(sql.toString());
//			if(num>0){
//				code=DBConstant.getRandStr();
//			}else{
//				flag=false;
//			}
//		};
//		customerComplaintInfo.setCode(code);
//		this.daoSupport.insert("DT_CustomerComplaintInfo", customerComplaintInfo);
//		
//	}
//
//	@Override
//	public CustomerComplaintInfo getCustomerComplaintInfoByCode(String code)
//			throws Exception {
//		// TODO Auto-generated method stub
//		return this.daoSupport.queryForObject("select * from DT_CustomerComplaintInfo where code=?",
//						CustomerComplaintInfo.class, code);
//	}
//
//	@Transactional
//	public void updateCustomerComplaintInfo(
//			CustomerComplaintInfo customerComplaintInfo) throws Exception 
//			
//	{
//		this.daoSupport.update("DT_CustomerComplaintInfo", customerComplaintInfo, "code='"+customerComplaintInfo.getCode()+"'");
//		
//	}
//
//	/**
//	 * 分配客服
//	 */
//	@Transactional
//	public int allotAcceptor(String code,String acceptor) throws Exception
//	{
//		String sql = "update DT_CustomerComplaintInfo set acceptState=1,acceptor=? where code= ?";
//		this.daoSupport.execute(sql,acceptor,code);
//		return 0;
//		
//	}
//
//	@Override
//	public List<CustomerComplaintInfo> queryCustomerComplaintStateList(int userType,int complaintState,String userCode) throws Exception 
//	{
//		StringBuffer querySql = new StringBuffer();
//		querySql.append("select complaintState,COUNT(complaintState) as complaintStateCount from DT_CustomerComplaintInfo  where 1=1 ");
//		       
//		        if(userType==1){
//					querySql.append(" and userCode ='")
//						.append(userCode).append("'");
//				}else{
//					querySql.append(" and acceptor ='")
//					.append(userCode).append("'");
//				}
//		        
//		if(complaintState != FrontConstant.QUERY_ALL)
//		{
//			querySql.append(" and complaintState = ").append(complaintState);
//		}
//		
//		querySql.append(" group by complaintState ").append(" order by complaintState ");
//		
//		
//	
//		
//		return this.daoSupport.queryForList(querySql.toString(),new RowMapper()
//		{
//
//			@Override
//			public CustomerComplaintInfo mapRow(ResultSet rs, int arg1) throws SQLException 
//			{
//				CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();
//				customerComplaintInfo.setComplaintState(rs.getInt("complaintState"));
//				customerComplaintInfo.setComplaintStateCount(rs.getInt("complaintStateCount"));
//				return customerComplaintInfo;
//			}
//		});
//	}
//
//	@Override
//	public int queryCustomerComplaintCount(int userType, String userCode)
//			throws Exception 
//	{
//		
//		StringBuffer sql = new StringBuffer();
//		sql.append("select count(*) from DT_CustomerComplaintInfo where ").append("acceptor='").append(userCode).append("'");
//		
//		return this.daoSupport.queryForInt(sql.toString());
//	}
//
//	@Override
//	public List<CustomerComplaintInfo> queryCustomerComplaintInfoList(
//			int timeRange, String userCode) throws Exception
//	{
//		StringBuffer querySql = new StringBuffer();
//		
//		querySql.append("select * from DT_CustomerComplaintInfo ");
//		querySql.append(" where userCode ='")
//				.append(userCode).append("'");
//		
//		Calendar calendar = Calendar.getInstance();
//		calendar.add(Calendar.MONTH, -3); //得到前3个月
//		String time = DateUtil.toString(calendar.getTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
//		
//		if(timeRange == FrontConstant.LATELY_THREE_MONTH)//查询最近3个月消息
//		{
//			querySql.append(" and complaintDate >= '").append(time).append("'");
//		}
//		else if(timeRange == FrontConstant.BEFORE_THREE_MONTH)//查询3个月前消息
//		{
//			querySql.append(" and complaintDate < '").append(time).append("'");
//		}
//		
//		querySql.append(" order by complaintDate desc");
//		
//		return this.daoSupport.queryForList(querySql.toString(), CustomerComplaintInfo.class);
//	}
//
//
//}
