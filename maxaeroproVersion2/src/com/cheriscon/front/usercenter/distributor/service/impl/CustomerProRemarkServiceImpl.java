package com.cheriscon.front.usercenter.distributor.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerProRemark;
import com.cheriscon.common.model.RGAInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.service.ICustomerProRemarkService;

@Service
public class CustomerProRemarkServiceImpl implements ICustomerProRemarkService {


	@Override
	public void addCustomerProRemark(CustomerProRemark customerProRemark)
			throws Exception {
//		if(!StringUtil.isEmpty(customerProRemark.getOrderNo()) && customerProRemark.getOrderNo().equals("1")){
//			customerProRemark.setOrderNo(getMaxNum());
//		}else{
//			customerProRemark.setOrderNo(null);
//		}
//		
//		this.daoSupport.insert("DT_CustomerProRemark", customerProRemark);
		HttpUtil.postRequestContent("rest.sealer.softwareRenewal.addCustomerProRemark", "customerProRemark.", customerProRemark);
	}

	public String getMaxNum() throws Exception{
		return null;
//		String sql="select * from DT_CustomerProRemark where orderNo is not null order by id desc limit 0,1";
//		CustomerProRemark info=this.daoSupport.queryForObject(sql, CustomerProRemark.class);
//		String DateStr=DateUtil.toString(new Date(), "yyyyMMdd");
//		String RGACODE="";
//		if(info==null){
//			RGACODE= "PNO"+DateStr+"-"+"00001";
//		}else{
//			
//			String code=info.getOrderNo();
//			int startLenth=code.indexOf("-");
//			String yyyymmdd=code.substring(3, startLenth);
//			if(DateStr.equals(yyyymmdd)){
//				String lastCode=code.substring(startLenth+1);
//				lastCode=String.valueOf(Integer.valueOf(lastCode)+1);
//				RGACODE="PNO"+DateStr+"-"+this.addZeroForNum(lastCode, 5);
//			}else{
//				RGACODE="PNO"+DateStr+"-"+"00001";
//			}
//			
//		}
//		return RGACODE;
	}
	
	public  String addZeroForNum(String str, int strLength) {
		int strLen = str.length();
		if (strLen < strLength) {
			while (strLen < strLength) {
			StringBuffer sb = new StringBuffer();
			sb.append("0").append(str);
			str = sb.toString();
			strLen = str.length();
			}
		}

			return str;
		}
	
	
	@Override
	public void updateCustomerProRemark(CustomerProRemark customerProRemark)
			throws Exception {
		// TODO Auto-generated method stub

	}
	
	public CustomerProRemark getCustomerProRemarkById(String id){
//		String sql="select * from DT_CustomerProRemark where id="+id;
//		CustomerProRemark remark=this.daoSupport.queryForObject(sql, CustomerProRemark.class);
//		return remark;
		return null;
	}

	@Override
	public CustomerProRemark getCustomerProRemarkByProCode(String code) {
//		String sql="select * from DT_CustomerProRemark where proCode=:proCode";
//		List<CustomerProRemark> list=this.daoSupport.queryForList(sql, CustomerProRemark.class, code);
//		if(list!=null && list.size()>0){
//			return list.get(0);
//		}
//		return null;
		return null;
	}

	public CustomerProRemark getCustomerProRemarkByAutelId(String autelId){
//		String sql="select * from DT_CustomerProRemark where autelId='"+autelId+"' and callFlag=1 order by id desc limit 0,1";
//		CustomerProRemark remark=this.daoSupport.queryForObject(sql, CustomerProRemark.class);
//		return remark;
		String jsonData = HttpUtil.getReqeuestContent(
				"rest.sealer.softwareRenewal.getCustomerProRemarkByAutelId", "autelId=" + autelId);
		return (CustomerProRemark) JsonUtil.getDTO(jsonData, CustomerProRemark.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerProRemark> getCustomerRemarkListByProCode(String code){
//		String sql="select * from DT_CustomerProRemark where proCode='"+code+"' order by  id desc";
//		List<CustomerProRemark> list=this.daoSupport.queryForList(sql, CustomerProRemark.class);
//		return list;
		return HttpUtil.getReqeuestContentList("rest.sealer.softwareRenewal.getCustomerRemarkListByProCode", 
				"code=" + code, CustomerProRemark.class);
	}
}
