package com.cheriscon.front.usercenter.distributor.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.model.ToCustomerCharge;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;
import com.cheriscon.front.usercenter.distributor.vo.ReChargeCardForUserVo;
import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.ParseException;

/**
 * 给客户续费业务逻辑实现类
 * 
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@SuppressWarnings("rawtypes")
@Service
public class ToCustomerChargeServiceImpl implements IToCustomerChargeService {

//   @Resource 
//   private IProductSoftwareValidChangeLogService productSoftwareValidChangeLogService;
	
	
	@Override
	public Page queryToCustomerChargePage(String sealerCode,String serialNo,String expDate,
						String proTypeCode,int pageNo, int pageSize) throws Exception {

		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("sealerCode", sealerCode);
		dataMap.put("serialNo", serialNo);
		dataMap.put("expDate", expDate);
		dataMap.put("proTypeCode", proTypeCode);
		dataMap.put("page", pageNo);
		dataMap.put("pageSize", pageSize);
		Page page = HttpUtil.postRequestContentPage("rest.sealer.softwareRenewal.queryToCustomerChargePage", dataMap, ToCustomerCharge.class);
		return page;
//		StringBuffer sql = new StringBuffer();
//
//		
//		sql.append("select * from (select distinct a.code,a.reChargePrice,a.areaCfgCode,a.saleCfgCode,b.name,b.code as proTypeCode ,")
//		   .append(" r.id as remarkId, r.orderNo,")
//		   .append(" d.serialNo ,d.code as proCode,e.name as saleConfigName,e.code as saleConfigCode ,f.validDate ,h.autelId,h.code as customerCode from DT_SaleContract a")
//		   .append(" left join DT_ProductForSealer b on b.code = a.proTypeCode")
//		   .append(" left join DT_ProductInfo d on d.saleContractCode = a.code")
//		   .append(" left join (select a.* from dt_customerproremark a, ")
//		   .append(" (select max(id) as id , proCode  from  dt_customerproremark  group by proCode) b where a.id=b.id ) r on r.proCode=d.code ")
//		   .append(" left join DT_SaleConfig e on a.saleCfgCode = e.code")
//		   .append(" left join DT_MinSaleUnitSaleCfgDetail m on a.saleCfgCode=m.saleCfgCode,DT_ProductSoftwareValidStatus f ,")
//		   .append(" DT_CustomerProInfo g,DT_CustomerInfo h where 1 = 1 and  d.code = f.procode and f.minSaleUnitCode=m.minSaleUnitCode")
//		   .append(" and d.code = g.proCode and h.code =g.customerCode ");
//
//		if (!StringUtil.isEmpty(sealerCode)) 
//		{
//			sql.append(" and a.sealerCode ='").append(sealerCode).append("'");
//		}
//
//		if (!StringUtil.isEmpty(expDate)) 
//		{
//			sql.append(" and f.validDate <='").append(expDate).append("'");
//		}
//		
//		if (!StringUtil.isEmpty(serialNo))
//		{
//			sql.append(" and d.serialNo like '%").append(serialNo).append("%'");
//		}
//		if (!StringUtil.isEmpty(proTypeCode) && !proTypeCode.equals("-1"))
//		{
//			sql.append(" and b.code ='").append(proTypeCode).append("'");
//		}
//
//		sql.append("  ) j order by j.validDate asc");
//
//		return this.daoSupport.queryForPage(sql.toString(),pageNo, pageSize ,
//				new RowMapper() 
//		{
//			public ToCustomerCharge mapRow(ResultSet rs, int arg1)
//					throws SQLException 
//			{
//				ToCustomerCharge toCustomerCharge = new ToCustomerCharge();
//				toCustomerCharge.setCode(rs.getString("code"));
//				toCustomerCharge.setReChargePrice(rs.getString("reChargePrice"));
//				toCustomerCharge.setAreaCfgCode(rs.getString("areaCfgCode"));
//				toCustomerCharge.setSaleCfgCode(rs.getString("saleCfgCode"));
//				toCustomerCharge.setProductName(rs.getString("name"));
//				toCustomerCharge.setSerialNo(rs.getString("serialNo"));
//				toCustomerCharge.setProCode(rs.getString("proCode"));
//				toCustomerCharge.setProTypeCode(rs.getString("proTypeCode"));
//				toCustomerCharge.setSaleCfgName(rs.getString("saleConfigName"));
//				toCustomerCharge.setCustomerName(rs.getString("autelId"));
//				toCustomerCharge.setValidDate(rs.getString("validDate"));
//				toCustomerCharge.setCustomerCode(rs.getString("customerCode"));
//				toCustomerCharge.setOrderNo(rs.getString("orderNo"));
//				toCustomerCharge.setRemarkId(rs.getInt("remarkId"));
//				
//				return toCustomerCharge;
//			}
//
//		});
	}

	@Override
	public String queryCustomerName(String serialNo) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append(
//				"select distinct(b.autelId)  from DT_ProductInfo a , DT_CustomerInfo b, DT_CustomerProInfo d ")
//				.append(" where a.code = d.proCode and b.code = d.customerCode and a.serialNo='")
//				.append(serialNo).append("'");
//		return this.daoSupport.queryForString(sql.toString());
		return null;
	}

//	public ReChargeCardInfo queryChargeCartInfo(String account, String pwd) {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from dt_rechargecardinfo where serialNo=");
//		sql.append("'");
//		sql.append(account);
//		sql.append("'");
//		sql.append(" and reChargePwd=");
//		sql.append("'");
//		sql.append(pwd);
//		sql.append("'");
//		ReChargeCardInfo reChargeCardInfo = (ReChargeCardInfo) this.daoSupport
//				.queryForObject(sql.toString(), ReChargeCardInfo.class,
//						new Object[] {});
//		return reChargeCardInfo;
//		return null;
//	}

	
	@SuppressWarnings("unchecked")
	public boolean toCharge(ReChargeCardInfo info ,ReChargeCardType reChargeCardType, CustomerInfo customerInfo,String proSerial)
			throws ParseException {
		
//		/**
//		 * 匹配当前产品是否属于该充值卡对应的产品型号
//		 */
//		StringBuffer s=new StringBuffer();
//		s.append("select p.* from  DT_ProductInfo p left join DT_SaleContract  s on p.saleContractCode=s.code  where p.serialNo=");
//		s.append("'");
//		s.append(proSerial);
//		s.append("'");
//		
//		s.append(" and p.proTypeCode=");
//		s.append("'");
//		s.append(reChargeCardType.getProTypeCode());
//		s.append("'");
//
//		
//		ProductInfo productInfo=(ProductInfo)this.daoSupport.queryForObject(s.toString(), ProductInfo.class, new Object[]{});
//		
//		if (productInfo==null) {
//			
//			return false;
//		}
//		
//		StringBuffer sql = new StringBuffer();
//		sql.append("select s.* from DT_ProductSoftwareValidStatus s left join DT_ProductInfo p on s.proCode=p.code " +
//				"left join DT_SaleContract sc on p.saleContractCode=sc.code left join DT_MinSaleUnitSaleCfgDetail d " +
//				"on sc.saleCfgCode=d.saleCfgCode where p.code=? and s.minSaleUnitCode=d.minSaleUnitCode");
//		
//		/**
//		 * 该产品下所有的最小销售单位
//		 */
//		List<ProductSoftwareValidStatus> statusList=this.daoSupport.queryForList(sql.toString(), ProductSoftwareValidStatus.class,productInfo.getCode());
//		
//		if (statusList.size()<1) {
//			return false;
//		}
//		
//		return chargeAndRecord(info, customerInfo, productInfo, statusList,reChargeCardType);
		return false;
	}

	/**
	 * 更新软件有效期以及添加充值记录
	 * @param info
	 * @param customerInfo
	 * @param productInfo
	 * @param statusList
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("deprecation")
//	@Transactional
	private boolean chargeAndRecord(ReChargeCardInfo info,
			CustomerInfo customerInfo, ProductInfo productInfo,
			List<ProductSoftwareValidStatus> statusList,ReChargeCardType type) throws ParseException {
//		try {
//			Calendar calendar = Calendar.getInstance();
//			String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//					.format(calendar.getTime());
//			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//			String ip=FrontConstant.getIpAddr(request);
//			Object obj=SessionUtil.getLoginUserInfo(request);
//			String operateUser="";
//			String operType="";
//			if(obj.getClass().equals(CustomerInfo.class)){
//				operateUser=((CustomerInfo)obj).getAutelId();
//				operType=BackageConstant.PRODUCT_VALID_CHANGE_CARD_OWN;
//			}
//			if(obj.getClass().equals(SealerInfo.class)){
//				operateUser=((SealerInfo)obj).getAutelId();
//				operType=BackageConstant.PRODUCT_VALID_CHANGE_CARD_SEALER;
//			}
//			// 使用充值卡续租(更新有效期)
//			for (int i = 0, j = statusList.size(); i < j; i++) {
//				ProductSoftwareValidStatus status = statusList.get(i);
//				
//				String validDate = status.getValidDate();
//				String oldDate=status.getValidDate();
//				Date tempDate = DateUtil.toDate(status.getValidDate().trim(), null);
//				Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), null), null);
//				String tempMonth=type.getMonth();
//				if(tempMonth==null){
//					return false;
//				}
//				//当前时间大于有效期时间(已经过期)
//				if(currentDate.after(tempDate)){
//					validDate  = DateUtil.toString(currentDate,null);
//				}
//				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(validDate);
//				date.setMonth(date.getMonth() + Integer.parseInt(tempMonth));
//				status.setValidDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
//				
//				StringBuffer sql=new StringBuffer();
//				sql.append("update DT_ProductSoftwareValidStatus set validDate=");
//				sql.append("'");
//				sql.append(status.getValidDate());
//				sql.append("'");
//				sql.append(" where code=");
//				sql.append("'");
//				sql.append(status.getCode());
//				sql.append("'");
//				this.daoSupport.execute(sql.toString(), new Object[]{});
//				
//				//记录升级日志
//				ProductSoftwareValidChangeLog log =new ProductSoftwareValidChangeLog();
//				log.setMinSaleUnit(status.getMinSaleUnitCode());
//				log.setNewDate(status.getValidDate());
//				log.setOldDate(oldDate);
//				log.setOperatorIp(ip);
//				log.setOperatorTime(time);
//				log.setOperatorUser(operateUser);
//				log.setOperType(operType);
//				log.setProductSN(productInfo.getSerialNo());
//				productSoftwareValidChangeLogService.saveProductSoftwareValidStatus(log);
//				
//				
//			}
//			
//			
//			//添加充值记录
//			ReChargeRecord reChargeRecord = new ReChargeRecord();
//			reChargeRecord.setIp(ip);
//			reChargeRecord.setProCode(productInfo.getCode());
//			reChargeRecord.setReChargeCardCode(info.getCode());
//			reChargeRecord.setReChargeTime(time);
//			reChargeRecord.setMemberAutelId(customerInfo.getAutelId());
//			reChargeRecord.setMemberCode(customerInfo.getCode());
//			reChargeRecord.setMemberType(FrontConstant.CUSTOMER_USER_TYPE);
//			reChargeRecord.setProSerialNo(productInfo.getSerialNo());
//			
//		
//			
//			reChargeRecord.setOperateUser(operateUser);
//			
//			String code = DBUtils.generateCode(DBConstant.RCR_INFO_CODE);
//			reChargeRecord.setCode(code);
//			
//			this.daoSupport.insert("DT_ReChargeRecord", reChargeRecord);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//		
//		// 修改充值卡的使用状态为已使用
//		info.setIsUse(FrontConstant.CARD_SERIAL_IS_YES_USE);
//		updateChargeUseStatus(info);
		
		return true;
	}

//	@Override
	public boolean updateChargeUseStatus(ReChargeCardInfo info) {

//		StringBuffer sql = new StringBuffer();
//		sql.append("UPDATE dt_rechargecardinfo set isUse=");
//		sql.append("'");
//		sql.append(info.getIsUse());
//		sql.append("'");
//		sql.append(" where code=");
//		sql.append("'");
//		sql.append(info.getCode());
//		sql.append("'");
//
//		this.daoSupport.execute(sql.toString(), new Object[] {});
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<MinSaleUnitSoftwareVO> getSoftwareInfoBySaleCfgCode(String saleCfgCode) throws Exception 
	{
//		StringBuffer sql=new StringBuffer();
//		
//		sql.append("select t.* ,e.versionName,e.softwareTypeCode,e.releaseDate from DT_SoftwareType t left join DT_MinSaleUnitSoftwareDetail d on t.code= d.softwareTypeCode")
//		   .append(" , DT_SoftwareVersion e where exists(select minSaleUnitCode from DT_MinSaleUnitSaleCfgDetail m where saleCfgCode=")
//		   .append("'").append(saleCfgCode).append("'").append(" and d.minSaleUnitCode= m.minSaleUnitCode) and t.code =e.softwareTypeCode");
//		
//		 
//		
//		List<MinSaleUnitSoftwareVO> softwareTypeList = this.daoSupport.queryForList(sql.toString(), 
//				new RowMapper(){
//
//			@Override
//			public MinSaleUnitSoftwareVO mapRow(ResultSet rs, int arg1) throws SQLException 
//			{
//				MinSaleUnitSoftwareVO minSaleUnitSoftwareVO = new MinSaleUnitSoftwareVO();
//				minSaleUnitSoftwareVO.setName(rs.getString("name"));
//				minSaleUnitSoftwareVO.setVersionNo(rs.getString("versionName"));
//				minSaleUnitSoftwareVO.setReleaseDate(rs.getString("releaseDate"));
//				return minSaleUnitSoftwareVO;
//			}
//		});
//
//		
//		return softwareTypeList;
		return null;
	}
	
	/**
	 * 查询所有即将到期或者已经到期的产品，发送邮件提醒
	 * 
	 * ***/
	@SuppressWarnings("unchecked")
	public List<ToCustomerCharge> queryValidForEmail() throws Exception{
//		String year=DateUtil.toString(new Date(), "yyyy");
//		StringBuffer sql = new StringBuffer();
//		/*sql.append(" select * from ( ");
//		sql.append(" select distinct a.code as code,a.reChargePrice as reChargePrice,b.name as productName,b.code as proTypeCode ,");
//		sql.append(" d.serialNo as serialNo ,d.code as proCode,f.validDate as validDate ,h.autelId as autelId,h.name as customerName from DT_SaleContract a");
//		sql.append(" left join DT_ProductForSealer b on b.code = a.proTypeCode");
//		sql.append(" left join DT_ProductInfo d on d.saleContractCode = a.code");
//		sql.append(" left join DT_SaleConfig e on a.saleCfgCode = e.code");
//		sql.append(" left join DT_MinSaleUnitSaleCfgDetail m on a.saleCfgCode=m.saleCfgCode,DT_ProductSoftwareValidStatus f ,");
//		sql.append(" DT_CustomerProInfo g,DT_CustomerInfo h ");
//		sql.append(" where 1 = 1 ");
//		sql.append(" and  d.code = f.procode ");
//		sql.append(" and f.minSaleUnitCode=m.minSaleUnitCode");
//		sql.append(" and d.code = g.proCode ");
//		sql.append(" and h.code =g.customerCode"); 
//		sql.append(" and f.validDate=date_format(DATE_ADD(NOW(),INTERVAL 30 DAY),'%Y-%m-%d')");
//		sql.append(" and h.autelid like '%@%'  ");
//		sql.append(" and not exists(select 1 from dt_productvalidemail where proCode=d.code and year='"+year+"')");
//		sql.append(" ) j order by j.validDate asc");*/
//		sql.append("select a.serialNo as serialNo,b.name as productName, e.validdate as validDate,d.autelId as autelId,d.name as customerName,d.languageCode as languageCode,a.code as proCode ");
//		sql.append(" from dt_productinfo a ,");
//		sql.append(" dt_productforsealer b,");
//		sql.append(" dt_customerproinfo c,");
//		sql.append(" dt_customerinfo d ,");
//		sql.append(" (select DISTINCT proCode,validdate from dt_productsoftwarevalidstatus where validDate= date_format(DATE_ADD(NOW(),INTERVAL 30 DAY),'%Y-%m-%d')  ) as e ");
//		sql.append(" where a.proTypeCode=b.`code`");
//		sql.append(" and a.`code`=c.proCode");
//		sql.append(" and c.customerCode=d.`code`");
//		sql.append(" and a.code=e.proCode");
//		sql.append(" and d.autelid like '%@%' ");
//		sql.append(" and not exists(select 1 from dt_productvalidemail where proCode=a.code and year='"+year+"') ");
//		
//		
//		return this.daoSupport.queryForList(sql.toString(), ToCustomerCharge.class);
		return null;
	 }

	@SuppressWarnings("unchecked")
	public Map<String, Object> toCharge(ReChargeCardForUserVo reChargeCardForUserVo) {
		String jsonData = HttpUtil.postRequestContent("rest.sealer.softwareRenewal.toCharge", 
				"reChargeCardForUserVo.", reChargeCardForUserVo);
		Map<String, Object> map = JsonUtil.getMapFromJson(jsonData);
		return map;
	}
	
}
