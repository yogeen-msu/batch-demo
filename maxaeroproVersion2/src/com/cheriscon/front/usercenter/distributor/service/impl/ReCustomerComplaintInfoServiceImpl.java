package com.cheriscon.front.usercenter.distributor.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;

/**
 * 客诉回复业务逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Service
public class ReCustomerComplaintInfoServiceImpl 
//extends BaseSupport<ReCustomerComplaintInfo>
	implements IReCustomerComplaintInfoService
{

	public void addReCustomerComplaintInfo(
			ReCustomerComplaintInfo reCustomerComplaintInfo) throws Exception 
	{
//		String code = DBUtils.generateCode(DBConstant.REC_INFO_CODE);
//		reCustomerComplaintInfo.setCode(code);
//		reCustomerComplaintInfo.setSourceType(1); //产品网站添加的客诉
//		this.daoSupport.insert("DT_ReCustomerComplaintInfo", reCustomerComplaintInfo);
		HttpUtil.postRequestContent("rest.complaint.addReComplaint",
				"reCustomerComplaintInfo.", reCustomerComplaintInfo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> queryReCustomerComplaintInfoByCode(String cusCode)  throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select id,reContent,reDate,rePerson,rePersonType,complaintScore,attachment,sourceType from DT_ReCustomerComplaintInfo where customerComplaintCode='").append(cusCode).append("' order by reDate Desc");
//		
//		return this.daoSupport.queryForList(sql.toString(),  new RowMapper()
//		{
//
//			int complaintCount = 0;
//			public ReCustomerComplaintInfo mapRow(ResultSet rs, int arg1) throws SQLException 
//			{
//				ReCustomerComplaintInfo reCustomerComplaintInfo = new ReCustomerComplaintInfo();
//				reCustomerComplaintInfo.setId(rs.getInt("id"));
//				reCustomerComplaintInfo.setReContent(rs.getString("reContent"));
//				reCustomerComplaintInfo.setReDate(rs.getString("reDate"));
//				reCustomerComplaintInfo.setRePerson(rs.getString("rePerson"));
//				reCustomerComplaintInfo.setRePersonType(rs.getInt("rePersonType"));
//				reCustomerComplaintInfo.setComplaintScore(rs.getInt("complaintScore"));
//				complaintCount++;
//				reCustomerComplaintInfo.setComplaintCount(complaintCount);
//				reCustomerComplaintInfo.setAttachment(rs.getString("attachment"));
//				reCustomerComplaintInfo.setSourceType(rs.getInt("sourceType"));
//				
//				if(!StringUtil.isEmpty(rs.getString("attachment")))
//				{
//					reCustomerComplaintInfo.setShowAttachment(rs.getString("attachment").substring(rs.getString("attachment").
//							  lastIndexOf("/")+1, rs.getString("attachment").length()));
//				}
//				else
//				{
//					reCustomerComplaintInfo.setShowAttachment(rs.getString("attachment"));
//				}
//			
//				return reCustomerComplaintInfo;
//				
//			}});
		String jsonData = HttpUtil.getReqeuestContent("rest.user.reCustomerComplaintInfo", "cusCode=" + cusCode);
		JSONObject jsonObj = new JSONObject();
		jsonObj = JSONObject.fromObject(jsonData);
		
//		dataMap.put("customerServiceUsers", customerServiceUsers);
//		dataMap.put("reCustomerComplaintInfoList", reCustomerComplaintInfoList);
//		dataMap.put("customerComplaintInfo", customerComplaintInfo);
		
		String customerServiceUsersJson = jsonObj.getString("customerServiceUsers");
		String reCustomerComplaintInfoListJson = jsonObj.getString("reCustomerComplaintInfoList");
		String customerComplaintInfoJson = jsonObj.getString("customerComplaintInfo");
		
		List<AdminUser> customerServiceUsers = 
				JsonUtil.getDTOList(customerServiceUsersJson, AdminUser.class);
		List<ReCustomerComplaintInfo> reCustomerComplaintInfoList = 
				JsonUtil.getDTOList(reCustomerComplaintInfoListJson, ReCustomerComplaintInfo.class);
		CustomerComplaintInfo customerComplaintInfo = (CustomerComplaintInfo) 
				JsonUtil.getDTO(customerComplaintInfoJson, CustomerComplaintInfo.class);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("customerServiceUsers", customerServiceUsers);
		map.put("reCustomerComplaintInfoList", reCustomerComplaintInfoList);
		map.put("customerComplaintInfo", customerComplaintInfo);
		
		return map;
	}

	/**
	  * 客诉回复信息分页显示方法
	  * @param customerComplaintCode
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageReCustomerComplaintInfoPage(String customerComplaintCode, int pageNo, int pageSize) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select * from DT_ReCustomerComplaintInfo");
//		sql.append(" where customerComplaintCode=?");
//		sql.append(" order by id desc");
//		
//		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
//				pageSize, ReCustomerComplaintInfo.class, customerComplaintCode);
//		return page;
		return null;
	}

	@Override
	public void updateReCustomerComplaintInfo(
			ReCustomerComplaintInfo reCustomerComplaintInfo) throws Exception 
	{
//		this.daoSupport.update("DT_ReCustomerComplaintInfo", reCustomerComplaintInfo, 
//							   "id='"+reCustomerComplaintInfo.getId()+"'");
		HttpUtil.postRequestContent("rest.complaint.updateScore", 
				"reCustomerComplaintInfo.", reCustomerComplaintInfo);
	}

	@Override
	public ReCustomerComplaintInfo getReCustomerComplaintInfoByCode(String code)
			throws Exception 
	{
//		return this.daoSupport.queryForObject(" select * from DT_ReCustomerComplaintInfo " +
//											 " where code='"+code+"'", ReCustomerComplaintInfo.class);
		return null;
	}

}
