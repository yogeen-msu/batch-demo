package com.cheriscon.front.usercenter.distributor.service;

import java.util.List;
import java.util.Map;

import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;
import com.cheriscon.front.usercenter.distributor.vo.ReChargeCardForUserVo;

/**
 * 给客户续费业务逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public interface IToCustomerChargeService
{
	
	/**
	 * 查询经销商给客户续租信息记录
	 * @param sealerCode 经销商code
	 * @param serialNo 产品序列号
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	public Page queryToCustomerChargePage(String sealerCode, String serialNo,String expDate,
			String proTypeCode,int pageNo, int pageSize) throws Exception;
	
	/**
	 * 根据产品序列号查询该产品的使用用户
	 * @param serialNo
	 * @return
	 * @throws Exception
	 */
	public String queryCustomerName(String serialNo)throws Exception;
	
	/**
	 * 根据充值卡账号和密码查询充值卡信息
	 * @param account
	 * @param pwd
	 * @return
	 */
//	public ReChargeCardInfo queryChargeCartInfo(String account,String pwd);
	
	/**
	 * 使用充值卡充值
	 * @param info
	 * @return
	 */
//	public boolean toCharge(ReChargeCardInfo info ,ReChargeCardType reChargeCardType,CustomerInfo customerInfo,String proSerial)throws Exception;
	
	/**
	 * 修改充值卡的使用状态
	 * @param info
	 * @return
	 */
//	public boolean updateChargeUseStatus(ReChargeCardInfo info);
	
	/**
	 * 根据标准销售code查询软件功能信息
	 * @param saleCfgCode
	 * @return
	 * @throws Exception
	 */
	public List<MinSaleUnitSoftwareVO> getSoftwareInfoBySaleCfgCode(String saleCfgCode) throws Exception;
	
	/**
	 * 查询即将到期和已经到期的产品，发送邮件提醒
	 * @return List<MyProduct>
	 */
//	public List<ToCustomerCharge> queryValidForEmail() throws Exception;
	
	public Map<String, Object> toCharge(ReChargeCardForUserVo reChargeCardForUserVo);
}
