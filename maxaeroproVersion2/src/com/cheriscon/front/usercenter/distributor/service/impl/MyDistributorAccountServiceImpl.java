﻿//package com.cheriscon.front.usercenter.distributor.service.impl;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Service;
//
//import com.cheriscon.common.model.MyDistributoAccountInfo;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.framework.util.StringUtil;
//import com.cheriscon.front.usercenter.distributor.service.IMyDistributorAccountService;
//
///**
// * 我的账户业务逻辑实现类
// * @author yangpinggui
// * @version 创建时间：2013-1-5
// */
//@Service
//public class MyDistributorAccountServiceImpl extends BaseSupport<MyDistributoAccountInfo> 
//	implements IMyDistributorAccountService
//{
//
//	@Override
//	public Page queryMyDistributorAccountPage(String saleCode, int pageNo,
//			int pageSize) throws Exception
//	{
//		StringBuffer sql= new StringBuffer();
//		
//		sql.append("select b.name as productName,d.name as saleName, e.name as languageName from DT_SaleContract a ")
//		 	.append(" left join DT_ProductType b on a.proTypeCode= b.code left join DT_AreaConfig d ")
//			.append(" on a.areaCfgCode=d.code left join DT_LanguageConfig e on a.languageCfgCode = e.code where 1= 1 ");
//		
//		if(!StringUtil.isEmpty(saleCode))
//		{
//			sql.append(" and a.sealerCode ='").append(saleCode).append("'");
//		}
//		
//		sql.append(" order by a.id ");
//		
//		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, 
//				new RowMapper()
//		{
//
//			@Override
//			public MyDistributoAccountInfo mapRow(ResultSet rs, int arg1) throws SQLException 
//			{
//				MyDistributoAccountInfo myDistributoAccountInfo = new MyDistributoAccountInfo();
//				myDistributoAccountInfo.setProductName(rs.getString("productName"));
//				myDistributoAccountInfo.setSaleArea(rs.getString("saleName"));
//				myDistributoAccountInfo.setLanguageName(rs.getString("languageName"));
//				
//				return myDistributoAccountInfo;
//			}
//			
//		});
//		
//		return page;
//	}
//
//
//
//}
