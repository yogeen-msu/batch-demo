package com.cheriscon.front.usercenter.distributor.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.vo.AddCustomerComplaintVo;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.Vehicle;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.vo.HangUpVo;
import com.cheriscon.front.usercenter.distributor.vo.ReplyCustomerComplaintVo;

@Service
public class CustomerComplaintInfoServiceImpl implements ICustomerComplaintInfoService {

	@Override
	public Page queryCustomerComplaintInfoPage(int userType, int timeRange,
			int complaintState, String userCode, int pageNo, int pageSize)
			throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.customer.complaintInfoPage", 
				"userType=" + userType + "&timeRange=" + timeRange + 
				"&complaintState=" + complaintState + "&userCode=" + userCode + 
				"&page=" + pageNo + "&pageSize=" + pageSize);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", CustomerComplaintInfo.class);
		Page page = (Page) JsonUtil.getDTO(jsonData, Page.class, map);
		return page;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> addCustomerComplaintInfo(AddCustomerComplaintVo acc) {
		String jsonData = HttpUtil.postRequestContent("rest.complaint.toAddComplaintInfo", 
				"addCustomerComplaintVo.", acc);
		JSONObject jsonObj = new JSONObject();
		jsonObj = JSONObject.fromObject(jsonData);
		String accJson = jsonObj.getString("acc");
		String yearListJson = jsonObj.getString("yearList");
		String listProductJson = jsonObj.getString("listProduct");
		
		AddCustomerComplaintVo acc1 = (AddCustomerComplaintVo) 
				JsonUtil.getDTO(accJson, AddCustomerComplaintVo.class);
		List<Vehicle> yearList = JsonUtil.getDTOList(yearListJson, Vehicle.class);
		List<MyProduct> listProduct = JsonUtil.getDTOList(listProductJson, MyProduct.class);
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("acc", acc1);
		dataMap.put("yearList", yearList);
		dataMap.put("listProduct", listProduct);
		return dataMap;
	}

	@Override
	public void addCustomerComplaintInfo(CustomerComplaintInfo customerComplaintInfo, 
			String languageCode, String userName, String autelId) throws Exception {
		HttpUtil.postRequestContent("rest.complaint.addComplaintInfo", 
				"customerComplaintInfo.", customerComplaintInfo);
	}
	
	public Map<String, Object> addCustomerComplaintInfo(CustomerComplaintInfo customerComplaintInfo) throws Exception {
		@SuppressWarnings("unused")
		String jsonData = HttpUtil.postRequestContent("rest.sealer.complaint.addCustomerComplaintInfo", 
				"customerComplaintInfo.", customerComplaintInfo);
//		JSONObject jsonObject = JSONObject.fromObject(jsonData);
//		String customerInfoStr = jsonObject.getString("customerInfo");
//		String languageStr = jsonObject.getString("language");
//		CustomerInfo customerInfo = (CustomerInfo) JsonUtil.getDTO(customerInfoStr, CustomerInfo.class);
//		Language language = (Language) JsonUtil.getDTO(languageStr, Language.class);
		Map<String, Object> dataMap = new HashMap<String, Object>();
//		dataMap.put("customerInfo", customerInfo);
//		dataMap.put("language", language);
		return dataMap;
	}
	
	@Override
	public Page queryCustomerComplaintInfoPage3(int userType, int timeRange,
			int complaintState, String userCode, int pageNo, int pageSize) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userType", userType);
		map.put("timeRange", timeRange);
		map.put("complaintState", complaintState);
		map.put("userCode", userCode);
		map.put("page", pageNo);
		map.put("pageSize", pageSize);
		Page page = HttpUtil.postRequestContentPage("rest.sealer.manager.queryCustomerComplaintInfoPage3", 
				map, CustomerComplaintInfo.class);
		return page;
	}

	public Page queryCustomerComplaintInfoPage2(String authCode, int userType,
			int timeRange, int complaintState, String userCode, int pageNo,
			int pageSize) throws Exception {
		Page page = HttpUtil.getReqeuestContentPage("rest.sealer.complaint.queryCustomerComplaintInfoPage2", 
				"authCode=" + authCode + "&userType=" + userType + "&timeRange=" + timeRange + 
				"&complaintState=" + complaintState + "&userCode=" + userCode + 
				"&page=" + pageNo + "&pageSize=" + pageSize, CustomerComplaintInfo.class);
		return page;
	}

	@Override
	public CustomerComplaintInfo getCustomerComplaintInfoByCode(String code)
			throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.complaint.getByCode", "code=" + code);
		return (CustomerComplaintInfo) JsonUtil.getDTO(jsonData, CustomerComplaintInfo.class);
	}

	@Override
	public void updateCustomerComplaintInfo(
			CustomerComplaintInfo customerComplaintInfo) throws Exception {
		HttpUtil.postRequestContent("rest.complaint.updateComplaint", 
				"customerComplaintInfo.", customerComplaintInfo);
	}

	@Override
	public List<CustomerComplaintInfo> queryCustomerComplaintStateList(
			int userType, int complaintState, String userCode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public int allotAcceptor(String code, String acceptor) throws Exception {
		HttpUtil.getReqeuestContent("rest.sealer.complaint.allotAcceptor", "code=" + code + "&acceptor=" + acceptor);
		return 0;
	}

	@Override
	public int queryCustomerComplaintCount(int userType, String userCode)
			throws Exception {
		String jsonData = HttpUtil.getReqeuestContent(
				"rest.sale.queryCustomerComplaintCount", "userType=" + userType + "&userCode=" + userCode);
		return Integer.parseInt(jsonData);
	}

	@Override
	public List<CustomerComplaintInfo> queryCustomerComplaintInfoList(
			int timeRange, String userCode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void hangUpComplaint(HangUpVo hangUpVo) {
		HttpUtil.postRequestContent("rest.sealer.complaint.hangUpComplaint", "hangUpVo.", hangUpVo);
	}
	
	public void replyCustomerComplaintByDistributor(ReplyCustomerComplaintVo replyCustomerComplaintVo) {
		HttpUtil.postRequestContent("rest.sealer.complaint.replyCustomerComplaintByDistributor", 
				"replyCustomerComplaintVo.", replyCustomerComplaintVo);
	}
	
}
