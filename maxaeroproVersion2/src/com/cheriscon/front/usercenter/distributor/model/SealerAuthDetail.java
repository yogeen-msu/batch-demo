package com.cheriscon.front.usercenter.distributor.model;


public class SealerAuthDetail  implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5526488622636020952L;

	private Integer id;
	
	private String sealerCode;
	
	private String authCode;

	public Integer getId() {
		return id;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	
	
}
