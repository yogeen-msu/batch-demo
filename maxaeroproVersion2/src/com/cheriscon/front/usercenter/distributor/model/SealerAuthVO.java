package com.cheriscon.front.usercenter.distributor.model;


public class SealerAuthVO  implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4012182048236169655L;
 
	private Integer id;
	
	private String code;
	
	private String url;
	
	private String sealermenu;
	
	private String name;
	
	private Integer sortNum;
	
	private String autelId;
	
	private String checkedFlag;
	
	public String getCheckedFlag() {
		return checkedFlag;
	}

	public void setCheckedFlag(String checkedFlag) {
		this.checkedFlag = checkedFlag;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Integer getSortNum() {
		return sortNum;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSortNum(Integer sortNum) {
		this.sortNum = sortNum;
	}

	public String getCode() {
		return code;
	}

	public String getUrl() {
		return url;
	}

	public String getSealermenu() {
		return sealermenu;
	}

	

	public void setCode(String code) {
		this.code = code;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setSealermenu(String sealermenu) {
		this.sealermenu = sealermenu;
	}

	
}
