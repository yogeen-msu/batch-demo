package com.cheriscon.front.usercenter.distributor.vo;

import java.io.Serializable;

public class ReplyCustomerComplaintVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5588842157677476472L;
	
	private String cusComCode;
	private String reContent;
	private String filePath;
	private Integer userType;
	private String autelId;
	
	private String code;
	private String frontAutelId;

	public String getCusComCode() {
		return cusComCode;
	}

	public void setCusComCode(String cusComCode) {
		this.cusComCode = cusComCode;
	}

	public String getReContent() {
		return reContent;
	}

	public void setReContent(String reContent) {
		this.reContent = reContent;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFrontAutelId() {
		return frontAutelId;
	}

	public void setFrontAutelId(String frontAutelId) {
		this.frontAutelId = frontAutelId;
	}

}
