package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.service.IChangeLanguageService;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;
import com.cheriscon.product.model.ProductForSealer;
import com.cheriscon.product.service.IProductForSealerService;

/**
 * 给客户更换语言
 * @author chenqichuan
 * @version 创建时间：2014-02-18
 */
@Component("changeLanguageWidget")
public class ChangeLanguageWidget extends RequestParamWidget
{
	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private IProductForSealerService productForSealerService;
	@Resource
	private IToCustomerChargeService toCustomerChargeService;
	@Resource
	private IChangeLanguageService changeLanguageService;
//	@Resource
//	private ILanguageConfigService LanguageService;
	@Resource
	private IProductInfoService productInfoService;
	
	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String operationType = params.get("operationType");
		
		
		if(!StringUtil.isEmpty(operationType))
		{
			if(Integer.parseInt(operationType) == 1)
			{
				try {
//					queryToCustomerChangePage(request,params);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if(Integer.parseInt(operationType) == 2)
			{
//				queryLanguagePage(request,params);
			}
			else if(Integer.parseInt(operationType) == 3)
			{
				querySealerProductPage(request,params);
			}
		}
		
	}
	
	//经销商更换语言，需要查出经销商当前销售契约中的语言
//	protected void queryLanguagePage(HttpServletRequest request,Map<String, String> params){
//		String proCode = params.get("proCode");
//		String language = params.get("language");
//		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
//		
//		if(sealerInfo == null)
//		{
//			return;
//		}
//		ProductInfo info=productInfoService.getByProCode(proCode);
//		if(info== null){
//			return ;
//		}
//		
//		List<LanguageConfig> listLanguage= LanguageService.queryBySealerCode(info.getProTypeCode(), sealerInfo.getCode());
//		
//		this.putData("language", language);
//		this.putData("listLanguage", listLanguage);
//		this.putData("productInfo", info);
//	}
	
	
	//查询经销商的MaxiSys产品
//	protected void queryToCustomerChangePage(HttpServletRequest request,Map<String, String> params)
//	{
//		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
//		
//		if(sealerInfo == null)
//		{
//			return;
//		}
//		
//		String pageSize = params.get("pageSize");
//		String pageNoStr = params.get("pageno");
//		Integer[] ids = this.parseId();
//		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
//		
//		String serialNo = params.get("serialNo");
//		String proTypeCode = params.get("productTypeCode");
//		
//		try
//		{
//			List<ProductForSealer> productType=productForSealerService.getProductForSealerChange();
//			Page dataPage =changeLanguageService.queryToCustomerChangePage(sealerInfo.getCode(), serialNo, proTypeCode, pageNo,Integer.parseInt(pageSize));
//			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
//			String page_html=pagerHtmlBuilder.buildPageHtml();
//			long totalPageCount = dataPage.getTotalPageCount();
//			long totalCount = dataPage.getTotalCount();
//			this.putData("pager", page_html); 
//			this.putData("pagesize", pageSize);
//			this.putData("pageno", pageNo);
//			this.putData("totalcount", totalCount);
//			this.putData("totalpagecount",totalPageCount);
//			this.putData("customerChargeList", dataPage.getResult());
//			this.putData("serialNo",serialNo);
//			this.putData("productType",productType);
//		} 
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}

	   //查询经销商的产品
		protected void querySealerProductPage(HttpServletRequest request,Map<String, String> params)
		{
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			
			if(sealerInfo == null)
			{
				return;
			}
			
			String pageSize = params.get("pageSize");
			String pageNoStr = params.get("pageno");
			Integer[] ids = this.parseId();
			Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
			
			String serialNo = params.get("serialNo");
			String proTypeCode = params.get("productTypeCode");
			
			try
			{
				List<ProductForSealer> productType=productForSealerService.getProductForSealer();
				Page dataPage =changeLanguageService.querySealerProductPage(sealerInfo.getCode(), serialNo, proTypeCode, pageNo,Integer.parseInt(pageSize));
				StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
				String page_html=pagerHtmlBuilder.buildPageHtml();
				long totalPageCount = dataPage.getTotalPageCount();
				long totalCount = dataPage.getTotalCount();
				this.putData("pager", page_html); 
				this.putData("pagesize", pageSize);
				this.putData("pageno", pageNo);
				this.putData("totalcount", totalCount);
				this.putData("totalpagecount",totalPageCount);
				this.putData("customerChargeList", dataPage.getResult());
				this.putData("serialNo",serialNo);
				this.putData("productType",productType);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}

	
	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
