package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.CustomerProRemark;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerProRemarkService;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;
import com.cheriscon.product.model.ProductForSealer;
import com.cheriscon.product.service.IProductForSealerService;
import com.cheriscon.user.service.ICustomerInfoService;

/**
 * 给客户续租控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Component("toCustomerChargeWidget")
public class ToCustomerChargeWidget extends RequestParamWidget
{
	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private IToCustomerChargeService toCustomerChargeService;
	
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	
	@Resource
	private IProductForSealerService productForSealerService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ICustomerProRemarkService  customerProRemarkService;
	
	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String operationType = params.get("operationType");
		
		
		if(!StringUtil.isEmpty(operationType))
		{
			if(Integer.parseInt(operationType) == 1)
			{
				queryToCustomerChargePage(request,params);
			}
			else if(Integer.parseInt(operationType) == 2)
			{
				try 
				{
					queryCustomerInfoPage(request,params);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}else if (Integer.parseInt(operationType) == 3){
				this.putData("proCode",  params.get("proCode"));
				this.putData("autelId",params.get("autelId"));
			}else if (Integer.parseInt(operationType) == 4){
//				CustomerProRemark remark=customerProRemarkService.getCustomerProRemarkById(params.get("id"));
//				this.putData("remark",remark);
			}
			else if (Integer.parseInt(operationType) == 5){
				List<CustomerProRemark> list = customerProRemarkService.getCustomerRemarkListByProCode(params.get("proCode"));
				this.putData("list",list);
			}
		}
		
	}
	
	
	protected void queryCustomerInfoPage(HttpServletRequest request,Map<String, String> params){
		String customerCode = params.get("customerCode");
		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(customerCode);
			CustomerProRemark remark = new CustomerProRemark();
			if (customerInfo != null) {
				remark = customerProRemarkService.getCustomerProRemarkByAutelId(customerInfo.getAutelId());
			}
			this.putData("remark",remark);
			this.putData("customerInfo", customerInfo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	protected void queryToCustomerChargePage(HttpServletRequest request,Map<String, String> params)
	{
		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
		
		if (sealerInfo == null) {
			return;
		}
		
		String pageSize = params.get("pageSize");
		String pageNoStr = params.get("pageno");
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		
		String serialNo = params.get("serialNo");
		String proTypeCode = request.getParameter("proTypeCode");
		String expDate=request.getParameter("expDate");
		
		try
		{
			Page dataPage = toCustomerChargeService.queryToCustomerChargePage(
					sealerInfo.getCode(), serialNo, expDate, proTypeCode, pageNo,Integer.parseInt(pageSize));
					
			List<ProductForSealer> productTypeList = productForSealerService.getProductForSealer();
			this.putData("productTypeList", productTypeList);
			
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
			String page_html = pagerHtmlBuilder.buildPageHtml();
			long totalPageCount = dataPage.getTotalPageCount();
			long totalCount = dataPage.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			this.putData("customerChargeList", dataPage.getResult());
			this.putData("serialNo",serialNo);
			this.putData("expDate",expDate);
			this.putData("proTypeCode",proTypeCode);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
