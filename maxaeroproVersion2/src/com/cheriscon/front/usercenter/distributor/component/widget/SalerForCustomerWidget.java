package com.cheriscon.front.usercenter.distributor.component.widget;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadFile;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.IVehicleService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.backstage.system.service.IDataLogService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.backstage.system.service.ISoftListService;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.DataLogging;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.SoftList;
import com.cheriscon.common.model.Vehicle;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.common.vo.SoftListVo;
import com.cheriscon.cop.processor.MultipartRequestWrapper;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;
import com.cheriscon.user.model.QuestionDesc;
import com.cheriscon.user.service.ICustomerInfoService;
import com.cheriscon.user.service.ICustomerProductService;
import com.cheriscon.user.service.IQuestionDescService;
//import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
//import com.cheriscon.app.cms.component.widget.RequestParamWidget;
//import com.cheriscon.backstage.member.service.IComplaintAdminuserInfoService;
//import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
//import com.cheriscon.backstage.member.service.ICustomerInfoVoService;
//import com.cheriscon.backstage.member.service.ICustomerLoginInfoService;
//import com.cheriscon.backstage.member.service.ICustomerProductService;
//import com.cheriscon.backstage.member.service.ICustomerSoftwareService;
//import com.cheriscon.backstage.member.service.IProUpgradeRecordService;
//import com.cheriscon.backstage.member.service.IQuestionDescService;
//import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
//import com.cheriscon.backstage.member.vo.CustomerInfoVo;
//import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;
//import com.cheriscon.backstage.member.vo.CustomerProductVo;
//import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
//import com.cheriscon.backstage.member.vo.ProUpgradeRecordVo;
//import com.cheriscon.backstage.system.service.IDataLogService;
//import com.cheriscon.backstage.system.service.IProductRepairRecordService;
//import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
//import com.cheriscon.backstage.system.service.ISoftListService;
//import com.cheriscon.backstage.system.service.IUpgradeRecordService;
//import com.cheriscon.common.model.ComplaintAdminuserInfo;
//import com.cheriscon.common.model.DataLogging;
//import com.cheriscon.common.model.DataLoggingContent;
//import com.cheriscon.common.model.ProductRepairRecord;
//import com.cheriscon.common.model.QuestionDesc;
//import com.cheriscon.common.model.SealerDataAuthDeails;
//import com.cheriscon.common.model.SoftList;
//import com.cheriscon.common.model.UpgradeRecord;
//import com.cheriscon.framework.jms.EmailModel;
//import com.cheriscon.framework.jms.EmailProducer;
//import com.cheriscon.front.user.service.ICustomerInfoService;

/**
 * 客户综合信息查询平台
 * @author chenqichuan
 * @version 创建时间：2013-7-5
 */
@Component("salerForCustomerWidget")
public class SalerForCustomerWidget extends RequestParamWidget
{
	/**
	 * 客诉添加或回复成功
	 */
	private static final String CUSTOMERCOMPLAINT_SUCCESS = "4";

	/**
	 * 客诉添加或回复失败
	 */
	private static final String CUSTOMERCOMPLAINT_FAIL = "3";

	/**
	 * 客诉添加或回复上传文件大小不能超过4 M
	 * 
	 */
	private static final String CUSTOMERCOMPLAINT_FILE_SIZE = "2";

	/**
	 * 客诉添加或回复上传文件格式的限制
	 * 
	 */
	private static final String CUSTOMERCOMPLAINT_FILE_TYPE = "1";
//	@Resource
//	private ICustomerLoginInfoService customerLoginInfoService;
	@Resource
	private ICustomerProductService customerProductService;
	@Resource
	private ICustomerComplaintInfoVoService customerComplaintInfoVoService;
	@Resource
	private ICustomerInfoService customerInfoService;
//	@Resource
//	private ICustomerInfoVoService customerInfoVoService;
//	@Resource
//	private IProUpgradeRecordService proUpgradeRecordService;
//	@Resource
//	private IProductRepairRecordService productRepairRecordService; 
//	@Resource
//	private ICustomerSoftwareService customerSoftwareService;
	@Resource
	private ILanguageService languageService;
//	@Resource
//	private IAdminUserManager adminUserManager;
	@Resource
	private IQuestionDescService questionDescService;
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;
	@Resource
	private IReCustomerComplaintInfoService reCustomerComplaintInfoService;
//	@Resource
//	private IEmailTemplateService emailTemplateService;
//	@Resource
//	private ISmtpManager smtpManager;
   /* @Resource
    private IUpgradeRecordService upgradeRecordService ;*/
	@Resource
	private IDataLogService dataLogService;
    @Resource
    private IVehicleService vehicleService;
//	@Resource
//	private EmailProducer emailProducer;
	@Resource
	private ISealerDataAuthDeailsService sealerDataDeailsService;
	@Resource
	private ISoftListService softListService;
	@Resource
	private IProductInfoService productInfoService;
	
//	private List<CustomerLoginInfoVo> customerLoginInfoVos;

	// 查询、修改、删除参数

//	private CustomerLoginInfoVo customerLoginInfoVoSel=new CustomerLoginInfoVo();
	private CustomerProductVo customerProductVoSel = new CustomerProductVo();
	private CustomerInfo customerInfoEdit=new CustomerInfo();
//	private CustomerInfoVo customerInfoVo=new CustomerInfoVo();
	private CustomerComplaintInfoVo customerComplaintInfoVoSel=new CustomerComplaintInfoVo();
//	private ProUpgradeRecordVo proUpgradeRecordVoSel=new ProUpgradeRecordVo();
//	private CustomerSoftwareVo customerSoftwareVoSel=new CustomerSoftwareVo();
//	private CustomerLoginInfoVo customerLoginInfoVo=new CustomerLoginInfoVo() ;
	private Language language =new Language();
	private List<AdminUser> customerServiceUsers;
	
	public List<AdminUser> getCustomerServiceUsers() {
		return customerServiceUsers;
	}
	public void setCustomerServiceUsers(List<AdminUser> customerServiceUsers) {
		this.customerServiceUsers = customerServiceUsers;
	}
	public Language getLanguage() {
		return language;
	}
	public void setLanguage(Language language) {
		this.language = language;
	}
	public ICustomerProductService getCustomerProductService() {
		return customerProductService;
	}
	public void setCustomerProductService(
			ICustomerProductService customerProductService) {
		this.customerProductService = customerProductService;
	}

//	public CustomerLoginInfoVo getCustomerLoginInfoVo() {
//		return customerLoginInfoVo;
//	}

//	public void setCustomerLoginInfoVo(CustomerLoginInfoVo customerLoginInfoVo) {
//		this.customerLoginInfoVo = customerLoginInfoVo;
//	}
//
//	public List<CustomerLoginInfoVo> getCustomerLoginInfoVos() {
//		return customerLoginInfoVos;
//	}
//
//	public void setCustomerLoginInfoVos(
//			List<CustomerLoginInfoVo> customerLoginInfoVos) {
//		this.customerLoginInfoVos = customerLoginInfoVos;
//	}
//
//	public CustomerLoginInfoVo getCustomerLoginInfoVoSel() {
//		return customerLoginInfoVoSel;
//	}

//	public void setCustomerLoginInfoVoSel(
//			CustomerLoginInfoVo customerLoginInfoVoSel) {
//		this.customerLoginInfoVoSel = customerLoginInfoVoSel;
//	}
//
//	public CustomerProductVo getCustomerProductVoSel() {
//		return customerProductVoSel;
//	}
//
//	public void setCustomerProductVoSel(CustomerProductVo customerProductVoSel) {
//		this.customerProductVoSel = customerProductVoSel;
//	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

//	public CustomerSoftwareVo getCustomerSoftwareVoSel() {
//		return customerSoftwareVoSel;
//	}
//
//	public void setCustomerSoftwareVoSel(CustomerSoftwareVo customerSoftwareVoSel) {
//		this.customerSoftwareVoSel = customerSoftwareVoSel;
//	}
//
//	public ProUpgradeRecordVo getProUpgradeRecordVoSel() {
//		return proUpgradeRecordVoSel;
//	}
//
//	public ProductRepairRecord getProductRepairRecord() {
//		return productRepairRecord;
//	}

//	public void setProUpgradeRecordVoSel(ProUpgradeRecordVo proUpgradeRecordVoSel) {
//		this.proUpgradeRecordVoSel = proUpgradeRecordVoSel;
//	}
//
//	public void setProductRepairRecord(ProductRepairRecord productRepairRecord) {
//		this.productRepairRecord = productRepairRecord;
//	}
//
//	private ProductRepairRecord productRepairRecord=new ProductRepairRecord();
//	
	public CustomerComplaintInfoVo getCustomerComplaintInfoVoSel() {
		return customerComplaintInfoVoSel;
	}

	public void setCustomerComplaintInfoVoSel(
			CustomerComplaintInfoVo customerComplaintInfoVoSel) {
		this.customerComplaintInfoVoSel = customerComplaintInfoVoSel;
	}
//
//	public CustomerInfoVo getCustomerInfoVo() {
//		return customerInfoVo;
//	}
//
//	public void setCustomerInfoVo(CustomerInfoVo customerInfoVo) {
//		this.customerInfoVo = customerInfoVo;
//	}

	public CustomerInfo getCustomerInfoEdit() {
		return customerInfoEdit;
	}

	public void setCustomerInfoEdit(CustomerInfo customerInfoEdit) {
		this.customerInfoEdit = customerInfoEdit;
	}

	private String jsonData;
	private String code;
	private String productSN;

	public String getProductSN() {
		return productSN;
	}
	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	public boolean cacheAble()
	{
		return false;
	}

	@Override
	protected void display(Map<String, String> params)
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String type=params.get("type"); 
		this.putData("type",type);
		 SealerInfo info= (SealerInfo)SessionUtil.getLoginUserInfo(request);
		  String showCity="N" ;
		  if(info.getAuth()!=null && info.getAuth().startsWith("0063")){
			  showCity="Y" ;
		  }
		  this.putData("showCity",showCity);
		if(type.equals("0")){  //根据条件查询客户信息
//        	listCustomerProduct(params,request);
//        	 this.putData("default","N");
		}
		if(type.equals("1")){  //默认首页
			 
			  this.putData("default","Y");
		}
        if(type.equals("2")){  //查询客户基本信息
			getCustomerInfo(params,request);
		}
        
        if(type.equals("3")){  //查询客户产品信息
//        	getProductInfo(params,request);
		}
        if(type.equals("4")){  //查询客户客诉信息
//        	getCustomerComplaint(params,request);
		}
        if(type.equals("5")){  //查询客户消费基本信息
//        	getCustomerConsume(params,request);
		}
        if(type.equals("6")){  //查询客户登录信息
//        	getCustomerLoginInfo(params,request);
		}if(type.equals("7")){  //查询产品code查询升级信息
//			getProUpgrade(params,request);
		}if(type.equals("8")){  //查询产品code查询软件信息
//			getProSoftware(params,request);
		}if(type.equals("9")){  //查询产品code查询维修信息
//			getProService(params,request);
		}
		if(type.equals("v")){ //查询客诉详情
			queryCustomerComplaintForSealer(params);
		}
		if(type.equals("q")){ //查询客诉表格
			queryCustomerComplaintDetails(request);
		}
		if(type.equals("A")){ //客诉平台
			 this.putData("AutelID",params.get("autelID"));
			 this.putData("code",params.get("code"));
		}
		if(type.equals("R")){ //重设密码
			
			 try {
				customerInfoEdit = customerInfoService.getCustomerByCode(code);
				this.putData("customerInfoEdit",customerInfoEdit);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(type.equals("S")){ //经销商根据条件查询序列号信息20140211
			listCustomerProduct(params,request);
			 this.putData("default","N");
		}
		if(type.equals("S1")){ //经销商根据序列号查询软件信息20140211
			String code=params.get("code");
			String productSN=params.get("productSN");
			this.putData("code", code);
			this.putData("productSN", productSN);
			this.putData("default","N");
		}
		if(type.equals("S11")){ //经销商根据序列号查询软件信息20140211
			getUpgradeSoftList(params,request);
			 this.putData("default","1");
		}
		
		if(type.equals("S2")){ //经销商根据序列号查询客户基本信息20140211
			getCustomerBaseInfo(params,request);
			 this.putData("default","0");
		}
		if(type.equals("S21")){ //经销商更换序列号20140211
			String code=params.get("code");
			String productSN=params.get("productSN");
			this.putData("code", code);
			this.putData("productSN", productSN);
			this.putData("default","0");
		}
		if(type.equals("S22")){ //经销商重置客户密码20140211
			String code=params.get("code");
			String productSN=params.get("productSN");
			this.putData("code", code);
			this.putData("productSN", productSN);
			this.putData("default","0");
		}if(type.equals("S23")){ //经销商更新客户信息20140421
			String code=params.get("code");
			String productSN=params.get("productSN");
			
			//客户基本信息
			try {
				customerInfoEdit = customerInfoService.getCustomerByCode(code);
			} catch (Exception e) {
				e.printStackTrace();
			}
			setDataList(2,request,customerInfoEdit.getAutelId());
			this.putData("customerInfo", customerInfoEdit);
			this.putData("code", code);
			this.putData("productSN", productSN);
			this.putData("default","0");
		}
		if(type.equals("S3")){ //经销商根据序列号查询客诉信息20140211
			String code=params.get("code");
			String productSN=params.get("productSN");
			this.putData("code", code);
			this.putData("productSN", productSN);
			 this.putData("default","2");
		}
		if(type.equals("S31")){ //经销商根据序列号查询客诉信息20140211
			getCustomerComplaintInfo(params,request);
			this.putData("default","2");
		}
		if(type.equals("S32")){ //经销商根据添加客诉信息20140211
			addComplaintInfo(params,request);
			 this.putData("default","2");
		}
		if(type.equals("S33")){ //经销商根据保存客诉信息20140211
//			addCustomerComplaintInfo(params,request);
//			this.putData("default","2");
		}
		if(type.equals("S34")){
			queryCustomerComplaintForSealer(params);
			this.putData("default","2");
		}
		if(type.equals("S4")){ //经销商根据序列号查询数据日志信息20140211
			getDataLogging(params,request);
			 this.putData("default","3");
		}
		
		if(type.equals("S41")){ //经销商根据查看数据日志的详细信息
//			getDataLogDetails(params,request);
			 this.putData("default","3");
		}
		
	}
	
	//经销商查询数据日志的详细信息
//	public void getDataLogDetails(Map<String, String> params,HttpServletRequest request){
//		String code=params.get("code");
//		String productSN=params.get("productSN");
//		String LogId=params.get("LogId");
//		try {
//			DataLogging log=dataLogService.getDataLogDetails(LogId);
//			
//			List<DataLoggingContent> logList=dataLogService.getDataLogContent(log.getId());
//			this.putData("log",log);
//			this.putData("logList",logList);
//			this.putData("code",code);
//			this.putData("productSN",productSN);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	//经销商根据产品序列号获取DataLogging
	public void getDataLogging(Map<String, String> params,HttpServletRequest request){
		String code=params.get("code");
		String productSN=params.get("productSN");
		
		try {
		List<DataLogging> dataList=dataLogService.getDataLogList(productSN);
		this.putData("dataList",dataList);
		this.putData("code",code);
		this.putData("productSN",productSN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//经销商根据产品序列号获得客户的升级软件20140212
	@SuppressWarnings("unused")
	public void getUpgradeSoftList(Map<String, String> params,HttpServletRequest request){
		String code=params.get("code");
		String productSN=params.get("productSN");
		String queryType=params.get("queryType");
		String softName = params.get("softName");
		if (softName != null && !"".equals(softName)) {
			try {
				softName = new String(softName.getBytes("iso-8859-1"), "UTF-8");
//				softName = new String(softName.getBytes("US-ASCII"), "UTF-8");
//				softName = new String(softName.getBytes("ISO-8859"), "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
		}
		
//		SoftList softList = new SoftList();
//		softList.setProductSN(productSN);
//		softList.setSoftName(softName);
		
		SealerInfo info=(SealerInfo)SessionUtil.getLoginUserInfo(request);
		String languageCode=info.getLanguageCode();
		
		SoftListVo softListVo = new SoftListVo();
		softListVo.setProductSN(productSN);
		softListVo.setSoftName(softName);
		softListVo.setQueryType(queryType);
		softListVo.setLanguageCode(languageCode);
		
		List<SoftList> record;
		try {
//			record = softListService.getSoftList(softList, languageCode,queryType);
			record = softListService.getSoftList(softListVo);
			this.putData("record", record);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.putData("softName",softName);
	}
	
	//经销商根据序列号添加客诉信息20140211
	public void addComplaintInfo(Map<String, String> params,HttpServletRequest request){
		String userName = "";
		String company = "";
		String phone = "";
		String daytimePhoneCC = "";
		String daytimePhoneAC = "";
		String daytimePhone = "";
		String email = "";
		String code=params.get("code");
		String productSN=params.get("productSN");
		String productTypeName="";
		this.putData("code", code);
		this.putData("productSN", productSN);
		ProductInfo info = productInfoService.getBySerialNo2(productSN);
		if(info!=null){
			productTypeName=info.getProTypeName();
		}
		this.putData("productTypeName", productTypeName);
		CustomerInfo customerInfo=new CustomerInfo();
		try {
			customerInfo = customerInfoService.getCustomerByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}
		company = customerInfo.getCompany();
		userName = customerInfo.getAutelId();

		if (StringUtil.isEmpty(customerInfo.getMobilePhoneCC()))
		{
			daytimePhoneCC = "";
		}else{
			daytimePhoneCC=customerInfo.getMobilePhoneCC();
		}

		if (StringUtil.isEmpty(customerInfo.getMobilePhoneAC()))
		{
			daytimePhoneAC = "";
		}else{
			daytimePhoneAC=customerInfo.getMobilePhoneAC();
		}

		if (StringUtil.isEmpty(customerInfo.getMobilePhone()))
		{
			daytimePhone = "";
		}else{
			daytimePhone=customerInfo.getMobilePhone();
		}
		email = customerInfo.getAutelId();
			
		Vehicle vehicle=new Vehicle();
		vehicle.setQueryType("1");
		List<Vehicle> yearList=null;
		try {
			yearList = vehicleService.getVehicleList(vehicle);
			List<DataLogging> dataList = dataLogService.getDataLogList(productSN);
			this.putData("dataList", dataList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.putData("yearList",yearList);
		this.putData("userCode", code);
		this.putData("company", company);
		this.putData("userName", userName);

		phone = daytimePhoneCC + daytimePhoneAC + daytimePhone;

		this.putData("phone", phone);
		this.putData("email", email);
	}
	
	//经销商保存客诉信息20140211
//	@SuppressWarnings("unused")
//	private void addCustomerComplaintInfo(Map<String, String> params,
//			HttpServletRequest request)
//	{
//
//		try
//		{
//			String filePath = fileUpLoad(params);
//
//			// 上传文件大小超过4M或上传文件失败
//			// 上传文件大小超过4M或上传文件失败
//			if (filePath.equals(CUSTOMERCOMPLAINT_FILE_TYPE)
//					|| filePath.equals(CUSTOMERCOMPLAINT_FILE_SIZE)
//					|| filePath.equals(CUSTOMERCOMPLAINT_FAIL))
//			{
//				this.showJson(filePath);
//			} else
//			{
//				CustomerComplaintInfo newCustomerComplaintInfo = new CustomerComplaintInfo();
//				newCustomerComplaintInfo.setComplaintTypeCode(params
//						.get("complaintTypeCode"));
//				newCustomerComplaintInfo.setComplaintTitle(params
//						.get("complaintTitle"));
//				newCustomerComplaintInfo.setComplaintPerson(params
//						.get("complaintPerson"));
//				newCustomerComplaintInfo.setCompany(params.get("company"));
//				newCustomerComplaintInfo.setComplaintDate(DateUtil.toString(
//						new Date(), "yyyy-MM-dd HH:mm:ss"));
//				newCustomerComplaintInfo.setPhone(params.get("phone"));
//				newCustomerComplaintInfo.setEmail(params.get("email"));
//				newCustomerComplaintInfo.setVehicleSystem(params
//						.get("vehicleSystem"));
//				newCustomerComplaintInfo.setVehicleType(params
//						.get("vehicleType"));
//				newCustomerComplaintInfo.setDiagnosticConnector(params
//						.get("diagnosticConnector"));
//				newCustomerComplaintInfo.setCarYear(params.get("carYear"));
//				newCustomerComplaintInfo.setCarnumber(params.get("carnumber"));
//				newCustomerComplaintInfo
//						.setEngineType(params.get("engineType"));
//				newCustomerComplaintInfo.setSystem(params.get("system"));
//				newCustomerComplaintInfo.setOther(params.get("other"));
//				newCustomerComplaintInfo.setProductName(params
//						.get("productName"));
//				newCustomerComplaintInfo.setProductNo(params.get("productNo"));
//				newCustomerComplaintInfo.setSoftwareName(params
//						.get("softwareName"));
//				newCustomerComplaintInfo.setSoftwareEdition(params
//						.get("softwareEdition"));
//				newCustomerComplaintInfo.setTestPath(params.get("testPath"));
//				newCustomerComplaintInfo.setComplaintContent(params
//						.get("complaintContent"));
//				newCustomerComplaintInfo.setEmergencyLevel(Integer
//						.parseInt(params.get("emergencyLevel")));
//				newCustomerComplaintInfo
//						.setComplaintState(FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE);
//				newCustomerComplaintInfo
//						.setComplaintEffective(FrontConstant.COMPLAINT_YES_STATE);
//				newCustomerComplaintInfo.setUserType(FrontConstant.CUSTOMER_USER_TYPE); //个人客户
//				newCustomerComplaintInfo.setUserCode(params.get("userCode"));
//				newCustomerComplaintInfo.setAdapter(params.get("adapter"));
//				newCustomerComplaintInfo.setDatalogging(params.get("datalogging"));
//				newCustomerComplaintInfo.setSourceType(FrontConstant.COMPLAINT_SOURCE_SEALER); //客诉来源
//				// 没有上传附件处理
//				if (filePath.equals(CUSTOMERCOMPLAINT_FILE_TYPE)
//						|| filePath.equals(CUSTOMERCOMPLAINT_FILE_SIZE)
//						|| filePath.equals(CUSTOMERCOMPLAINT_FAIL)
//						|| filePath.equals(CUSTOMERCOMPLAINT_SUCCESS))
//				{
//					filePath = null;
//				}
//
//				newCustomerComplaintInfo.setAttachment(filePath);
//
//				SealerInfo info=(SealerInfo)SessionUtil.getLoginUserInfo(request);
//				
//				newCustomerComplaintInfo.setAcceptor(info.getAutelId());
//
//				customerComplaintInfoService.addCustomerComplaintInfo(newCustomerComplaintInfo);
//
//				String autelId = "";
//				String languageCode = "";
//				String userName="";
//				
//				CustomerInfo customerInfo = customerInfoService.getCustomerByCode(params.get("userCode"));
//				autelId = customerInfo.getAutelId();
//				languageCode = customerInfo.getLanguageCode();
//				if(customerInfo.getName()!=null){
//					userName=customerInfo.getName();
//				}
//
//				Language lanuage = languageService.getByCode(languageCode);
//				String status = FrontConstant.getComplaintStatus(
//						lanuage.getCountryCode(),
//						FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE);
//
//				Smtp smtp = smtpManager.getCurrentSmtp();
//
//				EmailModel emailModel = new EmailModel();
//				emailModel.getData().put("username", smtp.getUsername());// username
//																			// 为freemarker模版中的参数
//				emailModel.getData().put("password", smtp.getPassword());
//				emailModel.getData().put("host", smtp.getHost());
//
//				
//				emailModel.getData().put("userName", userName);
//				emailModel.getData().put("autelId", autelId);
//				emailModel.getData().put("ticketId",
//						newCustomerComplaintInfo.getCode());
//				emailModel.getData().put("subject",
//						newCustomerComplaintInfo.getComplaintTitle());
//
//				String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
//				String url = requestUrl
//						+ "/queryCustomerComplaintDetail.html?operationType=3&userType="
//						+ Integer.parseInt(params.get("userType")) + "&code="
//						+ newCustomerComplaintInfo.getCode();
//				emailModel.getData().put("url", url);
//
//				emailModel.getData().put("status", status);
//
//				emailModel.setTitle("[SUPPORT #" + newCustomerComplaintInfo.getCode() + "]: "
//						+ newCustomerComplaintInfo.getComplaintTitle());
//				emailModel.setTo(newCustomerComplaintInfo.getEmail());  //将发送邮件的地址改为客诉填写的地址
//
//				EmailTemplate template = emailTemplateService
//						.getUseTemplateByTypeAndLanguage(
//								CTConsatnt.EMAIL_TEMPLATE_TYEP_ADD_COMPLAINT_SUCCESS,
//								languageCode);
//				if (template != null)
//				{
//					emailModel.setTemplate(template.getCode() + ".html"); // 此处一定需要这样写
//				}
//				emailProducer.send(emailModel);
//
//				this.showJson(CUSTOMERCOMPLAINT_SUCCESS);
//			}
//		}
//		// {complaintType=异常处理, username=it@auteltech.net,
//		// ticketId=cuc201305051525460206, host=smtp.ym.163.com, subject=AAAABB,
//		// autelId=527690766@qq.com, password=leisure,
//		// url=http://localhost:8086/autelproweb/queryCustomerComplaintDetail.html?operationType=3&userType=1&code=cuc201305051525460206}
//		catch (Exception e)
//		{
//			this.showJson(CUSTOMERCOMPLAINT_FAIL);
//			e.printStackTrace();
//		}
//	}
	
	//经销商根据序列号查询客户基本信息20140211
	public void getCustomerBaseInfo(Map<String, String> params,HttpServletRequest request){
		code=params.get("code");
		productSN=params.get("productSN");
		this.putData("code",code);
		this.putData("productSN",productSN);
		try {
			//客户基本信息
			customerInfoEdit = customerInfoService.getCustomerByCode(code);
			setDataList(2,request,customerInfoEdit.getAutelId());
			this.putData("customerInfo", customerInfoEdit);
		
			//客户产品信息
			customerProductVoSel =customerProductService.getCustomerPro(productSN);
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date period =DateUtil.toDate(customerProductVoSel.getProRegTime(), "MM/dd/yyyy");
			int Warrantymonth=Integer.parseInt(customerProductVoSel.getWarrantymonth());
			
			// 前台用户注册产品时，如果出厂日期在当前日期的三个月以内，按注册日期开始算保修期，如果在三个月以外，按出厂日期加三个月算保修期。
			// 硬件保修期=保修开始日期+契约的硬件保修期（月）
			
			if(DateUtil.compareDate(DateUtil.toDate(customerProductVoSel.getProDate(), "MM/dd/yyyy"), DateUtil.toDate(customerProductVoSel.getProRegTime(), "MM/dd/yyyy"))){
				period.setMonth(period.getMonth()+Warrantymonth);
			}else{
				period=DateUtil.toDate(customerProductVoSel.getProDate(), "MM/dd/yyyy");
				period.setMonth(period.getMonth()+Warrantymonth+3);
			}
			customerProductVoSel.setEndDate(sdf.format(period));
			this.putData("customerProductVoSel", customerProductVoSel);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
//	
//	//根据序列号和客户Code查询客诉信息 20140211
	public void getCustomerComplaintInfo(Map<String, String> params,HttpServletRequest request){
		try{
		String code=params.get("code");
		String productSN=params.get("productSN");
		String complaintState=params.get("complaintState")==null?"-1":params.get("complaintState");
		String complaintCode=params.get("complaintCode");
		String complaintDate=params.get("complaintDate");
		customerComplaintInfoVoSel.setUserCode(code);
		customerComplaintInfoVoSel.setProductNo(productSN);
		customerComplaintInfoVoSel.setComplaintState(Integer.parseInt(complaintState));
		customerComplaintInfoVoSel.setCode(complaintCode);
		customerComplaintInfoVoSel.setComplaintDate(complaintDate);
		
		this.putData("productSN",productSN);
		this.putData("code",code);
		List<CustomerComplaintInfoVo> complaintList= customerComplaintInfoVoService.pageCustomerComplaintInfoPage(customerComplaintInfoVoSel);
		this.putData("complaintList", complaintList);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
//	
//	// 根据用户code查询产品信息
//		public void getProductInfo(Map<String, String> params,HttpServletRequest request){
//			try{
//			String pageSize = params.get("pageSize");
//			String pageNoStr = params.get("pageno");
//			Integer[] ids = this.parseId();
//			Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
//			String code=params.get("code");
//			customerProductVoSel= new CustomerProductVo();
//			customerProductVoSel.setCode(code);
//			Page dataPage = customerProductService.pageCustomerProductVoPage(
//					customerProductVoSel, pageNo, Integer.valueOf(pageSize));
//			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
//			String page_html=pagerHtmlBuilder.buildPageHtml();
//			long totalPageCount = dataPage.getTotalPageCount();
//			long totalCount = dataPage.getTotalCount();
//			this.putData("pager", page_html); 
//			this.putData("pagesize", pageSize);
//			this.putData("pageno", pageNo);
//			this.putData("totalcount", totalCount);
//			this.putData("totalpagecount",totalPageCount);
//			this.putData("productList", dataPage.getResult());
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//		    
//		}
//
//		public void getCustomerLoginInfo(Map<String, String> params,HttpServletRequest request){
//			try{
//			String pageSize = params.get("pageSize");
//			String pageNoStr = params.get("pageno");
//			Integer[] ids = this.parseId();
//			Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
//			String code=params.get("code");
//			customerLoginInfoVoSel.setCode(code);
//			Page dataPage = customerLoginInfoService.pageCustomerLoginInfoVoPage(
//					customerLoginInfoVoSel, pageNo, Integer.valueOf(pageSize));
//			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
//			String page_html=pagerHtmlBuilder.buildPageHtml();
//			long totalPageCount = dataPage.getTotalPageCount();
//			long totalCount = dataPage.getTotalCount();
//			this.putData("pager", page_html); 
//			this.putData("pagesize", pageSize);
//			this.putData("pageno", pageNo);
//			this.putData("totalcount", totalCount);
//			this.putData("totalpagecount",totalPageCount);
//			this.putData("loginList", dataPage.getResult());
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//		}

		/**
		 * 查看客诉表格详情
		 * @param request
		 */
		protected void queryCustomerComplaintDetails(HttpServletRequest request)
		{
			String code = request.getParameter("code");
			try 
			{
				CustomerComplaintInfo customerComplaintInfo = customerComplaintInfoService.
																	getCustomerComplaintInfoByCode(code);
				
				if(customerComplaintInfo == null)
				{
					return;
				}

				this.putData("customerComplaintInfo",customerComplaintInfo);
				this.putData("userType",request.getParameter("userType"));
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		
//		public void getCustomerConsume(Map<String, String> params,HttpServletRequest request){
//			try{
//			String code=params.get("code");
//			customerInfoVo.setCode(code);
//			String pageSize = params.get("pageSize");
//			String pageNoStr = params.get("pageno");
//			Integer[] ids = this.parseId();
//			Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
//			customerInfoVo.setConsumeType(-1);
//			Page dataPage = customerInfoVoService.queryCustomerOrderInfo(
//					customerInfoVo, pageNo, Integer.valueOf(pageSize));
//			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
//			String page_html=pagerHtmlBuilder.buildPageHtml();
//			long totalPageCount = dataPage.getTotalPageCount();
//			long totalCount = dataPage.getTotalCount();
//			this.putData("pager", page_html); 
//			this.putData("pagesize", pageSize);
//			this.putData("pageno", pageNo);
//			this.putData("totalcount", totalCount);
//			this.putData("totalpagecount",totalPageCount);
//			this.putData("consumeList", dataPage.getResult());
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//		}
//		
//		public void getCustomerComplaint(Map<String, String> params,HttpServletRequest request){
//			try{
//			String pageSize = params.get("pageSize");
//			String pageNoStr = params.get("pageno");
//			String code=params.get("code");
//			customerComplaintInfoVoSel.setUserCode(code);
//			Integer[] ids = this.parseId();
//			Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
//			Page dataPage = customerComplaintInfoVoService.pageCustomerComplaintInfoVoPage(customerComplaintInfoVoSel,pageNo,Integer.valueOf(pageSize));
//			//customerInfoEdit = customerInfoService.getCustomerByCode(code);
//		
//			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
//			String page_html=pagerHtmlBuilder.buildPageHtml();
//			long totalPageCount = dataPage.getTotalPageCount();
//			long totalCount = dataPage.getTotalCount();
//			this.putData("pager", page_html); 
//			this.putData("pagesize", pageSize);
//			this.putData("pageno", pageNo);
//			this.putData("totalcount", totalCount);
//			this.putData("code", code);
//			this.putData("complaintList", dataPage.getResult());
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//		}
		
	
		protected void queryCustomerComplaintForSealer(Map<String, String> params)
		{
			String code=params.get("code");
			String type=params.get("type");
			String userCode=params.get("userCode");
			String productSN=params.get("productSN");
			try 
			{
				this.putData("customerComplaintInfo",customerComplaintInfoService.getCustomerComplaintInfoByCode(code));
				this.putData("reCustomerComplaintInfoList", reCustomerComplaintInfoService.
						queryReCustomerComplaintInfoByCode(code));
                this.putData("code",userCode);
                this.putData("type",type);
                this.putData("userCode",userCode);
                this.putData("productSN",productSN);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		
//	protected void queryCustomerComplaintInfoPage(Map<String, String> params,HttpServletRequest request)
//	{
//		String pageSize = params.get("pageSize");
//		String pageNoStr = params.get("pageno");
//		Integer[] ids = this.parseId();
//		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
//		
//		try {
//			
//			Page dataPage = customerComplaintInfoVoService.pageCustomerComplaintInfoVoPage(customerComplaintInfoVoSel,1, Integer.valueOf(pageSize));
//			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
//			String page_html=pagerHtmlBuilder.buildPageHtml();
//			long totalPageCount = dataPage.getTotalPageCount();
//			long totalCount = dataPage.getTotalCount();
//			this.putData("pager", page_html); 
//			this.putData("pagesize", pageSize);
//			this.putData("pageno", pageNo);
//			this.putData("totalcount", totalCount);
//			this.putData("totalpagecount",totalPageCount);
//			this.putData("customerComplaintInfoList", dataPage.getResult());
//
//		} catch (Exception e) {
//				
//			e.printStackTrace();
//		}
//	}
//	
//	public void getProUpgrade(Map<String, String> params,HttpServletRequest request){
//		try {
//			String proCode=params.get("proCode");
//			proUpgradeRecordVoSel.setProCode(proCode);
//			Page dataPage = proUpgradeRecordService.pageProUpgradeRecordVoPage(proUpgradeRecordVoSel,1, 50);
//			this.putData("dataList", dataPage.getResult());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//	}
//	
//	public void getProSoftware(Map<String, String> params,HttpServletRequest request){
//		
//		String proCode=params.get("proCode");
//		customerSoftwareVoSel = customerSoftwareVoSel != null ? customerSoftwareVoSel : new CustomerSoftwareVo();
//		customerSoftwareVoSel.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
//		customerSoftwareVoSel.setProCode(proCode);
//		try {
//			Page dataPage = customerSoftwareService.pageCustomerSoftwareVoPage(customerSoftwareVoSel,1,50);
//			this.putData("dataList", dataPage.getResult());
//		} catch (Exception e) {
//			
//		}
//	}
//	
//	public void getProService(Map<String, String> params,HttpServletRequest request){
//		String proCode=params.get("proCode");
//		productRepairRecord.setProCode(proCode);
//		Page dataPage= productRepairRecordService.pageProductRepairRecord(productRepairRecord,1,50);
//		this.putData("dataList", dataPage.getResult());
//	}
	
	
	protected void goRegCtstomerPage(int operationType,HttpServletRequest request)
	{
		setDataList(operationType,request,null);
	}
	
	protected void setDataList(int operationType,HttpServletRequest request,String autelId)
	{
		Language language = null;
		if(operationType == 1)
		{
			language = languageService.getByLocale(CopContext.getDefaultLocal());
		}
		else 
		{
			try 
			{
				CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
				if(customerInfo !=null)
				{
					language = languageService.getByCode(customerInfo.getLanguageCode());
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		List<QuestionDesc> questionInfoList = new ArrayList<QuestionDesc>();
		
		if(language != null)
		{
			try 
			{
				questionInfoList = questionDescService.listQuestionDescByLanguageCode(language.getCode());
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		this.putData("languageList", languageService.queryLanguage());
		this.putData("questionInfoList",questionInfoList);
	}
	
	public void getCustomerInfo(Map<String, String> params,HttpServletRequest request){
		code=params.get("code");
		try {
		customerInfoEdit = customerInfoService.getCustomerByCode(code);
		setDataList(2,request,customerInfoEdit.getAutelId());
		this.putData("customerInfo", customerInfoEdit);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public void listCustomerProduct(Map<String, String> params,HttpServletRequest request) {

		try {
			String autelID=new String(params.get("autelID").getBytes( "iso-8859-1" ), "UTF-8" );
			String productSN=new String(params.get("productSN").getBytes( "iso-8859-1" ), "UTF-8" );
			String city=new String(params.get("city").getBytes( "iso-8859-1" ), "UTF-8" );
			String province=new String(params.get("province").getBytes( "iso-8859-1" ), "UTF-8" );
			

			customerProductVoSel= new CustomerProductVo();
        	customerProductVoSel.setAutelId(autelID);
        	customerProductVoSel.setProSerialNo(productSN);
        	customerProductVoSel.setCity(city);
        	customerProductVoSel.setProvince(province);
        	String sealerAutelId="";
        	Object object=SessionUtil.getLoginUserInfo(request);
        	String auth="";
        	if(object!=null && object.getClass().equals(SealerInfo.class)){
        		sealerAutelId=((SealerInfo)SessionUtil.getLoginUserInfo(request)).getAutelId();
        		auth=((SealerInfo)SessionUtil.getLoginUserInfo(request)).getAuth();
        	}
        	if(auth!=null){
        	String [] str=auth.split(",");
        	SealerDataAuthDeails sealerData = sealerDataDeailsService.getDataAuth(str[0]);
        	if(sealerData!=null){
        		sealerAutelId=sealerData.getSealerCode();
        	}
        	}
        	
        	customerProductVoSel.setSealerAutelId(sealerAutelId);
			Page dataPage = customerProductService.pageCustomerProductVoPage(customerProductVoSel, 1, 15);
		    this.putData("listCustomerProduct", dataPage.getResult());
		    long totalCount=dataPage.getTotalCount();
		    String productSerialNo="";
		    String customerCode="";
		    if(totalCount==1){
		    	List<CustomerProductVo> list=(List<CustomerProductVo>) dataPage.getResult();
		    	productSerialNo=list.get(0).getProSerialNo();
		    	customerCode=list.get(0).getCode();
		    }
		    this.putData("productSerialNo", productSerialNo);
		    this.putData("customerCode", customerCode);
		    this.putData("totalCount", totalCount);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

	private String fileUpLoad(Map<String, String> params)
	{

		// 先上传图片
		String faceField = "uploadFile";
		String subFolder = "complaintinfo";
		HttpServletRequest requestUpload = ThreadContextHolder.getHttpRequest();

		if (MultipartFormDataRequest.isMultipartFormData(requestUpload))
		{
			try
			{

				String encoding = CopSetting.ENCODING;

				if (StringUtil.isEmpty(encoding))
				{
					encoding = "UTF-8";
				}

				MultipartFormDataRequest mrequest = new MultipartFormDataRequest(
						requestUpload, null, 1000 * 1024 * 1024,
						MultipartFormDataRequest.COSPARSER, encoding);
				Enumeration<String> enumer = mrequest.getParameterNames();
				while (enumer.hasMoreElements())
				{
					String key = enumer.nextElement();
					String value = mrequest.getParameter(key);
					params.put(key, value);
				}

				requestUpload = new MultipartRequestWrapper(requestUpload,
						mrequest);
				ThreadContextHolder.setHttpRequest(requestUpload);

				Hashtable files = mrequest.getFiles();
				UploadFile file = (UploadFile) files.get(faceField);

				if (file.getInpuStream() != null)
				{
					String fileFileName = file.getFileName();

					// 判断文件类型
					String allowTYpe = "doc,docx,xls,xlsx,txt,rar,zip,gif,png,jpg,pdf,bmp,jpeg";
					if (!fileFileName.trim().equals("")
							&& fileFileName.length() > 0)
					{
						String ex = fileFileName.substring(
								fileFileName.lastIndexOf(".") + 1,
								fileFileName.length());
						if (allowTYpe.toString().indexOf(ex.toLowerCase()) < 0)
						{
							return CUSTOMERCOMPLAINT_FILE_TYPE;
						}
					}

					// 判断文件大小
					if (file.getFileSize() > 4000 * 1024)
					{
						return CUSTOMERCOMPLAINT_FILE_SIZE;
					}

					String fileName = null;
					String filePath = "";

					String ext = FileUtil.getFileExt(fileFileName);

					fileName = DateUtil.toString(new Date(), "yyyyMMddHHmmss")
							+ StringUtil.getRandStr(4) + "." + ext;

					filePath = CopSetting.IMG_SERVER_PATH
							+ CopContext.getContext().getContextPath()
							+ "/attachment/";

					if (subFolder != null)
					{
						filePath += subFolder + "/";
					}

					String path = "/attachment/"
							+ (subFolder == null ? "" : subFolder) + "/"
							+ fileName;

					filePath += fileName;
					FileUtil.createFile(file.getInpuStream(), filePath);
					return path;
				}
			} catch (Exception ex)
			{
				ex.printStackTrace();
				return CUSTOMERCOMPLAINT_FAIL;
			}
		}

		return CUSTOMERCOMPLAINT_SUCCESS;

	}
	
}
