package com.cheriscon.front.usercenter.distributor.component.widget;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.IAreaConfigService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;

/**
 * 经销商信息控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-14
 */
@Component("saleUserInfoWidget")
public class SaleUserInfoWidget  extends RequestParamWidget
{
	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private ISealerInfoService sealerInfoService;
	
	@Resource
	private ILanguageService languageService;
	
	@Resource
	private IAreaConfigService areaConfigService;
	
	@Override
	protected void display(Map<String, String> params) 
	{
		String operationType = params.get("operationType");
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		
		try
		{
			
			if(!StringUtil.isEmpty(operationType))
			{
				if(Integer.parseInt(operationType) == 1)//进入经销商信息维护页面
				{
					goUpdateSealerInfo(request);
				}
				/*else if(Integer.parseInt(operationType) == 2)//经销商信息维护提交操作
				{
					updateSealerInfo(request);
				}*/
				else if(Integer.parseInt(operationType) == 3)//进入经销商密码修改页面
				{
					this.putData("code",((SealerInfo) SessionUtil.getLoginUserInfo(request)).getCode());
					this.putData("autelId",((SealerInfo) SessionUtil.getLoginUserInfo(request)).getAutelId());
				}
				/*else if(Integer.parseInt(operationType) == 4)//经销商密码修改提交操作
				{
					updatePassword(request);
				}*/
				else if(Integer.parseInt(operationType) == 5)  //查看经销商区域列表
				{
					querySealerInfoList(request, params);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取语言list
	 */
	protected void setLanguageList()
	{
		this.putData("languageList", languageService.queryLanguage());
	}
	
	/**
	 * 获取区域配置list
	 */
	protected void setAreaConfigList()
	{
		try 
		{
			this.putData("areaConfigList",areaConfigService.listAll());
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 进入修改个人信息页面
	 * @param request
	 */
	protected void goUpdateSealerInfo(HttpServletRequest request)
	{
		setLanguageList();
		
		String code = ((SealerInfo) SessionUtil.getLoginUserInfo(request)).getCode();
		
		try 
		{
			this.putData("sealerInfo", sealerInfoService.getSealerByCode(code));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	protected void querySealerInfoList(HttpServletRequest request,Map<String, String> params) 
			throws UnsupportedEncodingException
	{
		setAreaConfigList();
		String areaCode = request.getParameter("areaCode");
		String country = request.getParameter("country");
		String pageSize = params.get("pageSize");
		
	
		
		
		try
		{
			String pageNoStr = params.get("pageno");
			Integer[] ids = this.parseId();
			Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
			Page page = sealerInfoService.querySealerInfoPage(areaCode, country, 
					pageNo, Integer.parseInt(pageSize));
			
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, 
														page.getTotalCount(), Integer.valueOf(pageSize));
			String page_html=pagerHtmlBuilder.buildPageHtml();
			long totalPageCount = page.getTotalPageCount();
			long totalCount = page.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			this.putData("sealerInfoList", page.getResult());
			this.putData("areaCode",areaCode);
			this.putData("country",country);
		} 
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 经销商个人信息修改
	 * @param request
	 */
	/*protected void updateSealerInfo(HttpServletRequest request)
	{
		try 
		{
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			
			SealerInfo newSealerInfo = sealerInfoService.getSealerByCode(sealerInfo.getCode());
					
			
			if(newSealerInfo != null)
			{
				newSealerInfo.setFirstName(request.getParameter("firstName"));
				newSealerInfo.setMiddleName(request.getParameter("middleName"));
				newSealerInfo.setLastName(request.getParameter("lastName"));
				newSealerInfo.setLanguageCode(request.getParameter("languageCode"));
				newSealerInfo.setAddress(request.getParameter("address"));
				newSealerInfo.setCountry(request.getParameter("country"));
				newSealerInfo.setCompany(request.getParameter("company"));
				newSealerInfo.setZipCode(request.getParameter("zipCode"));
				newSealerInfo.setEmail(request.getParameter("email"));
				newSealerInfo.setCity(request.getParameter("city"));
				newSealerInfo.setMobilePhone(request.getParameter("mobilePhone"));
				newSealerInfo.setMobilePhoneAC(request.getParameter("mobilePhoneAC"));
				newSealerInfo.setMobilePhoneCC(request.getParameter("mobilePhoneCC"));
				newSealerInfo.setDaytimePhone(request.getParameter("dayTimePhone"));
				newSealerInfo.setDaytimePhoneAC(request.getParameter("dayTimePhoneAC"));
				newSealerInfo.setDaytimePhoneCC(request.getParameter("dayTimePhoneCC"));
				
				if(!StringUtil.isEmpty(request.getParameter("isAllowSendEmail")))
				{
					newSealerInfo.setIsAllowSendEmail(Integer.parseInt(request.getParameter("isAllowSendEmail")));
				}
				
				sealerInfoService.updateSealer(newSealerInfo);
			}
		} 
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}*/
	
	/**
	 * 修改密码操作
	 * @param request
	 */
	/*protected void updatePassword(HttpServletRequest request)
	{
		String password = request.getParameter("userPwd");
		SealerInfo sealerInfo = new SealerInfo();
		sealerInfo.setCode(((SealerInfo) SessionUtil.getLoginUserInfo(request)).getCode());
		sealerInfo.setUserPwd(DesModule.getInstance().encrypt(password) );
		
		try 
		{
			sealerInfoService.updateSealer(sealerInfo);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	@Override
	protected void config(Map<String, String> params) 
	{
		
	}

}
