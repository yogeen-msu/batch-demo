//package com.cheriscon.front.usercenter.distributor.component.widget;
//
//import java.util.Map;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.stereotype.Component;
//
//import com.cheriscon.app.cms.component.widget.RequestParamWidget;
//import com.cheriscon.backstage.product.service.IProductTypeService;
//import com.cheriscon.common.model.SealerInfo;
//import com.cheriscon.common.utils.SessionUtil;
//import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
//import com.cheriscon.framework.util.StringUtil;
//
///**
// * 我的可卖产品(经销商)控制器实现类
// * @author yangpinggui
// * @version 创建时间：2013-1-5
// */
//@Component("myVendProductWidget")
//public class MyVendProductWidget extends RequestParamWidget {
//	@Resource
//	private IProductTypeService productTypeService;
//
//	@Override
//	protected void display(Map<String, String> params) {
//		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
//		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
//
//		if (sealerInfo == null) {
//			return;
//		}
//
//		String pageSize = params.get("pageSize");
//		String pageNoStr = params.get("pageno");
//		Integer[] ids = this.parseId();
//		Integer pageNo = StringUtil.isEmpty(pageNoStr) ? ids[0] : Integer.valueOf(pageNoStr);
//
//		try {
//
//			Page page = productTypeService.queryProductTypeList(sealerInfo.getCode(), pageNo, Integer.parseInt(pageSize));
//
//			StaticPagerHtmlBuilder pagerHtmlBuilder = new StaticPagerHtmlBuilder(pageNo, page.getTotalCount(), Integer.valueOf(pageSize));
//			String page_html = pagerHtmlBuilder.buildPageHtml();
//			long totalPageCount = page.getTotalPageCount();
//			long totalCount = page.getTotalCount();
//			this.putData("pager", page_html);
//			this.putData("pagesize", pageSize);
//			this.putData("pageno", pageNo);
//			this.putData("totalcount", totalCount);
//			this.putData("totalpagecount", totalPageCount);
//			this.putData("myVendProduct", page.getResult());
//			this.putData("sealerInfo", sealerInfo);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	@Override
//	protected void config(Map<String, String> params) {
//		// TODO Auto-generated method stub
//
//	}
//
//}
