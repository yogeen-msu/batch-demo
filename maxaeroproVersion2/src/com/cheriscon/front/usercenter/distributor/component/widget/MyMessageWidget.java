package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.content.service.IMessageTypeService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Message;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

/**
 * 我的消息(经销商/个人用户)控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-19
 */
@Component("MyMessageWidget")
public class MyMessageWidget extends RequestParamWidget {
	
	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private IMessageTypeService messageTypeService;

	@Resource
	private IMessageService messageService;

	@Override
	protected void display(Map<String, String> params) {

		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String operationType = params.get("operationType");
		String userType="";
		if(StringUtil.isEmpty(userType)){
			Object obj= SessionUtil.getLoginUserInfo(request);
			if(obj.getClass().equals(CustomerInfo.class)){
				userType="1";
			}
			if(obj.getClass().equals(SealerInfo.class)){
				userType="2";
			}
		}
		if (StringUtil.isEmpty(operationType)) {
			params.put("userType", userType);
			request.setAttribute("userType", userType);
			queryMyMessageList(request, params);
		} else {
			try {
				this.putData("messageInfo", messageService.getMessageByCode(params.get("code")));
				
				
				this.putData("code", params.get("code"));
				this.putData("userType", userType);
				this.putData("title",new String(params.get("title").getBytes( "iso-8859-1" ), "UTF-8" ));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@SuppressWarnings("unchecked")
	protected void queryMyMessageList(HttpServletRequest request, Map<String, String> params) {

		String userType = params.get("userType");
		String timeRange = request.getParameter("timeRange");
//
		String languageCode = "";
		String userCode = "";
		String userName = "";
//
		if (!StringUtil.isEmpty(userType)) {
			if (Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE) {
				languageCode = ((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getLanguageCode();
				userCode = ((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getCode();
				userName = ((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getName();
			} else if (Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE) {
				languageCode = ((SealerInfo) SessionUtil.getLoginUserInfo(request)).getLanguageCode();
				userCode = ((SealerInfo) SessionUtil.getLoginUserInfo(request)).getCode();
				userName = ((SealerInfo) SessionUtil.getLoginUserInfo(request)).getName();
			}
		}
		
		//默认查询最近3个月
		if (timeRange == null || timeRange.equals("")) {
			timeRange = String.valueOf(FrontConstant.LATELY_THREE_MONTH);
		}
//
		String pageSize = params.get("pageSize");
		String msgTypeCode = request.getParameter("msgTypeCode");

		Message message = new Message();
		message.setLanguageCode(languageCode);
		message.setMsgTypeCode(msgTypeCode);
		message.setIsDialog(FrontConstant.USER_MESSAGE_IS_ALL_DIALOG);

		if (!StringUtil.isEmpty(userType)) {
			message.setUserType(Integer.parseInt(userType));
		}

		message.setTimeRange(Integer.parseInt(timeRange));
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -3); //得到前3个月

		message.setCreatTime(DateUtil.toString(calendar.getTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));

		try {
			String pageNoStr = params.get("pageno");
			Integer[] ids = this.parseId();
			Integer pageNo = StringUtil.isEmpty(pageNoStr) ? ids[0] : Integer.valueOf(pageNoStr);

//			List<MessageType> messageTypeList = messageTypeService.queryMessageType(
//					message.getUserType(), Integer.parseInt(timeRange), languageCode,
//					message.getCreatTime());
//
//			MessageType messageType = null;
//			List<MessageType> newMessageTypeList = new ArrayList<MessageType>();
			int totalMessageTypeCount = 0;
//
//			if (messageTypeList.size() > 0) {
//				int messageCount = 0;
//
//				for (int i = 0; i < messageTypeList.size(); i++) {
//					messageType = new MessageType();
//					messageType.setCode(messageTypeList.get(i).getCode());
//					messageType.setName(messageTypeList.get(i).getName());
//
//					messageType.setMessageTypeCount(messageService.queryMessageCount(userCode,messageTypeList.get(i).getCode(), languageCode,
//							message.getUserType(), message.getIsDialog(), message.getTimeRange(), message.getCreatTime()));
//					totalMessageTypeCount += messageType.getMessageTypeCount();
//					messageCount++;
//					messageType.setMessageCount(messageCount);
//					newMessageTypeList.add(messageType);
//				}
//			} else // 主要处理根据时间类型查不到数据时也要展示消息类型数据
//			{
//
//				int messageCount = 0;
//
//				for (int i = 0; i < messageTypeService.queryMessageType().size(); i++) {
//					messageType = new MessageType();
//					messageType.setCode(messageTypeService.queryMessageType().get(i).getCode());
//					messageType.setName(messageTypeService.queryMessageType().get(i).getName());
//					messageType.setMessageTypeCount(0);
//					messageCount++;
//					messageType.setMessageCount(messageCount);
//					newMessageTypeList.add(messageType);
//				}
//			}

			
			Map<String, Object> resultMap = messageService.queryMyMessageList(userCode, pageNo, 
				Integer.parseInt(pageSize), languageCode, msgTypeCode == null ? "" : msgTypeCode,
						userType, timeRange);
			
			Page dataPage = (Page) resultMap.get("pageData");
			List<MessageType> newMessageTypeList = (List<MessageType>) resultMap.get("messageTypeList");
			Integer messageCount = (Integer) resultMap.get("messageCount");
			
			StaticPagerHtmlBuilder pagerHtmlBuilder = new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
			String page_html = pagerHtmlBuilder.buildPageHtml();
			long totalPageCount = dataPage.getTotalPageCount();
			long totalCount = dataPage.getTotalCount();
			this.putData("pager", page_html);
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount", totalPageCount);
			this.putData("message", message);
			this.putData("messageList", dataPage.getResult());
			this.putData("userType", userType);
			this.putData("timeRange", timeRange);
			this.putData("messageTypeList", newMessageTypeList);
			this.putData("totalMessageTypeCount", messageCount);
			this.putData("userCode", userCode);
			this.putData("userName", userName);
			this.putData("divValue", params.get("divValue"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

}
