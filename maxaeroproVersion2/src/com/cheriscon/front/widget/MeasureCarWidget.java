package com.cheriscon.front.widget;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.RequestUtils;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.RequestUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.product.model.ProductForSealer;
import com.cheriscon.product.service.ILanguagePackService;
import com.cheriscon.product.service.IProductForSealerService;
import com.cheriscon.product.service.IProductTypeService;

/**
 * 动态数据列表挂件<br/>
 * 数据分类和页数由地址栏获取
 */
@Component("measureCarWidget")
@Scope("prototype")
public class MeasureCarWidget extends AbstractWidget {
	
	@Resource private ILanguagePackService languagePackService;
	@Resource
	private IProductForSealerService productForSealerService;
	@Override
	public boolean cacheAble() {
		return false;
	}
	

	@Override
	protected void display(Map<String, String> params) {
		try {
			String pageSize = params.get("pagesize");
			pageSize = StringUtil.isEmpty(pageSize) ? "20" :pageSize;
			int pageNo = getPage();
			
			Map<String,Object> paramMap = new HashMap<String,Object>();
			
			HttpServletRequest request = ThreadContextHolder.getHttpRequest();
			String softName = request.getParameter("softName");
			paramMap.put("softName",softName);
			
			
			
			
			String showall = request.getParameter("showall");
			if("1".equals(showall)){	//展示所有
				pageSize = String.valueOf(Integer.MAX_VALUE);
			}else{
				this.putData(paramMap);
			}
		//	List<ProductType> productTypeList = productTypeService.listAll();
			List<ProductForSealer> productTypeList=productForSealerService.getProductForSealer();
			ProductForSealer t=new ProductForSealer();
			
			String proTypeCode = request.getParameter("proTypeCode");
			if(StringUtils.isNotEmpty(proTypeCode)){
				paramMap.put("proTypeCode",proTypeCode);
			}
			if(productTypeList!=null && productTypeList.size()>0){
				t=productTypeList.get(0);
			}
			if(StringUtils.isEmpty(proTypeCode)){
			  paramMap.put("proTypeCode",t.getCode());
			  proTypeCode=t.getCode();
			}
			
			Page webpage  = languagePackService.searchPage(paramMap,pageNo,Integer.valueOf(pageSize));
			
			
			/*Language language = (Language)CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
			Locale locale = null;
			 String filePath=FrontConstant.getProperValue("download.url.other") ;
			if (language != null) {
				locale = new Locale(language.getLanguageCode(),language.getCountryCode());
			} else {
				String languageCode =request.getLocale().getLanguage();
				if(languageCode.toLowerCase().equals("zh")){
					filePath=FrontConstant.getProperValue("download.url.china");
				}else{
				}
			}*/
			
			
			
			StaticPagerHtmlBuilder pagerHtmlBuilder = new StaticPagerHtmlBuilder( pageNo, webpage.getTotalCount(), Integer.valueOf(pageSize));
			String page_html = pagerHtmlBuilder.buildPageHtml();
			
			long totalPageCount =webpage.getTotalPageCount();
			long totalCount = webpage.getTotalCount();
			String ip = RequestUtils.getIpAddr(request);
		//	String flag=getCountry(ip);
			//this.putData("flag", flag);
			this.putData("dataList", webpage.getResult());
			this.putData("pager", page_html);
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			this.putData("productTypeList",productTypeList);
			this.putData("proTypeCode",proTypeCode);
			/*this.putData("filePath",filePath);*/
			//给页面设置当前类别名称
			StringBuffer navBar = new StringBuffer();
			navBar.append("<span style='color:#a2a2a2;'> > </span><span style='color:#000000;'>"+FreeMarkerPaser.getBundleValue("cms.menu.supported")+"</span>");
			this.putData("navbar", navBar.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		
	}
	
	public String getCountry(String ip){
		 String flag="SHOW";
		 URLConnection webUrlRequest;
		 String code="";
		 JSONObject jsonObj2=new JSONObject();
		 JSONObject jsonObj=new JSONObject();
		try{
			webUrlRequest = webUrlRequest("http://ip.taobao.com/service/getIpInfo.php?ip="+ip);
			webUrlRequest.setConnectTimeout(3000);
			webUrlRequest.setReadTimeout(3000);
			String jsonString = jsonString(webUrlRequest.getInputStream());
			jsonObj = JSONObject.fromObject(jsonString);
			code=jsonObj.getString("code");
		}catch(Exception ex){
			
		}
		if(code.equals("0")){
		    jsonObj2=jsonObj.getJSONObject("data");
		    String ipCountry=jsonObj2.getString("country");
		    if(ipCountry.equals("中国")){
		    	flag="NO";
		    }
		}
		return flag;
	}
	public  URLConnection webUrlRequest(String requestUrl) throws IOException {
		return new URL(requestUrl).openConnection();
	}
	
	public String jsonString(InputStream inputStream) throws IOException {
		// List<String> list = new ArrayList<String>();
		BufferedReader bufferReader = null;
		try {
			bufferReader = new BufferedReader(
					new InputStreamReader(inputStream));

			String readline;
			while ((readline = bufferReader.readLine()) != null) {
				if (readline.indexOf("code") != -1) {
					return readline.toString();
				}
			}
		} finally {
			inputStream.close();
			bufferReader.close();
		}
		return null;

	}
	
	protected int getPage(){
		try {
			HttpServletRequest httpRequest = ThreadContextHolder.getHttpRequest();
			String url = RequestUtil.getRequestUrl(httpRequest);
			String pattern = "/(.*)-(\\d+)-(\\d+).html(.*)";
			String page= null;
			Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
			Matcher m = p.matcher(url);
			if (m.find()) {
				page = m.replaceAll("$3");
				return Integer.parseInt(page);
			} 
		} catch (Exception e) {
		}
		return 1;
	}
}
