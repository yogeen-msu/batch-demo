package com.cheriscon.front.widget.common;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.cheriscon.cop.sdk.widget.AbstractWidget;
/**
 * 404
 * @date:2013-4-26
 * @author caozhiyong
 *
 */
@Component("notFindPageWidget") 
public class NotFindPageWidget extends AbstractWidget {

	@Override
	protected void display(Map<String, String> params) {
		
	}

	@Override
	protected void config(Map<String, String> params) {
		
	}

}
