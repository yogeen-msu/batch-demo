package com.cheriscon.front.widget.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.widget.RequestParamWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
//import com.cheriscon.common.model.SealerInfo;

/**
 * 
 * @author yangpinggui
 * @version 创建时间：2013-1-10
 */
@Component("headerTitleWidget")
public class HeaderTitleWidget extends RequestParamWidget
{


	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
//		
		String userType = (String) request.getSession().getAttribute(FrontConstant.SESSION_USER_TYPE_FLAG);
		String autelId = null;
		String country = null;
		String userName = "";
		
		if(!StringUtil.isEmpty(userType))
		{
			if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)
			{
				CustomerInfo customerInfo = ((CustomerInfo) (SessionUtil.getLoginUserInfo(request)));
				
				if(customerInfo != null)
				{
					autelId = customerInfo.getAutelId();
					country = customerInfo.getCountry();
					userName = customerInfo.getName();
				}
			}
			else if(Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)
			{
				SealerInfo sealerInfo = ((SealerInfo) (SessionUtil.getLoginUserInfo(request)));
				
				if(sealerInfo != null)
				{
					autelId = sealerInfo.getAutelId();
					country = sealerInfo.getCountry();
					userName = sealerInfo.getName();
				}
			}
		}
		Language language = (Language) CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
		Locale locale = null;
		if (language != null) {
//			locale = new Locale("lag201304221104540500", language.getCountryCode());
			locale = new Locale(language.getLanguageCode(), language.getCountryCode());
		} else {
			locale = CopContext.getDefaultLocal();
		}
        this.putData("locale",locale);
		
        this.putData("userName", userName);
		this.putData("userType", userType);
		this.putData("autelId", autelId);
		this.putData("country",country);
		InputStream in = FileUtil.getResourceAsStream("cop.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String freshdeskUrl = props.getProperty("freshdesk.notlogin.url");
		if (request.getSession().getAttribute("facebookAccount") != null) {
			this.putData("facebookSSOUrl", freshdeskUrl + "/sso/facebook");
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		
	}

}
