package com.cheriscon.front.widget.common;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cheriscon.app.base.core.model.SiteMenu;
import com.cheriscon.app.base.core.service.ISiteMenuManager;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.util.RequestUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

@Component("site_menu")
@Scope("prototype")
public class MenuWidget extends AbstractWidget {
	
	private ISiteMenuManager siteMenuManager;
	@Resource private ILanguageService languageService;
	
	protected void display(Map<String, String> params) {
		HttpServletRequest request = CopContext.getHttpRequest();
		HttpSession session = CopContext.getHttpRequest().getSession();
		
		Language language = (Language) CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
		Locale locale = null;
		if (language != null) {
			locale = new Locale(language.getLanguageCode(), language.getCountryCode());
		} else {
			locale = CopContext.getDefaultLocal();
		}

		List<SiteMenu> menuList = siteMenuManager.list(0,"0",languageService.getByLocale(locale));
		this.putData("menuList",menuList);

		Object menu_index = session.getAttribute("show_menu_index");
		int showMenuIndex = (menu_index == null) ? 1 : (Integer) menu_index;

		String url = RequestUtil.getRequestUrl(request);
		for (int i = 0; i < menuList.size(); i++) {
			SiteMenu siteMenu = menuList.get(i);
			if (url.indexOf(siteMenu.getUrl()) > -1) {
				showMenuIndex = i;
				break;
			}
		}
		
		if(url.equals("/") || url.indexOf("login.html") != -1){
			showMenuIndex = 1;
		}
		
		String mi = request.getParameter("m");
		if(StringUtils.isNotEmpty(mi)){
			showMenuIndex = Integer.parseInt(mi);
		}
		
		this.putData("showMenuIndex", showMenuIndex);
		//当前菜单信息
		session.setAttribute("show_menu_index", showMenuIndex);
		
		this.putData(FrontConstant.SESSION_USER_TYPE_FLAG, session.getAttribute(FrontConstant.SESSION_USER_TYPE_FLAG));
		String isDropDown = params.get("isDropDown");
		isDropDown = StringUtil.isEmpty(isDropDown) ? "off" : "on";
		this.putData("isDropDown", isDropDown);

		this.putData("proCount", session.getAttribute("proCount"));
		this.putData("updateFlag",session.getAttribute("updateFlag"));
	}

	protected void config(Map<String, String> params) {

		this.setPageName("menu_config");
	}

	public ISiteMenuManager getSiteMenuManager() {
		return siteMenuManager;
	}

	public void setSiteMenuManager(ISiteMenuManager siteMenuManager) {
		this.siteMenuManager = siteMenuManager;
	}

}
