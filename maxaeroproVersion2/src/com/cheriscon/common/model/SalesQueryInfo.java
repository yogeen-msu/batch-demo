package com.cheriscon.common.model;

/**
 * 售前咨询产品信息表
 * 
 * @author chenqichuan
 * 
 */
public class SalesQueryInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3331960210111233850L;
	private Integer id;
	private String code;
	private String createUser;
	private String createDate;
	private String customerName;
	private String address;
	private String email;
	private String telphone;
	private String proTypeCode;
	private String openRemark;
	private String status;

	private String serialNo;
	private String closeRemark;
	private String closeUser;
	private String closeDate;

	private String city;
	private String zipCode;
	private String firstName;
	private String lastName;

	/**
	 * 非表字段
	 * 
	 * */

	private String proTypeName;
	private String createUserName;

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getCreateUser() {
		return createUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getAddress() {
		return address;
	}

	public String getEmail() {
		return email;
	}

	public String getTelphone() {
		return telphone;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public String getOpenRemark() {
		return openRemark;
	}

	public String getStatus() {
		return status;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public String getCloseRemark() {
		return closeRemark;
	}

	public String getCloseUser() {
		return closeUser;
	}

	public String getCloseDate() {
		return closeDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public void setOpenRemark(String openRemark) {
		this.openRemark = openRemark;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public void setCloseRemark(String closeRemark) {
		this.closeRemark = closeRemark;
	}

	public void setCloseUser(String closeUser) {
		this.closeUser = closeUser;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}

	public String getCity() {
		return city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}