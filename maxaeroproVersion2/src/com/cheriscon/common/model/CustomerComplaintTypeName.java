package com.cheriscon.common.model;

import java.io.Serializable;

public class CustomerComplaintTypeName implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6495182439691876624L;
	
	//客诉类型名称id
	private Integer id;
	//客诉类型code
	private String complaintTypeCode;
	//语言类型code
	private String languageCode;
	//客诉类型名称
	private String name;
	
	private String languageName;					//语言类型名称



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getComplaintTypeCode() {
		return complaintTypeCode;
	}



	public void setComplaintTypeCode(String complaintTypeCode) {
		this.complaintTypeCode = complaintTypeCode;
	}



	public String getLanguageCode() {
		return languageCode;
	}



	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getLanguageName() {
		return languageName;
	}



	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	
	
	
}
