package com.cheriscon.common.model;

import java.io.Serializable;

public class CustomerComplaintInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -777717708809868709L;
	
	private Integer id; // ID
	private String code; // 客诉code
	private String complaintTypeCode; // 客诉分类code
	private String complaintTitle; // 客诉主题
	private String complaintPerson; // 客诉人
	private String company; // 单位
	private String complaintDate; // 投诉日期
	private String phone; // 电话
	private String email; // 邮箱
	private String vehicleSystem; // 车系
	private String vehicleType; // 车型
	private String diagnosticConnector; // 诊断接头
	private String carYear; // 年款
	private String carnumber; // 车辆识别码
	private String engineType; // 发动机类型
	private String system; // 系统
	private String other; // 其他
	private String productName; // 产品名称
	private String productNo; // 产品序列号
	private String softwareName; // 软件名称
	private String softwareEdition; // 软件版本
	private String testPath; // 测试路径
	private String complaintContent; // 客诉内容
	private Integer emergencyLevel; // 紧急状态
	private String attachment; // 附件
	private Integer complaintState; // 客诉状态
	private Integer complaintEffective; // 投诉是否有效
	private Integer userType; // 用户类型
	private String userCode; // 用户code
	private Integer acceptState; // 受理状态
	private String acceptor; // 受理人
	private Integer complaintStateCount; // 客诉状态对应条数
	private String lastReDate; // 最后回复时间
	private String lastRePerson; // 最后回复人
	private Integer timeouts; // 超时设定
	private String adapter; // 测试用诊断头
	private String datalogging; // 是否采数
	private String dataloggingId; // 是否采数ID
	private Integer questionType; // 问题类型
	private Integer fixFlag;// 是否解决问题
	private String closeDate;// 关闭日期
	private Integer hoursBetween; // 从客诉开始到现在日期间隔
	private String solutionContent;
	
	private Integer sourceType; //标记客诉来源，0为客户自己添加，1为经销商添加的电话客诉，2为平板添加的客诉
	
	private String languageCode;
	private String userName;
	private String autelId;
	private String frontUserType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getComplaintTypeCode() {
		return complaintTypeCode;
	}

	public void setComplaintTypeCode(String complaintTypeCode) {
		this.complaintTypeCode = complaintTypeCode;
	}

	public String getComplaintTitle() {
		return complaintTitle;
	}

	public void setComplaintTitle(String complaintTitle) {
		this.complaintTitle = complaintTitle;
	}

	public String getComplaintPerson() {
		return complaintPerson;
	}

	public void setComplaintPerson(String complaintPerson) {
		this.complaintPerson = complaintPerson;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getComplaintDate() {
		return complaintDate;
	}

	public void setComplaintDate(String complaintDate) {
		this.complaintDate = complaintDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVehicleSystem() {
		return vehicleSystem;
	}

	public void setVehicleSystem(String vehicleSystem) {
		this.vehicleSystem = vehicleSystem;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getDiagnosticConnector() {
		return diagnosticConnector;
	}

	public void setDiagnosticConnector(String diagnosticConnector) {
		this.diagnosticConnector = diagnosticConnector;
	}

	public String getCarYear() {
		return carYear;
	}

	public void setCarYear(String carYear) {
		this.carYear = carYear;
	}

	public String getCarnumber() {
		return carnumber;
	}

	public void setCarnumber(String carnumber) {
		this.carnumber = carnumber;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getSoftwareName() {
		return softwareName;
	}

	public void setSoftwareName(String softwareName) {
		this.softwareName = softwareName;
	}

	public String getSoftwareEdition() {
		return softwareEdition;
	}

	public void setSoftwareEdition(String softwareEdition) {
		this.softwareEdition = softwareEdition;
	}

	public String getTestPath() {
		return testPath;
	}

	public void setTestPath(String testPath) {
		this.testPath = testPath;
	}

	public String getComplaintContent() {
		return complaintContent;
	}

	public void setComplaintContent(String complaintContent) {
		this.complaintContent = complaintContent;
	}

	public Integer getEmergencyLevel() {
		return emergencyLevel;
	}

	public void setEmergencyLevel(Integer emergencyLevel) {
		this.emergencyLevel = emergencyLevel;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public Integer getComplaintState() {
		return complaintState;
	}

	public void setComplaintState(Integer complaintState) {
		this.complaintState = complaintState;
	}

	public Integer getComplaintEffective() {
		return complaintEffective;
	}

	public void setComplaintEffective(Integer complaintEffective) {
		this.complaintEffective = complaintEffective;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Integer getAcceptState() {
		return acceptState;
	}

	public void setAcceptState(Integer acceptState) {
		this.acceptState = acceptState;
	}

	public String getAcceptor() {
		return acceptor;
	}

	public void setAcceptor(String acceptor) {
		this.acceptor = acceptor;
	}

	public Integer getComplaintStateCount() {
		return complaintStateCount;
	}

	public void setComplaintStateCount(Integer complaintStateCount) {
		this.complaintStateCount = complaintStateCount;
	}

	public String getLastReDate() {
		return lastReDate;
	}

	public void setLastReDate(String lastReDate) {
		this.lastReDate = lastReDate;
	}

	public String getLastRePerson() {
		return lastRePerson;
	}

	public void setLastRePerson(String lastRePerson) {
		this.lastRePerson = lastRePerson;
	}

	public Integer getTimeouts() {
		return timeouts;
	}

	public void setTimeouts(Integer timeouts) {
		this.timeouts = timeouts;
	}

	public String getAdapter() {
		return adapter;
	}

	public void setAdapter(String adapter) {
		this.adapter = adapter;
	}

	public String getDatalogging() {
		return datalogging;
	}

	public void setDatalogging(String datalogging) {
		this.datalogging = datalogging;
	}

	public String getDataloggingId() {
		return dataloggingId;
	}

	public void setDataloggingId(String dataloggingId) {
		this.dataloggingId = dataloggingId;
	}

	public Integer getQuestionType() {
		return questionType;
	}

	public void setQuestionType(Integer questionType) {
		this.questionType = questionType;
	}

	public Integer getFixFlag() {
		return fixFlag;
	}

	public void setFixFlag(Integer fixFlag) {
		this.fixFlag = fixFlag;
	}

	public String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}

	public Integer getHoursBetween() {
		return hoursBetween;
	}

	public void setHoursBetween(Integer hoursBetween) {
		this.hoursBetween = hoursBetween;
	}

	public String getSolutionContent() {
		return solutionContent;
	}

	public void setSolutionContent(String solutionContent) {
		this.solutionContent = solutionContent;
	}

	public Integer getSourceType() {
		return sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getFrontUserType() {
		return frontUserType;
	}

	public void setFrontUserType(String frontUserType) {
		this.frontUserType = frontUserType;
	}

	
}
