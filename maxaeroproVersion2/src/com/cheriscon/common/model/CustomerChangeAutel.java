package com.cheriscon.common.model;

import java.io.Serializable;

public class CustomerChangeAutel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -134250723297681145L;
	

	private Integer id;		
	
	private String code;	
	
	private String customerCode; //老用户code
	
	private String newAutelId; //新AutelID
	
	private int actState;   //更改autelID激活状态
	
	private int oldActState; //老autelID激活状态
	
	private String oldAutelId; //老autelID

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getNewAutelId() {
		return newAutelId;
	}

	public void setNewAutelId(String newAutelId) {
		this.newAutelId = newAutelId;
	}

	public int getActState() {
		return actState;
	}

	public void setActState(int actState) {
		this.actState = actState;
	}

	public int getOldActState() {
		return oldActState;
	}

	public void setOldActState(int oldActState) {
		this.oldActState = oldActState;
	}

	public String getOldAutelId() {
		return oldAutelId;
	}

	public void setOldAutelId(String oldAutelId) {
		this.oldAutelId = oldAutelId;
	}
	
	
}
