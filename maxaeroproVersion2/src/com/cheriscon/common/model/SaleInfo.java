package com.cheriscon.common.model;

import java.io.Serializable;




/**
 * 经销商销售信息实体类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public class SaleInfo implements Serializable
{	 
		private static final long serialVersionUID = -6185073129338515792L;
		
		private Integer id;				//ID
		private String code;			//销售信息code
		private String proName;			//产品名称（和产品型号不同）
		private String proExpand;		//产品规格
		private String company;			//单位
		private Integer salesNum;		//销货数量
		private Long salesMoney;		//销货金额
		private Integer pinBackNum;		//销退数量
		private Long pinBackMoney;		//销退金额
		private Integer netWeight;		//销货净量
		private Long netSales;			//销货净额
		private String sealerName;		//经销商名称（就是经销商ID、登录帐号）


	    // Constructors

	    /** default constructor */
	    public SaleInfo() {
	    }

	    
	    /** full constructor */
	    public SaleInfo(String code, String proName, String proExpand, String company, Integer salesNum, Long salesMoney, Integer pinBackNum, Long pinBackMoney, Integer netWeight, Long netSales, String sealerName) {
	        this.code = code;
	        this.proName = proName;
	        this.proExpand = proExpand;
	        this.company = company;
	        this.salesNum = salesNum;
	        this.salesMoney = salesMoney;
	        this.pinBackNum = pinBackNum;
	        this.pinBackMoney = pinBackMoney;
	        this.netWeight = netWeight;
	        this.netSales = netSales;
	        this.sealerName = sealerName;
	    }


	    public Integer getId() {
	        return this.id;
	    }
	    
	    public void setId(Integer id) {
	        this.id = id;
	    }

	    public String getCode() {
	        return this.code;
	    }
	    
	    public void setCode(String code) {
	        this.code = code;
	    }

	    public String getProName() {
	        return this.proName;
	    }
	    
	    public void setProName(String proName) {
	        this.proName = proName;
	    }

	    public String getProExpand() {
	        return this.proExpand;
	    }
	    
	    public void setProExpand(String proExpand) {
	        this.proExpand = proExpand;
	    }

	    public String getCompany() {
	        return this.company;
	    }
	    
	    public void setCompany(String company) {
	        this.company = company;
	    }

	    public Integer getSalesNum() {
	        return this.salesNum;
	    }
	    
	    public void setSalesNum(Integer salesNum) {
	        this.salesNum = salesNum;
	    }

	    public Long getSalesMoney() {
	        return this.salesMoney;
	    }
	    
	    public void setSalesMoney(Long salesMoney) {
	        this.salesMoney = salesMoney;
	    }

	    public Integer getPinBackNum() {
	        return this.pinBackNum;
	    }
	    
	    public void setPinBackNum(Integer pinBackNum) {
	        this.pinBackNum = pinBackNum;
	    }

	    public Long getPinBackMoney() {
	        return this.pinBackMoney;
	    }
	    
	    public void setPinBackMoney(Long pinBackMoney) {
	        this.pinBackMoney = pinBackMoney;
	    }

	    public Integer getNetWeight() {
	        return this.netWeight;
	    }
	    
	    public void setNetWeight(Integer netWeight) {
	        this.netWeight = netWeight;
	    }

	    public Long getNetSales() {
	        return this.netSales;
	    }
	    
	    public void setNetSales(Long netSales) {
	        this.netSales = netSales;
	    }

	    public String getSealerName() {
	        return this.sealerName;
	    }
	    
	    public void setSealerName(String sealerName) {
	        this.sealerName = sealerName;
	    }



}