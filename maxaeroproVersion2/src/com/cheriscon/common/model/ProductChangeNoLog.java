package com.cheriscon.common.model;

public class ProductChangeNoLog implements java.io.Serializable{
	
	/**
	 * 经销商更换产品序列号日志
	 */
	private static final long serialVersionUID = -8132563375023182117L;

	private Integer id ; 
	
	private String code;
	
	private String customerCode;
	
	private String oldProCode; //老产品code
	
	private String newProCode; //新产品code
	
	private String validDate; //软件有效期
	
	private String regDate; //注册日期，主要是为了计算保修期
	
	private String createDate;
	
	private String createUser;
	
	private String createIp;
	
	
	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public String getOldProCode() {
		return oldProCode;
	}

	public String getNewProCode() {
		return newProCode;
	}

	public void setOldProCode(String oldProCode) {
		this.oldProCode = oldProCode;
	}

	public void setNewProCode(String newProCode) {
		this.newProCode = newProCode;
	}
	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	

	public String getValidDate() {
		return validDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public String getCreateIp() {
		return createIp;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}


	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public void setCreateIp(String createIp) {
		this.createIp = createIp;
	}
	

}
