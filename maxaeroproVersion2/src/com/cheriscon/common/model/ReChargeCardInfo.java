package com.cheriscon.common.model;


/**
 * 充值卡信息实体类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */

public  class ReChargeCardInfo  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 2252057594527154475L;
	

	private Integer id;						//ID
	private String code;					//Code
	private String reChargeCardTypeCode;	//充值卡类型code
	private String serialNo;				//充值卡序列号
	private String reChargePwd;				//充值卡密码
	private String validDate;				//充值卡有效期
	private Integer isUse;					//充值卡是否已使用（1：未使用  2：已使用）
	private Integer isActive;				//充值卡是否已激活（1：未激活  2：已激活）
	

     
 	

 	/**  非充值卡信息表字段  begin */
 	
 	private String reChargeCardTypeName;	//充值卡类型名称
 	private String beginDate;				//开始时间
 	private String endDate;					//结束时间
 	private Integer isOverdue;				//是否过期（1：未过期  2：已过期）
 	private int isUseTotalNum;  //未使用的升级卡总和
 	private int usedTotalNum;   //已经使用的升级卡总和
 	private int totalNum;
 	private String sealerAutelId ;  // 经销商编码 20150323
 	

	/**  非充值卡信息表字段  end */
 	



    // Constructors
  	public int getTotalNum() {
 		return totalNum;
 	}

 	public String getSealerAutelId() {
		return sealerAutelId;
	}

	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}

	public void setTotalNum(int totalNum) {
 		this.totalNum = totalNum;
 	}
    public int getIsUseTotalNum() {
		return isUseTotalNum;
	}

	public int getUsedTotalNum() {
		return usedTotalNum;
	}


	public void setIsUseTotalNum(int isUseTotalNum) {
		this.isUseTotalNum = isUseTotalNum;
	}


	public void setUsedTotalNum(int usedTotalNum) {
		this.usedTotalNum = usedTotalNum;
	}


	/** default constructor */
    public ReChargeCardInfo() {
    }

    
    /** full constructor */
    public ReChargeCardInfo(String reChargeCardTypeCode, String serialNo, String reChargePwd, String validDate) {
        this.reChargeCardTypeCode = reChargeCardTypeCode;
        this.serialNo = serialNo;
        this.reChargePwd = reChargePwd;
        this.validDate = validDate;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

 

    public String getSerialNo() {
        return this.serialNo;
    }
    
    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getReChargePwd() {
        return this.reChargePwd;
    }
    
    public void setReChargePwd(String reChargePwd) {
        this.reChargePwd = reChargePwd;
    }

    public String getValidDate() {
        return this.validDate;
    }
    
    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }
   
    public String getReChargeCardTypeCode() {
		return reChargeCardTypeCode;
	}


	public void setReChargeCardTypeCode(String reChargeCardTypeCode) {
		this.reChargeCardTypeCode = reChargeCardTypeCode;
	}


	public Integer getIsUse() {
		return isUse;
	}


	public void setIsUse(Integer isUse) {
		this.isUse = isUse;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public Integer getIsActive() {
		return isActive;
	}


	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}


     
 	

	/**  非充值卡信息表字段  begin */

	public String getReChargeCardTypeName() {
		return reChargeCardTypeName;
	}


	public void setReChargeCardTypeName(String reChargeCardTypeName) {
		this.reChargeCardTypeName = reChargeCardTypeName;
	}


	public String getBeginDate() {
		return beginDate;
	}


	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public Integer getIsOverdue() {
		return isOverdue;
	}


	public void setIsOverdue(Integer isOverdue) {
		this.isOverdue = isOverdue;
	}
	
	/**  非充值卡信息表字段  end */
	
	
	
}