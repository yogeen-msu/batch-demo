package com.cheriscon.common.model;

import java.io.Serializable;


public class SealerDataAuthDeails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4773209277013270205L;
	private String sealerCode;
	private String authCode;
	private String sealerName;
	private Integer openNum;
	private Integer closeNum;

	public Integer getOpenNum() {
		return openNum;
	}

	public Integer getCloseNum() {
		return closeNum;
	}

	public void setOpenNum(Integer openNum) {
		this.openNum = openNum;
	}

	public void setCloseNum(Integer closeNum) {
		this.closeNum = closeNum;
	}

	public String getSealerName() {
		return sealerName;
	}

	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

}
