package com.cheriscon.common.model;



public class ReChargeCardSupport implements java.io.Serializable {

	/**
	 * 充值卡充值使用错误支持类
	 */
	private static final long serialVersionUID = 3542829465383782076L;
	
	private Integer id;
	
	private String code;   //编码
	
	private String customerCode; //用户code
	
	private String createTime;   //生成时间
	
	private Integer status;    //是否处理 0 未处理 1 已处理
	
	private String updateUser;  //处理人
	
	private String updateTime;  //处理时间
	
    private String productSN; //产品序列号
    
    private String email;  //电子邮件
    
    private String cardSN; //卡序列号
    
    private String cardPwd; //卡密码
    
    private String userName; //用户姓名
    
    private String ip;//请求IP
    
    private String autelId; //因为这张表记录了客户和经销商充值支持，方便统计，加入autelID

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public String getCreateTime() {
		return createTime;
	}

	public Integer getStatus() {
		return status;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public String getProductSN() {
		return productSN;
	}

	public String getEmail() {
		return email;
	}

	public String getCardSN() {
		return cardSN;
	}

	public String getCardPwd() {
		return cardPwd;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCardSN(String cardSN) {
		this.cardSN = cardSN;
	}

	public void setCardPwd(String cardPwd) {
		this.cardPwd = cardPwd;
	}
	
	

}
