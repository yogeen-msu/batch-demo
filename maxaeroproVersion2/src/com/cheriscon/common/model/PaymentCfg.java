package com.cheriscon.common.model;

/**
 * 支付配置
 * 
 * @author yinhb
 * 
 */
public class PaymentCfg implements java.io.Serializable {

	// Fields
	private Integer id;
	private String name;
	private String config;
	private String biref;
	private Double payFee;
	private String type;


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConfig() {
		return this.config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getBiref() {
		return this.biref;
	}

	public void setBiref(String biref) {
		this.biref = biref;
	}

	public Double getPayFee() {
		return this.payFee;
	}

	public void setPayFee(Double payFee) {
		this.payFee = payFee;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}