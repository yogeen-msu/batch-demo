package com.cheriscon.common.model;

import java.io.Serializable;

public class UserMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3280116218594348588L;
	
	private Integer id;
	private String code;
	private Integer userType; // 用户类型 1：个人用户 2：经销商用户
	private String userCode; // 用户code
	private String messageCode; // 消息code
	private Integer messageIsRead; // 用户消息是否已读

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public Integer getMessageIsRead() {
		return messageIsRead;
	}

	public void setMessageIsRead(Integer messageIsRead) {
		this.messageIsRead = messageIsRead;
	}

}
