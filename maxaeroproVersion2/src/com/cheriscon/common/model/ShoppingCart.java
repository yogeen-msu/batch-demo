package com.cheriscon.common.model;

import java.io.Serializable;

/**
 * 我的购物车
 * 
 * @author caozhiyong
 * @version 2013-1-15
 */
public class ShoppingCart implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String customerCode;// 客户code
	private String minSaleUnitCode;// 最小销售单位code
	private String areaCfgCode;// 区域配置code
	private String proSerial;// 产品序列号
	private String proName;// 产品名称
	private String proCode;// 产品code
	private Integer year;// 续租年限
	private Integer type;// 消费类型
	private String picPath;//产品图片路径
	private int isSoft;
	private String cfgPrice;//销售配置原价格
	private String validData;
	
	public ShoppingCart() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public ShoppingCart(Integer id, String customerCode,
			String minSaleUnitCode, String areaCfgCode, String proSerial,
			String proName, String proCode, Integer year, Integer type,
			String picPath, int isSoft, String cfgPrice, String validData) {
		super();
		this.id = id;
		this.customerCode = customerCode;
		this.minSaleUnitCode = minSaleUnitCode;
		this.areaCfgCode = areaCfgCode;
		this.proSerial = proSerial;
		this.proName = proName;
		this.proCode = proCode;
		this.year = year;
		this.type = type;
		this.picPath = picPath;
		this.isSoft = isSoft;
		this.cfgPrice = cfgPrice;
		this.validData = validData;
	}



	public String getCfgPrice() {
		return cfgPrice;
	}



	public void setCfgPrice(String cfgPrice) {
		this.cfgPrice = cfgPrice;
	}



	public String getValidData() {
		return validData;
	}



	public void setValidData(String validData) {
		this.validData = validData;
	}



	public int getIsSoft() {
		return isSoft;
	}

	public void setIsSoft(int isSoft) {
		this.isSoft = isSoft;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public String getProSerial() {
		return proSerial;
	}

	public void setProSerial(String proSerial) {
		this.proSerial = proSerial;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
