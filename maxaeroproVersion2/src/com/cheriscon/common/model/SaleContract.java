package com.cheriscon.common.model;


/**
 * 销售契约
 * 
 * @author yinhb
 * 
 */
public class SaleContract implements java.io.Serializable {
	
	private static final long serialVersionUID = -2571477865865517706L;
	
	private Integer id;
	private String code;
	private String name;
	private String sealerCode;					//经销商编号 
	private String sealerAutelId;               //经销商autel ID
	private String languageName;                //语言配置名称

	public String getSealerAutelId() {
		return sealerAutelId;
	}
	public String getLanguageName() {
		return languageName;
	}

	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	/**
	 * 经销商类型code
	 */
	public String sealerTypeCode;
	
	/**
	 * 产品型号code
	 */
	public String proTypeCode;
	
	/**
	 * 续费价格
	 */
	private String reChargePrice;
	
	/**
	 * 区域配置code
	 */
	private String areaCfgCode;
	
	/**
	 * 语言配置code
	 */
	private String languageCfgCode;
	
	/**
	 * 标准销售配置code
	 */
	private String saleCfgCode;
	
	/**
	 * 最大销售配置
	 */
	private String maxSaleCfgCode;
	
	/**
	 * 免费升级月
	 */
	private Integer upMonth ;
	
	/**
	 * 契约时间
	 */
	private String contractDate;
	
	/**
	 * 过期时间
	 */
	private String expirationTime;
	
	
	/**
	 * 保修月份
	 */
	private Integer warrantyMonth;
	
	
	/**
	 * 创建者
	 */
	private String createUser;
	/**
	 * 创建IP
	 */
	private String createIP;
	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * 最后更新人
	 */
	private String updateLastUser;
	/**
	 * 更新时间
	 */
	private String updateTime;
	
	
	public String getCreateUser() {
		return createUser;
	}
	public String getCreateIP() {
		return createIP;
	}
	public String getCreateTime() {
		return createTime;
	}
	public String getUpdateLastUser() {
		return updateLastUser;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public void setCreateIP(String createIP) {
		this.createIP = createIP;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public void setUpdateLastUser(String updateLastUser) {
		this.updateLastUser = updateLastUser;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getWarrantyMonth() {
		return warrantyMonth;
	}

	public void setWarrantyMonth(Integer warrantyMonth) {
		this.warrantyMonth = warrantyMonth;
	}

	public SaleContract() {
		super();
	}

	/**
	 * @param id
	 * @param code
	 * @param name
	 * @param sealerCode
	 * @param sealerTypeCode
	 * @param proTypeCode
	 * @param reChargePrice
	 * @param areaCfgCode
	 * @param languageCfgCode
	 * @param saleCfgCode
	 * @param maxSaleCfgCode
	 * @param upMonth
	 * @param contractDate
	 * @param expirationTime
	 */
	public SaleContract(Integer id, String code, String name, String sealerCode, String sealerTypeCode, String proTypeCode, String reChargePrice,
			String areaCfgCode, String languageCfgCode, String saleCfgCode, String maxSaleCfgCode, Integer upMonth, String contractDate,
			String expirationTime) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.sealerCode = sealerCode;
		this.sealerTypeCode = sealerTypeCode;
		this.proTypeCode = proTypeCode;
		this.reChargePrice = reChargePrice;
		this.areaCfgCode = areaCfgCode;
		this.languageCfgCode = languageCfgCode;
		this.saleCfgCode = saleCfgCode;
		this.maxSaleCfgCode = maxSaleCfgCode;
		this.upMonth = upMonth;
		this.contractDate = contractDate;
		this.expirationTime = expirationTime;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public String getSealerTypeCode() {
		return sealerTypeCode;
	}

	public void setSealerTypeCode(String sealerTypeCode) {
		this.sealerTypeCode = sealerTypeCode;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getReChargePrice() {
		return reChargePrice;
	}

	public void setReChargePrice(String reChargePrice) {
		this.reChargePrice = reChargePrice;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public String getLanguageCfgCode() {
		return languageCfgCode;
	}

	public void setLanguageCfgCode(String languageCfgCode) {
		this.languageCfgCode = languageCfgCode;
	}

	public String getSaleCfgCode() {
		return saleCfgCode;
	}

	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}

	public String getMaxSaleCfgCode() {
		return maxSaleCfgCode;
	}

	public void setMaxSaleCfgCode(String maxSaleCfgCode) {
		this.maxSaleCfgCode = maxSaleCfgCode;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	/**
	 * @return the upMonth
	 */
	public Integer getUpMonth() {
		return upMonth;
	}

	/**
	 * @param upMonth the upMonth to set
	 */
	public void setUpMonth(Integer upMonth) {
		this.upMonth = upMonth;
	}

}