package com.cheriscon.common.model;

import java.io.Serializable;

/**
 * 给客户续费信息实体类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public class ToCustomerCharge implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5624138997326676008L;
	
	private String code;
	private String reChargePrice;
	private String productName;
	private String serialNo;
	private String softwareName;
	private String versionName;
	private String validDate;
	private String customerName;
	private String proCode;//产品编码
	private String proTypeCode;//产品型号code
	private String areaCfgCode;//区域配置Code
	private String saleCfgCode;//标准销售配置Code
	private String saleCfgName;//标准销售单位名称
	private String autelId;
	private String languageCode;
	private String proTypeName;//产品型号名称
	private String regStatus; //产品注册状态
	private String regDate;//产品注册日期
	private String regPwd; //产品注册密码
	private String customerCode; //客户编码
	private String orderNo; //订单号
	private Integer remarkId;//备注主键
	public Integer getRemarkId() {
		return remarkId;
	}
	public void setRemarkId(Integer remarkId) {
		this.remarkId = remarkId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getRegPwd() {
		return regPwd;
	}
	public void setRegPwd(String regPwd) {
		this.regPwd = regPwd;
	}
	public String getProTypeName() {
		return proTypeName;
	}
	public String getRegStatus() {
		return regStatus;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}
	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	private String languageName;
	
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getAutelId() {
		return autelId;
	}
	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getReChargePrice() {
		return reChargePrice;
	}
	public void setReChargePrice(String reChargePrice) {
		this.reChargePrice = reChargePrice;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getSoftwareName() {
		return softwareName;
	}
	public void setSoftwareName(String softwareName) {
		this.softwareName = softwareName;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getValidDate() {
		return validDate;
	}
	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public String getProTypeCode() {
		return proTypeCode;
	}
	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}
	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}
	public String getSaleCfgCode() {
		return saleCfgCode;
	}
	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}
	public String getSaleCfgName() {
		return saleCfgName;
	}
	public void setSaleCfgName(String saleCfgName) {
		this.saleCfgName = saleCfgName;
	}
	
	
	
	

}
