package com.cheriscon.common.model;


public class ProductAssociatesDate implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6914689810784467084L;
	
	private int id;  //自动增长id
	
	
	private String proCode; //产品code
	
	private String associatesDate; //关联时期
	
	
	public String getProCode() {
		return proCode;
	}


	public String getAssociatesDate() {
		return associatesDate;
	}


	public void setId(int id) {
		this.id = id;
	}


	public void setProCode(String proCode) {
		this.proCode = proCode;
	}


	public void setAssociatesDate(String associatesDate) {
		this.associatesDate = associatesDate;
	}


	public int getId() {
		return id;
	}

	
}
