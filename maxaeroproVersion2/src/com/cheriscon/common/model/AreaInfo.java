package com.cheriscon.common.model;


/**
 * 区域配置
 * 
 * @author yinhb
 * 
 */
public class AreaInfo implements java.io.Serializable {

	private static final long serialVersionUID = 1315502889819338892L;
	
	// Fields
	private Integer id;
	private String code;			//编号
	private String name;			//名称
	private Integer continent;		//洲

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getContinent() {
		return this.continent;
	}

	public void setContinent(Integer continent) {
		this.continent = continent;
	}
}