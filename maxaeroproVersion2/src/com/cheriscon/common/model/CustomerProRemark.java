package com.cheriscon.common.model;


/**
 * 经销商在产品续租界面备注信息
 * 
 * @author chenqichuan
 * 
 */
public class CustomerProRemark implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8383760190208222593L;

	private Integer id;
	private String proCode;
	private String proSerialNo;
	private String remark;
	private String callFlag;  //0表示没有，1表示打过电话
	private String createUser;
	private String createDate;
	private String autelId;
	private String orderNo;
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public Integer getId() {
		return id;
	}
	public String getProCode() {
		return proCode;
	}
	public String getProSerialNo() {
		return proSerialNo;
	}
	public String getRemark() {
		return remark;
	}
	public String getCallFlag() {
		return callFlag;
	}
	public String getCreateUser() {
		return createUser;
	}
	public String getCreateDate() {
		return createDate;
	}
	public String getAutelId() {
		return autelId;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setCallFlag(String callFlag) {
		this.callFlag = callFlag;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}
	
}