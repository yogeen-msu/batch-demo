package com.cheriscon.common.model;

import java.util.List;

/**
 * 区域配置
 * 
 * @author yinhb
 */
public class AreaConfig implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5958381238938231138L;
	// Fields

	private Integer id;
	private String code; // 区域配置编号
	private String name; // 区域配置名称
	
	private List<AreaInfo> areaInfoList;

	// Property accessors
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AreaInfo> getAreaInfoList() {
		return areaInfoList;
	}

	public void setAreaInfoList(List<AreaInfo> areaInfoList) {
		this.areaInfoList = areaInfoList;
	}

	

	
	

}