package com.cheriscon.common.model;

public class DataLogging {

	private String id;
	private String title;
	private String status; //表示打开，1表示关闭
	private String createDate;
	private String productNo;
	private String car; //型号
	private String vin; //VIN码
	private String make; //品牌
	private String year; //年份
	private String engine; //发动机
	private String contractUser; //联系人
	private String contractTel; //联系电话
	private String contractEmail; //电子邮件
	private String model;
	private String content;
	private String version;
	public String getModel() {
		return model;
	}
	public String getContent() {
		return content;
	}
	public String getVersion() {
		return version;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getCar() {
		return car;
	}
	public String getVin() {
		return vin;
	}
	public String getMake() {
		return make;
	}
	public String getYear() {
		return year;
	}
	public String getEngine() {
		return engine;
	}
	public String getContractUser() {
		return contractUser;
	}
	public String getContractTel() {
		return contractTel;
	}
	public String getContractEmail() {
		return contractEmail;
	}
	public void setCar(String car) {
		this.car = car;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public void setEngine(String engine) {
		this.engine = engine;
	}
	public void setContractUser(String contractUser) {
		this.contractUser = contractUser;
	}
	public void setContractTel(String contractTel) {
		this.contractTel = contractTel;
	}
	public void setContractEmail(String contractEmail) {
		this.contractEmail = contractEmail;
	}
	public String getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public String getStatus() {
		return status;
	}
	public String getCreateDate() {
		return createDate;
	}
	public String getProductNo() {
		return productNo;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}
}
