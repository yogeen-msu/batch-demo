package com.cheriscon.common.utils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JsonConfig;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.constant.FrontConstant;

/**
 * 模拟HttpClient请求处理工具类
 * 
 * @ClassName: HttpUtil
 * @Description:
 * @author zhu.b
 * @date 2011-6-16 下午12:13:09
 */
public class HttpUtil {
	public static String adminHost = "";
	private static Logger log = Logger.getLogger(HttpUtil.class);
	private static String pkey = "b7022c97a7d8c9fc47b2c14b8b375a9e";

	/**
	 * get请求
	 * 
	 * @param key
	 * @param params
	 * @return
	 */
	public static String getReqeuestContent(String key, String params) {
		String url = getUrl(key);
		// String token=DigestUtils.md5Hex(System.currentTimeMillis()+"");
		// String sign = DigestUtils.md5Hex(token+pkey);
		if (StringUtils.isNotEmpty(params)) {
			String sign = getSign(params);
			// url = url + "?" + params+"&_sign="+sign+"&_token="+token;
			url = url + "?" + params + "&_sign=" + sign;
		} else {
			String token = DigestUtils.md5Hex(System.currentTimeMillis() + "");
			String sign = getSign("&_token=" + token);
			url = url + "?_sign=" + sign + "&_token=" + token;
		}
		url = url.replaceAll(" ", "%20");
		return getReqeuestt(url);
	}

	/**
	 * get获取对象
	 * 
	 * @param key
	 * @param params
	 * @param clazz
	 * @return
	 */
	public static Object getReqeuestContentObject(String key, String params,
			Class clazz) {
		String json = getReqeuestContent(key, params);

		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			return JsonUtil.getDTO(json, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * get获取List
	 * 
	 * @param web
	 * @param key
	 * @param params
	 * @param clazz
	 * @return
	 */
	public static List getReqeuestContentList(String key, String params,
			Class clazz) {
		String json = getReqeuestContent(key, params);
		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			return JsonUtil.getDTOList(json, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List getReqeuestContentList(String key, String params,
			JsonConfig jsonConfig) {
		String json = getReqeuestContent(key, params);
		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			return JsonUtil.getDTOList(json, jsonConfig);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Object getReqeuestContentObject(String key, String params,
			JsonConfig jsonConfig) {
		String json = getReqeuestContent(key, params);
		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			return JsonUtil.getDTO(json, jsonConfig);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Page getReqeuestContentPage(String key, String params,
			Class clazz) {
		String json = getReqeuestContent(key, params);
		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.setRootClass(Page.class);
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("result", clazz);
			jsonConfig.setClassMap(classMap);
			Page page = (Page) JsonUtil.getDTO(json, jsonConfig);
			if (page == null) {
				return new Page();
			}
			return page;
		} catch (Exception e) {
			e.printStackTrace();
			return new Page();
		}
	}

	public static String getUrl(String key) {
		ResourceBundle bundle = ResourceBundle.getBundle("url");
		bundle.clearCache();
		String url = bundle.getString(key);
		return adminHost + url;
	}

	/**
	 * 模拟get方式请求链接
	 * 
	 * @Title: getReqeuestContent
	 * @param @param url
	 * @param @param code
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @author zhu.b
	 * @date 2011-6-16 下午12:19:58
	 * @throws
	 */
	private static String getReqeuestt(String url) {
		HttpGet get = new HttpGet(url);
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String ip = FrontConstant.getIpAddr(request);
		get.addHeader("X-Real-IP", ip);
		String response = "";
		try {
			String sessionid = (String) ThreadContextHolder.getHttpRequest()
					.getSession().getAttribute(FrontConstant.SERVER_SESSION);
			if (sessionid != null) {
				get.addHeader("Cookie", "JSESSIONID=" + sessionid);
			}

			HttpResponse httpResponse = httpclient.execute(get);
			saveSession(httpResponse);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				response = EntityUtils.toString(httpResponse.getEntity(),
						"UTF-8");
			} else {
				log.info("请求" + url + "失败");
				throw new RuntimeException("数据加载异常");
			}
			httpclient.close();
		} catch (Exception e) {
			log.info("请求" + url + "失败");
			throw new RuntimeException("数据加载异常", e);
		}
		// log.info("请求:"+url+" 返回:"+response);
		return response;
	}

	@SuppressWarnings("unchecked")
	public static String postRequestContent(String key, Map map) {
		String url = getUrl(key);
		if (null == map) {
			map = new HashMap<String, Object>();
		}
		// String token=DigestUtils.md5Hex(System.currentTimeMillis()+"");
		// String sign = DigestUtils.md5Hex(token+pkey);
		String sign = getSign(map);
		// map.put("_token", token);
		map.put("_sign", sign);
		return postRequest(url, map);
	}

	/**
	 * hlh 20160401 test
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("**************************************");
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		// _sign=3e9489bf568350db31ff72583eaf714f
		hashMap.put("a", "createApp");
		hashMap.put("appId", "569489bf568350db31ff72583eaf7188");
		hashMap.put("appName", "starlink");
		hashMap.put("appPlatform", "0");
		hashMap.put("appDescription", "test");
		hashMap.put("autelId", "okboy%40163.com");
		hashMap.put("userCode", "cui201606170838080524895806");
		hashMap.put("actCode", "F5A9C654B1BF42D5BCBEF485E6F0FFB3");
		System.out.println(getSign(hashMap));
		System.out.println("**************************************");
	}

	public static Page postRequestContentPage(String key, Map map, Class clazz) {
		String json = postRequestContent(key, map);
		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.setRootClass(Page.class);
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("result", clazz);
			jsonConfig.setClassMap(classMap);
			Page page = (Page) JsonUtil.getDTO(json, jsonConfig);
			if (page == null) {
				return new Page();
			}
			return page;
		} catch (Exception e) {
			e.printStackTrace();
			return new Page();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Page postRequestContentPage(String key, Map map,
			String prefix, Object obj, Class clazz) {
		objToMap(map, prefix, obj);
		String json = postRequestContent(key, map);
		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.setRootClass(Page.class);
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("result", clazz);
			jsonConfig.setClassMap(classMap);
			Page page = (Page) JsonUtil.getDTO(json, jsonConfig);
			if (page == null) {
				return new Page();
			}
			return page;
		} catch (Exception e) {
			e.printStackTrace();
			return new Page();
		}
	}

	public static Object postRequestContentObject(String key, String prefix,
			Object obj, Class clazz) {
		Map<String, Object> map = new HashMap<String, Object>();
		objToMap(map, prefix, obj);
		String json = postRequestContent(key, map);
		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			return JsonUtil.getDTO(json, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List postRequestContentList(String key, String prefix,
			Object obj, Class clazz) {
		Map<String, Object> map = new HashMap<String, Object>();
		objToMap(map, prefix, obj);
		String json = postRequestContent(key, map);
		try {
			if (json != null && json.startsWith("\"")) {
				json = json.substring(1, json.length() - 1);
			}
			return JsonUtil.getDTOList(json, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String postRequestContent(String key, Object obj) {
		Map<String, Object> map = new HashMap<String, Object>();
		objToMap(map, obj);
		return postRequestContent(key, map);
	}

	public static String postRequestContent(String key, String prefix,
			Object obj) {
		Map<String, Object> map = new HashMap<String, Object>();
		objToMap(map, prefix, obj);
		return postRequestContent(key, map);
	}

	/**
	 * 封装为模拟为response连接调用
	 * 
	 * @Title: postRequestContent
	 * @param @param url
	 * @param @param map
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @author zhu.b
	 * @date 2011-6-16 下午12:19:52
	 * @throws
	 */
	private static String postRequest(String url, Map<String, Object> map) {
		System.out.println("post请求发送的url===" + url);
		System.out.println("通过Map.entrySet遍历key和value");
		System.out.println("post请求map***********************===");
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			System.out.println("key= " + entry.getKey() + " and value= "
					+ entry.getValue());
		}
		System.out.println("post***********************===");
		HttpPost post = new HttpPost(url);
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String ip = FrontConstant.getIpAddr(request);
		post.addHeader("X-Real-IP", ip);
		String response = "";
		try {
			System.out.println("MapToEntity(map)*************");
			System.out.println(MapToEntity(map));
			System.out.println("*****************************");
			post.setEntity(MapToEntity(map));

			String sessionid = (String) ThreadContextHolder.getHttpRequest()
					.getSession().getAttribute(FrontConstant.SERVER_SESSION);
			if (sessionid != null) {
				post.addHeader("Cookie", "JSESSIONID=" + sessionid);
			}

			HttpResponse httpResponse = httpclient.execute(post);

			saveSession(httpResponse);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// response = httpResponse.getEntity().toString();
				response = EntityUtils.toString(httpResponse.getEntity(),
						"UTF-8");
			} else {
				log.info("请求" + url + "失败");
				throw new RuntimeException("数据加载异常");
			}
			httpclient.close();
		} catch (Exception e) {
			log.info("请求" + url + "失败");
			throw new RuntimeException("数据加载异常", e);
		}
		// log.info("请求:"+url+" 返回:"+response);
		return response;
	}

	@SuppressWarnings("rawtypes")
	private static UrlEncodedFormEntity MapToEntity(Map<String, Object> map) {
		Iterator iter = map.entrySet().iterator();
		List<BasicNameValuePair> formparams = new ArrayList<BasicNameValuePair>();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			String key = (String) entry.getKey();
			String val;
			if (key == null || entry.getValue() == null) {
				continue;
			}
			if (entry.getValue() instanceof Short) {
				val = ((Short) entry.getValue()).toString();
			} else if (entry.getValue() instanceof Integer) {
				val = ((Integer) entry.getValue()).toString();
				// }else if(entry.getValue() instanceof Date){
				// val = DateUtil.format((Date)entry.getValue(),
				// "yyyy-MM-dd hh:mm:ss");
			} else {
				val = entry.getValue().toString();
			}
			formparams.add(new BasicNameValuePair(key, val));
		}
		try {
			return new UrlEncodedFormEntity(formparams, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("表单数据转换异常", e);
		}
	}

	private static void objToMap(Map<String, Object> map, Object obj) {

		Field[] fields = obj.getClass().getDeclaredFields();
		if (fields != null) {
			String name;
			Object value;
			for (Field field : fields) {
				name = field.getName();
				if (name.equals("serialVersionUID"))
					continue;
				try {
					value = PropertyUtils.getProperty(obj, field.getName());
					if (value != null) {
						map.put(name, value);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static void objToMap(Map<String, Object> map, String prefix,
			Object obj) {

		Field[] fields = obj.getClass().getDeclaredFields();
		if (fields != null) {
			String name;
			Object value;
			for (Field field : fields) {
				name = field.getName();
				if (name.equals("serialVersionUID"))
					continue;
				try {
					value = PropertyUtils.getProperty(obj, field.getName());
					if (value != null) {
						map.put(prefix + name, value);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static String getSign(String param) {
		Map<String, Object> map = new HashMap<String, Object>();
		String[] pas = param.split("&");
		String[] pb;
		for (String pa : pas) {
			try {
				pb = pa.split("=");
				map.put(pb[0], pb[1]);
			} catch (Exception e) {
			}
		}
		return getSign(map);
	}

	private static String getSign(Map<String, Object> map) {
		ArrayList<String> list = new ArrayList<String>();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			if (entry.getValue() != null && !entry.getValue().equals("")) {
				list.add(entry.getKey() + "=" + entry.getValue() + "&");
			}
		}
		int size = list.size();
		String[] arrayToSort = list.toArray(new String[size]);
		Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			sb.append(arrayToSort[i]);
		}
		String sign = sb.toString();
		sign = sign + pkey;
		try {
			sign = DigestUtils.md5Hex(sign.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			sign = DigestUtils.md5Hex(sign);
		}
		return sign;
	}

	private static void saveSession(HttpResponse httpResponse) {
		Header[] setcookie = httpResponse.getHeaders("Set-Cookie");
		if (setcookie != null && setcookie.length > 0) {
			String sessionid = setcookie[0].getValue();
			int isstart = sessionid.indexOf("JSESSIONID");
			if (isstart > -1) {
				sessionid = sessionid.substring(isstart + 11, isstart + 43);
				ThreadContextHolder.getHttpRequest().getSession()
						.setAttribute(FrontConstant.SERVER_SESSION, sessionid);
			}
		}
	}
}
