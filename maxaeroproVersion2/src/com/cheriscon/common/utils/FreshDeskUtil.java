package com.cheriscon.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.framework.util.FileUtil;

public class FreshDeskUtil {

	private final static String authInfo = "5m1NjlH55Atzhz7MkJ43:X";//生产环境的密钥  //HJIVeMndSGPVNN7FfpR
	//private final static String authInfo = "HMBzZLxxPJHIfXqqeVGs:X";//测试环境的密钥
	
	@SuppressWarnings("unused")
	public static void publishTicket(CustomerInfo customerInfo) {
		
		try {
			JSONObject jsonObject = new JSONObject();
			HttpPost httpPost = new HttpPost(getPropertiesValue("freshdesk.notlogin.url") + "/helpdesk/tickets.json");
			
			final String createJson = "{\"helpdesk_ticket\":{\"description\":\"developer测试发布ticket内容\"," +
					"\"subject\":\"developer测试发布ticket主题..\",\"email\":\"'"+customerInfo.getAutelId()+"'\"," +
							"\"priority\":1,\"status\":2},\"cc_emails\":\"support@autelrobotics.com,dsong@autelrobotics.com\"}";
			
//			BASE64Encoder encoder = new BASE64Encoder();
//			authInfo1 = encoder.encode(authInfo1.getBytes());
			String authInfoStr = Base64.encodeBase64URLSafeString(authInfo.getBytes());
			httpPost.setHeader("Authorization", "Basic " + authInfoStr);
			StringEntity entity = new StringEntity(createJson, "UTF-8");
			entity.setContentType("application/json");
//			entity.setContentType("multipart/form-data");
			httpPost.setEntity(entity);
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			
			HttpResponse response = httpClient.execute(httpPost);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public static String createUser(CustomerInfo customerInfo) throws ClientProtocolException, IOException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", customerInfo.getLastName());
		jsonObject.put("email", customerInfo.getAutelId());
		jsonObject.put("address", customerInfo.getAddress());
		jsonObject.put("phone", customerInfo.getTelephone());
		HttpPost httpPost = new HttpPost(getPropertiesValue("freshdesk.notlogin.url") + "/api/v2/contacts");
		String authInfoStr = Base64.encodeBase64URLSafeString(authInfo.getBytes());
		httpPost.setHeader("Authorization", "Basic " + authInfoStr);
		StringEntity entity = new StringEntity(jsonObject.toString(), "UTF-8");
		entity.setContentType("application/json");
		httpPost.setEntity(entity);
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		
		HttpResponse response = httpClient.execute(httpPost);
		String httpResult = EntityUtils.toString(response.getEntity(), "UTF-8");
		jsonObject = jsonObject.fromObject(httpResult);
		return jsonObject.getString("id");
	}
	
	public static boolean updateUser(CustomerInfo customerInfo) throws ClientProtocolException, IOException {
		if (customerInfo.getAddress() == null && customerInfo.getLastName() == null) {
			return true;
		}
		if(!getUser(customerInfo)){
			return true;
		}
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", customerInfo.getLastName());
		jsonObject.put("address", customerInfo.getAddress());
		HttpPut httpPut = new HttpPut(getPropertiesValue("freshdesk.notlogin.url") + "/api/v2/contacts/" + customerInfo.getFreshdeskId());
		String authInfoStr = Base64.encodeBase64URLSafeString(authInfo.getBytes());
		httpPut.setHeader("Authorization", "Basic " + authInfoStr);
		StringEntity entity = new StringEntity(jsonObject.toString(), "UTF-8");
		entity.setContentType("application/json");
		httpPut.setEntity(entity);
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse response = httpClient.execute(httpPut);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			return true;
		}
		return false;
	}
	
	public static boolean getUser(CustomerInfo customerInfo) throws ClientProtocolException, IOException {
		if (StringUtils.isEmpty(customerInfo.getFreshdeskId())) {
			return false;
		}
		HttpGet httpPut = new HttpGet(getPropertiesValue("freshdesk.notlogin.url") + "/api/v2/contacts/" + customerInfo.getFreshdeskId());
		String authInfoStr = Base64.encodeBase64URLSafeString(authInfo.getBytes());
		httpPut.setHeader("Authorization", "Basic " + authInfoStr);
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse response = httpClient.execute(httpPut);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			return true;
		}
		return false;
	}
	
	public static boolean getUserByEmail(String email) throws ClientProtocolException, IOException {
		if (StringUtils.isEmpty(email)) {
			return false;
		}
		HttpGet httpPut = new HttpGet(getPropertiesValue("freshdesk.notlogin.url") + "/api/v2/contacts?email=" + email);
		String authInfoStr = Base64.encodeBase64URLSafeString(authInfo.getBytes());
		httpPut.setHeader("Authorization", "Basic " + authInfoStr);
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse response = httpClient.execute(httpPut);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			String httpResult = EntityUtils.toString(response.getEntity(), "UTF-8");
			JSONArray json = JSONArray.fromObject(httpResult);
			return json.size() > 0;
		}
		return false;
	}
	
	public static void main(String[] args) throws ClientProtocolException, IOException {
		System.out.println(getUserByEmail("496171508@qq.com"));
	}
	
	private static String getPropertiesValue(String key) {
    	InputStream in = FileUtil.getResourceAsStream("cop.properties");
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return props.getProperty(key);
    }
	
}
