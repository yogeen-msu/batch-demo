package com.cheriscon.common.utils;

public class StrEncrypt {
	
	public static void main(String []args){
		System.out.println(decrypt("0DUGTnFhcXEWFhYWFhYW"));
	}
	
	public static String decrypt(String s){
		
		if(s==null||s.equals("")){
			return s;
		}
		int len = s.length()/4*4;
		int le=len/4*3;
		String outstr = UnBase64(s, len, le);
		if (1 == (outstr.charAt(0) & 0x03)) 
			le -= 2; 
		else if (2 == (outstr.charAt(0) & 0x03)) 
			le--; 
		StringBuffer chbuf = new StringBuffer();
		for (int i = 1; i < le; i++) 
		{ 
//			chBuf[i] = EncryptChar(chBuf[i]); 
			chbuf.append(EncryptChar(outstr.charAt(i)));
		} 
		return chbuf.toString();
	}
	private static char EncryptChar(char c){
		char x = 0; 
		x += (c & 0x80); 
		x += (c & 0x40) >> 6; 
		x += (c & 0x20) >> 4; 
		x += (c & 0x10) >> 2; 
		x += (c & 0x08);// << 3; 
		x += (c & 0x04) << 2; 
		x += (c & 0x02) << 4; 
		x += (c & 0x01) << 6; 

		return x; 
	}
	
	private static String UnBase64(String s,int len,int le){
		
		int i=0,j=0;
		char ch1,ch2,ch3,ch4;
		StringBuffer outsb = new StringBuffer();
		while (i + 3 < len) 
		{ 
			ch1 = Index64(s.charAt(i)); 
			ch2 = Index64(s.charAt(i+1)); 
			ch3 = Index64(s.charAt(i+2)); 
			ch4 = Index64(s.charAt(i+3)); 
			outsb.append((char)((ch1 << 2) | ((ch2 >> 4) & 0x3)));
			outsb.append((char)((ch2 << 4) | ((ch3 >> 2) & 0xf)));
			outsb.append((char)((ch3 << 6) | ch4));

			i += 4; 
			j += 3; 
		}
		return outsb.toString();
	}
	private static char Index64(char c){
		
		int nIndex =0;
		if(Character.isUpperCase(c)){
			nIndex = c-'A';
		}else if(Character.isLowerCase(c)){
			nIndex = 26 + c - 'a';
		}else if(Character.isDigit(c)){
			nIndex = 52 + c - '0'; 
		}else if (c == '_') 
		{ 
			nIndex = 62; 
		} 
		else if (c == '.') 
		{ 
			nIndex = 63; 
		} 
		else 
		{ 
			nIndex = 0; 
		} 
		
		return (char)nIndex;
	}

}
