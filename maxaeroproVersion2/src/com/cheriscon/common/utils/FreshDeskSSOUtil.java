package com.cheriscon.common.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.cheriscon.common.model.CustomerInfo;

public class FreshDeskSSOUtil {

    private static final String BASE_URL = "https://autelrobotics.freshdesk.com/login/sso"; //正式
    private static final String sharedSecret = "10145f12c8ae16556588a167da95e950"; //正式
//    private static final String BASE_URL = "https://https://autelgolive.freshdesk.com/login/sso"; //测试
//    private static final String sharedSecret = "9e163029c2849199df7abd9904e4da64"; //测试

    public static String hashToHexString(byte[] byteData)
    {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            String hex = Integer.toHexString(0xff & byteData[i]);
            // NB! E.g.: Integer.toHexString(0x0C) will return "C", not "0C"            
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private static String getHMACHash(String name,String email,long timeInMillis) throws Exception {
        byte[] keyBytes = sharedSecret.getBytes();
        String movingFact =name+email+timeInMillis;
        byte[] text = movingFact.getBytes();
        
        String hexString = "";
        Mac hmacMD5;
        try {
          hmacMD5 = Mac.getInstance("HmacMD5");
          SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
          hmacMD5.init(macKey);
          byte[] hash =  hmacMD5.doFinal(text);
          hexString = hashToHexString(hash);

        } catch (Exception nsae) {
            System.out.println("Caught the exception");
        }
                return hexString;
    }

    public static void ssoLogin(CustomerInfo customerInfo) throws NoSuchAlgorithmException, InvalidKeyException {

        String hash;
        String url = null;
        //User user; //Get the user details using your current authentication system
        String name = "abc";// Full name of the user
        String email = customerInfo.getAutelId();// Email of the user
        long timeInSeconds = System.currentTimeMillis()/1000; 
        
        try {       
            hash = getHMACHash(name,email,timeInSeconds);
            url = BASE_URL + "?name="+name+"&email="+email+"&timestamp="+timeInSeconds+"&hash=" + hash; 
        } catch (Exception e) {
            //Handle appropriate code
            System.out.println("There is an exception while constructing the URL");
            e.printStackTrace();
        }   
        System.out.println(url);
    }
	
}
