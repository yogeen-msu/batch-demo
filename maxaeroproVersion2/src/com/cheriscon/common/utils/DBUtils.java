package com.cheriscon.common.utils;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import com.cheriscon.cop.sdk.utils.DateUtil;

public class DBUtils {
	
	/**
	 * 数据库Code生成
	 * @param prefix	前缀
	 * @return
	 */
	public static String generateCode(String ... prefix) {
		StringBuffer code = new StringBuffer();
		code.append(prefix[0]);
		String dateStr = DateUtil.toString(new Date(),"yyyyMMddHHmmssSSSS");
		if(prefix.length > 1){
			int len =  prefix[1].length();
			String temp = dateStr.substring(0, dateStr.length()-len);
			dateStr = temp + prefix[1];
		}
		Random r=new Random ();
		String uuid = UUID.randomUUID().toString();
		code.append(dateStr).append(uuid.substring(uuid.length()-4)).append(r.nextInt(10)).append(r.nextInt(10));
		return code.toString();
	}
	
	/**
	 * 获取数据库表名
	 * @return
	 */
//	@SuppressWarnings("rawtypes")
//	public static String getTableName(Class clazz){
//		return DBConstant.DB_PREFIX + clazz.getSimpleName();
//	}
	

}
