package com.cheriscon.sdkapp.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import net.sf.json.JSONObject;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.sdkapp.action.SdkAppAction;
import com.cheriscon.sdkapp.service.ISdkAppService;
import com.cheriscon.sdkapp.vo.SdkAppDownloadVO;
import com.cheriscon.sdkapp.vo.SdkAppVO;

/**
 * sdk app逻辑处理实现类
 * 
 * @author autel
 * 
 */
@Service(value = "sdkAppService")
public class SdkAppServiceImpl implements ISdkAppService {

	private static Logger logger = Logger.getLogger(SdkAppAction.class);

	/**
	 * 查詢sdk app集合
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<SdkAppVO> querySdkAppList(SdkAppVO sdkAppVO) {
		List<SdkAppVO> list = (List<SdkAppVO>) HttpUtil.postRequestContentList(
				"rest.sdk.app.queryAppList", "sdkAppVO.", sdkAppVO,
				SdkAppVO.class);
		return list;
	}

	/**
	 * 查詢sdk app详情
	 * 
	 * @return
	 */
	public SdkAppVO getSdkAppDetails(SdkAppVO sdkAppVO) {
		return (SdkAppVO) HttpUtil.postRequestContentObject(
				"rest.sdk.app.getSdkAppDetails", "sdkAppVO.", sdkAppVO,
				SdkAppVO.class);
	}

	/**
	 * 添加sdk app
	 * 
	 * @return
	 */
	public String addSdkApp(SdkAppVO sdkAppVO) {
		String result = "-1";
		try {
			if (null != sdkAppVO) {
				HashMap<String, String> hashMap = new HashMap<String, String>();
				hashMap.put("appId", sdkAppVO.getPackageName());
				hashMap.put("appName", sdkAppVO.getAppName());
				hashMap.put("appPlatform", sdkAppVO.getSoftwarePlatform());
				hashMap.put("appDescription", sdkAppVO.getDescription());
				hashMap.put("autelId", sdkAppVO.getAutelId());
				hashMap.put("userCode", sdkAppVO.getUserCode());
				hashMap.put("actCode", sdkAppVO.getActCode());
				// 调用sdk app外部应用接口
				String returnContent = requestHttpUrlConnection("create",
						hashMap);
				logger.info("create method return content:" + returnContent);
				String code = toMap(returnContent).get("code");
				// 判断外部应用接口是否调用成功
				if ("1".equals(code)) {
					String appKey = toMap(toMap(returnContent).get("data"))
							.get("appKey");
					sdkAppVO.setAppKey(appKey);// 设置app key
					// 调用后台添加sdk app服务
					HttpUtil.postRequestContent("rest.sdk.app.addSdkAppList",
							"sdkAppVO.", sdkAppVO);
				}
				result = code;
			}
		} catch (Exception e) {
			logger.error("addSdkAppList error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 修改sdk app
	 * 
	 * @return
	 */
	public String updateSdkApp(SdkAppVO sdkAppVO) {
		String result = "-1";
		try {
			if (null != sdkAppVO) {
				HashMap<String, String> hashMap = new HashMap<String, String>();
				hashMap.put("appId", sdkAppVO.getPackageName());
				hashMap.put("appName", sdkAppVO.getAppName());
				hashMap.put("appPlatform", sdkAppVO.getSoftwarePlatform());
				hashMap.put("appDescription", sdkAppVO.getDescription());
				hashMap.put("autelId", sdkAppVO.getAutelId());
				hashMap.put("userCode", sdkAppVO.getUserCode());
				hashMap.put("actCode", sdkAppVO.getActCode());
				// 调用sdk app外部应用接口
				String returnContent = requestHttpUrlConnection("modify",
						hashMap);
				logger.info("modify method return content:" + returnContent);
				String code = toMap(returnContent).get("code");
				// 判断外部应用接口是否调用成功
				if ("1".equals(code)) {
					// 调用后台添加sdk app服务
					HttpUtil.postRequestContent(
							"rest.sdk.app.updateSdkAppList", "sdkAppVO.",
							sdkAppVO);
				}
				result = code;
			}
		} catch (Exception e) {
			logger.error("addSdkAppList error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 删除sdk app
	 * 
	 * @return
	 */
	public String deleteSdkApp(SdkAppVO sdkAppVO) {
		String result = "-1";
		try {
			if (null != sdkAppVO) {
				HashMap<String, String> hashMap = new HashMap<String, String>();
				hashMap.put("appId", sdkAppVO.getPackageName());
				hashMap.put("autelId", sdkAppVO.getAutelId());
				hashMap.put("userCode", sdkAppVO.getUserCode());
				hashMap.put("actCode", sdkAppVO.getActCode());
				// 调用sdk app外部应用接口
				String returnContent = requestHttpUrlConnection("delete",
						hashMap);
				logger.info("delete method return content:" + returnContent);
				String code = toMap(returnContent).get("code");
				// 判断外部应用接口是否调用成功
				if ("1".equals(code)) {
					HttpUtil.postRequestContent(
							"rest.sdk.app.deleteSdkAppList", "sdkAppVO.",
							sdkAppVO);
				}
				result = code;
			}
		} catch (Exception e) {
			logger.error("addSdkAppList error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 查詢sdk app download信息
	 * 
	 * @return list
	 */
	@SuppressWarnings("unchecked")
	public List<SdkAppDownloadVO> querySdkAppDownloadsInfo() {
		return HttpUtil.getReqeuestContentList(
				"rest.sdk.app.querySdkAppDownloadsInfo", "",
				SdkAppDownloadVO.class);
	}

	/**
	 * 查詢查询用户sdk app协议详情
	 * 
	 * @param sdkAppVO
	 * @return SdkAppVO
	 */
	public SdkAppVO getCustomerAgreementDetails(SdkAppVO sdkAppVO) {
		return (SdkAppVO) HttpUtil.postRequestContentObject(
				"rest.sdk.app.getCustomerAgreementDetails", "sdkAppVO.",
				sdkAppVO, SdkAppVO.class);
	}

	/**
	 * 用户同意sdk app协议详情
	 * 
	 * @param sdkAppVO
	 * @return String
	 */
	public boolean saveCustomerAgreementDetails(SdkAppVO sdkAppVO) {
		boolean result = false;
		try {
			if (null != sdkAppVO) {
				// 调用后台添加sdk app服务
				HttpUtil.postRequestContent(
						"rest.sdk.app.saveCustomerAgreementDetails",
						"sdkAppVO.", sdkAppVO);
				result = true;
			}
		} catch (Exception e) {
			logger.error("addSdkAppList error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 输入流参数
	 * 
	 * @param httpConn
	 * @param queryString
	 * @param charset
	 */
	private void createOutputStream(HttpURLConnection httpConn,
			String queryString, String charset) {
		OutputStream outputStream = null;
		try {
			// 建立输出流，并写入数据
			outputStream = httpConn.getOutputStream();
			outputStream.write(queryString.getBytes(charset));
			outputStream.flush();
		} catch (IOException ex) {
			System.out.println("............"+ex.getStackTrace());
			logger.error("httpUrlConnection error." + ex.getMessage());
			
			ex.printStackTrace();
		} finally {
			if (null != outputStream) {
				try {
					outputStream.close();
				} catch (IOException e) {
					logger.error("outputStream close error." + e.getMessage());
				}
			}
		}
	}

	/**
	 * 调用接口
	 * 
	 * @param pathURL
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	private String requestHttpUrlConnection(String type,
			Map<String, String> paramMap) throws Exception {
		String result = null;
		BufferedReader responseReader = null;
		InputStreamReader inputStreamReader = null;
		HttpURLConnection httpConn = null;
		try {
			// 获取配置
			ResourceBundle bundle = ResourceBundle.getBundle("config");
			ResourceBundle.clearCache();
			String serviceURL = bundle.getString("app.app.interface.url");
			String requestMethod = bundle
					.getString("app.app.interface.request.method");
			String contentLength = bundle
					.getString("app.app.interface.content.length");
			String contentType = bundle
					.getString("app.app.interface.content.type");
			String connection = bundle
					.getString("app.app.interface.connection");
			String charset = bundle.getString("app.app.interface.charset");
			String interfaceMethod = bundle
					.getString("sdk.app.interface.method." + type);
			// 调用服务完整地址
			String queryString = "a=" + interfaceMethod;
			if (null != paramMap && paramMap.size() > 0) {
				for (Map.Entry<String, String> entry : paramMap.entrySet()) {
					queryString += ("&" + entry.getKey() + "=" + entry
							.getValue());
				}
			}
			// 增加sign值
			queryString += ("&_sign=" + getSign(paramMap));
			logger.info("sdk app service:" + serviceURL + queryString);
			// 建立连接
			URL url = new URL(serviceURL);
			httpConn = (HttpURLConnection) url.openConnection();
			// //设置连接属性
			httpConn.setDoOutput(true);// 使用 URL 连接进行输出
			httpConn.setDoInput(true);// 使用 URL 连接进行输入
			httpConn.setUseCaches(false);// 忽略缓存
			httpConn.setRequestMethod(requestMethod);// 设置URL请求方法
			// 设置请求属性
			httpConn.setRequestProperty("Content-length", contentLength);
			httpConn.setRequestProperty("Content-Type", contentType);
			httpConn.setRequestProperty("Connection", connection);// 维持长连接
			httpConn.setRequestProperty("Charset", charset);
			// 建立输出流，并写入数据
			createOutputStream(httpConn, queryString, charset);
			// 获得响应状态
			int responseCode = httpConn.getResponseCode();
			if (HttpURLConnection.HTTP_OK == responseCode) {// 连接成功
				String readLine;
				// 当正确响应时处理数据
				StringBuffer sb = new StringBuffer();
				inputStreamReader = new InputStreamReader(
						httpConn.getInputStream(), charset);
				// 处理响应流，必须与服务器响应流输出的编码一致
				responseReader = new BufferedReader(inputStreamReader);
				while ((readLine = responseReader.readLine()) != null) {
					sb.append(readLine);
				}
				responseReader.close();
				result = sb.toString();
			}
			httpConn.disconnect();
		} catch (Exception ex) {
			result = null;
			logger.error("httpUrlConnection error." + ex.getMessage());
			throw new Exception("httpUrlConnection error." + ex.getMessage());
		} finally {
			if (null != inputStreamReader) {
				try {
					inputStreamReader.close();
				} catch (IOException ex) {
					logger.error("inputStreamReader close error."
							+ ex.getMessage());
				}
			}
			if (null != responseReader) {
				try {
					responseReader.close();
				} catch (IOException ex) {
					logger.error("responseReader close error."
							+ ex.getMessage());
				}
			}
			// 关闭连接
			if (null != httpConn) {
				httpConn.disconnect();
			}
		}
		return result;
	}

	/**
	 * JSOn转换成Map对象
	 * 
	 * @param object
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private Map<String, String> toMap(Object object) {
		Map<String, String> data = new HashMap<String, String>();
		// 将json字符串转换成jsonObject
		JSONObject jsonObject = JSONObject.fromObject(object);
		Iterator ite = jsonObject.keys();
		// 遍历jsonObject数据,添加到Map对象
		while (ite.hasNext()) {
			String key = ite.next().toString();
			String value = jsonObject.get(key).toString();
			data.put(key, value);
		}
		return data;
	}

	/**
	 * 获取sign值
	 * 
	 * @param map
	 * @return
	 */
	private String getSign(Map<String, String> map) {
		String pkey = "f8bd14a3a34e400f81f980314c8b99b4";
		ArrayList<String> list = new ArrayList<String>();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			if (entry.getValue() != null && !entry.getValue().equals("")) {
				list.add(entry.getKey() + "=" + entry.getValue() + "&");
			}
		}
		int size = list.size();
		String[] arrayToSort = list.toArray(new String[size]);
		Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			sb.append(arrayToSort[i]);
		}
		String sign = sb.toString();
		sign = sign + pkey;
		try {
			sign = DigestUtils.md5Hex(sign.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			sign = DigestUtils.md5Hex(sign);
		}
		return sign;
	}
}
