package com.cheriscon.sdkapp.service;

import java.util.List;

import com.cheriscon.sdkapp.vo.SdkAppDownloadVO;
import com.cheriscon.sdkapp.vo.SdkAppVO;

/**
 * sdk app逻辑处理接口
 * 
 * @author autel
 * 
 */
public interface ISdkAppService {
	/**
	 * 查詢sdk app集合
	 * 
	 * @return
	 */
	public List<SdkAppVO> querySdkAppList(SdkAppVO sdkAppVO);

	/**
	 * 查詢sdk app详情
	 * 
	 * @return
	 */
	public SdkAppVO getSdkAppDetails(SdkAppVO sdkAppVO);

	/**
	 * 添加sdk app
	 * 
	 * @return
	 */
	public String addSdkApp(SdkAppVO sdkAppVO);

	/**
	 * 修改sdk app
	 * 
	 * @return
	 */
	public String updateSdkApp(SdkAppVO sdkAppVO);

	/**
	 * 删除sdk app
	 * 
	 * @return
	 */
	public String deleteSdkApp(SdkAppVO sdkAppVO);

	/**
	 * 查詢sdk app download信息
	 * 
	 * @return list
	 */
	public List<SdkAppDownloadVO> querySdkAppDownloadsInfo();

	/**
	 * 查詢查询用户sdk app协议详情
	 * 
	 * @param sdkAppVO
	 * @return SdkAppVO
	 */
	public SdkAppVO getCustomerAgreementDetails(SdkAppVO sdkAppVO);

	/**
	 * 用户同意sdk app协议详情
	 * 
	 * @param sdkAppVO
	 * @return String
	 */
	public boolean saveCustomerAgreementDetails(SdkAppVO sdkAppVO);
}
