package com.cheriscon.sdkapp.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.sdkapp.service.ISdkAppService;
import com.cheriscon.sdkapp.vo.SdkAppDownloadVO;
import com.cheriscon.sdkapp.vo.SdkAppVO;

/**
 * SDK APP info
 * 
 * @author autel
 * 
 */
@ParentPackage("json-default")
@Namespace("/sdk/app")
public class SdkAppAction extends WWAction {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SdkAppAction.class);

	@Resource
	private ISdkAppService sdkAppService;
	/**
	 * 返回值
	 */
	private String jsonData;

	/**
	 * sdk app参数对象
	 */
	private SdkAppVO sdkAppVO;

	/**
	 * 查詢sdk app集合
	 * 
	 * @return
	 */
	@Action(value = "querySdkAppList", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String querySdkAppList() {
		// 返回消息对象
		Map<String, Object> map = new HashMap<String, Object>();
		// 获取当前登陆用户Id
		CustomerInfo customerInfo = getCurrentCustomerInfo();
		if (null == customerInfo) {
			map.put("result", false);
			map.put("msg", "Failed to obtain logon user.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		sdkAppVO.setCustomerId(customerInfo.getId() + "");// 设置用户id
		// 查询sdk集合信息
		List<SdkAppVO> sdkAppVOList = sdkAppService.querySdkAppList(sdkAppVO);
		if (null != sdkAppVOList && sdkAppVOList.size() > 0) {
			map.put("result", true);
			map.put("sdkAppVOList", sdkAppVOList);
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		map.put("result", false);
		map.put("msg", "No data was found!");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 查詢sdk app详情
	 * 
	 * @return
	 */
	@Action(value = "getSdkAppDetails", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getSdkAppDetails() {
		// 返回消息对象
		Map<String, Object> map = new HashMap<String, Object>();
		// 查询sdk集合信息
		SdkAppVO sdkAppObject = sdkAppService.getSdkAppDetails(sdkAppVO);
		if (null != sdkAppObject) {
			map.put("result", true);
			map.put("sdkAppVO", sdkAppObject);
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		map.put("result", false);
		map.put("msg", "No data was found.");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 添加sdk app
	 * 
	 * @return
	 */
	@Action(value = "addSdkApp", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String addSdkApp() {
		// 返回消息对象
		Map<String, Object> map = new HashMap<String, Object>();
		// 获取当前登陆用户Id
		CustomerInfo customerInfo = getCurrentCustomerInfo();
		if (null == customerInfo) {
			map.put("result", false);
			map.put("msg", "Failed to obtain logon user.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		sdkAppVO.setCustomerId(customerInfo.getId() + "");// 设置用户id
		sdkAppVO.setAutelId(customerInfo.getAutelId());
		sdkAppVO.setActCode(customerInfo.getActCode());
		sdkAppVO.setUserCode(customerInfo.getCode());
		// 查询sdk集合信息
		String result = sdkAppService.addSdkApp(sdkAppVO);
		if ("1".equals(result)) {
			map.put("result", result);
			map.put("msg", "Add successfully.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		if("-4101".equals(result)){
			map.put("result", result);
			map.put("msg", "Package name is registered.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		map.put("result", result);
		map.put("msg", "System error.");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 修改sdk app
	 * 
	 * @return
	 */
	@Action(value = "updateSdkApp", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateSdkApp() {
		// 返回消息对象
		Map<String, Object> map = new HashMap<String, Object>();
		// 获取当前登陆用户Id
		CustomerInfo customerInfo = getCurrentCustomerInfo();
		if (null == customerInfo) {
			map.put("result", false);
			map.put("msg", "Failed to obtain logon user.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		sdkAppVO.setAutelId(customerInfo.getAutelId());
		sdkAppVO.setActCode(customerInfo.getActCode());
		sdkAppVO.setUserCode(customerInfo.getCode());
		// 查询sdk集合信息
		String result = sdkAppService.updateSdkApp(sdkAppVO);
		if ("1".equals(result)) {
			map.put("result", result);
			map.put("msg", "Update successfully.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		if("-4101".equals(result)){
			map.put("result", result);
			map.put("msg", "Package name is registered.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		map.put("result", result);
		map.put("msg", "System error.");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 删除sdk app
	 * 
	 * @return
	 */
	@Action(value = "deleteSdkApp", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String deleteSdkApp() {
		// 返回消息对象
		Map<String, Object> map = new HashMap<String, Object>();
		// 获取当前登陆用户Id
		CustomerInfo customerInfo = getCurrentCustomerInfo();
		if (null == customerInfo) {
			map.put("result", false);
			map.put("msg", "Failed to obtain logon user.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		sdkAppVO.setAutelId(customerInfo.getAutelId());
		sdkAppVO.setActCode(customerInfo.getActCode());
		sdkAppVO.setUserCode(customerInfo.getCode());
		// 查询sdk集合信息
		String result = sdkAppService.deleteSdkApp(sdkAppVO);
		if ("1".equals(result)) {
			map.put("result", result);
			map.put("msg", "Delete successfully.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		map.put("result", result);
		map.put("msg", "System error.");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 查詢sdk app download信息
	 * 
	 * @return String
	 */
	@Action(value = "querySdkAppDownloadsInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String querySdkAppDownloadsInfo() {
		// 返回消息对象
		Map<String, Object> map = new HashMap<String, Object>();
		// 查询download信息
		List<SdkAppDownloadVO> querySdkAppDownloadsList = sdkAppService
				.querySdkAppDownloadsInfo();
		if (null != querySdkAppDownloadsList
				&& querySdkAppDownloadsList.size() > 0) {
			map.put("result", true);
			map.put("downloadList", querySdkAppDownloadsList);
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		map.put("result", false);
		map.put("msg", "No data was found.");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 查詢查询用户sdk app协议详情
	 * 
	 * @return String
	 */
	@Action(value = "getCustomerAgreementDetails", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getCustomerAgreementDetails() {
		// 返回消息对象
		Map<String, Object> map = new HashMap<String, Object>();
		// 获取当前登陆用户Id
		CustomerInfo customerInfo = getCurrentCustomerInfo();
		if (null == customerInfo) {
			map.put("result", false);
			map.put("handleType", 0);//类型：用户登陆失败
			map.put("msg", "Failed to obtain logon user.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		SdkAppVO sdkApp = new SdkAppVO();
		sdkApp.setCustomerId(customerInfo.getId() + "");// 设置用户id
		// 查询协议
		SdkAppVO sdkAppObject = sdkAppService
				.getCustomerAgreementDetails(sdkApp);
		if (null != sdkAppObject) {
			map.put("result", true);
			map.put("sdkAppVO", sdkAppObject);
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		map.put("result", false);
		map.put("handleType", 1);//类型：用户登陆成功，但没有确认协议
		map.put("msg", "No data was found.");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 用户同意sdk app协议详情
	 * 
	 * @return String
	 */
	@Action(value = "saveCustomerAgreementDetails", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String saveCustomerAgreementDetails() {
		// 返回消息对象
		Map<String, Object> map = new HashMap<String, Object>();
		// 获取当前登陆用户Id
		CustomerInfo customerInfo = getCurrentCustomerInfo();
		if (null == customerInfo) {
			map.put("result", false);
			map.put("msg", "Failed to obtain logon user.");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		SdkAppVO sdkApp = new SdkAppVO();
		sdkApp.setCustomerId(customerInfo.getId() + "");// 设置用户id
		// 同意协议
		boolean result = sdkAppService.saveCustomerAgreementDetails(sdkApp);
		if (result) {
			map.put("result", true);
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		map.put("result", false);
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 获取当前登陆用户Id
	 * 
	 * @return int
	 */
	private CustomerInfo getCurrentCustomerInfo() {
		CustomerInfo customerInfo = null;
		try {
			HttpServletRequest request = ThreadContextHolder.getHttpRequest();
			// 从session中获取当前登录的用户信息
			customerInfo = (CustomerInfo) SessionUtil.getLoginUserInfo(request);
		} catch (Exception e) {
			logger.error("getCurrentLoginUserId error." + e.getMessage());
		}
		return customerInfo;
	}

	public ISdkAppService getSdkAppService() {
		return sdkAppService;
	}

	public void setSdkAppService(ISdkAppService sdkAppService) {
		this.sdkAppService = sdkAppService;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public SdkAppVO getSdkAppVO() {
		return sdkAppVO;
	}

	public void setSdkAppVO(SdkAppVO sdkAppVO) {
		this.sdkAppVO = sdkAppVO;
	}

}
