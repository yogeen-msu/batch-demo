package com.cheriscon.sdkapp.action;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * SDK APP info Servlet
 * 
 * @author autel
 * 
 */

public class SdkAppServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SdkAppServlet.class);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String fileName = "Autel SDK EULA-Final Version.docx";
		try {
			request.setCharacterEncoding("utf-8");
			// 获取文件路径
			String filePath = this.getClass().getClassLoader()
					.getResource("/com/cheriscon/sdkapp/action").getPath()
					+ fileName;
			logger.debug("filePath:" + filePath);
			filePath = filePath == null ? "" : filePath;
			// 设置向浏览器端传送的文件格式
			response.setContentType("application/x-download");

			fileName = URLEncoder.encode(fileName, "UTF-8");
			response.addHeader("Content-Disposition", "attachment;filename="
					+ fileName);
			FileInputStream fis = null;
			OutputStream os = null;
			try {
				os = response.getOutputStream();
				fis = new FileInputStream(filePath);
				byte[] b = new byte[1024 * 10];
				int i = 0;
				while ((i = fis.read(b)) > 0) {
					os.write(b, 0, i);
				}
				os.flush();
				os.close();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
