package com.cheriscon.sdkapp.vo;

import java.io.Serializable;

/**
 * sdk app object
 * 
 * @author autel
 * 
 */
@SuppressWarnings("serial")
public class SdkAppVO implements Serializable {

	private String id;// 主键
	private String appName;// app名称
	private String appKey;// app钥匙
	private String softwarePlatform;// 软件平台
	private String packageName;// 包名称
	private String category;// 类别
	private String description;// 描述
	private String status;// 状态
	private String customerId;// 用户id,后台主键
	private String autelId;// 用户登录Id
	private String userCode;// 用户登录产品网站返回的code值
	private String actCode;// 用户登录产品网站的actcode值
	private String sign;// 用户是否同意协议标记
	private String createDate;// 创建时间
	private String lastUpdateDate;// 最后修改时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getSoftwarePlatform() {
		return softwarePlatform;
	}

	public void setSoftwarePlatform(String softwarePlatform) {
		this.softwarePlatform = softwarePlatform;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getActCode() {
		return actCode;
	}

	public void setActCode(String actCode) {
		this.actCode = actCode;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
