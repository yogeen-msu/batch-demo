/**
*/ 
package com.cheriscon.product.service;

import java.util.List;
import java.util.Map;

import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: ILanguagePackService
 * @Description: 软件语言包业务类
 * @author shaohu
 * @date 2013-1-10 下午07:43:44
 * 
 */
public interface ILanguagePackService {
//	
//	/**
//	 * 
//	* @Title: listAll
//	* @Description: 查询软件语言包集合
//	* @param    
//	* @return List<LanguagePack>    
//	* @throws
//	 */
//	public List<LanguagePack> listAll() throws Exception;
//	
//	/**
//	 * 翻页查询 查询软件语言包集合
//	 * @param pageNo
//	 * @param pageSize
//	 * @return
//	 * @throws Exception
//	 */
	public Page searchPage(Map<String,Object> params,int pageNo,int pageSize) throws Exception ;
//	/**
//	 * 
//	* @Title: addLanguagePack
//	* @Description: 添加软件语言包
//	* @param    软件语言包 对象
//	* @return void    
//	* @throws
//	 */
//	public void addLanguagePack(LanguagePack languagePack) throws Exception;
//
//	/**
//	 * 
//	* @Title: delLanguagePack
//	* @Description: 根据软件语言包code 删除软件语言包
//	* @param    code  软件语言包code
//	* @return void    
//	* @throws
//	 */
//	public void delLanguagePack(String code)  throws Exception;
//	
//	/**
//	 * 
//	* @Title: delLanguagePackByVersionCode
//	* @Description: 根据软件版本编码删除软件语言包
//	* @param   versionCode 软件版本编码
//	* @return void    
//	* @throws
//	 */
//	public void delLanguagePackByVersionCode(String versionCode) throws Exception;
//	
//	/**
//	 * 
//	* @Title: updateLanguagePack
//	* @Description: 修改语言包
//	* @param    
//	* @return void    
//	* @throws
//	 */
//	public void updateLanguagePack(LanguagePack languagePack) throws Exception;
//	
//	/**
//	 * 
//	* @Title: getLanguagePackByVersionCode
//	* @Description: 根据版本编码获取语言包
//	* @param    
//	* @return List<LanguagePack>    
//	* @throws
//	 */
//	public List<LanguagePack> getLanguagePackByVersionCode(String versionCode) throws Exception;
}
