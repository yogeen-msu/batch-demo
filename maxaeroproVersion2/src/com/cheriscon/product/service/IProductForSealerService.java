package com.cheriscon.product.service;



import java.util.List;

import com.cheriscon.product.model.ProductForSealer;

//import com.cheriscon.backstage.market.vo.ProductForSealer;
//import com.cheriscon.backstage.market.vo.ProductForSealerConfig;
//import com.cheriscon.backstage.product.vo.ProductTypeSerialPicVo;
//import com.cheriscon.framework.database.Page;

/**
 * 外部产品
 * @author chenqichuan
 *
 */
public interface IProductForSealerService {
//	/**
//	 * @Description:翻页查询所有外部产品
//	 * @param product
//	 * @param pageNo
//	 * @param pageSize
//	 * @return
//	 */
//	public Page pageProductForSealer(ProductForSealer product,int pageNo, int pageSize);
//	
//	
//	/**
//	 * 
//	* @Title: getProductForSealerByCode
//	* @Description: 根据编码查询外部产品
//	* @param   code 编码 
//	* @return SaleContract    
//	* @throws
//	 */
//	public ProductForSealer getProductForSealerByCode(String code) throws Exception;
//	
//	/**
//	 * 
//	* @Title: saveProductForSealer
//	* @Description: 保存外部产品
//	* @param    product 外部产品
//	* @return void    
//	* @throws
//	 */
//	public void saveProductForSealer(ProductForSealer product) throws Exception;
//	
//	/**
//	 * 
//	* @Title: updateProductForSealer
//	* @Description: 修改外部产品
//	* @param    product 外部产品
//	* @return void    
//	* @throws
//	 */
//	public void updateProductForSealer(ProductForSealer product) throws Exception;
//	
//	
//	/**
//	 * 
//	* @Title: delSaleContract
//	* @Description: 删除外部产品
//	* @param    code 外部产品编码
//	* @return void    
//	* @throws
//	 */
//	public void delProductForSealer(String code) throws Exception;
//	
//	/**
//	 * 
//	* @Title: getLanguageJson
//	* @Description: 获得产品序列号语言包
//	* @param    code 外部产品编码
//	* @return String    
//	* @throws
//	 */
//	public String getLanguageJson(String code) throws Exception;
//	
//	/**
//	 * 
//	* @Title: getProductTypeSerialList
//	* @Description: 获得产品序列号图片信息
//	* @param    code 外部产品编码
//	* @return String    
//	* @throws
//	 */
//	public List<ProductTypeSerialPicVo> getProductTypeSerialList(String code)
//			throws Exception;
//	
//	
//	/**
//	 * 
//	* @Title: getProductForSealerList
//	* @Description: 获得所有外部产品
//	* @param    
//	* @return List<ProductForSealer>   
//	* @throws
//	 */
//	public List<ProductForSealer> getProductForSealerList()
//			throws Exception;
	
	/**
	 * 
	* @Title: getProductForSealer908
	* @Description: 获得20130915上线产品
	* @param    
	* @return List<ProductForSealer>   
	* @throws
	 */
	public List<ProductForSealer> getProductForSealer908()
			throws Exception;
	
	/**
	 * 
	* @Title: getProductForSealer908
	* @Description: 获得20130915上线产品
	* @param    
	* @return List<ProductForSealer>   
	* @throws
	 */
	public List<ProductForSealer> getProductForSealer();
//	
//	/**
//	 * 
//	* @Title: getProductForSealerCfgList
//	* @Description: 根据外部产品编码获取内部产品
//	* @param   productForSealerCode：内部产品编码
//	* @return List<ProductForSealerConfig>   
//	* @throws
//	 */
//	public List<ProductForSealerConfig> getProductForSealerCfgList(String productForSealerCode)
//			throws Exception;
//	
//	
//	/**
//	 * 
//	* @Title: getProductForSealerChange
//	* @Description: 经销商更换客户语言产品
//	* @param    
//	* @return List<ProductForSealer>   
//	* @throws
//	 */
//	public List<ProductForSealer> getProductForSealerChange()
//			throws Exception;
}
