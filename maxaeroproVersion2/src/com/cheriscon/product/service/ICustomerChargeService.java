package com.cheriscon.product.service;

import com.cheriscon.user.model.ReChargeCardVo;

public interface ICustomerChargeService {
	
	/**
	 * 客户升级卡操作
	 * @param reChargeCardVo
	 * @return
	 */
	public String reChargeCardOperation(ReChargeCardVo reChargeCardVo);

}
