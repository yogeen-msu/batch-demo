package com.cheriscon.product.service.impl;

//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;
//import java.util.Set;

//import javax.annotation.Resource;

//import net.sf.json.JSONArray;

//import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.product.model.ProductForSealer;
import com.cheriscon.product.service.IProductForSealerService;
//
//import com.cheriscon.backstage.market.mapper.MinSaleUnitVoMapper;
//import com.cheriscon.backstage.market.service.IProductForSealerService;
//import com.cheriscon.backstage.market.vo.ProductForSealer;
//import com.cheriscon.backstage.market.vo.ProductForSealerConfig;
//import com.cheriscon.backstage.product.vo.ProductTypeSerialPicVo;
//import com.cheriscon.backstage.system.service.ILanguageService;
//import com.cheriscon.common.model.Language;
//import com.cheriscon.common.model.MinSaleUnit;
//import com.cheriscon.common.model.ProductType;
//import com.cheriscon.common.utils.JSONHelpUtils;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.framework.util.StringUtil;

@SuppressWarnings("rawtypes")
@Service
public class ProductForSealerServiceImpl  implements IProductForSealerService {
//
//	private final static String TABLE_NAME = "DT_ProductForSealer";
//	
//	private final static String TABLE_NAME_CFG = "DT_ProductForSealerConfig";
//	@Resource
//	private ILanguageService languageService;
//	@Override
//	public Page pageProductForSealer(ProductForSealer product, int pageNo,
//			int pageSize) {
//		StringBuffer sql=new StringBuffer();
//		sql.append("select a.code as code, a.name as name,a.picPath as picPath  from DT_ProductForSealer a ");
//		sql.append(" where 1=1");
//		if(null != product){
//			
//			if(StringUtils.isNotBlank(product.getName())){
//				sql.append(" and a.name like '%").append(product.getName()).append("%'");
//			}
//		}
//		sql.append(" order by a.name asc");
//		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
//	}
//
//	@Override
//	public ProductForSealer getProductForSealerByCode(String code)
//			throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from ").append(TABLE_NAME).append(" where code=?");
//		return (ProductForSealer) this.daoSupport.queryForObject(sql.toString(), ProductForSealer.class, code);
//	}
//
//	@Override
//	public void saveProductForSealer(ProductForSealer product) throws Exception {
//		
//		this.daoSupport.insert(TABLE_NAME, product);
//		String art[]=product.getProTypeCodeStr().split(",");
//		for(int i=0;i<art.length;i++){
//			if(StringUtils.isNotBlank(art[i])){
//			ProductForSealerConfig cfg=new ProductForSealerConfig();
//			cfg.setProductForSealerCode(product.getCode());
//			cfg.setProductTypeCode(art[i]);
//			this.daoSupport.insert(TABLE_NAME_CFG, cfg);
//			}
//		}
//	}
//
//	@Override
//	public void updateProductForSealer(ProductForSealer product)
//			throws Exception {
//		StringBuffer del=new StringBuffer();
//		del.append("delete from DT_ProductForSealerConfig where productForSealerCode=?");
//		this.daoSupport.execute(del.toString(), product.getCode());
//		this.daoSupport.update(TABLE_NAME, product, " code="+"'"+product.getCode()+"'");
//		String art[]=product.getProTypeCodeStr().split(",");
//		for(int i=0;i<art.length;i++){
//			if(StringUtils.isNotBlank(art[i])){
//			ProductForSealerConfig cfg=new ProductForSealerConfig();
//			cfg.setProductForSealerCode(product.getCode());
//			cfg.setProductTypeCode(art[i]);
//			this.daoSupport.insert(TABLE_NAME_CFG, cfg);
//			}
//		}
//		
//	}
//
//	@Override
//	public void delProductForSealer(String code) throws Exception {
//		StringBuffer del=new StringBuffer();
//		del.append("delete from DT_ProductForSealerConfig where productForSealerCode=?");
//		this.daoSupport.execute(del.toString(), code);
//		
//		StringBuffer sql = new StringBuffer();
//		sql.append("delete from DT_ProductForSealer where code=?");
//		this.daoSupport.execute(sql.toString(), code);
//
//	}
//	@Override
//	public String getLanguageJson(String code) throws Exception{
//		List<Language> languages = languageService.queryLanguage();
//		
//		Map<String, Language> map = this.languageListToMap(languages);
//		
//		//map为空，则说明所有语言已经添加
//		if(map.size() == 0){
//			return null;
//		}
//		
//		List<Language> tLanguages = new ArrayList<Language>();
//		Set<String> keys = map.keySet();
//		for(String key : keys){
//			tLanguages.add(map.get(key));
//		}
//		return JSONArray.fromObject(tLanguages).toString();
//	}
//	
//	/**
//	 * 语言list转为map
//	 * @param languages	语言集合
//	 * @return	返回code为键、语言为值的map对象
//	 * @throws Exception
//	 */
//	public Map<String, Language> languageListToMap(List<Language> languages)
//			throws Exception {
//		Map<String, Language> map = new HashMap<String, Language>();
//		for (Language language : languages) {
//			map.put(language.getCode(), language);
//		}
//		return map;
//	}
//	
//	public List<ProductTypeSerialPicVo> getProductTypeSerialList(String code) throws Exception{
//		ProductForSealer productType = this.getProductForSealerByCode(code);
//		if(StringUtils.isBlank(productType.getSerialPath())){
//			return null;
//		}
////		List list = JSONHelpUtils.jsonToList(productType.getProSerialPicPath(),
////				ProductTypeSerialPicVo.class.getName());
//		List list = new ArrayList();
//		try{
//			list = JSONHelpUtils.getList4Json(productType.getSerialPath(),ProductTypeSerialPicVo.class);
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
//		if(list.size() == 0 ){
//			return list;
//		}else{
//			//设置语言名称
//			List<Language> languages = languageService.queryLanguage();
//			Map<String, Language> map = this.languageListToMap(languages);
//			for (Object o : list) {
//				ProductTypeSerialPicVo t = (ProductTypeSerialPicVo) o;
//				Language language = map.get(t.getLanguageCode());
//				t.setLanguageName(language.getName());
//			}
//			
//		}
//		return list;
//	}
//	@Override
//	public List<ProductForSealer> getProductForSealerList() throws Exception{
//		String sql="select * from DT_ProductForSealer order by name asc";
//		return this.daoSupport.queryForList(sql, ProductForSealer.class);
//	}
	@Override
	public List<ProductForSealer> getProductForSealer908()
			throws Exception{
//		String sql="select a.* from DT_ProductForSealer a,DT_ProductForSealerShow b where a.code=b.code order by name asc";
//		return this.daoSupport.queryForList(sql, ProductForSealer.class);
		List<ProductForSealer> productForSealers = (List<ProductForSealer>) HttpUtil.getReqeuestContentList(
				"rest.product.for.sealer908", null, ProductForSealer.class);
		return productForSealers;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProductForSealer> getProductForSealer(){
		
		List<ProductForSealer> productForSealers = (List<ProductForSealer>) HttpUtil.getReqeuestContentList(
				"rest.product.getProductForSealer", null, ProductForSealer.class);
		return productForSealers;
	}
//	
//	@Override
//	public List<ProductForSealerConfig> getProductForSealerCfgList(String productForSealerCode)
//			throws Exception{
//	    StringBuffer sql=new StringBuffer();
//	    sql.append("select b.code as productTypeCode,b.name as productTypeName from DT_ProductForSealerConfig a,DT_ProductType b where a.productTypeCode=b.code and a.productForSealerCode=?");
//		return this.daoSupport.queryForList(sql.toString(), ProductForSealerConfig.class, productForSealerCode);
//	}
//	@Override
//	public List<ProductForSealer> getProductForSealerChange()
//			throws Exception{
//		    StringBuffer sql=new StringBuffer();
//		    sql.append("  select a.code ,a.name from DT_ProductForSealer a,DT_SealerProductChange b where a.code=b.proTypeCode order by a.name asc");
//			return this.daoSupport.queryForList(sql.toString(), ProductForSealer.class);
//	}
}
