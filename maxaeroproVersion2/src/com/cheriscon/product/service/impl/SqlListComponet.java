package com.cheriscon.product.service.impl;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Component;

import com.cheriscon.common.utils.HttpUtil;

@Component
public class SqlListComponet {
	
	public JSONArray list (String sql,String localParam ,String languageCode){
		Map<String, String> map = new HashMap<String, String>();
		sql = sql.substring(sql.indexOf("from"));
		map.put("sql", sql);
		map.put("localParam", localParam);
		map.put("languageCode", languageCode);
		String json = HttpUtil.postRequestContent("rest.sql.list", map);
		try{
		return JSONArray.fromObject(json);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
