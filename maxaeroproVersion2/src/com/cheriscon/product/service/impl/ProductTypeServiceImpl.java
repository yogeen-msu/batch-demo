package com.cheriscon.product.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.product.service.IProductTypeService;

@Service
public class ProductTypeServiceImpl implements IProductTypeService {

	public int queryProductTypeCount(String sealerCode) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.sale.queryProductTypeCount", "sealerCode=" + sealerCode);
		return Integer.parseInt(jsonData);
	}

}
