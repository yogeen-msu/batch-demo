package com.cheriscon.product.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.product.service.ICustomerChargeService;
import com.cheriscon.user.model.ReChargeCardVo;

@Component
public class CustomerChargeServiceImpl implements ICustomerChargeService {

	@Override
	public String reChargeCardOperation(ReChargeCardVo reChargeCardVo) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("reChargeCardVo.proSerialNo", reChargeCardVo.getProSerialNo());
		map.put("reChargeCardVo.reChargeCardPassword", reChargeCardVo.getReChargeCardPassword());
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
        CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	

		map.put("customerInfoCode", customerInfo.getCode());
		map.put("autelId", customerInfo.getAutelId());
		
		return HttpUtil.postRequestContent("rest.user.reChargeCardOperation", map);
	}

}
