package com.cheriscon.product.service.impl;

import java.util.List;
//import java.util.Locale;
import java.util.Map;

//import javax.servlet.http.HttpServletRequest;
//
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.cheriscon.backstage.product.service.ILanguagePackService;
//import com.cheriscon.backstage.product.vo.SoftwareName;
//import com.cheriscon.backstage.product.vo.VehicleTypeMapper;
//import com.cheriscon.common.constant.DBConstant;
//import com.cheriscon.common.model.Language;
//import com.cheriscon.common.model.LanguagePack;
//import com.cheriscon.common.model.ProductType;
//import com.cheriscon.common.model.SoftwareType;
//import com.cheriscon.common.model.SoftwareVersion;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.cop.sdk.context.CopContext;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.common.model.VehicleType;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.framework.database.Page;
//import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.product.service.ILanguagePackService;

/**
 * @Description: 软件语言包业务类
 * @author shaohu
 * @date 2013-1-10 下午07:44:04
 * 
 */
@Service
public class LanguagePackServiceImpl  implements ILanguagePackService {
//
//	private final static String TABLE_NAME = "DT_LanguagePack";
//	
////	@Resource private ILanguageService languageService;
//	
//	/**
//	  * <p>Title: listAll</p>
//	  * <p>Description: 查询软件语言包的集合</p>
//	  * @return
//	  * @throws Exception
//	  */ 
//	@Override
//	public List<LanguagePack> listAll() throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from ").append(TABLE_NAME);
//		return this.daoSupport.queryForList(sql.toString(), LanguagePack.class);
//	}
	
	public Page searchPage(Map<String,Object> params,int pageNo,int pageSize) throws Exception {
		StringBuilder pa = new StringBuilder();
		pa.append("pageNo=").append(pageNo);
		pa.append("&pageSize=").append(pageSize);
		if(params.get("softName")!=null){
			pa.append("&softName=").append(StringUtils.trim((String)params.get("softName")));
		}
		if(params.get("proTypeCode")!=null){
			pa.append("&proTypeCode=").append(params.get("proTypeCode"));
		}
		
		Page page = HttpUtil.getReqeuestContentPage("rest.languagePack.searchPage", pa.toString(), VehicleType.class);
		
		return page;
	}
//
//	/**
//	  * <p>Title: addLanguagePack</p>
//	  * <p>Description: 添加软件语言包</p>
//	  * @param languagePack
//	  * @throws Exception
//	  */ 
//	@Override
//	@Transactional
//	public void addLanguagePack(LanguagePack languagePack) throws Exception {
//		String code = DBUtils.generateCode(DBConstant.LANGUAGE_PACK_CODE);
//		languagePack.setCode(code);
//		this.daoSupport.insert(TABLE_NAME, languagePack);
//	}
//
//	/**
//	  * <p>Title: delLanguagePack</p>
//	  * <p>Description: 删除软件语言包</p>
//	  * @param code
//	  * @throws Exception
//	  */ 
//	@Override
//	@Transactional
//	public void delLanguagePack(String code) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("delete from ").append(TABLE_NAME).append(" where code = ?");
//		this.daoSupport.execute(sql.toString(),code);
//	}
//
//	/**
//	  * <p>Title: delLanguagePackByVersionCode</p>
//	  * <p>Description: </p>
//	  * @param versionCode
//	  * @throws Exception
//	  */ 
//	@Override
//	public void delLanguagePackByVersionCode(String versionCode)
//			throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("delete from ").append(TABLE_NAME).append( " where softwareVersionCode = ?");
//		this.daoSupport.execute(sql.toString(),versionCode);
//	}
//
//	/**
//	  * <p>Title: updateLanguagePack</p>
//	  * <p>Description: </p>
//	  * @param languagePack
//	  * @throws Exception
//	  */ 
//	@Override
//	public void updateLanguagePack(LanguagePack languagePack) throws Exception {
//		this.daoSupport.update(TABLE_NAME, languagePack, " code='"+languagePack.getCode().trim()+"'");
//	}
//
//	/**
//	  * <p>Title: getLanguagePackByVersionCode</p>
//	  * <p>Description: </p>
//	  * @param versionCode
//	  * @throws Exception
//	  */ 
//	@Override
//	public List<LanguagePack> getLanguagePackByVersionCode(String versionCode) throws Exception {
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from ").append(TABLE_NAME).append(" where softwareVersionCode = ?");;
//		return this.daoSupport.queryForList(sql.toString(), LanguagePack.class,versionCode);
//	}
//	

}
