package com.cheriscon.product.model;

import java.io.Serializable;
import java.util.UUID;

public class ProductTypeSerialPicVo implements Serializable{
	
	private static final long serialVersionUID = 7229267630588454738L;
	
	//图片id
	private String picId;
	//产品型号code
	private String code;
	//语言code
	private String languageCode;
	//语言名称
	private String languageName;
	//图片路径
	private String picPath;
	
	public ProductTypeSerialPicVo() {
		this.picId = UUID.randomUUID().toString().replaceAll("-", "");
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getPicPath() {
		return picPath;
	}
	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getPicId() {
		return picId;
	}
	public void setPicId(String picId) {
		this.picId = picId;
	}
}
