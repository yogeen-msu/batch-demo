package com.cheriscon.ofn.service;

import java.util.List;

import com.cheriscon.framework.database.Page;
import com.cheriscon.ofn.model.OldForNew;
import com.cheriscon.ofn.model.vo.OldForNewVo;

public interface IOldForNewSerice {

	/**
	 * 添加以旧换新
	 * 
	 * @param oldForNew
	 * @return
	 */
	public boolean saveOldForNew(OldForNew oldForNew);

	/**
	 * 修改以旧换新
	 * 
	 * @param oldForNew
	 * @return
	 */
	public boolean updateOldForNew(OldForNew oldForNew);

	/**
	 * 删除以旧换新
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteOldForNew(int id);

	/**
	 * 修改以旧换新状态
	 * 
	 * @param id
	 * @param state
	 * @return
	 */
	public boolean updateState(int id, int state);

	/**
	 * 通过ID查询
	 * 
	 * @param id
	 * @return
	 */
	public OldForNew get(int id);

	/**
	 * 翻页查看
	 * 
	 * @param oldForNew
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageOldForNew(OldForNew oldForNew, int pageNo, int pageSize);

	/**
	 * 通过新产品序列号查询参与的活动
	 * 
	 * @param proSerialNo
	 * @return
	 */
	public OldForNew getByProSerialNo(String proSerialNo);

	public List<OldForNew> getByProSerialNos(String proSerialNos);

	/**
	 * 通过新产品序列号查询参与的活动
	 * 
	 * @param proSerialNo
	 * @return
	 */
	public OldForNew getByProSerialNo1(String proSerialNo);

	/**
	 * 通过旧产品序列号查询参与的活动
	 * 
	 * @param oProSerialNo
	 * @return
	 */
	public OldForNew getByOProSerialNo(String oProSerialNo);

	public Page page(OldForNewVo oldForNewVo, int pageNo, int pageSize);
	
	public Page page(OldForNewVo oldForNewVo);

}
