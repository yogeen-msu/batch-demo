package com.cheriscon.ofn.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.ofn.model.OldForNew;
import com.cheriscon.ofn.model.vo.OldForNewVo;
import com.cheriscon.ofn.service.IOldForNewSerice;

@Service
public class OldForNewSericeImpl implements IOldForNewSerice {

	public boolean saveOldForNew(OldForNew oldForNew) {
//		String tableName = DBUtils.getTableName(OldForNew.class);
////		oldForNew.setCode(DBUtils.generateCode(DBConstant.PRODUCTINFO_CODE));
//		if(StringUtils.isEmpty(oldForNew.getCode())||getByCode(oldForNew.getCode())!=null){
//			oldForNew.setCode(DBUtils.generateCode("OFN"));
//		}
//		String now = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
//		oldForNew.setCreateTime(now);
//		oldForNew.setLastTime(now);
//		this.daoSupport.insert(tableName, oldForNew);
		return false;
	}

	public boolean updateOldForNew(OldForNew oldForNew) {
		HttpUtil.postRequestContent("rest.sealer.ofn.updateOldForNew", "oldForNew.", oldForNew);
		return true;
//		String oldForNewTbl = DBUtils.getTableName(OldForNew.class);
//		StringBuffer sql = new StringBuffer("update "  +  oldForNewTbl + " set ");
//		sql.append(" state=? ,logisticsCompany=?, logisticsNo=?,logisticsTime=?");
//		sql.append(" ,reasons=? ,receivedDate=?");
//		List<Object> params = new ArrayList<Object>();
//		params.add(oldForNew.getState());
//		params.add(oldForNew.getLogisticsCompany());
//		params.add(oldForNew.getLogisticsNo());
//		params.add(oldForNew.getLogisticsTime());
//		params.add(oldForNew.getReasons());
//		params.add(oldForNew.getReceivedDate());
//		if(StringUtils.isNotEmpty(oldForNew.getMailAddress())){
//			sql.append(",mailAddress=?");
//			params.add(oldForNew.getMailAddress());
//		}
//		if(StringUtils.isNotEmpty(oldForNew.getConsignee())){
//			sql.append(",consignee=?");
//			params.add(oldForNew.getConsignee());
//		}
//		if(StringUtils.isNotEmpty(oldForNew.getPhone())){
//			sql.append(",phone=?");
//			params.add(oldForNew.getPhone());
//		}
//		if(oldForNew.getGiftType()!=null){
//			sql.append(",giftType=?");
//			params.add(oldForNew.getGiftType());
//			sql.append(",giftProTypeCode=?");
//			params.add(oldForNew.getGiftProTypeCode());
//		}
//		sql.append(" where id=?");
//		params.add(oldForNew.getId());
//		 this.daoSupport.execute(sql.toString(),params.toArray());
//		 return true;
	}

	public boolean deleteOldForNew(int id) {
		return false;
	}

	public boolean updateState(int id, int state) {
		return false;
	}

	public OldForNew get(int id) {
		return (OldForNew) HttpUtil.getReqeuestContentObject("rest.sealer.ofn.get", "id=" + id, OldForNew.class);
//		String oldForNewTbl = DBUtils.getTableName(OldForNew.class);
//		String productForSealerTbl = DBUtils.getTableName(ProductForSealer.class);
//		StringBuffer sql = new StringBuffer("select ofn.* ,pt1.name as proTypeName,ofn.oproductTypeCode as oproTypeName from "  +  oldForNewTbl + " ofn ");
//		sql.append(" LEFT JOIN "+productForSealerTbl+" pt1 on pt1.code = ofn.productTypeCode ");
//		sql.append(" where ofn.id=? ");
//		return this.daoSupport.queryForObject(sql.toString(), OldForNew.class, id);
	}

	public Page pageOldForNew(OldForNew oldForNew, int pageNo, int pageSize) {
		return null;
	}

	public OldForNew getByProSerialNo(String proSerialNo) {
		return null;
//		String oldForNewTbl = DBUtils.getTableName(OldForNew.class);
//		StringBuffer sql = new StringBuffer("select ofn.* from "  +  oldForNewTbl + " ofn ");
//		sql.append(" where ofn.serialNo=? ");
//		return this.daoSupport.queryForObject(sql.toString(), OldForNew.class, proSerialNo);
	}

	public OldForNew getByOProSerialNo(String oProSerialNo) {
		return null;
//		String oldForNewTbl = DBUtils.getTableName(OldForNew.class);
//		StringBuffer sql = new StringBuffer("select ofn.* from "  +  oldForNewTbl + " ofn ");
//		sql.append(" where ofn.oSerialNo=? ");
//		return this.daoSupport.queryForObject(sql.toString(), OldForNew.class, oProSerialNo);
	}

	public OldForNew getByProSerialNo1(String proSerialNo) {
		return null;
//		String oldForNewTbl = DBUtils.getTableName(OldForNew.class);
//		String productForSealerTbl = DBUtils.getTableName(ProductForSealer.class);
//		StringBuffer sql = new StringBuffer("select ofn.* ,pt1.name as proTypeName,ofn.oproductTypeCode as oproTypeName from "  +  oldForNewTbl + " ofn ");
//		sql.append(" LEFT JOIN "+productForSealerTbl+" pt1 on pt1.code = ofn.productTypeCode ");
//		sql.append(" where ofn.serialNo=? ");
//		return this.daoSupport.queryForObject(sql.toString(), OldForNew.class, proSerialNo);
	}

	public List<OldForNew> getByProSerialNos(String proSerialNos) {
		return null;
//		String oldForNewTbl = DBUtils.getTableName(OldForNew.class);
//		StringBuffer sql = new StringBuffer("select ofn.* from "  +  oldForNewTbl + " ofn ");
//		sql.append(" where ofn.serialNo in ( ");
//		proSerialNos = "'"+proSerialNos.replace(",", "','")+"'";
//		sql.append(proSerialNos+") ");
//		return this.daoSupport.queryForList(sql.toString(), OldForNew.class);
	}
	
	public Page page(OldForNewVo oldForNewVo) {
		String pageStr = HttpUtil.postRequestContent("rest.sealer.ofn.page", "oldForNewVo.", oldForNewVo);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", OldForNew.class);
		Page page = (Page) JsonUtil.getDTO(pageStr, Page.class, map);
		return page;
	}

	public Page page(OldForNewVo oldForNewVo, int pageNo, int pageSize) {
		return null;
//		String oldForNewTbl = DBUtils.getTableName(OldForNew.class);
//		String productForSealerTbl = DBUtils.getTableName(ProductForSealer.class);
//		String productinfoTbl = DBUtils.getTableName(ProductInfo.class);
//		String saleContractTbl = DBUtils.getTableName(SaleContract.class);
//		StringBuffer sql = new StringBuffer("select ofn.* ,pt1.name as proTypeName,ofn.oproductTypeCode as oproTypeName from "  +  oldForNewTbl + " ofn ");
//		sql.append(" LEFT JOIN "+productForSealerTbl+" pt1 on pt1.code = ofn.productTypeCode ");
//		sql.append(" left join "+productinfoTbl+" pi on pi.serialNo = ofn.serialNo ");
//		sql.append(" left join "+saleContractTbl+" scr on scr.code = pi.saleContractCode ");
//		sql.append(" where 1=1 ");
//		if(oldForNewVo!=null){
//			if(StringUtils.isNotEmpty(oldForNewVo.getSealerCode())){
//				sql.append(" and scr.sealerCode = '"+oldForNewVo.getSealerCode()+"'");
//			}
//			if(StringUtils.isNotEmpty(oldForNewVo.getAutelId())){
//				sql.append(" and ofn.autelId like '%"+oldForNewVo.getAutelId()+"%'");
//			}
//			if(StringUtils.isNotEmpty(oldForNewVo.getSerialNo())){
//				sql.append(" and ofn.serialNo like '%"+oldForNewVo.getSerialNo()+"%'");
//			}
//			if(StringUtils.isNotEmpty(oldForNewVo.getCode())){
//				sql.append(" and ofn.code like '%"+oldForNewVo.getCode().toUpperCase()+"%'");
//			}
//		}
//		sql.append(" order by createTime desc");
//		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, OldForNew.class);
	}
	private OldForNew getByCode(String code){
		return null;
//		String oldForNewTbl = DBUtils.getTableName(OldForNew.class);
//		StringBuffer sql = new StringBuffer("select ofn.* from "  +  oldForNewTbl + " ofn ");
//		sql.append(" where ofn.code=? ");
//		return this.daoSupport.queryForObject(sql.toString(), OldForNew.class, code);
	}

}
