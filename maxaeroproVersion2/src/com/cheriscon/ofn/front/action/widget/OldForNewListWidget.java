package com.cheriscon.ofn.front.action.widget;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.RequestUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.ofn.model.vo.OldForNewVo;
import com.cheriscon.ofn.service.IOldForNewSerice;

@Component
public class OldForNewListWidget extends AbstractWidget {

	@Resource
	private IOldForNewSerice oldForNewSerice;
	@Resource
	private ISealerDataAuthDeailsService sealerDataAuthDeailsService;
	@Resource
	private ISealerInfoService sealerInfoService;
	
	
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
        SealerInfo sealerInfo=(SealerInfo)SessionUtil.getLoginUserInfo(request);
        String pageSize = params.get("pageSize");
		String pageNoStr = params.get("pageno");
		Integer[] ids= new Integer[]{1};
		
		HttpServletRequest httpRequest = ThreadContextHolder.getHttpRequest();
		String url = RequestUtil.getRequestUrl(httpRequest);
		String pattern = "/(.*)-(\\d+)-(\\d+).html(.*)";
		String page= null;
		String catid = null;
		Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
		Matcher m = p.matcher(url);
		if (m.find()) {
			page = m.replaceAll("$3");
			catid = m.replaceAll("$2");
			ids = new Integer[]{Integer.valueOf(""+page),Integer.valueOf(""+catid)};
		} 
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		String sealerCode = sealerInfo.getCode();
//		try {
//			SealerDataAuthDeails sealerDataAuthDeails = sealerDataAuthDeailsService.getDataAuthBySealerCode(sealerInfo.getAutelId());
////			authCode 含有，表示属于某个经销商下的账号
//			if(sealerDataAuthDeails!=null){
//				String autelId = sealerDataAuthDeails.getSealerCode();
//				if(sealerDataAuthDeails.getAuthCode().startsWith("0063")){
//					autelId = "C-0063";
//				}else if(sealerDataAuthDeails.getAuthCode().indexOf(",")>0){
//					sealerDataAuthDeails = sealerDataAuthDeailsService.getDataAuth(sealerDataAuthDeails.getAuthCode().substring(0,sealerDataAuthDeails.getAuthCode().indexOf(",")));
//					autelId = sealerDataAuthDeails.getSealerCode();
//				}
//				SealerInfo tem = sealerInfoService.getSealerByAutelId(autelId);
//				if(tem!=null){
//					sealerCode = tem.getCode();
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		String serialNo = params.get("serialNo");
		String autelId = params.get("autelId");
		String code = params.get("code");
		OldForNewVo oldForNewVo = new OldForNewVo();
		oldForNewVo.setSealerCode(sealerCode);
		oldForNewVo.setCode(code);
		oldForNewVo.setSerialNo(serialNo);
		oldForNewVo.setFrontAutelId(autelId);
		oldForNewVo.setAutelId(sealerInfo.getAutelId());
		oldForNewVo.setPageNo(pageNo);
		oldForNewVo.setPageSize(Integer.parseInt(pageSize));
//		Page dataPage =  oldForNewSerice.page(oldForNewVo, pageNo, Integer.parseInt(pageSize));
		Page dataPage = oldForNewSerice.page(oldForNewVo);
		StaticPagerHtmlBuilder pagerHtmlBuilder = new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
		String page_html = pagerHtmlBuilder.buildPageHtml();
		long totalPageCount = dataPage.getTotalPageCount();
		long totalCount = dataPage.getTotalCount();
		this.putData("pager", page_html); 
		this.putData("pagesize", pageSize);
		this.putData("pageno", pageNo);
		this.putData("totalcount", totalCount);
		this.putData("totalpagecount",totalPageCount);
		this.putData("oldForNewList", dataPage.getResult());
		this.putData("serialNo",serialNo);
		this.putData("autelId",autelId);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

}
