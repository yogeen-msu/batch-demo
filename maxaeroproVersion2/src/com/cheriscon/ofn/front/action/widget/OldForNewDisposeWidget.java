package com.cheriscon.ofn.front.action.widget;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.ofn.model.OldForNew;
import com.cheriscon.ofn.service.IOldForNewSerice;

@Component("oldForNewDisposeWidget")
public class OldForNewDisposeWidget extends AbstractWidget {

	@Resource
	private IOldForNewSerice oldForNewSerice;
	
	@Override
	protected void display(Map<String, String> params) {
		OldForNew oldForNew = oldForNewSerice.get(Integer.valueOf(params.get("id")));
		this.putData("oldForNew", oldForNew);
		this.putData("now", DateUtil.toString(new Date(), "dd/MM/yyyy"));
	}


	@Override
	protected void config(Map<String, String> params) {

	}

}
