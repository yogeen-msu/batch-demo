package com.cheriscon.ofn.model;

import java.io.Serializable;


@SuppressWarnings("serial")
public class OldForNew implements Serializable {


	private Integer id;
	private String code;
	private String serialNo; // 产品序列号
	private String productTypeCode; // 产品型号Code
	private String autelId; // 客户ID
	private String oserialNo; // 旧产品序列号
	private String oproductTypeCode; // 旧产品型号Code
	private Integer state; // 状态 1.待处理 2处理中 3 验证未通过 4已发货 5已完成
	private Integer giftType; // 赠品类型 1换产品 2 现金
	private String giftProTypeCode; // 赠品 商品分类
	private String mailAddress; // 邮寄地址
	private String createTime; // 参与时间
	private String lastTime; // 最后处理时间
	private String lastUser; // 最后处理人
	private String logisticsCompany; // 物流公司
	private String logisticsNo; // 物流编号
	private String logisticsTime; // 物流时间
	private String consignee; // 收货人
	private String phone; // 联系电话
	private String receivedDate; // 接货时间
	private String reasons; // 验证未通过原因

	// 非数据库字段
	private String proTypeName; // 产品型号
	private String oproTypeName; // 旧产品型号

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getGiftType() {
		return giftType;
	}

	public void setGiftType(Integer giftType) {
		this.giftType = giftType;
	}

	public String getGiftProTypeCode() {
		return giftProTypeCode;
	}

	public void setGiftProTypeCode(String giftProTypeCode) {
		this.giftProTypeCode = giftProTypeCode;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public String getOserialNo() {
		return oserialNo;
	}

	public void setOserialNo(String oserialNo) {
		this.oserialNo = oserialNo;
	}

	public String getOproductTypeCode() {
		return oproductTypeCode;
	}

	public void setOproductTypeCode(String oproductTypeCode) {
		this.oproductTypeCode = oproductTypeCode;
	}

	public String getLastUser() {
		return lastUser;
	}

	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}

	public String getLogisticsCompany() {
		return logisticsCompany;
	}

	public void setLogisticsCompany(String logisticsCompany) {
		this.logisticsCompany = logisticsCompany;
	}

	public String getLogisticsNo() {
		return logisticsNo;
	}

	public void setLogisticsNo(String logisticsNo) {
		this.logisticsNo = logisticsNo;
	}

	public String getLogisticsTime() {
		return logisticsTime;
	}

	public void setLogisticsTime(String logisticsTime) {
		this.logisticsTime = logisticsTime;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	public String getOproTypeName() {
		return oproTypeName;
	}

	public void setOproTypeName(String oproTypeName) {
		this.oproTypeName = oproTypeName;
	}

	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}

}
