/**
*/ 
package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

/**
 * @ClassName: SaleContractVo
 * @Description: 销售契约业务实体类
 * @author shaohu
 * @date 2013-1-18 下午01:59:37
 * 
 */
public class SaleContractVo implements Serializable{

	private static final long serialVersionUID = 3583665418095945356L;
	private Integer id;
	private String code;
	private String name;
	private String sealerCode;					//经销商编号 

	/**
	 * 经销商类型code
	 */
	public String sealerTypeCode;
	
	/**
	 * 产品型号code
	 */
	public String proTypeCode;
	
	/**
	 * 续费价格
	 */
	private String reChargePrice;
	
	/**
	 * 区域配置code
	 */
	private String areaCfgCode;
	
	/**
	 * 语言配置code
	 */
	private String languageCfgCode;
	
	/**
	 * 标准销售配置code
	 */
	private String saleCfgCode;
	
	/**
	 * 最大销售配置
	 */
	private String maxSaleCfgCode;
	
	/**
	 * 经销商名称
	 */
	private String sealerName;
	
	/**
	 * 经销商类型名称
	 */
	private String sealerTypeName;
	
	/**
	 * 产品型号名称
	 */
	private String productTypeName;
	
	/**
	 * 区域配置名称
	 */
	private String areaName;
	
	/**
	 * 语言配置名称
	 */
	private String languageCfgName;
	
	/**
	 * 标准销售配置名称
	 */
	private String saleName;
	
	/**
	 * 最大销售配置名称
	 */
	private String maxSaleName;

	/**
	 * 免费升级月
	 */
	private Integer upMonth ;
	
	/**
	 * 契约时间
	 */
	private String contractDate;
	
	/**
	 * 过期时间
	 */
	private String expirationTime;
	
	
	/**
	 * 保修月份
	 */
	private Integer warrantyMonth;
	
	public Integer getWarrantyMonth() {
		return warrantyMonth;
	}

	public void setWarrantyMonth(Integer warrantyMonth) {
		this.warrantyMonth = warrantyMonth;
	}
	
	
	
	public SaleContractVo() {
		super();
	}

	/**
	 * @param id
	 * @param code
	 * @param name
	 * @param sealerCode
	 * @param sealerTypeCode
	 * @param proTypeCode
	 * @param reChargePrice
	 * @param areaCfgCode
	 * @param languageCfgCode
	 * @param saleCfgCode
	 * @param maxSaleCfgCode
	 * @param sealerName
	 * @param sealerTypeName
	 * @param productTypeName
	 * @param areaName
	 * @param languageCfgName
	 * @param saleName
	 * @param maxSaleName
	 * @param upMonth
	 */
	public SaleContractVo(Integer id, String code, String name, String sealerCode, String sealerTypeCode, String proTypeCode, String reChargePrice,
			String areaCfgCode, String languageCfgCode, String saleCfgCode, String maxSaleCfgCode, String sealerName, String sealerTypeName,
			String productTypeName, String areaName, String languageCfgName, String saleName, String maxSaleName, Integer upMonth) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.sealerCode = sealerCode;
		this.sealerTypeCode = sealerTypeCode;
		this.proTypeCode = proTypeCode;
		this.reChargePrice = reChargePrice;
		this.areaCfgCode = areaCfgCode;
		this.languageCfgCode = languageCfgCode;
		this.saleCfgCode = saleCfgCode;
		this.maxSaleCfgCode = maxSaleCfgCode;
		this.sealerName = sealerName;
		this.sealerTypeName = sealerTypeName;
		this.productTypeName = productTypeName;
		this.areaName = areaName;
		this.languageCfgName = languageCfgName;
		this.saleName = saleName;
		this.maxSaleName = maxSaleName;
		this.upMonth = upMonth;
	}

	/**
	 * @return the upMonth
	 */
	public Integer getUpMonth() {
		return upMonth;
	}

	/**
	 * @param upMonth the upMonth to set
	 */
	public void setUpMonth(Integer upMonth) {
		this.upMonth = upMonth;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public String getSealerTypeCode() {
		return sealerTypeCode;
	}

	public void setSealerTypeCode(String sealerTypeCode) {
		this.sealerTypeCode = sealerTypeCode;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getReChargePrice() {
		return reChargePrice;
	}

	public void setReChargePrice(String reChargePrice) {
		this.reChargePrice = reChargePrice;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public String getLanguageCfgCode() {
		return languageCfgCode;
	}

	public void setLanguageCfgCode(String languageCfgCode) {
		this.languageCfgCode = languageCfgCode;
	}

	public String getSaleCfgCode() {
		return saleCfgCode;
	}

	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}

	public String getMaxSaleCfgCode() {
		return maxSaleCfgCode;
	}

	public void setMaxSaleCfgCode(String maxSaleCfgCode) {
		this.maxSaleCfgCode = maxSaleCfgCode;
	}

	public String getSealerName() {
		return sealerName;
	}

	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}

	public String getSealerTypeName() {
		return sealerTypeName;
	}

	public void setSealerTypeName(String sealerTypeName) {
		this.sealerTypeName = sealerTypeName;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getLanguageCfgName() {
		return languageCfgName;
	}

	public void setLanguageCfgName(String languageCfgName) {
		this.languageCfgName = languageCfgName;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public String getMaxSaleName() {
		return maxSaleName;
	}

	public void setMaxSaleName(String maxSaleName) {
		this.maxSaleName = maxSaleName;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}
	
	
}
