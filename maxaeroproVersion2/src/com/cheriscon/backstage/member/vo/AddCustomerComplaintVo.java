package com.cheriscon.backstage.member.vo;

import java.io.Serializable;

public class AddCustomerComplaintVo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5376304927964626888L;
	
	private String userType;
	private String userCode;

	/**
	 * 投诉人
	 */
	private String userName;

	/**
	 * 单位
	 */
	private String company;

	/**
	 * 电话
	 */
	private String phone;

	private String daytimePhoneCC;
	private String daytimePhoneAC;
	private String daytimePhone;


	/**
	 * 邮箱
	 */
	private String emai;
	
	private String queryType; //查询汽车类型


	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getUserCode() {
		return userCode;
	}


	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getDaytimePhoneCC() {
		return daytimePhoneCC;
	}


	public void setDaytimePhoneCC(String daytimePhoneCC) {
		this.daytimePhoneCC = daytimePhoneCC;
	}


	public String getDaytimePhoneAC() {
		return daytimePhoneAC;
	}


	public void setDaytimePhoneAC(String daytimePhoneAC) {
		this.daytimePhoneAC = daytimePhoneAC;
	}


	public String getDaytimePhone() {
		return daytimePhone;
	}


	public void setDaytimePhone(String daytimePhone) {
		this.daytimePhone = daytimePhone;
	}


	public String getEmai() {
		return emai;
	}


	public void setEmai(String emai) {
		this.emai = emai;
	}


	public String getQueryType() {
		return queryType;
	}


	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	
	
}
