package com.cheriscon.backstage.member.vo;

import java.io.Serializable;

/**
 * @remark	客户注册产品信息业务对象
 * @author pengdongan
 * @date 2013-01-10
 *
 */
public class CustomerProductVo implements Serializable{

	private static final long serialVersionUID = 3467292625842957660L;
	
	private String code;				//客户信息code
	private String autelId;				//AutelId
	private String firstName;			//第一个名字
	private String middleName;			//中间名字
	private String lastName;			//最后名字
	private String name;				//用户名
	private String city;                //客户地区
	private String province; //省份
	
	
	private String saleContractCode;	//销售契约
	private String sealerCode;			//经销商code
	private String sealerAutelId;		//经销商AutelId
	private String areaCfgCode;			//区域配置Code
	private String areaCfgName;			//区域配置名称
	private String proCode;				//产品code
	private String proSerialNo;			//产品序列号
	private String proDate;				//出厂日期
	private String proRegTime;			//产品注册时间
	private Integer proRegStatus;		//绑定状态
	private String proNoRegReason;		//解绑原因
	private String validDate; //软件有效期
	private String warrantymonth; //硬件保修期
	private String language; //产品语言
	private String regPwd; //产品注册密码
	private String productTypeName ; //产品类型名称

	public String getProductTypeName() {
		return productTypeName;
	}
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getRegPwd() {
		return regPwd;
	}
	public void setRegPwd(String regPwd) {
		this.regPwd = regPwd;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getWarrantymonth() {
		return warrantymonth;
	}
	public void setWarrantymonth(String warrantymonth) {
		this.warrantymonth = warrantymonth;
	}
	public String getValidDate() {
		return validDate;
	}
	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	private String beginDate;			//开始日期
	
	private String endDate;				//结束日期
	
	private String daytimePhone;		//固定电话
	private String daytimePhoneCC;		//固定电话国家代码
	private String daytimePhoneAC;		//固定电话区域代码
	private String daytimePhoneExtCode;	//固定电话分机号
	private String mobilePhone;			//移动电话
	private String mobilePhoneCC;		//移动电话国家代码
	private String mobilePhoneAC;		//移动电话区域代码
	

	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDaytimePhone() {
		return daytimePhone;
	}
	public String getDaytimePhoneCC() {
		return daytimePhoneCC;
	}
	public String getDaytimePhoneAC() {
		return daytimePhoneAC;
	}
	public String getDaytimePhoneExtCode() {
		return daytimePhoneExtCode;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public String getMobilePhoneCC() {
		return mobilePhoneCC;
	}
	public String getMobilePhoneAC() {
		return mobilePhoneAC;
	}
	public void setDaytimePhone(String daytimePhone) {
		this.daytimePhone = daytimePhone;
	}
	public void setDaytimePhoneCC(String daytimePhoneCC) {
		this.daytimePhoneCC = daytimePhoneCC;
	}
	public void setDaytimePhoneAC(String daytimePhoneAC) {
		this.daytimePhoneAC = daytimePhoneAC;
	}
	public void setDaytimePhoneExtCode(String daytimePhoneExtCode) {
		this.daytimePhoneExtCode = daytimePhoneExtCode;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public void setMobilePhoneCC(String mobilePhoneCC) {
		this.mobilePhoneCC = mobilePhoneCC;
	}
	public void setMobilePhoneAC(String mobilePhoneAC) {
		this.mobilePhoneAC = mobilePhoneAC;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAutelId() {
		return autelId;
	}
	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSaleContractCode() {
		return saleContractCode;
	}
	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}
	public String getSealerCode() {
		return sealerCode;
	}
	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}
	public String getSealerAutelId() {
		return sealerAutelId;
	}
	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}
	public String getAreaCfgCode() {
		return areaCfgCode;
	}
	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}
	public String getAreaCfgName() {
		return areaCfgName;
	}
	public void setAreaCfgName(String areaCfgName) {
		this.areaCfgName = areaCfgName;
	}
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public String getProSerialNo() {
		return proSerialNo;
	}
	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}
	public String getProDate() {
		return proDate;
	}
	public void setProDate(String proDate) {
		this.proDate = proDate;
	}
	public String getProRegTime() {
		return proRegTime;
	}
	public void setProRegTime(String proRegTime) {
		this.proRegTime = proRegTime;
	}
	public Integer getProRegStatus() {
		return proRegStatus;
	}
	public void setProRegStatus(Integer proRegStatus) {
		this.proRegStatus = proRegStatus;
	}
	public String getProNoRegReason() {
		return proNoRegReason;
	}
	public void setProNoRegReason(String proNoRegReason) {
		this.proNoRegReason = proNoRegReason;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
