package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.common.model.CustomerComplaintTypeName;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;

@Service
public class CustomerComplaintTypeNameServiceImpl implements ICustomerComplaintTypeNameService {

	public List<CustomerComplaintTypeName> listAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerComplaintTypeName> listComplaintTypeNameByLanguageCode(
			String languageCode) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent(
				"rest.complaint.typeName", "languageCode=" + languageCode);
		List<CustomerComplaintTypeName> names = 
				JsonUtil.getDTOList(jsonData, CustomerComplaintTypeName.class);
		return names;
	}

	@Override
	public List<CustomerComplaintTypeName> queryByTypeCode(String typeCode)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int insertCustomerComplaintTypeName(
			CustomerComplaintTypeName customerComplaintTypeName)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateCustomerComplaintTypeName(
			CustomerComplaintTypeName customerComplaintTypeName)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateCustomerComplaintTypeNames(
			List<CustomerComplaintTypeName> customerComplaintTypeNames,
			List<CustomerComplaintTypeName> delCustomerComplaintTypeNames)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CustomerComplaintTypeName getCustomerComplaintTypeNameByCode(
			String code) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerComplaintTypeName getCustomerComplaintTypeName(
			String complaintTypeCode, String languageCode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delCustomerComplaintTypeName(int id) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delCustomerComplaintTypeNameByTypeCode(String complaintTypeCode)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Page pageCustomerComplaintTypeNamePage(
			CustomerComplaintTypeName customerComplaintTypeName, int pageNo,
			int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Language> getAvailableLanguage(String typeCode)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
