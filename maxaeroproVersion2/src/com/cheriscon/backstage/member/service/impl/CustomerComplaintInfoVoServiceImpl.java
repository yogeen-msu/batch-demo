package com.cheriscon.backstage.member.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.database.Page;


/**
 * 客诉信息业务逻辑实现类
 * @author pengdongan
 * @date 2013-01-18
 */
@Service
public class CustomerComplaintInfoVoServiceImpl implements ICustomerComplaintInfoVoService
{

	
	@SuppressWarnings("unchecked")
	public List<CustomerComplaintInfoVo> pageCustomerComplaintInfoPage(CustomerComplaintInfoVo customerComplaintInfoVo) throws Exception{
//		StringBuffer sql = new StringBuffer();
//		sql.append("select a.*,CASE WHEN a.complaintState=4 then  HOUR(TIMEDIFF(a.closeDate,a.complaintDate)) else HOUR(TIMEDIFF(NOW(),a.complaintDate)) end as hoursBetween");
//		sql.append(" from DT_CustomerComplaintInfo a");
//		sql.append(" where 1=1");
//		
//		if (null != customerComplaintInfoVo) {
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getUserCode())) { //创建人
//				sql.append(" and userCode = '");
//				sql.append(customerComplaintInfoVo.getUserCode().trim());
//				sql.append("'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getCode())) { //客诉编码
//				sql.append(" and code like '%");
//				sql.append(customerComplaintInfoVo.getCode().trim());
//				sql.append("%'");
//			}
//			if (customerComplaintInfoVo.getComplaintState() != -1 && !customerComplaintInfoVo.getComplaintState().equals(-1)) { //客诉状态
//				sql.append(" and complaintState = ");
//				sql.append(customerComplaintInfoVo.getComplaintState());
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getComplaintDate())) { //客诉日期
//				sql.append(" and complaintDate like '%");
//				sql.append(customerComplaintInfoVo.getComplaintDate().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getProductNo())) { //产品序列号
//				sql.append(" and (productNo = '");
//				sql.append(customerComplaintInfoVo.getProductNo().trim());
//				sql.append("' or productNo is null)");
//			}
//			
//			
//		}
//
//		sql.append(" order by id desc");
//		
//		return this.daoSupport.queryForList(sql.toString(), CustomerComplaintInfoVo.class);
		String jsonData = HttpUtil.postRequestContent("rest.sealer.complaint.pageCustomerComplaintInfoPage",
				"customerComplaintInfoVo.", customerComplaintInfoVo);
		List<CustomerComplaintInfoVo> customerComplaintInfoVos = 
				JsonUtil.getDTOList(jsonData, CustomerComplaintInfoVo.class);
		return customerComplaintInfoVos;
	}
	/**
	  * 客诉信息分页显示方法
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerComplaintInfoVoPage(CustomerComplaintInfoVo customerComplaintInfoVo, int pageNo,
			int pageSize) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select *");
//		sql.append(" from DT_CustomerComplaintInfo");
//		sql.append(" where 1=1");
//		
//		if (null != customerComplaintInfoVo) {
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getUserCode())) {
//				sql.append(" and userCode = '");
//				sql.append(customerComplaintInfoVo.getUserCode().trim());
//				sql.append("'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getComplaintPerson())) {
//				sql.append(" and complaintPerson like '%");
//				sql.append(customerComplaintInfoVo.getComplaintPerson().trim());
//				sql.append("%'");
//			}
//			
//			if (null!=customerComplaintInfoVo.getComplaintState()&&customerComplaintInfoVo.getComplaintState()!=-1) {
//				sql.append(" and complaintState = ");
//				sql.append(customerComplaintInfoVo.getComplaintState());
//				sql.append("");
//			}
//			
//			
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getAcceptor())) {
//				sql.append(" and acceptor like '%");
//				sql.append(customerComplaintInfoVo.getAcceptor().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getProductName())) {
//				sql.append(" and productName like '%");
//				sql.append(customerComplaintInfoVo.getProductName().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getSoftwareName())) {
//				sql.append(" and softwareName like '%");
//				sql.append(customerComplaintInfoVo.getSoftwareName().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getProductNo())) {
//				sql.append(" and productNo like '%");
//				sql.append(customerComplaintInfoVo.getProductNo().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getCode())) {
//				sql.append(" and code like '%");
//				sql.append(customerComplaintInfoVo.getCode().trim());
//				sql.append("%'");
//			}
//			if (customerComplaintInfoVo.getAcceptState() != null) {
//				sql.append(" and acceptState = ");
//				sql.append(customerComplaintInfoVo.getAcceptState().toString());
//			}
//		}
//
//		sql.append(" order by id desc");
//		
//		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
//				pageSize, CustomerComplaintInfoVo.class);
//		return page;
		return null;
	}

	/**
	  * 分权限客诉信息分页显示方法
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerComplaintInfoVoUser(AdminUser user, CustomerComplaintInfoVo customerComplaintInfoVo, int pageNo,
			int pageSize) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select a.*");
//		sql.append(" from DT_CustomerComplaintInfo a");
//		sql.append(" where 1=1");
//		
//		if (user.getUserid() != 0) {
//			sql.append(" and a.acceptor = '");
//			sql.append(user.getUsername().trim());
//			sql.append("'");
//		}
//		
//		if (null != customerComplaintInfoVo) {
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getAcceptor())) {
//				sql.append(" and a.acceptor like '%");
//				sql.append(customerComplaintInfoVo.getAcceptor().trim());
//				sql.append("%'");
//			}
//			
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getComplaintPerson())) {
//				sql.append(" and complaintPerson like '%");
//				sql.append(customerComplaintInfoVo.getComplaintPerson().trim());
//				sql.append("%'");
//			}
//			
//			if (!customerComplaintInfoVo.getComplaintState().equals("-1")) {
//				sql.append(" and complaintState = ");
//				sql.append(customerComplaintInfoVo.getComplaintState());
//				sql.append("");
//			}
//			
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getProductName())) {
//				sql.append(" and a.productName like '%");
//				sql.append(customerComplaintInfoVo.getProductName().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getSoftwareName())) {
//				sql.append(" and a.softwareName like '%");
//				sql.append(customerComplaintInfoVo.getSoftwareName().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getProductNo())) {
//				sql.append(" and a.productNo like '%");
//				sql.append(customerComplaintInfoVo.getProductNo().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getCode())) {
//				sql.append(" and a.code like '%");
//				sql.append(customerComplaintInfoVo.getCode().trim());
//				sql.append("%'");
//			}
//			if (customerComplaintInfoVo.getAcceptState() != null) {
//				sql.append(" and a.acceptState = ");
//				sql.append(customerComplaintInfoVo.getAcceptState().toString());
//			}
//		}
//
//		sql.append(" order by a.id desc");
//		
//		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
//				pageSize, CustomerComplaintInfoVo.class);
//		return page;
		return null;
	}

	/**
	  * 客诉信息分类统计
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Map complaintCountByUser(AdminUser user) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select complaintState,count(*) num");
//		sql.append(" from DT_CustomerComplaintInfo");
//		sql.append(" where 1=1");
//		sql.append(" group by complaintState order by complaintState");
//		
//		List<Map> lMaps = this.daoSupport.queryForList(sql.toString());
//		Map complaintCount = new HashMap();
//
//		complaintCount.put("open", 0);
//		complaintCount.put("guaqi", 0);
//		complaintCount.put("wait", 0);
//		complaintCount.put("close", 0);
//		complaintCount.put("all", 0);
//		
//		int i,num;
//		int total = 0;
//		for (Map map : lMaps) {
//			i = Integer.parseInt(map.get("complaintState").toString());
//			num = Integer.parseInt(map.get("num").toString());
//			total += num;
//			if(i==1) complaintCount.put("open", num);
//			if(i==2) complaintCount.put("guaqi", num);
//			if(i==3) complaintCount.put("wait", num);
//			if(i==4) complaintCount.put("close", num);
//		}
//		complaintCount.put("all", total);
//
//		return complaintCount;
		return null;
	}

	/**
	  * 客诉积分统计
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerComplaintStatPage(CustomerComplaintInfoVo customerComplaintInfoVo, int pageNo,
			int pageSize) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append(" select rePerson,complaintScore,count(*) num from DT_ReCustomerComplaintInfo where rePersonType=2");
//		
//		if (null != customerComplaintInfoVo) {
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getAcceptor())) {
//				sql.append(" and rePerson like '%");
//				sql.append(customerComplaintInfoVo.getAcceptor().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getBeginDate())) {
//				sql.append(" and reDate >= '");
//				sql.append(customerComplaintInfoVo.getBeginDate().trim());
//				sql.append("'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getEndDate())) {
//				sql.append(" and reDate < cast('");
//				sql.append(customerComplaintInfoVo.getEndDate().trim());
//				sql.append("' as datetime)+1");
//			}
//		}
//		sql.append(" group by rePerson,complaintScore order by reperson");
//		
//		List<Map> lMaps = new ArrayList<Map>();
//		
//		List<Map> lMaps_t = new ArrayList<Map>();
//		
//		List<Map> lMaps_result = new ArrayList<Map>();
//		
//		String rePerson = "";
//		
//		Map map_t = new HashMap();
//		
//		lMaps = this.daoSupport.queryForList(sql.toString());
//		
//		
//		for (Map map : lMaps) {
//			if (map.get("rePerson") != null)
//			{
//				if (map.get("rePerson").toString().equals(rePerson)) {
//					
//					map_t.put("total", Integer.parseInt(map_t.get("total").toString())+Integer.parseInt(map.get("num").toString()));
//					
//					if(map.get("complaintScore") == null)
//					{
//						map_t.put("defaultPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("1"))
//					{
//						map_t.put("onePoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("2"))
//					{
//						map_t.put("twoPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("3"))
//					{
//						map_t.put("threePoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("4"))
//					{
//						map_t.put("fourPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("5"))
//					{
//						map_t.put("fivePoint", map.get("num"));
//					}
//				}
//				else
//				{					
//					rePerson = map.get("rePerson").toString();
//					
//					if(map_t.size() != 0) lMaps_t.add(map_t);
//					
//					map_t = new HashMap();
//
//					map_t.put("defaultPoint", 0);
//					map_t.put("onePoint", 0);
//					map_t.put("twoPoint", 0);
//					map_t.put("threePoint", 0);
//					map_t.put("fourPoint", 0);
//					map_t.put("fivePoint", 0);
//					map_t.put("total", 0);
//					
//					map_t.put("rePerson", map.get("rePerson"));
//					
//					map_t.put("total", Integer.parseInt(map_t.get("total").toString())+Integer.parseInt(map.get("num").toString()));
//					
//					if(map.get("complaintScore") == null)
//					{
//						map_t.put("defaultPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("1"))
//					{
//						map_t.put("onePoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("2"))
//					{
//						map_t.put("twoPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("3"))
//					{
//						map_t.put("threePoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("4"))
//					{
//						map_t.put("fourPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("5"))
//					{
//						map_t.put("fivePoint", map.get("num"));
//					}
//				}
//			}
//		}
//		
//		if(map_t != null) lMaps_t.add(map_t);
//		
//		//按总数排序
//		Collections.sort(lMaps_t,new Comparator() {
//			public int compare(Object a,Object b)
//			{
//				int ida = Integer.parseInt(((Map)a).get("total").toString());
//				
//				int idb = Integer.parseInt(((Map)b).get("total").toString());
//				
//				return idb-ida;
//			}
//		});
//		
//		for (int i = (pageNo-1)*pageSize; i < pageNo*pageSize; i++) {
//			if(i>=0 && i<lMaps_t.size())
//				lMaps_result.add(lMaps_t.get(i));			
//		}
//		
//		return new Page(0, lMaps_t.size(), pageSize, lMaps_result);
		return null;
	}



	/**
	  * 客诉积分统计列表
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<Map> listCustomerComplaintStat(CustomerComplaintInfoVo customerComplaintInfoVo) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append(" select rePerson,complaintScore,count(*) num from DT_ReCustomerComplaintInfo where rePersonType=2");
//		
//		if (null != customerComplaintInfoVo) {
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getAcceptor())) {
//				sql.append(" and rePerson like '%");
//				sql.append(customerComplaintInfoVo.getAcceptor().trim());
//				sql.append("%'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getBeginDate())) {
//				sql.append(" and reDate >= '");
//				sql.append(customerComplaintInfoVo.getBeginDate().trim());
//				sql.append("'");
//			}
//			if (StringUtils.isNotBlank(customerComplaintInfoVo.getEndDate())) {
//				sql.append(" and reDate < cast('");
//				sql.append(customerComplaintInfoVo.getEndDate().trim());
//				sql.append("' as datetime)+1");
//			}
//		}
//		sql.append(" group by rePerson,complaintScore order by reperson");
//		
//		List<Map> lMaps = new ArrayList<Map>();
//		
//		List<Map> lMaps_t = new ArrayList<Map>();
//		
//		String rePerson = "";
//		
//		Map map_t = new HashMap();
//		
//		lMaps = this.daoSupport.queryForList(sql.toString());
//		
//		
//		for (Map map : lMaps) {
//			if (map.get("rePerson") != null)
//			{
//				if (map.get("rePerson").toString().equals(rePerson)) {
//					
//					map_t.put("total", Integer.parseInt(map_t.get("total").toString())+Integer.parseInt(map.get("num").toString()));
//					
//					if(map.get("complaintScore") == null)
//					{
//						map_t.put("defaultPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("1"))
//					{
//						map_t.put("onePoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("2"))
//					{
//						map_t.put("twoPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("3"))
//					{
//						map_t.put("threePoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("4"))
//					{
//						map_t.put("fourPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("5"))
//					{
//						map_t.put("fivePoint", map.get("num"));
//					}
//				}
//				else
//				{					
//					rePerson = map.get("rePerson").toString();
//					
//					if(map_t.size() != 0) lMaps_t.add(map_t);
//					
//					map_t = new HashMap();
//
//					map_t.put("defaultPoint", 0);
//					map_t.put("onePoint", 0);
//					map_t.put("twoPoint", 0);
//					map_t.put("threePoint", 0);
//					map_t.put("fourPoint", 0);
//					map_t.put("fivePoint", 0);
//					map_t.put("total", 0);
//					
//					map_t.put("rePerson", map.get("rePerson"));
//					
//					map_t.put("total", Integer.parseInt(map_t.get("total").toString())+Integer.parseInt(map.get("num").toString()));
//					
//					if(map.get("complaintScore") == null)
//					{
//						map_t.put("defaultPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("1"))
//					{
//						map_t.put("onePoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("2"))
//					{
//						map_t.put("twoPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("3"))
//					{
//						map_t.put("threePoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("4"))
//					{
//						map_t.put("fourPoint", map.get("num"));
//					}
//					else if(map.get("complaintScore").toString().equals("5"))
//					{
//						map_t.put("fivePoint", map.get("num"));
//					}
//				}
//			}
//		}
//		
//		if(map_t != null) lMaps_t.add(map_t);
//		
//		//按总数排序
//		Collections.sort(lMaps_t,new Comparator() {
//			public int compare(Object a,Object b)
//			{
//				int ida = Integer.parseInt(((Map)a).get("total").toString());
//				
//				int idb = Integer.parseInt(((Map)b).get("total").toString());
//				
//				return idb-ida;
//			}
//		});
//		
//		return lMaps_t;
		return null;
	}

	/**
	  * 超时客诉列表
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<CustomerComplaintInfoVo> getComplaintInfoVosTimeout(int complaintState ,int hours) throws Exception {
//		StringBuffer sql = new StringBuffer();
//
//		sql.append("select *");
//		sql.append(" from DT_CustomerComplaintInfo");
//		sql.append(" where complaintState="+complaintState);
//		sql.append(" and lastReDate<DATE_ADD(NOW(),INTERVAL -"+hours+" HOUR)");
//		sql.append(" and (timeouts<"+hours+" or timeouts is null)");
//		
//		return this.daoSupport.queryForList(sql.toString(), CustomerComplaintInfoVo.class);
		return null;
	}

}
