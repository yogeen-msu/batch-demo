package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;

@Service
public class SealerAuthServiceImpl implements ISealerAuthService {

	@SuppressWarnings("unchecked")
	public List<SealerAuthVO> list(String sealerCode) {
		String jsonData = HttpUtil.getReqeuestContent("rest.sealerAuth.list", "sealerCode=" + sealerCode);
		List<SealerAuthVO> sealerAuths = JsonUtil.getDTOList(jsonData, SealerAuthVO.class);
		return sealerAuths;
	}

	@Override
	public Page pageSealerAuthVoPage(SealerAuthVO sealerAuthVo, int pageNo,
			int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveAuth(SealerAuthVO sealerAuthVo) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateAuth(SealerAuthVO sealerAuthVo) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAuth(String code) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SealerAuthVO getSealerAuthById(String code) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page pageAuthVoDetail(SealerAuthVO sealerAuthVo, int pageNo,
			int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SealerAuthVO> listSealerAuthVO() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveAuthDetail(String autelId, String sealerAuthDesc)
			throws Exception {
		HttpUtil.getReqeuestContent("rest.sealer.manager.saveAuthDetail", 
				"autelId=" + autelId + "&sealerAuthDesc=" + sealerAuthDesc);
	}

	@Override
	public void delAuthDetailById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
