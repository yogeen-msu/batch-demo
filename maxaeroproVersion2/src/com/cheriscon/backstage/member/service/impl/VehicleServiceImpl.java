package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.IVehicleService;
import com.cheriscon.common.model.Vehicle;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;

@Service
public class VehicleServiceImpl implements IVehicleService {

	@SuppressWarnings("unchecked")
	public List<Vehicle> getVehicleList(Vehicle vehicle) throws Exception {
		String jsonData = HttpUtil.postRequestContent("rest.vehicle.list", "vehicle.", vehicle);
		List<Vehicle> list = JsonUtil.getDTOList(jsonData, Vehicle.class);
		return list;
	}

}
