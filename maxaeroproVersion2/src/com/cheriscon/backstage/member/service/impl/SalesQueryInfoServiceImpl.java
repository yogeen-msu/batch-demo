/**
 * 
 */
package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ISalesQueryInfoService;
import com.cheriscon.backstage.member.vo.PreSalesSupportVo;
import com.cheriscon.common.model.SalesQueryInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.distributor.vo.SalesQueryInfoVo;

/**
 * @author A13045
 *
 */
@Service
public class SalesQueryInfoServiceImpl implements ISalesQueryInfoService {


	public Page pageSalesQuery(String auth,SalesQueryInfo salesQueryInfo, int pageNo,
			int pageSize) throws Exception {
		return null;
//		StringBuffer sql=new StringBuffer("");
//		sql.append("select a.id,a.code,a.createUser,d.name as createUserName," +
//				"date_format(a.createDate,'%m/%d/%Y') as createDate ," +
//				"a.customerName,a.email,a.telphone,a.address,a.proTypeCode,a.openRemark,a.status,a.serialNo," +
//				"a.closeRemark,a.closeUser,a.closeDate," +
//				"b.name as proTypeName " +
//				"from DT_SalesQueryInfo a,dt_productForSealer b,DT_SealerDataAuthDeails c,dt_sealerInfo d where a.proTypeCode=b.code ");
//		sql.append(" and a.createUser=c.sealerCode");
//		sql.append(" and a.createUser=d.autelId");
//		sql.append(" and c.authCode like'").append(auth).append("%'");
//		if(null != salesQueryInfo ){
//			if(!StringUtil.isEmpty(salesQueryInfo.getProTypeCode()) && !salesQueryInfo.getProTypeCode().equals("-1")){
//				sql.append(" and b.code='").append(salesQueryInfo.getProTypeCode().trim()).append("'");
//			}
//			if(!StringUtil.isEmpty(salesQueryInfo.getCustomerName())){
//				sql.append(" and a.customerName like '%").append(salesQueryInfo.getCustomerName().trim()).append("%'");
//			}
//			if(!StringUtil.isEmpty(salesQueryInfo.getStatus()) && !salesQueryInfo.getStatus().equals("-1")){
//				sql.append(" and a.status='").append(salesQueryInfo.getStatus().trim()).append("'");
//			}
//		}
//		sql.append(" order by a.id desc");
//		
//		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}
	
	@SuppressWarnings("unchecked")
	public Page pageSalesQuery(SalesQueryInfoVo salesQueryinfoVo) {
		String pageJson = HttpUtil.postRequestContent(
				"rest.sealer.preSalesSupport.pageSalesQuery", "salesQueryinfoVo.", salesQueryinfoVo);
		JSONObject jsonObject = new JSONObject();
		jsonObject = JSONObject.fromObject(pageJson);
		String result = jsonObject.getString("result");
		List<PreSalesSupportVo> list = JsonUtil.getDTOList(result, PreSalesSupportVo.class);
		String currentPageNo = jsonObject.getString("currentPageNo");
		String pageSize = jsonObject.getString("pageSize");
		String totalCount = jsonObject.getString("totalCount");
//		String totalPageCount = jsonObject.getString("totalPageCount");
		Page page = new Page();
		page.setData(list);
		page.setCurrentPageNo(Integer.parseInt(currentPageNo));
		page.setPageSize(Integer.parseInt(pageSize));
		page.setTotalCount(Integer.parseInt(totalCount));
//		Page page = (Page) JsonUtil.getDTO(pageJson, SalesQueryInfo.class);
//		Page page = (Page) JsonUtil.getDTO(pageJson, PreSalesSupportVo.class);
		return page;
	}
	
	@Override
	public void saveSalesQuery(SalesQueryInfo salesQueryInfo) throws Exception {
		HttpUtil.postRequestContent("rest.sealer.preSalesSupport.saveSalesQuery", "salesQueryInfo.", salesQueryInfo);
//		salesQueryInfo.setCode(DBUtils.generateCode(DBConstant.SALESQUERY_CODE));
//		salesQueryInfo.setStatus(DBConstant.SALES_QUERY_STATUS_OPEN);
//		this.daoSupport.insert("DT_SalesQueryInfo", salesQueryInfo);
	}

	@Override
	public void updateSalesQuery(SalesQueryInfo salesQueryInfo)
			throws Exception {
//		this.daoSupport.update("DT_SalesQueryInfo", salesQueryInfo, "id="+salesQueryInfo.getId()+"");
		HttpUtil.postRequestContent("rest.sealer.preSalesSupport.updateSalesQuery", "salesQueryInfo.", salesQueryInfo);
	}

	@Override
	public void deleteSalesQueryInfo(String code) throws Exception {
//		String sql = "delete from DT_salesQueryInfo where code= ?";
//		this.daoSupport.execute(sql,code);

	}

	public SalesQueryInfo getSalesQueryInfoByCode(String code) throws Exception{
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.preSalesSupport.getSalesQueryInfoByCode", "code=" + code);
		return (SalesQueryInfo) JsonUtil.getDTO(jsonData, SalesQueryInfo.class);
//		String sql="select * from DT_salesQueryInfo where code=?";
//		return this.daoSupport.queryForObject(sql, SalesQueryInfo.class, code);
	}
	
	
}
