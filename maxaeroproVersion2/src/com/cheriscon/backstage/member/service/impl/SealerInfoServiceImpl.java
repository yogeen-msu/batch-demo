package com.cheriscon.backstage.member.service.impl;

import java.io.File;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.SealerInfoVo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.framework.database.Page;

@Service
public class SealerInfoServiceImpl implements ISealerInfoService {

	public List<SealerInfoVo> listSealer(SealerInfoVo sealerInfoVo)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int saveCreateSealer(File sealerFile) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insertSealer(SealerInfo sealerInfo) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void insertSealerAuth(String authCode, String sealerCode)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int insertSealer2(SealerInfo sealerInfo) throws Exception {
		HttpUtil.postRequestContent("rest.sealer.manager.insertSealer2", "sealer.", sealerInfo);
		return 0;
	}

	@Override
	public int updateSealer(SealerInfo sealerInfo) throws Exception {
		 HttpUtil.postRequestContent("rest.sealer.updateSealer", "sealer.", sealerInfo);
		return 0;
	}

	@Override
	public void updateSealer(String code, String lastLoginTime)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	public SealerInfo getSealerByCode(String code) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.getSealerByCode", "code=" + code);
		return (SealerInfo) JsonUtil.getDTO(jsonData, SealerInfo.class);
	}

	@Override
	public SealerInfo getSealerByAutelId(String autelId) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.getByAutelId", "autelId=" + autelId);
		SealerInfo sealer = (SealerInfo) JsonUtil.getDTO(jsonData, SealerInfo.class);
		return sealer;
	}

	@Override
	public int delSealer(String code) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delSealers(String codes) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public SealerInfo getSealerInfoByAutelIdAndPassword(String autelId,
			String password) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.getByAutelIdAndPwd", "autelId=" + autelId + "&password=" + password);
		SealerInfo sealer = (SealerInfo) JsonUtil.getDTO(jsonData, SealerInfo.class);
		return sealer;
	}

	@Override
	public Page pageSealerInfoVoPage(SealerInfoVo sealerInfoVo, int pageNo,
			int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page pageSealerInfoVoPage2(SealerInfoVo sealerInfoVo, int pageNo,
			int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page querySealerInfoPage(String areaCode, String country,
			int pageNo, int pageSize) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SealerInfo> querySealerInfo() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String getSealerInfoByAuthCode(String authCode) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.ofn.getSealerInfoByAuthCode", "authCode=" + authCode);
		return jsonData;
	}
	
}
