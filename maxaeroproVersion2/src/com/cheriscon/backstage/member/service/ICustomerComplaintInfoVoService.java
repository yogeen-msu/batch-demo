package com.cheriscon.backstage.member.service;


import java.util.List;
import java.util.Map;

import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.database.Page;

/**
 * 客诉信息业务逻辑接口类
 * @author pengdongan
 * @date 2013-01-18
 */
public interface ICustomerComplaintInfoVoService 
{



	/**
	  * 客诉积分统计列表
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public List<Map> listCustomerComplaintStat(CustomerComplaintInfoVo customerComplaintInfoVo) throws Exception;

	/**
	  * 客诉积分统计
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerComplaintStatPage(CustomerComplaintInfoVo customerComplaintInfoVo, int pageNo,
			int pageSize) throws Exception;
	/**
	  * 客诉销售信息分页显示方法
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerComplaintInfoVoPage(CustomerComplaintInfoVo customerComplaintInfoVo, int pageNo,
			int pageSize) throws Exception;

	/**
	  * 分权限客诉信息分页显示方法
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerComplaintInfoVoUser(AdminUser user, CustomerComplaintInfoVo customerComplaintInfoVo, int pageNo,
			int pageSize) throws Exception;

	/**
	  * 客诉信息分类统计
	  * @return
	  * @throws Exception
	  */
	public Map complaintCountByUser(AdminUser user) throws Exception;

	/**
	  * 超时客诉列表
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public List<CustomerComplaintInfoVo> getComplaintInfoVosTimeout(int complaintState ,int hours) throws Exception;
	
	/**
	  * 客诉销售信息分页显示方法20140211
	  * @param CustomerComplaintInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public List<CustomerComplaintInfoVo> pageCustomerComplaintInfoPage(CustomerComplaintInfoVo customerComplaintInfoVo) throws Exception;
}
