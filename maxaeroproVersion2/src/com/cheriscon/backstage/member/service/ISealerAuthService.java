package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;


/**
 * 经销商权限控制类
 * @author kingapex
 * 2010-5-20下午02:54:53
 */
public interface ISealerAuthService {

	
	/**
	 * 读取子菜单列表，包括其所有子的
	 * 
	 * @param parentid
	 * @return
	 */
	public List<SealerAuthVO> list(String sealerCode);
	
	
	public Page pageSealerAuthVoPage(SealerAuthVO sealerAuthVo, int pageNo,
			int pageSize) throws Exception;
	
	public void saveAuth(SealerAuthVO sealerAuthVo) throws Exception;
	
	public void updateAuth(SealerAuthVO sealerAuthVo) throws Exception;
	
	public void deleteAuth(String code) throws Exception;
	
	public SealerAuthVO getSealerAuthById(String code) throws Exception;
	
	public Page pageAuthVoDetail(SealerAuthVO sealerAuthVo,int pageNo,int pageSize) throws Exception;

	public List<SealerAuthVO> listSealerAuthVO() throws Exception;
	
	public void saveAuthDetail(String autelId,String sealerAuthDesc) throws Exception;
	
	public void delAuthDetailById(Integer id) throws Exception;
	
}
