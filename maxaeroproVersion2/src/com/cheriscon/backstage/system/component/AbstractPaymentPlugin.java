package com.cheriscon.backstage.system.component;

import com.cheriscon.framework.plugin.AutoRegisterPlugin;

/**
 * 支付插件基类<br/>
 * @author kingapex
 * 2010-9-25下午02:55:10
 */
public abstract class AbstractPaymentPlugin extends AutoRegisterPlugin {
	
	/**
	 * 为支付插件定义唯一的id
	 * @return
	 */
	public abstract String getId();
	
	/**
	 * 定义插件名称
	 * @return
	 */
	public abstract String getName();
	
}
