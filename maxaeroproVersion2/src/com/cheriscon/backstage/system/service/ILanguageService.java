package com.cheriscon.backstage.system.service;

import java.util.List;
import java.util.Locale;

import com.cheriscon.common.model.Language;
import com.cheriscon.framework.database.Page;

public interface ILanguageService {
	
//	/**
//	 * 增加
//	 * @param language
//	 */
//	public boolean saveLanguage(Language language);
//	
//	/**
//	 * 修改
//	 * @param language
//	 */
//	public boolean updateLanguage(Language language);
//	
//	/**
//	 * 
//	 * @param id
//	 * @param isShow
//	 * @return
//	 */
//	public void updateIsShowState(int id,int isShow);
//	
//	/**
//	 * 删除
//	 */
//	public boolean delLanguageById(int id);
//	
//	/**
//	 * 根据ID查询
//	 * @param id
//	 * @return
//	 */
//	public Language getById(int id);
	
	/**
	 * 根据Code查询
	 * @param id
	 * @return
	 */
	public Language getByCode(String code);
	
	/**
	 * 根据当前语言环境选择，没有默认选择中文
	 * @param id
	 * @return
	 */
	public Language getByLocale(Locale locale);
	
	/**
	 * 查询出所有语言
	 * @return
	 */
	public List<Language> queryLanguage();
//	
//	
//	/**
//	 * 查询出所有语言
//	 * @return
//	 */
//	public List<Language> queryAllLanguage();
//	
//	/**
//	 * 查询没有分配的语言
//	 * @return
//	 */
//	public List<Language> queryNotSelectLanguage();
//	
//	/**
//	 * 翻页查询所有软件语言
//	 * @param pageNo
//	 * @param pageSize
//	 * @return
//	 */
//	public Page pageLanguage(int pageNo, int pageSize);
//
//	public List<Language> getByNewLanguage(Language language);
}
