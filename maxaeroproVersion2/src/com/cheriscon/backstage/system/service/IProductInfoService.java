package com.cheriscon.backstage.system.service;

//import java.util.List;

import com.cheriscon.common.model.ProductInfo;
//import com.cheriscon.framework.database.Page;

/**
 * 产品信息Service
 * @author yinhb
 *
 */
public interface IProductInfoService {
//	
//	/**
//	 * 增加
//	 * @param productInfo
//	 */
//	public boolean saveProductInfo(ProductInfo productInfo);
//	
//	/**
//	 * 修改
//	 * @param productInfo
//	 */
//	public boolean updateProductInfo(ProductInfo productInfo);
//	
//	/**
//	 * 删除
//	 */
//	public boolean delProductInfoById(int id);
//	
//	/**
//	 * 根据ID查询
//	 * @param id
//	 * @return
//	 */
//	public ProductInfo getById(int id);
//	
//	/**
//	 * 根据产品序列号查询
//	 * @param id
//	 * @return
//	 */
//	public ProductInfo getBySerialNo(String serialNo);
//	
//	/**
//	 * 查询出所有产品信息
//	 * @return
//	 */
//	public List<ProductInfo> queryProductInfo();
//	
//	/**
//	 * 翻页查询所有产品信息
//	 * @param productInfo
//	 * @param pageNo
//	 * @param pageSize
//	 * @return
//	 */
//	public Page pageProductInfo(ProductInfo productInfo,int pageNo, int pageSize);
//	
//	public List<ProductInfo> queryProductByUserCode(String userCode) throws Exception;
	
	/**
	 * 根据产品code查询
	 * @param id
	 * @return
	 */
	public ProductInfo getByProCode(String code);
//	
	public ProductInfo getBySerialNo2(String serialNo);
//	
//	public ProductInfo getBySerialNo3(String serialNo);
//	
//	
//	/**
//	 * 根据产品序列号和密码查询产品信息
//	 * @param serialNo password
//	 * @return
//	 */
//	public ProductInfo getBySerialNoAndPwd(String serialNo,String password);

	/**
	 * 修改产品备注
	 */
	public String updateProductRemark(String proCode,String remark);
	public String getProductCardType(String serialNo);
}
