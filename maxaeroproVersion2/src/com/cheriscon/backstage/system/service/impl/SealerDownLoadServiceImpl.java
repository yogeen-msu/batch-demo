package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.SealerDownLoadService;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.vo.SealerDownLoadVo;

@Service
public class SealerDownLoadServiceImpl implements SealerDownLoadService {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SealerDownLoadVo> querySealerDownLoadFiles() {
		return HttpUtil.getReqeuestContentList("rest.sealer.download.files", null, SealerDownLoadVo.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SealerDownLoadVo> querySealerDownLoadList(String fileType) {
		return HttpUtil.getReqeuestContentList("rest.sealer.download.list", "fileType="+fileType, SealerDownLoadVo.class);
	}
}
