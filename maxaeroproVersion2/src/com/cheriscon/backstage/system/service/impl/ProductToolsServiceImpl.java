package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.ProductToolsService;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.vo.ProductTypeToolsVo;

@Service
public class ProductToolsServiceImpl implements ProductToolsService {
	
	@SuppressWarnings("unchecked")
	public List<ProductTypeToolsVo> queryProductToolList(String languageCode, String autelId) throws Exception {
		List<ProductTypeToolsVo> tools = 
				HttpUtil.getReqeuestContentList("rest.productTypeTools.list", 
						"languageCode=" + languageCode + "&autelId=" + autelId, ProductTypeToolsVo.class);
		return tools;
	}

}
