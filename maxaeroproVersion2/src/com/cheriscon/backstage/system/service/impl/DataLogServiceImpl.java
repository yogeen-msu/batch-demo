package com.cheriscon.backstage.system.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import com.cheriscon.backstage.system.service.IDataLogService;
import com.cheriscon.common.model.DataLogging;
import com.cheriscon.common.model.DataLoggingContent;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.front.constant.DBHelper;


@Service
public class DataLogServiceImpl implements IDataLogService {

	static String sql = null;  
    static DBHelper db1 = null;  
    static ResultSet ret = null;  
	@SuppressWarnings("unchecked")
	@Override
	public List<DataLogging> getDataLogList(String productSN)
			throws Exception {
//		sql = "select a.title,a.id,a.p4,a.createTime,a.p2 from dt_datalog a where p4='"+productSN+"'";//SQL语句  
//        db1 = new DBHelper(sql);//创建DBHelper对象
//        List<DataLogging> list=new ArrayList<DataLogging>();
//        try {  
//            ret = db1.pst.executeQuery();//执行语句，得到结果集  
//            
//            while (ret.next()) {  
//            	DataLogging log=new DataLogging();
//                String title = ret.getString(1);  
//                String id = ret.getString(2);  
//                String productNo = ret.getString(3);  
//                String createDate = ret.getString(4);  
//                if(createDate!=null && !createDate.equals("")){
//                	createDate=new SimpleDateFormat("MM/dd/yyyy").format(new Date(Long.parseLong(createDate)*1000));
//                }
//                String status = ret.getString(5); 
//                log.setCreateDate(createDate);
//                log.setId(id);
//                log.setProductNo(productNo);
//                log.setTitle(title);
//                log.setStatus(status);
//                list.add(log);
//            }//显示数据  
//        } catch (SQLException e) {  
//            e.printStackTrace();  
//        }finally{
//        	 ret.close();  
//             db1.close();//关闭连接 
//        }
//        return list;
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.dataLog.getDataLogList", "productSN=" + productSN);
		return JsonUtil.getDTOList(jsonData, DataLogging.class);
	}

	public DataLogging getDataLogDetails(String id) throws Exception{
		sql = "select * from dt_datalog a where id="+id+"";//SQL语句  
        db1 = new DBHelper(sql);//创建DBHelper对象
        DataLogging log=new DataLogging();
        try {  
            ret = db1.pst.executeQuery();//执行语句，得到结果集  
            while (ret.next()) {  
            	log.setId(ret.getString(1));
            	log.setTitle(ret.getString(4));
            	log.setCar(ret.getString(2));
            	log.setContractEmail(ret.getString(8));
            	log.setContractTel(ret.getString(7));
            	log.setContractUser(ret.getString(6));
            	log.setEngine(ret.getString(17));
            	log.setMake(ret.getString(16));
            	log.setStatus(ret.getString(4));
            	log.setVin(ret.getString(3));
            	log.setYear(ret.getString(15));
            	log.setContent(ret.getString(5));
            	log.setModel(ret.getString(18));
            	log.setVersion(ret.getString(11));
            	log.setStatus(ret.getString(20));
            }
        }catch(SQLException e){
        	 e.printStackTrace(); 
		}finally{
			 ret.close();  
	         db1.close();//关闭连接
        }
        return log;
	}
	public List<DataLoggingContent> getDataLogContent(String datalogId) throws Exception{
		sql = "select * from dt_dataservercustomer a where datalogId='"+datalogId+"'";//SQL语句  
        db1 = new DBHelper(sql);//创建DBHelper对象
        List<DataLoggingContent> list=new ArrayList<DataLoggingContent>();
        try {  
            ret = db1.pst.executeQuery();//执行语句，得到结果集  
            
            while (ret.next()) {  
            	DataLoggingContent log=new DataLoggingContent();
            	log.setId(ret.getString(1));
            	log.setContent(ret.getString(4));
            	log.setDatalogId(ret.getString(2));
            	log.setIsSynced(ret.getString(7));
            	log.setMsgForm(ret.getString(5));
            	log.setMsgType(ret.getString(3));
                String createDate = ret.getString(6);  
                if(createDate!=null && !createDate.equals("")){
                	createDate=new SimpleDateFormat("MM/dd/yyyy").format(new Date(Long.parseLong(createDate)*1000));
                }
               log.setCreateTime(createDate);
               list.add(log);
            }//显示数据  
        } catch (SQLException e) {  
            e.printStackTrace();  
        }finally{
        	 ret.close();  
             db1.close();//关闭连接 
        }
        return list;
	}
}
