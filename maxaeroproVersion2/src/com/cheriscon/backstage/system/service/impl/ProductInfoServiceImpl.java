package com.cheriscon.backstage.system.service.impl;

//import java.util.Date;
//import java.util.List;
//
//import javax.annotation.Resource;
//
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
//import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.system.service.IProductInfoService;
//import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.utils.HttpUtil;
//import com.cheriscon.common.model.ProductInfoPwdChangeLog;
//import com.cheriscon.common.model.SaleContract;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.cop.resource.model.AdminUser;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.cop.sdk.utils.DateUtil;
//import com.cheriscon.framework.database.Page;
//import com.cheriscon.framework.util.StringUtil;
//import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.common.utils.JsonUtil;

@Service
public class ProductInfoServiceImpl  implements IProductInfoService {
//	
//	@Resource
//	private IAdminUserManager adminUserManager;
//	@Transactional
//	public boolean saveProductInfo(ProductInfo productInfo) {
//		String tableName = DBUtils.getTableName(ProductInfo.class);
//		productInfo.setCode(DBUtils.generateCode(DBConstant.PRODUCTINFO_CODE));
//		productInfo.setSerialNo(productInfo.getSerialNo().toUpperCase());
//		productInfo.setRegStatus(FrontConstant.PRODUCT_REGSTATUS_NO);
//		this.daoSupport.insert(tableName, productInfo);
//		return true;
//	}
//
//	@Transactional
//	public boolean updateProductInfo(ProductInfo productInfo) {
//		String tableName = DBUtils.getTableName(ProductInfo.class);
//		
//		ProductInfo dbProduct = getById(productInfo.getId());
//		if(!productInfo.getRegPwd().equals(dbProduct.getRegPwd())){
//			ProductInfoPwdChangeLog log=new ProductInfoPwdChangeLog();
//			log.setNewPwd(productInfo.getRegPwd());
//			log.setOldPwd(dbProduct.getRegPwd());
//			log.setProCode(dbProduct.getCode());
//			log.setOperatorDate(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
//			AdminUser user = adminUserManager.getCurrentUser();
//			user = adminUserManager.get(user.getUserid());
//			log.setOperatorUser(user.getUsername());
//			this.daoSupport.insert("DT_ProductInfoPwdChangeLog", log);
//		}
//		
//		
//		dbProduct.setProTypeCode(productInfo.getProTypeCode());
////		dbProduct.setSerialNo(productInfo.getSerialNo());
//		dbProduct.setSaleContractCode(productInfo.getSaleContractCode());
//		dbProduct.setRegPwd(productInfo.getRegPwd());
//		dbProduct.setProDate(productInfo.getProDate());
//		dbProduct.setRegStatus(productInfo.getRegStatus());
//		dbProduct.setRegTime(productInfo.getRegTime());
//		dbProduct.setExternInfo1(productInfo.getExternInfo1());
//		dbProduct.setExternInfo2(productInfo.getExternInfo2());
//		dbProduct.setExternInfo3(productInfo.getExternInfo3());
//		dbProduct.setExternInfo4(productInfo.getExternInfo4());
//		dbProduct.setExternInfo5(productInfo.getExternInfo5());
//		dbProduct.setRemark(productInfo.getRemark());
//		
//	
//		
//		
//		this.daoSupport.update(tableName,dbProduct,"id="+dbProduct.getId());
//		return true;
//	}
//
//	@Transactional
//	public boolean delProductInfoById(int id) {
//		String tableName = DBUtils.getTableName(ProductInfo.class);
//		StringBuffer sql = new StringBuffer("delete from "+ tableName);
//		sql.append(" where id=?");
//		this.daoSupport.execute(sql.toString(),id);
//		return true;
//	}
//	
//	public ProductInfo getById(int id){
//		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
//		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
//		String saleContractTbl = DBUtils.getTableName(SaleContract.class);
//		
//		StringBuffer sql = new StringBuffer("select pif.*,pt.name proTypeName,sc.name saleContractName from "  +  productInfoTbl + " pif ");
//		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
//		sql.append(" left join "+saleContractTbl + " sc on pif.saleContractCode = sc.code");
//		sql.append(" where pif.id=? ");
//		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, id);
//	}
//	
//	public ProductInfo getBySerialNo(String serialNo){
//		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
//		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
//		sql.append(" where pif.serialNo=? ");
//		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo);
//	}
//	
	public ProductInfo getBySerialNo2(String serialNo){
//		StringBuffer sql = new StringBuffer("select a.*,b.name as proTypeName  from dt_productinfo a ,DT_ProductForSealer b where a.proTypeCode=b.code");
//		sql.append(" and a.serialNo=? ");
//		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo);
		String jsonData = HttpUtil.getReqeuestContent("rest.sealer.complaint.getBySerialNo2", "serialNo=" + serialNo);
		return (ProductInfo) JsonUtil.getDTO(jsonData, ProductInfo.class);
	}
//	
//	public ProductInfo getBySerialNo3(String serialNo){
//		StringBuffer sql = new StringBuffer("select a.code,b.sealerCode as sealerAutelId,b.languagecfgCode as saleContractLanguage,b.saleCfgCode as saleContractCfg"
//				+ " from dt_productinfo a,dt_saleContract b where a.salecontractCode=b.code");
//		sql.append(" and a.serialNo=? ");
//		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo);
//	}
	
	
	public ProductInfo getByProCode(String code){
//		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
//		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
//		sql.append(" where pif.code=? ");
//		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, code);
		return null;
	}
//	
//	
//	public List<ProductInfo> queryProductInfo() {
//		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
//		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
//		String saleContractTbl = DBUtils.getTableName(SaleContract.class);
//		
//		StringBuffer sql = new StringBuffer("select pif.*,pt.name proTypeName,sc.name saleContractName from "  +  productInfoTbl + " pif ");
//		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
//		sql.append(" left join "+saleContractTbl + " sc on pif.saleContractCode = sc.code");
//		sql.append(" order by pif.id asc ");
//		
//		return this.daoSupport.queryForList(sql.toString(), ProductInfo.class, new Object[]{});
//	}
//
//	public Page pageProductInfo(ProductInfo productInfo,int pageNo, int pageSize) {
//		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
//		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
//		String saleContractTbl = DBUtils.getTableName(SaleContract.class);
//		
//		StringBuffer sql = new StringBuffer("select pif.*,pt.name proTypeName,sc.name saleContractName from "  +  productInfoTbl + " pif ");
//		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
//		sql.append(" left join "+saleContractTbl + " sc on pif.saleContractCode = sc.code");
//		
//		StringBuffer term = new StringBuffer();
//		if(productInfo != null){
//			//产品型号
//			if(!StringUtil.isEmpty(productInfo.getProTypeCode())){
//				if(term.length()>0){term.append(" and ");
//				}else{term.append(" where ");}
//				
//				term.append(" pif.proTypeCode like '%"+productInfo.getProTypeCode().trim()+"%' ");
//			}
//			
//			if(!StringUtil.isEmpty(productInfo.getSaleContractName())){
//				if(term.length()>0){term.append(" and ");
//				}else{term.append(" where ");}
//				term.append(" sc.name like '%" +productInfo.getSaleContractName()+"%'");
//			}
//			
//			if(!StringUtil.isEmpty(productInfo.getSaleContractArea())){
//				if(term.length()>0){term.append(" and ");
//				}else{term.append(" where ");}
//				term.append(" sc.areaCfgCode = '"+productInfo.getSaleContractArea()+"'");
//			}
//			
//			//产品序号号
//			if(!StringUtil.isEmpty(productInfo.getSerialNo())){
//				if(term.length()>0){term.append(" and ");
//				}else{term.append(" where ");}
//				
//				term.append(" pif.serialNo like '%"+productInfo.getSerialNo().trim()+"%' ");
////				term.append(" pif.serialNo = '"+productInfo.getSerialNo().trim()+"' ");
//			}
//			
//			sql.append(term);
//		}
//		sql.append(" order by pif.id desc ");
//		
//		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,ProductInfo.class,new Object[]{});
//		return page;
//	}
//
//	public List<ProductInfo> queryProductByUserCode(String userCode) throws Exception{
//		StringBuffer sql=new StringBuffer();
//		sql.append("select b.*,c.languageCfgCode as saleContractLanguage,c.saleCfgCode as saleContractCfg  from  dt_customerproinfo a ,dt_productInfo b,dt_salecontract c ");
//		sql.append("where a.proCode=b.code ");
//		sql.append("and b.saleContractCode=c.code ");
//		sql.append("and a.customerCode='"+userCode+"' ");
//		sql.append("and c.proTypeCode='prt_DS708' ");
//		sql.append("and c.areaCfgCode<>'acf_North_America'");
//		
//		return this.daoSupport.queryForList(sql.toString(), ProductInfo.class);
//	}
//	
//	public ProductInfo getBySerialNoAndPwd(String serialNo,String password){
//		StringBuffer sql = new StringBuffer("select a.*,b.name as proTypeName  from dt_productinfo a ,DT_ProductForSealer b where a.proTypeCode=b.code");
//		sql.append(" and a.serialNo like ?  and regPwd=?");
//		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo+ "%",password);
//	}

	@Override
	public String updateProductRemark(String proCode, String remark) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("proCode", proCode);
		param.put("remark", remark);
		return HttpUtil.postRequestContent("rest.regProduct.updateProductRemark", param);
	}

	@Override
	public String getProductCardType(String serialNo) {
		return HttpUtil.getReqeuestContent("rest.user.getProductCardType", "serialNo="+serialNo);
	}
	
	
}
