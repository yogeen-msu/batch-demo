package com.cheriscon.backstage.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.ILanguageService;
//import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
//import com.cheriscon.common.utils.DBUtils;
//import com.cheriscon.cop.sdk.database.BaseSupport;
//import com.cheriscon.framework.database.Page;
import com.cheriscon.common.utils.HttpUtil;

@Service
public class LanguageServiceImpl implements ILanguageService {
	
	private Map<String, Object> languageMap = new HashMap<String, Object>();
	
	public Language getByCode(String code){
		Language language = (Language) languageMap.get("language_code_"+code);
		if(language==null){
			language = (Language) HttpUtil.getReqeuestContentObject("rest.language.get.code", "code="+code, Language.class);
			languageMap.put("language_code_"+code,language);
		}
			return language;
	}
	
	public Language getByLocale(Locale locale){
		try {
			Language language = (Language) languageMap.get("language_locale_"+locale.getLanguage()+"_"+locale.getCountry());
			if(language==null){
				language = (Language) HttpUtil.getReqeuestContentObject("rest.language.get.locale", 
						"countryCode="+locale.getCountry()+"&languageCode="+locale.getLanguage(), Language.class);
				languageMap.put("language_locale_"+locale.getLanguage()+"_"+locale.getCountry(), language);
			}
			return language;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
//	
	public List<Language> queryLanguage() {
		List<Language> languages = (List<Language>) languageMap.get("language_list");
		if(languages==null){
			languages = HttpUtil.getReqeuestContentList("rest.language.list", null, Language.class);
			languageMap.put("language_list", languages);
		}
		//屏蔽中文
		if(null != languages && languages.size() > 0){
			for(int i = 0 ; i < languages.size(); i++){
				Language lan = languages.get(i);
				if("zh".equals(lan.getLanguageCode())){
					languages.remove(lan);
				}
			}
		}
		return languages;
	}
}
