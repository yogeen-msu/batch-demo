package com.cheriscon.backstage.system.service;

import java.util.List;
import com.cheriscon.common.vo.SealerDownLoadVo;

public interface SealerDownLoadService {
	
	/**
	 * 查询经销商资料文件夹
	 * @return
	 */
	public List<SealerDownLoadVo> querySealerDownLoadFiles();
	
	/**
	 * 查询经销商资料
	 * @param string 
	 * @return
	 */
	public List<SealerDownLoadVo> querySealerDownLoadList(String fileType);
}
