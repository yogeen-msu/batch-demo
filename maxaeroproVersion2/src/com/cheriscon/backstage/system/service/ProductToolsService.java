package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.vo.ProductTypeToolsVo;

public interface ProductToolsService {

	public List<ProductTypeToolsVo> queryProductToolList(final String languageCode, final String autelId) throws Exception;
	
}
