package com.cheriscon.backstage.content.constant;

/**
 * 内容管理常量
 * @author yinhb
 */
public class CTConsatnt {
	
/////////////////////////////邮件类型///////////////////////////////////////////////
	/** 注册提示 */
	public static final int EMAIL_TEMPLATE_TYEP_REGIST = 1;
	/** 密码找回 */
	public static final int EMAIL_TEMPLATE_TYEP_PWD = 2;
	/** 订单提交成功 */
	public static final int EMAIL_TEMPLATE_TYEP_ORDER = 3;
	/** 付款成功*/
	public static final int EMAIL_TEMPLATE_TYEP_PAY = 4;
	/** 提醒续费*/
	public static final int EMAIL_TEMPLATE_TYEP_WARN_PAY = 5;
	/** 续费成功*/
	public static final int EMAIL_TEMPLATE_TYEP_PAY_SUCCESS = 6;
	
	/**新增客诉*/
	public static final int EMAIL_TEMPLATE_TYEP_ADD_COMPLAINT_SUCCESS = 7;
	
	/**客诉回复*/
	public static final int EMAIL_TEMPLATE_TYEP_REPLY_COMPLAINT_SUCCESS = 8;
	
	
	/**客诉超时提醒*/
	public static final int EMAIL_TEMPLATE_TYEP_COMPLAINT_TIMEOUT_ALERT = 9;
	
	/**客诉自动关闭提醒*/
	public static final int EMAIL_TEMPLATE_TYEP_COMPLAINT_CLOSE_ALERT = 10;
	////////////////////////////////////////////////////////////////////////////////////
	
	/**用户账号找回*/
	public static final int EMAIL_TEMPLATE_TYEP_AUTELID_FIND = 11;
	
	/**用户账号激活*/
	public static final int EMAIL_TEMPLATE_TYEP_AUTELID_SECONDEMAIL_ACTIVE = 12;
	
	/**用户更换autelID激活*/
	public static final int EMAIL_TEMPLATE_TYEP_AUTELID_CHANGE_ACTIVE = 13;
	
	/**用户修改autelID激活*/
	public static final int EMAIL_TEMPLATE_TYEP_AUTELID_UPDATE_ACTIVE = 14;
	
	/**用户订单提醒*/
	public static final int EMAIL_TEMPLATE_TYEP_ORDER_CONFIRM = 15;
	
	/**订单处理提醒*/
	public static final int EMAIL_TEMPLATE_TYEP_ORDER_RESULT= 16;
	
	/**产品到期邮件提醒*/
	public static final int EMAIL_TEMPLATE_TYEP_PRODUCT_VALID= 17;
	
	/**MaxiSys升级到Pro邮件提醒*/
	public static final int EMAIL_TEMPLATE_TYEP_MAXISYS_TO_PRO= 18;
	
	
	public static final int CT_STATUS_NOT_USE = 0;	//未使用
	public static final int CT_STATUS_USE = 1;		//正在使用
	
	public static final int CT_SUCCESS = 0;			//操作成功
	public static final int CT_FAIL =1 ;			//操作成功
	public static final int CT_IS_USE =2 ;			//正在使用
	

	

}
