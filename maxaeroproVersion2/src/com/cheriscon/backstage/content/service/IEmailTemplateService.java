package com.cheriscon.backstage.content.service;

import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.framework.database.Page;

public interface IEmailTemplateService {
	
	/**
	 * 增加
	 * @param emailTemplate
	 */
	public boolean saveEmailTemplate(EmailTemplate emailTemplate);
	
	/**
	 * 修改
	 * @param emailTemplate
	 */
	public boolean updateEmailTemplate(EmailTemplate emailTemplate);
	
	/**
	 * 修改使用状态
	 * @param id
	 * @param status	当前状态
	 * @return
	 */
	public boolean updateStatus(int id,int status);
	
	/**
	 * 删除
	 * @param id
	 * @return  操作结果   0：成功   1：失败   2：正在使用
	 */
	public int delEmailTemplateById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public EmailTemplate getById(int id);
	
	/**
	 * 根据邮件类型和语言种类获取
	 * @param languageCode
	 * @param type
	 * @return
	 */
	public EmailTemplate getUseTemplateByTypeAndLanguage(int type,String languageCode);
	
	/**
	 * 翻页查询所有邮件模版
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageEmailTemplate(EmailTemplate emailTemplate ,int pageNo, int pageSize);

}
