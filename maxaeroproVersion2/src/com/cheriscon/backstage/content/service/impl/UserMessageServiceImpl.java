package com.cheriscon.backstage.content.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.content.service.IUserMessageService;
import com.cheriscon.common.model.UserMessage;
import com.cheriscon.common.utils.HttpUtil;

@Service
public class UserMessageServiceImpl implements IUserMessageService {

	@Override
	public String saveMessage(UserMessage userMessage) throws Exception {
		String jsonData = HttpUtil.postRequestContent(
				"rest.userMessage.readMessage", "userMessage.", userMessage);
		return jsonData;
	}
	
	@Override
	public int queryUserMessageIsReadCount(int userType, String languageCode,
			String userCode, int isDialog) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<UserMessage> queryUserMessageList(int userType,
			String userCode, String messageCode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public int queryUserMessageCount(int userType, String languageCode,
			String userCode) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.sale.queryUserMessageCount", 
				"userType=" + userType + "&languageCode=" + languageCode + "&userCode=" + userCode);
		return Integer.parseInt(jsonData);
	}

}
