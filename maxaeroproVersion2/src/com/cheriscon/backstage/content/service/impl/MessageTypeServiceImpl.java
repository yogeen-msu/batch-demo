package com.cheriscon.backstage.content.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.content.service.IMessageTypeService;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.common.utils.HttpUtil;

@Service
public class MessageTypeServiceImpl implements IMessageTypeService {

	@Override
	public boolean saveMessageType(MessageType messageType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateMessageType(MessageType messageType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delMessageTypeById(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public MessageType getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<MessageType> queryMessageType() {
		List<MessageType> list = HttpUtil.getReqeuestContentList(
				"rest.messageType.queryMessageType", null, MessageType.class);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<MessageType> queryMessageType(int userType, Integer timeRange,
			String languageCode, String creatTime) throws Exception {
		List<MessageType> messageTypeList = HttpUtil.getReqeuestContentList(
				"rest.messageType.list", "userType=" + userType + "&timeRange=" 
		+ timeRange + "&languageCode=" + languageCode + "&creatTime=" + creatTime.replace(" ", ","),
		MessageType.class);
		return messageTypeList;
	}

}
