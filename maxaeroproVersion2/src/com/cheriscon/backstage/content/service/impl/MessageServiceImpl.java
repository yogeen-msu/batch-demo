package com.cheriscon.backstage.content.service.impl;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.common.model.Message;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.common.utils.JsonUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class MessageServiceImpl implements IMessageService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, Object> queryMyMessageList(String userCode,
			int pageNo, int pageSize,String languageCode, String msgTypeCode, 
			String userType, String timeRange) throws Exception {
//		String url = HttpUtil.getUrl("rest.message.list");
//		url = url + "?" + "userCode=" + userCode + "&page=" + pageNo
//				+ "&pageSize=" + pageSize;
//		Map<String, Object> map = new HashMap<String, Object>();
//		this.objToMap(map, message);
//		map.put("userCode", userCode);
//		map.put("page", pageNo);
//		map.put("pageSize", pageSize);
//		String jsonData = HttpUtil.postRequestContent("rest.message.list", map);
//		HttpUtil.postRequest(url, map);
		StringBuilder param  = new StringBuilder();
		param.append("userCode=").append(userCode).append("&page=").append(pageNo)
			.append("&pageSize=").append(pageSize).append("&languageCode=").append(languageCode)
			.append("&userType=" ).append(userType);
		if(!StringUtil.isEmpty(timeRange)){
			param.append("&timeRange=").append(timeRange);
		}
		if(!StringUtil.isEmpty(msgTypeCode) ){
			param.append("&msgTypeCode=").append(msgTypeCode);
		}
		String jsonData = HttpUtil.getReqeuestContent(
				"rest.message.list", param.toString());
		
		JSONObject jsonObj = new JSONObject();
		jsonObj = JSONObject.fromObject(jsonData);
		String pageJson = jsonObj.getString("pageData");
		String messageTypesJson = jsonObj.getString("messageTypeList");
		String messageCount = jsonObj.getString("messageCount");
		
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put("result", Message.class);
		Page page = (Page) JsonUtil.getDTO(pageJson, Page.class, mapData);
		List<MessageType> types = JsonUtil.getDTOList(messageTypesJson, MessageType.class);
		
		paserLocalName(types);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageData", page);
		map.put("messageTypeList", types);
		map.put("messageCount", Integer.parseInt(messageCount));
		return map;
	}
	
	public Message getMessageByCode(String code) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.message.getByCode", "code=" + code);
		return (Message) JsonUtil.getDTO(jsonData, Message.class);
	}
	
	@Override
	public Page queryMyMessageList(Message message, String userCode,
			int pageNo, int pageSize) throws Exception {
		return null;
	}
	
	@Override
	public boolean saveMessage(Message message) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateMessage(Message message) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateStatus(String ids, int status) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean editDialogStatus(int id, int isDialog) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delMessageById(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Message getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page pageMessage(Message message, int pageNo, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map> queryMesageCount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int queryMessageCount(String userCode, String msgTypeCode,
			String languageCode, int userType, int isDialog, Integer timeRange,
			String createTime) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressWarnings("unchecked")
	public List<Message> queryUserMessage(int userType, String languageCode,
			String userCode) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.sale.queryUserMessage", 
				"userType=" + userType + "&languageCode=" + languageCode + "&userCode=" + userCode);
		List<Message> messages = JsonUtil.getDTOList(jsonData, Message.class);
		return messages;
	}

	@SuppressWarnings("unchecked")
	public List<Message> queryUserMessageNew(int userType, String languageCode,
			String userCode) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.message.queryUserMessageNew", 
				"userType=" + userType + "&languageCode=" + languageCode + 
				"&userCode=" +userCode);
		return JsonUtil.getDTOList(jsonData, Message.class);
	}

	@Override
	public List<Message> getMessageBySoftCodeAndVersion(String softwareCode)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delMessageBySoftCodeAndVersion(String softwareCode)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int queryMessageCountNotRead(String userCode, String msgTypeCode,
			String languageCode, int userType, int isDialog, Integer timeRange,
			String createTime) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> queryMessageCountNotReadNew(String userCode, String msgTypeCode,
			String languageCode, int userType, int isDialog, Integer timeRange,
			String createTime) throws Exception {
		String jsonData = HttpUtil.getReqeuestContent("rest.customer.messageCountNotRead",
				"userCode=" + userCode + "&msgTypeCode=" + msgTypeCode + 
				"&languageCode=" + languageCode + "&userType=" + userType + 
				"&isDialog=" + isDialog + "&timeRange=" + timeRange + 
				"&createTime=" + createTime);
		Map<String, Object> dataMap = JsonUtil.getMapFromJson(jsonData);
		return dataMap;
	}

	@Override
	public List<Message> queryUpdateMessage(String startId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Message> querySystemMessage(String startId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	private void objToMap(Map<String, Object> map,Object obj){
			
			Field[] fields = obj.getClass().getDeclaredFields();
			if(fields!=null){
				String name;
				Object value;
				for(Field field:fields){
					name = field.getName();
					if(name.equals("serialVersionUID"))continue;
					try {
						value = PropertyUtils.getProperty(obj, field.getName());
						if(value!=null){
							map.put(name, value);
						}
					} catch ( Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

	/**
	 * 国际化键值转成实际名称 
	 * @param menuList
	 */
	public void paserLocalName(List<MessageType> messageTypeList) {
		if (messageTypeList == null || messageTypeList.size() == 0)
			return;
		for (MessageType menu : messageTypeList) {
			paserName(menu);
		}
	}

	public void paserName(MessageType messageType) {
		if (messageType == null || StringUtils.isEmpty(messageType.getName()))
			return;

		String key = messageType.getName();
		String name = FreeMarkerPaser.getBundleValue(key);
		if (StringUtils.isNotEmpty(name)) {
			messageType.setName(name);
		}
	}
	
}