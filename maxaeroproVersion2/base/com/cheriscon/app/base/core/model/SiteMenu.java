package com.cheriscon.app.base.core.model;

import java.io.Serializable;
import java.util.List;


/**
 * 站点菜单
 * 
 * @author kingapex
 * 
 */
public class SiteMenu implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6671862044724283307L;
	private Integer menuid;
	private Integer parentid;
	private String name;
	private String url;
	private String target;
	private Integer sort;
	
	private String type;		//导航类型　空为默认菜单，1:底部菜单
	private String language_code;   

	public SiteMenu() {
		hasChildren = false;
	}

	// 子列表，非数据库字段
	private List<SiteMenu> children;

	// 是否有子，非数据库字段
	private boolean hasChildren;

	public Integer getMenuid() {
		return menuid;
	}

	public void setMenuid(Integer menuid) {
		this.menuid = menuid;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public List<SiteMenu> getChildren() {
		return children;
	}

	public void setChildren(List<SiteMenu> children) {
		this.children = children;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public boolean getHasChildren() {
		hasChildren = this.children == null || this.children.isEmpty() ? false
				: true;
		return hasChildren;
	}

	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}

	public String getLanguage_code() {
		return language_code;
	}

	public void setLanguage_code(String language_code) {
		this.language_code = language_code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}
