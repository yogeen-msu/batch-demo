package com.cheriscon.app.base.core.model;

import java.io.Serializable;

public class AuthAction implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2371805497898445833L;
	
	private Integer actid;
	private String name;
	private String type;
	private String objvalue;

	public Integer getActid() {
		return actid;
	}

	public void setActid(Integer actid) {
		this.actid = actid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getObjvalue() {
		return objvalue;
	}

	public void setObjvalue(String objvalue) {
		this.objvalue = objvalue;
	}

}
