package com.cheriscon.app.base.core.service.impl.cache;

import java.util.List;

import com.cheriscon.app.base.core.model.SiteMenu;
import com.cheriscon.app.base.core.service.ISiteMenuManager;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.cache.AbstractCacheProxy;

public class SiteMenuCacheProxy extends AbstractCacheProxy<List<SiteMenu>> implements ISiteMenuManager {
	public  static final String MENU_LIST_CACHE_KEY = "siteMenuList";
	private ISiteMenuManager siteMenuManager;
	public SiteMenuCacheProxy(ISiteMenuManager siteMenuManager) {
		super(MENU_LIST_CACHE_KEY);
		this.siteMenuManager = siteMenuManager;
		 
	}

//	private void cleanCache(){
//		CopSite site  = CopContext.getContext().getCurrentSite();
//		this.cache.remove( MENU_LIST_CACHE_KEY+"_"+site.getUserid() +"_"+site.getId());
//	}
//	
//	
//	public void add(SiteMenu siteMenu) {
//		this.siteMenuManager.add(siteMenu);
//		this.cleanCache();
//	}
//
//	
//	public void delete(Integer id) {
//		this.siteMenuManager.delete(id);
//		this.cleanCache();
//	 
//	}
//
//	
//	public void edit(SiteMenu siteMenu) {
//		this.siteMenuManager.edit(siteMenu);
//		this.cleanCache();
//	}
//
//	
//	public SiteMenu get(Integer menuid) {
//		return this.siteMenuManager.get(menuid);
//	}
//
//	
//	public List<SiteMenu> list(Integer parentid) {
//		return list(parentid,null,null);
//	}
//
//	
//	public void updateSort(Integer[] menuid, Integer[] sort) {
//		this.siteMenuManager.updateSort(menuid, sort);
//		this.cleanCache();
//	}

	public List<SiteMenu> list(Integer parentid, Language language) {
		return list(parentid,null,language);
	}

	@Override
	public List<SiteMenu> list(Integer parentid, String type, Language language) {
		CopSite site  = CopContext.getContext().getCurrentSite();
		List<SiteMenu> menuList  = null;
		if(language != null){
			menuList  = this.cache.get( MENU_LIST_CACHE_KEY+"_"+site.getUserid() +"_"+site.getId()+"_"+language.getCode()+"_"+type+"_"+parentid);
		}
		
		if(menuList== null ){
			menuList = this.siteMenuManager.list(parentid,type,language);
			this.cache.put( MENU_LIST_CACHE_KEY+"_"+site.getUserid() +"_"+site.getId()+"_"+language.getCode()+"_"+type+"_"+parentid,menuList);
			if(this.logger.isDebugEnabled()){
				this.logger.debug("load sitemenu from database");
			}
		}else{
			if(this.logger.isDebugEnabled()){
				this.logger.debug("load sitemenu from cache");
			}
		}
		
		return menuList;
	}
}
