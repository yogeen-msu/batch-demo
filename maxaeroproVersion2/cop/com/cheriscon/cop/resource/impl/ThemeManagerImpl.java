package com.cheriscon.cop.resource.impl;


import java.util.HashMap;
import java.util.Map;

import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.model.Theme;

public class ThemeManagerImpl  implements IThemeManager {
 
	private Map<String , Theme> themeMap  = new HashMap<String, Theme>();

	public void clean() {
//		this.baseDaoSupport.execute("truncate table theme");
	}
	public Theme getTheme(Integer themeid) {
		Theme theme = themeMap.get("theme_id_"+themeid);
		if(theme ==null){
			theme = (Theme)HttpUtil.getReqeuestContentObject("rest.theme.get", "themeid="+themeid,Theme.class);
			themeMap.put("theme_id_"+themeid,theme);
		}
		return theme;
	}

}
