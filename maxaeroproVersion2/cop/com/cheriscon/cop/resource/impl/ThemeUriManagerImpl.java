package com.cheriscon.cop.resource.impl;

import java.util.List;

import com.cheriscon.common.utils.HttpUtil;
import com.cheriscon.cop.resource.IThemeUriManager;
import com.cheriscon.cop.resource.model.ThemeUri;
import com.cheriscon.cop.sdk.context.CopSetting;

public class ThemeUriManagerImpl  implements
		IThemeUriManager {

	public void clean() {
//		this.baseDaoSupport.execute("truncate table themeuri");
		
	}
	public ThemeUri get(Integer id) {
		
//		return this.baseDaoSupport.queryForObject("select * from themeuri where id=?", ThemeUri.class, id);
		return null;
	}
	
	public void edit(List<ThemeUri> uriList) {
		for(ThemeUri uri: uriList){
//			this.baseDaoSupport.update("themeuri", uri, "id="+uri.getId());
		}
	}
	 
	public List<ThemeUri> list( ) {
//		String sql ="select * from themeuri";
//		return this.baseDaoSupport.queryForList(sql, ThemeUri.class);
		List<ThemeUri> themeUris = HttpUtil.getReqeuestContentList("rest.theme.uri.list", null, ThemeUri.class);
		return themeUris;
	}

	
	public ThemeUri getPath( String uri) {
		List<ThemeUri> list = list();
		 
		for(ThemeUri themeUri:list){
			if(themeUri.getUri().equals(uri)){
				return themeUri;
			}
		}
		return null;
	}

	
	public void add(ThemeUri uri) {
//		this.baseDaoSupport.insert("themeuri", uri);
	}

	
	public void delete(int id){
//		this.baseDaoSupport.execute("delete from themeuri where id=? ", id);
	}

	public void edit(ThemeUri themeUri) {
//		this.baseDaoSupport.update("themeuri", themeUri, "id="+themeUri.getId());
	}


}
