package com.cheriscon.cop.resource.impl.cache;

import java.util.List;

import com.cheriscon.cop.processor.core.CopException;
import com.cheriscon.cop.resource.IAppManager;
import com.cheriscon.cop.resource.model.CopApp;
import com.cheriscon.framework.cache.AbstractCacheProxy;
import com.cheriscon.framework.cache.CacheFactory;

/**
 * App Manager的缓存代理
 * @author kingapex
 * <p>2009-12-16 下午05:15:28</p>
 * @version 1.0
 */
public class AppCacheProxy extends AbstractCacheProxy<List<CopApp>> implements IAppManager {
	
	
	private IAppManager appManager;
	private static final String APP_LIST_CACHE_KEY = "applist";
	
	public  AppCacheProxy(IAppManager appManager){
		super(CacheFactory.APP_CACHE_NAME_KEY  );
		this.appManager = appManager;
	}
	
	
	
	public void add(CopApp app) {
		cache.clear();
	    appManager.add(app);
	}

	
	public CopApp get(String appid) {
		
		if(logger.isDebugEnabled()){
			logger.debug("get app : "+ appid);
		}
		List<CopApp> appList = this.list();
	 
		for(CopApp app :appList){
			if(app.getAppid().equals(appid)){
				return app;
			 
			}
		}
		
		throw new  CopException("App not found");
	}

	
	public List<CopApp> list() {
		
		List<CopApp> appList = this.cache.get(APP_LIST_CACHE_KEY);
		
		if(appList==null){
			if(logger.isDebugEnabled()){
				logger.debug("get applist from database");
			}
			appList = appManager.list();
			 this.cache.put(APP_LIST_CACHE_KEY, appList);
		}else{
//			if(logger.isDebugEnabled()){
//				logger.debug("get applist from cache");
//			}
		}
		return appList;
	}
	 
}
