package com.cheriscon.cop.sdk.context;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.cheriscon.app.base.core.model.MultiSite;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

public class CopContext {
	private static ThreadLocal<HttpServletRequest> HttpRequestHolder = new ThreadLocal<HttpServletRequest>();
	private static ThreadLocal<CopContext> CopContextHolder = new ThreadLocal<CopContext>();

	public static void setContext(CopContext context) {
		CopContextHolder.set(context);

	}

	public static void remove() {
		CopContextHolder.remove();
	}

	public static CopContext getContext() {
		CopContext context = CopContextHolder.get();
		return context;
	}

	public static void setHttpRequest(HttpServletRequest request) {
		HttpRequestHolder.set(request);
	}

	public static HttpServletRequest getHttpRequest() {
		return HttpRequestHolder.get();
	}
	
	public static void setDefaultLocal(){
		Locale.setDefault(new Locale("en", "US"));
	}
	
	public static Locale getDefaultLocal(){
		Locale locale = HttpRequestHolder.get().getLocale();
//		String contry = locale.getCountry();
		String language = locale.getLanguage();
		if(language.toLowerCase().equals("zh")){
			locale = new Locale("zh", "CN");
		}else if(language.toLowerCase().equals("de")){ //德语
			locale = new Locale("de", "DE");
		}else if(language.toLowerCase().equals("es")){ //西班牙语
			locale = new Locale("es", "LA");
		}
//		else{
			locale = new Locale("en", "US");
//		}
		return locale;
//		System.out.println(contry + " ==== " + language );
//		return new Locale("en", "US");
	}

	//当前站点（主站）
	private CopSite currentSite;

	//当前子站
	private MultiSite currentChildSite;

	public CopSite getCurrentSite() {
		return currentSite;
	}

	public void setCurrentSite(CopSite site) {
		currentSite = site;
	}

	public MultiSite getCurrentChildSite() {
		return currentChildSite;
	}

	public void setCurrentChildSite(MultiSite currentChildSite) {
		this.currentChildSite = currentChildSite;
	}

	//得到当前站点上下文
	public String getContextPath() {
		if ("2".equals(CopSetting.RUNMODE)) {
			CopSite site = this.getCurrentSite();
			StringBuffer context = new StringBuffer("/user");
			context.append("/");
			context.append(site.getUserid());
			context.append("/");
			context.append(site.getId());
			return context.toString();
		} else {
			return "";
		}
	}

	/**
	 * 获取当前站点资源的域名
	 * @return
	 * 单机版运行模式：
	 * 1.静态资源合并返回 空串   或虚拟目录 
	 * 2.静态资源分离返回 静态资源的域名 如：http://static.domain.com
	 * 
	 * SAAS模式运行：
	 * 1.静态资源合并返回 当前域名/userid/siteid 如: http://www.domain.com/user/1/1
	 * 2.静态资源分离返回 静态资源的域名/userid/siteid 如：http://static.domain.com/user/1/1 
	 */
	public String getResDomain() {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String domain = null;

		//如果采用静态资源分离模式由静态资源服务器地址读取
		//如果不采用，则由应用服务器所在地址读取
		//开发模式下不采用静态资源分离
		if ("1".equals(CopSetting.RESOURCEMODE) && !CopSetting.DEVELOPMENT_MODEL) {
			domain = CopSetting.IMG_SERVER_DOMAIN;
		} else {
			domain = request.getContextPath();
		}

		if (domain.endsWith("/"))
			domain = domain.substring(0, domain.length() - 1);//like this: http://wwww.abc.com/javamall or ''	

		//如果采用独立版运行模式路径不加userid,siteid
		//如果是saas版运行模式，加上userid,siteid
		domain = domain + CopContext.getContext().getContextPath();
		return domain;
	}

	/**
	 * 获取当前站点资源的服务器路径
	 * @return
	 * 单机版运行模式： d:/app/static
	 * SAAS模式运行：
	 * 返回 当前应用服务器路径/userid/siteid 如：d:/static/user/1/1
	 */
	public String getResPath() {

		String path = CopSetting.IMG_SERVER_PATH;

		if (path.endsWith("/"))
			path = path.substring(0, path.length() - 1);

		//如果采用独立版运行模式路径不加userid,siteid
		//如果是saas版运行模式，加上userid,siteid
		path = path + CopContext.getContext().getContextPath();
		return path;
	}

}
