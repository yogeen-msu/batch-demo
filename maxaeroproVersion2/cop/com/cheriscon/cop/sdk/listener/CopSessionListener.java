package com.cheriscon.cop.sdk.listener;

import java.util.List;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import com.cheriscon.cop.resource.IAppManager;
import com.cheriscon.cop.resource.model.CopApp;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.IApp;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.spring.SpringContextHolder;

public class CopSessionListener implements HttpSessionListener {
	
	protected final Logger logger = Logger.getLogger(getClass());
	
	public void sessionCreated(HttpSessionEvent se) {
		
	}
	
	public void sessionDestroyed(HttpSessionEvent se) {
		
		if(logger.isDebugEnabled()){
			logger.debug("session destroyed..");
		}
		
		//如果是已经安装状态
		if("YES".equals( CopSetting.INSTALL_LOCK.toUpperCase())){
			
			if(logger.isDebugEnabled()){
				logger.debug("installed...");
			}
			
			CopSite site = (CopSite) se.getSession().getAttribute("site_key");
			String sessionid = se.getSession().getId();
//			IAppManager appManager = SpringContextHolder.getBean("appManager");
//			List<CopApp> appList  = appManager.list();
//			for(CopApp CopApp:appList){
//
//				String appid  = CopApp.getAppid();
//				
//				if(logger.isDebugEnabled()){
//					logger.debug("call app["+appid+"] destory...");
//				}
//				
//				
//				IApp app = SpringContextHolder.getBean(appid);
//				app.sessionDestroyed(sessionid,site);
//			}
		}else{
			if(logger.isDebugEnabled()){
				logger.debug("not installed...");
			}
		}
	}

}
