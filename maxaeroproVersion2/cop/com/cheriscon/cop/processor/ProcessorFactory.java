package com.cheriscon.cop.processor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

//import com.cheriscon.cop.processor.backend.BackgroundProcessor;
import com.cheriscon.cop.processor.facade.FacadePageProcessor;
import com.cheriscon.cop.processor.facade.ResourceProcessor;
import com.cheriscon.cop.processor.facade.SiteMapProcessor;
import com.cheriscon.cop.processor.facade.WebResourceProcessor;
import com.cheriscon.cop.processor.facade.WidgetProcessor;
import com.cheriscon.cop.processor.facade.WidgetSettingProcessor;
import com.cheriscon.cop.resource.IAppManager;
import com.cheriscon.cop.resource.model.CopApp;
import com.cheriscon.cop.sdk.context.ConnectType;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.spring.SpringContextHolder;

/**
 * @author kingapex
 * @version 1.0
 * @created 13-十月-2009 11:36:29 
 */
public abstract class ProcessorFactory {

	/**
	 * 
	 * @param uri
	 */
	public static Processor newProcessorInstance(String uri,HttpServletRequest httpRequest){
		Processor processor =null;
 	//if(uri.endsWith(".action") || uri.endsWith(".do") ) return null;
		if(uri.startsWith("/statics")) return null;
		if(uri.startsWith("/install") && !uri.startsWith("/install.html")  ) return null;
		
		//sitemap生成
		if(uri.toLowerCase().equals( "/sitemap.xml")) { 
			return new SiteMapProcessor();
		}
		
		if(uri.toLowerCase().equals("/robots.txt")){return null;}
		
//		IAppManager appManager = SpringContextHolder.getBean("appManager");
//		List<CopApp> appList  =  appManager.list();
//		String path =httpRequest.getServletPath();
//		for(CopApp app:appList){
//			if(app.getDeployment()==ConnectType.remote) continue;
			
//			if(path.startsWith(app.getPath() +"/admin" )) {
//				if( isExinclude (path)){return null;}
//				
//				processor = new BackgroundProcessor();
//				return processor;
//			} 
//			if( path.startsWith( app.getPath()  ) ){
//				return null;
//			}
//		}
		
		if(uri.startsWith("/validcode")) return null;
		if(uri.startsWith("/commons")) return null;
		if(uri.startsWith("/editor/")) return null;
		if(uri.startsWith("/cop/")) return null;
		if(uri.startsWith("/test/")) return null;
		if(uri.endsWith("favicon.ico")) return null;
		
		if (uri.indexOf("/headerresource")>=0) { 
			return new ResourceProcessor();
		} 
		if (uri.startsWith("/resource/")) { 
			return new WebResourceProcessor();
		} 
		
		if(isExinclude(uri)) return null;
 
//		if (uri.startsWith("/admin/")) { 
//			if (!uri.startsWith("/admin/themes/")) {
//					processor = new BackgroundProcessor();
//			}
//		}else 
		if(uri.startsWith("/autel-cqc/") || uri.startsWith("/autel")){
//			processor = new BackgroundProcessor();
		} else if (uri.startsWith("/widget")) {
			
			if(uri.startsWith("/widgetSetting/")){
				processor = new WidgetSettingProcessor();
			}else if(uri.startsWith("/widgetBundle/")){
			//	processor = new WidgetBundleProcessor();
			}else{		 
				processor = new WidgetProcessor();
			}
		} else{
 
			if(uri.endsWith(".action") || uri.endsWith(".do") ) return null;
			if(CopSetting.TEMPLATEENGINE.equals("on"))
				processor = new FacadePageProcessor();
		}
		 
		return processor;
	}
	
 

	private static boolean isExinclude(String uri){
		
		String[] exts=new String[]{"jpg","gif","js","png","css","doc","xls","swf"};
		for(String ext:exts){ 
			if(uri.toUpperCase().endsWith(ext.toUpperCase())){
				return true;
			}
		}
		
		return false;
	}

}