package com.cheriscon.cop.processor;

import com.cheriscon.cop.resource.model.CopApp;
import com.cheriscon.cop.resource.model.CopSite;
 
/**
 * 前台页面实体
 * @author kingapex
 * @version 1.0
 * @created 22-十月-2009 16:31:34
 */
public class FacadePage {
	
	private Integer id;
	private CopSite site;
	private String uri;
	private CopApp app;
	

	public FacadePage(){

	}

	public FacadePage(CopSite site){
		this.site = site;
	}

	public CopSite getSite() {
		return site;
	}

	public void setSite(CopSite site) {
		this.site = site;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CopApp getApp() {
		return app;
	}

	public void setApp(CopApp app) {
		this.app = app;
	}
	
	
 
	

}