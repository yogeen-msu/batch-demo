package com.cheriscon.cop.processor.facade.support;

import java.util.Map;

import com.cheriscon.cop.processor.core.CopException;
import com.cheriscon.cop.processor.core.UrlNotFoundException;
import com.cheriscon.cop.processor.widget.IWidgetPaser;
import com.cheriscon.cop.sdk.widget.IWidget;
import com.cheriscon.framework.component.context.WidgetContext;
import com.cheriscon.framework.context.spring.SpringContextHolder;

/**
 * 本地挂件解析器     
 * @author kingapex
 * 2010-2-8下午03:56:17
 */
public class LocalWidgetPaser implements IWidgetPaser {

	 
	
	public String pase(Map<String, String> params) {
		if(params==null) throw new CopException("挂件参数不能为空");
		
		String widgetType = params.get("type");
		if(widgetType==null) throw new CopException("挂件类型不能为空");
		
		if(!WidgetContext.getWidgetState(widgetType)){ return "此挂件已停用";}
		
		
	 	//System.out.println("processor "+ widgetType+"["+params.get("widgetid")+"]");
		try{
			IWidget widget =SpringContextHolder.getBean(widgetType);
			
			String content;
			if(widget==null) content=("widget["+widgetType+"]not found");
			else  {   
				content= widget.process(params); //解析挂件内容
				widget.update(params); //执行挂件更新操作
			}

			return content;
			
		}catch(UrlNotFoundException e){
			throw e;		
		}catch(Exception e){
			e.printStackTrace();

			return "<script>window.location='common_error.html';</script>";
			//return "widget["+widgetType+"]pase error ";
		}		
		
	}

}
