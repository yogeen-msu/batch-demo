package com.cheriscon.cop.processor.facade.support.widget;

import java.util.Map;

import com.cheriscon.cop.processor.widget.IWidgetParamParser;
import com.cheriscon.framework.cache.AbstractCacheProxy;

public class XmlWidgetParamParserCacheProxy extends AbstractCacheProxy implements
		IWidgetParamParser {
	private static String cacheName = "widget_key";
	private IWidgetParamParser xmlWidgetParamParserImpl;
	private Map<String, Map<String, Map<String, String>>> widget;
	
	public XmlWidgetParamParserCacheProxy(IWidgetParamParser _xmlWidgetParamParserImpl) {
		super(cacheName);
		xmlWidgetParamParserImpl =  _xmlWidgetParamParserImpl;
	
	}

	public Map<String, Map<String, Map<String, String>>> parse() {
	
//		Object obj = this.cache.get("widget_obj");
		
		if(widget==null){
			widget  =this.xmlWidgetParamParserImpl.parse();
//			  this.cache.put("widget_obj", obj);
		//	  System.out.println("read from disc");
//		} else{
		//	System.out.println("read from cache");
		}
		
//		return ( Map<String, Map<String, Map<String, String>>>) obj;
		return ( Map<String, Map<String, Map<String, String>>>) widget;
	}

}
