package com.cheriscon.cop.processor.facade.support;

import java.util.Map;

import com.cheriscon.cop.processor.IPageParamJsonGetter;
import com.cheriscon.cop.processor.widget.IWidgetParamParser;
import com.cheriscon.cop.processor.widget.WidgetXmlUtil;
import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.IThemeUriManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.ThemeUri;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.context.spring.SpringContextHolder;

/**
 * 页面挂件json格式参数获取器
 * @author kingapex
 * 2010-2-10下午04:59:57
 */
public class PageParamJsonGetter implements IPageParamJsonGetter {
	private IWidgetParamParser widgetParamParser;
	 
	private IThemeManager themeManager ;
	
	
	
	public String getJson(String uri) {
		//去掉uri问号以后的东西
		if(uri.indexOf('?')>0)
			uri = uri.substring(0, uri.indexOf('?') );
		
		//站点使用模板
		CopSite site = CopContext.getContext().getCurrentSite();
	 
		
		//rewrite url = pageId
		IThemeUriManager themeUriManager =  SpringContextHolder.getBean("themeUriManager");
		ThemeUri themeUri  = themeUriManager.getPath(  uri);
		uri = themeUri.getPath();
		
		//此站点挂件参数集合
		Map<String, Map<String, Map<String, String>>> pages = this.widgetParamParser
				.parse();
		
		//此页面的挂件参数集合
		Map<String, Map<String,String>> params=pages.get(uri);
		String json = WidgetXmlUtil.mapToJson(params);
		json="{'pageId':'"+uri+"',params:"+json+"}";
		return json;
	}
	
	
	public void setWidgetParamParser(IWidgetParamParser widgetParamParser) {
		this.widgetParamParser = widgetParamParser;
	}
 

	public void setThemeManager(IThemeManager themeManager) {
		this.themeManager = themeManager;
	}

}
