package com.cheriscon.cop.processor.facade.support.widget;

import java.util.HashMap;
import java.util.Map;



import com.cheriscon.app.base.core.service.IWidgetCacheManager;
import com.cheriscon.cop.processor.widget.IWidgetHtmlGetter;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.widget.IWidget;
import com.cheriscon.framework.cache.AbstractCacheProxy;
import com.cheriscon.framework.cache.CacheFactory;
import com.cheriscon.framework.context.spring.SpringContextHolder;

/**
 * Saas挂件html缓存代理<br>
 * 缓存规则：
 * 以 userid_siteid为key划分出缓存区域，以map作为值。
 * map中存储此站点的挂件html片断 map
 * 
 * @author kingapex
 *
 */
public class SaasWdgtHtmlGetterCacheProxy extends AbstractCacheProxy<Map<String,String>>
		implements IWidgetHtmlGetter {
	private IWidgetHtmlGetter widgetHtmlGetter;

	public SaasWdgtHtmlGetterCacheProxy(IWidgetHtmlGetter _widgetHtmlGetter) {
		super(CacheFactory.WIDGET_CACHE_NAME_KEY);
		this.widgetHtmlGetter = _widgetHtmlGetter;
	} 
	
	private boolean getCachcopen(){
		 IWidgetCacheManager widgetCacheManager =  SpringContextHolder.getBean("widgetCacheManager");
		 return  widgetCacheManager.isOpen();
	}
	/**
	 * 实现挂件解析的缓存包装
	 */
	public String process(Map<String, String> params, String pageUri) {
		String widgetType = params.get("type");
		String html = null;
		
		//得到挂件类
		IWidget widget = SpringContextHolder.getBean(widgetType);
		if (widget == null)
			return "widget[" + widgetType + "] is null";
		
//		if(this.logger.isDebugEnabled() ){
//			this.logger.debug("缓存类解析挂件["+widgetType+"]");
//		}
		// 挂件可以被缓存，则尝试由缓存读取
		if (getCachcopen() && widget.cacheAble()) { 
//			if(this.logger.isDebugEnabled() ){
//				this.logger.debug("此挂件缓存");
//			}
			//以当前站点作为整站挂件缓存的key
			//缓存的是一个map，map的key为当前页面uri和挂件id的组合
			CopSite site = CopContext.getContext().getCurrentSite();
			String site_key = "widget_"+ site.getUserid() +"_"+site.getId();
			
			//由缓存中找此站点的挂件级存集合
			Map<String,String> htmlCache=  cache.get(site_key);
			
			//未找到缓存，生成一个新map
			if(  htmlCache==null){
				htmlCache = new HashMap<String, String>();
				cache.put(site_key, htmlCache);
			}
			
			//挂件map集合的key
			String key = pageUri + "_" + params.get("widgetid");
			
			//获取挂件html
			html = htmlCache.get(key);
			
			// 缓存未命中，解析挂件的内容并压入缓存
			if (html == null || "widget_forward".equals(html)) { 
//				if(this.logger.isDebugEnabled() ){
//					this.logger.debug("缓存未命中,the key is ["+key+"]");
//				}
				html = widgetHtmlGetter.process(params, pageUri);
//				
//				if(this.logger.isDebugEnabled() ){
//					this.logger.debug("put html to key ["+ key +"]");
//				}
				
				htmlCache.put(key, html);
				
			// 如果缓存命中，则只执行挂件更新操作	
			} else { 
//				if(this.logger.isDebugEnabled() ){
//					this.logger.debug("缓存命中");
//				}
				widget.update(params);
			}
		} else { // 挂件不缓存，由htmlGetter解析
//			if(this.logger.isDebugEnabled() ){
//				this.logger.debug("此挂件不缓存");
//			}
			html = widgetHtmlGetter.process(params, pageUri);
		}

		return html;
	}
	
	


}
