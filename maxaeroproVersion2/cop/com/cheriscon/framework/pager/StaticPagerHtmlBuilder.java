package com.cheriscon.framework.pager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;


import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.RequestUtil;

/**
 * 伪静态列表分页
 * 
 * @author kingapex
 * 
 */
public class StaticPagerHtmlBuilder {

	protected String url;
	private HttpServletRequest request;
	private long pageNum;
	private long totalCount;
	private int pageSize;
	private long pageCount;
	private int showCount = 10;

	public StaticPagerHtmlBuilder(long _pageNum, long _totalCount, int _pageSize) {
		pageNum = _pageNum;
		totalCount = _totalCount;
		pageSize = _pageSize;
		request = ThreadContextHolder.getHttpRequest();
	}

	/*
	public String buildPageHtml() {
		this.init();
		StringBuffer pageStr = new StringBuffer("");
		pageStr.append("<table align=\"right\" class=\"pager\"><tbody><tr>");
		pageStr.append(this.getHeadString());
		pageStr.append(this.());
		pageStr.append(this.getFooterString());
		pageStr.append("</tr></tbody></table>");
		return pageStr.toString();
	}*/

	public String buildPageHtml() {
		this.init();

		if (totalCount <= pageSize){
			if (totalCount<1) {
				return "<div style=\"background-color:#f2f2f2;padding-bottom:20px;padding-top:20px;text-align:center;\">"+FreeMarkerPaser.getBundleValue("common.have.nrecord")+"</div>";
			}
			return "";
			
		}
		
		StringBuffer pageStr = new StringBuffer("");
		pageStr.append("<div>");
		pageStr.append(this.getHeadString());
		pageStr.append(this.getBodyString());
		pageStr.append("</div>");
		return pageStr.toString();
	}

	/**
	 * 计算并初始化信息
	 * 
	 */
	private void init() {

		pageSize = pageSize < 1 ? 1 : pageSize;

		pageCount = totalCount / pageSize;
		pageCount = totalCount % pageSize > 0 ? pageCount + 1 : pageCount;

		pageNum = pageNum > pageCount ? pageCount : pageNum;
		pageNum = pageNum < 1 ? 1 : pageNum;

		url = request.getServletPath();

	}

	/**
	 * 生成分页头字串
	 * 
	 * @return
	 */
	protected String getHeadString() {

		StringBuffer headString = new StringBuffer("");

		headString.append("<div style=\"font-size:12px; float:left;\">");
		headString.append("</div>");

		return headString.toString();
	}

	/**
	 * 生成分页头字串
	 * 
	 * @return
	 */
	/*
	protected String getHeadString() {

		StringBuffer headString = new StringBuffer("");
		headString.append("<td>");

		if (pageNum > 1) { // 不是第一页，有上一页

			headString.append("<a title=\"上一页\"");
			headString.append(" onmouseout=\"this.className = 'prev'\" ");
			headString.append("  onmouseover=\"this.className = 'onprev'\" ");
			headString.append(" class=\"prev\" ");
			headString.append(" href=\"");
			headString.append(getUrlStr(this.pageNum - 1));
			headString.append("\" >上一页");
			headString.append("</a>\n");

		} else {// 第一页
			if (totalCount > 0 ) {
				headString.append("<span title=\"已经是第一页\" ");
				headString.append(" class=\"prev\"> 已经是第一页</span>");
			}
		}
		headString.append("</td>");
		return headString.toString();
	}*/

	/**
	 * 生成分页尾字串
	 * 
	 * @return
	 */
	protected String getFooterString() {
		StringBuffer footerStr = new StringBuffer("");
		footerStr.append("<td style=\"padding-right: 20px;\">");
		if (pageNum < pageCount) {

			footerStr.append("<a title=\"下一页\" onmouseout=\"this.className = 'next'\" onmouseover=\"this.className = 'onnext'\" class=\"next\" ");
			footerStr.append(" href=\"");
			footerStr.append(getUrlStr(this.pageNum + 1));
			footerStr.append("\"");
			footerStr.append("下一页</a>");

		} else {
			footerStr.append("<span title=\"已经是最后一页\" class=\"next\">已经是最后一页</span>");
		}
		footerStr.append("</td>\n");
		return footerStr.toString();
	}

	/**
	 * 生成分页主体字串
	 * 
	 * @return
	 */
	/*
	protected String getBodyString() {

		StringBuffer pageStr = new StringBuffer();

		long start = pageNum - showCount / 2;
		start = start <= 1 ? 1 : start;

		long end = start + showCount;
		end = end > pageCount ? pageCount : end;
		pageStr.append("<td>");

		for (long i = start; i <= end; i++) {

			if (i != pageNum) {
				pageStr.append("<a");
				pageStr.append(" href=\"");

				pageStr.append(getUrlStr(i));
				pageStr.append("\">");

				pageStr.append(i);
				pageStr.append("</a>\n");
			} else {
				pageStr.append(" <strong class=\"pagecurrent\">");
				pageStr.append(i);
				pageStr.append("</strong> ");
			}

		}
		pageStr.append("</td>");
		return pageStr.toString();
	}*/

	/**
	 * 生成分页主体字串
	 * 
	 * @return
	 */
	protected String getBodyString() {
		StringBuffer pageStr = new StringBuffer();
		pageStr.append("<div class=\"my_page\">");

		long start = 1;
		long end = pageCount;
		if(pageCount > showCount){
			int pageSpace = showCount / 2;
			if((pageNum - pageSpace) < 0){
				start = 1;
				end = showCount;
			}else{
				end = ((pageNum + pageSpace) > pageCount) ? pageCount : (pageNum + pageSpace);
				start = (end - showCount) + 1;
			}
		}
		
		pageStr.append("");

		pageStr.append("<span class=\"page_all\">");

		for (long i = start; i <= end; i++) {

			pageStr.append("<a");
			pageStr.append(" href=\"");
			pageStr.append(getUrlStr(i));
			if (i != pageNum) {
				pageStr.append("\" class=\"no_on page_a_query\" >");
			} else {
				pageStr.append("\" class=\"page_on page_a_query\" >");
			}

			pageStr.append(i);
			pageStr.append("</a>\n");
		}
		pageStr.append("</span>");
		
		

		if (pageCount <= 1) {

		} else if (pageNum == 1) {
			pageStr.append("<span class=\"next_pa\">");
			pageStr.append("<a class='page_a_query' href=\"").append(getUrlStr(this.pageNum + 1)).append("\" >");
			pageStr.append(FreeMarkerPaser.getBundleValue("common.next.page"));
			pageStr.append(" <img src=\"themes/autel/images/youp.gif\" width=\"4\" height=\"7\" />");
			pageStr.append("</a>");
			pageStr.append("</span>");
			
			//最后一页
			pageStr.append("<span class=\"next_pa\">&nbsp;&nbsp;");
			pageStr.append("<a class='page_a_query' href=\"").append(getUrlStr(pageCount)).append("\" >");
			pageStr.append(FreeMarkerPaser.getBundleValue("common.last.page"));
			pageStr.append(" <img src=\"themes/autel/images/youp.gif\" width=\"4\" height=\"7\" />");
			pageStr.append("</a>");
			pageStr.append("</span>");
			
		} else if (pageNum == pageCount) {
			//首页
			pageStr.append("<span style='margin-right:5px;' class=\"next_pa\">&nbsp;&nbsp;");
			pageStr.append("<a  class='page_a_query' href=\"").append(getUrlStr(1)).append("\" >");
			pageStr.append("<img src=\"themes/autel/images/zuop.gif\" width=\"4\" height=\"7\"  />  ");
			pageStr.append(FreeMarkerPaser.getBundleValue("common.first.page"));
			pageStr.append("</a>");
			pageStr.append("</span>");
			
			pageStr.append("<span class=\"pre_pa\">");
			pageStr.append("<a  class='page_a_query' href=\"").append(getUrlStr(this.pageNum - 1)).append("\" >");
			pageStr.append("<img src=\"themes/autel/images/zuop.gif\" width=\"4\" height=\"7\"  />  ");
		    pageStr.append(FreeMarkerPaser.getBundleValue("common.previous.page"));
			pageStr.append("</a>");
			pageStr.append("</span>");
		} else {
			
			//首页
			pageStr.append("<span style='float:left;margin-right:5px;' class=\"next_pa\">&nbsp;&nbsp;");
			pageStr.append("<a class='page_a_query' href=\"").append(getUrlStr(1)).append("\" >");
			pageStr.append("<img src=\"themes/autel/images/zuop.gif\" width=\"4\" height=\"7\"  />  ");
			pageStr.append(FreeMarkerPaser.getBundleValue("common.first.page"));
			pageStr.append("</a>");
			pageStr.append("</span>");
			
			pageStr.append("<span class=\"previ\">");

			pageStr.append("<span class=\"pre_pa\">");
			pageStr.append("<a  class='page_a_query' href=\"").append(getUrlStr(this.pageNum - 1)).append("\" >");
			pageStr.append("<img src=\"themes/autel/images/zuop.gif\" width=\"4\" height=\"7\"  /> ");
			pageStr.append(FreeMarkerPaser.getBundleValue("common.previous.page"));
			pageStr.append("</a>");
			pageStr.append("</span>");

			pageStr.append("<span class=\"next_pa\">");
			pageStr.append("<a  class='page_a_query' href=\"").append(getUrlStr(this.pageNum + 1)).append("\" >");
			pageStr.append(FreeMarkerPaser.getBundleValue("common.next.page"));
			pageStr.append(" <img src=\"themes/autel/images/youp.gif\" width=\"4\"  height=\"7\" />");
			pageStr.append("</a>");
			pageStr.append("</span>");
			
			pageStr.append("</span>");
			
			//最后一页
			pageStr.append("<span class=\"next_pa\">&nbsp;&nbsp;");
			pageStr.append("<a  class='page_a_query' href=\"").append(getUrlStr(pageCount)).append("\" >");
			pageStr.append(FreeMarkerPaser.getBundleValue("common.last.page"));
			pageStr.append(" <img src=\"themes/autel/images/youp.gif\" width=\"4\" height=\"7\" />");
			pageStr.append("</a>");
			pageStr.append("</span>");
		}

		pageStr.append("</div>");
		return pageStr.toString();
	}

	/**
	 * 根据页数生成超级连接href的字串
	 * 
	 * @param page
	 * @return
	 */
	protected String getUrlStr(long page) {
		String args = "";
		HttpServletRequest httpRequest = ThreadContextHolder.getHttpRequest();
		String uri = RequestUtil.getRequestUrl(httpRequest);
		if (StringUtils.indexOf(uri, "?") != -1) {
			args = uri.substring(uri.indexOf("?"));
			uri = uri.substring(0, uri.indexOf("?"));
		}
		String pattern = "(.*)-(\\d+)-(\\d+).html";
		Pattern p = Pattern.compile(pattern, 2 | Pattern.COMMENTS);
		Matcher m = p.matcher(uri);
		String page_url = "";
		if (m.find()) {
			page_url = m.replaceAll("$1-$2");
		}

		page_url = request.getContextPath() + page_url + "-" + page;
		return page_url + ".html" + args;

	}

	public static void main(String[] args) {
		String url = "/articleList-1-2.html?arg=1";

		System.out.println(url.substring(url.indexOf("?")));
		System.out.println(url.substring(0, url.indexOf("?")));
		String pattern = "/(.*)-(\\d+)-(\\d+).html";
		Pattern p = Pattern.compile(pattern, 2 | Pattern.COMMENTS);
		Matcher m = p.matcher(url);
		if (m.find()) {
			// catid = m.replaceAll("$2");
			// page = m.replaceAll("$3");

		} else {

		}

		System.out.println(url);

		// if (m.find()) {
		// System.out.println( "1--" + m.replaceAll("page=$2"));
		// // System.out.println( "2--" + m.replaceAll("$2"));
		// }
		//

	}


}
