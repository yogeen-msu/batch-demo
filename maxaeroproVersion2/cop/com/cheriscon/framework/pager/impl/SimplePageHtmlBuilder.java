package com.cheriscon.framework.pager.impl;

import com.cheriscon.framework.pager.AbstractPageHtmlBuilder;

/**
 * 正常的页面跳转的翻页
 * @author apexking
 *
 */
public class SimplePageHtmlBuilder extends AbstractPageHtmlBuilder   {
	
	public SimplePageHtmlBuilder(long _pageNum, long _totalCount, int _pageSize) {
		super(_pageNum, _totalCount, _pageSize);
	}

	/**
	 * 生成href的字串
	 */
	
	protected String getUrlStr(long page) {
		String method =request.getMethod().toUpperCase();
		StringBuffer linkHtml = new StringBuffer();
		
		if("GET".equals(method)){
			linkHtml.append("href='");
			linkHtml.append(url);
			linkHtml.append("page=");
			linkHtml.append(page);
			linkHtml.append("'>");
		}else{
			
			String pageUrl = url + "page="+page;
			StringBuffer submitForm = new StringBuffer();
			submitForm.append("var pageForm=document.forms[0];");
			submitForm.append("pageForm['action']='").append(pageUrl).append("';");
			submitForm.append("pageForm.submit();");
			
			linkHtml.append(" href='#' ");
			linkHtml.append(" onclick=\""+submitForm+"\"");
			linkHtml.append(">");
		}
		
		return linkHtml.toString();
	}
	
 
}
