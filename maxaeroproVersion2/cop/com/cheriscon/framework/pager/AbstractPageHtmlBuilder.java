package com.cheriscon.framework.pager;

import javax.servlet.http.HttpServletRequest;

import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.RequestUtil;
//import com.opensymphony.xwork2.ActionContext;
//import com.opensymphony.xwork2.ActionSupport;

/**
 * 抽像的分页html代码生成器基类
 * 提供基本的逻辑支持
 * @author kingapex
 *
 */
public abstract class AbstractPageHtmlBuilder implements IPageHtmlBuilder {
	
	protected String url;
	protected HttpServletRequest request ;
	protected long pageNum;
	protected long totalCount;
	protected int pageSize;
	protected long pageCount;
	private int showCount = 5;
	
	public AbstractPageHtmlBuilder(long _pageNum,long _totalCount,int _pageSize){
		pageNum= _pageNum;
		totalCount= _totalCount;
		pageSize= _pageSize ;
		request = ThreadContextHolder.getHttpRequest();
	}
	
	public String buildPageHtml() {
		this.init();
		StringBuffer pageStr = new StringBuffer("");
		pageStr.append("<div class=\"page\"  align='left' >" );
//		pageStr.append(this.getHeadString());
//		pageStr.append(this.getBodyString());
//		pageStr.append(this.getFooterString());
		pageStr.append(this.getPageBody());
		pageStr.append("</div>");
		return pageStr.toString();
	}
	
	/**
	 * 翻页样式二
	 * @return
	 */
	public String getPageBody(){
		StringBuffer page = new StringBuffer();
		long showMaxSize = 0;
		if(this.totalCount <= pageSize){
			showMaxSize = this.totalCount;
		}else{
			showMaxSize = pageNum * pageSize;
			showMaxSize = (pageNum * pageSize > this.totalCount) ? this.totalCount : showMaxSize;
		}
		long showMinSize = 1;
		if(this.totalCount <= pageSize){
			showMinSize = 1;
		}else{
			showMinSize =(pageNum-1)*pageSize + 1;
		}
		//page.append("<span style='float:right;'>"+getText("common.page.number.arg2")+"&nbsp;"+showMinSize+"-"+showMaxSize+"&nbsp;"+getText("common.page.number.arg3")+"&nbsp;"+getText("common.page.number.arg4")+"&nbsp;&nbsp;"+this.totalCount+"&nbsp"+getText("common.page.number.arg5")+"</span>");
		page.append("<ul>");
		if(pageNum > 1){
			page.append("  <li class='first'><a "+this.getUrlStr(1)+"&nbsp;&nbsp;&nbsp;</a></li>");
			page.append("  <li class='pre'><a "+this.getUrlStr(pageNum -1)+"&nbsp;&nbsp;&nbsp;</a></li>");
		}
		
		page.append("  <li class='split'></li>");
		page.append("  <li>"+getText("common.page.number.arg0")+"&nbsp;<input type='text' class='page_in' id='page_query_no'  value='"+this.pageNum+"' /> &nbsp;"+getText("common.page.number.arg4")+"&nbsp;"+pageCount+"&nbsp;"+getText("common.page.number.arg6")+"</li>");
		if(pageNum < pageCount){
			page.append("  <li class='split'></li>");
			page.append("  <li class='next'><a "+this.getUrlStr(pageNum + 1)+"&nbsp;&nbsp;&nbsp;</a></li>");
			page.append("  <li class='end'><a "+this.getUrlStr(pageCount)+"&nbsp;&nbsp;&nbsp;</a></li>");
		}
		page.append("  <li class='split'></li>");
		
		page.append("  <li class='refresh'><a "+this.getUrlStr(pageNum)+"&nbsp;&nbsp;&nbsp;</a></li>\n");
		page.append("  <li class='goto' style='cursor:pointer;padding-top:3px;'><font color='#0000FF'><a  pageNum='page_query_no' "+this.getForwardUrl()+"&nbsp;&nbsp;&nbsp;"+getText("common.info.jump")+"</a></font></li>\n");
//		page.append("  <li class='goto' style='cursor:pointer'><a pageNum='page_query_no'>&nbsp;&nbsp;&nbsp;跳转</a></li>\n");
		
		page.append("</ul>");
		
		return page.toString();
	}
	public String getText(String message){
//		ActionContext ctx = ActionContext.getContext();
		//ctx.setLocale(java.util.Locale.CHINA);
//		return super.getText(message);
		return "";
	}
	
	/**
	 * 初始化url,用于地址栏方式传
	 * <br/> 
	 * 将地址栏上的参数拼装
	 *
	 */
	protected  void initUrl() {
		url =request.getContextPath()+RequestUtil.getRequestUrl(request);
		url = url.replaceAll("(&||\\?)page=(\\d+)","");
		url = url.replaceAll("(&||\\?)rmd=(\\d+)","");
		url =url.indexOf('?')>0?url+"&": url + "?";
	}
	 
	
	/**
	 * 计算并初始化信息
	 *
	 */
	protected  void init() {	
 
		pageSize = pageSize<1? 1 :pageSize;
		
		pageCount = totalCount / pageSize;
		pageCount = totalCount % pageSize > 0 ? pageCount + 1 : pageCount;
	
//		pageNum = pageNum > pageCount ? pageCount : pageNum;
		pageNum = pageNum < 1 ? 1 : pageNum;
		
		if(this.url==null)
		initUrl();
//		url = url.indexOf('?') >= 0 ? (url += "&") : (url += "?");
	}
	
	
	 /**
	  * 生成分页头字串
	  * @return
	  */
	protected String getHeadString() {

		StringBuffer headString = new StringBuffer("");
		headString.append("<span class=\"info\" >");
		headString.append("共");
		headString.append(this.totalCount);
		headString.append("条记录");
		headString.append("</span>\n");

		headString.append("<span class=\"info\">");
		headString.append(this.pageNum);
		headString.append("/");
		headString.append(this.pageCount);
		headString.append("</span>\n");

		headString.append("<ul>");
		if (pageNum > 1) {

			headString.append("<li><a " );
			headString.append(" class=\"unselected\" ");
			headString.append(this.getUrlStr(1));
			headString.append("|&lt;");
			headString.append("</a></li>\n");

			headString.append("<li><a  ");
			headString.append(" class=\"unselected\" ");
			headString.append(this.getUrlStr(pageNum - 1));
			headString.append("&lt;&lt;");
			headString.append("</a></li>\n");
		}

		return headString.toString();
	}

	/**
	 * 生成分页尾字串
	 * @return
	 */
	protected String getFooterString() {
		StringBuffer footerStr = new StringBuffer("");
		if (pageNum < pageCount) {

			footerStr.append("<li><a ");
			footerStr.append(" class=\"unselected\" ");
			footerStr.append(this.getUrlStr(pageNum + 1));
			footerStr.append("&gt;&gt;");
			footerStr.append("</a></li>\n");

			footerStr.append("<li><a ");
			footerStr.append(" class=\"unselected\" ");
			footerStr.append(this.getUrlStr(pageCount));
			footerStr.append("&gt;|");
			footerStr.append("</a></li>\n");

		}
		footerStr.append("</ul>");
		return footerStr.toString();
	}

	/**
	 * 生成分页主体字串
	 * @return
	 */
	protected String getBodyString() {

		StringBuffer pageStr = new StringBuffer();

		long start = pageNum - showCount / 2;
		start = start <= 1 ? 1 : start;

		long end = start + showCount;
		end = end > pageCount ? pageCount : end;

		for (long i = start; i <= end; i++) {

			pageStr.append("<li><a ");
			if (i != pageNum) {
				pageStr.append(" class=\"unselected\"");
				pageStr.append(this.getUrlStr(i));
			} else {
				pageStr.append(" class=\"selected\">");
			}

		 
			pageStr.append(i);
			pageStr.append("</a></li>\n");

		}

		return pageStr.toString();
	}

	/**
	 * 根据页数生成超级连接href的字串
	 * @param page
	 * @return
	 */
	abstract protected   String getUrlStr(long page);

	/**
	 * 
	* @Title: getForwardUrl
	* @Description: 指定页面跳转
	* @param    
	* @return String    
	* @throws
	 */
	private String getForwardUrl(){
		
		String method =request.getMethod().toUpperCase();
		StringBuffer linkHtml = new StringBuffer();
		
		if("GET".equals(method)){
			String pageUrl = url + "page=";
			StringBuffer href = new StringBuffer();
			href.append("var $pageForm_=document.forms[0];");
			href.append("$pageForm_['action']='").append(pageUrl).append("'").append("+$('#page_query_no').val();");
			href.append("$pageForm_.submit();");
			
			linkHtml.append(" href='#' ");
			linkHtml.append(" onclick=\""+href+"\"");
			linkHtml.append(">");
		}else{
		
			String pageUrl = url + "page=";
			StringBuffer submitForm = new StringBuffer();
			submitForm.append("var $pageForm_=document.forms['gridfrom'];if($pageForm_ == null) $pageForm_= document.forms[0];");
			submitForm.append("var pno=$('#page_query_no').val();pno = (pno > 0) ? pno : 1;");
			submitForm.append("$pageForm_['action']='").append(pageUrl).append("'").append("+pno;");
			submitForm.append("$pageForm_.submit();");
			linkHtml.append(" href='#' ");
			linkHtml.append(" onclick=\""+submitForm+"\"");
			linkHtml.append(">");
		}
		return linkHtml.toString();
	}

}
