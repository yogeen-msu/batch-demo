package com.cheriscon.framework.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期相关的操作
 * @author Dawei
 *  
 */

public class DateUtil {

	/**
	 * 将一个字符串转换成日期格式
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static Date toDate(String date, String pattern) {
		if((""+date).equals("")){
			return null;
		}
		if(pattern == null){
			pattern = "yyyy-MM-dd";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date newDate = new Date();
		try {
			newDate = sdf.parse(date);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return newDate;
	}
	
	/**
	 * 把日期转换成字符串型
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String toString(Date date, String pattern){
		if(date == null){
			return "";
		}
		if(pattern == null){
			pattern = "yyyy-MM-dd";
		}
		String dateString = "";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			dateString = sdf.format(date);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return dateString;
	}
	
	public static String toString(Long time,String pattern){
		if(time>0){
			if(time.toString().length()==10){
				time = time*1000;
			}
			Date date = new Date(time);
			String str  = DateUtil.toString(date, pattern);
			return str;
		}
		return "";
	}

	
	
	/**
	 * 获取上个月的开始结束时间
	 * @return
	 */
	public static String[] getLastMonth() {
		   // 取得系统当前时间
		   Calendar cal = Calendar.getInstance();
		   int year = cal.get(Calendar.YEAR);
		   int month = cal.get(Calendar.MONTH) + 1;
		   
		   // 取得系统当前时间所在月第一天时间对象
		   cal.set(Calendar.DAY_OF_MONTH, 1);
		   
		   // 日期减一,取得上月最后一天时间对象
		   cal.add(Calendar.DAY_OF_MONTH, -1);
		   
		   // 输出上月最后一天日期
		   int day = cal.get(Calendar.DAY_OF_MONTH);

		   String months = "";
		   String days = "";

		   if (month > 1) {
		    month--;
		   } else {
		    year--;
		    month = 12;
		   }
		   if (!(String.valueOf(month).length() > 1)) {
		    months = "0" + month;
		   } else {
		    months = String.valueOf(month);
		   }
		   if (!(String.valueOf(day).length() > 1)) {
		    days = "0" + day;
		   } else {
		    days = String.valueOf(day);
		   }
		   String firstDay = "" + year + "-" + months + "-01";
		   String lastDay = "" + year + "-" + months + "-" + days;

		   String[] lastMonth = new String[2];
		   lastMonth[0] = firstDay;
		   lastMonth[1] = lastDay;

		 //  System.out.println(lastMonth[0] + "||" + lastMonth[1]);
		   return lastMonth;
		}
	
	/**
	 * 根据传入的日期进行比较，返回布尔值
	 * @return boolean
	 */
	
	public static boolean compareDate(Date date1,Date date2){
		date1.setMonth(date1.getMonth()+3);
		if(date1.compareTo(date2)>0){
			return true ;
		}
		else{
		return false;
		}
	}
	
	public static boolean compareDate2(Date date1,Date date2){
		if(date1.compareTo(date2)>0){
			return true ;
		}
		else{
		return false;
		}
	}
	
	/**
	 * 获取当月的开始结束时间
	 * @return
	 */
	public static String[] getCurrentMonth() {
		   // 取得系统当前时间
		   Calendar cal = Calendar.getInstance();
		   int year = cal.get(Calendar.YEAR);
		   int month = cal.get(Calendar.MONTH)+1 ;
		   
		   // 取得系统当前时间所在月第一天时间对象
		   cal.set(Calendar.DAY_OF_MONTH, 1);
		   
		   // 日期减一,取得上月最后一天时间对象
		   cal.add(Calendar.DAY_OF_MONTH, -1);
		   
		   // 输出上月最后一天日期
		   int day = cal.get(Calendar.DAY_OF_MONTH);

		   String months = "";
		   String days = "";


		   if (!(String.valueOf(month).length() > 1)) {
		    months = "0" + month;
		   } else {
		    months = String.valueOf(month);
		   }
		   if (!(String.valueOf(day).length() > 1)) {
		    days = "0" + day;
		   } else {
		    days = String.valueOf(day);
		   }
		   String firstDay = "" + year + "-" + months + "-01";
		   String lastDay = "" + year + "-" + months + "-" + days;

		   String[] currentMonth = new String[2];
		   currentMonth[0] = firstDay;
		   currentMonth[1] = lastDay;

		 //  System.out.println(lastMonth[0] + "||" + lastMonth[1]);
		   return currentMonth;
		}
		
	
	
	public static int getDateline(){
		
		return (int)(System.currentTimeMillis()/1000);
	}
	
 
	public static long getDatelineLong(){
		
		return System.currentTimeMillis()/1000;
	}
	
	public static int getDateline(String date){
		return (int)(toDate(date, "yyyy-MM-dd").getTime()/1000);
	}
	public static int getDateline(String date,String pattern){
		return (int)(toDate(date, pattern).getTime()/1000);
	}
	public static long getDatelineLong(String date){
		return (long)(toDate(date, "yyyy-MM-dd").getTime()/1000);
	}
	
	
	/**
	 * 传入一个日期加上年份
	 * @param date
	 * @param pattern
	 * @return
	 */
    @SuppressWarnings("deprecation")
	public static String dateAddYear(String validDate,String year) throws ParseException{  
		Date date;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(validDate);
			date.setYear(date.getYear() + Integer.parseInt(year));
			return new SimpleDateFormat("yyyy-MM-dd").format(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		}   
    }   
	
    /**
	 * 两个日期相差小时数
	 * @param date
	 * @param pattern
	 * @return
	 */
    public static int hoursBetween(String smdate,String bdate) throws ParseException{  
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(sdf.parse(smdate));    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(sdf.parse(bdate));    
        long time2 = cal.getTimeInMillis();         
        long between_hours=(time2-time1)/(1000*3600);  
        return Integer.parseInt(String.valueOf(between_hours));     
    }   
    
    
	
	public static void main(String[] args){
		
		String date1="2013-04-25";
		String date2="2013-07-16";
		System.out.println(compareDate(toDate(date1,"yyyy-MM-dd"),toDate(date2,"yyyy-MM-dd")));
		
		/*	long d= 1319990400 ;
					d=d*1000;
			int line =getDateline("2011-10-31");
		
			System.out.println( line +   "--"+toString(new Date(d), "yyyy-MM-dd"));
			System.out.println(d);*/
		
//		int d1 =getDateline("2011-10-30");
//		int d2 =getDateline("2011-10-15");
//		
//		System.out.println(d1);
//		System.out.println(d2);
//		
//		int f = 15 *24*60*60;
//		
//		System.out.println(d1-f);
		
		System.out.println( new Date(1320205608000l));
		System.out.println( DateUtil.toString( new Date(1320205608000l),"yyyy-MM-dd HH:mm:ss"));
	}
}
