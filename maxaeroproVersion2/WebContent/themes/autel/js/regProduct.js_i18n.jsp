<%@ include file="common.js_i18n.jsp" %> 
/**
 * äº§ååå·è·äº§ååºåå·å¾ççº§èJS
 */
$(document).ready(function(){
        $("#productType").change(function(){
            var path=$("#productType option:selected").attr("path");
            if(path != ""){
             $("#imageShow").html("");
             $("<img/>").appendTo("#imageShow").attr("src",path);
            }else{
                $("#imageShow").html("");
            }
        });
});

function clearInfo(){
    var selectProType = $("#selectProType");
    var inputSerialNo = $("#inputSerialNo");
    var inputPwd = $("#inputPwd");
    var isCommon = $("#isCommon");
    
    selectProType.html("");
    inputSerialNo.html("");
    inputPwd.html("");
    $("#isCommonErrorInfo").hide();
    $("#imagenumErrorInfo").hide();
}
/**
 * æ³¨åäº§å
 */
function regProduct(){
    var selectProType = $("#selectProType");
    var inputSerialNo = $("#inputSerialNo");
    var inputPwd = $("#inputPwd");
    var sealCodeInfo = $("#sealCodeInfo");
    
    var isCommon = $("#isCommon");
    
    selectProType.html("");
    inputSerialNo.html("");
    inputPwd.html("");
    $("#isCommonErrorInfo").hide();
    
    var isCommonErrorInfo=$("#isCommonErrorInfo");
    var imagenumErrorInfoTip=$("#imagenumErrorInfoTip");
    var imagenum = $("#imagenum").val();
    
    var proTypeCode=$("#productType").val();
    if(proTypeCode == ""){
        selectProType.html("<fmt:message key='product.selectprotype' />");
        return;
    }
    selectProType.html("");
    
    var proSerialNo=$("#proSerialNo").val();
    if(proSerialNo == ""){
        inputSerialNo.html("<fmt:message key='product.inputserial' />");
        return;
    }
    inputSerialNo.html("");
    
    var proRegPwd=$("#proRegPwd").val();
    if(proRegPwd == ""){
        
<!--         return; -->
    }
    inputPwd.html("");
    
    var sealCode=$("#sealCode").val();
    if(sealCode == ""){
        sealCodeInfo.html("<fmt:message key='product.inputcode' />");
        return;
    }
    sealCodeInfo.html("");
    
<!--     if(imagenum == null || imagenum == "") -->
<!--     { -->
<!--             $("#imagenumErrorInfo").show(); -->
<%--             imagenumErrorInfoTip.html("<fmt:message key='customerinfo.usercode.isnotnull'/>"); --%>
<!--             return; -->
<!--     } -->
    $("#regProductButton").attr("disabled","true");
    $.ajax({
        url:'front/usercenter/customer/regProduct.do',
        type:"post",
        data:{"proTypeCode":proTypeCode,"proSerialNo":proSerialNo,"proRegPwd":proRegPwd,"sealCode":sealCode,"imagenum":imagenum},
        dataType:"JSON",
        success:function(data){
            var jsonData=eval(data);
<!--             if(jsonData[0].regResult=="imagenum"){ -->
<!--                 $("#imagenumErrorInfo").show(); -->
<%--                 imagenumErrorInfoTip.html("<fmt:message key='customerinfo.usercode.illegalcharactercheck'/>"); --%>
<!--                 $("#regProductButton").removeAttr("disabled"); -->
<!--                 buildRandom(); -->
<!--             }else -->
             if(jsonData[0].regResult=="true1"){
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='product.regsucess' />"+jsonData[0].serialNo+".");
                
                setTimeout(jump("myProducts-1-1.html"),15000);
            }else if(jsonData[0].regResult=="true"){
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='product.regsucess' />"+jsonData[0].serialNo+".");
                setTimeout(jump("myProducts-1-1.html"),15000);
            }else if(jsonData[0].regResult=="regConfirm"){
              
               if (confirm(jsonData[0].returnMsg)){
                   changeProduct();
               }
            
            }
            else if(jsonData[0].regResult=="notFound"){
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='product.notfind' />");
                $("#regProductButton").removeAttr("disabled");
            }else if(jsonData[0].regResult=="reg"){
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='product.isregok' />");
                $("#regProductButton").removeAttr("disabled");
            }else if(jsonData[0].regResult=="unbinded"){
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='product.unbinded' />");
                $("#regProductButton").removeAttr("disabled");
            }else if(jsonData[0].regResult=="notLogin"){
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='customer.session.not.valid' />");
            }else if(jsonData[0].regResult=="SystemError"){
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='system.error.name' />");
                $("#regProductButton").removeAttr("disabled");
            }else{
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='Product.regfail' />");
                $("#regProductButton").removeAttr("disabled");
            }
        },
        error:function(data){
            $("#isCommonErrorInfo").show();
            isCommon.html("<fmt:message key='customer.network.not.valid' />");
            $("#regProductButton").removeAttr("disabled");
        }
    });
    
    
}

function changeProduct(){
    var proTypeCode=$("#productType").val();
    var proSerialNo=$("#proSerialNo").val();
    var proRegPwd=$("#proRegPwd").val();
    var isCommonErrorInfo=$("#isCommonErrorInfo");
    var isCommon = $("#isCommon");
    $.ajax({
            url:'front/usercenter/customer/changeProduct.do',
            type:"post",
            data:{"proTypeCode":proTypeCode,"proSerialNo":proSerialNo,"proRegPwd":proRegPwd},
            dataType:"JSON",
            success:function(data){
                var jsonData=eval(data);
                if(jsonData[0].regResult=="true"){
                    $("#isCommonErrorInfo").show();
                    isCommon.html("<fmt:message key='product.regsucess' />"+jsonData[0].serialNo+"。");
                    setTimeout(jump("myProducts-1-1.html"),15000);
                }else {
                    $("#isCommonErrorInfo").show();
                    isCommon.html("<fmt:message key='product.regfail' />");
                    $("#regProductButton").removeAttr("disabled");
                }
            },
            error:function(data){
                $("#isCommonErrorInfo").show();
                isCommon.html("<fmt:message key='system.error.name' />");
                $("#regProductButton").removeAttr("disabled");
            }
        });
        
}

function clearImagenumErrorInfo(){
        $("#imagenumErrorInfo").hide();
}
