<%@ include file="common.js_i18n.jsp" %> 
function goSupport(){
    var reChargeCardPassword=$("#reChargeCardPassword").val();
    var url="cardSupport.html?cardNum="+reChargeCardPassword;
    var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
}

function cardSupport(){
	var imagenumErrorInfoTip=$("#imagenumErrorInfoTip");
	var productSN=$("#productSN").val();
	var cardPwd=$("#cardPwd").val();
	var email=$("#email").val();
	var userName=$("#userName").val();
	var cardSN=$("#cardSN").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		
		
	
	if(productSN==""){
		$("#imagenumErrorInfo").show();
		 $("#submitSupport").removeAttr("disabled");
		imagenumErrorInfoTip.html("<fmt:message key='renew.card.product.null' />");
		return ;
	}
	 $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
	 
	if(cardPwd==""){
		$("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.code.null' />");
		 $("#submitSupport").removeAttr("disabled");
		return ;
	}
	 $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
	 
	 
	 if(cardPwd.length!=16){
	 $("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.lentgh.error' />");
		 $("#submitSupport").removeAttr("disabled");
		return ;
	 }
	  $("#imagenumErrorInfo").hide();
	  imagenumErrorInfoTip.html("");
	 
	 
	 
	 if($("#cardSN").val()==""){
		$("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='card.support.serial.null' />");
		 $("#submitSupport").removeAttr("disabled");
		return ;
	}
	 $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
	 
	 if($("#cardSN").val().length!=16){
	    $("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='card.support.serial.length.error' />");
		 $("#submitSupport").removeAttr("disabled");
		return ;
	 }
	  $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
	 
	 if($("#userName").val()==""){
		$("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='card.support.name.null' />");
		 $("#submitSupport").removeAttr("disabled");
		return ;
	}
	 $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
	 
	 if($("#email").val()==""){
		$("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='card.support.email.null' />");
		 $("#submitSupport").removeAttr("disabled");
		return ;
	}
	 $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
	 
     if(!regEmail.test($("#email").val()))
		{
		
			$("#imagenumErrorInfo").show();
			imagenumErrorInfoTip.html("<fmt:message key='card.support.email.check'/>");
			$("#autelId").focus();
			$("#submitSupport").removeAttr("disabled");
			return;
		}
     $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
   
	$.ajax({
		url:'front/user/cardSupport.do',
		type:"post",
		data:{"support.productSN":productSN,"support.cardPwd":cardPwd,"support.cardSN":cardSN,"support.userName":userName,"support.email":email},
		dataType:"JSON",
		success:function(data){
			var jsonData=eval(data);
			if(jsonData[0].result=="1"){
				$("#imagenumErrorInfo").show();
				$("#imagenumImg").hide();
				imagenumErrorInfoTip.html("<fmt:message key='card.support.success'/>");
				$("#submitSupport").attr("disabled","true");
			}else{
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='card.support.fail'/>");
			}
		},
		error:function(data){
			$("#isCommonErrorInfo").show();
			imagenumErrorInfoTip.html("<fmt:message key='system.error.name' />");
		}
	});
}

function resetForm(){
   $("#submitSupport").removeAttr("disabled");
    $("#cardPwd").val("");
	$("#email").val("");
	$("#userName").val("");
	$("#cardSN").val("");
	$("#imagenumErrorInfo").hide();
	imagenumErrorInfoTip.html("");
}
