<%@ include file="../common.js_i18n.jsp" %>
function resetPwd(){

    var code=$("#code").val();
	var userPwd = $("#userPwd").val();
	var confirmPassword = $("#confirmPassword").val();
	var isFlag=true;
	var userPwdResultTip=$("#userPwdResultTip");
	var confirmPasswordResultTip=$("#confirmPasswordResultTip");
	var confirmPasswordTip = $("#confirmPasswordTip");
	if(userPwd == null || userPwd == '')
	{
		$("#userPwdResult").show();
		$("#userPwdImg").show();
		userPwdResultTip.html("<fmt:message key='update.newpassword.isnotnull'/>");
		isFlag=false;
	}else{
		$("#userPwdResult").hide();
		$("#userPwdImg").hide();
	    userPwdResultTip.html("");
	}
	if(confirmPassword == null || confirmPassword == '')
	{
		$("#confirmPasswordResult").show();
		$("#confirmPasswordImg").show();
		confirmPasswordResultTip.html("<fmt:message key='update.confirmpassword.isnotnull'/>");
		isFlag=false;
	}else{
	    $("#confirmPasswordResult").hide();
		$("#confirmPasswordImg").hide();
		confirmPasswordResultTip.html("");
	}
	if(userPwd != confirmPassword)
	{
		$("#confirmPasswordResult").show();
		$("#confirmPasswordImg").show();
		confirmPasswordResultTip.html("<fmt:message key='update.newpasswordandconfirmpassword.identical'/>");
		isFlag=false;
	}else{
	    $("#confirmPasswordResult").hide();
		$("#confirmPasswordImg").hide();
		confirmPasswordResultTip.html("");
	}
	
	
	if(!isFlag){
		return;
	}
		
		$.ajax({
			url:'front/user/updateCustPwdForSealer.do',
			type:"post",
			async:false,
			data:{"userInfoVo.userPwd":userPwd,"userInfoVo.code":code},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				var oldPassword = jsonData[0].oldPassword;
				
				if(oldPassword == "false")
				{
					$("#oldPasswordResult").show();
					$("#oldPasswordImg").show();
					oldPasswordResultTip.html("<fmt:message key='update.oldpassword.iserror'/>");
					resultCode = false;
					return;
				}
				
				if(result == "true")
				{
					confirmPasswordTip.html("<fmt:message key='sealer.update.customer.pwd.success'/>");
					resultCode = true;
				}
				else
				{
					confirmPasswordTip.html("<fmt:message key='update.password.fail'/>");
					resultCode = false;
				}
				
			},
			error :function(){
				confirmPasswordTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
	
	
}

function clearAllErrorInfo(){
 $("#userPwd").val("");
 $("#confirmPassword").val("");
 $("#confirmPasswordResultTip").html("");
 $("#confirmPasswordTip").html("");
}