
<%@ include file="../common.js_i18n.jsp" %>  
function returnCustomerInfo(){
	var newProductSN = $("#newProductSN").val();
	var customerCode=  $("#code").val();
    var url="queryCustomerInfo-1-1.html?m=1&userType=2&type=S2&code="+customerCode+"&productSN="+newProductSN;
    var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
}

$(document).ready(function(){
	
	var resultTip=$("#resultTip");
	
	$("#changeNo").click(function()
	{
		var oldProductSN = $("#oldProductSN").val();
		var newProductSN = $("#newProductSN").val();
		var customerCode=  $("#code").val();
		var registerPassword = $("#registerPassword").val();
				
		if(newProductSN == null || newProductSN == '')
		{
			resultTip.html("<fmt:message key='customer.plat.soft.change.sn.null'/>");
			return;
		}
		resultTip.html("");
		
		if(registerPassword == null || registerPassword == '')
		{
			resultTip.html("<fmt:message key='customer.plat.soft.change.password.null'/>");
			return;
		}
		resultTip.html("");
		var html="<fmt:message key='customer.plat.soft.change.confirm'/>";
		html=html.replace("old",oldProductSN);
		html=html.replace("new",newProductSN);
		if(confirm(html)){
		$.ajax({
			url:'front/usercenter/customer/changeProductSerialNo.do',
			type:"post",
			async:false,
			data:{"oldProductSN":oldProductSN,"newProductSN":newProductSN,"proRegPwd":registerPassword,"customerCode":customerCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData=eval(data);
				if(jsonData == "SUCCESS")
				{
					resultTip.html("<fmt:message key='customer.plat.soft.change.success'/>"+"<a style='color:#08c;' onclick='javacript:returnCustomerInfo()'><fmt:message key='customer.plat.soft.change.success2'/></a>");
					return;
				}
				else if(jsonData == "NewProNotFind")
				{
					resultTip.html("<fmt:message key='customer.plat.soft.change.serial.notfind'/>");
					return;
				}
				else if(jsonData == "NotForYou")
				{
					resultTip.html("<fmt:message key='customer.plat.soft.change.sealer.error'/>");
					return;
				}
				else if(jsonData == "HadReged")
				{
					resultTip.html("<fmt:message key='customer.plat.soft.change.serial.register'/>");
					return;
				}
				else if(jsonData == "ProTypeError")
				{
					resultTip.html("<fmt:message key='customer.plat.soft.change.type.error'/>");
					return;
				}
				else if(jsonData == "RegPwdError")
				{
					resultTip.html("<fmt:message key='customer.plat.soft.change.password.error'/>");
					return;
				}else{
				    resultTip.html("<fmt:message key='customer.plat.soft.change.fail'/>");
					return;
				}
				
			},
			error :function(){
				confirmPasswordTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		}
			
	});
});

function clearInfo(){
	var confirmPasswordTip = $("#confirmPasswordTip");
	confirmPasswordTip.html("");
}

function clearAllErrorInfo(){	
	$("#oldPasswordResult").hide();
	$("#oldPasswordImg").hide();
	
	$("#userPwdResult").hide();
	$("#userPwdImg").hide();
	
	$("#confirmPasswordResult").hide();
	$("#confirmPasswordImg").hide();
	
	clearInfo();
}

