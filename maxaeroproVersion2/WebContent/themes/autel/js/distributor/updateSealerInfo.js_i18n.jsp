<%@ include file="../common.js_i18n.jsp" %>  
$(document).ready(function(){

	var firstNameResultTip=$("#firstNameResultTip");
	var middleNameResultTip=$("#middleNameResultTip");
	var lastNameResultTip=$("#lastNameResultTip");
	var secondEmailResultTip =$("#secondEmailResultTip");
	var comUsernameResultTip=$("#comUsernameResultTip");
	var questionCodeResultTip=$("#questionCodeResultTip");
	var answerResultTip=$("#answerResultTip");
	var countryResultTip=$("#countryResultTip");
	var addressResultTip=$("#addressResultTip");
	var cityResultTip=$("#cityResultTip");
	var companyResultTip=$("#companyResultTip");
	var zipCodeResultTip=$("#zipCodeResultTip");
	var languageCodeResultTip=$("#languageCodeResultTip");
	var dayTimeResultTip=$("#dayTimeResultTip");
	var mobileResultTip=$("#mobileResultTip");
	var emailResultTip=$("#emailResultTip");
	
	if('${local}'=='en_US')
	{
		$("#middlenametr").show();
		$("#middleNameDiv").show();
	}
	
	$("#checkUpdateSealerInfo").click(function()
	{
		var autelId = $("#autelId").val();
		var firstName = $("#firstName").val();
		var middleName = $("#middleName").val();
		var lastName = $("#lastName").val();
		var email = $("#email").val();
		var languageCode = $("#languageCode").val();
		var address = $("#address").val();
		var country = $("#country").val();
		var company = $("#company").val();
		var zipCode = $("#zipCode").val();
		var secondEmail = $("#secondEmail").val();
		var city = $("#city").val();
		var mobilePhone = $("#mobilePhone").val();
		var mobilePhoneAC = $("#mobilePhoneAC").val();
		var mobilePhoneCC = $("#mobilePhoneCC").val();
		var daytimePhone = $("#dayTimePhone").val();
		var daytimePhoneAC = $("#dayTimePhoneAC").val();
		var daytimePhoneCC = $("#dayTimePhoneCC").val();
		var IsAllowSendEmail = $("#isAllowSendEmail").val();
		var code = $("#code").val();
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_.,&'""()\s]+$/i;
		var regNumber = /^[0-9]\d*$/;
		
		var operationResultTip = $("#operationResultTip");
		
		operationResultTip.html("");
		
		clearAllErrorInfo();
		var isFlag=true;
		
		
		
		
		
		
		
		
		operationResultTip.html("");
	
		
		if(email == null || email == "")
		{
			$("#emaiResult").show();
			$("#emailImg").show();
			emailResultTip.html("<fmt:message key='accountinformation.firstemail.isnotnull'/>");
			if(isFlag){
				 $("#email").focus();
			}
			isFlag =false;
		}
		else if(!regEmail.test(email))
		{
			$("#emaiResult").show();
			$("#emailImg").show();
			emailResultTip.html("<fmt:message key='accountinformation.firstemail.illegalcharactercheck'/>");
			if(isFlag){
				 $("#email").focus();
			}
			isFlag =false;
		}
		
	
		if(secondEmail != null && secondEmail !="")
		{
			if(!regEmail.test(secondEmail))
			{
				$("#secondEmailResult").show();
				$("#secondEmailImg").show();
				secondEmailResultTip.html("<fmt:message key='accountinformation.secondemail.illegalcharactercheck'/>");
				if(isFlag){
				 $("#secondEmail").focus();
				}
				isFlag =false;
			}
			
			
			if(secondEmail==email)
			{
				$("#secondEmailResult").show();
				$("#secondEmailImg").show();
				secondEmailResultTip.html("<fmt:message key='accountinformation.firstandsecondemail.isthesame'/>");
				if(isFlag){
				 $("#secondEmail").focus();
				}
				isFlag =false;
			}
			
		}
		
		
		if(country == null || country == "")
		{
			$("#countryResult").show();
			$("#countryImg").show();
			countryResultTip.html("<fmt:message key='accountinformation.country.isnotnull'/>");
			if(isFlag){
				 $("#country").focus();
			}
			isFlag =false;
		}
		
		
		if(daytimePhoneCC !=null && daytimePhoneCC!="")
		{
			if(!regNumber.test(daytimePhoneCC))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.countrycode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		
		if(daytimePhoneAC !=null && daytimePhoneAC!="")
		{
			if(!regNumber.test(daytimePhoneAC))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.areacode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		
		if(daytimePhone !=null && daytimePhone!="")
		{
			if(!regNumber.test(daytimePhone))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.phone.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		
		if(mobilePhoneCC !=null && mobilePhoneCC!="")
		{
			
			if(!regNumber.test(mobilePhoneCC))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.countrycode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(mobilePhoneAC !=null && mobilePhoneAC!="")
		{
			if(!regNumber.test(mobilePhoneAC))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.areacode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(mobilePhone !=null && mobilePhone!="")
		{
			if(!regNumber.test(mobilePhone))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.phone.illegalcharactercheck'/>");
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
	
		
		if(languageCode == -1)
		{
			$("#languageCodeResult").show();
			$("#languageCodeImg").show();
			languageCodeResultTip.html("<fmt:message key='accountinformation.selectlanguage.name'/>");
			if(isFlag){
				 $("#languageCode").focus();
			}
			isFlag =false;
		}
		
		operationResultTip.html("");
		
		var resultCode = true;
		
		if(!isFlag){
			return;
		}
		
		$.ajax({
			url:'front/user/updateSealerInfo.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId,
				"userInfoVo.email":email,"userInfoVo.secondEmail":secondEmail,"userInfoVo.languageCode":languageCode,
				"userInfoVo.address":address,"userInfoVo.country":country,"userInfoVo.company":company,"userInfoVo.code":code,
				"userInfoVo.zipCode":zipCode,"userInfoVo.city":city,"userInfoVo.mobilePhone":mobilePhone,"userInfoVo.mobilePhoneAC":mobilePhoneAC,
				"userInfoVo.mobilePhoneCC":mobilePhoneCC,"userInfoVo.daytimePhone":daytimePhone,"userInfoVo.daytimePhoneAC":daytimePhoneAC,
				"userInfoVo.daytimePhoneCC":daytimePhoneCC,"userInfoVo.IsAllowSendEmail":IsAllowSendEmail},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				if(result == "true")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.success'/>");
					resultCode = true;
					return;
				}
				else
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.fail'/>");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});
});

	
	
function setEmail(obj)
{
	if(obj.checked)
	{
		$("#isAllowSendEmail").val(1);
	}
	else
	{
		$("#isAllowSendEmail").val(0);
	}
}
	
function clearAllErrorInfo(){
	
	$("#firstNameResult").hide();
	$("#firstNameImg").hide();
	
	$("#middleNameResult").hide();
	$("#middleNameImg").hide();
	
	$("#lastNameResult").hide();
	$("#lastNameImg").hide();
	
	$("#emailResult").hide();
	$("#emailImg").hide();
	
	$("#secondEmailResult").hide();
	$("#secondEmailImg").hide();
	
	$("#comUsernameResult").hide();
	$("#comUsernameImg").hide();
	
	$("#questionCodeResult").hide();
	$("#questionCodeImg").hide();
	
	$("#answerResult").hide();
	$("#answerImg").hide();
	
	$("#countryResult").hide();
	$("#countryImg").hide();
	
	$("#addressResult").hide();
	$("#addressImg").hide();
	
	$("#cityResult").hide();
	$("#cityImg").hide();
	
	$("#companyResult").hide();
	$("#companyImg").hide();
	
	$("#zipCodeResult").hide();
	$("#zipCodeImg").hide();
	
	$("#languageCodeResult").hide();
	$("#languageCodeImg").hide();
	
	$("#mobileResult").hide();
	$("#mobileImg").hide();
	
	$("#dayTimeResult").hide();
	$("#dayTimeImg").hide();
	
	
}	
	
	