<%@ include file="common.js_i18n.jsp" %> 
function goSupport(){
    var reChargeCardPassword=$("#reChargeCardPassword").val();
    var serialNo=$("#proSerialNo").val();
    var url="cardSupport.html?cardNum="+reChargeCardPassword+"&serialNo="+serialNo;
    var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
}

function showDesc(){

   var html="<p style='font-size: 14px;font-weight: bold;'>Total Care Program</p>";
       html+="<p>Welcome to Autel Total Care Program. The following information is to help you understand services and benefits available to Autel Update Cards.</p>";
       html+="<p style='font-weight: bold;padding-top: 5px;'>What is the Update Card Total Care Program?</p>";
       html+="<p>This program aims to provide users with quick and easy access to software update service by using the Update Card.</p>";
       html+="<p>The Total Care Program is currently only available for these models:</p>";
       html+="<p><img src='themes/autel/images/yuanq.gif'/><a style='color:#08c;' target='_blank' href='<fmt:message key='autelcms.path' />/Automotive%20Diagnostic&%20Analysi/341.jhtml'>MaxiDAS DS708</a></p>";
       html+="<p><img src='themes/autel/images/yuanq.gif'/><a style='color:#08c;' target='_blank' href='<fmt:message key='autelcms.path' />/Automotive%20Diagnostic&%20Analysi/513.jhtml'>MaxiSys Pro MS908P</a></p>";
       html+="<p><img src='themes/autel/images/yuanq.gif'/><a style='color:#08c;' target='_blank' href='<fmt:message key='autelcms.path' />/Automotive%20Diagnostic&%20Analysi/481.jhtml'>MaxiSys MS908</a></p>";
       html+="<p><img src='themes/autel/images/yuanq.gif'/><a style='color:#08c;' target='_blank' href='<fmt:message key='autelcms.path' />/Automotive%20Diagnostic&%20Analysi/515.jhtml'>MaxiSys Mini MS905</a></p>";
      
       html+="<p style='font-weight: bold;padding-top: 5px;'>What services are available to Update Card?</p> ";
       html+="<p>As the Update Card holder, you have a quick and easy access to redeem the product system.</p>";
       html+="<p>The main features of the Update Card are listed as below:</p>";
       html+="<p><img src='themes/autel/images/yuanq.gif'/>Includes the latest software updates for functionality and vehicle coverage</p>";
       html+="<p><img src='themes/autel/images/yuanq.gif'/>Technical support with our ASE certified technicians</p>";
       
       html+="<p style='font-weight: bold;padding-top: 5px;'>Where do I purchase the Update Card?</p> ";
       html+="<p>The Total Care Program can be purchased from your local tool distributor, auto-parts supplier or major automotive retailer.</p>";
       
       html+="<p style='font-weight: bold;padding-top: 5px;'>How to use the Update Card?</p> ";
       html+="<p>You can easily submit software redeem for your device following the simple steps below:</p>";
       
       html+="<p>1. Sign in to your Autel account at <a style='color:#08c;' target='_blank' href='<fmt:message key='autelcms.path' />'>www.autel.com</a>.</p>";
       html+="<p>2. Select Redeem on the left.</p>";
       html+="<p>3. Select the product.</p>";
       html+="<p>4. Peel off the label at the back of the card and enter Activation Code.</p>";
       html+="<p>5. Press OK button to confirm.</p>";
       html+="<p style='font-weight: bold;padding-top: 5px;'>Autel Support</p> ";
      
       html+="<p>At Autel we have a dedicated service team to guide you through the whole process from purchasing to applying services for the Update Card.</p>";
       html+="<p>If you are a North American user, please contact 1-855-AUTEL-US (288-3587) (Monday-Friday, 9:00AM-6:00PM Eastern Time), or visit www.autel.us.</p>";
       html+="<p>For users from other areas, please contact your local distributor or our Service Department at Support@autel.com.</p>";
   var dialog = art.dialog({
				    content: html,
				    title:'',	
				    fixed: true,
				    id: 'Fm7',
				    width: 850,
				    height: 300,
				    lock: true,
				    cancelVal:'Ok',
				    cancel: true
				});}

function showCardType(value){

	$.ajax({
		url:'front/user/getProductCardType.do',
		type:"post",
		data:{"serialNo":value},
		dataType:"JSON",
		success:function(data){
		    var jsonData=eval(data);
			if(jsonData[0].result=="1"){
				$("#cardTypeMsg").html("<fmt:message key='renew.card.use.america' />");
			}
			else if(jsonData[0].result=="0"){
				$("#cardTypeMsg").html("");
			}else{
			$("#cardTypeMsg").html("");
			}
		}
		});
}


function renewByCard(){
	var imagenumErrorInfoTip=$("#imagenumErrorInfoTip");
	var proSerialNo=$("#proSerialNo").val();
	var reChargeCardPassword=$("#reChargeCardPassword").val();
	var imgcode=$("#imgcode").val();
	if($("#proSerialNo").val()==""){
		$("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='renew.card.product.null' />");
		return ;
	}
	 $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
	 
	if($("#reChargeCardPassword").val()==""){
		$("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.code.null' />");
		return ;
	}
	
	if($("#reChargeCardPassword").val().length!=16){
	 $("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.lentgh.error' />");
		return ;
	 }
	  $("#imagenumErrorInfo").hide();
	  imagenumErrorInfoTip.html("");
	
	
	 $("#imagenumErrorInfo").hide();
	 imagenumErrorInfoTip.html("");
   if($("#imgcode").val()==""){
	    $("#imagenumErrorInfo").show();
		imagenumErrorInfoTip.html("<fmt:message key='customerinfo.usercode.isnotnull' />");
		return ;
	}
   $("#imagenumErrorInfo").hide();
   imagenumErrorInfoTip.html("");
   
   $("#renewButton").attr("disabled","true");
   
	$.ajax({
		url:'front/user/reChargeCardOperation.do',
		type:"post",
		data:{"reChargeCardVo.proSerialNo":proSerialNo,"reChargeCardVo.reChargeCardPassword":reChargeCardPassword,"imgcode":imgcode},
		dataType:"JSON",
		success:function(data){
			var jsonData=eval(data);
			if(jsonData[0].result=="1"){
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='customerinfo.usercode.illegalcharactercheck'/>"); 
				buildRandom();
				 $("#renewButton").removeAttr("disabled");
			}else if(jsonData[0].result=="2"){ //升级卡错误
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.valid'/>"+" <a style='color:#08c;' onclick='javascript:goSupport()'><fmt:message key='renew.card.active.valid.support'/></a>");
			    $("#renewButton").removeAttr("disabled");
			}else if(jsonData[0].result=="11"){ //登录失效
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='customer.session.not.valid' />");
			}else if(jsonData[0].result=="3"){ //升级卡没有激活
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.error' />");
			    $("#renewButton").removeAttr("disabled");
			}else if(jsonData[0].result=="4"){ //升级卡已经被使用
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.used'/>");
				$("#renewButton").removeAttr("disabled");
			}else if(jsonData[0].result=="5"){ //升级卡使用区域不匹配
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.area.error' />");
			    $("#renewButton").removeAttr("disabled");
			}else if(jsonData[0].result=="7"){ //升级卡使用区域不匹配
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.area.error2' />");
			    $("#renewButton").removeAttr("disabled");
			}
			else if(jsonData[0].result=="6"){ //升级卡类型和产品型号不匹配
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.valid'/>"+" <a style='color:#08c;' onclick='javascript:goSupport()'><fmt:message key='renew.card.active.valid.support'/></a>");
			    $("#renewButton").removeAttr("disabled");
			}else if(jsonData[0].result=="8"){ //错误次数查过五次
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='renew.card.active.error.num'/>");
			}else if(jsonData[0].result=="9"){
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='system.error.name'/>");
			    $("#renewButton").removeAttr("disabled");
			}else{
				$("#imagenumErrorInfo").show();
				$("#imagenumImg").hide();
				imagenumErrorInfoTip.html("<font style='color:#08c;'><fmt:message key='renew.card.success'/></font>");
				
				//setTimeout(jump("myProducts-1-1.html"),6000);
			}
		},
		error:function(data){
			$("#isCommonErrorInfo").show();
			imagenumErrorInfoTip.html("<fmt:message key='customer.network.not.valid' />");
			$("#renewButton").removeAttr("disabled");
		}
	});
}