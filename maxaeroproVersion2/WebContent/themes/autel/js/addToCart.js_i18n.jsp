<%@ include file="common.js_i18n.jsp" %> 
$(document).ready(function(){
	countMoney();
});

function countMoney(){

	var num=0;
	var allMoney=0;
	var $hidden=$("body").find("input[type='hidden']");
	$hidden.each(function(){
		num++;
		var mCode=$(this).attr("mCode");
		var cartid=$(this).attr("cartid");
		if($(this).attr("isRenew")=="0"){//购买
			var m=$(this).attr("price").toString();
			$.ajax({
				url:'front/usercenter/customer/calculate.do',
				type:"post",
				data:{"year":1,"money":m,"allMoney":allMoney},
				dataType:'JSON',
				async:false,
				success :function(data){
					var jsonData=eval(data);
					allMoney=jsonData[0].addResult;
				},
				error :function(){
					//toorder.html("<fmt:message key='system.error.name' />");
				}
			});
		}else{//续费
			var year=$("#year_" + cartid+mCode).val();
			var m=$(this).attr("price").toString();
			$.ajax({
				url:'front/usercenter/customer/calculate.do',
				type:"post",
				data:{"year":year,"money":m,"allMoney":allMoney},
				dataType:'JSON',
				async:false,
				success :function(data){
					var jsonData=eval(data);
					allMoney=jsonData[0].addResult;
				},
				error :function(){
					//toorder.html("<fmt:message key='system.error.name' />");
				}
			});
		}
	});
	
	$("#number").html(num);
	$("#item").html(num);
	
	$("#money").html("$"+parseFloat(allMoney.toString()));
	$("#money").attr("allmoney",parseFloat(allMoney.toString()));
}

function deleteOneData(proSerial, minSaleUnitCode) {
	<%-- if (confirm("<fmt:message key='cart.confirmdel' />")) {
		
	} --%>
	$.ajax({
			url : 'front/usercenter/customer/delShoppingCart.do',
			type : "post",
			data : {"proSerial":proSerial, "proCode":minSaleUnitCode},
			dataType : 'JSON',
			success : function(data) {
				var jsonData = eval(data);
				if (jsonData[0].isSuccess) {
					$("#dd_"+proSerial+"_"+minSaleUnitCode).remove();
					$("#"+proSerial+"_"+minSaleUnitCode).remove();
					countMoney();
				} else {
				
				}
			},
			error : function() {
			}
		});
}

/**
 * 续租年限更改
 * @param code
 */
function upadateYear(serialNo,minSaleUnitCode,id){
	var code=id+minSaleUnitCode;
	
	rentYear($("#year_" + code));
	var year=$("#year_" + code).val();
	
	if(year == "" || year == 0){
		year=1;
		$("#year_" + code).attr("value",year);
		
	}else{
		$("#year_" + code).attr("value",year);
	}
	
	$.ajax({
		url:'front/usercenter/customer/updateYear.do',
		type:"post",
		data:{"year":year,"serialNo":serialNo,"minSaleUnitCode":minSaleUnitCode},
		dataType:'JSON',
		async:false,
		success :function(data){
			var jsonData=eval(data);
			countMoney();
		},
		error :function(){
			//toorder.html("<fmt:message key='system.error.name' />");
		}
	});
					
	
}

function rentYear(rentYear){
	if(rentYear.val() == ""){
		rentYear.attr("value",1);
	}else{
		rentYear.attr("value",rentYear.val().replace(/\D+/g,'1'));
	}
}


/**
 * 提交订单
 */
function submitOrdey(){
	//var toorder = $("#toorder");
	//toorder.html("");
	
	var $hidden=$("body").find("input[type='hidden']");
	
	var orderInfos=new Array();
	var allMoney=0;
	var discountallmoney=0;//优惠总额
		
	$.ajax({
		url:'front/usercenter/customer/submintOrderInfo.do',
		type:"post",
		data:{"minSaleUnitList":orderInfos.join(","),"allmoney":allMoney,"discountallmoney":discountallmoney},
		dataType:'JSON',
		success :function(data){
			var jsonData=eval(data);
				
			if(jsonData[0].subResult == "true"){
				jump("onlinePay.html?orderInfoCode="+jsonData[0].orderInfoCode);
			}else{
				jump("cart.html?way=1");
			
				//toorder.html("<fmt:message key='cart.submitfail' />");
			}
				
		},
		error :function(){
			//toorder.html("<fmt:message key='system.error.name' />");
		}
	});
		
	
}
