//初始化
$(document).ready(function(){
	//加载协议信息，判断当前用户是否已经同意
	$.ajax({
		url:'sdk/app/getCustomerAgreementDetails.do',
		type:"post",
		async:true,
		data:{},
		dataType:'JSON',
		success :function(data){
			var jsonData = eval(data);
			if(jsonData[0].result){
				//显示主页信息
				$.showSdkAppMain();
			}else{
				if(jsonData[0].handleType == "0"){
					var userType = $.getUrlParam("userType");
					window.location.href = "loginOut.html?loginToType=1&operationType=18&userType="+userType+"&m=1";
				}else{
					//显示协议信息
					$("#termAgreementDiv").css("display","block");
				}
			}
		}
	});
	
	//同意协议
	$("#agreeAgreement").click(function(){
		//当前用户确定同意协议
		$.ajax({
			url:'sdk/app/saveCustomerAgreementDetails.do',
			type:"post",
			async:true,
			data:{},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].result){
					//显示主页信息
					$.showSdkAppMain();
				}
			}
		});
	});
	
	//取消协议
	$("#cancelAgreement").click(function(){
		var userType = $.getUrlParam("userType");
		window.location.href = "loginOut.html?loginToType=1&operationType=18&userType="+userType+"&m=1";
	});
	
	
	//获取sdk app数据
	$.getSdkAppList = function(type){
		$.ajax({
			url:'sdk/app/querySdkAppList.do',
			type:"post",
			async:false,
			data:{"sdkAppVO.softwarePlatform":type,"sdkAppVO.status":"1"},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].result){
					var sdkAppVOList = jsonData[0].sdkAppVOList;
					var docItem = "";
					if(null != sdkAppVOList && sdkAppVOList.length > 0){
						for(var i = 0; i < sdkAppVOList.length; i++){
							docItem += '<div class="sdk-1 '+(i==0?"sdk-a":"")+'">';
							var appName = sdkAppVOList[i].appName;
							if(appName.length >= 8){
								appName = appName.substring(0,8)+"......" ;
							}
							docItem += '<div class="zuo-1"style=" background:#1CC4E5;" ><a style="color: #fff;" title="'+sdkAppVOList[i].appName+'" href="javascript:$.showDetails('+sdkAppVOList[i].id+')">'+appName+'</a></div>';
							docItem += '<div class="you-1" style="margin-top:20px;">';
							docItem += '<h4 style="	font-weight: bold;">'+sdkAppVOList[i].packageName+'</h4>';
							docItem += '<h2>'+(sdkAppVOList[i].softwarePlatform=="0"?"Android":"iOS")+'</h2>';
							docItem += '</div>';
							docItem += '</div>';
						}
					}
					$("#sdkAppDivContent").html(docItem);
				}else{
					$("#sdkAppDivContent").html("");
				}
			},
			error :function(){
			}
		});
	};
	
	$("#selectSdkApp").change(function(){
		var type = $(this).val();
		$.getSdkAppList(type);
	});
	
	//弹出添加app模式窗口
	$("#demoBtn16").click(function(){
		//隐藏appkey
		$("#demo17 #appKeyDiv").css("display","none");
		$("#demo17 #softwarePlatform").removeAttr("disabled");
		$("#demo17 #packageName").removeAttr("disabled");
		//设置弹窗为添加弹出框
		$("#demo17 #opType").val("add");
		$("#demo17 #title").html("CREATE APP");
		$("#demo17 #createApp").html("CREATE");
		$("#demo17 #softwarePlatform").css("display","block");
		$("#demo17 #softwarePlatformInput").css("display","none");
		$("#demo17").css("height","600px");
	    $("#demo17").layerModel();
	});
	
	//提交app数据
	$("#createApp").click(function(){
		var opType =  $("#demo17 #opType").val();
		var appName =  $.trim($("#demo17 #appName").val());// app名称
		var description =  $.trim($("#demo17 #description").val());// 描述
		var softwarePlatform =  $.trim($("#demo17 #softwarePlatform").val());// 软件平台
		var packageName =  $.trim($("#demo17 #packageName").val());// 包名称
		if(null == appName || appName == ""){
			//alert("APP Name input cannot be empty");
			$.tConfirm.open({title:"Prompt",overlay:true,body:"APP Name input cannot be empty.",type:'info',onOk:function(){
                $("#demo17 #appName").focus();
            }});
			return;
		}
		if(null == packageName || packageName == ""){
			//alert("Package Name input cannot be empty");
			$.tConfirm.open({title:"Prompt",overlay:true,body:"Package Name input cannot be empty.",type:'info',onOk:function(){
				$("#demo17 #packageName").focus();
            }});
			return;
		}
		if(null == description || description == ""){
			//alert("Description input cannot be empty");
			$.tConfirm.open({title:"Prompt",overlay:true,body:"Description input cannot be empty.",type:'info',onOk:function(){
				$("#demo17 #description").focus();
            }});
			return;
		}
		//app sdk 数据
		var data = {
			"sdkAppVO.appName":appName,
			"sdkAppVO.softwarePlatform":softwarePlatform,
			"sdkAppVO.packageName":packageName,
			"sdkAppVO.description":description
		};
		var url = "";
		if(opType == "add"){
			url = "sdk/app/addSdkApp.do";
		}
		if(opType == "edit"){
			url = "sdk/app/updateSdkApp.do";
			data["sdkAppVO.id"] = $("#sdkAppDetailsDiv #id").val();
		}
		$.ajax({
			url:url,
			type:"post",
			async:false,
			data:data,
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].result == "1"){
					var userType = $.getUrlParam("userType");
					window.location.href ="sdkApp.html?userType="+userType;
				}else{
					$.tConfirm.open({title:"Prompt",overlay:true,body:jsonData[0].msg,type:'error'});
				}
				$('#demo18').close();
			},
			error :function(e){
				$('#demo18').close();
				$.tConfirm.open({title:"Prompt",overlay:true,body:"System error.",type:'error'});
			}
		});
	});
	
	//显示详情
	$.showDetails = function(id){
		$("#sdkAppDiv").css("display","none");
		$("#sdkAppDetailsDiv").css("display","block");
		$.ajax({
			url:'sdk/app/getSdkAppDetails.do',
			type:"post",
			async:false,
			data:{"sdkAppVO.id":id},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].result){
					var sdkAppVO = jsonData[0].sdkAppVO;
					$("#sdkAppDetailsDiv #id").val(sdkAppVO.id);
					$("#sdkAppDetailsDiv #appName").html(sdkAppVO.appName);
					$("#sdkAppDetailsDiv #softwarePlatform").html(sdkAppVO.softwarePlatform=="0"?"Android":"IOS");
					$("#sdkAppDetailsDiv #packageName").html(sdkAppVO.packageName);
					$("#sdkAppDetailsDiv #appKey").html(sdkAppVO.appKey);
					$("#sdkAppDetailsDiv #description").html(sdkAppVO.description);
				}else{
					$("#sdkAppDetailsDiv #id").val("");
					$("#sdkAppDetailsDiv #appName").html();
					$("#sdkAppDetailsDiv #softwarePlatform").html();
					$("#sdkAppDetailsDiv #packageName").html();
					$("#sdkAppDetailsDiv #appKey").html();
					$("#sdkAppDetailsDiv #description").html();
				}
			},
			error :function(e){
				$.tConfirm.open({title:"Prompt",overlay:true,body:"System error.",type:'error'});
			}
		});
	};
	
	//弹出编辑页面
	$("#editSdkAppBtn").click(function(){
		//隐藏appkey
		$("#demo17 #appKeyDiv").css("display","block");
		$("#demo17 #softwarePlatform").css("display","none");
		$("#demo17 #softwarePlatformInput").css("display","block");
		$("#demo17 #softwarePlatformInput").attr("disabled","disabled");
		$("#demo17 #packageName").attr("disabled","disabled");
		//设置弹窗为添加弹出框
		$("#demo17 #opType").val("edit");
		$("#demo17 #title").html("UPDATE APP");
		$("#demo17 #createApp").html("UPDATE");
		$("#demo17").css("height","660px");
		$("#demo17").layerModel();
		//赋值
		$("#demo17 #appKey").val($("#sdkAppDetailsDiv #appKey").text());
		$("#demo17 #appName").val($("#sdkAppDetailsDiv #appName").text());
		$("#demo17 #softwarePlatform").val($("#sdkAppDetailsDiv #softwarePlatform").text()=="Android"?"0":"1");
		$("#demo17 #softwarePlatformInput").val($("#sdkAppDetailsDiv #softwarePlatform").text());
		$("#demo17 #packageName").val($("#sdkAppDetailsDiv #packageName").text());
		$("#demo17 #description").val($("#sdkAppDetailsDiv #description").text());
	});
	
	//删除
	$("#delSdkAppBtn").click(function(){
		//确认信息
		$.tConfirm.open({title:"Prompt",overlay:true,body:"Are you sure you want to delete it?",type:'confirm',onOk:function(){
			var id = $("#sdkAppDetailsDiv #id").val();
			var packageName = $("#sdkAppDetailsDiv #packageName").text();
			$.ajax({
				url:'sdk/app/deleteSdkApp.do',
				type:"post",
				async:false,
				data:{"sdkAppVO.id":id,"sdkAppVO.packageName":packageName},
				dataType:'JSON',
				success :function(data){
					var jsonData = eval(data);
					if(jsonData[0].result == "1"){
						var userType = $.getUrlParam("userType");
						window.location.href ="sdkApp.html?userType="+userType;
					}else{
						$.tConfirm.open({title:"Prompt",overlay:true,body:jsonData[0].msg,type:'error'});
					}
				},
				error :function(e){
					$.tConfirm.open({title:"Prompt",overlay:true,body:"System error.",type:'error'});
				}
			});
        }});
	});
	
	$.loadSdkAppDownlaod = function(){
		 //初始化download数据
		$.ajax({
			url:'sdk/app/querySdkAppDownloadsInfo.do',
			type:"post",
			async:false,
			//data:{"sdkAppVO.autelId":"1234"},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				debugger;
				if(jsonData[0].result){
					var downloadList = jsonData[0].downloadList;
					if(null != downloadList && downloadList.length > 0){
						$("#iosDiv .xia:eq(0)").css("display","block");
						$("#iosDiv .xia:eq(1)").css("display","block");
						$("#iosDiv .xia:eq(2)").css("display","none");
						$("#androidDiv .xia:eq(0)").css("display","block");
						$("#androidDiv .xia:eq(1)").css("display","block");
						$("#androidDiv .xia:eq(2)").css("display","none");
						$("#iosDiv #category_h4").css({border:"1px #e5e5e5 solid"});
						$("#androidDiv #category_h4").css({border:"1px #e5e5e5 solid"});
						for(var i = 0; i < downloadList.length; i++){
							var category = (downloadList[i].categoryType == "0")?"#androidDiv":"#iosDiv";
							var splitSize = 8;//截断参数大小
							var sdkCategory = downloadList[i].sdkCategory;
							if(null != sdkCategory && sdkCategory.length >= splitSize){
								sdkCategory = sdkCategory.substring(0,splitSize)+"..." ;
							}
							var releaseNotes = downloadList[i].releaseNotes;
							if(null != releaseNotes && releaseNotes.length >= splitSize){
								releaseNotes = releaseNotes.substring(0,splitSize)+"..." ;
							}
							$(category+ " h4[id='category_h4']").html(sdkCategory +" "+downloadList[i].sdkVersion);
							$(category+ " h4[id='category_h4']").attr("title",downloadList[i].sdkCategory +" "+downloadList[i].sdkVersion);
							$(category+ " P[id='category_category_p']").html(sdkCategory);
							$(category+ " P[id='category_category_p']").attr("title",downloadList[i].sdkCategory);
							$(category+ " P[id='category_version_p']").html(downloadList[i].sdkVersion);
							$(category+ " A[id='category_download_a']").attr("href",(downloadList[i].sdkDownloadUrl==""?"#":downloadList[i].sdkDownloadUrl));
							$(category+ " P[id='release_notes_p']").html(releaseNotes);
							$(category+ " P[id='release_notes_p']").attr("title",downloadList[i].releaseNotes);
							$(category+ " P[id='release_notes_version_p']").html(downloadList[i].releaseNotesVersion);
							$(category+ " A[id='release_notes_download_a']").attr("href",(downloadList[i].releaseNotesDownloadUrl==""?"#":downloadList[i].releaseNotesDownloadUrl));
							
							if(downloadList.length == 1){
								if(downloadList[i].categoryType == "0"){
									$("#iosDiv .xia:eq(0)").css("display","none");
									$("#iosDiv .xia:eq(1)").css("display","none");
									$("#iosDiv .xia:eq(2)").css("display","block");
									$("#iosDiv #category_h4").html("");
									$("#iosDiv #category_h4").css({border:"none"});
								}else{
									$("#androidDiv .xia:eq(0)").css("display","none");
									$("#androidDiv .xia:eq(1)").css("display","none");
									$("#androidDiv .xia:eq(2)").css("display","block");
									$("#androidDiv #category_h4").html("");
									$("#androidDiv #category_h4").css({border:"none"});
								}
							}
						}
					}
				}else{
					$("#iosDiv .xia:eq(0)").css("display","none");
					$("#iosDiv .xia:eq(1)").css("display","none");
					$("#iosDiv .xia:eq(2)").css("display","block");
					$("#androidDiv .xia:eq(0)").css("display","none");
					$("#androidDiv .xia:eq(1)").css("display","none");
					$("#androidDiv .xia:eq(2)").css("display","block");
					$("#iosDiv #category_h4").html("");
					$("#iosDiv #category_h4").css({border:"none"});
					$("#androidDiv #category_h4").html("");
					$("#androidDiv #category_h4").css({border:"none"});
				}
			},
			error :function(){
			}
		}); 
	};
	
	$("#sdkApp").click(function(){
		$.showSdkAppMain();
	});
	
	$.showSdkAppMain = function(){
		//隐藏模块
		$("#sdkAppDetailsDiv").css("display","none");
		$("#sdkAppdownloadDiv").css("display","none");
		$("#termAgreementDiv").css("display","none");
		//显示模块
		$("#sdkApp").addClass("zhong");
		$("#sdkAppDownload").removeClass("zhong");
		$("#selectSdkApp").val("-1");
		$("#sdkAppMain").css("display","block");
		$("#sdkAppDiv").css("display","block");
		//初始化sdk app数据
		$.getSdkAppList(-1);
	};
	
	$("#sdkAppDownload").click(function(){
		//隐藏模块
		$("#sdkAppDiv").css("display","none");
		$("#sdkAppDetailsDiv").css("display","none");
		$("#termAgreementDiv").css("display","none");
		//显示模块
		$("#sdkAppDownload").addClass("zhong");
		$("#sdkApp").removeClass("zhong");
		$("#sdkAppMain").css("display","block");
		$("#sdkAppdownloadDiv").css("display","block");
		//初始化sdk app download数据
		$.loadSdkAppDownlaod();
	});
	
	$.getUrlParam = function(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if (r != null)
			return unescape(r[2]);
		return null;
	};
	
	$("#sdkMyAccount").click(function(){
		var userType = $.getUrlParam("userType");
		window.location.href = "myAccount.html?userType="+userType+"&operationType=5";
	});
	
	$("#sdkSignOut").click(function(){
		var userType = $.getUrlParam("userType");
		window.location.href = "loginOut.html?loginToType=1&operationType=18&userType="+userType+"&m=1";
	});
	
});