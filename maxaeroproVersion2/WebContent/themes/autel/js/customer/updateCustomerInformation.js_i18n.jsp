<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){
	var firstNameResultTip=$("#firstNameResultTip");
	var middleNameResultTip=$("#middleNameResultTip");
	var lastNameResultTip=$("#lastNameResultTip");
	var countryResultTip=$("#countryResultTip");
	var addressResultTip=$("#addressResultTip");
	var cityResultTip=$("#cityResultTip");
	var companyResultTip=$("#companyResultTip");
	var zipCodeResultTip=$("#zipCodeResultTip");
	var dayTimeResultTip=$("#dayTimeResultTip");
	var mobileResultTip=$("#mobileResultTip");
	var telephoneResultTip=$("#telephoneResultTip");
	
   var tempCountry=$('#country').val();
   if(tempCountry=='United States'){
		usaProvince();
	}
	
	if('${local}'=='en_US')
	{
		$("#middlenametr").show();
		$("#middleNameDiv").show();
	}
	
	$("#checkUpdateCustomerInfo").click(function()
	{
		var code = $("#code").val();
		var firstName = $("#firstName").val();
		var middleName = $("#middleName").val();
		var lastName = $("#lastName").val();
		var address = $("#address").val();
		var country = $("#country").val();
		var company = $("#company").val();
		var zipCode = $("#zipCode").val();
		var city = $("#city").val();
		var province=$('#province').val();
		var mobilePhone = $("#mobilePhone").val();
		var mobilePhoneAC = $("#mobilePhoneAC").val();
		var mobilePhoneCC = $("#mobilePhoneCC").val();
		var daytimePhone = $("#dayTimePhone").val();
		var daytimePhoneAC = $("#dayTimePhoneAC").val();
		var daytimePhoneCC = $("#dayTimePhoneCC").val();
		var IsAllowSendEmail = $("#isAllowSendEmail").val();
		var telephone = $("#telephone").val();
		var sex = $("input[name='sex']:checked").val();
		var birthdate = $("input[name='birthdate']").val();
		
		var actCode = $("#actCode").val();
<!-- 		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_.,&'""()\s]+$/i; -->
var regName = /^[-0-9a-zA-Z_. ]+$/;
		var regNumber = /^[0-9]\d*$/;
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regComUsername = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		

		var operationResultTip = $("#operationResultTip");

		
		operationResultTip.html("");
		
		clearAllErrorInfo();
		var isFlag=true;
		if(firstName == null || firstName == "")
		{
			$("#firstNameResult").show();
			$("#firstNameImg").show();
			firstNameResultTip.html("<fmt:message key='accountinformation.firstname.isnotnull'/>");
			if(isFlag){
				 $("#firstName").focus();
			}
			isFlag =false;
		}
		else
		{
			if(!regName.test(firstName))
			{
				$("#firstNameResult").show();
				$("#firstNameImg").show();
				firstNameResultTip.html("<fmt:message key='accountinformation.firstname.illegalcharactercheck'/>");
				if(isFlag){
				 $("#firstName").focus();
				}
				isFlag =false;
			}
		}
		
		
		if(middleName != null && middleName !="")
		{
			if(!regName.test(middleName))
			{
				$("#middleNameResult").show();
				$("#middleNameImg").show();
				middleNameResultTip.html("<fmt:message key='accountinformation.middlename.illegalcharactercheck'/>");
				if(isFlag){
				 $("#middleName").focus();
				}
				isFlag =false;
			}
		}
		
		
		if(lastName == null || lastName == "")
		{
			$("#lastNameResult").show();
			$("#lastNameImg").show();
			lastNameResultTip.html("<fmt:message key='accountinformation.lastname.isnotnull'/>");
			if(isFlag){
				 $("#lastName").focus();
			}
			isFlag =false;
		}
		else
		{
			if(!regName.test(lastName))
			{
				$("#lastNameResult").show();
				$("#lastNameImg").show();
				lastNameResultTip.html("<fmt:message key='accountinformation.lastname.illegalcharactercheck'/>");
				if(isFlag){
				 $("#lastName").focus();
				}
				isFlag =false;
			}
		}
		
		
		if(address){
			if(!regName.test(address)){
				$("#addressResult").show();
				$("#addressImg").show();
				addressResultTip.html("address cannot contain invalid characters.");
				if(isFlag){
				 $("#address").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(daytimePhoneCC !=null && daytimePhoneCC!="")
		{
			if(!regNumber.test(daytimePhoneCC))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.countrycode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(daytimePhoneAC !=null && daytimePhoneAC!="")
		{
			if(!regNumber.test(daytimePhoneAC))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.areacode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(daytimePhone !=null && daytimePhone!="")
		{
			if(!regNumber.test(daytimePhone))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.phone.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
	
		operationResultTip.html("");
		
		if(telephone !=null && telephone!="")
		{
			if(!regNumber.test(telephone))
			{
				$("#telephoneResult").show();
				$("#telephoneImg").show();
				telephoneResultTip.html("<fmt:message key='accountinformation.phone.illegalcharactercheck'/>");
				
				if(isFlag){
				 $("#telephone").focus();
				}
				isFlag =false;
			}
		}
		operationResultTip.html("");
		
		if(mobilePhoneCC !=null && mobilePhoneCC!="")
		{
			if(!regNumber.test(mobilePhoneCC))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.countrycode.illegalcharactercheck'/>");
				
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		
		
		operationResultTip.html("");
		
		if(mobilePhoneAC !=null && mobilePhoneAC!="")
		{
			if(!regNumber.test(mobilePhoneAC))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.areacode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(mobilePhone !=null && mobilePhone!="")
		{
			if(!regNumber.test(mobilePhone))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.phone.illegalcharactercheck'/>");
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
	
		operationResultTip.html("");
		
		var resultCode = true;
		
		if(!isFlag){
			return;
		}
		
		$.ajax({
			url:'front/user/updateCustomerInfoNew.do',
			type:"post",
			async:false,
			data:{"customerInfoEdit.code":code,"customerInfoEdit.firstName":firstName,"customerInfoEdit.middleName":middleName,
				"customerInfoEdit.lastName":lastName,"customerInfoEdit.address":address,"customerInfoEdit.country":country,
				"customerInfoEdit.company":company,"customerInfoEdit.zipCode":zipCode,"customerInfoEdit.city":city,"customerInfoEdit.telephone":telephone,
				"customerInfoEdit.mobilePhone":mobilePhone,"customerInfoEdit.mobilePhoneAC":mobilePhoneAC,"customerInfoEdit.province":province,
				"customerInfoEdit.mobilePhoneCC":mobilePhoneCC,"customerInfoEdit.daytimePhone":daytimePhone,"customerInfoEdit.daytimePhoneAC":daytimePhoneAC,
				"customerInfoEdit.daytimePhoneCC":daytimePhoneCC,"customerInfoEdit.IsAllowSendEmail":IsAllowSendEmail,"customerInfoEdit.actCode":actCode,
				"customerInfoEdit.sex":sex,"customerInfoEdit.birthdate":birthdate},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				
				if(result == "1")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.success'/>");					
					resultCode = true;
					<!-- 重新显示更改名称  by 2017-09-11 -->
					$("#userAutelId").html("Hello "+firstName+" "+lastName);
					return;
				}
				else if(result == "2")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.fail'/>");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});
	
	<!-- 控制出生日期，不能选择未来时间 by 2017-09-11 -->
	$("#birthdate").datebox({  
	    onSelect:function(date){  
	        var nowDate = new Date();  
	        if(date > nowDate){  
	            $("#birthdate").datebox("setValue",nowDate);  
	        }  
	    }  
	});   

});

function setEmail(obj)
{
	if(obj.checked)
	{
		$("#isAllowSendEmail").val(1);
	}
	else
	{
		$("#isAllowSendEmail").val(0);
	}
}

function clearAllErrorInfo(){
	
	$("#firstNameResult").hide();
	$("#firstNameImg").hide();
	
	$("#middleNameResult").hide();
	$("#middleNameImg").hide();
	
	$("#lastNameResult").hide();
	$("#lastNameImg").hide();
	
	$("#countryResult").hide();
	$("#countryImg").hide();
	
	$("#addressResult").hide();
	$("#addressImg").hide();
	
	$("#cityResult").hide();
	$("#cityImg").hide();
	
	$("#companyResult").hide();
	$("#companyImg").hide();
	
	$("#zipCodeResult").hide();
	$("#zipCodeImg").hide();
	
	$("#mobileResult").hide();
	$("#mobileImg").hide();
	
	$("#dayTimeResult").hide();
	$("#dayTimeImg").hide();
	
	
}