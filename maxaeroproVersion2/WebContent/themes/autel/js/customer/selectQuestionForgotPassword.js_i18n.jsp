<%@ include file="../common.js_i18n.jsp" %>
$(document).ready(function(){
	
	$("#checkQuestionAnswer").click(function()
	{
		var autelId = $("#autelId").val();
		var answerValue = $("#answer").val();
		var $form = $("#selectQuestionForgotFrom");
		var answerTip = $("#answerTip");
		
	    
		if(answerValue == null || answerValue == "")
		{
			showTitle();
			answerTip.html("<fmt:message key='accountinformation.useraccount.answerisnull'/>");
			return;
		}
		
		answerTip.html("");
		
		var resultCode = true;
		var autelID;
		var actCode;
		$.ajax({
			url:'front/user/checkAnswer.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId,"userInfoVo.answer":answerValue},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval("("+data+")");
				if("true" != jsonData.result)
				{
					showTitle();
					answerTip.html("<fmt:message key='accountinformation.useraccount.answererror'/>");
					resultCode = false;
					return;
				}
				 actCode = jsonData.actCode;
			},
			error :function(){
				showTitle();
				answerTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		var url="resetPassword.html?autelId="+autelId+"&actCode="+actCode+"&operationType=16";
		$form.attr("action",url);
		$form.submit();
	});
});

function showTitle()
{
	$("#answerErrorInfo").show();
	$("#answerImg").show();
}