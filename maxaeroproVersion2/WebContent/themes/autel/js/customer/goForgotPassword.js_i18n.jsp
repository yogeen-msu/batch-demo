<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function()
{

    var autelId=$('#autelId').val();
    if(autelId==''){
    	$("#autelId").val("Autel ID");
    }

	$("#checkAutelId").click(function()
	{
		var autelId = $("#autelId").val();
		var $form = $("#goForgotPasswordForm");
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		
		var usernameTip = $("#userNameErrorInfoTip");
		
		if(autelId == null || autelId == "" || autelId=="Autel ID")
		{
			showTitle();
			usernameTip.html("<fmt:message key='accountinformation.useraccount2.isnotnull'/>");
			return;
		}
		
		usernameTip.html("");
		
		var resultCode = true;
		
		
		
		$.ajax({
			url:'front/user/checkAutelIdIsExist.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
		
				if(autelId == "false")
				{
					showTitle();
					usernameTip.html("<fmt:message key='accountinformation.useraccount.isnotexist'/>");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				showTitle();
				usernameTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		
		if(!regEmail.test(autelId))
		{
			showTitle();
			usernameTip.html(autelId+" <fmt:message key='customer.change.info'/><fmt:message key='customer.change.link'/> <a style='color: #0088CC;text-decoration:underline' href='changeAutelID.html?operationType=21&autelId="+autelId+"'><fmt:message key='customer.change.info2'/></a>");
			return;
		}
		 
		
		
		$form.action="selectForgotPassword.html?operationType=11";
		$form.submit();
		
	});
	
	document.onkeypress=function(e)
    {
        var code;
        if (!e)
        {
            var e=window.event;
        }
        if(e.keyCode)
        {   
            code=e.keyCode;
        }
        else if(e.which)
        {
            code=e.which;
        }
        if(code==13)
        {
            return false;
        }
    }
});

function enterkey(e)
{
    if (e==13||e==32)
    {
    	$("#checkAutelId").click();
    }
}

function showTitle()
{
	$("#userNameErrorInfo").show();
	$("#userNameImg").show();
}

function clearautelIdValue(){
	if($("#autelId").val()=="Autel ID")
	{
		$("#autelId").attr("value","");
	} 
}
function onblurAutelId(){
if($("#autelId").val()==""){
 $("#autelId").val("Autel ID");
}
}
	
function checkAutelID(){
	var autelId = $("#autelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	if(!regEmail.test(autelId))
	{
		$("#emailError").attr("color","#ea0000");
		return ;
	}else{
		$("#emailError").attr("color","");
	}
	
	$.ajax({
			url:'front/user/checkAutelIDIsUse.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				
				if(autelId == "true")
				{
					$("#isUse").attr("color","#ea0000");
				}else{
					$("#isUse").attr("color","");
				}
				
			}
		});
	
}	
	
