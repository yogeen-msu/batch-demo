<%@ include file="../common.js_i18n.jsp" %>
$(document).ready(function()
{
	$("#findAutelId").click(function()
	{
		 var productSNInfoTip= $("#productSNInfoTip");
		 var productPwdErrorInfoTip= $("#productPwdErrorInfoTip");
		 
		 var productSN=$("#productSN").val();
		 var productPwd=$("#productPwd").val();
		 clearAllErrorInfo();
		 var isFlag=true;
		 if(productSN==""){
		    $("#productSNErrorInfo").show();
			$("#productSNNull").hide();
			$("#productSNImg").show();
			productSNInfoTip.html("<fmt:message key='customer.change.sn.null'/>");
			
			if(isFlag){
					$("#productSN").focus();
					isFlag=false;
			}
		 
		 }
	<%-- 	 if(productPwd==""){
		 $("#productPwdErrorInfo").show();
			$("#productPwdNull").hide();
			$("#productPwdImg").show();
			productPwdErrorInfoTip.html("<fmt:message key='customer.change.pwd.null'/>");
			
			if(isFlag){
					$("#productSN").focus();
					isFlag=false;
			}
		 
		 } --%>
		 
		var resultCode = true;
		
		if(!isFlag){
		  return;
		}
		
		$.ajax({
		url:'front/user/checkFindAutelId.do',
			type:"post",
			async:false,
			data:{"proSerialNo":productSN,"proPwd":productPwd},
			dataType:'JSON',
			success :function(data)
			{
			    var jsonData = eval(data);
				var result = jsonData[0].result;
				if(result=="1"){
					$("#productSNErrorInfo").show();
					$("#productSNNull").hide();
					$("#productSNImg").show();
					productSNInfoTip.html("<fmt:message key='customer.change.sn.notexist'/>");
				}else if(result=="2"){
					$("#productSNErrorInfo").show();
			        $("#productSNNull").hide();
					$("#productSNImg").show();
					productSNInfoTip.html("<fmt:message key='customer.change.pwd.error'/>");
				}else if(result=="3"){
					$("#productSNErrorInfo").show();
			        $("#productSNNull").hide();
					$("#productSNImg").show();
					productSNInfoTip.html("<fmt:message key='find.autelID.not.customer'/>");
				}else if(result=="0"){
					$("#geFindAutelIdForm").action="findAutelId.html";
					$("#geFindAutelIdForm").submit();
				}
				
			}
		});
});
});


function clearAllErrorInfo()
{
		 var productSNInfoTip= $("#productSNInfoTip");
		 var productPwdErrorInfoTip= $("#productPwdErrorInfoTip");

		
		productSNInfoTip.html("");
		productPwdErrorInfoTip.html("");
		
		$("#productSNErrorInfo").hide();
		$("#productSNImg").hide();
		$("#productPwdErrorInfo").hide();
		$("#productPwdImg").hide();
		
}
