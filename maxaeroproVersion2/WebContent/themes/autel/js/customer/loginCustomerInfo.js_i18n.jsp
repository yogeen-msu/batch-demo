<%@ include file="../common.js_i18n.jsp" %>
$(document).ready(function()
{

	$("#loginCheck").click(function()
	{
		if(!CookieEnable())
	    {
	       alert("<fmt:message key='customer.explor.cookie'/>");
	       return ;
	    }
		var userType = $("#userType").val();
		var autelId = $("#autelId").val();
		var password = $("#password").val();
		var imagenum = $("#imagenum").val();
		var $form = $("#loginCustomerInfo");
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		
		var userTypeErrorInfoTip = $("#userTypeErrorInfoTip");
		var autelIdErrorInfoTip	= $("#autelIdErrorInfoTip");
		var passwordErrorInfoTip = $("#passwordErrorInfoTip");
		var imagenumErrorInfoTip = $("#imagenumErrorInfoTip");
	   	
		clearAllErrorInfo();
		
		if(userType == -1)
		{
			$("#userTypeErrorInfo").show();
			$("#userTypeImg").show();
			userTypeErrorInfoTip.html("<fmt:message key='customerinfo.usertype.pleaseselect'/>");
			return;
		}
		
		if(autelId == null || autelId == "")
		{
			$("#autelIdErrorInfo").show();
			$("#autelIdImg").show();
			autelIdErrorInfoTip.html("<fmt:message key='customerinfo.username.isnotnull'/>");
			
			return;
		}
		
		<%-- if(userType == 1)
		{
			if(!regEmail.test(autelId))
			{
				$("#autelIdErrorInfo").show();
				$("#autelIdImg").show();
				autelIdErrorInfoTip.html("<fmt:message key='customerinfo.username.illegalcharactercheck'/>");
				return;
			}
		} --%>
	
		if(password == null || password == "")
		{
			$("#passwordErrorInfo").show();
			$("#passwordImg").show();
			passwordErrorInfoTip.html("<fmt:message key='customerinfo.userpassword.isnotnull'/>");
			return;
		}
		<%-- else if(password.length < 6)
		{
			$("#passwordErrorInfo").show();
			$("#passwordImg").show();
			passwordErrorInfoTip.html("<fmt:message key='accountinformation.password.lengthcheck'/>");
			return;
		} --%>
		
		if(imagenum == null || imagenum == "")
		{
			$("#imagenumErrorInfo").show();
			$("#imagenumImg").show();
			imagenumErrorInfoTip.html("<fmt:message key='customerinfo.usercode.isnotnull'/>");
			return;
		}
 
		var resultCode = true;
		
		$.ajax({
			url:'front/user/checkUserLogin.do',
			type:"post",
			async:false,
			data:{"userInfoVo.userType":userType,"userInfoVo.autelId":autelId,"userInfoVo.userPwd":password,"userInfoVo.imageCode":imagenum},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId1 = jsonData[0].autelId;
				var userPwd = jsonData[0].userPwd;
				var actCode = jsonData[0].actCode;
				var imageCode = jsonData[0].imageCode;
				var systemError = jsonData[0].systemError;
				if(imageCode == "false")
				{
					$("#imagenumErrorInfo").show();
					$("#imagenumImg").show();
					imagenumErrorInfoTip.html("<fmt:message key='customerinfo.usercode.illegalcharactercheck'/>");
					resultCode = false;
					buildRandom();
					return;
				}
				
				if(systemError == "false")
				{
					$("#imagenumErrorInfo").show();
					$("#imagenumImg").show();
					imagenumErrorInfoTip.html("<fmt:message key='common.system.error'/>");
					resultCode = false;
					buildRandom();
					return;
				}
				
				
				
				if(autelId1 == "false")
				{
					$("#autelIdErrorInfo").show();
					$("#autelIdImg").show();
					autelIdErrorInfoTip.html("<fmt:message key='customerinfo.username.isnotexist'/>");
					resultCode = false;
					return;
				}
				
				if(userPwd == "false")
				{
					$("#passwordErrorInfo").show();
					$("#passwordImg").show();
					passwordErrorInfoTip.html("<fmt:message key='customerinfo.userpassword.illegalcharactercheck'/>");
					resultCode = false;
					return;
				}
				
				if(actCode == "false")
				{
					$("#autelIdErrorInfo").show();
					$("#autelIdImg").show();
					autelIdErrorInfoTip.html("<fmt:message key='customerinfo.username.isnotactive'/>"+autelId+" <fmt:message key='customerinfo.username.isnotactive1'/>");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				$("#imagenumErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='customer.network.not.valid'/>");
				resultCode=false;
			}
		});
		
		
		if(resultCode == false)
		{
			return;
		}
		
		$("#returnUrl").val(location.href);
		$form.action="loginCustomerInfoResult.html?operationType=4";
		$form.submit();
	});
	
	$("#imageField").click(function()
	{
	   
		//window.location.href="regCustomerInfo.html?operationType=1";
		jump("regCustomerInfo.html?operationType=1");
	});
});

function enterkey(e)
{
    if (e==13)
    {
    	$("#loginCheck").click();
    }
}

function clearAllErrorInfo(){
		var userTypeErrorInfoTip = $("#userTypeErrorInfoTip");
		var autelIdErrorInfoTip	= $("#autelIdErrorInfoTip");
		var passwordErrorInfoTip = $("#passwordErrorInfoTip");
		var imagenumErrorInfoTip = $("#imagenumErrorInfoTip");
		
		userTypeErrorInfoTip.html("");
		autelIdErrorInfoTip.html("");
		passwordErrorInfoTip.html("");
		imagenumErrorInfoTip.html("");
		
		$("#userTypeErrorInfo").hide();
		$("#userTypeImg").hide();
			
		$("#autelIdErrorInfo").hide();
		$("#autelIdImg").hide();
		
		$("#passwordErrorInfo").hide();
		$("#passwordImg").hide();
			
		$("#imagenumErrorInfo").hide();
		$("#imagenumImg").hide();
				
}

function clearUserTypeErrorInfo(){
		$("#userTypeErrorInfo").hide();
		$("#userTypeImg").hide();
}

function clearAutelIdErrorInfo(){
		$("#autelIdErrorInfo").hide();
		$("#autelIdImg").hide();
}

function clearPasswordErrorInfo(){
		$("#passwordErrorInfo").hide();
		$("#passwordImg").hide();
}

function clearImagenumErrorInfo(){
		$("#imagenumErrorInfo").hide();
		$("#imagenumImg").hide();
}

function CookieEnable()
{
        var result=false;
        if(navigator.cookiesEnabled){
        	return true;
        }
          
        document.cookie = "testcookie=yes;";
        var cookieSet = document.cookie;
        if (cookieSet.indexOf("testcookie=yes") > -1){
          result=true;
        }
        document.cookie = "";
        return result;
}
function fawordUrl(){
    var autelId=$("#autelId").val();
	jump("goForgotPassword.html?operationType=10&autelId="+autelId);
}      