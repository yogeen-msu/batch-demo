<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){
	var questionCodeResultTip=$("#questionCodeResultTip");
	var answerResultTip=$("#answerResultTip");
	
	$("#checkUpdateCustomerInfo").click(function()
	{
		var code = $("#code").val();
		var questionCode = $("#questionCode").val();
		var answer = $("#answer").val();
		var actCode = $("#actCode").val();
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_.,&'""()\s]+$/i;
		var regNumber = /^[0-9]\d*$/;
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regComUsername = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		

		var operationResultTip = $("#operationResultTip");

		
		operationResultTip.html("");
		
		clearAllErrorInfo();
		var isFlag=true;
		
		if(questionCode == -1)
		{
			$("#questionCodeResult").show();
			$("#questionCodeImg").show();
			questionCodeResultTip.html("<fmt:message key='accountinformation.selectanswer.name'/>");
			if(isFlag){
				 $("#questionCode").focus();
			}
			isFlag =false;
		}
		
		
		operationResultTip.html("");
		
		if(answer == null || answer == "")
		{
			$("#answerResult").show();
			$("#answerImg").show();
			answerResultTip.html("<fmt:message key='accountinformation.answer.isnotnull'/>");
			if(isFlag){
				 $("#answer").focus();
			}
			isFlag =false;
		}
		else if(!regName.test(answer))
		{
			$("#answerResult").show();
			$("#answerImg").show();
			answerResultTip.html("<fmt:message key='accountinformation.answer.illegalcharactercheck'/>");
			if(isFlag){
				 $("#answer").focus();
			}
			isFlag =false;
		}
	
		operationResultTip.html("");
		
		var resultCode = true;
		
		if(!isFlag){
			return;
		}
		
		$.ajax({
			url:'front/user/updateCustomerInfoNew.do',
			type:"post",
			async:false,
			data:{"customerInfoEdit.questionCode":questionCode,"customerInfoEdit.answer":answer,
				"customerInfoEdit.code":code,"customerInfoEdit.actCode":actCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				
				if(result == "1")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.success'/>");
					resultCode = true;
					return;
				}
				else if(result == "2")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.fail'/>");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});

});

function clearAllErrorInfo(){
	
	$("#questionCodeResult").hide();
	$("#questionCodeImg").hide();

}



