<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){
	var comUsernameResultTip=$("#comUsernameResultTip");
	
	$("#checkUpdateCustomerInfo").click(function()
	{
		var code = $("#code").val();
		var comUsername = $("#comUsername").val();
		var actCode = $("#actCode").val();
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_.,&'""()\s]+$/i;
		var regNumber = /^[0-9]\d*$/;
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regComUsername = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		

		var operationResultTip = $("#operationResultTip");

		
		operationResultTip.html("");
		
		clearAllErrorInfo();
		var isFlag=true;
	
		if(comUsername == null || comUsername =="")
		{
			$("#comUsernameResult").show();
			$("#comUsernameImg").show();
			comUsernameResultTip.html("<fmt:message key='accountinformation.comusername.isnotnull'/>");
			if(isFlag){
				$("#comUsername").focus();
			}
			isFlag =false;
		}else if(!regComUsername.test(comUsername))
		{
			$("#comUsernameResult").show();
			$("#comUsernameImg").show();
			comUsernameResultTip.html("<fmt:message key='accountinformation.comusername.illegalcharacterche'/>");
			if(isFlag){
			 $("#comUsername").focus();
			}
			isFlag =false;
		}
	
		operationResultTip.html("");
		
		var resultCode = true;
		
		if(!isFlag){
			return;
		}
		
		$.ajax({
			url:'front/user/updateCustomerInfoNew.do',
			type:"post",
			async:false,
			data:{"customerInfoEdit.comUsername":comUsername,"customerInfoEdit.code":code,"customerInfoEdit.actCode":actCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				
				if(result == "1")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.success'/>");
					resultCode = true;
					return;
				}
				else if(result == "2")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.fail'/>");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});

});

function clearAllErrorInfo(){
	
	$("#comUsernameResult").hide();
	$("#comUsernameImg").hide();
	
}


