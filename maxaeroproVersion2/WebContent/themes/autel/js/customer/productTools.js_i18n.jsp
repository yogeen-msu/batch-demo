<%@ include file="../common.js_i18n.jsp" %>
function downProductTool(downToolPath)
{
	jump("downLoadProductTools.html?operationType=2&downToolPath="+downToolPath);
}

function backDownProductTool(type,code,userType)
{
	//添加客户时查看附件
	if(type == 1)
	{
		jump("queryCustomerComplaintTableDetail.html?operationType=8&code="+code+"&userType="+userType);
	}
	else if(type == 2)//客诉详情回复附件查看
	{
		jump("queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType);
	}else if(type==3){
	    jump("measurecar-1-1.html?m=4&proTypeCode="+code);
	}
	else
	{
		jump("queryProductToolsList.html?operationType=1");
	}
}

function queryProductTools()
{
	var languageCode = $("#languageCode").val();
	jump("queryProductToolsList.html?operationType=1&languageCode="+languageCode);	
}
