$(document).ready(function(){
	var autelId=$("#autelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	if(!regEmail.test(autelId))
	{
	 $('input:radio[name="type"]').get(0).disabled="true";
	 $('input:radio[name="type"]').attr("checked",'2');
	 $("#hiddenType").val("2");
	}
	
	$("#selectForGotSubmit").click(function()
	{
		var type = $("#hiddenType").val();
		var autelId = $("#autelId").val();
		
		if(type == 1)
		{
			jump("selectMailForgotPassword.html?operationType=13&autelId="+autelId);
		}
		else if(type == 2)
		{
			jump("selectQuestionForgotPassword.html?operationType=14&autelId="+autelId);
		}
		
	});
});

function jump(url){
	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
}
function setCheckValue(obj)
{
	$("#hiddenType").val(obj.value);
}