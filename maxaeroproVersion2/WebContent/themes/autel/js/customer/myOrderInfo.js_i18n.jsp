<%@ include file="../common.js_i18n.jsp" %> 
/**
 * 我的订单JS
 */
$(document).ready(function(){
		$("#orderDate").change(function(){
			var orderDate=$("#orderDate").val();
			var orderState=$("#orderState").val();
			jump("myOrders-1-1.html?orderDate="+orderDate+"&orderState="+orderState);
		});
		
		$("#orderState").change(function(){
			var orderDate=$("#orderDate").val();
			var orderState=$("#orderState").val();
			jump("myOrders-1-1.html?orderDate="+orderDate+"&orderState="+orderState);
		});
});

/**
 * 取消订单
 */
function closeOrder(orderCode){
	if(confirm("<fmt:message key='order.confirmcancel' />")){
		$.ajax({
			url:'front/usercenter/customer/removeOrderInfo.do',
			type:"post",
			data:{"orderInfoCode":orderCode},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				jump("myOrders-1-1.html");
			},
			error :function(){
				alert("<fmt:message key='system.error.name' />");
			}
		});
	}
}

