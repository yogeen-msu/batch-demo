package com.cheriscon.rest.base.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/site")
public class SiteRest extends WWAction {
	
	private String regResult;
	private String domain;
	private CopSite site;
	@Resource(name="siteManager")
	private ISiteManager siteManager;
	
	@Action(value = "get", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "site"}) })
	public String get(){
		 site = siteManager.get(domain);
//		regResult = JSONObject.fromObject(site).toString();
		return SUCCESS;
	}

	public String getRegResult() {
		return regResult;
	}

	public void setRegResult(String regResult) {
		this.regResult = regResult;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public CopSite getSite() {
		return site;
	}

	public void setSite(CopSite site) {
		this.site = site;
	}

}
