package com.cheriscon.rest.base.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.cop.resource.IThemeUriManager;
import com.cheriscon.cop.resource.model.ThemeUri;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/themeUri")
public class ThemeUriRest extends WWAction{
	
	@Resource(name="themeUriManager")
	private IThemeUriManager themeUriManager;
	
	private String regResult;
	private List<ThemeUri> themeUris;
	
	@Action(value = "list", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "themeUris"}) })
	public String list(){
		try{
			themeUris = themeUriManager.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
//		regResult=JSONArray.fromObject(themeUris).toString();
		return SUCCESS;
	}

	public String getRegResult() {
		return regResult;
	}

	public void setRegResult(String regResult) {
		this.regResult = regResult;
	}

	public List<ThemeUri> getThemeUris() {
		return themeUris;
	}

	public void setThemeUris(List<ThemeUri> themeUris) {
		this.themeUris = themeUris;
	}

}
