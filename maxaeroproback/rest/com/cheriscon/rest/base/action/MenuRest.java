package com.cheriscon.rest.base.action;


import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.SiteMenu;
import com.cheriscon.app.base.core.service.ISiteMenuManager;
import com.cheriscon.common.model.Language;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/menu")
public class MenuRest extends WWAction {

	@Resource(name="")
	private ISiteMenuManager siteMenuManager;
	
	private List<SiteMenu> menuList;
	private Integer parentid;
	private String type;
	private String languageCode;
	
	@Action(value = "list", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "menuList"}) })
	public String list(){
		Language language = new Language();
		language.setCode(languageCode);
		menuList = siteMenuManager.list(parentid, type, language);
		return SUCCESS;
	}

	public List<SiteMenu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<SiteMenu> menuList) {
		this.menuList = menuList;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

}
