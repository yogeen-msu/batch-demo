package com.cheriscon.rest.base.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.model.Theme;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/theme")
public class ThemeRest extends WWAction{

	private Theme theme;
	private int themeid;
	@Resource
	private IThemeManager themeManager;
	
	@Action(value = "get", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "theme"}) })
	public String get(){
		 theme = themeManager.getTheme(themeid);
		return SUCCESS;
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	public int getThemeid() {
		return themeid;
	}

	public void setThemeid(int themeid) {
		this.themeid = themeid;
	}
	
	
}
