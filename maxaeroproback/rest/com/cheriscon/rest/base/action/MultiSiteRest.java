package com.cheriscon.rest.base.action;


import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.MultiSite;
import com.cheriscon.app.base.core.service.IMultiSiteManager;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/multisite")
public class MultiSiteRest extends WWAction {
	private String  domain;
	private String regResult;
	private MultiSite multiSite;
	
	@Resource(name="multiSiteManager")
	private IMultiSiteManager multiSiteManager;
	
	@Action(value = "getByDomain", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "multiSite"}) })
	public String getByDomain(){
		 multiSite = multiSiteManager.get(domain);
//		regResult=JSONObject.fromObject(multiSite).toString();
		return SUCCESS;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getRegResult() {
		return regResult;
	}

	public void setRegResult(String regResult) {
		this.regResult = regResult;
	}

	public MultiSite getMultiSite() {
		return multiSite;
	}

	public void setMultiSite(MultiSite multiSite) {
		this.multiSite = multiSite;
	}
	
}
