package com.cheriscon.rest.base.action;


import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/language")
public class LanguageRest extends WWAction {
	
	@Resource
	private ILanguageService languageService;
	
	private String code ;
	private Language language;
	private String countryCode;
	private String languageCode;
	private List<Language> languages;
	
	@Action(value = "getByCode", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "language"}) })
	public String getByCode(){
		language = languageService.getByCode(code);
		return SUCCESS;
	}
	
	@Action(value = "getByLocale", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "language"}) })
	public String getByLocale(){
		Locale locale = new Locale(languageCode,countryCode);	//查询中文
		language = languageService.getByLocale(locale);
		return SUCCESS;
	}
	
	@Action(value = "list", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "languages"}) })
	public String list(){
		languages = languageService.queryLanguage();
		return SUCCESS;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

}
