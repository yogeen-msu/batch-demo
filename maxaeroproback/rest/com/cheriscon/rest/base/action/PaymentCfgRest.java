package com.cheriscon.rest.base.action;


import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.system.service.IPaymentCfgService;
import com.cheriscon.common.model.PaymentCfg;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/paymentCfg")
public class PaymentCfgRest extends WWAction {
	
	@Resource
	private IPaymentCfgService paymentCfgService;

	private String pluginid;
	
	@Action(value="getByPluginId")
	public String getByPluginId(){
		try{
			PaymentCfg paymentCfg = paymentCfgService.getByPluginId(pluginid);
			renderText(JSONObject.fromObject(paymentCfg).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getPluginid() {
		return pluginid;
	}

	public void setPluginid(String pluginid) {
		this.pluginid = pluginid;
	}

}
