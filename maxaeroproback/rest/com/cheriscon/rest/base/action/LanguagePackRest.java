package com.cheriscon.rest.base.action;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import net.sf.json.JSONObject;


import com.cheriscon.backstage.product.service.ILanguagePackService;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.Page;

@ParentPackage("json-default")
@Namespace("/rest/languagePack")
public class LanguagePackRest extends WWAction {
	private String softName;
	private String proTypeCode;
	private int pageNo=1;

	@Resource
	private ILanguagePackService languagePackService;
	
	@Action(value = "searchPage")
	public String searchPage(){
		if(softName!=null){
			try {
				softName = new String(softName.getBytes("ISO-8859-1"),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("softName", softName);
		paramMap.put("proTypeCode", proTypeCode);
		try {
			Page webpage  =  languagePackService.searchPage(paramMap, pageNo, pageSize);
			renderText(JSONObject.fromObject(webpage).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getSoftName() {
		return softName;
	}

	public void setSoftName(String softName) {
		this.softName = softName;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	
	
}
