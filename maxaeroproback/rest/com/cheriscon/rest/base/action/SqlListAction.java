package com.cheriscon.rest.base.action;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.component.widget.SqlListWidget;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.impl.SqlListComponet;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.IDBRouter;

@ParentPackage("json-default")
@Namespace("/rest/sql")
public class SqlListAction extends WWAction {
	@Resource private SqlListComponet sqlListComponet;

	private  String sql;
	private String localParam;
	private String languageCode;
	
	@Action(value = "list")
	public String list(){
		sql = "select * "+sql;
		List list =sqlListComponet.list(sql, localParam, languageCode);
		renderText(JSONArray.fromObject(list).toString());
		return null;
	}

	public SqlListComponet getSqlListComponet() {
		return sqlListComponet;
	}

	public void setSqlListComponet(SqlListComponet sqlListComponet) {
		this.sqlListComponet = sqlListComponet;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getLocalParam() {
		return localParam;
	}

	public void setLocalParam(String localParam) {
		this.localParam = localParam;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
	
}
