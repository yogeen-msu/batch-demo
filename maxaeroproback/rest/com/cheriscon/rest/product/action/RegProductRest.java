package com.cheriscon.rest.product.action;


import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.CustomerProInfo;
import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.database.PageFinder;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.customer.service.IProductDateService;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;
//import com.cheriscon.front.usercenter.customer.service.impl.proRegPwd;
//import com.cheriscon.front.usercenter.customer.service.impl.proTypeCode;

@ParentPackage("json-default")
@Namespace("/rest/regProduct")
public class RegProductRest extends WWAction {
	
	@Resource
	private IRegProductService regProductService ;
	@Resource
	private IProductDateService productDateService;
	@Resource
	private IMyAccountService myAccountService;
	@Resource
	private ISystemErrorLogService systemErrorService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private IProductInfoService productInfoService;
	
	private String jsonData;
	private RegProductAcountInfoVO regProductInfoVO;
	private List<ProductInfo> productInfos;
	private String proCode;
	private CustomerProInfo  custProInfo; 
	private String usercode;
	private String result="";
	private String regResult;
	private String customerCode;
	private String newProductSN;
	private String proRegPwd;
	private String oldProductSN;
	private String proSerialNo;
	private String proTypeCode;
	private String sealCode;
	private String remark;
	private int pageNo=1;
	private String autelId;
	private String ip;
	private String mobilePhone;
	private String aircraftSerialNumber;
	
	
	@Action(value = "queryProductInfo")
	public String queryProductInfo(){
		try{
			productInfos = regProductService.queryProductInfo(regProductInfoVO);
			renderText(JSONArray.fromObject(productInfos).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
//	@Action(value = "customerProductNum")
//	public String customerProductNum (){
//		try{
//			custProInfo = regProductService.CustomerProductNum(proCode);
//			result = JSONObject.fromObject(custProInfo).toString();
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//		renderText(result);
//		return null;
//	}
//	
//	@Action(value = "insertCustProInfo")
//	public String insertCustProInfo(){
//		try{
//			regProductService.insertCustProInfo(custProInfo);
//			renderText("success");
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	@Action(value = "addProductSoftwareValidStatus")
//	public String addProductSoftwareValidStatus(){
//		try{
//			regProductService.addProductSoftwareValidStatus(usercode, regProductInfoVO);
//			renderText("success");
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

	
	/**
	 *	注册产品 修改产品拥有者
	 * @return
	 */
	@Action(value = "changeProduct")
	public String changeProduct(){
//		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		regProductInfoVO = new RegProductAcountInfoVO();
		regProductInfoVO.setProRegPwd(proRegPwd.trim());//注册密码
		regProductInfoVO.setProSerialNo(proSerialNo.trim());//产品序列号
		regProductInfoVO.setProTypeCode(proTypeCode);//产品类型code
//		regProductInfoVO.setSealCode(sealCode);//经销商编码
		
		regResult="false";
		List<ProductInfo> list = regProductService.queryProductInfo(regProductInfoVO);
		if (list!=null && list.size()==1) {
			ProductInfo info=list.get(0);
			regProductInfoVO.setProCode(info.getCode());
			boolean flag=regProductService.changeCustProInfo(customerCode, regProductInfoVO);
			if(flag==true){
				 regResult="true";
			}
		}
//		Map<String,String> dataMap = new HashMap<String, String>();
//		dataMap.put("regResult", regResult);
//		regResult=JSONArray.fromObject(dataMap).toString();
//		return SUCCESS;
		renderText(regResult);
		return null;
	}
	
	/**
	 *	注册产品
	 * @return
	 */
	@Action(value = "regProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String regProduct() throws Exception{ 
//		proRegPwd proSerialNo proTypeCode customerCode
//		String returnMsg="";
		try{
			if(StringUtils.isNotEmpty(customerCode)){
			regProductInfoVO = new RegProductAcountInfoVO();
//			regProductInfoVO.setProRegPwd(proRegPwd.trim());//注册密码
			regProductInfoVO.setProSerialNo(proSerialNo.trim());//产品序列号
			regProductInfoVO.setProTypeCode(proTypeCode);//产品类型code
			regProductInfoVO.setSealCode(sealCode);//经销商编码
			
			List<ProductInfo> list = regProductService.queryProductInfo(regProductInfoVO);
			if (list!=null && list.size()==1) {
				ProductInfo info=list.get(0);
				if (info.getRegStatus() == FrontConstant.CHARGE_CARE_IS_YES_REG) {
					/****已注册的产品分为三种情况 
					 * 1.确实已经注册
					 * 2.由以前的老用户注册
					 * 3.已经移除用户关联的产品**/
					
					CustomerProInfo custProInfo=regProductService.CustomerProductNum(info.getCode());
					if(custProInfo!=null){
						try {
							CustomerInfo tempCustInfo=customerInfoService.getCustomerByCode(custProInfo.getCustomerCode());
							String checkemail = "^([a-z0-9A-Z]+[-_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
							Pattern pattern = Pattern.compile(checkemail);
							Matcher matcher = pattern.matcher(tempCustInfo.getAutelId());
							boolean flag=matcher.matches();
							if(flag==true){
								regResult="reg";//已注册
							}else{
								regResult="regConfirm,"+tempCustInfo.getAutelId();//提示已经关联
//								returnMsg=FreeMarkerPaser.getBundleValue("add.product.msg.title");
//								returnMsg=returnMsg.replace("{0}", tempCustInfo.getAutelId());
//								returnMsg=returnMsg.replace("{1}", regProductInfoVO.getProSerialNo());
//								returnMsg=returnMsg.replace("{2}", autelId);
							}
							
						} catch (Exception e) {
							
						}
						
					}else{
						CustomerProInfo tempInfo=new CustomerProInfo();
						tempInfo.setCustomerCode(customerCode);
						tempInfo.setProCode(info.getCode());
						boolean result=regProductService.insertCustProInfo(tempInfo);
						
						ProductAssociatesDate proDate=productDateService.getProductDateInfo(info.getCode());
						if(proDate==null){
							proDate=new ProductAssociatesDate();
							proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
							proDate.setProCode(info.getCode());
							result=productDateService.insertProductDate(proDate);
						}else{
							result=productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), info.getCode());
						}
						
						if(result){
							regResult="true";
						}else{
							regResult="false";
						}
					}
				}else if(info.getRegStatus() == FrontConstant.PRODUCT_REGSTATUS_UNBINDED){
					regResult="unbinded";//已解绑
					
				}else if(info.getSerialNo().length()-proSerialNo.trim().length()>1){ //序列号位数不一样
					regResult="notFound";
				}else {
					regProductInfoVO.setProSerialNo(info.getSerialNo());
					boolean result=insertRegProductInfo();
					
					ProductAssociatesDate proDate=productDateService.getProductDateInfo(info.getCode());
					if(proDate==null){
						proDate=new ProductAssociatesDate();
						proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
						proDate.setProCode(info.getCode());
						result=productDateService.insertProductDate(proDate);
					}else{
						result=productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), info.getCode());
					}
					
					int proCount=0;
					if (result) {
						
						//用户注册产品数量
						try {
							proCount=myAccountService.queryProductCount(customerCode);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (proCount>=2) {
						regResult=String.valueOf("true");
					}else{
						regResult=String.valueOf("true1");
					}
				}
			}else {
				regResult="notFound";
			}
		}else{
			regResult = "notLogin";
		}
		}catch(Exception ex){
			
			regResult="SystemError";
			
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.front.usercenter.customer.action.RegProductAction".equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
				System.out.println(errorMsg);
			}
			}
			systemErrorService.saveLog("产品注册",errorMsg);
		}
		
//		Map<String,String> dataMap = new HashMap<String, String>();
//		dataMap.put("regResult", regResult);
//		dataMap.put("returnMsg", returnMsg);
//		dataMap.put("serialNo", regProductInfoVO.getProSerialNo());
//		regResult=JSONArray.fromObject(dataMap).toString();
		renderText(regResult);
		return null;
	}

	/**
	 * 20160428 A16043 hlh
	 *	无人机APP注册产品
	 * @return
	 */
	@Action(value = "regProductUAV", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"}) })
	public String regProductUAV() throws Exception{ 
		Map map = new HashMap();
		try{
			if(StringUtils.isNotEmpty(customerCode)){
			regProductInfoVO = new RegProductAcountInfoVO();
			regProductInfoVO.setAricraftSerialNumber(aircraftSerialNumber.trim());//飞机序列号
			regProductInfoVO.setProTypeCode(proTypeCode);//产品类型code
			regProductInfoVO.setSealCode(sealCode);//经销商编码
			
			/*通过飞机序列号查询主序列号*/
			ProductInfo pro = regProductService.queryProductByAricraftSerialNumber(aircraftSerialNumber.trim());
			if(null != pro){
				regProductInfoVO.setProSerialNo(pro.getSerialNo());//飞机序列号
			}else{
				map.put("code",1);
				map.put("regResult","notFoundAircraftSN");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
			
			List<ProductInfo> list = regProductService.queryProductInfo(regProductInfoVO);
			if (list!=null && list.size()==1) {
				ProductInfo info=list.get(0);
				if (info.getRegStatus() == FrontConstant.CHARGE_CARE_IS_YES_REG) {
					/****已注册的产品分为三种情况 
					 * 1.确实已经注册
					 * 2.由以前的老用户注册
					 * 3.已经移除用户关联的产品**/
					
					CustomerProInfo custProInfo=regProductService.CustomerProductNum(info.getCode());
					if(custProInfo!=null){
						try {
							CustomerInfo tempCustInfo=customerInfoService.getCustomerByCode(custProInfo.getCustomerCode());
							String checkemail = "^([a-z0-9A-Z]+[-_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
							Pattern pattern = Pattern.compile(checkemail);
							Matcher matcher = pattern.matcher(tempCustInfo.getAutelId());
							boolean flag=matcher.matches();
							if(flag==true){
								//regResult="reg,"+tempCustInfo.getAutelId();//已注册，注册用户账号为 tempCustInfo.getAutelId()
								map.put("code",2);
								map.put("regResult","reg,"+tempCustInfo.getAutelId());
								jsonData = JSONArray.fromObject(map).toString();
								return SUCCESS;
							}else{
								//regResult="regConfirm,"+tempCustInfo.getAutelId();//提示已经关联
								map.put("code",3);
								map.put("regResult","regConfirm,"+tempCustInfo.getAutelId());
								jsonData = JSONArray.fromObject(map).toString();
								return SUCCESS;
							}
							
						} catch (Exception e) {
							
						}
						
					}else{
						CustomerProInfo tempInfo=new CustomerProInfo();
						tempInfo.setCustomerCode(customerCode);
						tempInfo.setProCode(info.getCode());
						boolean result=regProductService.insertCustProInfo(tempInfo);
						
						ProductAssociatesDate proDate=productDateService.getProductDateInfo(info.getCode());
						if(proDate==null){
							proDate=new ProductAssociatesDate();
							proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
							proDate.setProCode(info.getCode());
							result=productDateService.insertProductDate(proDate);
						}else{
							result=productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), info.getCode());
						}
						
						if(result){
							//regResult="true";
							map.put("code",0);
							map.put("regResult","true");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}else{
							//regResult="false";
							map.put("code",4);
							map.put("regResult","false");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}
					}
				}else if(info.getRegStatus() == FrontConstant.PRODUCT_REGSTATUS_UNBINDED){
					//regResult="unbinded";//已解绑
					map.put("code",5);
					map.put("regResult","unbinded");
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
					
				}else {
					regProductInfoVO.setProSerialNo(info.getSerialNo());
					boolean result=insertRegProductInfo();
					
					ProductAssociatesDate proDate=productDateService.getProductDateInfo(info.getCode());
					if(proDate==null){
						proDate=new ProductAssociatesDate();
						proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
						proDate.setProCode(info.getCode());
						result=productDateService.insertProductDate(proDate);
					}else{
						result=productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), info.getCode());
					}
					if(result){
						//regResult="true";
						map.put("code",0);
						map.put("regResult","true");
						jsonData = JSONArray.fromObject(map).toString();
						return SUCCESS;
					}else{
						//regResult="false";
						map.put("code",4);
						map.put("regResult","false");
						jsonData = JSONArray.fromObject(map).toString();
						return SUCCESS;
					}
				}
			}else {
				//regResult="notFound";
				map.put("code",6);
				map.put("regResult","notFound");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
		}else{
			//regResult = "notLogin";
			map.put("code",7);
			map.put("regResult","notLogin");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		}catch(Exception ex){
			
			//regResult="SystemError";
			map.put("code",8);
			map.put("regResult","SystemError");
			jsonData = JSONArray.fromObject(map).toString();
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.front.usercenter.customer.action.RegProductAction".equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
				System.out.println(errorMsg);
			}
			}
			systemErrorService.saveLog("产品注册",errorMsg);
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
//		renderText(regResult);
//		return null;
	}
	
	/**
	 * 
	 * 获取注册的产品数量
	 * @return
	 */
	@Action(value = "queryProductCount")
	public String queryProductCount(){
		int count = 0;
		try {
			 count = myAccountService.queryProductCount(customerCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		renderText(count+"");
		return null;
	}
	
	/**
	 * 分页我的产品
	 * @return
	 */
	@Action(value = "queryProducts")
	public String queryProducts(){
		try {
			Page page = myAccountService.queryProducts(remark, usercode, pageNo, pageSize);
			renderText(JSONObject.fromObject(page).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 根据产品序列号查询产品
	 * @return
	 */
	@Action(value = "queryProductBySerialNo")
	public String queryProductBySerialNo(){
		try {
			ProductInfo product = regProductService.queryProductBySerialNo(proSerialNo);
			renderText(JSONObject.fromObject(product).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Action(value = "queryProductsSec")
	public String queryProductSec() {
		try {
			if ("abc@abc.abc".equals(usercode)) {
				usercode = "cui2015121809520004259ffa10";
			}
			Page page = myAccountService.queryProducts(remark, usercode, pageNo, pageSize);
			PageFinder pageFinder = new PageFinder();
			pageFinder.setData( (List) page.getResult());
			pageFinder.setRowCount(page.getTotalCount());
//			pageFinder.setPageCount(page.getTotalPageCount());
			pageFinder.setPageNo(pageNo);
//			pageFinder.setPageSize(page.getPageSize());
			renderText(JSONObject.fromObject(pageFinder).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/***
	 * 修改产品备注
	 * @return
	 */
	@Action(value = "updateProductRemark", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String updateProductRemark(){
		ProductInfo proInfo=productInfoService.getByProCode(proCode);
		if(null==proInfo){
			regResult="FAIL";
		}else{
			proInfo.setRemark(remark);
			productInfoService.updateProductInfo(proInfo);
			regResult="SUCCESS";
		}
		renderText(regResult);
		return null;
	}
	
	/*** A16043 hlh 2016-11-11
	 * 给无人机产品取别名
	 * @return
	 */
	@Action(value = "updateProductName", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String updateProductName(){
		Map map = new HashMap();
		ProductInfo proInfo=productInfoService.getProInfoByProCodeAndCustomerCode(proCode, customerCode);
		if(null==proInfo){
			map.put("code",1);
			map.put("result","产品不存在，无法修改");
			regResult=JSONArray.fromObject(map).toString();
			return SUCCESS;
		}else{
			proInfo.setExternInfo6(remark); // 将无人机的别名用  备用字段 6  来存储。
			productInfoService.updateProductInfo(proInfo);
			map.put("code",0);
			map.put("result", "success");
			regResult=JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
	}
	
	/**
	 * 查询用户产品
	 * @return
	 */
	@Action(value = "queryProductByUserCode")
	public String queryProductByUserCode(){
		try {
			List<MyProduct> myproducts = myAccountService.queryProductByCode(usercode);
			renderText(JSONArray.fromObject(myproducts).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 移除产品注册
	 * @return
	 */
	@Action(value = "removeProductInfo")
	public String removeProductInfo(){
		if(proCode!=null){
			ProductInfo product=regProductService.queryProductByCode(proCode);
			if(null == product){
				regResult="false";
			}else{
				
				ProductAssociatesDate productDate=productDateService.getProductDateInfo(proCode);
				String nowDate=DateUtil.toString(new Date(), "yyyy-MM-dd");
				int num=compareDate(productDate.getAssociatesDate(),nowDate);
				if(num>0){
					regResult="time";
				}else{
					if(regProductService.removeProductInfo(proCode, usercode, ip)){
						regResult="true";
					}else{
						regResult="false";
					}
				}
				
				
			}
		}else{
			regResult="false";
		}
		renderText(regResult);
		return null;
	}
	
	@Transactional
	private boolean insertRegProductInfo() {
		
		// 把产品对应的标准出厂软件配置（最小销售单位）写到产品软件效状态表中
		boolean regProductResult =regProductService.addProductSoftwareValidStatus(customerCode,regProductInfoVO);	
		
		return regProductResult;
		
	}
	
	
	/**
	 * 为国内提供的特殊接口 通过 序列号 注册密码 手机号，生成一条注册用户 并绑定产品序列号
	 * 提供接口接受产品序列号和注册秘密 
	 * 根据序列号和密码添加新用户 并将对应的产品序列号绑定到对应的用户下
	 * @return
	 */
	@Action(value="regProductForCN")
	public String regProductForCN(){
		int total = 1;
		int successtotal = 0;
		StringBuilder errorSerialNo = new StringBuilder();
		if(StringUtils.isNotEmpty(proSerialNo)&&StringUtils.isNotBlank(proRegPwd)){
			ProductInfo	 productInfo = productInfoService.getBySerialNo(proSerialNo);
//			未找到产品
			if(productInfo==null){
				errorSerialNo.append(",{\"serialNo\":\"").append(proSerialNo)
				.append("\",\"code\":2,\"errormsg\":\"没有找到序列号对应的产品\"").append("}");
//			产品不为未注册状态
			}else if(productInfo.getRegStatus()!=FrontConstant.PRODUCT_REGSTATUS_NO){
				errorSerialNo.append(",{\"serialNo\":\"").append(proSerialNo)
				.append("\",\"code\":3,\"errormsg\":\"产品非未注册状态\"").append("}");
			}else if(!productInfo.getRegPwd().equals(proRegPwd)){
				errorSerialNo.append(",{\"serialNo\":\"").append(proSerialNo)
				.append("\",\"code\":4,\"errormsg\":\"产品注册密码不对\"").append("}");
			}else{
				try {
					CustomerInfo customerInfo = customerInfoService.getCustBySerialNo2(proSerialNo);
					if(customerInfo!=null){
						errorSerialNo.append(",{\"serialNo\":\"").append(proSerialNo)
						.append("\",\"code\":5,\"errormsg\":\"用户已存在\"").append("}");
					}else{
//								添加新用户
						CustomerInfo newCustomer = new CustomerInfo();
						newCustomer.setAutelId(proSerialNo);
						String upwd = Md5PwdEncoder.getInstance().encodePassword(proRegPwd, null);
						newCustomer.setUserPwd(upwd);
						newCustomer.setRegTime(new DateUtil().toString(new Date(), "yyyy/MM/dd HH:mm:ss"));
						newCustomer.setMobilePhone(mobilePhone);
						
//						默认语言和国家信息
//						中文
						newCustomer.setLanguageCode("lag201304221103300175");
//						中国
						newCustomer.setCountry("中国");
//						论坛名称
						newCustomer.setComUsername(proSerialNo);
//								newCustomer.setFirstName(serialNo.substring(0,8));
//						激活
						newCustomer.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
						newCustomer.setActCode(Md5PwdEncoder.getInstance().encodePassword(proSerialNo+proRegPwd, null));
						newCustomer.setSendActiveTime(new DateUtil().toString(new Date(), "yyyy-MM-dd HH:mm:ss"));

						newCustomer.setSecondActState(0);
						newCustomer.setIsAllowSendEmail(0);
								
						customerInfoService.addCustomer(newCustomer);
								
						customerInfo = customerInfoService.getCustomerInfoByAutelId(proSerialNo);
								
//						注册产品
						RegProductAcountInfoVO regProductInfoVO = new RegProductAcountInfoVO();
						regProductInfoVO.setProSerialNo(proSerialNo);
						regProductInfoVO.setProRegPwd(proRegPwd);
						regProductInfoVO.setProTypeCode(productInfo.getProTypeCode());
						regProductInfoVO.setProCode(productInfo.getCode());
						boolean regProductResult =regProductService.addProductSoftwareValidStatus(customerInfo.getCode(),
								regProductInfoVO);
						if(!regProductResult){
							errorSerialNo.append(",{\"serialNo\":\"").append(proSerialNo)
							.append("\",\"code\":6,\"errormsg\":\"产品注册失败\"").append("}");
						}else{
							successtotal++;
//							添加产品关联时间
							ProductAssociatesDate proDate=productDateService.getProductDateInfo(productInfo.getCode());
							if(proDate==null){
								proDate=new ProductAssociatesDate();
								proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
								proDate.setProCode(productInfo.getCode());
								productDateService.insertProductDate(proDate);
							}else{
								productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), productInfo.getCode());
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					errorSerialNo.append(",{\"serialNo\":\"").append(proSerialNo)
					.append("\",\"code\":7,\"errormsg\":\"验证用户出错\"").append("}");
				}
						
				}
			}
//		}
			
//		}
		if(errorSerialNo.length()>0){
			errorSerialNo.deleteCharAt(0);
		}
		StringBuilder sbd = new StringBuilder();
		sbd.append("{\"total\":").append(total).append(",\"success\":")
			.append(successtotal).append(",\"error\":[").append(errorSerialNo.toString()).append("]}");
		renderText(sbd.toString());
		return null;
	}
	
	private int  compareDate(String s1,String s2){
		java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd");
		java.util.Calendar c1=java.util.Calendar.getInstance();
		java.util.Calendar c2=java.util.Calendar.getInstance();
		try {
			c1.setTime(df.parse(s1));
			c1.add(Calendar.MONTH, 3);
			c2.setTime(df.parse(s2));
			int result=c1.compareTo(c2);
			System.out.println(result);
			return  result;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public RegProductAcountInfoVO getRegProductInfoVO() {
		return regProductInfoVO;
	}

	public void setRegProductInfoVO(RegProductAcountInfoVO regProductInfoVO) {
		this.regProductInfoVO = regProductInfoVO;
	}

	public List<ProductInfo> getProductInfos() {
		return productInfos;
	}

	public void setProductInfos(List<ProductInfo> productInfos) {
		this.productInfos = productInfos;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public CustomerProInfo getCustProInfo() {
		return custProInfo;
	}

	public void setCustProInfo(CustomerProInfo custProInfo) {
		this.custProInfo = custProInfo;
	}

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getRegResult() {
		return regResult;
	}

	public void setRegResult(String regResult) {
		this.regResult = regResult;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getNewProductSN() {
		return newProductSN;
	}

	public void setNewProductSN(String newProductSN) {
		this.newProductSN = newProductSN;
	}

	public String getProRegPwd() {
		return proRegPwd;
	}

	public void setProRegPwd(String proRegPwd) {
		this.proRegPwd = proRegPwd;
	}

	public String getOldProductSN() {
		return oldProductSN;
	}

	public void setOldProductSN(String oldProductSN) {
		this.oldProductSN = oldProductSN;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getSealCode() {
		return sealCode;
	}

	public void setSealCode(String sealCode) {
		this.sealCode = sealCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getAircraftSerialNumber() {
		return aircraftSerialNumber;
	}

	public void setAircraftSerialNumber(String aircraftSerialNumber) {
		this.aircraftSerialNumber = aircraftSerialNumber;
	}
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
}
