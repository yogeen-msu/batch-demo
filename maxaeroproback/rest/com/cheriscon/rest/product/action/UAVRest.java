package com.cheriscon.rest.product.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.market.vo.ProductForSealerConfig;
import com.cheriscon.backstage.member.service.ICustomerChangeAutelIdLogService;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.backstage.system.service.ICustomerProInfoHistoryService;
import com.cheriscon.backstage.system.service.ILanguageConfigService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductChangeNoLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.IProductLockService;
import com.cheriscon.backstage.system.service.IProductResetLogService;
import com.cheriscon.common.model.CustomerChangeAutelIdLog;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.CustomerProInfoHistory;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.LanguageConfig;
import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.common.model.ProductChangeNoLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductResetLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.JsonHelper;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.customer.service.IProductDateService;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author A16043  hlh
 * @description  此文档为无人机产品网站提供给 无人机APP接口调用的部分接口函数
 *
 */
@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/rest/UAV")
public class UAVRest extends WWAction{
	 
	private String jsonData;
	private String saleContractName;
	private String saleContractCode;
	private String startTime;
	private String endTime;
	private List<SaleContract> saleContractList;
	
	// autelID  与 url 用于APP客户的单点登录到论坛的操作。
	private String autelID;
	private String url;
	
	//用于SAP调用解绑（产品重置）
	private String serialNo; //需要重置的机器序列号
	private String resetReason;//产品重置的原因
	private String username; //SAP调用接口时的操作员
	
	//用户 无人机退换货系统的 换机操作
	private String oldProductSN;
	private String newProductSN;
	private String operatorName;
	
	//无人机更换AutelRobotics ID 的接口操作
	private String operator;//操作员
	private String aircraftSerialNumber;//飞机序列号
	private String newAutelROboticsId;//新AutelRoboticsID
	private String operatorReason;//操作原因
	
	//无人机退换货系统接口  查询换机日志
	private String oldSn;
	private String newSn;
	private String autelRoticsID;
	//无人机退换货系统接口  查询出厂产品重置记录
	private String resetSn;
	//无人机退换货系统接口 查询产品信息
	private String mainSn;
	private String airSn;
	private String imuSn;
	private String remoteSn;
	
	
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private IRegProductService regProductService;
	@Resource
	private ICustomerProductService customerProductService;
	@Resource
	private IProductChangeNoLogService productChangeNoLogService;
	@Resource
	private IProductDateService productDateService;
	@Resource
	private IProductForSealerService productForSealerService;
	@Resource
	private ILanguageService lanService;
	@Resource
	private ILanguageConfigService languageService;
	@Resource
	private ISealerInfoService sealerInfoService;
	@Resource
	private IProductInfoService productInfoService; 
	@Resource
	private IProductSoftwareValidStatusService productSoftwareValidStatusService;
	@Resource
	private IProductResetLogService productResetService;
	@Resource
	private ICustomerChangeAutelIdLogService custmerChangeAutelIdService;
	
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	public String getSaleContractName() {
		return saleContractName;
	}

	public void setSaleContractName(String saleContractName) {
		this.saleContractName = saleContractName;
	}

	public String getSaleContractCode() {
		return saleContractCode;
	}

	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public List<SaleContract> getSaleContractList() {
		return saleContractList;
	}

	public void setSaleContractList(List<SaleContract> saleContractList) {
		this.saleContractList = saleContractList;
	}
	
	public String getAutelID() {
		return autelID;
	}

	public void setAutelID(String autelID) {
		this.autelID = autelID;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getResetReason() {
		return resetReason;
	}

	public void setResetReason(String resetReason) {
		this.resetReason = resetReason;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOldProductSN() {
		return oldProductSN;
	}

	public void setOldProductSN(String oldProductSN) {
		this.oldProductSN = oldProductSN;
	}

	public String getNewProductSN() {
		return newProductSN;
	}

	public void setNewProductSN(String newProductSN) {
		this.newProductSN = newProductSN;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	
	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getAircraftSerialNumber() {
		return aircraftSerialNumber;
	}

	public void setAircraftSerialNumber(String aircraftSerialNumber) {
		this.aircraftSerialNumber = aircraftSerialNumber;
	}

	public String getNewAutelROboticsId() {
		return newAutelROboticsId;
	}

	public void setNewAutelROboticsId(String newAutelROboticsId) {
		this.newAutelROboticsId = newAutelROboticsId;
	}

	public String getOperatorReason() {
		return operatorReason;
	}

	public void setOperatorReason(String operatorReason) {
		this.operatorReason = operatorReason;
	}

	public String getOldSn() {
		return oldSn;
	}

	public void setOldSn(String oldSn) {
		this.oldSn = oldSn;
	}

	public String getNewSn() {
		return newSn;
	}

	public void setNewSn(String newSn) {
		this.newSn = newSn;
	}

	public String getAutelRoticsID() {
		return autelRoticsID;
	}

	public void setAutelRoticsID(String autelRoticsID) {
		this.autelRoticsID = autelRoticsID;
	}

	public String getResetSn() {
		return resetSn;
	}

	public void setResetSn(String resetSn) {
		this.resetSn = resetSn;
	}

	public String getMainSn() {
		return mainSn;
	}

	public void setMainSn(String mainSn) {
		this.mainSn = mainSn;
	}

	public String getAirSn() {
		return airSn;
	}

	public void setAirSn(String airSn) {
		this.airSn = airSn;
	}

	public String getImuSn() {
		return imuSn;
	}

	public void setImuSn(String imuSn) {
		this.imuSn = imuSn;
	}

	public String getRemoteSn() {
		return remoteSn;
	}

	public void setRemoteSn(String remoteSn) {
		this.remoteSn = remoteSn;
	}

	/**
	 * 根据 契约名，契约号，创建（更新）起止时间查询对应的契约
	 * A16043 hlh 2016-04-23
	 * @return
	 * @throws Exception
	 */
	@Action(value = "getSaleContractUAV", results = {@Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"})})
	public String saleContracts() throws Exception {
		Map map = new HashMap();
		try {
			List<SaleContract> list = this.saleContractService.getSaleContractUAV(saleContractName,saleContractCode,startTime,endTime);
			if(list != null){
				map.put("result", list);
			}else{
				map.put("result", "[]");
			}
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "[9999]");//异常
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}

	/**
	 * 根据无人机APP发过来的URL判断其单点登录的跳转
	 * @param url
	 * @return
	 */
	@Action(value = "checkSingleLoginByAutelIDAndURL", results = {@Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"})})
	public String checkSingleLoginByURL(){
		// 限制访问IP ，只允许 APP的服务器访问过来。
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		HttpServletResponse response  = ThreadContextHolder.getHttpResponse();
		//先判断用户id存在
		if(StringUtils.isNotEmpty(autelID)){
			//调用网页的登录方法
			CustomerInfo customerInfo;
			try {
				customerInfo = customerInfoService.getCustomerInfoByAutelId(autelID);
				SessionUtil.setLoginUserInfo(request, customerInfo);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			url = "https://support.autelrobotics.com/support/discussions"; // 无人机单点登录网址
			response.sendRedirect(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
 
	
	/**
	 * 以下接口提供给SAP中调用中调用无人机产品网站的接口
	 */
	//--------------------------------------------------------------------------------------------
	/**
	 * 20160331 A16043 hlh 获取所有销售契约信息
	 * @return
	 */
	@Action(value = "getAllSaleContract", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getAllSaleContract(){
		Map map = new HashMap();
		try {
			List<SaleContract> list = this.saleContractService.listAll();
			if(list != null){
				List<SaleContractSAP> listContractSAP = new ArrayList<SaleContractSAP>();
				for (SaleContract saleContract : list) {
					SaleContractSAP contract = new SaleContractSAP ();
					contract.setCode(saleContract.getCode());
					contract.setName(saleContract.getName());
					contract.setSealerCode(saleContract.getSealerCode());
					contract.setLanguageCfgCode(saleContract.getLanguageCfgCode());
					contract.setProTypeCode(saleContract.getProTypeCode());
					
					listContractSAP.add(contract);
				}
				map.put("result", listContractSAP);
			}else{
				map.put("result", "[]");
			}
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}
	
	/**
	 * 20160523 hlh 根据时间获取改最后修改时间在此时间之后的所有销售契约
	 * @return
	 */
	@Action(value = "getSaleContractByDate", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getSaleContractByDate(){
		Map map = new HashMap();
		try {
		    List<SaleContract> list = this.saleContractService.getContrctByDate(startTime) ;
			if(list != null){
				List<SaleContractSAP> listContractSAP = new ArrayList<SaleContractSAP>();
				for (SaleContract saleContract : list) {
					SaleContractSAP contract = new SaleContractSAP ();
					contract.setCode(saleContract.getCode());
					contract.setName(saleContract.getName());
					contract.setSealerCode(saleContract.getSealerCode());
					contract.setLanguageCfgCode(saleContract.getLanguageCfgCode());
					contract.setProTypeCode(saleContract.getProTypeCode());
					
					listContractSAP.add(contract);
				}
				map.put("result", listContractSAP);
			}else{
				map.put("result", "[]");
			}
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}
	
	/**
	 *  实现参考com.cheriscon.backstage.system.action.ProductContractLogAction 的copyProduct（） 方法。
	 * 产品网站提供给无人机退换货系统
	 * A16043 hlh  20160504
	 * 
	 * @return
	 */
	@Action(value = "copyProductSoldService", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String copyProductSoldService() {
		if (newProductSN.equals(oldProductSN)) {
//			jsonData = "新老序列号不能一样";
			jsonData = "The new serial number shall be different from the old one";
			return SUCCESS;
		}
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		ProductInfo newProduct = regProductService
				.queryProductAndSealer(newProductSN); // 判断新产品是否正确
		if (null == newProduct) {
//			jsonData = "新产品序列号错误，该产品不存在";
			jsonData = "Wrong serial number. The product does not exist";
		} else {

			if (newProduct.getRegStatus() == FrontConstant.CHARGE_CARE_IS_YES_REG) {
//				jsonData = "该新产品已经注册，请更换新序列号";
				jsonData = "This product has been registered, please try a new serial number";				
			} else {
				ProductInfo oldProduct = regProductService
						.queryProductAndSealer(oldProductSN);
				if (oldProduct == null
						|| oldProduct.getRegStatus() == FrontConstant.CHARGE_CARE_IS_NO_REG) {
//					jsonData = "该老产品未注册，或不存在";
					jsonData = "This product has not been registered before, or not exists";
				} else if (!newProduct.getProTypeCode().equals(
						oldProduct.getProTypeCode())) {
//					jsonData = "产品类型错误";
					jsonData = "Product type mismatch";
				} else if (!newProduct.getSealerAutelId().equals(
						oldProduct.getSealerAutelId())) {
//					jsonData = "产品序列号不属于经销商";
					jsonData = "This product does not belong to the distributor";
				} else {
					/**
					 * 开始执行更换操作 1.将产品的信息写入中间表，包括原用户code，有效期等
					 * ***/
					ProductChangeNoLog log = new ProductChangeNoLog();
					log.setCreateUser(operatorName);
					log.setCreateIp(request.getRemoteAddr());
					log.setCreateDate(DateUtil.toString(new Date(),
							FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));

					log.setOldProCode(oldProduct.getCode());
					log.setNewProCode(newProduct.getCode());
					log.setValidDate(oldProduct.getValidDate()); // 有效期
					log.setRegDate(oldProduct.getRegTime()); // 注册日期
					try {
						// 更换产品序列号-用户code
						String customerCode = customerProductService
								.getCustomerproinfo(oldProduct.getCode());
						log.setCustomerCode(customerCode);
						// 先将原来的关系记录日志后删除
						productChangeNoLogService.saveLog(log);
						// 绑定新的关系
						RegProductAcountInfoVO regProductInfoVO = new RegProductAcountInfoVO();
						regProductInfoVO.setProCode(newProduct.getCode());
						regProductInfoVO.setProRegPwd(newProduct.getRegPwd());
						regProductInfoVO.setProSerialNo(newProduct
								.getSerialNo());
						regProductInfoVO.setProTypeCode(newProduct
								.getProTypeCode());
						boolean result = regProductService
								.addProductSoftwareValidStatus(customerCode,
										oldProduct.getValidDate(),
										oldProduct.getRegTime(),
										oldProduct.getProDate(),
										regProductInfoVO);

						ProductAssociatesDate proDate = productDateService
								.getProductDateInfo(newProduct.getCode());
						if (proDate == null) {
							proDate = new ProductAssociatesDate();
							proDate.setAssociatesDate(DateUtil.toString(
									new Date(), "yyyy-MM-dd"));
							proDate.setProCode(newProduct.getCode());
							result = productDateService
									.insertProductDate(proDate);
						} else {
							result = productDateService
									.updateProductDate(DateUtil.toString(
											new Date(), "yyyy-MM-dd"),
											newProduct.getCode());
						}
						if (result) {
//							jsonData = "换机拷贝成功";
							jsonData = "Succeed to copy information";
						} else {
//							jsonData = "换机拷贝失败";
							jsonData = "Fail to copy information";
						}
					} catch (Exception e) {
//						jsonData = "系统异常:"+e.getMessage();
						jsonData = "System Exception:"+e.getMessage();
					}
				}
			}

		}
		return SUCCESS;
	}
	
	
	/**
	 * 给SAP调用的获取所有外部产品信息及其对应的内容产品名称的接口
	 * A16043 hlh 20160504
	 * @return
	 */
	@Action(value = "getAllProductForSealer", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getAllProductForSealer(){
		Map map = new HashMap();
		List<ProductForSealer> list = null;
		 try {
			 list =  productForSealerService.getProductForSealerList();
			 if(list == null || list.size()==0){
				 map.put("result", "[]"); //系统无对应的数据
				 jsonData = JSONArray.fromObject(map).toString();
			 }else{
				 List<ProductTypeSAP> listProductTypeSAP = new ArrayList<ProductTypeSAP>();
				 for (ProductForSealer proTypeSealer : list) {
					 ProductTypeSAP pType = new ProductTypeSAP();
					 pType.setCode(proTypeSealer.getCode()); //外产品code
					 pType.setName(proTypeSealer.getName()); //外部产品name
					 
					 // 无人机与诊断的区别在于：无人机传给SAP的外部产品名称 与内部产品名称为同一个值。  不需要进行转化。 
					 // 因为无人机生产系统不像诊断的生产管理系统一样，产品的名称对应不上。
					 //无人机生产管理系统没有产品名称，故只需要传递网站的外部名称即可。
					 pType.setInnerName(proTypeSealer.getName()); //内部产品name
					 
					 listProductTypeSAP.add(pType);
				 }
				 map.put("result", listProductTypeSAP);
				 jsonData = JSONArray.fromObject(map).toString();
			 }
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result","[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}
	
	
	/**
	 * 给SAP调用的获取所有语言配置的接口函数
	 * A16043 hlh 20160505
	 * @return
	 */
	@Action(value = "getAllLanguageConfigSAP", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getAllLanguageConfigSAP(){
		Map map = new HashMap();
		List<LanguageConfig> list = null;
		 try {
			 list =  languageService.listAll();
			 if(list == null || list.size()==0){
				 map.put("result", "[]"); //系统无对应的数据
				 jsonData = JSONArray.fromObject(map).toString();
			 }else{
				 List<LanguageConfigSAP> listLanguageConfigSAP = new ArrayList<LanguageConfigSAP>();
				 for (LanguageConfig languageCfg : list) {
					 LanguageConfigSAP cfg = new LanguageConfigSAP ();
					 cfg.setCode(languageCfg.getCode());
					 cfg.setName(languageCfg.getName());
					 
					 List<Language> langlist = lanService.queryByLanguageCfgCode(languageCfg.getCode());  //取得语言配置对应的基础语言信息
					 StringBuffer lanCodeBuffer = new StringBuffer(""); //拼接基础语言表中的languageCode字段，转为大写，传递给SAP系统 ,数据形式如  EN|CN|GN
					 for(int i=0;i<langlist.size();i++){
						 if(i==0){
							 lanCodeBuffer.append(langlist.get(i).getLanguageCode().toUpperCase());  
						 }else{
							 lanCodeBuffer.append("|").append(langlist.get(i).getLanguageCode().toUpperCase());
						 }
					 }
					 cfg.setBaseLanguageNames(lanCodeBuffer.toString());
					 listLanguageConfigSAP.add(cfg);
				 }
				 map.put("result", listLanguageConfigSAP);
				 jsonData = JSONArray.fromObject(map).toString();
			 }
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result","[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}
	
	
	/**
	 * 给SAP调用的获取所有经销商信息
	 * A16043 hlh 20160518
	 * @return
	 */
	@Action(value = "getAllSealerInfoSAP", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getAllSealerInfoSAP(){
		Map map = new HashMap();
		List<SealerInfo> list = null;
		 try {
			 list =  sealerInfoService.querySealerInfo();
			 if(list == null || list.size()==0){
				 map.put("result", "[]"); //系统无对应的数据
				 jsonData = JSONArray.fromObject(map).toString();
			 }else{
				 List<SealerInfoSAP> listSealer = new ArrayList<SealerInfoSAP>();
				 for (SealerInfo sealer : list) {
					 SealerInfoSAP sealerSAP = new SealerInfoSAP ();
					 //向SAP传递经销商信息中的 code 与 autelId的值
					 sealerSAP.setCode(sealer.getCode());
					 sealerSAP.setAutelId(sealer.getAutelId());
					 sealerSAP.setName(sealer.getName());
					 
					 listSealer.add(sealerSAP);
				 }
				 map.put("result", listSealer);
				 jsonData = JSONArray.fromObject(map).toString();
			 }
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result","[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}
	
	/**
	 * 给SAP调用的无人机解绑接口(即出厂产品重置)
	 * A16043 hlh 20160526
	 * @return
	 */
	@Action(value = "productresetSAP", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String productresetSAP() throws Exception{
		ProductResetLog log = new ProductResetLog();
		try{  
			ProductInfo info=productInfoService.getBySerialNo(serialNo);
			if(info==null){
				jsonData = "E61|系统没有对应序列号的产品";
			}else if(info.getRegStatus().equals(0)){
				jsonData = "E62|产品没有注册，不能重置";
			}else{
				log.setSerialNo(serialNo);
				log.setResetReason(resetReason);
				log.setResetUser(username);
				log.setResetUserName(username);
				String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
				log.setResetDate(dateStr);
				log.setProCode(info.getCode());
				log.setRegDate(info.getRegTime());
				
				log.setOldUserCode(customerProductService.getCustomerproinfo(info.getCode()));
				List<ProductSoftwareValidStatus> productSoftwareValidStatus=productSoftwareValidStatusService.getProductSoftwareValidStatusList(info.getCode());
				if(productSoftwareValidStatus!=null&& !productSoftwareValidStatus.isEmpty()){
					log.setValidDate(productSoftwareValidStatus.get(0).getValidDate());
				}
			    productResetService.saveLog(log);
				jsonData = "S01|SUCCESS";
			}
		}catch(Exception e){
			jsonData = "E03|系统异常"+e.getMessage();
		}
		
		return SUCCESS;
	}
	
	/**
	 * 给APP调用的显示目前网站支持的国际化语言
	 * A16043 hlh 20160612
	 * @return
	 */
	@Action(value = "queryLanguageIsShow", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryLanguageIsShow() throws Exception{
		Map map = new HashMap();
		try {
			List<Language> languageShowList = null;
			languageShowList = lanService.queryLanguage();
			if(languageShowList != null){
				map.put("result", languageShowList);
			}else{
				map.put("result", "[]");
			}
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	} 
	
	/**
	 * 给APP调用的更改机器所属的AutelID的接口(即更改客户AutelID)
	 * A16043 hlh 20160612
	 * @return
	 */
	@Action(value = "changeCustomerAutelRoboticsID", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String changeCustomerAutelRoboticsID() throws Exception{
		Map map = new HashMap();
		/*通过飞机序列号查询主序列号*/
		try{
			ProductInfo pro = regProductService.queryProductByAricraftSerialNumber(aircraftSerialNumber.trim());
			if(pro == null){
				map.put("code",1);
				map.put("result","找不到该飞机序列号对应的产品！");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
			String SerialNo = pro.getSerialNo();
			CustomerInfo oldCust = customerInfoService.getCustBySerialNo(SerialNo);
			if(null == oldCust){
				map.put("code",2);
				map.put("result","产品不存在或者没有注册！");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
			if(!autelID.equals(oldCust.getAutelId())){
				map.put("code",11);
				map.put("result","飞机不属于 "+autelID+",不能进行更改操作");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
			ProductInfo info=productInfoService.getBySerialNo(SerialNo);
			CustomerInfo newCust=customerInfoService.getCustomerInfoByAutelId(newAutelROboticsId.trim());
			if(null == newCust){
				map.put("code",3);
				map.put("result","New Autel Robotics ID 不存在！");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
			if(newCust.getActState()==0){
				map.put("code",4);
				map.put("result","New Autel Robotics ID 没有激活！");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
		    CustomerChangeAutelIdLog changeAutelIdLog = new CustomerChangeAutelIdLog();
			changeAutelIdLog.setOldCustomerCode(oldCust.getCode());
			changeAutelIdLog.setOldAutelId(oldCust.getAutelId());
			changeAutelIdLog.setNewAutelId(newCust.getAutelId());
			changeAutelIdLog.setNewCustomerCode(newCust.getCode());
			changeAutelIdLog.setProductSn(SerialNo);
			changeAutelIdLog.setOperatorUser(operator.trim());
			changeAutelIdLog.setOperatorDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			changeAutelIdLog.setOperatorIp(FrontConstant.getIpAddr(getRequest()));
			changeAutelIdLog.setProCode(info.getCode());
			changeAutelIdLog.setReason(operatorReason.trim());
			custmerChangeAutelIdService.saveLog(changeAutelIdLog);
			map.put("code",0);
			map.put("result", "success");
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();
			map.put("code",5);
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	} 
	
	/**
	 * 给APP调用的根据飞机序列号获得对应的产品信息
	 * A16043 hlh 20160616  20161202
	 * @return
	 */
	@Action(value = "getProInfoByAirNo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getProInfoByAirNo() throws Exception{
		Map map = new HashMap();
		/*通过飞机序列号查询产品信息*/
		try{
			ProductInfo pro = regProductService.queryProductByAricraftSerialNumber(aircraftSerialNumber.trim());
			if(pro == null){
				map.put("code",1);
				map.put("result","找不到该飞机序列号对应的产品！");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}else{
				map.put("code",0);
				map.put("result",pro);
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
		}catch(Exception e){
			e.printStackTrace();
			map.put("code",2);
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	} 
	
	/**
	 * 给APP调用的根据飞机序列号获得对应的注册状态
	 * A16043 hlh 20161208
	 * @return
	 */
	@Action(value = "getRegStatusByAirNo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getRegStatusByAirNo() throws Exception{
		Map map = new HashMap();
		/*通过飞机序列号查询产品信息*/
		try{
			ProductInfo pro = regProductService.queryProductByAricraftSerialNumber(aircraftSerialNumber.trim());
			if(pro == null){
				map.put("code",3);
				map.put("result","找不到该飞机序列号对应的产品！");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}else if(pro.getRegStatus()==0){  //未注册
				map.put("code",0);
				map.put("result","");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}else if(pro.getRegStatus()==1){ //已注册
				map.put("code",1);
				String email = regProductService.queryRegAutelIDByAricraftSerialNumber(aircraftSerialNumber.trim());
				map.put("result",email);
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}else if(pro.getRegStatus()==2){ // 已解绑
				map.put("code",2);
				String email = regProductService.queryRegAutelIDByAricraftSerialNumber(aircraftSerialNumber.trim());
				map.put("result",email);
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
		}catch(Exception e){
			e.printStackTrace();
			map.put("code",9);
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	} 
	
	/**
	 * 给无人机退换货系统调用的查询经销商换机记录接口
	 * A16043 hlh 20160613
	 * @return
	 */
	@Action(value = "queryProChangeNoLog" , results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryProChangeNoLog(){
		Map map = new HashMap();
		ProductChangeNoLog proChangelog = new ProductChangeNoLog();
		proChangelog.setOldProCode(oldSn);
		proChangelog.setNewProCode(newSn);
		proChangelog.setCustomerCode(autelRoticsID);
		try {
			List<ProductChangeNoLog> list = productChangeNoLogService.queryProChangeNologs(proChangelog);
			if(list != null){
				map.put("result", list);
			}else{
				map.put("result", "[]");
			}
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}
	
	/**
	 * 给无人机退换货系统调用的查询出厂产品重置记录接口
	 * A16043 hlh 20160613
	 * @return
	 */
	@Action(value = "queryProReset" , results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryProReset(){
		Map map = new HashMap();
		ProductResetLog log = new ProductResetLog();
		log.setSerialNo(resetSn);
		List<ProductResetLog> list = null;
		try {
			list = productResetService.getProReset(log);
			if(list != null){
				map.put("result", list);
			}else{
				map.put("result", "[]");
			}
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
		
	}
	
	/**
	 * 给无人机退换货系统调用的查询产品信息的接口
	 * A16043 hlh 20160613
	 * @return
	 */
	@Action(value = "queryProInfo" , results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryProInfo(){
		Map map = new HashMap();
		ProductInfo pro = new ProductInfo();
		pro.setSerialNo(mainSn);
		pro.setAricraftSerialNumber(airSn);
		pro.setImuSerialNumber(imuSn);
		pro.setRemoteControlSerialNumber(remoteSn);
		List<ProductInfo> list = null;
		try {
			list = productInfoService.queryProInfo(pro);
			if(list != null){
				map.put("result", list);
			}else{
				map.put("result", "[]");
			}
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
		
	}
	
	/**
	 * 给无人机退换货系统调用的查询产品及对应的用户信息的接口
	 * A16043 hlh 20160613
	 * @return
	 */
	@Action(value = "queryCustomerProInfo" , results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryCustomerProInfo(){
		Map map = new HashMap();
		CustomerProductVo cusPro = new CustomerProductVo();
		cusPro.setProSerialNo(mainSn);
		cusPro.setAricraftSerialNumber(airSn);;
		cusPro.setImuSerialNumber(imuSn);
		cusPro.setRemoteControlSerialNumber(remoteSn);
		List<CustomerProductVo> list = null;
		try {
			list = customerProductService.queryCustomerProductVo(cusPro);
			if(list != null){
				map.put("result", list);
			}else{
				map.put("result", "[]");
			}
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
		
	}
}
