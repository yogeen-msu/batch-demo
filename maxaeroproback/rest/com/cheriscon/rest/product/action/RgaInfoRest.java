package com.cheriscon.rest.product.action;


import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.member.service.IRGAInfoService;
import com.cheriscon.common.model.RGAInfo;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/rgaInfo")
public class RgaInfoRest extends WWAction {
	
	@Resource
	private IRGAInfoService rgaInfoService;
	
	private String code;
	
	@Action(value="getRGAInfoByCode")
	public String getRGAInfoByCode(){
		try {
			RGAInfo rga= rgaInfoService.getRGAInfoByCode(code);
			renderText(JSONObject.fromObject(rga).toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


}
