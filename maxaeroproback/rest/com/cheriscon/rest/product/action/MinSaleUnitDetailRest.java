package com.cheriscon.rest.product.action;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;

/**
 * 添加
 * @return
 */
@ParentPackage("json-default")
@Namespace("/rest/minSaleUnitDetail")
public class MinSaleUnitDetailRest extends WWAction {

	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;

	private String languageCode;
	private String contractCode;
	private String softwareVersionCode;
	private int type;
	private String minSaleUnitCode;
	private String areaCfgCode;
	private String validDate;
	private String saleCfgCode;
	private String serialNo;
	
	
	@Action(value = "querySoftWareInfoByContract")
	public String querySoftWareInfoByContract(){
		try{
			List<MinSaleUnitSoftwareVO> vos = minSaleUnitDetailService.querySoftWareInfoByContract(languageCode, contractCode, type);
			renderText(JSONArray.fromObject(vos).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 查询软件功能描述
	 * @return
	 */
	@Action(value = "querySoftWareVersionDetail")
	public String querySoftWareVersionDetail(){
		try{
			CustomerInfo customerInfo = new CustomerInfo();
			customerInfo.setLanguageCode(languageCode);
			LanguagePack languagePack = minSaleUnitDetailService.querySoftWareVersionDetail(softwareVersionCode, customerInfo);
			renderText(JSONObject.fromObject(languagePack).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 查询最小销售参与活动的折扣信息
	 * @return
	 */
	@Action(value = "queryDiscount")
	public String queryDiscount(){
		try{
			MarketPromotionDiscountInfoVO vo = minSaleUnitDetailService.queryDiscount(null, minSaleUnitCode,
				areaCfgCode, type, validDate);
		renderText(JSONObject.fromObject(vo).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询销售配置参与活动的折扣信息
	 * @return
	 */
	@Action(value = "querySaleCfgDiscount")
	public String querySaleCfgDiscount(){
		try{
			MarketPromotionDiscountInfoVO vo = minSaleUnitDetailService.querySaleCfgDiscount(null,
					saleCfgCode, areaCfgCode, type, validDate);
		renderText(JSONObject.fromObject(vo).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Action(value = "findSaleConfigDetailInfo")
	public String findSaleConfigDetailInfo(){
		try{
			CfgSoftRentInfoVO vo = minSaleUnitDetailService.findSaleConfigDetailInfo(saleCfgCode, languageCode, serialNo);
			renderText(JSONObject.fromObject(vo).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Action(value = "querySoftWareInfo")
	public String querySoftWareInfo(){
		try{
			List<MinSaleUnitSoftwareVO> vos = minSaleUnitDetailService.querySoftWareInfo(languageCode, saleCfgCode, type);
			renderText(JSONArray.fromObject(vos).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Action(value = "findMinSaleUnitDetailInfo")
	public String findMinSaleUnitDetailInfo(){
		try{
			MinSaleUnitDetailVO vo =minSaleUnitDetailService.findMinSaleUnitDetailInfo(minSaleUnitCode, languageCode, serialNo);
			renderText(JSONObject.fromObject(vo).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getLanguageCode() {
		return languageCode;
	}


	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}


	public String getSoftwareVersionCode() {
		return softwareVersionCode;
	}

	public void setSoftwareVersionCode(String softwareVersionCode) {
		this.softwareVersionCode = softwareVersionCode;
	}

	public String getContractCode() {
		return contractCode;
	}


	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}


	public int getType() {
		return type;
	}


	public void setType(int type) {
		this.type = type;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

	public String getSaleCfgCode() {
		return saleCfgCode;
	}

	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	
}
