package com.cheriscon.rest.product.action;


import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("struts-default")
@Namespace("/rest/productForSealer")
public class ProductForSealerRest extends WWAction {

	@Resource
	private IProductForSealerService productForSealerService;
	
	private List<ProductForSealer> productForSealers;
	
	private String jsonData;
	
	@Action(value = "getProductForSealer908")
	public String getProductForSealer908(){
		try {
			productForSealers = productForSealerService.getProductForSealer908();
//			jsonData = JSONArray.fromObject(productForSealers).toString();
			renderText(JSONArray.fromObject(productForSealers).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Action(value = "getProductForSealer")
	public String getProductForSealer(){
		try {
			List<ProductForSealer> productTypeList=productForSealerService.getProductForSealer();
			renderText(JSONArray.fromObject(productTypeList).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<ProductForSealer> getProductForSealers() {
		return productForSealers;
	}

	public void setProductForSealers(List<ProductForSealer> productForSealers) {
		this.productForSealers = productForSealers;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	
}
