package com.cheriscon.rest.product.action;


import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IBuySoftRentService;
import com.cheriscon.front.usercenter.customer.service.ICfgSoftRentService;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;

@ParentPackage("json-default")
@Namespace("/rest/cfgSoftRend")
public class CfgSoftRendRest extends WWAction {
	
	@Resource
	private ICfgSoftRentService  cfgSoftRentService ;
	@Resource
	private IBuySoftRentService buySoftRentService;
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	
	private String saleContractCode;
	private String proCode;
	private String languageCode;
	private int pageNo;
	
	@Action(value="querySoftRent")
	public String querySoftRent(){
		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setLanguageCode(languageCode);
		try{
			CfgSoftRentInfoVO cfg =cfgSoftRentService.querySoftRent(saleContractCode, proCode, customerInfo);
			renderText(JSONObject.fromObject(cfg).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Action(value="queryBuySoftRent")
	public String queryBuySoftRent(){
		try{
			Page page = buySoftRentService.queryBuySoftRent(saleContractCode, proCode, pageNo, pageSize, languageCode);
			List<MinSaleUnitDetailVO> detailVOs =(List<MinSaleUnitDetailVO>)page.getResult();
			for (int i = 0; i < detailVOs.size(); i++) {
				MinSaleUnitDetailVO minSaleUnit=detailVOs.get(i);
				
				//查询最小销售单位是否可参与活动
				MarketPromotionDiscountInfoVO  info=minSaleUnitDetailService.queryDiscount(null, minSaleUnit.getMinSaleUnitCode(), minSaleUnit.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT, minSaleUnit.getDate());
				String discountPrice=minSaleUnit.getPrice();
				if (info!=null) {
					//折后价
					discountPrice=String.valueOf((Float.parseFloat(minSaleUnit.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
				}
				minSaleUnit.setDiscountPrice(Float.valueOf(discountPrice).toString());
				
				Date date = DateUtil.toDate(minSaleUnit.getDate().trim(), "MM/dd/yyyy");
				Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "MM/dd/yyyy"), "MM/dd/yyyy");
				//当前时间大于有效期时间(已经过期)
				if(currentDate.after(date)){
					minSaleUnit.setIsValid(1);
				}
				
			}
			renderText(JSONObject.fromObject(page).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getSaleContractCode() {
		return saleContractCode;
	}

	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
}
