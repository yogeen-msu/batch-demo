package com.cheriscon.rest.product.action;


import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.customer.service.IProductDateService;

@ParentPackage("json-default")
@Namespace("/rest/productDate")
public class ProductDateRest extends WWAction {

	@Resource
	private IProductDateService productDateService;
	
	private String proCode;
	private String associatesDate;
	private ProductAssociatesDate proDate;
	private String result;

	/**
	 * 查询
	 * @return
	 */
	@Action(value = "getProductDateInfo")
	public String getProductDateInfo(){
		try{
			proDate = productDateService.getProductDateInfo(proCode);
			renderText(JSONObject.fromObject(proDate).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 更新
	 * @return
	 */
	@Action(value = "updateProductDate", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result"}) })
	public String updateProductDate(){
		try{
			productDateService.updateProductDate(associatesDate, proCode);
			result = "success";
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**
	 * 添加
	 * @return
	 */
	@Action(value = "insertProductDate", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result"}) })
	public String insertProductDate(){
		try{
			productDateService.insertProductDate(proDate);
			result = "success";
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getAssociatesDate() {
		return associatesDate;
	}

	public void setAssociatesDate(String associatesDate) {
		this.associatesDate = associatesDate;
	}

	public ProductAssociatesDate getProDate() {
		return proDate;
	}

	public void setProDate(ProductAssociatesDate proDate) {
		this.proDate = proDate;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	
}
