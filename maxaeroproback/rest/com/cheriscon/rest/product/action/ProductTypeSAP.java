package com.cheriscon.rest.product.action;

/**
 * 此类用于产品网站向SAP传递的产品类型信息类
 * @author A16043  20160505
 *
 */
public class ProductTypeSAP {
	    //产品型号code
		private String code;
		//产品型号名称
		private String name;
	    //内部产品名称
		private String innerName;
	
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getInnerName() {
			return innerName;
		}
		public void setInnerName(String innerName) {
			this.innerName = innerName;
		}
		
		
}
