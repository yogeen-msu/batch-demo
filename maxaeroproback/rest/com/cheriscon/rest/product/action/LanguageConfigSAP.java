package com.cheriscon.rest.product.action;

/**
 * 此类用于产品网站向SAP传递的语言配置信息类
 * @author A16043  20160505
 *
 */
public class LanguageConfigSAP {
	//语言配置编号
		private String code;							
		//语言配置名称
		private String name;
		//基础语言名称
		private String baseLanguageNames;
		
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getBaseLanguageNames() {
			return baseLanguageNames;
		}
		public void setBaseLanguageNames(String baseLanguageNames) {
			this.baseLanguageNames = baseLanguageNames;
		}	
		
}
