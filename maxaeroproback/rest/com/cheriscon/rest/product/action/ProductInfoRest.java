package com.cheriscon.rest.product.action;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/productInfo")
public class ProductInfoRest extends WWAction {
	
	private String serialNo;
	private String proCode;
	
	@Resource
	private IProductInfoService productInfoService;
	
	@Action("getBySerialNo")
	public String getBySerialNo(){
		
		ProductInfo productInfo = productInfoService.getAndVaildBySerialNo3(serialNo);
		if(productInfo!=null){
			renderText(JSONObject.fromObject(productInfo).toString());
		}
		return null;
	}
	@Action("queryProductUpgradeRecordList")
	public String queryProductUpgradeRecordList(){
		List<ProductInfo> pros = productInfoService.queryProductUpgradeRecordList(serialNo);
		if(pros!=null){
			renderText(JSONArray.fromObject(pros).toString());
		}
		return null;
	}
	@Action("queryProductRepairRecordList")
	public String queryProductRepairRecordList(){
		List<ProductInfo> pros = productInfoService.queryProductRepairRecordList(proCode);
		if(pros!=null){
			renderText(JSONArray.fromObject(pros).toString());
		}
		return null;
	}
	
	@Action("shouwProductInfoBySerialNo")
	public String shouwProductInfoBySerialNo(){

		ProductInfo productInfo = productInfoService.shouwProductInfoBySerialNo(serialNo);
		if(productInfo!=null){
			renderText(JSONObject.fromObject(productInfo).toString());
		}
		return null;
	}
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}


}
