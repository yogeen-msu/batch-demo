package com.cheriscon.rest.product.action;

/**
 * 此类用于产品网站向SAP传递的契约信息类
 * @author A16043  20160505
 *
 */
public class SaleContractSAP {
	private String code;
	private String name;
	private String sealerCode;
	private String languageCfgCode;
	private String proTypeCode;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSealerCode() {
		return sealerCode;
	}
	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}
	public String getLanguageCfgCode() {
		return languageCfgCode;
	}
	public void setLanguageCfgCode(String languageCfgCode) {
		this.languageCfgCode = languageCfgCode;
	}
	public String getProTypeCode() {
		return proTypeCode;
	}
	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
	
}
