package com.cheriscon.rest.product.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductToolsService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ProductTypeTools;
import com.cheriscon.common.utils.JsonHelper;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/rest/productTypeTools")
public class ProductTypeToolsRest extends WWAction {

	@Resource 
	private ILanguageService languageService;
	@Resource
	private IProductToolsService productToolsService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource 
	private ICustomerProductService customerProductService;
	
	private String languageCode;
	private String autelId;
	private List<ProductTypeTools> productTypeToolsList;
	
	@Action(value = "list", results = {@Result(name = SUCCESS, type = "json", params = {
			"root", "productTypeToolsList"})})
	public String productTypeTools() throws Exception {
		String languageName = languageService.getByCode(languageCode).getName();
		
		List<ProductTypeTools> list = productToolsService.queryProductToolList(languageCode);
		productTypeToolsList = new ArrayList<ProductTypeTools>();
		
		ProductTypeTools productTypeTools = null;
		String toolName = "";
		String productTypeName = "";
		
//		 Object object = (Object)SessionUtil.getLoginUserInfo(request);
		Object object = customerInfoService.getCustomerInfoByAutelId(autelId);
		 
		 String filePath=FrontConstant.getProperValue("download.url.other") ;
		if(object.getClass().equals(CustomerInfo.class)){
			String code=((CustomerInfo)object).getCode();
			String areaCode=FrontConstant.getProperValue("saleconfig.area.china");
			int num = customerProductService.queryProductArea(areaCode,code);
			if(num>0){
			
			filePath=FrontConstant.getProperValue("download.url.china");
			
			}
		}
		
		if(list.size() > 0)
		{
			for(int i = 0 ; i < list.size() ; i++)
			{
				Map<String,String> nameMap = JsonHelper.fromJson(list.get(i).getToolName(),
						new TypeToken<Map<String,String>>(){}.getType());
				
				for (String lanCode : nameMap.keySet())
				{
					if(!languageCode.equals(lanCode))
					{
						continue;
					}
					
					if(!toolName.equals(nameMap.get(lanCode)))
					{
						productTypeTools = new ProductTypeTools();
						productTypeTools.setToolName(nameMap.get(lanCode));
						productTypeName = list.get(i).getProductTypeName();
						productTypeTools.setProductTypeName(productTypeName);
						productTypeTools.setLastVersion(list.get(i).getLastVersion());
						Date date=DateUtil.toDate(list.get(i).getReleaseDate(), "yyyy-MM-dd");
						
						
						productTypeTools.setReleaseDate(DateUtil.toString(date, "MM/dd/yyyy"));
						
						String testPath=list.get(i).getDownloadPath();
						if(testPath!=null){
							testPath=testPath.replace("\\", "/");
						}
						
						productTypeTools.setDownloadPath(filePath+testPath);
						productTypeTools.setLanguageTypeName(languageName);
						productTypeToolsList.add(productTypeTools);
					}
					else
					{
						productTypeName = productTypeName + "、" + list.get(i).getProductTypeName();
						productTypeTools.setProductTypeName(productTypeName);
					}
					
					toolName = nameMap.get(lanCode);
				}
			}
		}
		return SUCCESS;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public List<ProductTypeTools> getProductTypeToolsList() {
		return productTypeToolsList;
	}

	public void setProductTypeToolsList(List<ProductTypeTools> productTypeToolsList) {
		this.productTypeToolsList = productTypeToolsList;
	}
	
	
}
