package com.cheriscon.rest.product.action;

/**
 * 此类用于产品网站向SAP传递的经销商信息
 * @author A16043  20160505
 *
 */
public class SealerInfoSAP {
	private String code; 
	private String autelId;
	private String name;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAutelId() {
		return autelId;
	}
	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
