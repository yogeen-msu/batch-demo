package com.cheriscon.rest.sealer.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ISoftListService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.SoftList;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.vo.SoftListVo;

@ParentPackage("json-default")
@Namespace("/rest/sealer")
public class SealerRest extends WWAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6749318181569192278L;

	@Resource
	private ISealerInfoService sealerService;
	@Resource
	private ISoftListService softListService;
	@Resource
	private ICustomerInfoService customerInfoService;
	
	private String autelId;
	private String password;
	private SealerInfo sealer;
	private SoftListVo softListVo;
	private List<SoftList> softList;
	private CustomerInfo customerInfoEdit;
	private boolean flag;
	private String code;

	@Action(value = "getByAutelId", results = {@Result(name = "success", type = "json", params = {"root", "sealer"})})
	public String getByAutelId() {
		try {
			sealer = sealerService.getSealerByAutelId(autelId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "getByAutelIdAndPwd", results = {@Result(name = "success", type = "json", params = {"root", "sealer"})})
	public String getByAutelIdAndPwd() {
		try {
			sealer = sealerService.getSealerInfoByAutelIdAndPassword(autelId, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	
	@Action(value = "getSoftList", results = {@Result(name = "success", type = "json", params = {"root", "softList"})})
	public String getSofts() {
		softList = softListService.getSoftList(softListVo);
		return "success";
	}
	
	@Action(value = "updateCustPwdForSealer", results = {@Result(name = "success", type = "json", params = {"root", "flag"})})
	public String updateCustPwdForSealer() {
		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(customerInfoEdit.getCode());
			customerInfo.setUserPwd(customerInfoEdit.getUserPwd());
			flag = customerInfoService.updateCustomerByCode(customerInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "getSealerByCode", results = {@Result(name = SUCCESS, type = "json", params = {"root", "sealer"})})
	public String getSealerByCode() {
		try {
			sealer = sealerService.getSealerByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return SUCCESS;
	}
	
	@Action(value = "updateSealer", results = {@Result(name = SUCCESS, type = "json", params = {"root", "sealer"})})
	public String updateSealer() {
		try {
			sealerService.updateSealer(sealer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public SealerInfo getSealer() {
		return sealer;
	}

	public void setSealer(SealerInfo sealer) {
		this.sealer = sealer;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public SoftListVo getSoftListVo() {
		return softListVo;
	}

	public void setSoftListVo(SoftListVo softListVo) {
		this.softListVo = softListVo;
	}

	public List<SoftList> getSoftList() {
		return softList;
	}

	public void setSoftList(List<SoftList> softList) {
		this.softList = softList;
	}

	public CustomerInfo getCustomerInfoEdit() {
		return customerInfoEdit;
	}

	public void setCustomerInfoEdit(CustomerInfo customerInfoEdit) {
		this.customerInfoEdit = customerInfoEdit;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
