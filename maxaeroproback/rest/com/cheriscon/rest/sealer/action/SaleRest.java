package com.cheriscon.rest.sealer.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.content.service.IUserMessageService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.common.model.Message;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.ISaleInfoService;

@ParentPackage("json-default")
@Namespace("/rest/sale")
public class SaleRest extends WWAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6490945522108354364L;

	@Resource
	private ISaleInfoService saleService;
	@Resource 
	private ICustomerComplaintInfoService customerComplaintInfoService;
	@Resource 
	private IUserMessageService userMessageInfoService;
	@Resource
	private IMessageService messageService;
	@Resource
	private IProductTypeService productTypeService;
	
	private String autelId;
	private Integer saleInfoCount;
	private int userType;
	private String userCode;
	private String languageCode;
	private List<Message> messages;
	
	@Action(value = "querySaleInfoCount", results = {@Result(name = "success", type = "json", params = {"root", "saleInfoCount"})})
	public String querySaleInfoCount() {
		try {
			saleInfoCount = saleService.querySaleInfoCount(autelId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "queryCustomerComplaintCount", results = {@Result(name = "success", type = "json", params = {"root", "saleInfoCount"})})
	public String queryCustomerComplaintCount() {
		try {
			saleInfoCount = this.customerComplaintInfoService.queryCustomerComplaintCount(userType, userCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value = "queryUserMessageCount", results = {@Result(name = "success", type = "json", params = {"root", "saleInfoCount"})})
	public String queryUserMessageCount() {
		try {
			saleInfoCount = this.userMessageInfoService.queryUserMessageCount(userType, languageCode, userCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "queryUserMessage", results = {@Result(name = "success", type = "json", params = {"root", "messages"})})
	public String queryUserMessage() {
		try {
			messages = messageService.queryUserMessage(userType, languageCode, userCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "queryProductTypeCount", results = {@Result(name = "success", type = "json", params = {"root", "saleInfoCount"})})
	public String queryProductTypeCount() {
		try {
			saleInfoCount = this.productTypeService.queryProductTypeCount(userCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public Integer getSaleInfoCount() {
		return saleInfoCount;
	}

	public void setSaleInfoCount(Integer saleInfoCount) {
		this.saleInfoCount = saleInfoCount;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	
}
