package com.cheriscon.rest.sealer.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.IReChargeCardErrorLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;
import com.cheriscon.backstage.trade.vo.ReChargeCardForUserVo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.CustomerProRemark;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ReChargeCardErrorLog;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerProRemarkService;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;

@ParentPackage("json-default")
@Namespace("/rest/sealer/softwareRenewal")
public class SoftwareRenewalRest extends WWAction {
	
	@Resource
	private IToCustomerChargeService toCustomerChargeService;
	@Resource
	private ICustomerProRemarkService customerProRemarkService;
	@Resource
	private IReChargeCardErrorLogService logService;
	@Resource
	private IReChargeCardInfoService reChargeCardInfoService;
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IReChargeCardTypeService reChargeCardTypeService;
	@Resource
	private ICustomerInfoService customerInfoService;
	
	private String sealerCode;
	private String serialNo;
	private String expDate;
	private String proTypeCode;
	private String autelId;
	private String code;
	private CustomerProRemark customerProRemark;
	private List<CustomerProRemark> customerProRemarks;
	private Map<String, Object> dataMap;
	private ReChargeCardForUserVo reChargeCardForUserVo;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9204100879898486954L;

	@Action(value = "queryToCustomerChargePage", results = {@Result(name = "success", type = "json", params = {"root", "webpage"})})
	public String queryToCustomerChargePage() {
		try {
			webpage = this.toCustomerChargeService.queryToCustomerChargePage(
					sealerCode, serialNo, expDate, proTypeCode, page, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "getCustomerProRemarkByAutelId", results = {@Result(
			name = SUCCESS, type = "json", params = {"root", "customerProRemark"})})
	public String getCustomerProRemarkByAutelId() {
		customerProRemark = customerProRemarkService.getCustomerProRemarkByAutelId(autelId);
		return SUCCESS;
	}

	@Action(value = "addCustomerProRemark", results = {@Result(name = SUCCESS, type = "json", params = {"root", ""})})
	public String addCustomerProRemark() {
		try {
			customerProRemarkService.addCustomerProRemark(customerProRemark);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "getCustomerRemarkListByProCode", results = {@Result(
			name = SUCCESS, type = "json", params = {"root", "customerProRemarks"})})
	public String getCustomerRemarkListByProCode() {
		customerProRemarks = customerProRemarkService.getCustomerRemarkListByProCode(code);
		return SUCCESS;
	}
	
	@SuppressWarnings("unused")
	@Action(value = "toCharge", results = {@Result(name = SUCCESS, type = "json", params = {"root", "dataMap"})})
	public String toCharge() {
		dataMap = new HashMap<String, Object>();
		
		String reChargeCardSerialNo = reChargeCardForUserVo.getReChargeCardSerialNo();
//		String reChargePwd = reChargeCardForUserVo.getReChargeCardPassword();
		String sealerCode = reChargeCardForUserVo.getSealerCode();
		String ip = reChargeCardForUserVo.getIp();
		ReChargeCardInfo reChargeCardInfo = null;
		
		try {
//			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//			SealerInfo sealer=(SealerInfo)SessionUtil.getLoginUserInfo(request);
			int num = logService.getErrorLogCount(sealerCode);
			if (num >= 5) {
				dataMap.put("result", "8"); // 今日充值错误次数超过五次
				return SUCCESS;
			}

//			String md5Pwd = Md5PwdEncoder.getInstance().encodePassword(reChargePwd,null);
			String md5Pwd = reChargeCardForUserVo.getMd5Pwd();
			reChargeCardInfo = reChargeCardInfoService.getReChargeCardInfoByCardNoAndPwd(reChargeCardSerialNo, md5Pwd);
			
			if (reChargeCardInfo == null) {
				dataMap.put("result", "2"); // 充值卡不存在
				this.saveErrorLog("输入的续租卡密码不存在", "1", sealerCode, ip);
//				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
			// 校验充值卡是否已激活
			else if (reChargeCardInfo.getIsActive() == FrontConstant.CHARGE_CARE_IS_NO_ACTIVE) {
				dataMap.put("result","3"); //充值卡没有激活
				this.saveErrorLog("输入的续租卡没有激活", "1", sealerCode, ip);
//				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			// 校验充值卡是否已使用
			} else if (reChargeCardInfo.getIsUse() == FrontConstant.CARD_SERIAL_IS_YES_USE) {
				dataMap.put("result","4"); //续租卡已经使用
				this.saveErrorLog("输入的续租卡已经被使用", "1", sealerCode, ip);
//				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			} else {
				//1.根据产品序列号获得产品信息，包括产品类型和销售契约编号
				ProductInfo info = productInfoService.getBySerialNo(reChargeCardForUserVo.getProSerialNo());
				//2.根据销售契约编号获取销售契约
				SaleContract proContract = saleContractService.getSaleContractByCode(info.getSaleContractCode());
				//3.根据充值卡类型code查询充值卡类型信息,获取续租卡的销售契约契约和地区
				ReChargeCardType reChargeCardType = reChargeCardTypeService.getReChargeCardTypeByCode(reChargeCardInfo.getReChargeCardTypeCode());
				//4.根据卡类型获取卡类型的销售契约
				SaleContract cardContract = saleContractService.getSaleContractByCode(reChargeCardType.getSaleContractCode());
				if (reChargeCardType == null) {
					dataMap.put("result","9"); //续租卡类型不存在
					this.saveErrorLog("续租卡类型不存在", "0", sealerCode, ip);
//					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				} else if (cardContract.getAreaCfgCode().equals("acf_North_America") && !proContract.getAreaCfgCode().equals("acf_North_America")) {
					dataMap.put("result","5"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
					this.saveErrorLog("美国卡不能在非美国区使用", "0", sealerCode, ip);
//					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				} else if (!cardContract.getAreaCfgCode().equals("acf_North_America") && proContract.getAreaCfgCode().equals("acf_North_America")) {
					dataMap.put("result","7"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
					this.saveErrorLog("非美国卡不能在美国区使用", "0", sealerCode, ip);
//					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				} else if (!proContract.getProTypeCode().equals(cardContract.getProTypeCode())) {
					dataMap.put("result","6"); //充值卡类型和产品型号不匹配
					this.saveErrorLog("充值卡类型和产品型号不匹配", "0", sealerCode, ip);
//					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				} else {
					CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(reChargeCardForUserVo.getMemberName());
					// 充值
					boolean resultResult = toCustomerChargeService.toCharge(reChargeCardInfo, reChargeCardType, customerInfo,
							reChargeCardForUserVo.getProSerialNo());

					if (resultResult == true) {
						dataMap.put("result", "0");
					} else {
						dataMap.put("result", "9");
					}
				}
			} 

		} catch (Exception e) {
			dataMap.put("result", "9");
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	private void saveErrorLog(String remark, String type, String userCode, String ip){
		ReChargeCardErrorLog log = new ReChargeCardErrorLog();
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		log.setCreateTime(date);
//		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
//		String ip = FrontConstant.getIpAddr(request);
		log.setCustomerCode(userCode);
		log.setIp(ip);
		log.setStatus(0);
		log.setType(type);
		log.setRemark(remark);
		try {
			logService.saveErrorLog(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public CustomerProRemark getCustomerProRemark() {
		return customerProRemark;
	}

	public void setCustomerProRemark(CustomerProRemark customerProRemark) {
		this.customerProRemark = customerProRemark;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<CustomerProRemark> getCustomerProRemarks() {
		return customerProRemarks;
	}

	public void setCustomerProRemarks(List<CustomerProRemark> customerProRemarks) {
		this.customerProRemarks = customerProRemarks;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	public ReChargeCardForUserVo getReChargeCardForUserVo() {
		return reChargeCardForUserVo;
	}

	public void setReChargeCardForUserVo(ReChargeCardForUserVo reChargeCardForUserVo) {
		this.reChargeCardForUserVo = reChargeCardForUserVo;
	}

}
