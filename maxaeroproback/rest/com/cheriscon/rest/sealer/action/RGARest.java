package com.cheriscon.rest.sealer.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.IRGAInfoService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.RGAInfo;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.vo.RGAInfoVo;

@ParentPackage("json-default")
@Namespace("/rest/sealer/rga")
public class RGARest extends WWAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3002741400152630481L;

	@Resource
	private IRGAInfoService rgaInfoService;
	@Resource
	private ICustomerInfoService customerInfoService;
	
	private RGAInfoVo rgaInfoVo;
	private RGAInfo rgaInfo;
	private CustomerInfo customerInfo;
	private String serialNo;
	
	@Action(value = "pageRGAQuery", results = {@Result(name = "success", type = "json", params = {"root", "webpage"})})
	public String pageRGAQuery() {
		try {
			webpage = rgaInfoService.pageRGAQuery(rgaInfoVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value = "updateRGAInfo", results = {@Result(name = "success", type = "json", params = {"root", ""})})
	public String updateRGAInfo() {
		try {
			rgaInfoService.updateRGAInfo(rgaInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "saveRGAInfo", results = {@Result(name = SUCCESS, type = "json", params = {"root", ""})})
	public String saveRGAInfo() {
		try {
			rgaInfoService.saveRGAInfo(rgaInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "getCustBySerialNo3", results = {@Result(name = SUCCESS, type = "json", params = {"root", "customerInfo"})})
	public String getCustBySerialNo3() {
		try {
			customerInfo = customerInfoService.getCustBySerialNo3(serialNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public RGAInfoVo getRgaInfoVo() {
		return rgaInfoVo;
	}

	public void setRgaInfoVo(RGAInfoVo rgaInfoVo) {
		this.rgaInfoVo = rgaInfoVo;
	}

	public RGAInfo getRgaInfo() {
		return rgaInfo;
	}

	public void setRgaInfo(RGAInfo rgaInfo) {
		this.rgaInfo = rgaInfo;
	}

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

}
