package com.cheriscon.rest.sealer.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.distributor.service.IChangeLanguageService;

@ParentPackage("json-default")
@Namespace("/rest/sealer/proRegPwd")
public class ProRegPwdRest extends WWAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1381047099814602636L;
	
	@Resource
	private IChangeLanguageService changeLanguageService;
	
	private String sealerCode;
	private String serialNo;
	private String proTypeCode;

	@Action(value = "querySealerProductPage", results = {@Result(name = "success", type = "json", params = {"root", "webpage"})})
	public String querySealerProductPage() {
		try {
			webpage = changeLanguageService.querySealerProductPage(sealerCode, serialNo, proTypeCode, page, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

}
