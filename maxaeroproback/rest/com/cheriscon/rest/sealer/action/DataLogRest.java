package com.cheriscon.rest.sealer.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.system.service.IDataLogService;
import com.cheriscon.common.model.DataLogging;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/sealer/dataLog")
public class DataLogRest extends WWAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -863361462273080201L;

	@Resource
	private IDataLogService dataLogService;
	
	private String productSN;
	private List<DataLogging> dataLogs;
	
	@Action(value = "getDataLogList", results = {@Result(name = "success", type = "json", params = {"root", "dataLogs"})})
	public String getDataLogList() {
		try {
			dataLogs = dataLogService.getDataLogList(productSN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getProductSN() {
		return productSN;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	public List<DataLogging> getDataLogs() {
		return dataLogs;
	}

	public void setDataLogs(List<DataLogging> dataLogs) {
		this.dataLogs = dataLogs;
	}

}
