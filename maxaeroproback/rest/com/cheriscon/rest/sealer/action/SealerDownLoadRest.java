package com.cheriscon.rest.sealer.action;

import java.util.List;
import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.system.service.ISealerDownloadService;
import com.cheriscon.common.model.SealerDownload;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/rest/sealerDownload")
public class SealerDownLoadRest extends WWAction {
	
	@Resource
	public ISealerDownloadService sealerDownloadService;
	public List<SealerDownload> sealerDownloadList;
	private String fileType;
	
	@Action(value = "files", results = {@Result(name = "success", type = "json", params = {"root", "sealerDownloadList"})})
	public String files(){
		sealerDownloadList = sealerDownloadService.getSealerDownloadFiles();
		return SUCCESS;
	}
	
	@Action(value = "list", results = {@Result(name = "success", type = "json", params = {"root", "sealerDownloadList"})})
	public String list(){
		sealerDownloadList = sealerDownloadService.getSealerDownloadList(fileType);
		return SUCCESS;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
