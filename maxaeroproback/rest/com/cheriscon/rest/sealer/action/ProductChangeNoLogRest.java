package com.cheriscon.rest.sealer.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.system.service.IProductChangeNoLogService;
import com.cheriscon.common.model.ProductChangeNoLog;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/sealer/productChangeNoLog")
public class ProductChangeNoLogRest extends WWAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4789548075796552994L;
	
	@Resource
	private IProductChangeNoLogService productChangeNoLogService;
	
	private ProductChangeNoLog log;

	@Action(value = "saveLog", results = {@Result(name = SUCCESS, type = "json", params = {"root", ""})})
	public String saveLog() {
		try {
			this.productChangeNoLogService.saveLog(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public ProductChangeNoLog getLog() {
		return log;
	}

	public void setLog(ProductChangeNoLog log) {
		this.log = log;
	}
	
}
