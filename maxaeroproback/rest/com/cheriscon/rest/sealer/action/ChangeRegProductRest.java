package com.cheriscon.rest.sealer.action;


import java.util.Date;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.system.service.IProductChangeNoLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.common.model.ProductChangeNoLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.customer.service.IProductDateService;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;

@ParentPackage("json-default")
@Namespace("/rest/sealer/regProduct")
public class ChangeRegProductRest extends WWAction {
	
	/**
	 * rest.sealer.regProduct.changeRegProduct
	 */
	private static final long serialVersionUID = 1L;
	
	@Resource
	private IRegProductService regProductService;
	@Resource
	private IProductChangeNoLogService productChangeNoLogService;
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private IProductDateService productDateService;
	@Resource
	private IRegProductService regProductInfoService;
	
	private ProductChangeNoLog log;
	private RegProductAcountInfoVO regProductAcountInfoVo;
	private String proSerialNo;
	private ProductInfo productInfo;
	private String proDate;
	private String regResult;
	
	@Action(value = "queryProductAndSealer", results = {@Result(name = SUCCESS, type = "json", params = {"root", "productInfo"})})
	public String queryProductAndSealer() {
		productInfo = regProductInfoService.queryProductAndSealer(proSerialNo);
		return SUCCESS;
	}
	
	@Action(value = "changeRegProduct", results = {@Result(name = SUCCESS, type = "json", params = {"root", "regResult"})})
	public String changeRegProduct() {
		/**开始执行更换操作
		 * 1.将产品的信息写入中间表，包括原用户code，有效期等
		 * ***/
//		ProductChangeNoLog log=new ProductChangeNoLog();
//		Object obj=SessionUtil.getLoginUserInfo(request);
//		String createUser="";
//		if(obj.getClass().equals(CustomerInfo.class)){
//			createUser=((CustomerInfo)obj).getCode();
//		}
//		if(obj.getClass().equals(SealerInfo.class)){
//			createUser=((SealerInfo)obj).getCode();
//		}
//		log.setCreateUser(createUser);
//		log.setCreateIp(request.getRemoteAddr());
//		log.setCreateDate(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
//		log.setCustomerCode(customerCode);
//		log.setOldProCode(oldProduct.getCode());
//		log.setNewProCode(newProduct.getCode());
//		log.setValidDate(oldProduct.getValidDate()); //有效期
//		log.setRegDate(oldProduct.getRegTime()); //注册日期
//			//先将原来的关系记录日志后删除
			try {
				productChangeNoLogService.saveLog(log);
			
//			regProductInfoVO.setProCode(newProduct.getCode());
//			regProductInfoVO.setProRegPwd(newProduct.getRegPwd());
//			regProductInfoVO.setProSerialNo(newProduct.getSerialNo());
//			regProductInfoVO.setProTypeCode(newProduct.getProTypeCode());
				boolean result = regProductService.addProductSoftwareValidStatus(log.getCustomerCode(), log.getValidDate(), log.getRegDate(), proDate, regProductAcountInfoVo);	
	//			
				ProductAssociatesDate proDate = productDateService.getProductDateInfo(log.getNewProCode());
				if (proDate == null) {
					proDate = new ProductAssociatesDate();
					proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
					proDate.setProCode(log.getNewProCode());
					result = productDateService.insertProductDate(proDate);
				} else {
					result = productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), log.getNewProCode());
				}
				if (result) {
					regResult = "SUCCESS";
//					regResult = "{\"regResult\":\"SUCCESS\"}";
				} else {
					regResult = "FAIL";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		return SUCCESS;
	}

	public ProductChangeNoLog getLog() {
		return log;
	}

	public void setLog(ProductChangeNoLog log) {
		this.log = log;
	}

	public RegProductAcountInfoVO getRegProductAcountInfoVo() {
		return regProductAcountInfoVo;
	}

	public void setRegProductAcountInfoVo(
			RegProductAcountInfoVO regProductAcountInfoVo) {
		this.regProductAcountInfoVo = regProductAcountInfoVo;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public String getProDate() {
		return proDate;
	}

	public void setProDate(String proDate) {
		this.proDate = proDate;
	}

	public String getRegResult() {
		return regResult;
	}

	public void setRegResult(String regResult) {
		this.regResult = regResult;
	}

}
