package com.cheriscon.rest.sealer.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.framework.action.WWAction;


@ParentPackage("json-default")
@Namespace("/rest/sealer/regProduct")
public class CustomerRegProductRest extends WWAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2293015474001755409L;
	
	@Resource
	private ICustomerProductService customerProductServiceImpl;
	
	private CustomerProductVo customerProductVo;
	

	@Action(value = "pageCustomerProductVoPage", results = {@Result(name = "success", type = "json", params = {"root", "webpage"})})
	public String pageCustomerProductVoPage() {
		try {
			webpage = this.customerProductServiceImpl.pageCustomerProductVoPage(customerProductVo, 1, 15);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}


	public CustomerProductVo getCustomerProductVo() {
		return customerProductVo;
	}

	public void setCustomerProductVo(CustomerProductVo customerProductVo) {
		this.customerProductVo = customerProductVo;
	}
	
	
	
}
