package com.cheriscon.rest.sealer.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ISalesQueryInfoService;
import com.cheriscon.common.model.SalesQueryInfo;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.distributor.vo.SalesQueryInfoVo;

@ParentPackage("json-default")
@Namespace("/rest/sealer/preSalesSupport")
public class PreSalesSupportRest extends WWAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3098485355064944859L;
	
	@Resource
	private ISalesQueryInfoService salesQueryInfoService;
	
	private SalesQueryInfoVo salesQueryinfoVo;
	private SalesQueryInfo salesQueryInfo;
	private String code;

	@Action(value = "pageSalesQuery", results = {@Result(name = SUCCESS, type = "json", params = {"root", "webpage"})})
	public String pageSalesQuery() {
		try {
			webpage = salesQueryInfoService.pageSalesQuery(salesQueryinfoVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "getSalesQueryInfoByCode", results = {@Result(name = SUCCESS, type = "json", params = {"root", "salesQueryInfo"})})
	public String getSalesQueryInfoByCode() {
		try {
			salesQueryInfo = salesQueryInfoService.getSalesQueryInfoByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "updateSalesQuery", results = {@Result(name = SUCCESS, type = "json", params = {"root", "salesQueryInfo"})})
	public String updateSalesQuery() {
		try {
			salesQueryInfoService.updateSalesQuery(salesQueryInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "saveSalesQuery", results = {@Result(name = SUCCESS, type = "json", params = {"root", ""})})
	public String saveSalesQuery() {
		try {
			salesQueryInfoService.saveSalesQuery(salesQueryInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public SalesQueryInfoVo getSalesQueryinfoVo() {
		return salesQueryinfoVo;
	}

	public void setSalesQueryinfoVo(SalesQueryInfoVo salesQueryinfoVo) {
		this.salesQueryinfoVo = salesQueryinfoVo;
	}

	public SalesQueryInfo getSalesQueryInfo() {
		return salesQueryInfo;
	}

	public void setSalesQueryInfo(SalesQueryInfo salesQueryInfo) {
		this.salesQueryInfo = salesQueryInfo;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
