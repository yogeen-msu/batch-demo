package com.cheriscon.rest.sealer.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.ofn.model.OldForNew;
import com.cheriscon.ofn.model.vo.OldForNewVo;
import com.cheriscon.ofn.service.IOldForNewSerice;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/rest/sealer/ofn")
public class OfnRest extends WWAction {
	
	@Resource
	private IOldForNewSerice oldForNewSerice;
	@Resource
	private ISealerDataAuthDeailsService sealerDataAuthDeailsService;
	@Resource
	private ISealerInfoService sealerInfoService;
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IEmailTemplateService emailTemplateService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ILanguageService languageService;
	
	private int id;
	private OldForNew oldForNew;
	private OldForNewVo oldForNewVo;
	private String sealerCode;
	private String authCode;
	private boolean result;
	
	@Action(value = "page", results = {@Result(name = SUCCESS, type = "json", params = {"root", "webpage"})})
	public String page() {
		String sealerCode = oldForNewVo.getSealerCode();
		try {
			SealerDataAuthDeails sealerDataAuthDeails = sealerDataAuthDeailsService.getDataAuthBySealerCode(oldForNewVo.getAutelId());
//			authCode 含有，表示属于某个经销商下的账号
			if (sealerDataAuthDeails != null) {
				String autelId = sealerDataAuthDeails.getSealerCode();
				if (sealerDataAuthDeails.getAuthCode().startsWith("0063")) {
					autelId = "C-0063";
				} else if (sealerDataAuthDeails.getAuthCode().indexOf(",") > 0) {
					sealerDataAuthDeails = sealerDataAuthDeailsService.getDataAuth(
							sealerDataAuthDeails.getAuthCode().substring(0, sealerDataAuthDeails.getAuthCode().indexOf(",")));
					autelId = sealerDataAuthDeails.getSealerCode();
				}
				SealerInfo tem = sealerInfoService.getSealerByAutelId(autelId);
				if (tem != null) {
					sealerCode = tem.getCode();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		oldForNewVo.setSealerCode(sealerCode);
		webpage =  oldForNewSerice.page(oldForNewVo, oldForNewVo.getPageNo(), oldForNewVo.getPageSize());
		return SUCCESS;
	}
	
	@Action(value = "get", results = {@Result(name = SUCCESS, type = "json", params = {"root", "oldForNew"})})
	public String get() {
		oldForNew = oldForNewSerice.get(id);
		return SUCCESS;
	}
	
	@Action(value = "getSealerInfoByAuthCode", results = {@Result(name = "success", type = "json", params = {"root", "sealerCode"})})
	public String getSealerInfoByAuthCode() {
		try {
			if (authCode != null && authCode.indexOf(",") > 0) {
				authCode = authCode.substring(0, authCode.indexOf(","));
			}
			SealerDataAuthDeails sealerDataAuthDeails = sealerDataAuthDeailsService.getDataAuth(authCode);
			if (sealerDataAuthDeails != null) {
				SealerInfo tsi = sealerInfoService.getSealerByAutelId(sealerDataAuthDeails.getSealerCode());
				sealerCode = tsi.getCode();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	@Action(value = "updateOldForNew", results = {@Result(name = SUCCESS, type = "json", params = {"root", "result"})})
	public String updateOldForNew() {
		Integer state = oldForNew.getState();
		oldForNewSerice.updateOldForNew(oldForNew);
//		result = true;
		if (state == 4 || state == 3) {
//			发送邮件
			try {
				Smtp smtp = smtpManager.getCurrentSmtp( );
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username", smtp.getUsername()); //username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("autelId", oldForNew.getAutelId());
				
				CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(oldForNew.getAutelId());
				String requestUrl = FreeMarkerPaser.getBundleValue("autelpro.url");
				String oldForNewUrl = requestUrl + "/oldForNewDetail.html?proSerialNo=" + oldForNew.getSerialNo();
				String userName = "";
				if (customerInfo != null && customerInfo.getName() != null) {
					userName=customerInfo.getName();
				}
				
				emailModel.getData().put("oldForNewUrl", oldForNewUrl);
				emailModel.getData().put("userName", userName);
				emailModel.getData().put("oldForNew", oldForNew);
				emailModel.getData().put("state", state);
				Language language = languageService.getByCode(customerInfo.getLanguageCode());
				
				if (language != null) {
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("ofn.mailtitle",
							language.getLanguageCode(), language.getCountryCode()));
				}
				else {
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("ofn.mailtitle"));
				}
				
				emailModel.setTo(customerInfo.getAutelId());
				EmailTemplate template = emailTemplateService.getUseTemplateByTypeAndLanguage(
						CTConsatnt.EMAIL_TEMPLATE_TYEP_OLD_FOR_DISPATCH, customerInfo.getLanguageCode());
				
				if (template != null) {
					emailModel.setTemplate(template.getCode() + ".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return SUCCESS;
	}
	
	public OldForNewVo getOldForNewVo() {
		return oldForNewVo;
	}

	public void setOldForNewVo(OldForNewVo oldForNewVo) {
		this.oldForNewVo = oldForNewVo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public OldForNew getOldForNew() {
		return oldForNew;
	}

	public void setOldForNew(OldForNew oldForNew) {
		this.oldForNew = oldForNew;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public boolean getResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}
