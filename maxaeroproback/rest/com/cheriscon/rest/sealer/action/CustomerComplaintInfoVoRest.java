package com.cheriscon.rest.sealer.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.member.service.IComplaintAdminuserInfoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.ComplaintAdminuserInfo;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.vo.HangUpVo;
import com.cheriscon.front.usercenter.distributor.vo.ReplyCustomerComplaintVo;

@ParentPackage("json-default")
@Namespace("/rest/sealer/complaint")
public class CustomerComplaintInfoVoRest extends WWAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8139423602475242839L;
	
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private ICustomerComplaintInfoVoService customerComplaintInfoVoService;
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;
	@Resource
	private IComplaintAdminuserInfoService complaintAdminuserInfoService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private IEmailTemplateService emailTemplateService;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IReCustomerComplaintInfoService reCustomerComplaintInfoService;
	@Resource
	private ISealerInfoService sealerInfoService;
	
	
	private String serialNo;
	private ProductInfo productInfo;
	private Map<String, Object> dataMap;
	private CustomerComplaintInfo customerComplaintInfo;
	private ReplyCustomerComplaintVo replyCustomerComplaintVo;
	
	private String authCode;
	private int userType;
	private int timeRange;
	private int complaintState; 
	private String userCode;
	private HangUpVo hangUpVo;
	private String code;
	private String acceptor;
	

	public CustomerComplaintInfoVo customerComplaintInfoVo;
	public List<CustomerComplaintInfoVo> customerComplaintInfoVos;
	
	@Action(value = "pageCustomerComplaintInfoPage", results = {
			@Result(name = "success", type = "json", params = {"root", "customerComplaintInfoVos"})})
	public String pageCustomerComplaintInfoPage() {
		try {
			customerComplaintInfoVos = this.customerComplaintInfoVoService.pageCustomerComplaintInfoPage(customerComplaintInfoVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "getBySerialNo2", results = {@Result(name = SUCCESS, type = "json", params = {"root", "productInfo"})})
	public String getBySerialNo2() {
		productInfo = productInfoService.getBySerialNo2(serialNo);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	@Action(value = "addCustomerComplaintInfo", results = {@Result(name = SUCCESS, type = "json", params = {"root", "dataMap"})})
	public String addCustomerComplaintInfo() {

		try {	
			ComplaintAdminuserInfo adminUser = complaintAdminuserInfoService.getComplaintAdminuserInfoBySealer(customerComplaintInfo.getAutelId());
			if (adminUser != null) {
				customerComplaintInfo.setComplaintTypeCode(adminUser.getComplaintTypeCode());
			}
			customerComplaintInfoService.addCustomerComplaintInfo(customerComplaintInfo);

			String autelId = "";
			String languageCode = "";
			String userName="";
			
			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(customerComplaintInfo.getUserCode());
			autelId = customerInfo.getAutelId();
			languageCode = customerInfo.getLanguageCode();
			if(customerInfo.getName()!=null){
				userName=customerInfo.getName();
			}

			Language lanuage = languageService.getByCode(languageCode);
			String status = FrontConstant.getComplaintStatus(
					lanuage.getCountryCode(),
					FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE);

			Smtp smtp = smtpManager.getCurrentSmtp();

			EmailModel emailModel = new EmailModel();
			emailModel.getData().put("username", smtp.getUsername());// username
																		// 为freemarker模版中的参数
			emailModel.getData().put("password", smtp.getPassword());
			emailModel.getData().put("host", smtp.getHost());

			
			emailModel.getData().put("userName", userName);
			emailModel.getData().put("autelId", autelId);
			emailModel.getData().put("ticketId", customerComplaintInfo.getCode());
			emailModel.getData().put("subject", customerComplaintInfo.getComplaintTitle());

			String requestUrl = FreeMarkerPaser.getBundleValue("autelpro.url");
			String url = requestUrl
					+ "/queryCustomerComplaintDetail.html?operationType=3&userType="
					+ customerComplaintInfo.getFrontUserType() + "&code="
					+ customerComplaintInfo.getCode();
			emailModel.getData().put("url", url);

			emailModel.getData().put("status", status);

			emailModel.setTitle("[SUPPORT #" + customerComplaintInfo.getCode() + "]: "
					+ customerComplaintInfo.getComplaintTitle());
			emailModel.setTo(customerComplaintInfo.getEmail());  //将发送邮件的地址改为客诉填写的地址

			EmailTemplate template = emailTemplateService
					.getUseTemplateByTypeAndLanguage(
							CTConsatnt.EMAIL_TEMPLATE_TYEP_ADD_COMPLAINT_SUCCESS,
							languageCode);
			if (template != null)
			{
				emailModel.setTemplate(template.getCode() + ".html"); // 此处一定需要这样写
			}
			emailProducer.send(emailModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "queryCustomerComplaintInfoPage2", results = {@Result(name = SUCCESS, type = "json", params = {"root", "webpage"})})
	public String queryCustomerComplaintInfoPage2() {
		try {
			webpage = customerComplaintInfoService.queryCustomerComplaintInfoPage2(
					authCode, userType, timeRange, complaintState, userCode, page, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "hangUpComplaint", results = {@Result(name = "success", type = "json", params = {"root", ""})})
	public String hangUpComplaint() {
		
		CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();

		customerComplaintInfo.setCode(hangUpVo.getCode());
		customerComplaintInfo.setComplaintState(Integer.valueOf(hangUpVo.getComplaintState()));

		ReCustomerComplaintInfo reCustomerComplaintInfoAdd = new ReCustomerComplaintInfo();

		reCustomerComplaintInfoAdd.setReContent(hangUpVo.getHangUpReason());

		try	{
			customerComplaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);

			if (customerComplaintInfo.getComplaintState().equals(new Integer(2))
					&& !reCustomerComplaintInfoAdd.getReContent().trim().equals("")) {
				String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
				reCustomerComplaintInfoAdd.setCustomerComplaintCode(customerComplaintInfo.getCode());
				reCustomerComplaintInfoAdd.setReDate(dateStr);
				
				reCustomerComplaintInfoAdd.setRePerson(hangUpVo.getAutelId());
				
				reCustomerComplaintInfoAdd.setRePersonType(2);
				reCustomerComplaintInfoService.addReCustomerComplaintInfo(reCustomerComplaintInfoAdd);
			}

			try
			{
				// 发邮件////////////////////////////////////////////////////////
				Smtp smtp = smtpManager.getCurrentSmtp();
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username", smtp.getUsername());// username
																			// 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				String reContent = reCustomerComplaintInfoAdd.getReContent();
				customerComplaintInfo = customerComplaintInfoService
						.getCustomerComplaintInfoByCode(customerComplaintInfo.getCode());
				String code = customerComplaintInfo.getCode();
				String email = customerComplaintInfo.getEmail();
				String languageCode = "";
				if (customerComplaintInfo.getUserType() == FrontConstant.CUSTOMER_USER_TYPE) {
					languageCode = customerInfoService.getCustomerByCode(
							customerComplaintInfo.getUserCode()).getLanguageCode();
				} else if (customerComplaintInfo.getUserType() == FrontConstant.DISTRIBUTOR_USER_TYPE) {
					languageCode = sealerInfoService.getSealerByCode(
							customerComplaintInfo.getUserCode()).getLanguageCode();
				}
				String userType = customerComplaintInfo.getUserType().toString();
				
				String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");;
				String customerComplaintUrl = requestUrl+ "/queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType;

				Language lanuage = languageService.getByCode(languageCode);
				String status = FrontConstant.getComplaintStatus(lanuage.getCountryCode(),
						customerComplaintInfo.getComplaintState());
				CustomerInfo info = customerInfoService.getCustomerByCode(customerComplaintInfo.getUserCode());
				String userName = "";
				if (info != null && info.getName() != null) {
				  userName = info.getName();
				}
				
				emailModel.getData().put("userName", userName);
				emailModel.getData().put("autelId", email);
				emailModel.getData().put("reContent", reContent);
				emailModel.getData().put("customerComplaintCode", code);
				emailModel.getData().put("subject", customerComplaintInfo.getComplaintTitle());
				emailModel.getData().put("status", status);
				//emailModel.getData().put("rePerson", user.getUsername());
				emailModel.getData().put("rePerson", hangUpVo.getAutelId());
				emailModel.getData().put("url", customerComplaintUrl);
				emailModel.setTitle("[SUPPORT #" + code + "]: "
						+ customerComplaintInfo.getComplaintTitle());

				emailModel.setTo(email);

				EmailTemplate template = emailTemplateService
						.getUseTemplateByTypeAndLanguage(
								CTConsatnt.EMAIL_TEMPLATE_TYEP_REPLY_COMPLAINT_SUCCESS, languageCode);

				emailModel.setTemplate(template.getCode() + ".html"); // 此处一定需要这样写
				emailProducer.send(emailModel);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "allotAcceptor", results = {@Result(name = SUCCESS, type = "json", params = {"root", ""})})
	public String allotAcceptor() {
		try {
			customerComplaintInfoService.allotAcceptor(code, acceptor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "replyCustomerComplaintByDistributor", results = {@Result(name = SUCCESS, type = "json", params = {"root", ""})})
	public String replyCustomerComplaintByDistributor() {
//		String filePath = fileUpLoad(params);
//
//		// 上传文件大小超过4M或上传文件失败
//		if (filePath.equals(CUSTOMERCOMPLAINT_FILE_TYPE)
//				|| filePath.equals(CUSTOMERCOMPLAINT_FILE_SIZE)
//				|| filePath.equals(CUSTOMERCOMPLAINT_FAIL))
//		{
//			this.showJson(filePath);
//		} else
//		{
			ReCustomerComplaintInfo reCustomerComplaintInfo = new ReCustomerComplaintInfo();
			reCustomerComplaintInfo.setCustomerComplaintCode(replyCustomerComplaintVo.getCusComCode());
			reCustomerComplaintInfo.setRePersonType(2);
			reCustomerComplaintInfo.setReContent(replyCustomerComplaintVo.getReContent());
			reCustomerComplaintInfo.setReDate(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
//
//			// 没有上传附件处理
//			if (filePath.equals(CUSTOMERCOMPLAINT_FILE_TYPE)
//					|| filePath.equals(CUSTOMERCOMPLAINT_FILE_SIZE)
//					|| filePath.equals(CUSTOMERCOMPLAINT_FAIL)
//					|| filePath.equals(CUSTOMERCOMPLAINT_SUCCESS))
//			{
//				filePath = null;
//			}
//
			reCustomerComplaintInfo.setAttachment(replyCustomerComplaintVo.getFilePath());
			reCustomerComplaintInfo.setRePerson(replyCustomerComplaintVo.getAutelId());
//
//			String userType = params.get("userType");
			CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();
			customerComplaintInfo.setLastRePerson(replyCustomerComplaintVo.getAutelId());
//			if (userType != null)
//			{
//				if (Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)
//				{
//					reCustomerComplaintInfo
//							.setRePerson(((CustomerInfo) SessionUtil
//									.getLoginUserInfo(request)).getAutelId());
//					customerComplaintInfo.setLastRePerson(((CustomerInfo) SessionUtil
//							.getLoginUserInfo(request)).getAutelId());
//				} else if (Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)
//				{
//					reCustomerComplaintInfo
//							.setRePerson(((SealerInfo) SessionUtil
//									.getLoginUserInfo(request)).getAutelId());
//					customerComplaintInfo.setLastRePerson(((SealerInfo) SessionUtil
//							.getLoginUserInfo(request)).getAutelId());
//				}
//			}
//
			customerComplaintInfo.setCode(replyCustomerComplaintVo.getCusComCode());
//			customerComplaintInfo.setCode(params.get("cusComCode"));
			customerComplaintInfo.setComplaintState(FrontConstant.CUSTOMER_COMPLAINT_WAIT_STATE);
			customerComplaintInfo.setLastReDate(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
//			
			try {
				customerComplaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
				reCustomerComplaintInfoService.addReCustomerComplaintInfo(reCustomerComplaintInfo);
				//add send email to customer.
//				sendEmailToCustomer(request, params.get("reContent"));
				sendEmailToCustomer(replyCustomerComplaintVo);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
//		}
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	private void sendEmailToCustomer(ReplyCustomerComplaintVo replyCustomerComplaintVo)
	{
//		String code = request.getParameter("code");
		String code = replyCustomerComplaintVo.getCode();
		String autelId = replyCustomerComplaintVo.getFrontAutelId();
//		String autelId = request.getParameter("autelId");
		
		ReCustomerComplaintInfo reCustomerComplaintInfoAdd = new ReCustomerComplaintInfo();

		reCustomerComplaintInfoAdd.setReContent(replyCustomerComplaintVo.getReContent());
		
		CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();

		customerComplaintInfo.setCode(code);
		
		try {
			//发邮件////////////////////////////////////////////////////////
			
			/*Smtp smtp = smtpManager.getCurrentSmtp( );*/
			EmailModel emailModel = new EmailModel();
			
			String reContent = reCustomerComplaintInfoAdd.getReContent();
			customerComplaintInfo = customerComplaintInfoService.getCustomerComplaintInfoByCode(code);
			emailModel.setType(customerComplaintInfo.getAcceptor());
			
			String email = customerComplaintInfo.getEmail();
			String languageCode = "";
			if (customerComplaintInfo.getUserType() == FrontConstant.CUSTOMER_USER_TYPE) {
				languageCode = customerInfoService.getCustomerByCode(customerComplaintInfo.getUserCode()).getLanguageCode();
			}
			else if (customerComplaintInfo.getUserType() == FrontConstant.DISTRIBUTOR_USER_TYPE) {
				languageCode = sealerInfoService.getSealerByCode(customerComplaintInfo.getUserCode()).getLanguageCode();
			}
			String userType = customerComplaintInfo.getUserType().toString();
			
			String requestUrl = FreeMarkerPaser.getBundleValue("autelpro.url");;
			String customerComplaintUrl = requestUrl+ "/queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType;
			Language lanuage = languageService.getByCode(languageCode);
			String status = FrontConstant.getComplaintStatus(lanuage.getCountryCode(), customerComplaintInfo.getComplaintState());
			
			CustomerInfo info = customerInfoService.getCustomerByCode(customerComplaintInfo.getUserCode());
			String userName = "";
			if (info != null && info.getName() != null) {
				userName = info.getName();
			}
			
			emailModel.getData().put("autelId", email);
			emailModel.getData().put("userName", userName);
			emailModel.getData().put("reContent", reContent);
			emailModel.getData().put("customerComplaintCode", code);
			emailModel.getData().put("subject", customerComplaintInfo.getComplaintTitle());
			emailModel.getData().put("status", status);
			emailModel.getData().put("rePerson", autelId);
			emailModel.getData().put("url", customerComplaintUrl);
			emailModel.setTitle("[SUPPORT #"+code+"]: "+customerComplaintInfo.getComplaintTitle());
			
			emailModel.setTo(email);
			
			
			EmailTemplate template = emailTemplateService.getUseTemplateByTypeAndLanguage(
					CTConsatnt.EMAIL_TEMPLATE_TYEP_REPLY_COMPLAINT_SUCCESS, languageCode);
			
			emailModel.setTemplate(template.getCode() + ".html");		//此处一定需要这样写
			emailProducer.send(emailModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public CustomerComplaintInfoVo getCustomerComplaintInfoVo() {
		return customerComplaintInfoVo;
	}

	public void setCustomerComplaintInfoVo(
			CustomerComplaintInfoVo customerComplaintInfoVo) {
		this.customerComplaintInfoVo = customerComplaintInfoVo;
	}

	public List<CustomerComplaintInfoVo> getCustomerComplaintInfoVos() {
		return customerComplaintInfoVos;
	}

	public void setCustomerComplaintInfoVos(
			List<CustomerComplaintInfoVo> customerComplaintInfoVos) {
		this.customerComplaintInfoVos = customerComplaintInfoVos;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	public CustomerComplaintInfo getCustomerComplaintInfo() {
		return customerComplaintInfo;
	}

	public void setCustomerComplaintInfo(CustomerComplaintInfo customerComplaintInfo) {
		this.customerComplaintInfo = customerComplaintInfo;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public int getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(int timeRange) {
		this.timeRange = timeRange;
	}

	public int getComplaintState() {
		return complaintState;
	}

	public void setComplaintState(int complaintState) {
		this.complaintState = complaintState;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public HangUpVo getHangUpVo() {
		return hangUpVo;
	}

	public void setHangUpVo(HangUpVo hangUpVo) {
		this.hangUpVo = hangUpVo;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAcceptor() {
		return acceptor;
	}

	public void setAcceptor(String acceptor) {
		this.acceptor = acceptor;
	}

	public ReplyCustomerComplaintVo getReplyCustomerComplaintVo() {
		return replyCustomerComplaintVo;
	}

	public void setReplyCustomerComplaintVo(ReplyCustomerComplaintVo replyCustomerComplaintVo) {
		this.replyCustomerComplaintVo = replyCustomerComplaintVo;
	}

}
