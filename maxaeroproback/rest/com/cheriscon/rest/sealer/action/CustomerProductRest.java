package com.cheriscon.rest.sealer.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/customerProduct")
public class CustomerProductRest extends WWAction {
	
	@Resource
	private ICustomerProductService customerProductService;
	
	private String serialNo;
	private CustomerProductVo customerProductVo;

	/**
	 * 
	 */
	private static final long serialVersionUID = 56366375227153765L;
	
	@Action(value = "getCustomerPro", results = {@Result(name = "success", type = "json", params = {"root", "customerProductVo"})})
	public String customerProduct() {
		try {
			customerProductVo = customerProductService.getCustomerPro(serialNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public CustomerProductVo getCustomerProductVo() {
		return customerProductVo;
	}

	public void setCustomerProductVo(CustomerProductVo customerProductVo) {
		this.customerProductVo = customerProductVo;
	}

}
