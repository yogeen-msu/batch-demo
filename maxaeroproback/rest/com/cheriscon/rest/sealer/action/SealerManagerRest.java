package com.cheriscon.rest.sealer.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

@ParentPackage("json-default")
@Namespace("/rest/sealer/manager")
public class SealerManagerRest extends WWAction {
	
	@Resource
	private ISealerDataAuthDeailsService sealerDataAuthDeailsService;
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;
	@Resource
	private ISealerAuthService sealerAuthService;
	@Resource
	private ISealerInfoService sealerInfoService;
	
	private String sealerName;
	private String authCode;
	private String sealerCode;
	
	private Integer userType;
	private Integer timeRange;
	private Integer complaintState;
	private String userCode;
	private String autelId;
	private String sealerAuthDesc;
	private List<SealerDataAuthDeails> sealerDataAuthDeailsList;
	private SealerInfo sealer;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1227461983154469200L;

	@Action(value = "getAuthSealer", results = {@Result(name = "success", type = "json", params = {"root", "webpage"})})
	public String getAuthSealer() {
		try {
			webpage = this.sealerDataAuthDeailsService.getAuthSealer(sealerName, authCode, sealerCode, page, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "queryCustomerComplaintInfoPage3", results = {@Result(
			name = "success", type = "json", params = {"root", "webpage"})}) 
	public String queryCustomerComplaintInfoPage3() {
		try {
			webpage = customerComplaintInfoService.queryCustomerComplaintInfoPage3(
					userType, timeRange, complaintState, userCode, page, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value = "getAuthSealerList", results = {@Result(
			name = "success", type = "json", params = {"root", "sealerDataAuthDeailsList"})})
	public String getAuthSealerList() {
		try {
			sealerDataAuthDeailsList = sealerDataAuthDeailsService.getAuthSealerList(authCode, sealerCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "saveAuthDetail", results = {@Result(name = SUCCESS, type = "json", params = {"root", ""})})
	public String saveAuthDetail() {
		try {
			sealerAuthService.saveAuthDetail(autelId, sealerAuthDesc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "insertSealer2", results = {@Result(name = SUCCESS, type = "json", params = {"root", ""})})
	public String insertSealer2() {
		sealer.setCode(DBUtils.generateCode(DBConstant.SEALERINFO_CODE));
		String[] addressAndAutelId = sealer.getAddress().split("_frontSealerInfoAutelId_");
		sealer.setAddress(addressAndAutelId[0]);
		try {
			sealerInfoService.insertSealer2(sealer);
			//插入左边导航权限
			sealerInfoService.insertSealerAuth("queryCustomer", sealer.getCode());
			sealerInfoService.insertSealerAuth("sealerComplaint", sealer.getCode());
			sealerInfoService.insertSealerAuth("updateSealer", sealer.getCode());
			sealerInfoService.insertSealerAuth("salesList", sealer.getCode());
			sealerInfoService.insertSealerAuth("rgamanagement", sealer.getCode());
			
			//插入数据角色权限
			SealerDataAuthDeails auth = new SealerDataAuthDeails();
			if ("C-0063".equals(addressAndAutelId[1])) {
				auth.setAuthCode("0063,0001");
			}
			if ("C-0759".equals(addressAndAutelId[1])) {
				auth.setAuthCode("0064,0001");
			}
			auth.setSealerCode(sealer.getAutelId());
			sealerDataAuthDeailsService.insertAuth(auth);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String getSealerName() {
		return sealerName;
	}

	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Integer getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(Integer timeRange) {
		this.timeRange = timeRange;
	}

	public Integer getComplaintState() {
		return complaintState;
	}

	public void setComplaintState(Integer complaintState) {
		this.complaintState = complaintState;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public List<SealerDataAuthDeails> getSealerDataAuthDeailsList() {
		return sealerDataAuthDeailsList;
	}

	public void setSealerDataAuthDeailsList(List<SealerDataAuthDeails> sealerDataAuthDeailsList) {
		this.sealerDataAuthDeailsList = sealerDataAuthDeailsList;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getSealerAuthDesc() {
		return sealerAuthDesc;
	}

	public void setSealerAuthDesc(String sealerAuthDesc) {
		this.sealerAuthDesc = sealerAuthDesc;
	}

	public SealerInfo getSealer() {
		return sealer;
	}

	public void setSealer(SealerInfo sealer) {
		this.sealer = sealer;
	}

}
