package com.cheriscon.rest.sealer.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;

@ParentPackage("json-default")
@Namespace("/rest/sealerAuth")
public class SealerAuthRest extends WWAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5331250604603737061L;

	@Resource
	private ISealerAuthService sealerAuthService;
	
	private String sealerCode;
	private List<SealerAuthVO> sealerAuths;
	
	@Action(value = "list", results = {@Result(name = "success", type = "json", params = {"root", "sealerAuths"})})
	public String list() {
		sealerAuths = this.sealerAuthService.list(sealerCode);
		return SUCCESS;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public List<SealerAuthVO> getSealerAuths() {
		return sealerAuths;
	}

	public void setSealerAuths(List<SealerAuthVO> sealerAuths) {
		this.sealerAuths = sealerAuths;
	}

}
