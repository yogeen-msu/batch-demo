package com.cheriscon.rest.user.action;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.IQuestionDescService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.QuestionDesc;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.user.service.ICustomerInfoService;

@ParentPackage("json-default")
@Namespace("/rest/question")
public class QuestionRest extends WWAction {

	@Resource
	private IQuestionDescService questionDescService;
	@Resource
	private ICustomerInfoService customerInfoService;
	
	private List<QuestionDesc> questionDescs;
	private String languageCode;
	private String autelId;
	
	@Action(value = "listQuestionDescByLanguageCode", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "questionDescs"}) })
	public String listQuestionDescByLanguageCode(){
		try {
			questionDescs = questionDescService.listQuestionDescByLanguageCode(languageCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	/**
	 * 获取安全问题
	 * @return
	 */
	@Action(value = "getQuestionDescByAutelId")
	public String getQuestionDescByAutelId(){
		String res = "";
		try {
			CustomerInfo customer = customerInfoService.getCustomerInfoByAutelId(autelId);
			if(customer!=null){
				QuestionDesc desc = questionDescService.getQuestionDesc(customer.getQuestionCode(), customer.getLanguageCode());
				if(desc!=null){
					res = JSONObject.fromObject(desc).toString();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		renderText(res);
		return null;
	}
	public List<QuestionDesc> getQuestionDescs() {
		return questionDescs;
	}

	public void setQuestionDescs(List<QuestionDesc> questionDescs) {
		this.questionDescs = questionDescs;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}
	
	

}
