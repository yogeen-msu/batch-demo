package com.cheriscon.rest.user.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ISdkAppDownloadService;
import com.cheriscon.backstage.member.service.ISdkAppService;
import com.cheriscon.backstage.member.vo.SdkAppDownloadVO;
import com.cheriscon.backstage.member.vo.SdkAppVO;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/sdkapp")
public class SdkAppRest extends WWAction {

	private static final long serialVersionUID = 1L;
	/**
	 * sdk app参数对象
	 */
	private SdkAppVO sdkAppVO;

	/**
	 * sdk app查询返回对象
	 */
	private SdkAppVO returnSdkAppVO;

	/**
	 * sdk app查询返回集合
	 */
	private List<SdkAppVO> sdkAppVOList;

	/**
	 * sdk app download查询返回对象
	 */
	private SdkAppDownloadVO sdkAppDownloadVO;

	/**
	 * sdk app download查询返回集合
	 */
	private List<SdkAppDownloadVO> sdkAppDownloadVOList;

	private boolean result;// 返回结果

	@Resource
	private ISdkAppService sdkAppService;

	@Resource
	private ISdkAppDownloadService sdkAppDownloadService;

	/**
	 * 查詢sdk app集合
	 * 
	 * @return String
	 */
	@Action(value = "queryAppList", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "sdkAppVOList" }) })
	public String queryAppList() {
		sdkAppVOList = sdkAppService.querySdkAppList(sdkAppVO);
		return SUCCESS;
	}

	/**
	 * 查詢sdk app详情
	 * 
	 * @return String
	 */
	@Action(value = "getSdkAppDetails", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "returnSdkAppVO" }) })
	public String getSdkAppDetails() {
		returnSdkAppVO = sdkAppService.getSdkAppDetails(sdkAppVO);
		return SUCCESS;
	}

	/**
	 * 添加sdk app
	 * 
	 * @return String
	 */
	@Action(value = "addSdkAppList", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result" }) })
	public String addSdkAppList() {
		result = sdkAppService.addSdkAppList(sdkAppVO);
		return SUCCESS;
	}

	/**
	 * 修改sdk app
	 * 
	 * @return String
	 */
	@Action(value = "updateSdkAppList", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result" }) })
	public String updateSdkAppList() {
		result = sdkAppService.updateSdkAppList(sdkAppVO);
		return SUCCESS;
	}

	/**
	 * 删除sdk app
	 * 
	 * @return String
	 */
	@Action(value = "deleteSdkAppList", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result" }) })
	public String deleteSdkAppList() {
		result = sdkAppService.deleteSdkAppList(sdkAppVO);
		return SUCCESS;
	}

	/**
	 * 查詢sdk app download信息
	 * 
	 * @return list
	 */
	@Action(value = "querySdkAppDownloadsInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "sdkAppDownloadVOList" }) })
	public String querySdkAppDownloadsInfo() {
		sdkAppDownloadVOList = sdkAppDownloadService.querySdkAppDownloadsInfo();
		return SUCCESS;
	}

	/**
	 * 查詢查询用户sdk app协议详情
	 * 
	 * @return String
	 */
	@Action(value = "getCustomerAgreementDetails", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "returnSdkAppVO" }) })
	public String getCustomerAgreementDetails() {
		returnSdkAppVO = sdkAppService.getCustomerAgreementDetails(sdkAppVO);
		return SUCCESS;
	}

	/**
	 * 用户同意sdk app协议详情
	 * 
	 * @return String
	 */
	@Action(value = "saveCustomerAgreementDetails", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result" }) })
	public String saveCustomerAgreementDetails() {
		result = sdkAppService.saveCustomerAgreementDetails(sdkAppVO);
		return SUCCESS;
	}

	public SdkAppVO getSdkAppVO() {
		return sdkAppVO;
	}

	public void setSdkAppVO(SdkAppVO sdkAppVO) {
		this.sdkAppVO = sdkAppVO;
	}

	public SdkAppVO getReturnSdkAppVO() {
		return returnSdkAppVO;
	}

	public void setReturnSdkAppVO(SdkAppVO returnSdkAppVO) {
		this.returnSdkAppVO = returnSdkAppVO;
	}

	public SdkAppDownloadVO getSdkAppDownloadVO() {
		return sdkAppDownloadVO;
	}

	public void setSdkAppDownloadVO(SdkAppDownloadVO sdkAppDownloadVO) {
		this.sdkAppDownloadVO = sdkAppDownloadVO;
	}

	public List<SdkAppDownloadVO> getSdkAppDownloadVOList() {
		return sdkAppDownloadVOList;
	}

	public void setSdkAppDownloadVOList(
			List<SdkAppDownloadVO> sdkAppDownloadVOList) {
		this.sdkAppDownloadVOList = sdkAppDownloadVOList;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public ISdkAppService getSdkAppService() {
		return sdkAppService;
	}

	public void setSdkAppService(ISdkAppService sdkAppService) {
		this.sdkAppService = sdkAppService;
	}

	public ISdkAppDownloadService getSdkAppDownloadService() {
		return sdkAppDownloadService;
	}

	public void setSdkAppDownloadService(
			ISdkAppDownloadService sdkAppDownloadService) {
		this.sdkAppDownloadService = sdkAppDownloadService;
	}

	public List<SdkAppVO> getSdkAppVOList() {
		return sdkAppVOList;
	}

	public void setSdkAppVOList(List<SdkAppVO> sdkAppVOList) {
		this.sdkAppVOList = sdkAppVOList;
	}

}
