package com.cheriscon.rest.user.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.content.service.IMessageTypeService;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/rest/messageType")
public class MessageTypeRest extends WWAction {
	
	@Resource
	private IMessageTypeService messageTypeService;
			
	private List<MessageType> messageTypeList;
	private String userType;
	private String timeRange;
	private String languageCode;
	private String creatTime;
	private List<MessageType> messageTypes;
	
	@Action(value = "list", results = {@Result(name = "success", type="json", params = {
			"root", "messageTypeList"
	})})
	public String list() {
		try {
			creatTime = creatTime.replace(",", " ");
			messageTypeList = messageTypeService.queryMessageType(
					Integer.parseInt(userType), Integer.parseInt(timeRange), 
					languageCode, creatTime);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "queryMessageType", results = {@Result(name = SUCCESS, type = "json", params = {
			"root", "messageTypes"
	})})
	public String queryMessageType() {
		messageTypes = messageTypeService.queryMessageType();
		return SUCCESS;
	}
	
	public List<MessageType> getMessageTypeList() {
		return messageTypeList;
	}

	public void setMessageTypeList(List<MessageType> messageTypeList) {
		this.messageTypeList = messageTypeList;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}

	public List<MessageType> getMessageTypes() {
		return messageTypes;
	}

	public void setMessageTypes(List<MessageType> messageTypes) {
		this.messageTypes = messageTypes;
	}

	
}
