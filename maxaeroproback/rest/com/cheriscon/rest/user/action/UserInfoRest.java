package com.cheriscon.rest.user.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.service.IReChargeCardErrorLogService;
import com.cheriscon.backstage.member.service.IReChargeCardSupportService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;
import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ReChargeCardErrorLog;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.model.ReChargeCardSupport;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.IChangeAutelService;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.user.vo.ReChargeCardVo;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;

@ParentPackage("json-default")
@Namespace("/rest/user")
public class UserInfoRest extends WWAction{
	
	@Resource
	private IMyAccountService myAccountService;
	@Resource
	private ICustomerProductService customerProductService;
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private ISystemErrorLogService systemErrorService;
	
	@Resource
	private IReChargeCardErrorLogService logService;
	@Resource
	private IToCustomerChargeService toCustomerChargeService;
	@Resource
	private IProductContractLogService productContractService;
	@Resource
	private IReChargeCardTypeService reChargeCardTypeService;
	@Resource
	private IReChargeCardInfoService reChargeCardInfoService;
	@Resource
	private IReChargeCardSupportService supportService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private IChangeAutelService changeService;
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private IEmailTemplateService emailTemplateService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IReCustomerComplaintInfoService reCustomerComplaintInfoService;
	@Resource 
	private IChangeAutelService changeAutelService;
	
	private String jsonData;
	private String customerInfoCode;
	private String areaCode;
	private String serialNo;
	private ReChargeCardVo reChargeCardVo;
	private String autelId;
	private ReChargeCardSupport support;
	private CustomerInfo customerInfoEdit;
	private String customerId;
	private String oldPassword;
	private String password;
	private ReCustomerComplaintInfo reCustomerComplaintInfo;
	private Map<String, Object> dataMap = new HashMap<String, Object>();
	/**
	 * 产品列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "queryRenewByCode", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryRenewByCode() throws Exception{
//		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);
		try{
//		List<MyProduct> list=myAccountService.queryRenewByCode(customerInfo.getCode());
		List<MyProduct> list=myAccountService.queryRenewByCode(customerInfoCode);
		renderText(JSONArray.fromObject(list).toString());
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 根据用户和地区查询产品
	 * @return
	 */
	@Action(value = "queryProductArea")
	public String queryProductArea(){
		int count =0;
		try {
			count = customerProductService.queryProductArea(areaCode, customerInfoCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		renderText(count+"");
		return null;
	}

	@Action(value = "getProductCardType", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getProductCardType() throws Exception{
		ProductInfo info=productInfoService.getBySerialNo(serialNo);
		Map<String, String> map = new HashMap<String, String>();
		if(info!=null){
			
			String ds708_code=this.getPropertValue("product.type.maxidas");
			String usa_area_code=this.getPropertValue("saleconfig.area.usa");
			
			SaleContract proContract=saleContractService.getSaleContractByCode(info.getSaleContractCode());
			if(info.getProTypeCode().equals(ds708_code) && proContract.getAreaCfgCode().equals(usa_area_code)){
				map.put("result","1"); //区域为美国并且产品为708才提示使用美国卡
			}else{
				map.put("result","0");
			}
		}else{
			map.put("result","9");
		}
		jsonData=JSONArray.fromObject(map).toString();
		renderText(jsonData);
		return null;
	}
	
	/**
	 * 客户升级卡操作
	 * 
	 * @param request
	 * @throws Exception
	 */
	
	@SuppressWarnings("unused")
	@Action(value = "reChargeCardOperation", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String reChargeCardOperation() throws Exception{
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		Map<String, String> map = new HashMap<String, String>();
		String reChargeCardSerialNo = reChargeCardVo.getReChargeCardSerialNo();
		String reChargePwd = reChargeCardVo.getReChargeCardPassword()==null?"":reChargeCardVo.getReChargeCardPassword().trim().toUpperCase();
		ReChargeCardInfo reChargeCardInfo = null;

		String proType=this.getPropertValue("product.type.maxidas");
		
		
		try {
			if(customerInfoCode==null){
				map.put("result", "11"); // 登录失效
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
			
			int num=logService.getErrorLogCount(customerInfoCode);
			if(num>=5){
				map.put("result", "8"); // 今日充值错误次数超过五次
				//this.saveErrorLog("输入的续租卡密码不存在","1");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
            
			String md5Pwd=Md5PwdEncoder.getInstance().encodePassword(reChargePwd,null);
//			String md5Pwd="eee5e42375df3d8d313c17fa69191d4e";
			reChargeCardInfo = reChargeCardInfoService.getReChargeCardInfoByCardNoAndPwd(reChargeCardSerialNo,md5Pwd);
			
			if (reChargeCardInfo == null)
			{
				map.put("result", "2"); // 充值卡不存在
				this.saveErrorLog("输入的续租卡密码不存在","1",customerInfoCode);
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			} else if (reChargeCardInfo.getIsActive() == FrontConstant.CHARGE_CARE_IS_NO_ACTIVE)// 校验充值卡是否已激活
			{
				map.put("result","3"); //充值卡没有激活
				this.saveErrorLog("输入的续租卡没有激活","1",customerInfoCode);
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			} else if (reChargeCardInfo.getIsUse() == FrontConstant.CARD_SERIAL_IS_YES_USE)// 校验充值卡是否已使用
			{
				map.put("result","4"); //续租卡已经使用
				this.saveErrorLog("输入的续租卡已经被使用","1",customerInfoCode);
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}else{
//				设置临时登录用户信息
				CustomerInfo customerInfo = new CustomerInfo();
				customerInfo.setCode(customerInfoCode);
				customerInfo.setAutelId(autelId);
				SessionUtil.setLoginUserInfo(request, customerInfo);
				
				//1.根据产品序列号获得产品信息，包括产品类型和销售契约编号
				ProductInfo info=productInfoService.getBySerialNo(reChargeCardVo.getProSerialNo());
				//2.根据销售契约编号获取销售契约
				SaleContract proContract=saleContractService.getSaleContractByCode(info.getSaleContractCode());
				//3.根据充值卡类型code查询充值卡类型信息,获取续租卡的销售契约契约和地区
				ReChargeCardType reChargeCardType = reChargeCardTypeService.getReChargeCardTypeByCode(reChargeCardInfo.getReChargeCardTypeCode());
				//4.根据卡类型获取卡类型的销售契约
				SaleContract cardContract=saleContractService.getSaleContractByCode(reChargeCardType.getSaleContractCode());
				if(reChargeCardType==null){
					map.put("result","9"); //续租卡类型不存在
					this.saveErrorLog("续租卡类型不存在","0",customerInfoCode);
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				} else if(!proContract.getProTypeCode().equals(cardContract.getProTypeCode())){
					map.put("result","6"); //充值卡类型和产品型号不匹配
					this.saveErrorLog("充值卡类型和产品型号不匹配","0",customerInfoCode);
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}else if(info.getProTypeCode().equals(proType) &&  cardContract.getAreaCfgCode().equals("acf_North_America") && !proContract.getAreaCfgCode().equals("acf_North_America")){
					if(proContract.getAreaCfgCode().equals("acf_AutelDefault")){ 
						//如果产品的销售契约地区是“AutelDefault”，则修改它的销售契约为美国
						SaleContract temp=new SaleContract();
						temp.setAreaCfgCode(cardContract.getAreaCfgCode());  //美国区域
						temp.setLanguageCfgCode(proContract.getLanguageCfgCode()); //产品语言
						temp.setSaleCfgCode(proContract.getSaleCfgCode()); //产品销售配置
						temp.setSealerCode(cardContract.getSealerCode());  //美国经销商
						temp.setProTypeCode(cardContract.getProTypeCode()); //升级卡类型
						
						List<SaleContract> list=saleContractService.getContrctList(temp);
						if(list== null || list.size()==0){
							map.put("result","2"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
							this.saveErrorLog("AutelDefault对应的销售配置在美国区域找不到匹配","0",customerInfoCode);
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}else{
							SaleContract con=list.get(0);
							
							String ip=FrontConstant.getIpAddr(request);
							ProductContractChangeLog log=new ProductContractChangeLog();
							log.setNewContract(con.getCode());
							log.setOldContract(info.getSaleContractCode());
							log.setOldMinSaleUnit("使用升级卡更换契约");
							log.setOperatorIp(ip);
							log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
							log.setOperatorUser(customerInfoCode);
							log.setProductSN(info.getSerialNo());
							//1.保存日志
							productContractService.saveLog(log);
							
							info.setSaleContractCode(con.getCode());
							productInfoService.updateProductInfo(info);  //更新该产品的销售契约
							
						
							
							
							// 充值
							boolean resultResult = toCustomerChargeService.toCharge(
									reChargeCardInfo, reChargeCardType, customerInfo,
									reChargeCardVo.getProSerialNo());

							if (resultResult == true) {
								map.put("result", "0");
							} else {
								map.put("result", "9");
							}
						}
						
						
					}else{
						map.put("result","5"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
						this.saveErrorLog("美国卡不能在非美国区使用","0",customerInfoCode);
						jsonData = JSONArray.fromObject(map).toString();
						return SUCCESS;
					}
				}else if(!cardContract.getAreaCfgCode().equals("acf_North_America") && proContract.getAreaCfgCode().equals("acf_North_America")){
					map.put("result","7"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
					this.saveErrorLog("非美国卡不能在美国区使用","0",customerInfoCode);
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}
				else{
				//	CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(reChargeCardVo.getMemberName());
					// 充值
					boolean resultResult = toCustomerChargeService.toCharge(
							reChargeCardInfo, reChargeCardType, customerInfo,
							reChargeCardVo.getProSerialNo());

					if (resultResult == true) {
						map.put("result", "0");
					} else {
						map.put("result", "9");
					}
				}
			} 

		} catch (Exception ex) {
			map.put("result", "9");
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.front.user.action.UserInfoAction".equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
				System.out.println(errorMsg);
				systemErrorService.saveLog("升级卡",errorMsg);
			}
			}
			
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	@Action(value = "cardSupport")
	public String cardSupport() throws Exception{
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		support.setCreateTime(date);
		support.setStatus(0);
		try{
			supportService.saveCardSupport(support);
			renderText("success");
		}catch(Exception e){
			renderText("fail");
		}
		return null;
	}
	
	
	private void saveErrorLog(String remark,String type,String userCode){
		ReChargeCardErrorLog log=new ReChargeCardErrorLog();
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		log.setCreateTime(date);
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		String ip=FrontConstant.getIpAddr(request);
		log.setCustomerCode(userCode);
		log.setIp(ip);
		log.setStatus(0);
		log.setType(type);
		log.setRemark(remark);
		try {
			logService.saveErrorLog(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 用户个人信息修改New
	 * 
	 * @param request
	 * @author pengdongan
	 */
	@Action(value = "updateCustomerInfoNew", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustomerInfoNew() {
		Map<String, String> map = new HashMap<String, String>();

		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(customerInfoEdit.getCode());

			if (customerInfo != null) {
				
				//修改论坛名字
				if (!StringUtil.isEmpty(customerInfoEdit.getComUsername())) {
					customerInfoService.updateBbsUsername(customerInfoEdit.getCode(), customerInfoEdit.getComUsername());
				}

				
				//获得新的autelId值
				String autelIdNew = customerInfoEdit.getAutelId();

				//获得新的secondEmail值
				String secondEmailNew = customerInfoEdit.getSecondEmail();
				
				if (!StringUtil.isEmpty(autelIdNew)) {
					// 判断第一邮箱是否重复
					if(!customerInfo.getAutelId().equals(autelIdNew)){
						if (changeService.getCustomerInfoNumByIdAndSrouce(autelIdNew) != 0) {
							map.put("result", "3");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}else{
							updateAutelIdNew(customerInfo.getAutelId(),autelIdNew);
						}
					}
	
				}

				if (!StringUtil.isEmpty(secondEmailNew)) {
					// 判断第二邮箱是否重复
					if(customerInfo.getSecondEmail()==null){
						if (customerInfoService.getCustomerInfoListBySecondEmail(secondEmailNew).size() > 0) {
							map.put("result", "3");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}
					}else if(!customerInfo.getSecondEmail().equals(secondEmailNew)){
						if (customerInfoService.getCustomerInfoListBySecondEmail(secondEmailNew).size() > 0) {
							map.put("result", "3");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}
					}
	
				}

				if(!StringUtil.isEmpty(customerInfoEdit.getUserPwd())){
					customerInfoEdit.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(customerInfoEdit.getUserPwd(),null));
				}
				
				customerInfoEdit.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));

				if(!StringUtil.isEmpty(customerInfoEdit.getFirstName())){
					customerInfoEdit.setName(customerInfoEdit.getFirstName()
//								+ customerInfoEdit.getMiddleName()
							    +" " //应东升（美国）要求，页面显示的姓与名 之间要用  空格 隔开，故 用 空格 隔开 firstName 与 lastName
								+ customerInfoEdit.getLastName());
				}
				
				//autelId值不在原表中直接修改，要先激活
				customerInfoEdit.setAutelId(customerInfo.getAutelId());
				
				//更新用户信息表
				customerInfoService.updateCustomerByCode(customerInfoEdit);

				//填充customerInfoEdit对象 用于发邮件的相关值
				if (StringUtil.isEmpty(customerInfoEdit.getLanguageCode())){
					customerInfoEdit.setLanguageCode(customerInfo.getLanguageCode());
				}
				
				if (StringUtil.isEmpty(customerInfoEdit.getName())){
					customerInfoEdit.setName(customerInfo.getName());
				}
				
				
				//发邮件////////////////////////////
				
				if (!StringUtil.isEmpty(autelIdNew)) {
					// 设置个人信息时只有当第一邮箱变动时才发送邮件
					if (!customerInfo.getAutelId().equals(autelIdNew)) {
						sendAutelIDEmailForChange(customerInfoEdit,autelIdNew);
					}
				}

				if (!StringUtil.isEmpty(secondEmailNew)) {
					// 设置个人信息时只有只有当第一次输入第二邮箱以及第二邮箱变动时才发送邮件
					if (customerInfo.getSecondEmail()!=null) {
						if (!customerInfo.getSecondEmail().equals(secondEmailNew)) {
							sendSecondEmail(customerInfoEdit);
						}
					}else{
						sendSecondEmail(customerInfoEdit);
					}
				}

				map.put("result", "1");
			}
		} catch (Exception e) {
			map.put("result", "2");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;

	}
	
	
	/**20170110 A16043 hlh  提供给无人机APP调用的新的修改个人用户信息的接口
	 * 用户个人信息修改New
	 * 
	 * @param request
	 * @author pengdongan
	 */
	@Action(value = "updateCustomerInfoNewUAV", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustomerInfoNewUAV() {
		Map<String, String> map = new HashMap<String, String>();
		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(customerInfoEdit.getCode());
			if (customerInfo != null) {
				//修改论坛名字
				if (!StringUtil.isEmpty(customerInfoEdit.getComUsername())) {
					customerInfoService.updateBbsUsername(customerInfoEdit.getCode(), customerInfoEdit.getComUsername());
				}
				//获得新的autelId值
				String autelIdNew = customerInfoEdit.getAutelId();
				//获得新的secondEmail值
				String secondEmailNew = customerInfoEdit.getSecondEmail();
				
				if (!StringUtil.isEmpty(autelIdNew)) {
					// 判断第一邮箱是否重复
					if(!customerInfo.getAutelId().equals(autelIdNew)){
						if (changeService.getCustomerInfoNumByIdAndSrouce(autelIdNew) != 0) {
							//map.put("result", "3");
							map.put("code", "3");
							map.put("result", "用户第一邮箱(AutelID)已经被使用");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}else{
							updateAutelIdNew(customerInfo.getAutelId(),autelIdNew);
						}
					}
	
				}

				if (!StringUtil.isEmpty(secondEmailNew)) {
					// 判断第二邮箱是否重复
					if(customerInfo.getSecondEmail()==null){
						if (customerInfoService.getCustomerInfoListBySecondEmail(secondEmailNew).size() > 0) {
							//map.put("result", "3");
							map.put("code", "32");
							map.put("result", "用户第二邮箱已经被使用");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}
					}else if(!customerInfo.getSecondEmail().equals(secondEmailNew)){
						if (customerInfoService.getCustomerInfoListBySecondEmail(secondEmailNew).size() > 0) {
							//map.put("result", "3");
							map.put("code", "32");
							map.put("result", "用户第二邮箱已经被使用");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}
					}
	
				}

				if(!StringUtil.isEmpty(customerInfoEdit.getUserPwd())){
					customerInfoEdit.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(customerInfoEdit.getUserPwd(),null));
				}
				
				customerInfoEdit.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));

				if(!StringUtil.isEmpty(customerInfoEdit.getFirstName())){
					customerInfoEdit.setName(customerInfoEdit.getFirstName()
//								+ customerInfoEdit.getMiddleName()
							    +" " //应东升（美国）要求，页面显示的姓与名 之间要用  空格 隔开，故 用 空格 隔开 firstName 与 lastName
								+ customerInfoEdit.getLastName());
				}
				
				//autelId值不在原表中直接修改，要先激活
				customerInfoEdit.setAutelId(customerInfo.getAutelId());
				
				//更新用户信息表
				customerInfoService.updateCustomerByCode(customerInfoEdit);

				//填充customerInfoEdit对象 用于发邮件的相关值
				if (StringUtil.isEmpty(customerInfoEdit.getLanguageCode())){
					customerInfoEdit.setLanguageCode(customerInfo.getLanguageCode());
				}
				
				if (StringUtil.isEmpty(customerInfoEdit.getName())){
					customerInfoEdit.setName(customerInfo.getName());
				}
				
				
				//发邮件////////////////////////////
				
				if (!StringUtil.isEmpty(autelIdNew)) {
					// 设置个人信息时只有当第一邮箱变动时才发送邮件
					if (!customerInfo.getAutelId().equals(autelIdNew)) {
						sendAutelIDEmailForChange(customerInfoEdit,autelIdNew);
					}
				}

				if (!StringUtil.isEmpty(secondEmailNew)) {
					// 设置个人信息时只有只有当第一次输入第二邮箱以及第二邮箱变动时才发送邮件
					if (customerInfo.getSecondEmail()!=null) {
						if (!customerInfo.getSecondEmail().equals(secondEmailNew)) {
							sendSecondEmail(customerInfoEdit);
						}
					}else{
						sendSecondEmail(customerInfoEdit);
					}
				}

				//map.put("result", "1");
				map.put("code", "1");
				map.put("result", "更新用户信息成功");
			}
		} catch (Exception e) {
			//map.put("result", "2");
			map.put("code", "2");
			map.put("result","[系统异常"+e.getMessage()+"]");
			e.printStackTrace();
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}

		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;

	}
	
	/**
	 * 修改密码操作
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "updateCustomerPassword", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "dataMap" }) })
	public String updateCustomerPassword() throws Exception {
		try{
//			Map<String, String> map = new HashMap<String, String>();
//			String password = userInfoVo.getUserPwd();
//			String oldPassword = userInfoVo.getOldPassword();
	
			// 检查旧密码是否正确
			if (customerInfoService.getCustomerInfoByAutelIdAndPassword(autelId, oldPassword) == null) {
				dataMap.put("oldPassword", "false");
//				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
	
			try {
				CustomerInfo customerInfo = new CustomerInfo();
				customerInfo.setId(Integer.parseInt(customerId));
				customerInfo.setUserPwd(password);
				customerInfoService.updateCustomer(customerInfo);
				dataMap.put("result", "true");
			} catch (Exception e) {
				dataMap.put("result", "false");
				e.printStackTrace();
			}
	
//			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return SUCCESS;
	}
	
	/**
	 * 修改密码操作
	 * 20170110 A16043 hlh 提供给无人机APP调用的新的修改密码接口
	 * @return
	 * @throws Exception
	 */
	@Action(value = "updateCustomerPasswordUAV", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustomerPasswordUAV() throws Exception {
		Map map = new HashMap();
		try{
			// 检查旧密码是否正确
			if (customerInfoService.getCustomerInfoByAutelIdAndPassword(autelId, oldPassword) == null) {
			//	dataMap.put("oldPassword", "false");
				map.put("code",0);
				map.put("result","旧密码不正确");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
	
			try {
				CustomerInfo customerInfo = new CustomerInfo();
				customerInfo.setId(Integer.parseInt(customerId));
				customerInfo.setUserPwd(password);
				customerInfoService.updateCustomer(customerInfo);
				//dataMap.put("result", "true");
				map.put("code",1);
				map.put("result","true");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			} catch (Exception e) {
				//dataMap.put("result", "false");
				map.put("code",9);
				map.put("result", "[系统异常"+e.getMessage()+"]");
				jsonData = JSONArray.fromObject(map).toString();
				e.printStackTrace();
			}
	
//			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return SUCCESS;
	}
	
	/**
	 * 对系统客服回复进行评分
	 * 
	 * @param request
	 */
	@Action(value = "updateCustomerComplaintScore", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustomerComplaintScore() {
		Map<String, String> map = new HashMap<String, String>();
//		ReCustomerComplaintInfo reCustomerComplaintInfo = new ReCustomerComplaintInfo();

		try {
//			reCustomerComplaintInfo.setId(reCustomerComplaintInfoVO.getId());
//			reCustomerComplaintInfo.setComplaintScore(reCustomerComplaintInfoVO
//					.getComplaintScore());
			reCustomerComplaintInfoService
					.updateReCustomerComplaintInfo(reCustomerComplaintInfo);
			map.put("result", "true");

		} catch (Exception e) {
			map.put("result", "false");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	/**
	 * 修改AutelId时，关联表DT_CustomerChangeAutel信息维护
	 * 
	 * @param request
	 */
	private void updateAutelIdNew(String oldAutelId,String newAutelId) throws Exception {
		
		CustomerInfo updateCust = customerInfoService.getCustomerInfoByAutelId(oldAutelId);
		
		//修改关联表DT_CustomerChangeAutel信息
		CustomerChangeAutel change = changeService.getCustomerChangeAutel(updateCust.getCode());
		if (change == null) {
			change = new CustomerChangeAutel();
		}
		change.setCustomerCode(updateCust.getCode());
		change.setNewAutelId(newAutelId);
		change.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
		if (change.getId() == null) {
			changeService.addCustomerChangeAutel(change);
		} else {
			changeService.updateCustomerChange(change);
	    }
	}
	
	@Action(value = "activateAutelId", results = {@Result(name = "success",
			type = "json", params = {"root", "dataMap"})}) 
	public String activateAutelId() {
		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			String result = "0";
			if (customerInfo != null) {
				CustomerChangeAutel change = changeAutelService.getCustomerChangeAutel(customerInfo.getCode());
				if (change == null) {
					dataMap.put("activeResult", "0");
					return "success";
				}
				
				//判断用户是否激活
				if (change.getActState() == 1) {
					dataMap.put("activeResult", "3");
					return "success";
				}
				
				if (!StringUtil.isEmpty(customerInfo.getSendActiveTime())) {
					//获取邮箱账户激活时设置的时间
					Date sendActiveTime = DateUtil.toDate(customerInfo.getSendActiveTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
					//获取点击邮箱账户激活链接时的时间
	                Date currentSendActiveTime = DateUtil.toDate(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS),
	                												FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
	                //获取时间差
				    long timeDifference = (currentSendActiveTime.getTime() - sendActiveTime.getTime())/1000;

				    //如果3小时内账户未激活、不能进行账户激活操作
	                if(timeDifference > 10800) {
//	                	dataMap.put("activeResult", "1");
	                	result = "1";
	                } else {
	                	change.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
	    				changeAutelService.updateCustomerChange(change);
	    				autelId = change.getNewAutelId();
	    				CustomerInfo delCust = customerInfoService.getCustomerInfoByAutelId(autelId);
	    				if (delCust != null) {
	    					customerInfoService.delCustomer(delCust.getCode());
	    				}
	    				//更新actCode
	    				customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
	    				customerInfo.setAutelId(change.getNewAutelId());
	    				customerInfo.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
	    				customerInfoService.updateCustomer(customerInfo);
//	    				dataMap.put("activeResult", "2");
	    				result = "2";
	                }
				}
				
			} else {
				dataMap.put("activeResult", "0");
				result = "0";
			}
			dataMap.put("autelId", autelId);
			dataMap.put("activeResult", result);
		} catch (Exception e) {
			e.printStackTrace();
			dataMap.put("activeResult", "0");
		}
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "resendEmail", results = {@Result(name = "success", 
			type = "json", params = {"root", "dataMap"})})
	public String resendEmail() {
		String result = "0";
		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			if (customerInfo!=null) {
			
				customerInfo.setSendActiveTime(DateUtil.toString(new Date(), 
						  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				customerInfoService.updateCustomer(customerInfo);
				
				Smtp smtp = smtpManager.getCurrentSmtp();
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("port", smtp.getPort());
				
				String requestUrl = FreeMarkerPaser.getBundleValue("autelpro.url");
				String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
								 + customerInfo.getAutelId() + "&actCode="
								 + customerInfo.getActCode() + "&operationType=9";
				
				String userName = "";
			if (customerInfo != null && customerInfo.getName() != null) {
				userName = customerInfo.getName();
			}
			
				emailModel.getData().put("autelId", customerInfo.getAutelId());
				emailModel.getData().put("userName",userName);
				emailModel.getData().put("activeUrl", activeUrl);
				
				Language language = languageService.getByCode(customerInfo.getLanguageCode());
			
			if (language != null) {
				emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
						language.getLanguageCode(),language.getCountryCode()));
			} else {
				emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
			}
			
				emailModel.setTo(customerInfo.getAutelId());
				EmailTemplate template = emailTemplateService.
						getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REGIST,
								customerInfo.getLanguageCode());
			
			if (template != null) {
				emailModel.setTemplate(template.getCode() + ".html");		//此处一定需要这样写
			}
			
				emailProducer.send(emailModel);
				String webSite = "http://www."+customerInfo.getAutelId().substring(customerInfo.getAutelId().indexOf("@") + 1, customerInfo.getAutelId().length());
				dataMap.put("webSite",webSite);
				result = "2";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		dataMap.put("result", result);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * 根据个人信息设置、如第一邮箱不为空需给第一邮箱发送激活链接
	 */
	private void sendAutelIDEmailForChange(CustomerInfo cust,String autelIdNew) {
		// 发送激活第一邮箱邮件		
		Smtp smtp = smtpManager.getCurrentSmtp();
			
		EmailModel emailModel = new EmailModel();
		emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
		emailModel.getData().put("password", smtp.getPassword());
		emailModel.getData().put("host", smtp.getHost());
		emailModel.getData().put("port", smtp.getPort());
			
		HttpServletRequest request= ThreadContextHolder.getHttpRequest();
		
		String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");
		String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
							 + cust.getAutelId() + "&actCode="+cust.getActCode()+"&operationType=24";
		
		String userName="";
		if(cust!=null && cust.getName()!=null){
		  userName=cust.getName();
		}
		
		emailModel.getData().put("autelId", autelIdNew);
		emailModel.getData().put("userName", userName);
		emailModel.getData().put("activeUrl", activeUrl);
		Language language = languageService.getByCode(cust.getLanguageCode());
			
		if(language != null)
		{
			emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
						language.getLanguageCode(),language.getCountryCode()));
		}
		else 
		{
			emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
		}
			
		emailModel.setTo(autelIdNew);
		EmailTemplate template = emailTemplateService.
					getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_AUTELID_UPDATE_ACTIVE,cust.getLanguageCode());
			
		if(template != null)
		{
			emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
		}
		
		emailProducer.send(emailModel);

		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(cust.getCode());

			if (customerInfo != null) {
				// 每次发送第一邮箱激活时需重新设置第一邮箱激活状态以及激活时间时间点
				//customerInfo.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
				customerInfo.setSendActiveTime(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				customerInfo.setSendResetPasswordTime(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				customerInfoService.updateCustomer(customerInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * 根据个人信息设置、如第二邮箱不为空需给第二邮箱发送激活链接
	 */
	private void sendSecondEmail(CustomerInfo cust) {
		// 发送激活第二邮箱邮件
		Smtp smtp = smtpManager.getCurrentSmtp();

		EmailModel emailModel = new EmailModel();
		emailModel.getData().put("username", smtp.getUsername());// username
																	// 为freemarker模版中的参数
		emailModel.getData().put("password", smtp.getPassword());
		emailModel.getData().put("host", smtp.getHost());

		String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");;
		String activeUrl = requestUrl
				+ "/goActiveSecondEmail.html?secondEmail="
				+ cust.getSecondEmail() + "&actCode="
				+ cust.getActCode() + "&operationType=20";

		String userName="";
		if(cust!=null && cust.getName()!=null){
		  userName=cust.getName();
		}
		emailModel.getData().put("secondEmail", cust.getSecondEmail());
		emailModel.getData().put("activeUrl", activeUrl);
		emailModel.getData().put("autelId", cust.getAutelId());
		emailModel.getData().put("userName", userName);
		Language language = languageService.getByCode(cust
				.getLanguageCode());

		if (language != null) {
			emailModel.setTitle(FreeMarkerPaser.getBundleValue(
					"accountinformation.activesecondemail.mailtitle",
					language.getLanguageCode(), language.getCountryCode()));
		} else {
			emailModel
					.setTitle(FreeMarkerPaser
							.getBundleValue("accountinformation.activesecondemail.mailtitle"));
		}

		emailModel.setTo(cust.getSecondEmail());
		EmailTemplate template = emailTemplateService
				.getUseTemplateByTypeAndLanguage(
						CTConsatnt.EMAIL_TEMPLATE_TYEP_AUTELID_SECONDEMAIL_ACTIVE,
						cust.getLanguageCode());

		if (template != null) {
			emailModel.setTemplate(template.getCode() + ".html"); // 此处一定需要这样写
		}

		emailProducer.send(emailModel);

		try {
			List<CustomerInfo> customerInfoList = customerInfoService
					.getCustomerInfoListBySecondEmail(cust
							.getSecondEmail());

			if (customerInfoList.size() > 0) {
				// 每次发送第二邮箱激活时需重新设置第二邮箱激活状态以及激活时间时间点
				CustomerInfo customerInfo = customerInfoList.get(0);
				customerInfo
						.setSecondActState(FrontConstant.USER_STATE_NOT_ACTIVE);
				customerInfo.setSendSecondEmailActiveTime(DateUtil.toString(
						new Date(),
						FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				customerInfoService.updateCustomer(customerInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getCustomerInfoCode() {
		return customerInfoCode;
	}

	public void setCustomerInfoCode(String customerInfoCode) {
		this.customerInfoCode = customerInfoCode;
	}
	
	private String getPropertValue(String key) throws IOException{
		InputStream in = FileUtil.getResourceAsStream("config.properties");
		Properties properties = new Properties();
		properties.load(in);
		return properties.getProperty(key);
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public ReChargeCardVo getReChargeCardVo() {
		return reChargeCardVo;
	}

	public void setReChargeCardVo(ReChargeCardVo reChargeCardVo) {
		this.reChargeCardVo = reChargeCardVo;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public ReChargeCardSupport getSupport() {
		return support;
	}

	public void setSupport(ReChargeCardSupport support) {
		this.support = support;
	}

	public CustomerInfo getCustomerInfoEdit() {
		return customerInfoEdit;
	}

	public void setCustomerInfoEdit(CustomerInfo customerInfoEdit) {
		this.customerInfoEdit = customerInfoEdit;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ReCustomerComplaintInfo getReCustomerComplaintInfo() {
		return reCustomerComplaintInfo;
	}

	public void setReCustomerComplaintInfo(
			ReCustomerComplaintInfo reCustomerComplaintInfo) {
		this.reCustomerComplaintInfo = reCustomerComplaintInfo;
	}
	
}
