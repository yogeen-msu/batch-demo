package com.cheriscon.rest.user.action;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.common.model.CustomerComplaintTypeName;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json-default")
@Namespace("/rest/customerComplaintTypeName")
public class CustomerComplaintTypeNameRest extends WWAction{
	
	private String languageCode;
	private String complaintTypeCode;
	
	@Resource
	private ICustomerComplaintTypeNameService customerComplaintTypeNameService;
	
	@Action(value="listComplaintTypeNameByLanguageCode1")
	public String listComplaintTypeNameByLanguageCode1(){
		try{
			List<CustomerComplaintTypeName> names = customerComplaintTypeNameService.listComplaintTypeNameByLanguageCode1(languageCode);
			if(names!=null){
				renderText(JSONArray.fromObject(names).toString());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	@Action(value="getCustomerComplaintTypeName")
	public String getCustomerComplaintTypeName(){
		try {
			CustomerComplaintTypeName name=customerComplaintTypeNameService.getCustomerComplaintTypeName(complaintTypeCode, languageCode);
			if(name!=null){
				renderText(JSONObject.fromObject(name).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getComplaintTypeCode() {
		return complaintTypeCode;
	}

	public void setComplaintTypeCode(String complaintTypeCode) {
		this.complaintTypeCode = complaintTypeCode;
	}

	
	
}
