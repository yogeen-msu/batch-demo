package com.cheriscon.rest.user.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.common.model.UserLoginInfo;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.user.service.IUserLoginInfoService;

@ParentPackage("json-default")
@Namespace("/rest/userLoginInfo")
public class UserLoginInfoRest extends WWAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1350673748863290398L;

	private UserLoginInfo userLoginInfo;
	
	@Resource
	private IUserLoginInfoService userLoginInfoService;
	
	@Action(value = "save", results = { @Result(name = "success", type = "json", params = {"root", ""})})
	public String save() {
		try {
			userLoginInfoService.addUserLoginInfo(userLoginInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}

	public UserLoginInfo getUserLoginInfo() {
		return userLoginInfo;
	}

	public void setUserLoginInfo(UserLoginInfo userLoginInfo) {
		this.userLoginInfo = userLoginInfo;
	}
	
	
	
}
