package com.cheriscon.rest.user.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.order.service.IOrderInfoBackService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.common.model.PayLog;
import com.cheriscon.common.model.PayPalToken;
import com.cheriscon.common.model.ShoppingCart;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.cheriscon.front.usercenter.customer.service.IPayPalTokenService;
import com.cheriscon.front.usercenter.customer.service.IPayService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.MyOrderPageData;
import com.cheriscon.front.usercenter.customer.vo.OneOrderInfoVo;
import com.cheriscon.front.usercenter.customer.vo.OrderInfoVO;

/**
 * 
 * @author AUTEL
 *
 */
@ParentPackage("json-default")
@Namespace("/rest/orderInfo")
public class OrderInfoRest extends WWAction{
		
	@Resource
	private IOrderInfoService orderInfoService ;
	@Resource
	private IOrderInfoBackService orderInfoBackService;
	@Resource
	private IPayService payService;
	@Resource
	private IShoppingCartService shoppingCartService;
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private ICustomerInfoService  customerInfoService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private IEmailTemplateService emailTemplateService;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IPayPalTokenService payPalTokenService ;
	
	private int orderDate;
	private int orderState;
	private int pageNo;
	private String userCode;
	private String languageCode;
	private String orderNo;
	private String orderCode;
	private String tradeNo;
	private String tradeStatus;
	private String orderResult;
	private PayErrorLog payErrorLog;
	private PayLog payLog;
	private String secureToken;
	private String secureTokenId;
	
	/**
	 * 查询我的订单
	 * @return
	 */
	@Action(value="pageOrderInfo")
	public String pageOrderInfo(){
		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setCode(userCode);
		customerInfo.setLanguageCode(languageCode);
		try {
			Page page = orderInfoService.pageOrderInfo(orderDate, orderState, customerInfo, pageSize, pageNo);
			renderText(JSONObject.fromObject(page).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 取消订单
	 */
	@Action(value="removeOrder")
	public String removeOrder(){
		try{
			boolean removeResult = orderInfoService.removeOrder(orderNo);
			if(removeResult){
				renderText("success");
				return null;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		renderText("fail");
		return null;
	}
	
	/**
	 * 查看订单详情
	 * @return
	 */
	@Action(value="queryOrderDetailInfoByOrderCode")
	public String queryOrderDetailInfoByOrderCode(){
		try {
			List<MyOrderPageData> data = orderInfoService.queryOrderDetailInfoByOrderCode(orderCode, languageCode);
			renderText(JSONArray.fromObject(data).toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Action(value="queryOrderInfoByCode")
	public String queryOrderInfoByCode(){
		try {
			OrderInfo orderInfo = orderInfoService.queryOrderInfoByCode(orderCode);
			renderText(JSONObject.fromObject(orderInfo).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 订单支付成功
	 * @return
	 */
	@Action(value="updateOrderPayState")
	public String updateOrderPayState(){
		try {
			OrderInfo orderInfo = orderInfoService.queryOrderInfoByCode(orderCode);
			
//			如果订单支付状态未修改就更新支付状态 ，以更新就跳过 防止重复更新
			if(orderInfo!=null&&FrontConstant.ORDER_PAY_STATE_YES!=orderInfo.getOrderPayState().intValue()){
				boolean flag = orderInfoService.updateOrderPayState(orderCode);
			    if (flag) {
			    	orderInfoBackService.processState(orderCode);
				}
			    PayLog payLog=new PayLog();
			    payLog.setOut_trade_no(orderCode);
			    payLog.setResult(FrontConstant.PAY_TRUE);
				 HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			    String ip=FrontConstant.getIpAddr(request);
			    String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			    payLog.setIp(ip);
			    payLog.setCreateTime(date);
			    
			    payLog.setTrade_no(tradeNo);
			    payLog.setTrade_status(tradeStatus);
			    try {
			    	payService.insertPayLog(payLog);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					
				}
			}
			
			renderText("success");
		} catch (Exception e) {
			e.printStackTrace();
			renderText("fail");
		}
		return null;
	}
	
	/**
	 * 生成订单
	 * @return
	 */
	@Action(value = "submintOrderInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "orderResult"}) })
	public String submintOrderInfo(){
		String subResult="";
		CustomerInfo customerInfo;
		try {
			customerInfo = customerInfoService.getCustomerByCode(userCode);
		} catch (Exception e) {
			e.printStackTrace();
			orderResult = "{\"subResult\":\"false\"}";
			renderText(orderResult);
			return null;
		}
		Map<String,String> dataMap = new HashMap<String, String>();
		BigDecimal orderAllMoney=new BigDecimal(0.00);
		
		List<OneOrderInfoVo> infoVos=new ArrayList<OneOrderInfoVo>();
		List<ShoppingCart> shoppingCarts=shoppingCartService.querShoppingCarts(userCode);
		if (shoppingCarts.size()>0) {
			for (int i = 0; i < shoppingCarts.size(); i++) {
				OneOrderInfoVo info=new OneOrderInfoVo();
				ShoppingCart cart= shoppingCarts.get(i);
				if (cart.getIsSoft() == 1) {
					MinSaleUnitDetailVO detailVO=minSaleUnitDetailService.findMinSaleUnitDetailInfo(cart.getMinSaleUnitCode(), customerInfo.getLanguageCode(), cart.getProSerial());
					
					String discountPrice="0";
					//查询最小销售单位是否可参与活动
					MarketPromotionDiscountInfoVO  discountInfoVO=null;
					if (cart.getType() ==0) {
						info.setYear(1);//续租年限
						discountInfoVO=minSaleUnitDetailService.queryDiscount(null, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_BUY,detailVO.getDate());
						discountPrice=detailVO.getPrice();
						if (discountInfoVO!=null) {
							//折后价
							discountPrice=String.valueOf((Float.parseFloat(detailVO.getPrice())*1000 * Float.parseFloat(String.valueOf( discountInfoVO.getDiscount())))/10/1000);
						}
						info.setPrice(discountPrice);//原价
					}else {
						info.setYear(Integer.valueOf(cart.getYear()));//续租年限
						//查询最小销售单位是否可参与活动
						discountInfoVO=minSaleUnitDetailService.queryDiscount(null, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,detailVO.getDate());
						discountPrice=detailVO.getPrice();
						if (discountInfoVO!=null) {
							//折后价
							discountPrice=String.valueOf((Float.parseFloat(detailVO.getPrice())*1000 * Float.parseFloat(String.valueOf( discountInfoVO.getDiscount())))/10/1000);
						}
						info.setPrice(discountPrice);//原价
					}
					
					BigDecimal total=new BigDecimal(0.00);
					BigDecimal num1=new BigDecimal(info.getYear());
					num1.setScale(2, BigDecimal.ROUND_HALF_UP);
					
					BigDecimal num2=new BigDecimal(discountPrice);
					num2.setScale(2, BigDecimal.ROUND_HALF_UP);
					total=num1.multiply(num2);
					orderAllMoney=orderAllMoney.add(total);
				}else {
					
					CfgSoftRentInfoVO rentInfoVO=minSaleUnitDetailService.findSaleConfigDetailInfo(cart.getMinSaleUnitCode(), customerInfo.getLanguageCode(),cart.getProSerial());
					
					MarketPromotionDiscountInfoVO info2=minSaleUnitDetailService.querySaleCfgDiscount(null, rentInfoVO.getSaleCfgCode(), rentInfoVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,rentInfoVO.getDate());
					String discountPrice=rentInfoVO.getPrice();
					rentInfoVO.setPrice(Float.valueOf(discountPrice).toString());
					if (info2!=null) {
						//折后价
						discountPrice=String.valueOf((Float.parseFloat(rentInfoVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info2.getDiscount())))/10/1000);
					}
					info.setPrice(discountPrice);//原价
					
					BigDecimal total=new BigDecimal(0.00);
					BigDecimal num1=new BigDecimal(cart.getYear());
					num1.setScale(2, BigDecimal.ROUND_HALF_UP);
					
					BigDecimal num2=new BigDecimal(discountPrice);
					num2.setScale(2, BigDecimal.ROUND_HALF_UP);
					total=num1.multiply(num2);
					orderAllMoney=orderAllMoney.add(total);
					info.setYear(Integer.valueOf(cart.getYear()));//续租年限
				}
				
				
				info.setMinSaleUnitCode(cart.getMinSaleUnitCode());//最小销售单位code
				info.setSerial(cart.getProSerial());//序列号
				info.setType(cart.getType());
				//String picPath=CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+minSaleUnit[5];
				info.setPicPath(cart.getPicPath());//产品图片路径
				info.setIsSoft(Integer.valueOf(cart.getIsSoft()));
				infoVos.add(info);
			}
			
			String orderInfoCode=DBConstant.getRandStr();
			OrderInfoVO orderInfoVO = new OrderInfoVO();
			orderInfoVO.setAllMoney(orderAllMoney.toString());
			orderInfoVO.setCustomerCode(userCode);
			orderInfoVO.setDiscountAllMoney("0");
			orderInfoVO.setOrderInfoVos(infoVos);
			
			orderInfoVO.setOrderDate(DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss"));//下单时间
			orderInfoVO.setOrderCode(orderInfoCode);//订单编码
			
			boolean submitResult=orderInfoService.submitOrderInfo(orderInfoVO);
			if(submitResult){
				Smtp smtp = smtpManager.getCurrentSmtp( );
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("port", smtp.getPort());
				
				String userName="";
				if(customerInfo!=null && customerInfo.getName()!=null){
				  userName=customerInfo.getName();
				}
				String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");
				String orderUrl=requestUrl+"/OnlinePay.html?orderInfoCode="+orderInfoVO.getOrderCode();
				
				emailModel.getData().put("autelId", customerInfo.getAutelId());
				emailModel.getData().put("userName", userName);
				emailModel.getData().put("orderCode", orderInfoVO.getOrderCode()); //订单编号
				emailModel.getData().put("orderDate", orderInfoVO.getOrderDate()); //订单时间
				emailModel.getData().put("orderMoney", orderInfoVO.getAllMoney()); //订单金额
				emailModel.getData().put("orderUrl", orderUrl); //订单金额
				
				Language language = languageService.getByCode(customerInfo.getLanguageCode());
				if(language != null)
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("cententman.messageman.order.confirm.mailtitle",
							language.getLanguageCode(),language.getCountryCode()));
				}
				else 
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("cententman.messageman.order.confirm.mailtitle"));
				}
				emailModel.getData().put("orderStatus", FreeMarkerPaser.getBundleValue("order.nopay",
						language.getLanguageCode(),language.getCountryCode())); //订单状态
				
				emailModel.setTo(customerInfo.getAutelId());
				EmailTemplate template = emailTemplateService.
						getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_ORDER_CONFIRM,customerInfo.getLanguageCode());
				
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
			}
			//将结果转成json数据格式传输
			subResult=String.valueOf(submitResult);
			dataMap.put("orderInfoCode", orderInfoCode);
			dataMap.put("allmoney", orderAllMoney.toString());
		}
		
		dataMap.put("subResult", subResult);
		orderResult=JSONArray.fromObject(dataMap).toString();
		renderText(orderResult);
		return null;
		
	}
	
	/**
	 * 保存订单出错日志
	 * @return
	 */
	@Action("payErrorLog")
	public String payErrorLog(){
		orderInfoBackService.payErrorLog(payErrorLog);
		renderText("success");
		return null;
	}
	
	@Action("insertPayLog")
	public String insertPayLog(){
		try {
			payService.insertPayLog(payLog);
			renderText("success");
		} catch (Exception e) {
			e.printStackTrace();
			renderText("fail");
			 PayErrorLog errorLog =new PayErrorLog();
			   String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			   HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			   String ip=FrontConstant.getIpAddr(request);
			   errorLog.setIp(ip);
			   errorLog.setErrorDate(date);
			   errorLog.setRemark("信用卡失败返回-响应失败：插入支付日志错误");
			   errorLog.setOrderNo(orderNo);
			   orderInfoBackService.payErrorLog(payErrorLog);
		}
		
		return null;
	}
	
	/**
	 * 保存paypal信用卡支付的token
	 * @return
	 */
	@Action("savePayPalToken")
	public String  savePayPalToken(){
		try{
			PayPalToken payPalToken = new PayPalToken(orderNo, secureToken, secureTokenId);
			payPalTokenService.savePayPalToken(payPalToken);
			renderText("success");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	/**
	 * paypal信用卡支付成功后，需要验证 token 走独立的一个流程
	 * @return
	 */
	@Action("updateOrderPayStateByBill")
	public String updateOrderPayStateByBill(){
		try{
			PayPalToken payPalToken = payPalTokenService.getPayPalToken(secureTokenId);
			if(payPalToken!=null){
				if(payPalToken.getSecureToken().equals(secureToken)){
					orderCode = payPalToken.getOrderNo();
					updateOrderPayState();
				}else{
					renderText("fail");
				}
				
			}else{
				updateOrderPayState();
			}
		}catch (Exception e) {
			e.printStackTrace();
			renderText("fail");
		}
		return null;
	}
	
	/**
	 * 查询未支付订单数量
	 * @return
	 */
	@Action("queryOrderIsNotPay")
	public String queryOrderIsNotPay(){
		int num = orderInfoService.queryOrderIsNotPay(userCode);
		renderText(num+"");
		return null; 
	}
	
	public int getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(int orderDate) {
		this.orderDate = orderDate;
	}

	public int getOrderState() {
		return orderState;
	}

	public void setOrderState(int orderState) {
		this.orderState = orderState;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public String getOrderResult() {
		return orderResult;
	}

	public void setOrderResult(String orderResult) {
		this.orderResult = orderResult;
	}

	public PayErrorLog getPayErrorLog() {
		return payErrorLog;
	}

	public void setPayErrorLog(PayErrorLog payErrorLog) {
		this.payErrorLog = payErrorLog;
	}

	public PayLog getPayLog() {
		return payLog;
	}

	public void setPayLog(PayLog payLog) {
		this.payLog = payLog;
	}

	public String getSecureToken() {
		return secureToken;
	}

	public void setSecureToken(String secureToken) {
		this.secureToken = secureToken;
	}

	public String getSecureTokenId() {
		return secureTokenId;
	}

	public void setSecureTokenId(String secureTokenId) {
		this.secureTokenId = secureTokenId;
	}

}
