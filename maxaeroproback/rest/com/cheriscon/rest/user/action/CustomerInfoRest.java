package com.cheriscon.rest.user.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.UserLoginInfo;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.IChangeAutelService;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.user.service.IUserLoginInfoService;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/rest/customerInfo")
public class CustomerInfoRest extends WWAction {
	
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private ISystemErrorLogService systemErrorService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private IEmailTemplateService emailTemplateService;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IUserLoginInfoService userLoginInfoService;
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IProductContractLogService productContractService;
	@Resource
	private IProductSoftwareValidStatusService validService;
	@Resource
	private IChangeAutelService changeService;
//	@Resource
//	private IChangeAutelService changeService;
	
	private CustomerInfo customerInfo;
	private String autelId;
	private String password;
	private String regResult;
	private String code;
	private String loginIp;
	private String userCode;
	private String serialNo;
	private String name;
	
	private CustomerInfo customerInfoEdit;
	private String jsonData;
	private String facebookId;
	private String oldId;
	private String newId;
	private Map<String, Object> dataMap;
	
	
	@Action(value = "getbyFacebookId", results = {@Result(name = SUCCESS, type = "json", params = {"root", "customerInfo"})}) 
	public String getbyFacebookId() {
		customerInfo = customerInfoService.getByFacebookId(facebookId);
		return SUCCESS;
	}
	
	/**
	 * 登录
	 * @return
	 */
	@Action(value = "login", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "customerInfo"}) })
	public  String login(){
		try {

			customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			if(customerInfo!=null){
//				password = Md5PwdEncoder.getInstance().encodePassword(password, null);
				if(!customerInfo.getUserPwd().equals(password)){
					customerInfo = null;
				}else{
//					登录日志
					UserLoginInfo userLoginInfo = new UserLoginInfo();
					userLoginInfo.setUserCode(customerInfo.getCode());
					userLoginInfo.setLoginIp(loginIp);
					userLoginInfo.setLoginTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
					userLoginInfo.setUserType(1);
					
					try 
					{
						userLoginInfoService.addUserLoginInfo(userLoginInfo);
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/** A16043 20161223 hlh
	 * 无人机产品网站APP登录
	 * @return
	 */
	@Action(value = "loginUAV", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"}) })
	public  String loginUAV(){
		Map map = new HashMap();
		try {
			customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			if(customerInfo!=null){
				if(!customerInfo.getUserPwd().equals(password)){
					customerInfo = null;
					map.put("code",2);
					map.put("result","登录密码不正确！");
					jsonData = JSONArray.fromObject(map).toString();
				}else{
//					登录日志
					UserLoginInfo userLoginInfo = new UserLoginInfo();
					userLoginInfo.setUserCode(customerInfo.getCode());
					userLoginInfo.setLoginIp(loginIp);
					userLoginInfo.setLoginTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
					userLoginInfo.setUserType(1);
					
					try 
					{
						userLoginInfoService.addUserLoginInfo(userLoginInfo);
						map.put("code",0);
						map.put("result",customerInfo );
						jsonData = JSONArray.fromObject(map).toString();
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
						map.put("code",19);
						map.put("result", "[记录用户登录信息时出现系统异常"+e.getMessage()+"]");
						jsonData = JSONArray.fromObject(map).toString();
					}
				}
			}else{
				map.put("code",1);
				map.put("result","找不到对应AutelID的用户信息");
				jsonData = JSONArray.fromObject(map).toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code",9);
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}
	/**
	 * 登录完成后续操作
	 * 对美国和加拿大的额外操作
	 * @return
	 */
	@Action(value = "loginAfter")
	public String loginAfter(){
		try{
			// 从登陆IP判断是否是南美区域，如果是，并且DS708区域不是南美区，则调整销售契约

		List<ProductInfo> list=productInfoService.queryProductByUserCode(userCode);
		   if(null!=list && list.size()>0){
			   for(int i=0;i<list.size();i++){
				    ProductInfo temp=list.get(i);
				      SaleContract sale=new SaleContract();
				      sale.setAreaCfgCode("acf_North_America");  //美国区域
				      sale.setLanguageCfgCode(temp.getSaleContractLanguage()); //产品语言
				      sale.setSaleCfgCode("scf_DS708_Basic"); //产品销售配置
				      sale.setSealerCode("sei201305101150570536");  //美国经销商编码
				      sale.setProTypeCode("prt_DS708"); //升级卡类型
					  List<SaleContract> tempList=saleContractService.getContrctList(sale);
					  if(tempList!=null && tempList.size()!=0){
						  SaleContract changeContract=tempList.get(0);
						  
							ProductContractChangeLog log=new ProductContractChangeLog();
							log.setNewContract(changeContract.getCode());
							log.setOldContract(temp.getSaleContractCode());
							log.setOperatorIp(loginIp);
							log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
							log.setOperatorUser("登陆区域为南美自动更换契约");
							log.setProductSN(temp.getSerialNo());
							//1.保存日志
							productContractService.saveLog(log);
							//更新产品销售契约
							temp.setSaleContractCode(changeContract.getCode());
							productInfoService.updateProductInfo(temp); 
							//删除多余的最小销售单位有效期
							validService.deleteMoreMinCode(temp.getCode());
							
					  }
			   }
		   }
		}catch (Exception e) {
			e.printStackTrace();
		}
		renderText("success");
		return null;
	}
	/**
	 * 根据AutelId查询用户
	 * @return
	 */
	@Action(value = "getCustomerInfoByAutelId", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "customerInfo"}) })
	public String getCustomerInfoByAutelId(){
		try {
			customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 根据AutelId查询用户  20170110 提供给 无人机APP调用
	 * @return
	 */
	@Action(value = "getCustomerInfoByAutelIdUAV", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"}) })
	public String getCustomerInfoByAutelIdUAV(){
		Map map = new HashMap();
		try {
			customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
		    if(null != customerInfo){
		    	map.put("code", 0);
		    	map.put("result", customerInfo);
		    	jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
		    }else{
		    	map.put("code", 1);
		    	map.put("result", "[]");
		    	jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
		    }
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code",9);
			map.put("result", "[系统异常"+e.getMessage()+"]");
		    jsonData = JSONArray.fromObject(map).toString();
		}
		return SUCCESS;
	}
	
	/**
	 * 注册用户
	 * @return
	 */
	@Action(value = "register", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String register(){
		try {
			customerInfoService.addCustomer(customerInfo);
			regResult = "success";
			try
			{
				Smtp smtp = smtpManager.getCurrentSmtp( );
				
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("port", smtp.getPort());
				
				//20160816 add by A16043 send Office365 email
//				emailModel.getData().put("mail.smtp.auth", "true");
//				emailModel.getData().put("mail.smtp.starttls.enable", "true");
//				emailModel.getData().put("mail.smtp.debug", true);
//				emailModel.getData().put("mail.transport.protocol", "smtp");
//				emailModel.getData().put("mail.smtp.socketFactory.port", smtp.getHost());
//				emailModel.getData().put("mail.smtp.socketFactory.fallback", true);
//				emailModel.getData().put("mail.smtps.ssl.trust", smtp.getHost());
		        // end 20160816
				
				String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");
				String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
								 + customerInfo.getAutelId() + "&actCode="+customerInfo.getActCode()+"&operationType=9";
				
				String userName="";
				if(customerInfo!=null && customerInfo.getName()!=null){
				  userName=customerInfo.getName();
				}
				
				emailModel.getData().put("autelId", customerInfo.getAutelId());
				emailModel.getData().put("userName",userName);
				emailModel.getData().put("activeUrl", activeUrl);
				
				Language language = languageService.getByCode(customerInfo.getLanguageCode());
				
				if(language != null)
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
							language.getLanguageCode(),language.getCountryCode()));
				}
				else 
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
				}
				
				emailModel.setTo(customerInfo.getAutelId());
				EmailTemplate template = emailTemplateService.
						getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REGIST,customerInfo.getLanguageCode());
				
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
//				String webSite = "http://www."+customerInfo.getAutelId().substring(customerInfo.getAutelId().indexOf("@") + 1, customerInfo.getAutelId().length());
//				this.putData("webSite",webSite);
		} 
		catch (Exception ex2) 
		{
			ex2.printStackTrace();
			String errorMsg2="";
			StackTraceElement[] st2 = ex2.getStackTrace();
			for (StackTraceElement stackTraceElement : st2) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.rest.user.action.CustomerInfoRest".equals(exclass)){
				
				errorMsg2=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex2.getClass().getName();
				systemErrorService.saveLog("用户注册提交发送邮件",errorMsg2);
			}
			}
			
		}
		} catch (Exception e) {
			e.printStackTrace();
			regResult = "fail";
		}
		
		return SUCCESS;
	}
	
	
	/**
	 * 注册用户   20170110 A16043 给无人机APP用的用户注册接口
	 * @return
	 */
	@Action(value = "registerUAV", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"}) })
	public String registerUAV(){
		Map map = new HashMap();
		try {
			customerInfoService.addCustomer(customerInfo);
			map.put("code",0);
			map.put("result","success");
			regResult = "success";
			jsonData = JSONArray.fromObject(map).toString();
			try
			{
				Smtp smtp = smtpManager.getCurrentSmtp( );
				
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("port", smtp.getPort());
				
				//20160816 add by A16043 send Office365 email
//				emailModel.getData().put("mail.smtp.auth", "true");
//				emailModel.getData().put("mail.smtp.starttls.enable", "true");
//				emailModel.getData().put("mail.smtp.debug", true);
//				emailModel.getData().put("mail.transport.protocol", "smtp");
//				emailModel.getData().put("mail.smtp.socketFactory.port", smtp.getHost());
//				emailModel.getData().put("mail.smtp.socketFactory.fallback", true);
//				emailModel.getData().put("mail.smtps.ssl.trust", smtp.getHost());
		        // end 20160816
				
				String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");
				String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
								 + customerInfo.getAutelId() + "&actCode="+customerInfo.getActCode()+"&operationType=9";
				
				String userName="";
				if(customerInfo!=null && customerInfo.getName()!=null){
				  userName=customerInfo.getName();
				}
				
				emailModel.getData().put("autelId", customerInfo.getAutelId());
				emailModel.getData().put("userName",userName);
				emailModel.getData().put("activeUrl", activeUrl);
				
				Language language = languageService.getByCode(customerInfo.getLanguageCode());
				
				if(language != null)
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
							language.getLanguageCode(),language.getCountryCode()));
				}
				else 
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
				}
				
				emailModel.setTo(customerInfo.getAutelId());
				EmailTemplate template = emailTemplateService.
						getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REGIST,customerInfo.getLanguageCode());
				
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
//				String webSite = "http://www."+customerInfo.getAutelId().substring(customerInfo.getAutelId().indexOf("@") + 1, customerInfo.getAutelId().length());
//				this.putData("webSite",webSite);
		} 
		catch (Exception ex2) 
		{
			ex2.printStackTrace();
			String errorMsg2="";
			StackTraceElement[] st2 = ex2.getStackTrace();
			for (StackTraceElement stackTraceElement : st2) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.rest.user.action.CustomerInfoRest".equals(exclass)){
				
				errorMsg2=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex2.getClass().getName();
				systemErrorService.saveLog("用户注册提交发送邮件",errorMsg2);
			}
			}
			
		}
		} catch (Exception e) {
			e.printStackTrace();
			regResult = "fail";
			map.put("code",1);
			map.put("result","fail :"+e.getMessage());
			jsonData = JSONArray.fromObject(map).toString();
		}
		
		return SUCCESS;
	}
	
	/**
	 * 修改用户
	 * @return
	 */
	@Action(value = "update", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String update(){
		try {
			customerInfoService.updateCustomer(customerInfo);
			regResult = "success";
		}catch (Exception e) {
			e.printStackTrace();
			regResult = "fail";
		}
		return SUCCESS;
	}
	
	/**
	 * 根据用户code查询用户
	 * @return
	 */
	@Action(value = "getCustomerByCode", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "customerInfo"}) })
	public String getCustomerByCode(){
		try {
			customerInfo = customerInfoService.getCustomerByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 退出更新信息
	 * @return
	 */
	@Action(value = "logout", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String logout(){
		try {
			customerInfoService.updateCustomer(code, DateUtil.toString(new Date(), 
					FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		regResult="success";
		return SUCCESS;
	}
	
	@Action(value = "getLastLogin")
	public String getLastLogin(){
		try {
			List<UserLoginInfo> userLoginInfo = userLoginInfoService.queryUserLoginInfoByCode(userCode);
			if(userLoginInfo!=null&&userLoginInfo.size()>0){
				renderText(JSONObject.fromObject(userLoginInfo.get(userLoginInfo.size()-1)).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Action(value = "updateAutelId", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"}) })
	public String updateAutelId() {
		jsonData = "0";
		try {
			CustomerInfo updateCust=customerInfoService.getCustomerInfoByAutelId(oldId);
			int cust=changeService.getCustomerInfoNumByIdAndSrouce(newId);
			if(updateCust!=null){
				if(cust!=0){
					jsonData = "1" ; //输入的autelID已经存在
				}else{
					updateCust.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				//	updateCust.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
					updateCust.setSendActiveTime(DateUtil.toString(new Date(), 
							  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
					customerInfoService.updateCustomer(updateCust);
					
					 CustomerChangeAutel changeAutel=changeService.getCustomerChangeAutel(updateCust.getCode());
					  if(changeAutel==null){
						  changeAutel=new CustomerChangeAutel();
					  }
					changeAutel.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
					changeAutel.setCustomerCode(updateCust.getCode());
					changeAutel.setNewAutelId(newId);
					
					if(changeAutel.getId()==null){
					changeService.addCustomerChangeAutel(changeAutel);
					}else{
					changeService.updateCustomerChange(changeAutel);
					}
					
					Smtp smtp = smtpManager.getCurrentSmtp( );
						
					EmailModel emailModel = new EmailModel();
					emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
					emailModel.getData().put("password", smtp.getPassword());
					emailModel.getData().put("host", smtp.getHost());
					emailModel.getData().put("port", smtp.getPort());
						
					HttpServletRequest request= ThreadContextHolder.getHttpRequest();
					
					String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");
					String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
										 + updateCust.getAutelId() + "&actCode="+updateCust.getActCode()+"&operationType=24";
					
					String userName="";
					if(updateCust!=null && updateCust.getName()!=null){
					  userName=updateCust.getName();
					}
					
					emailModel.getData().put("autelId", updateCust.getAutelId());
					emailModel.getData().put("userName", userName);
					emailModel.getData().put("activeUrl", activeUrl);
					Language language = languageService.getByCode(updateCust.getLanguageCode());
						
					if(language != null)
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
									language.getLanguageCode(),language.getCountryCode()));
					}
					else 
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
					}
						
					emailModel.setTo(newId);
					EmailTemplate template = emailTemplateService.
								getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_AUTELID_UPDATE_ACTIVE,updateCust.getLanguageCode());
						
					if(template != null)
					{
						emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
					}
					emailProducer.send(emailModel);
				}
			}
		} catch (Exception e) {
			jsonData = "2";
		}
//		dataMap.put("result", jsonData);
//		jsonData = JSONArray.fromObject(dataMap).toString();
		return SUCCESS;
	}
	
//	/**
//	 * 用户个人信息修改New
//	 * 
//	 * @param request
//	 * @author pengdongan
//	 */
//	@Action(value = "updateCustomerInfoNew", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
//	public String updateCustomerInfoNew() {
//		Map<String, String> map = new HashMap<String, String>();
//
//		try {
//			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(customerInfoEdit.getCode());
//
//			if (customerInfo != null) {
//				
//				//修改论坛名字
//				if (!StringUtil.isEmpty(customerInfoEdit.getComUsername())) {
//					customerInfoService.updateBbsUsername(customerInfoEdit.getCode(), customerInfoEdit.getComUsername());
//				}
//
//				
//				//获得新的autelId值
//				String autelIdNew = customerInfoEdit.getAutelId();
//
//				//获得新的secondEmail值
//				String secondEmailNew = customerInfoEdit.getSecondEmail();
//				
//				if (!StringUtil.isEmpty(autelIdNew)) {
//					// 判断第一邮箱是否重复
//					if(!customerInfo.getAutelId().equals(autelIdNew)){
//						if (changeService.getCustomerInfoNumByIdAndSrouce(autelIdNew) != 0) {
//							map.put("result", "3");
//							jsonData = JSONArray.fromObject(map).toString();
//							return SUCCESS;
//						}else{
//							updateAutelIdNew(customerInfo.getAutelId(),autelIdNew);
//						}
//					}
//	
//				}
//
//				if (!StringUtil.isEmpty(secondEmailNew)) {
//					// 判断第二邮箱是否重复
//					if(customerInfo.getSecondEmail()==null){
//						if (customerInfoService.getCustomerInfoListBySecondEmail(secondEmailNew).size() > 0) {
//							map.put("result", "3");
//							jsonData = JSONArray.fromObject(map).toString();
//							return SUCCESS;
//						}
//					}else if(!customerInfo.getSecondEmail().equals(secondEmailNew)){
//						if (customerInfoService.getCustomerInfoListBySecondEmail(secondEmailNew).size() > 0) {
//							map.put("result", "3");
//							jsonData = JSONArray.fromObject(map).toString();
//							return SUCCESS;
//						}
//					}
//	
//				}
//
//				if(!StringUtil.isEmpty(customerInfoEdit.getUserPwd())){
//					customerInfoEdit.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(customerInfoEdit.getUserPwd(),null));
//				}
//				
//				customerInfoEdit.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
//
//				if(!StringUtil.isEmpty(customerInfoEdit.getFirstName())){
//					customerInfoEdit.setName(customerInfoEdit.getFirstName()
//								+ customerInfoEdit.getMiddleName()
//								+ customerInfoEdit.getLastName());
//				}
//				
//				//autelId值不在原表中直接修改，要先激活
//				customerInfoEdit.setAutelId(customerInfo.getAutelId());
//				
//				//更新用户信息表
//				customerInfoService.updateCustomerByCode(customerInfoEdit);
//
//				//填充customerInfoEdit对象 用于发邮件的相关值
//				if (StringUtil.isEmpty(customerInfoEdit.getLanguageCode())){
//					customerInfoEdit.setLanguageCode(customerInfo.getLanguageCode());
//				}
//				
//				if (StringUtil.isEmpty(customerInfoEdit.getName())){
//					customerInfoEdit.setName(customerInfo.getName());
//				}
//				
//				
//				//发邮件////////////////////////////
//				
//				if (!StringUtil.isEmpty(autelIdNew)) {
//					// 设置个人信息时只有当第一邮箱变动时才发送邮件
//					if (!customerInfo.getAutelId().equals(autelIdNew)) {
//						sendAutelIDEmailForChange(customerInfoEdit,autelIdNew);
//					}
//				}
//
//				if (!StringUtil.isEmpty(secondEmailNew)) {
//					// 设置个人信息时只有只有当第一次输入第二邮箱以及第二邮箱变动时才发送邮件
//					if (customerInfo.getSecondEmail()!=null) {
//						if (!customerInfo.getSecondEmail().equals(secondEmailNew)) {
//							sendSecondEmail(customerInfoEdit);
//						}
//					}else{
//						sendSecondEmail(customerInfoEdit);
//					}
//				}
//
//				map.put("result", "1");
//			}
//		} catch (Exception e) {
//			map.put("result", "2");
//			e.printStackTrace();
//		}
//
//		jsonData = JSONArray.fromObject(map).toString();
//		return SUCCESS;
//
//	}
	
	/**
	 * 忘记密码发送邮件
	 */
	@Action(value = "forgotPasswordForMail")
	public String forgotPasswordForMail(){
		try 
		{
			CustomerInfo customer = customerInfoService.getCustomerInfoByAutelId(autelId);
			
			if(customer != null)
			{
				customer.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				//发送密码找回邮件需设置发送重置密码时间
				customer.setSendResetPasswordTime(DateUtil.toString(new Date(), 
												  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				
				customerInfoService.updateCustomer(customer);
				
				Smtp smtp = smtpManager.getCurrentSmtp( );
				
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("autelId", autelId);
				
				String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");;
				
				String resetPasswordUrl = requestUrl+ "/resetPassword.html?autelId="
				 						+ autelId + "&actCode="+customer.getActCode()+"&operationType=16";
				
				String userName="";
				if(customer!=null && customer.getName()!=null){
				  userName=customer.getName();
				}
				
				emailModel.getData().put("resetPasswordUrl", resetPasswordUrl);
				emailModel.getData().put("userName", userName);
				
				//emailModel.getData().put("userPwd", DesModule.getInstance().decrypt(customer.getUserPwd()));
				emailModel.getData().put("userPwd", Md5PwdEncoder.getInstance().encodePassword(customer.getUserPwd(), null));
				
				Language language = languageService.getByCode(customer.getLanguageCode());
				
				if(language != null)
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.forgotpassword.mailtitle",
							language.getLanguageCode(),language.getCountryCode()));
				}
				else 
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.forgotpassword.mailtitle"));
				}
				
				emailModel.setTo(autelId);
				EmailTemplate template = emailTemplateService.
						getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_PWD,customer.getLanguageCode());
				
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
				renderText("success");
			}else{
				renderText("fail");
			}
			
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			renderText("fail");
		}
		return null;
	}
	
	/** 20170110 A16043 hlh  提供给 无人机APP使用的忘记密码的新接口
	 * 忘记密码发送邮件
	 */
	@Action(value = "forgotPasswordForMailUAV", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData"}) })
	public String forgotPasswordForMailUAV(){
		Map map = new HashMap();
		try 
		{
			CustomerInfo customer = customerInfoService.getCustomerInfoByAutelId(autelId);
			
			if(customer != null)
			{
				customer.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				//发送密码找回邮件需设置发送重置密码时间
				customer.setSendResetPasswordTime(DateUtil.toString(new Date(), 
												  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				
				customerInfoService.updateCustomer(customer);
				
				Smtp smtp = smtpManager.getCurrentSmtp( );
				
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("autelId", autelId);
				
				String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");;
				
				String resetPasswordUrl = requestUrl+ "/resetPassword.html?autelId="
				 						+ autelId + "&actCode="+customer.getActCode()+"&operationType=16";
				
				String userName="";
				if(customer!=null && customer.getName()!=null){
				  userName=customer.getName();
				}
				
				emailModel.getData().put("resetPasswordUrl", resetPasswordUrl);
				emailModel.getData().put("userName", userName);
				
				//emailModel.getData().put("userPwd", DesModule.getInstance().decrypt(customer.getUserPwd()));
				emailModel.getData().put("userPwd", Md5PwdEncoder.getInstance().encodePassword(customer.getUserPwd(), null));
				
				Language language = languageService.getByCode(customer.getLanguageCode());
				
				if(language != null)
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.forgotpassword.mailtitle",
							language.getLanguageCode(),language.getCountryCode()));
				}
				else 
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.forgotpassword.mailtitle"));
				}
				
				emailModel.setTo(autelId);
				EmailTemplate template = emailTemplateService.
						getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_PWD,customer.getLanguageCode());
				
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
//				renderText("success");
				map.put("code",0);
				map.put("result","success");
				jsonData = JSONArray.fromObject(map).toString();
			}else{
				//renderText("fail");
				map.put("code",1);
				map.put("result","fail");
				jsonData = JSONArray.fromObject(map).toString();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			//renderText("fail");
			map.put("code",9);
			map.put("result", "[系统异常"+e.getMessage()+"]");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		return SUCCESS;
	}
	
	@Action(value = "reSetPasswordByActCode")
	public String reSetPasswordByActCode(){
		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			if(customerInfo != null)
			{
				//更新actCode
				customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				customerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(password,null));
				customerInfoService.updateCustomer(customerInfo);
				renderText("success");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	/**
	 * 根据序列号查询用户
	 * @return
	 */
	@Action(value="getCustomerInfoBySerialNo")
	public String getCustomerInfoBySerialNo(){
		String res = "";
		try {
			CustomerInfo info  = null;
			if(password!=null){
				info = customerInfoService.getCustBySerialNo1(serialNo, password);
//				info = customerInfoService.getCustBySerialNo(serialNo, password);
			}else{
				info = customerInfoService.getCustomerInfoBySerialNo(serialNo);
			}
			if(info!=null){
				res = JSONObject.fromObject(info).toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		renderText(res);
		return null;
	}
	
	@Action(value="listCustomerInfoByAutelId")
	public String listCustomerInfoByAutelId(){
		List<CustomerInfo> list= customerInfoService.listCustomerInfoByAutelId(autelId);
		if(list!=null){
			renderText(JSONArray.fromObject(list).toString());
		}
		return null;
	}
	@Action(value="listCustomerInfoByAutelName")
	public String listCustomerInfoByAutelName(){
		List<CustomerInfo> list= customerInfoService.listCustomerInfoByAutelName(name);
		if(list!=null){
			renderText(JSONArray.fromObject(list).toString());
		}
		return null;
	}
	
	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRegResult() {
		return regResult;
	}

	public void setRegResult(String regResult) {
		this.regResult = regResult;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	
	public String getUserCode() {
		return userCode;
	}
	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	public CustomerInfo getCustomerInfoEdit() {
		return customerInfoEdit;
	}
	
	public void setCustomerInfoEdit(CustomerInfo customerInfoEdit) {
		this.customerInfoEdit = customerInfoEdit;
	}
	public String getJsonData() {
		return jsonData;
	}
	
	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	
	public String getSerialNo() {
		return serialNo;
	}
	
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getFacebookId() {
		return facebookId;
	}
	
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	public String getNewId() {
		return newId;
	}

	public void setNewId(String newId) {
		this.newId = newId;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}
	
	
}
