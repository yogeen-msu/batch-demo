package com.cheriscon.rest.user.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.member.service.IComplaintAdminuserInfoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.IQueryTranslateUser;
import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.service.IVehicleService;
import com.cheriscon.backstage.member.vo.AddCustomerComplaintVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.ComplaintAdminuserInfo;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerComplaintTypeName;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.Vehicle;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/rest/complaintInfo")
public class CustomerComplaintInfoRest extends WWAction {
	
	private int userType;
	private int timeRange;
	private int complaintState; 
	private String userCode;
	private String cusCode;
	private String languageCode;
	private String userName;
	private String autelId;
	private List<CustomerComplaintTypeName> customerComplaintTypeNames;
	private AddCustomerComplaintVo addCustomerComplaintVo;
	private CustomerComplaintInfo customerComplaintInfo;
	private String code;
	private ReCustomerComplaintInfo reCustomerComplaintInfo;
	private Vehicle vehicle = new Vehicle();
	private String jsonData;
	private SealerAuthVO sealerAuth;
	private String sealerAuthDesc;
	private List<Vehicle> vehicles;
	private Map<String, Object> dataMap = new HashMap<String, Object>();
	
	private List<CustomerComplaintInfo> complaintInfos;
	private String complaintTypeCode;
	private List<ReCustomerComplaintInfo> reComplaintInfos;
	
	@Resource
	private IReCustomerComplaintInfoService  reCustomerComplaintInfoService;
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;
	@Resource
	private IQueryTranslateUser queryTranslateUserImpl;
	@Resource 
	private ICustomerComplaintTypeNameService customerComplaintTypeNameService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ISealerInfoService sealerInfoService;
	@Resource
	private IMyAccountService myAccountService;
	@Resource
	private IVehicleService vehicleService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private IComplaintAdminuserInfoService complaintAdminuserInfoService;
	@Resource
	private IEmailTemplateService emailTemplateService;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private ISealerAuthService sealerAuthService;
	
	
	@Action(value = "complaintInfoPage", results = {@Result(name = SUCCESS, 
			type = "json", params = {"root", "webpage"})})
	public String complaintInfoPage() {
		try {
			webpage = this.customerComplaintInfoService
					.queryCustomerComplaintInfoPage(userType, timeRange, 
							complaintState, userCode, page, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "reCustomerComplaintInfo", results = {@Result(name = SUCCESS,
			type = "json", params = {"root", "dataMap"})})
	public String reCustomerComplaintInfo() {
		
		List<AdminUser> customerServiceUsers = queryTranslateUserImpl.listByRoleId2(2);
		CustomerComplaintInfo customerComplaintInfo = null;
		List<ReCustomerComplaintInfo> reCustomerComplaintInfoList = null;
		try {
			reCustomerComplaintInfoList = this.reCustomerComplaintInfoService
					.queryReCustomerComplaintInfoByCode(cusCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			customerComplaintInfo = customerComplaintInfoService
					.getCustomerComplaintInfoByCode(cusCode);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		dataMap.put("customerServiceUsers", customerServiceUsers);
		dataMap.put("reCustomerComplaintInfoList", reCustomerComplaintInfoList);
		dataMap.put("customerComplaintInfo", customerComplaintInfo);
		return SUCCESS;
	}
	
	@Action(value = "reComplaintInfos", results = {@Result(name = "success", 
			type = "json", params = {"root", "reComplaintInfos"})})
	public String reComplaintInfos() {
		try {
			reComplaintInfos = this.reCustomerComplaintInfoService
					.queryReCustomerComplaintInfoByCode(cusCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "listComplaintTypeNameByLanguageCode", 
			results = {@Result(name = SUCCESS, type = "json", params = {
					"root", "customerComplaintTypeNames"
			})})
	public String listComplaintTypeNameByLanguageCode() {
		try {
			customerComplaintTypeNames = customerComplaintTypeNameService
					.listComplaintTypeNameByLanguageCode(languageCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "toAddComplaintInfo", results = {@Result(name = SUCCESS,
			type = "json", params = {"root", "dataMap"})})
	public String toAddComplaintInfo() {
		if (!StringUtil.isEmpty(this.addCustomerComplaintVo.getUserType())) {
			try {
				if (Integer.parseInt(this.addCustomerComplaintVo.getUserType())
						== FrontConstant.CUSTOMER_USER_TYPE) {
	//				userCode = ((CustomerInfo) SessionUtil
	//						.getLoginUserInfo(request)).getCode();
	
					CustomerInfo customerInfo = customerInfoService.getCustomerByCode(
							this.addCustomerComplaintVo.getUserCode());
					
					this.addCustomerComplaintVo.setCompany(customerInfo.getCompany());
					this.addCustomerComplaintVo.setUserName(customerInfo.getAutelId());
	
					if (StringUtil.isEmpty(customerInfo.getMobilePhoneCC())) {
						this.addCustomerComplaintVo.setDaytimePhoneCC("");
					} else {
						this.addCustomerComplaintVo.setDaytimePhoneCC(customerInfo.getMobilePhoneCC());
					}
	
					if (StringUtil.isEmpty(customerInfo.getMobilePhoneAC())) {
						this.addCustomerComplaintVo.setDaytimePhoneAC("");
					} else {
						this.addCustomerComplaintVo.setDaytimePhoneAC(customerInfo.getMobilePhoneAC());
					}
	
					if (StringUtil.isEmpty(customerInfo.getMobilePhone())) {
						this.addCustomerComplaintVo.setDaytimePhone("");
					} else {
						this.addCustomerComplaintVo.setDaytimePhone(customerInfo.getMobilePhone());
					}
	
					this.addCustomerComplaintVo.setEmai(customerInfo.getAutelId());
				} else if (Integer.parseInt(this.addCustomerComplaintVo.getUserType()) 
						== FrontConstant.DISTRIBUTOR_USER_TYPE) {
	//				userCode = ((SealerInfo) SessionUtil.getLoginUserInfo(request))
	//						.getCode();
					SealerInfo sealerInfo = sealerInfoService.getSealerByCode(
							this.addCustomerComplaintVo.getUserCode());
					this.addCustomerComplaintVo.setCompany(sealerInfo.getCompany());
					this.addCustomerComplaintVo.setUserName(sealerInfo.getAutelId());
	
					if (StringUtil.isEmpty(sealerInfo.getMobilePhoneCC()))
					{
						this.addCustomerComplaintVo.setDaytimePhoneCC("");
					} else {
						this.addCustomerComplaintVo.setDaytimePhoneCC(sealerInfo.getMobilePhoneCC());
					}
	
					if (StringUtil.isEmpty(sealerInfo.getDaytimePhoneAC()))
					{
						this.addCustomerComplaintVo.setDaytimePhoneAC("");
					} else {
						this.addCustomerComplaintVo.setDaytimePhoneAC(sealerInfo.getMobilePhoneAC());
					}
	
					if (StringUtil.isEmpty(sealerInfo.getDaytimePhone()))
					{
						this.addCustomerComplaintVo.setDaytimePhone("");
					} else {
						this.addCustomerComplaintVo.setDaytimePhone(sealerInfo.getMobilePhone());
					}
					this.addCustomerComplaintVo.setEmai(sealerInfo.getAutelId());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		List<MyProduct> listProduct = null;
		try {
			listProduct = myAccountService.queryProductByCode(
					addCustomerComplaintVo.getUserCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Vehicle vehicle = new Vehicle();
		vehicle.setQueryType(this.addCustomerComplaintVo.getQueryType());
		List<Vehicle> yearList = null;
		try {
			yearList = vehicleService.getVehicleList(vehicle);
			//List<DataLogging> dataList=dataLogService.getDataLogList(productSN);
			//this.putData("dataList",dataList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		dataMap.put("acc", this.addCustomerComplaintVo);
		dataMap.put("listProduct", listProduct);
		dataMap.put("yearList", yearList);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	@Action(value = "addComplaintInfo", results = {@Result(name = SUCCESS, 
	type = "json", params = {"root", ""})})
	public String addComplaintInfo() {
		try {
			ComplaintAdminuserInfo complaintAdminuserInfo = complaintAdminuserInfoService
					.getComplaintAdminuserInfoBycomplaintTypeCode(
							this.customerComplaintInfo.getComplaintTypeCode());
			if (complaintAdminuserInfo != null) {
				customerComplaintInfo.setAcceptor(complaintAdminuserInfo.getAdminUserid());
			}
			
			customerComplaintInfoService.addCustomerComplaintInfo(customerComplaintInfo);
			
			Language lanuage = languageService.getByCode(customerComplaintInfo.getLanguageCode());
			if(lanuage!=null){
				String status = FrontConstant.getComplaintStatus(
						lanuage.getCountryCode(),
						FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE);
				
				EmailModel emailModel = new EmailModel();
				/*emailModel.getData().put("username", smtp.getUsername());// username
																			// 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("port", smtp.getPort());*/
				
				emailModel.setType(customerComplaintInfo.getAcceptor());
				
				emailModel.getData().put("userName", customerComplaintInfo.getUserName());
				emailModel.getData().put("autelId", customerComplaintInfo.getAutelId());
				emailModel.getData().put("ticketId",
						customerComplaintInfo.getCode());
				emailModel.getData().put("subject",
						customerComplaintInfo.getComplaintTitle());
				String requestUrl = FreeMarkerPaser.getBundleValue("autelpro.url");
				String url = requestUrl + "/queryCustomerComplaintDetail.html?operationType=3&userType="
						+ customerComplaintInfo.getUserType() + "&code=" + customerComplaintInfo.getCode();
				emailModel.getData().put("url", url);
				
				emailModel.getData().put("status", status);

				emailModel.setTitle("[SUPPORT #" + customerComplaintInfo.getCode() + "]: "
						+ customerComplaintInfo.getComplaintTitle());
				emailModel.setTo(customerComplaintInfo.getEmail());  //将发送邮件的地址改为客诉填写的地址

				EmailTemplate template = emailTemplateService.getUseTemplateByTypeAndLanguage(
								CTConsatnt.EMAIL_TEMPLATE_TYEP_ADD_COMPLAINT_SUCCESS,
								customerComplaintInfo.getLanguageCode());
				if (template != null)
				{
					emailModel.setTemplate(template.getCode() + ".html"); // 此处一定需要这样写
				}
				emailProducer.send(emailModel);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(value = "getByCode", results = {@Result(name = SUCCESS, type = "json",
			params = {"root", "customerComplaintInfo"})})
	public String getByCode() {
		try {
			this.customerComplaintInfo = customerComplaintInfoService
					.getCustomerComplaintInfoByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "updateComplaint", results = {@Result(name = "success", type = "json", 
			params = {"root", ""})})
	public String updateComplaint() {
		try {
			this.customerComplaintInfoService
			.updateCustomerComplaintInfo(customerComplaintInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "addReComplaint", results = {@Result(name = "success", type = "json",
			params = {"root", ""})})
	public String addReComplaint() {
		try {
			CustomerComplaintInfo cci = this.customerComplaintInfoService
					.getCustomerComplaintInfoByCode(this.reCustomerComplaintInfo
							.getCustomerComplaintCode());
			cci.setComplaintState(FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE);
			
//			customerComplaintInfoService.updateCustomerComplaintInfo(cci);
//			reCustomerComplaintInfoService.addReCustomerComplaintInfo(reCustomerComplaintInfo);
			reCustomerComplaintInfoService.replayCustomerComplaintInfo(cci, reCustomerComplaintInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	
	//客诉获取车型列表
		@Action(value = "getVehicleJson", results = { @Result(name = SUCCESS, type = "json", params = {
					"root", "vehicles" }) })
		public String getVehicleJson() {
				
			try {
				vehicles = vehicleService.getVehicleList(vehicle);
			} catch (Exception e) {
				e.printStackTrace();
			}
//			jsonData = JSONArray.fromObject(listJson).toString();	
			return SUCCESS;
		}
		
		@Action(value = "updateSealerAuthInfo", results = { @Result(name = SUCCESS, type = "json", params = {
				"root", "jsonData" }) })
		public String updateSealerAuthInfo() throws Exception{
			SealerInfo info =sealerInfoService.getSealerByAutelId(sealerAuth.getAutelId());
			if(info==null){
				jsonData="FAIL";
			}
			try{
				sealerAuthService.saveAuthDetail(info.getCode(), sealerAuthDesc);
				jsonData="SUCCESS";
			}catch(Exception e){
				jsonData="FAIL";
			}
			return SUCCESS;
		}
	
		@Action(value = "complaintInfos", results = {@Result(name = SUCCESS, type = "json", params = {
				"root", "complaintInfos"})})
		public String complaintInfos() {
			try {
				complaintInfos = this.customerComplaintInfoService.queryCustomerComplaintInfoList(timeRange, userCode, complaintState);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return SUCCESS;
		}
		
	@Action(value="getCAInfoBycomplaintTypeCode")
	public String getCAInfoBycomplaintTypeCode(){
		try {
			ComplaintAdminuserInfo info = complaintAdminuserInfoService.getComplaintAdminuserInfoBycomplaintTypeCode(complaintTypeCode);
			if(info!=null){
				renderText(JSONObject.fromObject(info).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Action(value="updateCustomerComplaintInfoState")
	public String updateCustomerComplaintInfoState(){
	
		try {
			CustomerComplaintInfo cci = this.customerComplaintInfoService.getCustomerComplaintInfoByCode(code);
			cci.setComplaintState(complaintState);
			customerComplaintInfoService.updateCustomerComplaintInfo(cci);
			renderText(JSONObject.fromObject(cci).toString());
		} catch (Exception e) {
			e.printStackTrace();
			renderText("false");
		}
		
		return null;
	}
		
	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public int getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(int timeRange) {
		this.timeRange = timeRange;
	}

	public int getComplaintState() {
		return complaintState;
	}

	public void setComplaintState(int complaintState) {
		this.complaintState = complaintState;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getCusCode() {
		return cusCode;
	}

	public void setCusCode(String cusCode) {
		this.cusCode = cusCode;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public List<CustomerComplaintTypeName> getCustomerComplaintTypeNames() {
		return customerComplaintTypeNames;
	}

	public void setCustomerComplaintTypeNames(
			List<CustomerComplaintTypeName> customerComplaintTypeNames) {
		this.customerComplaintTypeNames = customerComplaintTypeNames;
	}

	public AddCustomerComplaintVo getAddCustomerComplaintVo() {
		return addCustomerComplaintVo;
	}

	public void setAddCustomerComplaintVo(
			AddCustomerComplaintVo addCustomerComplaintVo) {
		this.addCustomerComplaintVo = addCustomerComplaintVo;
	}

	public CustomerComplaintInfo getCustomerComplaintInfo() {
		return customerComplaintInfo;
	}

	public void setCustomerComplaintInfo(CustomerComplaintInfo customerComplaintInfo) {
		this.customerComplaintInfo = customerComplaintInfo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ReCustomerComplaintInfo getReCustomerComplaintInfo() {
		return reCustomerComplaintInfo;
	}

	public void setReCustomerComplaintInfo(
			ReCustomerComplaintInfo reCustomerComplaintInfo) {
		this.reCustomerComplaintInfo = reCustomerComplaintInfo;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public SealerAuthVO getSealerAuth() {
		return sealerAuth;
	}

	public void setSealerAuth(SealerAuthVO sealerAuth) {
		this.sealerAuth = sealerAuth;
	}

	public String getSealerAuthDesc() {
		return sealerAuthDesc;
	}

	public void setSealerAuthDesc(String sealerAuthDesc) {
		this.sealerAuthDesc = sealerAuthDesc;
	}

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public List<CustomerComplaintInfo> getComplaintInfos() {
		return complaintInfos;
	}

	public void setComplaintInfos(List<CustomerComplaintInfo> complaintInfos) {
		this.complaintInfos = complaintInfos;
	}

	public String getComplaintTypeCode() {
		return complaintTypeCode;
	}

	public void setComplaintTypeCode(String complaintTypeCode) {
		this.complaintTypeCode = complaintTypeCode;
	}

	public List<ReCustomerComplaintInfo> getReComplaintInfos() {
		return reComplaintInfos;
	}

	public void setReComplaintInfos(List<ReCustomerComplaintInfo> reComplaintInfos) {
		this.reComplaintInfos = reComplaintInfos;
	}

	
}
