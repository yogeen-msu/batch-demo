package com.cheriscon.rest.user.action;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.member.service.IOnlineServiceTypeNameService;
import com.cheriscon.common.model.OnlineServiceTypeName;
import com.cheriscon.framework.action.WWAction;


@ParentPackage("json-default")
@Namespace("/rest/onlineServiceTypeName")
public class OnlineServiceTypeNameRest extends WWAction{
	
	private String languageCode;
	
	@Resource
	private IOnlineServiceTypeNameService onlineServiceTypeNameService;
	
	@Action(value="listOnlineTypeNameByLanguageCode")
	public String listOnlineTypeNameByLanguageCode(){
		try {
			List<OnlineServiceTypeName> osts= onlineServiceTypeNameService.listOnlineTypeNameByLanguageCode(languageCode);
			renderText(JSONArray.fromObject(osts).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	
}
