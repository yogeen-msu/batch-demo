package com.cheriscon.rest.user.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.content.service.IMessageTypeService;
import com.cheriscon.backstage.content.service.IUserMessageService;
import com.cheriscon.common.model.Message;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.common.model.UserMessage;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.PageFinder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.RequestUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/rest/message")
public class MessageRest extends WWAction {

	@Resource
	private IMessageService messageService;
	@Resource 
	private IMessageTypeService messageTypeService;
	@Resource
	private IUserMessageService userMessageService;
	
	private Message message;
	private String userCode;
	private String languageCode;
	private String msgTypeCode;
	private String userType;
	private String timeRange;
	private String code;
	private UserMessage userMessage;
	private String jsonData;
	private String isDialog;
	private String createTime;
	private List<Message> messageList = new ArrayList<Message>();
	private Map<String, Object> dataMap = new HashMap<String, Object>();

	@Action(value = "list", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "dataMap" }) })
	public String list() {
		
		Message message = new Message();
		message.setLanguageCode(languageCode);
		message.setMsgTypeCode(msgTypeCode);
		message.setIsDialog(FrontConstant.USER_MESSAGE_IS_ALL_DIALOG);

		if (!StringUtil.isEmpty(userType)) {
			message.setUserType(Integer.parseInt(userType));
		}
		
		if (timeRange == null || timeRange.equals("")) {
			timeRange = String.valueOf(FrontConstant.LATELY_THREE_MONTH);
		}

		message.setTimeRange(Integer.parseInt(timeRange));
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -3); //得到前3个月

		message.setCreatTime(DateUtil.toString(calendar.getTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
		
		String pageNoStr = page + "";
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr) ? ids[0] : Integer.valueOf(pageNoStr);

		List<MessageType> messageTypeList = null;
		try {
			messageTypeList = messageTypeService.queryMessageType(message.getUserType(), 
					Integer.parseInt(timeRange), languageCode,
					message.getCreatTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
//
		MessageType messageType = null;
		List<MessageType> newMessageTypeList = new ArrayList<MessageType>();
		int totalMessageTypeCount = 0;
//
		if (messageTypeList != null && messageTypeList.size() > 0) {
			int messageCount = 0;

			for (int i = 0; i < messageTypeList.size(); i++) {
				messageType = new MessageType();
				messageType.setCode(messageTypeList.get(i).getCode());
				messageType.setName(messageTypeList.get(i).getName());

				try {
					messageType.setMessageTypeCount(messageService.queryMessageCount(userCode,messageTypeList.get(i).getCode(), languageCode,
							message.getUserType(), message.getIsDialog(), message.getTimeRange(), message.getCreatTime()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				totalMessageTypeCount += messageType.getMessageTypeCount();
				messageCount++;
				messageType.setMessageCount(messageCount);
				newMessageTypeList.add(messageType);
			}
		} else // 主要处理根据时间类型查不到数据时也要展示消息类型数据
		{

			int messageCount = 0;
			List<MessageType> types = messageTypeService.queryMessageType();
			for (int i = 0; i < types.size(); i++) {
				messageType = new MessageType();
				messageType.setCode(types.get(i).getCode());
				messageType.setName(types.get(i).getName());
				messageType.setMessageTypeCount(0);
				messageCount++;
				messageType.setMessageCount(messageCount);
				newMessageTypeList.add(messageType);
			}
		}
		
		try {
			webpage = messageService.queryMyMessageList(message, userCode, pageNo, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Integer messageCount = 0;
		try {
			messageCount = messageService.queryMessageCount(userCode, "", 
					message.getLanguageCode(), message.getUserType(), message.getIsDialog(), 
					message.getTimeRange(), message.getCreatTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		dataMap.put("pageData", webpage);
		dataMap.put("messageTypeList", newMessageTypeList);
		dataMap.put("messageCount", messageCount);
		return "success";
	}

	@Action(value = "listSec", results = {@Result(name = SUCCESS, type = "json", params = {"root", "dataMap"})})
	public String listSec() {
		Message message = new Message();
		message.setLanguageCode(languageCode);
		message.setMsgTypeCode(msgTypeCode);
		message.setIsDialog(FrontConstant.USER_MESSAGE_IS_ALL_DIALOG);

		if (!StringUtil.isEmpty(userType)) {
			message.setUserType(Integer.parseInt(userType));
		}
		
		if (timeRange == null || timeRange.equals("")) {
			timeRange = String.valueOf(FrontConstant.LATELY_THREE_MONTH);
		}

		message.setTimeRange(Integer.parseInt(timeRange));
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -3); //得到前3个月

		message.setCreatTime(DateUtil.toString(calendar.getTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
		
		String pageNoStr = page + "";
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr) ? ids[0] : Integer.valueOf(pageNoStr);

		List<MessageType> messageTypeList = null;
		try {
			messageTypeList = messageTypeService.queryMessageType(message.getUserType(), 
					Integer.parseInt(timeRange), languageCode,
					message.getCreatTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
//
		MessageType messageType = null;
		List<MessageType> newMessageTypeList = new ArrayList<MessageType>();
		int totalMessageTypeCount = 0;
//
		if (messageTypeList != null && messageTypeList.size() > 0) {
			int messageCount = 0;

			for (int i = 0; i < messageTypeList.size(); i++) {
				messageType = new MessageType();
				messageType.setCode(messageTypeList.get(i).getCode());
				messageType.setName(messageTypeList.get(i).getName());

				try {
					messageType.setMessageTypeCount(messageService.queryMessageCount(userCode,messageTypeList.get(i).getCode(), languageCode,
							message.getUserType(), message.getIsDialog(), message.getTimeRange(), message.getCreatTime()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				totalMessageTypeCount += messageType.getMessageTypeCount();
				messageCount++;
				messageType.setMessageCount(messageCount);
				newMessageTypeList.add(messageType);
			}
		} else // 主要处理根据时间类型查不到数据时也要展示消息类型数据
		{

			int messageCount = 0;
			List<MessageType> types = messageTypeService.queryMessageType();
			for (int i = 0; i < types.size(); i++) {
				messageType = new MessageType();
				messageType.setCode(types.get(i).getCode());
				messageType.setName(types.get(i).getName());
				messageType.setMessageTypeCount(0);
				messageCount++;
				messageType.setMessageCount(messageCount);
				newMessageTypeList.add(messageType);
			}
		}
		
		try {
			webpage = messageService.queryMyMessageList(message, userCode, pageNo, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Integer messageCount = 0;
		try {
			messageCount = messageService.queryMessageCount(userCode, "", 
					message.getLanguageCode(), message.getUserType(), message.getIsDialog(), 
					message.getTimeRange(), message.getCreatTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PageFinder pageFinder = new PageFinder();
		pageFinder.setPageNo(webpage.getCurrentPageNo());
		pageFinder.setPageCount(Integer.parseInt((webpage.getTotalPageCount() + "")));
		pageFinder.setPageSize(webpage.getPageSize());
		pageFinder.setRowCount(webpage.getTotalCount());
		pageFinder.setData((List)webpage.getResult());
		
		dataMap.put("pageData", pageFinder);
		dataMap.put("messageTypeList", newMessageTypeList);
		dataMap.put("messageCount", messageCount);
		return "success";
	}
	
	@Action(value = "getByCode", results = {@Result(name = SUCCESS, type = "json", params={
			"root", "message"
	})})
	public String getByCode() {
		try {
			message = messageService.getMessageByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 增加入库记录用户已读消息
	 * 
	 * @param request
	 */
	@Action(value = "addUserMessageIsRead", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String addUserMessageIsRead() {
		Map<String, String> map = new HashMap<String, String>();
//		UserMessage userMessage = new UserMessage();
//		userMessage.setUserType(userMessageVo.getUserType());
//		userMessage.setMessageCode(userMessageVo.getMessageCode());
//		userMessage.setUserCode(userMessageVo.getUserCode());
//		userMessage.setMessageIsRead(FrontConstant.USER_MESSAGE_IS_YES_READ);
		try {
			userMessageService.saveMessage(userMessage);
			map.put("result", "true");
		} catch (Exception e) {
			map.put("result", "false");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	@Action(value = "queryDialogMessageCount", results = {@Result(name = "success", 
			type = "json", params = {"root", "dataMap"})})
	public String queryDialogMessageCount() {
		int userMessageIsNoDialogReadCount = 0;
		try {
			userMessageIsNoDialogReadCount = messageService.queryMessageCountNotReadNew(
					userCode, null, languageCode, Integer.parseInt(userType), 
					FrontConstant.USER_MESSAGE_IS_YES_DIALOG, null, null);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		dataMap.put("userMessageIsNoDialogReadCount",
				String.valueOf(userMessageIsNoDialogReadCount));
//		jsondata = jsonarray.fromobject(datamap).tostring();
		return "success";
	}
	
	@Action(value = "queryUserMessageNew", results = {@Result(name = "success",
			type = "json", params = {"root", "messageList"})})
	public String queryUserMessageNew() {
		try {
			messageList = this.messageService.queryUserMessageNew(
					Integer.parseInt(userType), languageCode, userCode);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMsgTypeCode() {
		return msgTypeCode;
	}

	public void setMsgTypeCode(String msgTypeCode) {
		this.msgTypeCode = msgTypeCode;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	

	public UserMessage getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(UserMessage userMessage) {
		this.userMessage = userMessage;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	
	public String getIsDialog() {
		return isDialog;
	}

	public void setIsDialog(String isDialog) {
		this.isDialog = isDialog;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public List<Message> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}

	private Integer[] parseId(){
		HttpServletRequest httpRequest = ThreadContextHolder.getHttpRequest();
		String url = RequestUtil.getRequestUrl(httpRequest);
		String pattern = "/(.*)-(\\d+)-(\\d+).html(.*)";
		String page= null;
		String catid = null;
		Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
		Matcher m = p.matcher(url);
		if (m.find()) {
			page = m.replaceAll("$3");
			catid = m.replaceAll("$2");
			return new Integer[]{Integer.valueOf(""+page),Integer.valueOf(""+catid)};
		} 
		return null;
	}
	
}
