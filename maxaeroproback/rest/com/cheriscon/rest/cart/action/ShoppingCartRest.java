package com.cheriscon.rest.cart.action;


import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IDeleteShoppingCartService;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsMinUnitVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;
import com.cheriscon.front.usercenter.customer.vo.ShoppingCartVO;

@ParentPackage("json-default")
@Namespace("/rest/shoppingCart")
public class ShoppingCartRest extends WWAction {

	@Resource
	private IShoppingCartService shoppingCartService;
	@Resource
	private IMinSaleUnitDetailService detailService;
	@Resource
	private IDeleteShoppingCartService deleteShoppingCartService;
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	
	private String customerCode;
	private String languageCode;
	private String serialNo;
	private String minSaleUnitCode;
	private String areaCfgCode;
	private int type;
	private int year;
	private String picPath;
	private int isSoft;
	
	/**
	 * 查询销售配置参与活动的折扣信息
	 * @return
	 */
	@Action(value="queryShoppingCartByCustomerCode")
	public String queryShoppingCartByCustomerCode(){
		try{
//			List<ShoppingCartVO> list = shoppingCartService.queryShoppingCartVoByCustomerCode(customerCode, languageCode);
			List<ShopingCartsProListVO> proListVOs = shoppingCartService.queryShoppingCartByCustomerCode(customerCode, languageCode);
			for (int i = 0; i < proListVOs.size(); i++) {
				for (int j = 0; j < proListVOs.get(i).getMinUnitVOs().size(); j++) {
					
					ShopingCartsMinUnitVO minUnitVO=proListVOs.get(i).getMinUnitVOs().get(j);
					
					MarketPromotionDiscountInfoVO info;
					if (minUnitVO.getIsSoft() == 1) {
						// 查询最小销售单位是否可参与活动
						info = minSaleUnitDetailService.queryDiscount(null,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
										minUnitVO.getType(),
										minUnitVO.getValidDate());
					} else {
						// 查询销售配置是否可参与活动
						info = minSaleUnitDetailService.querySaleCfgDiscount(null,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
										minUnitVO.getType(),
										minUnitVO.getValidDate()); 
					}
					String discountPrice = minUnitVO.getCostPrice();
					minUnitVO.setCostPrice(Float.valueOf(discountPrice).toString());
					if (info != null) {
						// 折后价
						discountPrice = String.valueOf((Float.parseFloat(minUnitVO.getCostPrice())*1000 * Float.parseFloat(String.valueOf(info.getDiscount()))) / 10/1000);
					}
					minUnitVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
					if(minUnitVO.getValidDate() != null){
						Date date = null;
						if(minUnitVO.getValidDate().indexOf("/")>-1){
							 date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy/MM/dd");
							}
							else{
							 date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy-MM-dd");	
							}
						Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
						//当前时间大于有效期时间(已经过期)
						if(currentDate.after(date)){
							minUnitVO.setIsValid(1);
						}
					}
					proListVOs.get(i).getMinUnitVOs().set(j, minUnitVO);
				}
			}
			renderText(JSONArray.fromObject(proListVOs).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将续租产品加入购物车
	 * @return
	 */
	@Action(value="addShoppingCartIsRent")
	public String addShoppingCartIsRent(){
		CfgSoftRentInfoVO rentInfoVO=null;
		MinSaleUnitDetailVO detailVO=null;
		String isSuccess="false";
		try{
			if (isSoft == 0) {
				rentInfoVO=detailService.findSaleConfigDetailInfo(minSaleUnitCode, languageCode,serialNo);
				
				MarketPromotionDiscountInfoVO info=detailService.querySaleCfgDiscount(null, rentInfoVO.getSaleCfgCode(), rentInfoVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,rentInfoVO.getDate());
				String discountPrice=rentInfoVO.getPrice();
				rentInfoVO.setPrice(Float.valueOf(discountPrice).toString());
				if (info!=null) {
					//折后价
					discountPrice=String.valueOf((Float.parseFloat(rentInfoVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
				}
				if(StringUtil.isEmpty(areaCfgCode)){
					picPath=rentInfoVO.getCfgpicpath();
					areaCfgCode=rentInfoVO.getAreaCfgCode();
				}
				isSuccess=shoppingCartService.addShoppingCartIsRent(customerCode, "", serialNo, "", minSaleUnitCode, areaCfgCode, type, year,picPath,isSoft,rentInfoVO.getPrice(),rentInfoVO.getDate());	
			}else {
				detailVO=detailService.findMinSaleUnitDetailInfo(minSaleUnitCode, languageCode,serialNo);
				//查询最小销售单位是否可参与活动
				MarketPromotionDiscountInfoVO  info=null;
				if (type == 1) {
					info=detailService.queryDiscount(null, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,detailVO.getDate());
				}else {
					info=detailService.queryDiscount(null, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_BUY,detailVO.getDate());
				}
						
				String discountPrice=detailVO.getPrice();
				detailVO.setPrice(Float.valueOf(discountPrice).toString());
				if (info!=null) {
					//折后价
					discountPrice=String.valueOf((Float.parseFloat(detailVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
				}
				
				isSuccess=shoppingCartService.addShoppingCartIsRent(customerCode, "", serialNo, "", minSaleUnitCode, areaCfgCode, type, year,picPath,isSoft,discountPrice,detailVO.getDate());	
			}
			
			renderText(isSuccess);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 修改续租年限
	 * @return
	 */
	@Action(value="updateYear")
	public String updateYear(){
		try{
			shoppingCartService.updateYear(customerCode, serialNo, minSaleUnitCode, year+"");
			renderText("success");
		}catch (Exception e) {
			e.printStackTrace();
			renderText("fail");
		}
		return null;
	}
	
	/**
	 * 删除购物车中商品
	 * @return
	 */
	@Action(value="delShoppingCart")
	public String delShoppingCart(){
		try{
			deleteShoppingCartService.delShoppingCart(customerCode, serialNo,minSaleUnitCode);;
			renderText("success");
		}catch (Exception e) {
			e.printStackTrace();
			renderText("fail");
		}
		return null;
	}
	
	/**
	 * 查询购物车数量
	 * @return
	 */
	@Action(value="queryShoppingCartsSoftNumber")
	public String queryShoppingCartsSoftNumber(){
		int count = shoppingCartService.queryShoppingCartsSoftNumber(customerCode);
		renderText(count+"");
		return null;
	}
	
	public String getCustomerCode() {
		return customerCode;
	}


	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}


	public String getLanguageCode() {
		return languageCode;
	}


	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public int getIsSoft() {
		return isSoft;
	}

	public void setIsSoft(int isSoft) {
		this.isSoft = isSoft;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	
	
	
}
