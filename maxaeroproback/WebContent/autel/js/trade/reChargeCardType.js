
///////  声明全局变量 Begin  ///////////////////////////////////////////

//sealername控件
var $sealername;

//protypecode控件
var $protypecode;

//salecfgcode控件
var $salecfgcode;

//areacfgcode控件
var $areacfgcode;

//salecontractcode控件
var $salecontractcode;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	$sealername = $(".sealername");
	$protypecode = $(".protypecode");
	$salecfgcode = $(".salecfgcode");
	$areacfgcode = $(".areacfgcode");
	$salecontractcode = $(".salecontractcode");
	
	//初始化变量 End ////
	
	
	if($protypecode.length > 0){
		$protypecode.find("option").each(function (){
		if($(this).val() == $protypecode.attr("protypecode")) $(this).attr("selected", true);
		});
	}
	
	
	if($salecfgcode.length > 0){
		$salecfgcode.find("option").each(function (){
		if($(this).val() == $salecfgcode.attr("salecfgcode")) $(this).attr("selected", true);
		});
	}
	
	
	if($areacfgcode.length > 0){
		$areacfgcode.find("option").each(function (){
		if($(this).val() == $areacfgcode.attr("areacfgcode")) $(this).attr("selected", true);
		});
	}
	
	
	if($salecontractcode.length > 0){
		$salecontractcode.find("option").each(function (){
		if($(this).val() == $salecontractcode.attr("salecontractcode")) $(this).attr("selected", true);
		});
	}
	
	$(".addrechargecardtype").click(function(){
		var $this = $(this);
		addReChargeCardType();
	});
	
	$(".editrechargecardtype").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		editReChargeCardType(code);
	});
	
	$(".insertrechargecardtype").click(function(){
		var $this = $(this);
		
		insertReChargeCardType();
	});
	
	$(".updaterechargecardtype").click(function(){
		var $this = $(this);
		updateReChargeCardType();
	});
	
	$(".dellrechargecardtype").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delReChargeCardType(code);
	});
	
	$(".dellrechargecardtypes").click(function(){
		var $this = $(this);
		delReChargeCardTypes();
	});
});

//去到添加充值卡类型页面
function addReChargeCardType()
{
	$("#f1").attr("action","addrechargecardtype.do").submit();
}

//去到修改充值卡类型页面
function editReChargeCardType(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editrechargecardtype.do").submit();
}

//添加充值卡类型方法
function insertReChargeCardType()
{
	$("#f1").attr("action","insertrechargecardtype.do").submit();
}

//修改充值卡类型方法
function updateReChargeCardType()
{
	$("#f1").attr("action","updaterechargecardtype.do").submit();
}

//删除充值卡类型方法
function delReChargeCardType(code)
{
	if(confirm(message1)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delrechargecardtype.do").submit();
	}
}

//删除多个充值卡类型方法
function delReChargeCardTypes()
{
	if(confirm(message2)){		
		$("#f1").attr("action","delrechargecardtypes.do").submit();
	}
}

function selectContract(module){
	$.ajax({
		url:'getContractByProCode.do?ajax=yes',
		type:"post",
		data:{"proTypeCode":$(module).val()},
		dataType:'JSON',
		async: false,
		success :function(data){
			var jsonData = eval(eval(data));
			$('#SaleContractCode').empty();
			for(var i=0;i<jsonData.length;i++){
				$('#SaleContractCode').append("<option value="+jsonData[i].code+">"+jsonData[i].name+"</option>");
			}
		},
		error :function(data){
			
		}
	});
}


