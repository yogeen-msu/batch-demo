
///////  声明全局变量 Begin  ///////////////////////////////////////////

//membertypesel控件
var $membertypesel;

//showrechargerecord控件
var $showrechargerecord;

//membertype控件
var $membertype;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	$membertypesel = $(".membertypesel");
	$showrechargerecord = $(".showrechargerecord");
	$membertype = $(".membertype");
	
	//初始化变量 End ////
	
	/**
	 * 初始化会员类型
	 */
	if($membertype.length > 0){
		$membertype.each(function (){
			if($(this).attr("membertype") == "1") $(this).append("客户");
			if($(this).attr("membertype") == "2") $(this).append("经销商");
		});
	}
	
	/**
	 * 初始化会员类型选中状态
	 */
	if($membertypesel.length > 0){
		$membertypesel.find("option").each(function (){
		if($(this).val() == $membertypesel.attr("membertypesel")) $(this).attr("selected", true);
		});
	}
	
	$showrechargerecord.click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		showReChargeRecord(code);
	});
});



//修改经销商销售信息方法
function showReChargeRecord(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","showrechargerecord.do").submit();
}



