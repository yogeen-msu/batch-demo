
///////  声明全局变量 Begin  ///////////////////////////////////////////

//isusesel控件
var $isusesel;

//isactivesel控件
var $isactivesel;

//isuse控件
var $isuse;

//isactive控件
var $isactive;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	$isusesel = $(".isusesel");
	$isactivesel = $(".isactivesel");
	$isuse = $(".isuse");
	$isactive = $(".isactive");
	
	isYinhao = true;       //在字符串codes中每个code值加单引号
	
	//初始化变量 End ////
	
	
	
	/**
	 * 初始化使用状态选中状态
	 */
	if($isusesel.length > 0){
		$isusesel.find("option").each(function (){
		if($(this).val() == $isusesel.attr("isusesel")) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 初始化激活状态选中状态
	 */
	if($isactivesel.length > 0){
		$isactivesel.find("option").each(function (){
		if($(this).val() == $isactivesel.attr("isactivesel")) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 初始化使用状态
	 */
	if($isuse.length > 0){
		$isuse.each(function (){
			if($(this).parent().parent().attr("isuse") == "1") $(this).append(nouse_str);
			if($(this).parent().parent().attr("isuse") == "2") $(this).append(isused_str);
		});
	}
	
	
	/**
	 * 初始化激活状态
	 */
	if($isactive.length > 0){
		$isactive.each(function (){
			if($(this).parent().parent().attr("isactive") == "1") $(this).append(noactivated_str);
			if($(this).parent().parent().attr("isactive") == "2") $(this).append(activated_str);
		});
	}
	
	
	//初始化操作栏
	$(".operate").each(function(){
		var $this = $(this);
		var isuse = $this.parent().attr("isuse");
		var isactive = $this.parent().attr("isactive");
		var isOverdue = $this.parent().attr("isOverdue");
		
		if (isuse=="1" && isactive=="1" && isOverdue=="1")
		{
			$this.html("<a href='#' class='activerechargecardinfo'>"+activate_str+"</a>&nbsp;");
		}
		else
		{
			$this.html(""+activate_str+"&nbsp;");
		}
		
		if (isuse=="1" && isactive=="2" && isOverdue=="1")
		{
			$this.append(del_str);
		}
		else
		{
			$this.append("<a href='#' class='dellrechargecardinfo'>"+del_str+"</a>");
		}
	});
	
	
	$(".createrechargecardinfo").click(function(){
		var $this = $(this);
		$("#f1").attr("action","createrechargecardinfo.do").submit();
	});
	
	$(".savecreaterechargecard").click(function(){
		var $this = $(this);
		
		if(!checkData(document.getElementsByName("reChargeCardInfoAdd.reChargeCardTypeCode")[0].value,"string",true))
		{
			alert("请选择充值卡类型");
			document.getElementsByName("reChargeCardInfoAdd.reChargeCardTypeCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("reChargeCardInfoAdd.sealerAutelId")[0].value,"string",true))
		{
			alert("请输入经销商编号");
			document.getElementsByName("reChargeCardInfoAdd.reChargeCardTypeCode")[0].focus();
			return false;
		}
		
		
		if(!checkData(document.getElementsByName("reChargeCardFile")[0].value,"excelfilename",true))
		{
			alert("请上传文件");
			document.getElementsByName("reChargeCardFile")[0].focus();
			return false;
		}
		
		//$("#f1").attr("action","savecreaterechargecard.do").attr("target","hidden_frame").submit();
		$("#f1").attr("action","savecreaterechargecard.do").submit();
	});
	
	$(".activerechargecardinfo").click(function(){
		var $this = $(this);
		var code = $this.parent().parent().attr("code");
		activeReChargeCardInfo(code);
	});
	
	$(".activerechargecardinfos").click(function(){
		var $this = $(this);
		if(checkednum>0)
			activeReChargeCardInfos();
		else
			alert(message2);
	});
	
	$(".dellrechargecardinfo").click(function(){
		var $this = $(this);
		var code = $this.parent().parent().attr("code");
		delReChargeCardInfo(code);
	});
	
	$(".dellrechargecardinfos").click(function(){
		var $this = $(this);
		if(checkednum>0)
			delReChargeCardInfos();
		else
			alert(message3);
	});
});

//激活充值卡方法
function activeReChargeCardInfo(code)
{
	if(confirm(message4)){
		$("#code").attr("value",code);
		$("#f1").attr("action","activerechargecardinfo.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'activerechargecardinfo.do?ajax=yes',
			type:"post",
			data:{"code":code},
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("充值卡激活成功");
			$("#f1").attr("action","listrechargecardinfo.do").submit();
		}
		else
		{
			alert("充值卡激活失败");
		}
		*/
	}
}

//激活多个充值卡方法
function activeReChargeCardInfos()
{
	if(confirm(message5)){
		$("#f1").attr("action","activerechargecardinfos.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'activerechargecardinfos.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("充值卡激活成功");
			$("#f1").attr("action","listrechargecardinfo.do").submit();
		}
		else
		{
			alert("充值卡激活失败");
		}
		*/
	}
}

//删除充值卡信息方法
function delReChargeCardInfo(code)
{
	if(confirm(message6)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delrechargecardinfo.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delrechargecardinfo.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("充值卡删除成功");
			$("#f1").attr("action","listrechargecardinfo.do").submit();
		}
		else
		{
			alert("充值卡删除失败");
		}
		*/
	}
}

//删除多个充值卡信息方法
function delReChargeCardInfos()
{
	if(confirm(message7)){
		$("#f1").attr("action","delrechargecardinfos.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delrechargecardinfos.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("充值卡删除成功");
			$("#f1").attr("action","listrechargecardinfo.do").submit();
		}
		else
		{
			alert("充值卡删除失败");
		}
		*/
	}
}


