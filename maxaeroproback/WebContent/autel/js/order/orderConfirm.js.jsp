<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 订单确认JS
 */
$(document).ready(function() {
	//全选
	$("input[name='chkAll']").click(function(){
		if($(this).attr("checked")){
			$("input[name='chk']").attr("checked","checked");
		}else{
			$("input[name='chk']").removeAttr("checked");
		}
	});
	
	/**
	 * 确认选中订单
	 */
	$("#confirmBtn").click(function(){
		var orderCodes = $("input[name='chk']:checked").map(function(){
			return $(this).val();
		}).get().join(",");
		jump("orderConfirm.do?orderCodes="+orderCodes);
	});
	
	/**
	 * 添加或是修改银行流水号
	 */
	$("#serFinishBtn").click(function(){
		var code = $("#bankNumber").attr("code");
		var oldBankNumber = $("#bankNumber").attr("bankNumber");
		var bankNumber = $("#bankNumber").val();
		
		if(bankNumber == ""){
			alert("<fmt:message key='orderman.js.orderconfirm.emptyFlownum' />");
			return false;
		}else if(isNaN(bankNumber)){
			alert("<fmt:message key='orderman.js.orderconfirm.flownum' />");
			return false;
		}
		if(oldBankNumber == bankNumber){
			alert("<fmt:message key='orderman.js.orderconfirm.addOrDelFlownum' />");
			return false;
		}
		jump("updateBankNumber.do?orderInfo.code="+code+"&orderInfo.bankNumber="+bankNumber);
	});
});
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}

function selfOpenDialog(opt) {

	var option = opt;

	option.disX = 0;
	option.disY = 0;

	if (option.id == null || option.btnId == null)
		return;

	option.ctx = $("#" + option.id);

	option.oWin = $(option.ctx).children("div .dialog_win");
	option.oLay = $(option.ctx).children("div .dialog_overlay");
	option.oBtn = $("a[id^='" + option.btnId + "']");
	option.oClose = $(option.oWin).find(".dialog_close");
	option.oH2 = $(option.oClose).parent();

	$(option.oBtn).click(function() {
		var action = $(this).attr("action");
		var code = $(this).attr("code");
		$("#bankNumber").attr("code",code);
		if (action == "update") {
			var bankNumber = $(this).attr("bankNumber");
			$("#bankNumber").val(bankNumber);
			$("#bankNumber").attr("bankNumber",bankNumber);
		}else{
			$("#bankNumber").attr("bankNumber","");
		}
		$(option.oLay).css({
			"display" : "block"
		});
		$(option.oWin).css({
			"display" : "block"
		});
	});

	$(option.oClose).click(
			function() {
				$(option.oLay).css({
					"display" : "none"
				});
				$(option.oWin).css({
					"display" : "none"
				});

				if (option.callback != null
						&& typeof (option.callback) == "function") {
					option.callback();
				}
			});

	$(option.oClose).mousedown(function(event) {
		(event || window.event).cancelBubble = true;
	});

	this.close = function() {
		$(option.oLay).css({
			"display" : "none"
		});
		$(option.oWin).css({
			"display" : "none"
		});
		if (option.callback != null
				&& typeof (option.callback) == "function") {
			option.callback();
		}
	};

	selfOpenDialog.close = function() {
		$(option.oLay).css({
			"display" : "none"
		});
		$(option.oWin).css({
			"display" : "none"
		});
		if (option.callback != null
				&& typeof (option.callback) == "function") {
			option.callback();
		}
	};
}