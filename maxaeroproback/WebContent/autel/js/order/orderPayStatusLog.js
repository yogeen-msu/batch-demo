


$(document).ready(function(){
	
	$(".addorderpaystatuslog").click(function(){
		var $this = $(this);
		addOrderPayStatusLog();
	});
	
	$(".insertorderpaystatuslog").click(function(){
		var $this = $(this);
		insertOrderPayStatusLog();
	});
});

//去到添加订单修改信息页面
function addOrderPayStatusLog()
{
	$("#f1").attr("action","addorderpaystatuslog.do").submit();
}

//添加订单修改信息方法
function insertOrderPayStatusLog()
{
	$("#f1").attr("action","insertorderpaystatuslog.do").submit();
}

//验证是否未支付有效订单
function checkOrderNopay(orderCode)
{
	var result = -1;
	$.ajax({
		url:'paystatuscheckordernopay.do?ajax=yes',
		type:"post",
		data:{"orderPayStatusLog.orderCode":orderCode},
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
		return true;
	else
		return false;
}

//验证订单号有效性
function checkOrderCode(orderCode)
{
	var result = -1;
	$.ajax({
		url:'paystatuscheckordercode.do?ajax=yes',
		type:"post",
		data:{"orderPayStatusLog.orderCode":orderCode},
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
		return true;
	else
		return false;
}
