<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 订单列表JS
 */
$(document).ready(function() {
	//全选
	$("input[name='chkAll']").click(function(){
		if($(this).attr("checked")){
			$("input[name='chk'][disabled!='disabled']").attr("checked","checked");
		}else{
			$("input[name='chk']").removeAttr("checked");
		}
	});
	
	/**
	 * 更新有效期
	 */
	$("#updateYearBtn").click(function(){
		var orderCodes = $("input[name='chk']:checked").map(function(){
			if(!$(this).attr("disabled")){
				return $(this).val();
			}
		}).get().join(",");
		
		if(orderCodes != null && orderCodes != ""){
			jump("processState.do?code="+orderCodes);
			/*
			$.ajax({
				url:"processState.do?ajax=yes",
				type:"post",
				data:{"code":orderCodes},
				dataType:"json",
				success:function(data){
					var jsonData = eval(data);
					if(jsonData[0].flag == "true"){
					
						alert("<fmt:message key='orderman.js.updatevalidateSuccess' />");
						window.location.reload(true);
						return false;
					}else{
						alert("<fmt:message key='orderman.js.updatevalidateFail' />");
						return false;
					}
				},
				error:function(data){
					alert("<fmt:message key='common.js.system.error' />");
					return false;
				}
			});*/
		}else{
			alert("<fmt:message key='orderman.js.chooseOrder' />");
			return false;
		}
	});
	
	
	/**
	 * 更新产品有效期
	 */
	$("a[name^='updateaLink']").click(function() {
		var code = $(this).attr("code");
		jump("processState.do?code="+code);
		/*
		$.ajax({
			url:"processState.do?ajax=yes",
			type:"post",
			data:{"code":code},
			dataType:"JSON",
			success:function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("<fmt:message key='orderman.js.updatevalidateSuccess' />");
					window.location.reload(true);
					return false;
				}else{
					alert("<fmt:message key='orderman.js.updatevalidateFail' />");
					return false;
				}
			},
			error:function(data){
				alert("<fmt:message key='common.js.system.error' />");
				return false;
			}
		});*/
	});
	
	
});
function jump(url){
		var e=document.createElement("a");
		e.href=url;
		document.body.appendChild(e);
		e.click();
	}

