/**
 * 订单列表JS
 */
$(document).ready(function() {
	//全选
	$("input[name='chkAll']").click(function(){
		if($(this).attr("checked")){
			$("input[name='chk'][disabled!='disabled']").attr("checked","checked");
		}else{
			$("input[name='chk']").removeAttr("checked");
		}
	});
	
	/**
	 * 更新有效期
	 */
	$("#updateYearBtn").click(function(){
		var orderCodes = $("input[name='chk']:checked").map(function(){
			if(!$(this).attr("disabled")){
				return $(this).val();
			}
		}).get().join(",");
		
		if(orderCodes != null && orderCodes != ""){
			$.ajax({
				url:"processState.do?ajax=yes",
				type:"post",
				data:{"code":orderCodes},
				dataType:"json",
				success:function(data){
					var jsonData = eval(data);
					if(jsonData[0].flag == "true"){
						alert("更新有效期成功");
						window.location.reload(true);
						return false;
					}else{
						alert("更新有效期失败");
						return false;
					}
				},
				error:function(data){
					alert("系统异常,请联系管理员");
					return false;
				}
			});
		}else{
			alert("请选择订单");
			return false;
		}
	});
	
	
	/**
	 * 更新产品有效期
	 */
	$("a[name^='updateaLink']").click(function() {
		var code = $(this).attr("code");
		$.ajax({
			url:"processState.do?ajax=yes",
			type:"post",
			data:{"code":code},
			dataType:"JSON",
			success:function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("更新有效期成功");
					window.location.reload(true);
					return false;
				}else{
					alert("更新有效期失败");
					return false;
				}
			},
			error:function(data){
				alert("系统异常,请联系管理员");
				return false;
			}
		});
	});
	
	
});


