
///////  声明全局变量 Begin  ///////////////////////////////////////////

//选中code字符串控件
var $selobj;

//复选框
var $checkbs;

//全选框
var $allckbs;

//code组成的字符串中每个code值要不要加单引号（true：要加单引号，false：不加单引号）
var isYinhao = false;

//复选框数量
var checkbnum = 0;

//选中的复选框数量
var checkednum = 0;

//sealername控件
var $sealername;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	/**
	$selobj = $("#selectedcodes");
	$checkbs = $("input[name='checkboxcode']");
	$allckbs = $("input[name='allCheckbox']");
	isYinhao = true;
	checkbnum = $checkbs.length;
	*/
	$sealername = $(".sealername");
	
	//初始化变量 End ////
	
	
	if($sealername.length > 0){
		$sealername.find("option").each(function (){
		if($(this).val() == $sealername.attr("sealername")) $(this).attr("selected", true);
		});
	}
	
	$(".addorderinfolog").click(function(){
		var $this = $(this);
		addOrderInfoLog();
	});
	
	$(".editorderinfolog").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		var orderCode = $this.attr("orderCode");
		if(!checkOrderNopay(orderCode))
		{
			alert(message3);
			return false;
		}
		editOrderInfoLog(code);
	});
	
	$(".insertorderinfolog").click(function(){
		var $this = $(this);
		insertOrderInfoLog();
	});
	
	$(".updateorderinfolog").click(function(){
		var $this = $(this);
		updateOrderInfoLog();
	});
	
	$(".dellorderinfolog").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		var orderCode = $this.attr("orderCode");
		if(!checkOrderNopay(orderCode))
		{
			alert(message3);
			return false;
		}
		delOrderInfoLog(code);
	});
	
	$(".dellorderinfologs").click(function(){
		var $this = $(this);
		delOrderInfoLogs();
	});
	
	/**
	
	$checkbs.click(function(){
		var $this = $(this);
		xuanze($this);
	});
	
	$allckbs.click(function(){
		var $this = $(this);
		quanxuan($this);
	});
	*/
});

//去到添加订单修改信息页面
function addOrderInfoLog()
{
	$("#f1").attr("action","addorderinfolog.do").submit();
}

//去到修改订单修改信息页面
function editOrderInfoLog(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editorderinfolog.do").submit();
}

//添加订单修改信息方法
function insertOrderInfoLog()
{
	$("#f1").attr("action","insertorderinfolog.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'insertorderinfolog.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("订单修改信息添加成功");
		$("#f1").attr("action","listorderinfolog.do").submit();
	}
	else
	{
		alert("订单修改信息添加失败");
	}
	*/
}

//修改订单修改信息方法
function updateOrderInfoLog()
{
	$("#f1").attr("action","updateorderinfolog.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'updateorderinfolog.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("订单修改信息修改成功");
		$("#f1").attr("action","listorderinfolog.do").submit();
	}
	else
	{
		alert("订单修改信息修改失败");
	}
	*/
}

//删除订单修改信息方法
function delOrderInfoLog(code)
{
	if(confirm(message1)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delorderinfolog.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delorderinfolog.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("订单修改信息删除成功");
			$("#f1").attr("action","listorderinfolog.do").submit();
		}
		else if(result == 11)
		{
			alert("订单修改信息已被使用，无法删除");
		}
		else
		{
			alert("订单修改信息删除失败");
		}
		*/
	}
}

//删除多个订单修改信息方法
function delOrderInfoLogs()
{
	if(confirm(message2)){
		$("#f1").attr("action","delorderinfologs.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delorderinfologs.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("订单修改信息删除成功");
			$("#f1").attr("action","listorderinfolog.do").submit();
		}
		else if(result == 11)
		{
			alert("订单修改信息已被使用，无法删除");
		}
		else
		{
			alert("订单修改信息删除失败");
		}
		*/
	}
}

//验证是否未支付有效订单
function checkOrderNopay(orderCode)
{
	var result = -1;
	$.ajax({
		url:'checkordernopay.do?ajax=yes',
		type:"post",
		data:{"orderInfoLog.orderCode":orderCode},
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
		return true;
	else
		return false;
}

//验证订单号有效性
function checkOrderCode(orderCode)
{
	var result = -1;
	$.ajax({
		url:'checkordercode.do?ajax=yes',
		type:"post",
		data:{"orderInfoLog.orderCode":orderCode},
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
		return true;
	else
		return false;
}

//获取订单价格
function getOrderMoney(orderCode)
{
	var result=0;
	var orderMoney = "";
	$.ajax({
		url:'getordermoney.do?ajax=yes',
		type:"post",
		data:{"orderInfoLog.orderCode":orderCode},
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			orderMoney = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	return orderMoney;
}


//复选方法///////////////
function xuanze($event)
{
	var selcos = $selobj.attr("value");
	var code = $event.attr("value");
	
	if(isYinhao) code = "'"+code+"'";
	
	if($event.attr("checked"))
	{
		if(selcos=="") selcos=code;
			else selcos=selcos+","+code;
		checkednum++;

		if(checkednum==checkbnum)
		{
			$allckbs.each(function(){
				$(this).attr("checked",true);
			});
		}
	}
	else
	{		
		$allckbs.each(function(){
			$(this).attr("checked",false);
		});
		
		var sarr = selcos.split(",");
		if(sarr!=null&&sarr.length>0){
			for(var i=0;i<sarr.length;i++){
				if(code==sarr[i]){
					if(i==0){
						sarr = sarr.slice(i+1,sarr.length);
					}else{
						sarr = sarr.slice(0,i).concat(sarr.slice(i+i,sarr.length));
					}
					
					checkednum--;
				}
			}
		}
		
		if(sarr.length>0){
			selcos = sarr.join(",");
		}else{
			selcos="";
		}
	}
	$selobj.attr("value",selcos);
}

//全选方法////////////
function quanxuan($event)
{
	var flag=false;
	
	if ($event.attr("checked"))
	{
		flag=true;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	else
	{
		flag=false;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	
	if ($checkbs.length>0)
	{
	   for (var i=0;i<$checkbs.length;i++)
		{
			if(flag)
			{
				if(!$checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}
			else
			{
				if($checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}			 
		}
	}
}
