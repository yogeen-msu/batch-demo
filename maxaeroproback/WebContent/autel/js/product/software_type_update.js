
/**
 * 添加软件版本JS
 */
$(document).ready(function() {

	/**
	 * 确认
	 */
	
	$("#finishBtn").click(function(){
		var typeName = $("#typeName").val();
		if(typeName == ""){
			alert("功能软件名称不能为空");
			return false;
		}
		var carSign = $("#carSign").val();
		if(carSign == ""){
			alert("车标路径不能为空");
			return false;
		}
		
		var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
		if(carSign != ""){
			if (!patrn.exec(carSign)){
				alert("图片格式错误");
				return false ;
			}
		}
	
		$("#softwareForm").submit();
	});
	
});