/**
 * 产品型号图片管理JS
 */
$(document).ready(function(){
	
	$("#addPicTr").click(function(){
		var $table = $("#picTable");
		$.ajax({
			url:'queryLanguageJson.do?ajax=yes',
			type:"post",
			data:{"productType.code":$("#code").val()},
			dataType:'JSON',
			success :function(data){
				if(data != null){
					var selectHtml = buildSelect(data,"");
					var tr = "<tr languageCode='' picId=''>";
					tr += "<td>"+selectHtml+"</td>";
					tr += "<td>";
					tr += "<input type='file' name='picPathFile'/>";
					tr += "</td>";
					tr += "<td class='caoz'>";
					tr += "<a href='#' action='save' onclick='productType(this);'>"+"保存"+"</a>";
					tr += "&nbsp;&nbsp;";
					tr += "<a href='#' onclick='cancel(this);'>"+"取消"+"</a>";
					tr += "</td>";
					tr += "</tr>";
					$table.append(tr);
				}
				
			},
			error :function(){
				alert("系统异常、请联系管理员....");
			}
		});
	});
});
/**
 * 生成下拉框
 * @param data
 */
function buildSelect(data,option){
	var jsonData = eval(data);
	var select = "<select class='act_zt' name='languageSelect'>";
	if(option != ""){
		select += option;
	}
	for(var i = 0 ; i < jsonData.length; i ++){
		select += "<option value='"+jsonData[i].code+"'>"+jsonData[i].name+"</option>";
	}
	select += "</select>";
	return select;
}
/**
 * @param event	保存语言图片
 */
function productType(event){
	var $this = $(event);
	var action = $this.attr("action");
	//save方法
	if(action == "save"){
		saveProductTypePic(event);
	}else if(action == "update"){
		//修改
		toUpdateProductTypePic(event);
	}
}

/**
 * 添加语言图片路径
 * @param event
 */
function saveProductTypePic(event){
	var $this = $(event);
	
	var $picTd = $this.parent().prev();
	//图片路径
	var $picPathInput = $picTd.find("input");
	var picPath= $picPathInput.val();
	if(picPath == ""){
		alert("图片路径不能为空");
		return false;
	}
	
	var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
	if (!patrn.exec(picPath)){
		alert("图片格式错误");
		return false ;
	}
	//语言选择
	var $languageTd = $this.parent().prev().prev();
	var $languageSelect = $languageTd.find("select");
	//语言code
	var languageCode = $languageSelect.val();
	if(languageCode == ""){
		alert("必须选择语言");
		return false;
	}
	var $form = $("<form action='addSerialPicPath.do' method='post' enctype='multipart/form-data'></form>");
	
	$form.append("<input type='hidden' name='productType.code' value='"+$("#code").val()+"'>");
	$form.append("<input type='hidden' name='productTypeSerialPicVo.languageCode' value='"+languageCode+"'>");
	var $div=$("<div style='display: none;'></div>");
	$div.append($this.closest("tr").find("input[type='file']").clone());
	$form.append($div);
	$("body").append($form);
	$form.submit();
}

/**
 * 跳转到可编辑
 * @param event
 */
function toUpdateProductTypePic(event){
	var $this = $(event);
	
	//修改时 点击取消即还原修改数据
	var $next = $this.next();
	
	//图片路径列
	var $picTd = $this.parent().prev();
	//语言名称列
	var $languageTd = $this.parent().prev().prev();
	//语言名称
	var languageName = $languageTd.html();
	//事件触发行
	var $tr = $this.closest("tr");
	//语言code
	var languageCode = $tr.attr("languageCode");
	
	$.ajax({
		url:'queryLanguageJson.do?ajax=yes',
		type:"post",
		data:{"productType.code":$("#code").val()},
		dataType:'JSON',
		success :function(data){
			var option = "<option value='"+languageCode+"'>"+languageName+"</option>";
			if(data != null){
				var selectHtml = buildSelect(data,option);
				$languageTd.html(selectHtml);
			}else{
				var select = "<select name='languageSelect'>";
				select += option;
				select += "</select>";
				$languageTd.html(select);
			}
			var picPathinput = "<input type='file' name='picPathFile'/>";
			$picTd.html(picPathinput);
			$next.attr("onclick","resetData(this);");
			$next.html("取消");
			$this.html("保存");
			var method = 'updateLanguagePic(this);';
			$this.attr("onclick",method);
			
		}
	});
	
}

/**
 * 事件触发源
 * @param event	
 */
function updateLanguagePic(event){
	var $this = $(event);
	
	//事件触发行
	var $tr = $this.closest("tr");
	//picId
	var picId = $tr.attr("picId");
	var $picTd = $this.parent().prev();
	//图片路径
	var $picPathInput = $picTd.find("input");
	
	var picPath= $picPathInput.val();
	if(picPath == ""){
		alert("图片路径不能为空");
		return false;
	}
	
	var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
	if (!patrn.exec(picPath)){
		alert("图片格式错误");
		return false ;
	}
	
	//语言选择
	var $languageTd = $this.parent().prev().prev();
	var $languageSelect = $languageTd.find("select");
	//语言code
	var languageCode = $languageSelect.val();
	if(languageCode == ""){
		alert("必须选择语言");
		return false;
	}
	
	var $form = $("<form action='updateLanguagePic.do' method='post' enctype='multipart/form-data'></form>");
	
	$form.append("<input type='hidden' name='productType.code' value='"+$("#code").val()+"'>");
	$form.append("<input type='hidden' name='productTypeSerialPicVo.languageCode' value='"+languageCode+"'>");
	$form.append("<input type='hidden' name='productTypeSerialPicVo.picId' value='"+picId+"'>");
	var $div=$("<div style='display: none;'></div>");
	$div.append($this.closest("tr").find("input[type='file']").clone());
	$form.append($div);
	$("body").append($form);
	$form.submit();
	
	
}

/**
 * 还原数据
 * @param event
 */
function resetData(event){
	var $this = $(event);
	//事件触发行
	var $tr = $this.closest("tr");
	//语言名称
	var languageName = $tr.attr("languageName");
	//图片路径
	var picPath = $tr.attr("picPath");
	
	var languageCode = $tr.attr("languageCode");
	
	//图片路径列
	var $picTd = $this.parent().prev();
	//语言名称列
	var $languageTd = $this.parent().prev().prev();
	
	$languageTd.html(languageName);
	$picTd.html(picPath);
	
	$this.prev().attr("action","update");
	$this.prev().attr("onclick","productType(this);");
	$this.prev().html("修改");
	//重新绑定为删除事件
	$this.attr("onclick","delProductPic(this,'"+$("#code").val()+"','"+languageCode+"');");
	$this.html("删除");
	
}

/**
 * 删除语言图片
 * @param code	产品型号编码
 */
function delProductPic(event,code,languageCode){
	if(confirm("是否确认删除")){
		$.ajax({
			url:'delProductPic.do?ajax=yes',
			type:"post",
			data:{"productType.code":code,
				"productTypeSerialPicVo.languageCode":languageCode},
				dataType:'JSON',
				success :function(data){
					cancel(event);
					alert("删除成功...");
				},
				error :function(){
					alert("数据使用中,无法删除");
				}
		});
	}
}

/**
 * 取消
 * @param event
 */
function cancel(event){
	var $tr = $(event).closest("tr");
	$tr.remove();
}