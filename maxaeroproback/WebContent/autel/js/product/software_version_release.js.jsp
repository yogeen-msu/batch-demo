<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 发布软件版本js
 */
$(document).ready(function(){
	
	/**
	 * 全选反选
	 */
	$("input[type='checkbox'][name^='chkAll']").click(function(){
		if($(this).attr("checked")){
			$("input[type='checkbox'][name='chk']").attr("checked",$(this).attr("checked"));
		}else{
			$("input[type='checkbox'][name='chk']").removeAttr("checked");
		}
	});
	
	/**
	 * 发布或是未发布
	 */
	$("input[name^='relealseBtn']").click(function(){
		var state = $(this).attr("state");
		var code = $(this).attr("code");
		var $this = $(this);
		var $prevTd = $this.closest("td").prev();;
		$.ajax({
			url:'releaseVersion.do?ajax=yes',
			type:"post",
			data:{"state":state,"code":code},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag){
					//不发布
					if(state == "0"){
						$this.val("<fmt:message key='cententman.messageman.publish' />");
						$this.attr("state","1");
						$prevTd.html("<fmt:message key='product.software.version.publishstate.no' />");
					}else if(state == "1"){
						$this.val("<fmt:message key='product.software.publish.no' />");
						$this.attr("state","0");
						$prevTd.html("<fmt:message key='product.software.version.publishstate.yes' />");
					}
					alert("<fmt:message key='common.js.modSuccess' />");
				}else{
					alert("<fmt:message key='common.js.modFail' />");
				}
			},
			error :function(){
				alert("<fmt:message key='common.js.system.error' />");
				return false;
			}
		});
		
	});
	
	/**
	 * 发布或是未发布
	 */
	$("#releaseBtn,#notReleaseBtn").click(function(){
		var state = $(this).attr("state");
		
		var array = new Array();
		
		$("input[type='checkbox'][name='chk']").each(function(){
			if($(this).attr("checked")){
				array.push($(this).val());
			}
		});
		
		if(array.length == 0){
		    if(state==0){
		    alert("<fmt:message key='sys.product.notrelease.select' />");
		    }else{
		    alert("<fmt:message key='sys.product.release.select' />");
		    }
			return;
		}
		
		var code = array.join(" ");
		
		$.ajax({
			url:'releaseVersion.do?ajax=yes',
			type:"post",
			data:{"state":state,"code":code},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag){
					var text = "";
					var btText ="";
					//不发布
					if(state == "0"){
						text = "<fmt:message key='product.software.version.publishstate.no' />";
						btText="<fmt:message key='cententman.messageman.publish' />";
					}else if(state == "1"){
						text = "<fmt:message key='product.software.version.publishstate.yes' />";
						btText="<fmt:message key='product.software.publish.no' />";
					}
					$("input[type='checkbox'][name='chk']").each(function(){
						if($(this).attr("checked")){
							$(this).closest("tr").find("td").eq(4).html(text);;
							$("#relealseBtn_"+$(this).val()).val(btText);
						}
					});
					alert("<fmt:message key='common.js.modSuccess' />");
				}else{
					alert("<fmt:message key='common.js.modFail' />");
				}
			},
			error :function(){
				alert("<fmt:message key='common.js.system.error' />");
			}
		});
		
	});
});