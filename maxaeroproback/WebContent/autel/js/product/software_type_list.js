
/**
 * 
 * @param event this
 * @param code	编码
 */
function deleteSoftwareType(event,code){
	var $this = $(event);
	var $tr = $this.closest("tr");
	if(confirm("确认删除吗?")){
		$.ajax({
			url:"deleteSoftwareType.do?ajax=yes",
			type:"POST",
			data:{"softwareType.code":code},
			dataType:"JSON",
			success:function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("删除成功!");
					$tr.remove();
				}else{
					alert("删除失败!");
				}
			},
			error:function(data){
				alert("数据使用中,无法删除");
				return false;
			}
		});
	}
}