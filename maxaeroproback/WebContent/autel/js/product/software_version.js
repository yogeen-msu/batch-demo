/**
 * 软件版本JS
 */

var languagePackArray = new Array();

$(document).ready(function(){
	
	var bool = false;
	
	var languageArray = new Array();
	
	/**
	 * 添加语言包事件
	 */
	
	$("#addLanguagePackBtn").click(function(){
		
		var oWin = $("#win");
		var oLay = $("#overlay");
		
		oLay.show();
		oWin.show();
		
		$("#win").find("select option").each(function(){
			if(bool){
				return false;
			}
			//option选项的值
			var languageCode = $(this).val();
			//option选项的文本
			var languageName = $(this).text();
			
			var language = new Language(languageCode,languageName);
			languageArray.push(language);
		});
		bool = true;;
	});
	
	/**
	 * 确定
	 */
	$("#finsh").click(function(){
		
		//语言类型
		var languageCode = $("#language option:selected").val();
		//语言名称
		var languageName = $("#language option:selected").text();
		if(languageCode == "" || languageName == ""){
			alert("语言包类型不能为空");
			return false;
		}
		//语言包相对路径
		var downloadPath = $("#downloadPath").val();
		if(downloadPath == ""){
			alert("语言包相对路径不能为空");
			return false;
		}
		//可测车型文档路径
		var testPath = $("#testPath").val();
		if(testPath == ""){
			alert("可测车型文档路径不能为空");
			return false;
		}
		//升级公告标题
		var title = $("#title").val();
		if(title == ""){
			alert("升级公告标题不能为空");
			return false;
		}
		//版本说明
		var memo = $("#memo").val();
		
	    var languagePack = new LanguagePack(languageCode,languageName,downloadPath,testPath,title,memo);
	    languagePackArray.push(languagePack);
	    buildLanguagePackHtml(languagePack);
	});
	
	/**
	 * 数据提交
	 */
	$("#sumbitBtn").click(function(){
		var versionName = $("#versionName").val();
		if(versionName == ""){
			alert("版本不能为空");
			return false;
		}
		var softwarePackPath = $("#softwarePackPath").val();
		if(softwarePackPath == ""){
			alert("基础包相对路径不能为空");
			return false;
		}
		var prefix = "languagePacks";
		var $form = $("#softwareVersionForm");
		var len = languagePackArray.length;
		if(len == 0){
			alert("请添加语言包");
			return false;
		}
		var html = "";
		for(var i = 0; i<len ; i ++){
			var languagePack = languagePackArray[i];
			html += "<input type='hidden' name='"+prefix+"["+i+"].languageTypeCode"+"' value='"+languagePack.languageTypeCode+"'/>";
			html += "<input type='hidden' name='"+prefix+"["+i+"].title"+"' value='"+languagePack.title+"'/>";
			html += "<input type='hidden' name='"+prefix+"["+i+"].downloadPath"+"' value='"+languagePack.downloadPath+"'/>";
			html += "<input type='hidden' name='"+prefix+"["+i+"].testPath"+"' value='"+languagePack.testPath+"'/>";
			html += "<input type='hidden' name='"+prefix+"["+i+"].memo"+"' value='"+languagePack.memo+"'/>";
		}
		$form.append(html);
		$form.submit();
	});
});

function buildLanguagePackHtml(languagePack){
	//移除选择的语言
	$("#win").find("select option:selected").remove();;
	
	var $tr = $("<tr></tr>");
	var $languageTd = $("<td></td>");
	$languageTd.append(languagePack.languageName);
	
	var $downloadPathTd = $("<td></td>");
	$downloadPathTd.append(languagePack.downloadPath);
	
	var $testPathTd = $("<td></td>");
	$testPathTd.append(languagePack.testPath);
	
	var $memoTd = $("<td></td>");
	$memoTd.append(languagePack.memo);
	
	var $operTd = $("<td class='caoz'></td>");
	var $a = $("<a href='###' languageCode='"+languagePack.languageTypeCode+"' languageName='"+languagePack.languageName+"'>删除</a>");
	
	$a.click(function(){
		var $select = $("#win").find("select");
		var languageCode = $(this).attr("languageCode");
		var languageName = $(this).attr("languageName");
		$select.append("<option value='"+languageCode+"'>"+languageName+"</option>");
		$(this).closest("tr").remove();
		languagePackArray.removePack(languageCode);
	});
	
	$operTd.append($a);
	
	$tr.append($languageTd).append($downloadPathTd).append($testPathTd).append($memoTd).append($operTd);
	
	$("#languagePackTable").find("tr").last().before($tr);
	
	var oWin = $("#win");
	var oLay = $("#overlay");
	
	oLay.hide();
	oWin.hide();
	
}

Array.prototype.remove = function(language){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(language.languageCode == this[i].languageCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

/**
 * 删除语言包
 * @param languagePack
 */
Array.prototype.removePack = function(languageCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(languageCode == this[i].languageTypeCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

/**
 * 语言对象
 * @param languageCode	语言编码
 * @param languageName	语言名称
 */
function Language(languageCode,languageName){
	this.languageCode = languageCode;
	this.languageName = languageName;
}

/**
 * 软件语言包对象
 * @param softwareVersionCode	软件版本编码
 * @param languageTypeCode	语言类型
 * @param languageName 语言名称
 * @param downloadPath	语言包相对路径
 * @param testPath	可测车型文档路径
 * @param title	升级公告标题
 * @param memo	版本说明
 */
function LanguagePack(languageTypeCode,languageName,downloadPath,testPath,title,memo){
	this.languageTypeCode = languageTypeCode;
	this.languageName = languageName;
	this.downloadPath = downloadPath;
	this.testPath = testPath;
	this.title = title;
	this.memo = memo;
};
