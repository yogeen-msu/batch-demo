<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 添加软件版本JS
 */
$(document).ready(function() {

	/**
	 * 确认
	 */
	$("#finishBtn").click(function(){
		var typeName = $("#typeName").val();
		if(typeName == ""){
			alert("<fmt:message key='product.software.message2' />");
			return false;
		}
		var carSign = $("#carSign").val();
		if(carSign == ""){
			alert("<fmt:message key='product.software.message3' />");
			return false;
		}
		
		var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
		if(carSign != ""){
			if (!patrn.exec(carSign)){
				alert("<fmt:message key='common.js.formatError' />");
				return false ;
			}
		}
	
		$("#softwareForm").submit();
	});
	
});