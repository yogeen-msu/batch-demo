


/**
 * 初始化的语言选项
 */
var languageOption ;
var bool = true;

document.onkeypress = function(event) {
	if (event.keyCode == 13) {
		return false;
	}
};

$(document).ready(function(){

	//添加语言包
	$("#addLanguageFinishBtn").click(function(){
		var $this = $(this);
		var code = $("#addVersionCode").val();
		var languageTypeCode = $("#addLanguage option:selected").val();
		var downloadPath = $("#languageDownloadPathAdd").val();
		
		if(downloadPath == ""){
		
			alert("语言包相对路径不能为空");
			return false;
		}
		var testPath = $("#testPathAdd").val();
		if(testPath == ""){
			alert("可测车型文档路径不能为空");
			return false;
		}
		var title = $("#titleAdd").val();
		if(title == ""){
		
			alert("标题不能为空");
			return false;
		}
		var memo = $("#memoAdd").val();
		$.ajax({
			url:"addLanguagePack.do?ajax=yes",
			type:"POST",
			data:{"languagePack.softwareVersionCode":code,
				"languagePack.languageTypeCode":languageTypeCode,
				"languagePack.downloadPath":downloadPath,
				"languagePack.testPath":testPath,
				"languagePack.title":title,
				"languagePack.memo":memo},
			dataType:"json",
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("添加成功");
					$("#addVersionCode").val("");
					$("#languageDownloadPath").val("");
					$("#testPath").val("");
					$("#testPath").val("");
					$("#title").val("");
					$("#memo").val("");
					closeDiv($this);
					window.location.reload(true);
					return false;
				}else{
					alert("添加失败");
					return false;
				}
			},
			error:function(data){
				alert("<fmt:message key='common.js.system.error' />");
				return false;
			}
		});
	});
	
	//window.location.reload(true);
	$("#lianguageFinishBtn").click(function(){
		var $this = $(this);
		var code = $("#languagePackCode").val();
		var languageTypeCode = $("#language option:selected").val();
		var downloadPath = $("#languageDownloadPath").val();
		
		if(downloadPath == ""){
			alert("语言包相对路径不能为空");
			return false;
		}
		var testPath = $("#testPath").val();
		if(testPath == ""){
			alert("可测车型文档路径不能为空");
			return false;
		}
		var title = $("#title").val();
		if(title == ""){
			alert("标题不能为空");
			return false;
		}
		var memo = $("#memo").val();
		$.ajax({
			url:"updateLanguagePack.do?ajax=yes",
			type:"POST",
			data:{"languagePack.code":code,
				"languagePack.languageTypeCode":languageTypeCode,
				"languagePack.downloadPath":downloadPath,
				"languagePack.testPath":testPath,
				"languagePack.title":title,
				"languagePack.memo":memo},
			dataType:"json",
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("修改成功");
					$("#languagePackCode").val("");
					$("#languageDownloadPath").val("");
					$("#testPath").val("");
					$("#testPath").val("");
					$("#title").val("");
					$("#memo").val("");
					closeDiv($this);
					window.location.reload(true);
					return false;
				}else{
					alert("修改失败");
					return false;
				}
			},
			error:function(data){
				alert("系统错误,请联系管理员");
				return false;
			}
		});
		
	});
	
	/**
	 * 修改版本
	 */
	$("#versionFinishBtn").click(function(){
		var $this = $(this);
		var versionCode = $("#versionCode").val();
		var versionName = $("#versionName").val();
		if(versionName == ""){
			alert("版本名称不能为空");
			return false;
		}
		var softwarePackCode = $("#softwarePackCode").val();
		var downloadPath = $("#downloadPath").val();
		if(downloadPath == ""){
			alert("基础包路径不能为空");
			return false;
		}
		$.ajax({
			url:"updateSoftversion.do?ajax=yes",
			type:"POST",
			data:{"softwareVersion.code":versionCode,
				"softwareVersion.versionName":versionName,
				"softwarePack.downloadPath":downloadPath,
				"softwarePack.code":softwarePackCode},
			dataType:"json",
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("修改成功");
					closeDiv($this);
					$("#versionCode").val("");
					$("#versionName").val("");
					$("#softwarePackCode").val("");
					$("#downloadPath").val("");
					window.location.reload(true);
					return false;
				}else{
					alert("修改失败");
					return false;
				}
			},
			error:function(data){
				alert("系统错误,请联系管理员");
				return false;
			}
		});
	});

	/**
	 * 异步发布版本
	 */
	$("input[name^='a1']").click(function(){
		if(confirm("确认修改?")){
			var state = $(this).val();
			var versionCode = $(this).attr("versionCode");
			$.ajax({
				url:'releaseVersion.do?ajax=yes',
				type:"post",
				data:{"state":state,"code":versionCode},
				dataType:'JSON',
				success :function(data){
					var jsonData = eval(data);
					if(jsonData[0].flag){
						window.location.reload(true);
						//alert("保存成功...");
					}else{
						//alert("保存失败...");
					}
				},
				error :function(){
					alert("系统异常,请联系管理员");
					return false;
				}
			});
		}else{
			window.location.reload(true);
		}
	});
	/**
	 * 删除软件版本
	 */
		$("a[name^='delSoftwareVersion']").click(function() {
			if(confirm("确认删除?")){
				var versionCode = $(this).attr("versionCode");
				var typeCode = $(this).attr("typeCode");
				$.ajax({
					url:"delSoftwareVersion.do?ajax=yes",
					type:"post",
					data:{"softwareVersion.code":versionCode,"softwareType.code":typeCode},
					dataType:"json",
					success:function(data){
						var jsonData = eval(data);
						if(jsonData[0].flag == "true"){
							alert("删除成功!");
							window.location.reload(true);
						}else{
							alert("删除失败!");
						}
					},
					error:function(data){
						alert("数据使用中,无法删除");
						return false;
					}
				});
			}
		});
	
		
	/**
	 * 删除语言包
	 */
	$("a[name^='delLanguagepack']").click(function() {
		var $tr = $(this).closest("tr");
		var languagePackCode = $(this).attr("languagePackCode");
		var typeCode = $(this).attr("typeCode");
		$.ajax({
			url:"delLanguagepack.do?ajax=yes",
			type:"post",
			data:{"languagePack.languagePackCode":languagePackCode,"softwareType.code":typeCode},
			dataType:"json",
			success:function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("删除成功!");
					$tr.remove();
				}else{
					alert("删除失败!");
				}
			},
			error:function(data){
				alert("数据使用中,无法删除");
				return false;
			}
		});
	});
});

/**
 * 弹出窗口
 * @param opt
 * @returns {selfOpenDialog}
 */
function selfOpenDialog(opt) {

	var option = opt;

	option.disX = 0;
	option.disY = 0;

	if (option.id == null || option.btnId == null)
		return;

	option.ctx = $("#" + option.id);

	option.oWin = $(option.ctx).children("div .dialog_win");
	option.oLay = $(option.ctx).children("div .dialog_overlay");
	option.oBtn = $("a[id^='" + option.btnId + "']");
	option.oClose = $(option.oWin).find(".dialog_close");
	option.oH2 = $(option.oClose).parent();

	$(option.oBtn).click(function() {
		var id =$(this).attr("id") ;
		if(id.indexOf("language") != -1){
			if(bool){
				//预先保存所有可供选择的语言
				languageOption = $("#language option").get();
				bool = false;
			}
			
			$(languageOption).each(function(i,item){
				$("#language").append(item);
			});
			//事件触发源
			var eventName  = $(this).attr("name");
			var lanCode  = $(this).attr("languageCode");
			
			//语言包编码
			var code = $(this).attr("code");
			//语言包路径
			var downloadPath = $(this).attr("downloadPath");
			//标题
			var title = $(this).attr("title");
			//可测文档路径
			var testPath = $(this).attr("testPath");
			//说明
			var memo = $(this).attr("memo");
			
			$("#languageDownloadPath").val(downloadPath);
			$("#testPath").val(testPath);
			$("#title").val(title);
			$("#memo").val(memo);
			$("#languagePackCode").val(code);
			
			$("a[name='"+eventName+"']").each(function(){
				var languageCode = $(this).attr("languageCode");
				if(lanCode != languageCode){
					$("#language option[value='"+languageCode+"']").remove();
				}
			});
		}else if(id.indexOf("version") != -1){
			//版本编码
			var code = $(this).attr("code");
			//版本名称
			var versionName = $(this).attr("versionName");
			//基础包编码s
			var softwarePackCode = $(this).attr("softwarePackCode");
			//基础包路径
			var softPackPath = $(this).attr("softPackPath");
			
			$("#versionCode").val(code);
			$("#versionName").val(versionName);
			$("#softwarePackCode").val(softwarePackCode);
			$("#downloadPath").val(softPackPath);
		}else if(id.indexOf("addLanguage") != -1){
			//添加语言
			//版本编码
			var code = $(this).attr("versionCode");
			$("#addVersionCode").val(code);
			$("#addLanguage").empty();
			$.ajax({
				url:"getAvailableLanguage.do?ajax=yes",
				type:"POST",
				data:{"softwareVersion.code":code},
				dataType:"json",
				success :function(data){
					var jsonData = eval(data);
					var len = jsonData.length;
					for(var i=0; i<len ;i++){
						var language = jsonData[i];
						var $option = $("<option value='"+language.code+"'>"+language.name+"</option>");
						$("#addLanguage").append($option);
					}
				},
				error:function(data){
					alert("异常");
					return false;
				}
			});
		}
		
		$(option.oLay).css({
			"display" : "block"
		});
		$(option.oWin).css({
			"display" : "block"
		});
	});

	$(option.oClose).click(
			function() {
				$(option.oLay).css({
					"display" : "none"
				});
				$(option.oWin).css({
					"display" : "none"
				});

				if (option.callback != null
						&& typeof (option.callback) == "function") {
					option.callback();
				}
			});

	$(option.oClose).mousedown(function(event) {
		(event || window.event).cancelBubble = true;
	});

	this.close = function() {
		$(option.oLay).css({
			"display" : "none"
		});
		$(option.oWin).css({
			"display" : "none"
		});
		if (option.callback != null
				&& typeof (option.callback) == "function") {
			option.callback();
		}
	};

	selfOpenDialog.close = function() {
		$(option.oLay).css({
			"display" : "none"
		});
		$(option.oWin).css({
			"display" : "none"
		});
		if (option.callback != null
				&& typeof (option.callback) == "function") {
			option.callback();
		}
	};
}

/**
 * 隐藏div
 * @param event
 */
function closeDiv(event){
	var overlay = $(event).closest("div.active").find(".dialog_overlay");
	var win = $(event).closest("div.active").find(".dialog_win");
	overlay.hide();
	win.hide();
}