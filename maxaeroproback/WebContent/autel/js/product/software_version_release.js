/**
 * 发布软件版本js
 */
$(document).ready(function(){
	
	/**
	 * 全选反选
	 */
	$("input[type='checkbox'][name^='chkAll']").click(function(){
		if($(this).attr("checked")){
			$("input[type='checkbox'][name='chk']").attr("checked",$(this).attr("checked"));
		}else{
			$("input[type='checkbox'][name='chk']").removeAttr("checked");
		}
	});
	
	/**
	 * 发布或是未发布
	 */
	$("input[name^='relealseBtn']").click(function(){
		var state = $(this).attr("state");
		var code = $(this).attr("code");
		var $this = $(this);
		var $prevTd = $this.closest("td").prev();;
		$.ajax({
			url:'releaseVersion.do?ajax=yes',
			type:"post",
			data:{"state":state,"code":code},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag){
					//不发布
					if(state == "0"){
						$this.val("已发布");
						$this.attr("state","1");
						$prevTd.html("未发布");
					}else if(state == "1"){
						$this.val("不发布");
						$this.attr("state","0");
						$prevTd.html("已发布");
					}
					alert("保存成功...");
				}else{
					alert("保存失败...");
				}
			},
			error :function(){
				alert("系统异常,请联系管理员");
				return false;
			}
		});
		
	});
	
	/**
	 * 发布或是未发布
	 */
	$("#releaseBtn,#notReleaseBtn").click(function(){
		var state = $(this).attr("state");
		
		var array = new Array();
		
		$("input[type='checkbox'][name='chk']").each(function(){
			if($(this).attr("checked")){
				array.push($(this).val());
			}
		});
		
		var code = array.join(" ");
		
		$.ajax({
			url:'releaseVersion.do?ajax=yes',
			type:"post",
			data:{"state":state,"code":code},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag){
					var text = "";
					//不发布
					if(state == "0"){
						text = "未发布";
					}else if(state == "1"){
						text = "已发布";
					}
					$("input[type='checkbox'][name='chk']").each(function(){
						if($(this).attr("checked")){
							$(this).closest("tr").find("td").eq(4).html(text);;
						}
					});
				}else{
					alert("保存失败...");
				}
			},
			error :function(){
				alert("系统异常、请联系管理员....");
			}
		});
		
	});
});