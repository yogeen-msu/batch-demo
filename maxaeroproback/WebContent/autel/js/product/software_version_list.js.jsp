<%@ include file="/autel/js/common/fmt.js.jsp"%>


/**
 * 初始化的语言选项
 */
var languageOption ;
var bool = true;


$(document).ready(function(){


   
	$("img[name^='showLanguageFlag']").click(function(){
	
	    var showLanguage=$(this).attr("showCode");
		if($(this).attr("name") == "m1"){
						$(this).attr("src","../../adminthemes/default/images/leg_11.gif");
						$(this).attr("name","m");
						$("tr").each(function(i){
						    if($(this).attr("id")==showLanguage){
						       $(this).hide();
						    }
						 });
						
						
		}else{
						$(this).attr("src","../../adminthemes/default/images/leg_08.gif");
						$(this).attr("name","m1");
						$("tr").each(function(i){
						    if($(this).attr("id")==showLanguage){
						       $(this).show();
						    }
						 });
		}
		<!-- $(this).next().next().toggle(); -->
	 
	
	});
	//添加语言包
	$("#addLanguageFinishBtn").click(function(){
		var $this = $(this);
		var code = $("#addVersionCode").val();
		var languageTypeCode = $("#addLanguage option:selected").val();
		if(languageTypeCode == "" || languageTypeCode == undefined){
			alert("<fmt:message key='product.softwareversion.sitelanguage.emptyLanguage' />");
			return false;
		}
		var downloadPath = $("#languageDownloadPathAdd").val();
		
		
		if(downloadPath == ""){
			alert("<fmt:message key='product.js.version.noLanguage' />");
			return false;
		}
		
		var testPath = $("#testPathAdd").val();
		if(testPath == ""){
			alert("<fmt:message key='product.js.version.noIstestcardoc' />");
			return false;
		}
		var title = $("#titleAdd").val();
		if(title == ""){
		
			alert("<fmt:message key='product.js.version.noTitle' />");
			return false;
		}
		var memo = $("#memoAdd").val();
		$("#addLanguagePackForm").submit();
	});
	

	//修改语言包
	$("#lianguageFinishBtn").click(function(){
		var $this = $(this);
		var code = $("#languagePackCode").val();
		var languageTypeCode = $("#language option:selected").val();
		var downloadPath = $("#languageDownloadPath").val();
		
		if(downloadPath == ""){
		
			alert("<fmt:message key='product.js.version.noLanguage' />");
			return false;
		}
		
		var testPath = $("#testPath").val();
		if(testPath == ""){
			alert("<fmt:message key='product.js.version.noIstestcardoc' />");
			return false;
		}
		var title = $("#title").val();
		if(title == ""){
		
			alert("<fmt:message key='product.js.version.noTitle' />");
			return false;
		}
		var memo = $("#memo").val();
		$("#languagePackForm").submit();
		
		
	});
	
	/**
	 * 修改版本
	 */
	$("#versionFinishBtn").click(function(){
		var $this = $(this);
		var versionCode = $("#versionCode").val();
		var versionName = $("#versionName").val();
		if(versionName == ""){
			alert("<fmt:message key='product.js.version.emptyVersionName' />");
			return false;
		}
		var softwarePackCode = $("#softwarePackCode").val();
		var downloadPath = $("#downloadPath").val();
		if(downloadPath == ""){
			alert("<fmt:message key='product.js.version.emptyBasepack' />");
			return false;
		}
		$.ajax({
			url:"updateSoftversion.do?ajax=yes",
			type:"POST",
			data:{"softwareVersion.code":versionCode,
				"softwareVersion.versionName":versionName,
				"softwarePack.downloadPath":downloadPath,
				"returnPage":$("#returnPage").val(),
				"softwarePack.code":softwarePackCode},
			dataType:"json",
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("<fmt:message key='common.js.modSuccess' />");
					closeDiv($this);
					$("#versionCode").val("");
					$("#versionName").val("");
					$("#softwarePackCode").val("");
					$("#downloadPath").val("");
					window.location.reload(true);
					return false;
				}else{
					alert("<fmt:message key='common.js.modFail' />");
					return false;
				}
			},
			error:function(data){
				alert("<fmt:message key='common.js.system.error' />");
				return false;
			}
		});
	});

	/**
	 * 异步发布版本
	 */
	$("input[name^='a1']").click(function(){
	    var releaseMsg="<fmt:message key='product.version.release.confirm' />";
	    var delMsg="<fmt:message key='product.version.release.confirm2' />"
	    var state = $(this).val();
	    var msg="";
	    if(state==0){
	    	msg=delMsg;
	    }
	    if(state==1){
	       msg=releaseMsg;
	    }
		if(confirm(msg)){
			
			var versionCode = $(this).attr("versionCode");
			$.ajax({
				url:'releaseVersion.do?ajax=yes',
				type:"post",
				data:{"state":state,"code":versionCode,"returnPage":$("#returnPage").val()},
				dataType:'JSON',
				success :function(data){
					var jsonData = eval(data);
					if(jsonData[0].flag){
						window.location.reload(true);
						//alert("保存成功...");
					}else{
						//alert("保存失败...");
					}
				},
				error :function(){
					alert("<fmt:message key='common.js.system.error' />");
					return false;
				}
			});
		}else{
			window.location.reload(true);
		}
	});
	
	/**
	 *批量删除版本
	 */
	$("#batchDelBtn").click(function(){
		var codes = $("input[name='chk']:checked").map(function(){
			return $(this).val();
		}).get().join(",");
		
		if(codes == ""){
			alert("<fmt:message key='product.js.version.chooseDelVersion' />");
			return false;
		}
		jump("batchDelSoftwareVersion.do?versionCodes="+codes+"&softwareType.code="+$("#typeCode").val()+"&returnPage="+$("#returnPage").val());
	});
	
	$("#batchDelPublishBtn").click(function(){
		var codes = $("input[name='chk']:checked").map(function(){
			return $(this).val();
		}).get().join(" ");
		
		if(codes == ""){
			alert("<fmt:message key='product.js.version.chooseDelVersion' />");
			return false;
		}
		jump(" batchDelPublishVersion.do?versionCodes="+codes+"&softwareType.code="+$("#typeCode").val()+"&returnPage="+$("#returnPage").val()+"&code="+codes+"&state=0");
	});
	
	
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
	/**
	 * 删除软件版本
	 */
		$("a[name^='delSoftwareVersion']").click(function() {
			if(confirm("<fmt:message key='common.js.confirmDel' />")){
				var versionCode = $(this).attr("versionCode");
				var typeCode = $(this).attr("typeCode");
				$.ajax({
					url:"delSoftwareVersion.do?ajax=yes",
					type:"post",
					data:{"softwareVersion.code":versionCode,"softwareType.code":typeCode,"returnPage":$("#returnPage").val()},
					dataType:"json",
					success:function(data){
						var jsonData = eval(data);
						if(jsonData[0].flag == "true"){
							alert("<fmt:message key='common.js.del' />");
							window.location.reload(true);
						}else{
							alert("<fmt:message key='common.js.delFail' />");
						}
					},
					error:function(data){
						alert("<fmt:message key='common.js.nonDel' />");
						return false;
					}
				});
			}
		});
	
		
	/**
	 * 删除语言包
	 */
	$("a[name^='delLanguagepack']").click(function() {
	
	    if(confirm("<fmt:message key='common.js.confirmDel' />")){
			var $tr = $(this).closest("tr");
			var languagePackCode = $(this).attr("languagePackCode");
			var typeCode = $(this).attr("typeCode");
			$.ajax({
				url:"delLanguagepack.do?ajax=yes",
				type:"post",
				data:{"languagePack.languagePackCode":languagePackCode,"softwareType.code":typeCode,"returnPage":$("#returnPage").val()},
				dataType:"json",
				success:function(data){
					var jsonData = eval(data);
					if(jsonData[0].flag == "true"){
						alert("<fmt:message key='common.js.del' />");
						$tr.remove();
					}else{
						alert("<fmt:message key='common.js.nonDel' />");
					}
				},
				error:function(data){
					alert("<fmt:message key='common.js.nonDel' />");
					return false;
				}
			});
		}
	});
	
});

/**
 * 弹出窗口
 * @param opt
 * @returns {selfOpenDialog}
 */
function selfOpenDialog(opt) {

	var option = opt;

	option.disX = 0;
	option.disY = 0;

	if (option.id == null || option.btnId == null)
		return;

	option.ctx = $("#" + option.id);

	option.oWin = $(option.ctx).children("div .dialog_win");
	option.oLay = $(option.ctx).children("div .dialog_overlay");
	option.oBtn = $("a[id^='" + option.btnId + "']");
	option.oClose = $(option.oWin).find(".dialog_close");
	option.oH2 = $(option.oClose).parent();

	$(option.oBtn).click(function() {
		var id =$(this).attr("id") ;
		if(id.indexOf("language") != -1){
			if(bool){
				//预先保存所有可供选择的语言
				languageOption = $("#language option").get();
				bool = false;
			}
			
			$(languageOption).each(function(i,item){
				$("#language").append(item);
			});
			//事件触发源
			var eventName  = $(this).attr("name");
			var lanCode  = $(this).attr("languageCode");
			
			//语言包编码
			var code = $(this).attr("code");
			
			//语言包路径
			var downloadPath = $(this).attr("downloadPath");
			//标题
			var title = $(this).attr("title");
			//可测文档路径
			var testPath = $(this).attr("testPath");
			//说明
			var memo = $(this).attr("memo");
			
			$("#languageDownloadPath").val(downloadPath);
			$("#testPath").val(testPath);
			$("#title").val(title);
			$("#memo").val(memo);
			$("#languagePackCode").val(code);
			$("#language").find("option[value="+lanCode+"]").attr("selected",true);
			
		}else if(id.indexOf("version") != -1){
			//版本编码
			var code = $(this).attr("code");
			//版本名称
			var versionName = $(this).attr("versionName");
			//基础包编码s
			var softwarePackCode = $(this).attr("softwarePackCode");
			//基础包路径
			var softPackPath = $(this).attr("softPackPath");
			
			$("#versionCode").val(code);
			$("#versionName").val(versionName);
			$("#softwarePackCode").val(softwarePackCode);
			$("#downloadPath").val(softPackPath);
		}else if(id.indexOf("addLanguage") != -1){
			//添加语言
			//版本编码
			var code = $(this).attr("versionCode");
			$("#addVersionCode").val(code);
			$("#addLanguage").empty();
			var show=1;
			$.ajax({
				url:"getAvailableLanguage.do?ajax=yes",
				type:"POST",
				data:{"softwareVersion.code":code},
				dataType:"json",
				async:false,
				success :function(data){
					var jsonData = eval(data);
					var len = jsonData.length;
					show=len;
					for(var i=0; i<len ;i++){
						var language = jsonData[i];
						var $option = $("<option value='"+language.code+"'>"+language.name+"</option>");
						$("#addLanguage").append($option);
					}
				},
				error:function(data){
					alert("<fmt:message key='common.js.system.error' />");
					return false;
				}
			});
		}
		if(show!=0){
			$(option.oLay).css({
				"display" : "block"
			});
			$(option.oWin).css({
				"display" : "block"
			});
		}
	});

	$(option.oClose).click(
			function() {
				$(option.oLay).css({
					"display" : "none"
				});
				$(option.oWin).css({
					"display" : "none"
				});

				if (option.callback != null
						&& typeof (option.callback) == "function") {
					option.callback();
				}
			});

	$(option.oClose).mousedown(function(event) {
		(event || window.event).cancelBubble = true;
	});

	this.close = function() {
		$(option.oLay).css({
			"display" : "none"
		});
		$(option.oWin).css({
			"display" : "none"
		});
		if (option.callback != null
				&& typeof (option.callback) == "function") {
			option.callback();
		}
	};

	selfOpenDialog.close = function() {
		$(option.oLay).css({
			"display" : "none"
		});
		$(option.oWin).css({
			"display" : "none"
		});
		if (option.callback != null
				&& typeof (option.callback) == "function") {
			option.callback();
		}
	};
}

/**
 * 隐藏div
 * @param event
 */
function closeDiv(event){
	var overlay = $(event).closest("div.active").find(".dialog_overlay");
	var win = $(event).closest("div.active").find(".dialog_win");
	overlay.hide();
	win.hide();
}