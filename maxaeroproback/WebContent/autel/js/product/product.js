/**
 * 产品型号管理JS
 */
$(document).ready(function(){
	$("#addProductType").click(function(){
		//console.log($(window.parent.document).find(".lanm dt a").get());
		/*
		$.each($(window.parent.document).find(".lanm dt a").get(),function(){
			if($(this).attr("class") == "current"){
				$(this).find("a").click();
			}
		});*/
		
		var $table = $("#productTypeTable").find("table");
		var tr = "<tr tName='' code=''>";
		tr += "<td></td>";
		tr += "<td>";
		tr += "<input type='text' name='productName' value='' maxlength='32'/>";
		tr += "</td>";
		
		
		tr += "<td>";
		tr += "<input type='file' name='picPathFile'/>";
		tr += "</td>";
		
		
		tr += "<td>";
		tr += "<input type='button' onclick='toSerialPicPath(this);' disabled='disabled' class='diw_but' value='路径管理'/>";
		tr += "</td>";
		tr += "<td class='caoz'>";
		tr += "<a href='#' action='save' onclick='productType(this);'>"+"保存"+"</a>";
		tr += "&nbsp;&nbsp;";
		tr += "<a href='#' onclick='cancel(this);'>"+"取消"+"</a>";
		tr += "</td>";
		tr += "</tr>";
		$table.append(tr);
	});
	
	//路径图片管理
	$("input[name^='serialPicPathBtn']").click(function(){
		toSerialPicPath(this);
	});
	
});

function toSerialPicPath(event){
	var code = $(event).parent().prev().prev().prev().text();
	var string = "toSerialPicPath.do?productType.code="+code;
	window.location = string;
}


function productType(event){
	var $this = $(event);
	var action = $this.attr("action");
	//save方法
	if(action == "save"){
		saveProductType(event);
	}else if(action == "update"){
		//修改
		toUpdateProductType(event);
	}
}

/**
 * 删除产品型号
 * @param code	产品型号编码
 */
function delProductType(event,code){
	if(confirm("是否确认删除")){
		$.ajax({
			url:'delProductType.do?ajax=yes',
			type:"post",
			data:{"productType.code":code},
			dataType:'JSON',
			success :function(data){
				cancel(event);
				alert("删除成功...");
				
				updateLeftMenu("del","",code);
				//刷新树
//				reloadFrames("leftProductFrame");
				window.location.reload(true);
			},
			error :function(){
				alert("数据使用中,无法删除");
			}
		});
	}
}


/**
 * 添加产品型号
 * @param event
 */
function saveProductType(event){
	var $this = $(event);
	
	var $tr = $this.closest("tr");
	
	
	
	
	var $productTd = $this.parent().prev().prev().prev();
	
	
	var $picTd =  $this.parent().prev().prev();
	//图片路径
	var $picPathInput = $picTd.find("input");
	var picPath= $picPathInput.val();
	if(picPath == ""){
		alert("图片路径不能为空");
		return false;
	}
	
	var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
	if (!patrn.exec(picPath)){
		alert("图片格式错误");
		return false ;
	}
	
	var $picInput = $this.parent().prev().find("input");
	var $input = $productTd.find("input");
	var $code = $this.parent().prev().prev().prev();
	var productName = $input.val();
	
	if(!valProductName(productName)){
		alert("产品型号名称不能为空,且必须由数字、字母、下划线组成");
		return false;
	}
	
	
	//var $form = $("<form action='saveProductType.do' method='post' enctype='multipart/form-data'></form>");
	var $form = $("#productForm");
	$form.attr("action","saveProductType.do");
	
	$form.append("<input type='hidden' name='productType.name' value='"+productName+"'>");
	//var $div=$("<div style='display: none;'></div>");
	//$div.append($this.closest("tr").find("input[type='file']").clone());
	//$form.append($div);
	//$("body").append($form);
	$form.submit();
	//updateLeftMenu("add",productName,code);
	
//	if(productName == ""){
//		alert("产品型号名称不能为空");
//		return false;
//	}
	//取消按钮
	
	/*
	var $next = $this.next();
	
	$.ajax({
		url:'saveProductType.do?ajax=yes',
		type:"post",
		data:{"productType.name":productName},
		dataType:'JSON',
		success :function(data){
			var jsonData = eval(data);
			var productName = jsonData[0].productName;
			var code = jsonData[0].code;
			$input.remove();
			$productTd.html(productName);
			$code.html(code);
			
			$tr.attr("tName",productName);
			$tr.attr("code",code);
			$next.attr("onclick","delProductType(this,'"+code+"')");
			$next.html("删除");
			
			$this.attr("action","update");
			$picInput.removeAttr("disabled");
			$this.html("修改");
			//刷新树
//			reloadFrames("leftProductFrame");
			
			alert("保存成功");
			
			updateLeftMenu("add",productName,code);
//			window.location.reload(true);
		},
		error :function(){
			alert("系统异常、请联系管理员....");
		}
	});
	*/
}

/**
 * 事件触发源
 * @param event
 */
function toUpdateProductType(event){
	
	var $this = $(event);
	
	//取消按钮
	var $next = $this.next();
	
	var $productTd = $this.parent().prev().prev().prev();
	var productName = $productTd.html();
	
	var $input = "<input type='text' name='productName' maxlength='32' value='"+productName+"'/>";
	$productTd.html($input);
	
	//文件上传
	var $picTd = $this.parent().prev().prev();
	var $fileInput="<input type='file' name='picPathFile'/>";
	$picTd.html($fileInput);
	
	$this.html("保存");
	
	$next.attr("onclick","resetData(this);");
	$next.html("取消");
	
	var method = 'updateProductType(this)';
	$this.attr("onclick",method);
	
}

/**
 * 修改产品型号
 * @param event
 */
function updateProductType(event){
	
	var $this = $(event);
	
	//取消按钮
	var $next = $this.next();
	
	var $productTd = $this.parent().prev().prev().prev();
	var $input = $productTd.find("input");
	var $code = $this.parent().prev().prev().prev().prev();
	var productName = $input.val();
	if(!valProductName(productName)){
		alert("产品型号名称不能为空,且必须由数字、字母、下划线组成");
		return false;
	}
	
	var code = $code.html();
	
	var $picTd =  $this.parent().prev().prev();
	//图片路径
	var $picPathInput = $picTd.find("input");
	var picPath= $picPathInput.val();
//	if(picPath == ""){
//		alert("图片路径不能为空");
//		return false;
//	}
	
	var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
	if(picPath != ""){
		if (!patrn.exec(picPath)){
			alert("图片格式错误");
			return false ;
		}
	}
	
	var $form = $("#productForm");
	$form.attr("action","updateProductType.do");
	
//	var $form = $("<form action='updateProductType.do' method='post' enctype='multipart/form-data'></form>");
	
	$form.append("<input type='hidden' name='productType.name' value='"+productName+"'>");
	$form.append("<input type='hidden' name='productType.code' value='"+code+"'>");
//	var $div=$("<div style='display: none;'></div>");
//	$div.append($this.closest("tr").find("input[type='file']").clone());
//	$form.append($div);
//	$("body").append($form);
	$form.submit();
	
	//console.log(productName,code);
	/*
	$.ajax({
		url:'updateProductType.do?ajax=yes',
		type:"post",
		data:{"productType.name":productName,"productType.code":code},
		dataType:'JSON',
		success :function(data){
			var jsonData = eval(data);
			var productName = jsonData[0].productName;
			$($input).remove();
			$productTd.html(productName);
			$this.attr("action","update");
			$this.attr("onclick","productType(this);");
			$this.html("修改");
			
			$next.attr("onclick","delProductType(this,'"+code+"')");
			$next.html("删除");
			
			//刷新树
//			reloadFrames("leftProductFrame");
			
			alert("修改成功");
//			window.location.reload(true);
			updateLeftMenu("update",productName,code);
			//console.log($($(window.parent.document).find("#left_menu_content")).find("a").size());
		},
		error :function(){
			alert("系统异常、请联系管理员....");
		}
	});
	*/
}

/**
 * 还原数据
 * @param event
 */
function resetData(event){
	var $this = $(event);
	//事件触发行
	var $tr = $this.closest("tr");
	var productTypeName = $tr.attr("tName");
	var code = $tr.attr("code");
	var picPath = $tr.attr("picPath");
	
	//产品型号名称列
	var $productTypeNameTd = $this.parent().prev().prev().prev();
	//产品型号编码列
	var $codeTd = $this.parent().prev().prev().prev().prev();
	
	//文件上传
	var $picTd = $this.parent().prev().prev();
	$picTd.html(picPath);
	
//	console.log(productTypeName,code);
	
	$productTypeNameTd.html(productTypeName);
	$codeTd.html(code);
	
	$this.prev().attr("action","update");
	$this.prev().attr("onclick","productType(this);");
	$this.prev().html("修改");
	//重新绑定为删除事件
	$this.attr("onclick","delProductType(this,'"+code+"');");
	$this.html("删除");
	
}

/**
 * 刷新frame
 * @param framesName frame名称
 */
function reloadFrames(framesName){
	window.parent.frames[framesName].location.reload();
}

/**
 * 取消
 * @param event
 */
function cancel(event){
	var $tr = $(event).closest("tr");
	$tr.remove();
}

/**
 * 验证产品名称是否由字母,数字,下划线组成
 * @param s
 * @returns {Boolean}
 */
function valProductName(s)
{
	//var patrn=/^[A-Za-z0-9_]+$/;
	var patrn=/^(\w){1,32}$/;
	if (!patrn.exec(s))
		return false;
	return true;
} 

/**
 * 添加 ,修改 , 删除
 * @param action
 * @param productName	产品名称
 * @param code	产品编码
 */
function updateLeftMenu(action , productName , code){
	//console.log($($(window.parent.document).find("#left_menu_content")).find(".lanm>dl>dt").last().html());
	if(action == "update" || action == "del"){
		$($(window.parent.document).find("#left_menu_content")).find("a").each(function(i,item){
			//左边树形菜单节点编码等于产品型号操作编码
			if($(this).attr("code") == code){
				if(action == "update"){
					$(this).html(productName);
					return false;
				}else if(action == "del"){
					$(this).parent().remove();
				}
			}
		});
	}else if(action == "add"){
//		var $a = $("<a href='"+ "../autel/product/toSoftwareType.do?productType.name=" +productName +"&productType.code="+code +"' code='"+code+"'>" + productName + "</a>");
		var $img = $('<img width="9" height="15" src="../adminthemes/default/images/leg_08.gif" name="m1">');
		var $a = $("<a code='"+code+"'>" + productName + "</a>");
		var $dt = $("<dt></dt>");
		$dt.append($img).append($a);
		$($(window.parent.document).find("#left_menu_content")).find(".lanm>dl>dt").last().after($dt);
//		$a.click(function(){
//			Cop.AdminUI.load($(this));
//			return false;
//		});
	}
	
}
