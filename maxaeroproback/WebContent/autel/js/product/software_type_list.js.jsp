<%@ include file="/autel/js/common/fmt.js.jsp"%>

function deleteSoftwareType(event,code){
	var $this = $(event);
	var $tr = $this.closest("tr");
	if(confirm("<fmt:message key='common.js.confirmDel' />")){
		$.ajax({
			url:"deleteSoftwareType.do?ajax=yes",
			type:"POST",
			data:{"softwareType.code":code},
			dataType:"JSON",
			success:function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("<fmt:message key='common.js.del' />");
					window.location.reload(true);
					//$tr.remove();
				}else{
					alert("<fmt:message key='common.js.nonDel' />");
				}
			},
			error:function(data){
				alert("<fmt:message key='common.js.nonDel' />");
				return false;
			}
		});
	}
}