<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 产品型号图片管理JS
 */
$(document).ready(function(){
	
	$("#addPicTr").click(function(){
		var $table = $("#picTable");
		$.ajax({
			url:'queryLanguageJson.do?ajax=yes',
			type:"post",
			data:{"productType.code":$("#code").val()},
			dataType:'JSON',
			success :function(data){
				if(data != null){
					var selectHtml = buildSelect(data,"");
					var tr = "<tr languageCode='' picId=''>";
					tr += "<td>"+selectHtml+"</td>";
					tr += "<td>";
					tr += "<input type='text' class='text-left' name='productTypeSerialPicVo.picPath' style='width:80%;' value='' languageName='' picPath='' maxlength='1000'/>";  
					tr += "</td>";
					tr += "<td class='caoz'>";
					tr += "<a href='#' action='save' onclick='productType(this);'>"+"<fmt:message key='produdt.js.save' />"+"</a>";
					tr += "&nbsp;&nbsp;";
					tr += "<a href='#' onclick='cancel(this);'>"+"<fmt:message key='produdt.js.cancel' />"+"</a>";
					tr += "</td>";
					tr += "</tr>";
					$table.append(tr);
				}
				
			},
			error :function(){
				alert("<fmt:message key='common.js.system.error' />");
			}
		});
	});
});
/**
 * 生成下拉框
 * @param data
 */
function buildSelect(data,option){
	var jsonData = eval(data);
	var select = "<select class='act_zt' name='languageSelect'>";
	if(option != ""){
		select += option;
	}
	for(var i = 0 ; i < jsonData.length; i ++){
		select += "<option value='"+jsonData[i].code+"'>"+jsonData[i].name+"</option>";
	}
	select += "</select>";
	return select;
}
/**
 * @param event	保存语言图片
 */
function productType(event){
	var $this = $(event);
	var action = $this.attr("action");
	//save方法
	if(action == "save"){
		saveProductTypePic(event);
	}else if(action == "update"){
		//修改
		toUpdateProductTypePic(event);
	}
}

/**
 * 添加语言图片路径
 * @param event
 */
function saveProductTypePic(event){
	var $this = $(event);
	
	var $picTd = $this.parent().prev();
	//图片路径
	var $picPathInput = $picTd.find("input");
	var picPath= $picPathInput.val();
	if(picPath == ""){
		alert("<fmt:message key='common.js.nonPic' />");
		return false;
	}
	
	var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
	if (!patrn.exec(picPath)){
		alert("<fmt:message key='common.js.formatError' />");
		return false ;
	}
	//语言选择
	var $languageTd = $this.parent().prev().prev();
	var $languageSelect = $languageTd.find("select");
	//语言code
	var languageCode = $languageSelect.val();
	if(languageCode == ""){
	
		alert("<fmt:message key='product.js.chooseLuange' />");
		return false;
	}
	//var $form = $("<form action='addSerialPicPath.do' method='post' enctype='multipart/form-data'></form>");
	var $form = $("#picForm");
	$form.attr("action","addSerialPicPath.do");
	$form.append("<input type='hidden' name='productType.code' value='"+$("#code").val()+"'>");
	$form.append("<input type='hidden' name='productTypeSerialPicVo.languageCode' value='"+languageCode+"'>");
	//var $div=$("<div style='display: none;'></div>");
	//$div.append($this.closest("tr").find("input[type='file']").clone());
	//$form.append($div);
	//$("body").append($form);
	$form.submit();
}

/**
 * 跳转到可编辑
 * @param event
 */
function toUpdateProductTypePic(event){
	var $this = $(event);
	
	//修改时 点击取消即还原修改数据
	var $next = $this.next();
	
	//图片路径列
	var $picTd = $this.parent().prev();
	//语言名称列
	var $languageTd = $this.parent().prev().prev();
	//语言名称
	var languageName = $languageTd.html();
	//事件触发行
	var $tr = $this.closest("tr");
	//语言code
	var languageCode = $tr.attr("languageCode");
	
	//图片路径文本
	var picPath = $picTd.text();
	
	$.ajax({
		url:'queryLanguageJson.do?ajax=yes',
		type:"post",
		data:{"productType.code":$("#code").val()},
		dataType:'JSON',
		success :function(data){
			var option = "<option value='"+languageCode+"'>"+languageName+"</option>";
			if(data != null){
				var selectHtml = buildSelect(data,option);
				$languageTd.html(selectHtml);
			}else{
				var select = "<select name='languageSelect'>";
				select += option;
				select += "</select>";
				$languageTd.html(select);
			}
			var picPathinput = "<input type='text' name='productTypeSerialPicVo.picPath' style='width:80%;' value='"+picPath+"' maxlength='1000'/>";
			$picTd.html(picPathinput);
			$next.attr("onclick","resetData(this);");
			$next.html("<fmt:message key='produdt.js.cancel' />");
			$this.html("<fmt:message key='produdt.js.save' />");
			var method = 'updateLanguagePic(this);';
			$this.attr("onclick",method);
			
		}
	});
	
}

/**
 * 事件触发源
 * @param event	
 */
function updateLanguagePic(event){
	var $this = $(event);
	
	//事件触发行
	var $tr = $this.closest("tr");
	//picId
	var picId = $tr.attr("picId");
	var $picTd = $this.parent().prev();
	//图片路径
	var $picPathInput = $picTd.find("input");
	
	var picPath= $picPathInput.val();
	if(picPath == ""){
		alert("<fmt:message key='common.js.nonPic' />");
		return false;
	}
	
	var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
	if (!patrn.exec(picPath)){
		alert("<fmt:message key='common.js.formatError' />");
		return false ;
	}
	
	//语言选择
	var $languageTd = $this.parent().prev().prev();
	var $languageSelect = $languageTd.find("select");
	//语言code
	var languageCode = $languageSelect.val();
	if(languageCode == ""){
		alert("<fmt:message key='product.js.chooseLuange' />");
		return false;
	}
	
//	var $form = $("<form action='updateLanguagePic.do' method='post' enctype='multipart/form-data'></form>");
	var $form = $("#picForm");
	$form.attr("action","updateLanguagePic.do");
	$form.append("<input type='hidden' name='productType.code' value='"+$("#code").val()+"'>");
	$form.append("<input type='hidden' name='productTypeSerialPicVo.languageCode' value='"+languageCode+"'>");
	$form.append("<input type='hidden' name='productTypeSerialPicVo.picId' value='"+picId+"'>");
	//var $div=$("<div style='display: none;'></div>");
	//$div.append($this.closest("tr").find("input[type='file']").clone());
	//$form.append($div);
	//$("body").append($form);
	$form.submit();
	
	
}

/**
 * 还原数据
 * @param event
 */
function resetData(event){
	var $this = $(event);
	//事件触发行
	var $tr = $this.closest("tr");
	//语言名称
	var languageName = $tr.attr("languageName");
	//图片路径
	var picPath = $tr.attr("picPath");
	
	var languageCode = $tr.attr("languageCode");
	
	//图片路径列
	var $picTd = $this.parent().prev();
	//语言名称列
	var $languageTd = $this.parent().prev().prev();
	
	$languageTd.html(languageName);
	$picTd.html(picPath);
	
	$this.prev().attr("action","update");
	$this.prev().attr("onclick","productType(this);");
	$this.prev().html("<fmt:message key='common.list.mod' />");
	//重新绑定为删除事件
	$this.attr("onclick","delProductPic(this,'"+$("#code").val()+"','"+languageCode+"');");
	$this.html("<fmt:message key='common.list.del' />");
	
}

/**
 * 删除语言图片
 * @param code	产品型号编码
 */
function delProductPic(event,code,languageCode){
	if(confirm("<fmt:message key='common.js.confirmDel' />")){
		$.ajax({
			url:'delProductPic.do?ajax=yes',
			type:"post",
			data:{"productType.code":code,
				"productTypeSerialPicVo.languageCode":languageCode},
				dataType:'JSON',
				success :function(data){
					cancel(event);
					alert("<fmt:message key='common.js.del' />");
				},
				error :function(){
					alert("<fmt:message key='common.js.nonDel' />");
				}
		});
	}
}

/**
 * 取消
 * @param event
 */
function cancel(event){
	var $tr = $(event).closest("tr");
	$tr.remove();
}

/**
 * 添加 ,修改 , 删除
 * @param action
 * @param productName	产品名称
 * @param code	产品编码
 */
function updateLeftMenu(action , productName , code){
	if(action == "update" || action == "del"){
		$($(window.parent.document).find("#left_menu_content")).find("a").each(function(i,item){
			//左边树形菜单节点编码等于产品型号操作编码
			if($(this).attr("code") == code){
				if(action == "update"){
					$(this).html(productName);
					return false;
				}else if(action == "del"){
					$(this).parent().remove();
				}
			}
		});
	}else if(action == "add"){
		var $img = $('<img width="9" height="15" src="../adminthemes/default/images/leg_08.gif" name="m1">');
		var $a = $("<a code='"+code+"' href='../autel/product/toSoftwareType.do?productSoftwareConfig.parentCode="+code+"' onclick='return adminUiLoad(this);'>" + productName + "</a>");
		var $dt = $("<dt></dt>");
		$dt.append($img).append($a);
		$($(window.parent.document).find("#left_menu_content")).find(".lanm>dl>dt:first").before($dt);
	}
	
}