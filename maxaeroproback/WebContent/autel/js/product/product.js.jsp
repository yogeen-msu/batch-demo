<%@ include file="/autel/js/common/fmt.js.jsp"%>

$(document).ready(function(){
	$("#addProductType").click(function(){
		
		var $table = $("#productTypeTable").find("table");
		var tr = "<tr tName='' code=''>";
		tr += "<td></td>";
		tr += "<td>";
		tr += "<input type='text' class='text-left' name='productName' value='' maxlength='32'/>";
		tr += "</td>";
		
		tr += "<td>";
		tr += "<input type='text' class='text-left' name='productType.picPath' style='width:90%;' value='' maxlength='1000'/>";
		tr += "</td>";
		
		tr += "<td>";
		tr += "<input type='button' onclick='toSerialPicPath(this);' disabled='disabled' class='diw_but' value='<fmt:message key="product.producttypevo.serialPicPathMan" />'/>";
		tr += "</td>";
		tr += "<td class='caoz'>";
		tr += "<a href='#' action='save' onclick='productType(this);'>"+"<fmt:message key='produdt.js.save' />"+"</a>";
		tr += "&nbsp;&nbsp;";
		tr += "<a href='#' onclick='cancel(this);'>"+"<fmt:message key='produdt.js.cancel' />"+"</a>";
		tr += "</td>";
		tr += "</tr>";
		$table.find("tr").eq(0).after(tr);
	});
	
	$("input[name^='serialPicPathBtn']").click(function(){
		toSerialPicPath(this);
	});
	
});

function toSerialPicPath(event){
	var code = $(event).parent().prev().prev().prev().text();
	var string = "toSerialPicPath.do?productType.code="+code;
	window.location = string;
}


function productType(event){
	var $this = $(event);
	var action = $this.attr("action");
	if(action == "save"){
		saveProductType(event);
	}else if(action == "update"){
		toUpdateProductType(event);
	}
}

function delProductType(event,code){
	if(confirm("<fmt:message key='common.js.confirmDel' />")){
		$.ajax({
			url:'delProductType.do?ajax=yes',
			type:"post",
			data:{"productType.code":code},
			dataType:'JSON',
			success :function(data){
				var jsonData = eval(data);
				if(jsonData[0].flag == "true"){
					alert("<fmt:message key='common.js.del' />");
				
					updateLeftMenu("del","",code);
					window.location.reload(true);
				}else{
					alert("<fmt:message key='common.js.nonDelProduct' />");
				}
			},
			error :function(){
				alert("<fmt:message key='common.js.nonDelProduct' />");
			}
		});
	}
}


function saveProductType(event){
	var $this = $(event);
	
	var $tr = $this.closest("tr");
	
	var $productTd = $this.parent().prev().prev().prev();
	
	var $picTd =  $this.parent().prev().prev();
	//图片路径
	var $picPathInput = $picTd.find("input");
	var picPath= $picPathInput.val();
	if(picPath == ""){
		alert("<fmt:message key='common.js.nonPic' />");
		return false;
	}
	
	var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
	if (!patrn.exec(picPath)){
	
		alert("<fmt:message key='common.js.formatError' />");
		return false ;
	}
	
	var $picInput = $this.parent().prev().find("input");
	var $input = $productTd.find("input");
	var $code = $this.parent().prev().prev().prev();
	var productName = $input.val();
	
	if(!valProductName(productName)){
	
		alert("<fmt:message key='produdt.js.picPrompt' />");
		return false;
	}
	
	//var $form = $("<form action='saveProductType.do' method='post' enctype='multipart/form-data'></form>");
	var $form = $("#productForm");
	$form.attr("action","saveProductType.do");
	
	$form.append("<input type='hidden' name='productType.name' value='"+productName+"'>");
	//var $div=$("<div style='display: none;'></div>");
	//$div.append($this.closest("tr").find("input[type='file']").clone());
	//$form.append($div);
	//$("body").append($form);
	$form.submit();
	
}

/**
 * 事件触发源
 * @param event
 */
function toUpdateProductType(event){
	
	var $this = $(event);
	
	//取消按钮
	var $next = $this.next();
	
	var $productTd = $this.parent().prev().prev().prev();
	var productName = $productTd.text();
	
	var $input = "<input type='text'  class='text-left' name='productName' maxlength='32' value='"+productName+"'/>";
	$productTd.html($input);
	
	//文件上传
	var $picTd = $this.parent().prev().prev();
	var picPath = $picTd.text();
	var $fileInput="<input type='text' class='text-left' name='productType.picPath' style='width:90%;' value='"+picPath+"' maxlength='1000'/>";
	$picTd.html($fileInput);
	
	$this.html("<fmt:message key='produdt.js.save' />");
	
	$next.attr("onclick","resetData(this);");
	$next.html("<fmt:message key='produdt.js.cancel' />");
	
	var method = 'updateProductType(this)';
	$this.attr("onclick",method);
	
}

/**
 * 修改产品型号
 * @param event
 */
function updateProductType(event){
	
	var $this = $(event);
	
	//取消按钮
	var $next = $this.next();
	
	var $productTd = $this.parent().prev().prev().prev();
	var $input = $productTd.find("input");
	var $code = $this.parent().prev().prev().prev().prev();
	var productName = $input.val();
	if(!valProductName(productName)){
		alert("<fmt:message key='produdt.js.picPrompt' />");
		return false;
	}
	
	var code = $code.html();
	
	var $picTd =  $this.parent().prev().prev();
	//图片路径
	var $picPathInput = $picTd.find("input");
	var picPath= $picPathInput.val();
	
	var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
	if(picPath != ""){
		if (!patrn.exec(picPath)){
			alert("<fmt:message key='common.js.formatError' />");
			return false ;
		}
	}
	
	//var $form = $("<form action='updateProductType.do' method='post' enctype='multipart/form-data'></form>");
	
	var $form = $("#productForm");
	$form.attr("action","updateProductType.do");
	
	$form.append("<input type='hidden' name='productType.name' value='"+productName+"'>");
	$form.append("<input type='hidden' name='productType.code' value='"+code+"'>");
	//var $div=$("<div style='display: none;'></div>");
	//$div.append($this.closest("tr").find("input[type='file']").clone());
	//$form.append($div);
	//$("body").append($form);
	$form.submit();
	
}

/**
 * 还原数据
 * @param event
 */
function resetData(event){
	var $this = $(event);
	//事件触发行
	var $tr = $this.closest("tr");
	var productTypeName = $tr.attr("tName");
	var code = $tr.attr("code");
	var picPath = $tr.attr("picPath");
	
	//产品型号名称列
	var $productTypeNameTd = $this.parent().prev().prev().prev();
	//产品型号编码列
	var $codeTd = $this.parent().prev().prev().prev().prev();
	
	//文件上传
	var $picTd = $this.parent().prev().prev();
	$picTd.html(picPath);
	
//	console.log(productTypeName,code);
	
//	$productTypeNameTd.html(productTypeName);
	$productTypeNameTd.empty();
	var a = "<a href='"+"toSoftwareType.do?productSoftwareConfig.parentCode="+code+"'>"+productTypeName+"</a>";
	$productTypeNameTd.append(a);
	$codeTd.html(code);
	
	$this.prev().attr("action","update");
	$this.prev().attr("onclick","productType(this);");
	$this.prev().html("<fmt:message key='common.list.mod' />");
	//重新绑定为删除事件
	$this.attr("onclick","delProductType(this,'"+code+"');");
	$this.html("<fmt:message key='common.list.del' />");
	
}

/**
 * 刷新frame
 * @param framesName frame名称
 */
function reloadFrames(framesName){
	window.parent.frames[framesName].location.reload();
}

/**
 * 取消
 * @param event
 */
function cancel(event){
	var $tr = $(event).closest("tr");
	$tr.remove();
}

/**
 * 验证产品名称是否由字母,数字,下划线组成
 * @param s
 * @returns {Boolean}
 */
function valProductName(s)
{
	var patrn=/^(\w){1,32}$/;
	if (!patrn.exec(s))
		return false;
	return true;
} 

/**
 * 添加 ,修改 , 删除
 * @param action
 * @param productName	产品名称
 * @param code	产品编码
 */
function updateLeftMenu(action , productName , code){
	if(action == "update" || action == "del"){
		$($(window.parent.document).find("#left_menu_content")).find("a").each(function(i,item){
			//左边树形菜单节点编码等于产品型号操作编码
			if($(this).attr("code") == code){
				if(action == "update"){
					$(this).html(productName);
					return false;
				}else if(action == "del"){
					$(this).parent().remove();
				}
			}
		});
	}else if(action == "add"){
		var $img = $('<img width="9" height="15" src="../adminthemes/default/images/leg_08.gif" name="m1">');
		var $a = $("<a code='"+code+"' href='../autel/product/toSoftwareType.do?productSoftwareConfig.parentCode="+code+"' onclick='return adminUiLoad(this);'>" + productName + "</a>");
		var $dt = $("<dt></dt>");
		$dt.append($img).append($a);
		$($(window.parent.document).find("#left_menu_content")).find(".lanm>dl>dt:first").before($dt);
	}
	
}

