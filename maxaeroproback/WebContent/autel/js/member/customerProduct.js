
///////  声明全局变量 Begin  ///////////////////////////////////////////

//注册状态TD
var $regstatustd;

//要解绑的产品code
var code = "";

//客户ID用于记录日志
var customerCode ="";

//解绑对话框中的input
var $pronoregi;

//解绑对话框中的解绑按钮
var $pronoregb;

//win_pronoreg控件
var $win_pronoreg;

//overlay控件
var $overlay;

//close控件
var $close;

///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin ////
	$regstatustd = $(".regstatustd");
	$pronoregi = $(".pronoregi");
	$pronoregb = $(".pronoregb");
	
	$win_pronoreg = $("#win_pronoreg");
	$overlay = $("#overlay");
	$close = $("#close");
	//初始化变量 End   ////
	
	
	/**
	 * 初始化遮罩层大小
	 */
	if($win_pronoreg.length > 0){
		$overlay.css("height",$("#f1").height());
	}
	
	/**
	 * 初始化绑定状态首选项
	 */
	if($(".proregstatussel").length > 0){
		$(".proregstatussel").find("option").each(function (){
		if($(this).val() == $(".proregstatussel").attr("proregstatussel")) $(this).attr("selected", true);
		});
	}
	
	//初始化绑定状态
	$regstatustd.each(function(){
		var $this = $(this);
		var proRegStatus = $this.parent().attr("proRegStatus");
		var proNoRegReason = $this.parent().attr("proNoRegReason");
		
		if (proRegStatus=="1")
		{
			$this.text(isbinding_str).next().html("<A class='operate' href='#'>"+unbinding_str+"</A>");
		}
		else if (proRegStatus=="2")
		{
			$this.text(unbinded_str).attr("title",unbindingreson_str+"："+proNoRegReason).next().html(unbinding_str);
		}
		else
		{
			$this.text(pronoreg_str).next().html(unbinding_str);
		}
	});
	

	//解绑链接
	var $operate = $(".operate");
	
	//弹出解绑对话框
	$operate.click(function(){
		var $this = $(this);
		code = $this.parent().parent().attr("code");     
		customerCode = $this.parent().parent().attr("customerCode");  
		$win_pronoreg.show();
		$overlay.show();
	});
	
	//点击解绑
	$pronoregb.click(function(){
		var proNoRegReason = $pronoregi.attr("value");  
		 
		if(!checkData(proNoRegReason,"string",true))
		{
			alert(promessage1);
			$pronoregi.focus();
			return false;
		}
		
		noRegPro(proNoRegReason);
	});
	
	/**
	 * 关闭分配客服窗口
	 */
	$close.click(function ()
	{
		colseWin();
	});
	
	$(".exportexcel").click(function(){
		var $this = $(this);
		var contextpath = $("#contextpath").val();
		$("#f1").attr("action",contextpath+"/customerProductExportExcel.do").submit();
	});
});



/**
 * 弹出对话框相关操作
 */
window.onload = function ()
{
	/**
	var oWin = document.getElementById("win_pronoreg");
	var oLay = document.getElementById("overlay");
	var oClose = document.getElementById("close");
	*/
	
	var oWin = $win_pronoreg.get(0);
	var oLay = $overlay.get(0);
	var oClose = $close.get(0);
	
	if (oWin != null)
	{
		var oH2 = oWin.getElementsByTagName("h2")[0];
		var bDrag = false;
		var disX = disY = 0;
		
		oClose.onmousedown = function (event)
		{
			(event || window.event).cancelBubble = true;
		};
		oH2.onmousedown = function (event)
		{		
			var event = event || window.event;
			bDrag = true;
			disX = event.clientX - oWin.offsetLeft;
			disY = event.clientY - oWin.offsetTop;
			this.setCapture && this.setCapture();
			return false;
		};
		document.onmousemove = function (event)
		{
			if (!bDrag) return;
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
			var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL; 		
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;
			
			oWin.style.marginTop = oWin.style.marginLeft = 0;
			oWin.style.left = iL + "px";
			oWin.style.top = iT + "px";		
			return false;
		};
		document.onmouseup = window.onblur = oH2.onlosecapture = function ()
		{
			bDrag = false;				
			oH2.releaseCapture && oH2.releaseCapture();
		};
	}
};


//用户产品解绑方法
function noRegPro(proNoRegReason){
	if(confirm(promessage2)){
		var result = -1;
		
		$.ajax({
			url:'noRegPro.do?ajax=yes',
			type:"post",
			data:{"code":code,"proNoRegReason":proNoRegReason,"customerCode":customerCode},
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(eval(data));
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			colseWin();
			alert(promessage3);
			$("#f1").attr("action","listcustomerpro.do").submit();
		}
		else
		{
			alert(promessage4);
		}
	}
}

//关闭解绑对话框
function colseWin(){
	code = "";
	customerCode = "";
	$pronoregi.attr("value","");
	$win_pronoreg.hide();
	$overlay.hide();
}