
///////  声明全局变量 Begin  ///////////////////////////////////////////

//sealername控件
var $sealername;

//sourcecode控件
var $sourcecode;

//salecfgcode控件
var $salecfgcode;

//areacfgcode控件
var $areacfgcode;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	$sealername = $(".sealername");
	$sourcecode = $(".sourcecode");
	$salecfgcode = $(".salecfgcode");
	$areacfgcode = $(".areacfgcode");
	
	//初始化变量 End ////
	
	
	if($sourcecode.length > 0){
		$sourcecode.find("option").each(function (){
		if($(this).val() == $sourcecode.attr("sourcecode")) $(this).attr("selected", true);
		});
	}
	
	
	if($salecfgcode.length > 0){
		$salecfgcode.find("option").each(function (){
		if($(this).val() == $salecfgcode.attr("salecfgcode")) $(this).attr("selected", true);
		});
	}
	
	
	if($areacfgcode.length > 0){
		$areacfgcode.find("option").each(function (){
		if($(this).val() == $areacfgcode.attr("areacfgcode")) $(this).attr("selected", true);
		});
	}
	
	$(".addcustomercomplainttype").click(function(){
		var $this = $(this);
		addCustomerComplaintType();
	});
	
	$(".editcustomercomplainttype").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		editCustomerComplaintType(code);
	});
	
	$(".insertcustomercomplainttype").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("customerComplaintTypeAdd.name")[0].value,"string",true))
		{
			alert("客诉类型名称不能为空");
			document.getElementsByName("customerComplaintTypeAdd.name")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("customerComplaintTypeAdd.proTypeCode")[0].value,"string",true))
		{
			alert("请选择产品型号");
			document.getElementsByName("customerComplaintTypeAdd.proTypeCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("customerComplaintTypeAdd.saleCfgCode")[0].value,"string",true))
		{
			alert("请选择销售配置");
			document.getElementsByName("customerComplaintTypeAdd.saleCfgCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("customerComplaintTypeAdd.areaCfgCode")[0].value,"string",true))
		{
			alert("请选择区域配置");
			document.getElementsByName("customerComplaintTypeAdd.areaCfgCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("customerComplaintTypeAdd.price")[0].value,"float",false))
		{
			alert("请填写正确的金额");
			document.getElementsByName("customerComplaintTypeAdd.price")[0].focus();
			return false;
		}
		*/
		insertCustomerComplaintType();
	});
	
	$(".updatecustomercomplainttype").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("customerComplaintTypeEdit.name")[0].value,"string",true))
		{
			alert("客诉类型名称不能为空");
			document.getElementsByName("customerComplaintTypeEdit.name")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("customerComplaintTypeEdit.proTypeCode")[0].value,"string",true))
		{
			alert("请选择产品型号");
			document.getElementsByName("customerComplaintTypeEdit.proTypeCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("customerComplaintTypeEdit.saleCfgCode")[0].value,"string",true))
		{
			alert("请选择销售配置");
			document.getElementsByName("customerComplaintTypeEdit.saleCfgCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("customerComplaintTypeEdit.areaCfgCode")[0].value,"string",true))
		{
			alert("请选择区域配置");
			document.getElementsByName("customerComplaintTypeEdit.areaCfgCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("customerComplaintTypeEdit.price")[0].value,"float",false))
		{
			alert("请填写正确的金额");
			document.getElementsByName("customerComplaintTypeEdit.price")[0].focus();
			return false;
		}
		*/
		updateCustomerComplaintType();
	});
	
	$(".dellcustomercomplainttype").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delCustomerComplaintType(code);
	});
	
	$(".dellcustomercomplainttypes").click(function(){
		var $this = $(this);
		delCustomerComplaintTypes();
	});
});

//去到添加客诉类型页面
function addCustomerComplaintType()
{
	$("#f1").attr("action","addcustomercomplainttype.do").submit();
}

//去到修改客诉类型页面
function editCustomerComplaintType(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editcustomercomplainttype.do").submit();
}

//添加客诉类型方法
function insertCustomerComplaintType()
{
	$("#f1").attr("action","insertcustomercomplainttype.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'insertcustomercomplainttype.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("客诉类型添加成功");
		$("#f1").attr("action","listcustomercomplainttype.do").submit();
	}
	else
	{
		alert("客诉类型添加失败");
	}
	*/
}

//修改客诉类型方法
function updateCustomerComplaintType()
{
	$("#f1").attr("action","updatecustomercomplainttype.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'updatecustomercomplainttype.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("客诉类型修改成功");
		$("#f1").attr("action","listcustomercomplainttype.do").submit();
	}
	else
	{
		alert("客诉类型修改失败");
	}
	*/
}

//删除客诉类型方法
function delCustomerComplaintType(code)
{
	if(confirm(message1)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delcustomercomplainttype.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delcustomercomplainttype.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("客诉类型删除成功");
			$("#f1").attr("action","listcustomercomplainttype.do").submit();
		}
		else if(result == 11)
		{
			alert("客诉类型已被使用，无法删除");
		}
		else
		{
			alert("客诉类型删除失败");
		}
		*/
	}
}

//删除多个客诉类型方法
function delCustomerComplaintTypes()
{
	if(confirm(message2)){		
		$("#f1").attr("action","delcustomercomplainttypes.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delcustomercomplainttypes.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("客诉类型删除成功");
			$("#f1").attr("action","listcustomercomplainttype.do").submit();
		}
		else if(result == 11)
		{
			alert("客诉类型已被使用，无法删除");
		}
		else
		{
			alert("客诉类型删除失败");
		}
		*/
	}
}

