//code组成的字符串中每个code值要不要加单引号（true：要加单引号，false：不加单引号）
var isYinhao = false;

//复选框数量
var checkbnum = 0;

//选中的复选框数量
var checkednum = 0;

//languagecode控件
var $languagecode;

//isallowsendemail控件
var $isallowsendemail;

var $allotcomplaint ;
var $win_complaint;;
var $overlay;
var $close;
var $selacceptor;
///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	$languagecode = $(".languagecode");
	$isallowsendemail = $(".isallowsendemail");
	$allotcomplaint = $(".allotcomplaint");
	$win_complaint= $("#win_complaint");;
	$overlay = $("#overlay");
	$close = $("#close");
	$selacceptor = $(".selacceptor");
	//初始化变量 End ////
	$close.click(function ()
			{
				colseWin();
			});

	/**
	 * 初始化用户状态首选项
	 */
	if($(".actstatesel").length > 0){
		$(".actstatesel").find("option").each(function (){
		if($(this).val() == $(".actstatesel").attr("actstatesel")) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 初始化用户状态
	 */
	if($(".actstate").length > 0){
		$(".actstate").each(function (){
			if($(this).attr("actstate") == "1") $(this).append(m_activated);
			if($(this).attr("actstate") == "0") $(this).append(m_noactivated);
		});
	}
	
	
	if($languagecode.length > 0){
		$languagecode.find("option").each(function (){
		if($(this).val() == $languagecode.attr("languagecode")) $(this).attr("selected", true);
		});
		if($languagecode.attr("languagecode") ==$("#languageCodeCN").val())
		{
			$(".customername").show();
			$(".customernameenglish").hide();
		}
		else
		{
			$(".customername").hide();
			$(".customernameenglish").show();
		}
	}
	
	if($isallowsendemail.length > 0){
		if($isallowsendemail.attr("isallowsendemail") == "1") $isallowsendemail.attr("checked", true);
	}
	
	$(".languagecode").change(function(){
		var $this = $(this);
		if($this.val()==$("#languageCodeCN").val())
		{
			$(".customername").show();
			$(".customernameenglish").hide();
		}
		else
		{
			$(".customername").hide();
			$(".customernameenglish").show();
		}
	});
	
	$(".edituser").click(function(){
		var $this = $(this);
		var code = $this.parent().parent().attr("code");
		editCustomer(code);
	});

});

function queryInfo(){
	var autelID=$("#autelID").val();
	var serialNo=$("#serialNo").val();
	if(autelID=="" && serialNo==""){
		alert(QueryBuilder);
		return;
	}
	$("#CustDiv").empty();
	var result;
	$.ajax({
		url:'listCustomerProduct.do?ajax=yes',
		type:"post",
		title:'1',
		data:{"customerProductVoSel.autelId":autelID,"customerProductVoSel.proSerialNo":serialNo},
		dataType:'JSON',
		async: false,
		success :function(data){
			var jsonData = eval(data);
			result = jsonData[0];
			var div="<table><td width='250px' style='text-align: center;font-weight: bold;'>"+customerID+"</td><td width='250px' style='text-align: center;font-weight: bold;'>"+productserial+"</td><td width='100px' style='text-align: center;font-weight: bold;'>"+option+"</td>";
			if(result.result.length==0){
				div+="<tr><td colspan='3' style='text-align: center;'>"+nrecord+"</td></tr>";
			}else{
			for(var i=0;i<result.result.length;i++){
				var id=result.result[i].autelId.toString();
				div+="<tr>";
				div+="<td style='text-align: center;'>"+result.result[i].autelId+"</td>";
				div+="<td style='text-align: center;'>"+result.result[i].proSerialNo+"</td>";
				div+="<td style='text-align: center;'><input onclick='selectCust(this);' code='"+result.result[i].code+"'   type='button' value='"+choose+"'></td></tr>";
			}
			}
			div+="</table>";
			$("#CustDiv").append(div);
			
			 $allotcomplaint = $(".allotcomplaint");
			 $win_complaint= $("#win_complaint");;
			 $overlay = $("#overlay");
			 $close = $("#close");
			 $selacceptor = $(".selacceptor");
			 $win_complaint.show();
			 $overlay.show();
			 $("#right_tab").hide();	
			
			
//			var dialog = art.dialog({
//				title:' ',
//				padding:0,
//				content: div,
//			    fixed: true,
//			    id: 'Fm7',
//			    cancelVal:cancel,
//			    okVal: submit,
//			    ok: function () {
//			    	var input=$('input:radio[name="checkNum"]:checked').val();
//			    	if (input == '') {
//			    		this.close();
//			        } else {
//			        	setCustomerInfo(input);
//			        	setHref(input);
//			        	this.close();
//			        };
//			    },
//			    cancel: true
//			});
			
			
		},
		error :function(data){
			result = -2;
		}
	});
}

function  locking(){   
	   document.all.right_tab.style.display="block";   
	   document.all.right_tab.style.width=document.body.clientWidth;   
	   document.all.right_tab.style.height=document.body.clientHeight;   
	//   document.all.Layer2.style.display='block';  
	    
	   }   


function selectCust(module){
	var $this = $(module);
	var code = $this.attr("code");
	setCustomerInfo(code);
	setHref(code);
	colseWin();
}

function colseWin()
{
	$("#right_tab").show();
	$win_complaint.hide();
	$overlay.hide();
}

function setCustomerInfo(code){
	$("#detail").attr("src","getCustomerInfo.do?code="+code);
	document.getElementById("span0").className="hover";
	document.getElementById("span1").className="";
	document.getElementById("span2").className="";
	document.getElementById("span3").className="";
	document.getElementById("span4").className="";
	//setMessageTab(0);
}

function setHref(code){
	$("#span0").attr("name","getCustomerInfo.do?code="+code);
	$("#span1").attr("name","getProductInfo.do?customerProductVoSel.code="+code);
	$("#span2").attr("name","getCustomerComplaint.do?customerComplaintInfoVoSel.userCode="+code);
	$("#span3").attr("name","getCustomerConsume.do?customerInfoVo.code="+code);
	$("#span4").attr("name","getCustomerLoginInfo.do?customerLoginInfoVoSel.code="+code);
}


function setMessageTab(n)
{
	var url=$("#span"+n).attr("name");
	if(url==""){
		url="bank.do";
	}
	for(var i=0;i<=4;i++){
			if(n==i){
				document.getElementById("span"+i).className="hover";
			}else{
				document.getElementById("span"+i).className="";
			}
		}
	
	
	$("#detail").attr("src",url);

}

