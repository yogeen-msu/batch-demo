

$(document).ready(function(){
	
	$(".addquestioninfo").click(function(){
		var $this = $(this);
		addQuestionInfo();
	});
	
	$(".editquestioninfo").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		editQuestionInfo(code);
	});
	
	$(".insertquestioninfo").click(function(){
		var $this = $(this);
		insertQuestionInfo();
	});
	
	$(".updatequestioninfo").click(function(){
		var $this = $(this);
		updateQuestionInfo();
	});
	
	$(".dellquestioninfo").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delQuestionInfo(code);
	});
	
	$(".dellquestioninfos").click(function(){
		var $this = $(this);
		delQuestionInfos();
	});
});

//去到添加安全问题页面
function addQuestionInfo()
{
	$("#f1").attr("action","addquestioninfo.do").submit();
}

//去到修改安全问题页面
function editQuestionInfo(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editquestioninfo.do").submit();
}

//添加安全问题方法
function insertQuestionInfo()
{
	$("#f1").attr("action","insertquestioninfo.do").submit();
}

//修改安全问题方法
function updateQuestionInfo()
{
	$("#f1").attr("action","updatequestioninfo.do").submit();
}

//删除安全问题方法
function delQuestionInfo(code)
{
	if(confirm(message1)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delquestioninfo.do").submit();
	}
}

//删除多个安全问题方法
function delQuestionInfos()
{
	if(confirm(message2)){		
		$("#f1").attr("action","delquestioninfos.do").submit();
	}
}

