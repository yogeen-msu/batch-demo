
///////  声明全局变量 Begin  ///////////////////////////////////////////

//选中code字符串控件
var $selobj;

//复选框
var $checkbs;

//全选框
var $allckbs;

//code组成的字符串中每个code值要不要加单引号（true：要加单引号，false：不加单引号）
var isYinhao = false;

//复选框数量
var checkbnum = 0;

//选中的复选框数量
var checkednum = 0;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	/**
	$selobj = $("#selectedcodes");
	$checkbs = $("input[name='checkboxcode']");
	$allckbs = $("input[name='allCheckbox']");
	isYinhao = true;
	checkbnum = $checkbs.length;
	*/
	
	//初始化变量 End ////
	
	$(".addcomplaintsource").click(function(){
		var $this = $(this);
		addCustomerComplaintSource();
	});
	
	$(".editcomplaintsource").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		editCustomerComplaintSource(code);
	});
	
	$(".insertcomplaintsource").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("customerComplaintSourceAdd.name")[0].value,"string",true))
		{
			alert("客诉来源名称不能为空");
			document.getElementsByName("customerComplaintSourceAdd.name")[0].focus();
			return false;
		}
		*/
		insertCustomerComplaintSource();
	});
	
	$(".updatecomplaintsource").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("customerComplaintSourceEdit.name")[0].value,"string",true))
		{
			alert("客诉来源名称不能为空");
			document.getElementsByName("customerComplaintSourceEdit.name")[0].focus();
			return false;
		}
		*/
		updateCustomerComplaintSource();
	});
	
	$(".dellcomplaintsource").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delCustomerComplaintSource(code);
	});
	
	$(".dellcomplaintsources").click(function(){
		var $this = $(this);
		delCustomerComplaintSources();
	});
	
	/**
	$checkbs.click(function(){
		var $this = $(this);
		xuanze($this);
	});
	
	$allckbs.click(function(){
		var $this = $(this);
		quanxuan($this);
	});
	*/
});

//去到添加客诉来源页面
function addCustomerComplaintSource()
{
	$("#f1").attr("action","addcomplaintsource.do").submit();
}

//去到修改客诉来源页面
function editCustomerComplaintSource(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editcomplaintsource.do").submit();
}

//添加客诉来源方法
function insertCustomerComplaintSource()
{
	$("#f1").attr("action","insertcomplaintsource.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'insertcomplaintsource.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("客诉来源添加成功");
		$("#f1").attr("action","listcomplaintsource.do").submit();
	}
	else if(result == 2)
	{
		alert("这个客诉来源已存在，添加失败");
	}
	else
	{
		alert("客诉来源添加失败");
	}
	*/
}

//修改客诉来源方法
function updateCustomerComplaintSource()
{
	$("#f1").attr("action","updatecomplaintsource.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'updatecomplaintsource.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("客诉来源修改成功");
		$("#f1").attr("action","listcomplaintsource.do").submit();
	}
	else
	{
		alert("客诉来源修改失败");
	}
	*/
}

//删除客诉来源方法
function delCustomerComplaintSource(code)
{
	if(confirm("确定要删除这个客诉来源吗?")){
		$("#code").attr("value",code);
		$("#f1").attr("action","delcomplaintsource.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delcomplaintsource.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("客诉来源删除成功");
			$("#f1").attr("action","listcomplaintsource.do").submit();
		}
		else if(result == 11)
		{
			alert("客诉来源已被使用，无法删除");
		}
		else
		{
			alert("客诉来源删除失败");
		}
		*/
	}
}

//删除多个客诉来源方法
function delCustomerComplaintSources()
{
	if(confirm("确定要删除这"+checkednum+"个客诉来源吗?")){
		$("#f1").attr("action","delcomplaintsources.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delcomplaintsources.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("客诉来源删除成功");
			$("#f1").attr("action","listcomplaintsource.do").submit();
		}
		else if(result == 11)
		{
			alert("客诉来源已被使用，无法删除");
		}
		else
		{
			alert("客诉来源删除失败");
		}
		*/
	}
}


//复选方法///////////////
function xuanze($event)
{
	var selcos = $selobj.attr("value");
	var code = $event.attr("value");
	
	if(isYinhao) code = "'"+code+"'";
	
	if($event.attr("checked"))
	{
		if(selcos=="") selcos=code;
			else selcos=selcos+","+code;
		checkednum++;

		if(checkednum==checkbnum)
		{
			$allckbs.each(function(){
				$(this).attr("checked",true);
			});
		}
	}
	else
	{		
		$allckbs.each(function(){
			$(this).attr("checked",false);
		});
		
		var sarr = selcos.split(",");
		if(sarr!=null&&sarr.length>0){
			for(var i=0;i<sarr.length;i++){
				if(code==sarr[i]){
					if(i==0){
						sarr = sarr.slice(i+1,sarr.length);
					}else{
						sarr = sarr.slice(0,i).concat(sarr.slice(i+i,sarr.length));
					}
					
					checkednum--;
				}
			}
		}
		
		if(sarr.length>0){
			selcos = sarr.join(",");
		}else{
			selcos="";
		}
	}
	$selobj.attr("value",selcos);
}

//全选方法////////////
function quanxuan($event)
{
	var flag=false;
	
	if ($event.attr("checked"))
	{
		flag=true;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	else
	{
		flag=false;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	
	if ($checkbs.length>0)
	{
	   for (var i=0;i<$checkbs.length;i++)
		{
			if(flag)
			{
				if(!$checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}
			else
			{
				if($checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}			 
		}
	}
}
