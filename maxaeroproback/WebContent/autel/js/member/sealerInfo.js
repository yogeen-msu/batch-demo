
///////  声明全局变量 Begin  ///////////////////////////////////////////

//选中code字符串控件
var $selobj;

//复选框
var $checkbs;

//全选框
var $allckbs;

//code组成的字符串中每个code值要不要加单引号（true：要加单引号，false：不加单引号）
var isYinhao = false;

//复选框数量
var checkbnum = 0;

//选中的复选框数量
var checkednum = 0;

//languagecode控件
var $languagecode;

//isallowsendemail控件
var $isallowsendemail;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	/**
	$selobj = $("#selectedcodes");
	$checkbs = $("input[name='checkboxcode']");
	$allckbs = $("input[name='allCheckbox']");
	isYinhao = true;
	checkbnum = $checkbs.length;
	*/
	$languagecode = $(".languagecode");
	$isallowsendemail = $(".isallowsendemail");
	
	//初始化变量 End ////
	
	
	if($languagecode.length > 0){
		$languagecode.find("option").each(function (){
		if($(this).val() == $languagecode.attr("languagecode")) $(this).attr("selected", true);
		});
		if($languagecode.attr("languagecode") ==$("#languageCodeCN").val())
		{
			$(".sealername").show();
			$(".sealernameenglish").hide();
		}
		else
		{
			$(".sealername").hide();
			$(".sealernameenglish").show();
		}
	}
	
	if($isallowsendemail.length > 0){
		if($isallowsendemail.attr("isallowsendemail") == "1") $isallowsendemail.attr("checked", true);
	}
	
	$(".languagecode").change(function(){
		var $this = $(this);
		if($this.val()==$("#languageCodeCN").val())
		{
			$(".sealername").show();
			$(".sealernameenglish").hide();
		}
		else
		{
			$(".sealername").hide();
			$(".sealernameenglish").show();
		}
	});
	
	$(".languagecodeadd").change(function(){
		var $this = $(this);
		if($this.val()==$("#languageCodeCN").val())
		{
			$(".sealername").show();
			$(".sealernameenglish").hide();
		}
		else
		{
			$(".sealername").hide();
			$(".sealernameenglish").show();
		}
	});
	
	
	$(".createsealer").click(function(){
		var $this = $(this);
		$("#f1").attr("action","createsealerinfo.do").submit();
	});
	
	$(".savecreatesealer").click(function(){
		var $this = $(this);
		if(!checkData(document.getElementsByName("sealerFile")[0].value,"excelfilename",true))
		{
			alert("请选择充值卡excel文档");
			document.getElementsByName("sealerFile")[0].focus();
			return false;
		}

		$("#f1").attr("action","savecreatesealer.do").submit();
	});
	
	$(".addsealer").click(function(){
		var $this = $(this);
		addSealer();
	});
	
	$(".editsealer").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		editSealer(code);
	});
	
	$(".insertsealer").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("sealerInfoAdd.autelId")[0].value,"string",true))
		{
			alert("经销商编号不能为空");
			document.getElementsByName("sealerInfoAdd.autelId")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.userPwd")[0].value,"string",true))
		{
			alert("密码不能为空");
			document.getElementsByName("sealerInfoAdd.userPwd")[0].focus();
			return false;
		}
		
		if($("input[type='password']").eq(0).val() != $("input[type='password']").eq(1).val())
		{
			alert("两次密码不一至");
			$("input[type='password']").eq(1).val("").focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.languageCode")[0].value,"string",true))
		{
			alert("请选择首选语言");
			document.getElementsByName("sealerInfoAdd.languageCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.email")[0].value,"email",true))
		{
			alert("请输入有效的电子邮箱地址");
			document.getElementsByName("sealerInfoAdd.email")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.daytimePhoneCC")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoAdd.daytimePhoneCC")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.daytimePhoneAC")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoAdd.daytimePhoneAC")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.daytimePhone")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoAdd.daytimePhone")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.daytimePhoneExtCode")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoAdd.daytimePhoneExtCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.mobilePhoneCC")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoAdd.mobilePhoneCC")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.mobilePhoneAC")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoAdd.mobilePhoneAC")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoAdd.mobilePhone")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoAdd.mobilePhone")[0].focus();
			return false;
		}
		*/
		insertSealer();
	});
	
	$(".updatesealer").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("sealerInfoEdit.autelId")[0].value,"string",true))
		{
			alert("经销商编号不能为空");
			document.getElementsByName("sealerInfoEdit.autelId")[0].focus();
			return false;
		}
		
		if($("input[type='password']").eq(0).val() != $("input[type='password']").eq(1).val())
		{
			alert("两次密码不一至");
			$("input[type='password']").eq(1).val("").focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.languageCode")[0].value,"string",true))
		{
			alert("请选择首选语言");
			document.getElementsByName("sealerInfoEdit.languageCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.email")[0].value,"email",true))
		{
			alert("请输入有效的电子邮箱地址");
			document.getElementsByName("sealerInfoEdit.email")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.daytimePhoneCC")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoEdit.daytimePhoneCC")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.daytimePhoneAC")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoEdit.daytimePhoneAC")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.daytimePhone")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoEdit.daytimePhone")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.daytimePhoneExtCode")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoEdit.daytimePhoneExtCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.mobilePhoneCC")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoEdit.mobilePhoneCC")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.mobilePhoneAC")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoEdit.mobilePhoneAC")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("sealerInfoEdit.mobilePhone")[0].value,"number",false))
		{
			alert("请输入有效的电话号码");
			document.getElementsByName("sealerInfoEdit.mobilePhone")[0].focus();
			return false;
		}
		*/
		updateSealer();
	});
	
	$(".dellsealer").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delSealer(code);
	});
	
	$(".dellsealers").click(function(){
		var $this = $(this);
		delSealers();
	});
	
	/**
	
	$checkbs.click(function(){
		var $this = $(this);
		xuanze($this);
	});
	
	$allckbs.click(function(){
		var $this = $(this);
		quanxuan($this);
	});
	*/
	
	$(".syncsealer").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		syncSealer(code);
	});
});
//经销商同步到Nexternal商城
function syncSealer(code){
	$("#code").attr("value",code);
	$("#f1").attr("action","toNexternal.do").submit();
}

//去到添加经销商页面
function addSealer()
{
	$("#f1").attr("action","addsealer.do").submit();
}

//去到修改经销商页面
function editSealer(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editsealer.do").submit();
}

//添加经销商方法
function insertSealer()
{
	$("#f1").attr("action","insertsealer.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'insertsealer.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("经销商添加成功");
		$("#f1").attr("action","listsealer.do").submit();
	}
	else if(result == 2)
	{
		alert("该经销商编号已经存在，不能重复添加");
		document.getElementsByName("sealerInfoAdd.autelId")[0].focus();
	}
	else
	{
		alert("经销商添加失败");
	}
	*/
}

//修改经销商方法
function updateSealer()
{
	$("#f1").attr("action","updatesealer.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'updatesealer.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("经销商修改成功");
		$("#f1").attr("action","listsealer.do").submit();
	}
	else
	{
		alert("经销商修改失败");
	}
	*/
}

//删除经销商方法
function delSealer(code)
{
	if(confirm(message2)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delsealer.do").submit();
	}
	/**
	if(confirm("确定要删除这个经销商吗?")){
		$("#code").attr("value",code);
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
		
		$.ajax({
			url:'delsealer.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("经销商删除成功");
			$("#f1").attr("action","listsealer.do").submit();
		}
		else if(result == 11)
		{
			alert("经销商已被使用，无法删除");
		}
		else
		{
			alert("经销商删除失败");
		}
	}
	*/
}

//删除多个经销商方法
function delSealers()
{
	if(confirm(message3)){
		$("#f1").attr("action","delsealers.do").submit();
	}
	/**
	if(confirm("确定要删除这"+checkednum+"个经销商吗?")){
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
		
		$.ajax({
			url:'delsealers.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("经销商删除成功");
			$("#f1").attr("action","listsealer.do").submit();
		}
		else if(result == 11)
		{
			alert("经销商已被使用，无法删除");
		}
		else
		{
			alert("经销商删除失败");
		}
	}
	*/
}

//复选方法///////////////
function xuanze($event)
{
	var selcos = $selobj.attr("value");
	var code = $event.attr("value");
	
	if(isYinhao) code = "'"+code+"'";
	
	if($event.attr("checked"))
	{
		if(selcos=="") selcos=code;
			else selcos=selcos+","+code;
		checkednum++;

		if(checkednum==checkbnum)
		{
			$allckbs.each(function(){
				$(this).attr("checked",true);
			});
		}
	}
	else
	{		
		$allckbs.each(function(){
			$(this).attr("checked",false);
		});
		
		var sarr = selcos.split(",");
		if(sarr!=null&&sarr.length>0){
			for(var i=0;i<sarr.length;i++){
				if(code==sarr[i]){
					if(i==0){
						sarr = sarr.slice(i+1,sarr.length);
					}else{
						sarr = sarr.slice(0,i).concat(sarr.slice(i+i,sarr.length));
					}
					
					checkednum--;
				}
			}
		}
		
		if(sarr.length>0){
			selcos = sarr.join(",");
		}else{
			selcos="";
		}
	}
	$selobj.attr("value",selcos);
}

//全选方法////////////
function quanxuan($event)
{
	var flag=false;
	
	if ($event.attr("checked"))
	{
		flag=true;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	else
	{
		flag=false;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	
	if ($checkbs.length>0)
	{
	   for (var i=0;i<$checkbs.length;i++)
		{
			if(flag)
			{
				if(!$checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}
			else
			{
				if($checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}			 
		}
	}
}
