
/**
 * 客诉类型名称管理JS
 */
//添加的数据
var addArray = new Array();
//删除的数据
var delArray = new Array();

var tableId = "#complaintTypeNameTable";

var form = "#complaintTypeNameForm";

var complaintTypeName1 = "customerComplaintTypeNames";

var complaintTypeName2 = "delCustomerComplaintTypeNames";

$(document).ready(function(){

	$("#finsh").click(function(){
		var complaintTypeCode = $("#complaintTypeCode").val();
		//选择的option
		var $selectOption = $("#language option:selected");
		//languageCode
		var value= $selectOption.val();
		//languageName
		var text = $selectOption.text();
		var complaintTypeName = $("#complaintTypeName").val();
		if(value==null || complaintTypeName=="")
		{
			alert(message5);
			return false;
		}
		
		var customerComplaintTypeName = new CustomerComplaintTypeName(text,value,complaintTypeCode,complaintTypeName);
		
		addArray.push(customerComplaintTypeName);
		
		var oWin = $("#win");
		var oLay = $("#overlay");
		oWin.hide();
		oLay.hide();
		buildHtml(customerComplaintTypeName,tableId);
		$selectOption.remove();
	});
	//表单提交
	$("#sumbitBtn").click(function(){
		var aLen = addArray.length;
		var dLen = delArray.length;
		
		//if(aLen == 0 && dLen == 0){
			//alert(message3);
			//return false;
		//}else{
			var $form = $(form);
			var inputHiddenHtml = "";
			for ( var i = 0; i < aLen; i++) {
				var v = addArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+complaintTypeName1+"["+i+"].complaintTypeCode' value='"+v.complaintTypeCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+complaintTypeName1+"["+i+"].languageCode' value='"+v.languageCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+complaintTypeName1+"["+i+"].name' value='"+v.complaintTypeName+"'/>";
			}
			for ( var i = 0; i < dLen; i++) {
				var v = delArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+complaintTypeName2+"["+i+"].id' value='"+v+"'/>";
			}
			$form.append(inputHiddenHtml);
			$form.submit();
		//}
	});
});

/**
 * 
 * @param customerComplaintTypeName	客诉类型名称
 */
function buildHtml(customerComplaintTypeName,tableId){
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+customerComplaintTypeName.languageName+"</td>");
	var $td2 = $("<td>"+customerComplaintTypeName.complaintTypeName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='###' languageName='"+customerComplaintTypeName.languageName+"' languageCode='"+customerComplaintTypeName.languageCode+"'>移除</a>");
	
	$a.click(function(){
		var languageCode = $(this).attr("languageCode");
		addArray.remove(languageCode);
		addOption(this);
		$(this).closest("tr").remove();
	});
	$td4.append($a);
	
	$tr.append($td1).append($td2).append($td4);
	$(tableId).append($tr);
}

/**
 * 删除客诉类型名称
 * @param event
 */
function delName(event){
	if(confirm(message4)){
		var $this = $(event);
		//客诉类型名称id
		var complaintTypeNameId = $this.attr("complaintTypeNameId");
		delArray.push(complaintTypeNameId);
		addOption(event);
		$this.closest("tr").remove();
	}
}

/**
 * 添加下拉菜单语言选项
 * @param event
 */
function addOption(event){
	var $this = $(event);
	var languageCode = $this.attr("languageCode");
	var languageName = $this.attr("languageName");
	var $option = $("<option value='"+languageCode+"'>"+languageName+"</option>");
	$("#language").append($option);
}

/**
 * 
 * @param languageName	语言名称
 * @param languageCode	语言编码
 * @param complaintTypeCode	客诉类型编码
 * @param complaintTypeName	客诉类型名称
 */
function CustomerComplaintTypeName(languageName,languageCode,complaintTypeCode,complaintTypeName){
	this.languageName = languageName;
	this.languageCode = languageCode;
	this.complaintTypeCode = complaintTypeCode;
	this.complaintTypeName = complaintTypeName;
}

Array.prototype.remove = function(languageCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(languageCode == this[i].languageCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};