
///////  声明全局变量 Begin  ///////////////////////////////////////////

//选中code字符串控件
var $selobj;

//复选框
var $checkbs;

//全选框
var $allckbs;

//code组成的字符串中每个code值要不要加单引号（true：要加单引号，false：不加单引号）
var isYinhao = false;

//复选框数量
var checkbnum = 0;

//选中的复选框数量
var checkednum = 0;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	/**
	$selobj = $("#selectedcodes");
	$checkbs = $("input[name='checkboxcode']");
	$allckbs = $("input[name='allCheckbox']");
	isYinhao = true;
	checkbnum = $checkbs.length;
	*/
	
	//初始化变量 End ////
	
	$(".addsealertype").click(function(){
		var $this = $(this);
		addSealerType();
	});
	
	$(".editsealertype").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		editSealerType(code);
	});
	
	$(".insertsealertype").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("sealerTypeAdd.name")[0].value,"string",true))
		{
			alert("经销商类型名称不能为空");
			document.getElementsByName("sealerTypeAdd.name")[0].focus();
			return false;
		}
		*/
		insertSealerType();
	});
	
	$(".updatesealertype").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("sealerTypeEdit.name")[0].value,"string",true))
		{
			alert("经销商类型名称不能为空");
			document.getElementsByName("sealerTypeEdit.name")[0].focus();
			return false;
		}
		*/
		updateSealerType();
	});
	
	$(".dellsealertype").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delSealerType(code);
	});
	
	$(".dellsealertypes").click(function(){
		var $this = $(this);
		delSealerTypes();
	});
	
	/**
	$checkbs.click(function(){
		var $this = $(this);
		xuanze($this);
	});
	
	$allckbs.click(function(){
		var $this = $(this);
		quanxuan($this);
	});
	*/
});

//去到添加经销商类型页面
function addSealerType()
{
	$("#f1").attr("action","addsealertype.do").submit();
}

//去到修改经销商类型页面
function editSealerType(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editsealertype.do").submit();
}

//添加经销商类型方法
function insertSealerType()
{
	$("#f1").attr("action","insertsealertype.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'insertsealertype.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("经销商类型添加成功");
		$("#f1").attr("action","listsealertype.do").submit();
	}
	else if(result == 2)
	{
		alert("这个经销商类型已存在，添加失败");
	}
	else
	{
		alert("经销商类型添加失败");
	}
	*/
}

//修改经销商类型方法
function updateSealerType()
{
	$("#f1").attr("action","updatesealertype.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'updatesealertype.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("经销商类型修改成功");
		$("#f1").attr("action","listsealertype.do").submit();
	}
	else
	{
		alert("经销商类型修改失败");
	}
	*/
}

//删除经销商类型方法
function delSealerType(code)
{
	if(confirm(message1)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delsealertype.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delsealertype.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("经销商类型删除成功");
			$("#f1").attr("action","listsealertype.do").submit();
		}
		else if(result == 11)
		{
			alert("经销商类型已被使用，无法删除");
		}
		else
		{
			alert("经销商类型删除失败");
		}
		*/
	}
}

//删除多个经销商类型方法
function delSealerTypes()
{
	if(confirm(message2)){
		$("#f1").attr("action","delsealertypes.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delsealertypes.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("经销商类型删除成功");
			$("#f1").attr("action","listsealertype.do").submit();
		}
		else if(result == 11)
		{
			alert("经销商类型已被使用，无法删除");
		}
		else
		{
			alert("经销商类型删除失败");
		}
		*/
	}
}


//复选方法///////////////
function xuanze($event)
{
	var selcos = $selobj.attr("value");
	var code = $event.attr("value");
	
	if(isYinhao) code = "'"+code+"'";
	
	if($event.attr("checked"))
	{
		if(selcos=="") selcos=code;
			else selcos=selcos+","+code;
		checkednum++;

		if(checkednum==checkbnum)
		{
			$allckbs.each(function(){
				$(this).attr("checked",true);
			});
		}
	}
	else
	{		
		$allckbs.each(function(){
			$(this).attr("checked",false);
		});
		
		var sarr = selcos.split(",");
		if(sarr!=null&&sarr.length>0){
			for(var i=0;i<sarr.length;i++){
				if(code==sarr[i]){
					if(i==0){
						sarr = sarr.slice(i+1,sarr.length);
					}else{
						sarr = sarr.slice(0,i).concat(sarr.slice(i+i,sarr.length));
					}
					
					checkednum--;
				}
			}
		}
		
		if(sarr.length>0){
			selcos = sarr.join(",");
		}else{
			selcos="";
		}
	}
	$selobj.attr("value",selcos);
}

//全选方法////////////
function quanxuan($event)
{
	var flag=false;
	
	if ($event.attr("checked"))
	{
		flag=true;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	else
	{
		flag=false;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	
	if ($checkbs.length>0)
	{
	   for (var i=0;i<$checkbs.length;i++)
		{
			if(flag)
			{
				if(!$checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}
			else
			{
				if($checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}			 
		}
	}
}
