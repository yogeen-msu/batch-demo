
///////  声明全局变量 Begin  ///////////////////////////////////////////

//选中code字符串控件
var $selobj;

//复选框
var $checkbs;

//全选框
var $allckbs;

//code组成的字符串中每个code值要不要加单引号（true：要加单引号，false：不加单引号）
var isYinhao = false;

//复选框数量
var checkbnum = 0;

//选中的复选框数量
var checkednum = 0;

//sealername控件
var $sealername;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	/**
	$selobj = $("#selectedcodes");
	$checkbs = $("input[name='checkboxcode']");
	$allckbs = $("input[name='allCheckbox']");
	isYinhao = true;
	checkbnum = $checkbs.length;
	*/
	$sealername = $(".sealername");
	
	//初始化变量 End ////
	
	
	if($sealername.length > 0){
		$sealername.find("option").each(function (){
		if($(this).val() == $sealername.attr("sealername")) $(this).attr("selected", true);
		});
	}
	
	$(".addsaleinfo").click(function(){
		var $this = $(this);
		addSaleInfo();
	});
	
	$(".editsaleinfo").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		editSaleInfo(code);
	});
	
	$(".insertsaleinfo").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("saleInfoAdd.sealerName")[0].value,"string",true))
		{
			alert("请选择经销商");
			document.getElementsByName("saleInfoAdd.sealerName")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoAdd.proName")[0].value,"string",true))
		{
			alert("产品名称不能为空");
			document.getElementsByName("saleInfoAdd.proName")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoAdd.proExpand")[0].value,"string",true))
		{
			alert("产品规格不能为空");
			document.getElementsByName("saleInfoAdd.proExpand")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoAdd.company")[0].value,"string",true))
		{
			alert("单位不能为空");
			document.getElementsByName("saleInfoAdd.company")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoAdd.salesNum")[0].value,"number",true))
		{
			alert("请输入正确的销货数量");
			document.getElementsByName("saleInfoAdd.salesNum")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoAdd.salesMoney")[0].value,"float",true))
		{
			alert("请输入正确的销货金额");
			document.getElementsByName("saleInfoAdd.salesMoney")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoAdd.pinBackNum")[0].value,"number",false))
		{
			alert("请输入正确的销退数量");
			document.getElementsByName("saleInfoAdd.pinBackNum")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoAdd.pinBackMoney")[0].value,"float",false))
		{
			alert("请输入正确的销退金额");
			document.getElementsByName("saleInfoAdd.pinBackMoney")[0].focus();
			return false;
		}
		*/
		insertSaleInfo();
	});
	
	$(".updatesaleinfo").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("saleInfoEdit.sealerName")[0].value,"string",true))
		{
			alert("请选择经销商");
			document.getElementsByName("saleInfoEdit.sealerName")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoEdit.proName")[0].value,"string",true))
		{
			alert("产品名称不能为空");
			document.getElementsByName("saleInfoEdit.proName")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoEdit.proExpand")[0].value,"string",true))
		{
			alert("产品规格不能为空");
			document.getElementsByName("saleInfoEdit.proExpand")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoEdit.company")[0].value,"string",true))
		{
			alert("单位不能为空");
			document.getElementsByName("saleInfoEdit.company")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoEdit.salesNum")[0].value,"number",true))
		{
			alert("请输入正确的销货数量");
			document.getElementsByName("saleInfoEdit.salesNum")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoEdit.salesMoney")[0].value,"float",true))
		{
			alert("请输入正确的销货金额");
			document.getElementsByName("saleInfoEdit.salesMoney")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoEdit.pinBackNum")[0].value,"number",false))
		{
			alert("请输入正确的销退数量");
			document.getElementsByName("saleInfoEdit.pinBackNum")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("saleInfoEdit.pinBackMoney")[0].value,"float",false))
		{
			alert("请输入正确的销退金额");
			document.getElementsByName("saleInfoEdit.pinBackMoney")[0].focus();
			return false;
		}
		*/
		updateSaleInfo();
	});
	
	$(".dellsaleinfo").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delSaleInfo(code);
	});
	
	$(".dellsaleinfos").click(function(){
		var $this = $(this);
		delSaleInfos();
	});
	
	
	$(".createsaleinfo").click(function(){
		var $this = $(this);
		$("#f1").attr("action","createsaleinfo.do").submit();
	});
	
	$(".savecreatesaleinfo").click(function(){
		var $this = $(this);
		if(!checkData(document.getElementsByName("saleInfoFile")[0].value,"excelfilename",true))
		{
			alert("请选择充值卡excel文档");
			document.getElementsByName("saleInfoFile")[0].focus();
			return false;
		}

		$("#f1").attr("action","savecreatesaleinfo.do").submit();
	});
	
	/**
	
	$checkbs.click(function(){
		var $this = $(this);
		xuanze($this);
	});
	
	$allckbs.click(function(){
		var $this = $(this);
		quanxuan($this);
	});
	*/
});

//去到添加经销商销售信息页面
function addSaleInfo()
{
	$("#f1").attr("action","addsaleinfo.do").submit();
}

//去到修改经销商销售信息页面
function editSaleInfo(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editsaleinfo.do").submit();
}

//添加经销商销售信息方法
function insertSaleInfo()
{
	$("#f1").attr("action","insertsaleinfo.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'insertsaleinfo.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("经销商销售信息添加成功");
		$("#f1").attr("action","listsaleinfo.do").submit();
	}
	else
	{
		alert("经销商销售信息添加失败");
	}
	*/
}

//修改经销商销售信息方法
function updateSaleInfo()
{
	$("#f1").attr("action","updatesaleinfo.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'updatesaleinfo.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("经销商销售信息修改成功");
		$("#f1").attr("action","listsaleinfo.do").submit();
	}
	else
	{
		alert("经销商销售信息修改失败");
	}
	*/
}

//删除经销商销售信息方法
function delSaleInfo(code)
{
	if(confirm(message1)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delsaleinfo.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delsaleinfo.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("经销商销售信息删除成功");
			$("#f1").attr("action","listsaleinfo.do").submit();
		}
		else if(result == 11)
		{
			alert("经销商销售信息已被使用，无法删除");
		}
		else
		{
			alert("经销商销售信息删除失败");
		}
		*/
	}
}

//删除多个经销商销售信息方法
function delSaleInfos()
{
	if(confirm(message2)){
		$("#f1").attr("action","delsaleinfos.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delsaleinfos.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("经销商销售信息删除成功");
			$("#f1").attr("action","listsaleinfo.do").submit();
		}
		else if(result == 11)
		{
			alert("经销商销售信息已被使用，无法删除");
		}
		else
		{
			alert("经销商销售信息删除失败");
		}
		*/
	}
}


//复选方法///////////////
function xuanze($event)
{
	var selcos = $selobj.attr("value");
	var code = $event.attr("value");
	
	if(isYinhao) code = "'"+code+"'";
	
	if($event.attr("checked"))
	{
		if(selcos=="") selcos=code;
			else selcos=selcos+","+code;
		checkednum++;

		if(checkednum==checkbnum)
		{
			$allckbs.each(function(){
				$(this).attr("checked",true);
			});
		}
	}
	else
	{		
		$allckbs.each(function(){
			$(this).attr("checked",false);
		});
		
		var sarr = selcos.split(",");
		if(sarr!=null&&sarr.length>0){
			for(var i=0;i<sarr.length;i++){
				if(code==sarr[i]){
					if(i==0){
						sarr = sarr.slice(i+1,sarr.length);
					}else{
						sarr = sarr.slice(0,i).concat(sarr.slice(i+i,sarr.length));
					}
					
					checkednum--;
				}
			}
		}
		
		if(sarr.length>0){
			selcos = sarr.join(",");
		}else{
			selcos="";
		}
	}
	$selobj.attr("value",selcos);
}

//全选方法////////////
function quanxuan($event)
{
	var flag=false;
	
	if ($event.attr("checked"))
	{
		flag=true;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	else
	{
		flag=false;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	
	if ($checkbs.length>0)
	{
	   for (var i=0;i<$checkbs.length;i++)
		{
			if(flag)
			{
				if(!$checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}
			else
			{
				if($checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}			 
		}
	}
}
