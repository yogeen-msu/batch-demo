
///////  声明全局变量 Begin  ///////////////////////////////////////////

//acceptstate控件
var $acceptstate;

//selacceptor控件
var $selacceptor;

//win_online控件
var $win_online;

//overlay控件
var $overlay;

//close控件
var $close;

//allotonline控件
var $allotonline;



///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	
	$acceptstate = $(".acceptstate");
	$selacceptor = $(".selacceptor");
	
	$win_online = $("#win_online");
	$overlay = $("#overlay");
	$close = $("#close");
	$allotonline = $(".allotonline");
	
	//初始化变量 End ////
	
	
	/**
	 * 初始化遮罩层大小
	 */
	if($win_online.length > 0){
		$overlay.css("height",$("#f1").height());
	}
	
	
	/**
	 * 初始化受理状态首选项
	 */
	if($acceptstate.length > 0){
		$acceptstate.find("option").each(function (){
		if($(this).val() == $acceptstate.attr("acceptstate")) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 分配客服
	 */
	$selacceptor.click(function(){
		selAcceptor();
	});
	
	/**
	 * 弹出分配客服窗口
	 */
	$allotonline.click(function ()
	{
		$("#code").attr("value",$(this).attr("code"));
		var adminuserids = $(this).attr("adminuserids");
		var useridArray = adminuserids.split(",");
		for (var i=0 ; i < useridArray.length ; ++i)
	　　{
		　　 $(".tc_tab_ts input[value="+useridArray[i]+"]").attr("checked",true);
	　　}
		$win_online.show();
		$overlay.show();
	});
	
	/**
	 * 关闭分配客服窗口
	 */
	$close.click(function ()
	{
		colseWin();
	});
	
	$(".closewin").click(function ()
	{
		colseWin();
	});

});


/**
 * 弹出对话框相关操作
 */
window.onload = function ()
{
	/**
	var oWin = document.getElementById("win_online");
	var oLay = document.getElementById("overlay");
	var oClose = document.getElementById("close");
	*/
	
	var oWin = $win_online.get(0);
	var oLay = $overlay.get(0);
	var oClose = $close.get(0);
	
	if (oWin != null)
	{
		var oH2 = oWin.getElementsByTagName("h2")[0];
		var bDrag = false;
		var disX = disY = 0;
		
		oClose.onmousedown = function (event)
		{
			(event || window.event).cancelBubble = true;
		};
		oH2.onmousedown = function (event)
		{		
			var event = event || window.event;
			bDrag = true;
			disX = event.clientX - oWin.offsetLeft;
			disY = event.clientY - oWin.offsetTop;
			this.setCapture && this.setCapture();
			return false;
		};
		document.onmousemove = function (event)
		{
			if (!bDrag) return;
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
			var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL; 		
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;
			
			oWin.style.marginTop = oWin.style.marginLeft = 0;
			oWin.style.left = iL + "px";
			oWin.style.top = iT + "px";		
			return false;
		};
		document.onmouseup = window.onblur = oH2.onlosecapture = function ()
		{
			bDrag = false;				
			oH2.releaseCapture && oH2.releaseCapture();
		};
	}
};

//分配客服方法
function selAcceptor()
{
	var code = $("#code").val();
	var result = -1;
	
	if(confirm(message1)){		
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
		
		$.ajax({
			url:'allotacceptor.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(data);
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
	
		if(result == 0)
		{
			colseWin();
			alert(message2);
			$("#f1").attr("action","listonlinerole.do").submit();
		}
		else
		{
			alert(message3);
		}		
	}
}

//关闭分配客服窗口方法
function colseWin()
{
	$("#code").attr("value","");
	$(".tc_tab_ts input").each(function (){
		$(this).attr("checked", false);
	});
	
	$win_online.hide();
	$overlay.hide();
}

