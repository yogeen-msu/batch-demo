
///////  声明全局变量 Begin  ///////////////////////////////////////////

//complaintState控件
var $complaintState;

//complaintEffective控件
var $complaintEffective;

//emergencyLevel控件
var $emergencyLevel;

//openrecomplaint控件
var $openrecomplaint;

//openksdetail控件
var $openksdetail;

//addrecomplaint控件
var $addrecomplaint;

//refreshpage控件
var $refreshpage;

//win_tc控件
var $win_tc;

//overlay控件
var $overlay;

//close控件
var $close;

var $allotcomplaint ;

var $win_complaint;

var $close2 ;

var  $selacceptor ;

///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	
	$complaintState = $(".complaintState");
	$complaintEffective = $(".complaintEffective");
	$emergencyLevel = $(".emergencyLevel");
	$openrecomplaint = $(".openrecomplaint");
	$openksdetail = $(".openksdetail");
	$addrecomplaint = $(".addrecomplaint");
	$refreshpage = $(".refreshpage");
	
	$win_tc = $("#win_tc");
	$overlay = $("#overlay");
	$close = $("#close");
	
	//初始化变量 End ////
	
	
	 $allotcomplaint = $("#showSupport");
	 $win_complaint= $("#win_complaint");;
	 $close2 = $("#close2");
	 $selacceptor = $(".selacceptor");
	$allotcomplaint.click(function ()
			{
				//$("#code").attr("value",$(this).attr("code"));
				$win_complaint.show();
				$overlay.show();
			});
	 
		$selacceptor.click(function(){
			var $this = $(this);
			var acceptor = $this.attr("acceptor");
			selAcceptor(acceptor);
		});
		
		$(".selacceptortrans").click(function(){
			var $this = $(this);
			var acceptor = $this.attr("acceptor");
			selAcceptorTrans(acceptor);
		});
		
		$close2.click(function ()
				{
					colseWin2();
				});
	
	/**
	 * 初始化遮罩层大小
	 */
	if($win_tc.length > 0){
		$overlay.css("height",$("#f1").height());
	}
	
	
	/**
	 * 初始化客诉状态首选项
	 */
	if($complaintState.length > 0){
		$complaintState.find("option").each(function (){
		if($(this).val() == $complaintState.attr("complaintState")) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 初始化投诉是否有效
	 */
	if($complaintEffective.length > 0){
		$complaintEffective.find("option").each(function (){
		if($(this).val() == $complaintEffective.attr("complaintEffective")) $(this).attr("selected", true);
		});
	
		$complaintEffective.change(function(){
			var $this = $(this);
			modiComplaintEffective($this.val());
		});
	}
	

	/**
	 * 初始化客诉客户紧急情况
	 */
	if($emergencyLevel.attr("emergencyLevel") == "1") $emergencyLevel.append(inoneday);
	if($emergencyLevel.attr("emergencyLevel") == "2") $emergencyLevel.append(inthreeday);
	if($emergencyLevel.attr("emergencyLevel") == "3") $emergencyLevel.append(intwoweek);
	if($emergencyLevel.attr("emergencyLevel") == "4") $emergencyLevel.append(intwomonth);
	
	/**
	 * 清空上传文件控件
	 */
	/**
	$(".cleanfile").click(function(){
		$(".attachmentfile").val("");
	});
	*/
	
	/**
	 * 刷新页面
	 */
	$refreshpage.click(function(){
		//window.location.reload();
		$("#f1").attr("action","showcomplaint.do").submit();
	});
	
	/**
	 * 点击回复事件
	 */
	$openrecomplaint.click(function(){
		addWinColseOpen();
	});
	
	$addrecomplaint.find(".addbutton").click(function(){
		
		if(!checkData(document.getElementsByName("reCustomerComplaintInfoAdd.reContent")[0].value,"string",true))
		{
			alert(message21);
			document.getElementsByName("reCustomerComplaintInfoAdd.reContent")[0].focus();
			return false;
		}
		else if(document.getElementsByName("reCustomerComplaintInfoAdd.reContent")[0].value.length>500)
		{
			message27 = message27.replace("{0}","500");
			alert(message27);
			document.getElementsByName("reCustomerComplaintInfoAdd.reContent")[0].focus();
			return false;
		}
		
		addRecomplaint();
	});
	
	/**
	 * 弹出客诉表格
	 */
	$openksdetail.click(function ()
	{		
		$win_tc.show();
		$overlay.show();
	});
	
	/**
	 * 关闭分配客服窗口
	 */
	$close.click(function ()
	{
		colseWin();
	});
	
	$(".close_win_tc").click(function ()
	{
		colseWin();
	});
	
	
	$(".queryAttachment").click(function()
	{
		var $this = $(this);
		var attachment = $this.attr("attachment");
		var contextpath = $("#contextpath").val();
		
		if(attachment == null || attachment =='')
		{
			alert(message22);
			return;
		}
//		jump(contextpath+"/download.do?downloadPath="+attachment);
		jump(contextpath + "/DownloadComplaintAction.do?downloadPath="+attachment);
	});

});


/**
 * 弹出对话框相关操作
 */
window.onload = function ()
{
	
	var oWin = $win_tc.get(0);
	var oLay = $overlay.get(0);
	var oClose = $close.get(0);
	
	if (oWin != null)
	{
		var oH2 = oWin.getElementsByTagName("h2")[0];
		var bDrag = false;
		var disX = disY = 0;
		
		oClose.onmousedown = function (event)
		{
			(event || window.event).cancelBubble = true;
		};
		oH2.onmousedown = function (event)
		{		
			var event = event || window.event;
			bDrag = true;
			disX = event.clientX - oWin.offsetLeft;
			disY = event.clientY - oWin.offsetTop;
			this.setCapture && this.setCapture();
			return false;
		};
		document.onmousemove = function (event)
		{
			if (!bDrag) return;
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
			var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL; 		
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;
			
			oWin.style.marginTop = oWin.style.marginLeft = 0;
			oWin.style.left = iL + "px";
			oWin.style.top = iT + "px";		
			return false;
		};
		document.onmouseup = window.onblur = oH2.onlosecapture = function ()
		{
			bDrag = false;				
			oH2.releaseCapture && oH2.releaseCapture();
		};
	}
};
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}

//添加客诉回复
function addRecomplaint()
{
	$("#f1").attr("action","addrecomplaint.do").attr("target","hidden_frame").submit();
}


//修改客诉有效性
function modiComplaintEffective(complaintEffective)
{
	var code = $("#code").val();
	var meg = message23;
	if(complaintEffective==1) meg=message24;
	
	var result = -1;
	
	if(confirm(meg)){
		$.ajax({
			url:'modicomplainteffective.do?ajax=yes',
			type:"post",
			data:{"customerComplaintInfo.code":code,"customerComplaintInfo.complaintEffective":complaintEffective},
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(eval(data));
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
	
		if(result == 0)
		{
			alert(message25);
		}
		else
		{
			alert(message26);
			$("#f1").attr("action","showcomplaint.do").submit();
		}
	}
	else
	{
		$("#f1").attr("action","showcomplaint.do").submit();
	}
}



//修改客诉状态
function modiComplaintState(complaintState)
{
	var code = $("#code").val();
	var meg = confirmMod_str;
	var reason="";
	
	if(complaintState==2){
	var dialog = art.dialog({
	    content: '<p>'+message28+'</p>'
	    	+ '<textarea id="demo-labs-input" style="width:30em; padding:4px;height:30px;" ></textarea>',
	    fixed: true,
	    id: 'Fm7',
	    cancelVal:message30,
	    okVal: message29,
	    ok: function () {
	    	var input = document.getElementById('demo-labs-input');
	       
	    	if (input.value == '') {
	            input.select();
	            input.focus();
	            return false;
	        } else {
	        	reason=input.value ;
	        	this.close();
	        	ajaxUptStatus(code,complaintState,reason);
	        };
	    },
	    cancel: true
	});
	}else{
		      ajaxUptStatus(code,complaintState,reason);
	}
		
	
}


function ajaxUptStatus(code,complaintState,reason){
	var result = -1;
	$.ajax({
		url:'modicomplaintstate.do?ajax=yes',
		type:"post",
		data:{"customerComplaintInfo.code":code,"customerComplaintInfo.complaintState":complaintState,"reCustomerComplaintInfoAdd.reContent":reason},
		dataType:'JSON',
		async: false,
		success :function(data){
			var jsonData = eval(eval(data));
			result = jsonData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	if(result == 0)
	{
		alert(modSuccess_str);
		$("#f1").attr("action","showcomplaint.do").submit();
	}
	else
	{
		alert(modFail_str);
	}
	
	
}


//分配客服方法
function selAcceptor(acceptor)
{
	var code = $("#code").val();
	var result = -1;
	
	if(confirm(message1)){
		$.ajax({
			url:'allotacceptor.do?ajax=yes',
			type:"post",
			data:{"code":code,"acceptor":acceptor},
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(data);
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
	
		if(result == 0)
		{
			colseWin();
			alert(message2);
			window.parent.setMessageTab(2);
		}
		else
		{
			alert(message3);
		}		
	}
}
//关闭分配客服窗口方法
function colseWin()
{
	$("#code").attr("value","");
	
	$win_complaint.hide();
	$overlay.hide();
}

//转发客诉方法
function selAcceptorTrans(acceptor)
{
	var code = $("#code").val();
	var result = -1;
	if(confirm(message4)){
		$.ajax({
			url:'transcomplaint.do?ajax=yes',
			type:"post",
			data:{"code":code,"acceptor":acceptor},
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(data);
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
	
		if(result == 0)
		{
			colseWin2();
			alert(message5);
			window.parent.setMessageTab(2);
		}
		else
		{
			alert(message5);
			colseWin2();
			
		}		
	}
}

//回复窗口弹出或隐藏
function addWinColseOpen()
{	
	$addrecomplaint.slideToggle("fast");
	
	if($addrecomplaint.is(":hidden"))
	{
		$addrecomplaint.find("textarea").attr("text","");
	}
	else
	{
		$addrecomplaint.find("textarea").focus();
	}
}
//关闭分配客服窗口方法
function colseWin2()
{	
	$win_complaint.hide();
	$overlay.hide();
}

//关闭分配客服窗口方法
function colseWin()
{	
	$win_tc.hide();
	$overlay.hide();
}

