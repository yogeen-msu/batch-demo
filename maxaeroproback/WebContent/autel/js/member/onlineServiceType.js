
///////  声明全局变量 Begin  ///////////////////////////////////////////

//sealername控件
var $sealername;

//sourcecode控件
var $sourcecode;

//salecfgcode控件
var $salecfgcode;

//areacfgcode控件
var $areacfgcode;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	$sealername = $(".sealername");
	$sourcecode = $(".sourcecode");
	$salecfgcode = $(".salecfgcode");
	$areacfgcode = $(".areacfgcode");
	
	//初始化变量 End ////
	
	
	if($sourcecode.length > 0){
		$sourcecode.find("option").each(function (){
		if($(this).val() == $sourcecode.attr("sourcecode")) $(this).attr("selected", true);
		});
	}
	
	
	if($salecfgcode.length > 0){
		$salecfgcode.find("option").each(function (){
		if($(this).val() == $salecfgcode.attr("salecfgcode")) $(this).attr("selected", true);
		});
	}
	
	
	if($areacfgcode.length > 0){
		$areacfgcode.find("option").each(function (){
		if($(this).val() == $areacfgcode.attr("areacfgcode")) $(this).attr("selected", true);
		});
	}
	
	$(".addonlineservicetype").click(function(){
		var $this = $(this);
		addOnlineServiceType();
	});
	
	$(".editonlineservicetype").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		editOnlineServiceType(code);
	});
	
	$(".insertonlineservicetype").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("onlineServiceTypeAdd.name")[0].value,"string",true))
		{
			alert("在线客服类型名称不能为空");
			document.getElementsByName("onlineServiceTypeAdd.name")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("onlineServiceTypeAdd.proTypeCode")[0].value,"string",true))
		{
			alert("请选择产品型号");
			document.getElementsByName("onlineServiceTypeAdd.proTypeCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("onlineServiceTypeAdd.saleCfgCode")[0].value,"string",true))
		{
			alert("请选择销售配置");
			document.getElementsByName("onlineServiceTypeAdd.saleCfgCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("onlineServiceTypeAdd.areaCfgCode")[0].value,"string",true))
		{
			alert("请选择区域配置");
			document.getElementsByName("onlineServiceTypeAdd.areaCfgCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("onlineServiceTypeAdd.price")[0].value,"float",false))
		{
			alert("请填写正确的金额");
			document.getElementsByName("onlineServiceTypeAdd.price")[0].focus();
			return false;
		}
		*/
		insertOnlineServiceType();
	});
	
	$(".updateonlineservicetype").click(function(){
		var $this = $(this);
		/**
		if(!checkData(document.getElementsByName("onlineServiceTypeEdit.name")[0].value,"string",true))
		{
			alert("在线客服类型名称不能为空");
			document.getElementsByName("onlineServiceTypeEdit.name")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("onlineServiceTypeEdit.proTypeCode")[0].value,"string",true))
		{
			alert("请选择产品型号");
			document.getElementsByName("onlineServiceTypeEdit.proTypeCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("onlineServiceTypeEdit.saleCfgCode")[0].value,"string",true))
		{
			alert("请选择销售配置");
			document.getElementsByName("onlineServiceTypeEdit.saleCfgCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("onlineServiceTypeEdit.areaCfgCode")[0].value,"string",true))
		{
			alert("请选择区域配置");
			document.getElementsByName("onlineServiceTypeEdit.areaCfgCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("onlineServiceTypeEdit.price")[0].value,"float",false))
		{
			alert("请填写正确的金额");
			document.getElementsByName("onlineServiceTypeEdit.price")[0].focus();
			return false;
		}
		*/
		updateOnlineServiceType();
	});
	
	$(".dellonlineservicetype").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delOnlineServiceType(code);
	});
	
	$(".dellonlineservicetypes").click(function(){
		var $this = $(this);
		delOnlineServiceTypes();
	});
});

//去到添加在线客服类型页面
function addOnlineServiceType()
{
	$("#f1").attr("action","addonlineservicetype.do").submit();
}

//去到修改在线客服类型页面
function editOnlineServiceType(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editonlineservicetype.do").submit();
}

//添加在线客服类型方法
function insertOnlineServiceType()
{
	$("#f1").attr("action","insertonlineservicetype.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'insertonlineservicetype.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("在线客服类型添加成功");
		$("#f1").attr("action","listonlineservicetype.do").submit();
	}
	else
	{
		alert("在线客服类型添加失败");
	}
	*/
}

//修改在线客服类型方法
function updateOnlineServiceType()
{
	$("#f1").attr("action","updateonlineservicetype.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'updateonlineservicetype.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("在线客服类型修改成功");
		$("#f1").attr("action","listonlineservicetype.do").submit();
	}
	else
	{
		alert("在线客服类型修改失败");
	}
	*/
}

//删除在线客服类型方法
function delOnlineServiceType(code)
{
	if(confirm(message1)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delonlineservicetype.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delonlineservicetype.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("在线客服类型删除成功");
			$("#f1").attr("action","listonlineservicetype.do").submit();
		}
		else if(result == 11)
		{
			alert("在线客服类型已被使用，无法删除");
		}
		else
		{
			alert("在线客服类型删除失败");
		}
		*/
	}
}

//删除多个在线客服类型方法
function delOnlineServiceTypes()
{
	if(confirm(message2)){		
		$("#f1").attr("action","delonlineservicetypes.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delonlineservicetypes.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("在线客服类型删除成功");
			$("#f1").attr("action","listonlineservicetype.do").submit();
		}
		else if(result == 11)
		{
			alert("在线客服类型已被使用，无法删除");
		}
		else
		{
			alert("在线客服类型删除失败");
		}
		*/
	}
}

