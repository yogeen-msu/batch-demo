
///////  声明全局变量 Begin  ///////////////////////////////////////////

//选中code字符串控件
var $selobj;

//复选框
var $checkbs;

//全选框
var $allckbs;

//code组成的字符串中每个code值要不要加单引号（true：要加单引号，false：不加单引号）
var isYinhao = false;

//复选框数量
var checkbnum = 0;

//选中的复选框数量
var checkednum = 0;

//languagecode控件
var $languagecode;

//isallowsendemail控件
var $isallowsendemail;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	$selobj = $("#selectedcodes");
	$checkbs = $("input[name='checkboxcode']");
	$allckbs = $("input[name='allCheckbox']");
	isYinhao = true;
	checkbnum = $checkbs.length;
	$languagecode = $(".languagecode");
	$isallowsendemail = $(".isallowsendemail");
	//初始化变量 End ////
	
	
	/**
	 * 初始化用户状态首选项
	 */
	if($(".actstatesel").length > 0){
		$(".actstatesel").find("option").each(function (){
		if($(this).val() == $(".actstatesel").attr("actstatesel")) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 初始化用户状态
	 */
	if($(".actstate").length > 0){
		$(".actstate").each(function (){
			if($(this).attr("actstate") == "1") $(this).append(m_activated);
			if($(this).attr("actstate") == "0") $(this).append(m_noactivated);
		});
	}
	
	
	if($languagecode.length > 0){
		$languagecode.find("option").each(function (){
		if($(this).val() == $languagecode.attr("languagecode")) $(this).attr("selected", true);
		});
		if($languagecode.attr("languagecode") ==$("#languageCodeCN").val())
		{
			$(".customername").hide();
			$(".customernameenglish").show();
		}
		else
		{
			$(".customername").hide();
			$(".customernameenglish").show();
		}
	}
	
	if($isallowsendemail.length > 0){
		if($isallowsendemail.attr("isallowsendemail") == "1") $isallowsendemail.attr("checked", true);
	}
	
	$(".languagecode").change(function(){
		var $this = $(this);
		if($this.val()==$("#languageCodeCN").val())
		{
			$(".customername").hide();
			$(".customernameenglish").show();
		}
		else
		{
			$(".customername").hide();
			$(".customernameenglish").show();
		}
	});
	
	$(".edituser").click(function(){
		var $this = $(this);
		var code = $this.parent().parent().attr("code");
		editCustomer(code);
	});
	
	$(".updatePassword").click(function(){
		$("#f1").attr("action","savePwd.do").submit();
	});
	
	$(".updatecustomer").click(function(){
		var $this = $(this);
		updateCustomer();
	});
	
	$(".delluser").click(function(){
		var $this = $(this);
		var code = $this.parent().parent().attr("code");
		delCustomer(code);
	});
	
	$(".activeUser").click(function(){
		var $this = $(this);
		var code = $this.parent().parent().attr("code");
		activeCustomer(code);
	});
	
	
	$(".dellusers").click(function(){
		var $this = $(this);
		if(checkednum>0)
			delCustomers();
		else
			alert(message4);
	});
	
	$checkbs.click(function(){
		var $this = $(this);
		xuanze($this);
	});
	
	$allckbs.click(function(){
		var $this = $(this);
		quanxuan($this);
	});
	
	$(".exportexcel").click(function(){
		var $this = $(this);
		var contextpath = $("#contextpath").val();
		$("#f1").attr("action",contextpath+"/customerInfoExportExcel.do").submit();
	});
	
	$(".searchbutton").click(function(){
		var $this = $(this);
		$("#f1").submit();
	});
});

//去到修改用户页面
function editCustomer(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","editcustomer.do").submit();
}

//修改用户方法
function updateCustomer()
{
	$("#f1").attr("action","updatecustomer.do").submit();
	/**
	var result = -1;
	var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
	$.ajax({
		url:'updatecustomer.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert("用户修改成功");
		$("#f1").attr("action","listcustomer.do").submit();
	}
	else
	{
		alert("用户修改失败");
	}
	*/
}

//删除用户方法
function delCustomer(code)
{
	if(confirm(message2)){
		$("#code").attr("value",code);
		$("#f1").attr("action","delcustomer.do").submit();
	}
}

//激活用户方法
function activeCustomer(code)
{
	
		$("#code").attr("value",code);
		$("#f1").attr("action","activecustomer.do").submit();
	
}


//删除多个用户方法
function delCustomers()
{
	if(confirm(message3)){
		$("#f1").attr("action","delcustomers.do").submit();
		/**
		var result = -1;
		var data = $("#f1").serializeArray(); //自动将form表单封装成json
	
		$.ajax({
			url:'delcustomers.do?ajax=yes',
			type:"post",
			data:data,
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert("用户删除成功");
			$("#f1").attr("action","listcustomer.do").submit();
		}
		else if(result == 11)
		{
			alert("用户已被使用，无法删除");
		}
		else
		{
			alert("用户删除失败");
		}
		*/
	}
}


//复选方法///////////////
function xuanze($event)
{
	var selcos = $selobj.attr("value");
	var code = $event.attr("value");
	
	if(isYinhao) code = "'"+code+"'";
	
	if($event.attr("checked"))
	{
		if(selcos=="") selcos=code;
			else selcos=selcos+","+code;
		checkednum++;

		if(checkednum==checkbnum)
		{
			$allckbs.each(function(){
				$(this).attr("checked",true);
			});
		}
	}
	else
	{		
		$allckbs.each(function(){
			$(this).attr("checked",false);
		});
		
		var sarr = selcos.split(",");
		if(sarr!=null&&sarr.length>0){
			for(var i=0;i<sarr.length;i++){
				if(code==sarr[i]){
					if(i==0){
						sarr = sarr.slice(i+1,sarr.length);
					}else{
						sarr = sarr.slice(0,i).concat(sarr.slice(i+i,sarr.length));
					}
					
					checkednum--;
				}
			}
		}
		
		if(sarr.length>0){
			selcos = sarr.join(",");
		}else{
			selcos="";
		}
	}
	$selobj.attr("value",selcos);
}

//全选方法////////////
function quanxuan($event)
{
	var flag=false;
	
	if ($event.attr("checked"))
	{
		flag=true;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	else
	{
		flag=false;
		$allckbs.each(function(){
			$(this).attr("checked",flag);
		});
	}
	
	if ($checkbs.length>0)
	{
	   for (var i=0;i<$checkbs.length;i++)
		{
			if(flag)
			{
				if(!$checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}
			else
			{
				if($checkbs.eq(i).attr("checked"))
				{
					$checkbs.eq(i).attr("checked",flag);
					xuanze($checkbs.eq(i));
				}
			}			 
		}
	}
}
