
///////  声明全局变量 Begin  ///////////////////////////////////////////

//acceptstate控件
var $acceptstate;

//usertype控件
var $usertype;

//selacceptor控件
var $selacceptor;

//showcomplaint控件
var $showcomplaint;

//win_complaint控件
var $win_complaint;

//overlay控件
var $overlay;

//close控件
var $close;

//allotcomplaint控件
var $allotcomplaint;

var $complaintState2;

///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	
	$acceptstate = $(".acceptstate");
	$usertype = $(".usertype");
	$selacceptor = $(".selacceptor");
	$showcomplaint = $(".showcomplaint");
	
	$win_complaint = $("#win_complaint");
	$overlay = $("#overlay");
	$close = $("#close");
	$allotcomplaint = $(".allotcomplaint");
	$complaintState2=$("#complaintState2");
	//初始化变量 End ////
	
	
	/**
	 * 初始化遮罩层大小
	 */
	if($win_complaint.length > 0){
		$overlay.css("height",$("#f1").height());
	}
	
	
	/**
	 * 初始化受理状态首选项
	 */
	if($acceptstate.length > 0){
		$acceptstate.find("option").each(function (){
		if($(this).val() == $acceptstate.attr("acceptstate")) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 初始化投诉人类型首选项
	 */
	if($usertype.length > 0){
		$usertype.find("option").each(function (){
		if($(this).val() == $usertype.attr("usertype")) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 初始化查询条件之状态
	 */
	if($complaintState2.length > 0){
		$complaintState2.find("option").each(function (){
		if($(this).val() == $("#complaintState").val()) $(this).attr("selected", true);
		});
	}
	
	/**
	 * 初始化客诉状态
	 */
	if($(".complaintstate").length > 0){
		$(".complaintstate").each(function (){
			if($(this).attr("complaintstate") == "1") $(this).append(stateopen);
			if($(this).attr("complaintstate") == "2") $(this).append(stateguaqi);
			if($(this).attr("complaintstate") == "3") $(this).append(stateddhf);
			if($(this).attr("complaintstate") == "4") $(this).append(stateguanbi);
		});
	}
	
	/**
	 * 分配客服
	 */
	$selacceptor.click(function(){
		var $this = $(this);
		var acceptor = $this.attr("acceptor");
		selAcceptor(acceptor);
	});
	/**
	 * 转发客诉
	 */
	$(".selacceptortrans").click(function(){
		var $this = $(this);
		var acceptor = $this.attr("acceptor");
		selAcceptorTrans(acceptor);
	});
	
	/**
	 * 查看客诉信息
	 */
	$showcomplaint.click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		showComplaint(code);
	});
	
	/**
	 * 弹出分配客服窗口
	 */
	$allotcomplaint.click(function ()
	{
		$("#code").attr("value",$(this).attr("code"));
		
		$win_complaint.show();
		$overlay.show();
	});
	
	/**
	 * 关闭分配客服窗口
	 */
	$close.click(function ()
	{
		colseWin();
	});

});


/**
 * 弹出对话框相关操作
 */
window.onload = function ()
{
	/**
	var oWin = document.getElementById("win_complaint");
	var oLay = document.getElementById("overlay");
	var oClose = document.getElementById("close");
	*/
	
	var oWin = $win_complaint.get(0);
	var oLay = $overlay.get(0);
	var oClose = $close.get(0);
	
	if (oWin != null)
	{
		var oH2 = oWin.getElementsByTagName("h2")[0];
		var bDrag = false;
		var disX = disY = 0;
		
		oClose.onmousedown = function (event)
		{
			(event || window.event).cancelBubble = true;
		};
		oH2.onmousedown = function (event)
		{		
			var event = event || window.event;
			bDrag = true;
			disX = event.clientX - oWin.offsetLeft;
			disY = event.clientY - oWin.offsetTop;
			this.setCapture && this.setCapture();
			return false;
		};
		document.onmousemove = function (event)
		{
			if (!bDrag) return;
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
			var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL; 		
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;
			
			oWin.style.marginTop = oWin.style.marginLeft = 0;
			oWin.style.left = iL + "px";
			oWin.style.top = iT + "px";		
			return false;
		};
		document.onmouseup = window.onblur = oH2.onlosecapture = function ()
		{
			bDrag = false;				
			oH2.releaseCapture && oH2.releaseCapture();
		};
	}
};

//分配客服方法
function selAcceptor(acceptor)
{
	var code = $("#code").val();
	var result = -1;
	
	if(confirm(message1)){
		$.ajax({
			url:'allotacceptor.do?ajax=yes',
			type:"post",
			data:{"code":code,"acceptor":acceptor},
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(data);
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
	
		if(result == 0)
		{
			colseWin();
			alert(message2);
			$("#f1").attr("action","listcomplaintrole.do").submit();
		}
		else
		{
			alert(message3);
		}		
	}
}

//转发客诉方法
function selAcceptorTrans(acceptor)
{
	var code = $("#code").val();
	var result = -1;
	
	if(confirm(message4)){
		$.ajax({
			url:'transcomplaint.do?ajax=yes',
			type:"post",
			data:{"code":code,"acceptor":acceptor},
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(data);
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
	
		if(result == 0)
		{
			colseWin();
			alert(message5);
			$("#f1").attr("action","listcustomercomplaint.do").submit();
		}
		else
		{
			alert(message6);
		}		
	}
}

//查看客诉信息方法
function showComplaint(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","showcomplaint.do").submit();
}

//关闭分配客服窗口方法
function colseWin()
{
	$("#code").attr("value","");
	
	$win_complaint.hide();
	$overlay.hide();
}

