<%@ include file="/autel/js/common/fmt.js.jsp"%>
var ProductInfo  ={
	init:function(){
		var self  = this;
		Cop.Dialog.init({id:"selectProductTypeDlg",modal:false,title:"<fmt:message key='product.producttypevo' />",height:"400px",width:"650px"});
		$("#productTypeSelect").click(function(){
			self.openProductTypeSelectDlg();
		});
		
		Cop.Dialog.init({id:"selectSaleContractDlg",modal:false,title:"<fmt:message key='marketman.salecontractman.sale.contract' />",height:"400px",width:"650px"});
		$("#saleContractSelect").click(function(){
			var proTypeCode = $("#productInfo_proTypeCode").val();		//销售契约关联的产品型号
			if(proTypeCode == null || proTypeCode == ""){
				alert("<fmt:message key='product.productinfo.release.select' />");
			}else{
				self.openSaleContractSelectDlg(proTypeCode);
			}
		});
		
		Cop.Dialog.init({id:"selectRepairProductDlg",modal:false,title:"<fmt:message key='syssetting.outfactoryproman.repairproduct' />",height:"400px",width:"750px"});
		$("#repairProductSelect").click(function(){
			self.openRepairProductSelectDlg();
		});
		
	}, 
	
	/**
	 * 打开产品型号选择对话框
	 */
	openProductTypeSelectDlg:function(){
		var self  = this;
		$("#selectProductTypeDlg").load("getProUpgrade.do?ajax=yes",function(){
			$(".selectPdTypeItem").click(function(){
				var code = $(this).attr("selectCode");		//产品型号编号
				var name = $(this).attr("selectName");		//产品型号名称
				self.productTypeAdd(code,name);
			});
		});
		Cop.Dialog.open("selectProductTypeDlg");
	},
	
	/**
	 * 保存选择编号
	 */
	productTypeAdd:function(code,name){
		$("#productInfo_proTypeCode").val(code);
		$("#productInfo_proTypName").val(name);
		Cop.Dialog.close("selectProductTypeDlg");
		
		$("#productInfo_saleContractCode").val("");
		$("#productInfo_saleContractName").val("");
		
	},
	
	/**
	 * 打开销售契约选择对话框
	 */
	openSaleContractSelectDlg:function(proTypeCode){
		var self  = this;
		$("#selectSaleContractDlg").load("product-info!selectsaleContract.do?ajax=yes&proTypeCode="+proTypeCode,function(){
			$(".selectSaleContractItem").click(function(){
				var code = $(this).attr("selectCode");		//产品型号编号
				var name = $(this).attr("selectName");		//产品型号名称
				self.saleContractAdd(code,name);
			});
		});
		Cop.Dialog.open("selectSaleContractDlg");
	},
	/**
	 * 保存选择编号
	 */
	saleContractAdd:function(code,name){
		$("#productInfo_saleContractCode").val(code);
		$("#productInfo_saleContractName").val(name);
		Cop.Dialog.close("selectSaleContractDlg");
	},
	
	
	/**
	 * 打开维修产品选择对话框
	 */
	openRepairProductSelectDlg:function(){
		var self  = this;
		$("#selectRepairProductDlg").load("product-repair-record!selectProductInfo.do?ajax=yes",function(){
			$(".selectProductItem").click(function(){
				var code = $(this).attr("selectCode");		//编号
				var proTypeName = $(this).attr("selectProTypeName");		//产品型号
				var serialNo = $(this).attr("selectSerialNo");		//序列号
				var proDate = $(this).attr("selectProDate");		//出厂日期
				
				repairProductAdd(code,proTypeName,serialNo,proDate);
			});
		});
		Cop.Dialog.open("selectRepairProductDlg");
	}
};

var repairProductAdd = function(code,proTypeName,serialNo,proDate){
	$("#productRepairRecord_proCode").val(code);
	$("#productRepairRecord_proTypName").val(proTypeName);
	$("#productRepairRecord_proSerialNo").val(serialNo);
	$("#productRepairRecord_proDate").val(proDate);
	
	Cop.Dialog.close("selectRepairProductDlg");
};





function bindEvent(pager,grid){
	var grid = pager.parent();
	 pager.find("li>a").unbind(".click").bind("click",function(){
		 load($(this).attr("pageno"),grid);
	 }); 
	 pager.find("a.selected").unbind("click");
};

function pageQuery(){
	$.ajax({
		url:"product-repair-record!selectProductInfo.do",
		data: $('#pageSelectForm').serialize(), 
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			
			$(".selectProductItem").click(function(){
				var code = $(this).attr("selectCode");		//编号
				var proTypeName = $(this).attr("selectProTypeName");		//产品型号
				var serialNo = $(this).attr("selectSerialNo");		//序列号
				var proDate = $(this).attr("selectProDate");		//出厂日期
				
				repairProductAdd(code,proTypeName,serialNo,proDate);
			});
		},
		error:function(){
			alert("Error :(");
		}
	});
};

