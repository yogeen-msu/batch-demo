
/**
 * 在线客服类型名称管理JS
 */
//添加的数据
var addArray = new Array();
//删除的数据
var delArray = new Array();

var tableId = "#onlineTypeNameTable";

var form = "#onlineTypeNameForm";

var onlineTypeName1 = "onlineServiceTypeNames";

var onlineTypeName2 = "delOnlineServiceTypeNames";

$(document).ready(function(){

	$("#finsh").click(function(){
		var onlineTypeCode = $("#onlineTypeCode").val();
		//选择的option
		var $selectOption = $("#language option:selected");
		//languageCode
		var value= $selectOption.val();
		//languageName
		var text = $selectOption.text();
		var onlineTypeName = $("#onlineTypeName").val();
		if(value==null || onlineTypeName=="")
		{
			alert(message5);
			return false;
		}
		
		var onlineServiceTypeName = new OnlineServiceTypeName(text,value,onlineTypeCode,onlineTypeName);
		
		addArray.push(onlineServiceTypeName);
		
		var oWin = $("#win");
		var oLay = $("#overlay");
		oWin.hide();
		oLay.hide();
		buildHtml(onlineServiceTypeName,tableId);
		$selectOption.remove();
	});
	//表单提交
	$("#sumbitBtn").click(function(){
		var aLen = addArray.length;
		var dLen = delArray.length;
		
		//if(aLen == 0 && dLen == 0){
			//alert(message3);
			//return false;
		//}else{
			var $form = $(form);
			var inputHiddenHtml = "";
			for ( var i = 0; i < aLen; i++) {
				var v = addArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+onlineTypeName1+"["+i+"].onlineTypeCode' value='"+v.onlineTypeCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+onlineTypeName1+"["+i+"].languageCode' value='"+v.languageCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+onlineTypeName1+"["+i+"].name' value='"+v.onlineTypeName+"'/>";
			}
			for ( var i = 0; i < dLen; i++) {
				var v = delArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+onlineTypeName2+"["+i+"].id' value='"+v+"'/>";
			}
			$form.append(inputHiddenHtml);
			$form.submit();
		//}
	});
});

/**
 * 
 * @param onlineServiceTypeName	在线客服类型名称
 */
function buildHtml(onlineServiceTypeName,tableId){
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+onlineServiceTypeName.languageName+"</td>");
	var $td2 = $("<td>"+onlineServiceTypeName.onlineTypeName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='###' languageName='"+onlineServiceTypeName.languageName+"' languageCode='"+onlineServiceTypeName.languageCode+"'>移除</a>");
	
	$a.click(function(){
		var languageCode = $(this).attr("languageCode");
		addArray.remove(languageCode);
		addOption(this);
		$(this).closest("tr").remove();
	});
	$td4.append($a);
	
	$tr.append($td1).append($td2).append($td4);
	$(tableId).append($tr);
}

/**
 * 删除在线客服类型名称
 * @param event
 */
function delName(event){
	if(confirm(message4)){
		var $this = $(event);
		//在线客服类型名称id
		var onlineTypeNameId = $this.attr("onlineTypeNameId");
		delArray.push(onlineTypeNameId);
		addOption(event);
		$this.closest("tr").remove();
	}
}

/**
 * 添加下拉菜单语言选项
 * @param event
 */
function addOption(event){
	var $this = $(event);
	var languageCode = $this.attr("languageCode");
	var languageName = $this.attr("languageName");
	var $option = $("<option value='"+languageCode+"'>"+languageName+"</option>");
	$("#language").append($option);
}

/**
 * 
 * @param languageName	语言名称
 * @param languageCode	语言编码
 * @param onlineTypeCode	在线客服类型编码
 * @param onlineTypeName	在线客服类型名称
 */
function OnlineServiceTypeName(languageName,languageCode,onlineTypeCode,onlineTypeName){
	this.languageName = languageName;
	this.languageCode = languageCode;
	this.onlineTypeCode = onlineTypeCode;
	this.onlineTypeName = onlineTypeName;
}

Array.prototype.remove = function(languageCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(languageCode == this[i].languageCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};