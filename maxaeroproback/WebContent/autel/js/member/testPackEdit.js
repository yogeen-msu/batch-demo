
///////  声明全局变量 Begin  ///////////////////////////////////////////

var testPackLanguagePackArray = new Array();


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////

	
	//初始化变量 End ////
	
	$(".addtestpack").click(function(){
		var $this = $(this);
		addTestPack();
	});
	
	$(".delltestpack").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		delTestPack(code);
	});
	
	$(".showtestpack").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		showTestPack(code);
	});
	
	$(".close_win_testpackdetail").click(function(){
		closeWinTestPackDetail();
	});
	
});









/**
 * 测试包测试包JS
 */

$(document).ready(function(){
	
	var bool = false;
	
	var languageArray = new Array();
	
	/**
	 * 添加语言包事件
	 */
	
	$("#addTestPackLanguagePackBtn").click(function(){
		
		var oWin = $("#win");
		var oLay = $("#overlay");
		 $("#downloadPath").val("");
		 $("#testFilePath").val("");
		 $("#memo").val("");
		 $("#languageTestCode").val("");
		 $("#operateType").val("add");
		
		oLay.show();
		oWin.show();
		//oLay.style.display = "block";
		//oWin.style.display = "block";
		
		$("#win").find("select option").each(function(){
			if(bool){
				return false;
			}
			//option选项的值
			var languageCode = $(this).val();
			//option选项的文本
			var languageName = $(this).text();
			
			var language = new Language(languageCode,languageName);
			languageArray.push(language);
		});
		bool = true;
	});
	
$("a[name^='editTestPackLanguagePack']").click(function(){
	    var code =$(this).attr("code") ;
	    var testFilePath =$(this).attr("testFilePath") ;	
	    var languageName =$(this).attr("languageName") ;	
	    var memo =$(this).attr("memo") ;	
	    var downloadPath =$(this).attr("downloadPath") ;	
	    $("#language").find("option[text="+languageName+"]").attr("selected",true);
	    $("#language").attr("disabled","true");
	    $("#downloadPath").val(downloadPath);
	    $("#testFilePath").val(testFilePath);
	    $("#memo").val(memo);
	    $("#languageTestCode").val(code);
	    $("#operateType").val("update");
	    
	    var oWin = $("#win");
		var oLay = $("#overlay");
		
		oLay.show();
		oWin.show();
		//oLay.style.display = "block";
		//oWin.style.display = "block";
		
		$("#win").find("select option").each(function(){
			if(bool){
				return false;
			}
			//option选项的值
			var languageCode = $(this).val();
			//option选项的文本
			var languageName = $(this).text();
			
			var language = new Language(languageCode,languageName);
			languageArray.push(language);
		});
		bool = true;
	});
	
	
$("a[name^='delTestPackLanguagePack']").click(function(){
	 var code =$(this).attr("code") ;
	 delTestLanguagePack(code);
});

	/**
	 * 确定
	 */
	$("#finsh").click(function(){
		
		//语言类型
		var languageCode = $("#language option:selected").val();
		//语言名称
		var languageName = $("#language option:selected").text();
		//语言包相对路径
		var downloadPath = $("#downloadPath").val();
		//可测车型文档路径
		var testFilePath = $("#testFilePath").val();
		//操作类型
		var operateType=$("#operateType").val();
		//语言包Code
		var languageTestCode=$("#languageTestCode").val();
		//测试包code
		var testPackVoCode=$('#testPackVoCode').val();
		//语言包说明
		var memo = $("#memo").val();
	
		
		
		
		$("#testPackLanguagePackTable tr").each(function(i){
			if($(this).attr("id")=="languageClass"){
				languageStr+=$(this).attr("languageCode")+",";
			 
			}
		});
		
		if(languageCode == "" || languageName == ""){
			alert(message3);
			$("#language").focus();
			return false;
		}

		if(downloadPath == ""){
			alert(message4);
			$("#downloadPath").focus();
			return false;
		}

		if(testFilePath == ""){
			alert(message5);
			$("#testFilePath").focus();
			return false;
		}
		
		if(operateType=="update"){  //修改操作
			var result = -1;
			$.ajax({
				url:'updateLanguagePack.do?ajax=yes',
				type:"post",
				data:{"testPackLanguagePack.testPackCode":testPackVoCode,"testPackLanguagePack.code":languageTestCode,"testPackLanguagePack.testFilePath":testFilePath,"testPackLanguagePack.downloadPath":downloadPath,"testPackLanguagePack.languageTypeCode":languageCode,"testPackLanguagePack.memo":memo},
				dataType:'JSON',
				async: false,
				success :function(data){
					var oWin = $("#win");
					var oLay = $("#overlay");
					oLay.hide();
					oWin.hide();
					window.location.href=window.location.href;
				},
				error :function(data){
					result = -2;
				}
			});
		}else{
			var languageStr="";
			$("#testPackLanguagePackTable tr").each(function(i){
				if($(this).attr("id")=="languageClass"){
					languageStr+=$(this).attr("languageCode")+",";
				 
				}
			});
			
			if(languageStr.indexOf(languageCode)!=-1){
				alert("当前语言已经存在测试版本");
				return false;
			}
			
			
			$.ajax({
				url:'insertLanguagePack.do?ajax=yes',
				type:"post",
				data:{"testPackLanguagePack.testPackCode":testPackVoCode,"testPackLanguagePack.code":languageTestCode,"testPackLanguagePack.testFilePath":testFilePath,"testPackLanguagePack.downloadPath":downloadPath,"testPackLanguagePack.languageTypeCode":languageCode,"testPackLanguagePack.memo":memo},
				dataType:'JSON',
				async: false,
				success :function(data){
					var oWin = $("#win");
					var oLay = $("#overlay");
					oLay.hide();
					oWin.hide();
					window.location.href=window.location.href;
				},
				error :function(data){
					result = -2;
				}
			});
		}
		
		
	 
	});
	
	
	
	
	/**
	 * 修改数据提交
	 */
	$("#sumbitUpdateBtn").click(function(){
		
		if(!checkData(document.getElementsByName("testPackAdd.versionName")[0].value,"string",true))
		{
			alert(message6);
			document.getElementsByName("testPackAdd.versionName")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("testPackVo.proSerialNo")[0].value,"string",true))
		{
			alert(message7);
			document.getElementsByName("testPackVo.proSerialNo")[0].focus();
			return false;
		}
		else if(!checkProSerialNo())
		{
			alert(message8);
			document.getElementsByName("testPackVo.proSerialNo")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("testPackAdd.softwareTypeCode")[0].value,"string",true))
		{
			alert(message9);
			document.getElementsByName("testPackAdd.softwareTypeCode")[0].focus();
			return false;
		}
		
		if(!checkData(document.getElementsByName("testPackAdd.downloadPath")[0].value,"string",true))
		{
			alert(message10);
			document.getElementsByName("testPackAdd.downloadPath")[0].focus();
			return false;
		}
		
		var prefix = "testPackLanguagePacks";
		var $form = $("#theForm");
		
		
		$form.submit();

//		var data = $form.serializeArray(); //自动将form表单封装成json
//		insertTestPack(data);
		
	});
	
});




window.onload = function ()
{
	
	/**
	 * 弹出语言包修改窗口
	 */
	var oWin3 = document.getElementById("win3");
	var oLay3 = document.getElementById("overlay3");	
	var oClose3 = document.getElementById("close3");
	if(oWin3 != null)
	{
		var oH2 = oWin.getElementsByTagName("h2")[1];
		var bDrag = false;
		var disX = disY = 0;
		oClose3.onclick = function ()
		{
			oLay3.style.display = "none";
			oWin3.style.display = "none";
			
		};
		oClose3.onmousedown = function (event)
		{
			(event || window.event).cancelBubble = true;
		};
		oH2.onmousedown = function (event)
		{		
			var event = event || window.event;
			bDrag = true;
			disX = event.clientX - oWin.offsetLeft;
			disY = event.clientY - oWin.offsetTop;		
			this.setCapture && this.setCapture();		
			return false;
		};
		document.onmousemove = function (event)
		{	
			if (!bDrag) return;
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
			var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL; 		
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;
			
			oWin3.style.marginTop = oWin.style.marginLeft = 0;
			oWin3.style.left = iL + "px";
			oWin3.style.top = iT + "px";		
			return false;
		};
		document.onmouseup = window.onblur = oH2.onlosecapture = function ()
		{
			bDrag = false;				
			oH2.releaseCapture && oH2.releaseCapture();
		};
	}
	
	
	/**
	 * 弹出语言包添加窗口
	 */
	var oWin = document.getElementById("win");
	var oLay = document.getElementById("overlay");	
	var oClose = document.getElementById("close");
	if(oWin != null)
	{
		var oH2 = oWin.getElementsByTagName("h2")[0];
		var bDrag = false;
		var disX = disY = 0;
		oClose.onclick = function ()
		{
			oLay.style.display = "none";
			oWin.style.display = "none";
			
		};
		oClose.onmousedown = function (event)
		{
			(event || window.event).cancelBubble = true;
		};
		oH2.onmousedown = function (event)
		{		
			var event = event || window.event;
			bDrag = true;
			disX = event.clientX - oWin.offsetLeft;
			disY = event.clientY - oWin.offsetTop;		
			this.setCapture && this.setCapture();		
			return false;
		};
		document.onmousemove = function (event)
		{	
			if (!bDrag) return;
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
			var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL; 		
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;
			
			oWin.style.marginTop = oWin.style.marginLeft = 0;
			oWin.style.left = iL + "px";
			oWin.style.top = iT + "px";		
			return false;
		};
		document.onmouseup = window.onblur = oH2.onlosecapture = function ()
		{
			bDrag = false;				
			oH2.releaseCapture && oH2.releaseCapture();
		};
	}
	
	
	
	

	/**
	 * 弹出测试包详情窗口
	 */	
	var oWin2 = document.getElementById("win_testpackdetail");
	var oLay2 = document.getElementById("overlay");	
	var oClose2 = document.getElementById("close");
	if(oWin2 != null)
	{
		var oH22 = oWin2.getElementsByTagName("h2")[0];
		var bDrag2 = false;
		var disX2 = disY2 = 0;
		oClose2.onclick = function ()
		{
			//oLay2.style.display = "none";
			//oWin2.style.display = "none";
			closeWinTestPackDetail();
			
		};
		oClose2.onmousedown = function (event)
		{
			(event || window.event).cancelBubble = true;
		};
		oH22.onmousedown = function (event)
		{		
			var event = event || window.event;
			bDrag2 = true;
			disX2 = event.clientX - oWin2.offsetLeft;
			disY2 = event.clientY - oWin2.offsetTop;		
			this.setCapture && this.setCapture();		
			return false;
		};
		document.onmousemove = function (event)
		{	
			if (!bDrag2) return;
			var event = event || window.event;
			var iL = event.clientX - disX2;
			var iT = event.clientY - disY2;
			var maxL = document.documentElement.clientWidth - oWin2.offsetWidth;
			var maxT = document.documentElement.clientHeight - oWin2.offsetHeight;		
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL; 		
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;
			
			oWin2.style.marginTop = oWin2.style.marginLeft = 0;
			oWin2.style.left = iL + "px";
			oWin2.style.top = iT + "px";		
			return false;
		};
		document.onmouseup = window.onblur = oH22.onlosecapture = function ()
		{
			bDrag2 = false;				
			oH22.releaseCapture && oH22.releaseCapture();
		};
	}
	
};





function buildTestPackLanguagePackHtml(testPackLanguagePack){
	//移除选择的语言
	$("#win").find("select option:selected").remove();;
	
	var $tr = $("<tr></tr>");
	var $languageTd = $("<td></td>");
	$languageTd.append(testPackLanguagePack.languageName);
	
	var $downloadPathTd = $("<td></td>");
	$downloadPathTd.append(testPackLanguagePack.downloadPath);
	
	var $testFilePathTd = $("<td></td>");
	$testFilePathTd.append(testPackLanguagePack.testFilePath);
	
	var $memoTd = $("<td></td>");
	$memoTd.append(testPackLanguagePack.memo);
	
	var $operTd = $("<td class='caoz'></td>");
	var $a = $("<a href='###' languageCode='"+testPackLanguagePack.languageTypeCode+"' languageName='"+testPackLanguagePack.languageName+"'>"+message12+"</a>");
	
	$a.click(function(){
		var $select = $("#win").find("select");
		var languageCode = $(this).attr("languageCode");
		var languageName = $(this).attr("languageName");
		$select.append("<option value='"+languageCode+"'>"+languageName+"</option>");
		$(this).closest("tr").remove();
		testPackLanguagePackArray.removePack(languageCode);
	});
	
	$operTd.append($a);
	
	$tr.append($languageTd).append($downloadPathTd).append($testFilePathTd).append($memoTd).append($operTd);
	
	//$("#testPackLanguagePackTable").find("tr").last().before($tr);
	$("#testPackLanguagePackTable").find("tr:last").after($tr);
	
	var oWin = $("#win");
	var oLay = $("#overlay");
	
	oLay.hide();
	oWin.hide();
}

Array.prototype.remove = function(language){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(language.languageCode == this[i].languageCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

/**
 * 删除语言包
 * @param testPackLanguagePack
 */
Array.prototype.removePack = function(languageCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(languageCode == this[i].languageTypeCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

/**
 * 语言对象
 * @param languageCode	语言编码
 * @param languageName	语言名称
 */
function Language(languageCode,languageName){
	this.languageCode = languageCode;
	this.languageName = languageName;
}

/**
 * 测试包语言包对象
 * @param languageTypeCode	语言类型
 * @param languageName 语言名称
 * @param downloadPath	语言包相对路径
 * @param testFilePath	可测车型文档路径
 * @param memo	测试包说明
 */
function TestPackLanguagePack(languageTypeCode,languageName,downloadPath,testFilePath,memo){

	this.languageTypeCode = languageTypeCode;
	this.languageName = languageName;
	this.downloadPath = downloadPath;
	this.testFilePath = testFilePath;
	this.memo = memo;
};








//关闭测试包详情窗口
function closeWinTestPackDetail()
{
	$("#win_testpackdetail").hide();
	$("#overlay").hide();
	$(".testpacklanguagepacktr").remove();
}



//转到添加测试包页面
function addTestPack()
{
	$("#f1").attr("action","addtestpack.do").submit();
}


//保存测试包方法
function insertTestPack(data)
{
	var result = -1;
	
	$.ajax({
		url:'inserttestpack.do?ajax=yes',
		type:"post",
		data:data,
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
	{
		alert(message13);
		$("#f1").attr("action","listtestpack.do").submit();
	}
	else
	{
		alert(message14);
	}
}

//删除测试包方法
function delTestLanguagePack(code)
{
	if(confirm(message15)){
		
		$.ajax({
			url:'deleteLanguagePack.do?ajax=yes',
			type:"post",
			data:{"testPackLanguagePack.code":code},
			dataType:'JSON',
			async: false,
			success :function(data){
				window.location.href=window.location.href;
			
			},
			error :function(data){
				
			}
		});
		
	}
}

//删除测试包方法
function delTestPack(code)
{
	if(confirm(message15)){
		var result = -1;
		
		$.ajax({
			url:'deltestpack.do?ajax=yes',
			type:"post",
			data:{"code":code},
			dataType:'JSON',
			async: false,
			success :function(data){
				var resultData = eval(eval(data));
				result = resultData[0];
			},
			error :function(data){
				result = -2;
			}
		});
		
		if(result == 0)
		{
			alert(message16);
			$("#f1").attr("action","listtestpack.do").submit();
		}
		else
		{
			alert("测试包删除失败");
		}
	}
}




//验证产品序列号有效性
function checkProSerialNo()
{
	var serialNo = $(".proserialno").val();
	var result ="";
	$.ajax({
		url:'checkproserialno.do?ajax=yes',
		type:"post",
		data:{"productInfo.serialNo":serialNo},
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			for(var i=0 ;i<resultData.length;i++){
				result+=resultData[i]+",";
			}
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == ""){
		$(".pronovalidity").html("");
		return true;
	}
	else{
		$(".pronovalidity").html(message8+" ["+result+"]");
	    return false;
	}
}




//按产品序列号过滤功能软件
function selSoftByproNo()
{
	var serialNo = $(".proserialno").val();
	var resultData;
	var code;
	var name;
	
	$.ajax({
		url:'getsoftbyprono.do?ajax=yes',
		type:"post",
		data:{"productInfo.serialNo":serialNo},
		dataType:'JSON',
		async: false,
		success :function(data){
			resultData = eval(eval(data));
		},
		error :function(data){
			alert(message17);
		}
	});
	
	$(".softtypesel").empty();
	$(".softtypesel").append("<option value=''>--"+message18+"--</option>");
	
	$.each(resultData[0].softwareTypes,function(i,n){
		code = resultData[0].softwareTypes[i].code;
		name = resultData[0].softwareTypes[i].name;
		$(".softtypesel").append("<option value='"+code+"'>"+name+"</option>");
	});
}


//显示测试包详情
function showTestPack(code)
{
	var resultData;
	
	$.ajax({
		url:'showtestpack.do?ajax=yes',
		type:"post",
		data:{"code":code},
		dataType:'JSON',
		async: false,
		success :function(data){
			resultData = eval(data);
		},
		error :function(data){
			alert(message17);
		}
	});

	$(".testpackvo").find(".testpackname").text(resultData[0].testPackVo.versionName);
	$(".testpackvo").find(".testpackprono").text(resultData[0].testPackVo.proCode);
	$(".testpackvo").find(".testpacktypename").text(resultData[0].testPackVo.softwareName);
	$(".testpackvo").find(".testpackpath").text(resultData[0].testPackVo.downloadPath);
		
	$.each(resultData[0].testPackLanguagePacks,function(i,n){
		//alert(resultData[0].testPackLanguagePacks[i].code);

		var $tr = $("<tr class='testpacklanguagepacktr'></tr>");
		var $languagetd = $("<td>&nbsp;"+resultData[0].testPackLanguagePacks[i].languageTypeName+"</td>");
		var $languagepathtd = $("<td>&nbsp;"+resultData[0].testPackLanguagePacks[i].downloadPath+"</td>");
		var $languagetestFiletd = $("<td>&nbsp;"+resultData[0].testPackLanguagePacks[i].testFilePath+"</td>");
		var $languagememotd = $("<td>&nbsp;"+resultData[0].testPackLanguagePacks[i].memo+"</td>");
		$tr.append($languagetd).append($languagepathtd).append($languagetestFiletd);
		$tr.append($languagememotd);
		$("#testPackLanguagePackTable").append($tr);
		
	});

	$("#win_testpackdetail").show();
	$("#overlay").show();
}

