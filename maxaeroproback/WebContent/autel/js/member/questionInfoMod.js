
/**
 * 安全问题名称管理JS
 */
//添加的数据
var addArray = new Array();
//删除的数据
var delArray = new Array();

var tableId = "#questionDescTable";

var form = "#questionDescForm";

var questionDesc1 = "questionDescs";

var questionDesc2 = "delQuestionDescs";

$(document).ready(function(){

	$("#finsh").click(function(){
		var questionCode = $("#questionCode").val();
		//选择的option
		var $selectOption = $("#language option:selected");
		//languageCode
		var value= $selectOption.val();
		//languageName
		var text = $selectOption.text();
		var questionDesc = $("#questionDesc").val();
		if(value==null || questionDesc=="")
		{
			alert(message5);
			return false;
		}
		
		var questionDesc = new QuestionDesc(text,value,questionCode,questionDesc);
		
		addArray.push(questionDesc);
		
		var oWin = $("#win");
		var oLay = $("#overlay");
		oWin.hide();
		oLay.hide();
		buildHtml(questionDesc,tableId);
		$selectOption.remove();
	});
	//表单提交
	$("#sumbitBtn").click(function(){
		var aLen = addArray.length;
		var dLen = delArray.length;
		
		//if(aLen == 0 && dLen == 0){
			//alert(message3);
			//return false;
		//}else{
			var $form = $(form);
			var inputHiddenHtml = "";
			for ( var i = 0; i < aLen; i++) {
				var v = addArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+questionDesc1+"["+i+"].questionCode' value='"+v.questionCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+questionDesc1+"["+i+"].languageCode' value='"+v.languageCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+questionDesc1+"["+i+"].questionDesc' value='"+v.questionDesc+"'/>";
			}
			for ( var i = 0; i < dLen; i++) {
				var v = delArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+questionDesc2+"["+i+"].id' value='"+v+"'/>";
			}
			$form.append(inputHiddenHtml);
			$form.submit();
		//}
	});
});

/**
 * 
 * @param questionDesc	安全问题名称
 */
function buildHtml(questionDesc,tableId){
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+questionDesc.languageName+"</td>");
	var $td2 = $("<td>"+questionDesc.questionDesc+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='###' languageName='"+questionDesc.languageName+"' languageCode='"+questionDesc.languageCode+"'>移除</a>");
	
	$a.click(function(){
		var languageCode = $(this).attr("languageCode");
		addArray.remove(languageCode);
		addOption(this);
		$(this).closest("tr").remove();
	});
	$td4.append($a);
	
	$tr.append($td1).append($td2).append($td4);
	$(tableId).append($tr);
}

/**
 * 删除安全问题名称
 * @param event
 */
function delQuestionDesc(event){
	if(confirm(message4)){
		var $this = $(event);
		//安全问题名称id
		var questionDescId = $this.attr("questionDescId");
		delArray.push(questionDescId);
		addOption(event);
		$this.closest("tr").remove();
	}
}

/**
 * 添加下拉菜单语言选项
 * @param event
 */
function addOption(event){
	var $this = $(event);
	var languageCode = $this.attr("languageCode");
	var languageName = $this.attr("languageName");
	var $option = $("<option value='"+languageCode+"'>"+languageName+"</option>");
	$("#language").append($option);
}

/**
 * 
 * @param languageName	语言名称
 * @param languageCode	语言编码
 * @param questionCode	安全问题编码
 * @param questionDesc	安全问题名称
 */
function QuestionDesc(languageName,languageCode,questionCode,questionDesc){
	this.languageName = languageName;
	this.languageCode = languageCode;
	this.questionCode = questionCode;
	this.questionDesc = questionDesc;
}

Array.prototype.remove = function(languageCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(languageCode == this[i].languageCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};