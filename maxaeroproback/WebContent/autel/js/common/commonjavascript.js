﻿
/**
 * 验证值是否合法
 * @param {} val  被验证的值
 * @param {} dataType  值的类型
 * @param {} isrequired  true:非空  false:可为空
 * @return {Boolean}
 */
function checkData(val,dataType,isrequired)
{
	var regDate = /^\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2]\d|3[0-1])$/;
	var regTime = /^([0-1]\d|2[0-3]):[0-5]\d:[0-5]\d$/;
	var regEmail = /^([a-z0-9+_]|\-|\.|\-)+@([\w|\-]+\.)+[a-z]{2,4}$/i;
	var regTel = /^([0]\d{2,3}-)?(\d{7,11})(-(\d{2,4}))?$/;
	var regMobile = /^[\d|-|\+]{11}$/;

	val = $.trim(val);

	if(!isrequired && val == "") return true;

	if(isrequired && val == "") return false;
	
	if(dataType == "") return true;

	if(dataType == "string") return true;

	if(dataType == "number") return parseInt(val) == val;

	if(dataType == "int") return parseInt(val) == val;

	if(dataType == "float") return parseFloat(val) == val;

	if(dataType == "date") return regDate.test(val);

	if(dataType == "time") return regTime.test(val);

	if(dataType == "email") return regEmail.test(val);

	if(dataType == "tel") return regTel.test(val);

	if(dataType == "mobile") return regMobile.test(val);

	if(dataType == "excelfilename") return getFiletype(val) == "XLS" ||getFiletype(val) == "XLSX";

	if(dataType == "txtfilename") return getFiletype(val) == "TXT";

	if(dataType == "wordfilename") return getFiletype(val) == "DOC" ||getFiletype(val) == "DOCX";
}


/**
 * 根据文件名或路径获取文件类型
 * 
 */
function getFiletype(filepath)
{
	var extStart  = filepath.lastIndexOf(".")+1;
	return filepath.substring(extStart,filepath.length).toUpperCase();
}

