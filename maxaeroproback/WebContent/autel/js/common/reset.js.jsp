<%@ include file="/autel/js/common/fmt.js.jsp"%>
/**
 * 重置按钮js
 */
$(document).ready(function(){

		var $resetBtn = $("<input type='button' value='<fmt:message key='common.btn.reset' />' class='search_but' />");
		var html="&nbsp;<input type='button' id='resetBtn' value='<fmt:message key='common.btn.reset' />' class='search_but' />";
		$(".sear_table,.sear_table1").find("input[type='submit']").after(html);
//		$(".sear_table,.sear_table1").find("input[type='submit']").closest("td").attr("width","100");
		$("#resetBtn").click(function(){
			//清空文本框
			$(".sear_table,.sear_table1").find("input[type='text']").val("");
			//重置下拉框
			$(".sear_table,.sear_table1").find("select").each(
				function(i,item){
						$(this).find("option").eq(0).attr('selected', 'selected');
				}
				);
		});
});