<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 销售契约JS
 * @author sh
 */
$(document).ready(function() {
	$("#submitBtn").click(function() {
		var $from = $("#saleContractForm");
		//销售契约名称
		var name = $("#name").val();
		if(name == ""){
			alert("<fmt:message key='marketman.js.salecontractman.name' />");
			return false;
		}
		
		var sealerTypeCode = $("#sealerTypeCode option:selected").val();
		if(sealerTypeCode == ""){
			alert("<fmt:message key='marketman.js.salecontractman.emptySupplierType' />");
			return false;
		}
		
		var sealerCode = $("#sealerCode").val();
		if(sealerCode == ""){
			alert("<fmt:message key='marketman.js.salecontractman.emptySupplier' />");
			return false;
		}
		
		var proTypeCode = $("#proTypeCode").val();
		if(proTypeCode == ""){
		
			alert("<fmt:message key='market.mainsale.message1' />");
			return false;
		}
		
		var saleCfgCode = $("#saleCfgCode").val();
		if(saleCfgCode == ""){
			alert("<fmt:message key='marketman.js.salecontractman.emptystandardsaleconfig' />");
			return false;
		}
		
		var maxSaleCfgCode = $("#maxSaleCfgCode").val();
		if(maxSaleCfgCode == ""){
			alert("<fmt:message key='marketman.js.salecontractman.emptymaxsaleconfig' />");
			return false;
		}
		
		var languageCfgCode = $("#languageCfgCode option:selected").val();
		if(languageCfgCode == ""){
			alert("<fmt:message key='marketman.js.salecontractman.emptyLanguageConfig' />");
			return false;
		}
		
		var areaCfgCode = $("#areaCfgCode option:selected").val();
		if(areaCfgCode == ""){
			alert("<fmt:message key='marketman.js.salecontractman.emptyAreaConfig' />");
			return false;
		}
		
		var reChargePrice = $("#reChargePrice").val();
		
		var patrn = /^\d+(\.\d{1,1})?$/;
		if (!patrn.exec(reChargePrice) ){
			alert("<fmt:message key='marketman.js.emptyPriceNumPrice' />");
			return false ;
		}
		
		//if(reChargePrice == "" || isNaN(reChargePrice)){
			//alert("<fmt:message key='marketman.js.salecontractman.noNum' />");
			//return false;
		//}
		
		//免费升级月
		var upMonth = $("#upMonth").val();
		
		var patrnMonth = /^[1-9]\d*$/
		
		if (!patrnMonth.exec(upMonth)){
			alert("<fmt:message key='marketman.js.salecontractman.message6' />");
			return false ;
		}
		
		//硬件保修期
		var warrantyMonth = $("#warrantyMonth").val();
		if (!patrnMonth.exec(warrantyMonth)){
			alert("<fmt:message key='marketman.js.salecontractman.message10' />");
			return false ;
		}
		
		
		var saleAreaCfgCode = $("#saleCfgCode").attr("areaCfgCode");
		
		var maxSaleAreaCfgCode = $("#maxSaleCfgCode").attr("areaCfgCode");
		
		<%-- if(!contains(saleAreaCfgCode,maxSaleAreaCfgCode)){
		
			alert("<fmt:message key='marketman.js.salecontractman.message1' />");
			return false;
		}
		
		if(!contains(areaCfgCode,maxSaleAreaCfgCode)){
			alert("<fmt:message key='marketman.js.salecontractman.message2' />");
			return false;
		} --%>
		
		var contractDate = $("#contractDate").val();
		
		if(contractDate == ""){
			alert("<fmt:message key='marketman.js.salecontractman.message3' />");
			return false;
		}
		
		
		var expirationTime = $("#expirationTime").val();
		
		if(expirationTime == ""){
			alert("<fmt:message key='marketman.js.salecontractman.message4' />");
			return false;
		}
		
		if(contractDate >= expirationTime){
			alert("<fmt:message key='marketman.js.salecontractman.message5' />");
			return false;
		}
		
		$from.submit();
	});
});

/**
 * 比较是否相等 
 * @param arg1
 * @param arg2
 */
function contains(arg1,arg2){
	
	if(arg1 == arg2){
		return true;
	}else{
		var arr1 = arg1.split(",");
		var result1 = arr1.sort().join(",");
		
		var arr2 = arg2.split(",");
		var result2 = arr2.sort().join(",");
		
		if(result2.indexOf(result1) != -1){
			return true;
		}else{
			return false;
		}
	}
	
}