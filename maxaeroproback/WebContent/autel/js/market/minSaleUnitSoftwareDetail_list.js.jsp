<%@ include file="/autel/js/common/fmt.js.jsp"%>
/**
 * 最小销售单位软件管理JS
 */
//添加的数据
var addArray = new Array();
//删除的数据
var delArray = new Array();

var tableId = "#detailTable";

var form = "#detailForm";

var memo1 = "details";

var memo2 = "delDetails";

$(document).ready(function(){

	$("#finsh").click(function(){
		var unitCode = $("#unitCode").val();
		//选择的option
		var $selectOption = $("#softwareType option:selected");
		var size = $selectOption.size();
		if(size ==0){
			alert("<fmt:message key='marketman.js.xzSoftwaretype' />");
			return false;
		}
		$selectOption.each(function(){
			//softwareTypeCode
			var value= $(this).val();
			//softwareTypeName
			var text = $(this).text();
			
			var softwaredetail = new Softwaredetail(text,value,unitCode);
			
			addArray.push(softwaredetail);
			var $tr = buildHtml(softwaredetail,tableId);
			$(tableId).append($tr);
		});
		
		var oWin = $("#win");
		var oLay = $("#overlay");
		oWin.hide();
		oLay.hide();
		$selectOption.remove();
	});
	//表单提交
	$("#sumbitBtn").click(function(){
		var aLen = addArray.length;
		var dLen = delArray.length;
		
		if(aLen == 0 && dLen == 0){
			alert("<fmt:message key='marketman.js.chooseSaleAddOrDel' />");
			return false;
		}else{
			var $form = $(form);
			var inputHiddenHtml = "";
			for ( var i = 0; i < aLen; i++) {
				var v = addArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].minSaleUnitCode' value='"+v.unitCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].softwareTypeCode' value='"+v.softwareTypeCode+"'/>";
			}
			for ( var i = 0; i < dLen; i++) {
				var v = delArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo2+"["+i+"].id' value='"+v+"'/>";
			}
			$form.append(inputHiddenHtml);
			$form.submit();
		}
	});
});

/**
 * 
 * @param softwaredetail	功能软件
 */
function buildHtml(softwaredetail,tableId){
	var $tr = $("<tr></tr>");
	var $td0 = $("<td>"+softwaredetail.softwareTypeCode+"</td>");
	var $td1 = $("<td>"+softwaredetail.softwareTypeName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='###' softwareTypeName='"+softwaredetail.softwareTypeName+"' softwareTypeCode='"+softwaredetail.softwareTypeCode+"'><fmt:message key='marketman.saleconfigman.moveout' /></a>");
	
	$a.click(function(){
		var softwareTypeCode = $(this).attr("softwareTypeCode");
		addArray.remove(softwareTypeCode);
		addOption(this);
		$(this).closest("tr").remove();
	});
	$td4.append($a);
	
	$tr.append($td0).append($td1).append($td4);
	return $tr;
	//$(tableId).append($tr);
}

/**
 * 删除最小销售单位说明
 * @param event
 */
function delDetail(event){
	if(confirm("<fmt:message key='marketman.js.confirmYc' />")){
		var $this = $(event);
		//最小销售单位功能软件详细id
		var detailId = $this.attr("detailId");
		delArray.push(detailId);
		addOption(event);
		$this.closest("tr").remove();
	}
}

/**
 * 添加下拉菜单语言选项
 * @param event
 */
function addOption(event){
	var $this = $(event);
	var softwareTypeCode = $this.attr("softwareTypeCode");
	var softwareTypeName = $this.attr("softwareTypeName");
	var $option = $("<option value='"+softwareTypeCode+"'>"+softwareTypeName+"</option>");
	$("#softwareType").append($option);
}

/**
 * 
 * @param softwareTypeName	软件名称
 * @param softwareTypeCode	软件编码
 * @param unitCode	最小销售单位编码
 */
function Softwaredetail(softwareTypeName,softwareTypeCode,unitCode){
	this.softwareTypeName = softwareTypeName;
	this.softwareTypeCode = softwareTypeCode;
	this.unitCode = unitCode;
}

Array.prototype.remove = function(softwareTypeCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(softwareTypeCode == this[i].softwareTypeCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};