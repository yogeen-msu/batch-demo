
///////  声明全局变量 Begin  ///////////////////////////////////////////

//选中code字符串控件
var $selobj2;

//复选框
var $checkbs2;

//全选框
var $allckbs2;

//code组成的字符串中每个code值要不要加单引号（true：要加单引号，false：不加单引号）
var isYinhao2 = false;

//复选框数量
var checkbnum2 = 0;

//选中的复选框数量
var checkednum2 = 0;


///////  声明全局变量 End  //////////////////////////////////////////////



$(document).ready(function(){
	
	//初始化变量 Begin /////
	$selobj2 = $("#selectedcodes2");
	$checkbs2 = $("input[name='checkboxcode2']");
	$allckbs2 = $("input[name='allCheckbox2']");
	isYinhao2 = true;
	checkbnum2 = $checkbs2.length;
	//初始化变量 End ////
	
	$checkbs2.click(function(){
		var $this = $(this);
		xuanze2($this);
	});
	
	$allckbs2.click(function(){
		var $this = $(this);
		quanxuan2($this);
	});
});


//复选方法///////////////
function xuanze2($event)
{
	var selcos = $selobj2.attr("value");
	var code = $event.attr("value");
	
	if(isYinhao2) code = "'"+code+"'";
	
	if($event.attr("checked"))
	{
		if(selcos=="") selcos=code;
			else selcos=selcos+","+code;
		checkednum2++;

		if(checkednum2==checkbnum2)
		{
			$allckbs2.each(function(){
				$(this).attr("checked",true);
			});
		}
	}
	else
	{		
		$allckbs2.each(function(){
			$(this).attr("checked",false);
		});
		
		var sarr = selcos.split(",");
		if(sarr!=null&&sarr.length>0){
			for(var i=0;i<sarr.length;i++){
				if(code==sarr[i]){
					if(i==0){
						sarr = sarr.slice(i+1,sarr.length);
					}else{
						sarr = sarr.slice(0,i).concat(sarr.slice(i+i,sarr.length));
					}
					
					checkednum2--;
				}
			}
		}
		
		if(sarr.length>0){
			selcos = sarr.join(",");
		}else{
			selcos="";
		}
	}
	$selobj2.attr("value",selcos);
}

//全选方法////////////
function quanxuan2($event)
{
	var flag=false;
	
	if ($event.attr("checked"))
	{
		flag=true;
		$allckbs2.each(function(){
			$(this).attr("checked",flag);
		});
	}
	else
	{
		flag=false;
		$allckbs2.each(function(){
			$(this).attr("checked",flag);
		});
	}
	
	if ($checkbs2.length>0)
	{
	   for (var i=0;i<$checkbs2.length;i++)
		{
			if(flag)
			{
				if(!$checkbs2.eq(i).attr("checked"))
				{
					$checkbs2.eq(i).attr("checked",flag);
					xuanze2($checkbs2.eq(i));
				}
			}
			else
			{
				if($checkbs2.eq(i).attr("checked"))
				{
					$checkbs2.eq(i).attr("checked",flag);
					xuanze2($checkbs2.eq(i));
				}
			}			 
		}
	}
}
