/**
 * 销售配置管理JS
 */
$(document).ready(function(){
	
	$("a[name^='delSaleConfig']").click(function(){
		if(confirm("确认删除吗?")){
			var $from = $("#saleConfigForm");
			var code = $(this).attr("saleCode");
			$from.attr("action","delSaleConfig.do");
			$from.append("<input type='hidden' name='saleConfig.code' value='"+code+"'/>");
			$from.submit();
		}
	});
});