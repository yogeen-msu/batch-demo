<%@ include file="/autel/js/common/fmt.js.jsp"%>
/**
 * 最小销售单位软件管理JS
 */
//添加的数据
var addArray = new Array();
//删除的数据
var delArray = new Array();

//视图的数据
var viewArray = new Array();

var tableId = "#detailTable";

var form = "#detailForm";

var memo1 = "details";

var memo2 = "delDetails";

$(document).ready(function(){
	//初始化视图的数据
	$("input[name='checkboxcode2']").each(function(){
		var softwareTypeId = $(this).attr("detailId");
		var softwareTypeCode= $(this).val();
		var softwareTypeName = $(this).attr("softname");
		var saveType = $(this).attr("saveType");

		var softwareViewDetail = new SoftwareViewDetail(softwareTypeId,softwareTypeCode,softwareTypeName,saveType);

		viewArray.push(softwareViewDetail);
	});
	
	//表单提交
	$("#sumbitBtn").click(function(){
		var aLen = addArray.length;
		var dLen = delArray.length;
		
		if(aLen == 0 && dLen == 0){
			alert("<fmt:message key='marketman.js.chooseSaleAddOrDel' />");
			return false;
		}else{
			var $form = $(form);
			var inputHiddenHtml = "";
			for ( var i = 0; i < aLen; i++) {
				var v = addArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].minSaleUnitCode' value='"+v.unitCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].softwareTypeCode' value='"+v.softwareTypeCode+"'/>";
			}
			for ( var i = 0; i < dLen; i++) {
				var v = delArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo2+"["+i+"].id' value='"+v+"'/>";
			}
			$form.append(inputHiddenHtml);
			$form.submit();
		}
	});
	
	//删除软件
	$(".dellsofts").click(function(){	
		if(confirm("<fmt:message key='marketman.js.confirmYc' />")){
			$("input[name='checkboxcode2']:checked").each(function(){
				delDetailNew($(this));
			});
			
			//复选框对象重置
			resetCheck2();
		}
	});
});

/**
 * 
 * 	添加功能软件
 */
function clickok(){
	var unitCode = $("#unitCode").val();
	//选择的checkbox
	var $selectCheckbox = $("input[name='checkboxcode']:checked");
	var size = $selectCheckbox.size();
	if(size ==0){
		alert("<fmt:message key='marketman.js.xzSoftwaretype' />");
		return false;
	}
	$selectCheckbox.each(function(){
		//softwareTypeCode
		var value= $(this).val();
		//softwareTypeName
		var text = $(this).attr("softname");
		
		var softwaredetail = new Softwaredetail(text,value,unitCode);
		var softwareViewDetail = new SoftwareViewDetail(0,value,text,0);
		
		addArray.push(softwaredetail);
		viewArray.push(softwareViewDetail);
		
		var $tr = buildHtml(softwaredetail,tableId);
		$(tableId).append($tr);
	});
			
	//复选框对象重置
	resetCheck2();
	
	Cop.Dialog.close("selectSoftwareDlg");
}

/**
 * 
 * 	复选框对象重置
 */
function resetCheck2(){
	$selobj2 = $("#selectedcodes2");
	$checkbs2 = $("input[name='checkboxcode2']");
	$allckbs2 = $("input[name='allCheckbox2']");
	isYinhao2 = true;
	checkbnum2 = $checkbs2.length;
	checkednum2 = $("input[name='checkboxcode2']:checked");
	$selobj2.val("");

	$checkbs2.click(function(){
		var $this = $(this);
		xuanze2($this);
	});
	
	$allckbs2.click(function(){
		var $this = $(this);
		quanxuan2($this);
	});
}

/**
 * 
 * 	按条件查询已选中的软件
 */
function querySoft(){
	var softwareTypeName = $("#softwareTypeName").val();
	
	$(tableId).empty();
	
	$.each(viewArray,function(k,v){
		if(v.softwareTypeName.toLowerCase().indexOf(softwareTypeName.toLowerCase()) != -1)
		{
			if(v.saveType == "1")
			{
				var $tr = buildHtmlOk(v,tableId);
			}
			else
			{
				var $tr = buildHtml(v,tableId);
			}
			
			$(tableId).append($tr);
		}
	});
			
	//复选框对象重置
	resetCheck2();
}

/**
 * 
 * @param softwaredetail	功能软件
 */
function buildHtml(softwaredetail,tableId){
	var $tr = $("<tr></tr>");
	var $td5 = $("<td width='70'><input type='checkbox' name='checkboxcode2' saveType='0' value='"+softwaredetail.softwareTypeCode+"' softname='"+softwaredetail.softwareTypeName+"'/></td>");
	var $td0 = $("<td width='20%'>"+softwaredetail.softwareTypeCode+"</td>");
	var $td1 = $("<td>"+softwaredetail.softwareTypeName+"</td>");
	var $td4 = $("<td class='caoz' width='20%'></td>");
	var $a = $("<a href='###' class='softwaretypeclass' softwareTypeName='"+softwaredetail.softwareTypeName+"' softwareTypeCode='"+softwaredetail.softwareTypeCode+"'><fmt:message key='marketman.saleconfigman.moveout' /></a>");
	
	$a.click(function(){
		var softwareTypeCode = $(this).attr("softwareTypeCode");
		
		addArray.remove(softwareTypeCode);
		viewArray.remove(softwareTypeCode);
		
		$(this).closest("tr").remove();
			
		//复选框对象重置
		resetCheck2();
	});
	$td4.append($a);
	
	$tr.append($td5).append($td0).append($td1).append($td4);
	return $tr;
	//$(tableId).append($tr);
}

/**
 * 已保存的软件
 * @param softwareViewDetail	功能软件
 */
function buildHtmlOk(softwareViewDetail,tableId){
	var $tr = $("<tr></tr>");
	var $td5 = $("<td width='70'><input type='checkbox' name='checkboxcode2' saveType='1' detailId='"+softwareViewDetail.softwareTypeId+"' value='"+softwareViewDetail.softwareTypeCode+"' softname='"+softwareViewDetail.softwareTypeName+"'/></td>");
	var $td0 = $("<td width='20%'>"+softwareViewDetail.softwareTypeCode+"</td>");
	var $td1 = $("<td>"+softwareViewDetail.softwareTypeName+"</td>");
	var $td4 = $("<td class='caoz' width='20%'></td>");
	var $a = $("<a class='softwaretypeclass' detailId='"+softwareViewDetail.softwareTypeId+"' softwareTypeCode='"+softwareViewDetail.softwareTypeCode+"' softwareTypeName='"+softwareViewDetail.softwareTypeName+"' onclick='delDetail(this);' href='###'><fmt:message key='marketman.saleconfigman.moveout' /></a>");
	
	$td4.append($a);
	$tr.append($td5).append($td0).append($td1).append($td4);
	return $tr;
	//$(tableId).append($tr);
}

/**
 * 删除最小销售单位说明
 * @param event
 */
function delDetail(event){
	if(confirm("<fmt:message key='marketman.js.confirmYc' />")){
		var $this = $(event);
		//最小销售单位功能软件详细id
		var detailId = $this.attr("detailId");
		delArray.push(detailId);
		
		var softwareTypeCode = $this.attr("softwareTypeCode");
		viewArray.remove(softwareTypeCode);
		
		$this.closest("tr").remove();
			
		//复选框对象重置
		resetCheck2();
	}
}

/**
 * 删除最小销售单位说明
 * @param event
 */
function delDetailNew(event){
	var $this = $(event);
	
	var saveType = $this.attr("saveType");
	var detailId = $this.attr("detailId");
	var softwareTypeCode = $this.val();
	
	if(saveType == "1")
	{
		delArray.push(detailId);
	}
	else
	{
		addArray.remove(softwareTypeCode);
	}
	
	viewArray.remove(softwareTypeCode);

	$this.closest("tr").remove();
}

/**
 * 
 * @param softwareTypeName	软件名称
 * @param softwareTypeCode	软件编码
 * @param unitCode	最小销售单位编码
 */
function Softwaredetail(softwareTypeName,softwareTypeCode,unitCode){
	this.softwareTypeName = softwareTypeName;
	this.softwareTypeCode = softwareTypeCode;
	this.unitCode = unitCode;
}

/**
 * 软件视图对象
 * @param softwareTypeId	软件ID
 * @param softwareTypeCode	软件编码
 * @param softwareTypeName	软件名称
 * @param saveType      	1:已保存  0:未保存
 */
function SoftwareViewDetail(softwareTypeId,softwareTypeCode,softwareTypeName,saveType){
	this.softwareTypeId = softwareTypeId;
	this.softwareTypeCode = softwareTypeCode;
	this.softwareTypeName = softwareTypeName;
	this.saveType = saveType;
}

Array.prototype.remove = function(softwareTypeCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(softwareTypeCode == this[i].softwareTypeCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};