<%@ include file="/autel/js/common/fmt.js.jsp"%>

//销售配置数组
var saleConfigArray = new Array();
var delSaleConfiArray = new Array();

//销售配置表格
var configTable = "configTable";

//销售配置
var promotionSaleConfigs = "promotionSaleConfigs";

var delPromotionSaleConfigs = "delPromotionSaleConfigs";

$(document).ready(function(){
	
	/**
	 * 销售配置确认事件
	 */
	$("#saleConfigFinishBtn").click(function() {
		
		var size = $("#minSaleConfigSelect option:selected").size();
		
		if(size > 0){
			$("#minSaleConfigSelect option:selected").each(function(){
				var code = $(this).val();
				var name = $(this).text();
				var typeName = $(this).attr("typeName");
				var typeCode = $(this).attr("typeCode");
				
				var saleConfig = new SaleConfig(code,name,typeName,typeCode);
				saleConfigArray.push(saleConfig);
				$("#"+configTable).append(createSaleConfigTr(saleConfig));
				
			});
			closeDiv(this);
			$("#minSaleConfigSelect option:selected").remove();
		}else{
			alert("<fmt:message key='marketman.js.saleconfigman.check' />");
			return false;
		}
		
	});
});

/**
 * 创建销售配置行
 * @param saleConfig	销售配置
 */
function createSaleConfigTr(saleConfig) {
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+saleConfig.code+"</td>");
	var $td2 = $("<td>"+saleConfig.name+"</td>");
	var $td3 = $("<td>"+saleConfig.typeName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='#' typeCode='"+saleConfig.typeCode+"' saleCode='"+saleConfig.code+"' saleName='"+saleConfig.name+"' typeName='"+saleConfig.typeName+"'><fmt:message key='common.js.delInfo' /></a>");

	$a.click(function(){
		var saleCode = $(this).attr("saleCode");
		var saleName = $(this).attr("saleName");
		var typeName = $(this).attr("typeName");
		var typeCode = $(this).attr("typeCode");
		
		$(this).closest("tr").remove();
		saleConfigArray.removeSale(saleCode);
	
		$("#minSaleConfigSelect").append("<option value='"+saleCode+"' typeCode='"+typeCode+"' typeName='"+typeName+"'>"+saleName+"</option>");
	});
	
	$td4.append($a);
	$tr.append($td1).append($td2).append($td3).append($td4);
	return $tr;
}

/**
 * 删除销售配置
 * @param event
 */
function delSaleConfig(event){
	if(confirm("<fmt:message key='common.js.confirmDel' />")){
		var $this = $(event);
		var saleId = $this.attr("saleId");
		var saleCode = $this.attr("saleCode");
		var typeCode = $this.attr("typeCode");
		var typeName = $this.attr("typeName");
		var saleName = $this.attr("saleName");
		$("#minSaleConfigSelect").append("<option value='"+saleCode+"' typeCode='"+typeCode+"' typeName='"+typeName+"'>"+saleName+"</option>");;
		delSaleConfiArray.push(saleId);
		$this.closest("tr").remove();
	}
}

/**
 * 
 * @param code	编码
 * @param name	销售配置名称
 * @param typeName	产品型号名称
 * @returns {MinSaleUnit}	销售配置对象
 */
function SaleConfig(code,name,typeName,typeCode){
	this.code = code;
	this.name = name;
	this.typeName = typeName;
	this.typeCode = typeCode;
}


Array.prototype.removeSale = function(saleCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(saleCode == this[i].code){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};