<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 销售契约JS
 */
$(document).ready(function(){
	
	$("a[name^='delSaleContract']").click(function(){
		if(confirm("<fmt:message key='common.js.confirmDel' />")){
			var $from = $("#saleContractForm");
			var code = $(this).attr("saleCode");
			$from.attr("action","delSaleContract.do");
			$from.append("<input type='hidden' name='saleContract.code' value='"+code+"'/>");
			$from.submit();
		}
	});
});