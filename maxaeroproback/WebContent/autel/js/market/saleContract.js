/**
 * 销售契约JS
 * @author sh
 */
$(document).ready(function() {
	$("#submitBtn").click(function() {
		var $from = $("#saleContractForm");
		//销售契约名称
		var name = $("#name").val();
		
		if(name == ""){
			alert("销售契约名称不能为空");
			return false;
		}
		
		var sealerTypeCode = $("#sealerTypeCode option:selected").val();
		if(sealerTypeCode == ""){
			alert("经销商类型不能为空");
			return false;
		}
		
		var sealerCode = $("#sealerCode option:selected").val();
		if(sealerCode == ""){
			alert("经销商不能为空");
			return false;
		}
		
		var proTypeCode = $("#proTypeCode option:selected").val();
		if(proTypeCode == ""){
			alert("产品型号不能为空");
			return false;
		}
		
		var saleCfgCode = $("#saleCfgCode option:selected").val();
		if(saleCfgCode == ""){
			alert("标准销售配置不能为空");
			return false;
		}
		
		var maxSaleCfgCode = $("#maxSaleCfgCode option:selected").val();
		if(maxSaleCfgCode == ""){
			alert("最大销售配置不能为空");
			return false;
		}
		
		var languageCfgCode = $("#languageCfgCode option:selected").val();
		if(languageCfgCode == ""){
			alert("语言配置不能为空");
			return false;
		}
		
		var areaCfgCode = $("#areaCfgCode option:selected").val();
		if(areaCfgCode == ""){
			alert("区域配置不能为空");
			return false;
		}
		
		var reChargePrice = $("#reChargePrice").val();
		if(reChargePrice == "" || isNaN(reChargePrice)){
			alert("出厂续租价格不能为空且必须为数字");
			return false;
		}
		
		var saleAreaCfgCode = $("#saleCfgCode option:selected").attr("areaCfgCode");
		
		var maxSaleAreaCfgCode = $("#maxSaleCfgCode option:selected").attr("areaCfgCode");
		
		if(!contains(saleAreaCfgCode,maxSaleAreaCfgCode)){
			alert("标准销售配置的区域配置范围必须小于或等于最大销售配置的区域配置范围");
			return false;
		}
		
		if(!contains(areaCfgCode,maxSaleAreaCfgCode)){
			alert(" 区域配置信息不在最大销售配置的区域配置范围");
			return false;
		}
		
		var contractDate = $("#contractDate").val();
		
		if(contractDate == ""){
			alert("契约时间不能为空");
			return false;
		}
		
		var expirationTime = $("#expirationTime").val();
		
		if(expirationTime == ""){
			alert("过期时间不能为空");
			return false;
		}
		
		if(contractDate >= expirationTime){
			alert("过期时间必须大于契约时间");
			return false;
		}
		
		$from.submit();
	});
});

/**
 * 比较是否相等 
 * @param arg1
 * @param arg2
 */
function contains(arg1,arg2){
	
	if(arg1 == arg2){
		return true;
	}else{
		var arr1 = arg1.split(",");
		var result1 = arr1.sort().join(",");
		
		var arr2 = arg2.split(",");
		var result2 = arr2.sort().join(",");
		
		if(result2.indexOf(result1) != -1){
			return true;
		}else{
			return false;
		}
	}
	
}