<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 修改销售配置JS
 */
//添加的数据
var addArray = new Array();

//删除的数据
var delArray = new Array();

var tableId = "#configTable";

var form = "#configForm";

var memo1 = "details";

var memo2 = "delDetails";

var flag = false;

//临时结果集
var arrResult = new Array();

$(document).ready(function(){

	$("#finsh").click(function(){
		//选择的option
		var $selectOption = $("#minSaleUnit option:selected");
		var size = $selectOption.size();
		if(size ==0){
			alert("<fmt:message key='marketman.js.xzMinSaleUnit' />");
			return false;
		}
		
		var bool = true;
		

		$selectOption.each(function(){
			
			var areaCfgCode = $(this).attr("areaCfgCode");
			if(areaCfgCode == ""){
				alert("<fmt:message key='marketman.js.addMinSaleUnitPrice' />");
				bool = false;
				return false;
			}
			
		});
		
		if(bool){
			$selectOption.each(function(){
				var areaCfgCode = $(this).attr("areaCfgCode");
				arrResult.push(areaCfgCode);
			});
		}
		
		var b = true;
		
		if(arrResult.length == 1 && areas == ""){
			$selectOption.each(function(){
				//最小销售单位编码
				var value= $(this).val();
				//最小销售单位名称
				var text = $(this).text();
				
				var areaCfgCode = $(this).attr("areaCfgCode");
				
				//第一次选择最小销售单位时赋值
				if(flag){
					areas = areaCfgCode;
					flag = false;
				}
				
				
				var minSaleUnit = new MinSaleUnit(value,text,areaCfgCode);
				
				addArray.push(minSaleUnit);
				var $tr = buildHtml(minSaleUnit,tableId);
				$(tableId).append($tr);
			});
			b = false;
		}else{
			if(areas == ""){
				areas = arrResult[0];
			}else{
				arrResult.unshift(areas);
			}
			for(var i = 1 ; i < arrResult.length; i ++){
				if(!compare(areas,arrResult[i])){
					alert("<fmt:message key='marketman.js.sameArea' />");
					areas = "";
					bool = false;
					break;
				}
			}
			//清空数组
			if(!bool){
				arrResult.length = 0;
			}
		}
		
		if(b && bool && arrResult.length >= 1){
			$selectOption.each(function(){
				//最小销售单位编码
				var value= $(this).val();
				//最小销售单位名称
				var text = $(this).text();
				var areaCfgCode = $(this).attr("areaCfgCode");
				
				var minSaleUnit = new MinSaleUnit(value,text,areaCfgCode);
				
				addArray.push(minSaleUnit);
				var $tr = buildHtml(minSaleUnit,tableId);
				$(tableId).append($tr);
			});
		}
		
		
		if(bool){
			
			var oWin = $("#win");
			var oLay = $("#overlay");
			arrResult.length = 0;
			oWin.hide();
			oLay.hide();
			$selectOption.remove();
		}

	});
	//表单提交
	$("#sumbitBtn").click(function(){
		var saleConfigName = $("#saleConfigName").val();
		if(saleConfigName == ""){
			alert("销售配置名称不能为空");
			return false;
		}
		var saleConfigPicPath = $("#saleConfigPicPath").val();
		if(saleConfigPicPath == ""){
		
			alert("<fmt:message key='common.js.nonPic' />");
			return false;
		}
	
		var patrn = /^.+(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/;
		if (!patrn.exec(saleConfigPicPath)){
			alert("<fmt:message key='common.js.formatError' />");
			return false ;
		}
		
		var saleConfigProduct = $("#saleConfigProduct option:selected").val();
		if(saleConfigProduct == ""){
			alert("请选择产品型号");
			return false;
		}
		
		$("#configTable").find("#minSaleTR").each(function(i){
			var minCode=$(this).attr("minCode");
			var minName=$(this).attr("minName");
			var softwaredetail = new Softwaredetail(minName,minCode);
			addArray.push(softwaredetail);
		});
		
		var aLen = addArray.length;
		
		if(aLen == 0){
			alert("<fmt:message key='marketman.js.xzAddMinsaleunit' />");
			return false;
		}else{
			var $form = $(form);
			var inputHiddenHtml = "";
			for ( var i = 0; i < aLen; i++) {
				var v = addArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].minSaleUnitCode' value='"+v.minSaleCode+"'/>";
			}
			
			$form.append(inputHiddenHtml);
			$form.submit();
		}
	});
});

/**
 * 
 * @param minSaleUnit	最小销售单位
 */
function buildHtml(minSaleUnit,tableId){
	var $tr = $("<tr></tr>");
	var $td0 = $("<td>"+minSaleUnit.saleCode+"</td>");
	var $td1 = $("<td>"+minSaleUnit.saleName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='###' areaCfgCode='"+minSaleUnit.areaCfgCode+"' saleCode='"+minSaleUnit.saleCode+"' saleName='"+minSaleUnit.saleName+"'><fmt:message key='marketman.saleconfigman.moveout' /></a>");
	
	$a.click(function(){
		var saleCode = $(this).attr("saleCode");
		addArray.remove(saleCode);
		addOption(this);
		var trSize = $(this).closest("table").find("tr").size();
		if(trSize == 2){
			areas = "";
			flag = true;
		}
		$(this).closest("tr").remove();
	});
	$td4.append($a);
	
	$tr.append($td0).append($td1).append($td4);
	return $tr;
	//$(tableId).append($tr);
}

/**
 * 添加下拉最小销售单位选项
 * @param event
 */
function addOption(event){
	var $this = $(event);
	var saleCode = $this.attr("saleCode");
	var saleName = $this.attr("saleName");
	var areaCfgCode = $this.attr("areaCfgCode");
	var $option = $("<option value='"+saleCode+"' areaCfgCode='"+areaCfgCode+"'>"+saleName+"</option>");
	$("#minSaleUnit").append($option);
}

/**
 * 
 * @param saleCode	最小销售单位编码
 * @param saleName	最小销售单位名称
 * @param areaCfgCode 区域配置信息
 */
function MinSaleUnit(saleCode,saleName,areaCfgCode){
	this.saleCode = saleCode;
	this.saleName = saleName;
	this.areaCfgCode = areaCfgCode;
}

Array.prototype.remove = function(saleCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(saleCode == this[i].saleCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

/**
 * 删除最小销售单位
 * @param event
 */
function delUnit(event){
	if(confirm("<fmt:message key='marketman.js.confirmYc' />")){
		var $this = $(event);
		//最小销售单位功能软件详细id
		var detailId = $this.attr("detailId");
		delArray.push(detailId);
		addOption(event);
		var trSize = $this.closest("table").find("tr").size();
		if(trSize == 2){
			areas = "";
			flag = true;
		}
		$this.closest("tr").remove();
	}
}

/**
 * 比较是否相等 
 * @param arg1
 * @param arg2
 */
function compare(arg1,arg2){
	var result = new Array();
	var temp = "";
	if(arg2.length < arg1.length){
		temp = arg1;
		arg1 = arg2;
		arg2 = temp;
	}
	if(arg1 == arg2){
		return true;
	}else{
		var arr1 = arg1.split(",");
		var bool = false;
		var len = arr1.length;
		
		for(var i=0; i < len ; i++){
			if(arg2.indexOf(arr1[i]) != -1){
				result.push(arr1[i]);
				bool = true;
			}
		}
		areas = result.join(",");
		return bool;
	}
	
}
