<%@ include file="/autel/js/common/fmt.js.jsp"%>
document.onkeypress = function(event) {
	if (event.keyCode == 13) {
		return false;
	}
};

var Software={
	init:function(){
		var self = this;
			
		$("#selectSoftDlg").click(function(){
			clearAllDivContent();
			self.openSoftwareSelectDlg();
		});
		
		Cop.Dialog.init({id:"selectSoftwareDlg",title:'<fmt:message key="saleDialog.choose.software" />',height:"400px",width:"800px",scroll:"auto"});
	},
	openSoftwareSelectDlg:function(){
		var self  = this;
		
		$("#selectSoftwareDlg").load("toSearchSoftDlgNew.do?ajax=yes",function(){
			self.createTree();
			
			$("ul.checktree").checkTree();
		});
		
		Cop.Dialog.open("selectSoftwareDlg");
	},
	/**
	创建菜单树
	*/
	createTree:function(){
		var self = this;
		
		$.each(treeJson[0],function(k,v){
			var li = self.createNode(v);
			var ul = self.createChildren(v.children);
			li.append(ul);
			$("#productsoftwareconfigbox>.checktree").append(li);
		});
	},
	/**
	根据子创建ul结点
	*/
	createChildren:function(children){
		var ul=$("<ul></ul>");
		var self = this;
		$.each(children,function(k,v){
			var li = self.createNode(v);
		
			//如果有子则递归 
			var children2 =v.children;
			if(children2 && children2.length>0){
				li.append(self.createChildren(children2));
			}	
			ul.append(li);
		});
		return ul;
	},
	/**
	根据productSoftwareConfig json创建菜单节点
	*/
	createNode:function(v){
		var li = $("<li><input type=\"checkbox\" name=\"productsoftwareconfigcode\" value=\""+v.code+"\"><label>"+v.name+"</label></li>");
		return li;
	}
	
};

function pageQuery(){
	
	//处理选中的虚拟目录
	var $selprosoftconfigcode = $("input[name='productsoftwareconfigcode']:checked");
	var size = $selprosoftconfigcode.size();
	if(size ==0){
		alert("<fmt:message key='product.virtualdir.noselected' />");
		return false;
	}
	
	var selprosoftconfigcodes = "";
	$selprosoftconfigcode.each(function(){
		if(selprosoftconfigcodes == "")
		{
			selprosoftconfigcodes = "'"+$(this).val()+"'";
		}
		else
		{
			selprosoftconfigcodes = selprosoftconfigcodes + ",'"+$(this).val()+"'";
		}
	});
	
	$('#selprosoftconfigcodes').val(selprosoftconfigcodes);
	
	
	//已有的软件
	var excludecodes = "";
	
	/**
	$(".softwaretypeclass").each(function(){
		if(excludecodes==""){
			excludecodes="'"+$(this).attr("softwareTypeCode")+"'";
		}else{
			excludecodes=excludecodes+",'"+$(this).attr("softwareTypeCode")+"'";
		}
	});
	*/
	
	$.each(viewArray,function(k,v){
		if(excludecodes==""){
			excludecodes="'"+v.softwareTypeCode+"'";
		}else{
			excludecodes=excludecodes+",'"+v.softwareTypeCode+"'";
		}
	});
	
	$('#excludecodes').val(excludecodes);
			
	//执行查询
	$.ajax({
		url:"toSearchSoftDlgNew.do",
		type:"post",
		data: $('#pageSelectForm').serialize(),
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			
			//对象重载
			$selobj = $("#selectedcodes");
			$checkbs = $("input[name='checkboxcode']");
			$allckbs = $("input[name='allCheckbox']");
			isYinhao = true;
			checkbnum = $checkbs.length;
			checkednum = 0;
			$selobj.val("");
	
			$checkbs.click(function(){
				var $this = $(this);
				xuanze($this);
			});
			
			$allckbs.click(function(){
				var $this = $(this);
				quanxuan($this);
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};


function clearAllDivContent()
{
	$("#selectSoftwareDlg").html("");
};

