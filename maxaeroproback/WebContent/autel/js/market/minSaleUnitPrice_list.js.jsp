<%@ include file="/autel/js/common/fmt.js.jsp"%>
/**
 * 最小销售单位价格管理JS
 */
//添加的数据
var addArray = new Array();
//删除的数据
var delArray = new Array();

var tableId = "#priceTable";

var form = "#priceForm";

var memo1 = "prices";

var memo2 = "delPrices";

document.onkeypress = function(event) {
	if (event.keyCode == 13) {
		return false;
	}
};

$(document).ready(function(){

	$("#finsh").click(function(){
		var unitCode = $("#unitCode").val();
		//选择的option
		var $selectOption = $("#areaConfig option:selected");
		var size = $selectOption.size();
		if(size ==0){
			alert("<fmt:message key='marketman.js.chooseArea' />");
			return false;
		}
		var price = $("#price").val();
		
		var patrn = /^\d+(\.\d{1,1})?$/;
		if (!patrn.exec(price) || parseFloat(price) == 0){
			alert("<fmt:message key='marketman.js.emptyPriceNumPrice' />");
			return false ;
		}
		
		//if(price == "" ){
			//alert("<fmt:message key='marketman.js.emptyPrice' />");
			//return false;
		//}
		//if(isNaN(price)){
			//alert("<fmt:message key='marketman.js.numPrice' />");
			//return false;
		//}
		
		$selectOption.each(function(){
			var value= $(this).val();
			var text = $(this).text();
			
			var areaInfo = new AreaInfo(text,value,unitCode,price);
			
			addArray.push(areaInfo);
			var $tr = buildHtml(areaInfo,tableId);
			$(tableId).append($tr);
		});
		
		var oWin = $("#win");
		var oLay = $("#overlay");
		oWin.hide();
		oLay.hide();
		$selectOption.remove();
	});
	//表单提交
	$("#sumbitBtn").click(function(){
		var aLen = addArray.length;
		var dLen = delArray.length;
		
		if(aLen == 0 && dLen == 0){
			alert("<fmt:message key='marketman.js.chooseSaleAddOrDel' />");
			return false;
		}else{
			var $form = $(form);
			var inputHiddenHtml = "";
			for ( var i = 0; i < aLen; i++) {
				var v = addArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].minSaleUnitCode' value='"+v.unitCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].areaCfgCode' value='"+v.areaCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].price' value='"+v.price+"'/>";
			}
			for ( var i = 0; i < dLen; i++) {
				var v = delArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo2+"["+i+"].id' value='"+v+"'/>";
			}
			$form.append(inputHiddenHtml);
			$form.submit();
		}
	});
});

/**
 * 
 * @param minSaleUnitMemo	最小销售单位说明
 */
function buildHtml(areaInfo,tableId){
	var $tr = $("<tr></tr>");
	var $td0 = $("<td>"+areaInfo.areaName+"</td>");
	var $td1 = $("<td>"+areaInfo.price+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='###' areaName='"+areaInfo.areaName+"' areaCode='"+areaInfo.areaCode+"'><fmt:message key='marketman.saleconfigman.moveout' /></a>");
	
	$a.click(function(){
		var areaCode = $(this).attr("areaCode");
		addArray.remove(areaCode);
		addOption(this);
		$(this).closest("tr").remove();
	});
	$td4.append($a);
	
	$tr.append($td0).append($td1).append($td4);
	return $tr;
}

/**
 * 删除最小销售单位价格
 * @param event
 */
function delPrice(event){
	if(confirm("<fmt:message key='marketman.js.confirmYc' />")){
		var $this = $(event);
		//最小销售单位价格id
		var priceId = $this.attr("priceId");
		delArray.push(priceId);
		addOption(event);
		$this.closest("tr").remove();
	}
}

/**
 * 添加下拉菜单区域选项
 * @param event
 */
function addOption(event){
	var $this = $(event);
	var areaCode = $this.attr("areaCode");
	var areaName = $this.attr("areaName");
	var $option = $("<option value='"+areaCode+"'>"+areaName+"</option>");
	$("#areaConfig").append($option);
}

/**
 * 
 * @param areaName	区域配置名称
 * @param areaCode	区域配置编码
 * @param memo	最小销售单位说明
 * @param price 区域配置价格
 */
function AreaInfo(areaName,areaCode,unitCode,price){
	this.areaName = areaName;
	this.areaCode = areaCode;
	this.unitCode = unitCode;
	this.price = price;
}

Array.prototype.remove = function(areaCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(areaCode == this[i].areaCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};