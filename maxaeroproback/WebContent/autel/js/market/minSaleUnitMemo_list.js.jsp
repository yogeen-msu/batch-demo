<%@ include file="/autel/js/common/fmt.js.jsp"%>
/**
 * 最小销售单位说明管理JS
 */
//添加的数据
var addArray = new Array();
//删除的数据
var delArray = new Array();

var tableId = "#memoTable";

var form = "#unitMemoForm";

var memo1 = "minSaleUnitMemos";

var memo2 = "delMinSaleUnitMemos";

$(document).ready(function(){

	$("#finsh").click(function(){
		var unitCode = $("#unitCode").val();
		//选择的option
		var $selectOption = $("#language option:selected");
		//languageCode
		var value= $selectOption.val();
		//languageName
		var text = $selectOption.text();
		if(text == ""){
			alert("<fmt:message key='accountinformation.selectlanguage.name' />");
			return  ;
		}
		
		var unitName = $("#unitName").val();
		if(unitName == ""){
			alert("<fmt:message key='marketman.minusalenit.isEmpty' />");
			return  ;
		}
		
		var memo = $("#memo").val();
		if(memo == ""){
			alert("<fmt:message key='marketman.minusalenit.isMemoEmpty' />");
			return  ;
		}
		
		var memoDetail = $("#memoDetail").val();
		if(memoDetail == ""){
			alert("<fmt:message key='marketman.minusalenit.isMemoDetailEmpty' />");
			return  ;
		}
		
		var minSaleUnitMemo = new MinSaleUnitMemo(text,value,unitCode,unitName,memo,memoDetail);
		
		addArray.push(minSaleUnitMemo);
		
		var oWin = $("#win");
		var oLay = $("#overlay");
		oWin.hide();
		oLay.hide();
		buildHtml(minSaleUnitMemo,tableId);
		$selectOption.remove();
	});
	//表单提交
	$("#sumbitBtn").click(function(){
		var aLen = addArray.length;
		var dLen = delArray.length;
		
		if(aLen == 0 && dLen == 0){
			alert("<fmt:message key='marketman.js.chooseSaleAddOrDel' />");
			return false;
		}else{
			var $form = $(form);
			var inputHiddenHtml = "";
			for ( var i = 0; i < aLen; i++) {
				var v = addArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].minSaleUnitcode' value='"+v.unitCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].languageCode' value='"+v.languageCode+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].memo' value='"+v.memo+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].memoDetail' value='"+v.memoDetail+"'/>";
				inputHiddenHtml += "<input type='hidden' name='"+memo1+"["+i+"].name' value='"+v.unitName+"'/>";
			}
			for ( var i = 0; i < dLen; i++) {
				var v = delArray[i];
				inputHiddenHtml += "<input type='hidden' name='"+memo2+"["+i+"].id' value='"+v+"'/>";
			}
			$form.append(inputHiddenHtml);
			$form.submit();
		}
	});
});

/**
 * 
 * @param minSaleUnitMemo	最小销售单位说明
 */
function buildHtml(minSaleUnitMemo,tableId){
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+minSaleUnitMemo.languageName+"</td>");
	var $td2 = $("<td>"+minSaleUnitMemo.unitName+"</td>");
	var $td3 = $("<td style='word-wrap:break-word;word-break:break-all;'>"+minSaleUnitMemo.memo+"</td>");
	var $td5 = $("<td style='word-wrap:break-word;word-break:break-all;'>"+minSaleUnitMemo.memoDetail+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='###' languageName='"+minSaleUnitMemo.languageName+"' languageCode='"+minSaleUnitMemo.languageCode+"'><fmt:message key='marketman.saleconfigman.moveout' /></a>");
	
	$a.click(function(){
		var languageCode = $(this).attr("languageCode");
		addArray.remove(languageCode);
		addOption(this);
		$(this).closest("tr").remove();
	});
	$td4.append($a);
	
	$tr.append($td1).append($td2).append($td3).append($td5).append($td4);
	$(tableId).append($tr);
}

/**
 * 删除最小销售单位说明
 * @param event
 */
function delMemo(event){
	if(confirm("<fmt:message key='marketman.js.confirmYc' />")){
		var $this = $(event);
		//最小销售单位说明id
		var memoId = $this.attr("memoId");
		delArray.push(memoId);
		addOption(event);
		$this.closest("tr").remove();
	}
}

/**
 * 添加下拉菜单语言选项
 * @param event
 */
function addOption(event){
	var $this = $(event);
	var languageCode = $this.attr("languageCode");
	var languageName = $this.attr("languageName");
	var $option = $("<option value='"+languageCode+"'>"+languageName+"</option>");
	$("#language").append($option);
}

/**
 * 
 * @param languageName	语言名称
 * @param languageCode	语言编码
 * @param unitCode	最小销售单位编码
 * @param unitName	最小销售单位名称
 * @param memo	最小销售单位说明
 */
function MinSaleUnitMemo(languageName,languageCode,unitCode,unitName,memo,memoDetail){
	this.languageName = languageName;
	this.languageCode = languageCode;
	this.unitCode = unitCode;
	this.unitName = unitName;
	this.memo = memo;
	this.memoDetail = memoDetail;
}

Array.prototype.remove = function(languageCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(languageCode == this[i].languageCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};