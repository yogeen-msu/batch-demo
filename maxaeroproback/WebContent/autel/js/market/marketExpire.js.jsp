<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 营销活动公共JS
 */
//日期数组
var dateArray = new Array();
//最小销售单位数组
var unitArray = new Array();
//区域配置数组
var areaArray = new Array();
//规则条件数组
var disArray = new Array();

var index = 1;
var disIndex = 1;
//时间表格
var timeTable = "timeTable";
//最小销售单位表格
var unitTable = "unitTable";
//区域表格
var areaTable = "areaTable";
//折扣方式表格
var disTable = "disTable";


//时间段
var timesInfos = "timesInfos";
//最小销售单位
var softwareRules = "softwareRules";
//区域配置
var areaRules = "areaRules";
//规则条件
var discountRules = "discountRules";

$(document).ready(function(){
	
	/**
	 * 时间确认按钮
	 */
	$("#timeFinishBtn").click(function() {
		
		var sDate = $("#startDate").val();
		var eDate = $("#endDate").val();
		if(sDate == "" || eDate == ""){
			alert("<fmt:message key='marketman.js.emptyTime' />");
			return false;
		}
		var startDate = getSpecTime("startDate");
		var endDate = getSpecTime("endDate");
		if(endDate <= startDate){
			alert("<fmt:message key='marketman.js.endTimeGtStartTime' />");
			return false;
		}else{
			var specTime = new SpecTime(startDate,endDate);
			if(!contain(specTime)){
				dateArray.push(specTime);
				$("#"+timeTable).append(createTimeTr(specTime));
				closeDiv(this);
			}else{
				alert("<fmt:message key='marketman.js.timeGtHasAdd' />");
				return false;
			}
		}
	});
	
	/**
	 * 折扣方式
	 */
	$("#disFinishBtn").click(function(){
		var month1 = $("#month1").val();
		var month2 = $("#month2").val();
		var discount = $("#discount").val();
		
		//正整数
		var reg2 = /^[0-9]*[1-9][0-9]*$/;
		
		if(month1 == "" || !reg2.exec(month1) ){
			alert("<fmt:message key='marketman.js.message3' />");
			return false;
		}
		if(month2 == "" || !reg2.exec(month2) ){
			alert("<fmt:message key='marketman.js.message3' />");
			return false;
		}
		
		if(parseFloat(month1) >= parseFloat(month2)){
			alert("<fmt:message key='marketman.js.message4' />");
			return false;
		}
		
		if(discount == "" || !reg2.exec(discount)){
			alert("<fmt:message key='marketman.js.message1' />");
			return false;
		}else{
			discount = parseFloat(discount);
			if(discount <=10){
				alert("<fmt:message key='marketman.js.message6' />");
				return false;
			}
			//if(discount <1 || discount >=10){
				//alert("<fmt:message key='marketman.js.message1' />");
				//return false;
			//}
		}
		
		var $td = $("#month1").closest("td");
		var span1 = $td.find("span:eq(0)").html();
		var span2 = $td.find("span:eq(1)").html();
		var span3 = $td.find("span:eq(2)").html();
		//描述
		var conditionDesc = span1 + month1 + span2 + month2 + span3;
		
		var promotionDiscount = new PromotionDiscount(month1,month2,discount,conditionDesc);
		
		disArray.push(promotionDiscount);
		
		var $tr = createDiscountTr(promotionDiscount);
		$("#"+disTable).append($tr);
		closeDiv(this);
		
	});
	
	/**
	 * 最小销售单位确认事件
	 */
	$("#saleUnitFinishBtn").click(function() {
		
		var size = $("#minSaleUnitSelect option:selected").size();
		
		if(size > 0){
			$("#minSaleUnitSelect option:selected").each(function(){
				var code = $(this).val();
				var name = $(this).text();
				var typeName = $(this).attr("typeName");
				var typeCode = $(this).attr("typeCode");
				
				var minSaleUnit = new MinSaleUnit(code,name,typeName,typeCode);
				unitArray.push(minSaleUnit);
				$("#"+unitTable).append(createMinSaleUnitTr(minSaleUnit));
				
			});
			closeDiv(this);
			$("#minSaleUnitSelect option:selected").remove();
		}else{
			alert("<fmt:message key='marketman.js.chooseMinSale' />");
			return false;
		}
		
	});
	
	
	/**
	 * 区域配置确认按钮
	 */
	$("#areaFinishBtn").click(function(){
		var size = $("#areaSelect option:selected").size();
		
		if(size > 0){
			$("#areaSelect option:selected").each(function(){
				var code = $(this).val();
				var name = $(this).text();
				var area = new Area(code,name);
				areaArray.push(area);
				$("#"+areaTable).append(createAreaTr(area));
				
			});
			closeDiv(this);
			$("#areaSelect option:selected").remove();
		}else{
			alert("<fmt:message key='marketman.js.message2' />");
			return false;
		}
	});
	
	//确认提交按钮
	$("#finishBtn").click(function(){
		var $from = $("#marketForm");
		
		var promotionName = $("#promotionName").val();
		
		if(promotionName == ""){
			alert("<fmt:message key='marketman.js.emptyActity' />");
			return false;
		}
		
		//时间数组长度
		var timeLen = dateArray.length;
		if(timeLen == 0){
			alert("<fmt:message key='marketman.js.addTimeInfo' />");
			return false;
		}
		//折扣方式记录条数
		var disLen = disArray.length;
		if(disLen == 0){
			alert("<fmt:message key='marketman.js.message5' />");
			return false;
		}
		//
		if(!($("#allSaleUnit").attr("checked") == true)){
			var unitLen =  unitArray.length;
			if(unitLen == 0){
				alert("<fmt:message key='marketman.js.chooseMinSale' />");
				return false;
			}
		}
		if(!($("#allArea").attr("checked") == true)){
			var areaLen = areaArray.length;
			if(areaLen == 0){
				alert("<fmt:message key='marketman.js.message2' />");
				return false;
			}
		}
		
		var hiddenHtml ="";
		if(timeLen > 0){
			for(var i = 0 ; i < timeLen ; i++){
				var date = dateArray[i];
				hiddenHtml  += "<input type='hidden' name='"+timesInfos+"["+i+"].startTime' value='"+date.startDate+"'>";
				hiddenHtml  += "<input type='hidden' name='"+timesInfos+"["+i+"].endTime' value='"+date.endDate+"'>";
			}
		}
		
		if(disLen > 0){
			for(var i = 0 ; i < disLen ; i++){
				var date = disArray[i];
				hiddenHtml  += "<input type='hidden' name='"+discountRules+"["+i+"].betweenMonth' value='"+date.betweenMonth+"'>";
				hiddenHtml  += "<input type='hidden' name='"+discountRules+"["+i+"].endMonth' value='"+date.endMonth+"'>";
				hiddenHtml  += "<input type='hidden' name='"+discountRules+"["+i+"].discount' value='"+date.discount+"'>";
				hiddenHtml  += "<input type='hidden' name='"+discountRules+"["+i+"].conditionDesc' value='"+date.conditionDesc+"'>";
			}
		}
		
		if(!($("#allSaleUnit").attr("checked") == true)){
			var unitLen =  unitArray.length;
			if(unitLen > 0){
				for(var i = 0 ; i < unitLen ; i ++){
					var unit = unitArray[i];
					hiddenHtml  += "<input type='hidden' name='"+softwareRules+"["+i+"].minSaleUnitCode' value='"+unit.code+"'>";
					hiddenHtml  += "<input type='hidden' name='"+softwareRules+"["+i+"].productTypeCode' value='"+unit.typeCode+"'>";
					hiddenHtml  += "<input type='hidden' name='"+softwareRules+"["+i+"].minSaleUnitName' value='"+unit.name+"'>";
				}
			}
		}
		
		/**
		 * 销售配置
		 */
		var saleConfigLength = saleConfigArray.length;
		if(saleConfigLength > 0){
			for(var i = 0 ; i < saleConfigLength ; i ++){
				var saleConfig = saleConfigArray[i];
				hiddenHtml  += "<input type='hidden' name='"+promotionSaleConfigs+"["+i+"].saleConfigCode' value='"+saleConfig.code+"'>";
				hiddenHtml  += "<input type='hidden' name='"+promotionSaleConfigs+"["+i+"].productTypeCode' value='"+saleConfig.typeCode+"'>";
				hiddenHtml  += "<input type='hidden' name='"+promotionSaleConfigs+"["+i+"].saleConfigName' value='"+saleConfig.name+"'>";
			}
		}
		
		if(!($("#allArea").attr("checked") == true)){
			var areaLen =  areaArray.length;
			if(areaLen > 0){
				for(var i = 0 ; i < areaLen ; i ++){
					var area = areaArray[i];
					hiddenHtml  += "<input type='hidden' name='"+areaRules+"["+i+"].areaCode' value='"+area.areaCode+"'>";
				}
			}
		}
		$from.append(hiddenHtml);
		$from.submit();
		
	});
});

/**
 * 自定义时间
 * @param startDate	开始时间
 * @param endDate	结束时间
 */
function SpecTime(startDate,endDate){
	this.specId = index;;
	this.startDate = startDate;
	this.endDate = endDate;
	index++;
}

/**
 * 
 * @param betweenMonth	开始月
 * @param endMonth	结束月
 * @param discount	折扣
 */
function PromotionDiscount(betweenMonth,endMonth,discount,conditionDesc){
	this.disId = disIndex;
	this.betweenMonth = betweenMonth;
	this.endMonth = endMonth;
	this.discount = discount;
	this.conditionDesc = conditionDesc;
	disIndex ++ ;
}

/**
 * 
 * @param code	编码
 * @param name	最小销售单位名称
 * @param typeName	产品型号名称
 * @returns {MinSaleUnit}	最小销售单位对象
 */
function MinSaleUnit(code,name,typeName,typeCode){
	this.code = code;
	this.name = name;
	this.typeName = typeName;
	this.typeCode = typeCode;
}

/**
 * 
 * @param areaCode	区域编码
 * @param areaName	区域名称
 * @returns {Area}	区域对象
 */
function Area(areaCode,areaName) {
	this.areaCode = areaCode;
	this.areaName = areaName;
}

/**
 * 获取自定义时间
 * @param id
 */
function getSpecTime(id){
	var $date = $("#"+id);
	var date = $date.val();
	var $hour = $date.closest("td").find(".hour");
	var hour = $hour.val();
	var $minute = $date.closest("td").find(".minute");
	var minute = $minute.val();
	return date + " " + hour + ":" + minute + ":"+"00"; 
}

/**
 * 数组排序方法  返回false 表示不包含(时间无交叉),true 表示包含(时间有交叉)
 * @param specTime 
 */
function contain(specTime){
	var len  = dateArray.length;
	if(len == 0){
		return false;
	}
	for(var i  = 0 ;i < len ; i++){
		var dtime = dateArray[i];
		var startDate = dtime.startDate;
		var endDate = dtime.endDate;
		if(!((specTime.startDate < startDate && specTime.endDate <= startDate) || (specTime.startDate >= endDate && specTime.endDate > endDate))){
			return true;
		}
	}
}
/**
 * 创建时间表格行
 * @param specTime
 * @returns
 */
function createTimeTr(specTime){
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+specTime.startDate+"</td>"); 
	var $td2 = $("<td>"+specTime.endDate+"</td>"); 
	var $td3 = $("<td class='caoz'></td>"); 
	var $a = $("<a href='#' specId='"+specTime.specId+"'><fmt:message key='common.js.delInfo' /></a>");
	$a.click(function() {
		var specId = $(this).attr("specId");
		$(this).closest("tr").remove();
		dateArray.remove(specId);
	});
	$td3.append($a);
	$tr.append($td1).append($td2).append($td3);
	return $tr;
}

/**
 * 创建折扣方式行
 * @param promotionDiscount
 * @returns
 */
function createDiscountTr(promotionDiscount){
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+promotionDiscount.conditionDesc+"</td>"); 
	var $td2 = $("<td>"+promotionDiscount.discount+"</td>"); 
	var $td3 = $("<td class='caoz'></td>"); 
	var $a = $("<a href='#' disId='"+promotionDiscount.disId+"'><fmt:message key='common.js.delInfo' /></a>");
	$a.click(function() {
		var disId = $(this).attr("disId");
		$(this).closest("tr").remove();
		disArray.removeDis(disId);
	});
	$td3.append($a);
	$tr.append($td1).append($td2).append($td3);
	return $tr;
}

/**
 * 创建最小销售单位行
 * @param minSaleUnit	最小销售单位
 */
function createMinSaleUnitTr(minSaleUnit) {
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+minSaleUnit.code+"</td>");
	var $td2 = $("<td>"+minSaleUnit.name+"</td>");
	var $td3 = $("<td>"+minSaleUnit.typeName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='#' typeCode='"+minSaleUnit.typeCode+"' unitCode='"+minSaleUnit.code+"' unitName='"+minSaleUnit.name+"' typeName='"+minSaleUnit.typeName+"'><fmt:message key='common.js.delInfo' /></a>");

	$a.click(function(){
		var unitCode = $(this).attr("unitCode");
		var unitName = $(this).attr("unitName");
		var typeName = $(this).attr("typeName");
		var typeCode = $(this).attr("typeCode");
		
		$(this).closest("tr").remove();
		unitArray.removeUnit(unitCode);
		$("#minSaleUnitSelect").append("<option value='"+unitCode+"' typeCode='"+typeCode+"' typeName='"+typeName+"'>"+unitName+"</option>");
	});
	
	$td4.append($a);
	$tr.append($td1).append($td2).append($td3).append($td4);
	return $tr;
}

/**
 * 创建area行
 * @param area
 * @returns
 */
function createAreaTr(area) {
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+area.areaCode+"</td>");
	var $td2 = $("<td>"+area.areaName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='#' areaCode='"+area.areaCode+"' areaName='"+area.areaName+"'><fmt:message key='common.js.delInfo' /></a>");

	$a.click(function(){
		var areaCode = $(this).attr("areaCode");
		var areaName = $(this).attr("areaName");
		$(this).closest("tr").remove();
		areaArray.removeArea(areaCode);
		$("#areaSelect").append("<option value='"+areaCode+"'>"+areaName+"</option>");
	});
	
	$td4.append($a);
	$tr.append($td1).append($td2).append($td4);
	return $tr;
}

/**
 * 删除时间段
 */
Array.prototype.remove = function(specId){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(specId == this[i].specId){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

/**
 * 删除折扣方式
 */
Array.prototype.removeDis = function(disId){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(disId == this[i].disId){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

Array.prototype.removeUnit = function(unitCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(unitCode == this[i].code){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

Array.prototype.removeArea = function(areaCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(areaCode == this[i].areaCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

/**
 * 隐藏div
 * @param event
 */
function closeDiv(event){
	var overlay = $(event).closest("div.active").find(".dialog_overlay");
	var win = $(event).closest("div.active").find(".dialog_win");
	overlay.hide();
	win.hide();
}





