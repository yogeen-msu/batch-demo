<%@ include file="/autel/js/common/fmt.js.jsp"%>

var Software={
	init:function(){
		var self = this;
			
		$("#selectSoftDlg").click(function(){
			clearAllDivContent();
			self.openSoftwareSelectDlg();
		});
		
		Cop.Dialog.init({id:"selectSoftwareDlg",title:'',height:"800px",width:"400px",scroll:"auto"});
	},
	openSoftwareSelectDlg:function(){
		var self  = this;
		
		$("#selectSoftwareDlg").load("toSearchMinSale.do?ajax=yes",function(){
			self.createTree();
			
			$("ul.checktree").checkTree();
		});
		
		Cop.Dialog.open("selectSoftwareDlg");
	},
	
	createTree:function(){
		var self = this;
		
		$.each(treeJson[0],function(k,v){
			var li = self.createNode(v,0);
			var ul = self.createChildren(v.minList);
			li.append(ul);
			$("#productsoftwareconfigbox>.checktree").append(li);
		});
	},
	
	createChildren:function(children){
		var ul=$("<ul></ul>");
		var self = this;
		$.each(children,function(k,v){
			var li = self.createNode(v,1);
		
			var children2 =v.children;
			if(children2 && children2.length>0){
				li.append(self.createChildren(children2));
			}	
			ul.append(li);
		});
		return ul;
	},
	
	createNode:function(v,flag){
		var li = $("<li><input flag=\""+flag+"\" minName=\""+v.name+"\"  type=\"checkbox\" name=\"productsoftwareconfigcode\" value=\""+v.code+"\"><label>"+v.name+"</label></li>");
		return li;
	}
	
};

function pageQuery(){
	
	//å¤çéä¸­çèæç®å½
	var $selprosoftconfigcode = $("input[name='productsoftwareconfigcode']:checked");
	var size = $selprosoftconfigcode.size();
	if(size ==0){
		alert("<fmt:message key='product.virtualdir.noselected' />");
		return false;
	}
	
	var selprosoftconfigcodes = "";
	$selprosoftconfigcode.each(function(){
		if(selprosoftconfigcodes == "")
		{
			selprosoftconfigcodes = "'"+$(this).val()+"'";
		}
		else
		{
			selprosoftconfigcodes = selprosoftconfigcodes + ",'"+$(this).val()+"'";
		}
	});
	
	$('#selprosoftconfigcodes').val(selprosoftconfigcodes);
	
	
	//å·²æçè½¯ä»¶
	var excludecodes = "";
	
	/**
	$(".softwaretypeclass").each(function(){
		if(excludecodes==""){
			excludecodes="'"+$(this).attr("softwareTypeCode")+"'";
		}else{
			excludecodes=excludecodes+",'"+$(this).attr("softwareTypeCode")+"'";
		}
	});
	*/
	
	$.each(viewArray,function(k,v){
		if(excludecodes==""){
			excludecodes="'"+v.softwareTypeCode+"'";
		}else{
			excludecodes=excludecodes+",'"+v.softwareTypeCode+"'";
		}
	});
	
	$('#excludecodes').val(excludecodes);
			
	//æ§è¡æ¥è¯¢
	$.ajax({
		url:"toSearchSoftDlgNew.do",
		type:"post",
		data: $('#pageSelectForm').serialize(),
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			
			//å¯¹è±¡éè½½
			$selobj = $("#selectedcodes");
			$checkbs = $("input[name='checkboxcode']");
			$allckbs = $("input[name='allCheckbox']");
			isYinhao = true;
			checkbnum = $checkbs.length;
			checkednum = 0;
			$selobj.val("");
	
			$checkbs.click(function(){
				var $this = $(this);
				xuanze($this);
			});
			
			$allckbs.click(function(){
				var $this = $(this);
				quanxuan($this);
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};


function clearAllDivContent()
{
	$("#selectSoftwareDlg").html("");
};

