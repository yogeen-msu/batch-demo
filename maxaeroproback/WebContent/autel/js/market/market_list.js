
/**
 * 营销活动JS
 */
$(document).ready(function(){
	//全选
	$("input[name^='chkAll']").click(function(){
		if($(this).attr("checked")){
			$("input[name='chk']").attr("checked","checked");
		}else{
			$("input[name='chk']").removeAttr("checked");
		}
	});
	
	//启用
	$("#startBtn").click(function(){
		var promotionType = $(this).attr("promotionType");
		var promotionCodes = $("input[name='chk']:checked").map(function(){
			return $(this).val();
		}).get().join(",");
		jump ("startPromotion.do?promotionCodes="+promotionCodes+"&search.promotionType="+promotionType);
	});
	
	//终止
	$("#stopBtn").click(function(){
		var promotionType = $(this).attr("promotionType");
		var promotionCodes = $("input[name='chk']:checked").map(function(){
			return $(this).val();
		}).get().join(",");
		jump("stopPromotion.do?promotionCodes="+promotionCodes+"&search.promotionType="+promotionType);
	});
});

function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}

function delPromotion(code,promotionType){
	if(confirm("确认删除吗?")){
		jump("delPromotion.do?baseInfo.code="+code+"&search.promotionType="+promotionType);
	}
}