/**
 * 销售契约JS
 */
$(document).ready(function(){
	
	$("a[name^='delSaleContract']").click(function(){
		if(confirm("确认删除吗?")){
			var $from = $("#saleContractForm");
			var code = $(this).attr("saleCode");
			$from.attr("action","delSaleContract.do");
			$from.append("<input type='hidden' name='saleContract.code' value='"+code+"'/>");
			$from.submit();
		}
	});
});