<%@ include file="/autel/js/common/fmt.js.jsp"%>

/**
 * 销售配置管理JS
 */
$(document).ready(function(){
	
	$("a[name^='delSaleConfig']").click(function(){
		if(confirm("<fmt:message key='common.js.confirmDel' />")){
			var $from = $("#saleConfigForm");
			var code = $(this).attr("saleCode");
			$from.attr("action","delSaleConfig.do");
			$from.append("<input type='hidden' name='saleConfig.code' value='"+code+"'/>");
			$from.submit();
		}
	});
});