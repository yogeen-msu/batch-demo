<%@ include file="/autel/js/common/fmt.js.jsp"%>
/**
 * 营销活动公共JS
 */
//日期数组
var dateArray = new Array();
var delDateArray = new Array();
//最小销售单位数组
var unitArray = new Array();
var delUnitArray = new Array();
//区域配置数组
var areaArray = new Array();
var delAreaArray = new Array();

var index = 1;
//时间表格
var timeTable = "timeTable";
//最小销售单位表格
var unitTable = "unitTable";
//区域表格
var areaTable = "areaTable";

//时间段
var timesInfos = "timesInfos";
var delTimesInfos = "delTimesInfos";
//最小销售单位
var softwareRules = "softwareRules";
var delSoftwareRules = "delSoftwareRules";
//区域配置
var areaRules = "areaRules";
var delAreaRules = "delAreaRules";

$(document).ready(function(){
	
	/**
	 * 时间确认按钮
	 */
	$("#timeFinishBtn").click(function() {
		
		var sDate = $("#startDate").val();
		var eDate = $("#endDate").val();
		if(sDate == "" || eDate == ""){
		
			alert("<fmt:message key='marketman.js.emptyTime' />");
			return false;
		}
		var startDate = getSpecTime("startDate");
		var endDate = getSpecTime("endDate");
		if(endDate <= startDate){
			alert("<fmt:message key='marketman.js.endTimeGtStartTime' />");
			return false;
		}else{
			var specTime = new SpecTime(startDate,endDate);
			var hasTime = new Array();
			$("#"+timeTable).find("tr:gt(0)").each(function(){
				startTime = $(this).find("td:eq(0)").text();
				endTime = $(this).find("td:eq(1)").text();
				var hasSpecTime = new SpecTime(startTime,endTime);
				hasTime.push(hasSpecTime);
			});
			if(!contain(dateArray,specTime) && !contain(hasTime,specTime)){
				dateArray.push(specTime);
				$("#"+timeTable).append(createTimeTr(specTime));
				closeDiv(this);
			}else{
			
				alert("<fmt:message key='marketman.js.timeGtHasAdd' />");
				return false;
			}
		}
	});
	
	/**
	 * 最小销售单位确认事件
	 */
	$("#saleUnitFinishBtn").click(function() {
		
		var size = $("#minSaleUnitSelect option:selected").size();
		
		if(size > 0){
			$("#minSaleUnitSelect option:selected").each(function(){
				var code = $(this).val();
				var name = $(this).text();
				var typeName = $(this).attr("typeName");
				var typeCode = $(this).attr("typeCode");
				
				var minSaleUnit = new MinSaleUnit(code,name,typeName,typeCode);
				unitArray.push(minSaleUnit);
				$("#"+unitTable).append(createMinSaleUnitTr(minSaleUnit));
				
			});
			closeDiv(this);
			$("#minSaleUnitSelect option:selected").remove();
		}else{
		
			alert("<fmt:message key='marketman.js.chooseMinSale' />");
			return false;
		}
		
	});
	
	/**
	 * 区域配置确认按钮
	 */
	$("#areaFinishBtn").click(function(){
		var size = $("#areaSelect option:selected").size();
		
		if(size > 0){
			$("#areaSelect option:selected").each(function(){
				var code = $(this).val();
				var name = $(this).text();
				var area = new Area(code,name);
				areaArray.push(area);
				$("#"+areaTable).append(createAreaTr(area));
				
			});
			closeDiv(this);
			$("#areaSelect option:selected").remove();
		}else{
		
			alert("<fmt:message key='marketman.js.chooseArea' />");
			return false;
		}
	});
	
	//确认提交按钮
	$("#finishBtn").click(function(){
		var $from = $("#marketForm");
		
		var promotionName = $("#promotionName").val();
		
		if(promotionName == ""){
		
			alert("<fmt:message key='marketman.js.emptyActity' />");
			return false;
		}
		
		//时间数据的验证
		var timeSize = $("#"+timeTable).find("tr").size();
		
		if(timeSize == 1){
		
			alert("<fmt:message key='marketman.js.addTimeInfo' />");
			return false;
		}
		
		//正浮点数
		var reg1 = /^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
		//正整数
		var reg2 = /^[0-9]*[1-9][0-9]*$/;
		
		var discount = $("#discount").val();
		if(discount == "" || !reg1.exec(discount) || !reg2.exec(discount)){
		
			alert("<fmt:message key='marketman.js.message1' />");
			return false;
		}else{
			discount = parseFloat(discount);
			if(discount <1 || discount >=10){
				alert("<fmt:message key='marketman.js.message1' />");
				return false;
			}
		}
		//
		if(!($("#allSaleUnit").attr("checked") == true)){
			var unitSize = $("#"+unitTable).find("tr").size();
			if(unitSize == 1){
			
				alert("<fmt:message key='marketman.js.chooseMinSale' />");
				return false;
			}
		}
		if(!($("#allArea").attr("checked") == true)){
			var areaSize = $("#"+areaTable).find("tr").size();
			if(areaSize == 1){
			
				alert("<fmt:message key='marketman.js.message2' />");
				return false;
			}
		}
		
		var hiddenHtml ="";
		var timeLen = dateArray.length;
		if(timeLen > 0){
			for(var i = 0 ; i < timeLen ; i++){
				var date = dateArray[i];
				hiddenHtml  += "<input type='hidden' name='"+timesInfos+"["+i+"].startTime' value='"+date.startDate+"'>";
				hiddenHtml  += "<input type='hidden' name='"+timesInfos+"["+i+"].endTime' value='"+date.endDate+"'>";
			}
		}
		var delTimeLen = delDateArray.length;
		if(delTimeLen > 0){
			for(var i = 0 ; i < delTimeLen ; i++){
				var date = delDateArray[i];
				hiddenHtml  += "<input type='hidden' name='"+delTimesInfos+"["+i+"].id' value='"+date+"'>";
			}
		}
		
		if(!($("#allSaleUnit").attr("checked") == true)){
			var unitLen =  unitArray.length;
			if(unitLen > 0){
				for(var i = 0 ; i < unitLen ; i ++){
					var unit = unitArray[i];
					hiddenHtml  += "<input type='hidden' name='"+softwareRules+"["+i+"].minSaleUnitCode' value='"+unit.code+"'>";
					hiddenHtml  += "<input type='hidden' name='"+softwareRules+"["+i+"].productTypeCode' value='"+unit.typeCode+"'>";
					hiddenHtml  += "<input type='hidden' name='"+softwareRules+"["+i+"].minSaleUnitName' value='"+unit.name+"'>";
				}
			}
			
			var delUnitLen =  delUnitArray.length;
			if(delUnitLen > 0){
				for(var i = 0 ; i < delUnitLen ; i ++){
					var unit = delUnitArray[i];
					hiddenHtml  += "<input type='hidden' name='"+delSoftwareRules+"["+i+"].id' value='"+unit+"'>";
				}
			}
		}
		
		//销售配置
		var saleConfigLength = saleConfigArray.length;
		if(saleConfigLength > 0){
			for(var i = 0 ; i < saleConfigLength ; i ++){
				var saleConfig = saleConfigArray[i];
				hiddenHtml  += "<input type='hidden' name='"+promotionSaleConfigs+"["+i+"].saleConfigCode' value='"+saleConfig.code+"'>";
				hiddenHtml  += "<input type='hidden' name='"+promotionSaleConfigs+"["+i+"].productTypeCode' value='"+saleConfig.typeCode+"'>";
				hiddenHtml  += "<input type='hidden' name='"+promotionSaleConfigs+"["+i+"].saleConfigName' value='"+saleConfig.name+"'>";
			}
		}
		
		var delSaleLength = delSaleConfiArray.length;
		if(delSaleLength > 0){
			for(var i = 0 ; i < delSaleLength ; i ++){
				var saleid = delSaleConfiArray[i];
				hiddenHtml  += "<input type='hidden' name='"+delPromotionSaleConfigs+"["+i+"].id' value='"+saleid+"'>";
			}
		}
		
		if(!($("#allArea").attr("checked") == true)){
			var areaLen =  areaArray.length;
			if(areaLen > 0){
				for(var i = 0 ; i < areaLen ; i ++){
					var area = areaArray[i];
					hiddenHtml  += "<input type='hidden' name='"+areaRules+"["+i+"].areaCode' value='"+area.areaCode+"'>";
				}
			}
			
			var delAreaLen = delAreaArray.length;
			if(delAreaLen > 0){
				for(var i = 0 ; i < delAreaLen ; i ++){
					var area = delAreaArray[i];
					hiddenHtml  += "<input type='hidden' name='"+delAreaRules+"["+i+"].id' value='"+area+"'>";
				}
			}
		}
		$from.append(hiddenHtml);
		$from.submit();
		
	});
});

/**
 * 自定义时间
 * @param startDate	开始时间
 * @param endDate	结束时间
 */
function SpecTime(startDate,endDate){
	this.specId = index;;
	this.startDate = startDate;
	this.endDate = endDate;
	index++;
}


/**
 * 
 * @param code	编码
 * @param name	最小销售单位名称
 * @param typeName	产品型号名称
 * @returns {MinSaleUnit}	最小销售单位对象
 */
function MinSaleUnit(code,name,typeName,typeCode){
	this.code = code;
	this.name = name;
	this.typeName = typeName;
	this.typeCode = typeCode;
}

/**
 * 
 * @param areaCode	区域编码
 * @param areaName	区域名称
 * @returns {Area}	区域对象
 */
function Area(areaCode,areaName) {
	this.areaCode = areaCode;
	this.areaName = areaName;
}

/**
 * 获取自定义时间
 * @param id
 */
function getSpecTime(id){
	var $date = $("#"+id);
	var date = $date.val();
	var $hour = $date.closest("td").find(".hour");
	var hour = $hour.val();
	var $minute = $date.closest("td").find(".minute");
	var minute = $minute.val();
	return date + " " + hour + ":" + minute + ":"+"00"; 
}

/**
 * 数组排序方法  返回false 表示不包含(时间无交叉),true 表示包含(时间有交叉)
 * @param specTime 
 */
function contain(arg ,specTime){
	var len  = arg.length;
	if(len == 0){
		return false;
	}
	for(var i  = 0 ;i < len ; i++){
		var dtime = arg[i];
		var startDate = dtime.startDate;
		var endDate = dtime.endDate;
		if(!((specTime.startDate < startDate && specTime.endDate <= startDate) || (specTime.startDate >= endDate && specTime.endDate > endDate))){
			return true;
		}
	}
}
/**
 * 创建时间表格行
 * @param specTime
 * @returns
 */
function createTimeTr(specTime){
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+specTime.startDate+"</td>"); 
	var $td2 = $("<td>"+specTime.endDate+"</td>"); 
	var $td3 = $("<td class='caoz'></td>"); 
	var $a = $("<a href='#' specId='"+specTime.specId+"'><fmt:message key='common.js.delInfo' /></a>");
	$a.click(function() {
		var specId = $(this).attr("specId");
		$(this).closest("tr").remove();
		dateArray.remove(specId);
	});
	$td3.append($a);
	$tr.append($td1).append($td2).append($td3);
	return $tr;
}

/**
 * 创建最小销售单位行
 * @param minSaleUnit	最小销售单位
 */
function createMinSaleUnitTr(minSaleUnit) {
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+minSaleUnit.code+"</td>");
	var $td2 = $("<td>"+minSaleUnit.name+"</td>");
	var $td3 = $("<td>"+minSaleUnit.typeName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='#' typeCode='"+minSaleUnit.typeCode+"' unitCode='"+minSaleUnit.code+"' unitName='"+minSaleUnit.name+"' typeName='"+minSaleUnit.typeName+"'><fmt:message key='common.js.delInfo' /></a>");

	$a.click(function(){
		var unitCode = $(this).attr("unitCode");
		var unitName = $(this).attr("unitName");
		var typeName = $(this).attr("typeName");
		var typeCode = $(this).attr("typeCode");
		
		$(this).closest("tr").remove();
		unitArray.removeUnit(unitCode);
		$("#minSaleUnitSelect").append("<option value='"+unitCode+"' typeCode='"+typeCode+"' typeName='"+typeName+"'>"+unitName+"</option>");
	});
	
	$td4.append($a);
	$tr.append($td1).append($td2).append($td3).append($td4);
	return $tr;
}

/**
 * 创建area行
 * @param area
 * @returns
 */
function createAreaTr(area) {
	var $tr = $("<tr></tr>");
	var $td1 = $("<td>"+area.areaCode+"</td>");
	var $td2 = $("<td>"+area.areaName+"</td>");
	var $td4 = $("<td class='caoz'></td>");
	var $a = $("<a href='#' areaCode='"+area.areaCode+"' areaName='"+area.areaName+"'><fmt:message key='common.js.delInfo' /></a>");

	$a.click(function(){
		var areaCode = $(this).attr("areaCode");
		var areaName = $(this).attr("areaName");
		$(this).closest("tr").remove();
		areaArray.removeArea(areaCode);
		$("#areaSelect").append("<option value='"+areaCode+"'>"+areaName+"</option>");
	});
	
	$td4.append($a);
	$tr.append($td1).append($td2).append($td4);
	return $tr;
}

Array.prototype.remove = function(specId){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(specId == this[i].specId){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

Array.prototype.removeUnit = function(unitCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(unitCode == this[i].code){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

Array.prototype.removeArea = function(areaCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(areaCode == this[i].areaCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};

/**
 * 隐藏div
 * @param event
 */
function closeDiv(event){
	var overlay = $(event).closest("div.active").find(".dialog_overlay");
	var win = $(event).closest("div.active").find(".dialog_win");
	overlay.hide();
	win.hide();
}

/**
 * 已有数据的时间删除
 * @param event
 */
function deleteTime(event){
	if(confirm("<fmt:message key='common.js.confirmDel' />")){
		var $this =  $(event);
		var timeId = $this.attr("timeId");
		delDateArray.push(timeId);
		$this.closest("tr").remove();
	}
}

/**
 * 删除最小销售单位
 * @param event
 */
function delMinSaleUnit(event){
	if(confirm("<fmt:message key='common.js.confirmDel' />")){
		var $this = $(event);
		var softwareId = $this.attr("softwareId");
		var code = $this.attr("code");
		var typeCode = $this.attr("typeCode");
		var typeName = $this.attr("typeName");
		var unitName = $this.attr("unitName");
		$("#minSaleUnitSelect").append("<option value='"+code+"' typeCode='"+typeCode+"' typeName='"+typeName+"'>"+unitName+"</option>");;
		delUnitArray.push(softwareId);
		$this.closest("tr").remove();
	}
}

/**
 *  删除区域配置
 * @param event
 */
function delArea(event){
	if(confirm("<fmt:message key='common.js.confirmDel' />")){
		var $this = $(event);
		var areaId = $this.attr("areaId");
		var areaCode = $this.attr("areaCode");
		var areaName = $this.attr("areaName");
		$("#areaSelect").append("<option value='"+areaCode+"'>"+areaName+"</option>");
		delAreaArray.push(areaId);
		$this.closest("tr").remove();
	}
}