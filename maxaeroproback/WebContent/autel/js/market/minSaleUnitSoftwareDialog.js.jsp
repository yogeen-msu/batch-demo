<%@ include file="/autel/js/common/fmt.js.jsp"%>

var Software  ={
	init:function(){
		var self  = this;
		//打开选择软件页面
		Cop.Dialog.init({id:"selectSoftwareDlg",modal:false,title:'<fmt:message key="saleDialog.choose.software" />',height:"400px",width:"750px",scroll:"auto"});
		$("#selectSoftDlg").click(function(){
			clearAllDivContent();
			self.openSoftwareSelectDlg();
			
		});
		
	}, 

	//打开选择软件页面
	openSoftwareSelectDlg:function(){
		
			
		//var softwareArray = new Array();
		var excludecodes = "";
		
		$(".softwaretypeclass").each(function(){			
			//var softwareType = new SoftwareType($(".softwaretypeclass").attr("softwareTypeCode"),$(".softwaretypeclass").attr("softwareTypeName"));
			//softwareArray.push(softwareType);
			if(excludecodes==""){
				excludecodes="'"+$(this).attr("softwareTypeCode")+"'";
			}else{
				excludecodes=excludecodes+",'"+$(this).attr("softwareTypeCode")+"'";
			}
			
		});
		
		
		$("#selectSoftwareDlg").load("toSearchSoftDlg.do?minSaleUnit.code="+$("#unitCode").val()+"&&softwareType.name=&&excludecodes="+excludecodes+"&&ajax=yes",function(){
		});
		Cop.Dialog.open("selectSoftwareDlg");
		
	}
	
};

/**
 * 软件对象
 */
function SoftwareType(code,name){
	this.code = code;
	this.name = name;
}


function bindEvent(pager,grid){
	 var grid = pager.parent();
	 pager.find("li>a").unbind(".click").bind("click",function(){
		 load($(this).attr("pageno"),grid);
	 }); 
	 pager.find("a.selected").unbind("click");
};

function pageQuery(){
	$.ajax({
		url:"toSearchSoftDlg.do",
		type:"post",
		data: $('#pageSelectForm').serialize(), 
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			//bindEvent(grid.children(".page"));			//绑定分页事件
			
			
			$selobj = $("#selectedcodes");
			$checkbs = $("input[name='checkboxcode']");
			$allckbs = $("input[name='allCheckbox']");
			isYinhao = true;
			checkbnum = $checkbs.length;
			checkednum = 0;
			$selobj.val("");
	
			$checkbs.click(function(){
				var $this = $(this);
				xuanze($this);
			});
			
			$allckbs.click(function(){
				var $this = $(this);
				quanxuan($this);
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};

function clearAllDivContent()
{
	$("#selectSoftwareDlg").html("");
};
