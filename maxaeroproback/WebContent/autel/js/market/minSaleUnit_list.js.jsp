<%@ include file="/autel/js/common/fmt.js.jsp"%>
/**
 * 最小销售单位管理JS
 */
$(document).ready(function(){
	
	$("a[name^='delMinSaleUnit']").click(function(){
		if(confirm("<fmt:message key='common.js.confirmDel' />")){
			var $from = $("#minSaleUnitForm");
			var code = $(this).attr("code");
			$from.attr("action","delMinSaleUnit.do");
			$from.append("<input type='hidden' name='minSaleUnitSearch.code' value='"+code+"'/>");
			$from.submit();
		}
	});
});