/**
 * 后台界面构建js
 * @author kingapex
 */
var BackendMenuUi={
	menu:undefined,
	pid:undefined,
	init:function(menu,pid){
		this.menu =menu;
		this.pid = pid;

	},
	disMenu:function(){
		this.disAppMenu();
	},
	
	/**
	 * 显示应用菜单
	 */
	disAppMenu:function(){
		var self=this;
		var menu = this.menu;
		var pid = this.pid;
		$.each(menu.app,function(k,v){
			if(founder ==1 && (v.id==237 || v.id==244 ||v.id==266)){}else{
				
				if(v.id == pid){
					var children = v.children;
					if(children){
						self.disAppChildren(children);
					}
					return false;
				}
				
			}
		});			
	},
	/**
	 * 显示应用的子菜单
	 */
	disAppChildren:function(children){
		var self= this;
		var leftMenu = $(".left_menu");
		leftMenu.empty();
		$.each(children,function(k,v){
			var $div = $("<div class=\"lanm\"></div>");
			var $img = $("<img src=\"images/leg_09.gif\" width=\"35\" height=\"15\"> "+this.text +"</img>");
			$div.append($("<div></div>").append($img));
			
			$img.click(function(){
				$(this).parent().next().toggle();
			});
			
			if(this.children){
				var $dl = $("<dl></dl>");
				$.each(this.children,function(k,v){
					var link = self.createLink(v);
						$dl.append($("<dd></dd>").append(link));
				});
				$div.append($dl);
			}
			leftMenu.append($div);
		});
	},
	createLink:function(v){
		v.target = "mainContentFrame";
		var link = $("<a  target='"+v.target+"' href='"+ v.url +"' >" + v.text + "</a>");
		return link;
	}
};

$(function(){
	BackendMenuUi.init(menu,pid);
	BackendMenuUi.disMenu();
});
