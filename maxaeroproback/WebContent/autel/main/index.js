/**
 * 后台界面构建js
 * @author shaohu
 */
var BackendUi={
	menu:undefined,
	init:function(menu){
		this.menu =menu;
	},
	disMenu:function(){
		//this.disSysMenu();
		this.disAppMenu();
	},
	
	/**
	 * 显示系统菜单
	 */
	disSysMenu:function(){
		var self =this;
		var menu = this.menu;
		
		$.each(menu.sys,function(k,v){
			var link = self.createLink(v);
			 $("<li/>").appendTo( $(".sysmenu>ul") ).append(link);
			 if(v.target!='_blank'){
				link.click(function(){
						Cop.AdminUI.load($(this));
						return false;
					});
			 }
		});		
	},
	/**
	 * 显示应用菜单
	 */
	disAppMenu:function(){
		var menu = this.menu;
		var i=0;
		$.each(menu.app,function(k,v){
			if(founder ==1 && (v.id==237 || v.id==244 ||v.id==266)){}else{
				v.target = "mainContentFrame";
				var link = $("<a target='"+v.target+"' href='"+ v.url +"' fid='"+v.id+"'>" + v.text + "</a>");
				$("<li></li>").appendTo($(".menu>ul")).append(link);
				
				link.click(function(){
					console.log("test");
					$(".menu li").removeClass("current");
					$(this).parent().addClass("current");
					
				});
				
				if(i==0){ 
					var href= link.attr("href");
					var target=link.attr("target");
					link.attr("href",app_path+"/core/admin/index.do");
					link.removeAttr("target");
					link.parent().addClass("current");
					link.attr("href",href);
					link.attr("target",target);
				}
				
				i++;
			}
		});			
	},
	
	createLink:function(v){
		var link = $("<a  target='"+v.target+"' href='"+ v.url +"' >" + v.text + "</a>");
		return link;
	}

};

$(function(){
	BackendUi.init(menu);
	BackendUi.disMenu();
});
