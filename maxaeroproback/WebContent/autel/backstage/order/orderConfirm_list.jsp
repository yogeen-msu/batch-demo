<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="orderman.list" /></title>
<link href="../../autel/main/css/right.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="../../autel/js/order/orderConfirm.js.jsp?ajax=yes"></script>
	<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">
	//$(document).ready(function(){
		//$("#endTime").attr("style","float:right ;margin-right:-28px;");
	//});
</script>

</head>

<body>
	<form action="toOrderConfirmList.do" method="post">
		<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="orderman.title" />&gt; <fmt:message key="orderman.orderconfirm" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../../autel/main/images/leg_16.gif" width="14"
						height="16" />&nbsp;<fmt:message key="orderman.orderconfirm" />
				</h4>
				<div class="sear_table1">
					<table width="100%" border="0" style="background-color: #F1F5FE;color: #5B5A59;">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="orderman.list.ddh" />:</td>
							<td width="154"><input name="orderSearch.orderCode"
								type="text" class="time_in" value="${orderSearch.orderCode }" />
							</td>
							<td width="80" align="right"><fmt:message key="accountinformation.autelid.name" />:</td>
							<td width="154"><input name="orderSearch.customerName"
								type="text" class="time_in" value="${orderSearch.customerName }" />
							</td>
							<td width="80" align="right"><fmt:message key="orderman.list.dealstate" />:</td>
							<td><select name="orderSearch.processState" class="act_zt"><option
										value=""><fmt:message key="orderman.list.all" /></option>
									<option value="1"
										<c:if test="${orderSearch.processState ==1 }"> selected="selected"</c:if>><fmt:message key="orderman.list.dealed" /></option>
									<option value="0"
										<c:if test="${orderSearch.processState ==0 }"> selected="selected"</c:if>><fmt:message key="orderman.list.nodeal" /></option>
							</select></td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="orderman.list.getmoneyconfirmstate" />:</td>
								<td><select name="orderSearch.recConfirmState"
									class="act_zt"><option value=""><fmt:message key="orderman.list.all" /></option>
										<option value="1"
											<c:if test="${orderSearch.recConfirmState ==1 }"> selected="selected"</c:if>><fmt:message key="orderman.list.confirmed" /></option>
										<option value="0"
											<c:if test="${orderSearch.recConfirmState ==0 }"> selected="selected"</c:if>><fmt:message key="orderman.list.noconfirm" /></option>
								</select></td>
								<td align="right"><fmt:message key="memberman.customman.orderstate" />:</td>
								<td><select name="orderSearch.orderState" class="act_zt"><option
											value=""><fmt:message key="orderman.list.all" /></option>
										<option value="1"
											<c:if test="${orderSearch.orderState ==1 }"> selected="selected"</c:if>><fmt:message key="memberman.customman.valid" /></option>
										<option value="0"
											<c:if test="${orderSearch.orderState ==0 }"> selected="selected"</c:if>><fmt:message key="memberman.customman.canceled" /></option>
								</select></td>
						
							<td width="80" align="right"><fmt:message key="orderman.list.paystate" />:</td>
							<td><select name="orderSearch.orderPayState" class="act_zt"><option
										value=""><fmt:message key="orderman.list.all" /></option>
									<option value="1"
										<c:if test="${orderSearch.orderPayState ==1 }"> selected="selected"</c:if>><fmt:message key="orderman.list.payed" /></option>
									<option value="0"
										<c:if test="${orderSearch.orderPayState ==0 }"> selected="selected"</c:if>><fmt:message key="orderman.list.nopay" /></option>
							</select></td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="orderman.list.ordertime" />:</td>
							<td width="154"><input name="orderSearch.startTime"
								type="text" value="${orderSearch.startTime }" class="dateinput"
								readonly="readonly" style="width: 60px;"/>&nbsp;<fmt:message key="common.html.hr" />&nbsp;<input id="endTime" name="orderSearch.endTime"
								type="text" value="${orderSearch.endTime }" class="dateinput"
								readonly="readonly"  style="width: 60px;"/>
							</td>
							<td width="80" align="center"></td>
							<td width="154"></td>
							
							<td>&nbsp;</td>
							<td><input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" /></td>
						</tr>
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4><fmt:message key="orderConfirm.list.jsp.oreder.confirmation.record" /></h4>
				</div>
			</div>
			<div id="serDiv" class="active" >
				<div class="dialog_overlay"></div>
				<div class="dialog_win" style="background: none repeat scroll 0 0 #F1F5FE;">
					<h2>
						<span class="dialog_close">×</span>
					</h2>
					<div class="tc_tab">
						<table width="100%" border="0">
							<tr>
								<td width="40%" align="right"><fmt:message key="trademan.paylog.bankflownum" />:</td>
								<td><input code="" bankNumber="" id="bankNumber" type="text" class="time_in" maxlength="32" />
								</td>
							</tr>
							<tr>
								<td width="40%" align="right">&nbsp;</td>
								<td><input id="serFinishBtn" type="button" value="<fmt:message key="common.btn.ok" />"
									class="diw_but_tc" /></td>
							</tr>
						</table>
					</div>
				</div>

			</div>

			<div class="act_dd">
				<grid:grid from="webpage">
					<c:forEach items="${webpage.result}" var="item">
						<tr class="title_ht">
							<td width="35" rowspan="2"
								style="border-right: #dddddd 1px solid;"></td>
							<td colspan="2"><fmt:message key="orderman.list.ddh" />:${item.orderInfo.code }</td>
							<td colspan="2"><fmt:message key="orderman.list.ordertime" />:${item.orderInfo.orderDate }</td>
							<td colspan="2"><fmt:message key="accountinformation.autelid.name" />:${item.orderInfo.customerName }</td>
							<td width="20%"><fmt:message key="orderman.orderconfirm.ddze" />:${item.orderInfo.orderMoney }</td>

						</tr>
						<tr class="title_ht">
							<td colspan="2"><fmt:message key="trademan.paylog.bankflownum" />:&nbsp;${item.orderInfo.bankNumber }</td>
							<td><fmt:message key="orderman.list.paystate" />:<c:if test="${item.orderInfo.orderPayState == 0 }"><fmt:message key="orderman.list.nopay" /></c:if>
								<c:if test="${item.orderInfo.orderPayState == 1 }"><fmt:message key="orderman.list.payed" /></c:if>
							</td>
							<td><fmt:message key="orderman.list.getmoneyconfirmstate" />:<c:if
									test="${item.orderInfo.recConfirmState == 0 }"><fmt:message key="orderman.list.noconfirm" /></c:if> <c:if
									test="${item.orderInfo.recConfirmState == 1 }"><fmt:message key="orderman.list.confirmed" /></c:if>
							</td>
							<td colspan="2"><fmt:message key="memberman.customman.orderstate" />:<c:if
									test="${item.orderInfo.orderState ==1 }"> <fmt:message key="memberman.customman.valid" /></c:if> <c:if
									test="${item.orderInfo.orderState ==0 }"> <fmt:message key="memberman.customman.canceled" /></c:if></td>
							<td width="20%"><fmt:message key="orderman.list.dealstate" />:<c:if
									test="${item.orderInfo.processState ==1 }"> <fmt:message key="orderman.list.dealed" /></c:if> <c:if
									test="${item.orderInfo.processState ==0 }"> <fmt:message key="orderman.list.nodeal" /></c:if></td>
						</tr>
						<c:set var="status" value="1"></c:set>
						<c:forEach items="${item.details}" var="detail" varStatus="v">
							<tr>
								<td></td>
								<td colspan="2"><fmt:message key="memberman.complaintman.productserial" />:${detail.productSerialNo }</td>
								<td colspan="2"><fmt:message key="marketman.minusalenit" />:${detail.saleName }&nbsp; <c:if
										test="${detail.consumeType ==0 }">(<fmt:message key="orderman.list.gm" />)</c:if> <c:if
										test="${detail.consumeType ==1 }">(<fmt:message key="orderman.list.xf" />)</c:if></td>
								<td colspan="2"><span style="display: none;"></span></td>
								<c:if test="${status=='1' }">
									<td rowspan="${fn:length(item.details)}" class="caoz"
										style="border-left: #dddddd 1px solid;"><c:if
											test="${item.orderInfo.recConfirmState == 0  && item.orderInfo.bankNumber != '' && item.orderInfo.bankNumber != null}">
											<a href="orderConfirm.do?orderCodes=${item.orderInfo.code}"><fmt:message key="orderman.orderconfirm.getmoney" /></a>
										</c:if> <c:if test="${item.orderInfo.recConfirmState == 1 || item.orderInfo.bankNumber =='' || item.orderInfo.bankNumber == null}">
											<a href="###" style="color: gray;"><fmt:message key="orderman.orderconfirm.getmoney" /></a>
										</c:if>&nbsp; <c:if test="${item.orderInfo.bankNumber != null && item.orderInfo.orderPayState ==1 && item.orderInfo.orderState ==1}">
											<a href="###" code="${item.orderInfo.code }"
												bankNumber="${item.orderInfo.bankNumber }" action="update"
												id="sera${v.index }"><fmt:message key="orderman.orderconfirm.modflownum" /></a>
										</c:if> <c:if test="${item.orderInfo.bankNumber == null && item.orderInfo.orderPayState ==1 && item.orderInfo.orderState ==1}">
											<a href="###" code="${item.orderInfo.code }" action="add"
												id="ser${v.index }"><fmt:message key="orderman.orderconfirm.addflownum" /></a>
										</c:if></td>
								</c:if>
								<c:set var="status" value="0"></c:set>
							</tr>
						</c:forEach>
					</c:forEach>
				</grid:grid>
			</div>


		</div>
	</form>
</body>
<script type="text/javascript">
	var serSelect = new selfOpenDialog({
		id : "serDiv",
		btnId : "ser"
	});
</script>
</html>
