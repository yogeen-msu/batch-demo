<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="grid">
	<grid:grid from="webpage" ajax="yes">
		<grid:header>
			<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
			<grid:cell width="200px"><fmt:message key="common.list.code" /></grid:cell>
			<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
			<grid:cell width="100px"></grid:cell>
		</grid:header>
	
		<grid:body item="item">
			<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
			<grid:cell>${item.code} </grid:cell>
			<grid:cell>${item.name}</grid:cell>
			<grid:cell>
				<input type="button" class="selectPdTypeItem" value=<fmt:message key="common.openselect.choose" /> selectCode="${item.code}" selectName="${item.name}" />
			</grid:cell>
		</grid:body>
	</grid:grid>
</div>
