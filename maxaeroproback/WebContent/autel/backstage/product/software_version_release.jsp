<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="product.softwareversion.pubt" /></title>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/product/software_version_release.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
</head>

<body>
<form action="toReleaseSoftwareversionNew.do" method="post">
<input type="hidden" name="productSoftwareConfig.parentCode" value="${productSoftwareConfig.parentCode }">
<input type="hidden" name="productType.code" value="${productType.code}">
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="product.rootName" /> &gt; <fmt:message key="product.producttypevo" /> &gt; <fmt:message key="product.softwareversion.pubt" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="product.softwareversion.pubt" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="154">${productType.name}</td>
						<td width="80" align="right"><fmt:message key="product.virtualdir" />:</td>
						<td width="154">${productSoftwareConfigParent.name}</td>
						<td width="80" align="right"></td>
						<td></td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="product.software.name" />:</td>
						<td width="154">
							<input name="search.name" value="${search.name }" type="text" class="time_in" />
						</td>
						<td width="111" align="right">
							<fmt:message key="product.software.version.publishstate" />:
						</td>
						<td width="154">
							<select class="act_zt_tc" name="search.releaseState">
								<option value="">-- <fmt:message key="common.sel.choose" /> --</option>
								<option value="0" <c:if test="${search.releaseState != null && search.releaseState == 0 }"> selected="selected" </c:if>><fmt:message key="product.software.version.publishstate.no" /></option>
								<option value="1" <c:if test="${search.releaseState != null && search.releaseState == 1 }"> selected="selected" </c:if>><fmt:message key="product.software.version.publishstate.yes" /></option>
							</select>
						</td>
						<td width="160" align="right"><input type="submit"
							value="<fmt:message key="common.btn.search" />" class="search_but" />
						</td>
						<td width="80" align="right"></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span>
					<fmt:message key="product.software" />
				</h4>
			</div>
		</div>
		<div class="grid">
		
		<grid:grid from="webpage">
			<tr class="title">
					<td width="10%"><input type="checkbox" name="chkAll"/></td>
					<td width="20%"><fmt:message key="common.list.code" /></td>
					<td width="20%"><fmt:message key="product.software.name" /></td>
					<td width="20%"><fmt:message key="syssetting.toolman.newversion" /></td>
					<td width="20%"><fmt:message key="product.software.version.publishstate" /></td>
					<td width="10%"><fmt:message key="common.list.operation" /></td>
			</tr>
		
			<grid:body item="item" >
				<td><input type="checkbox" name="chk" value="${item.versionCode }"/></td>
					<td>${item.versionCode }</td>
					<td><a href="toSoftwareVersion2.do?softwareType.code=${item.softwareTypeCode }&returnPage=${page}" target='_blank'>${item.name }</a></td>
					<td>${item.versionName }</td>
					<td><c:if test="${item.releaseState == null || item.releaseState == 0}"><fmt:message key="product.software.version.publishstate.no" /></c:if><c:if test="${item.releaseState != null && item.releaseState == 1}"><fmt:message key="product.software.version.publishstate.yes" /></c:if></td>
					<td class="caoz">
					<input name="relealseBtn" type="button" code="${item.versionCode }" id="relealseBtn_${item.versionCode }"
						<c:if test="${item.releaseState == null || item.releaseState == 0}">
							 value="<fmt:message key="cententman.messageman.publish" />" state="1"
						</c:if> 
						<c:if test="${item.releaseState != null && item.releaseState == 1}">
							value="<fmt:message key="product.software.publish.no" />" state="0" 
						</c:if> class="search_but" />
					</td>
			</grid:body>
				<tr>
						<td colspan="6"><span style="float: left;display: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chkAlls"/>&nbsp;<fmt:message key="common.list.all" /></span>
							<span style="float: right;">
							<input id="releaseBtn" type="button"
								value="<fmt:message key="cententman.messageman.publish" />" state="1" class="search_but" />
								&nbsp;&nbsp;
							<input id="notReleaseBtn" type="button"
									value="<fmt:message key="product.software.publish.no" />" state="0" class="search_but" />
													&nbsp;&nbsp;&nbsp;&nbsp;
									</span>
						</td>
					</tr>
		</grid:grid>
		
		<center>
			<input
				type="button" value="<fmt:message key="common.btn.return" />"
				onclick="jump('toSoftwareType.do?productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}')"
				class="diw_but" />
			</center>
		</div>
	</div>
</form>
</body>
</html>
