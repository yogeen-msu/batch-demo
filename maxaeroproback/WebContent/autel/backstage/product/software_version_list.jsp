<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="product.softwareversion" /></title>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="../../autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="../../autel/js/product/software_version_list.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
</head>

<body>
		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			<fmt:message key="product.rootName" />
			&gt;
			<fmt:message key="product.producttypevo" />
			&gt;
			<fmt:message key="product.softwares" />
			&gt;
			<fmt:message key="product.softwareversion" />
		</div>
		<div class="right_tab">
		<form action="toSoftwareVersion.do" method="post">
		
		   <input type="hidden" id="returnPage" name="returnPage" value="${returnPage}"/>
		
			<input type="hidden" name="versionPageVo.softwareTypeCode"
				value="${softwareType.code }" /> <input type="hidden" id="typeCode" 
				name="softwareType.code" value="${softwareType.code }" /> <input
				type="hidden" name="softwareVersion.softwareTypeCode"
				value="${softwareType.code }" />
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;
					<fmt:message key="product.softwareversion" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message
									key="product.producttypevo" />:</td>
							<td width="154">${productType.name}</td>
							<td width="80" align="right"><fmt:message
									key="product.softwares" />:</td>
							<td width="154">${softwareType.name}</td>
							<td width="80" align="right"></td>
							<td></td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message
									key="product.softwareversion" />:</td>
							<td width="154"><input name="versionPageVo.versionName"
								type="text" class="time_in"
								value="<c:if test="${versionPageVo.versionName == null }">V</c:if><c:if test="${versionPageVo.versionName != null }">${versionPageVo.versionName}</c:if>" /></td>
							<td width="160" align="right"><input type="submit"
								value="<fmt:message key="common.btn.search" />"
								class="search_but" />
							</td>
							<td width="154"></td>
							<td width="80" align="right"></td>
							<td></td>
						</tr>
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4>
						<span>
						<input name="" type="button" value="<fmt:message key="common.btn.return" />"
						onclick="jump('toSoftwareType.do?productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}&page=${returnPage}')"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp;<input name="" type="button"
							onclick="jump('toAddSoftwareVersion.do?productType.name=${productType.name}&productType.code=${productType.code}&softwareType.code=${softwareType.code}&softwareType.name=${softwareType.name }&returnPage=${returnPage }')"
							value="<fmt:message key="product.softwareversion.addSt" />"
							class="search_but" /> </span>
						<fmt:message key="common.btn.search" />
						<fmt:message key="common.list.result" />
					</h4>
				</div>
			</div>
		</form>
		<div id="versionDiv" class="active">
				<div class="dialog_overlay"></div>
				<div class="dialog_win" style="width: 700px;">
					<h2>
						<span class="dialog_close">×</span>
					</h2>
					<div class="tc_tab">
						<table width="100%" border="0">

							<input type="hidden" id="versionCode" value="" />
							<input type="hidden" id="softwarePackCode" value="" />

							<tr>
								<td width="30%" align="right"><fmt:message
										key="product.softwareversion.name" /> :&nbsp;</td>
								<td><input name="" type="text" class="time_in_tc"
									id="versionName" maxlength="25" />
								</td>
							</tr>
							<tr>
								<td width="30%" align="right"><fmt:message
										key="product.softwareversion.basepack.path" />:&nbsp;</td>
								<td><input id="downloadPath" type="text" class="time_in_tc" style="width:500px"
									 />
								</td>
							</tr>
							<tr>
								<td width="40%" align="right">&nbsp;</td>
								<td><input id="versionFinishBtn" type="button"
									value="<fmt:message key="common.btn.ok" />" class="diw_but_tc" />
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		<form action="updateLanguagePack.do" id="languagePackForm"
			method="post">
			  <input type="hidden" id="returnPage" name="returnPage" value="${returnPage}"/>
			<div id="languageDiv" class="active">
				<div class="dialog_overlay"></div>
				<div class="dialog_win">
					<h2>
						<span class="dialog_close">×</span>
					</h2>
					<div class="tc_tab">
						<table width="100%" border="0">
							<input type="hidden" name="code" value="${softwareType.code }" />
							<input type="hidden" name="languagePack.code"
								id="languagePackCode" />
							<tr>
								<td width="40%" align="right"><fmt:message
										key="memberman.complaintman.language" />:</td>
								<td><select name="languagePack.languageTypeCode"
									id="language" class="act_zt_tc">
										<c:forEach items="${ languages}" var="language">
											<option value="${language.code }">${language.name }</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td width="40%" align="right"><fmt:message
										key="memberman.complaintman.languagepackloction" />:</td>
								<td><input name="languagePack.downloadPath" type="text" style="width: 400px;"
									class="time_in_tc" id="languageDownloadPath"  maxlength="200" />
								</td>
							</tr>
							<tr>
								<td width="40%" align="right"><fmt:message
										key="memberman.complaintman.testcardocloction" />:</td>
								<td><input name="languagePack.testPath" type="text" style="width: 400px;"
									class="time_in_tc" id="testPath"  maxlength="200" /></td>
							</tr>
							<tr>
								<td width="40%" align="right"><fmt:message
										key="product.softwareversion.sitelanguage.upgradenotice" />:</td>
								<td><input name="languagePack.title" type="text" style="width: 400px;"
									class="time_in_tc" id="title"  maxlength="200" /></td>
							</tr>
							<tr>
								<td width="40%" align="right"><fmt:message
										key="product.softwareversion.desc" />:</td>
								<td><textarea name="languagePack.memo" class="banb_tc"
										style="width: 400px; height: 200px;" class="banb_tc" id="memo"></textarea></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input id="lianguageFinishBtn" type="button"
									value="<fmt:message key="common.btn.ok" />" class="diw_but_tc" />
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

		</form>
		
		
		<form action="addLanguagePack.do" id="addLanguagePackForm" method="post"
			enctype="multipart/form-data">
			<input type="hidden" name="code" value="${softwareType.code }" />
			 <input type="hidden" id="returnPage" name="returnPage" value="${returnPage}"/>
			<div id="addLanguageDiv" class="active">
				<div class="dialog_overlay"></div>
				<div class="dialog_win">
					<h2>
						<span class="dialog_close">×</span>
					</h2>
					<div class="tc_tab">
						<table width="100%" border="0">
							<input type="hidden" name="languagePack.softwareVersionCode" id="addVersionCode" />
							<tr>
								<td width="40%" align="right"><fmt:message
										key="memberman.complaintman.language" />:</td>
								<td><select name="languagePack.languageTypeCode" id="addLanguage"
									class="act_zt_tc">
								</select></td>
							</tr>
							<tr>
								<td width="40%" align="right"><fmt:message
										key="memberman.complaintman.languagepackloction" />:</td>
								<td><input name="languagePack.downloadPath" type="text" style="width: 400px;" class="time_in_tc"
									id="languageDownloadPathAdd"  maxlength="200" /></td>
							</tr>
							<tr>
								<td width="40%" align="right"><fmt:message
										key="memberman.complaintman.testcardocloction" />:</td>
								<td>
								<input name="languagePack.testPath" type="text" style="width: 400px;"
									class="time_in_tc" id="testPathAdd" maxlength="200"  /></td>
							</tr>
							<tr>
								<td width="40%" align="right"><fmt:message
										key="product.softwareversion.sitelanguage.upgradenotice" />:</td>
								<td><input name="languagePack.title" type="text" class="time_in_tc"
									style="width: 400px; " id="titleAdd"  maxlength="200" /></td>
							</tr>
							<tr>
								<td width="40%" align="right"><fmt:message
										key="product.softwareversion.desc" />:</td>
								<td><textarea name="languagePack.memo" class="banb_tc"
										style="width: 400px; height: 200px;" id="memoAdd"></textarea></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input id="addLanguageFinishBtn" type="button"
									value="<fmt:message key="common.btn.ok" />" class="diw_but_tc" />
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</form>
		<div class="act_dd"  style="padding-right:0px; ">
			<grid:grid from="webpage">
				<table style="table-layout:fixed;word-wrap:break-word;">
					<c:forEach items="${webpage.result }" var="item" varStatus="result">
						<tr class="title">
							<td width="10%">
							<img width="9" showCode="${item.softwareVersion.code}" name="showLanguageFlag_${result.index }" id="showLanguageFlag_${result.index }" height="15" src="../../adminthemes/default/images/leg_11.gif" name="m">
							&nbsp;&nbsp;<input type="checkbox" name="chk" value="${item.softwareVersion.code}">&nbsp;&nbsp;<fmt:message key="product.software.carlogo.version" />:</td>
							<td width="10%">${item.softwareVersion.versionName}</td>
							<td width="20%"><fmt:message
									key="product.softwareversion.basepack.path" />:</td>
							<td width="20%">${item.softwareVersion.softPackPath}</td>
							<td width="10%"><fmt:message
									key="product.softwareversion.ispublish" />:</td>
							<td width="13%"><input name="a1${result.index }"
								type="radio" value="1"
								versionCode="${item.softwareVersion.code}"
								<c:if test="${item.softwareVersion.releaseState==1}"> checked="checked" </c:if> />&nbsp;<fmt:message
									key="product.softwareversion.ispublish.yes" />&nbsp;&nbsp;&nbsp;&nbsp;<input
								name="a1${result.index }"
								versionCode="${item.softwareVersion.code}" type="radio"
								value="0"
								<c:if test="${item.softwareVersion.releaseState==0}"> checked="checked" </c:if> />&nbsp;<fmt:message
									key="product.softwareversion.ispublish.no" /></td>
							<td width="22%"><a name="addSoftwareLanguage"
								versionCode="${item.softwareVersion.code }"
								id="addLanguage${result.index }"><fmt:message
										key="common.btn.addlanguage" />
							</a>
							
							&nbsp;&nbsp;<a name="delSoftwareVersion"
								versionCode="${item.softwareVersion.code }"
								typeCode="${softwareType.code}"><fmt:message
										key="common.list.del" /> </a> &nbsp;&nbsp;<a
								id="version${result.index }" code="${item.softwareVersion.code}"
								softwarePackCode="${item.softwareVersion.softwarePackCode}"
								versionName="${item.softwareVersion.versionName}"
								softPackPath="${item.softwareVersion.softPackPath}" href="###"><fmt:message
										key="common.list.mod" /> </a>
										&nbsp;&nbsp;<%-- <a href="http://192.168.1.2:8089/${item.softwareVersion.softPackPath}">下载 </a> --%></td>

						</tr>
						<c:forEach items="${item.languagePacks }" var="languagePack"
							varStatus="pack">
							<tr style="display:none" id="${item.softwareVersion.code}">
								<td><fmt:message key="product.softwareversion.sitelanguage" />:</td>
								<td>${languagePack.languageName }</td>
								<td><fmt:message key="product.softwareversion.sitelanguage" />
									<fmt:message key="product.softwareversion.common.path" />:</td>
								<td>${languagePack.downloadPath }</td>
								<td><fmt:message
										key="memberman.complaintman.testcardocloction" />:</td>
								<td>${languagePack.testPath }</td>
								<td class="caoz"><a name="languagePack${result.index }"
									code="${languagePack.languagePackCode }"
									languageCode="${languagePack.languageTypeCode }"
									downloadPath="${languagePack.downloadPath }"
									title="${languagePack.title }"
									testPath="${languagePack.testPath }"
									memo="${languagePack.memo }" id="languagePack${pack.index }"
									href="###"><fmt:message key="common.list.mod" /> </a>&nbsp;&nbsp;&nbsp;<a
									name="delLanguagepack"
									languagePackCode="${languagePack.languagePackCode}"
									typeCode="${softwareType.code}" href="###"><fmt:message
											key="common.list.del" /> </a>
											
											<%-- <a href="http://192.168.1.2:8089/${languagePack.downloadPath}">下载 </a> --%>
											</td>

							</tr>
						</c:forEach>
					</c:forEach>
					<tr>
						<td colspan="7" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="search_but" id="batchDelBtn" value="<fmt:message key="cart.clearecart.name" /> ">
						<input type="button" class="search_but" id="batchDelPublishBtn" value="批量取消发布">
						</td>
					</tr>
				</table>
			</grid:grid>
			</div>

		</div>

</body>
<script type="text/javascript">
	var languageSelect = new selfOpenDialog({
		id : "languageDiv",
		btnId : "languagePack"
	});
	var versionDivSelect = new selfOpenDialog({
		id : "versionDiv",
		btnId : "version"
	});
	
	var addLanguageDivSelect = new selfOpenDialog({
		id : "addLanguageDiv",
		btnId : "addLanguage"
	});
	function jump(url){
		var e=document.createElement("a");
		e.href=url;
		document.body.appendChild(e);
		e.click();
	}
	
</script>
</html>