<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="product.software" /><fmt:message key="common.btn.search" /></title>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/product/software_type_list.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}

</script>
</head>

<body>
<form action="toSoftwareType.do" method="post">
<input type="hidden" name="productSoftwareConfig.parentCode" value="${productSoftwareConfig.parentCode}">
<input type="hidden" name="productType.code" value="${productType.code}">
<input type="hidden" name="productType.name" value="${productType.name}">

	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="product.rootName" /> &gt; <fmt:message key="product.producttypevo" /> &gt; <fmt:message key="product.software" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="product.softwares" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="200">${productType.name}</td>
						<td width="250"><fmt:message key="product.virtualdir" />:${productSoftwareConfigParent.name}</td>
						<td width="80" align="right"></td>
						<td></td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="product.software.name" />:</td>
						<td width="200"><input name="softwareType.name" value="${softwareType.name}" type="text" class="time_in" />
						</td>
						<td width="250"><input type="submit"
							value="<fmt:message key="common.btn.search" />" class="search_but" /></td>
						<td width="80" align="right"></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					<input name="" type="button" value="<fmt:message key="common.btn.return" />"
						onclick="jump('search.do')"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp;
					<%-- <input name="" type="button" value="<fmt:message key="product.software.addSoftware" />"
						onclick="jump('toAddSoftwareType.do?productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}')"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp; <input name=""
						type="button" value="<fmt:message key="product.software.version.publish" />"
						onclick="jump('toReleaseSoftwareversion.do?productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}')"
						class="search_but" /> </span><fmt:message key="product.software" /> 20150204注释for刘妍群--%>
						<input name="" type="button" value="<fmt:message key="product.software.addSoftware" />"
						onclick="jump('toAddSoftwareType.do?productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}')"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp; <input name=""
						type="button" value="<fmt:message key="product.software.version.publish" />"
						onclick="jump('toReleaseSoftwareversionNew.do?productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}&productType.code=${productType.code}')"
						class="search_but" /> </span><fmt:message key="product.software" />
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				
				<grid:header>
					<grid:cell><fmt:message key="product.software.carlogo" />(中文)</grid:cell>
					<grid:cell><fmt:message key="product.software.carlogo" />(非中文)</grid:cell>
					<grid:cell><fmt:message key="common.list.code" /></grid:cell>
					<grid:cell><fmt:message key="product.software.name" /></grid:cell>
					<grid:cell><fmt:message key="product.software.carlogo.path" /></grid:cell>
					<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				
				<grid:body item="item" >
					<td><img width="75" height="75" src="${item.carSignPath }"></td>
					<td><img width="75" height="75" src="${item.newCarSignPath }"></td>
					<td>${item.code }</td>
					<td><a href="toSoftwareVersion.do?softwareType.code=${item.code }&returnPage=${page}">${item.name }</a></td>
					<td style="word-wrap:break-word;word-break:break-all;">${item.carSignPath }<br/>${item.newCarSignPath }</td>
					<td class="caoz"><a href="toUpdataSoftwareType.do?softwareType.code=${item.code }&returnPage=${page}"><fmt:message key="common.list.mod" /></a>&nbsp;
				<%-- 	<a href="###" onclick="deleteSoftwareType(this,'${item.code }');"><fmt:message key="common.list.del" /></a> --%>
				        <a href="toProductName.do?softwareType.code=${item.code }&returnPage=${page}"><fmt:message key="software.type.language.name"/></a>
				        
				        <a href="refreshBySoftCode.do?softwareType.code=${item.code }&productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}">软件刷新</a>
				        
					</td>
				</grid:body>
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>
