<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>

<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<!-- 
<script type="text/javascript" src="../../autel/js/product/software_type_update.js"></script>
 -->
<form action="updateSoftwareType.do" class="validate" method="post" name="softwareForm" id="softwareForm">
		<input type="hidden" name="softwareType.code" value="${softwareType.code }"/>
		<input type="hidden" name="softwareType.proTypeCode" value="${softwareType.proTypeCode}"/>
		<input type="hidden" name="productType.name" value="${productType.name}"/>
		<input type="hidden" id="returnPage" name="returnPage" value="${returnPage}"/>
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="product.rootName" /> &gt; <fmt:message key="product.software" />&gt;<fmt:message key="product.software.modSoftware" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="product.software.modSoftware" />
			</h4>
			<div class="sear_table">
				<table class="form-table" width="100%" border="0" style="font-size: 12px;">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:&nbsp;&nbsp;</td>
						<td width="154">${productType.name }
						</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="product.virtualdir" />:&nbsp;&nbsp;</td>
						<td width="154">
							<select name="productSoftwareConfig.parentCode"  id="parentCode">
							<option style="padding-left:10px;" value="${productType.code}">${productType.name}</option>
							<c:forEach var="item" items="${productSoftwareConfigList}">
						 		<option style="padding-left:${(item.level+1)*20+10}px;" value="${item.code}" <c:if test="${productSoftwareConfig.parentCode==item.code}">selected</c:if>>
						 		${item.name}</option>
							</c:forEach>
							</select>
						</td>
					</tr>
					<tr height="5">
						<td width="111" align="right"><fmt:message key="product.software.name" />:&nbsp;&nbsp;</td>
						<td width="154"><input name="softwareType.name" id="typeName" value="${softwareType.name }" maxlength="32" dataType="string" isrequired="true"  type="text" class="time_in"/>
						</td>
					</tr>
					    
						<tr height="5">
						<td width="111" align="right"><fmt:message key="product.software.type" />:&nbsp;&nbsp;</td>
						<td width="154">
						<select name="softwareType.softType" id="softwareType.softType">
						<option value="1" <c:if test="${softwareType.softType==1}"> selected </c:if>><fmt:message key="product.software.type.pay" /></option>
						<option value="2" <c:if test="${softwareType.softType==2}"> selected </c:if>><fmt:message key="product.software.type.free" /></option>
						<option value="3" <c:if test="${softwareType.softType==3}"> selected </c:if>><fmt:message key="product.software.type.system" /></option>
						<option value="4" <c:if test="${softwareType.softType==4}"> selected </c:if>><fmt:message key="product.software.type.solidify" /></option>
						</select>
						</td>
					</tr>
					
					<tr height="5">
						<td width="111" align="right"><fmt:message key="product.software.carlogo.path" />(中文):&nbsp;&nbsp;</td>
						<td width="154"><input name="softwareType.carSignPath" id="carSignPath"  value="${softwareType.carSignPath }" maxlength="500"  type="text" class="time_in" style="width:450px;"  dataType="string" isrequired="true"/>&nbsp;&nbsp;
						</td>
					</tr>
					
					</tr>
					
						<tr height="5">
						<td width="111" align="right"><fmt:message key="product.software.carlogo.path" />(非中文):&nbsp;&nbsp;</td>
						<td width="154"><input name="softwareType.newCarSignPath" id="newCarSignPath" value="${softwareType.newCarSignPath }" maxlength="500" type="text" class="time_in" style="width:450px;" dataType="string" />&nbsp;&nbsp;
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					
				</table>
			</div>
		</div>
		<div style="padding-top: 20px; text-align: center;">
			<input type="submit"  id="finishBtn" value="<fmt:message key="common.btn.finish" />" class="diw_but" /><input
				type="button" value="<fmt:message key="common.btn.return" />" onclick="jump('toSoftwareType.do?productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}&page=${returnPage}')" class="diw_but"/>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("form.validate").validate();
</script>

<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>


