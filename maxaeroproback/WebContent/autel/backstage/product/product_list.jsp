<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<html>
<head>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/product/product.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">
	var action = "${action}";
	var productName = "";
	var code = "";
	if(action == "add" || action == "update"){
		productName = "${productType.name}";
			code = "${productType.code}";
	}
	
	if(productName != ""  && code != ""){
		updateLeftMenu(action,productName,code);
	}
	
	if(action == "add" || action == "update"){
		jump("search.do");
	}
	function jump(url){
		var e=document.createElement("a");
		e.href=url;
		document.body.appendChild(e);
		e.click();
	}
</script>


</head>
<body>
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="product.rootName" />&gt; <fmt:message key="sys.product.list" /> </div>
	<div class="right_tab">
		<form action="search.do" method="post" id="productForm" >
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="sys.product.list" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38" style="font-size: 12px;">
						<td width="111" align="right"><fmt:message key="product.producttypevo.name" />:</td>
						<td width="154"><input type="text" name="productTypeVo.productName" value="${productTypeVo.productName}" class="time_in"/>
						</td>
						<td width="160" align="right">
						<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />
							
						</td>
						<td width="154"></td>
						<td width="80" align="right"></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span><input type="button" value="<fmt:message key="product.producttypevo.add" />" id="addProductType"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp; 
						
							<input name="" type="button" value="刷新软件包"  onclick="location.href='refreshProduct.do'" class="search_but refreshtpack" />
						 </span><fmt:message key="sys.product.list" />
				</h4>
			</div>
		</div>
		
		<div class="grid" id="productTypeTable">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell><fmt:message key="common.list.code" /></grid:cell>
					<grid:cell><fmt:message key="product.producttypevo.name" /></grid:cell>
					<grid:cell><fmt:message key="product.producttypevo.picture" /></grid:cell>
					<grid:cell><fmt:message key="product.producttypevo.serialPicPath" /></grid:cell>
					<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="productType"
					varStatus="productTypeStatus">
					<tr tName='${productType.name }' code="${productType.code }" picPath="${productType.picPath }">
						<td>${productType.code }</td>
						<td><a href="toSoftwareType.do?productSoftwareConfig.parentCode=${productType.code}">${productType.name }</a></td>
						<td style="word-wrap:break-word;word-break:break-all;">${productType.picPath}</td>
						<td><input type="button"
							name="serialPicPathBtn${productTypeStatus.index }" value="<fmt:message key="product.producttypevo.serialPicPathMan" />"
							class="search_but"/>
						</td>
						<td class="caoz"><a href="###" action="update"
							onclick="javascript:productType(this);"><fmt:message key="common.list.mod" /></a>&nbsp;&nbsp;<a href="###"
							onclick="delProductType(this,'${productType.code }');"><fmt:message key="common.list.del" /></a>
							
							&nbsp;&nbsp;<a href="###"
							onclick="location.href='refreshByProType.do?productType.code=${productType.code }'">软件刷新</a>
							
						</td>
					</tr>
				</c:forEach>
			</grid:grid>
		</div>
		</form>	
	</div>

	</body>	
	</html>
