<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/product/product_pic.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/artDialog.js?skin=default"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/plugins/iframeTools.js"></script>
<script type="text/javascript">
var languagePackArray = new Array();
Array.prototype.removePack = function(languageCode){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(languageCode == this[i].languageTypeCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};
Array.prototype.remove = function(language){
	var len = this.length;
	var index = -1;
	for(var i = 0 ; i < len ; i++ ){
		if(language.languageCode == this[i].languageCode){
			index = i;
			break;
		}
	}
	if(index > -1){
		this.splice(index, 1);
	}
};
$(document).ready(function(){
	
	var oWin = document.getElementById("win");
	var oLay = document.getElementById("overlay");	
	$("#addProductName").click(function()
	{
		var t=$("#language").find("option").length;
		if(t!=0){
			oLay.style.display = "block";
			oWin.style.display = "block";
		}
	});
	$("#close").click(function()
	{
		oLay.style.display = "none";
		oWin.style.display = "none";
		
	});
	
$("#finsh").click(function(){
		
		var languageCode = $("#language option:selected").val();
		var languageName = $("#language option:selected").text();
		if(languageCode == "" || languageName == ""){
			alert("请选择语言");
			return false;
		}
		var softwareName = $("#softwareName2").val();
		if(softwareName == ""){
			alert("请输入软件名称");
			return false;
		}
		
	    var languagePack = new LanguagePack(languageCode,languageName,softwareName);
	    languagePackArray.push(languagePack);
	    buildLanguagePackHtml(languagePack);
	    
	});
	
	
$("#sumbitBtn").click(function(){
	var prefix = "softwareNameList";
	var $form=document.createElement("form");;
	var code=$("#code").val();
	var html = "";
	var num=0;
	$("#productNameTable").find("#softwareNameTR").each(function(i){
		if($(this).attr("id")=="softwareNameTR"){
			var languageCode=$(this).attr("languageCode");
			var softwareName=$(this).attr("softwareName");
			html += "<input type='hidden' name='"+prefix+"["+i+"].languageCode"+"' value='"+languageCode+"'/>";
			html += "<input type='hidden' name='"+prefix+"["+i+"].softwareName"+"' value='"+softwareName+"'/>";
			html += "<input type='hidden' name='"+prefix+"["+i+"].softwareCode"+"' value='"+code+"'/>";
		    num++;	
		}
		 });
    if(num==0){
    	alert("请填写软件语言名称");
    	return;
    }
	
	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).append(html);
	$(tmpForm).attr("action","saveLanguageNameJson.do");
	tmpForm.appendTo(document.body).submit();
});
	
});

function LanguagePack(languageTypeCode,languageName,softwareName){
	this.languageTypeCode = languageTypeCode;
	this.languageName = languageName;
	this.softwareName = softwareName;
};

function removeTr(module){
	var $select = $("#win").find("select");
	var languageCode = $(module).attr("languageCode");
	var languageName = $(module).attr("languageName");
	$select.append("<option value='"+languageCode+"'>"+languageName+"</option>");
	$(module).parent("td").parent("tr").remove();
	languagePackArray.removePack(languageCode);
}

function batchAdd(){
	var langugagStr="";
	$("#productNameTable tr").each(function(i){
			if($(this).attr("id")=="softwareNameTR"){
				langugagStr+=$($(this).find("td")[0]).html()+","
			}
	});
	
	
	 var  url="batchAddLanguageName.do?langugagStr="+langugagStr;
	    art.dialog.open(url, {
	    title: '',
		lock:true,
		cancelVal:'Cancel',
		okVal:'Ok',
		width: 900, 
		height: 600,
		init: function () {
			var iframe = this.iframe.contentWindow;
			var top = art.dialog.top;
		},  
		ok: function () {
		var flag=false;
		this.iframe.contentWindow.$("#languageTable tr").each(function(i){
			if($(this).attr("id")!="mustShow"){
			  var languageCode=$(this).attr("languageCode");
		      var languageName= $($(this).find("td")[0]).html();
		      var softwareName=$($(this).find("input")[0]).val();
		      
		      if(softwareName==""){
		        alert('<fmt:message key="common.js.dataimperfect" />');
		        return false;
		      }else{
			      var languagePack = new LanguagePack(languageCode,languageName,softwareName);
		          languagePackArray.push(languagePack);
		          buildLanguagePackHtml(languagePack);
		          flag=true;
	          }
			
			}
		});
		if(flag!=true){
			return false ;
		}
		
		
        },
		cancel:function(){
		  this.close();
		}
		});

}


function buildLanguagePackHtml(languagePack){
	$("#win").find("select option:selected").remove();;
	
	var $tr = $("<tr id='softwareNameTR' languageCode='"+languagePack.languageTypeCode+"' softwareName='"+languagePack.softwareName+"'></tr>");
	var $languageTd = $("<td></td>");
	$languageTd.append(languagePack.languageName);
	
	var $softwareName = $("<td></td>");
	$softwareName.append(languagePack.softwareName);

	var $operTd = $("<td class='caoz'></td>");
	var $a = $("<a href='###' languageCode='"+languagePack.languageTypeCode+"' languageName='"+languagePack.languageName+"'><fmt:message key='common.js.delInfo' /></a>");
	
	$a.click(function(){
		var $select = $("#win").find("select");
		var languageCode = $(this).attr("languageCode");
		var languageName = $(this).attr("languageName");
		$select.append("<option value='"+languageCode+"'>"+languageName+"</option>");
		$(this).closest("tr").remove();
		languagePackArray.removePack(languageCode);
	});
	
	$operTd.append($a);
	
	$tr.append($languageTd).append($softwareName).append($operTd);
	
	$("#productNameTable").find("tr").last().before($tr);
	
	var oWin = $("#win");
	var oLay = $("#overlay");
	
	oLay.hide();
	oWin.hide();
	
}



function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
	
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="product.rootName" /> &gt; <fmt:message key="product.producttypevo" />&gt; 语言名称 </div>
	<form method='post' action="saveLanguageNameJson.do"  id="softwareNameForm">
	<input type="hidden" id="returnPage" name="returnPage" value="${returnPage}"/>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;语言名称
			</h4>
			<div class="sear_table">
				<table width="100%" border="0" class="form-table">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38" style="font-size: 12px;">
						<td width="111" align="right"><fmt:message key="product.software.name" />:</td>
						<td width="154">&nbsp;${softwareType.name }
						</td>
						<td width="154" align="right">
						</td>
						<td width="154"></td>
						<td width="80" align="right"></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<input type="hidden" id="code" value="${softwareType.code }">
			<div class="active">
				<h4>
				
				    <span><input type="button" onclick="javascript:batchAdd()" value="批量添加" id="batchAddProductName" class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp;  </span>
					<span><input type="button" value="<fmt:message key='common.btn.add'/>" id="addProductName" class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp;  </span><fmt:message key="software.type.name.info"/>
				</h4>
			</div>
		</div>
		<div class="grid">
			<table width="100%" border="0" id="productNameTable" bordercolor="#dddddd" style="font-size: 12px;">
				
				<tr class="title">
					<th width="50px;"><fmt:message key="memberman.complaintman.language" /></th>
					<th width="180px;"><fmt:message key="software.type.name.name"/></th>
					
					<th width="120px;"><fmt:message key="common.list.operation" /></th>
				</tr>
				
				<c:forEach items="${config }" var="config">
			<tr id="softwareNameTR" softwareName="${config.softwareName}" languageCode="${config.languageCode }" picId="${config.code }" class="NameTR">
				<td>${config.languageName}</td>
				<td id="softwareName">${config.softwareName}</td>
	
				<td id="listTD"  class="caoz">
					<a href="###" onclick="javacript:removeTr(this);" languageCode='${config.languageCode }' languageName='${config.languageName}'><fmt:message key="common.list.del" /></a>
				</td>
				
				
			</tr>
		</c:forEach>
		<tr>
			</table>
			<div style="padding-top:15px;text-align:center;">
			<input name="" id="sumbitBtn" type="button"
						value="<fmt:message key="common.btn.ok" />" class="diw_but" />
			
			<input
				type="button" value="<fmt:message key="common.btn.return" />"
				onclick="jump('toSoftwareType.do?productSoftwareConfig.parentCode=${productSoftwareConfig.parentCode}&page=${returnPage}')"
				class="diw_but" />
			</div>
		</div>
	</div>
	<div class="active">
					<div id="overlay"></div>
					<div id="win">
						<h2>
							<span id="close">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0">
								<tr>

									<td width="40%" align="right"><fmt:message key="memberman.complaintman.language" />:</td>
									<td><select name="language" id="language"
										class="act_zt_tc">
											<c:forEach items="${language}" var="language">
												<option value="${language.code }">${language.name }</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td width="40%" align="right"><fmt:message key="software.type.name.name" />:</td>
									<td><input id="softwareName2" type="text" class="time_in_tc" />
									</td>
								</tr>
								<tr>
									<td width="40%" align="right">&nbsp;</td>
									<td><input id="finsh" type="button" value="<fmt:message key="emption.add.jsp.button.save" />"
										class="diw_but_tc" />
									</td>
								</tr>
							</table>
						</div>
					</div>

					
				</div>
	
	</form>