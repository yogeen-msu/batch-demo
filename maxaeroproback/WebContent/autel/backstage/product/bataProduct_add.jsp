<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<form action="save.do" class="validate" method="post" name="bataForm" id="bataForm" >
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="product.rootName" /> &gt; <fmt:message key="product.test.platform" />&gt; <fmt:message key="product.test.bata.product.add" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="product.test.bata.product.add" />
			</h4>
			<div class="sear_table">
			 <input type="hidden" name="bataProduct.proCode" id="proCode" value=""/>
				<table class="form-table" width="100%" border="0" style="font-size: 12px;">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					
					<tr height="5">
					   
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:&nbsp;&nbsp;</td>
						<td width="154"><input name="bataProduct.serialNo" id="serialNo"   type="text" class="time_in" dataType="string" isrequired="true"/>
						</td>
					</tr>
					
				
				</table>
			</div>
		</div>
		<div style="padding-top: 20px; text-align: center;">
			<input type="button" onclick="checkSerialNo()" id="finishBtn" value="<fmt:message key="common.btn.finish" />" class="diw_but" /><input
				type="button" value="<fmt:message key="common.btn.return" />"
				onclick="jump('list.do')"
				class="diw_but" />
		</div>
	</div>
	<input type="hidden" name="errorMsg" id="errorMsg" value='<fmt:message key='system.error.name'/>'/>
	<input type="hidden" name="errorMsg1" id="errorMsg1" value='<fmt:message key='product.test.bata.error1'/>'/>
	<input type="hidden" name="errorMsg2" id="errorMsg2" value='<fmt:message key='product.test.bata.error2'/>'/>
	</form>
	
<script type="text/javascript">
	$("form.validate").validate();
</script>

<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}

//验证输入产品序列号是否有效
function checkSerialNo()
{
	var serialNo=$('#serialNo').val();
	$.ajax({
		url:'checkSerialNo.do?ajax=yes',
		type:"post",
		data:{"serialNo":serialNo},
		dataType:'JSON',
		async: false,
		success :function(data){
			var jsonData = eval(eval(data));
			var result = jsonData[0].result;
			if(result=="1"){
				alert($('#errorMsg1').val());
				return;
			}else if(result=="2"){
				alert($('#errorMsg2').val());
			}else{
				$('#proCode').val(result);
				$('#bataForm').submit();
			}
		},
		error :function(data){
			alert($('#errorMsg').val());
		}
	});
	
}
</script>

