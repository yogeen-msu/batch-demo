<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/product/product_pic.js.jsp?ajax=yes"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
<script type="text/javascript">
	var action = "${action}";
	var productName = "";
	var code = "";
	if(action == "add" || action == "update"){
		productName = "${productType.name}";
			code = "${productType.code}";
	}
	
	if(productName != ""  && code != ""){
		updateLeftMenu(action,productName,code);
	}
	

</script>

	
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="product.rootName" /> &gt; <fmt:message key="product.producttypevo" />&gt; <fmt:message key="product.producttypevo.serialPicPathMan" /> </div>
	<form method='post' id="picForm">
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="product.producttypevo.serialPicPathMan" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0" class="form-table">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38" style="font-size: 12px;">
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="154">&nbsp;${productType.name }
						</td>
						<td width="154" align="right">
						</td>
						<td width="154"></td>
						<td width="80" align="right"></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<input type="hidden" id="code" value="${productType.code }">
			<div class="active">
				<h4>
					<span><input type="button" value="<fmt:message key='common.btn.add.path' />" id="addPicTr"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp;  </span><fmt:message key="product.producttypevo.serialPicPath.info" />
				</h4>
			</div>
		</div>
		<div class="grid">
			<table width="100%" border="0" id="picTable" bordercolor="#dddddd" style="font-size: 12px;">
				
				<tr class="title">
					<th width="50px;"><fmt:message key="memberman.complaintman.language" /></th>
					<th width="180px;"><fmt:message key="product.producttypevo.serialPicPath" /></th>
					
					<th width="120px;"><fmt:message key="common.list.operation" /></th>
				</tr>
				
				<c:forEach items="${productTypeSerialPicVos }" var="pic">
			<tr languageCode="${pic.languageCode }" picId="${pic.picId }" picPath="${pic.picPath}" languageName="${pic.languageName}">
				<td>${pic.languageName}</td>
				<td>${pic.picPath}</td>
	
				<td class="caoz"><a href="###" action="update" onclick="productType(this);"><fmt:message key="common.list.mod" /></a>&nbsp;&nbsp;<a href="###" onclick="delProductPic(this,'${productType.code }','${pic.languageCode }');"><fmt:message key="common.list.del" /></a></td>
			</tr>
		</c:forEach>
		<tr>
			</table>
			<div style="padding-top:15px;text-align:center;">
			<input
				type="button" value="<fmt:message key="common.btn.return" />"
				onclick="jump('search.do')"
				class="diw_but" />
			</div>
		</div>
	</div>
	</form>
