<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script type="text/javascript">
var languagePackArray = new Array();
function Map(){
this.container = new Object();
}
Map.prototype.put = function(key, value){
this.container[key] = value;
}
Map.prototype.get = function(key){
return this.container[key];
}
function copyUrl(){
	var obj= $("#languageTable tr")[1];
	$($(obj).find("input")[0]).val();
	var value=$($(obj).find("input")[0]).val(); 
	var startStr="";
	var endStr="";
	var startStr2="";
	var endStr2="";
	if(value.indexOf("|")>-1){
	var str= new Array(); 
	str=value.split("|");  
	startStr=str[0].substring(0,str[0].lastIndexOf("\\"));
	endStr=str[0].substring(str[0].lastIndexOf("\\"));
	endStr=endStr.substring(endStr.lastIndexOf("."));      
	startStr2=str[1].substring(0,str[1].lastIndexOf("\\"));
	endStr2=str[1].substring(str[1].lastIndexOf("\\"));
	endStr2=endStr2.substring(endStr2.lastIndexOf(".")); 
	}else{
	startStr=value.substring(0,value.lastIndexOf("\\"));
	endStr=value.substring(value.lastIndexOf("\\"));
	endStr=endStr.substring(endStr.lastIndexOf("."));
	}
	if(startStr!="" && endStr!=""){
	$("#languageTable tr").each(function(i){
				if(i>1){
				var langageSimple=$(this).attr("langageSimple");
				if(value.indexOf("|")>-1){
				 var str=startStr+"\\"+langageSimple+endStr+"|"+startStr2+"\\"+langageSimple+endStr2;
				 $($(this).find("input")[0]).val(str);
				}else{
				 
				 var str=startStr+"\\"+langageSimple+endStr;
				 $($(this).find("input")[0]).val(str);
				 }
				}
	});
	}
	}

function copyLoction(){
	var obj= $("#languageTable tr")[1];
	var value=$($(obj).find("input")[1]).val(); 
	$("#languageTable tr").each(function(i){
				if(i>1){
				 $($(this).find("input")[1]).val(value);
				}
	});	
	}
	
function copyDesc(){
	var obj= $("#languageTable tr")[1];
	var value=$($(obj).find("textarea")[0]).val(); 
	$("#languageTable tr").each(function(i){
	if(i>1){
		$($(this).find("textarea")[0]).val(value);
	}
	});	
	}	



$(document).ready(function(){
	
    var languageArray = new Array();	
	var languageMap=new Map();
	languageMap.put("Chinese","cn");
	languageMap.put("English","en");
	languageMap.put("Japanese","jp");
	languageMap.put("Hong Kong","hk");
	languageMap.put("Spanish","es");
	languageMap.put("Portuguese","pt");
	languageMap.put("German","de");
	languageMap.put("French","fr");
	languageMap.put("Nederlands","nl");
	languageMap.put("Polish","pl");
	languageMap.put("Russian","ru");
	languageMap.put("Swedish","se");
	languageMap.put("Korean","kr");
	languageMap.put("Italiano","it");
	languageMap.put("Hungarian","hu"); //add 2015/10/16 增加匈牙利语
	languageMap.put("Czech","cz"); //add 2015/11/12 增加捷克语
	
	
	
$("input[type='checkbox'][name^='checkAll']").click(function(){
	if($(this).attr("checked")){
		$("input[type='checkbox'][name^='languageCheck']").attr("checked",$(this).attr("checked"));
		
		$("#languageTable tr").each(function(i){
		if($(this).attr("id")!="mustShow"){
		 $(this).remove();
		}
		});
		
		$("input[type='checkbox'][name^='languageCheck']").each(function(i){
		    
			var languageCode=$(this).attr("languageCode");
	  		var languageName=$(this).attr("languageName");
	  		var langageSimple=languageMap.get(languageName);
	  		var html='<tr langageSimple='+langageSimple+'  style="height: 35px;" id='+languageCode+' languageCode='+languageCode+' languageName='+languageName+'><td width="10%" align="center">'+languageName+'</td><td width="20%"><input name="" type="text" class="time_in_tc" maxlength="200" style="width: 200px;" id="downloadPath"/></td>'+
	  		         '<td width="20%"><input name="" type="text" class="time_in_tc" maxlength="200" style="width: 200px;" id="downloadPath"/></td><td width="50%"><textarea name=""  class="time_in_tc"  style="width: 400px;height:20px" id="downloadPath"></textarea></td></tr>' ;
	  		$('#languageTable').append(html);
		});
		
		
	}else{
		$("#languageTable tr").each(function(i){
		if($(this).attr("id")!="mustShow"){
		 $(this).remove();
		}
		});
		$("input[type='checkbox'][name^='languageCheck']").removeAttr("checked");
	}
});

$("input[type='checkbox'][name^='languageCheck']").click(function(){
	if($(this).attr("checked")){
	  var languageCode=$(this).attr("languageCode");
	  var languageName=$(this).attr("languageName");
	  var langageSimple=languageMap.get(languageName);
	  var html='<tr langageSimple='+langageSimple+'  style="height: 35px;" id='+languageCode+' languageCode='+languageCode+' languageName='+languageName+'><td width="10%" align="center">'+languageName+'</td><td width="20%"><input name="" type="text" class="time_in_tc" maxlength="200" style="width: 200px;" id="downloadPath"/></td>'+
        '<td width="20%"><input name="" type="text" class="time_in_tc" maxlength="200" style="width: 200px;" id="downloadPath"/></td><td width="50%"><textarea name=""  class="time_in_tc"  style="width: 400px;height:20px" id="downloadPath"></textarea></td></tr>' ;
	  
	  $('#languageTable').append(html);
	}else{
	  var languageCode=$(this).attr("languageCode");
	  $('#'+languageCode).remove();
	
	}

});


});

</script>
<div>					
						<div class="grid" style="padding-left: 2px;padding-right: 2px;padding-top:8px">
							<table width="100%" border="0">
								<tr id="mustShow">

									<td  width="70%" >
									<c:forEach items="${ languages}" var="language">
									<input  type="checkbox"   name="languageCheck_${language.code }" languageCode='${language.code }' languageName='${language.name }' value="${language.code}"> ${language.name} 
									</c:forEach>
									
									
									
									</td>
									<td><span style="float:center"><input  type="checkbox" name="checkAll" >全选</span></td>
									
								</tr>
								</table>
								<table width="100%" border="0" id="languageTable">
								<tr id="mustShow">
								<td width="10%" align="center"><fmt:message
											key="memberman.complaintman.language" /></td>
								<td width="20%" align="center"><fmt:message
											key="memberman.complaintman.languagepackloction" />
											
											<span><a style="color: #d71200;" href="javascript:copyUrl();">复制</a></span>
											</td>
								<td width="20%" align="center"><fmt:message
											key="memberman.complaintman.testcardocloction" />
											<span><a style="color: #d71200;" href="javascript:copyLoction();">复制</a></span>
								</td>
								
								<td width="50%" align="center"><fmt:message
											key="product.softwareversion.desc" />
											<span><a style="color: #d71200;" href="javascript:copyDesc();">复制</a></span>
								</td>
								
								</tr>
								
								
							</table>
						</div>
					</div>
</html>