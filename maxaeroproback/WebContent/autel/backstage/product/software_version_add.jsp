<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="product.softwareversion.add" /></title>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/product/software_version.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/artDialog.js?skin=default"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/plugins/iframeTools.js"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
<script>
	window.onload = function() {
		var oWin = document.getElementById("win");
		var oLay = document.getElementById("overlay");
		var oClose = document.getElementById("close");
		var oH2 = oWin.getElementsByTagName("h2")[0];
		var bDrag = false;
		var disX = disY = 0;
		oClose.onclick = function() {
			oLay.style.display = "none";
			oWin.style.display = "none";

		};
		oClose.onmousedown = function(event) {
			(event || window.event).cancelBubble = true;
		};
		oH2.onmousedown = function(event) {
			var event = event || window.event;
			bDrag = true;
			disX = event.clientX - oWin.offsetLeft;
			disY = event.clientY - oWin.offsetTop;
			this.setCapture && this.setCapture();
			return false;
		};
		document.onmousemove = function(event) {
			if (!bDrag)
				return;
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
			var maxT = document.documentElement.clientHeight
					- oWin.offsetHeight;
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL;
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;

			oWin.style.marginTop = oWin.style.marginLeft = 0;
			oWin.style.left = iL + "px";
			oWin.style.top = iT + "px";
			return false;
		};
		document.onmouseup = window.onblur = oH2.onlosecapture = function() {
			bDrag = false;
			oH2.releaseCapture && oH2.releaseCapture();
		};
		
		
		
		
		
		
		
		
		
		
		
		
		/* var oWin2 = document.getElementById("win2");
		var oLay2 = document.getElementById("overlay2");
		var oClose2 = document.getElementById("close2");
		var oH22 = oWin2.getElementsByTagName("h2")[0];
		var bDrag = false;
		var disX = disY = 0;
		oClose2.onclick = function() {
			oLay2.style.display = "none";
			oWin2.style.display = "none";

		};
		oClose2.onmousedown = function(event) {
			(event || window.event).cancelBubble = true;
		};
		oH22.onmousedown = function(event) {
			var event = event || window.event;
			bDrag = true;
			disX = event.clientX - oWin2.offsetLeft;
			disY = event.clientY - oWin2.offsetTop;
			this.setCapture && this.setCapture();
			return false;
		};
		document.onmousemove = function(event) {
			if (!bDrag)
				return;
			var event = event || window.event;
			var iL = event.clientX - disX;
			var iT = event.clientY - disY;
			var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
			var maxT = document.documentElement.clientHeight
					- oWin.offsetHeight;
			iL = iL < 0 ? 0 : iL;
			iL = iL > maxL ? maxL : iL;
			iT = iT < 0 ? 0 : iT;
			iT = iT > maxT ? maxT : iT;

			oWin2.style.marginTop = oWin2.style.marginLeft = 0;
			oWin2.style.left = iL + "px";
			oWin2.style.top = iT + "px";
			return false;
		};
		document.onmouseup = window.onblur = oH22.onlosecapture = function() {
			bDrag = false;
			oH22.releaseCapture && oH22.releaseCapture();
		};
		
		
		 */
		
		
	};
</script>
</head>

<body>
	<form action="addSoftwareVersion.do" method="post"
		id="softwareVersionForm">
		<input type="hidden" name="softwareVersion.softwareTypeCode"
			value="${softwareType.code }"> <input type="hidden"
			name="softwareType.code" value="${softwareType.code }">

         <input type="hidden" id="returnPage" name="returnPage" value="${returnPage}"/>
		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			<fmt:message key="product.rootName" />
			&gt;
			<fmt:message key="product.producttypevo" />
			&gt;
			<fmt:message key="product.software" />
			&gt;
			<fmt:message key="product.softwareversion.add" />
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;
					<fmt:message key="product.softwareversion.add" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message
									key="product.producttypevo" />:</td>
							<td width="154">${productType.name}</td>
							<td width="80" align="right"><fmt:message
									key="product.softwares" />:</td>
							<td width="154">${softwareType.name }</td>
							<td width="80" align="right"></td>
							<td></td>
						</tr>
						<tr height="38">
							<td width="120" align="right"><fmt:message
									key="product.softwareversion.name" />:</td>
							<td width="154"><input type="text" id="versionName"
								class="time_in" name="softwareVersion.versionName" value="V"
								maxlength="25" /></td>
							<td width="300" align="right"><fmt:message
									key="memberman.complaintman.basepacklocation" />:</td>
							<td width="154"><input type="text" id="softwarePackPath" maxlength="200"
								class="time_in" style="width:450px;" name="softwarePack.downloadPath"  />
							</td>
							<td width="" align="right"></td>
							<td></td>
						</tr>
						
					   <tr>
							<td></td>
							<td></td>
							<td></td>
							<td>
								<font color="red">
								<fmt:message key="software.version.addWarn1" /><br>
									<fmt:message key="software.version.addWarn2" /><br>
									&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="software.version.addWarn3" />
								</font>
							</td>
							<td></td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<div id="overlay"></div>
					<div id="win">
						<h2>
							<span id="close">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0">
								<tr>

									<td width="40%" align="right"><fmt:message
											key="memberman.complaintman.language" />:</td>
									<td><select name="language" id="language"
										class="act_zt_tc">
											<c:forEach items="${ languages}" var="language">
												<option value="${language.code }">${language.name }</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>

									<td width="40%" align="right"><fmt:message
											key="memberman.complaintman.languagepackloction" />:</td>
									<td>
										<div style="height:10px"></div>
									<font color="red">
								<fmt:message key="software.version.addWarn4" /><br>
									<fmt:message key="software.version.addWarn5" /><br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="software.version.addWarn6" />
								</font>
									<div style="height:6px"></div>
									<input name="" type="text" class="time_in_tc" maxlength="200"
										style="width: 400px;" id="downloadPath"/>
										<div style="height:10px"></div>
									</td>
									
								</tr>
								
								<tr>
									<td width="40%" align="right"><fmt:message
											key="memberman.complaintman.testcardocloction" />:</td>
									<td><input name="" type="text" class="time_in_tc" maxlength="200"
										style="width: 400px;" id="testPath"  />
									</td>
								</tr>
								
								<tr>

									<td width="40%" align="right"><fmt:message
											key="product.softwareversion.sitelanguage.upgradenotice" />:</td>
									<td><input name="" type="text" class="time_in_tc" maxlength="200"
										style="width: 400px;" id="title" />
									</td>
								</tr>
								<tr>

									<td width="40%" align="right"><fmt:message
											key="product.softwareversion.desc" />:</td>
									<td><textarea name="" cols="" rows="" class="banb_tc"
											style="width: 400px; height: 200px;" id="memo"></textarea>
									</td>
								</tr>
								<tr>

									<td width="40%" align="right">&nbsp;</td>
									<td style="padding-left:70px"><input name="" id="finsh" type="button"
										value="<fmt:message key="common.btn.ok" />" class="diw_but_tc" />
									</td>
								</tr>
							</table>
						</div>
					</div>




					<div id="overlay2" style=" background: none repeat scroll 0 0 #000;display: none;height: 100%;left: 0;opacity: 0.1;position: absolute;top: 0;width: 100%;"></div>
					<div id="win2" style="background: none repeat scroll 0 0 #f1f5fe;display: none;margin: -30% 0 0 20%;position: absolute;top: 50%;width: 600px;">
						<h2>
							<span id="close2">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0" id="languageTable">
								<tr id="mustShow">

									<td colspan="2" width="100%" >
									<c:forEach items="${ languages}" var="language">
									<input  type="checkbox"   name="languageCheck_${language.code }" languageCode='${language.code }' languageName='${language.name }' value="${language.code}"> ${language.name} 
									</c:forEach>
									
									<span style="float:right"><input  type="checkbox" name="checkAll" >全选</span>
									
									</td>
									
								</tr>
								
								<tr id="mustShow">
								<td width="40%" align="center"><fmt:message
											key="memberman.complaintman.language" /></td>
								<td width="60%" align="center"><fmt:message
											key="memberman.complaintman.languagepackloction" />
											
											<span><a style="color: #d71200;" href="javascript:copyUrl();">复制</a></span>
											</td>
								
								
								</tr>
								
								
							</table>
							
								<div style="padding-left:200px">

									<input name="" id="batchFinsh" type="button"
										value="<fmt:message key="common.btn.ok" />" class="diw_but_tc" />
									
								</div>
						</div>
					</div>



					<h4>
						<span>
						<%-- <button type="button" class="pro_compar"
								id="addLanguagePackBtn2"
								value="<fmt:message key="common.btn.addlanguage" />">批量添加</button> --%>
						
						<button type="button" class="pro_compar"
								id="addLanguagePackBtn3"
								value="<fmt:message key="common.btn.addlanguage" />">批量添加</button> 
						
						
						
						<button type="button" class="pro_compar"
								id="addLanguagePackBtn"
								value="<fmt:message key="common.btn.addlanguage" />"><fmt:message key="common.btn.addlanguage" /></button> </span>
						<fmt:message key="software.version.language.info" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table id="languagePackTable" style="table-layout:fixed;word-wrap:break-word;" width="100%" border="0"
					bordercolor="#dddddd">
					<tr class="title">
						<td width="10%"><fmt:message
								key="memberman.complaintman.language" /></td>
						<td width="25%"
							style="word-wrap: break-word; word-break: break-all;"><fmt:message
								key="memberman.complaintman.languagepackloction" /></td>
						<td width="25%"
							style="word-wrap: break-word; word-break: break-all;"><fmt:message
								key="memberman.complaintman.testcardocloction" /></td>
						<td width="30%"
							style="word-wrap: break-word; word-break: break-all;"><fmt:message
								key="product.softwareversion.desc" /></td>
						<td width="10%"><fmt:message key="common.list.operation" />
						</td>

					</tr>

					<tr class="bott_col" style="display: none;">
						<td colspan="5" class="page" align="left"><span
							style="float: right;"><fmt:message
									key="common.page.number.arg2" />1-10<fmt:message
									key="common.page.number.arg3" /> <fmt:message
									key="common.page.number.arg4" />30<fmt:message
									key="common.page.number.arg5" /> </span> <span
							style="margin-right: 5px;"><img
								src="../main/images/leg_34.gif" width="10" height="12" /> </span> <span
							style="margin-right: 5px;"><img
								src="../main/images/leg_36.gif" width="6" height="11" /> </span> <span
							style="margin-right: 5px;"><img
								src="../main/images/shx.gif" width="2" height="21" /> </span> <span
							style="margin-right: 5px;"><fmt:message
									key="common.page.number.arg0" /> <input name="" type="text"
								class="page_in" value="1" /> <fmt:message
									key="common.page.number.arg1" /> <fmt:message
									key="common.page.number.arg4" />5<fmt:message
									key="common.page.number.arg1" /> </span> <span
							style="margin-right: 5px;"><img
								src="../main/images/shx.gif" width="2" height="21" /> </span> <span
							style="margin-right: 5px;"><img
								src="../main/images/leg_39.gif" width="6" height="11" /> </span> <span
							style="margin-right: 5px;"><img
								src="../main/images/leg_41.gif" width="10" height="12" /> </span> <span
							style="margin-right: 5px;"><img
								src="../main/images/shx.gif" width="2" height="21" /> </span> <span
							style="margin-right: 5px;"><img
								src="../main/images/leg_31.gif" width="15" height="14" /> </span>
						</td>
					</tr>
				</table>
				</div>
				<div style="padding-top: 20px;padding-left:40%">
					<input name="" id="sumbitBtn" type="button"
						value="<fmt:message key="common.btn.ok" />" class="diw_but" /><input
						name="" type="button"
						onclick="jump('toSoftwareVersion.do?softwareType.code=${softwareType.code }')"
						value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			
		</div>

	</form>
</body>
</html>


