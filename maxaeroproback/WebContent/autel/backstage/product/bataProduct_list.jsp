<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<html>
<head>
<script type="text/javascript" src="../js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../backstage/system/js/bataProductInfo.js"></script>
<script type="text/javascript">
	
	function jump(url){
		var e=document.createElement("a");
		e.href=url;
		document.body.appendChild(e);
		e.click();
	}
</script>

</head>
<body>

	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="product.rootName" />&gt; <fmt:message key="product.test.platform" />&gt; <fmt:message key="product.test.bata.product" /> </div>
		<form action="list.do" method="post" id="productForm">
		<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="sys.product.list" /></h4>
			<div class="sear_table">
				<table width="100%" style="border:0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38" style="font-size: 12px;">
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td width="154"><input type="text" name="bataProduct.serialNo" value="${bataProduct.serialNo}" class="time_in"/>
						</td>
						<td width="160" align="right">
						<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />
							
						</td>
						<td width="154"></td>
						<td width="80" align="right"></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span><input type="button" value="<fmt:message key="common.btn.add" />" id="addProductType"  onclick="jump('add.do')"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp;  </span><fmt:message key="sys.product.list" />
				</h4>
			</div>
		</div>
		
		<div class="grid" id="productTypeTable">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell><fmt:message key="memberman.customman.customerid" /></grid:cell>
					<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
					<grid:cell><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
					<grid:cell>销售契约</grid:cell>
					<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="bataProduct"
					varStatus="productTypeStatus">
					<tr>
						<td style="20%">${bataProduct.autelId}</td>
						<td style="20%">${bataProduct.proTypeName}</td>
						<td style="20%">${bataProduct.serialNo}</td>
						<td style="20%"><span id="saleCode_${bataProduct.serialNo}"><a href="../market/showSaleContract.do?saleContract.code=${bataProduct.saleCode }">${bataProduct.saleName}</a></span></td>
						<td style="20%" class="caoz"><a href="delete.do?code=${bataProduct.code }"><fmt:message key="common.list.del" /></a>   
						    &nbsp;<a href="javascript:ProductInfo.openSaleContractSelectDlg('${bataProduct.serialNo}')" >修改契约</a>
						</td>
					</tr>
				</c:forEach>
			</grid:grid>
		</div>
		</div>
		</form>	
	 
	<input type="hidden" id="batchSerialNo">
    <div id="selectSaleContractDlg"></div>
	</body>	
	</html>
<script type="text/javascript">
$(function(){
	ProductInfo.init();
});

function initProductInfo(){
	clickEvent();
}
setInterval('initProductInfo()',100);
</script>	
