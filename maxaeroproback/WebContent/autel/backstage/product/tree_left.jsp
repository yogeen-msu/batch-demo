<%@ page contentType="text/html;charset=UTF-8"%>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<link rel="stylesheet" href="../js/common/ztree/css/demo.css"
	type="text/css">
<link rel="stylesheet"
	href="../js/common/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript"
	src="../js/common/ztree/js/jquery.ztree.core-3.5.js"></script>

<SCRIPT type="text/javascript">
		<!--
		var setting = {
			async: {
				enable: true,
				url:"getProductTree.do",
				autoParam:["id=treeParam.id"],
				dataFilter: filter
			}
		};

		function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			childNodes = eval(childNodes);
			for (var i=0, l=childNodes.length; i<l; i++) {
				childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
			}
			return childNodes;
		}

		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		//-->
	</SCRIPT>


<div class="content_wrap">
	<div class="zTreeDemoBackground left">
		<ul id="treeDemo" class="ztree"></ul>
	</div>
</div>
