<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  
                      > 出厂产品管理 
                      > 软件对比查询</div>
<div class="grid">
	<form method="post" action="data-compare!list.do">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div>软件对比查询</h4>
				<div class="sear_table">
				<table width="100%">
					<tr height="8">
					    <td width="120px" align="right"></td>
						<td width="111" align="right"> 序列号:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="serialNo" value="${serialNo}" />
						</td>
						<td width="154">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
						<td align="right">
						</td>
					</tr>
				</table>
			</div>
			
		</div>
</form>	

      <div style="width:800px">
      <table style="border:0px;">
           <tr><td style="border-left:0px;border-right:0px;color:#B84435;font-size:14px;text-align:left;font-weight:bold;">语言对比</td></tr>
           <tr><td style="border:0px;">
	       <grid:grid from="languageDataCompare">
			   <grid:header>
					<grid:cell width="100px">产品网站配置</grid:cell>
					<grid:cell width="100px">系统平台部优化</grid:cell>
				</grid:header>
			    <grid:body item="item">
					<grid:cell>
						${item.oldLanguageCode}
					</grid:cell>
					<grid:cell>
						${item.newLanguageCode}
					</grid:cell>
				</grid:body>
			</grid:grid>
			</td>
			</tr>
		</table>
     </div> 
     
     <div style="width:800px">
	     <table style="border:0px;">
		       <tr><td style="border-left:0px;border-right:0px;color:#B84435;font-size:14px;text-align:left;font-weight:bold;">最小销售单位对比</td></tr>
		       <tr><td style="border:0px;">
			       <grid:grid from="minSaleCodeCompare">
						<grid:header>
							<grid:cell width="100px">产品网站配置</grid:cell>
							<grid:cell width="100px">系统平台部优化</grid:cell>
						</grid:header>
					<grid:body item="item" >
						<grid:cell style="padding-left:3px;text-align:left">
							${item.minSaleUnitCode} 
							<c:if test="${item.minSaleUnitCode != null}">
							<font color="#b74033">(${item.oldName})</font>
							</c:if>
						</grid:cell>
						<grid:cell style="padding-left:3px;text-align:left">
							${item.minSaleCode} <font color="#b74033">(${item.newName})</font>
						</grid:cell>
					</grid:body>	 
				   </grid:grid>
			   </td></tr>
		   </table>
     </div> 
     
      <div>
         <div style="height:30px;line-height:30px;color:#B84435;font-size:14px;text-align:leftl;font-weight: bold;">软件配置对比</div>
	     <div style="width:45%;float:left;padding-top:0px;"> 
		    <div style="width: 45%; float: left; padding-left: 0px; padding-top: 0px; position: absolute;">
		    
			    <table style="border:0px;">
		            <tr><td style="border-left:0px;border-right:0px;color:#B84435;font-size:14px;text-align:left;font-weight:bold;">产品网站配置</td></tr>
					 <tr><td style="border:0px;">
						<grid:grid from="oldDataSoftware">
							<grid:header>
								<grid:cell width="150px">编码</grid:cell>
								<grid:cell width="100px">名称</grid:cell>
								<grid:cell width="100px">语言</grid:cell>
								<grid:cell width="100px">版本</grid:cell>
							</grid:header>
						
							<grid:body item="item">
								<grid:cell>
									${item.carId}
								</grid:cell>
								<grid:cell>
									${item.carName}
								</grid:cell>
								<grid:cell>
									${item.languageCode}
								</grid:cell>
								<grid:cell>
									${item.versionName}
								</grid:cell>
							</grid:body>
						</grid:grid>
					</td></tr>
				</table>
			 
		   </div>
		  </div>
		  
		  <div style="width:45%; float:left;padding-left:500px;padding-top:0px;">
		  	<table style="border:0px;">
	            <tr><td style="border-left:0px;border-right:0px;color:#B84435;font-size:14px;text-align:left;font-weight:bold;">系统部优化配置</td></tr>
				<tr><td style="border:0px;">
					<grid:grid from="newDataSoftware">
						<grid:header>
							<grid:cell width="150px">编码</grid:cell>
							<grid:cell width="100px">名称</grid:cell>
							<grid:cell width="100px">语言</grid:cell>
							<grid:cell width="100px">版本</grid:cell>
						</grid:header>
					
						<grid:body item="item">
							<grid:cell>
								${item.carId}
							</grid:cell>
							<grid:cell>
								${item.carName}
							</grid:cell>
							<grid:cell>
								${item.languageCode}
							</grid:cell>
							<grid:cell>
								${item.versionName}
							</grid:cell>
						</grid:body>
					</grid:grid>
				</td></tr>
			</table>
		   </div>	
	   </div> 
	</div>

<script type="text/javascript">
  $(function(){
	  $("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").each(function(i,obj){
				$(obj).attr("selected","selected");
			});		
			//重置单选
			$(".sear_table").find("input[type='radio'][value='-1']").each(function(i,obj){
				$(obj).attr("checked","checked");
			});
		});
  });
</script>
