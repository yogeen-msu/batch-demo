<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderman.paystatus.log" /></title>

<link href="${ctx}/autel/main/css/right.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/cop.js"></script>

</head>

<body>
	<form action="customer-change-autel!list.do" method="post" id="f1"
		name="f1">

		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			会员管理
			>
			客戶管理
			> 更改autelid激活
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;更改autelid激活
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">

							<td width="65" align="right">新AutelID:</td>
							<td width="154"><input class="sear_item" type="text"
								name="customerChangeAutel.newAutelId" value="${customerChangeAutel.newAutelId}" />
							</td>
							<td width="65" align="right">老AutelID:</td>
							<td width="154"><input class="sear_item" type="text"
								name="customerChangeAutel.oldAutelId " value="${customerChangeAutel.oldAutelId}" />
							</td>
							<td width="100" align="right">新AutelID激活状态:</td>
							<td width="154">
							    <select id="actState_select" name="customerChangeAutel.actState" class="sear_item">
							       <option value="-1">请选择</option>
							       <option value="0">未激活</option>
							       <option value="1">已激活</option>
							    </select>
							    <script type="text/javascript">
									var actState = "${customerChangeAutel.actState}";
									$("#actState_select option[value='"+actState+"']").attr("selected","selected");
								</script>
							</td>
							<td width="120" align="right"><input type="submit"
								value=<fmt:message key="common.btn.search" /> class="search_but" />
								<input type="button"
								value=<fmt:message key="common.btn.reset" /> class="search_but"
								id="reset_but" /></td>
						</tr>
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="grid">
				<grid:grid from="webpage">
					<grid:header>
						<grid:cell width="100">CustomerCode</grid:cell>
						<grid:cell width="100">新AutelID</grid:cell>
						<grid:cell width="100">新AutelID激活状态</grid:cell>
						<grid:cell width="100">老AutelID</grid:cell>
						<grid:cell width="100">老AutelID激活状态</grid:cell>
						<grid:cell width="100">
							<fmt:message key="common.list.operation" />
						</grid:cell>
					</grid:header>
					<grid:body item="item">
						<grid:cell>${item.customerCode}</grid:cell>
						<grid:cell>${item.newAutelId}</grid:cell>
						<grid:cell>
							<c:if test="${item.actState==0 }">未激活</c:if>
							<c:if test="${item.actState==1 }">已激活</c:if>
						</grid:cell>
						<grid:cell>${item.oldAutelId}</grid:cell>
						<grid:cell>
							<c:if test="${item.oldActState==0 }">未激活</c:if>
							<c:if test="${item.oldActState==1 }">已激活</c:if>
						</grid:cell>
						<grid:cell>
							<c:if test="${item.actState==0 }">
								<a id="active"  customerCode="${item.customerCode}" newAutelId="${item.newAutelId }">激活 </a>
							</c:if>
							<c:if test="${item.actState==1 }">
							        激活
							</c:if>
						</grid:cell>
					</grid:body>
				</grid:grid>
			</div>
		</div>
	</form>

</body>

<script type="text/javascript">
	$("form.validate").validate();

	$(function() {
		$("#reset_but").click(
				function() {
					//重置文本框
					$(".sear_table").find("input[type='text']").each(
							function(i, obj) {
								$(obj).val("");
							});

					//重置下拉框
					$(".sear_table").find("select option[value='']").attr(
							"selected", "selected");
				});

		$("#active")
				.click(
						function() {
							if(confirm("確定激活")){
								var newAutelId = $(this).attr("newAutelId");
								var customerCode =$(this).attr("customerCode");
								window.location.href = "customer-change-autel!active.do?customerChangeAutel.customerCode="+customerCode
										+"&customerChangeAutel.newAutelId="+newAutelId;
										
							}
						})

	});

	function jump(url) {
		var tmpForm = $("<form  method='post'></form>");
		$(tmpForm).attr("action", url);
		tmpForm.appendTo(document.body).submit();
	}
</script>

</html>


