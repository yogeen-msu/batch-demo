<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   
                      >  <fmt:message key="syssetting.outfactoryproman" />  
                      > <fmt:message key="syssetting.outfactoryproman.info" />   
                      >  MAXIFLASH PRO 添加</div>
<form action="product-for-pro!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;MAXIFLASH PRO 添加 </h4>

	<table class="form-table">
		<tr>
			<th><label class="text">序列号:</label></th>
			<td>
				<input type="text" name="productForPro.serialNo"  dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th><label class="text">类型:</label></th>
			<td>
			   <select name="productForPro.type">
			      <option value="1">蓝牙盒子</option>
			      <option value="2">J23534盒子</option>
			   </select>
			</td>
		</tr>
		<tr>
			<th style="width: 140px"><label class="text">注册密码:</label></th>
			<td>
				<input type="text" name="productForPro.regPwd" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th><label class="text">序列号出厂日期:</label></th>
			<td>
				<input type="text" name="productForPro.proDate" readonly="readonly" dataType="date" isrequired="true" class="dateinput" maxlength="23"/>
			</td>
		</tr>
		
	</table>
</div>

	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="javascript:history.go(-1)" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>                                
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
</script>