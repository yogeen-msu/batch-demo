<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderman.paystatus.log" /></title>
<style>
 .mask {    
         position: absolute; top: 0px; filter: alpha(opacity=60); background-color: #777;  
         z-index: 1002; left: 0px;  
         opacity:0.5; -moz-opacity:0.5;  
}  
</style>

<link href="${ctx}/autel/main/css/right.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript"
	src="${ctx}/statics/js/cop.js"></script>

</head>

<body>
	<form action="product-for-pro!list.do" method="post" id="f1" name="f1">

		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			<fmt:message key="menu.system.setting" />
			>
			<fmt:message key="syssetting.outfactoryproman" />
			> MAXIFLASH PRO 管理
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;MAXIFLASH PRO 管理
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
						
						    <td width="80" align="right">序列号:</td>
							<td width="154">
								<input class="sear_item" type="text" name="productForPro.serialNo" value="${productForPro.serialNo}" />
							</td>
						    <td width="60" align="right">类型:</td>
							<td width="154">
								<select name="productForPro.type" id="productForProType_select" class="sear_item">
									<option value="-1"><fmt:message key="select.option.select" /></option> 
									<option value="1">蓝牙盒子</option>
									<option value="2">J23534盒子</option>
								</select>
								<script type="text/javascript">
									var proValue = "${productForPro.type}";
									$("#productForProType_select option[value='"+proValue+"']").attr("selected","selected");
								</script>
							</td>
							<td width="80" align="right">关联序列号:</td>
							<td width="154">
								<input class="sear_item" type="text" name="productForPro.proCode" value="${productForPro.proCode}" />
							</td>
							<td width="150" align="right">
								<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
								<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
							</td>
						</tr>
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4>
						<span> 
						<c:if test="${adminFlag=='Y'}">
						<input name="" type="button"
							onclick="jump('product-for-pro!add.do')"
							value="<fmt:message key="common.btn.add" />"
							class="search_but addorderpaystatuslog" />
							</c:if>
						</span>MAXIFLASH PRO 管理:
					</h4>
				</div>
			</div>
			<div class="grid">
				<grid:grid from="webpage">
					<grid:header>
						<grid:cell width="100">
							<fmt:message key="memberman.complaintman.productserial" />
						</grid:cell>
						<grid:cell width="80">类型</grid:cell>
						<grid:cell width="100">关联产品序列号</grid:cell>
						<grid:cell width="60">绑定状态</grid:cell>
						<grid:cell width="60">注册密码</grid:cell>
						<grid:cell width="60">绑定时间</grid:cell>
						<grid:cell width="60">出厂日期</grid:cell>
						<grid:cell width="120">操作原因</grid:cell>
						
						 <grid:cell width="100">
							<fmt:message key="common.list.operation" />
						</grid:cell>  
					</grid:header>
					<grid:body item="item">
						<grid:cell>${item.serialNo}</grid:cell>
						<grid:cell>
							<c:if test="${item.type==1 }">蓝牙盒子</c:if>
							<c:if test="${item.type==2 }">J23534盒子</c:if>
						</grid:cell>
						<grid:cell>${item.proCode}</grid:cell>
						<grid:cell>
						    <c:if test="${item.regStatus==0 }">未綁定</c:if>
							<c:if test="${item.regStatus==1 }">已綁定</c:if>
						</grid:cell>
						<grid:cell>${item.regPwd}</grid:cell>
						<grid:cell>${fn:substring(item.regTime,0,10) }</grid:cell>
						<grid:cell>${fn:substring(item.proDate,0,10) }</grid:cell>
						<grid:cell>${item.reason }</grid:cell>
						<grid:cell>
						
							<a href="product-for-pro!edit.do?productForPro.id=${item.id}">修改 </a> &nbsp;&nbsp;
							<c:if test="${item.regStatus!=1 }">
							<a href="product-for-pro!delete.do?productForPro.id=${item.id}" onclick="javascript:return confirm('<fmt:message key="product.delete.check" />')" >删除 </a> &nbsp;&nbsp; 
							</c:if>
							<c:if test="${item.regStatus==0 }">
							   <a id="addBound" class="addBound" onclick="openAddBoundDlg('${item.id}','${item.type}')">绑定 </a> 
							</c:if>
							<c:if test="${item.regStatus==1 }">
							         绑定  
							</c:if>
						</grid:cell> 
					</grid:body>
				</grid:grid>
			</div>
		</div>
	</form>
	
	   <div id="dlg_addDlg" class="dialog jqmID1" style="width: 450px; z-index: 3000; opacity: 1;">
	   <div class="dialog_box"><div class="head" style="height:15px;">
	   <div style="font-weight:sold">添加产品绑定</div>
	   <span class="closeBtn"></span></div><div class="body dialogContent"><div name="addDlg" id="addDlg">
	   <form class="validate" id="addForm">
<div class="input">
   <table class="form-table" cellpadding="3" cellspacing="1" width="100%">
	<tbody><tr height="5">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
     <tr> 
       <th style="width:80px;padding-left:0px"><label class="text">关联序列号:</label></th>
       <td><input name="productForPro.proCode" id="productForPro_proCode" maxlength="60" value="" datatype="string" isrequired="true" type="text">  
       <input type="hidden" id="productForPro_id" name="productForPro.id">
       <input type="hidden" id="productForPro_type" name="productForPro.type">
            </td>
       
     </tr>
     <tr height="125">
       <th style="width:50px;padding-left:20px;"><label class="text">操作原因:</label></th>
       <td>
          <textarea cols="35" rows="3" name="productForPro.reason" id="productForPro_reason" datatype="string" isrequired="true"></textarea>
	   </td>
     </tr>
	<tr height="5">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
   </tbody></table>
   
   </div>
	<div class="submitlist" align="center">
	 <table>
	 <tbody><tr><td>
	           <input value="添加" class="submitBtn" id="saveAddBtn" type="button">
	           <input name="cancle" id="cancle" value="取消" class="submitBtn" type="button">
	   </td>
	   </tr>
	 </tbody></table>
	</div> 
	
   </form>

</div></div><div class="foot" style="height:9px;">&nbsp;</div></div></div>

<div id="main_mask" class="mask"></div>
</body>

<script type="text/javascript">

   $("form.validate").validate();
   $("#dlg_addDlg").hide();
   
	$(function() {
		$("#reset_but").click(
				function() {
					//重置文本框
					$(".sear_table").find("input[type='text']").each(
							function(i, obj) {
								$(obj).val("");
							});

					//重置下拉框
					$(".sear_table").find("select option[value='']").attr(
							"selected", "selected");
				});
		
		
		
		$("#saveAddBtn").click(function(){  
			add();  
		});
		
		
		$("#cancle").click(function(){  
			hideDIV();
		});
		 
	});
	
	function jump(url) {
		var tmpForm = $("<form  method='post'></form>");
		$(tmpForm).attr("action", url);
		tmpForm.appendTo(document.body).submit();
	}
	
	/**
	 * 打开添加对话框
	 */
	function openAddDlg(id){
		$("#dlg_addDlg").show();
	}
	
	function openAddBoundDlg(id,type){   
		$("#productForPro_id").val(id);
		$("#productForPro_type").val(type);
		showDIV();
	}
	/**
	 * 保存添加
	 */
	function add(){
		
		 if( !$("#addForm").checkall() ){
			return ;
		} 
		 $("#submitBtn").attr("disabled",false);
		var options = {
				url :"product-for-pro!addBoundSave.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {
					
				 	if(result.result==1){
				 		alert("邦定成功");
				 		$('#addForm')[0].reset(); 
				 		hideDIV();
				 		location.reload();
				 	}else{
				 		alert(result.message);
				 		$("#submitBtn").removeattr("disabled");
				 	}
				},
				error : function(e) {
					alert("邦定失败");
					$("#submitBtn").removeattr("disabled");
 				}
 		};
 
		$("#addForm").ajaxSubmit(options);
	}
	
	function showDIV(){
		showMask();
		$("#dlg_addDlg").show();
	}
	
	function hideDIV(){
		$("#main_mask").hide();  
		$("#dlg_addDlg").hide();
	}
	
	function showMask()
	{
		$("#main_mask").css("height",$(document).height());  
	    $("#main_mask").css("width",$(document).width());  
	    $("#main_mask").show();   
	}
</script>

</html>


