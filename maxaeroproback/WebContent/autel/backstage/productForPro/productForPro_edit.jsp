<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   
                    >  <fmt:message key="syssetting.outfactoryproman" />  
                    > <fmt:message key="syssetting.outfactoryproman.info" />   
                    >  MAXIFLASH PRO 更新</div>
<form action="product-for-pro!editSave.do"  class="validate" method="post" name="theForm" id="theForm">

<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;MAXIFLASH PRO 更新 </h4>
	<input type="hidden" name="productForPro.id" value="${productForPro.id}" />
	<input type="hidden" name="productForPro.code" value="${productForPro.code}" />
	<table class="form-table">
		<tr>
			<th><label class="text">序列号:</label></th>
			<td>
				<label name="productForPro.serialNo" >${productForPro.serialNo }</label>
			</td>
		</tr>
		<tr>
			<th><label class="text">类型:</label></th>
			<td>
			   <select name="productForPro.type" id="productForProType">
			      <option value="1" ${productForPro.type==1?"selected":"" }>蓝牙盒子</option>
			      <option value="2" ${productForPro.type==2?"selected":"" }>J23534盒子</option>
			   </select>
			</td>
		</tr>
		<tr>
			<th style="width: 140px"><label class="text">注册密码:</label></th>
			<td>
				<input type="text" name="productForPro.regPwd" value="${productForPro.regPwd }"  maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th><label class="text">序列号出厂日期:</label></th>
			<td>
				<input type="text" name="productForPro.proDate" value="${fn:substring(productForPro.proDate,0,10) }" readonly="readonly" dataType="date" isrequired="true" class="dateinput" maxlength="23"/>
			</td>
		</tr>
		
	</table>

</div>

<div class="submitlist" align="center">
	<table>
		<tr><td >
			<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
			<input name="reset" type="button" onclick="javascript:history.go(-1)" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
		</td></tr>
	</table>
</div>

</form>

<script type="text/javascript">
	$("form.validate").validate();
	/* var type='${productForPro.type}';  
	$("#productForProType option [value='"+type+"']").attr("selected","selected"); */
</script>