<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  
                      > 网站运营监控 
                      > 产品情况</div>
<div class="grid">   <div>

 <div style="width:100%;float:left;padding-top:0px;"> 
		    <div style="width: 100%; float: left; padding-left: 0px; padding-top: 0px; position: absolute;">
		    
			    <table >
					 <tr>
								<th width="20%">产品总计</th>
								<th width="20%">已注册</th>
								<th width="20%">未注册</th>
								<th width="20%">已解绑</th>
								<th width="20%">已过期</th>
					</tr>
					
					
					<tr>
								<td>
									${totalProduct}
								</td>
								<td>
									${regProduct}

								</td>
								<td>
									${notRegProduct}
								</td>
								<td>
									${notBoundProduct}
								</td>
								<td>
									${expireProduct}
								</td>
					</tr>
					
					
					<tr>
								<td colspan="5">
								
						统计类型:<select id="queryType" name="autel.queryType">
					   <option value="1">按产品数量</option>
					   <option value="2">按区域</option>
					   <option value="3">按语言</option>
					</select>
								
								
								
								 产品类型:<select name="autel.proTypeCode" id="proTypeCode_select" class="sear_item">
								<option value=""><fmt:message key="select.option.select" /></option> 
								<c:forEach var="item" items="${productTypeList}">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
					   注册状态:<select name="autel.proRegStatus" id="proRegStatus">
					<option value=""><fmt:message key="select.option.select" /></option> 
					   <option value="0">未注册</option>
					   <option value="1">已注册</option>
					   <option value="2">已解绑</option>
					</select>
								<input onclick="queryProducts()" type="button" id="searchProduct" value="确定" class="search_but" />
								</td>
					</tr>
					
					<tr>
				
					<td colspan="5">
					
					<div>
<iframe id="productFrame" frameborder="no" scrolling="no" src="toProductTotalPage.do" style="width:100%;height:700px;border:0px;float:left;padding-top:0px;"></iframe>
</div>
					
</td>
					</tr>
				</table>
			 
		   </div>
		  </div>
<script >

function queryProducts(){
	 var queryType= $("#queryType").val();
	 var productType=$("#proTypeCode_select").val();
	 var proRegStatus=$("#proRegStatus").val();
	 var url="";
	 if(queryType=="1"){
		 url="toProductTotalPage.do?1=1";
		 if(proRegStatus!=""){
			 url+="&autel.proRegStatus="+proRegStatus;
		 }
	 }
	 if(queryType=="2"){
		 url="toProductAreaPage.do?1=1";
		 if(productType!=""){
			 url+="&autel.proType="+productType;
		 }
		 if(proRegStatus!=""){
			 url+="&autel.proRegStatus="+proRegStatus;
		 }
	 }
	 if(queryType=="3"){
		 url="toProductLanguagePage.do?1=1";
		 if(productType!=""){
			 url+="&autel.proType="+productType;
		 }
		 if(proRegStatus!=""){
			 url+="&autel.proRegStatus="+proRegStatus;
		 }
	 }
	 $("#productFrame").attr("src",url);
}

</script>