var SaleContract  ={
	init:function(){
		var self  = this;
	
		
		//经销商
		Cop.Dialog.init({id:"sealerDlg",modal:false,title:'',height:"400px",width:"750px"});
		$("#selectSealerCodeDlg").click(function(){
			setHiddenValue(2);
			clearAllDivContent();
			self.openSealerDlg();
			
		});
		
		
		
	}, 

	
	
	//经销商
	openSealerDlg:function(){
		$("#sealerDlg").load("toSelectSealerDlg.do?ajax=yes",function(){
			$(".selectSealerItem").click(function(){
				var selectCode = $(this).attr("selectCode");		
				var selectName = $(this).attr("selectName");
				salerAdd(selectCode,selectName);
			});
		});
		Cop.Dialog.open("sealerDlg");
		
	},
	

	
};

var configAdd = function(code,name,areaCfgCode){
	$("#saleCfgCode").val(code);
	$("#saleCfgCodeName").val(name);
	$("#saleCfgCode").attr("areaCfgCode",areaCfgCode);
	Cop.Dialog.close("selectConfig");
};

var configMaxAdd = function(code,name,areaCfgCode){
	$("#maxSaleCfgCode").val(code);
	$("#maxSaleCfgCodeName").val(name);;
	$("#maxSaleCfgCode").attr("areaCfgCode",areaCfgCode);
	Cop.Dialog.close("selectMaxConfig");
};

var productAdd = function(code,proTypeName){
	$("#proTypeCode").val(code);
	$("#proTypeCodeName").val(proTypeName);
	Cop.Dialog.close("selectProductDlg");
};

var salerAdd = function(code,name){
	$("#sealerCode").val(code);
	$("#sealerCodeName").val(name);
	Cop.Dialog.close("sealerDlg");
};


function bindEvent(pager,grid){
	 var grid = pager.parent();
	 pager.find("li>a").unbind(".click").bind("click",function(){
		 load($(this).attr("pageno"),grid);
	 }); 
	 pager.find("a.selected").unbind("click");
};

function pageQuery(){
	$.ajax({
		url:"toSearchPtDlg.do",
		data: $('#pageSelectForm').serialize(), 
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			
			$(".selectProductItem").click(function(){
				var productCode = $(this).attr("selectCode");		//编号
				var proTypeCodeName = $(this).attr("selectName");		//产品型号
				
				productAdd(productCode,proTypeCodeName);
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};

function pageConfigQuery(){
	$.ajax({
		url:"toSelectSaleConfig.do",
		data: $('#pageSelectConfigForm').serialize(), 
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			
			$(".selectConfigItem").click(function(){
				var selectCode = $(this).attr("selectCode");	
				var selectName = $(this).attr("selectName");		
				var areaCfgCode = $(this).attr("areaCfgCode");
				configAdd(selectCode,selectName,areaCfgCode);
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};


function pageMaxConfigQuery(){
	$.ajax({
		url:"toSelectMaxSaleConfig.do",
		data: $('#pageSelectMaxConfigForm').serialize(), 
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			
			$(".selectMaxConfigItem").click(function(){
				var selectCode = $(this).attr("selectCode");	
				var selectName = $(this).attr("selectName");		
				var areaCfgCode = $(this).attr("areaCfgCode");
				configMaxAdd(selectCode,selectName,areaCfgCode);
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};

function paegSealerQuery(){
	$.ajax({
		url:"toSelectSealerDlg.do",
		data: $('#pageSelectSealerForm').serialize(), 
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			$(".selectSealerItem").click(function(){
				var selectCode = $(this).attr("selectCode");		
				var selectName = $(this).attr("selectName");		
				
				salerAdd(selectCode,selectName);
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};


function addNewAEvent()
{   //alert(122);
	
	//alert($("#selectGridId").attr("value"));
	//var clickConeten = $(".goto").find("font>a").attr("onclick");
	$(".goto").find("font>a").remove();
	$(".goto").find("font").append("<a  href='#' pagenum='page_query_no'>"+forward+"</a>");
	//clickConeten = String(clickConeten).replace("forms[0]","forms[1]");
	$(".goto").find("font>a").unbind(".click").bind("click",function(){
		paegSealerQuery2();

	 }); 
	/*
	$("#page_query_no").unbind(".change").bind("change",function(){
		alert(122);
	 }); 
	 */
	
	
};
function paegSealerQuery2(){
	
	var hiddeValue = Number($("#selectGridId").attr("value"));
	//alert("hiddeValue="+ hiddeValue);
	var ajaxUrl = "";
	var dataForm = "";
	
	//alert("$('#page_query_no').val()=" +$('#page_query_no').val());
	if(1 == hiddeValue)
	{
		ajaxUrl ='/autelproweb/autel/market/toSearchPtDlg.do?ajax=yes&_=1374473726761&page='+$('#page_query_no').val();
		dataForm = $('#pageSelectForm').serialize();
		//alert(ajaxUrl);
		//alert(dataForm);
	}
	else if(2 == hiddeValue)
	{
		ajaxUrl ='/autelproweb/autel/market/toSelectSealerDlg.do?ajax=yes&_=1374463163635&page='+$('#page_query_no').val();
		dataForm = $('#pageSelectSealerForm').serialize();
		//alert(ajaxUrl);
		//alert(dataForm);
	}
	else if(3 == hiddeValue)
	{
		ajaxUrl ='/autelproweb/autel/market/toSelectSaleConfig.do?ajax=yes&_=1374473789577&page='+$('#page_query_no').val();
		dataForm = $('#pageSelectConfigForm').serialize();
		//alert(ajaxUrl);
		//alert(dataForm);
	}
	else if(4 == hiddeValue)
	{
		ajaxUrl ='/autelproweb/autel/market/toSelectMaxSaleConfig.do?ajax=yes&_=1374473832445&page='+$('#page_query_no').val();
		dataForm = $('#pageSelectMaxConfigForm').serialize();
		//alert(ajaxUrl);
		//alert(dataForm);
	}
	$.ajax({
		url:ajaxUrl,
		data:dataForm , 
		success:function(html){
			//alert(html);
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			
			if(1 == hiddeValue)
			{
				$(".selectProductItem").click(function(){
					var productCode = $(this).attr("selectCode");		//编号
					var proTypeCodeName = $(this).attr("selectName");		//产品型号
					
					productAdd(productCode,proTypeCodeName);
				});
			}
			else if(2 == hiddeValue)
			{
				$(".selectSealerItem").click(function(){
					var selectCode = $(this).attr("selectCode");		
					var selectName = $(this).attr("selectName");		
					
					salerAdd(selectCode,selectName);
				});
			}
			else if(3 == hiddeValue)
			{
				$(".selectConfigItem").click(function(){
					var selectCode = $(this).attr("selectCode");		
					var selectName = $(this).attr("selectName");		
					var areaCfgCode = $(this).attr("areaCfgCode");
					configAdd(selectCode,selectName,areaCfgCode);
				
				});
			}
			else if(4 == hiddeValue)
			{
				$(".selectMaxConfigItem").click(function(){
					var selectCode = $(this).attr("selectCode");		
					var selectName = $(this).attr("selectName");		
					var areaCfgCode = $(this).attr("areaCfgCode");
					configMaxAdd(selectCode,selectName.areaCfgCode);
				
				});
			}
		},
		error:function(){
			alert("Error :");
		}
	});
};

function setHiddenValue(value)
{
	$("#selectGridId").attr("value",value);
};

function clearAllDivContent()
{
	$("#selectProductDlg").html("");
	$("#selectConfig").html("");
	$("#selectMaxConfig").html("");
	$("#sealerDlg").html("");
};
