<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Portal - jQuery EasyUI</title>
<link type="text/css" rel="stylesheet"
	href="../../themes/autel/css/easyui.css" />
<style type="text/css">
.title {
	font-size: 16px;
	font-weight: bold;
	padding: 20px 10px;
	background: #eee;
	overflow: hidden;
	border-bottom: 1px solid #ccc;
}

.t-list {
	padding: 5px;
}
</style>
<script type="text/javascript"
	src="../../themes/autel/js/common/jquery.min.js"></script>
<script type="text/javascript"
	src="../../themes/autel/js/common/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../../themes/autel/js/common/jquery.portal.js"></script>

<script>
	$(function() {
		$('#pp').portal({
			border : false,
			fit : true
		});

	});

	function remove() {
		$('#pp').portal('remove', $('#pgrid'));
		$('#pp').portal('resize');
	}
</script>
</head>
<body class="easyui-layout">
	<div region="north" class="title" border="false" style="height: 60px;">
		销售信息查询平台</div>
	<div region="center" border="false">
		<div id="pp" style="position: relative">
			<div style="width: 45%;">

				<div id="chartdiv" title="经销商销售TOP10" collapsible="true"
					closable="false" style="height: 350px; padding: 5px;">


					<OBJECT width="800" height="300" id="Column3D">
						<embed
							src="../js/FusionCharts/Charts/FCF_Column3D.swf?chartWidth=800&chartHeight=300"
							flashVars="&dataXML=<graph caption='Monthly Sales Summary' subcaption='For the year 2004' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='$' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='008ED6' divLineColor='008ED6' divLineAlpha='20' alternateHGridAlpha='5' >
							   <set name='Jan' value='17400' hoverText='January'/>
							   <set name='Feb' value='19800' hoverText='February'/>
							   <set name='Mar' value='21800' hoverText='March'/>
							   <set name='Apr' value='23800' hoverText='April'/>
							   <set name='May' value='29600' hoverText='May'/>
							   <set name='Jun' value='27600' hoverText='June'/>
							   <set name='Jul' value='31800' hoverText='July'/>
							   <set name='Aug' value='39700' hoverText='August'/>
							   <set name='Sep' value='37800' hoverText='September'/>
							   <set name='Oct' value='21900' hoverText='October'/>
							   <set name='Nov' value='32900' hoverText='November' />
							   <set name='Dec' value='39800' hoverText='December' />
                              </graph>"quality="high" width="800" height="300" type="application/x-shockwave-flash" />
					</object>
				</div>

				<div title="产品销售TOP10" collapsible="true" closable="false"
					style="height: 350px; padding: 5px;">
					<OBJECT width="800" height="300" id="Column3D">
						<embed
							src="../js/FusionCharts/Charts/FCF_Line.swf?chartWidth=800&chartHeight=300"
							flashVars="&dataXML=<graph caption='Monthly Sales Summary' subcaption='For the year 2004' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='$' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >
   
						   <set name='Jan' value='17400' hoverText='January'/>
						   <set name='Feb' value='19800' hoverText='February'/>
						   <set name='Mar' value='21800' hoverText='March'/>
						   <set name='Apr' value='23800' hoverText='April'/>
						   <set name='May' value='29600' hoverText='May'/>
						   <set name='Jun' value='27600' hoverText='June'/>
						   <set name='Jul' value='31800' hoverText='July'/>
						   <set name='Aug' value='39700' hoverText='August'/>
						   <set name='Sep' value='37800' hoverText='September'/>
						   <set name='Oct' value='21900' hoverText='October'/>
						   <set name='Nov' value='32900' hoverText='November' />
						   <set name='Dec' value='39800' hoverText='December' />
   </graph>"
							quality="high" width="800" height="300"
							type="application/x-shockwave-flash" />
					</object>
				</div>

				<div title="产品销售区域TOP10" collapsible="true" closable="false"
					style="height: 350px; padding: 5px;">
					<OBJECT width="800" height="300" id="Column3D">
						<embed
							src="../js/FusionCharts/Charts/FCF_Line.swf?chartWidth=800&chartHeight=300"
							flashVars="&dataXML=<graph caption='Monthly Sales Summary' subcaption='For the year 2004' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='$' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >
							   <set name='Jan' value='17400' hoverText='January'/>
							   <set name='Feb' value='19800' hoverText='February'/>
							   <set name='Mar' value='21800' hoverText='March'/>
							   <set name='Apr' value='23800' hoverText='April'/>
							   <set name='May' value='29600' hoverText='May'/>
							   <set name='Jun' value='27600' hoverText='June'/>
							   <set name='Jul' value='31800' hoverText='July'/>
							   <set name='Aug' value='39700' hoverText='August'/>
							   <set name='Sep' value='37800' hoverText='September'/>
							   <set name='Oct' value='21900' hoverText='October'/>
							   <set name='Nov' value='32900' hoverText='November' />
							   <set name='Dec' value='39800' hoverText='December' />
   </graph>"
							quality="high" width="800" height="300"
							type="application/x-shockwave-flash" />
					</object>
				</div>
			</div>
			<div style="width: 50%; padding-left: 1%">
				<div title="升级卡统计报表" collapsible="true" closable="false"
					style="height: 350px; padding: 5px;">
					<OBJECT width="800" height="300" id="Column3D">
						<embed
							src="../js/FusionCharts/Charts/FCF_Line.swf?chartWidth=800&chartHeight=300"
							flashVars="&dataXML=<graph caption='Monthly Sales Summary' subcaption='For the year 2004' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='$' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >
						   <set name='Jan' value='17400' hoverText='January'/>
						   <set name='Feb' value='19800' hoverText='February'/>
						   <set name='Mar' value='21800' hoverText='March'/>
						   <set name='Apr' value='23800' hoverText='April'/>
						   <set name='May' value='29600' hoverText='May'/>
						   <set name='Jun' value='27600' hoverText='June'/>
						   <set name='Jul' value='31800' hoverText='July'/>
						   <set name='Aug' value='39700' hoverText='August'/>
						   <set name='Sep' value='37800' hoverText='September'/>
						   <set name='Oct' value='21900' hoverText='October'/>
						   <set name='Nov' value='32900' hoverText='November' />
						   <set name='Dec' value='39800' hoverText='December' />
   							</graph>"
							quality="high" width="800" height="300"
							type="application/x-shockwave-flash" /></object>
				</div>
				<div title="订单统计报表" collapsible="true" closable="false"
					style="height: 350px; padding: 5px;">
					<OBJECT width="800" height="300" id="Column3D">
						<embed
							src="../js/FusionCharts/Charts/FCF_Pie2D.swf?chartWidth=800&chartHeight=300"
							flashVars="&dataXML=<graph caption='Monthly Sales Summary' subcaption='For the year 2004' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='$' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >
						   <set name='Jan' value='17400' hoverText='January'/>
						   <set name='Feb' value='19800' hoverText='February'/>
						   <set name='Mar' value='21800' hoverText='March'/>
						   <set name='Apr' value='23800' hoverText='April'/>
						   <set name='May' value='29600' hoverText='May'/>
						   <set name='Jun' value='27600' hoverText='June'/>
						   <set name='Jul' value='31800' hoverText='July'/>
						   <set name='Aug' value='39700' hoverText='August'/>
						   <set name='Sep' value='37800' hoverText='September'/>
						   <set name='Oct' value='21900' hoverText='October'/>
						   <set name='Nov' value='32900' hoverText='November' />
						   <set name='Dec' value='39800' hoverText='December' />
   							</graph>"
							quality="high" width="800" height="300"
							type="application/x-shockwave-flash" /></object>
				</div>
				<div title="升级情况统计表" collapsible="true" closable="false"
					style="height: 350px; padding: 5px;">
					<OBJECT width="800" height="300" id="Column3D">
						<embed
							src="../js/FusionCharts/Charts/FCF_Pie2D.swf?chartWidth=800&chartHeight=300"
							flashVars="&dataXML=<graph showNames='1'  decimalPrecision='0'  >
							   <set name='USA' value='20' isSliced='1'/>
								<set name='France' value='7'  />
							    <set name='India' value='12' />
							    <set name='England' value='11' />
								<set name='Italy' value='8' />
							    <set name='Canada' value='19'/>
							    <set name='Germany' value='15'/>
</graph>"
							quality="high" width="800" height="300"
							type="application/x-shockwave-flash" /></object>
				</div>
			</div>
			
		</div>
	</div>
</body>
</html>