<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderman.paystatus.log" /></title>
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/backstage/check/js/saleDialog.js"></script>
<script language="JavaScript" src="${ctx}/autel/js/FusionCharts/FusionCharts.js"></script>

</head>

<body>
	<form action="productSoftware.do" class="validate" method="post" id="f1" name="f1">


		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			系统管理
			>
			网站整体监控
			> 产品软件使用情况统计报表
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;产品软件使用情况统计报表
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
						
							<td width="300px;"> 产品类型:<select name="autel.proType" id="proTypeCode_select" class="sear_item" style="width:200px">
								<option value=""><fmt:message key="select.option.select" /></option> 
								<c:forEach var="item" items="${productTypeList}">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
							<script type="text/javascript">
								var proValue = "${autel.proType}";
								$("#proTypeCode_select option[value='"+proValue+"']").attr("selected","selected");
							</script>
							
							</td>
							<td><input type="submit"
								value="<fmt:message key="common.btn.search" />"
								class="search_but" />&nbsp;<input type="button"
								value="<fmt:message key="common.btn.reset" />"
								class="search_but" id="reset_but" /></td>
								<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
				</div>
			</div>
			<div>
				<OBJECT   width="900" height="700" id="Column3D" >
   <embed src="../js/FusionCharts/Charts/FCF_Bar2D.swf?chartWidth=800&chartHeight=700"   wmode="Opaque"
   flashVars="&dataXML=<graph caption='产品软件使用情况统计报表' xAxisName='类型' yAxisName='数量' unescapeLinks='0' baseFontSize='13' showNames='1' rotateNames='1'   decimalPrecision='0' formatNumberScale='0' exportEnabled='1'>
   
  
   
 <c:forEach var="item" items="${upgradeInfo}">
   <set name='${item.key}' value='${item.value}' />
   </c:forEach> 
  
     
   </graph>" quality="high" width="900" height="700" name="Column3D" type="application/x-shockwave-flash"  />
   
</object>
			</div>
		</div>
	</form>
	
	<div id="sealerDlg"></div>
	
</body>
<script type="text/javascript">
var forward= '<fmt:message key="common.info.jump" />';
function initSealer(){
	$(".selectSealerItem").click(function(){
		var selectCode = $(this).attr("selectCode");		
		var selectName = $(this).attr("selectName");		
		
		$("#sealerCode").val(selectCode);
		$("#sealerCodeName").val(selectName);
		Cop.Dialog.close("sealerDlg");
	});
}



	$(function() {
		
		SaleContract.init();
		
		$("#reset_but").click(
				function() {
					//重置文本框
					$(".sear_table").find("input[type='text']").each(
							function(i, obj) {
								$(obj).val("");
							});
                     $("#sealerCode").val(""); 
					//重置下拉框
					$(".sear_table").find("select option[value='']").attr(
							"selected", "selected");
				});
	});
	function jump(url) {
		var tmpForm = $("<form  method='post'></form>");
		$(tmpForm).attr("action", url);
		tmpForm.appendTo(document.body).submit();
	}	
</script>
</html>


