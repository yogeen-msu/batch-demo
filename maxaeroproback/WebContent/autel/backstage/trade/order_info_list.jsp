<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.paylogman" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>


</head>

<body>
<form action="listorderinfo.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; <fmt:message key="trademan.logman" /> &gt; <fmt:message key="trademan.paylogman" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="trademan.paylogman" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="orderman.list.ddh" />:</td>
						<td width="154"><input name="orderInfoVoSel.code" value="${orderInfoVoSel.code}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="154">
							<select class="act_zt protypecode" name="orderInfoVoSel.proTypeCode" protypecode="${orderInfoVoSel.proTypeCode}">
								<option value="">--<fmt:message key="orderman.list.all" />--</option>
								<c:forEach items="${productTypes }" var="item">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
						</td>
						<td width="111" align="right"><fmt:message key="memberman.customman.orderstate" />:</td>
						<td>
							<select class="act_zt orderstatesel" name="orderInfoVoSel.orderState" orderstatesel="${orderInfoVoSel.orderState}">
								<option value="">--<fmt:message key="orderman.list.all" />--</option>
								<option value="0"><fmt:message key="memberman.customman.canceled" /></option>
								<option value="1"><fmt:message key="memberman.customman.valid" /></option>
								
							</select>
						</td>
					</tr>
					<tr height="38">
						<td align="right"><fmt:message key="orderman.list.paystate" />:</td>
						<td>
							<select class="act_zt orderpaystatesel" name="orderInfoVoSel.orderPayState" orderpaystatesel="${orderInfoVoSel.orderPayState}">
								<option value="">--<fmt:message key="orderman.list.all" />--</option>
								<option value="0"><fmt:message key="orderman.list.nopay" /></option>
								<option value="1"><fmt:message key="orderman.list.payed" /></option>
								<option value="2">已退款</option>
								<option value="3">已回款</option>
							</select>
						</td>
						<td align="right"><fmt:message key="orderman.list.getmoneyconfirmstate" />:</td>
						<td>
							<select class="act_zt recconfirmstatesel" name="orderInfoVoSel.recConfirmState" recconfirmstatesel="${orderInfoVoSel.recConfirmState}">
								<option value="">--<fmt:message key="orderman.list.all" />--</option>
								<option value="0"><fmt:message key="orderman.list.noconfirm" /></option>
								<option value="1"><fmt:message key="orderman.list.confirmed" /></option>
							</select>
						</td>
						<td align="right"><fmt:message key="orderman.list.dealstate" />:</td>
						<td>
							<select class="act_zt processstatesel" name="orderInfoVoSel.processState" processstatesel="${orderInfoVoSel.processState}">
								<option value="">--<fmt:message key="orderman.list.all" />--</option>
								<option value="0"><fmt:message key="orderman.list.nodeal" /></option>
								<option value="1"><fmt:message key="orderman.list.dealed" /></option>
							</select>
						</td>
					</tr>
					<tr height="38" >
						<td align="right"><fmt:message key="orderman.list.ordertime" />:</td>
						<td width="180px">
							<input name="orderInfoVoSel.beginDate" value="${orderInfoVoSel.beginDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="orderInfoVoSel.endDate" value="${orderInfoVoSel.endDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							</td>
						<td align="right"><fmt:message key="orderman.list.paydate" />:</td>
						<td width="180px">
							<input name="orderInfoVoSel.payBeginDate" value="${orderInfoVoSel.payBeginDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="orderInfoVoSel.payEndDate" value="${orderInfoVoSel.payEndDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							</td>
						<td align="right"><fmt:message key="trademan.paylog.bankflownum" />:</td>
						<td>
							<input name="orderInfoVoSel.payNumber" value="${orderInfoVoSel.payNumber}" type="text"    />
						</td>
						<td align="right"><fmt:message key="trademan.paylog.paymember" />:</td>
						<td>
							<input name="orderInfoVoSel.autelId" value="${orderInfoVoSel.autelId}" type="text"    />
						</td>
						<td>
							&nbsp;&nbsp;<input type="button" id="submit_but" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
						<td></td>
						<td></td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="trademan.paylogman" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="11%"><fmt:message key="orderman.list.ddh" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="trademan.paylog.paymember" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="orderman.list.ordertime" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="orderman.list.money" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.orderstate" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="orderman.list.paystate" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="orderman.list.paydate" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="trademan.paylog.bankflownum" /></grid:cell>
<%-- 					<grid:cell width="9%"><fmt:message key="orderman.list.getmoneyconfirmstate" /></grid:cell>
					<grid:cell width="7%"><fmt:message key="orderman.list.dealstate" /></grid:cell> --%>
					<grid:cell width="9%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.code}</grid:cell>
					<grid:cell>${item.autelId}</grid:cell>
					<grid:cell>${item.orderDate}</grid:cell>
					<grid:cell>${item.orderMoney}</grid:cell>
					<grid:cell><span class="orderstate" orderstate="${item.orderState}"></span></grid:cell>
					<grid:cell><span class="orderpaystate" orderpaystate="${item.orderPayState}"></span></grid:cell>
					<grid:cell>${item.payDate}</grid:cell>
					<grid:cell>${item.payNumber}</grid:cell>
<%-- 					<grid:cell><span class="recconfirmstate" recconfirmstate="${item.recConfirmState}"></span></grid:cell>
					<grid:cell><span class="processstate" processstate="${item.processState}"></span></grid:cell> --%>
					<grid:cell>
						<a code="${item.code}" href="#" class="showorderinfo"><fmt:message key="common.list.detail" /></a>
					</grid:cell>
				</grid:body>
				  <tr>
				    <td colspan="11" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but exportexcel" /></td>
				  </tr>
			</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">

$(document).ready(function(){
	
	/**
	 * 初始化订单状态
	 */
	if($(".orderstate").length > 0){
		$(".orderstate").each(function (){
			if($(this).attr("orderstate") == "0") $(this).text("<fmt:message key="memberman.customman.canceled" />");
			if($(this).attr("orderstate") == "1") $(this).text("<fmt:message key="memberman.customman.valid" />");
		});
	}

	if($(".orderpaystate").length > 0){
		$(".orderpaystate").each(function (){
			if($(this).attr("orderpaystate") == "0") $(this).text("<fmt:message key="orderman.list.nopay" />");
			if($(this).attr("orderpaystate") == "1") $(this).text("<fmt:message key="orderman.list.payed" />");
			if($(this).attr("orderpaystate") == "2") $(this).text("已退款");
			if($(this).attr("orderpaystate") == "3") $(this).text("已回款");
		});
	}

	if($(".recconfirmstate").length > 0){
		$(".recconfirmstate").each(function (){
			if($(this).attr("recconfirmstate") == "0") $(this).text("<fmt:message key="orderman.list.noconfirm" />");
			if($(this).attr("recconfirmstate") == "1") $(this).text("<fmt:message key="orderman.list.confirmed" />");
		});
	}

	if($(".processstate").length > 0){
		$(".processstate").each(function (){
			if($(this).attr("processstate") == "0") $(this).text("<fmt:message key="orderman.list.nodeal" />");
			if($(this).attr("processstate") == "1") $(this).text("<fmt:message key="orderman.list.dealed" />");
		});
	}
	
	/**
	 * 初始化产品型号选中状态
	 */
	if($(".protypecode").length > 0){
		$(".protypecode").find("option").each(function (){
		if($(this).val() == $(".protypecode").attr("protypecode")) $(this).attr("selected", true);
		});
	}
	
	if($(".orderstatesel").length > 0){
		$(".orderstatesel").find("option").each(function (){
		if($(this).val() == $(".orderstatesel").attr("orderstatesel")) $(this).attr("selected", true);
		});
	}
	
	if($(".orderpaystatesel").length > 0){
		$(".orderpaystatesel").find("option").each(function (){
		if($(this).val() == $(".orderpaystatesel").attr("orderpaystatesel")) $(this).attr("selected", true);
		});
	}
	
	if($(".recconfirmstatesel").length > 0){
		$(".recconfirmstatesel").find("option").each(function (){
		if($(this).val() == $(".recconfirmstatesel").attr("recconfirmstatesel")) $(this).attr("selected", true);
		});
	}
	
	if($(".processstatesel").length > 0){
		$(".processstatesel").find("option").each(function (){
		if($(this).val() == $(".processstatesel").attr("processstatesel")) $(this).attr("selected", true);
		});
	}
	
	$(".showorderinfo").click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		showOrderInfo(code);
	});
	
	$(".exportexcel").click(function(){
		var $this = $(this);
		var contextpath = $("#contextpath").val();
		$("#f1").attr("action",contextpath+"/orderInfoExportExcel.do").submit();
	});
});



//修改经销商销售信息方法
function showOrderInfo(code)
{
	$("#code").attr("value",code);
	$("#f1").attr("action","showorderinfo.do").submit();
}


	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		$("#submit_but").click(function(){
			//搜索
			$("#f1").attr("action","listorderinfo.do").submit();
		});
	});
</script>
