<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecard.infoman" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/trade/reChargeCardInfo.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/multiSelect.js"></script>


<script type="text/javascript">
var isused_str = '<fmt:message key="trademan.rechargecard.isused" />';
var nouse_str = '<fmt:message key="trademan.rechargecard.nouse" />';
var noactivated_str = '<fmt:message key="memberman.customman.noactivated" />';
var activated_str = '<fmt:message key="memberman.customman.activated" />';
var activate_str = '<fmt:message key="trademan.rechargecard.activate" />';
var del_str = '<fmt:message key="common.list.del" />';
var message1 = '<fmt:message key="trademan.rechargecard.message1" />';
var message2 = '<fmt:message key="trademan.rechargecard.message2" />';
var message3 = '<fmt:message key="trademan.rechargecard.message3" />';
var message4 = '<fmt:message key="trademan.rechargecard.message4" />';
var message5 = '<fmt:message key="trademan.rechargecard.message5" />';
var message6 = '<fmt:message key="trademan.rechargecard.message6" />';
var message7 = '<fmt:message key="trademan.rechargecard.message7" />';
</script>

</head>

<body>
<form action="listrechargecardinfo.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; <fmt:message key="trademan.rechargecard.title" /> &gt; <fmt:message key="trademan.rechargecard.infoman" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="rechargecard.nfo.list.jsp.title" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="trademan.rechargecard.serialnum" />:</td>
						<td width="154"><input name="reChargeCardInfoSel.serialNo" value="${reChargeCardInfoSel.serialNo}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="trademan.rechargecard.rechargecardtype" />:</td>
						<td width="154"><input name="reChargeCardInfoSel.reChargeCardTypeName" value="${reChargeCardInfoSel.reChargeCardTypeName}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="trademan.rechargecard.activatestate" />:</td>
						<td>
							<select class="act_zt isactivesel" name="reChargeCardInfoSel.isActive" isactivesel="${reChargeCardInfoSel.isActive}">
								<option value="">--<fmt:message key="orderman.list.all" />--</option>
								<option value="1"><fmt:message key="memberman.customman.noactivated" /></option>
								<option value="2"><fmt:message key="memberman.customman.activated" /></option>
							</select>
						</td>
					</tr>
					<tr height="38">
						<td align="right"><fmt:message key="trademan.rechargecard.usestate" />:</td>
						<td>
							<select class="act_zt isusesel" name="reChargeCardInfoSel.isUse" isusesel="${reChargeCardInfoSel.isUse}">
								<option value="">--<fmt:message key="orderman.list.all" />--</option>
								<option value="1"><fmt:message key="trademan.rechargecard.nouse" /></option>
								<option value="2"><fmt:message key="trademan.rechargecard.isused" /></option>
							</select>
						</td>
						<td align="right"><fmt:message key="trademan.rechargecard.validate" />:</td>
						<td width="180px">
							<input name="reChargeCardInfoSel.beginDate" value="${reChargeCardInfoSel.beginDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="reChargeCardInfoSel.endDate" value="${reChargeCardInfoSel.endDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
						</td>
						<td>&nbsp;</td>
						<td>
							<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
						<input name="" type="button" value="<fmt:message key="common.btn.excelimport" />" class="search_but_fb createrechargecardinfo" />&nbsp;&nbsp;
						<input name="" type="button" value="<fmt:message key="common.btn.excelimporttemplate" />" class="search_but_fb"
						onclick="jump('${ctx}/download.do?downloadPath=/template/importrechargecard.xls')" />
					</span><fmt:message key="rechargecard.nfo.list.jsp.prepaid.card.information" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<%-- <grid:cell width="30px"><input type="checkbox" id="allCheckbox" name="allCheckbox" /></grid:cell> --%>
					<grid:cell width="16%"><fmt:message key="trademan.rechargecard.serialnum" /></grid:cell>
					<grid:cell width="16%"><fmt:message key="trademan.rechargecard.typename" /></grid:cell>
					<grid:cell width="16%"><fmt:message key="trademan.rechargecard.validate" /></grid:cell>
					<grid:cell width="16%"><fmt:message key="trademan.rechargecard.activatestate" /></grid:cell>
					<grid:cell width="16%"><fmt:message key="trademan.rechargecard.usestate" /></grid:cell>
					<grid:cell width="16%">出货经销商</grid:cell>
					<%-- <grid:cell><fmt:message key="common.list.operation" /></grid:cell> --%>
				</grid:header>
				<c:forEach items="${webpage.result}" var="item">
					<tr code="${item.code}" isactive="${item.isActive}" isuse="${item.isUse}" isOverdue="${item.isOverdue}">
					<%-- 	<td><input type="checkbox" id="checkboxcode_${item.code}" name="checkboxcode" value="${item.code}" /></td> --%>
						<td>${item.serialNo}</td>
						<td>${item.reChargeCardTypeName}</td>
						<td class="validdate">${item.validDate}</td>
						<td><span class="isactive" /></td>
						<td><span class="isuse" /></td>
						<td>${item.sealerAutelId }</td>
					<%-- 	<td class="operate">
							<a href="#" class="activerechargecardinfo"><fmt:message key="trademan.rechargecard.activate" /></a>&nbsp;
							<a href="#" class="dellrechargecardinfo"><fmt:message key="common.list.del" /></a>
						</td> --%>
					</tr>
				</c:forEach>
		
				<%-- <tr height="38px">
					<th colspan="7" align="left">
						&nbsp;<input type="checkbox" id="allCheckbox2" name="allCheckbox" />&nbsp;<fmt:message key="common.list.all" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input class="diw_but_sh activerechargecardinfos" type="button" value="<fmt:message key="trademan.rechargecard.activate" />" />
					</th>
				</tr> --%>
				
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>


<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});

	function jump(url){
		var e=document.createElement("a");
		e.href=url;
		document.body.appendChild(e);
		e.click();
	}
</script>
