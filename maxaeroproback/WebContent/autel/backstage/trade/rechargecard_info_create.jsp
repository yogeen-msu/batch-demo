<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecard.importinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/trade/reChargeCardInfo.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/selectLocationByFPY.js"></script>

</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; <fmt:message key="trademan.rechargecard.title" /> &gt; <fmt:message key="trademan.rechargecard.importinfo" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="trademan.rechargecard.importinfo" />
			</h4>
	<form action="savecreaterechargecard.do"  class="validate" method="post" name="f1" id="f1" enctype="multipart/form-data">
	<iframe name='hidden_frame' id="hidden_frame" style='display:none'></iframe>
	
		<input type="hidden" name="reChargeCardInfoSel.serialNo" value="${reChargeCardInfoSel.serialNo}" />
		<input type="hidden" name="reChargeCardInfoSel.reChargeCardTypeName" value="${reChargeCardInfoSel.reChargeCardTypeName}" />
		<input type="hidden" name="reChargeCardInfoSel.isUse" value="${reChargeCardInfoSel.isUse}" />
		<input type="hidden" name="reChargeCardInfoSel.beginDate" value="${reChargeCardInfoSel.beginDate}" />
		<input type="hidden" name="reChargeCardInfoSel.endDate" value="${reChargeCardInfoSel.endDate}" />
		<input type="hidden" name="reChargeCardInfoSel.isActive" value="${reChargeCardInfoSel.isActive}" />
		
		<table class="form-table">

			<tr>
				<th><label class="text"><fmt:message key="trademan.rechargecard.rechargecardtype" />:</label></th>
				<td>
					<select name="reChargeCardInfoAdd.reChargeCardTypeCode" isrequired="true">
						<option value="">--<fmt:message key="common.sel.choose" />--</option>
						<c:forEach items="${reChargeCardTypes }" var="item">
							<option value="${item.code}">${item.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			
			<tr>
				<th><label class="text">经销商编号:</label></th>
				<td>
					<input type="text" name="reChargeCardInfoAdd.sealerAutelId" isrequired="true" maxlength="100"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="trademan.rechargecard.excel" />:</label></th>
				<td>
					<input type="file" class="rechargecardfile" name="reChargeCardFile" dataType="string" isrequired="true"/>
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="" type="button" value="<fmt:message key="common.btn.ok" />" class="submitBtn savecreaterechargecard" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
</body>
</html>



<script type="text/javascript">
$("form.validate").validate();

function callback(result)
{
	document.f1.target="_self";
	var msg;
	
	if(result==0)
	{
		msg = "数据导入成功";
		alert(msg);
		document.f1.action="listrechargecardinfo.do";
		document.f1.submit();
	}
	else
	{
		msg = "数据导入失败";
		alert(msg);
	}
}

</script>


