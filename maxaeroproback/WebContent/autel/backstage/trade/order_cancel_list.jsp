<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.paylogman" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>


</head>

<body>
<form action="listordercancel.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; <fmt:message key="trademan.logman" /> &gt; 付款取消管理
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;付款取消日志
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="orderman.list.ddh" />:</td>
						<td width="154"><input name="orderCancelLog.orderCode" value="${orderCancelLog.orderCode}" type="text" class="time_in" /></td>
						<td width="111" align="right">序列号:</td>
						<td width="154">
						<input name="orderCancelLog.proSreialNo" value="${orderCancelLog.proSreialNo}" type="text" class="time_in" />
						</td>
						<td width="111" align="right">Autel ID:</td>
						<td>
							<input name="orderCancelLog.custAutelId" value="${orderCancelLog.custAutelId}" type="text" class="time_in" />
						</td>
					</tr>
			
					<tr height="38" >
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							&nbsp;&nbsp;<input type="button" id="submit_but" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
						<td></td>
						<td></td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					<input id="addOrderCancelLog" name="" type="button" value=<fmt:message key="common.btn.add" /> class="search_but addrechargecardtype" />
					</span>付款取消日志:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="11%"><fmt:message key="orderman.list.ddh" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="trademan.paylog.paymember" /></grid:cell>
					<grid:cell width="11%">序列号</grid:cell>
					<grid:cell width="11%">状态</grid:cell>
					<grid:cell width="11%">操作时间</grid:cell>
					<grid:cell width="11%">操作人</grid:cell>
					<grid:cell width="11%">操作原因</grid:cell>
					<grid:cell width="9%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.orderCode}</grid:cell>
					<grid:cell>${item.custAutelId}</grid:cell>
					<grid:cell>${item.proSreialNo}</grid:cell>
					<grid:cell>
					<c:if test="${item.status == 0}">已解除</c:if>
					<c:if test="${item.status == 1}">黑名单</c:if>
					</grid:cell>
					<grid:cell>${item.operDate}</grid:cell>
					<grid:cell>${item.operUser}</grid:cell>
					<grid:cell>${item.operReason}</grid:cell>
					<grid:cell>
					 <c:if test="${item.status == 1}">
					 	<a code="${item.code}" href="cancelOrderCancelLog.do?orderCancelLog.id=${item.id }" class="showorderinfo">解除</a>
					 </c:if>
					</grid:cell>
				</grid:body>
			</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">

$(document).ready(function(){
	
	$("#addOrderCancelLog").click(function(){
		var $this = $(this);
		$("#f1").attr("action","addOrderCancelLog.do").submit();
	});
});
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		$("#submit_but").click(function(){
			//搜索
			$("#f1").attr("action","listordercancel.do").submit();
		});
	});
</script>
