<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.paylog.paylogdetail" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>


</head>

<body>
<form action="listorderinfo.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; <fmt:message key="trademan.logman" /> &gt; <fmt:message key="trademan.paylog.paylogdetail" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="trademan.paylog.paylogdetail" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="orderman.list.ddh" />:</td>
						<td width="154">${orderInfoVos[0].code}</td>
						<td width="111" align="right"><fmt:message key="trademan.paylog.paymember" />:</td>
						<td width="154">${orderInfoVos[0].autelId}</td>
						<td width="111" align="right"><fmt:message key="orderman.list.ordertime" />:</td>
						<td width="154">${orderInfoVos[0].orderDate}</td>
						<td width="111" align="right"><fmt:message key="orderman.list.money" />:</td>
						<td width="154">${orderInfoVos[0].orderMoney}</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.customman.orderstate" />:</td>
						<td width="154"><span class="orderstate" orderstate="${orderInfoVos[0].orderState}"></span></td>
						<td width="111" align="right"><fmt:message key="orderman.list.paystate" />:</td>
						<td width="154"><span class="orderpaystate" orderpaystate="${orderInfoVos[0].orderPayState}"></span></td>
						<td width="111" align="right"><fmt:message key="orderman.list.paydate" />:</td>
						<td width="154">${orderInfoVos[0].payDate}</td>
						<td width="111" align="right"><fmt:message key="trademan.paylog.bankflownum" />:</td>
						<td width="154">${orderInfoVos[0].payNumber}</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="orderman.list.getmoneyconfirmstate" />:</td>
						<td width="154"><span class="recconfirmstate" recconfirmstate="${orderInfoVos[0].recConfirmState}"></span></td>
						<td width="111" align="right"><fmt:message key="orderman.list.payconfirmdate" />:</td>
						<td width="154">${orderInfoVos[0].recConfirmDate}</td>
						<td width="111" align="right"><fmt:message key="orderman.list.dealstate" />:</td>
						<td width="154"><span class="processstate" processstate="${orderInfoVos[0].processState}"></span></td>
						<td width="111" align="right"><fmt:message key="orderman.list.dealdate" />:</td>
						<td width="154">${orderInfoVos[0].processDate}</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="trademan.paylog.buydetail" />:
				</h4>
			</div>
		</div>
		<div>
		<table width="100%" border="0" cellpadding="1" cellspacing="1">
			<c:forEach items="${orderInfoVos}" var="item">
					<tr height="1" bgcolor="black">
						<td width="3"></td>
						<td colspan="4"></td>
						<td width="3"></td>
					</tr>
					<tr height="38" align="left">
						<td>&nbsp;</td>
						<td><fmt:message key="product.producttypevo" />: ${item.proTypeName}</td>
						<td><fmt:message key="memberman.complaintman.productserial" />:${item.productSerialNo}</td>
						<td><fmt:message key="marketman.salecontractman.name" />:${item.saleContractName}</td>
						<td><fmt:message key="memberman.customman.minsaleunitname" />: ${item.minSaleUnitName}</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38" align="left">
						<td>&nbsp;</td>
						<td><fmt:message key="trademan.paylog.cunsumetype" />:
						<c:if test="${item.consumeType == 0}"><fmt:message key="orderman.list.gm" /></c:if>
						<c:if test="${item.consumeType == 1}"><fmt:message key="orderman.list.xf" />（${item.year}<fmt:message key="marketman.minusalenitman.year" />）</c:if>
						</td>
						<td><fmt:message key="trademan.paylog.price" />:${item.minSaleUnitPrice}</td>
						<td><fmt:message key="trademan.rechargecard.validate" />:${item.validDate}</td>
						<td><fmt:message key="trademan.rechargecard.sealer" />:${item.sealerName}</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="1" bgcolor="black">
						<td></td>
						<td colspan="4"></td>
						<td></td>
					</tr>
			</c:forEach>
		</table>
		</div>
		<br>
		<input name="button" type="button" onclick="jump('listorderinfo.do')" value="<fmt:message key="common.btn.return" />" class="diw_but" />
	</div>
</form>
</body>
</html>

<script type="text/javascript">
function jump(url){
	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
   
}
$(document).ready(function(){
	
	/**
	 * 初始化订单状态
	 */
	if($(".orderstate").length > 0){
		$(".orderstate").each(function (){
			if($(this).attr("orderstate") == "0") $(this).text("<fmt:message key="memberman.customman.canceled" />");
			if($(this).attr("orderstate") == "1") $(this).text("<fmt:message key="memberman.customman.valid" />");
		});
	}

	if($(".orderpaystate").length > 0){
		$(".orderpaystate").each(function (){
			if($(this).attr("orderpaystate") == "0") $(this).text("<fmt:message key="orderman.list.nopay" />");
			if($(this).attr("orderpaystate") == "1") $(this).text("<fmt:message key="orderman.list.payed" />");
		});
	}

	if($(".recconfirmstate").length > 0){
		$(".recconfirmstate").each(function (){
			if($(this).attr("recconfirmstate") == "0") $(this).text("<fmt:message key="orderman.list.noconfirm" />");
			if($(this).attr("recconfirmstate") == "1") $(this).text("<fmt:message key="orderman.list.confirmed" />");
		});
	}

	if($(".processstate").length > 0){
		$(".processstate").each(function (){
			if($(this).attr("processstate") == "0") $(this).text("<fmt:message key="orderman.list.nodeal" />");
			if($(this).attr("processstate") == "1") $(this).text("<fmt:message key="orderman.list.dealed" />");
		});
	}
});
</script>

