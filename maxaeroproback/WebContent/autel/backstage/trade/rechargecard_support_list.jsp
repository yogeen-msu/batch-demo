<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecard.typeman" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/trade/reChargeCardType.js"></script>

<script type="text/javascript">
var message1 = '<fmt:message key="memberman.rechargecardtype.message1" />';
var message2 = '<fmt:message key="memberman.rechargecardtype.message2" />';
</script>


</head>

<body>
<form action="listrechargesupport.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; 日志管理 &gt; 升级卡支持日志
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;升级卡支持日志
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right">升级卡卡号:</td>
						<td width="154"><input name="cardSupport.cardSN" value="${cardSupport.cardSN}" type="text" class="time_in" /></td>
						<td width="111" align="right">升级卡密码:</td>
						<td width="154"><input name="cardSupport.cardPwd" value="${cardSupport.cardPwd}" type="text" class="time_in" /></td>
						<td>
							&nbsp;&nbsp;<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
						
					</span>升级卡支持列表:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="13%">产品序列号</grid:cell>
					<grid:cell width="10%">客户姓名</grid:cell>
					<grid:cell width="15%">客户Email</grid:cell>
					<grid:cell width="13%">升级卡卡号</grid:cell>
					<grid:cell width="12%">升级卡密码</grid:cell>
					<grid:cell width="12%">创建时间</grid:cell>
					<grid:cell width="8%">状态</grid:cell>
					<grid:cell width="11%">操作</grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.productSN}</grid:cell>
					<grid:cell>${item.userName}</grid:cell>
					<grid:cell>${item.email}</grid:cell>
					<grid:cell>${item.cardSN}</grid:cell>
					<grid:cell>${item.cardPwd}</grid:cell>
					<grid:cell>${item.createTime}</grid:cell>
					<grid:cell>
					<c:if test="${item.status == 0}">
					   未处理
					</c:if>
					<c:if test="${item.status == 1}">
					   已处理
					</c:if>
					</grid:cell>
					<grid:cell>
					<c:if test="${item.status == 0}">
					<a href="updateReChargeSupport.do?cardSupport.code=${item.code}">改变状态</a>
					</c:if>
					</grid:cell>
				</grid:body>
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>

