<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecardtype.modtype" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/trade/reChargeCardType.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; <fmt:message key="trademan.rechargecard.title" /> &gt; <fmt:message key="trademan.rechargecardtype.modtype" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="trademan.rechargecardtype.modtype" />
			</h4>
	<form action="updaterechargecardtype.do"  class="validate" method="post" name="f1" id="f1">
	
		<input type="hidden" name="reChargeCardTypeEdit.code" id="reChargeCardTypeEdit.code" value="${reChargeCardTypeEdit.code}" />
		<input type="hidden" name="selectedcodes" id="selectedcodes" value="${selectedcodes}" />
		<input type="hidden" name="reChargeCardTypeSel.name" value="${reChargeCardTypeSel.name}" />
		<input type="hidden" name="reChargeCardTypeSel.proTypeName" value="${reChargeCardTypeSel.proTypeName}" />
		
		<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="trademan.rechargecard.typename" />:</label></th>
				<td>
					<input style="width:300px" type="text" name="reChargeCardTypeEdit.name" value="${reChargeCardTypeEdit.name}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>

			<tr>
				<th><label class="text"><fmt:message key="product.producttypevo" />:</label></th>
				<td>
					<select style="width:305px" class="protypecode" name="reChargeCardTypeEdit.proTypeCode" protypecode="${reChargeCardTypeEdit.proTypeCode}" isrequired="true" onchange="selectContract(this)">
						<option value="">--<fmt:message key="common.sel.choose" />--</option>
						<c:forEach items="${productTypes }" var="item">
							<option value="${item.code}">${item.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<th><label class="text"><fmt:message key="rechargecard.type.list.jsp.sales.contract.name" />:</label></th>
				<td>
					<select style="width:305px" class="salecontractcode" name="reChargeCardTypeEdit.saleContractCode" id="SaleContractCode" salecontractcode="${reChargeCardTypeEdit.saleContractCode}" isrequired="true">
						<option value="">--<fmt:message key="common.sel.choose" />--</option>
						<c:forEach items="${saleContracts }" var="item">
							<option value="${item.code}">${item.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
				
					<tr>
							<th><label class="text"><fmt:message key="syssetting.areaconfig" />:</label></th>
							<td><select style="width:305px" name="reChargeCardTypeAdd.areaCfgCode" id='areaCfgCode'
								class="areacfgcode" isrequired="true" areacfgcode="${reChargeCardTypeEdit.areaCfgCode}">
									<option value="">-- <fmt:message key="select.option.select" /> --</option>
									<c:forEach items="${areaConfigs }" var="item">
										<option value="${item.code }">${item.name }</option>
									</c:forEach>
							</select>
							</td>
						</tr>
				
				
			<tr>
				<th><label class="text"><fmt:message key="trademan.rechargecardtype.money" />:</label></th>
				<td>
					<input style="width:300px" type="text" name="reChargeCardTypeEdit.price" value="${reChargeCardTypeEdit.price}" maxlength="40" dataType="float"/>
				</td>
			</tr>
			
			
			<tr>
				<th><label class="text"><fmt:message key="recharge.type.month" />:</label></th>
				<td>
					<input style="width:300px" type="text" name="reChargeCardTypeAdd.month" value="${reChargeCardTypeEdit.month}" maxlength="40" dataType="float" isrequired="true"/>
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="submit" type="submit" value="<fmt:message key="emption.add.jsp.button.save" />" class="submitBtn updaterechargecardtype" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
</body>
</html>

<script type="text/javascript">
$("form.validate").validate();
</script>

