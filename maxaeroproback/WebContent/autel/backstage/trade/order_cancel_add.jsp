<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecardtype.addtype" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; <fmt:message key="trademan.logman" /> &gt; 付款取消日志
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;添加付款取消日志
			</h4>
	<form action="saveOrderCancelLog.do"  class="validate" method="post" name="f1" id="f1">
		<input type="hidden" name="reChargeCardTypeSel.name" value="${reChargeCardTypeSel.name}" />
		<input type="hidden" name="reChargeCardTypeSel.proTypeName" value="${reChargeCardTypeSel.proTypeName}" />
		
		<table class="form-table">
				
			<tr>
				<th><label class="text">订单号:</label></th>
				<td>
					<input style="width:300px" type="text" name="orderCancelLog.orderCode" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>

			
			
			<tr>
				<th><label class="text">取消原因:</label></th>
				<td>
					<textarea name="orderCancelLog.operReason" rows="8" cols="37" dataType="string" isrequired="true"></textarea>
				</td>
			</tr>
			
			
			
			
			
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="submit" type="submit" value="<fmt:message key="emption.add.jsp.button.save" />" class="submitBtn insertrechargecardtype" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
</body>
</html>

<script type="text/javascript">
$("form.validate").validate();
$(document).ready(function(){
	$(".insertrechargecardtype").click(function(){
		$("#f1").attr("action","saveOrderCancelLog.do").submit();
	});
});

</script>
