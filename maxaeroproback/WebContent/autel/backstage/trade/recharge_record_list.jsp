<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecardlog" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/trade/reChargeRecord.js"></script>


</head>

<body>
<form action="listrechargerecord.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="trademan.title" /> &gt; <fmt:message key="trademan.logman" /> &gt; <fmt:message key="trademan.rechargecardlog" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="trademan.rechargecardlog" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="trademan.rechargecard.serialnum" />:</td>
						<td width="154"><input name="reChargeRecordSel.reChargeCardSerialNo" value="${reChargeRecordSel.reChargeCardSerialNo}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="154"><input name="reChargeRecordSel.proTypeName" value="${reChargeRecordSel.proTypeName}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td width="154"><input name="reChargeRecordSel.proSerialNo" value="${reChargeRecordSel.proSerialNo}" type="text" class="time_in" /></td>
					</tr>
					<tr height="38">
						<td width="111" align="right">统计时间段:</td>
						<td width="154">
						
							<input name="reChargeRecordSel.startDate" value="${reChargeRecordSel.startDate}" type="text" dataType="date" class="dateinput" style="width:70px" readonly="readonly" />
							--
							<input name="reChargeRecordSel.endDate" value="${reChargeRecordSel.endDate}" type="text" dataType="date" class="dateinput" style="width:70px" readonly="readonly" />
						</td>
						<td width="111" align="right"><fmt:message key="memberman.customman.userid" />:</td>
						<td width="154"><input name="reChargeRecordSel.memberAutelId" value="${reChargeRecordSel.memberAutelId}" type="text" class="time_in" /></td>
						<td>
							&nbsp;&nbsp;<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="trademan.rechargecardlog" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="12%"><fmt:message key="trademan.rechargecard.serialnum" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="trademan.rechargecard.rechargecardtype" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="trademan.rechargecard.membertype" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.userid" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="trademan.rechargecard.relettime" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="product.producttypevo" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="recharge.record.list.jsp.sales.configuration" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="syssetting.areaconfig.areaconfigname" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.reChargeCardSerialNo}</grid:cell>
					<grid:cell>${item.reChargeCardTypeName}</grid:cell>
					<grid:cell>${item.proSerialNo}</grid:cell>
					<grid:cell><span class="membertype" membertype="${item.memberType}"></span></grid:cell>
					<grid:cell>${item.memberAutelId}</grid:cell>
					<grid:cell>${item.reChargeTime}</grid:cell>
					<grid:cell>${item.proTypeName}</grid:cell>
					<grid:cell>${item.saleCfgName}</grid:cell>
					<grid:cell>${item.areaCfgName}</grid:cell>
				</grid:body>
				
				 <tr>
				    <td colspan="11" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but exportexcel" /></td>
				  </tr>
				
			</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">


	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		
		$(".exportexcel").click(function(){
			var $this = $(this);
			var contextpath = $("#contextpath").val();
			$("#f1").attr("action",contextpath+"/rechargeRecordExportExcel.do").submit();
			$("#f1").attr("action","listrechargerecord.do");
			
		});
		
		
	});
</script>
