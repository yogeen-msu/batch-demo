<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
      <tbody>        
          <tr>
            <th><span><fmt:message key="homepage.customercomplaint.total" />:</span></th>
            <td><span>${ complaintCount.all}<fmt:message key="homepage.customercomplaint.records" /></span></td>
          </tr>
          <tr>
            <th><span><fmt:message key="homepage.customercomplaint.open" />:</span></th>
            <td><span>${ complaintCount.open}<fmt:message key="homepage.customercomplaint.records" /></span></td>
          </tr>
          <tr>
            <th><span><fmt:message key="homepage.customercomplaint.hangup" />:</span></th>
            <td><span>${ complaintCount.guaqi}<fmt:message key="homepage.customercomplaint.records" /></span></td>
          </tr>
          <tr>
            <th><span><fmt:message key="homepage.customercomplaint.close" />:</span></th>
            <td><span>${ complaintCount.close}<fmt:message key="homepage.customercomplaint.records" /></span></td>
          </tr>
          <tr>
            <th align="left"><span><fmt:message key="homepage.customercomplaint.return" />:</span></th>
            <td><span>${ complaintCount.wait}<fmt:message key="homepage.customercomplaint.records" /></span></td>
          </tr>          
       </tbody>
</table>