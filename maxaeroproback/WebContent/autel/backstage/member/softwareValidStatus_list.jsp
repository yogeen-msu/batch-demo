<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<link href="../../autel/main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../autel/backstage/system/js/selfdialog.js"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> > <fmt:message key="memberman.customman.title" /> > <fmt:message key="memberman.customman.softwarevalidate" /></div>

<form method="post" id="productValid" action="product-software-valid-status!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="memberman.customman.softwarevalidate" /></h4>
			<div class="sear_table">
				<table width="100%">
					  <tr height="38">    
						<td width="111" align="right"><fmt:message key="memberman.customman.customerid" />:</td>
						<td width="154"><input type="text"  name="softwareValidStatus.cusAutelid" value="${softwareValidStatus.cusAutelid}" /></td>
						<td></td>
						<td width="111"><fmt:message key="memberman.sealer.jxsid" />:</td>
						<td width="154" align="right"><input type="text" name="softwareValidStatus.sealAutelid" value="${softwareValidStatus.sealAutelid}" /></td>
						<td></td>
					  </tr>
			   		<tr height="38">    
					   <td width="111" align="right"><fmt:message key="syssetting.areaconfig" />:</td>
						<td width="154"><input type="text" name="softwareValidStatus.areaConfig.name" value="${softwareValidStatus.areaConfig.name}" /></td>
						<td></td>
						<td width="80px"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td width="154" align="right"><input type="text" name="softwareValidStatus.productInfo.serialNo" value="${softwareValidStatus.productInfo.serialNo}" /></td>
						<td align="left">
					 		&nbsp;&nbsp;
					 		<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
					 		<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
					 	</td>
					  </tr>
				</table>
			</div>
			<div class="active">
				<h4><fmt:message key="memberman.customman.softwarevalidate" /></h4>
			</div>
		</div>
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell><fmt:message key="memberman.customman.customerid" /></grid:cell>
				<grid:cell><fmt:message key="memberman.sealer.jxsid" /></grid:cell>
				<grid:cell><fmt:message key="rechargecard.type.list.jsp.sales.contract.name" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.areaconfig" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.exptime" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${item.cusAutelid}</grid:cell>
				<grid:cell>${item.sealAutelid}</grid:cell>
				<grid:cell>${item.saleContract.name}</grid:cell>
				<grid:cell>${item.areaConfig.name}</grid:cell>
				<grid:cell>${item.productInfo.serialNo}</grid:cell>
				<grid:cell id="Tr_${item.id}" >${item.validDate}</grid:cell>
				<grid:cell>
					<a href="#" class="validateSoftItem" itemDate="${item.validDate}" itemid="${item.productInfo.serialNo}"  >
						<fmt:message key="memberman.customman.setvalidate" />
					</a>
				</grid:cell>
			</grid:body>
		</grid:grid>
	</div>
</form>

<!-- <fmt:message key="syssetting.toolman.name" />选择 -->
<div id="validateEditDiv" class="active">
	<div class="dialog_overlay"></div>
	<div class="dialog_win">
		<h2><span class="dialog_close">×</span></h2>
		<div class="tc_tab">
			<form class="validate" method="post" id="softwareEditForm">
				<input type="hidden" name="productSN" id="validateItemSN"/>
				<input type="hidden" name="oldValidDate" id="validateItemDate"/>
				<label class="text"><fmt:message key="memberman.customman.validate" />:</label>
				<input type="text" name="validDate" id="validDateItemValue" dataType="date" class="dateinput"  style="height: 22px"  isrequired="true" />
				<input type="button"  value=<fmt:message key="common.btn.ok" /> class="search_but"  id="saveEditBtn"/>
				<div style="height:30px">&nbsp;</div>
			</form>
			
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		
		$("form.validate").validate();
		
		//
		var beforeEvent = function(evenEle){
			var itemid = $(evenEle).attr("itemid");
			var itemDate=$(evenEle).attr("itemDate");
			$("#validateItemSN").val(itemid);
			$("#validateItemDate").val(itemDate);
		};
		
		var validateReset = function(){
			$("#validateItemId").val("");
			$("#validateItemDate").val("");
			$("#validDateItemValue").val("");
		};
		
		//<fmt:message key="memberman.customman.validate" />设置
		var validateEditDlg = new selfOpenDialog({
			id:"validateEditDiv",
			eventEle:$(".validateSoftItem"),
			beforeEvent:beforeEvent,
			callback:validateReset});

		
		//保存
		$("#saveEditBtn").click(function(){
			if( !$("#softwareEditForm").checkall() ){
				return ;
			}
			
			//$.Loading.show('正在保存，请稍侯...');
			$.Loading.show('<fmt:message key="syssetting.toolman.urimapping.saving" />');
			var options = {
					url :"product-software-valid-status!changeValidDate.do?ajax=yes",
					type : "POST",
				//	data: $('#softwareEditForm').serialize(), // 从表单中获取数据
					dataType : 'json',
					success : function(result) {	
						
						$.Loading.hide();
					 	if(result.result==1){
					 		//$("#Tr_"+result.id).text(result.date);
					 		alert("<fmt:message key="market.mainsale.message4" />");
					 		$('#productValid').submit();
					 	}else{
					 		alert(result.message);
					 	}
					},
					error : function(e) {
						$.Loading.hide();
						alert("error:(");
	 				}
	 		};
	 
			$("#softwareEditForm").ajaxSubmit(options);	
			validateEditDlg.close();
		});
						
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
		});
	});
</script>
