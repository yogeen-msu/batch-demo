<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.onlineman.authority" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/onlineRole.js"></script>


<script type="text/javascript">
var message3 = '<fmt:message key="memberman.complaintman.message3" />';
var message2 = '<fmt:message key="memberman.complaintman.message2" />';
var message1 = '<fmt:message key="memberman.onlineman.message1" />';
</script>


</head>

<body>
<form action="listonlinerole.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.onlineman.title" /> &gt; <fmt:message key="memberman.onlineman.authority" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.onlineman.authority" />
			</h4>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="30%"><fmt:message key="memberman.onlineman.type" /></grid:cell>
					<grid:cell width="50%"><fmt:message key="memberman.complaintman.acceptman" /></grid:cell>
					<grid:cell width="20%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.onlineTypeName}</grid:cell>
					<grid:cell style="word-wrap:break-word;word-break:break-all;">${item.adminUserids}</grid:cell>
					<grid:cell>
						<a class="allotonline" code="${item.onlineTypeCode}" adminuserids="${item.adminUserids}" href="#"><fmt:message key="memberman.complaintman.modacceptman" /></a>
					</grid:cell>
				</grid:body>
			</grid:grid>
		</div>
	</div>
	
	<!-- 弹开窗口 -->
	<div id="overlay"></div>
	
	<div id="win_online">
	   	<h2><span id="close">×</span></h2>
	   	<div style="max-height: 450px;overflow: auto;border: 1px;">
			<div class="tc_tab_ts">
			<table width="100%" border="0">
			  <tr>
			    <td width="45%" align="center"><fmt:message key="memberman.complaintman.customserviceid" /></td>
			    <td width="45%" align="center"><fmt:message key="syssetting.authorityman.managername" /></td>
			    <td width="10%" align="center"><fmt:message key="common.openselect.choose" /></td>
			  </tr>
			  <c:forEach items="${customerServiceUsers}" var="item">
			  <tr align="center">
			    <td>${item.username}</td>
			    <td>${item.realname}</td>
			    <td><input type="checkbox" name="acts" value="${item.username}" /></td>
			  </tr>
			  </c:forEach>
			</table>
			</div>
		</div>
		<div style="width: 100%;height: 50px;text-align: center;vertical-align:middle;line-height:50px;">
			<input type="button" class="diw_but selacceptor" value="<fmt:message key="common.btn.ok" />" />&nbsp;&nbsp;
			<input type="button" class="diw_but closewin" value="<fmt:message key="common.btn.close" />" />
		</div>
	</div>
</form>
</body>
</html>
