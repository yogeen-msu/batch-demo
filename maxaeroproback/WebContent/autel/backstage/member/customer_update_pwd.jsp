<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerInfo.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>
<title><fmt:message key="memberman.customman.cusinfomod" /></title>
</head>
<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="memberman.customman.cusinfomod" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.customman.cusinfomod" />
			</h4>
	<form id="f1" name="f1" class="validate" action="savePwd.do" method="post">
	<input type="hidden" name="customerInfoEdit.code" id="customerInfoEdit.code" value="${customerInfoEdit.code}" />

	
	<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.cusid" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.autelId" value="${customerInfoEdit.autelId}" maxlength="40"  isrequired="true" readonly="readonly"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.passwd" />:</label></th>
				<td>
					<input type="password" name="customerInfoEdit.userPwd" maxlength="40" dataType="string"/>(<fmt:message key="memberman.customman.passwordinfo" />)
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.repasswd" />:</label></th>
				<td>
					<input type="password" name="" maxlength="40" dataType="string" fun="checkpwd"/>
				</td>
			</tr>
				
			
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="submit" type="submit" value="<fmt:message key="common.btn.ok" />" class="submitBtn updatePassword" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">
	$("form.validate").validate();

		function checkpwd(){
			 
			if(  $("input[type='password']").eq(0).val() != $("input[type='password']").eq(1).val() ){
				return "<fmt:message key="memberman.customman.message1" />";
			}
			return true;
		}
	</script>
	
</body>
</html>