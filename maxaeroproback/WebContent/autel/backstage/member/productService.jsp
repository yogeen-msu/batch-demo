<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/artDialog.js?skin=aero"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/plugins/iframeTools.js"></script>
<form method="post" action="product-repair-record!list.do">
		<div class="right_tab" style="padding-right:0px;">
	<div class="grid" style="padding-right:0px;">
		<grid:grid from="webpage" ajax="yes">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.date" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.repairdate" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.brokentype" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.proTypeName} </grid:cell>
				<grid:cell>${item.proSerialNo}</grid:cell>
				<grid:cell>${item.proDate}</grid:cell>
				<grid:cell>${item.repDate}</grid:cell>
				<grid:cell>${item.repType}</grid:cell>
				<grid:cell>
				<a id="viewRepair" href="javascript:openRepair('${item.id}')">
						<fmt:message key="memberman.complaintman.show" />
					</a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
	</div>
</form>

<script type="text/javascript">
function openRepair(id){
	//window.open ('ViewRepair.do?productRepairRecord.id='+id,'viewRepair','height=500,width=700,toolbar=no,menubar=no, resizable=no,location=no, status=no'); 
	art.dialog.open('ViewRepair.do?productRepairRecord.id='+id,
		    {title: ' ', width: 500, height: 600,padding:0,});
}
</script>
