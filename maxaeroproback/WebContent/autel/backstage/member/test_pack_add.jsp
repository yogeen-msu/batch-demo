<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="${ctx}/autel/js/member/testPack.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/testpack.js.jsp?ajax=yes"></script>
<link href="${ctx}/autel/main/css/table.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
var message1 = '<fmt:message key="memberman.testpackman.message1" />';
var message2 = '<fmt:message key="memberman.testpackman.message2" />';
var message3 = '<fmt:message key="memberman.testpackman.message3" />';
var message4 = '<fmt:message key="memberman.testpackman.message4" />';
var message5 = '<fmt:message key="memberman.testpackman.message5" />';
var message6 = '<fmt:message key="memberman.testpackman.message6" />';
var message7 = '<fmt:message key="memberman.testpackman.message7" />';
var message8 = '<fmt:message key="memberman.testpackman.message8" />';
var message9 = '<fmt:message key="memberman.testpackman.message9" />';
var message10 = '<fmt:message key="memberman.testpackman.message10" />';
var message11 = '<fmt:message key="memberman.testpackman.message11" />';
var message12 = '<fmt:message key="memberman.testpackman.message12" />';
var message13 = '<fmt:message key="memberman.testpackman.message13" />';
var message14 = '<fmt:message key="memberman.testpackman.message14" />';
var message15 = '<fmt:message key="memberman.testpackman.message15" />';
var message16 = '<fmt:message key="memberman.testpackman.message16" />';
var message17 = '<fmt:message key="memberman.testpackman.message17" />';
var message18 = '<fmt:message key="memberman.testpackman.message18" />';
</script>
<form action="inserttestpack.do" class="validate" method="post" id="theForm" name="theForm">
<input type="hidden" name="testPackVoSel.name" value="${testPackVoSel.name}">
<input type="hidden" name="testPackVoSel.proTypeName" value="${testPackVoSel.proTypeName}">
<input type="hidden" name="testPackVoSel.proSerialNo" value="${testPackVoSel.proSerialNo}">
<input type="hidden" name="testPackVoSel.softwareName" value="${testPackVoSel.softwareName}">
<input type="hidden" name="testPackVoSel.customerName" value="${testPackVoSel.customerName}">

    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="memberman.complaintman.addtestpack" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.complaintman.addtestpack" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="150" align="right"><fmt:message key="memberman.complaintman.testpackname" />:</td>
							<td width="20"></td>
							<td width="300">
								<input type="text" class="time_in" name="testPackAdd.versionName" dataType="string" isrequired="true" />
							</td>
							<td></td>
						</tr>
						<tr>
							<td align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
							<td></td>
							<td>
								<span><textarea  class="time_in proserialno" style="width:400px;height:150px;"  dataType="string"  name="testPackVo.proSerialNo" isrequired="true" onblur="checkProSerialNo()"></textarea>
							    <span style="color: red"><fmt:message key="test.pack.add.productNote"></fmt:message></span>	&nbsp;&nbsp;&nbsp;
							    <br/><span style="color: red" class="pronovalidity"></span>
							</span>
							</td>
							<td></td>
						</tr>
					
						<tr height="38">
							<td align="right"><fmt:message key="product.software.name" />:</td>
							<td></td>
							<td>
								    <input type="text" readonly value="" name="softwareTypeName" id="softwareTypeName" />
								    <input type="hidden" value="" name="testPackAdd.softwareTypeCode" id="softwareTypeCode"/>
									<input type="button" id="productTypeSelect" value=<fmt:message key="common.openselect.choose" /> class="diw_but">
							</td>
							<td></td>
						</tr>
						
						<tr>
							<td align="right" width="170px"><fmt:message key="memberman.complaintman.basepacklocation" />:</td>
							<td></td>
							<td>
								<input type="text" class="time_in" name="testPackAdd.downloadPath" style="width:400px" dataType="string" isrequired="true" />
							</td>
							<td></td>
						</tr>
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<div id="overlay"></div>
					<div id="win">
						<h2>
							<span id="close">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0">
								<tr>

									<td width="30%" align="right"><fmt:message key="memberman.complaintman.language" />:</td>
									<td><select name="language" id="language"
										class="act_zt_tc">
											<c:forEach items="${ languages}" var="language">
												<option value="${language.code }">${language.name }</option>
											</c:forEach>
									</select></td>
												
								</tr>
								<tr>

									<td width="30%" align="right"><fmt:message key="memberman.complaintman.languagepackloction" />:</td>
									<td><input style="width:400px" name="" type="text" class="time_in_tc"
										id="downloadPath" />
									</td>
								</tr>
								<tr>

									<td width="30%" align="right"><fmt:message key="memberman.complaintman.testcardocloction" />:</td>
									<td><input style="width:400px" name="" type="text" class="time_in_tc"
										id="testFilePath" />
									</td>
								</tr>
								<tr>

									<td width="30%" align="right"><fmt:message key="memberman.complaintman.versiondesc" />:</td>
									<td><textarea style="width:400px;height:150px" name="" cols="" rows="" class="banb_tc"
											id="memo"></textarea>
									</td>
								</tr>
								<tr>

									<td width="30%" align="right">&nbsp;</td>
									<td><input name="" id="finsh" type="button" value="<fmt:message key="produdt.js.save" />"
										class="diw_but_tc" />
									</td>
								</tr>
							</table>
						</div>
					</div>

					<h4>
						<span><button type="button" class="search_but" id="addTestPackLanguagePackBtn"
								value="<fmt:message key="common.btn.addlanguage" />"><fmt:message key="common.btn.addlanguage" /></button>
						</span><fmt:message key="product.softwareversion.sitelanguage" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table id="testPackLanguagePackTable" width="100%" border="0" bordercolor="#dddddd">
					<tr class="title2">
						<td width="10%"><fmt:message key="memberman.complaintman.language" /></td>
						<td width="25%"><fmt:message key="memberman.complaintman.languagepackloction" /></td>
						<td width="25%"><fmt:message key="memberman.complaintman.testcardocloction" /></td>
						<td width="30%"><fmt:message key="memberman.complaintman.versiondesc" /></td>
						<td width="10%"><fmt:message key="common.list.operation" /></td>
					</tr>
				</table>
				</div>
				<div style="padding-top: 20px;padding-left:40%">
					<input name="" id="sumbitBtn" type="button" value="<fmt:message key="produdt.js.save" />" class="diw_but" />
					<input name="" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			
		</div>

	</form>
<script>var selectPageCtx;</script>
<div id="selectProductTypeDlg"></div>
<script type="text/javascript">
	$(function(){
		ProductInfo.init();
	});
	
	function initProductInfo(){
		$(".selectProductItem").click(function(){
			var code = $(this).attr("selectCode");		
			var name = $(this).attr("selectName");	
			$("#softwareTypeCode").val(code);
			$("#softwareTypeName").val(name);
			Cop.Dialog.close("selectProductTypeDlg");
		});
		
	}
	setInterval('initProductInfo()',100);

</script>


