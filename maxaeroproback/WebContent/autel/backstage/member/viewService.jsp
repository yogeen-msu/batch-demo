<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<style>
.input table th{text-align:right;height:40px;font-weight:normal}
.input table td{font-weight:normal}
.input table th {
padding-left:0px;
}
</style>
<div class="input">
	<table class="form-table" style="background-color: #FFFFFF;">
		
		<tr>
			<th><label class="text"><fmt:message key="product.producttypevo" />:</label></th>
			<td>
				${productRepairRecord.proTypeName}
				<input type="hidden" value="${productRepairRecord.proTypeName}" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="memberman.complaintman.productserial" />:</label></th>
			<td>
				${productRepairRecord.proSerialNo}
				<input type="hidden" name="productRepairRecord.proSerialNo" value="${productRepairRecord.proSerialNo}" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.date" />:</label></th>
			<td>
				${productRepairRecord.proDate}
				<input type="hidden" name="productRepairRecord.proDate" value="${productRepairRecord.proDate}" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.repairdate" />:</label></th>
			<td>
			${productRepairRecord.repDate}
			</td>
		</tr>
		
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.brokentype" />:</label></th>
			<td>
			${productRepairRecord.repType}
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.repaircontent" />:</label></th>
			<td>
			${productRepairRecord.repContent}
			</td>
		</tr>
		
	</table>
</div>

