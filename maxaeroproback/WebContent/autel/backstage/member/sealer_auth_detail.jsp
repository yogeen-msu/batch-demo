<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > 
	工具 > 经销商权限管理
</div>
<form method="post" action="listSealerAuth.do">
	<div class="grid">
		
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div>经销商权限管理</h4>
			
			<div class="sear_table">
				<table width="100%" style="border:0">
					<tr height="2">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right">经销商编码:</td>
						<td width="154">
							<input class="sear_item" type="text" name="sealerAuth.autelId" value="${sealerAuth.autelId}" />
						</td>
						
						
						
						<td width="160" align="right">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="2">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			
			<div class="active">
				<h4>
				如果要修改经销商权限，请在“权限处理”里重新输入经销商账号
				<span>
					<input type="button" onclick="location.href='addSealerAuthDetail.do'" value="权限处理" class="search_but" />
				</span>
				&nbsp;&nbsp;
				</h4>
			</div>
		</div>
		
		
		<grid:grid from="webpage">
			<grid:header>
					<grid:cell width="5%">账号</grid:cell>
					<grid:cell width="15%">编码</grid:cell>
					<grid:cell width="10%">名称</grid:cell>
					<grid:cell width="10%">操作</grid:cell>
				</grid:header>
		
			<grid:body item="item">
					<grid:cell>${item.autelId}</grid:cell>
					<grid:cell>${item.code}</grid:cell>
					<grid:cell><fmt:message key="${item.name}"/></grid:cell>
					
					<grid:cell>
						<a href="delAuthDetailByCode.do?detail.id=${item.id}"  ><fmt:message key="common.js.delInfo" /></a>
					</grid:cell>
				</grid:body>
		
		</grid:grid>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		
		
		
	});
</script>

