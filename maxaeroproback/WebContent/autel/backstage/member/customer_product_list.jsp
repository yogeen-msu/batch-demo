<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerProduct.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>
<title><fmt:message key="memberman.customman.registerproinfo" /></title>

<script type="text/javascript">
var promessage1 = '<fmt:message key="memberman.customman.promessage1" />';
var promessage2 = '<fmt:message key="memberman.customman.promessage2" />';
var promessage3 = '<fmt:message key="memberman.customman.promessage3" />';
var promessage4 = '<fmt:message key="memberman.customman.promessage4" />';
var unbinding_str = '<fmt:message key="memberman.customman.unbinding" />';
var isbinding_str = '<fmt:message key="memberman.customman.isbinding" />';
var unbinded_str = '<fmt:message key="memberman.customman.unbinded" />';
var unbindingreson_str = '<fmt:message key="memberman.customman.unbindingreson" />';
var pronoreg_str = '<fmt:message key="memberman.customman.pronoreg" />';
</script>

</head>
<body>
<form action="listcustomerpro.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="customerCode" id="customerCode" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="memberman.customman.registerproinfo" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.customman.registerproinfo" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.customman.cusid" />:</td>
						<td width="154"><input name="customerProductVoSel.autelId" value="${customerProductVoSel.autelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.saleinfoman.sealerid" />:</td>
						<td width="154"><input name="customerProductVoSel.sealerAutelId" value="${customerProductVoSel.sealerAutelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="syssetting.areaconfig.areaconfigname" />:</td>
						<td><input name="customerProductVoSel.areaCfgName" value="${customerProductVoSel.areaCfgName}" type="text" class="time_in" /></td>
					</tr>
					<tr height="38">
						<td align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td><input name="customerProductVoSel.proSerialNo" value="${customerProductVoSel.proSerialNo}" type="text" class="time_in" /></td>
						<td align="right"><fmt:message key="memberman.customman.bindingstate" />:</td>
						<td>
							<SELECT class="proregstatussel" proregstatussel="${customerProductVoSel.proRegStatus}" name="customerProductVoSel.proRegStatus">
								<OPTION value="">--<fmt:message key="orderman.list.all" />--</OPTION>
								<OPTION value="1"><fmt:message key="memberman.customman.isbinding" /></OPTION>
								<OPTION value="2"><fmt:message key="memberman.customman.unbinded" /></OPTION>
							</SELECT>
						</td>
						<td align="right">
							<fmt:message key="memberman.customman.proregisterdate" />:
						</td>
						<td>
							<input name="customerProductVoSel.beginDate" value="${customerProductVoSel.beginDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="customerProductVoSel.endDate" value="${customerProductVoSel.endDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							
						</td>
					</tr>
					<tr height="38">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<input type="button" id="submit_but" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="memberman.customman.registerproduct" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="11%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="syssetting.areaconfig.areaconfigname" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.saleinfoman.sealerid" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.outfactorydate" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.proregisterdate" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.bindingstate" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="list">
					<tr code="${list.proCode}" customerCode="${list.code }" proRegStatus="${list.proRegStatus}" proNoRegReason="${list.proNoRegReason}" align="center" height="30px">
						<td>${list.autelId}</td>
						<td>${list.name}</td>
						<td>${list.proSerialNo}</td>
						<td>${list.areaCfgName}</td>
						<td>${list.sealerAutelId}</td>
						<td>${list.proDate}</td>
						<td>${list.proRegTime}</td>
						<td class="regstatustd"></td>
						<td><A class="operate" href="#"></A></td>
					</tr>
				</c:forEach>
				  <tr>
				    <td colspan="9" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but exportexcel" /></td>
				  </tr>
			</grid:grid>
		</div>
	</div>	
			
</form>
	
				
	<div id="overlay"></div>
	
					<div id="win_pronoreg">
						<h2>
							<span id="close">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0">
								<tr>
									<td width="30%" align="right"><fmt:message key="memberman.customman.unbindingreson" />:</td>
									<td><input type="text" class="pronoregi" style="width: 300px"/></td>
								</tr>
								<tr>

									<td width="30%" align="right">&nbsp;</td>
									<td><input name="" type="button" value="<fmt:message key="customer_product.list.jsp.button.ok" />"
										class="diw_but_tc pronoregb" />
									</td>
								</tr>
							</table>
						</div>
					</div>
		
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		$("#submit_but").click(function(){
			//搜索
			$("#f1").attr("action","listcustomerpro.do").submit();
		});
	});
</script>
