<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>	
<title><fmt:message key="memberman.sealerman.info" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/artDialog.js?skin=simple"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/plugins/iframeTools.js?skin=opera"></script>
<script type="text/javascript">
var message3 = '<fmt:message key="memberman.complaintman.message3" />';
var message2 = '<fmt:message key="memberman.complaintman.message2" />';
var message1 = '<fmt:message key="memberman.complaintman.message1" />';
var message4 = '<fmt:message key="memberman.complaintman.message4" />';
var message5 = '<fmt:message key="memberman.complaintman.message5" />';
var message6 = '<fmt:message key="memberman.complaintman.message6" />';
</script>
<script type="text/javascript">
var $allotcomplaint ;
var $win_complaint;;
var $overlay;
var $close;
var $selacceptor;
$(function(){
	var $showcomplaint = $(".showcomplaint");
	$showcomplaint.click(function(){
		var $this = $(this);
		var code = $this.attr("code");
		//window.open("showcomplaint.do?code="+code,'complaint','height=800,width=900,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
		art.dialog.open('showcomplaint.do?code='+code,
			    {title: ' ', width: 900, height: 800});
	});
	
	 $allotcomplaint = $(".allotcomplaint");
	 $win_complaint= $("#win_complaint");;
	 $overlay = $("#overlay");
	 $close = $("#close");
	 $selacceptor = $(".selacceptor");
	$allotcomplaint.click(function ()
			{
				$("#code").attr("value",$(this).attr("code"));
				
				$win_complaint.show();
				$overlay.show();
			});
	 
		$selacceptor.click(function(){
			var $this = $(this);
			var acceptor = $this.attr("acceptor");
			selAcceptor(acceptor);
		});
		
		$(".selacceptortrans").click(function(){
			var $this = $(this);
			var acceptor = $this.attr("acceptor");
			selAcceptorTrans(acceptor);
		});
		
		$close.click(function ()
				{
					colseWin();
				});
	
});
//分配客服方法
function selAcceptor(acceptor)
{
	var code = $("#code").val();
	var result = -1;
	
	if(confirm(message1)){
		$.ajax({
			url:'allotacceptor.do?ajax=yes',
			type:"post",
			data:{"code":code,"acceptor":acceptor},
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(data);
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
	
		if(result == 0)
		{
			colseWin();
			alert(message2);
			window.parent.setMessageTab(2);
		}
		else
		{
			alert(message3);
		}		
	}
}
//关闭分配客服窗口方法
function colseWin()
{
	$("#code").attr("value","");
	
	$win_complaint.hide();
	$overlay.hide();
}

//转发客诉方法
function selAcceptorTrans(acceptor)
{
	var code = $("#code").val();
	var result = -1;
	
	if(confirm(message4)){
		$.ajax({
			url:'transcomplaint.do?ajax=yes',
			type:"post",
			data:{"code":code,"acceptor":acceptor},
			dataType:'JSON',
			async: false,
			success :function(data){
				var jsonData = eval(data);
				result = jsonData[0];
			},
			error :function(data){
				result = -2;
			}
		});
	
		if(result == 0)
		{
			colseWin();
			alert(message5);
			window.parent.setMessageTab(2);
		}
		else
		{
			alert(message6);
		}		
	}
}

</script>
<div style="height:10px"></div>
<form action="listcustomercomplaint.do" method="post" id="f1" name="f1">
<div class="grid" style="padding-right:0px;border:1px solid #DDDDDD;border-left: #dddddd 0px solid;border-right: #dddddd 0px solid;width:97.8%">
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
			<grid:grid from="webpage">
				<grid:header>
				    <grid:cell width="10%"><fmt:message key="customercomplaint.tickedid.name" /></grid:cell>
				    <grid:cell width="25%"><fmt:message key="memberman.complaintman.complaintmanname" /></grid:cell>
				    <grid:cell width="10%"><fmt:message key="memberman.complaintman.acceptman" /></grid:cell>
				    <grid:cell width="15%"><fmt:message key="memberman.complaintman.state" /></grid:cell>
				    <grid:cell width="15%"><fmt:message key="memberman.complaintman.createtime" /></grid:cell>
				    <grid:cell width="15%"><fmt:message key="memberman.complaintman.lastreplytime" /></grid:cell>
				    <grid:cell width="20%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="item">
				  
				  <tr class="tr_bgc">
				    <td>${item.code}</td>
				    <td>${item.complaintPerson}</td>
				    <td>${item.acceptor}</td>
				    <td class="complaintstate">
				    <c:if test="${item.complaintState == 1 }"><fmt:message key="memberman.complaintman.stateopen" /></c:if>
				    <c:if test="${item.complaintState == 2 }"><fmt:message key="memberman.complaintman.stateguaqi" /></c:if>
				    <c:if test="${item.complaintState == 3 }"><fmt:message key="memberman.complaintman.stateddhf" /></c:if>
				    <c:if test="${item.complaintState == 4 }"><fmt:message key="memberman.complaintman.stateguanbi" /></c:if>
				    </td>
				    <td>${item.complaintDate}</td>
				    <td>${item.lastReDate}</td>
				    <td class="caoz">
					    <a id="showComplaint" class="showcomplaint" code="${item.code}" href="#"><fmt:message key="memberman.complaintman.show" /></a>&nbsp;
				         <a class="allotcomplaint" code="${item.code}" href="#"><fmt:message key="memberman.complaintman.transcomplaint" /></a>
				    </td>
				  </tr>
				</c:forEach>
				  <tr>
<%-- 				    <td colspan="9" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but1" /></td> --%>
				  </tr>
			</grid:grid>
		</div>
		
<div id="win_complaint">
	   	<h2><span id="close" onclick="javascript:colseWin()">×</span></h2>
		<div class="tc_tab_ts">
		<table width="100%" border="0">
		  <tr>
		    <td width="35%" align="center" style="font-weight: bold;"><fmt:message key="memberman.complaintman.customserviceid" /></td>
		    <td width="35%" align="center" style="font-weight: bold;"><fmt:message key="syssetting.authorityman.managername" /></td>
		    <td width="30%" align="center" style="font-weight: bold;"><fmt:message key="common.list.operation" /></td>
		  </tr>
		  <c:forEach items="${customerServiceUsers}" var="item">
		  <tr align="center">
		    <td>${item.username}</td>
		    <td>${item.realname}</td>
		    <td><input type="button" class="diw_but selacceptortrans" acceptor="${item.username}" value="<fmt:message key="common.openselect.choose" />" /></td>
		  </tr>
		  </c:forEach>
		</table>
		</div>
	</div>
</form>		