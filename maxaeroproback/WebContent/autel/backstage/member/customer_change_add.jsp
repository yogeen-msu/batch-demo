<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderman.log.addinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>
</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:
	<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; 更改Autel ID
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;更改Autel ID
			</h4>
	<form action="saveChangeAutelLog.do"   method="post" name="f1" id="f1">
		
		<table class="form-table">
				
			<tr>
				<th><label class="text">产品序列号:</label></th>
				<td>
					<input type="text" name="changeAutelIdLog.productSn" maxlength="40" dataType="string" isrequired="true" onblur="getProductCust()"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text">Old Autel ID:</label></th>
				<td>
					<input type="text" readonly name="changeAutelIdLog.oldAutelId"  dataType="string" isrequired="true" />
				</td>
			</tr>
				
			<tr>
				<th><label class="text">New Autel ID:</label></th>
				<td>
					<input type="text" name="changeAutelIdLog.newAutelId"  dataType="string" isrequired="true" fun="checkupdatemoney"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text">更换原因:</label></th>
				<td>
					<textarea id="reason" name="changeAutelIdLog.reason" cols="60" rows="5" dataType="string" isrequired="true"  maxlength="200"></textarea>
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="button" type="button" value="<fmt:message key="saleConfig.update.jsp.button.save" />" class="submitBtn insertorderinfolog" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">

	$(".insertorderinfolog").click(function(){
		var productSn=$("input[name='changeAutelIdLog.productSn']").eq(0).val();
		var oldAutelId=$("input[name='changeAutelIdLog.oldAutelId']").eq(0).val();
		var newAutelId=$("input[name='changeAutelIdLog.newAutelId']").eq(0).val();
		var reason=$.trim($("#reason").val());
		if(productSn==""){
			alert("请输入正确的序列号");
			return;
		}
		if(newAutelId==""){
			alert("请输入新的Autel ID");
			return;
		}
		if(reason==""){
			alert("请输入变更原因");
			return;
		}
		if(oldAutelId==$.trim(newAutelId)){
			alert("Autel ID一致，不需要改变");
			return;
		}
		
		
		$("#f1").attr("action","saveChangeAutelLog.do").submit();
	});
	
	function getProductCust()
	{
		var productSn=$("input[name='changeAutelIdLog.productSn']").eq(0).val();
		$.ajax({
			url:'getCustomerInfoBySerialNo.do?ajax=yes',
			type:"post",
			data:{"changeAutelIdLog.productSn":productSn},
			dataType:'JSON',
			async: false,
			success :function(data){
				
				$("input[name='changeAutelIdLog.oldAutelId']").eq(0).val(eval(data));
			},
			error :function(data){
				result = -2;
			}
		});
		
	}
	
	</script>
	
</body>
</html>
