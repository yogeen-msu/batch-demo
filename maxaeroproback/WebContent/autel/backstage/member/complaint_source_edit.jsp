<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客诉来源修改</title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerComplaintSource.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; 客诉来源修改
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;客诉来源修改
			</h4>
	<form action="updatecomplaintsource.do"  class="validate" method="post" name="f1" id="f1">
	
		<input type="hidden" name="customerComplaintSourceEdit.code" id="customerComplaintSourceEdit.code" value="${customerComplaintSourceEdit.code}" />
		<input type="hidden" name="selectedcodes" id="selectedcodes" value="${selectedcodes}" />

		<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="common.list.code" />:</label></th>
				<td>
					${customerComplaintSourceEdit.code}
				</td>
			</tr>
				
			<tr>
				<th><label class="text">客诉来源名称:</label></th>
				<td>
					<input type="text" name="customerComplaintSourceEdit.name" value="${customerComplaintSourceEdit.name}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="button" type="button" value="<fmt:message key="common.btn.ok" />" class="submitBtn updatecomplaintsource" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">
	$("form.validate").validate();
	</script>
	
</body>
</html>
