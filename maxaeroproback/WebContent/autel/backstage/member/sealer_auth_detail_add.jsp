<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.sealerinfoman.addsealerinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > 
	工具 > 增加经销商权限
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="${ctx}/autel/main/images/leg_16.gif" width="14" height="16" />&nbsp;增加经销商权限
			</h4>
	<form action="saveSealerAuthDetail.do"  class="validate" method="post" name="f1" id="f1">
		
		<table class="form-table">
				
			<tr>
				<th><label class="text">经销商账号:</label></th>
				<td>
					<input type="text" name="sealerAuth.autelId" style="width: 280px" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text">权限:</label></th>
				<td>
				<c:forEach var="item" items="${listSealerAuthVO}">
						<div><input type="checkbox" name=sealerAuthDesc value="${item.code}">  <fmt:message key="${item.name}" />
						</div>
				</c:forEach>
				</td>
			</tr>
				
			
		
				
			
			
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="button" type="button" value="<fmt:message key="produdt.js.save" />" class="submitBtn insertsealerAuth" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
<script type="text/javascript">
	$(".sealernameenglish").hide();
	$("form.validate").validate();
		
	$(document).ready(function(){		
		$(".insertsealerAuth").click(function(){
			
			$("#f1").attr("action","saveSealerAuthDetail.do").submit();
		});
	});

	</script>
</body>
</html>
