<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> > <fmt:message key="memberman.customman.title" /> > <fmt:message key="memberman.customman.custom.consumeinfo" /></div>

<form method="post" action="product-software-valid-status!listCustomerOrderInfo.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="customer.order.info.list.jsp.title" /></h4>
			<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					  <tr height="38">    
						<td width="111" align="right"><fmt:message key="memberman.customman.cusid" />:</td>
						<td width="154"><input type="text"  name="customerInfoVo.autelId" value="${customerInfoVo.autelId}" /></td>
						<td width="111"  align="right"><fmt:message key="memberman.sealer.jxsid" />:</td>
						<td width="200"><input type="text" style="width:180px" name="customerInfoVo.sealAutelId" value="${customerInfoVo.sealAutelId}" /></td>
						<td width="111" align="right"><fmt:message key="system.config.area" />:</td>
						<td width="154"><input type="text" name="customerInfoVo.areaCfgName" value="${customerInfoVo.areaCfgName}" /></td>
					  </tr>
					  <tr height="38">    
						<td width="111" align="right"><fmt:message key="memberman.customman.cunsumetype" />:</td>
						<td width="154">
							<select name="customerInfoVo.consumeType" id="customerInfoVo_consumeType" >
								<option value="-1" ><fmt:message key="orderman.list.all" /></option>
								<option value="0" ><fmt:message key="orderman.list.gm" /></option>
								<option value="1" ><fmt:message key="orderman.list.xf" /></option>
							</select>
						</td>
						<td width="111"  align="right"><fmt:message key="memberman.complaintman.time" />:</td>
						<td width="200" >
							<input  dataType="date" class="dateinput" style="width:80px"  readonly="readonly" type="text" name="customerInfoVo.startTime" value="${customerInfoVo.startTime}" />
							--
							<input  dataType="date" class="dateinput" style="width:80px"  readonly="readonly" type="text" name="customerInfoVo.endTime" value="${customerInfoVo.endTime}" />
						</td>	
						<td width="111" align="right"></td>
					 	<td>
					 		<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
					 		<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
					 	</td>
					  </tr>
					  <tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4><fmt:message key="customer.order.info.list.jsp.title" /></h4>
			</div>
		</div>
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.ordernum" /></grid:cell>
				<grid:cell><fmt:message key="customer.order.info.list.jsp.purchase.type" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.cusid" /></grid:cell>
				<grid:cell><fmt:message key="memberman.sealer.jxsid" /></grid:cell>
				<grid:cell><fmt:message key="system.config.area" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
				<grid:cell><fmt:message key="customer.order.info.list.jsp.sales.unit" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.cunsumeprice" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.time" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.orderstate" /></grid:cell>
				<grid:cell><fmt:message key="orderman.list.paystate" /></grid:cell>
				<grid:cell><fmt:message key="order.gathering.money.status" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.code}</grid:cell>
				<grid:cell>
					<c:if test="${item.consumeType ==0}"><fmt:message key="orderman.list.gm" /></c:if>
					<c:if test="${item.consumeType ==1}"><fmt:message key="orderman.list.xf" /></c:if>
				</grid:cell>
				<grid:cell>${item.autelId}</grid:cell>
				<grid:cell>${item.sealAutelId}</grid:cell>
				<grid:cell>${item.areaCfgName}</grid:cell>
				<grid:cell>${item.serialNo}</grid:cell>
				<grid:cell>${item.minSaleUnitName}</grid:cell>
				<grid:cell>${item.orderMoney}</grid:cell>
				<grid:cell>${fn:substring(item.orderDate,0,10) }</grid:cell>
				<grid:cell>
					<c:if test="${item.orderState == 0 }"><fmt:message key="memberman.customman.canceled" /></c:if>
					<c:if test="${item.orderState == 1 }"><fmt:message key="memberman.customman.valid" /></c:if>
				</grid:cell>
				<grid:cell>
					<c:if test="${item.orderPayState == 0 }"><fmt:message key="orderman.list.nopay" /></c:if>
					<c:if test="${item.orderPayState == 1 }"><fmt:message key="orderman.list.payed" /></c:if>
				</grid:cell>
				<grid:cell>
					<c:if test="${item.recConfirmState == 0 }"><fmt:message key="orderman.list.noconfirm" /></c:if>
					<c:if test="${item.recConfirmState == 1 }"><fmt:message key="orderman.list.confirmed" /></c:if>
				</grid:cell>
			</grid:body>
		</grid:grid>
	</div>
</form>

<div id="validateEditDlg"></div>

<script type="text/javascript">

//选中洲
var ctype = "${customerInfoVo.consumeType}";
$("#customerInfoVo_consumeType option[value='"+ctype+"']").attr("selected","selected");

$("#reset_but").click(function(){
	//重置文本框
	$(".sear_table").find("input[type='text']").each(function(i,obj){
		$(obj).val("");
	});		
	
	//重置下拉框
	$(".sear_table").find("select option[value='-1']").attr("selected","selected");
});
</script>
