<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.complaintman.testpack" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/testPack.js"></script>


<script type="text/javascript">
var message1 = '<fmt:message key="memberman.testpackman.message1" />';
var message2 = '<fmt:message key="memberman.testpackman.message2" />';
var message3 = '<fmt:message key="memberman.testpackman.message3" />';
var message4 = '<fmt:message key="memberman.testpackman.message4" />';
var message5 = '<fmt:message key="memberman.testpackman.message5" />';
var message6 = '<fmt:message key="memberman.testpackman.message6" />';
var message7 = '<fmt:message key="memberman.testpackman.message7" />';
var message8 = '<fmt:message key="memberman.testpackman.message8" />';
var message9 = '<fmt:message key="memberman.testpackman.message9" />';
var message10 = '<fmt:message key="memberman.testpackman.message10" />';
var message11 = '<fmt:message key="memberman.testpackman.message11" />';
var message12 = '<fmt:message key="memberman.testpackman.message12" />';
var message13 = '<fmt:message key="memberman.testpackman.message13" />';
var message14 = '<fmt:message key="memberman.testpackman.message14" />';
var message15 = '<fmt:message key="memberman.testpackman.message15" />';
var message16 = '<fmt:message key="memberman.testpackman.message16" />';
var message17 = '<fmt:message key="memberman.testpackman.message17" />';
var message18 = '<fmt:message key="memberman.testpackman.message18" />';
</script>


</head>

<body>
<form action="listtestpack.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="memberman.complaintman.testpack" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.complaintman.testpack" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.complaintman.testpackname" />:</td>
						<td width="154"><input name="testPackVoSel.versionName" value="${testPackVoSel.versionName}" type="text" class="time_in" /></td>
						<!-- 
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="154"><input name="testPackVoSel.proTypeName" value="${testPackVoSel.proTypeName}" type="text" class="time_in" /></td>
						 -->
						 
						 
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td><input name="testPackVoSel.proSerialNo" value="${testPackVoSel.proSerialNo}" type="text" class="time_in" /></td>
					</tr>
					<tr height="38">
						<td align="right"><fmt:message key="product.software.name" />:</td>
						<td><input name="testPackVoSel.softwareName" value="${testPackVoSel.softwareName}" type="text" class="time_in" /></td>
						<!-- 
						<td align="right"><fmt:message key="memberman.customman.cusname" />:</td>
						<td><input name="testPackVoSel.customerName" value="${testPackVoSel.customerName}" type="text" class="time_in" /></td>
						 -->
						<td>&nbsp;</td>
						<td>
							<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
						<input name="" type="button" value=<fmt:message key="common.btn.add" /> class="search_but addtestpack" />
						
						<input name="" type="button" value="刷新测试包"  onclick="location.href='refresh.do'" class="search_but refreshtpack" />
						
					</span><fmt:message key="memberman.complaintman.testpacks" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="13%"><fmt:message key="common.list.code" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="memberman.complaintman.testpackname" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="product.software.name" /></grid:cell>
					<!--  
					<grid:cell width="13%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="12%"><fmt:message key="product.producttypevo" /></grid:cell>
					-->
					<grid:cell width="13%"><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="memberman.complaintman.publishdate" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.code}</grid:cell>
					<grid:cell>${item.versionName}</grid:cell>
					<grid:cell>${item.softwareName}</grid:cell>
					<!--  
					<grid:cell>${item.customerName}</grid:cell>
					<grid:cell>${item.proTypeName}</grid:cell>
					-->
					<grid:cell style="word-break:break-all; word-wrap:break-word;"> ${item.proCode}</grid:cell>
					<grid:cell>${item.releaseDate}</grid:cell>
					<grid:cell>
					<%-- 	<a code="${item.code}" href="#" class="showtestpack"><fmt:message key="common.list.detail" /></a> --%>
						<a code="${item.code}" href="editTestPack.do?code=${item.code}" class="edittestpack"><fmt:message key="common.list.mod" /></a>
						<a code="${item.code}" href="#" class="delltestpack"><fmt:message key="common.list.del" /></a>
					</grid:cell>
				</grid:body>
				</grid:grid>
		</div>
	</div>
</form>

<div id="overlay"></div>
<div id="win_testpackdetail">
<h2><span id="close">×</span></h2>
	<div class="tc_tab_ts">
		<table width="100%" border="0" class="testpackvo">
			<tr height="38">
				<td width="111" align="right"><fmt:message key="memberman.complaintman.testpackname" />:</td>
				<td width="154" class="testpackname">&nbsp;</td>
				<td width="154" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
				<td width="154" class="testpackprono">&nbsp;</td>
			
			</tr>
			<tr height="38">
				<td width="111" align="right"><fmt:message key="product.software.name" />:</td>
				<td width="154" class="testpacktypename">&nbsp;</td>
				<td width="170" align="right"><fmt:message key="memberman.complaintman.basepacklocation" />:</td>
				<td width="154" class="testpackpath">&nbsp;</td>
				
			</tr>
		</table>
		<table id="testPackLanguagePackTable" width="100%" border="0">
			<tr class="title" align="center">
				<td width="10%"><fmt:message key="memberman.complaintman.language" /></td>
				<td width="25%"><fmt:message key="memberman.complaintman.languagepackloction" /></td>
				<td width="25%"><fmt:message key="memberman.complaintman.testcardocloction" /></td>
				<td width="30%"><fmt:message key="memberman.complaintman.versiondesc" /></td>
			</tr>
		</table>
			
		<div style="margin-top:10px; text-align:center;">
		<input name="" type="button" value="<fmt:message key="common.btn.close" />" class="diw_but close_win_testpackdetail" />
		</div>
	</div>
</div>
 
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>
