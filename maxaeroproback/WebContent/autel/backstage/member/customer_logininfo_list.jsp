<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.customman.cuslogininfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${context}/js/getIp.js"></script>
</head>

<body>
<form action="listcustomerlogininfo.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="memberman.customman.cuslogininfo" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.customman.cuslogininfo" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.customman.cusid" />:</td>
						<td width="154"><input name="customerLoginInfoVoSel.customerAutelId" value="${customerLoginInfoVoSel.customerAutelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td><input name="customerLoginInfoVoSel.proSerialNo" value="${customerLoginInfoVoSel.proSerialNo}" type="text" class="time_in" /></td>
					</tr>
					<tr height="38">
						<td align="right">
							<fmt:message key="memberman.customman.cuslogintime" />:
						</td>
						<td width="180px">
							<input name="customerLoginInfoVoSel.beginDate" value="${customerLoginInfoVoSel.beginDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="customerLoginInfoVoSel.endDate" value="${customerLoginInfoVoSel.endDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							
						</td>
						<td></td>
						<td><input type="button" id="submit_but" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" /></td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="memberman.customman.cuslogininfo" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusloginip" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cuslogintime" /></grid:cell>
					<grid:cell width="15%">显示国家</grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.customerAutelId}</grid:cell>
					<grid:cell>${item.customerName}</grid:cell>
					<grid:cell>${item.loginIP}</grid:cell>
					<grid:cell>${item.loginTime}</grid:cell>	
					<grid:cell><a onclick="javascript:getIp('${ctx}','${item.loginIP}')">显示</a></grid:cell>			
				</grid:body>
			  	<tr>
			    	<td colspan="7" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but exportexcel" />&nbsp;&nbsp;<input name="" type="button" value="<fmt:message key="common.btn.clearall" />" class="diw_but clearall" /></td>
			  	</tr>
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">

	$(document).ready(function(){
		
		$(".exportexcel").click(function(){
			var $this = $(this);
			var contextpath = $("#contextpath").val();
			$("#f1").attr("action",contextpath+"/customerLoginInfoExportExcel.do").submit();
		});
		
		$(".clearall").click(function(){
			var $this = $(this);
			var contextpath = $("#contextpath").val();
			if(confirm("<fmt:message key="memberman.customman.clearalllongininfo" />")){
				$("#f1").attr("action","clearcustomerlogin.do").submit();
			}
		});
	});

	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		$("#submit_but").click(function(){
			//搜索
			$("#f1").attr("action","listcustomerlogininfo.do").submit();
		});
	});
</script>
