<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../statics/js/admin/Grid.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".selectProductItem").click(function(){
		var productCode = $(this).attr("selectCode");		//编号
		var proTypeCodeName = $(this).attr("selectName");		//产品型号
		productAdd(productCode,proTypeCodeName);
	});
	
});

var productAdd = function(code,proTypeName){
	$("#proTypeCode").val(code);
	$("#proTypeCodeName").val(proTypeName);
	Cop.Dialog.close("selectProductTypeDlg");
};


function bindEvent(pager,grid){
	 var grid = pager.parent();
	 pager.find("li>a").unbind(".click").bind("click",function(){
		 load($(this).attr("pageno"),grid);
	 }); 
	 pager.find("a.selected").unbind("click");
};

function pageQuery(){
	$.ajax({
		url:"selectProductTypeValue.do",
		data: $('#pageSelectForm').serialize(), 
		type:"post",  
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			
			$(".selectProductItem").click(function(){
				var productCode = $(this).attr("selectCode");		//编号
				var proTypeCodeName = $(this).attr("selectName");		//产品型号
				productAdd(productCode,proTypeCodeName);
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};
</script>
<form method="post" id="pageSelectForm" action="selectProductTypeValue.do">
<div class="grid">
	<div class="toolbar" style="height:30px">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="product.producttypevo" />:
			<select  name="softType.proTypeCode" id="softType.proTypeCode" class="sear_item">
								<option value=""><fmt:message key="select.option.select"  /></option> 
								<c:forEach var="item" items="${productTypeList}">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
				</select>
			<input type="hidden" name="productTypeCode" id="productTypeCode" value="${softType.proTypeCode}"/>
			&nbsp;&nbsp;&nbsp;<fmt:message key="product.software.name" />:
			<input type="text" name="softType.name" id="softType.name" value="${softType.name}"/>
			
			<input type="button" style="height:25px"  name="submit" onclick="pageQuery()" value=<fmt:message key="common.btn.search" /> />
			<div style="clear: both"></div>
	</div>
	<grid:grid from="webpage" ajax="yes">
		<grid:header>
			<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
			<grid:cell width="200px"><fmt:message key="product.producttypevo" /></grid:cell>
			<grid:cell><fmt:message key="product.software.name" /></grid:cell>
			<grid:cell width="100px"><fmt:message key="common.list.operation"/></grid:cell>
		</grid:header>
	
		<grid:body item="item">
			<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
			<grid:cell>${item.proTypeName} </grid:cell>
			<grid:cell>${item.name}</grid:cell>
			<grid:cell>
				<input type="button" class="selectProductItem" value=<fmt:message key="common.openselect.choose" /> selectCode="${item.code}" selectName="${item.name}" />
			</grid:cell>
		</grid:body>
	</grid:grid>
</div>
</form>
