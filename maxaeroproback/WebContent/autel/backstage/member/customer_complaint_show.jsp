<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.complaintman.customman" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerComplaintDetail.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/artDialog.js?skin=simple"></script>
<script type="text/javascript">
var message3 = '<fmt:message key="memberman.complaintman.message3" />';
var message2 = '<fmt:message key="memberman.complaintman.message2" />';
var message1 = '<fmt:message key="memberman.complaintman.message1" />';
var message4 = '<fmt:message key="memberman.complaintman.message4" />';
var message5 = '<fmt:message key="memberman.complaintman.message5" />';
var message6 = '<fmt:message key="memberman.complaintman.message6" />';

var message21 = '<fmt:message key="memberman.complaintman.message21" />';
var message22 = '<fmt:message key="memberman.complaintman.message22" />';
var message23 = '<fmt:message key="memberman.complaintman.message23" />';
var message24 = '<fmt:message key="memberman.complaintman.message24" />';
var message25 = '<fmt:message key="memberman.complaintman.message25" />';
var message26 = '<fmt:message key="memberman.complaintman.message26" />';
var message27 = '<fmt:message key="memberman.complaintman.message27" />';
var message28 = '<fmt:message key="memberman.complaintman.message28" />';
var message29 = '<fmt:message key="memberman.complaintman.message29" />';
var confirmMod_str = '<fmt:message key="common.js.confirmMod" />';
var modSuccess_str = '<fmt:message key="common.js.modSuccess" />';
var modFail_str = '<fmt:message key="common.js.modFail" />';
var message30 = '<fmt:message key="order.cancel.name" />';
var inoneday = '<fmt:message key="memberman.complaintman.inoneday" />';
var inthreeday = '<fmt:message key="memberman.complaintman.inthreeday" />';
var intwoweek = '<fmt:message key="memberman.complaintman.intwoweek" />';
var intwomonth = '<fmt:message key="memberman.complaintman.intwomonth" />';
</script>
</head>
<body>


<form action="showcomplaint.do" class="validate" method="post" id="f1" name="f1" enctype="multipart/form-data">
	
	
	<iframe name='hidden_frame' id="hidden_frame" style='display:none'></iframe>
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="${code}" />
	<input type="hidden" name="contextpath" id="contextpath" value="${autelproUrl}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="memberman.complaintman.customman" />
	</div>
        <div class="right_tab">
        	<div class="sear">
            	<h4 class="sear_tj"><img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.complaintman.customman" /></h4>
                <div class="sear_table">
                	<table width="100%" border="0">
					  <tr height="5">
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr height="38">
					     <td colspan="2"  style="font-size:17px; color:#b84435;text-align:center"><strong>${customerComplaintInfo.complaintTitle}</strong></td>
					    <td colspan="2" align="center">
						    <input id="showSupport" code="${code}" name="showSupport" type="button" value="<fmt:message key="memberman.complaintman.transcomplaint" />" class="diw_but" />
							<input name="" type="button" value="<fmt:message key="memberman.complaintman.btnreply" />" class="diw_but openrecomplaint" />
						</td>
					
					  </tr>
					  <tr height="38">
					    <td colspan="2"  style="font-size:14px; color:#b84435;padding-left:200px"><strong>Ticket ID:${customerComplaintInfo.code}</strong></td>
					    <td colspan="2" align="left" style="font-size:14px; color:#b84435;"><strong></strong></td>
					  </tr>
					  <tr height="38" class="new_kes">
					    <td align="center" width="25%"><fmt:message key="memberman.complaintman.state" /></td>
					    <td align="center" width="25%"><fmt:message key="common.list.operation" /></td>
					    <td align="center" width="25%"><fmt:message key="memberman.complaintman.complanteffective" /></td>
					    <td align="center" width="25%"><fmt:message key="memberman.complaintman.complaintdetails" /></td>
					  </tr>
					  <tr height="38" class="new_kes">
					    <td align="center">
						    <c:if test="${customerComplaintInfo.complaintState == 1}"><fmt:message key="memberman.complaintman.stateopen" /></c:if>
						    <c:if test="${customerComplaintInfo.complaintState == 2}"><fmt:message key="memberman.complaintman.stateguaqi" /></c:if>
						    <c:if test="${customerComplaintInfo.complaintState == 3}"><fmt:message key="memberman.complaintman.stateddhf" /></c:if>
						    <c:if test="${customerComplaintInfo.complaintState == 4}"><fmt:message key="memberman.complaintman.stateguanbi" /></c:if>
						</td>
					    <td align="center">
						    
						    <c:if test="${customerComplaintInfo.complaintState == 1}">
						    	<input onclick="modiComplaintState(2);" type="button" value="<fmt:message key="memberman.complaintman.stateguaqi2" />" class="diw_but" />
						    </c:if>
						    <c:if test="${customerComplaintInfo.complaintState == 2}">
						    	<input onclick="modiComplaintState(1);" type="button" value="<fmt:message key="memberman.complaintman.stateopen" />" class="diw_but" />
						    </c:if>
						</td>
					    <td align="center">
						    <select class="act_zt complaintEffective" name="customerComplaintInfo.complaintEffective" complaintEffective="${customerComplaintInfo.complaintEffective}">
								<option value="1"><fmt:message key="memberman.complaintman.avlid" /></option>
								<option value="2"><fmt:message key="memberman.complaintman.inavlid" /></option>
							</select>
						</td>
					    <td align="center">
							<h4><span><input type="button" class="search_but_fb openksdetail" value="<fmt:message key="memberman.complaintman.complainttable" />" /></span></h4>
					    </td>
					  </tr>
					  <tr height="5">
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					</table>
					<table class="addrecomplaint" width="100%" border="0" style="display:none;">
					  <tr>
					    <td align="right" width="11%"><fmt:message key="memberman.complaintman.infodetails" />:</td>
					    <td colspan="2">
					    	<textarea style="height:140px;font-family: Arial,'宋体',Verdana,sans-serif;" name="reCustomerComplaintInfoAdd.reContent" class="xinx_xq" isrequired="true"  maxlength="500"></textarea>
					    </td>					   
					  </tr>
					  <tr>
					    <td align="right"><fmt:message key="memberman.complaintman.attachment" />:</td>
					    <td>
						    <input type="file" class="attachmentfile" name="attachment" />
<!-- 						    <input type="button" class="cleanfile" value="清空"> -->
					    </td>
					    <td></td>
					  </tr>
					  <tr height="38">
					    <td>&nbsp;</td>
					    <td><input type="button" value="<fmt:message key="common.btn.ok" />" class="diw_but_tc addbutton" /></td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr height="5">
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					</table>

				</div>
                <div class="active">
                	<h4><fmt:message key="memberman.complaintman.complaintlist" /></h4>
                </div>
            </div>
            <div class="grid">
            
				<grid:grid from="webpage">
					<c:forEach items="${webpage.result}" var="item">
					  <tr height="28">
					    <td width="120" rowspan="2">
						    <img src="../main/images/tx.jpg" width="60" height="60" /><br />
						    ${item.rePerson}
					    </td>
					    <td class="fj" align="left"><span class="caoz">
					    <c:if test="${item.attachment!='' && item.attachment!=null}">
<%-- 					    	<a target="_blank" href="${ctx}/download.do?downloadPath=${item.attachment}"><fmt:message key="memberman.complaintman.attachment" /></a> --%>
					    	<a target="_blank" href="${autelproUrl}/DownloadComplaintAction.do?downloadPath=${item.attachment}"><fmt:message key="memberman.complaintman.attachment" /></a>
					    </c:if>
					    </span>${item.reDate}</td>
					  </tr>
					  <tr height="68">
					    <td align="left" class="ke_nr" style="word-wrap:break-word;word-break:break-all;"><pre style="white-space: pre-wrap;word-wrap: break-word;">${item.reContent}</pre></td>
					  </tr>
					</c:forEach>	            
				</grid:grid>
			</div>
      	</div>	
			
</form>
	
				
	<div id="overlay"></div>
				
	
	<div id="win_tc">
	   	<h2><span id="close">×</span></h2>
		<div class="tc_tab_ts">
		
			<table width="100%" border="0">
			  <tr>
			    <td colspan="2" align="center"><fmt:message key="memberman.complaintman.complaintmanname" /></td>
			    <td width="120"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.complaintPerson}" /></td>
			    <td width="110" align="center"><fmt:message key="customercomplaint.company.name" /></td>
			    <td colspan="3"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.company}" /></td>
			  </tr>
			  <tr>
			  	
			    <td colspan="2" align="center"><fmt:message key="memberman.complaintman.date" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.complaintDate}" /></td>
			    <td align="center"><fmt:message key="memberman.complaintman.phone" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.phone}" /></td>
			    <td width="110" align="center"><fmt:message key="memberman.sealerinfoman.email" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.email}" /></td>
			
			  </tr>
			  <tr>
			    <td colspan="2" align="center"><fmt:message key="memberman.complaintman.complainttitle" /></td>
			    <td colspan="5"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.complaintTitle}" /></td>
			    </tr>
			  <tr>
			    <td rowspan="4" width="20" align="center"><fmt:message key="memberman.complaintman.carinfo" /></td>
			    <td align="center" width="90"><fmt:message key="memberman.complaintman.cars" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.vehicleSystem}" /></td>
			    <td align="center"><fmt:message key="memberman.complaintman.cartype" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.vehicleType}" /></td>
			    <td align="center"><fmt:message key="memberman.complaintman.diagnosticconnector" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.diagnosticConnector}" /></td>
			  </tr>
			  <tr>
			    <td align="center"><fmt:message key="memberman.complaintman.caryear" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.carYear}" /></td>
			    <td align="center"><fmt:message key="memberman.complaintman.carnumber" /></td>
			    <td colspan="3"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.carnumber}" /></td>
			    </tr>
			  <tr>
			    <td align="center"><fmt:message key="memberman.complaintman.enginetype" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.engineType}" /></td>
			    <td align="center"><fmt:message key="memberman.complaintman.system" /></td>
			    <td colspan="3"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.system}" /></td>
			    </tr>
			  <tr>
			    <td align="center"><fmt:message key="memberman.complaintman.other" /></td>
			    <td colspan="5"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.other}" /></td>
			    </tr>
			  <tr>
			    <td align="center" rowspan="3"><fmt:message key="memberman.complaintman.deviceinfo" /></td>
			    <td align="center"><fmt:message key="memberman.complaintman.productname" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.productName}" /></td>
			    <td align="center"><fmt:message key="customercomplaint.productno.name" /></td>
			    <td colspan="3"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.productNo}" /></td>
			    </tr>
			  <tr>
			    <td align="center"><fmt:message key="memberman.complaintman.softwarename" /></td>
			    <td><input name="" type="text" class="tc_input" value="${customerComplaintInfo.softwareName}" /></td>
			    <td align="center"><fmt:message key="memberman.complaintman.softwareedition" /></td>
			    <td colspan="3"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.softwareEdition}" /></td>
			    </tr>
			  <tr>
			    <td align="center"><fmt:message key="memberman.complaintman.testpath" /></td>
			    <td colspan="5"><input name="" type="text" class="tc_input" value="${customerComplaintInfo.testPath}" /></td>
			    </tr>
			  <tr>
			    <td align="center"><fmt:message key="memberman.complaintman.complaintcontent" /></td>
			    <td colspan="6"><textarea name="" cols="" rows="" class="tc_wb">${customerComplaintInfo.complaintContent}</textarea></td>
			    </tr>
			  <tr>
			    <td align="center"><fmt:message key="memberman.complaintman.attachment" /></td>
			    <td colspan="6"><input attachment="${customerComplaintInfo.attachment}" type="button" value="<fmt:message key="memberman.complaintman.showattachment" />" class="search_but queryAttachment" /></td>
			    </tr>
			  <tr>
			    <td align="center"><fmt:message key="memberman.complaintman.emergencylevel" /></td>
			    <td colspan="6"><textarea style="font-size:13px" name="" cols="" rows="" class="tc_wb emergencyLevel" emergencyLevel="${customerComplaintInfo.emergencyLevel}"><fmt:message key="memberman.complaintman.compmangtime" />:</textarea></td>
			    </tr>
			  
			</table>
			
			<div style="margin-top:10px; text-align:center;">
			<input name="" type="button" value="<fmt:message key="common.btn.close" />" class="diw_but close_win_tc" />
			</div>
                        
		</div>
	</div>
	
	<div id="win_complaint" style="top:30%">
	   	<h2><span id="close2" onclick="javascript:colseWin()">×</span></h2>
		<div class="tc_tab_ts">
		<table width="100%" border="0">
		  <tr>
		    <td width="35%" align="center" style="font-weight: bold;"><fmt:message key="memberman.complaintman.customserviceid" /></td>
		    <td width="35%" align="center" style="font-weight: bold;"><fmt:message key="syssetting.authorityman.managername" /></td>
		    <td width="30%" align="center" style="font-weight: bold;"><fmt:message key="common.list.operation" /></td>
		  </tr>
		  <c:forEach items="${customerServiceUsers}" var="item">
		  <tr align="center">
		    <td>${item.username}</td>
		    <td>${item.realname}</td>
		    <td><input type="button" class="diw_but selacceptortrans" acceptor="${item.username}" value="<fmt:message key="common.openselect.choose" />" /></td>
		  </tr>
		  </c:forEach>
		</table>
		</div>
	</div>
	
</body>
</html>

<script type="text/javascript">
function callback(result)
{
	document.f1.target="_self";
	var msg;
	
	if(result==0)
	{
		msg = "回复成功";
	}
	else if(result==1)
	{
		msg = "选择的上传文件不支持";
	}
	else if(result==2)
	{
		msg = "上传附件失败";
	}
	else if(result==3)
	{
		msg = "回复失败";
	}
	else if(result==4)
	{
		msg = "邮件发送失败";
	}
	else
	{
		msg = "回复不正常";
	}
	
	if(result==0 || result==4)
	{
		alert(msg);
		document.f1.action="showcomplaint.do";
		document.f1.submit();
	}
	else
	{
		alert(msg);
	}
}

</script>

