<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.customman.cussoftwareinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>


</head>

<body>
<form action="listcustomersoft.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="memberman.customman.cussoftwareinfo" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.customman.cussoftwareinfo" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.customman.cusid" />:</td>
						<td width="154"><input name="customerSoftwareVoSel.customerAutelId" value="${customerSoftwareVoSel.customerAutelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td width="154"><input name="customerSoftwareVoSel.proSerialNo" value="${customerSoftwareVoSel.proSerialNo}" type="text" class="time_in" /></td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td align="right"><fmt:message key="memberman.saleinfoman.sealerid" />:</td>
						<td><input name="customerSoftwareVoSel.sealerAutelId" value="${customerSoftwareVoSel.sealerAutelId}" type="text" class="time_in" /></td>
						<td align="right"><fmt:message key="syssetting.areaconfig.areaconfigname" />:</td>
						<td><input name="customerSoftwareVoSel.areaCfgName" value="${customerSoftwareVoSel.areaCfgName}" type="text" class="time_in" /></td>
						<td>
						&nbsp;&nbsp;<input type="button" id="submit_but" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="memberman.customman.cussoftwareinfo" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="18%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
				<%-- 	<grid:cell width="15%"><fmt:message key="memberman.customman.cusname" /></grid:cell> --%>
					<grid:cell width="20%"><fmt:message key="rechargecard.type.list.jsp.sales.contract.name" /></grid:cell>
					<grid:cell width="12%"><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="syssetting.areaconfig.areaconfigname" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="memberman.saleinfoman.sealerid" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="syssetting.languageconfig" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="myvendproduct.regist.date" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.exptime" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.customerAutelId}</grid:cell>
					<%-- <grid:cell>${item.customerName}</grid:cell> --%>
					<grid:cell>${item.saleContract}</grid:cell>
					<grid:cell>${item.proSerialNo}</grid:cell>
					<grid:cell>${item.areaCfgName}</grid:cell>
					<grid:cell>${item.sealerAutelId}</grid:cell>
					<grid:cell>${item.softLanguage}</grid:cell>
					<grid:cell>${item.proRegisterDate}</grid:cell>
					<grid:cell>${item.proSoftValidDate}</grid:cell>					
				</grid:body>
			  	<%-- <tr>
			    	<td colspan="4" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but exportexcel" /></td>
			  	</tr> --%>
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">

	$(document).ready(function(){
		
		$(".exportexcel").click(function(){
			var $this = $(this);
			var contextpath = $("#contextpath").val();
			$("#f1").attr("action",contextpath+"/customerSoftwareExportExcel.do").submit();
		});
	});

	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		$("#submit_but").click(function(){
			//搜索
			$("#f1").attr("action","listcustomersoft.do").submit();
		});
	});
</script>
