<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>	
<title><fmt:message key="memberman.sealerman.info" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>		
<div style="height:10px"></div>
<div class="grid" style="padding-right:0px;border:1px solid #DDDDDD;border-left: #dddddd 0px solid;border-right: #dddddd 0px solid;width:97.8%">
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell><fmt:message key="memberman.customman.ordernum" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.cunsumetype" /></grid:cell>
				
				<grid:cell><fmt:message key="memberman.sealer.jxsid" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.areaconfig" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
				<grid:cell><fmt:message key="marketman.minusalenit" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.cunsumeprice" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.cunsumetime" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.orderstate" /></grid:cell>
				<grid:cell><fmt:message key="orderman.list.paystate" /></grid:cell>
				<grid:cell><fmt:message key="order.gathering.money.status" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				
				<grid:cell>${item.code}</grid:cell>
				<grid:cell>
					<c:if test="${item.consumeType ==0}"><fmt:message key="orderman.list.gm" /></c:if>
					<c:if test="${item.consumeType ==1}"><fmt:message key="orderman.list.xf" /></c:if>
				</grid:cell>
				
				<grid:cell>${item.sealAutelId}</grid:cell>
				<grid:cell>${item.areaCfgName}</grid:cell>
				<grid:cell>${item.serialNo}</grid:cell>
				<grid:cell>${item.minSaleUnitName}</grid:cell>
				<grid:cell>${item.orderMoney}</grid:cell>
				<grid:cell>${fn:substring(item.orderDate,0,10) }</grid:cell>
				<grid:cell>
					<c:if test="${item.orderState == 0 }"><fmt:message key="memberman.customman.canceled" /></c:if>
					<c:if test="${item.orderState == 1 }"><fmt:message key="memberman.customman.valid" /></c:if>
				</grid:cell>
				<grid:cell>
					<c:if test="${item.orderPayState == 0 }"><fmt:message key="orderman.list.nopay" /></c:if>
					<c:if test="${item.orderPayState == 1 }"><fmt:message key="orderman.list.payed" /></c:if>
				</grid:cell>
				<grid:cell>
					<c:if test="${item.recConfirmState == 0 }"><fmt:message key="orderman.list.noconfirm" /></c:if>
					<c:if test="${item.recConfirmState == 1 }"><fmt:message key="orderman.list.confirmed" /></c:if>
				</grid:cell>
			</grid:body>
		</grid:grid>
		</div>