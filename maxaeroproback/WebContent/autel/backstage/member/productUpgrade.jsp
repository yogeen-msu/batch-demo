<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.customman.customproupgradeinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
</head>

<body>
<form  method="post" id="f1" name="f1">
		<div class="right_tab" style="padding-right:0px;">
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="product.software.name" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.upgradetime" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.autelId}</grid:cell>
					<grid:cell>${item.customerName}</grid:cell>
					<grid:cell>${item.proSerialNo}</grid:cell>
					<grid:cell>${item.softwareTypeName}</grid:cell>
					<grid:cell>${item.upgradeTime}</grid:cell>					
				</grid:body>
			  
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>
