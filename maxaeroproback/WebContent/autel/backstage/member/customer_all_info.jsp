<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.complainttypeman.typeman" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/themes/autel/css/css_bak.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customer_all_Info.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/artDialog.js?skin=simple"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/plugins/iframeTools.js"></script>

<script type="text/javascript">
var option = '<fmt:message key="common.list.operation" />';
var choose = '<fmt:message key="common.openselect.choose" />';
var productserial = '<fmt:message key="memberman.complaintman.productserial" />';
var customerID = '<fmt:message key="accountinformation.autelid.name"/>';
var nrecord = '<fmt:message key="common.have.nrecord" />';
var QueryBuilder="<fmt:message key='homepage.customer.QueryBuilder' />";
var submit='<fmt:message key="common.btn.ok" />';
var cancel='<fmt:message key="order.cancel.name" />';


/* function reinitIframe(){
	var iframe = document.getElementById("detail");
	try{
	var bHeight = iframe.contentWindow.document.body.scrollHeight;
	var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
	var height = Math.max(bHeight, dHeight);
	iframe.height =  height;
	}catch (ex){}
	}
	window.setInterval("reinitIframe()", 200);
	 */
</script>


</head>

<body>
<form action="listcustomercomplainttype.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="homepage.customer.allinfo" />
	</div>
	
	
<div id="win_complaint" style="top:30%">
	   	<h2><span id="close" onclick="javascript:colseWin()">×</span></h2>
		<div class="tc_tab_ts" id="CustDiv">
	
		</div>
	</div>
	
	

	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="homepage.customer.allinfo" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="accountinformation.autelid.name"/>:</td>
						<td width="154"><input id="autelID" name="customerComplaintTypeSel.name" value="" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial"/>:</td>
						<td width="154"><input id="serialNo" name="customerComplaintTypeSel.name" value="" type="text" class="time_in" /></td>
						<td>
							&nbsp;&nbsp;<input id="sreach" onclick="javascript:queryInfo()" type="button" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
				<div class="active"><h4>
					<span>
					</span><fmt:message key="homepage.customer.queryresult"/>:
				</h4></div>
	<div class="my_tt" id="right_tab" style="padding-top:10px">
	<ul class="my_menu0" id="my_menu0">
		<li id="span0" name="" onclick="javascript:setMessageTab('0')" class="hover"><fmt:message key="homepage.customer.customerInfo"/></li>
		<li id="span1" name="" onclick="javascript:setMessageTab('1')"><fmt:message key="homepage.customer.productInfo"/></li>
	    <li id="span2" name="" onclick="javascript:setMessageTab('2')"><fmt:message key="homepage.customer.complaintInfo"/></li>
	    <li id="span3" name="" onclick="javascript:setMessageTab('3')"><fmt:message key="homepage.customer.consumptionInfo"/></li>
	    <li id="span4" name="" onclick="javascript:setMessageTab('4')"><fmt:message key="homepage.customer.loginInfo"/></li>

	</ul></div>
				</div>
		</div>
			<iframe name="detail" height="400px" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="no" id="detail" width="100%"   src="bank.do"></iframe>
		
		
	
</form>
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>

