<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.sealerinfoman.modsealerinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/sealerInfo.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.sealerman.title" /> &gt; <fmt:message key="memberman.sealerinfoman.modsealerinfo" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.sealerinfoman.modsealerinfo" />
			</h4>
	<form action="updatesealer.do"  class="validate" method="post" name="f1" id="f1">
	
		<input type="hidden" name="sealerInfoEdit.code" id="sealerInfoEdit.code" value="${sealerInfoEdit.code}" />
		<input type="hidden" name="selectedcodes" id="selectedcodes" value="${selectedcodes}" />
		<input type="hidden" name="sealerInfoVoSel.sealerName" value="${sealerInfoVoSel.sealerName}" />
		<input type="hidden" name="sealerInfoVoSel.areaCfgName" value="${sealerInfoVoSel.areaCfgName}" />
		<input type="hidden" name="languageCodeCN" id="languageCodeCN" value="${languageCodeCN}" />

		<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.number" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.autelId" value="${sealerInfoEdit.autelId}" maxlength="40" dataType="string" isrequired="true" readonly="readonly"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.passwd" />:</label></th>
				<td>
					<input type="password" name="sealerInfoEdit.userPwd" maxlength="40" dataType="string" fun="checkpwdlen"/>(<fmt:message key="memberman.sealerinfoman.passwdmoddemo" />)
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.repasswd" />:</label></th>
				<td>
					<input type="password" name="" maxlength="40" dataType="string" fun="checkpwd"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.preferredlanguage" />:</label></th>
				<td>
					<select class="languagecode" name="sealerInfoEdit.languageCode" languagecode="${sealerInfoEdit.languageCode}" isrequired="true">
									<option value="">--<fmt:message key="select.option.select" />--</option>
									<c:forEach items="${languages }" var="item">
										<option value="${item.code}">${item.name}</option>
									</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.firstname" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.firstName" value="${sealerInfoEdit.firstName}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.lastname" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.lastName" value="${sealerInfoEdit.lastName}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.email" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.email" value="${sealerInfoEdit.email}" maxlength="40" dataType="email" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.secondemail" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.secondEmail" value="${sealerInfoEdit.secondEmail}" maxlength="40" dataType="email"/>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="regcustomer.info.province" />:</label></th>
				<td>
					<select class="provincecodes" name="sealerInfoEdit.province" id="province" onchange="selectProinces()" fun="validateProvince">
						<option value="">--<fmt:message key="select.option.select" />--</option>
						<c:forEach items="${stateProvinces}" var="item">
							<c:if test="${item.code == sealerInfoEdit.stateCode}">
								<option value="${item.code};${item.country}" selected="selected">${item.state}</option>
							</c:if>
							<c:if test="${item.code != sealerInfoEdit.stateCode}">
								<option value="${item.code};${item.country}">${item.state}</option>
							</c:if>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.country" />:</label></th>
				<td>
					<select class="countrycode" name="sealerInfoEdit.country" id="country" onchange="selectCountry()" isrequired="true">
						<option value="">--<fmt:message key="select.option.select" />--</option>
						<c:forEach items="${countryCodes }" var="item">
							<c:if test="${item.code == sealerInfoEdit.countryCode}">
								<option value="${item.code};${item.state}" selected="selected">${item.country}</option>
							</c:if>
							<c:if test="${item.code != sealerInfoEdit.countryCode}">
								<option value="${item.code};${item.state}">${item.country}</option>
							</c:if>
						</c:forEach>
					</select>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.company" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.company" value="${sealerInfoEdit.company}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.address" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.address" value="${sealerInfoEdit.address}" maxlength="80" style="width: 300px" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.city" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.city" value="${sealerInfoEdit.city}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
			
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.zipcode" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.zipCode" value="${sealerInfoEdit.zipCode}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.phonec" />:</label></th>
				<td>
					<input type="text" name="sealerInfoEdit.telephone" value="${sealerInfoEdit.telephone}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><input class="isallowsendemail" type="checkbox" name="sealerInfoEdit.isAllowSendEmail" isallowsendemail="${sealerInfoEdit.isAllowSendEmail}" value="1" /> <fmt:message key="memberman.sealerinfoman.isallowsendemail" /></label></th>
				<td>
					<fmt:message key="memberman.sealerinfoman.sendemaildemo" />
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="button" type="button" value="<fmt:message key="produdt.js.save" />" class="submitBtn updatesealer" />
						<input name="button" type="button" onclick="window.location='listsealer.do'" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">
	$("form.validate").validate();

		function checkpwd(){
			 
			if(  $("input[type='password']").eq(0).val() != $("input[type='password']").eq(1).val() ){
				return "<fmt:message key="memberman.sealerinfoman.message1" />";
			}
			return true;
		}

		function checkpwdlen(){
			 
			if( $("input[type='password']").eq(0).val() != "" && $("input[type='password']").eq(0).val().length < 6){
				return "<fmt:message key="update.confirmpassword.lengthcheck" />";
			}
			return true;
		}
		
		function validateProvince(){
			var country = $("#country").val();
			var provinces = country.split(";")[1];
			if(!$("#province").val() && provinces != "none"){
				return "<fmt:message key="memberman.sealerinfoman.message4" />";
			}
			return true;
		}
		
		function selectCountry(){
			var country = $("#country").val();
			if(!country)
				return false;
			var provinces = country.split(";")[1];
			if(provinces && provinces != "none"){
				selected($("#province option"),provinces);
			}else if(provinces == "none"){
				$("#province option:eq(0)").attr("selected",true);
			}else{
				selectProinces();
			}
		}
		function selectProinces(){
			var provinces = $("#province").val();
			if(!provinces)
				return false;
			var country = provinces.split(";")[1];
			selected($("#country option"),country);
		}
		function selected(obj,t){
			$(obj).each(function(){
				if($(this).text() == t){
					$(this).attr("selected",true);
				}
			});
		}
	</script>
</body>
</html>
