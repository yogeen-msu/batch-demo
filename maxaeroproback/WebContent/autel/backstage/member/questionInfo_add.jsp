<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> > <fmt:message key="memberman.customman.title" /> > <fmt:message key="memberman.customman.safeproblem" />  >  <fmt:message key="memberman.customman.addsafeproblem" /></div>
<form action="question-info!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="memberman.customman.addsafeproblem" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="memberman.customman.safeproblem" />:</label></th>
			<td>
				<input type="text" name="questionInfo.questionDesc" style="width: 300px;" dataType="string" isrequired="true" maxlength="30"/>
			</td>
		</tr>
	</table>

</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='question-info!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
</script>