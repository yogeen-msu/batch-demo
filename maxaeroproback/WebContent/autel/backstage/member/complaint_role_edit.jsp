<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecardtype.modtype" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/trade/reChargeCardType.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>

</head>

<body>
	<div class="leg_topri">
<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="memberman.complaintman.authority" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;修改
			</h4>
	<form action="savecomplaintrole.do"  class="validate" method="post" name="f1" id="f1">
		
		<input type="hidden" name="complaintAdminuserInfo.complaintTypeCode" value="${customerComplaintType.code}"/>
		
		<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.complainttypeman.typename" />:</label></th>
				<td>
					<input style="width:300px" type="text" name="complaintAdminuserInfo.complaintTypeName" value="${customerComplaintType.name}" readonly  dataType="string" isrequired="true"/>
				</td>
			</tr>

			<tr>
				<th><label class="text"><fmt:message key="memberman.complaintman.support" />:</label></th>
				<td>
					<select style="width:305px" class="protypecode" name="complaintAdminuserInfo.adminUserid"  id="adminUserid" isrequired="true" >
						<option value="">--<fmt:message key="common.sel.choose" />--</option>
						<c:forEach items="${customerServiceUsers}" var="item">
							<option <c:if test="${complaintAdminuserInfo!=null && item.username ==complaintAdminuserInfo.adminUserid }">selected</c:if> value="${item.username}">${item.username}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<th><label class="text"><fmt:message key="accountinformation.first.email" />:</label></th>
				<td>
					<select style="width:305px" class="salecontractcode" name="complaintAdminuserInfo.emailId" id="emailId"  isrequired="true">
						<option value="">--<fmt:message key="common.sel.choose" />--</option>
						<c:forEach items="${smtp}" var="item">
							<option <c:if test="${complaintAdminuserInfo!=null && item.id ==complaintAdminuserInfo.emailId }">selected</c:if> value="${item.id}">${item.username}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
				
			
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="submit" type="submit" value="<fmt:message key="emption.add.jsp.button.save" />" class="submitBtn updatecomplaintrole" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
</body>
</html>

<script type="text/javascript">
$("form.validate").validate();
$(".updatecomplaintrole").click(function(){
	var $this = $(this);
	$("#f1").attr("action","savecomplaintrole.do").submit();
});


</script>

