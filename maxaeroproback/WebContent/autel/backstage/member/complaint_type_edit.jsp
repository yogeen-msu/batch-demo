<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="memberman.complainttypeman.modtype" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/complaintTypeMod.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


<script type="text/javascript">
var message3 = '<fmt:message key="memberman.complainttypeman.message3" />';
var message4 = '<fmt:message key="memberman.complainttypeman.message4" />';
var message5 = '<fmt:message key="common.js.dataimperfect" />';
</script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
<script>
window.onload = function ()
{
	var oWin = document.getElementById("win");
	var oLay = document.getElementById("overlay");	
	var oBtn = document.getElementsByTagName("button")[0];
	var oClose = document.getElementById("close");
	var oH2 = oWin.getElementsByTagName("h2")[0];
	var bDrag = false;
	var disX = disY = 0;
	oBtn.onclick = function ()
	{
		oLay.style.display = "block";
		oWin.style.display = "block";
	};
	oClose.onclick = function ()
	{
		oLay.style.display = "none";
		oWin.style.display = "none";
		
	};
	oClose.onmousedown = function (event)
	{
		(event || window.event).cancelBubble = true;
	};
	oH2.onmousedown = function (event)
	{		
		var event = event || window.event;
		bDrag = true;
		disX = event.clientX - oWin.offsetLeft;
		disY = event.clientY - oWin.offsetTop;		
		this.setCapture && this.setCapture();		
		return false;
	};
	document.onmousemove = function (event)
	{	
		if (!bDrag) return;
		var event = event || window.event;
		var iL = event.clientX - disX;
		var iT = event.clientY - disY;
		var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
		var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
		iL = iL < 0 ? 0 : iL;
		iL = iL > maxL ? maxL : iL; 		
		iT = iT < 0 ? 0 : iT;
		iT = iT > maxT ? maxT : iT;
		
		oWin.style.marginTop = oWin.style.marginLeft = 0;
		oWin.style.left = iL + "px";
		oWin.style.top = iT + "px";		
		return false;
	};
	document.onmouseup = window.onblur = oH2.onlosecapture = function ()
	{
		bDrag = false;				
		oH2.releaseCapture && oH2.releaseCapture();
	};
};
</script>
</head>

<body>
<form action="updateCustomerComplaintTypeName.do" method="post" id="complaintTypeNameForm">
		<input type="hidden" id="complaintTypeCode" name="customerComplaintType.code" value="${customerComplaintType.code }">
    	<div class="leg_topri">
    	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="memberman.complainttypeman.modtype" />
    	</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.complainttypeman.modtype" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="150" align="right"><fmt:message key="memberman.complainttypeman.typecode" />:</td>
							<td width="154">&nbsp;&nbsp;${customerComplaintType.code }</td>
							<td width="80" align="right"></td>
							<td></td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<div id="overlay"></div>
					<div id="win">
						<h2>
							<span id="close">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0">
								<tr>

									<td width="40%" align="right"><fmt:message key="memberman.complaintman.language" />:</td>
									<td><select name="language" id="language"
										class="act_zt_tc">
											<c:forEach items="${ languages}" var="language">
												<option value="${language.code }">${language.name }</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>

									<td width="40%" align="right"><fmt:message key="memberman.complainttypeman.typename" />:</td>
									<td><input id="complaintTypeName" type="text" class="time_in_tc" />
									</td>
								</tr>
								<tr>

									<td width="40%" align="right">&nbsp;</td>
									<td><input id="finsh" type="button" value="<fmt:message key="common.btn.ok" />"
										class="diw_but_tc" />
									</td>
								</tr>
							</table>
						</div>
					</div>

					<h4>
						<span><button type="button" class="search_but"
								value="<fmt:message key="memberman.complainttypeman.addname" />"><fmt:message key="memberman.complainttypeman.addname" /></button>
						</span><fmt:message key="memberman.complainttypeman.typename" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="complaintTypeNameTable">
				
					<tr class="title">
						<td width="10%"><fmt:message key="memberman.complaintman.language" /></td>
						<td width="30%"><fmt:message key="memberman.complainttypeman.typename" /></td>
						<td width="20%"><fmt:message key="common.list.operation" /></td>

					</tr>
					
					<c:forEach items="${customerComplaintTypeNames }" var="item">
						<tr>
							<td>${item.languageName}</td>
							<td>${item.name}</td>
							<td class="caoz"><a complaintTypeNameId="${item.id}" languageCode="${item.languageCode}" languageName="${item.languageName}" onclick="delName(this);" href="#"><fmt:message key="marketman.saleconfigman.moveout" /></a></td>
						</tr>
					</c:forEach>

				</table>
				</div>
				<div style="padding-top: 20px;padding-left:40%">
					<input id="sumbitBtn" type="button" value="<fmt:message key="common.btn.ok" />" class="diw_but" /><input
						type="button" onclick="jump('listcustomercomplainttype.do')" value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			
		</div>

	</form>
</body>
</html>


