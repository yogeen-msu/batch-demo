<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.customman.safeproblem" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/questionInfo.js"></script>


<script type="text/javascript">
var message1 = '<fmt:message key="common.js.confirmDel" />';
var message2 = '<fmt:message key="common.js.confirmDel" />';
</script>


</head>

<body>
<form action="listquestioninfo.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="memberman.customman.safeproblem" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.customman.safeproblem" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="180" align="right"><fmt:message key="memberman.customman.safeproblem" />:</td>
						<td width="154"><input name="questionInfoSel.questionDesc" value="${questionInfoSel.questionDesc}" type="text" class="time_in" /></td>
						<td>
							&nbsp;&nbsp;<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
						<input name="" type="button" value=<fmt:message key="common.btn.add" /> class="search_but addquestioninfo" />
					</span><fmt:message key="memberman.customman.safeproblem" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="13%"><fmt:message key="common.list.code" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="memberman.customman.safeproblem" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.code}</grid:cell>
					<grid:cell>${item.questionDesc}</grid:cell>
					<grid:cell>
						<a code="${item.code}" href="#" class="editquestioninfo"><fmt:message key="common.list.mod" /></a>&nbsp;
						<a code="${item.code}" href="#" class="dellquestioninfo"><fmt:message key="common.list.del" /></a>
					</grid:cell>
				</grid:body>
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>

