<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>	
<title><fmt:message key="memberman.sealerman.info" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerNewProduct.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/artDialog.js?skin=aero"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/artDialog/plugins/iframeTools.js"></script>
<script type="text/javascript">
var errormsg='<fmt:message key="homepage.customer.chooseProduct" />';
</script>	

	
<div style="height:10px"></div>
<div class="grid" style="padding-right:0px;border:1px solid #DDDDDD;border-left: #dddddd 0px solid;border-right: #dddddd 0px solid;width:97.8%">
			<grid:grid from="webpage">
				<grid:header>
				    <grid:cell width="11%"><fmt:message key="common.openselect.choose" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="syssetting.areaconfig.areaconfigname" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.saleinfoman.sealerid" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.outfactorydate" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.proregisterdate" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.bindingstate" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="list">
					<tr code="${list.proCode}" proRegStatus="${list.proRegStatus}" proNoRegReason="${list.proNoRegReason}" align="center" height="30px">
						<td><input type='radio' name="selectCode" id="selectCode" value="${list.proCode}"/></td>
						<td>${list.autelId}</td>
						<td>${list.name}</td>
						<td>${list.proSerialNo}</td>
						<td>${list.areaCfgName}</td>
						<td>${list.sealerAutelId}</td>
						<td>${list.proDate}</td>
						<td>${list.proRegTime}</td>
						<td class="regstatustd">
					     <c:if test="${list.proRegStatus == 1 }"><fmt:message key="memberman.customman.isbinding" /></c:if>
				         <c:if test="${list.proRegStatus == 2 }"><fmt:message key="memberman.customman.unbinded" /></c:if>
						 <c:if test="${list.proRegStatus == 0 }"><fmt:message key="memberman.customman.pronoreg" /></c:if>
						</td>
					</tr>
				</c:forEach>
				 <tr>
				    <td colspan="9" align="left" class="color">
				    <input name="proUpgrade" id="proUpgrade" type="button" value="<fmt:message key="homepage.customer.queryUpgrade" />" class="diw_but exportexcel" />
				    <input name="proSoftware" id="proSoftware" type="button" value="<fmt:message key="homepage.customer.querySoftware" />" class="diw_but exportexcel" />
				    <input name="proService" id="proService" type="button" value="<fmt:message key="homepage.customer.queryRepair" />" class="diw_but exportexcel" />
				    </td>
				  </tr>
			</grid:grid>
		</div>
