<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.sealerman.info" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/sealerInfo.js"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
<script type="text/javascript">
var message3 = '<fmt:message key="memberman.sealerinfoman.message3" />';
var message2 = '<fmt:message key="memberman.sealerinfoman.message2" />';
</script>

</head>

<body>
<form action="listsealer.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.sealerman.title" /> &gt; <fmt:message key="memberman.sealerman.info" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.sealerman.info" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.sealerinfoman.number" />:</td>
						<td width="154"><input name="sealerInfoVoSel.autelId" value="${sealerInfoVoSel.autelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.sealerinfoman.name" />:</td>
						<td width="154"><input name="sealerInfoVoSel.sealerName" value="${sealerInfoVoSel.sealerName}" type="text" class="time_in" />
						</td>
						<td>
						&nbsp;&nbsp;<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
						<input name="" type="button" value="<fmt:message key="common.btn.add" />" class="search_but addsealer" />&nbsp;&nbsp;
						<input name="" type="button" value="<fmt:message key="common.btn.excelimport" />" class="search_but createsealer" />&nbsp;&nbsp;
						<input name="" type="button" value="<fmt:message key="common.btn.excelimporttemplate" />" class="search_but_fb"
						onclick="jump('${ctx}/download.do?downloadPath=/template/importsealer.xls')" />
					</span><fmt:message key="memberman.sealer.info" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="13%"><fmt:message key="memberman.sealerinfoman.number" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="memberman.sealerinfoman.name" /></grid:cell>
<%-- 					<grid:cell width="13%"><fmt:message key="memberman.sealerinfoman.areaname" /></grid:cell> --%>
					<grid:cell width="13%"><fmt:message key="memberman.sealerinfoman.email" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="memberman.sealerinfoman.secondemail" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="memberman.sealerinfoman.phone" /></grid:cell>
					<grid:cell width="13%"><fmt:message key="memberman.sealerinfoman.registertime" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.autelId}</grid:cell>
					<grid:cell>${item.sealerName}</grid:cell>
					<%-- <grid:cell>${item.areaCfgName}</grid:cell> --%>
					<grid:cell>${item.email}</grid:cell>
					<grid:cell>${item.secondEmail}</grid:cell>
					<grid:cell>${item.telephone}</grid:cell>
					<grid:cell>${item.regTime}</grid:cell>
					<grid:cell style="white-space:nowrap;">
						<a code="${item.code}" href="#" class="syncsealer"><fmt:message key="common.list.sync" /></a>&nbsp;
						<a code="${item.code}" href="#" class="editsealer"><fmt:message key="common.list.mod" /></a>&nbsp;
						<a code="${item.code}" href="#" class="dellsealer"><fmt:message key="common.list.del" /></a>
					</grid:cell>
				</grid:body>
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>

