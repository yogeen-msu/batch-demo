<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>	
<title><fmt:message key="memberman.sealerman.info" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
var $isallowsendemail;
$(document).ready(function(){
$isallowsendemail = $(".isallowsendemail");
if($isallowsendemail.length > 0){
	if($isallowsendemail.attr("isallowsendemail") == "1") $isallowsendemail.attr("checked", true);
}

});

</script>
<style>
.input table th{text-align:right;height:40px;font-weight:normal}
.input table td{font-weight:normal}

</style>	
<div style="height:10px"></div>	
				<div class="active" style="width:97.8%">
				
				<div class="input">
				<table class="form-table" style="background-color: #FFFFFF;">
				
			<tr>
				<th><label  class="text"><fmt:message key="memberman.customman.cusid" />:</label></th>
				<td width="200px">
				    <label  class="text">${customerInfoEdit.autelId}</label>
				</td>
				
				<th><label  class="text"><fmt:message key="memberman.customman.preferredlanguage" />:</label></th>
				<td>
				 <label  class="text">${customerInfoEdit.languageCode}</label>
				</td>
			</tr>
			
			<tr>
				<th><label  class="text"><fmt:message key="memberman.customman.firstname" />:</label></th>
				<td>
				    <label  class="text">${customerInfoEdit.firstName}</label>
				</td>
				
			
			<th><label  class="text"><fmt:message key="memberman.customman.middlename" /></label></th>
				<td>
				    <label  class="text">${customerInfoEdit.middleName}</label>
				</td>
			
				</tr>
				<tr>
				<th><label  class="text"><fmt:message key="memberman.customman.lastname" />:</label></th>
				<td>
				   <label  class="text">${customerInfoEdit.lastName}</label>
				</td>
				<th><label  class="text"><fmt:message key="memberman.customman.secondemail" />:</label></th>
				<td>
				    <label  class="text">${customerInfoEdit.secondEmail}</label>
				</td>
				</tr>
				<tr>
				<th><label  class="text"><fmt:message key="memberman.customman.country" />:</label></th>
				<td>
				    <label  class="text">${customerInfoEdit.country}</label>
				</td>
					<th><label  class="text"><fmt:message key="memberman.customman.company" />:</label></th>
				<td>
				    <label  class="text">${customerInfoEdit.company}</label>
				</td>
				</tr>
				<tr>
				<th><label  class="text"><fmt:message key="memberman.customman.address" />:</label></th>
				<td>
				    <label  class="text">${customerInfoEdit.address}</label>
				</td>
				<th><label  class="text"><fmt:message key="memberman.customman.city" />:</label></th>
				<td>
				    <label  class="text">${customerInfoEdit.city}</label>
				</td>
				</tr>
				<tr>
					<th><label  class="text"><fmt:message key="memberman.customman.zipcode" />:</label></th>
				<td>
				       <label  class="text">${customerInfoEdit.zipCode}</label>
				</td>
				</tr>
				 <!-- 
				 <tr>
				<th>&nbsp;</th>
				<th colspan="3">
					<label style="width: 100px;float:left;margin-left:0px;" class="text"><fmt:message key="memberman.customman.countrycode" /></label>
					<label style="width: 100px;float:left;margin-left:5px;" class="text"><fmt:message key="memberman.customman.areacode" /></label>
					<label style="width: 110px;float:left;margin-left:5px;" ><fmt:message key="memberman.customman.phonenum" /></label>
				</th>
			</tr>
			-->
				<tr>
				<th><label class="text"><fmt:message key="memberman.customman.telphonenum" />:</label></th>
				<td colspan="3">
				    <label style="width: 100px;"  class="text">${customerInfoEdit.daytimePhoneCC}</label>
				    <label style="width: 100px;"  class="text">${customerInfoEdit.daytimePhoneAC}</label>
				    <label style="width: 100px;"  class="text">${customerInfoEdit.daytimePhone}</label>
				</td>
				</tr>
			
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.cellphonenum" />:</label></th>
				<td colspan="3">
				    <label style="width: 100px;"  class="text">${customerInfoEdit.mobilePhoneCC}</label>
				    <label style="width: 100px;"  class="text">${customerInfoEdit.mobilePhoneAC}</label>
				    <label style="width: 100px;"  class="text">${customerInfoEdit.mobilePhone}</label>
				</td>
			</tr>
			<tr>
				<th><label class="text"><input disabled class="isallowsendemail" type="checkbox" name="customerInfoEdit.isAllowSendEmail" isallowsendemail="${customerInfoEdit.isAllowSendEmail}" value="1" /> <fmt:message key="memberman.customman.receivemail" /></label></th>
				<td colspan="3">
					<fmt:message key="memberman.customman.mailcontent" />
				</td>
			</tr>
		</table>
		</div>
				
</div>
			
			
	

