<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.saleinfoman.addsaleinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/saleInfo.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.sealerman.title" /> &gt; <fmt:message key="memberman.saleinfoman.addsaleinfo" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.saleinfoman.addsaleinfo" />
			</h4>
	<form action="insertsaleinfo.do"  class="validate" method="post" name="f1" id="f1">
		<input type="hidden" name="saleInfoVoSel.sealerName" value="${saleInfoVoSel.sealerName}" />
		<input type="hidden" name="saleInfoVoSel.proName" value="${saleInfoVoSel.proName}" />
		<input type="hidden" name="saleInfoVoSel.proExpand" value="${saleInfoVoSel.proExpand}" />
		<input type="hidden" name="saleInfoVoSel.company" value="${saleInfoVoSel.company}" />
		
		<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.saleinfoman.sealer" />:</label></th>
				<td>
					<select name="saleInfoAdd.sealerName" isrequired="true">
						<option value="">--<fmt:message key="select.option.select" />--</option>
						<c:forEach items="${sealerInfos }" var="item">
							<option value="${item.autelId}">${item.autelId}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.saleinfoman.productname" />:</label></th>
				<td>
					<input type="text" name="saleInfoAdd.proName" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.saleinfoman.productsize" />:</label></th>
				<td>
					<input type="text" name="saleInfoAdd.proExpand" maxlength="80" style="width: 300px" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.saleinfoman.unit" />:</label></th>
				<td>
					<input type="text" name="saleInfoAdd.company" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.saleinfoman.salesnum" />:</label></th>
				<td>
					<input type="text" name="saleInfoAdd.salesNum" maxlength="40" dataType="int" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.saleinfoman.salesamount" />:</label></th>
				<td>
					<input type="text" name="saleInfoAdd.salesMoney" maxlength="40" dataType="int" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.saleinfoman.returnnum" />:</label></th>
				<td>
					<input type="text" name="saleInfoAdd.pinBackNum" maxlength="40" dataType="int"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.saleinfoman.returnamount" />:</label></th>
				<td>
					<input type="text" name="saleInfoAdd.pinBackMoney" maxlength="40" dataType="int"/>
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="button" type="button" value="<fmt:message key="common.btn.ok" />" class="submitBtn insertsaleinfo" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">
	$("form.validate").validate();
	</script>
	
</body>
</html>
