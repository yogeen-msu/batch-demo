<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> > <fmt:message key="memberman.customman.title" /> > <fmt:message key="memberman.customman.safeproblem" /></div>
<form method="post" action="question-info!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="memberman.customman.safeproblem" /></h4>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='question-info!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
			</div>
		</div>
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="200"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="memberman.customman.safeproblem" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.code} </grid:cell>
				<grid:cell>${item.questionDesc}</grid:cell>
				<grid:cell>
					<a href="question-info!edit.do?id=${item.id}">
						<fmt:message key="common.list.mod" />
					</a>
					&nbsp;&nbsp;
					<a  href="question-info!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="memberman.customman.question.removecheck" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
