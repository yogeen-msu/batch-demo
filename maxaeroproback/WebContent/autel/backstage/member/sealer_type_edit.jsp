<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.sealertypeman.modsealertype" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/sealerType.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.sealerman.title" /> &gt; <fmt:message key="memberman.sealertypeman.modsealertype" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.sealertypeman.modsealertype" />
			</h4>
	<form action="updatesealertype.do"  class="validate" method="post" name="f1" id="f1">
	
		<input type="hidden" name="sealerTypeEdit.code" id="sealerTypeEdit.code" value="${sealerTypeEdit.code}" />
		<input type="hidden" name="selectedcodes" id="selectedcodes" value="${selectedcodes}" />
		<input type="hidden" name="sealerTypeVoSel.sealerName" value="${sealerTypeVoSel.sealerName}" />
		<input type="hidden" name="sealerTypeVoSel.areaCfgName" value="${sealerTypeVoSel.areaCfgName}" />

		<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="common.list.code" />:</label></th>
				<td>
					${sealerTypeEdit.code}
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealertypeman.name.name" />:</label></th>
				<td>
					<input type="text" name="sealerTypeEdit.name" value="${sealerTypeEdit.name}" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="button" type="button" value="<fmt:message key="produdt.js.save" />" class="submitBtn updatesealertype" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">
	$("form.validate").validate();
	</script>
	
</body>
</html>
