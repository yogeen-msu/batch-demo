<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > 
	工具 > 经销商权限管理
</div>
<form method="post" action="product-info!list.do">
	<div class="grid">
		
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div>经销商权限管理</h4>
			<div class="active">
				<h4><span>
					<input type="button" onclick="location.href='addSealerAuth.do'" value=<fmt:message key="common.btn.add" /> class="search_but" />
				</span>
				&nbsp;&nbsp;
				</h4>
			</div>
		</div>
		
		
		<grid:grid from="webpage">
			<grid:header>
					<grid:cell width="15%">编码</grid:cell>
					<grid:cell width="40%">URL</grid:cell>
					<grid:cell width="8%">导航编码</grid:cell>
					<grid:cell width="10%">名称</grid:cell>
					<grid:cell width="5%">排序</grid:cell>
					<grid:cell width="10%">操作</grid:cell>
				</grid:header>
		
			<grid:body item="item">
					<grid:cell>${item.code}</grid:cell>
					<grid:cell>${item.url}</grid:cell>
					<grid:cell>${item.sealermenu}</grid:cell>
					<grid:cell>${item.name}</grid:cell>
					<grid:cell>${item.sortNum}</grid:cell>
					<grid:cell>
						<a href="editSealerAuth.do?sealerAuth.code=${item.code}"  ><fmt:message key="cententman.cms.edit" /></a>
						&nbsp;&nbsp;
						<a href="delSealerAuth.do?sealerAuth.code=${item.code}"  ><fmt:message key="common.js.delInfo" /></a>
					</grid:cell>
				</grid:body>
		
		</grid:grid>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		
		
		
	});
</script>

