<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.customman.customproupgradeinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>


</head>

<body>
<form action="listproupgrade.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="memberman.customman.customproupgradeinfo" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.customman.customproupgradeinfo" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.customman.cusid" />:</td>
						<td width="154"><input name="proUpgradeRecordVoSel.autelId" value="${proUpgradeRecordVoSel.autelId}" type="text" class="time_in"/></td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td width="154"><input name="proUpgradeRecordVoSel.proSerialNo" value="${proUpgradeRecordVoSel.proSerialNo}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="product.software" />:</td>
						<td><input name="proUpgradeRecordVoSel.softwareTypeName" value="${proUpgradeRecordVoSel.softwareTypeName}" type="text" class="time_in" /></td>
					</tr>
					<tr height="38">
						<td align="right">
							<fmt:message key="memberman.customman.upgradetime" />:
						</td>
						<td width="180px">
							<input name="proUpgradeRecordVoSel.beginDate" value="${proUpgradeRecordVoSel.beginDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="proUpgradeRecordVoSel.endDate" value="${proUpgradeRecordVoSel.endDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							
						</td>
						<td align="right"></td>
						<td><input type="button" id="submit_but" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" /></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="memberman.customman.customproupgradeinfo" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="product.software" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.upgradetime" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.autelId}</grid:cell>
					<grid:cell>${item.customerName}</grid:cell>
					<grid:cell>${item.proSerialNo}</grid:cell>
					<grid:cell>${item.softwareTypeName}</grid:cell>
					<grid:cell>${item.upgradeTime}</grid:cell>					
				</grid:body>
			  	<tr>
			    	<td colspan="5" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but exportexcel" />&nbsp;&nbsp;<input name="" type="button" value="<fmt:message key="common.btn.clearall" />" class="diw_but clearall" /></td>
			  	</tr>
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>

<script type="text/javascript">

	$(document).ready(function(){
		
		$(".exportexcel").click(function(){
			var $this = $(this);
			var contextpath = $("#contextpath").val();
			$("#f1").attr("action",contextpath+"/proUpgradeRecordExportExcel.do").submit();
		});
		
		$(".clearall").click(function(){
			var $this = $(this);
			var contextpath = $("#contextpath").val();
			if(confirm("<fmt:message key="memberman.customman.clearallupgrade" />")){
				$("#f1").attr("action","clearproupgrade.do").submit();
			}
		});
	});
	
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		$("#submit_but").click(function(){
			//搜索
			$("#f1").attr("action","listproupgrade.do").submit();
		});
	});
</script>
