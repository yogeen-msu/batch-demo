<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.complaintman.following" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerComplaintInfo.js"></script>


<script type="text/javascript">
var message3 = '<fmt:message key="memberman.complaintman.message3" />';
var message2 = '<fmt:message key="memberman.complaintman.message2" />';
var message1 = '<fmt:message key="memberman.complaintman.message1" />';
var message4 = '<fmt:message key="memberman.complaintman.message4" />';
var message5 = '<fmt:message key="memberman.complaintman.message5" />';
var message6 = '<fmt:message key="memberman.complaintman.message6" />';
var stateopen = '<fmt:message key="memberman.complaintman.stateopen" />';
var stateguaqi = '<fmt:message key="memberman.complaintman.stateguaqi" />';
var stateddhf = '<fmt:message key="memberman.complaintman.stateddhf" />';
var stateguanbi = '<fmt:message key="memberman.complaintman.stateguanbi" />';
</script>


</head>

<body>
<form action="listcustomercomplaint.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
    <input type="hidden" name="complaintState" id="complaintState" value="${customerComplaintInfoVoSel.complaintState}" />
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="memberman.complaintman.following" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.complaintman.following" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.complaintman.acceptman" />:&nbsp;&nbsp;</td>
						<td width="154"><input name="customerComplaintInfoVoSel.acceptor" value="${customerComplaintInfoVoSel.acceptor}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:&nbsp;&nbsp;</td>
						<td width="154"><input name="customerComplaintInfoVoSel.productNo" value="${customerComplaintInfoVoSel.productNo}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.code" />:&nbsp;&nbsp;</td>
						<td><input name="customerComplaintInfoVoSel.code" value="${customerComplaintInfoVoSel.code}" type="text" class="time_in" /></td>
					</tr>
					<tr height="38">
						<td align="right"><fmt:message key="memberman.complaintman.complaintmanname" />:&nbsp;&nbsp;</td>
						<td><input name="customerComplaintInfoVoSel.complaintPerson" value="${customerComplaintInfoVoSel.complaintPerson}" type="text" class="time_in" /></td>
						<td align="right"><fmt:message key="memberman.complaintman.state" />:</td>
						<td>
							<select style="width:167px" class="act_zt usertype" name="customerComplaintInfoVoSel.complaintState" id="complaintState2">
								<option value="-1"><fmt:message key="common.btn.search.all" /></option>
								<option value="1" ><fmt:message key="customercomplaint.complaintstate.open2"/></option>	
	         		            <option value="2" ><fmt:message key="customercomplaint.complaintstate.guaqi2"/></option>
	         		            <option value="3" ><fmt:message key="customercomplaint.complaintstate.waitreply2"/></option>
	         		            <option value="4" ><fmt:message key="customercomplaint.complaintstate.close2"/></option>
							</select>
							
						</td>
						<td>&nbsp;</td>
						<td style="padding-right:0px"><input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" /></td>
					</tr>
					
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="memberman.complaintman.customcomplaint" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
				    <grid:cell width="10%"><fmt:message key="customercomplaint.tickedid.name" /></grid:cell>
				    <grid:cell width="25%"><fmt:message key="memberman.complaintman.complaintmanname" /></grid:cell>
				    <grid:cell width="10%"><fmt:message key="memberman.complaintman.acceptman" /></grid:cell>
				    <grid:cell width="15%"><fmt:message key="memberman.complaintman.state" /></grid:cell>
				    <grid:cell width="15%"><fmt:message key="memberman.complaintman.createtime" /></grid:cell>
				    <grid:cell width="15%"><fmt:message key="memberman.complaintman.lastreplytime" /></grid:cell>
				    <grid:cell width="20%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="item">
				
				  <tr class="tr_bgc">
				    <td>${item.code}</td>
				    <td>${item.complaintPerson}</td>
				    <td>${item.acceptor}</td>
				    <td class="complaintstate" complaintstate="${item.complaintState}"></td>
				    <td>${item.complaintDate}</td>
				    <td>${item.lastReDate}</td>
				    <td class="caoz">
					    <a class="showcomplaint" code="${item.code}" href="#"><fmt:message key="memberman.complaintman.show" /></a>&nbsp;
					    <a class="allotcomplaint" code="${item.code}" href="#"><fmt:message key="memberman.complaintman.transcomplaint" /></a>
				    </td>
				  </tr>
				</c:forEach>
				  <tr>
<%-- 				    <td colspan="9" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but1" /></td> --%>
				  </tr>
			</grid:grid>
		</div>
	</div>	
			
</form>
	
				
	<div id="overlay"></div>
	
	<div id="win_complaint">
	   	<h2><span id="close">×</span></h2>
		<div class="tc_tab_ts">
		<table width="100%" border="0">
		  <tr>
		    <td width="35%" align="center"><fmt:message key="memberman.complaintman.customserviceid" /></td>
		    <td width="35%" align="center"><fmt:message key="syssetting.authorityman.managername" /></td>
		    <td width="30%" align="center"><fmt:message key="common.list.operation" /></td>
		  </tr>
		  <c:forEach items="${customerServiceUsers}" var="item">
		  <tr align="center">
		    <td>${item.username}</td>
		    <td>${item.realname}</td>
		    <td><input type="button" class="diw_but selacceptortrans" acceptor="${item.username}" value="<fmt:message key="common.openselect.choose" />" /></td>
		  </tr>
		  </c:forEach>
		</table>
		</div>
	</div>
	
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
	
	
	
</script>

