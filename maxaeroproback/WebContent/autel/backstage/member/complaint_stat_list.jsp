<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.complaintstatman.info" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>


</head>

<body>
<form action="listcustomercomplaintstat.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="memberman.complaintstatman.info" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.complaintstatman.rating" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="150" align="right"><fmt:message key="memberman.complaintman.customserviceid" />:</td>
						<td width="154"><input name="customerComplaintInfoVoSel.acceptor" value="${customerComplaintInfoVoSel.acceptor}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="customer.complaint.rating.date" />:</td>
						<td width="180">
							<input name="customerComplaintInfoVoSel.beginDate" value="${customerComplaintInfoVoSel.beginDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="customerComplaintInfoVoSel.endDate" value="${customerComplaintInfoVoSel.endDate}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
						</td>
						<td>&nbsp;&nbsp;<input type="button" id="submit_but" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" /></td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="memberman.complaintstatman.info" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
				    <grid:cell width="12%"><fmt:message key="memberman.complaintman.customserviceid" /></grid:cell>
				    <grid:cell width="12%"><fmt:message key="memberman.complaintstatman.fivepoint" /></grid:cell>
				    <grid:cell width="12%"><fmt:message key="memberman.complaintstatman.fourpoint" /></grid:cell>
				    <grid:cell width="12%"><fmt:message key="memberman.complaintstatman.threepoint" /></grid:cell>
				    <grid:cell width="12%"><fmt:message key="memberman.complaintstatman.twopoint" /></grid:cell>
				    <grid:cell width="12%"><fmt:message key="memberman.complaintstatman.onepoint" /></grid:cell>
				    <grid:cell width="12%"><fmt:message key="memberman.complaintstatman.default" /></grid:cell>
				    <grid:cell width="12%"><fmt:message key="memberman.complaintstatman.total" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="item">
				  <tr class="tr_bgc">
				    <td>${item.rePerson}</td>
				    <td>${item.fivePoint}</td>
				    <td>${item.fourPoint}</td>
				    <td>${item.threePoint}</td>
				    <td>${item.twoPoint}</td>
				    <td>${item.onePoint}</td>
				    <td>${item.defaultPoint}</td>
				    <td>${item.total}</td>
				  </tr>
				</c:forEach>
				  <tr>
				    <td colspan="8" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but exportexcel" /></td>
				  </tr>
			</grid:grid>
		</div>
	</div>	
			
</form>
	
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
		
		$(".exportexcel").click(function(){
			var $this = $(this);
			var contextpath = $("#contextpath").val();
			$("#f1").attr("action",contextpath+"/complaintStatExportExcel.do").submit();
		});
		
		$("#submit_but").click(function(){
			//搜索
			$("#f1").attr("action","listcustomercomplaintstat.do").submit();
		});
	});
</script>

