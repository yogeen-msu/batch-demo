<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerInfo.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>
<title><fmt:message key="memberman.customman.cusinfomod" /></title>
</head>
<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="memberman.customman.cusinfomod" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.customman.cusinfomod" />
			</h4>
	<form id="f1" name="f1" class="validate" action="updatecustomer.do" method="post">
	<input type="hidden" name="customerInfoEdit.id" id="customerInfoEdit.id" value="${customerInfoEdit.id}" />
	<input type="hidden" name="customerInfoEdit.code" id="customerInfoEdit.code" value="${customerInfoEdit.code}" />
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="${selectedcodes}" />
	<input type="hidden" name="customerInfoVoSel.autelId" value="${customerInfoVoSel.autelId}" />
	<input type="hidden" name="customerInfoVoSel.sealerAutelId" value="${customerInfoVoSel.sealerAutelId}" />
	<input type="hidden" name="customerInfoVoSel.areaCfgName" value="${customerInfoVoSel.areaCfgName}" />
	<input type="hidden" name="customerInfoVoSel.actState" value="${customerInfoVoSel.actState}" />
	<input type="hidden" name="languageCodeCN" id="languageCodeCN" value="${languageCodeCN}" />
	
	<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.cusid" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.autelId" value="${customerInfoEdit.autelId}" maxlength="40"  isrequired="true" readonly="readonly"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.passwd" />:</label></th>
				<td>
					<input type="password" name="customerInfoEdit.userPwd" maxlength="20" dataType="string"/>(<fmt:message key="memberman.customman.passwordinfo" />)
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.repasswd" />:</label></th>
				<td>
					<input type="password" name="" maxlength="20" dataType="string" fun="checkpwd"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.preferredlanguage" />:</label></th>
				<td>
					<select class="languagecode" name="customerInfoEdit.languageCode" languagecode="${customerInfoEdit.languageCode}" isrequired="true">
									<option value="">--<fmt:message key="common.sel.choose" />--</option>
									<c:forEach items="${languages }" var="item">
										<option value="${item.code}">${item.name}</option>
									</c:forEach>
					</select>
				</td>
			</tr>
				
		<%-- 	<tr class="customername">
				<th><label class="text"><fmt:message key="memberman.customman.cusname" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.name" value="${customerInfoEdit.name}" maxlength="40" dataType="string"/>
				</td>
			</tr> --%>
				
			<tr class="customernameenglish">
				<th><label class="text"><fmt:message key="memberman.customman.firstname" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.firstName" value="${customerInfoEdit.firstName}" maxlength="40" dataType="string"/>
				</td>
			</tr>
				
			<tr class="customernameenglish">
				<th><label class="text"><fmt:message key="memberman.customman.middlename" /></label></th>
				<td>
					<input type="text" name="customerInfoEdit.middleName" value="${customerInfoEdit.middleName}" maxlength="40" dataType="string"/>
				</td>
			</tr>
				
			<tr class="customernameenglish">
				<th><label class="text"><fmt:message key="memberman.customman.lastname" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.lastName" value="${customerInfoEdit.lastName}" maxlength="40" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.secondemail" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.secondEmail" value="${customerInfoEdit.secondEmail}" maxlength="40" dataType="email"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.country" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.country" value="${customerInfoEdit.country}" maxlength="40" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.company" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.company" value="${customerInfoEdit.company}" maxlength="40" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.address" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.address" value="${customerInfoEdit.address}" maxlength="80" style="width: 300px" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.city" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.city" value="${customerInfoEdit.city}" maxlength="40" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.zipcode" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.zipCode" value="${customerInfoEdit.zipCode}" maxlength="40" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th></th>
				<td>
					<div style="width: 55px;float:left;margin-left:0px;"><fmt:message key="memberman.customman.countrycode" /></div>
					<div style="width: 40px;float:left;margin-left:5px;"><fmt:message key="memberman.customman.areacode" /></div>
					<div style="width: 50px;float:left;margin-left:5px;"><fmt:message key="memberman.customman.phonenum" /></div>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.telphonenum" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.daytimePhoneCC" value="${customerInfoEdit.daytimePhoneCC}" maxlength="5" style="width: 40px" dataType="string"/>
					<input type="text" name="customerInfoEdit.daytimePhoneAC" value="${customerInfoEdit.daytimePhoneAC}" maxlength="6" style="width: 50px" dataType="string"/>
					<input type="text" name="customerInfoEdit.daytimePhone" value="${customerInfoEdit.daytimePhone}" maxlength="15" style="width: 120px" dataType="string"/>
					<input type="text" name="customerInfoEdit.daytimePhoneExtCode" value="${customerInfoEdit.daytimePhoneExtCode}" maxlength="8" style="width: 60px" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th></th>
				<td>
					<div style="width: 55px;float:left;margin-left:0px;"><fmt:message key="memberman.customman.countrycode" /></div>
					<div style="width: 40px;float:left;margin-left:5px;"><fmt:message key="memberman.customman.areacode" /></div>
					<div style="width: 50px;float:left;margin-left:5px;"><fmt:message key="memberman.customman.phonenum" /></div>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.customman.cellphonenum" />:</label></th>
				<td>
					<input type="text" name="customerInfoEdit.mobilePhoneCC" value="${customerInfoEdit.mobilePhoneCC}" maxlength="5" style="width: 40px" dataType="string"/>
					<input type="text" name="customerInfoEdit.mobilePhoneAC" value="${customerInfoEdit.mobilePhoneAC}" maxlength="6" style="width: 50px" dataType="string"/>
					<input type="text" name="customerInfoEdit.mobilePhone" value="${customerInfoEdit.mobilePhone}" maxlength="15" style="width: 120px" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><input class="isallowsendemail" type="checkbox" name="customerInfoEdit.isAllowSendEmail" isallowsendemail="${customerInfoEdit.isAllowSendEmail}" value="1" /> <fmt:message key="memberman.customman.receivemail" /></label></th>
				<td>
					<fmt:message key="memberman.customman.mailcontent" />
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="submit" type="submit" value="<fmt:message key="common.btn.ok" />" class="submitBtn updatecustomer" />
						<input name="button" type="button" onclick="location.href='listcustomer.do'" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">
	$("form.validate").validate();

		function checkpwd(){
			 
			if(  $("input[type='password']").eq(0).val() != $("input[type='password']").eq(1).val() ){
				return "<fmt:message key="memberman.customman.message1" />";
			}
			return true;
		}
	</script>
	
</body>
</html>