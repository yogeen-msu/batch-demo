<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>	
<title><fmt:message key="memberman.sealerman.info" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/sealerInfo.js"></script>		
<div style="height:10px"></div>
<div class="grid" style="padding-right:0px;border:1px solid #DDDDDD;border-left: #dddddd 0px solid;border-right: #dddddd 0px solid;width:97.8%">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cusloginip" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.cuslogintime" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.customerAutelId}</grid:cell>
					<grid:cell>${item.customerName}</grid:cell>
					<grid:cell>${item.loginIP}</grid:cell>
					<grid:cell>${item.loginTime}</grid:cell>				
				</grid:body>
			  	
				</grid:grid>
		</div>