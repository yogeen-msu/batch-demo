<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客诉来源管理</title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerComplaintSource.js"></script>


</head>

<body>
<form action="listcomplaintsource.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; 客诉来源管理
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;客诉来源管理
			</h4>
			<div class="active">
				<h4>
					<span>
						<input name="" type="button" value=<fmt:message key="common.btn.add" /> class="search_but addcomplaintsource" />
					</span>客诉来源:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="15%"><fmt:message key="common.list.code" /></grid:cell>
					<grid:cell width="15%">客诉来源名称</grid:cell>
					<grid:cell width="10%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.code}</grid:cell>
					<grid:cell>${item.name}</grid:cell>
					<grid:cell>
						<a code="${item.code}" href="#" class="editcomplaintsource"><fmt:message key="common.list.mod" /></a>&nbsp;
						<a code="${item.code}" href="#" class="dellcomplaintsource"><fmt:message key="common.list.del" /></a>
					</grid:cell>
				</grid:body>
				</grid:grid>
		</div>
	</div>
</form>
</body>
</html>
