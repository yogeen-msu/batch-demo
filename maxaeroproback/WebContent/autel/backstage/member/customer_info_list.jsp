<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.customman.infoman" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerInfo.js"></script>


<script type="text/javascript">
var message3 = '<fmt:message key="memberman.customman.message3" />';
var message2 = '<fmt:message key="memberman.customman.message2" />';
var message4 = '<fmt:message key="memberman.customman.message4" />';
var m_activated = '<fmt:message key="memberman.customman.activated" />';
var m_noactivated = '<fmt:message key="memberman.customman.noactivated" />';
</script>
</head>
<body>


<form action="listcustomer.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="memberman.customman.infoman" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.customman.infoman" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.customman.cusid" />:</td>
						<td width="154"><input name="customerInfoVoSel.autelId" value="${customerInfoVoSel.autelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.saleinfoman.sealerid" />:</td>
						<td width="154"><input name="customerInfoVoSel.sealerAutelId" value="${customerInfoVoSel.sealerAutelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.sealerinfoman.areaname" />:</td>
						<td><input name="customerInfoVoSel.areaCfgName" value="${customerInfoVoSel.areaCfgName}" type="text" class="time_in" /></td>
					</tr>
					<tr height="38">
						<td align="right"><fmt:message key="memberman.customman.cusstate" />:</td>
						<td>
							<SELECT style="width:170px"  class="actstatesel" actstatesel="${customerInfoVoSel.actState}" name="customerInfoVoSel.actState">
								<OPTION value="">--<fmt:message key="orderman.list.all" />--</OPTION>
								<OPTION value="0"><fmt:message key="memberman.customman.noactivated" /></OPTION>
								<OPTION value="1"><fmt:message key="memberman.customman.activated" /></OPTION>
							</SELECT>
						</td>
						<td align="right">
							<fmt:message key="memberman.customman.registertime" />:
						</td>
						<td width="170px">
							<input name="customerInfoVoSel.startTime" value="${customerInfoVoSel.startTime}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="customerInfoVoSel.endTime" value="${customerInfoVoSel.endTime}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							
						</td>
						<td></td>
						<td>
							<input type="button" value="<fmt:message key="common.btn.search" />" class="search_but searchbutton" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="memberman.customman.info" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="25px"><input type="checkbox" id="allCheckbox" name="allCheckbox" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.secondemail" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.customman.cellphonenum" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.saleinfoman.sealerid" /></grid:cell>
					<grid:cell width="11%"><fmt:message key="memberman.sealerinfoman.areaname" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="memberman.customman.registertime" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="memberman.customman.cusstate" /></grid:cell>
					<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="list">
					<tr code="${list.code}" align="center" height="30px">
						<td>
							<input type="checkbox" id="checkboxcode_${list.code}" name="checkboxcode" value="${list.code}" />
						</td>
						<td>${list.autelId}</td>
						<td>${list.name}</td>
						<td>${list.secondEmail}</td>
						<td>${list.mobilePhone}</td>
						<td>${list.sealerAutelId}</td>
						<td>${list.areaCfgName}</td>
						<td>${list.regTime}</td>
						<td class="actstate" actstate="${list.actState}"></td>
						<td style="white-space:nowrap;">
							<%-- <A class="edituser" href="#"><fmt:message key="common.list.mod" /></A> --%>
							<A href="editcustomer.do?code=${list.code}"><fmt:message key="common.list.mod" /></A>
							<A class="delluser" href="#"><fmt:message key="common.list.del" /></A>
						</td>
					</tr>
				</c:forEach>
				
				<tr height="30px">
					<th colspan="10" align="left">
						&nbsp;<input type="checkbox" id="allCheckbox2" name="allCheckbox" />&nbsp;<fmt:message key="common.list.all" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input class="dellusers" type="button" value="<fmt:message key="memberman.customman.delcheckeduser" />" />
					</th>
				</tr>
			  	<tr>
			    	<td colspan="10" align="left" class="color"><input name="" type="button" value="<fmt:message key="common.btn.exportexcel" />" class="diw_but exportexcel" /></td>
			  	</tr>
			</grid:grid>
		</div>
	</div>	
			
</form>

</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>