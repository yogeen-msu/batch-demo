<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.customman.infoman" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerInfo.js"></script>


<script type="text/javascript">
var message3 = '<fmt:message key="memberman.customman.message3" />';
var message2 = '<fmt:message key="memberman.customman.message2" />';
var message4 = '<fmt:message key="memberman.customman.message4" />';
var m_activated = '<fmt:message key="memberman.customman.activated" />';
var m_noactivated = '<fmt:message key="memberman.customman.noactivated" />';
</script>
</head>
<body>


<form action="listActiveCustomer.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />
	<input type="hidden" name="contextpath" id="contextpath" value="${ctx}" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.customman.title" /> &gt; <fmt:message key="customer.active.list"/>
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="customer.active.list"/>
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.customman.cusid" />:</td>
						<td width="154"><input name="customerInfoVoSel.autelId" value="${customerInfoVoSel.autelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="memberman.saleinfoman.sealerid" />:</td>
						<td width="154"><input name="customerInfoVoSel.sealerAutelId" value="${customerInfoVoSel.sealerAutelId}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="myproducts.sn.name" />:</td>
						<td><input name="customerInfoVoSel.serialNo" value="${customerInfoVoSel.serialNo}" type="text" class="time_in" /></td>
					</tr>
					<tr height="38">
						<td align="right"><fmt:message key="memberman.customman.cusstate" />:</td>
						<td>
							<SELECT style="width:170px"  class="actstatesel" actstatesel="${customerInfoVoSel.actState}" name="customerInfoVoSel.actState">
								<OPTION value="">--<fmt:message key="orderman.list.all" />--</OPTION>
								<OPTION value="0"><fmt:message key="memberman.customman.noactivated" /></OPTION>
								<OPTION value="1"><fmt:message key="memberman.customman.activated" /></OPTION>
							</SELECT>
						</td>
						<td align="right">
							<fmt:message key="memberman.customman.registertime" />:
						</td>
						<td width="170px">
							<input name="customerInfoVoSel.startTime" value="${customerInfoVoSel.startTime}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							--
							<input name="customerInfoVoSel.endTime" value="${customerInfoVoSel.endTime}" type="text" dataType="date" class="dateinput" style="width:66px" readonly="readonly" />
							
						</td>
						<td></td>
						<td>
							<input type="button" value="<fmt:message key="common.btn.search" />" class="search_but searchbutton" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
					</span><fmt:message key="memberman.customman.info" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="20%"><fmt:message key="memberman.customman.cusid" /></grid:cell>
					<grid:cell width="20%"><fmt:message key="memberman.customman.cusname" /></grid:cell>
					<grid:cell width="20%"><fmt:message key="myproducts.sn.name" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.customman.registertime" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="memberman.customman.cusstate" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<c:forEach items="${webpage.result}" var="list">
					<tr code="${list.code}" align="center" height="30px">
						<td>${list.autelId}</td>
						<td>${list.name}</td>
						<td>${list.serialNo}</td>
						<td>${list.regTime}</td>
						<td class="actstate" actstate="${list.actState}"></td>
						<td style="white-space:nowrap;">
						    <c:if test="${list.actState==0}">
							<A class="activeUser" href="#"><fmt:message key="customer.active.active"/></A>
						    </c:if>
						    
						    <A href="updatePwd.do?code=${list.code}" class="updateUserPwd" href="#"><fmt:message key="mydistributor.editpassword"/></A>
						    
						</td>
					</tr>
				</c:forEach>
				
			</grid:grid>
		</div>
	</div>	
			
</form>

</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>