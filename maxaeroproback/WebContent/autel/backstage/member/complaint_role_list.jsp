<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.complaintman.authority" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/customerComplaintInfo.js"></script>


<script type="text/javascript">
var message3 = '<fmt:message key="memberman.complaintman.message3" />';
var message2 = '<fmt:message key="memberman.complaintman.message2" />';
var message1 = '<fmt:message key="memberman.complaintman.message1" />';
var stateopen = '<fmt:message key="memberman.complaintman.stateopen" />';
var stateguaqi = '<fmt:message key="memberman.complaintman.stateguaqi" />';
var stateddhf = '<fmt:message key="memberman.complaintman.stateddhf" />';
var stateguanbi = '<fmt:message key="memberman.complaintman.stateguanbi" />';
</script>


</head>

<body>
<form action="listcomplaintrole.do" method="post" id="f1" name="f1">
	
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="code" id="code" value="" />

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt; <fmt:message key="memberman.complaintman.title" /> &gt; <fmt:message key="memberman.complaintman.authority" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.complaintman.assign.info" />
			</h4>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell width="15%"><fmt:message key="memberman.complainttypeman.typename" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="memberman.complaintman.support" /></grid:cell>
					<grid:cell width="15%"><fmt:message key="accountinformation.first.email" /></grid:cell>
					<grid:cell width="10%"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item">
					<grid:cell>${item.complaintTypeName}</grid:cell>
					<grid:cell>${item.adminUserid}</grid:cell>
					<grid:cell>${item.emailName}</grid:cell>
					<grid:cell>
						<a href="editcomplaintrole.do?code=${item.complaintTypeCode}" code="${item.complaintTypeCode}" ><fmt:message key="cententman.cms.edit" /></a>
					</grid:cell>
				</grid:body>
			</grid:grid>
		</div>
	</div>
</form>
	
				
	<div id="overlay"></div>
	
	<div id="win_complaint">
	   	<h2><span id="close">×</span></h2>
		<div class="tc_tab_ts">
		<table width="100%" border="0">
		  <tr>
		    <td width="35%" align="center"><fmt:message key="memberman.complaintman.customserviceid" /></td>
		    <td width="35%" align="center"><fmt:message key="syssetting.authorityman.managername" /></td>
		    <td width="30%" align="center"><fmt:message key="common.list.operation" /></td>
		  </tr>
		  <c:forEach items="${customerServiceUsers}" var="item">
		  <tr align="center">
		    <td>${item.username}</td>
		    <td>${item.realname}</td>
		    <td><input type="button" class="diw_but selacceptor" acceptor="${item.username}" value="<fmt:message key="common.openselect.choose" />" /></td>
		  </tr>
		  </c:forEach>
		</table>
		</div>
	</div>
</body>
</html>
