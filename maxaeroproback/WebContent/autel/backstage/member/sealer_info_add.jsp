<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="memberman.sealerinfoman.addsealerinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/member/sealerInfo.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="memberman.title" /> &gt;
	<fmt:message key="memberman.sealerman.title" /> &gt; <fmt:message key="memberman.sealerinfoman.addsealerinfo" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="memberman.sealerinfoman.addsealerinfo" />
			</h4>
	<form action="insertsealer.do"  class="validate" method="post" name="f1" id="f1">
		<input type="hidden" name="sealerInfoVoSel.sealerName" value="${sealerInfoVoSel.sealerName}" />
		<input type="hidden" name="sealerInfoVoSel.areaCfgName" value="${sealerInfoVoSel.areaCfgName}" />
		<input type="hidden" name="languageCodeCN" id="languageCodeCN" value="${languageCodeCN}" />
		
		<table class="form-table">
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.number" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.autelId" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.passwd" />:</label></th>
				<td>
					<input type="password" name="sealerInfoAdd.userPwd" maxlength="40" dataType="string" isrequired="true" fun="checkpwdlen"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.repasswd" />:</label></th>
				<td>
					<input type="password" name="" maxlength="40" dataType="string" fun="checkpwd"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.preferredlanguage" />:</label></th>
				<td>
					<select class="languagecodeadd" name="sealerInfoAdd.languageCode" isrequired="true">
									<option value="">--<fmt:message key="select.option.select" />--</option>
									<c:forEach items="${languages }" var="item">
										<option value="${item.code}">${item.name}</option>
									</c:forEach>
							</select>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.firstname" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.firstName" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.lastname" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.lastName" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.email" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.email" maxlength="40" dataType="email" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.secondemail" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.secondEmail" maxlength="40" dataType="email"/>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="regcustomer.info.province" />:</label></th>
				<td>
					<select class="provincecodesadd" name="sealerInfoAdd.province" id="province" onchange="selectProinces();" fun="validateProvince">
						<option value="">--<fmt:message key="select.option.select" />--</option>
						<c:forEach items="${stateProvinces }" var="item">
							<option value="${item.code};${item.country}">${item.state}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.country" />:</label></th>
				<td>
					<select class="countrycodesadd" name="sealerInfoAdd.country" isrequired="true" id="country" onchange="selectCountry()">
						<option value="">--<fmt:message key="select.option.select" />--</option>
						<c:forEach items="${countryCodes }" var="item">
							<option value="${item.code};${item.state}">${item.country}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.company" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.company" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.address" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.address" maxlength="80" style="width: 300px" dataType="string"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.city" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.city" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.zipcode" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.zipCode" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.sealerinfoman.phonec" />:</label></th>
				<td>
					<input type="text" name="sealerInfoAdd.telephone" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
			<tr>
				<th><label class="text"><input type="checkbox" name="sealerInfoAdd.isAllowSendEmail" value="1" /><fmt:message key="memberman.sealerinfoman.isallowsendemail" /></label></th>
				<td>
					<fmt:message key="memberman.sealerinfoman.sendemaildemo" />
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="button" type="button" value="<fmt:message key="produdt.js.save" />" class="submitBtn insertsealer" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">
		$("form.validate").validate();

		function checkpwd(){
			 
			if(  $("input[type='password']").eq(0).val() != $("input[type='password']").eq(1).val() ){
				return "<fmt:message key="memberman.sealerinfoman.message1" />";
			}
			return true;
		}

		function checkpwdlen(){
			 
			if(  $("input[type='password']").eq(0).val().length < 6){
				return "<fmt:message key="update.confirmpassword.lengthcheck" />";
			}
			return true;
		}
		function validateProvince(){
			var country = $("#country").val();
			var provinces = country.split(";")[1];
			if(!$("#province").val() && provinces != "none"){
				return "<fmt:message key="memberman.sealerinfoman.message4" />";
			}
			return true;
		}
		function selectCountry(){
			var country = $("#country").val();
			if(!country)
				return false;
			var provinces = country.split(";")[1];
			if(provinces && provinces != "none"){
				selected($("#province option"),provinces);
			}else if(provinces == "none"){
				$("#province option:eq(0)").attr("selected",true);
			}else{
				selectProinces();
			}
		}
		function selectProinces(){
			var provinces = $("#province").val();
			if(!provinces)
				return false;
			var country = provinces.split(";")[1];
			selected($("#country option"),country);
		}
		function selected(obj,t){
			$(obj).each(function(){
				if($(this).text() == t){
					$(this).attr("selected",true);
				}
			});
		}
	</script>
</body>
</html>
