<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="iepngfix_tilebg.js"></script>
<script type="text/javascript">
	function CheckBrowser(){
	    var cb = "Unknown";
	    if(window.ActiveXObject){
	        cb = "IE";
	    }else if(navigator.userAgent.toLowerCase().indexOf("firefox") != -1){
	        cb = "Firefox";
	    }else if((typeof document.implementation != "undefined") && (typeof document.implementation.createDocument != "undefined") && (typeof HTMLDocument != "undefined")){
	        cb = "Mozilla";
	    }else if(navigator.userAgent.toLowerCase().indexOf("opera") != -1){
	        cb = "Opera";
	    }
	    return cb;
	}
	
	//根据浏览器类型选择是否刷新frame
	var browserType = CheckBrowser();
	if(browserType == "IE"){
		window.parent.frames["leftMenuFrame"].location.reload();
	}
</script>
</head>
<body>
    	<div class="leg_topri">您的当前位置:营销管理 &gt; 产品软件销售策略 &gt; 限时购买</div>
        <div class="right_tab">
        	<div class="sear">
            	<h4 class="sear_tj"><img src="images/leg_16.gif" width="14" height="16" />&nbsp;搜索条件111</h4>
                <div class="sear_table">
                <table width="100%" border="0">
  <tr height="5">
    
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr height="38">    
    <td width="111" align="right">创建时间:</td>
    <td width="154"><input name="" type="text" class="time_in" /></td>
    <td width="60" align="center">至</td>
    <td width="154"><input name="" type="text" class="time_in" /></td>
    <td width="60">创建人:</td>
    <td><input name="" type="text" class="time_in" /></td>
  </tr>
  <tr height="38">
    
    <td width="111" align="right">创建时间:</td>
    <td width="154"><input name="" type="text" class="time_in" /></td>
    <td width="60" align="center">至</td>
    <td width="154"><input name="" type="text" class="time_in" /></td>
    <td width="60">活动状态:</td>
    <td><select name="" class="act_zt"><option>全部活动</option><option>正在进行</option><option>未开始</option><option>已过期</option><option>已暂停</option></select></td>
  </tr>
  <tr height="38">
    
    <td>&nbsp;</td>
    <td><input name="" type="button" value="搜索" class="search_but" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr height="5">
    
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
				</div>
                <div class="active">
                	<h4><span><input name="" type="button" value="添加活动" class="search_but" /></span>活动列表</h4>
                </div>
            </div>
            <div class="act"><table width="100%" border="0" bordercolor="#dddddd">
  <tr class="title">
    <td width="35">&nbsp;</td>
    <td width="25%">活动名称</td>
    <td width="13%">开始时间</td>
    <td width="13%">结束时间</td>
    <td width="12%">创建人</td>
    <td width="11%">状态</td>
    <td>操作</td>
  </tr>
  <tr>
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr class="tr_bgc">
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr>
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr class="tr_bgc">
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr>
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr class="tr_bgc">
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr>
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr class="tr_bgc">
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr>
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr class="tr_bgc">
    <td><input name="" type="checkbox" value="" /></td>
    <td>v3.2.14.0</td>
    <td>2012-12-30</td>
    <td>2012-12-31</td>
    <td>张三</td>
    <td>正在进行</td>
    <td class="caoz"><a href="#">追踪</a>&nbsp;&nbsp;&nbsp;<a href="#">修改</a>&nbsp;&nbsp;&nbsp;<a href="#">删除</a></td>
  </tr>
  <tr>
    <td colspan="7" align="left" class="color"><input name="" type="checkbox" value="" style="margin-left:11px; margin-right:11px;" />全选<input name="" type="button" value="暂停选中活动" class="diw_but" /><input name="" type="button" value="启用选中活动" class="diw_but" /><input name="" type="button" value="删除选中活动" class="diw_but" /></td>
    </tr>
  <tr class="bott_col">
    <td colspan="7" class="page" align="left" >
    	<span style="float:right;">当前显示1-10条记录 共30条记录</span>
        <span style="margin-right:5px;"><img src="images/leg_34.gif" width="10" height="12" /></span>
        <span style="margin-right:5px;"><img src="images/leg_36.gif" width="6" height="11" /></span>
        <span style="margin-right:5px;"><img src="images/shx.gif" width="2" height="21" /></span>
        <span style="margin-right:5px;">第 <input name="" type="text" class="page_in"  value="1" /> 页 共5页</span>
        <span style="margin-right:5px;"><img src="images/shx.gif" width="2" height="21" /></span>
        <span style="margin-right:5px;"><img src="images/leg_39.gif" width="6" height="11" /></span>
        <span style="margin-right:5px;"><img src="images/leg_41.gif" width="10" height="12" /></span>
        <span style="margin-right:5px;"><img src="images/shx.gif" width="2" height="21" /></span>
        <span style="margin-right:5px;"><img src="images/leg_31.gif" width="15" height="14" /></span>
     </td>
    </tr>
</table>
</div>
      </div>
  
   
    	
        
    

</body>
</html>