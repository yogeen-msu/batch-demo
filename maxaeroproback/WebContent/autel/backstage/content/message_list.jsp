<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.messageman" /> > <fmt:message key="cententman.messageman.sitemessage" /></div>
<form method="post" action="message!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="cententman.messageman.sitemessage" /></h4>
			<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="cententman.messageman.title" />:</td>
						<td width="154">
							<input type="text" name="message.title" value="${message.title}" />
						</td>
						<td width="111" align="right"><fmt:message key="cententman.messageman.classify" />:</td>
						<td width="154">
							<select name="message.msgTypeCode" id="message_msgTypeCode">
								<option value=""><fmt:message key="cententman.messageman.allmsg" /></option>
								<c:forEach var="item" items="${messageTypeList}">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
						</td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.language" />:</td>
						<td width="154">
							<select name="message.languageCode" id="message_languageCode">
								<option value=""><fmt:message key="cententman.messageman.alllanguage" /></option>
								<c:forEach var="item" items="${languageList}">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="cententman.messageman.showobject" />:</td>
						<td width="154">
							<select name="message.userType" id="message_userType">
								<option value=""><fmt:message key="cententman.messageman.alluser" /></option>
								<option value="1"><fmt:message key="cententman.messageman.personaluser" /></option>
								<option value="2"><fmt:message key="memberman.sealer.jxs" /></option>
							</select>
						</td>
						<td width="111" align="right"><fmt:message key="product.software.version.publishstate" />:</td>
						<td width="154">
							<input type="radio" name="message.status" value="-1" <c:if test="${message.status == null || message.status == -1}">checked="checked"</c:if> ><fmt:message key="orderman.list.all" />
							&nbsp;&nbsp;
							<input type="radio" name="message.status" value="1" <c:if test="${message.status == 1}">checked="checked"</c:if> ><fmt:message key="product.software.version.publishstate.yes" />
							&nbsp;&nbsp;
							<input type="radio" name="message.status" value="0" <c:if test="${message.status == 0}">checked="checked"</c:if> ><fmt:message key="product.software.version.publishstate.no" />
						</td>
						<td width="111" align="right"><fmt:message key="cententman.messageman.ispop" />:</td>
						<td width="154">
							<input type="radio" name="message.isDialog" value="-1"  <c:if test="${message.isDialog == null || message.isDialog == -1}">checked="checked"</c:if>  ><fmt:message key="orderman.list.all" />
							&nbsp;&nbsp;
							<input type="radio" name="message.isDialog" value="1" <c:if test="${message.isDialog == 1}">checked="checked"</c:if> ><fmt:message key="common.yes" />
							&nbsp;&nbsp;
							<input type="radio" name="message.isDialog" value="0" <c:if test="${message.isDialog == 0}">checked="checked"</c:if> ><fmt:message key="common.no" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			
			<div class="active">
				<h4>
				<span>
					<input type="button" onclick="location.href='message!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" />
					<input type="button" id="publishBtn" value=<fmt:message key="cententman.messageman.publish" /> class="search_but" />
					<input type="button" id="cancelPublishBtn" value=<fmt:message key="cententman.messageman.cancelpublish" /> class="search_but" />
				</span><fmt:message key="cententman.messageman.sitemessage" /></h4>
			</div>
		</div>
		
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><input type="checkbox"  id="selectAllItemBox" /> </grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.title" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.classify" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.language" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.ispop" /></grid:cell>
				<grid:cell><fmt:message key="cententman.cms.sort" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.megshowobject" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.uploadtime" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.authorityman.state" /></grid:cell>
				<grid:cell width="200px"> <fmt:message key="common.list.operation" /> </grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell> <input type="checkbox" value="${item.id}" class="selectItemBox" /></grid:cell>
				<grid:cell>${item.title} </grid:cell>
				<grid:cell>
					<c:forEach var="mtl" items="${messageTypeList}">
						<c:if test="${item.msgTypeCode ==mtl.code }">${mtl.name}</c:if>
					</c:forEach>
				</grid:cell>
				<grid:cell>${item.languageName}</grid:cell>
				<grid:cell>
					<c:if test="${item.isDialog==1}"><fmt:message key="cententman.messageman.pop" /></c:if>
					<c:if test="${item.isDialog==0}"><fmt:message key="cententman.messageman.nopop" /></c:if>
				</grid:cell>
				<grid:cell>${item.sort}</grid:cell>
				<grid:cell>
					<c:if test="${item.userType ==1}"><fmt:message key="cententman.messageman.personaluser" /></c:if>
					<c:if test="${item.userType ==2}"><fmt:message key="memberman.sealer.jxs" /></c:if>
				</grid:cell>
				<grid:cell>${item.creatTime}</grid:cell>
				<grid:cell>
					<c:if test="${item.status==1}"><fmt:message key="product.software.version.publishstate.yes" /></c:if>
					<c:if test="${item.status==0}"><fmt:message key="product.software.version.publishstate.no" /></c:if>
				</grid:cell>
				<grid:cell>
					<a href="message!edit.do?id=${item.id}"><fmt:message key="common.list.mod" /></a>
					&nbsp;&nbsp;
					<a  href="message!delete.do?id=${item.id}" onclick="javascript:return confirm('<fmt:message key="cententman.messageman.removecheck" />')">	
						<fmt:message key="common.list.del" />
					 </a>
					 &nbsp;&nbsp;
					 <c:if test="${item.isDialog==1}">
						<a href="#" class="cancelShowDialog" itemDialogId="${item.id}" ><fmt:message key="cententman.messageman.cancelpop" /></a>
					</c:if>
					<c:if test="${item.isDialog==0}">
						<a  href="#" class="showDialog" itemDialogId="${item.id}" ><fmt:message key="cententman.messageman.pop" /></a>
					</c:if>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>

<script type="text/javascript">
	
	$(function(){
		//选中分类
		var val1 = "${message.msgTypeCode}";
		$("#message_msgTypeCode option[value='"+val1+"']").attr("selected","selected");
		
		//选中语言
		var val2 = "${message.languageCode}";
		$("#message_languageCode option[value='"+val2+"']").attr("selected","selected");
		
		var val3 = "${message.userType}";
		$("#message_userType option[value='"+val3+"']").attr("selected","selected");
		
		
		//全选按扭
		$("#selectAllItemBox").click(function(){
			if($(this).attr("checked")){
				$(".selectItemBox").attr("checked",'true');//全选
			}else{
				$(".selectItemBox").attr("checked","");//不全选
			}
		});
		
		//单个钩选
		$(".selectItemBox").click(function(){
			var isAllSelect = true;
			$(".selectItemBox").each(function(){
				if(!$(this).attr("checked")){
					isAllSelect = false;
					return;
				}
			});
			
			if(isAllSelect){
				$("#selectAllItemBox").attr("checked",'true');//全选
			}else{
				$("#selectAllItemBox").attr("checked","");//不全选
			}
		});
		
		//发布
		$("#publishBtn").click(function(){
			var idArray = [];
			$(".selectItemBox").each(function(){
				if($(this).attr("checked")){
					idArray.push($(this).val());
				}
			});
			if(idArray.length > 0){
				var msgIds = idArray.join(",");
				updateStatus(msgIds,1);
			}else{
				alert("请选择需要修改的数据!");
			}
		});
		
		//取消发布
		$("#cancelPublishBtn").click(function(){
			var idArray = [];
			$(".selectItemBox").each(function(){
				if($(this).attr("checked")){
					idArray.push($(this).val());
				}
			});
			if(idArray.length > 0){
				var msgIds = idArray.join(",");
				updateStatus(msgIds,0);
			}else{
				alert("请选择需要修改的数据!");
			}
		});
		
		//<fmt:message key="common.list.mod" /><fmt:message key="product.software.version.publishstate" />
		function updateStatus(ids,status){
			//$.Loading.show('正在保存，请稍侯...');
			$.Loading.show('<fmt:message key="syssetting.toolman.urimapping.saving" />');
			
			var options = {
				url :"message!editStatus.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				data:"msgIds="+ids+"&status="+status,
				success : function(result) {	
					$.Loading.hide();
				 	if(result.result==1){
				 		if(status == 0){ 		//取消发布
				 			alert('<fmt:message key="cms.message.publish.cancel" />');
				 		}else if(status == 1){	//发布
				 			alert('<fmt:message key="cms.message.publish.success" />');
				 		}else{
					 		alert('<fmt:message key="market.mainsale.message4" />');
				 		}
				 		location.reload();
				 	}else{
				 		alert(result.message);
				 	}
				},
				error : function(e) {
					$.Loading.hide();
					alert("error :(");
 				}
	 		};
			$.ajax(options);	
		};
		
		
		
		
		//设置弹出显示
		$(".showDialog").click(function(){
			var id = $(this).attr("itemDialogId");
			updateDialogStatus(id,1);
		});
		
		//取消弹出显示
		$(".cancelShowDialog").click(function(){
			var id = $(this).attr("itemDialogId");
			updateDialogStatus(id,0);
		});
		
		//修改消弹显示状态
		function updateDialogStatus(id,status){
			//$.Loading.show('正在保存，请稍侯...');
			$.Loading.show('<fmt:message key="syssetting.toolman.urimapping.saving" />');
			var options = {
				url :"message!editDialogStatus.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				data:"id="+id+"&status="+status,
				success : function(result) {	
					$.Loading.hide();
				 	if(result.result==1){
				 		alert('<fmt:message key="market.mainsale.message4" />');
				 		window.location.href="message!list.do";
				 	}else{
				 		alert(result.message);
				 	}
				},
				error : function(e) {
					$.Loading.hide();
					alert("error :(");
 				}
	 		};
			$.ajax(options);	
		};
		 
		
		
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").each(function(i,obj){
				$(obj).attr("selected","selected");
			});		
			//重置单选
			$(".sear_table").find("input[type='radio'][value='-1']").each(function(i,obj){
				$(obj).attr("checked","checked");
			});
		});
	});
</script>
