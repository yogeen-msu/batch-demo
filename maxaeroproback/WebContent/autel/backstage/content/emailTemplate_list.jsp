<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.messageman" /> > <fmt:message key="cententman.messageman.emailmodel" /></div>
<form method="post" action="email-template!list.do">
	<div class="grid">
		<div class="toolbar" >
			<div class="active">
				<h4><span><input type="button" onclick="location.href='email-template!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
			</div>
		</div>
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.title" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.modeltype" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.languageenv" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.isstart" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.starttime" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.title} </grid:cell>
				<grid:cell>
					<c:if test="${item.type == 1}"><fmt:message key="cententman.messageman.registertip" /></c:if>
					<c:if test="${item.type == 2}"><fmt:message key="cententman.messageman.findpwd" /></c:if>
					<c:if test="${item.type == 3}"><fmt:message key="cententman.messageman.ordersubmitsuccess" /></c:if>
					<c:if test="${item.type == 4}"><fmt:message key="cententman.messageman.paysuccess" /></c:if>
					<c:if test="${item.type == 5}"><fmt:message key="cententman.messageman.tippaymoney" /></c:if>
					<c:if test="${item.type == 6}"><fmt:message key="cententman.messageman.continuepay" /></c:if>
					<c:if test="${item.type == 7}"><fmt:message key="cententman.messageman.emailtemplate.addcustomer" /></c:if>
					<c:if test="${item.type == 8}"><fmt:message key="cententman.messageman.emailtemplate.customerreply" /></c:if>
					<c:if test="${item.type == 9}"><fmt:message key="cententman.messageman.complanittimeoutalertmodel" /></c:if>
					<c:if test="${item.type == 10}"><fmt:message key="cententman.messageman.complanitclosealertmodel" /></c:if>
					<c:if test="${item.type == 11}"><fmt:message key="cententman.messageman.findautelIdsecondemail" /></c:if>
					<c:if test="${item.type == 12}"><fmt:message key="cententman.messageman.activeautelIdsecondemail" /></c:if>
					<c:if test="${item.type == 13}"><fmt:message key="cententman.messageman.change.autel" /></c:if>
					<c:if test="${item.type == 14}"><fmt:message key="cententman.messageman.update.autel" /></c:if>
					<c:if test="${item.type == 15}"><fmt:message key="cententman.messageman.order.confirm" /></c:if>
					<c:if test="${item.type == 16}"><fmt:message key="cententman.messageman.order.result" /></c:if>
					<c:if test="${item.type == 17}"><fmt:message key="product.valid.email.mailtitle" /></c:if>
					<c:if test="${item.type == 18}"><fmt:message key="maxisys.to.pro.email.temp" /></c:if>
					<c:if test="${item.type == 19}"><fmt:message key="ofn.title" /></c:if>
                    <c:if test="${item.type == 20}"><fmt:message key="ofn.dispatch" /></c:if>
				</grid:cell>
				<grid:cell>${item.languageName}</grid:cell>
				<grid:cell>
					<c:if test="${item.status == 0}"><fmt:message key="cententman.messageman.nostart" /></c:if>
					<c:if test="${item.status == 1}"><font color="red"><fmt:message key="cententman.messageman.started" /></font></c:if>
				</grid:cell>
				<grid:cell>${item.useTime}</grid:cell>
				<grid:cell>
					<a href="email-template!edit.do?id=${item.id}"><fmt:message key="common.list.mod" /></a>
					&nbsp;
					<a  href="email-template!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="cententman.messageman.template.removecheck" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
					 &nbsp;
					 <c:if test="${item.status == 0}">
					 	<a href="email-template!editStatus.do?id=${item.id}&status=1" onclick="javascript:return confirm('<fmt:message key="cententman.messageman.template.sureuse" />')">
						 	<fmt:message key="syssetting.authorityman.statestart" />
						 </a>
					 </c:if>
					 <c:if test="${item.status == 1}">
					 	<a href="email-template!editStatus.do?id=${item.id}&status=0" onclick="javascript:return confirm('<fmt:message key="cententman.messageman.template.surestop" />')">
						 	<fmt:message key="cententman.messageman.stop" />
						 </a>
					 </c:if>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
