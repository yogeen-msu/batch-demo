<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" />   >  <fmt:message key="cententman.messageman" />   > <fmt:message key="cententman.messageman.emailmodel" />   >  <fmt:message key="cententman.messageman.addemailmodel" /></div>
<form action="email-template!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.messageman.addemailmodel" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.languageenv" />:</label></th>
			<td>
				<select name="emailTemplate.languageCode">
					<c:forEach var="item" items="${languageList}">
						<option value="${item.code}">${item.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.title" />:</label></th>
			<td>
				<input type="text" name="emailTemplate.title"  dataType="string" isrequired="true" maxlength="32"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.modeltype" />:</label></th>
			<td>
				<select name="emailTemplate.type">
					<option value="1"><fmt:message key="cententman.messageman.registertip" /></option>
					<option value="2"><fmt:message key="cententman.messageman.findpwd" /></option>
					<option value="3"><fmt:message key="cententman.messageman.ordersubmitsuccess" /></option>
					<option value="4"><fmt:message key="cententman.messageman.paysuccess" /></option>
					<option value="5"><fmt:message key="cententman.messageman.tippaymoney" /></option>
					<option value="6"><fmt:message key="cententman.messageman.continuepay" /></option>
					<option value="7"><fmt:message key="cententman.messageman.emailtemplate.addcustomer" /></option>
					<option value="8"><fmt:message key="cententman.messageman.emailtemplate.customerreply" /></option>
					<option value="9"><fmt:message key="cententman.messageman.complanittimeoutalertmodel" /></option>
					<option value="10"><fmt:message key="cententman.messageman.complanitclosealertmodel" /></option>
					<option value="11"><fmt:message key="cententman.messageman.findautelIdsecondemail" /></option>
					<option value="12"><fmt:message key="cententman.messageman.activeautelIdsecondemail" /></option>
					<option value="13"><fmt:message key="cententman.messageman.change.autel" /></option>
					<option value="14"><fmt:message key="cententman.messageman.update.autel" /></option>
					<option value="15"><fmt:message key="cententman.messageman.order.confirm" /></option>
					<option value="16"><fmt:message key="cententman.messageman.order.result" /></option>
					<option value="17"><fmt:message key="product.valid.email.mailtitle" /></option>
					<option value="18"><fmt:message key="maxisys.to.pro.email.temp" /></option>
					<option value="19"><fmt:message key="ofn.title" /></option>
                    <option value="20"><fmt:message key="ofn.dispatch" /></option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.content" /></label></th>
			<td>
				<fmt:message key="cententman.messageman.usefreemarker" /><br/><br/>
				<textarea id="email_content" name="emailTemplate.content" style="width:250px;height:100px"  ></textarea>
			</td>
		</tr>
	</table>

</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='email-template!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
	$(function(){
		$("#email_content").ckeditor();
	});
</script>