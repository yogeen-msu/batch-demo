<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.messageman" /> > <fmt:message key="cententman.messageman.cellphonesmsmodel" /></div>
<form method="post" action="phone-message-template!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="cententman.messageman.cellphonesmsmodel" /></h4>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='phone-message-template!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
			</div>
		</div>
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.title" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.modeltype" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.languageenv" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.isstart" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.starttime" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.title} </grid:cell>
				<grid:cell>
					<c:if test="${item.type == 1}"><fmt:message key="cententman.messageman.registertip" /></c:if>
					<c:if test="${item.type == 2}"><fmt:message key="cententman.messageman.findpwd" /></c:if>
					<c:if test="${item.type == 3}"><fmt:message key="cententman.messageman.ordersubmitsuccess" /></c:if>
					<c:if test="${item.type == 4}"><fmt:message key="cententman.messageman.paysuccess" /></c:if>
					<c:if test="${item.type == 5}"><fmt:message key="cententman.messageman.tippaymoney" /></c:if>
					<c:if test="${item.type == 6}"><fmt:message key="cententman.messageman.continuepay" /></c:if>
				</grid:cell>
				<grid:cell>${item.languageName}</grid:cell>
				<grid:cell>
					<c:if test="${item.status == 0}"><fmt:message key="cententman.messageman.nostart" /></c:if>
					<c:if test="${item.status == 1}"><font color="red"><fmt:message key="cententman.messageman.started" /></font></c:if>
				</grid:cell>
				<grid:cell>${item.useTime}</grid:cell>
				<grid:cell>
					<a href="phone-message-template!edit.do?id=${item.id}"><fmt:message key="common.list.mod" /></a>
					&nbsp;
					<a  href="phone-message-template!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="cententman.messageman.template.removecheck" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
					 &nbsp;
					 <c:if test="${item.status == 0}">
					 	<a href="phone-message-template!editStatus.do?id=${item.id}&status=1" onclick="javascript:return confirm('<fmt:message key="cententman.messageman.template.sureuse" />')">
						 	<fmt:message key="syssetting.authorityman.statestart" />
						 </a>
					 </c:if>
					 <c:if test="${item.status == 1}">
					 	<a href="phone-message-template!editStatus.do?id=${item.id}&status=0" onclick="javascript:return confirm('<fmt:message key="cententman.messageman.template.surestop" />')">
						 	<fmt:message key="cententman.messageman.stop" />
						 </a>
					 </c:if>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
