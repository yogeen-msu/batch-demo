<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
      <tbody>     
          <c:forEach var="item" items="${statisticList}">
          	<tr>
	            <th><span>${item.name}:</span></th>
	            <td><span>${item.count} 条</span></td>
	          </tr>
          </c:forEach>
          <tr>
            <th style="height:21px"><span> </span></th>
            <td><span></span></td>
          </tr>          
        </tbody>            
 </table>