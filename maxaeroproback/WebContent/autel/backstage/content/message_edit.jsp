<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" />   >  <fmt:message key="cententman.messageman" />   > <fmt:message key="cententman.messageman.sitemessage" />   >  <fmt:message key="cententman.messageman.editsitemeg" /></div>
<form action="message!editSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.messageman.editsitemeg" /> </h4>

	<input type="hidden" name="message.id" value="${message.id}" />
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.languageenv" />:</label></th>
			<td>
				<select name="message.languageCode" id="message_languageCode">
					<c:forEach var="item" items="${languageList}">
						<option value="${item.code}">${item.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.belongclassify" />:</label></th>
			<td>
				<select name="message.msgTypeCode" id="message_msgTypeCode" >
					<c:forEach var="item" items="${messageTypeList}">
						<option value="${item.code}">${item.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.title" />:</label></th>
			<td>
				<input type="text" name="message.title" value="${message.title}"  dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.cms.sort" />:</label></th>
			<td>
				<input type="text" name="message.sort" value="${message.sort}"  dataType="int" isrequired="true" maxlength="7"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.megshowobject" />:</label></th>
			<td> 
				
				<input type="radio" name="message.userType" value="1" <c:if test="${message.userType ==1}">checked="checked"</c:if> ><fmt:message key="cententman.messageman.personaluser" />
				&nbsp;&nbsp;
				<input type="radio" name="message.userType" value="2" <c:if test="${message.userType ==2}">checked="checked"</c:if> ><fmt:message key="memberman.sealer.jxs" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.pulisher" />:</label></th>
			<td>
				<input type="text" name="message.creator" value="${message.creator}" dataType="string" isrequired="true" maxlength="32"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.ispublish" />:</label></th>
			<td>
				<input type="radio" name="message.status" value="1" <c:if test="${message.status==1}">checked="checked"</c:if> ><fmt:message key="common.yes" />
				&nbsp;&nbsp;
				<input type="radio" name="message.status" value="0" <c:if test="${message.status==0}">checked="checked"</c:if> ><fmt:message key="common.no" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.ispop" />:</label></th>
			<td>
				<input type="radio" name="message.isDialog" value="1" <c:if test="${message.isDialog==1}">checked="checked"</c:if>  ><fmt:message key="common.yes" />
				&nbsp;&nbsp;
				<input type="radio" name="message.isDialog" value="0" <c:if test="${message.isDialog==0}">checked="checked"</c:if>  ><fmt:message key="common.no" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.content" /></label></th>
			<td>
				<textarea id="message_content" name="message.content" style="width:250px;height:100px"  >${message.content}</textarea>
			</td>
		</tr>
	</table>

</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='message!list.do'"	 value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
	$(function(){
		//选中语言
		var lanCode = "${message.languageCode}";
		$("#message_languageCode option[value='"+lanCode+"']").attr("selected","selected");
		
		//选中消息分类
		var msgType = "${message.msgTypeCode}";
		$("#message_msgTypeCode option[value='"+msgType+"']").attr("selected","selected");
		
		$("#message_content").ckeditor();
	});
</script>