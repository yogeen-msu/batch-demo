<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.messageman" /> > <fmt:message key="cententman.messageman.classify" /></div>
<form method="post" action="message-type!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="cententman.messageman.classify" /></h4>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='message-type!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
			</div>
		</div>
		
		<grid:grid from="messageTypeList">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="200px"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="cententman.messageman.classify" /></grid:cell>
				<grid:cell width="150px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.code} </grid:cell>
				<grid:cell>${item.name}</grid:cell>
				<grid:cell>
					<a href="message-type!edit.do?id=${item.id}">
						<fmt:message key="common.list.mod" />
					</a>
					&nbsp;&nbsp;
					<a  href="message-type!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="cententman.messageman.classify.removecheck" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
