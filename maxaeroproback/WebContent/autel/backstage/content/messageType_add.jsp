<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" />   >  <fmt:message key="cententman.messageman" />   > <fmt:message key="cententman.messageman.classify" />   >  <fmt:message key="cententman.messageman.addclassify" /></div>
<form action="message-type!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.messageman.addclassify" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.classifyname" />:</label></th>
			<td>
				<input type="text" name="messageType.name"  dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
	</table>

</div>
<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='message-type!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
</script>