<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" />  >  <fmt:message key="cententman.messageman" />   > <fmt:message key="cententman.messageman.cellphonesmsmodel" />   >  <fmt:message key="cententman.messageman.addsmsmodel" /></div>
<form action="phone-message-template!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.messageman.addsmsmodel" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.languageenv" />:</label></th>
			<td>
				<select name="phoneMessageTemplate.languageCode">
					<c:forEach var="item" items="${languageList}">
						<option value="${item.code}">${item.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.title" />:</label></th>
			<td>
				<input type="text" name="phoneMessageTemplate.title"  dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.modeltype" />:</label></th>
			<td>
				<select name="phoneMessageTemplate.type">
					<option value="1"><fmt:message key="cententman.messageman.registertip" /></option>
					<option value="2"><fmt:message key="cententman.messageman.findpwd" /></option>
					<option value="3"><fmt:message key="cententman.messageman.ordersubmitsuccess" /></option>
					<option value="4"><fmt:message key="cententman.messageman.paysuccess" /></option>
					<option value="5"><fmt:message key="cententman.messageman.tippaymoney" /></option>
					<option value="6"><fmt:message key="cententman.messageman.continuepay" /></option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.messageman.content" />:</label></th>
			<td>
				<textarea id="email_content" name="phoneMessageTemplate.content" style="width:400px;height:150px"  ></textarea>
			</td>
		</tr>
	</table>

</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='phone-message-template!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
</script>