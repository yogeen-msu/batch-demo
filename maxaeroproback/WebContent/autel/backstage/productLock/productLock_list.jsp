<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderman.paystatus.log" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript">
	$(function() {
		$("#reset_but").click(
				function() {
					//重置文本框
					$(".sear_table").find("input[type='text']").each(
							function(i, obj) {
								$(obj).val("");
							});

					//重置下拉框
					$(".sear_table").find("select option[value='']").attr(
							"selected", "selected");
				});
	});
	function jump(url) {
		var tmpForm = $("<form  method='post'></form>");
		$(tmpForm).attr("action", url);
		tmpForm.appendTo(document.body).submit();
	}
</script>


</head>

<body>
	<form action="product-lock!list.do" method="post" id="f1" name="f1">


		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			<fmt:message key="menu.system.setting" />
			>
			<fmt:message key="syssetting.outfactoryproman" />
			> 未注册产品锁定列表
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;未注册产品锁定
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
						    <td></td>
							<td align="right"><fmt:message
									key="memberman.complaintman.productserial" />:</td>
							<td width="180px"><input name="productInfo.serialNo"
								value="${productInfo.serialNo}" type="text" class="sear_item" /></td>
							<td><input type="submit"
								value="<fmt:message key="common.btn.search" />"
								class="search_but" />&nbsp;<input type="button"
								value="<fmt:message key="common.btn.reset" />"
								class="search_but" id="reset_but" /></td>
							<td></td>
						</tr>
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4>
						<span> <input name="" type="button"
							onclick="jump('product-lock!add.do')"
							value="<fmt:message key="common.btn.add" />"
							class="search_but addorderpaystatuslog" />
						</span>未注册产品锁定列表:
					</h4>
				</div>
			</div>
			<div class="grid">
				<grid:grid from="webpage">
					<grid:header>
						<grid:cell width="15%">
							<fmt:message key="memberman.complaintman.productserial" />
						</grid:cell>
						<grid:cell width="10%">autelID</grid:cell>
						<grid:cell width="30%">操作原因</grid:cell>
						<grid:cell width="10%"><fmt:message key="product.reset.operate.time" /></grid:cell>
						<grid:cell width="10%"><fmt:message key="product.reset.operater" /></grid:cell>
						<grid:cell width="10%">操作</grid:cell>
						
						<%-- <grid:cell width="20%">
							<fmt:message key="common.list.operation" />
						</grid:cell> --%>
					</grid:header>
					<grid:body item="item">
						<grid:cell>${item.serialNo}</grid:cell>
						<grid:cell>${item.customerCode}</grid:cell>
						<grid:cell>${item.noRegReason}</grid:cell>
						<grid:cell>${fn:substring(item.noRegDate,0,10)}</grid:cell>
						<grid:cell>${item.createUser}</grid:cell>
						 <grid:cell>
						 <c:if test="${ ! empty item.customerCode}" >
							<a href="product-lock!rebanding.do?id=${item.id}"> 重新绑定
							</a>
							</c:if>
						</grid:cell> 
					</grid:body>
				</grid:grid>
			</div>
		</div>
	</form>
</body>
</html>


