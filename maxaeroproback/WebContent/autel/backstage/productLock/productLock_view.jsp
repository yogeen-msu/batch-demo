<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet"
	type="text/css" />
<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > 
	<fmt:message key="syssetting.outfactoryproman" /> > <fmt:message key="product.reset.view" />
</div>
<form action="product-repair-record!editSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="product.reset.view" /></h4>

	<table class="form-table">
		<tr style="height:40px">
			<th><label class="text"><fmt:message key="memberman.complaintman.productserial" />:</label></th>
			<td>
				${productInfo.serialNo}
			</td>
		</tr>
		<tr style="height:40px">
			<th><label class="text"><fmt:message key="product.reset.operater" />:</label></th>
			<td>
				${log.resetUser}
			</td>
		</tr>
		<tr style="height:40px">
			<th><label class="text"><fmt:message key="product.reset.operate.time" />:</label></th>
			<td>
				${log.resetDate}
			</td>
		</tr>
		<tr style="height:40px">
			<th><label class="text"><fmt:message key="product.reset.operate.oldUserCode" />:</label></th>
			<td style="table-layout:word-wrap:break-word;word-break:break-all">
				${log.oldUserCode}
			</td>
		</tr>
		<tr style="height:40px">
			<th><label class="text"><fmt:message key="product.reset.operate.validDate" />:</label></th>
			<td style="table-layout:word-wrap:break-word;word-break:break-all">
				${log.validDate}
			</td>
		</tr>
		<tr style="height:40px">
			<th><label class="text"><fmt:message key="product.reset.operate.reason" />:</label></th>
			<td style="table-layout:word-wrap:break-word;word-break:break-all">
				${log.resetReason}
			</td>
		</tr>
	</table>
</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="reset" type="button" onclick="history.go(-1)" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>
