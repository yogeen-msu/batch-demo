<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  
                     > 出厂产品管理
                     > 产品软件信息查询</div>

<div class="grid">
<form method="post" action="product-soft-search!getUpgradeSoftList.do">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div>产品软件信息查询</h4>
				<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="8">
						<td width="111" align="right"> 序列号:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="softList.productSN" value="${softList.productSN}" />
						</td>
						<td width="111" align="right"> 软件名称:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="softList.softName" value="${softList.softName}" />
						</td>
						
						<td width="154">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			
		</div>
</form>		
		<grid:grid from="record">
			<grid:header>
				<grid:cell width="140px">软件名称</grid:cell>
				<grid:cell width="140px">机器版本</grid:cell>
				<grid:cell width="140px">系统版本</grid:cell>
				<grid:cell width="140px">更新日期</grid:cell>
				<grid:cell width="140px">最后操作</grid:cell>
			</grid:header>
		 
			<grid:body item="item">
				<grid:cell>
					${item.softName}
				</grid:cell>
				<grid:cell>
					${item.softVersion}
				</grid:cell>
				<grid:cell>
					${item.softSysVersion }
				</grid:cell>
				<grid:cell>
				    ${fn:substring(item.syncTime,0,10) }
				</grid:cell>
				
				<grid:cell>
				    ${item.lastOpCode}
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>

<script type="text/javascript">
  $(function(){
	  $("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").each(function(i,obj){
				$(obj).attr("selected","selected");
			});		
			//重置单选
			$(".sear_table").find("input[type='radio'][value='-1']").each(function(i,obj){
				$(obj).attr("checked","checked");
			});
		});
  });
</script>