<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="syssetting.otherconfig" />  > <fmt:message key="syssetting.otherconfig.telphoneconfig" />   >  <fmt:message key="syssetting.otherconfig.addsmsconfig" /></div>
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.otherconfig.addsmsconfig" /> </h4>
	
<form action="phone-message-cfg!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="common.name" />:</label></th>
			<td>
				<input type="text" name="phoneMessageCfg.name" dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.authorityman.state" />:</label></th>
			<td>
				<input type="radio" name="phoneMessageCfg.status" value="0" checked="checked" /> <fmt:message key="cententman.messageman.nostart" />
				&nbsp;&nbsp;&nbsp;
				<input type="radio" name="phoneMessageCfg.status" value="1" /> <fmt:message key="syssetting.authorityman.statestart" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.smsconfigcontent" /></label></th>
			<td>
				<input type="text" name="phoneMessageCfg.config"  dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.smsconfigdesc" /></label></th>
			<td>
				<textarea name="phoneMessageCfg.biref" style="width:350px;height:150px" ></textarea>
			</td>
		</tr>
	</table>
</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='phone-message-cfg!list.do'"	 value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
	$(function(){
		//选中语言
		var lanCode = "${phoneMessageCfg.languageCode}";
		$("#phoneMessageCfg_languageCode option[value='"+lanCode+"']").attr("selected","selected");
		
		//选中<fmt:message key="cententman.messageman.modeltype" />
		var messageType = "${phoneMessageCfg.type}";
		$("#phoneMessageCfg_type option[value='"+messageType+"']").attr("selected","selected");
		
	});
</script> 