<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> &gt;
	<fmt:message key="syssetting.authorityman" /> &gt; <fmt:message key="syssetting.authorityman.addmanager" />
	</div>
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.authorityman.addmanager" /> </h4>
<form action="userAdmin!addSave.do"  class="validate" method="post" name="theForm" id="theForm" >
<table  class="form-table">

<c:if test="${multiSite==1}">
	<tr>
		<th><label class="text"><fmt:message key="syssetting.authorityman.siteid" />:</label></th>
		<td >
		<select name="adminUser.siteid" id="adminUserSite"/>
		</td>
	</tr>
<script>
$(function(){

	$.ajax({
		 type: "GET",
		 url: "../multiSite!listJson.do",
		 data:   "ajax=yes",
		 dataType:'json',
		 success: function(result){
			 if(result.result==0){
				 $("#adminUserSite").selectTree(result.data);
			 }else{
				 alert("<fmt:message key="syssetting.authorityman.result" />");
			 }
	     },
	     error:function(){
			alert("<fmt:message key="syssetting.authorityman.result" />");
		 }
	}); 
});
</script>
</c:if>

	<tr>
		<th><label class="text"><fmt:message key="syssetting.authorityman.username" />:</label></th>
		<td><input type="text" name="adminUser.username"  dataType="string" isrequired="true" fun='usernameExist'/></td>
	</tr>

	<tr  >
		<th><label class="text"><fmt:message key="memberman.sealerinfoman.passwd" />:</label></th>
		<td><input type="password" name="adminUser.password" id="pwd" dataType="string" isrequired="true" /></td>
	</tr>
	<tr >
		<th><label class="text"><fmt:message key="memberman.sealerinfoman.repasswd" />:</label></th>
		<td><input type="password" id="repwd"  fun='checkpwd' /></td>
	</tr>
	<tr>
		<th><label class="text"><fmt:message key="syssetting.authorityman.managername" />:</label></th>
		<td><input type="text" name="adminUser.realname" dataType="string" isrequired="true"/></td>
	</tr>
	<tr>
		<th><label class="text"><fmt:message key="syssetting.authorityman.number" />:</label></th>
		<td><input type="text" name="adminUser.userno"   /></td>
	</tr>
	<tr>
		<th><label class="text"><fmt:message key="syssetting.authorityman.managerdepartment" />:</label></th>
		<td><input type="text" name="adminUser.userdept"   /></td>
	</tr>
	<tr>
		<th><label class="text"><fmt:message key="syssetting.authorityman.managertype" />:</label></th>
		<td>
			<input type="radio"  name="adminUser.founder" value="0" id="notSuperChk" checked="true"><fmt:message key="syssetting.authorityman.commonmanager" />&nbsp;&nbsp;
			<input type="radio"  name="adminUser.founder" value="1" id="superChk"><fmt:message key="syssetting.authorityman.supermanager" />
		</td>
	</tr>
	<tr id="roletr">
		<th><label class="text"><fmt:message key="syssetting.authorityman.role" />:</label></th>
		<td>
			<ul style="width:100%" id="rolesbox">
				<c:forEach var="role" items="${roleList }">
				<li style="width:33%;display:block"><input autocomplete="off" type="checkbox" name="roleids" value="${role.roleid }"  />
					${role.rolename }&nbsp;</li>
				</c:forEach>
			</ul>
		</td>		
	</tr>
	
	<c:forEach items="${htmlList }" var="html">${html }</c:forEach>
	
	<tr>
		<th><label class="text"><fmt:message key="syssetting.authorityman.state" />:</label></th>
		<td>
			<input type="radio"  name="adminUser.state" value="1" checked=true><fmt:message key="syssetting.authorityman.statestart" />&nbsp;&nbsp;
			<input type="radio"  name="adminUser.state" value="0"><fmt:message key="syssetting.authorityman.statestop" /> 
		</td>
	</tr>
	<tr>
		<th><label class="text"><fmt:message key="syssetting.authorityman.remark" />:</label></th>
		<td><input type="text" name="adminUser.remark"   /></td>
	</tr>

</table>

<div class="submitlist" align="center">
 <table>
 <tr><td >
  <input name="submit" type="submit"	  value="<fmt:message key="common.btn.ok" />" class="submitBtn" />
   </td>
   </tr>
 </table>
</div>

<script type="text/javascript">

//验证用户名是否可用
function usernameExist()
{
	var userid=0;
	var username=$("input[name='adminUser.username']").eq(0).val();
	var result = -1;
	$.ajax({
		url:'userAdmin!usernameExist.do?ajax=yes',
		type:"post",
		data:{"adminUser.userid":userid,"adminUser.username":username},
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
		return true;
	else
		return "<fmt:message key="common.js.existed" />";
}

function checkpwd(){
	 
	if(  $("#repwd").val() !=   $("#pwd").val() ){
		return "<fmt:message key="memberman.sealerinfoman.message1" />";
	}
	return true;
}

$(function(){
	$("form.validate").validate();
	$("#notSuperChk").click(function(){
		if(this.checked)
		$("#roletr").show();
	});
	$("#superChk").click(function(){
		if(this.checked)
		$("#roletr").hide();
	});	
	$("#updatePwd").click(function(){
		if(this.checked){
			$("#pwdtr").show();
			$("#repwdtr").show();			
		}else{
			$("#pwdtr").hide();
			$("#repwdtr").hide();
		}
	});
	
});
</script>

</form>
</div>