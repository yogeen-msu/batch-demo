<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../autel/backstage/system/js/productInfo.js.jsp?ajax=yes"></script>
<form method="post" id="pageSelectForm" >
	<div class="grid">
		<div class="toolbar">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="product.producttypevo" />:
			<select name="productInfo.proTypeCode" id="proTypeCode_select">
				<option value=""><fmt:message key="common.sel.choose" /></option>
				<c:forEach var="item" items="${productTypeList}">
					<option value="${item.code}">${item.name}</option>
				</c:forEach>
			</select>
			<script type="text/javascript">
				var proValue = "${productInfo.proTypeCode}";
				$("#proTypeCode_select option[value='"+proValue+"']").attr("selected","selected");
			</script>
			&nbsp;&nbsp;
			<fmt:message key="memberman.complaintman.productserial" />:<input type="text" name="productInfo.serialNo" value="${productInfo.serialNo}" />&nbsp;&nbsp;
			<input type="button"  name="submit" onclick="pageQuery()" value=<fmt:message key="common.btn.search" /> />
			<div style="clear: both"></div>
		</div>
		<grid:grid from="webpage" ajax="yes">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.salecontractcode" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.date" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.registstate" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1} </grid:cell>
				<grid:cell>${fn:substring(item.proTypeName,0,10)}</grid:cell>
				<grid:cell>${fn:substring(item.serialNo,0,18)}</grid:cell>
				<grid:cell>${item.saleContractName}</grid:cell>
				<grid:cell>${item.proDate}</grid:cell>
				<grid:cell>
					<c:if test="${item.regStatus == 0}"><fmt:message key="syssetting.outfactoryproman.noregist" /></c:if>
					<c:if test="${item.regStatus == 1}"><fmt:message key="syssetting.outfactoryproman.registed" /></c:if>
				</grid:cell>
				<grid:cell>
					<input type="button" class="selectProductItem" value=<fmt:message key="common.openselect.choose" /> selectCode="${item.code}" selectProTypeName="${item.proTypeName}" 
								selectSerialNo="${item.serialNo}" selectProDate="${item.proDate}"  />
				</grid:cell>
			</grid:body>
		</grid:grid>
	</div>
</form>

