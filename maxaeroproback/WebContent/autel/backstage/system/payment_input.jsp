<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.payset" />  > <fmt:message key="syssetting.payset.payconfig" />   >  <fmt:message key="syssetting.payset.editpayconfig" /></div>

<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.payset.editpayconfig" /> </h4>
	
	<form method="post" action="payment-cfg!save.do" class="validate" name="theForm" id="theForm"  >
		<input type="hidden" name="paymentId" value="${paymentId }" />
		<table cellspacing="1" cellpadding="3" width="100%" class="form-table" id="pluginTable">
			<tbody>
				<tr id="first">
					<th><fmt:message key="syssetting.payset.payconfig.item" />:</th>
					<td>
						<select name="type" id="pluginList"  isrequired="true"  >
							<option value=""><fmt:message key="syssetting.payset.checkconfig" /></option>
							<c:forEach items="${pluginList}" var="plugin">
								<option value="${plugin.id }" <c:if test="${ type==plugin.id }">selected="selected"</c:if> >${plugin.name }</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr id="name">
					<th><fmt:message key="syssetting.payset.payconfig.item.name" />:</th>
					<td>
						<input type="text" name="name"  id="payment_name" value="${name }" dataType="string" isrequired="true"  maxlength="100" />
					</td>
				</tr>
				<tr id="last">
					<th><fmt:message key="syssetting.payset.alipaydesc" />:</th>
					<td>
						<textarea name="biref" id="biref">${biref}</textarea>
					</td>
				</tr>
			</tbody>
		</table>
</div>
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
					<input name="submit" type="submit" value=<fmt:message key="common.btn.ok" /> class="submitBtn" />
					<input name="submit" type="button" onclick="location.href='payment-cfg!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	</form>
<script>
function cleanTr(){
	$("#pluginTable>tbody>tr").each(function(){
		if($(this).attr("id")!="first" &&$(this).attr("id")!="last"   &&$(this).attr("id")!="name"  ){
				$(this).remove();
		}
	});
}

function loadHtml(paymentid,pluginid,name){
	 
 	if(pluginid && pluginid!=''){
 		
	$.ajax({
		url:"payment-cfg!getPluginHtml.do?ajax=yes&pluginId="+pluginid+"&paymentId="+paymentid,
		success:function(html){
			cleanTr(); 
			$("#pluginTable tr[id=name]").after(html);
			$("#payment_name").val(name);
			
		},
		error:function(){
			alert("error");
		} 
	});
 	}	
}
$(function(){
	$("form.validate").validate();
	
	$('#biref').ckeditor();
	$("#pluginList").change(function(){
		var pluginid = $(this).val();
		var name = $(this).find("option[selected]").text();
		loadHtml("",pluginid,name);
	});
	loadHtml('${paymentId}','${type}','${name}');

});

</script>