<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="syssetting.areaconfig" />  > <fmt:message key="syssetting.languageconfig.baselanguage" />   >  <fmt:message key="syssetting.languageconfig.editbaselanguage" /></div>

<form action="language!editSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.languageconfig.editbaselanguage" /> </h4>

	<input type="hidden" name="language.id" value="${language.id}" />
	<input type="hidden" name="language.code" value="${language.code}" />
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="common.list.code" />:</label></th>
			<td>${language.code}</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.languagename" />:</label></th>
			<td><input type="text" name="language.name" value="${language.name}"  dataType="string" isrequired="true"  maxlength="40"  /></td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.countrycode" />:</label></th>
			<td>
				<input type="text" name="language.countryCode" value="${language.countryCode}"  dataType="string" isrequired="true"  maxlength="10"  />
				<fmt:message key="syssetting.languageconfig.areacodedesc" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.languagecode" />:</label></th>
			<td>
				<input type="text" name="language.languageCode" value="${language.languageCode}"  dataType="string" isrequired="true"  maxlength="10"  />
				<fmt:message key="syssetting.languageconfig.languagecodedesc" />
			</td>
		</tr>
	</table>
</div>

<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='language!list.do'"	 value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
</script>