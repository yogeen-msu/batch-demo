<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../autel/backstage/system/js/productInfo.js.jsp?ajax=yes"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   >  <fmt:message key="syssetting.outfactoryproman" />  > <fmt:message key="syssetting.outfactoryproman.info" />   >  <fmt:message key="syssetting.outfactoryproman.edit" /></div>
<form action="product-info!editForChina.do?operType=2"  class="validate" method="post" name="theForm" id="theForm">

<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.outfactoryproman.edit" /> </h4>
	<input type="hidden" name="productInfo.id" value="${productInfo.id}" />
	<input type="hidden" name="productInfo.code" value="${productInfo.code}" />
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="product.producttypevo" />:</label></th>
			<td>
				<input type="hidden" name="productInfo.proTypeCode" id="productInfo_proTypeCode" value="${productInfo.proTypeCode}"  />
				<input type="text" id="productInfo_proTypName" isrequired="true" readonly="readonly" value="${productInfo.proTypeName}" />
				
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="memberman.complaintman.productserial" />:</label></th>
			<td>
			${productInfo.serialNo}
<!--				<input type="text" name="productInfo.serialNo" value="${productInfo.serialNo}"  readonly="readonly"/>-->
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="marketman.salecontractman.sale.contract" />:</label></th>
			<td>
				<input type="hidden" name="productInfo.saleContractCode" id="productInfo_saleContractCode" value="${productInfo.saleContractCode}" />
				<input type="text" id="productInfo_saleContractName" value="${productInfo.saleContractName}" isrequired="true" readonly="readonly"/>
				
			</td>
		</tr>
		<tr>
			<th style="width: 140px"><label class="text"><fmt:message key="product.productinfo.regpwd" />:</label></th>
			<td>
				<input type="text" name="productInfo.regPwd" value="${productInfo.regPwd}"  readonly   dataType="string" isrequired="true" maxlength="12" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.date" />:</label></th>
			<td>
		
			<input type="text" name="productInfo.proDate" value="${productInfo.proDate}" readonly="readonly"  maxlength="23"  dataType="date" isrequired="true" />
			
			
			</td>
		</tr>
	
		<tr>
			<th><label class="text"><fmt:message key="product.info.externInfo1" />:</label></th>
			<td>
				<input type="text" name="productInfo.externInfo1" value="${productInfo.externInfo1}"   maxlength="40" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.info.externInfo2" />:</label></th>
			<td>
				<input type="text" name="productInfo.externInfo2" value="${productInfo.externInfo2}"   maxlength="40" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.info.externInfo3" />:</label></th>
			<td>
				<input type="text" name="productInfo.externInfo3"  value="${productInfo.externInfo3}"  maxlength="40"  />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.info.externInfo4" />:</label></th>
			<td>
				<input type="text" name="productInfo.externInfo4" value="${productInfo.externInfo4}"   maxlength="40" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.info.externInfo5" />:</label></th>
			<td>
				<input type="text" name="productInfo.externInfo5" value="${productInfo.externInfo5}"   maxlength="40"  />
			</td>
		</tr>
		
	</table>

</div>

<div class="submitlist" align="center">
	<table>
		<tr><td >
			<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
			<input name="reset" type="button" onclick="location.href='product-info!listForChinaSealer.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
		</td></tr>
	</table>
</div>
</form>
<div id="selectProductTypeDlg"></div>
<div id="selectSaleContractDlg"></div>

<script type="text/javascript">
	$("form.validate").validate();
	$(function(){
		ProductInfo.init();
	});
	
	
	function initProductInfo(){
		$(".selectPdTypeItem").click(function(){
			var code = $(this).attr("selectCode");		//<fmt:message key="product.producttypevo" />编号
			var name = $(this).attr("selectName");		//<fmt:message key="product.producttypevo" />名称
			$("#productInfo_proTypeCode").val(code);
			$("#productInfo_proTypName").val(name);
			Cop.Dialog.close("selectProductTypeDlg");
		});
		
		
		$(".selectSaleContractItem").click(function(){
			var code = $(this).attr("selectCode");		//<fmt:message key="product.producttypevo" />编号
			var name = $(this).attr("selectName");		//<fmt:message key="product.producttypevo" />名称

			$("#productInfo_saleContractCode").val(code);
			$("#productInfo_saleContractName").val(name);
			Cop.Dialog.close("selectSaleContractDlg");
		});
	}
	setInterval('initProductInfo()',100);
</script>