<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<link href="../../autel/main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../autel/backstage/system/js/selfdialog.js"></script>
<style>
.form-table th {
    text-align: center;
}
.input table th {
    padding-left:0px;
    padding-right:0px;
}
</style>
<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:
	<fmt:message key="menu.system.setting" />   > 
	<fmt:message key="syssetting.toolman" />  > 
	<fmt:message key="syssetting.sealerman" />  > 
	<fmt:message key="syssetting.sealerman.add" /> 
</div>

<form action="sealer-download!save.do"  class="validate" method="post" name="theForm" id="theForm" enctype="multipart/form-data">
	<div class="input" style="width: 98%">
		<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.sealerman.add" /> </h4>
		<table class="form-table">
			<tr>
				<th><label class="text"><fmt:message key="syssetting.sealerman.type" />:</label></th>
				<td><input type="text" name="sealerDownload.type"  dataType="string" isrequired="true"/></td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="syssetting.sealerman.name" />:</label></th>
				<td><input type="text" name="sealerDownload.name"  dataType="string" isrequired="true"/></td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="syssetting.toolman.downloadlocation" />:</label></th>
				<td>
					<input type="text" name="sealerDownload.fileUrl"  dataType="string" isrequired="true" />
				</td>
			</tr>
		</table>
	</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit" value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='sealer-download!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>
<script type="text/javascript">
	$("form.validate").validate();
</script>