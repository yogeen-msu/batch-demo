<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.areaconfig" /> > <fmt:message key="syssetting.areaconfig.basearea" /></div>
<form method="post" action="area-info!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.areaconfig.basearea" /></h4>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='area-info!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
			</div>
		</div>
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="200px"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.areaconfig.areaname" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.areaconfig.continent" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.code} </grid:cell>
				<grid:cell>${item.name}</grid:cell>
				<grid:cell>
					<c:choose>
						<c:when test="${item.continent == 1}"><fmt:message key="syssetting.areaconfig.asia" /></c:when>
						<c:when test="${item.continent == 2}"><fmt:message key="syssetting.areaconfig.europe" /></c:when>
						<c:when test="${item.continent == 3}"><fmt:message key="syssetting.areaconfig.southamerica" /></c:when>
						<c:when test="${item.continent == 4}"><fmt:message key="syssetting.areaconfig.northamerica" /></c:when>
						<c:when test="${item.continent == 5}"><fmt:message key="syssetting.areaconfig.africa" /></c:when>
						<c:when test="${item.continent == 6}"><fmt:message key="syssetting.areaconfig.abstract" /></c:when>
						<c:when test="${item.continent == 7}"><fmt:message key="syssetting.areaconfig.antarctica" /></c:when>
					</c:choose>
				</grid:cell>
				<grid:cell>
					<a href="area-info!edit.do?id=${item.id}">
						<fmt:message key="common.list.mod" />
					</a>
					&nbsp;&nbsp;
					<a  href="area-info!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="syssetting.areaconfig.removecheck" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
