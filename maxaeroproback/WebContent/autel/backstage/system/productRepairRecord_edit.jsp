<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   >  <fmt:message key="syssetting.outfactoryproman" />  > <fmt:message key="syssetting.outfactoryproman.hardrepairresult" />   >  <fmt:message key="syssetting.outfactoryproman.edithardrepairresult" /></div>
<form action="product-repair-record!editSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.outfactoryproman.edithardrepairresult" /> </h4>

	<input type="hidden" name="productRepairRecord.id" value="${productRepairRecord.id}" />
	<input type="hidden" name="productRepairRecord.code" value="${productRepairRecord.code}" />
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.repairproduct" />:</label></th>
			<td>
				${productRepairRecord.proCode}
				<input type="hidden" name="productRepairRecord.proCode" value="${productRepairRecord.proCode}" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.producttypevo" />:</label></th>
			<td>
				${productRepairRecord.proTypeName}
				<input type="hidden" value="${productRepairRecord.proTypeName}" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="memberman.complaintman.productserial" />:</label></th>
			<td>
				${productRepairRecord.proSerialNo}
				<input type="hidden" name="productRepairRecord.proSerialNo" value="${productRepairRecord.proSerialNo}" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.date" />:</label></th>
			<td>
				${productRepairRecord.proDate}
				<input type="hidden" name="productRepairRecord.proDate" value="${productRepairRecord.proDate}" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.repairdate" />:</label></th>
			<td>
				<input type="text" name="productRepairRecord.repDate" value="${productRepairRecord.repDate}" dataType="date" isrequired="true"  class="dateinput"  readonly="readonly"/>
			</td>
		</tr>
		
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.brokentype" />:</label></th>
			<td>
				<input type="text" name="productRepairRecord.repType"  value="${productRepairRecord.repType}" maxlength="40" dataType="string" isrequired="true"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.repaircontent" />:</label></th>
			<td>
				<textarea rows="8" cols="40" name="productRepairRecord.repContent">${productRepairRecord.repContent}</textarea>
			</td>
		</tr>
		
	</table>
</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit" value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='product-repair-record!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>
