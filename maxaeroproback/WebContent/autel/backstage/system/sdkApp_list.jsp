<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<style type="text/css">
	table td{
		word-wrap:break-word;word-break:break-all;
	}
</style>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="sdk.app.main.menu" /> > <fmt:message key="sdk.app.menu" /></div>
<form method="post" action="#">
	<div class="grid">
		<div class="toolbar" >
			<div class="active">
				<h4></h4>
			</div>
		</div>
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.appkey" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.appname" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.softwareplatform" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.packagename" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.description" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.autelId" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.status" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.createDate" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.lastUpdateDate" /></grid:cell>
				<!--<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>-->
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.appKey} </grid:cell>
				<grid:cell>${item.appName}</grid:cell>
				<grid:cell>${item.softwarePlatform==0?"ANDROID":"IOS"}</grid:cell>
				<grid:cell>${item.packageName}</grid:cell>
				<grid:cell>${item.description}</grid:cell>
				<grid:cell>${item.autelId}</grid:cell>
				<grid:cell>${item.status==0?"invalid":"valid"}</grid:cell>
				<grid:cell>${item.createDate} </grid:cell>
				<grid:cell>${item.lastUpdateDate}</grid:cell>
				<!--
				<grid:cell>
					<a  href="deleteSdkApp.do?sdkAppVO.id=${item.id }" onclick="javascript:return confirm('<fmt:message key="sdk.app.common.del.msg" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
				</grid:cell>
				-->
			</grid:body>
		
		</grid:grid>
	</div>
</form>
