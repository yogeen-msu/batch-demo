<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderman.paystatus.log" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${context}/js/getIp.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript">

/* alert(remote_ip_info["country"]); */
	$(function() {
		$("#reset_but").click(
				function() {
					//重置文本框
					$(".sear_table").find("input[type='text']").each(
							function(i, obj) {
								$(obj).val("");
							});

					//重置下拉框
					$(".sear_table").find("select option[value='']").attr(
							"selected", "selected");
				});
	});
	function jump(url) {
		var tmpForm = $("<form  method='post'></form>");
		$(tmpForm).attr("action", url);
		tmpForm.appendTo(document.body).submit();
	}
	

	
</script>


</head>

<body>
	<form action="upgradeLogin.do" class="validate" method="post" id="f1" name="f1">


		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			会员管理
			>
			客户管理
			> 客户升级产品IP查询
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;客户升级产品IP查询
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td align="right"><fmt:message
									key="memberman.complaintman.productserial" />:</td>
							<td width="180px"><input name="log.productSn" dataType="string" isrequired="true"
								value="${log.productSn}" type="text" class="sear_item" /></td>
							<td></td>
							<td><input type="submit"
								value="<fmt:message key="common.btn.search" />"
								class="search_but" />&nbsp;<input type="button"
								value="<fmt:message key="common.btn.reset" />"
								class="search_but" id="reset_but" /></td>
						</tr>
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
				</div>
			</div>
			<div class="grid">
				<grid:grid from="webpage">
					<grid:header>
						<grid:cell width="20%">
							<fmt:message key="memberman.complaintman.productserial" />
						</grid:cell>
						<grid:cell width="20%">
							<fmt:message key="marketman.salecontractman.name" />
						</grid:cell>
						<grid:cell width="20%">
							登录IP
						</grid:cell>
						<grid:cell width="20%">登录区域</grid:cell>
						<grid:cell width="20%"><fmt:message key="memberman.customman.cuslogintime" /></grid:cell>
						<%-- <grid:cell width="20%">显示国家</grid:cell> --%>
					</grid:header>
					<grid:body item="item">
						<grid:cell>${item.productSn}</grid:cell>
						
						<grid:cell>
						<a href="${ctx}/autel/market/showSaleContract.do?saleContract.code=${item.saleContractCode}" style="text-decoration: none;color: rgb(215, 18, 0);">
						${item.saleContract}
						</a></grid:cell>
						<grid:cell>${item.ip}</grid:cell>
						<grid:cell>${item.ipStr}</grid:cell>
						<grid:cell>${item.loginStampStr}</grid:cell>
						<%-- <grid:cell><a onclick="javascript:getIp('${ctx}','${item.ipStr}')">显示</a></grid:cell> --%>
					</grid:body>
				</grid:grid>
			</div>
		</div>
	</form>
</body>
</html>


