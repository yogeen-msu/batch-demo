<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../autel/backstage/system/js/productInfo.js.jsp?ajax=yes"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   >  <fmt:message key="syssetting.outfactoryproman" />  > <fmt:message key="syssetting.outfactoryproman.hardrepairresult" />   >  <fmt:message key="syssetting.outfactoryproman.addhardrepairresult" /></div>
<form action="product-repair-record!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.outfactoryproman.addhardrepairresult" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="product.producttypevo" />:</label></th>
			<td>
			    <input type="text" id="productRepairRecord_proTypName"  readonly="readonly"/>
				<input type="hidden" id="productRepairRecord_proCode" name="productRepairRecord.proCode" isrequired="true" readonly="readonly" />
				<input type="button" id="repairProductSelect" value=<fmt:message key="common.openselect.choose" /> >
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="memberman.complaintman.productserial" />:</label></th>
			<td>
				<input type="text" id="productRepairRecord_proSerialNo"  readonly="readonly"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.date" />:</label></th>
			<td>
				<input type="text" id="productRepairRecord_proDate"  readonly="readonly"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.repairdate" />:</label></th>
			<td>
				<input type="text" name="productRepairRecord.repDate"  dataType="date" isrequired="true"  class="dateinput"  readonly="readonly"/>
			</td>
		</tr>
		
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.brokentype" />:</label></th>
			<td>
				<input type="text" name="productRepairRecord.repType" maxlength="40" dataType="string" isrequired="true"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.repaircontent" />:</label></th>
			<td>
				<textarea rows="8" cols="40" name="productRepairRecord.repContent"></textarea>
			</td>
		</tr>
		
	</table>

</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='product-repair-record!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<div id="selectRepairProductDlg"></div>

<script type="text/javascript">
	$("form.validate").validate();
	
	$(function(){
		ProductInfo.init();
	});
	
	function initProductInfo(){
		$(".selectProductItem").click(function(){
			var code = $(this).attr("selectCode");		//编号
			var proTypeName = $(this).attr("selectProTypeName");		//<fmt:message key="product.producttypevo" />
			var serialNo = $(this).attr("selectSerialNo");		//序列号
			var proDate = $(this).attr("selectProDate");		//<fmt:message key="syssetting.outfactoryproman.date" />
			
			$("#productRepairRecord_proCode").val(code);
			$("#productRepairRecord_proTypName").val(proTypeName);
			$("#productRepairRecord_proSerialNo").val(serialNo);
			$("#productRepairRecord_proDate").val(proDate);
			
			Cop.Dialog.close("selectRepairProductDlg");
		});
	}
	setInterval('initProductInfo()',100);
</script>