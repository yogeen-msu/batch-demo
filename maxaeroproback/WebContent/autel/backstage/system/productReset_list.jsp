<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderman.paystatus.log" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript">
	$(function() {
		$("#reset_but").click(
				function() {
					//重置文本框
					$(".sear_table").find("input[type='text']").each(
							function(i, obj) {
								$(obj).val("");
							});

					//重置下拉框
					$(".sear_table").find("select option[value='']").attr(
							"selected", "selected");
				});
	});
	function jump(url) {
		var tmpForm = $("<form  method='post'></form>");
		$(tmpForm).attr("action", url);
		tmpForm.appendTo(document.body).submit();
	}
</script>


</head>

<body>
	<form action="productResetList.do" method="post" id="f1" name="f1">


		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			<fmt:message key="menu.system.setting" />
			>
			<fmt:message key="syssetting.outfactoryproman" />
			> <fmt:message key="product.reset.list" />
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="product.reset" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td align="right"><fmt:message
									key="memberman.complaintman.productserial" />:</td>
							<td width="180px"><input name="log.serialNo"
								value="${log.serialNo}" type="text" class="sear_item" /></td>
							<td></td>
							<td><input type="submit"
								value="<fmt:message key="common.btn.search" />"
								class="search_but" />&nbsp;<input type="button"
								value="<fmt:message key="common.btn.reset" />"
								class="search_but" id="reset_but" /></td>
						</tr>
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4>
						<span> <input name="" type="button"
							onclick="jump('addProductReset.do')"
							value="<fmt:message key="common.btn.add" />"
							class="search_but addorderpaystatuslog" />
						</span><fmt:message key="product.reset.list" />:
					</h4>
				</div>
			</div>
			<div class="grid">
				<grid:grid from="webpage">
					<grid:header>
						<grid:cell width="20%">
							<fmt:message key="memberman.complaintman.productserial" />
						</grid:cell>
						<grid:cell width="20%"><fmt:message key="product.reset.operater" /></grid:cell>
						<grid:cell width="20%"><fmt:message key="memberman.sealerinfoman.registertime" /></grid:cell>
						<grid:cell width="20%"><fmt:message key="product.reset.operate.time" /></grid:cell>
						<grid:cell width="20%">
							<fmt:message key="common.list.operation" />
						</grid:cell>
					</grid:header>
					<grid:body item="item">
						<grid:cell>${item.serialNo}</grid:cell>
						<grid:cell>${item.resetUserName}</grid:cell>
						<grid:cell>${item.regDate}</grid:cell>
						<grid:cell>${item.resetDate}</grid:cell>
						<grid:cell>
							<a href="viewProductReset.do?code=${item.code}"> <fmt:message
									key="memberman.complaintman.show" />
							</a>
						</grid:cell>
					</grid:body>
				</grid:grid>
			</div>
		</div>
	</form>
</body>
</html>


