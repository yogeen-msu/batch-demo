<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../autel/backstage/system/js/productInfo.js.jsp?ajax=yes"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   >  <fmt:message key="syssetting.outfactoryproman" />  > <fmt:message key="syssetting.outfactoryproman.info" />   >  <fmt:message key="syssetting.outfactoryproman.edit" /></div>
<form action="product-info!editSave.do"  class="validate" method="post" name="theForm" id="theForm">

<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.outfactoryproman.edit" /> </h4>
	<input type="hidden" name="productInfo.id" value="${productInfo.id}" />
	<input type="hidden" name="productInfo.code" value="${productInfo.code}" />
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="product.producttypevo" />:</label></th>
			<td>
				<input type="hidden" name="productInfo.proTypeCode" id="productInfo_proTypeCode" value="${productInfo.proTypeCode}"  />
				<input type="text" id="productInfo_proTypName" isrequired="true" readonly="readonly" value="${productInfo.proTypeName}" />
				<c:if test="${productInfo.regStatus != 1}">
					<input type="button" id="productTypeSelect" value=<fmt:message key="common.openselect.choose" /> >
				</c:if>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="memberman.complaintman.productserial" />:</label></th>
			<td>
			${productInfo.serialNo}
<!--				<input type="text" name="productInfo.serialNo" value="${productInfo.serialNo}"  readonly="readonly"/>-->
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="marketman.salecontractman.sale.contract" />:</label></th>
			<td>
				<input type="hidden" name="productInfo.saleContractCode" id="productInfo_saleContractCode" value="${productInfo.saleContractCode}" />
				<input type="text" id="productInfo_saleContractName" value="${productInfo.saleContractName}" isrequired="true" readonly="readonly"/>
				<c:if test="${productInfo.regStatus != 1}">
					<input type="button" id="saleContractSelect" value=<fmt:message key="common.openselect.choose" /> >
				</c:if>
			</td>
		</tr>
		<tr>
			<th style="width: 140px"><label class="text"><fmt:message key="product.productinfo.aricraftSerialNumber" />:</label></th>
			<td>
				<input type="text" name="productInfo.aricraftSerialNumber" value="${productInfo.aricraftSerialNumber}"     dataType="string" isrequired="true" maxlength="250" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.date" />:</label></th>
			<td>
			<c:if test="${productInfo.regStatus != 1}">
				<input type="text" name="productInfo.proDate" value="${productInfo.proDate}" readonly="readonly"  maxlength="23"  dataType="date" isrequired="true"  class="dateinput"/>
			</c:if>
			<c:if test="${productInfo.regStatus == 1}">
				<input type="text" name="productInfo.proDate" value="${productInfo.proDate}" readonly="readonly"  maxlength="23"  dataType="date" isrequired="true" />
			</c:if>
			
			</td>
		</tr>
		<%-- <tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.registstate" />:</label></th>
			<td>
				<input type="radio" name="productInfo.regStatus" value="0" <c:if test="${productInfo.regStatus == null || productInfo.regStatus == 0}">checked="checked"</c:if> > <fmt:message key="syssetting.outfactoryproman.noregist" />
				<input type="radio" name="productInfo.regStatus" value="1" <c:if test="${productInfo.regStatus == 1}">checked="checked"</c:if> > <fmt:message key="syssetting.outfactoryproman.registed" />
			</td>
		</tr> --%>
	<%-- 	<tr>
			<th><label class="text"><fmt:message key="syssetting.outfactoryproman.registdate" />:</label></th>
			<td>
				<input type="text" name="productInfo.regTime" value="${productInfo.regTime}" readonly="readonly"  maxlength="23" class="dateinput"/>
			</td>
		</tr> --%>
		<tr>
			<th><label class="text"><fmt:message key="product.info.imuSerialNumber" />:</label></th>
			<td>
				<input type="text" name="productInfo.imuSerialNumber" value="${productInfo.imuSerialNumber}"   maxlength="250" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.info.remoteControlSerialNumber" />:</label></th>
			<td>
				<input type="text" name="productInfo.remoteControlSerialNumber" value="${productInfo.remoteControlSerialNumber}"   maxlength="250" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.info.batterySerialNumber" />:</label></th>
			<td>
				<input type="text" name="productInfo.batterySerialNumber"  value="${productInfo.batterySerialNumber}"  maxlength="250"  />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.info.gimbalMac" />:</label></th>
			<td>
				<input type="text" name="productInfo.gimbalMac" value="${productInfo.gimbalMac}"   maxlength="250" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="product.info.remoteControlMac" />:</label></th>
			<td>
				<input type="text" name="productInfo.remoteControlMac" value="${productInfo.remoteControlMac}"   maxlength="250"  />
			</td>
		</tr>
		
	</table>

</div>

<div class="submitlist" align="center">
	<table>
		<tr><td >
			<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
			<input name="reset" type="button" onclick="location.href='product-info!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
		</td></tr>
	</table>
</div>
</form>
<div id="selectProductTypeDlg"></div>
<div id="selectSaleContractDlg"></div>

<script type="text/javascript">
	$("form.validate").validate();
	$(function(){
		ProductInfo.init();
	});
	
	
	function initProductInfo(){
		$(".selectPdTypeItem").click(function(){
			var code = $(this).attr("selectCode");		//<fmt:message key="product.producttypevo" />编号
			var name = $(this).attr("selectName");		//<fmt:message key="product.producttypevo" />名称
			$("#productInfo_proTypeCode").val(code);
			$("#productInfo_proTypName").val(name);
			$("#productInfo_proTypName").focus();
			Cop.Dialog.close("selectProductTypeDlg");
		});
		
		
		$(".selectSaleContractItem").click(function(){
			var code = $(this).attr("selectCode");		//<fmt:message key="product.producttypevo" />编号
			var name = $(this).attr("selectName");		//<fmt:message key="product.producttypevo" />名称

			$("#productInfo_saleContractCode").val(code);
			$("#productInfo_saleContractName").val(name);
			$("#productInfo_saleContractName").focus();
			Cop.Dialog.close("selectSaleContractDlg");
		});
	}
	setInterval('initProductInfo()',100);
</script>