<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.outfactoryproman" /> > <fmt:message key="syssetting.outfactoryproman.hardrepairresult" /></div>
<form method="post" action="product-repair-record!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.outfactoryproman.hardrepairresult" /></h4>
			<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				  	<tr height="38">
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="154">
							<select name="productRepairRecord.proTypeCode" id="proTypeCode_select">
								<option value=""><fmt:message key="common.sel.choose" /></option>
								<c:forEach var="item" items="${productTypeList}">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
							<script type="text/javascript">
								var proValue = "${productRepairRecord.proTypeCode}";
								$("#proTypeCode_select option[value='"+proValue+"']").attr("selected","selected");
							</script>
						</td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td width="154">
							<input type="text" name="productRepairRecord.proSerialNo" value="${productRepairRecord.proSerialNo}" />
						</td>
						<td width="160" align="right">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
					</tr>
					  <tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='product-repair-record!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span><fmt:message key="syssetting.outfactoryproman.hardrepairresult" /></h4>
			</div>
		</div>
		
		
		
		<grid:grid from="webpage" ajax="yes">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.date" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.repairdate" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.brokentype" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.proTypeName} </grid:cell>
				<grid:cell>${item.proSerialNo}</grid:cell>
				<grid:cell>${item.proDate}</grid:cell>
				<grid:cell>${item.repDate}</grid:cell>
				<grid:cell>${item.repType}</grid:cell>
				<grid:cell>
					<a href="product-repair-record!edit.do?id=${item.id}">
						<fmt:message key="common.list.mod" />
					</a>
					&nbsp;&nbsp;
					<a  href="product-repair-record!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="syssetting.outfactoryproman.removechecktip" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>
