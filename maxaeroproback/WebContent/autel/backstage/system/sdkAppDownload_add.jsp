<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="sdk.app.main.menu" />  > <fmt:message key="sdk.app.download.menu" />  >  <c:if test="${opeaType =='1' }"><fmt:message key="sdk.app.download.add.menu" /></c:if><c:if test="${opeaType =='2' }"><fmt:message key="sdk.app.download.edit.menu" /></c:if></div>

<form action="addSdkAppDownloads.do"  class="validate" method="post" name="theForm" id="theForm">
<input type="hidden" name="opeaType" value="${opeaType}"/>
<input type="hidden" name="sdkAppDownloadVO.id" value="${sdkAppDownloadVO.id}"/>
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;
		<c:if test="${opeaType =='1' }"><fmt:message key="sdk.app.download.add.menu" /></c:if><c:if test="${opeaType =='2' }"><fmt:message key="sdk.app.download.edit.menu" /></c:if>
	</h4>
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="sdk.app.download.categoryType" />:</label></th>
			<td>
				<select id="selectSdkApp" name="sdkAppDownloadVO.categoryType">
		            <option value="0" <c:if test="${sdkAppDownloadVO.categoryType =='0' }">selected="selected"</c:if> >ANDROID SDK</option>
		            <option value="1" <c:if test="${sdkAppDownloadVO.categoryType =='1' }">selected="selected"</c:if> >IOS SDK</option>
		        </select>
			</td>
		</tr>
		<tr>
			<th style="width:250px;"><label class="text"><fmt:message key="sdk.app.download.sdkCategory" />:</label></th>
			<td><input type="text" name="sdkAppDownloadVO.sdkCategory"  dataType="string" isrequired="true"  maxlength="50" value="${sdkAppDownloadVO.sdkCategory }"  style="width:500px;"/></td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="sdk.app.download.sdkVersion" />:</label></th>
			<td>
				<input type="text" name="sdkAppDownloadVO.sdkVersion" dataType="string" isrequired="true"  maxlength="10"  value="${sdkAppDownloadVO.sdkVersion }"  style="width:500px;"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="sdk.app.download.sdkDownloadUrl" />:</label></th>
			<td>
				<input type="text" name="sdkAppDownloadVO.sdkDownloadUrl" dataType="string" isrequired="true"  maxlength="100" value="${sdkAppDownloadVO.sdkDownloadUrl }" style="width:500px;" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="sdk.app.download.releaseNotes" />:</label></th>
			<td>
				<input type="text" name="sdkAppDownloadVO.releaseNotes" dataType="string" isrequired="true"  maxlength="50" value="${sdkAppDownloadVO.releaseNotes }"  style="width:500px;" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="sdk.app.download.releaseNotesVersion" />:</label></th>
			<td>
				<input type="text" name="sdkAppDownloadVO.releaseNotesVersion" dataType="string" isrequired="true"  maxlength="10" value="${sdkAppDownloadVO.releaseNotesVersion }" style="width:500px;" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="sdk.app.download.releaseNotesDownloadUrl" />:</label></th>
			<td>
				<input type="text" name="sdkAppDownloadVO.releaseNotesDownloadUrl" dataType="string" isrequired="true"  maxlength="100"  value="${sdkAppDownloadVO.releaseNotesDownloadUrl }"  style="width:500px;"/>
			</td>
		</tr>
	</table>
</div>

	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit" value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='querySdkAppDownloadsList.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>
<script type="text/javascript">
	$("form.validate").validate();
</script> 