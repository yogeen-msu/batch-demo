<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="syssetting.toolman" />  > <fmt:message key="syssetting.toolman.download" />   >  <fmt:message key="syssetting.toolman.list" /> </div>
<form method="post" action="tool-download!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.toolman.list" /></h4>
			<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="syssetting.toolman.code" />:</td>
						<td width="154">
							<input type="text" name="toolDownload.code" style="width: 180px;" value="${toolDownload.code}" />
						</td>
						<td width="111" align="right"><fmt:message key="syssetting.toolman.fitproducttypevo" />:</td>
						<td width="154">
							<select name="toolDownload.productTypeCode" id="toolDownload_productTypeCode">
								<option value=""><fmt:message key="syssetting.toolman.checkproducttypevo" /></option>
								<c:forEach var="item" items="${productTypeList}" >
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
						</td>
						<td width="160" align="right">
						</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="memberman.complaintman.publishdate" />:</td>
						<td width="154">
							<input type="text" readonly="readonly" class="dateinput" style="width:80px"  name="toolDownload.startDate" value="${toolDownload.startDate}" />
							--
							<input type="text" readonly="readonly" class="dateinput" style="width:80px"  name="toolDownload.endDate" value="${toolDownload.endDate}" />
						</td>
						<td width="111" align="right"></td>
						<td width="154">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
						<td width="160" align="right">
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='tool-download!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span><fmt:message key="syssetting.toolman.list" /></h4>
			</div>
		</div>
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="180px"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="common.name" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.toolman.newversion" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.toolman.downloadlocation" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.publishdate" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item" >
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.code} </grid:cell>
				<grid:cell>${item.toolName}</grid:cell>
				<grid:cell>${item.lastVersion}</grid:cell>
				<grid:cell style="word-wrap: break-word; word-break: break-all;width:300px;">
				${item.downloadPath }
				</grid:cell>
				<grid:cell>${item.releaseDate}</grid:cell>
				<grid:cell>
					<a href="tool-download!edit.do?code=${item.code}">
						<fmt:message key="common.list.mod" />
					</a>
					 &nbsp;&nbsp;&nbsp;&nbsp;
					<a href="tool-download!delete.do?code=${item.code}" onclick="javascript:return confirm('<fmt:message key="syssetting.toolman.removecheck" />')">
						<fmt:message key="common.list.del" />
					 </a> 
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>

<script type="text/javascript">

$(function(){
	//选中
	var sopt = "${toolDownload.productTypeCode}";
	$("#toolDownload_productTypeCode option[value='"+sopt+"']").attr("selected","selected");
	
	$("#reset_but").click(function(){
		//重置文本框
		$(".sear_table").find("input[type='text']").each(function(i,obj){
			$(obj).val("");
		});		
		
		//重置下拉框
		$(".sear_table").find("select option[value='']").attr("selected","selected");
	});
});
	
</script>
