<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderman.paystatus.log.addinfo" /></title>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/adminthemes/default/css/input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/statics/js/common/common.js"></script>
<link href="${ctx}/adminthemes/default/css/validate.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/common/commonjavascript.js"></script>


</head>

<body>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > 
	<fmt:message key="syssetting.outfactoryproman" /> > <fmt:message key="product.reset.product" />
	</div>
	<div class="input">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="product.reset.product" />
			</h4>
	<form action="saveProductReset.do"  class="validate" method="post" name="f1" id="f1">
		
		<table class="form-table">
			
			<tr>
			<td style="text-align:right">
				<font color="red"><fmt:message key="other.pay.warn" />:</font></td>
				<td>
					<font color="red"><fmt:message key="product.reset.warn" /></font>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="memberman.complaintman.productserial" />:</label></th>
				<td>
					<input type="text" name="log.serialNo" maxlength="40" dataType="string" isrequired="true" fun="checkordercode"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text"><fmt:message key="product.reset.operate.reason" />:</label></th>
				<td>
					<textarea name="log.resetReason" cols="60" rows="5" dataType="string" isrequired="true"  maxlength="200"></textarea>
				</td>
			</tr>
					
		</table>
	</form>
	</div>
	
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="button" type="button" value="<fmt:message key="saleConfig.update.jsp.button.save" />" class="submitBtn insertorderpaystatuslog" />
						<input name="button" type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="submitBtn" />
					</td>
				</tr>
			</table>
		</div>
	
	<script type="text/javascript">
	$("form.validate").validate();

	$(document).ready(function(){
		
		$(".insertorderpaystatuslog").click(function(){
			$("#f1").attr("action","saveProductReset.do").submit();
		});
	});
	
	</script>
	
</body>
</html>
