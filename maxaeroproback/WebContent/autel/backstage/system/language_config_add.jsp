<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../autel/backstage/system/js/language.js.jsp?ajax=yes"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="syssetting.areaconfig" />  > <fmt:message key="syssetting.languageconfig.baselanguage" />   >  <fmt:message key="syssetting.languageconfig.addconfig" /></div>

<form action="language-config!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.languageconfig.addconfig" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="syssetting.languageconfig.name" />:</label></th>
			<td><input type="text" name="languageConfig.name"  dataType="string" isrequired="true" maxlength="40"/></td>
		</tr>
	</table>
	
	<div class="grid" style="width:99%;">
		<div style="margin:0px -7px -6px 7px;" class="toolbar" >
			<div class="active">
				<h4><span><input type="button" id="selectLanguageBtn" value=<fmt:message key="common.btn.add" /> class="search_but" /></span><fmt:message key="syssetting.config.baselanguage" /></h4>
			</div>
		</div>
		<grid:grid from="languageConfig.languageList">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.otherconfig.languagename" /></grid:cell>
				<grid:cell width="20px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item" id="languageTable" >
				<grid:cell></grid:cell>
				<grid:cell></grid:cell>
				<grid:cell></grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
	
</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='language-config!list.do'"	 value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<div id="selectDlg"></div>

<script type="text/javascript">
	$("form.validate").validate();
	
	/** <fmt:message key="common.list.del" /><fmt:message key="syssetting.languageconfig" />项*/
	function removeLgItem(code){
		$("#lg_"+code).parent().remove();
	}
	
	$(function(){
		Language.init();
	});
</script>