<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ taglib uri="/WEB-INF/grid.tld" prefix="grid"%>
<script type="text/javascript" src="../js/PasswordOperator.js"></script>
<script type="text/javascript" src="${ctx}/autel/js/system/adminUser.js"></script>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
<form action="userAdmin.do" method="post" id="f1" name="f1">

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> &gt;
	<fmt:message key="syssetting.authorityman" /> &gt; <fmt:message key="syssetting.authorityman.manager" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="${ctx}/autel/main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="syssetting.authorityman.manager" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="syssetting.authorityman.username" />:</td>
						<td width="154"><input name="adminUser.username" value="${adminUser.username}" type="text" class="time_in" /></td>
						<td width="111" align="right"><fmt:message key="syssetting.authorityman.managername" />:</td>
						<td width="154"><input name="adminUser.realname" value="${adminUser.realname}" type="text" class="time_in" />
						</td>
						<td>
						&nbsp;&nbsp;<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
						<input name="" type="button" value="<fmt:message key="common.btn.add" />" onclick="javascript:jump('userAdmin!add.do');" class="search_but adduseradmin" />
					</span><fmt:message key="syssetting.authorityman.managerinfo" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
				<grid:cell width="50px">ID</grid:cell>
					<grid:cell width="110px"><fmt:message key="syssetting.authorityman.username" /></grid:cell>
					<grid:cell width="200px"><fmt:message key="syssetting.authorityman.managername" /></grid:cell>
					<grid:cell><fmt:message key="syssetting.authorityman.state" /></grid:cell>
					<grid:cell width="180px"><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="userAdmin">
					<grid:cell>
						${userAdmin.userid}
					</grid:cell>
					<grid:cell>&nbsp;${userAdmin.username }
					</grid:cell>
					<grid:cell>&nbsp;${userAdmin.realname } </grid:cell>
					<grid:cell> 
					 <c:if test="${userAdmin.state==1}"><fmt:message key="syssetting.authorityman.statestart" /></c:if>
					 <c:if test="${userAdmin.state==0}"><fmt:message key="syssetting.authorityman.statestop" /></c:if>
					</grid:cell>
					<grid:cell>&nbsp;
						<a href="userAdmin!edit.do?id=${userAdmin.userid }">
							<fmt:message key="common.list.mod" />
						</a>
						 &nbsp;&nbsp;
						<a href="userAdmin!delete.do?id=${userAdmin.userid }" onclick="javascript:return confirm('<fmt:message key="syssetting.authorityman.message1" />')">
						<fmt:message key="common.list.del" /></a>					
					</grid:cell>
				</grid:body>
			</grid:grid>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//<fmt:message key="common.btn.reset" />文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//<fmt:message key="common.btn.reset" />下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>

