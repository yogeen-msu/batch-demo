<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > <fmt:message key="syssetting.otherconfig" /> > <fmt:message key="syssetting.otherconfig.telphoneconfig" /></div>
<form method="post" action="phone-message-cfg!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.otherconfig.telphoneconfig" /></h4>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='phone-message-cfg!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
			</div>
		</div>
		
		<grid:grid from="phoneMessageCfgList">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="250px"><fmt:message key="common.name" /></grid:cell>
				<grid:cell><fmt:message key="common.list.config" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.authorityman.state" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>
					${fn:substring(item.name,0,20) }
					<c:if test="${fn:length(item.name) > 20 }">...</c:if>
				</grid:cell>
				<grid:cell>
					${fn:substring(item.config,0,30) }
					<c:if test="${fn:length(item.config) > 30 }">...</c:if>
				</grid:cell>
				<grid:cell>
					<c:if test="${item.status == 0}"><fmt:message key="cententman.messageman.nostart" /></c:if>
					<c:if test="${item.status == 1}"><font color="red"><fmt:message key="cententman.messageman.started" /></font></c:if>
				</grid:cell>
				<grid:cell>
					<a href="phone-message-cfg!edit.do?id=${item.id}"><fmt:message key="common.list.mod" /></a>
					&nbsp;
					<a href="phone-message-cfg!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="syssetting.otherconfig.telphoneconfig.removecheck" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
