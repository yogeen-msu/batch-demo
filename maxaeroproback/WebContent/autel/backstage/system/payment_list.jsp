<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../autel/backstage/system/js/payment.js.jsp?ajax=yes"></script>
<script>
	$(function() { Payment.init(); });
</script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.payset" />  > <fmt:message key="syssetting.payset.payconfig" /></div>
<div class="grid">

	<div class="toolbar" >
		<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.payset.payconfig" /></h4>
		<div class="active">
			<h4>
				<span>
					<input type="button" onclick="location.href='payment-cfg!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" />
					<input type="button" id="delBtn" value=<fmt:message key="common.list.del" /> class="search_but" />
				</span>
				&nbsp;
			</h4>
		</div>
	</div>
	
<form method="POST"><grid:grid from="paymentCfglist">

	<grid:header>
		<grid:cell width="100px">
			<input type="checkbox" id="toggleChk" />
		</grid:cell>
		<grid:cell sort="name" ><fmt:message key="syssetting.payset.payconfig.name" /></grid:cell>
		<grid:cell sort="name" width="250px"><fmt:message key="common.list.operation" /></grid:cell>
	</grid:header>

	<grid:body item="payment">
		<grid:cell>
			<input type="checkbox" name="id" value="${payment.id}" /></grid:cell>
		<grid:cell>${payment.name}</grid:cell>
		<grid:cell>
			<a href="payment-cfg!edit.do?paymentId=${payment.id }"><fmt:message key="common.list.mod" /></a>
			&nbsp;&nbsp;
			<a class="delete" deleteItemId="${payment.id }" ><fmt:message key="common.list.del" /></a>
		</grid:cell>
	</grid:body>

</grid:grid></form>
<div style="clear: both; padding-top: 5px;"></div>
</div>
