<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="syssetting.otherconfig" />  > <fmt:message key="syssetting.otherconfig.telphoneconfig" />   >  <fmt:message key="syssetting.otherconfig.editsmsconfig" /></div>
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.otherconfig.editsmsconfig" /> </h4>

<form action="phone-message-cfg!editSave.do"  class="validate" method="post" name="theForm" id="theForm">
	<input type="hidden" name="phoneMessageCfg.id" value="${phoneMessageCfg.id}" />
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="common.name" />:</label></th>
			<td>
				<input type="text" name="phoneMessageCfg.name" value="${phoneMessageCfg.name}"  dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.authorityman.state" />:</label></th>
			<td>
				
				<input type="radio" name="phoneMessageCfg.status" value="0" <c:if test="${phoneMessageCfg.status == 0}">checked="checked"</c:if> /> <fmt:message key="cententman.messageman.nostart" />
				&nbsp;&nbsp;&nbsp;
				<input type="radio" name="phoneMessageCfg.status" value="1" <c:if test="${phoneMessageCfg.status == 1}">checked="checked"</c:if> /> <fmt:message key="marketman.promotion.start" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.smsconfigcontent" /></label></th>
			<td>
				<input type="text" name="phoneMessageCfg.config" value="${phoneMessageCfg.config}"  dataType="string" isrequired="true" maxlength="40" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.smsconfigdesc" /></label></th>
			<td>
				<textarea name="phoneMessageCfg.biref" style="width:350px;height:150px" >${phoneMessageCfg.biref}</textarea>
			</td>
		</tr>
	</table>
</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='phone-message-cfg!list.do'"	 value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>