<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:
	<fmt:message key="menu.system.setting" />   > 
	<fmt:message key="syssetting.toolman" />  > 
	<fmt:message key="syssetting.sealerman" />   >  
	<fmt:message key="syssetting.sealerman.list" /> 
</div>
<form method="post" action="sealer-download!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.sealerman.list" /></h4>
			<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="100" align="right"><fmt:message key="syssetting.sealerman.name" />:</td>
						<td width="200">
							<input type="text" name="sealerDownload.name" style="width: 180px;" value="${sealerDownload.name}" />
						</td>
						<td width="100" align="right"><fmt:message key="memberman.complaintman.publishdate" />:</td>
						<td width="300">
							<input type="text" readonly="readonly" class="dateinput" style="width:80px"  name="sealerDownload.startDate" value="${sealerDownload.startDate}" />
							--
							<input type="text" readonly="readonly" class="dateinput" style="width:80px"  name="sealerDownload.endDate" value="${sealerDownload.endDate}" />
						</td>
						<td width="" align="left">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='sealer-download!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span><fmt:message key="syssetting.sealerman.list" /></h4>
			</div>
		</div>
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.sealerman.name" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.sealerman.type" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.toolman.downloadlocation" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.publishdate" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item" >
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.name} </grid:cell>
				<grid:cell>${item.type} </grid:cell>
				<grid:cell>${item.fileUrl }</grid:cell>
				<grid:cell>${item.releaseDate}</grid:cell>
				<grid:cell>
					<a href="sealer-download!edit.do?id=${item.id}"><fmt:message key="common.list.mod" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="sealer-download!delete.do?id=${item.id}" onclick="javascript:return confirm('<fmt:message key="syssetting.sealerman.removecheck" />')">
						<fmt:message key="common.list.del" />
					</a> 
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>

<script type="text/javascript">

$(function(){
	$("#reset_but").click(function(){
		//重置文本框
		$(".sear_table").find("input[type='text']").each(function(i,obj){
			$(obj).val("");
		});	
	});
});
	
</script>
