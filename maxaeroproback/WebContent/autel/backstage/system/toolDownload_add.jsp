<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<link href="../../autel/main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../autel/backstage/system/js/selfdialog.js"></script>
<style>
.form-table th {
    text-align: center;
}
.input table th {
    padding-left:0px;
    padding-right:0px;
}
</style>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="syssetting.toolman" />  > <fmt:message key="syssetting.toolman.download" />     > <fmt:message key="syssetting.toolman.add" /> </div>

<form action="tool-download!addSave.do"  class="validate" method="post" name="theForm" id="theForm" enctype="multipart/form-data">
<div class="input" style="width: 98%">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.toolman.add" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="syssetting.toolman.version" />:</label></th>
			<td><input type="text" name="toolDownload.lastVersion"  dataType="string" isrequired="true" maxlength="40"/></td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.toolman.downloadlocation" />:</label></th>
			<td>
				<input style="width:400px" type="text" name="toolDownload.downloadPath"  dataType="string" isrequired="true" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="grid">
					<div class="toolbar" style="width:100%;margin-left:7px;" >
						<div class="active">
							<h4><span><input type="button" id="addToolName" value=<fmt:message key="common.btn.add" /> class="search_but" /></span><fmt:message key="syssetting.toolman.name" /></h4>
						</div>
					</div>
					<grid:grid from="toolNameList">
						<grid:header>
							<grid:cell width="50px"><fmt:message key="memberman.complaintman.language" /></grid:cell>
							<grid:cell><fmt:message key="common.name" /></grid:cell>
							<grid:cell width="20px"><fmt:message key="common.list.operation" /></grid:cell>
						</grid:header>
					
						<grid:body item="item" id="tool_language_view" >
							<grid:cell id="LanCode_${item.code}" attrs="languageName">
								<input type="hidden" name="languageCode" value="${item.language}" />
								${item.language}
							</grid:cell>
							<grid:cell id="LanText_${item.code}">
								<input type="hidden" name="languageName" value="${item.toolName}" />
								${item.toolName}
							</grid:cell>
							<grid:cell>
								<a href="#" onclick="removeLanItem('${item.code}',true)">
									<fmt:message key="marketman.saleconfigman.moveout" />
								</a>
							</grid:cell>
						</grid:body>
					
					</grid:grid>
				</div>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
				<div class="grid">
					<div class="toolbar"  style="width:100%;margin-left:7px;" >
						
						<div class="active">
							<h4><span><input type="button" id="addProductType" value=<fmt:message key="common.btn.add" /> class="search_but" /></span><fmt:message key="syssetting.toolman.fitproducttypevo" /></h4>
						</div>
					</div>
					<grid:grid from="toolproTypeList">
						<grid:header>
							<grid:cell width="50px"><fmt:message key="syssetting.authorityman.userno" /></grid:cell>
							<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
							<grid:cell width="20px"><fmt:message key="common.list.operation" /></grid:cell>
						</grid:header>
					
						<grid:body item="item" id="tool_proType_view" >
							<grid:cell id="ProCode_${item.code}" attrs="productTypeSelect">
								<input type="hidden" name="productTypeCode" value="${item.code}" />
								${item.code}
							</grid:cell>
							<grid:cell id="ProText_${item.code}">${item.name}</grid:cell>
							<grid:cell>
								<a href="#" onclick="removeProItem('${item.code}',true)">
									<fmt:message key="marketman.saleconfigman.moveout" />
								 </a>
							</grid:cell>
						</grid:body>
					
					</grid:grid>
				</div>
			</td>
		</tr>
		
		
	</table>

</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='tool-download!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>


<div class="input" style="border: 0px;">
<!-- 下载工具选择 -->
<div id="toolNameSelectDiv" class="active">
	<div class="dialog_overlay"></div>
	<div class="dialog_win">
		<h2><span class="dialog_close">×</span></h2>
		<div class="tc_tab">
			<form id="language_add_form"  class="validate" onsubmit="return false;" >
				<table>
					<tr>
						<td></td>
						<td width="100px"><fmt:message key="syssetting.toolman.name" />:</td>
						<td width="90px">
							<select id="language_select"  style="height:22px"  >
								<c:forEach var="item" items="${languageList}">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
						</td>
						<td align="left"><input type="text" id="language_text"  dataType="string" isrequired="true" maxlength="32" style="height:22px" /></td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3" align="right"><input name="submit" type="button"  value=<fmt:message key="common.btn.finish" /> id="language_select_submit" class="search_but" /></td>
					</tr>
					
				</table>
			</form>
			
		</div>
	</div>
</div>


<!-- 工具适用产品型号选择-->
<div id="productTypeSelectDiv" class="active">
	<div class="dialog_overlay"></div>
	<div class="dialog_win">
		<h2><span class="dialog_close">×</span></h2>
		<div class="tc_tab">
			<form id="proType_add_form"  class="validate" onsubmit="return false;" >
				<table>
					<tr>
						<td><fmt:message key="product.producttypevo" />:</td>
					</tr>
					<tr>
						<td>
							<c:forEach var="item" items="${productTypeList}" varStatus="index" >
								<div style="width:260px;float:left;" title="${item.name}">
									<input type="checkbox" name="proType_select_item" value="${item.code}" itemName="${item.name}" >
									<c:if test="${fn:length(item.name) > 20 }">${fn:substring(item.name,0,20)}...</c:if>
									<c:if test="${fn:length(item.name) <= 20 }">${item.name}</c:if>
								</div>
							</c:forEach>
						</td>
					</tr>
				</table>
				    <span style="padding-left:500px">
					<input name="submit" type="button"  value=<fmt:message key="common.btn.finish" /> id="product_type_select_submit" class="search_but" />
					</span>
					<br/>
			</form>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	var deleteName = '<fmt:message key="common.list.del" />';
	$("form.validate").validate();
	
	$("#language_select_submit").click(function(){
		if(!$("#language_add_form").checkall()){
			return;
		}
		
		var selectOption =  $("#language_select").find("option:selected");
		var selectText = $("#language_text").val();
		
		if($("#LanCode_"+selectOption.val()).length == 0){
			var td1 = $("<td id='LanCode_"+selectOption.val()+"' attrs='languageName' >"
							+"<input type='hidden' name='languageCode' value='"+selectOption.val()+"' />"
							+selectOption.text()+"</td>");
			var td2 = $("<td id='LanText_"+selectOption.val()+"'>"
							+"<input type='hidden' name='languageName' value='"+selectText+"' />"
							+selectText+"</td>");
			var td3 = $("<td><a href='#' onclick=\"removeLanItem('"+selectOption.val()+"',false);\" >"+deleteName+"</a></td>");
			var tr = $("<tr></tr>").append(td1).append(td2).append(td3);
			$("#tool_language_view").append(tr);
			
		}else{		//当前语言已存在
			var newValue = $("<input type='hidden' name='languageName' value='"+selectText+"' />"+selectText);
			//$("#LanText_"+selectOption.val()).text(selectText);
			$("#LanText_"+selectOption.val()).empty().append(newValue);
		}
		
		languageDialog.close();
	});
	
	function removeLanItem(lanCode,hasSer){
		if( confirm('<fmt:message key="syssetting.toolman.removecheck" />')){	//确认删除此下载工具吗？
			$("#LanCode_"+lanCode).parent().remove();		//删除当前页面数据
		}		
	}
	
	var  languageSelectCallback = function(){
		$("#language_text").val("");
	};
	
	//语言名称选择
	var languageDialog = new selfOpenDialog({id:"toolNameSelectDiv",btnId:"addToolName",callback:languageSelectCallback});
	
		//产品型号选择
	var productTypeDialog = new selfOpenDialog({id:"productTypeSelectDiv",btnId:"addProductType"});
	$("#product_type_select_submit").click(function(){
		
		$("input[name='proType_select_item']:checked").each(function(i,obj){
			
			var value = $(obj).val();
			var text = $(obj).attr("itemName");
			
			if($("#ProCode_"+value).length == 0){
				var td1 = $("<td id='ProCode_"+value+"' attrs='productTypeSelect' ><input type='hidden' name='productTypeCode' value='"+value+"' />"+value+"</td>");
				var td2 = $("<td id='ProText_"+value+"'>"+text+"</td>");
				var td3 = $("<td><a href='#' onclick=\"removeProItem('"+value+"',false);\" >"+deleteName+"</a></td>");
				var tr = $("<tr></tr>").append(td1).append(td2).append(td3);
				$("#tool_proType_view").append(tr);
				
			}else{		//当前产品型号已存在
				$("#ProText_"+value).text(text);
			}
		});
		
		productTypeDialog.close();
	});
	
	function removeProItem(toolCode,hasSer){
		if( confirm('<fmt:message key="syssetting.toolman.productcode.removecheck" />')){			//确认删除此适应型号吗？
			$("#ProCode_"+toolCode).parent().remove();		//删除当前页面数据
			
			$("input[name='proType_select_item']").attr("checked",false);
			if(hasSer){
				location.href="tool-download!delete.do?code="+toolCode;
			}
		}		
	}
	
</script>