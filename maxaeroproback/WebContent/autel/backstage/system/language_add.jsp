<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="syssetting.areaconfig" />  > <fmt:message key="syssetting.languageconfig.baselanguage" />   >  <fmt:message key="syssetting.languageconfig.addbaselanguage" /></div>

<form action="language!addSave.do"  class="validate" method="post" name="theForm" id="theForm">
<input type="hidden" id="language.isShow" name="language.isShow" value="0"/>
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.languageconfig.addbaselanguage" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.languagename" />:</label></th>
			<td><input type="text" name="language.name"  dataType="string" isrequired="true"  maxlength="40" /></td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.countrycode" />:</label></th>
			<td>
				<input type="text" name="language.countryCode" dataType="string" isrequired="true"  maxlength="10"  />
				<fmt:message key="syssetting.languageconfig.areacodedesc" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.languagecode" />:</label></th>
			<td>
				<input type="text" name="language.languageCode" dataType="string" isrequired="true"  maxlength="10"  />
				<fmt:message key="syssetting.languageconfig.languagecodedesc" />
			</td>
		</tr>
		
	</table>
</div>

	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='language!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
</script>