<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   > <fmt:message key="syssetting.areaconfig" />  > <fmt:message key="syssetting.areaconfig.basearea" />   >  <fmt:message key="syssetting.areaconfig.editbasearea" /> </div>

<form action="area-info!editSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.areaconfig.editbasearea" /> </h4>

	<input type="hidden" name="areaInfo.id" value="${areaInfo.id}" />
	<input type="hidden" name="areaInfo.code" value="${areaInfo.code}" />
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="common.list.code" />:</label></th>
			<td>${areaInfo.code}</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.areaconfig.continent" />:</label></th>
			<td>
				<select name="areaInfo.continent" id="area_config_continent">
					<option value="1"><fmt:message key="syssetting.areaconfig.asia" />
					<option value="2"><fmt:message key="syssetting.areaconfig.europe" />
					<option value="3"><fmt:message key="syssetting.areaconfig.southamerica" />
					<option value="4"><fmt:message key="syssetting.areaconfig.northamerica" />
					<option value="5"><fmt:message key="syssetting.areaconfig.africa" />
					<option value="6"><fmt:message key="syssetting.areaconfig.abstract" />
					<option value="7"><fmt:message key="syssetting.areaconfig.antarctica" />
				</select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.areaconfig.areaname" />:</label></th>
			<td><input type="text" name="areaInfo.name" value="${areaInfo.name}"  dataType="string" isrequired="true" maxlength="40"/></td>
		</tr>
	</table>

</div>
<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='area-info!list.do'"	 value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
	
	//选中洲
	var continent = "${areaInfo.continent}";
	$("#area_config_continent option[value='"+continent+"']").attr("selected","selected");
</script>