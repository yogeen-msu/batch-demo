<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../autel/backstage/system/js/productInfo.js"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   >  <fmt:message key="syssetting.outfactoryproman" />  > <fmt:message key="syssetting.outfactoryproman.info" />   >  <fmt:message key="syssetting.batch.productupdate" /></div>
<form action="product-info!batchEditSave.do"  class="validate" method="post" name="theForm" id="theForm">

<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.batch.productupdate" /></h4>
	<table class="form-table">
		<c:if test="${editSetp == 0}">
			<tr>
				<th><label class="text"><fmt:message key="syssetting.batch.productnumber" /></label></th>
				<td>
					<input type="hidden" name="editSetp" value="1" />
					<fmt:message key="syssetting.batch.productNote" /><br/><br/>
					<textarea style="width:450px;height:200px;" name="batchSerialNo" isrequired="true"></textarea>
				</td>
			</tr>
		</c:if>
		<c:if test="${editSetp == 1}">
			<tr>
				<th><label class="text"><fmt:message key="syssetting.batch.productnumber" /></label></th>
				<td>
					${batchSerialNo }
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="syssetting.batch.productmodel" /></label></th>
				<td>
					${proTypeName }
				</td>
			</tr>
			<tr>
				<th><label class="text"><fmt:message key="marketman.salecontractman.sale.contract" />:</label></th>
				<td>
					<input type="hidden" name="proTypeCode" id="productInfo_proTypeCode" value="${proTypeCode}"  />
					<input type="hidden" name="batchSerialNo" value="${batchSerialNo }" />
					
					<input type="hidden" name="editSetp" value="2" />
					<input type="hidden" id="productInfo_saleContractCode" name="saleContractCode" />
					<input type="text" id="productInfo_saleContractName" style="width:300px;" isrequired="true" readonly="readonly"/>
					<input type="button" id="saleContractSelect" value=<fmt:message key="common.openselect.choose" /> >
				</td>
			</tr>
		</c:if>
	</table>

</div>

<div class="submitlist" align="center">
	<table>
		<tr><td >
			<c:if test="${editSetp == 0}">
				<input name="submit" type="submit"	  value="<fmt:message key='common.next.name' />" class="submitBtn" />
			</c:if>
			<c:if test="${editSetp == 1}">
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
			</c:if>
			
			<input name="reset" type="button" onclick="history.go(-1);" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
		</td></tr>
	</table>
</div>
</form>
<div id="selectProductTypeDlg"></div>
<div id="selectSaleContractDlg"></div>

<script type="text/javascript">
	$("form.validate").validate();
	$(function(){
		ProductInfo.init();
	});
	
	
	function initProductInfo(){
		$(".selectPdTypeItem").click(function(){
			var code = $(this).attr("selectCode");		//<fmt:message key="product.producttypevo" />编号
			var name = $(this).attr("selectName");		//<fmt:message key="product.producttypevo" />名称
			$("#productInfo_proTypeCode").val(code);
			$("#productInfo_proTypName").val(name);
			Cop.Dialog.close("selectProductTypeDlg");
		});
		
		
		$(".selectSaleContractItem").click(function(){
			var code = $(this).attr("selectCode");		//<fmt:message key="product.producttypevo" />编号
			var name = $(this).attr("selectName");		//<fmt:message key="product.producttypevo" />名称

			$("#productInfo_saleContractCode").val(code);
			$("#productInfo_saleContractName").val(name);
			Cop.Dialog.close("selectSaleContractDlg");
		});
	}
	setInterval('initProductInfo()',100);
</script>