<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.otherconfig" /> > <fmt:message key="syssetting.otherconfig.sitelanguage" /></div>
<div class="grid">
	<div class="toolbar" >
		<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.otherconfig.sitelanguage" /></h4>
		<div class="active">
			<h4><span><input type="button" onclick="location.href='site-language!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
		</div>
	</div>
		
<grid:grid from="languageList">
	<grid:header>
		<grid:cell width="100px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
		<grid:cell width="300px"><fmt:message key="syssetting.otherconfig.languagename" /></grid:cell>
		<grid:cell width="300px"><fmt:message key="syssetting.otherconfig.countrycode" /></grid:cell>
		<grid:cell><fmt:message key="syssetting.otherconfig.languagecode" /></grid:cell>
		<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
	</grid:header>

  <grid:body item="language">
		<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
		<grid:cell>&nbsp;${language.name} </grid:cell>
		<grid:cell>&nbsp;${language.stateCode} </grid:cell>
		<grid:cell>&nbsp;${language.languageCode} </grid:cell>
		<grid:cell>&nbsp; 
			 <a  href="site-language!delete.do?id=${language.id }" onclick="javascript:return confirm('<fmt:message key="syssetting.otherconfig.sitelanguage.removecheck" />')">
			 	<fmt:message key="common.list.del" />
			 </a>
		</grid:cell>
	</grid:body>
  
</grid:grid>  
</div>


