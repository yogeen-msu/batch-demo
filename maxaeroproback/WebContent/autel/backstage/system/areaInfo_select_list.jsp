<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="grid">
	<input type="button" value=<fmt:message key="common.btn.finish" /> class="submitBtn" id="selectBtn" />
	<br/>
	
	
	<c:choose>
		<c:when test="${fn:length(areaInfoList) > 0 }">
			<c:forEach var="item" items="${areaInfoList}" varStatus="index" >
				<div style="width: 120px;float:left;">
					<input type="checkbox" name="select_Item" itemLanId="${item.id}" value="${item.code}" itemLanName="${item.name}" >
					${item.name}
				</div>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<p><fmt:message key="syssetting.areaconfig.noselect" /></p>
		</c:otherwise>
	</c:choose>
</div>
