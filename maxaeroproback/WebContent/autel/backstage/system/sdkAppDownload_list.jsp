<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<style type="text/css">
	table td{
		word-wrap:break-word;word-break:break-all;
	}
</style>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="sdk.app.main.menu" /> > <fmt:message key="sdk.app.download.menu" /></div>
<form method="post" action="language!list.do">
	<div class="grid">
		<div class="toolbar" >
			<div class="active">
				<h4><span><input type="button" onclick="location.href='addAppDownloadsPage.do?opeaType=1'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
			</div>
		</div>
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.sdkCategory" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.sdkVersion" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.sdkDownloadUrl" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.categoryType" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.releaseNotes" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.releaseNotesVersion" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.releaseNotesDownloadUrl" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.createDate" /></grid:cell>
				<grid:cell><fmt:message key="sdk.app.download.lastUpdateDate" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.sdkCategory} </grid:cell>
				<grid:cell>${item.sdkVersion}</grid:cell>
				<grid:cell>${item.sdkDownloadUrl}</grid:cell>
				<grid:cell>${item.categoryType==0?"ANDROID SDK":"IOS SDK"}</grid:cell>
				<grid:cell>${item.releaseNotes} </grid:cell>
				<grid:cell>${item.releaseNotesVersion}</grid:cell>
				<grid:cell>${item.releaseNotesDownloadUrl}</grid:cell>
				<grid:cell>${item.createDate} </grid:cell>
				<grid:cell>${item.lastUpdateDate}</grid:cell>
				<grid:cell>
					<a  href="deleteSdkAppDownloads.do?sdkAppDownloadVO.id=${item.id }" onclick="javascript:return confirm('<fmt:message key="sdk.app.common.del.msg" />')">
						<fmt:message key="common.list.del" />
					</a>
					<a  href="addAppDownloadsPage.do?opeaType=2&sdkAppDownloadVO.id=${item.id }">
						<fmt:message key="common.list.mod" />
					</a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
