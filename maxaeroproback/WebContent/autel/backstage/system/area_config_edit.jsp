<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../autel/backstage/system/js/area.js.jsp?ajax=yes"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > <fmt:message key="syssetting.areaconfig" /> ><fmt:message key="syssetting.areaconfig" />  > <fmt:message key="syssetting.areaconfig.editareaconfig" /> </div>

<form action="area-config!editSave.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.areaconfig.editareaconfig" /> </h4>
	<input type="hidden" name="areaConfig.id" value="${areaConfig.id}" />
	<input type="hidden" name="areaConfig.code" value="${areaConfig.code}" />
	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="syssetting.areaconfig.areaconfigname2" />:</label></th>
			<td><input type="text" name="areaConfig.name" value="${areaConfig.name}" dataType="string" isrequired="true" maxlength="40"/></td>
		</tr>
	</table>
	
	<div class="grid" style="width:99%;">
		<div style="margin:0px -7px -6px 7px;" class="toolbar" >
			<div class="active">
				<h4><span><input type="button" id="selectAreaBtn" value=<fmt:message key="syssetting.areaconfig.addarea" /> class="search_but" /></span><fmt:message key="syssetting.areaconfig.basearea2" /></h4>
			</div>
		</div>
		<grid:grid from="areaConfig.areaInfoList">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.areaconfig.areaname" /></grid:cell>
				<grid:cell width="20px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item" id="areaInfoTable" >
				<grid:cell clazz="areaInfoTable_item" id="tr_${item.code}">
					<input type="hidden" name="ids" value="${item.id}"/>
					${item.code}
				</grid:cell>
				<grid:cell>${item.name}</grid:cell>
				<grid:cell>
					<a href="javascript:removeTrItem('${item.code}')" onclick="javascript:return confirm('<fmt:message key="syssetting.areaconfig.removecheck" />')">
				 		<fmt:message key="common.list.del" />
				 	</a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='area-config!list.do'"	 value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

<div id="selectDlg"></div>

<script type="text/javascript">
	$("form.validate").validate();
	
	/** <fmt:message key="common.list.del" /><fmt:message key="syssetting.areaconfig" />项*/
	function removeTrItem(code){
		$("#tr_"+code).parent().remove();
	}
	
	$(function(){
		Area.init();
	});
</script>