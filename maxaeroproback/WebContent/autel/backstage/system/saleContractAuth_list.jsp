<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  
                     > 权限管理
                     > 销售契约权限管理</div>

<div class="grid">
<form method="post" action="listAddSaleContractAuth.do">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div>销售契约权限管理</h4>
				<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="8">
						<td width="111" align="right"> 销售契约名称:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="saleContractAuth.saleContractName" value="${saleContractAuth.saleContractName}" />
						</td>
						
						<td width="111" align="right"> 管理人员:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="saleContractAuth.userName" value="${saleContractAuth.userName}" />
						</td>
						
						<td width="154">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			
			<div class="active">
				<h4><span>
					<input type="button" onclick="jump('toAddSaleContractAuth.do')" value=<fmt:message key="common.btn.add" /> class="search_but" />
				</span>
				&nbsp;
				</h4>
			</div>
			
			
		</div>
</form>		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="140px">销售契约编码</grid:cell>
				<grid:cell width="140px">销售契约名称</grid:cell>
				<grid:cell width="250px">管理人员</grid:cell>
				<grid:cell width="65px">操作</grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>
					${item.saleContractCode }
				</grid:cell>
				<grid:cell>
					<a href="${ctx}/autel/market/showSaleContract.do?saleContract.code=${item.saleContractCode}" style="text-decoration: none;color: rgb(215, 18, 0);">${item.saleContractName}</a>
				</grid:cell>
				<grid:cell>
					${item.userName}
				</grid:cell>
				<grid:cell>
				    <a id="addBound" class="addBound" href="delSaleContractAuth.do?id=${item.id}">删除 </a> 
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>

<script type="text/javascript">
$(function(){
	$("#reset_but").click(function(){
		//重置文本框
		$(".sear_table").find("input[type='text']").each(function(i,obj){
			$(obj).val("");
		});		
		
		//重置下拉框
		$(".sear_table").find("select option[value='']").attr("selected","selected");
	});
});
function jump(url) {
	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action", url);
	tmpForm.appendTo(document.body).submit();
}
</script>