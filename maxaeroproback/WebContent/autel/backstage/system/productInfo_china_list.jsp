<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > 
	<fmt:message key="syssetting.outfactoryproman" /> > <fmt:message key="syssetting.outfactoryproman.info" />
</div>
<form method="post" action="product-info!listForChinaSealer.do">
	<div class="grid">
		
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.outfactoryproman.info" /></h4>
			<div class="sear_table">
				<table width="100%" style="border:0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="154">
							<select name="productInfo.proTypeCode" id="proTypeCode_select" class="sear_item">
								<option value=""><fmt:message key="select.option.select" /></option> 
								<c:forEach var="item" items="${productTypeList}">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
							</select>
							<script type="text/javascript">
								var proValue = "${productInfo.proTypeCode}";
								$("#proTypeCode_select option[value='"+proValue+"']").attr("selected","selected");
							</script>
						</td>
						<td width="111" align="right"><fmt:message key="memberman.complaintman.productserial" />:</td>
						<td width="154">
							<input class="sear_item" type="text" name="productInfo.serialNo" value="${productInfo.serialNo}" />
						</td>
						
						<td width="111" align="right"><fmt:message key="rechargecard.type.list.jsp.sales.contract.name" />:</td>
						<td width="154">
							<input class="sear_item" type="text" name="productInfo.saleContractName" value="${productInfo.saleContractName}" />
						</td>
						
						
						<td width="160" align="right">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4><span>

				</span>
				<fmt:message key="syssetting.outfactoryproman.info" />
				</h4>
			</div>
		</div>
		
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
				<grid:cell><fmt:message key="memberman.complaintman.productserial" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.date" /></grid:cell>
				<grid:cell><fmt:message key="memberman.sealerinfoman.registertime" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.outfactoryproman.registstate" /></grid:cell>
				<grid:cell><fmt:message key="product.info.externInfo1" /></grid:cell>
				<grid:cell><fmt:message key="product.info.externInfo2" /></grid:cell>
				<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>
					${fn:substring(item.proTypeName,0,20)}
					<c:if test="${fn:length(item.proTypeName) > 20 }">...</c:if>
				</grid:cell>
				<grid:cell>
					${fn:substring(item.serialNo,0,20)}
					<c:if test="${fn:length(item.serialNo) > 20 }">...</c:if>
				</grid:cell>
				<grid:cell>${item.proDate}</grid:cell>
				<grid:cell>${fn:substring(item.regTime,0,10)}</grid:cell>
				<grid:cell>
					<c:if test="${item.regStatus == 0}"><fmt:message key="syssetting.outfactoryproman.noregist" /></c:if>
					<c:if test="${item.regStatus == 1}"><fmt:message key="syssetting.outfactoryproman.registed" /></c:if>
					<c:if test="${item.regStatus == 2}"><fmt:message key="memberman.customman.unbinded" /></c:if>
				</grid:cell>
				<grid:cell>${item.externInfo1}</grid:cell>
				<grid:cell>${item.externInfo2}</grid:cell>
				<grid:cell>
				<a href="product-info!editForChina.do?id=${item.id}&operType=1"><fmt:message key="common.list.mod" /></a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>

