<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.areaconfig" /> > <fmt:message key="syssetting.areaconfig" /></div>
<form method="post" action="area-config!list.do">
	<div class="grid">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.areaconfig" /></h4>
			<div class="sear_table">
				<table width="100%">
					  <tr height="38">    
						<td width="150" align="right"><fmt:message key="syssetting.areaconfig.areaconfigname2" />:</td>
						<td width="200">
							<input type="text" name="areaConfig.name" value="${areaConfig.name}" />
						</td>
						<td align="left">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />	
						</td>
						<td></td>
					  </tr>
				</table>
			</div>
			<div class="active">
				<h4><span><input type="button" onclick="location.href='area-config!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span><fmt:message key="syssetting.areaconfig" /></h4>
			</div>
		</div>
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="200px"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.areaconfig.areaconfigname" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.code} </grid:cell>
				<grid:cell>${item.name}</grid:cell>
				<grid:cell>
					<a href="area-config!edit.do?id=${item.id}">
						<fmt:message key="common.list.mod" />
					</a>
					&nbsp;&nbsp;
					<a  href="area-config!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="syssetting.areaconfig.removeareacheck" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
		});
	});
</script>
