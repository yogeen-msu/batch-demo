<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="js/jquery.checktree.js"></script>
<script type="text/javascript" src="../../autel-cqc/menu.do?showall=yes"></script>
<script type="text/javascript" src="js/Auth.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="auth/checktree.css" />
<script type="text/javascript">
var message1 = '<fmt:message key="role.message.message1" />';
var message2 = '<fmt:message key="role.message.message2" />';
var message3 = '<fmt:message key="role.message.message3" />';
var message4 = '<fmt:message key="role.message.message4" />';
var message5 = '<fmt:message key="role.message.message5" />';
</script>
	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> &gt;
<fmt:message key="syssetting.authorityman" /> &gt;
		<c:if test="${isEdit==1}"><fmt:message key="syssetting.roleman.mod" /></c:if>
		<c:if test="${isEdit==0}"><fmt:message key="syssetting.roleman.add" /></c:if>
	</div>
<div class="input">
<style>
#new_action{
cursor:pointer;
text-indent:-9999px; 
display:block;
}
#actionDlg th{
	width:60px;
	padding:0px;
}
#menubox{
width:250px;
height:300px;
overflow:auto;
}
#actbox {width:300px}
#actbox li{
	margin-top:5px;
	background-color:#F4F9FF;
}
</style> 
<c:if test="${isEdit==1}">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.roleman.mod" /> </h4>
<form method="post" action="role!saveEdit.do" class="validate"   id="brandForm" >
<input type="hidden" name="role.roleid" value="${role.roleid }" />
<input type="hidden" name="roleid" value="${role.roleid }" />

</c:if> 
<c:if test="${isEdit==0}">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.roleman.add" /> </h4>
<form method="post" action="role!saveAdd.do" class="validate"   id="brandForm" >
<input type="hidden" name="roleid" value="0" />
</c:if>


<table cellspacing="1" cellpadding="3" width="100%" class="form-table">
     <tr>
       <th><label class="text"><fmt:message key="syssetting.roleman.name" />:</label></th>
       <td><input type="text" name="role.rolename" id="rolename" maxlength="60" value="${role.rolename }"  dataType="string" isrequired="true" fun='rolenameExist'/></td>
      </tr>
     <tr>
       <th><label class="text"><fmt:message key="syssetting.roleman.rolememo" />:</label></th>
       <td> 
    	  <textarea name="role.rolememo" style="width:300px;height:150px;" dataType="string" isrequired="true">${role.rolememo}</textarea>
       </td>
      </tr>
</table>
   
   
<table cellspacing="1" cellpadding="3" width="100%" class="form-table">
    <tr>
      <th><label class="text"><fmt:message key="syssetting.roleman.check" />:</label></th>
      <td>
		<div id="actbox">
			<div id="opbar"><span id="new_action" class="add"><fmt:message key="common.btn.add" /></span></div>
			<ul>
			<c:forEach items="${authList}" var="act">
				<li id="li_${act.actid }"><input type="checkbox" name="acts" value="${act.actid }" /><span>${act.name }</span>
<%-- 					 <img class="modify" src="images/transparent.gif" authid="${act.actid }">&nbsp; <img authid="${act.actid }" class="delete" src="images/transparent.gif" > --%>
					 <a class="modify" href="#" authid="${act.actid }" style="color: #d71200;text-decoration: none;"><fmt:message key="common.list.mod" /></a>&nbsp; <a authid="${act.actid }" class="delete" href="#" style="color: #d71200;text-decoration: none;"><fmt:message key="common.list.del" /></a>
       
				</li>
			</c:forEach>
			</ul>
		</div>
	  </td>
     </tr>
</table>

   
<div class="submitlist" align="center">
 <table>
 <tr><td >
  <input type="submit" name="submit"  value="<fmt:message key="common.btn.ok" />" class="submitBtn" />
   </td>
   </tr>
 </table>
</div>
</form>
</div>
<div id="actionDlg"></div>
<script>

//验证用户名是否可用
function rolenameExist()
{
	var roleid=$("input[name='roleid']").eq(0).val();
	var rolename=$("input[name='role.rolename']").eq(0).val();
	var result = -1;
	$.ajax({
		url:'role!rolenameExist.do?ajax=yes',
		type:"post",
		data:{"role.roleid":roleid,"role.rolename":rolename},
		dataType:'JSON',
		async: false,
		success :function(data){
			var resultData = eval(eval(data));
			result = resultData[0];
		},
		error :function(data){
			result = -2;
		}
	});
	
	if(result == 0)
		return true;
	else
		return "<fmt:message key="common.js.existed" />";
}

$(function(){
	$("form.validate").validate();
	<c:if test="${isEdit==1}">
		<c:forEach items="${role.actids}" var="actid">
			$("#actbox input[value=${actid}]").attr("checked",true);
		</c:forEach>
	</c:if>
	AuthAction.init();
	
});
</script>
