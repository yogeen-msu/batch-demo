<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp"%>
<link href="${ctx}/autel/main/css/right.css" rel="stylesheet" type="text/css" />

<form action="role!list.do" method="post" id="f1" name="f1">

	<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> &gt;
	<fmt:message key="syssetting.authorityman" /> &gt; <fmt:message key="syssetting.roleman" />
	</div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="${ctx}/autel/main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="syssetting.roleman" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="syssetting.roleman.name" />:</td>
						<td width="154"><input name="role.rolename" value="${role.rolename}" type="text" class="time_in" /></td>
						</td>
						<td>
						&nbsp;&nbsp;<input type="submit" value="<fmt:message key="common.btn.search" />" class="search_but" />&nbsp;<input type="button" value="<fmt:message key="common.btn.reset" />" class="search_but" id="reset_but" />
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span>
						<input name="" type="button" value="<fmt:message key="common.btn.add" />" onclick="window.location.href='role!add.do';" class="search_but addrole" />
					</span><fmt:message key="syssetting.roleman" />:
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid  from="webpage">
			
				<grid:header>
				<grid:cell sort="name" width="250px"><fmt:message key="syssetting.roleman.name" /></grid:cell> 
				<grid:cell sort="url"><fmt:message key="syssetting.roleman.rolememo" /></grid:cell> 
				<grid:cell  width="100px"><fmt:message key="common.list.operation" /></grid:cell> 
				</grid:header>
			
			  <grid:body item="role">
			        <grid:cell>${role.rolename } </grid:cell>
			        <grid:cell>${role.rolememo} </grid:cell> 
			        <grid:cell> 
			       		 <a  href="role!edit.do?roleid=${role.roleid }" ><fmt:message key="common.list.mod" /></a> 
			 &nbsp;&nbsp;<a  href="role!delete.do?roleid=${role.roleid }" onclick="javascript:return confirm('<fmt:message key="syssetting.roleman.message1" />');" ><fmt:message key="common.list.del" /></a> 
			        </grid:cell> 
			  </grid:body>  
			  
			</grid:grid>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(function(){
		$("#reset_but").click(function(){
			//<fmt:message key="common.btn.reset" />文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//<fmt:message key="common.btn.reset" />下拉框
			$(".sear_table").find("select option[value='']").attr("selected","selected");
		});
	});
</script>
