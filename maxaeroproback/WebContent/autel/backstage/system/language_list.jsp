<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.languageconfig" /> > <fmt:message key="syssetting.languageconfig.baselanguage" /></div>
<form method="post" action="language!list.do">
	<div class="grid">
		<div class="toolbar" >
			<div class="active">
				<h4><span><input type="button" onclick="location.href='language!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" /></span>&nbsp;</h4>
			</div>
		</div>
	
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="200px"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.otherconfig.languagename" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.otherconfig.countrycode" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.otherconfig.languagecode" /></grid:cell>
				<grid:cell><fmt:message key="syssetting.otherconfig.showonfrant" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.code} </grid:cell>
				<grid:cell>${item.name}</grid:cell>
				<grid:cell>${item.countryCode}</grid:cell>
				<grid:cell>${item.languageCode}</grid:cell>
				<grid:cell>
					<c:choose>
						<c:when test="${item.isShow == 1}">
							<a href='language!editIsShowState.do?id=${item.id}&isShow=0'><fmt:message key="common.opt.hidden" /></a>
						</c:when>
						<c:otherwise>
							<a href="language!editIsShowState.do?id=${item.id}&isShow=1"><fmt:message key="common.opt.show" /></a>
						</c:otherwise>
					</c:choose>
					
				</grid:cell>
				<grid:cell>
					<a href="language!edit.do?id=${item.id}">
						<fmt:message key="common.list.mod" />
					</a>
					&nbsp;&nbsp;
					<a  href="language!delete.do?id=${item.id }" onclick="javascript:return confirm('<fmt:message key="syssetting.languageconfig.removecheck" />')">
					 	<fmt:message key="common.list.del" />
					 </a>
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
