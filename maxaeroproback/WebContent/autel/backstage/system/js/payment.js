var Payment=$.extend({},Cop.Grid,{
	init:function(){
		var self =this;
		$("#delBtn").click(function(){self.doDelete();});
		$("#toggleChk").click(function(){
			self.toggleSelected(this.checked);}
		);
		$(".delete").click(function(){self.deleteItem($(this).attr("deleteItemId"));});
	},
	doDelete:function(){
		
		if(!this.checkIdSeled()){
			alert("请选择要删除的品牌");
			return ;
		}
	 
		if(!confirm("确认要将删除支付方式吗？")){	
			return ;
		}
		
		$.Loading.show("正在删除支付方式...");
		
		this.deletePost("payment-cfg!delete.do");
			
	},
	
	deleteItem:function(id){
		if(confirm("确认要删除该支付方式?")){
			$("input[name='id'][value='"+id+"']").attr("checked","checked");
			this.deletePost("payment-cfg!delete.do?id="+id);
		}
	}
	
});
