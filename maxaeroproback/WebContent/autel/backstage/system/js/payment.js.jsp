<%@ include file="/autel/js/common/fmt.js.jsp"%>
var Payment=$.extend({},Cop.Grid,{
	init:function(){
		var self =this;
		$("#delBtn").click(function(){self.doDelete();});
		$("#toggleChk").click(function(){
			self.toggleSelected(this.checked);}
		);
		$(".delete").click(function(){self.deleteItem($(this).attr("deleteItemId"));});
	},
	doDelete:function(){
		
		if(!this.checkIdSeled()){
		
			alert("<fmt:message key='syssetting.payset.payremoveselect' />");
			return ;
		}
	 
		if(!confirm("<fmt:message key='syssetting.payset.payremovecheck' />")){	
			return ;
		}
		
		$.Loading.show("<fmt:message key='syssetting.payset.payremoving' />");
		
		this.deletePost("payment-cfg!delete.do");
			
	},
	
	deleteItem:function(id){
		if(confirm("<fmt:message key='syssetting.payset.payremovecheck' />")){
			$("input[name='id'][value='"+id+"']").attr("checked","checked");
			this.deletePost("payment-cfg!delete.do?id="+id);
		}
	}
	
});
