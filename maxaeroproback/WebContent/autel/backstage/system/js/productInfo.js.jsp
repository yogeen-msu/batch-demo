<%@ include file="/autel/js/common/fmt.js.jsp"%>
var ProductInfo  ={
	init:function(){
		var self  = this;
		Cop.Dialog.init({id:"selectProductTypeDlg",modal:false,title:"<fmt:message key='product.producttypevo' />",height:"400px",width:"650px"});
		$("#productTypeSelect").click(function(){
			self.openProductTypeSelectDlg();
		});
		
		Cop.Dialog.init({id:"selectSaleContractDlg",modal:false,title:"<fmt:message key='marketman.salecontractman.sale.contract' />",height:"400px",width:"650px"});
		$("#saleContractSelect").click(function(){
			var proTypeCode = $("#productInfo_proTypeCode").val();		//éå®å¥çº¦å³èçäº§ååå·
			if(proTypeCode == null || proTypeCode == ""){
				alert("<fmt:message key='product.productinfo.release.select' />");
			}else{
				self.openSaleContractSelectDlg(proTypeCode);
			}
		});
		
		Cop.Dialog.init({id:"selectRepairProductDlg",modal:false,title:"<fmt:message key='syssetting.outfactoryproman.repairproduct' />",height:"400px",width:"750px"});
		$("#repairProductSelect").click(function(){
			self.openRepairProductSelectDlg();
		});
		
	}, 
	
	/**
	 * æå¼äº§ååå·éæ©å¯¹è¯æ¡
	 */
	openProductTypeSelectDlg:function(){
		var self  = this;
		$("#selectProductTypeDlg").load("product-info!selectProductType.do?ajax=yes",function(){
			$(".selectPdTypeItem").click(function(){
				var code = $(this).attr("selectCode");		//äº§ååå·ç¼å·
				var name = $(this).attr("selectName");		//äº§ååå·åç§°
				self.productTypeAdd(code,name);
			});
		});
		Cop.Dialog.open("selectProductTypeDlg");
	},
	
	/**
	 * ä¿å­éæ©ç¼å·
	 */
	productTypeAdd:function(code,name){
		$("#productInfo_proTypeCode").val(code);
		$("#productInfo_proTypName").val(name);
		Cop.Dialog.close("selectProductTypeDlg");
		
		$("#productInfo_saleContractCode").val("");
		$("#productInfo_saleContractName").val("");
		
	},
	
	/**
	 * æå¼éå®å¥çº¦éæ©å¯¹è¯æ¡
	 */
	openSaleContractSelectDlg:function(proTypeCode){
		var self  = this;
		$("#selectSaleContractDlg").load("product-info!selectsaleContract.do?ajax=yes&proTypeCode="+proTypeCode,function(){
			$(".selectSaleContractItem").click(function(){
				var code = $(this).attr("selectCode");		//äº§ååå·ç¼å·
				var name = $(this).attr("selectName");		//äº§ååå·åç§°
				self.saleContractAdd(code,name);
			});
		});
		Cop.Dialog.open("selectSaleContractDlg");
	},
	/**
	 * ä¿å­éæ©ç¼å·
	 */
	saleContractAdd:function(code,name){
		$("#productInfo_saleContractCode").val(code);
		$("#productInfo_saleContractName").val(name);
		Cop.Dialog.close("selectSaleContractDlg");
	},
	
	
	/**
	 * æå¼ç»´ä¿®äº§åéæ©å¯¹è¯æ¡
	 */
	openRepairProductSelectDlg:function(){
		var self  = this;
		$("#selectRepairProductDlg").load("product-repair-record!selectProductInfo.do?ajax=yes",function(){
			$(".selectProductItem").click(function(){
				var code = $(this).attr("selectCode");		//ç¼å·
				var proTypeName = $(this).attr("selectProTypeName");		//äº§ååå·
				var serialNo = $(this).attr("selectSerialNo");		//åºåå·
				var proDate = $(this).attr("selectProDate");		//åºåæ¥æ
				
				repairProductAdd(code,proTypeName,serialNo,proDate);
			});
		});
		Cop.Dialog.open("selectRepairProductDlg");
	}
};

var repairProductAdd = function(code,proTypeName,serialNo,proDate){
	$("#productRepairRecord_proCode").val(code);
	$("#productRepairRecord_proTypName").val(proTypeName);
	$("#productRepairRecord_proSerialNo").val(serialNo);
	$("#productRepairRecord_proDate").val(proDate);
	
	Cop.Dialog.close("selectRepairProductDlg");
};

function pageConfigQuery(){
	$.ajax({
		url:"product-info!selectsaleContract.do",
		data: $('#pageSelectConfigForm').serialize(), 
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			
			$(".selectSaleContractItem").click(function(){
				var code = $(this).attr("selectCode");		//äº§ååå·ç¼å·
				var name = $(this).attr("selectName");		//äº§ååå·åç§°
				$("#productInfo_saleContractCode").val(code);
		        $("#productInfo_saleContractName").val(name);
		        Cop.Dialog.close("selectSaleContractDlg");
			});
		},
		error:function(){
			alert("Error :");
		}
	});
};




function bindEvent(pager,grid){
	var grid = pager.parent();
	 pager.find("li>a").unbind(".click").bind("click",function(){
		 load($(this).attr("pageno"),grid);
	 }); 
	 pager.find("a.selected").unbind("click");
};

function pageQuery(){
	$.ajax({
		url:"product-repair-record!selectProductInfo.do",
		data: $('#pageSelectForm').serialize(), 
		success:function(html){
			var grid = $(".grid");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//ç»å®åé¡µäºä»¶
			
			$(".selectProductItem").click(function(){
				var code = $(this).attr("selectCode");		//ç¼å·
				var proTypeName = $(this).attr("selectProTypeName");		//äº§ååå·
				var serialNo = $(this).attr("selectSerialNo");		//åºåå·
				var proDate = $(this).attr("selectProDate");		//åºåæ¥æ
				
				repairProductAdd(code,proTypeName,serialNo,proDate);
			});
		},
		error:function(){
			alert("Error :(");
		}
	});
};

