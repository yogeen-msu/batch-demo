function selfOpenDialog(opt){
	
	var option = opt;
	
	option.disX = 0;
	option.disY = 0;
	
	option.eventEle = opt.eventEle || [];			//展示事件促发元素
	
	if(option.id == null) return;
	
	if(option.btnId != null){
		option.oBtn = $("#"+option.btnId);
		option.eventEle.push(option.oBtn);
	}
		
	
	option.ctx = $("#"+option.id);
	option.oWin = $(option.ctx).children("div .dialog_win");
	option.oLay = $(option.ctx).children("div .dialog_overlay");
	option.oClose = $(option.oWin).find(".dialog_close");
	option.oH2 = $(option.oClose).parent();
	
	$(option.eventEle).each(function(i,ele){
		$(ele).click(function(){
			
			//展示前置事件
			if(option.beforeEvent != null && typeof(option.beforeEvent)=="function" ){	
				option.beforeEvent(this);
			}
			
			$(option.oLay).css({"display":"block"});
			$(option.oWin).css({"display":"block"});
		});
	});
	
	
	$(option.oClose).click(function(){
		$(option.oLay).css({"display":"none"});
		$(option.oWin).css({"display":"none"});
		
		if(option.callback != null && typeof(option.callback)=="function" ){
			 option.callback();
		}
	});
	
	$(option.oClose).mousedown(function(event){
		(event || window.event).cancelBubble = true;
	});
	
	
	this.close=function(){
		$(option.oLay).css({"display":"none"});
		$(option.oWin).css({"display":"none"});
		if(option.callback != null && typeof(option.callback)=="function" ){
			option.callback();
		}
	};
	
//	$(option.oH2).mousedown(function(event){
//		var event = event || window.event;
//		option.bDrag = true;
//		option.disX = event.clientX - $(option.oWin).offset().left;
//		option.disY = event.clientY - $(option.oWin).offset().top;	
//		this.setCapture && this.setCapture();		
//		return false;
//	});
//	
//	$(document).mousemove(function(event){
//		if (!option.bDrag) return;
//		var event = event || window.event;
//		var iL = event.clientX - option.disX;
//		var iT = event.clientY - option.disY;
////		var maxL = document.documentElement.clientWidth - option.oWin.offsetWidth;
////		var maxT = document.documentElement.clientHeight - option.oWin.offsetHeight;	
//		var maxL = document.documentElement.clientWidth - $(option.oWin).offset().left;
//		var maxT = document.documentElement.clientHeight - $(option.oWin).offset().top;	
//		iL = iL < 0 ? 0 : iL;
//		iL = iL > maxL ? maxL : iL; 		
//		iT = iT < 0 ? 0 : iT;
//		iT = iT > maxT ? maxT : iT;
//		
//		$(option.oWin).css({"margin-top":"0px","margin-left":"0px",});
//		$(option.oWin).offset({left:iL,top:iT})
//		
////		option.oWin.style.marginTop = option.oWin.style.marginLeft = 0;
////		option.oWin.style.left = iL + "px";
////		option.oWin.style.top = iT + "px";		
//		return false;
//	});
//	
//	document.onmouseup = window.onblur = option.oH2.onlosecapture = function(){
//		option.bDrag = false;				
//		option.oH2.releaseCapture && option.oH2.releaseCapture();
//	};
};

/*selfOpenDialog.option = {
	id:null,		//弱出div id
	btnId:null,		//弹出按扭ID
	ctx:null,
	oWin:null,
	oLay:null,
	oBtn:null,
	oClose:null,
	oH2:null,
	bDrag:false,
	disX:0,
	disY:0,
	callback:null		//关闭时的回调方法
};*/

selfOpenDialog.close=function(){
	$(option.oLay).css({"display":"none"});
	$(option.oWin).css({"display":"none"});
	if(option.callback != null && typeof(option.callback)=="function" ){
		option.callback();
	}
};
