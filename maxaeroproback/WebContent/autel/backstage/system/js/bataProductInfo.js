var ProductInfo  ={
	init:function(){
		Cop.Dialog.init({id:"selectSaleContractDlg",modal:false,title:"销售契约选择",height:"400px",width:"750px"});
	}, 
	
	/**
	 * 打开销售契约选择对话框
	 */
	openSaleContractSelectDlg:function(serialNo){    
	    $("#batchSerialNo").val(serialNo);
		$("#selectSaleContractDlg").load("selectsaleContract.do?ajax=yes",function(){
			clickEvent();
		});
		Cop.Dialog.open("selectSaleContractDlg");
	},
	/**
	 * 保存选择编号
	 */
	saleContractAdd:function(code,name){
		$("#productInfo_saleContractCode").val(code);
		$("#productInfo_saleContractName").val(name);
		Cop.Dialog.close("selectSaleContractDlg");
	},
};


function bindEvent(pager,grid){   
	var grid = pager.parent();
	 pager.find("li>a").unbind(".click").bind("click",function(){
		 load($(this).attr("pageno"),grid);
	 }); 
	 pager.find("a.selected").unbind("click");
};

function clickEvent(){
	$(".selectSaleContractItem").click(function(){     
	var batchSerialNo=$("#batchSerialNo").val();
	var code = $(this).attr("selectCode");		//产品型号编号
	var name = $(this).attr("selectName");		//产品型号名称
	$.ajax({
		url:"saveEditContract.do?ajax=yes",
		data:({batchSerialNo:batchSerialNo,saleContractCode:code}),
		type:"GET",
		dataType:'JSON',
		async: false,
		success:function(data){
			var jsonData = eval(eval(data));   
			if(jsonData[0].result==1){
				//alert("更新成功");
				$("#saleCode_"+batchSerialNo).html(name);
			}
			/*else{
				alert(jsonData[0].message);
			}*/
			$("#batchSerialNo").val('');
			Cop.Dialog.close("selectSaleContractDlg");
		},
		error:function(data){
			alert("Error :");
		}
	});
	
});
}
;
function pageConfigQuery(){   
	$.ajax({
		url:"selectsaleContract.do",
		//data: $('#pageSelectConfigForm').serialize(), 
		//type:"json",
		data:({"contract.proTypeCode":$("#selectProType option:selected").val(),"contract.name":$("#contractName").val()}),
		type:"GET",
		success:function(html){          
			var grid = $("#searchSale");
			grid.empty().append( $(html).find(".grid").children() );
			bindEvent(grid.children(".page"));			//绑定分页事件
			clickEvent();
		},
		error:function(){
			alert("Error :");
		}
	});
};