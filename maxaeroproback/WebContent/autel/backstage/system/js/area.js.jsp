<%@ include file="/autel/js/common/fmt.js.jsp"%>
var Area  ={
	init:function(){
		var self  = this;
		Cop.Dialog.init({id:"selectDlg",modal:false,title:"<fmt:message key='syssetting.areaconfig.checkarea' />",height:"300px",width:"450px"});
		$("#selectAreaBtn").click(function(){
			self.openSelectDlg();
		});
	}, 
	
	/**
	 * 打开选择对话框
	 */
	openSelectDlg:function(){
		var self  = this;
		$("#selectDlg").load("area-info!selectlist.do?ajax=yes",function(){
			$("#selectBtn").click(function(){
				self.add();
			});
		});
		Cop.Dialog.open("selectDlg");
	},

	/**
	 * 保存添加
	 */
	add:function(){
		
		//已有的区域
		var oldlans = [];
		$(".areaInfoTable_item").each(function(i,obj){
			oldlans.push($(obj).attr("id"));
		});
		
		//	$.Loading.show('正在保存，请稍侯...');
		var slelans = [];
		$("input[type='checkbox'][name='select_Item']").each(function(i,obj){
			if($(obj).attr("checked")){
				for(var j=0;j<oldlans.length;j++){
					var tempCode = oldlans[j];
					if(tempCode == ("tr_"+$(obj).val()) ){	//已存在
						return;
					}
				}
				slelans.push(obj);
			}
		});
		
		//增加新选中的
		var areaTable = $("#areaInfoTable");
		if($(".areaInfoTable_item").size()== 0){
			$(areaTable).empty();
		}
		
		$(slelans).each(function(i,obj){
			var tr = $("<tr></tr>");
			var td1=$("<td class='areaInfoTable_item' id='tr_"+$(obj).val()+"' ><input type='hidden' name='ids' value='"+$(obj).attr("itemLanId")+"'/>"+$(obj).val()+"</td>");
			var td2=$("<td>"+$(obj).attr("itemLanName")+"</td>");
			var td3=$("<td><a href=\"javascript:removeTrItem('"+$(obj).val()+"')\" onclick=\"javascript:return confirm('<fmt:message key='syssetting.areaconfig.removecheck' />')\">" +
					"<fmt:message key='common.list.del' /></a></td>");
					
			$(tr).append(td1).append(td2).append(td3);
			areaTable.append(tr);
		});
		
		$.Loading.hide();
		Cop.Dialog.close("selectDlg");
	}
};