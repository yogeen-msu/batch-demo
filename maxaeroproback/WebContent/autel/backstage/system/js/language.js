var Language  ={
	init:function(){
		var self  = this;
		Cop.Dialog.init({id:"selectDlg",modal:false,title:"语言选择",height:"300px",width:"450px"});
		$("#selectLanguageBtn").click(function(){
			self.openSelectDlg();
		});
	}, 
	
	/**
	 * 打开选择对话框
	 */
	openSelectDlg:function(){
		var self  = this;
		$("#selectDlg").load("language!selectlist.do?ajax=yes",function(){
			$("#selectBtn").click(function(){
				self.add();
			});
		});
		Cop.Dialog.open("selectDlg");
	},

	/**
	 * 保存添加
	 */
	add:function(){
		
		//已有的语言
		var oldlans = [];
		$(".languageTable_item").each(function(i,obj){
			oldlans.push($(obj).attr("id"));
		});
		
		//	$.Loading.show('正在保存，请稍侯...');
		var slelans = [];
		$("input[type='checkbox'][name='select_language_Item']").each(function(i,obj){
			if($(obj).attr("checked")){
				for(var j=0;j<oldlans.length;j++){
					var tempCode = oldlans[j];
					if(tempCode == ("lg_"+$(obj).val()) ){	//已存在
						return;
					}
				}
				slelans.push(obj);
			}
		});
		
		//增加新选中的
		var lanTable = $("#languageTable");
		if($(".languageTable_item").size()== 0){
			$(lanTable).empty();
		}
		
		$(slelans).each(function(i,obj){
			var tr = $("<tr></tr>");
			var td1=$("<td class='languageTable_item' id='lg_"+$(obj).val()+"' ><input type='hidden' name='lanIds' value='"+$(obj).attr("itemLanId")+"'/>"+$(obj).val()+"</td>");
			var td2=$("<td>"+$(obj).attr("itemLanName")+"</td>");
			var td3=$("<td><a href=\"javascript:removeLgItem('"+$(obj).val()+"')\" onclick=\"javascript:return confirm('确认删除此语言置吗？')\">" +
					"删除</a></td>");
			$(tr).append(td1).append(td2).append(td3);
			lanTable.append(tr);
		});
		
		$.Loading.hide();
		Cop.Dialog.close("selectDlg");
	}
};