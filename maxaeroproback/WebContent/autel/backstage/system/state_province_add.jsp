<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:
	<fmt:message key="menu.system.setting" /> &gt;
	<fmt:message key="syssetting.areaconfig" /> &gt; 
	<fmt:message key="syssetting.province" />
</div>
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.province.add" /> </h4>
	<form action="state-province!save.do"  class="validate" method="post" name="theForm" id="theForm" >
	<table  class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="common.info.country" />:</label></th>
			<td>
				<select class="countrycode" name="stateProvince.countryId" isrequired="true">
					<option value="">--<fmt:message key="select.option.select" />--</option>
					<c:forEach items="${countrys }" var="item">
						<option value="${item.id}">${item.country}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="regcustomer.info.province" />:</label></th>
			<td><input type="text" name="stateProvince.state"  dataType="string" isrequired="true"/></td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.province.code" />:</label></th>
			<td><input type="text" name="stateProvince.code" dataType="string" isrequired="true" /></td>
		</tr>
	</table>
	
	<div class="submitlist" align="center">
	 	<table>
	 	<tr>
	 		<td>
	 			<input name="submit" type="submit" value="<fmt:message key="common.btn.ok" />" class="submitBtn" />
	 			<input name="reset" type="button" onclick="location.href='state-province!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
	 		</td>
	   	</tr>
	 	</table>
	</div>
	</form>
</div>
<script type="text/javascript">
	$("form.validate").validate();
</script>