<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:
	<fmt:message key="menu.system.setting" /> &gt;
	<fmt:message key="syssetting.areaconfig" /> &gt; 
	<fmt:message key="syssetting.countrycode" />
</div>
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.countrycode.add" /> </h4>
	<form action="country-code!save.do"  class="validate" method="post" name="theForm" id="theForm" >
	<table  class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="common.info.country" />:</label></th>
			<td><input type="text" name="countryCode.country"  dataType="string" isrequired="true"/></td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="memberman.customman.countrycode" />:</label></th>
			<td><input type="text" name="countryCode.code" dataType="string" isrequired="true" /></td>
		</tr>
	</table>
	
	<div class="submitlist" align="center">
	 	<table>
	 	<tr>
	 		<td>
	 			<input name="submit" type="submit" value="<fmt:message key="common.btn.ok" />" class="submitBtn" />
	 			<input name="reset" type="button" onclick="location.href='country-code!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
	 		</td>
	   	</tr>
	 	</table>
	</div>
	</form>
</div>
<script type="text/javascript">
	$("form.validate").validate();
</script>