<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   
                      >  权限管理 
                      >  销售契约</div>
<form action="saveSaleContractAuth.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;权限添加 </h4>

	<table class="form-table">
		<tr>
				<th><label class="text">销售契约编码:</label></th>
				<td>
					<input type="text" name="saleContractAuth.saleContractCode" maxlength="40" dataType="string" isrequired="true"/>
				</td>
			</tr>
				
			<tr>
				<th><label class="text">管理人员:</label></th>
				<td>
					<select name="saleContractAuth.userId" id="proTypeCode_select" class="sear_item">
								<c:forEach var="item" items="${adminUser}">
									<option value="${item.userid}">${item.username}</option>
								</c:forEach>
					</select>
				</td>
			</tr>
 
		
	</table>
</div>

	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="javascript:history.go(-1)" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>                                
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
</script>