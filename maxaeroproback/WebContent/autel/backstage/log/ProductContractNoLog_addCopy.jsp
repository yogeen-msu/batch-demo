<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />   
                      >  日志管理
                      >  经销商换机
                      >  拷贝到新序列号</div>
<form action="product-contract-log!copyProduct.do"  class="validate" method="post" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;拷贝到新序列号 </h4>

	<table class="form-table">
		<tr>
			<th><label class="text">老序列号:</label></th>
			<td>
				<input type="text" name="oldProductSN"  dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th style="width: 140px"><label class="text">新序列号:</label></th>
			<td>
				<input type="text" name="newProductSN" dataType="string" isrequired="true" maxlength="40"/>
			</td>
		</tr>
		
	</table>
</div>

	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="javascript:history.go(-1)" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>                                
		</table>
	</div>
</form>

<script type="text/javascript">
	$("form.validate").validate();
</script>