<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  
                      > 日志管理 
                      > 产品更换销售契约</div>
<form method="post" action="product-contract-log!list.do">
	<div class="grid">
		<div class="toolbar" >
			 <h4 class="sear_tj"><div class="icon">&nbsp;</div> 产品更换销售契约列表</h4>
				<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="8">
					    <td align="right">
						<td width="80" align="right"> 序列号:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="productContractChangeLog.productSN" value="${productContractChangeLog.productSN}" />
						</td>
						<%-- <td width="111" align="right"> 老合约名称:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="productContractChangeLog.oldContract" value="${productContractChangeLog.oldContract}" />
						</td>
						<td width="111" align="right"> 新合约名称:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="productContractChangeLog.newContract" value="${productContractChangeLog.newContract}" />
						</td> --%>
						<td width="154">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
						<td align="right">
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>  
			
		</div>
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="100px">产品序列号</grid:cell>
				<%-- <grid:cell width="150px">oldMinSaleUnit</grid:cell> --%>
				<grid:cell width="180px">老合约名称</grid:cell>
				<grid:cell width="180px">新合约名称</grid:cell>
				<grid:cell width="100px">操作人</grid:cell>
				<grid:cell width="70px">操作时间</grid:cell>
				<grid:cell width="90px">操作IP</grid:cell>
				<%-- <grid:cell width="100px">邮件提醒</grid:cell> --%>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>
					${item.productSN }
				</grid:cell>
				<%-- <grid:cell>
					${fn:substring(item.oldMinSaleUnit,0,20) }
					<c:if test="${fn:length(item.oldMinSaleUnit) > 20 }">...</c:if>
				</grid:cell> --%>
				<grid:cell>
					${item.oldContract }
				</grid:cell>
				<grid:cell>
					${item.newContract }
				</grid:cell>
				<grid:cell>
					${item.operatorUser}
				</grid:cell>
				<grid:cell>
				    ${fn:substring(item.operatorTime,0,10) }
				</grid:cell>
				<grid:cell>
					${item.operatorIp}
				</grid:cell>
				<%-- <grid:cell>
					${item.sendEmailFlag}
				</grid:cell> --%>
			</grid:body>
		
		</grid:grid>
	</div>
</form>

<script type="text/javascript">
  $(function(){
	  $("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").each(function(i,obj){
				$(obj).attr("selected","selected");
			});		
			//重置单选
			$(".sear_table").find("input[type='radio'][value='-1']").each(function(i,obj){
				$(obj).attr("checked","checked");
			});
		});
  });
</script>