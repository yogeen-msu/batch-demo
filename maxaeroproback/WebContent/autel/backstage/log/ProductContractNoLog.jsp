<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  
                      > 日志管理 
                      > 经销商换机</div>
<form method="post" action="product-contract-log!noLoglist.do">
	<div class="grid">
		 <div class="toolbar" >
		  <h4 class="sear_tj"><div class="icon">&nbsp;</div>经销商换机列表</h4>
				<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="8">
						<td width="111" align="right"> autelID:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="productChangeNoLog.customerCode" value="${productChangeNoLog.customerCode}" />
						</td>
						<td width="111" align="right"> 老序列号:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="productChangeNoLog.oldProCode" value="${productChangeNoLog.oldProCode}" />
						</td>
						<td width="111" align="right"> 新序列号:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="productChangeNoLog.newProCode" value="${productChangeNoLog.newProCode}" />
						</td>
						<td width="50" align="right"></td>
						<td width="154">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
						<td width="50" align="right">
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div> 
			
			<div class="active">
					<h4>
						<span> <input name="" type="button"
							onclick="jump('product-contract-log!addCopy.do')"
							value="<fmt:message key="common.btn.add" />"
							class="search_but addorderpaystatuslog" />
						</span>经销商换机列表:
					</h4>
				</div>
				 
			</div>  
		
		
		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="150px">autelID</grid:cell>
				<grid:cell width="150px">老序列号</grid:cell>
				<grid:cell width="200px">新序列号</grid:cell>
				<grid:cell width="100px">软件有效期</grid:cell>
				<grid:cell width="80px">注册日期</grid:cell>
				<grid:cell width="100px">创建时间</grid:cell>
				<grid:cell width="100px">创建人</grid:cell>
				<grid:cell width="100px">创建IP</grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>
					${item.customerCode }
				</grid:cell>
				<grid:cell>
					${item.oldProCode }
				</grid:cell>
				<grid:cell>
					${item.newProCode }
				</grid:cell>
				<grid:cell>
					${fn:substring(item.validDate,0,10)}
				</grid:cell>
				<grid:cell>
					${fn:substring(item.regDate,0,10)}
				</grid:cell>
				<grid:cell>
					${fn:substring(item.createDate,0,10)}
				</grid:cell>
				<grid:cell>
					${item.createUser}
				</grid:cell>
				<grid:cell>
					${item.createIp}
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>

<script type="text/javascript">
  $(function(){
	  $("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").each(function(i,obj){
				$(obj).attr("selected","selected");
			});		
			//重置单选
			$(".sear_table").find("input[type='radio'][value='-1']").each(function(i,obj){
				$(obj).attr("checked","checked");
			});
		});
  });
  
  function jump(url) {
		var tmpForm = $("<form  method='post'></form>");
		$(tmpForm).attr("action", url);
		tmpForm.appendTo(document.body).submit();
  }
  
</script>
