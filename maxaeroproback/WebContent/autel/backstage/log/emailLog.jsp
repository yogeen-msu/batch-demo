<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  
                     > 日志管理
                     > 邮件日志列表</div>

<div class="grid">
<form method="post" action="email-log!list.do">
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div>邮件日志列表</h4>
				<div class="sear_table">
				<table width="100%">
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="8">
					    <td align="right">
						<td width="111" align="right"> 收件人:</td>
						<td width="154">
							<input type="text" style="width:150px"  name="emailLog.toUser" value="${emailLog.toUser}" />
						</td>
						<td width="154">
							<input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" />
							<input type="button" value=<fmt:message key="common.btn.reset" /> class="search_but" id="reset_but" />
						</td>
						<td align="right">
						</td>
					</tr>
					<tr height="5">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			
		</div>
</form>		
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="140px">发送人</grid:cell>
				<grid:cell width="140px">接收人</grid:cell>
				<grid:cell width="250px">标题</grid:cell>
				<grid:cell width="65px">创建时间</grid:cell>
				<grid:cell width="55px">发送次数</grid:cell>
				<grid:cell width="65px">更新时间</grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>
					${item.fromUser }
				</grid:cell>
				<grid:cell>
					${item.toUser}
				</grid:cell>
				<grid:cell>
					${item.title }
				</grid:cell>
				<grid:cell>
				    ${fn:substring(item.createDate,0,10) }
				</grid:cell>
				<grid:cell>
					${item.countNum}
				</grid:cell>
				<grid:cell>
				    ${fn:substring(item.updateDate,0,10) }
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>

<script type="text/javascript">
  $(function(){
	  $("#reset_but").click(function(){
			//重置文本框
			$(".sear_table").find("input[type='text']").each(function(i,obj){
				$(obj).val("");
			});		
			
			//重置下拉框
			$(".sear_table").find("select option[value='']").each(function(i,obj){
				$(obj).attr("selected","selected");
			});		
			//重置单选
			$(".sear_table").find("input[type='radio'][value='-1']").each(function(i,obj){
				$(obj).attr("checked","checked");
			});
		});
  });
</script>