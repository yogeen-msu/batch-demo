<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="marketman.salecontractman" /></title>
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<style type="text/css">

.right_tab {
    padding-right: 22px;
    width: auto;
}
leg_topri {
    height: 42px;
    line-height: 42px;
    width: 100%;
}
.sear {
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
    width: 100%;
    margin: 0;
    padding: 0
}

.sear_tj{
	background: url("../main/images/leg_12.gif") repeat-x scroll left top transparent;
    border-bottom: 1px solid #DDDDDD;
    height: 28px;
    line-height: 28px;
    padding-left: 20px;
}
</style>
</head>
<body>
	<form action="saveCopySaleContract.do" method="post" id="saleContractForm">
	    <input id = "selectGridId" type="hidden" value = "1"></input>
		<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.salecontractman" /> &gt; <fmt:message key="marketman.salecontractman.add" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj" style="font-size: 100%;font-weight: bold;margin: 0;">
					<img src="../main/images/leg_16.gif" width="14" height="16" style="line-height: 8px ;border: 0 none;vertical-align: middle;"/>&nbsp;<fmt:message key="saleContract.add.jsp.title" />
				</h4>
				<div class="sear_table">
					<table class="form-table" width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right">老的经销商编码:</td>
							<td width="154"><input name="oldSealerAutelId" id="name"
								type="text" class="time_in" /></td>
						</tr>
						<tr>
							<td width="111" align="right">新的经销商编码:</td>
							<td width="154"><input name="newSealerAutelId" id="name"
								type="text" class="time_in" /></td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>

			</div>
			<div style="padding-top: 20px; text-align: center;">
				<input id="submitBtn" type="submit" value="<fmt:message key="common.btn.finish" />" class="diw_but" /><input
					type="button" value="<fmt:message key="common.btn.return" />"
					onclick="jump('toSaleContractList.do')"
					class="diw_but" />
			</div>
		</div>
	</form>
	
	
	<script type="text/javascript">
	function jump(url){
		var e=document.createElement("a");
		e.href=url;
		document.body.appendChild(e);
		e.click();
	}
		
	</script>
</body>
</html>