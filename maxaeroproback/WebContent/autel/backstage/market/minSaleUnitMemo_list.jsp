<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.explainman" /></title>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/market/minSaleUnitMemo_list.js.jsp?ajax=yes"></script>
<script>
window.onload = function ()
{
	var oWin = document.getElementById("win");
	var oLay = document.getElementById("overlay");	
	var oBtn = document.getElementsByTagName("button")[0];
	var oClose = document.getElementById("close");
	var oH2 = oWin.getElementsByTagName("h2")[0];
	var bDrag = false;
	var disX = disY = 0;
	oBtn.onclick = function ()
	{
		oLay.style.display = "block";
		oWin.style.display = "block";
	};
	oClose.onclick = function ()
	{
		oLay.style.display = "none";
		oWin.style.display = "none";
		
	};
	oClose.onmousedown = function (event)
	{
		(event || window.event).cancelBubble = true;
	};
	oH2.onmousedown = function (event)
	{		
		var event = event || window.event;
		bDrag = true;
		disX = event.clientX - oWin.offsetLeft;
		disY = event.clientY - oWin.offsetTop;		
		this.setCapture && this.setCapture();		
		return false;
	};
	document.onmousemove = function (event)
	{	
		if (!bDrag) return;
		var event = event || window.event;
		var iL = event.clientX - disX;
		var iT = event.clientY - disY;
		var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
		var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
		iL = iL < 0 ? 0 : iL;
		iL = iL > maxL ? maxL : iL; 		
		iT = iT < 0 ? 0 : iT;
		iT = iT > maxT ? maxT : iT;
		
		oWin.style.marginTop = oWin.style.marginLeft = 0;
		oWin.style.left = iL + "px";
		oWin.style.top = iT + "px";		
		return false;
	};
	document.onmouseup = window.onblur = oH2.onlosecapture = function ()
	{
		bDrag = false;				
		oH2.releaseCapture && oH2.releaseCapture();
	};
};

function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
</head>

<body>
<form action="updateMinSaleUnitMemo.do" method="post" id="unitMemoForm">
		<input type="hidden" id="unitCode" name="minSaleUnit.code" value="${minSaleUnit.code }">
    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.explainman" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" /><fmt:message key="marketman.minusalenitman.explainman" />&nbsp;<fmt:message key="marketman.minusalenitman.explainman.manage" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
							<td width="154">&nbsp;&nbsp;${productType.name }</td>
							<td width="160" align="right"><fmt:message key="minSaleUnitMemo.list.jsp.sale.unit.code" />:</td>
							<td width="154">&nbsp;&nbsp;${minSaleUnit.code }</td>
							<td width="80" align="right"></td>
							<td width="300"><fmt:message key="memberman.customman.minsaleunitname" />:&nbsp;&nbsp;${minSaleUnit.name }</td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<div id="overlay"></div>
					<div id="win">
						<h2>
							<span id="close">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0">
								<tr>

									<td width="40%" align="right"><fmt:message key="memberman.complaintman.language" />:</td>
									<td><select name="language" id="language"
										class="act_zt_tc">
											<c:forEach items="${ languages}" var="language">
												<option value="${language.code }">${language.name }</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>

									<td width="40%" align="right"><fmt:message key="memberman.customman.minsaleunitname" />:</td>
									<td><input id="unitName" type="text" class="time_in_tc" />
									</td>
								</tr>
								
		<!-- 						<tr>

									<td width="40%" align="right">升级消息推送名称:</td>
									<td><input id="updateName" type="text" class="time_in_tc" />
									</td>
								</tr> -->
								
								<tr>

									<td width="40%" align="right"><fmt:message key="marketman.minusalenitman.sm" />:</td>
									<td><textarea cols="" rows="" style="width: 400px; height: 80px;" class="banb_tc"
											id="memo"></textarea>
									</td>
								</tr>
								<tr>

									<td width="40%" align="right"><fmt:message key="marketman.minusalenitman.xxsm" />:</td>
									<td><textarea cols="" rows="" style="width: 400px; height: 150px;" class="banb_tc"
											id="memoDetail"></textarea>
									</td>
								</tr>
								<tr>

									<td width="40%" align="right">&nbsp;</td>
									<td><input id="finsh" type="button" value="<fmt:message key="emption.add.jsp.button.save" />"
										class="diw_but_tc" />
									</td>
								</tr>
							</table>
						</div>
					</div>

					<h4>
						<span><button type="button" class="search_but"
								value="<fmt:message key="marketman.minusalenitman.tjsm" />"><fmt:message key="marketman.minusalenitman.tjsm" /></button>
						</span><fmt:message key="minSaleUnitMemo.list.jsp.Introduction.lists" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="memoTable">
				
					<tr class="title">
						<td width="10%"><fmt:message key="memberman.complaintman.language" /></td>
						<td width="20%"><fmt:message key="memberman.customman.minsaleunitname" /></td>
						<td width="20%"><fmt:message key="marketman.minusalenitman.sm" /></td>
						<td width="25%"><fmt:message key="marketman.minusalenitman.xxsm" /></td>
						<!-- <td width="15%">升级消息推送名称</td> -->
						<td width="10%"><fmt:message key="common.list.operation" /></td>

					</tr>
					
					<c:forEach items="${minSaleUnitMemos }" var="item">
						<tr>
							<td>${item.languageName}</td>
							<td>${item.name}</td>
							<td style="word-wrap:break-word;word-break:break-all;">${item.memo}</td>
							<td style="word-wrap:break-word;word-break:break-all;">${item.memoDetail}</td>
							<%-- <td>${item.updateName}</td> --%>
							<td class="caoz"><a memoId="${item.id}" languageCode="${item.languageCode}" languageName="${item.languageName}" onclick="delMemo(this);" href="#"><fmt:message key="marketman.saleconfigman.moveout" /></a></td>
						</tr>
					</c:forEach>

				</table>
				</div>
				<div style="padding-top: 20px;padding-left:40%;">
					<input id="sumbitBtn" type="button" value="<fmt:message key="minSaleUnitMemo.list.jsp.button.save" />" class="diw_but" />
					<input type="button" onclick="jump('toMinSaleUnitPage.do')" value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			
		</div>

	</form>
</body>
</html>


