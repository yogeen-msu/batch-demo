<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.softwareman" /></title>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
</head>

<body>
<form action="updateMinSaleUnitSoftwareDetail.do" method="post" id="detailForm">
		<input type="hidden" id="unitCode" name="minSaleUnit.code" value="${minSaleUnit.code }">
    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.minusalenit" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="marketman.minusalenit" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="160" align="right"><fmt:message key="minSaleUnitSoftwareDetail.list.jsp.sales.unit.code" />:</td>
							<td width="154">&nbsp;&nbsp;${minSaleUnit.code }</td>
							<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
							<td width="154">&nbsp;&nbsp;${productType.name }</td>
							<td width="160" align="right"><fmt:message key="marketman.minusalenitman.picture" />:</td>
							<td width="154">&nbsp;&nbsp;${minSaleUnit.picPath }</td>
							<td width="80" align="right"></td>
							<td></td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<%-- <div class="active">
					<h4>
						<fmt:message key="marketman.minusalenitman.sm" />
					</h4>
				</div> --%>
			</div>

	<%-- 		<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="memoTable">
				
					<tr class="title">
						<td width="20%"><fmt:message key="memberman.complaintman.language" /></td>
						<td width="20%"><fmt:message key="memberman.customman.minsaleunitname" /></td>
						<td width="30%"><fmt:message key="marketman.minusalenitman.sm" /></td>
						<td width="30%"><fmt:message key="marketman.minusalenitman.xxsm" /></td>
					</tr>
					
					<c:forEach items="${minSaleUnitMemos}" var="item">
						<tr>
							<td>${item.languageName}</td>
							<td>${item.name}</td>
							<td style="word-wrap:break-word;word-break:break-all;">${item.memo}</td>
							<td style="word-wrap:break-word;word-break:break-all;">${item.memoDetail}</td>
						</tr>
					</c:forEach>

				</table>
			</div> --%>
			
			<div class="sear">
				<div class="active">
					<h4>
						<fmt:message key="product.software" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="detailTable">
				
					<tr class="title">
						<td width="40%"><fmt:message key="common.list.code" /></td>
						<td width="40%"><fmt:message key="product.software.name" /></td>
						<td width="20%">可用版本数量</td>
					</tr>
					
					<c:forEach items="${softwareDetails}" var="item">
						<tr>
							<td>${item.softwareTypeCode}</td>
							<td>${item.softwareTypeName}</td>
							<td>${item.releaseFlag}</td>
						</tr>
					</c:forEach>

				</table>
			</div>
			
			<%-- <div class="sear">
				<div class="active">
					<h4>
						<fmt:message key="marketman.minusalenitman.zxjg" />
					</h4>
				</div>
			</div> --%>
			<%-- <div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="priceTable">
				
					<tr class="title">
						<td width="40%"><fmt:message key="syssetting.areaconfig.areaconfigname" /></td>
						<td width="60%"><fmt:message key="minSaleUnitPrice.list.jsp.unit.price" /></td>
					</tr>
					
					<c:forEach items="${prices}" var="item">
						<tr>
							<td>${item.areaName}</td>
							<td>${item.price}</td>
						</tr>
					</c:forEach>

				</table>
			</div> --%>
			
			<div style="padding-top: 20px;padding-left:40%;">
				<input type="button" onclick="history.go(-1);" value="<fmt:message key="common.btn.return" />" class="diw_but" />
			</div>
			
		</div>

	</form>
</body>
</html>


