<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.softwareman" /></title>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript">
	var areas = "${areas}";
</script>
</head>

<body>
<form action="updateSaleConfig.do" method="post" id="configForm">
    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.saleconfigman.info" />&gt; <fmt:message key="marketman.saleconfigman.info" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="marketman.saleconfigman.info" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="marketman.saleconfigman.name" />:</td>
							<td width="154">&nbsp;&nbsp;${saleConfig.name }</td>
							<td width="160" align="right"><fmt:message key="product.producttypevo" />:</td>
							<td width="154">&nbsp;&nbsp;${saleConfig.proTypeName }</td>
							<td width="160" align="right"><fmt:message key="marketman.saleconfigman.picture" />:</td>
							<td width="154">&nbsp;&nbsp;${saleConfig.picPath }</td>
							<td width="80" align="right"></td>
							<td></td>
						</tr>
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4>
						<fmt:message key="marketman.saleconfigman.sm" />
					</h4>
				</div>
			</div>

			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="configTable">
				
					<tr class="title">
						<td width="20%"><fmt:message key="memberman.complaintman.language" /></td>
						<td width="30%"><fmt:message key="saleConfig_list.jsp.saleconfig.name" /></td>
						<td width="50%"><fmt:message key="marketman.saleconfigman.sm" /></td>
					</tr>
					
					<c:forEach items="${saleConfigMemos }" var="item">
						<tr>
							<td>${item.languageName}</td>
							<td>${item.name}</td>
							<td>${item.memo}</td>
						</tr>
					</c:forEach>

				</table>
			</div>
			
			<div class="sear">
				<div class="active">
					<h4>
						<fmt:message key="saleConfig.update.jsp.sale.unit.list" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="configTable">
				
					<tr class="title">
						<td width="30%"><fmt:message key="common.list.code" /></td>
						<td width="70%"><fmt:message key="memberman.customman.minsaleunitname" /></td>
					</tr>
					
					<c:forEach items="${minSaleUnitConfigVos }" var="item">
						<tr>
							<td>${item.code }</td>
							<td>
								<a href="${ctx}/autel/market/showMinSaleUnit.do?minSaleUnit.code=${item.code}" style="text-decoration: none;color: rgb(215, 18, 0);">
									${item.name }
								</a>
							</td>
						</tr>
					</c:forEach>

				</table>
			</div>
			<div style="padding-top: 20px;padding-left:40%">
				<input type="button" onclick="history.go(-1)" value="<fmt:message key="common.btn.return" />" class="diw_but" />
			</div>
		</div>
		

	</form>
</body>
</html>


