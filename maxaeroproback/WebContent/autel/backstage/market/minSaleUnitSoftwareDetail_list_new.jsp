<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.softwareman" /></title>
<link href="../main/css/table.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/market/jquery.checktree.js"></script>
<script type="text/javascript" src="../js/market/minSaleUnitSoftwareSelect.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../js/market/minSaleUnitSoftwareDetail_list_new.js.jsp?ajax=yes"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../core/admin/auth/checktree.css" />
<script type="text/javascript" src="${ctx}/autel/js/market/checkboxCodeTwo.js"></script>
<script>
var forward= '<fmt:message key="common.info.jump" />';

function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>

</head>

<body>
<input type="hidden" name="selectedcodes2" id="selectedcodes2" value="" />
<form action="updateMinSaleUnitSoftwareDetail.do" method="post" id="detailForm">
		<input type="hidden" id="unitCode" name="minSaleUnit.code" value="${minSaleUnit.code }">
    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.softwareman" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="minSaleUnitSoftwareDetail.list.jsp.title" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
							<td width="200">&nbsp;&nbsp;${productType.name }</td>
							<td width="300"><fmt:message key="minSaleUnitSoftwareDetail.list.jsp.sales.unit.code" />:&nbsp;&nbsp;${minSaleUnit.code }</td>
							<td width="40" align="right"></td>
							<td width="300"><fmt:message key="memberman.customman.minsaleunitname" />:&nbsp;&nbsp;${minSaleUnit.name }</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="product.software.name" />:</td>
							<td width="200">&nbsp;&nbsp;<input id="softwareTypeName" name="softwareTypeName" type="text" class="time_in" />
							</td>
							<td width="300"><input type="button"
								value="<fmt:message key="common.btn.search" />" class="search_but" onclick="querySoft()" /></td>
							<td width="80" align="right"></td>
							<td></td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4>
						<span><button type="button" class="search_but" id="selectSoftDlg"><fmt:message key="marketman.minusalenitman.tjrj" /></button>
						</span><fmt:message key="product.software" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd">
					<tr class="title2">
						<td width="70">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="allCheckbox2" />&nbsp;&nbsp;全选
						</td>
						<td width="20%"><fmt:message key="common.list.code" /></td>
						<td><fmt:message key="product.software.name" /></td>
						<td width="20%"><fmt:message key="common.list.operation" /></td>
					</tr>
				</table>
				<table width="100%" border="0" bordercolor="#dddddd" id="detailTable">					
					<c:forEach items="${softwareDetails }" var="item">
						<tr>
							<td width="70">
								<input type="checkbox" name="checkboxcode2" saveType="1" detailId="${item.id}" value="${item.softwareTypeCode}" softname="${item.softwareTypeName}"/>
							</td>
							<td width="20%">${item.softwareTypeCode}</td>
							<td>${item.softwareTypeName}</td>
							<td class="caoz" width="20%"><a class="softwaretypeclass" detailId="${item.id}" softwareTypeCode="${item.softwareTypeCode}" softwareTypeName="${item.softwareTypeName}" onclick="delDetail(this);" href="###"><fmt:message key="marketman.saleconfigman.moveout" /></a></td>
						</tr>
					</c:forEach>
				</table>
				<table width="100%" border="0" bordercolor="#dddddd">
					<tr class="title2">
						<td align="left">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="allCheckbox2" />&nbsp;&nbsp;全选
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input class="dellsofts" type="button" value="删除选中软件 "></input>
						</td>
					</tr>
				</table>
				</div>
				<div style="padding-top: 20px;padding-left:40%;">
					<input id="sumbitBtn" type="button" value="<fmt:message key="emption.add.jsp.button.save" />" class="diw_but" /><input
						type="button" onclick="jump('toMinSaleUnitPage.do')" value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			
		</div>

	</form>

	<div id="selectSoftwareDlg" style="max-height: 400px;"></div>

	<script type="text/javascript">
		var treeJson;
		$(function() {
			$.ajax({
				url:'../../core/admin/productSoftwareConfig!jsonTree.do?productSoftwareConfigCode=0&&ajax=yes',
				type:"get",
				async: false,
				success :function(data){
					treeJson = eval(eval(data));
				}
			});
			
			Software.init();
		});
	</script>
</body>
</html>


