<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="marketman.salecontractman" /></title>
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />

<script type="text/javascript" src="../js/market/saleDialog.js"></script>

<script type="text/javascript" src="../js/market/saleContract.js.jsp?ajax=yes"></script>
<script type="text/javascript">
var chooseModel= '<fmt:message key="saleDialog.choose.product" />';
var chooseSealer= '<fmt:message key="saleDialog.choose.sealer" />';
var chooseConfig= '<fmt:message key="saleDialog.choose.config" />';
var forward= '<fmt:message key="common.info.jump" />';

</script>

<style type="text/css">

.right_tab {
    padding-right: 22px;
    width: auto;
}
leg_topri {
    height: 42px;
    line-height: 42px;
    width: 100%;
}
.sear {
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
    width: 100%;
    margin: 0;
    padding: 0
}

.sear_tj{
	background: url("../main/images/leg_12.gif") repeat-x scroll left top transparent;
    border-bottom: 1px solid #DDDDDD;
    height: 28px;
    line-height: 28px;
    padding-left: 20px;
}
</style>
</head>
<body>
	<form action="addSaleContract.do" method="post" id="saleContractForm">
	    <input id = "selectGridId" type="hidden" value = "1"></input>
		<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.salecontractman" /> &gt; <fmt:message key="marketman.salecontractman.add" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj" style="font-size: 100%;font-weight: bold;margin: 0;">
					<img src="../main/images/leg_16.gif" width="14" height="16" style="line-height: 8px ;border: 0 none;vertical-align: middle;"/>&nbsp;<fmt:message key="saleContract.add.jsp.title" />
				</h4>
				<div class="sear_table">
					<table class="form-table" width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="marketman.salecontractman.name" />:</td>
							<td width="154"><input name="saleContract.name" id="name"
								type="text" class="time_in" /></td>
						</tr>
						<tr>
							<td width="111" align="right"><fmt:message key="memberman.sealer.type" />:</td>
							<td><select name="saleContract.sealerTypeCode"
								id="sealerTypeCode" class="act_zt">
									<option value="">-- <fmt:message key="select.option.select" /> --</option>
									<c:forEach items="${sealerTypes }" var="item">
										<option value="${item.code }">${item.name }</option>
									</c:forEach>
							</select>
							</td>
						</tr>
						<tr height="38">

							<td width="111" align="right"><fmt:message key="memberman.sealer.jxs" />:</td>
							<td width="154">
								<input id="sealerCode" type="hidden" name="saleContract.sealerCode">
								
								<input id="sealerCodeName" type="text" class="time_in" readonly="readonly"><input type="button" value="<fmt:message key="marketman.saleconfigman.check" />" id="selectSealerCodeDlg">
							</td>
							
						</tr>
						<tr>
							<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
							<td width="284">
								<input id="proTypeCode" type="hidden" name="saleContract.proTypeCode">
								<input id="proTypeCodeName" type="text" class="time_in" readonly="readonly"><input type="button" value="<fmt:message key="marketman.saleconfigman.check" />" id="selectSaleProductDlg">
							</td>
						</tr>
						<tr>
							<td width="111" align="right"><fmt:message key="marketman.salecontractman.standardsaleconfig" />:</td>
							<td width="284">
							<input id="saleCfgCode" type="hidden" areaCfgCode="" name="saleContract.saleCfgCode">
							<input id="saleCfgCodeName" type="text" class="time_in" readonly="readonly"><input type="button" value="<fmt:message key="marketman.saleconfigman.check" />" id="selectConfigDlg">
							</td>
						</tr>
						<tr height="38">

							<td width="111" align="right"><fmt:message key="marketman.salecontractman.maxsaleconfig" />:</td>
							<td width="284">
							<input id="maxSaleCfgCode" type="hidden" areaCfgCode="" name="saleContract.maxSaleCfgCode">
							<input id="maxSaleCfgCodeName" type="text" class="time_in" readonly="readonly"><input type="button" value="<fmt:message key="marketman.saleconfigman.check" />" id="selectMaxConfigDlg">
							</td>
							
						</tr>
						
						<tr height="38">
							<td width="111" align="right"><fmt:message key="syssetting.languageconfig" />:</td>
							<td><select name="saleContract.languageCfgCode"
								id='languageCfgCode' class="act_zt">
									<option value="">-- <fmt:message key="select.option.select" /> --</option>
									<c:forEach items="${languageConfigs }" var="item">
										<option value="${item.code }">${item.name }</option>
									</c:forEach>
							</select>
							</td>
						</tr>
						
						<tr height="38">
							<td width="111" align="right"><fmt:message key="syssetting.areaconfig" />:</td>
							<td><select name="saleContract.areaCfgCode" id='areaCfgCode'
								class="act_zt">
									<option value="">-- <fmt:message key="select.option.select" /> --</option>
									<c:forEach items="${areaConfigs }" var="item">
										<option value="${item.code }">${item.name }</option>
									</c:forEach>
							</select>
							</td>
						</tr>
						
						<tr height="38">

							<td width="111" align="right"><fmt:message key="marketman.salecontractman.outfactoryreletprice" />:</td>
							<td><input name="saleContract.reChargePrice"
								id='reChargePrice' type="text" class="time_in" />
							</td>
						</tr>
						
						<tr height="38">
							<td width="111" align="right"><fmt:message key="marketman.js.salecontractman.message8" />:</td>
							<td><input name="saleContract.upMonth"
								id='upMonth' type="text" class="time_in" />
							</td>
						</tr>
						
						<tr height="38">
							<td width="111" align="right"><fmt:message key="marketman.js.salecontractman.message9" />:</td>
							<td><input name="saleContract.warrantyMonth"
								id='warrantyMonth' type="text" class="time_in" />
							</td>
						</tr>
						
						<tr height="38">

							<td width="111" align="right"><fmt:message key="marketman.salecontractman.contracttime" />:</td>
							<td>
								<input id="contractDate" name="saleContract.contractDate"
								type="text" class="dateinput"
								readonly="readonly" size="12"/>
							</td>
						</tr>
						
						<tr height="38">

							<td width="111" align="right"><fmt:message key="marketman.salecontractman.expiretime" />:</td>
							<td><input id="expirationTime" name="saleContract.expirationTime"
								type="text" class="dateinput"
								readonly="readonly" size="12"/></td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>

			</div>
			<div style="padding-top: 20px; text-align: center;">
				<input id="submitBtn" type="button" value="<fmt:message key="common.btn.finish" />" class="diw_but" /><input
					type="button" value="<fmt:message key="common.btn.return" />"
					onclick="jump('toSaleContractList.do')"
					class="diw_but" />
			</div>
		</div>
	</form>
	
	<div id="selectProductDlg"></div>
	
	<div id="selectConfig"></div>
	
	<div id="selectMaxConfig"></div>
	
	<div id="sealerDlg"></div>
	
	<script type="text/javascript">
	function jump(url){
		var e=document.createElement("a");
		e.href=url;
		document.body.appendChild(e);
		e.click();
	}
		$(function(){
			SaleContract.init();
		});
		
		
		function initProduct(){
			$(".selectProductItem").click(function(){
				var productCode = $(this).attr("selectCode");		//编号
				var proTypeCodeName = $(this).attr("selectName");		//产品型号
				
				
				$("#proTypeCode").val(productCode);
				$("#proTypeCodeName").val(proTypeCodeName);
				Cop.Dialog.close("selectProductDlg");
			});
		}
		
		function initConfig(){
			$(".selectConfigItem").click(function(){
				var selectCode = $(this).attr("selectCode");		
				var selectName = $(this).attr("selectName");		
				var areaCfgCode = $(this).attr("areaCfgCode");
				
				$("#saleCfgCode").val(selectCode);
				$("#saleCfgCode").attr("areaCfgCode",areaCfgCode);
				$("#saleCfgCodeName").val(selectName);
				Cop.Dialog.close("selectConfig");
			});
		}
		
		function initSealer(){
			$(".selectSealerItem").click(function(){
				var selectCode = $(this).attr("selectCode");		
				var selectName = $(this).attr("selectName");		
				
				$("#sealerCode").val(selectCode);
				$("#sealerCodeName").val(selectName);
				Cop.Dialog.close("sealerDlg");
			});
		}
		
		function initMaxConfig(){
			$(".selectMaxConfigItem").click(function(){
				var selectCode = $(this).attr("selectCode");		
				var selectName = $(this).attr("selectName");		
				var areaCfgCode = $(this).attr("areaCfgCode");
				
				$("#maxSaleCfgCode").val(selectCode);
				$("#maxSaleCfgCode").attr("areaCfgCode",areaCfgCode);
				$("#maxSaleCfgCodeName").val(selectName);
				Cop.Dialog.close("selectMaxConfig");
			});
		}
		
		setInterval('initSealer()',100);
		setInterval('initProduct()',100);
		setInterval('initConfig()',100);
		setInterval('initMaxConfig()',100);
		
	</script>
</body>
</html>