<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.softwareman" /></title>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$("#sumbitBtn").click(function(){
		
		if($("#productName").val()==""){
			alert("<fmt:message key='sale.product.addjsp.name.msg' />");
			return;
		}
		if($("#productImg").val()==""){
			alert("<fmt:message key='sale.product.addjsp.img.msg' />");
			return;
		}
		
		var a=[];
		$("#picTable tr").each(function(i){
			if($(this).attr("id")=="picPathTR"){
			 var path=  $($(this).find("input[name='picPath']")).val();
			 var code=  $($(this).find("select[name='languageSelect']")).val();
			 a.push({ "code":"", "languageCode": code,"languageName":"","picId":"",picPath:path}); 
			}
		});
		
		var proCodeStr="";
		$("#productTable tr").each(function(i){
			if($(this).attr("id")=="ProductTR"){
			 var code=  $($(this).find("td")[0]).html();
			 proCodeStr=code+","+proCodeStr;
			}
		});
		$("#jsonStr").val(JSON.stringify(a));
		$("#proCodeStr").val(proCodeStr);
		if(a.length==0){
			alert("<fmt:message key='sale.product.addjsp.serial.msg' />");
			return;
		}
		if(proCodeStr==""){
			alert("<fmt:message key='sale.product.addjsp.product.msg' />");
			return;
		}
		$("#configForm").submit();
	});
	
	$("#addProductTr").click(function(){
		
		var $win_complaint= $("#win_complaint");;
		var $overlay = $("#overlay");
		var $close = $("#close");
		 $win_complaint.show();
		 $overlay.show();
		
	});
	
	$("#finsh").click(function(){
		var unitCode = $("#unitCode").val();
		//éæ©çoption
		var $selectOption = $("#productType option:selected");
		var size = $selectOption.size();
		if(size ==0){
			alert("<fmt:message key='marketman.js.xzSoftwaretype' />");
			return false;
		}
		$selectOption.each(function(){
			var value= $(this).val();
			var text = $(this).text();
			var html="<tr id='ProductTR'>";
			html += "<td>"+value+"</td>";
			html += "<td>"+text+"</td>";
			html +="<td class='caoz'>";
			html +="<a href='#' onclick='cancel(this);'>"+"<fmt:message key='common.list.del' />"+"</a></td>";
			html +="</tr>";
			$("#productTable").append(html);
		});
		
		var oWin = $("#win_complaint");
		var oLay = $("#overlay");
		oWin.hide();
		oLay.hide();
		$selectOption.remove();
	});
	
	
	$("#addPicTr").click(function(){
		var $table = $("#picTable");
		$.ajax({
			url:'getLanguageJson.do?ajax=yes',
			type:"post",
			
			dataType:'JSON',
			success :function(data){
				if(data != null){
					var selectHtml = buildSelect(data,"");
					if(selectHtml!=0){
					var tr = "<tr id='picPathTR' languageCode='' picId=''>";
					tr += "<td>"+selectHtml+"</td>";
					tr += "<td>";
					tr += "<input type='text' class='text-left' id='picPath' name='picPath' style='width:80%;' value='' languageName='' picPath='' maxlength='1000'/>";  
					tr += "</td>";
					tr += "<td class='caoz'>";
					tr += "&nbsp;&nbsp;";
					tr += "<a href='#' onclick='cancel(this);'>"+"<fmt:message key='common.list.del' />"+"</a>";
					tr += "</td>";
					tr += "</tr>";
					$table.append(tr);
					}
				}
				
			},
			error :function(){
				alert("<fmt:message key='common.js.system.error' />");
			}
		});
	});
});

function cancel(event){
	var $tr = $(event).closest("tr");
	$tr.remove();
}

function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
function buildSelect(data,option){
	var jsonData = eval(data);
	var select = "<select id='languageSelect' class='act_zt' name='languageSelect'>";
	if(option != ""){
		select += option;
	}
	var code="";
	$("#picTable tr").each(function(i){
		if($(this).attr("id")=="picPathTR"){
		 code=$($(this).find("select[name='languageSelect']")).val()+","+code;
		}
	});
	var num=0;
	for(var i = 0 ; i < jsonData.length; i ++){
		if(code.indexOf(jsonData[i].code)==-1){
		select += "<option value='"+jsonData[i].code+"'>"+jsonData[i].name+"</option>";
		num++;
		}
	}
	select += "</select>";
	if(num==0){
		return num;
	}
	return select;
}

function colseWin()
{
	$("#win_complaint").hide();
	$("#overlay").hide();
}

</script>
</head>

<body>
<form action="save.do" class="validate" method="post" id="configForm">
<input type="hidden" name="product.serialPath" id="jsonStr" value=""/>
<input type="hidden" name="product.proTypeCodeStr" id="proCodeStr" value=""/>
    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="sale.product.menu.title" />&gt; <fmt:message key="sale.product.addjsp.add" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="sale.product.addjsp.add" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<%-- <tr height="38">
							<td width="111" align="right"><fmt:message key="sale.product.addjsp.product" />:</td>
							<td>&nbsp;&nbsp;
							<select class="act_zt" name="product.proTypeCode">
								<option value="">--<fmt:message key="select.option.select" />--</option>
								<c:forEach items="${productTypes }" var="item">
								<option value="${item.code}" <c:if test="${configSearch.proTypeCode == item.code}"> selected="selected"</c:if>>${item.name}</option>
								</c:forEach>
							</select>
							</td>
							<td></td>
						</tr> --%>
						<tr height="38">
							<td width="150" align="right"><fmt:message key="sale.product.addjsp.productName" />:</td>
							<td>&nbsp;&nbsp;<input id="productName" name="product.name" style="width: 300px" type="text" dataType="string" isrequired="true"/></td>
							<td></td>
						</tr>
						
						<tr height="38">
							<td width="150" align="right"><fmt:message key="sale.product.addjsp.productImge" />:</td>
							<td>&nbsp;&nbsp;<input id="productImg" name="product.picPath" style="width: 300px" type="text" dataType="string" isrequired="true"/></td>
							<td></td>
						</tr>
						
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
					<div class="active">
					<div id="overlay"></div>
					<h4>
						<span><button id="addProductTr" type="button" class="search_but"><fmt:message key="saleConfig.add.jsp.button.add" /></button>
						</span><fmt:message key="sale.product.addjsp.add.product"/>
					</h4>
				</div>
				
				<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="productTable" style="border-top: 1px solid #DDDDDD;border-left: 0px solid #DDDDDD;">
				
					<tr class="title">
						<td width="20%"><fmt:message key="common.list.code" /></td>
						<td width="50%"><fmt:message key="sale.product.addjsp.product" /></td>
						<td width="30%"><fmt:message key="common.list.operation" /></td>
					</tr>
					
				</table>
				</div>
				
				
				<div class="active">
					<div id="overlay"></div>
					<h4>
						<span><button id="addPicTr" type="button" class="search_but"><fmt:message key="saleConfig.add.jsp.button.add" /></button>
						</span><fmt:message key="sale.product.addjsp.serial.add" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="picTable">
				
					<tr class="title">
						<td width="20%"><fmt:message key="memberman.complaintman.language" /></td>
						<td width="50%"><fmt:message key="product.producttypevo.serialPicPath" /></td>
						<td width="30%"><fmt:message key="common.list.operation" /></td>
					</tr>
					
				</table>
				</div>
				<div style="padding-top: 20px;padding-left:40%">
					<input id="sumbitBtn" type="button" value="<fmt:message key="saleConfig.add.jsp.button.save" />" class="diw_but" /><input
						type="button" onclick="jump('list.do')" value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			
		</div>

<div id="win_complaint" style="top:30%" class="active">
	   	<h2><span id="close" onclick="javascript:colseWin()">×</span></h2>
		<div class="tc_tab">
							<table width="100%" border="0">
								<tr>

									<td width="40%" align="right"><fmt:message key="sale.product.addjsp.list.product" />:&nbsp;&nbsp;</td>
									<td><select name="productType" id="productType"
										 multiple="multiple" size="15" style="width: 150px;">
											<c:forEach items="${productTypes}" var="detail">
												<option value="${detail.code }">${detail.name }</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>

									<td width="40%" align="right">&nbsp;</td>
									<td><input id="finsh" type="button" value="<fmt:message key="emption.add.jsp.button.save" />"
										class="diw_but_tc" />
									</td>
								</tr>
							</table>
						</div>
</div>

	</form>
</body>
</html>


