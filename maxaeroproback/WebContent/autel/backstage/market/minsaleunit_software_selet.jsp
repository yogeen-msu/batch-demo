<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="${ctx}/autel/js/market/checkboxCode.js"></script>
<form method="post" id="pageSelectForm" action="toSearchPtDlg.do">
	<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
	<input type="hidden" name="excludecodes" value="${excludecodes }" value="" />
	<input type="hidden" name="minSaleUnit.code" value="${minSaleUnit.code }" />
	<div class="grid">
		<div class="toolbar" style="padding: 5px">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="product.software.name" />:&nbsp;&nbsp;
			<input type="text" name="softwareType.name" value="${softwareType.name }" />&nbsp;&nbsp;
			<input type="button" class="search_but"  name="submit" onclick="pageQuery()" value="<fmt:message key="common.btn.search" />" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" class="search_but"  name="submit" onclick="clickok();" value="<fmt:message key="common.btn.ok" />" />
		</div>
		
		<table width="100%" border="0" bordercolor="#dddddd" id="detailTable">
		
			<tr class="title2">
				<td width="30%"><fmt:message key="common.list.code" /></td>
				<td width="58%"><fmt:message key="product.software.name" /></td>
				<td width="12%" align="right">
					全选&nbsp;&nbsp;<input type="checkbox" name="allCheckbox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			
			<c:forEach items="${softwareTypes }" var="item">
				<tr>
					<td>${item.code}</td>
					<td>${item.name}</td>
					<td align="right">
						<input type="checkbox" name="checkboxcode" value="${item.code}" softname="${item.name}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</c:forEach>
			<tr class="title2">
				<td colspan="3" align="right">
					<input type="button" class="search_but"  name="submit" onclick="clickok();" value="<fmt:message key="common.btn.ok" />" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					全选&nbsp;&nbsp;<input type="checkbox" name="allCheckbox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
		</table>
	</div>
</form>