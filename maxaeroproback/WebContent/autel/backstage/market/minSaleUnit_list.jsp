<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="marketman.minusalenitman" /></title>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/market/minSaleUnit_list.js.jsp?ajax=yes"></script>
	<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
</head>
<body>
<form action="toMinSaleUnitPage.do" method="post" id="minSaleUnitForm">
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.minusalenitman" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="marketman.minusalenitman" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td width="154">
						<select class="act_zt" name="minSaleUnitSearch.productTypeCode">
						<option value="">--<fmt:message key="select.option.select" />--</option>
							<c:forEach items="${productTypes }" var="item">
								<option value="${item.code}" <c:if test="${minSaleUnitSearch.productTypeCode == item.code}"> selected="selected"</c:if>>${item.name}</option>
							</c:forEach>
						</select>
						</td>
						<td width="154" align="right">
						<fmt:message key="memberman.customman.minsaleunitname" />:
						</td>
						<td>&nbsp;<input name="minSaleUnitSearch.name" class="time_in" type="text" value="${minSaleUnitSearch.name }"></td>
						<td width="160" align="right"><input type="submit"
							value="<fmt:message key="common.btn.search" />" class="search_but" /></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
				</table>
			</div>
			<div class="active">
				<h4>
					<span><input type="button" value="<fmt:message key="marketman.promotion.addminsaleunit" />" onclick="jump('toAddMinSaleUnit.do')"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp; 
							<input name="" type="button" value="刷新最小销售单位"  onclick="location.href='refreshMin.do'" class="search_but refreshtpack" />
						
						<input name=""
						type="button" value="Excel批量导入" class="search_but" style="display: none;"/> </span><fmt:message key="marketman.minusalenit" />
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell><fmt:message key="common.list.code" /></grid:cell>
					<grid:cell><fmt:message key="memberman.customman.minsaleunitname" /></grid:cell>
					<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
					<grid:cell><fmt:message key="marketman.minusalenitman.picture" /></grid:cell>
					<grid:cell><fmt:message key="marketman.minusalenitman.explainman" /></grid:cell>
					<grid:cell><fmt:message key="marketman.minusalenitman.softwareman" /></grid:cell>
					<grid:cell><fmt:message key="marketman.minusalenitman.priceman" /></grid:cell>
					<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item" >
					<td>${item.code }</td>
					<td>${item.name }</td>
					<td>${item.productTypeName}</td>
					<td>${item.picPath}</td>
					<td><input type="button" onclick="jump('toMinSaleUnitMemo.do?minSaleUnit.code=${item.code }&minSaleUnit.name=${item.name}')" class="search_but" value="<fmt:message key="marketman.minusalenitman.explainman.manage" />"></td>
					<td><input type="button" onclick="jump('toMinSaleUnitSoftwareDetail.do?minSaleUnit.code=${item.code }&minSaleUnit.name=${item.name}')" class="search_but" value="<fmt:message key="marketman.minusalenitman.softwareman.manage" />"></td>
					<td><input type="button" onclick="jump('toMinSaleUnitPrice.do?minSaleUnit.code=${item.code }&minSaleUnit.name=${item.name}')" class="search_but" value="<fmt:message key="marketman.minusalenitman.priceman.manage" />"></td>
					<td class="caoz"><a href="toUpdateMinSaleUnit.do?minSaleUnit.code=${item.code }&minSaleUnit.name=${item.name}"><fmt:message key="common.list.mod" /></a>&nbsp;&nbsp;
					<%-- <a code="${item.code }" name="delMinSaleUnit" href="#"><fmt:message key="common.list.del" /></a> --%>
					</td>
				</grid:body>
				</grid:grid>
		</div>
	</div>
	</form>
</body>
</html>