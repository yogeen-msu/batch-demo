<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="marketman.salecontractman" /></title>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/market/saleContract_list.js.jsp?ajax=yes"></script>
	<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
</head>
<body>
	<form action="toSaleContractList.do" method="post" id="saleContractForm">
		<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.salecontractman" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="marketman.salecontractman" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="141" align="right"><fmt:message key="marketman.salecontractman.name" />:</td>
							<td width="154"><input name="saleContractvo.name" type="text" class="time_in" value="${saleContractvo.name }"/>
							</td>
							<td width="101" align="right"><fmt:message key="memberman.sealer.jxsmc" />:</td>
							<td width="154"><input name="saleContractvo.sealerName" type="text" class="time_in" value="${ saleContractvo.sealerName}"/>
							</td>
							<td width="121" align="right"><fmt:message key="product.producttypevo" />:</td>
							<td>
							<select class="act_zt" name="saleContractvo.proTypeCode">
						        <option value="">--<fmt:message key="select.option.select" />--</option>
							    <c:forEach items="${productTypes }" var="item">
								<option value="${item.code}" <c:if test="${saleContractvo.proTypeCode == item.code}"> selected="selected"</c:if>>${item.name}</option>
							    </c:forEach>
						   </select>
							</td>
						</tr>
						<tr height="38">

							<td width="141" align="right"><fmt:message key="syssetting.areaconfig.areaconfigname" />:</td>
							<td width="154"><input name="saleContractvo.areaName" type="text" class="time_in" value="${ saleContractvo.areaName}"/>
							</td>
							<td width="101" align="right"><fmt:message key="syssetting.languageconfig" />:</td>
							<td width="154"><input name="saleContractvo.languageCfgName" type="text" class="time_in" value="${saleContractvo.languageCfgName }"/>
							</td>
							<td width="121" align="right"><fmt:message key="marketman.salecontractman.standardsaleconfig" />:</td>
							<td><input name="saleContractvo.saleName" type="text" class="time_in" value="${ saleContractvo.saleName}"/>
							</td>
						</tr>
						<tr height="38">

							<td width="141" align="right"><fmt:message key="marketman.salecontractman.maxsaleconfig" />:</td>
							<td><input name="saleContractvo.maxSaleName" type="text" class="time_in" value="${saleContractvo.maxSaleName }"/>
							</td>
							<td align="right"></td>
							<td>
							</td>
							<td></td>
							<td><input type="submit" value="<fmt:message key="common.btn.search" />"
								class="search_but" /></td>
						</tr>
						<tr height="5">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4>
					<span><input type="button" value="复制销售契约" onclick="jump('toCopySalContract.do')" class="search_but" /></span>
					
					<span><input type="button" value="<fmt:message key="marketman.salecontractman.add" />" onclick="jump('toAddSalContract.do')" class="search_but" /></span><fmt:message key="marketman.salecontractman.sale.contract" />&nbsp;&nbsp;&nbsp;&nbsp;</h4>
				</div>
			</div>
			<div class="grid">
				<grid:grid from="webpage">
					<grid:header>
						<grid:cell><fmt:message key="common.list.code" /></grid:cell>
						<grid:cell><fmt:message key="marketman.salecontractman.name" /></grid:cell>
						<grid:cell><fmt:message key="memberman.sealer.jxsmc" /></grid:cell>
						<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
						<grid:cell><fmt:message key="syssetting.areaconfig" /></grid:cell>
						<grid:cell><fmt:message key="syssetting.languageconfig" /></grid:cell>
						<%-- <grid:cell><fmt:message key="marketman.salecontractman.standardsaleconfig" /></grid:cell>
						<grid:cell><fmt:message key="marketman.salecontractman.maxsaleconfig" /></grid:cell> --%>
						<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
					</grid:header>
					<grid:body item="item">
							<td>${item.code}</td>
							<td>${item.name}</td>
							<td>${item.sealerName}</td>
							<td>${item.productTypeName}</td>
							<td>${item.areaName}</td>
							<td>${item.languageCfgName}</td>
							<%-- <td>${item.saleName}</td>
							<td>${item.maxSaleName}</td> --%>
						
						<td>
							<a href="toUpdateSaleContract.do?saleContract.code=${item.code}" ><fmt:message key="common.list.mod" /></a>&nbsp;&nbsp;<a href="###" name="delSaleContract" saleCode="${item.code}"><fmt:message key="common.list.del" /></a>
						</td>
						
						<%-- <c:if test="${adminFlag=='N'}">
							<td>
							<a href="toViewSaleContract.do?saleContract.code=${item.code}"><fmt:message key="order.view.name" /></a>
							</td>
						</c:if> --%>
						
					</grid:body>
				</grid:grid>
			</div>
		</div>
	</form>
</body>
</html>