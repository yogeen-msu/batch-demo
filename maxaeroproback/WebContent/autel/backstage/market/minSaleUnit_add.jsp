<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="marketman.minusalenitman" /></title>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>

</head>
<body>
<form action="addMinSaleUnit.do" class="validate" method="post">
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="marketman.minusalenitman" />&gt; <fmt:message key="marketman.promotion.addminsaleunit" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="minSaleUnit.add.jsp.title" />
			</h4>
			<div class="sear_table">
				<table class="form-table" width="100%" border="0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						<td width="170" align="right"><fmt:message key="product.producttypevo" />:</td>
						<td><select class="act_zt" name="minSaleUnit.proTypeCode">
								<c:forEach items="${productTypes }" var="item">
									<option value="${item.code}">${item.name}</option>
								</c:forEach>
						</select>
						</td>
					</tr>
					<tr height="5">
						<td align="right"><fmt:message key="memberman.customman.minsaleunitname" />:</td>
						<td><input name="minSaleUnitMemo.name" type="text" class="time_in"  dataType="string" isrequired="true"/>
						</td>
					</tr>
					<tr height="5">
						<td align="right"><fmt:message key="market.mainsale.message10" />:</td>
						<td><input name="minSaleUnit.upCode" type="text" maxlength="50"  class="time_in"  dataType="string"/>
						</td>
					</tr>
					<tr height="5">
						<td align="right"><fmt:message key="marketman.minusalenitman.picture" />:</td>
						<td><input name="minSaleUnit.picPath" type="text" maxlength="50"  class="time_in" style="width: 300px"  dataType="string" isrequired="true"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div style="padding-top: 20px; text-align: center;">
			<input type="submit" value="<fmt:message key="common.btn.finish" />" class="diw_but" /><input
				 type="button" value="<fmt:message key="common.btn.return" />" class="diw_but" onclick="jump('toMinSaleUnitPage.do')"/>
		</div>
	</div>
	</form>
		<script type="text/javascript">
	$("form.validate").validate();
</script>
</body>
</html>