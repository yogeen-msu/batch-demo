<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="marketman.salecontractman" />
</title>

<link href="../../adminthemes/default/css/grid.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<!-- 
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
 -->

<script type="text/javascript" src="../js/market/saleDialog.js"></script>
	<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
<script type="text/javascript">
var chooseModel= '<fmt:message key="saleDialog.choose.product" />';
var chooseSealer= '<fmt:message key="saleDialog.choose.sealer" />';
var chooseConfig= '<fmt:message key="saleDialog.choose.config" />';
var forward= '<fmt:message key="common.info.jump" />';

</script>
<style type="text/css">
.right_tab {
	padding-right: 22px;
	width: auto;
}

leg_topri {
	height: 42px;
	line-height: 42px;
	width: 100%;
}

.sear {
	background-color: #FFFFFF;
	border: 1px solid #DDDDDD;
	width: 100%;
	margin: 0;
	padding: 0
}

.sear_tj {
	background: url("../main/images/leg_12.gif") repeat-x scroll left top
		transparent;
	border-bottom: 1px solid #DDDDDD;
	height: 28px;
	line-height: 28px;
	padding-left: 20px;
}
</style>
</head>
<body>
	<form action="updateSaleContract.do" method="post"
		id="saleContractForm">
		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			<fmt:message key="marketman.title" />
			&gt;
			<fmt:message key="marketman.salecontractman" />
			&gt;
			<fmt:message key="marketman.salecontractman.info" />
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj"
					style="font-size: 100%; font-weight: bold; margin: 0;">
					<img src="../main/images/leg_16.gif" width="14" height="16"
						style="line-height: 8px; border: 0 none; vertical-align: middle;" />&nbsp;
					<fmt:message key="marketman.salecontractman.info" />
				</h4>
				<div class="sear_table">
					<table class="form-table" width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="marketman.salecontractman.name" />:</td>
							<td>${saleContractvo.name}</td>
						</tr>
						<tr height="38">
							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="memberman.sealer.type" />:</td>
							<td>${saleContractvo.sealerTypeName}</td>
						</tr>


						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="memberman.sealer.jxs" />:</td>
							<td>${saleContractvo.sealerName}</td>
						</tr>
						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="product.producttypevo" />:</td>
							<td>${saleContractvo.productTypeName}</td>
						</tr>

						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="marketman.salecontractman.standardsaleconfig" />:</td>
							<td>
								<a href="${ctx}/autel/market/showSaleConfig.do?saleConfig.code=${saleContractvo.saleCfgCode}" style="text-decoration: none;color: rgb(215, 18, 0);">
									${saleContractvo.saleName}
								</a>
							</td>
						</tr>

						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="marketman.salecontractman.maxsaleconfig" />:</td>
							<td>
								<a href="${ctx}/autel/market/showSaleConfig.do?saleConfig.code=${saleContractvo.maxSaleCfgCode}" style="text-decoration: none;color: rgb(215, 18, 0);">
									${saleContractvo.maxSaleName}
								</a>
							</td>
						</tr>

						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="syssetting.languageconfig" />:</td>
							<td>${saleContractvo.languageCfgName}</td>
						</tr>

						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="syssetting.areaconfig" />:</td>
							<td>${saleContractvo.areaName}</td>
						</tr>

						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="marketman.salecontractman.outfactoryreletprice" />:</td>
							<td>${saleContractvo.reChargePrice}</td>
						</tr>

						<tr height="38">
							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message key="marketman.js.salecontractman.message8" />:</td>
							<td>${saleContractvo.upMonth}</td>
						</tr>
						
						<tr height="38">
							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message key="marketman.js.salecontractman.message9" />:</td>
							<td>${saleContractvo.warrantyMonth}</td>
						</tr>

						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="marketman.salecontractman.contracttime" />:</td>
							<td>${saleContractvo.contractDate}</td>
						</tr>


						<tr height="38">

							<td style="width:200px;text-align:right;padding-right:10px;"><fmt:message
									key="marketman.salecontractman.expiretime" />:</td>
							<td>${saleContractvo.expirationTime}</td>
						</tr>

						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>

			</div>
			<div style="padding-top: 20px; text-align: center;"><input
					type="button" value="<fmt:message key="common.btn.return" />"
					onclick="history.go(-1);"
					class="diw_but" />
			</div>
		</div>
	</form>
	
</body>
</html>