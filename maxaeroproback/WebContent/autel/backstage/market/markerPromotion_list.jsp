<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="market.promotion.yxhd" /></title>
<link href="../../autel/main/css/right.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="../../autel/main/iepngfix_tilebg.js"></script>
<script type="text/javascript"
	src="../../autel/js/market/market_list.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
</head>
<body>
	<form
		action="toMarkerPromotion.do?search.promotionType=${search.promotionType}"
		method="post">
		<div class="leg_topri">
			<fmt:message key="common.nva.currentloaction" />
			:
			<fmt:message key="marketman.title" />
			&gt;
			<fmt:message key="market.promotion.yxhd" />
			&gt;
			<c:if test="${search.promotionType==1 }">
				<fmt:message key="marketman.promotion.limittime" />
			</c:if>
			<c:if test="${search.promotionType==2 }">
				<fmt:message key="markerPromotion.list.jsp.early.bird.promotion" />
			</c:if>
			<c:if test="${search.promotionType==3 }">
				<fmt:message key="marketman.promotion.setpriviledge" />
			</c:if>
			<c:if test="${search.promotionType==4 }">
				<fmt:message key="markerPromotion.list.jsp.overdue.renewal" />
			</c:if>
		</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../../autel/main/images/leg_16.gif" width="14"
						height="16" />&nbsp;
					<c:if test="${search.promotionType==1 }">
				       <fmt:message key="marketman.promotion.limittime" />
					</c:if>
					<c:if test="${search.promotionType==2 }">
						<fmt:message key="markerPromotion.list.jsp.early.bird.promotion" />
					</c:if>
					<c:if test="${search.promotionType==3 }">
						<fmt:message key="marketman.promotion.setpriviledge" />
					</c:if>
					<c:if test="${search.promotionType==4 }">
						<fmt:message key="markerPromotion.list.jsp.overdue.renewal" />
					</c:if>
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td width="141" align="right"><fmt:message
									key="marketman.promotion.name" />:&nbsp;&nbsp;</td>
							<td width="180"><input name="search.promotionName"
								type="text" value="${search.promotionName}">
							</td>
							<td width="154" align="right"><fmt:message
									key="marketman.promotion.state" />:</td>
							<td><select name="search.status">
									<option value="">
										--
										<fmt:message key="marketman.promotion.state" />
										--
									</option>
									<option value="0"
										<c:if test="${search.status == 0 }">selected="selected"</c:if>>
										<fmt:message key="marketman.promotion.noexcute" />
									</option>
									<option value="1"
										<c:if test="${search.status == 1 }">selected="selected"</c:if>>
										<fmt:message key="marketman.promotion.start" />
									</option>
									<option value="2"
										<c:if test="${search.status == 2 }">selected="selected"</c:if>>
										<fmt:message key="marketman.promotion.stopped" />
									</option>
							</select></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="141" align="right"><fmt:message
									key="marketman.promotion.time" />:&nbsp;&nbsp;</td>
							<td width="180"><input type="text"
								value="${search.startTime }" name="search.startTime"
								style="width:70px" class="dateinput" readonly="readonly" /><fmt:message key="common.html.hr" /><input
								type="text" value="${search.endTime }" name="search.endTime"
								style="width: 70px" class="dateinput" readonly="readonly" />
							</td>
							<td width="154"></td>
							<td width="154"></td>
							<td width="160" align="right"><input type="submit"
								value="<fmt:message key="common.btn.search" />"
								class="search_but" /></td>
							<td></td>
						</tr>
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<h4>
						<span><input type="button"
							value="<fmt:message key="marketman.promotion.add" />" class="search_but"
							<c:if test="${search.promotionType==1 }">onclick="jump('toAddEmption.do?search.promotionType=1')"</c:if>
							<c:if test="${search.promotionType==2 }">onclick="jump('toAddCleap.do?search.promotionType=2')"</c:if>
							<c:if test="${search.promotionType==4 }">onclick="jump('toAddExpire.do?search.promotionType=4')"</c:if>
							<c:if test="${search.promotionType==3 }">onclick="jump('toAddPackage.do?search.promotionType=3')"</c:if> />
							&nbsp;&nbsp;&nbsp;&nbsp; </span>
						<c:if test="${search.promotionType==1 }">
							<fmt:message key="marketman.promotion.limittime" />
						</c:if>
						<c:if test="${search.promotionType==2 }">
							<fmt:message key="markerPromotion.list.jsp.promotion.plan" />
						</c:if>
						<c:if test="${search.promotionType==3 }">
							<fmt:message key="marketman.promotion.setpriviledge" />
						</c:if>
						<c:if test="${search.promotionType==4 }">
							<fmt:message key="markerPromotion.list.jsp.overdue.renewal" />
						</c:if>
					</h4>
				</div>
			</div>
			<div class="grid">
				<grid:grid from="webpage">
					<grid:header>
						<grid:cell><input type="checkbox" name="chkAll"></grid:cell>
						<grid:cell><fmt:message key="marketman.promotion.name" /></grid:cell>
						<grid:cell><fmt:message key="marketman.promotion.time" /></grid:cell>
						<grid:cell><fmt:message key="marketman.promotion.creater" /></grid:cell>
						<grid:cell><fmt:message key="marketman.promotion.state" /></grid:cell>
						<grid:cell><fmt:message key="common.list.operation" />
						</grid:cell>
					</grid:header>
					<grid:body item="item">
						<td><input type="checkbox" name="chk" value="${item.code }">
						</td>
						<td>${item.promotionName }</td>
						<td><c:set var="string1" value="${ item.rsultTime }" /> <c:set
								var="result" value="${fn:split(string1, '|')}" /> <c:forEach
								items="${ result}" var="t">
								${t }
								<br>
							</c:forEach>
						</td>
						<td>${item.creator }</td>
						<td><c:if test="${item.status == 0 }">
								<fmt:message key="marketman.promotion.noexcute" />
							</c:if> <c:if test="${item.status == 1 }">
								<fmt:message key="marketman.promotion.start" />
							</c:if> <c:if test="${item.status == 2 }">
								<fmt:message key="marketman.promotion.stopped" />
							</c:if>
						</td>
						<td><c:if test="${item.status != 1 }">
								<c:if test="${search.promotionType==1 }">
									<a
										href="toUpdateEmption.do?baseInfo.code=${item.code}&search.promotionType=1"><fmt:message
											key="common.list.mod" /> </a>
								</c:if>
								<c:if test="${search.promotionType==3 }">
									<a
										href="toUpdatePackage.do?baseInfo.code=${item.code}&search.promotionType=3"><fmt:message
											key="common.list.mod" /> </a>
								</c:if>
								<c:if test="${search.promotionType==2 }">
									<a
										href="toUpdateCleap.do?baseInfo.code=${item.code}&search.promotionType=2"><fmt:message
											key="common.list.mod" /> </a>
								</c:if>
								<c:if test="${search.promotionType==4 }">
									<a
										href="toUpdateExpire.do?baseInfo.code=${item.code}&search.promotionType=4"><fmt:message
											key="common.list.mod" /> </a>
								</c:if>
								&nbsp;
								<a href="###"
									onclick="delPromotion('${item.code}',${search.promotionType});"><fmt:message
										key="common.list.del" /> </a>
							</c:if>
							
							<c:if test="${item.status == 1 }">
								<c:if test="${search.promotionType==1 }">
										<a
											href="toUpdateEmption.do?baseInfo.code=${item.code}&search.promotionType=1&display=1"><fmt:message
												key="memberman.complaintman.show" /> </a>
									</c:if>
									<c:if test="${search.promotionType==3 }">
										<a
											href="toUpdatePackage.do?baseInfo.code=${item.code}&search.promotionType=3&display=1"><fmt:message
												key="memberman.complaintman.show" /> </a>
									</c:if>
									<c:if test="${search.promotionType==2 }">
										<a
											href="toUpdateCleap.do?baseInfo.code=${item.code}&search.promotionType=2&display=1"><fmt:message
												key="memberman.complaintman.show" /> </a>
									</c:if>
									<c:if test="${search.promotionType==4 }">
										<a
											href="toUpdateExpire.do?baseInfo.code=${item.code}&search.promotionType=4&display=1"><fmt:message
												key="memberman.complaintman.show" /> </a>
									</c:if>
							</c:if>
						</td>
					</grid:body>
					<tr>
						<td colspan="6" align="left">&nbsp;&nbsp;<input
							type="checkbox" name="chkAllT">&nbsp;&nbsp;<fmt:message
								key="common.list.all" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
							id="startBtn" promotionType="${search.promotionType}"
							type="button"
							value="<fmt:message key="marketman.promotion.qy" />"
							class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
							id="stopBtn" promotionType="${search.promotionType}"
							type="button"
							value="<fmt:message key="marketman.promotion.stop" />"
							class="search_but" />
						</td>
					</tr>
				</grid:grid>
			</div>
		</div>
	</form>
</body>
</html>