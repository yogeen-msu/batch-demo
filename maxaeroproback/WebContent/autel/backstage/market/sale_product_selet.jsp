<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="../../statics/js/admin/Grid.js"></script>
<script type="text/javascript" src="../js/market/saleDialog.js"></script>
<form method="post" id="pageSelectForm" action="toSearchPtDlg.do">
<div class="grid">
	<div class="toolbar">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="product.producttypevo" />:<input type="text" name="productTypeVo.name" value="${productTypeVo.name }" />&nbsp;&nbsp;
			<input type="button"  name="submit" onclick="pageQuery()" value=<fmt:message key="common.btn.search" /> />
			<div style="clear: both"></div>
	</div>
	
	<grid:grid from="webpage" ajax="yes">
		<grid:header>
			<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
			<grid:cell width="200px"><fmt:message key="common.list.code" /></grid:cell>
			<grid:cell><fmt:message key="product.producttypevo" /><fmt:message key="common.name" /></grid:cell>
			<grid:cell width="100px"></grid:cell>
		</grid:header>
	
		<grid:body item="item">
			<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
			<grid:cell>${item.code} </grid:cell>
			<grid:cell>${item.name}</grid:cell>
			<grid:cell>
				<input type="button" class="selectProductItem" value=<fmt:message key="common.openselect.choose" /> selectCode="${item.code}" selectName="${item.name}" />
			</grid:cell>
		</grid:body>
	</grid:grid>
</div>
</form>
