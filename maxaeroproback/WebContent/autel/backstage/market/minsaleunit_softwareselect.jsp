<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<link href="../main/css/table.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/autel/js/market/checkboxCode.js"></script>
<input type="hidden" name="selectedcodes" id="selectedcodes" value="" />
<form method="post" id="pageSelectForm" action="toSearchPtDlgNew.do">
<input type="hidden" name="minSaleUnit.code" value="${minSaleUnit.code }" />
<input type="hidden" id="excludecodes" name="excludecodes" value="" />
<input type="hidden" id="selprosoftconfigcodes" name="selprosoftconfigcodes" value="" />

<table cellspacing="1" cellpadding="3" width="100%" class="form-table">
	<tr>
		<td style="vertical-align: top;padding: 5px;border-right: #dddddd 1px solid;">
			<table>
				<tr>
					<td style="font-size: 16px;height: 20px;">
						<fmt:message key="product.virtualdir" />：
					</td>
				</tr>
				<tr>
					<td>
						<div id="productsoftwareconfigbox">
							<ul class="checktree" >
							</ul>
						</div>
					</td>
				</tr>
			</table>
		</td>
		<td width="500" style="vertical-align: top;padding: 5px;">
			<div class="grid">
				<div class="toolbar" style="padding: 5px;height: 25px;">
					<div style="float: left;">
						&nbsp;&nbsp;&nbsp;&nbsp;
						<fmt:message key="product.software.name" />:&nbsp;&nbsp;
						<input type="text" name="softwareType.name" value="${softwareType.name }" />&nbsp;&nbsp;
						<input type="button" class="search_but"  name="submit" onclick="pageQuery()" value="<fmt:message key="common.btn.search" />" />
					</div>
					<div style="float: right;">
						<input type="button" class="search_but"  name="submit" onclick="clickok();" value="<fmt:message key="common.btn.ok" />" />
						&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
				</div>
				
				<table width="100%" border="0" bordercolor="#dddddd" id="detailTable">
				
					<tr class="title2">
						<td width="160"><fmt:message key="common.list.code" /></td>
						<td><fmt:message key="product.software.name" /></td>
						<td width="70" align="right">
							全选&nbsp;&nbsp;<input type="checkbox" name="allCheckbox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
					
					<c:forEach items="${softwareTypes }" var="item">
						<tr>
							<td>${item.code}</td>
							<td>${item.name}</td>
							<td align="right">
								<input type="checkbox" name="checkboxcode" value="${item.code}" softname="${item.name}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
						</tr>
					</c:forEach>
					<tr class="title2">
						<td colspan="3" align="right">
							<input type="button" class="search_but"  name="submit" onclick="clickok();" value="<fmt:message key="common.btn.ok" />" />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							全选&nbsp;&nbsp;<input type="checkbox" name="allCheckbox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>
</form>