<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecard.saleconfig" /></title>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/market/saleConfig_list.js.jsp?ajax=yes"></script>
	<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">
function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
</head>
<body>
<form action="toSaleConfig.do" method="post" id="saleConfigForm">
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="trademan.rechargecard.saleconfig" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="trademan.rechargecard.saleconfig" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						
						<td width="110" align="right">
						<fmt:message key="product.producttypevo" />:
						</td>
						<td width="180">&nbsp;
						<select class="act_zt" name="configSearch.proTypeCode">
						<option value="">--<fmt:message key="select.option.select" />--</option>
							<c:forEach items="${productTypes }" var="item">
								<option value="${item.code}" <c:if test="${configSearch.proTypeCode == item.code}"> selected="selected"</c:if>>${item.name}</option>
							</c:forEach>
						</select>
						</td>
						<td width="180" align="right"><fmt:message key="saleConfig_list.jsp.saleconfig.name" />:</td>
						<td width="154">
						<input name="configSearch.configName" class="time_in" type="text" value="${configSearch.configName }">
						</td>
						<td width="160" align="right"><input type="submit"
							value="<fmt:message key="common.btn.search" />" class="search_but" /></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span><input type="button" value="<fmt:message key="marketman.promotion.addsaleconfig" />" onclick="jump('toAddSaleConfig.do')"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp; </span><fmt:message key="saleConfig_list.jsp.saleconfig" />
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell><fmt:message key="common.list.code" /></grid:cell>
					<grid:cell><fmt:message key="saleConfig_list.jsp.saleconfig.name" /></grid:cell>
					<grid:cell><fmt:message key="product.producttypevo" /></grid:cell>
					<grid:cell><fmt:message key="marketman.saleconfigman.picture" /></grid:cell>
					<grid:cell><fmt:message key="saleConfig_list.jsp.sale.description" /></grid:cell>
					<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item" >
						<td>${item.code}</td>
						<td>${item.name}</td>
						<td>${item.proTypeName}</td>
						<td>${item.picPath}</td>
						<td><input type="button" onclick="jump('toSaleConfigMemo.do?saleConfig.code=${item.code }')" class="search_but" value="<fmt:message key="saleConfig_list.jsp.sale.edit" />"></td>
						<c:if test="${adminFlag=='Y'}">
							<td>
							<a href="toUpdateSaleConfig.do?saleConfig.code=${item.code}"><fmt:message key="common.list.mod" /></a>
							&nbsp;&nbsp;<a href="###" name="delSaleConfig" saleCode="${item.code}"><fmt:message key="common.list.del" /></a></td>
						</c:if>
						<c:if test="${adminFlag=='N'}">
							<td>
							<a href="toViewSaleConfig.do?saleConfig.code=${item.code}"><fmt:message key="order.view.name" /></a>
							&nbsp;&nbsp;<a href="###" name="delSaleConfig" saleCode="${item.code}"><fmt:message key="common.list.del" /></a></td>
						</c:if>
				</grid:body>
			</grid:grid>
		</div>
	</div>
	</form>
</body>
</html>