<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.softwareman" /></title>
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript">
	var areas = "${areas}";
</script>
<script type="text/javascript" src="../js/market/saleConfig_update.js.jsp?ajax=yes"></script>
<script>
window.onload = function ()
{
	var oWin = document.getElementById("win");
	var oLay = document.getElementById("overlay");	
	var oBtn = document.getElementsByTagName("button")[0];
	var oClose = document.getElementById("close");
	var oH2 = oWin.getElementsByTagName("h2")[0];
	var bDrag = false;
	var disX = disY = 0;
	oBtn.onclick = function ()
	{
		oLay.style.display = "block";
		oWin.style.display = "block";
	};
	oClose.onclick = function ()
	{
		oLay.style.display = "none";
		oWin.style.display = "none";
		
	};
	oClose.onmousedown = function (event)
	{
		(event || window.event).cancelBubble = true;
	};
	oH2.onmousedown = function (event)
	{		
		var event = event || window.event;
		bDrag = true;
		disX = event.clientX - oWin.offsetLeft;
		disY = event.clientY - oWin.offsetTop;		
		this.setCapture && this.setCapture();		
		return false;
	};
	document.onmousemove = function (event)
	{	
		if (!bDrag) return;
		var event = event || window.event;
		var iL = event.clientX - disX;
		var iT = event.clientY - disY;
		var maxL = document.documentElement.clientWidth - oWin.offsetWidth;
		var maxT = document.documentElement.clientHeight - oWin.offsetHeight;		
		iL = iL < 0 ? 0 : iL;
		iL = iL > maxL ? maxL : iL; 		
		iT = iT < 0 ? 0 : iT;
		iT = iT > maxT ? maxT : iT;
		
		oWin.style.marginTop = oWin.style.marginLeft = 0;
		oWin.style.left = iL + "px";
		oWin.style.top = iT + "px";		
		return false;
	};
	document.onmouseup = window.onblur = oH2.onlosecapture = function ()
	{
		bDrag = false;				
		oH2.releaseCapture && oH2.releaseCapture();
	};
};

function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}

</script>
</head>

<body>
<form action="updateSaleConfig.do" method="post" id="configForm">
<input type="hidden" name="saleConfig.code" value="${saleConfig.code }"/>
    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="trademan.rechargecard.saleconfig" />&gt; <fmt:message key="common.list.mod" /><fmt:message key="trademan.rechargecard.saleconfig" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="saleConfig.update.jsp.title" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="marketman.saleconfigman.name" />:</td>
							<td>&nbsp;&nbsp;<input disabled id="saleConfigName" name="saleConfig.name" type="text" value="${saleConfig.name }"/></td>
							<td></td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="marketman.saleconfigman.picture" />:</td>
							<td>&nbsp;&nbsp;<input id="saleConfigPicPath" disabled name="saleConfig.picPath" value="${saleConfig.picPath }" style="width: 300px" type="text"/></td>
							<td></td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
							<td>&nbsp;&nbsp;
							<select id="saleConfigProduct" disabled name="saleConfig.proTypeCode">
								<option value="">-- <fmt:message key="select.option.select" /> --</option>
								<c:forEach items="${productTypes }" var="item">
									<option value="${item.code }" <c:if test="${saleConfig.proTypeCode == item.code}"> selected="selected" </c:if> >${item.name}</option>
								</c:forEach>
							</select>
							</td>
							<td></td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<div id="overlay"></div>
					<div id="win">
						<h2>
							<span id="close">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0">
								<tr>

									<td width="40%" align="right"><fmt:message key="marketman.minusalenit" />:</td>
									<td><select id="minSaleUnit" style="width: 300px;" multiple="multiple" size="10">
										<c:forEach items="${configVos }" var="item">
											<c:if test="${item.name !=null}">
												<option value="${item.code }" areaCfgCode="${item.areaCfgCode }">${item.name }</option>
											</c:if>
										</c:forEach>
									</select>
									</td>
								</tr>
								<tr>

									<td width="40%" align="right">&nbsp;</td>
									<td><input id="finsh" type="button" value="<fmt:message key="saleConfig.update.jsp.button.save" />"
										class="diw_but_tc" />
									</td>
								</tr>
							</table>
						</div>
					</div>

					<h4>
						<span>
<%-- 						<button type="button" class="search_but"><fmt:message key="saleConfig.update.jsp.button.add" /></button> --%>
						</span><fmt:message key="saleConfig.update.jsp.sale.unit.list" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="configTable">
				
					<tr class="title">
						<td width="20%"><fmt:message key="common.list.code" /></td>
						<td width="50%"><fmt:message key="memberman.customman.minsaleunitname" /></td>
						<%-- <td width="30%"><fmt:message key="common.list.operation" /></td> --%>

					</tr>
					
					<c:forEach items="${minSaleUnitConfigVos }" var="item">
						<tr>
							<td>${item.code }</td>
							<td>${item.name }</td>
							<%-- <td class="caoz"><a href="###" detailId="${item.id }" saleCode="${item.code }" saleName="${item.name }" areaCfgCode="${item.areaCfgCode }" onclick="delUnit(this);"><fmt:message key="marketman.saleconfigman.moveout" /></a></td> --%>
						</tr>
					</c:forEach>

				</table>
				</div>
				<div style="padding-top: 20px;padding-left:40%">
					<%-- <input id="sumbitBtn" type="button" value="<fmt:message key="saleConfig.update.jsp.button.save" />" class="diw_but" /> --%>
					<input type="button" onclick="jump('toSaleConfig.do')" value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			</div>
		

	</form>
</body>
</html>


