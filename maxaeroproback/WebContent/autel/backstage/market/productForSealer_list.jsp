<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="trademan.rechargecard.saleconfig" /></title>
<link href="../main/css/right.css" rel="stylesheet" type="text/css" />
<link href="../../adminthemes/default/css/grid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../autel/js/common/reset.js.jsp?ajax=yes"></script>
<script type="text/javascript">

$(document).ready(function(){
	
	$("a[name^='delProductType']").click(function(){
		if(confirm("<fmt:message key='common.js.confirmDel' />")){
			var $from = $("#productTypeForm");
			var code = $(this).attr("productTypeCode");
			$from.attr("action","delete.do");
			$from.append("<input type='hidden' name='code' value='"+code+"'/>");
			$from.submit();
		}
	});
});

function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
</script>
</head>
<body>
<form action="list.do" method="post" id="productTypeForm">
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="sale.product.menu.title" />&gt; <fmt:message key="sale.product.menu.title" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="sale.product.menu.title" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr height="38">
						
					<%-- 	<td width="110" align="right">
						<fmt:message key="sale.product.addjsp.product" />:
						</td>
						<td width="180">&nbsp;
						<select class="act_zt" name="product.proTypeCode">
						<option value="">--<fmt:message key="select.option.select" />--</option>
							<c:forEach items="${productTypes }" var="item">
								<option value="${item.code}" <c:if test="${product.proTypeCode == item.code}"> selected="selected"</c:if>>${item.name}</option>
							</c:forEach>
						</select>
						</td> --%>
						<td width="180" align="right"><fmt:message key="sale.product.addjsp.productName" />:</td>
						<td width="154">
						<input name="product.name" class="time_in" type="text" value="${product.name}">
						</td>
						<td width="160" align="right"><input type="submit"
							value="<fmt:message key="common.btn.search" />" class="search_but" /></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="active">
				<h4>
					<span><input type="button" value="<fmt:message key="sale.product.addjsp.add"/>" onclick="jump('add.do')"
						class="search_but" /> &nbsp;&nbsp;&nbsp;&nbsp; </span><fmt:message key="sale.product.listjsp.listinfo"/>
				</h4>
			</div>
		</div>
		<div class="grid">
			<grid:grid from="webpage">
				<grid:header>
					<grid:cell><fmt:message key="common.list.code" /></grid:cell>
					<grid:cell><fmt:message key="sale.product.addjsp.productName"/></grid:cell>
				
					<grid:cell><fmt:message key="product.producttypevo.picture" /></grid:cell>
					<grid:cell><fmt:message key="common.list.operation" /></grid:cell>
				</grid:header>
				<grid:body item="item" >
						<td>${item.code}</td>
						<td>${item.name}</td>
					
						<td>${item.picPath}</td>
						<td>
						<a href="edit.do?code=${item.code}"><fmt:message key="common.list.mod" /></a>&nbsp;&nbsp;
						<a href="###" id="delProductType" name="delProductType" productTypeCode="${item.code}"><fmt:message key="common.list.del" /></a>
						</td>
				</grid:body>
			</grid:grid>
		</div>
	</div>
	</form>
</body>
</html>