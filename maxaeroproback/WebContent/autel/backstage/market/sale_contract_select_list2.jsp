<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<form method="post" id="pageSelectConfigForm" action="selectsaleContract.do">
	<div class="grid">
	
	<div class="toolbar">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="product.producttypevo" />:<select id="selectProType" name="contract.proTypeCode"  >
			  <option value=""><fmt:message key="select.option.select" /></option>
			   <c:forEach var="item" items="${productTypeList}">
			   

					<c:choose>

						<c:when test="${empty contract.proTypeCode}"> 
							<option value="${item.code}">${item.name}</option>  
                        </c:when>
						<c:otherwise>  
							<option value="${item.code}" <c:if test="${contract.proTypeCode==item.code }">selected</c:if>>${item.name}</option>  
                        </c:otherwise>
					</c:choose>


				</c:forEach>
			
			</select>&nbsp;&nbsp;
			<fmt:message key="marketman.saleconfigman.name" />:<input type="text" name="contract.name" value="${contract.name}" />&nbsp;&nbsp;
			<input type="button"  name="submit" onclick="pageConfigQuery()" value=<fmt:message key="common.btn.search" /> />
			 <input type="hidden" value="${contract.proTypeCode}" id="queryProType" name="queryProType"/> 
			<div style="clear: both"></div>
	</div>
	
		<grid:grid from="webpage" ajax="yes">
			<grid:header>
				<grid:cell width="80px"><fmt:message key="memberman.sealer.jxs" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="syssetting.languageconfig" /></grid:cell>
				<%-- <grid:cell width="200px"><fmt:message key="common.list.code" /></grid:cell> --%>
				<grid:cell><fmt:message key="marketman.salecontractman.name" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${item.sealerAutelId}</grid:cell>
				<grid:cell>${item.languageName}</grid:cell>
				<%-- <grid:cell>${item.code} </grid:cell> --%>
				<grid:cell>${item.name}</grid:cell>
				<grid:cell>
					<input type="button" class="selectSaleContractItem" value="<fmt:message key="marketman.saleconfigman.check" />" selectCode="${item.code}" selectName="${item.name}" />
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
