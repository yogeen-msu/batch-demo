<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script >

</script>
<form method="post" id="pageSelectConfigForm" action="product-info!selectsaleContract.do">
	<div class="grid">
	
	<div class="toolbar">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<fmt:message key="marketman.saleconfigman.name" />:<input type="text" name="contract.name" value="${contract.name}" />&nbsp;&nbsp;
			<input type="button"  name="submit" onclick="pageConfigQuery()" value=<fmt:message key="common.btn.search" /> />
			<%-- <input type="hidden" value="${contract.proTypeCode}" name="proTypeCode"/> --%>
			<div style="clear: both"></div>
	</div>
	
		<grid:grid from="webpage" ajax="yes">
			<grid:header>
				<grid:cell width="50px"><fmt:message key="memberman.customman.seqnum" /></grid:cell>
				<grid:cell width="200px"><fmt:message key="common.list.code" /></grid:cell>
				<grid:cell><fmt:message key="marketman.salecontractman.name" /></grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>
		
			<grid:body item="item">
				<grid:cell>${(pageNo-1)*pageSize+index+1}</grid:cell>
				<grid:cell>${item.code} </grid:cell>
				<grid:cell>${item.name}</grid:cell>
				<grid:cell>
					<input type="button" class="selectSaleContractItem" value="<fmt:message key="marketman.saleconfigman.check" />" selectCode="${item.code}" selectName="${item.name}" />
				</grid:cell>
			</grid:body>
		
		</grid:grid>
	</div>
</form>
