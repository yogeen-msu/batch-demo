<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.softwareman" /></title>
<link href="../main/css/table.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../main/iepngfix_tilebg.js"></script>
<script type="text/javascript" src="../js/market/minSaleUnitSelect.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../js/market/jquery.checktree.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../core/admin/auth/checktree.css" />
<script type="text/javascript" src="../js/market/saleConfig_update.js.jsp?ajax=yes"></script>
<script>
var addArray = new Array();
var delArray = new Array();
var viewArray = new Array();

$(document).ready(function(){
	$("#configTable").find("#minSaleTR").each(function(i){
		var minCode=$(this).attr("minCode");
		var minName=$(this).attr("minName");
		var softwaredetail = new Softwaredetail(minName,minCode);
		viewArray.push(softwaredetail);
	});
	$(".softwaretypeclass").click(function(){
	  	var softwareTypeCode = $(this).attr("softwareTypeCode");
		addArray.remove(softwareTypeCode);
		viewArray.remove(softwareTypeCode);
		$(this).closest("tr").remove();
	});
});


function hasExist(softwaredetail){
	var flag=false;
	$("#configTable").find("#minSaleTR").each(function(i){
		var minCode=$(this).attr("minCode");
		var minName=$(this).attr("minName");
		var softwaredetail = new Softwaredetail(minName,minCode);
		viewArray.push(softwaredetail);
	});
	for(var i=0;i<viewArray.length;i++){
		if(viewArray[i].minSaleCode==softwaredetail.minSaleCode){
			flag=true;
		}
	}
	return flag;
}

function Softwaredetail(minSaleName,minSaleCode){
	this.minSaleName = minSaleName;
	this.minSaleCode = minSaleCode;
}


function clickok(){
	var $selectCheckbox = $("input[name='productsoftwareconfigcode']:checked");
	var size = $selectCheckbox.size();
	if(size ==0){
		alert("<fmt:message key='marketman.js.xzSoftwaretype' />");
		return false;
	}
 	$selectCheckbox.each(function(){
		var value= $(this).val();
		var text = $(this).attr("minName");
		
		var flag= $(this).attr("flag");
		if(flag=="1"){
			var softwaredetail = new Softwaredetail(text,value);
			var test=hasExist(softwaredetail);
			if(test==false){
				var $tr = buildHtml(softwaredetail,"configTable");
				$(tableId).append($tr);
			}
		}
		
		
	});
			
	
	Cop.Dialog.close("selectSoftwareDlg");
}

function buildHtml(softwaredetail,tableId){
	
	var $tr = $("<tr id='minSaleTR' minName='"+softwaredetail.minSaleName+"' minCode='"+softwaredetail.minSaleCode+"'></tr>");
	var $td0 = $("<td>"+softwaredetail.minSaleCode+"</td>");
	var $td1 = $("<td>"+softwaredetail.minSaleName+"</td>");
	var $td4 = $("<td class='caoz' width='20%'></td>");
	var $a = $("<a href='###' class='softwaretypeclass' softwareTypeName='"+softwaredetail.softwareTypeName+"' softwareTypeCode='"+softwaredetail.softwareTypeCode+"'><fmt:message key='marketman.saleconfigman.moveout' /></a>");
	
	$a.click(function(){
		var softwareTypeCode = $(this).attr("softwareTypeCode");
		
		addArray.remove(softwareTypeCode);
		viewArray.remove(softwareTypeCode);
		
		$(this).closest("tr").remove();
			
	});
	$td4.append($a);
	
	$tr.append($td0).append($td1).append($td4);
	return $tr;
	//$(tableId).append($tr);
}
function jump(url){
	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
   
}
</script>
</head>

<body>
<form action="updateSaleConfig.do" method="post" id="configForm">
<input type="hidden" name="saleConfig.code" value="${saleConfig.code }"/>
    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt; <fmt:message key="trademan.rechargecard.saleconfig" />&gt; <fmt:message key="common.btn.add" /><fmt:message key="trademan.rechargecard.saleconfig" /></div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					<img src="../main/images/leg_16.gif" width="14" height="16" />&nbsp;<fmt:message key="saleConfig.add.jsp.title" />
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="saleConfig_list.jsp.saleconfig.name" />:</td>
							<td>&nbsp;&nbsp;<input id="saleConfigName" name="saleConfig.name" type="text" value="${saleConfig.name}"/></td>
							<td></td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="marketman.saleconfigman.picture" />:</td>
							<td>&nbsp;&nbsp;<input id="saleConfigPicPath" name="saleConfig.picPath" style="width: 300px" type="text" value="${saleConfig.picPath }"/></td>
							<td></td>
						</tr>
						<tr height="38">
							<td width="111" align="right"><fmt:message key="product.producttypevo" />:</td>
							<td>&nbsp;&nbsp;
							<select id="saleConfigProduct" name="saleConfig.proTypeCode">
								<c:forEach items="${productTypes }" var="item">
									<option value="${item.code }" <c:if test="${saleConfig.proTypeCode == item.code}"> selected="selected" </c:if> >${item.name}</option>
								</c:forEach>
							</select>
							</td>
							<td></td>
						</tr>
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
				<div class="active">
					<div id="overlay"></div>
					<div id="win">
						<h2>
							<span id="close">×</span>
						</h2>
						<div class="tc_tab">
							<table width="100%" border="0">
								<tr>

									<td width="30%" align="right"><fmt:message key="marketman.minusalenit" />:</td>
									<td><select id="minSaleUnit" style="width: 400px;" multiple="multiple" size="10">
										<c:forEach items="${configVos }" var="item">
											<c:if test="${item.name !=null}">
												<option value="${item.code }" proTypeCode="${item.proTypeCode}" areaCfgCode="${item.areaCfgCode }">${item.name }</option>
											</c:if>
										</c:forEach>
									</select>
									</td>
								</tr>
								<tr>

									<td width="40%" align="right">&nbsp;</td>
									<td><input id="finsh" type="button" value="<fmt:message key="emption.add.jsp.button.save" />"
										class="diw_but_tc" />
									</td>
								</tr>
							</table>
						</div>
					</div>

					<h4>
						<span><button id="selectSoftDlg" type="button" class="search_but"><fmt:message key="saleConfig.add.jsp.button.add" /></button>
						</span><fmt:message key="saleConfig.add.jsp.sale.unit.list" />
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="configTable">
				
					<tr class="title">
						<td width="20%"><fmt:message key="common.list.code" /></td>
						<td width="50%"><fmt:message key="memberman.customman.minsaleunitname" /></td>
						<td width="30%"><fmt:message key="common.list.operation" /></td>

					</tr>
					
					<c:forEach items="${minSaleUnitConfigVos }" var="item">
						<tr id="minSaleTR" minCode="${item.code}" minName="${item.name}">
							<td>${item.code}</td>
							<td>${item.name}</td>
							<td class="caoz"><a class='softwaretypeclass' softwareTypeName="${item.name}" softwareTypeCode='${item.code}'  href="###"><fmt:message key="marketman.saleconfigman.moveout" /></a></td>
						</tr>
					</c:forEach>

				</table>
				</div>
				
				
				
				
				<div style="padding-top: 20px;padding-left:40%">
					<input id="sumbitBtn" type="button" value="<fmt:message key="saleConfig.add.jsp.button.save" />" class="diw_but" /><input
						type="button" onclick="jump('toSaleConfig.do')" value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			
		</div>

	</form>
	<div id="selectSoftwareDlg" style="max-height: 800px;"></div>

	<script type="text/javascript">
		var treeJson;
		$(function() {
			$.ajax({
				url:'jsonTree.do?productSoftwareConfigCode=0&&ajax=yes',
				type:"get",
				async: false,
				success :function(data){
					treeJson = eval(eval(data));
				}
			});
			
			Software.init();
		});
	</script>
</body>
</html>

