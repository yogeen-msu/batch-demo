<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<link href="../../autel/main/css/right.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	.hour{}
	.minute{}
</style>
<script type="text/javascript" src="../../autel/backstage/system/js/selfdialog.js"></script>
<script type="text/javascript" src="../../autel/js/market/market.js.jsp?ajax=yes"></script>
<script type="text/javascript" src="../../autel/js/market/market_saleconfig.js.jsp?ajax=yes"></script>

<form action="addPromotion.do" method="post" id="marketForm">
<input type="hidden" name="search.promotionType" value="${search.promotionType}"/>
<input type="hidden" name="baseInfo.promotionType" value="3"/>
	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="marketman.title" />&gt;<fmt:message key="marketman.stagey.title" /> &gt; <fmt:message key="marketman.promotion.add" /></div>
	<div class="right_tab">
		<div class="sear">
			<h4 class="sear_tj">
				<img src="../../autel/main/images/leg_16.gif" width="14" height="16" />&nbsp; <fmt:message key="marketman.promotion.add" />
			</h4>
			<div class="sear_table">
				<table width="100%" border="0">
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr height="38">
						<td width="111" align="right"><fmt:message key="marketman.promotion.name" />:</td>
						<td width="154"><input id="promotionName" name="baseInfo.promotionName" type="text" class="time_in" />
						</td>
						<td width="154" align="right"></td>
						<td width="154"></td>
						<td width="" align="right"></td>
						<td></td>
					</tr>
					<tr height="5">

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</div>
			<div id="timeSelectDiv" class="active">
				<div class="dialog_overlay"></div>
				<div class="dialog_win">
					<h2>
						<span  class="dialog_close" >×</span>
					</h2>
					<div class="tc_tab">
						<table width="100%" border="0">
							<tr>

								<td width="40%" align="right"><fmt:message key="marketman.promotion.starttime" /></td>
								<td><input type="text" value="" id="startDate" name="startDate" style="width:80px" class="dateinput" readonly="readonly"/>
									<select class="hour"></select> <select class="minute"></select>
								</td>
							</tr>
							<tr>

								<td width="40%" align="right"><fmt:message key="marketman.promotion.endtime" />:</td>
								<td><input type="text" value="" id="endDate" name="endDate" style="width:80px" class="dateinput" readonly="readonly"/>
								<select class="hour"></select> <select class="minute"></select>
								</td>
							</tr>

							<tr>

								<td width="40%" align="right">&nbsp;</td>
								<td><input id="timeFinishBtn" type="button" value="<fmt:message key="common.btn.ok" />"
									class="diw_but_tc" />
								</td>
							</tr>
						</table>
					</div>
				</div>

				<h4>
					<span><button type="button" id="timeBtn" class="pro_compar_time" value="<fmt:message key="marketman.promotion.addtimes" />"><fmt:message key="marketman.promotion.addtimes" /></button>
					</span><fmt:message key="marketman.promotion.time" />
				</h4>
			</div>

			<div class="act_tj">
				<table width="100%" border="0" id="timeTable" bordercolor="#dddddd">
					<tr class="title">
						<td width="34%"><fmt:message key="marketman.promotion.starttime" /></td>
						<td width="34%"><fmt:message key="marketman.promotion.endtime" /></td>
						<td width="32%"><fmt:message key="common.list.operation" /></td>

					</tr>
					
				</table>

			</div>
			<div class="active">
				<h4>
					<fmt:message key="marketman.promotion.buyorrelet" />:<input name="rule.conditionType" type="radio" checked="checked" value="0" class="xz" /> <fmt:message key="marketman.promotion.softwarebuy" />&nbsp;&nbsp; <input name="rule.conditionType" type="radio" value="1" class="xz" /> <fmt:message key="marketman.promotion.softwarerelet" />
				</h4>
			</div>
			
			<div id="saleUnitDiv" class="active">
				<div class="dialog_overlay"></div>
				<div class="dialog_win">
					<h2>
						<span  class="dialog_close">×</span>
					</h2>
					<div class="tc_tab">
						<table width="100%" border="0">
							<tr>
								<td width="40%" align="right"><fmt:message key="marketman.minusalenit" />:</td>
								<td>
								<select id="minSaleUnitSelect" multiple="multiple" size="20">
									<c:forEach items="${minSaleUnitVos }" var="item">
										<option value="${item.code }" typeCode="${item.proTypeCode }" typeName="${item.productTypeName}">${item.name }</option>
									</c:forEach>
								</select>
								</td>
							</tr>
							<tr>
								<td width="40%" align="right">&nbsp;</td>
								<td><input id="saleUnitFinishBtn" type="button" value="<fmt:message key="common.btn.ok" />"
									class="diw_but_tc" />
								</td>
							</tr>
						</table>
					</div>
				</div>
				<h4>
					<span><button type="button" id="saleUnitBtn" class="pro_compar_dw" value="<fmt:message key="common.btn.add" />"><fmt:message key="common.btn.add" /></button>
					</span><fmt:message key="marketman.promotion.tcxq" />:<input id="allSaleUnit" style="display: none;" name="baseInfo.allSaleUnit" type="checkbox" value="1" /> 
				</h4>
			</div>
			
			<div class="act_tj">
				<table width="100%" border="0" bordercolor="#dddddd" id="unitTable">
					<tr class="title">
						<td width="25%"><fmt:message key="common.list.code" /></td>
						<td width="25%"><fmt:message key="memberman.customman.minsaleunitname" /></td>
						<td width="25%"><fmt:message key="product.producttypevo" /></td>
						<td width="25%"><fmt:message key="common.list.operation" /></td>
					</tr>
				</table>
			</div>
			<div class="active">
				<div class="dialog_overlay"></div>
				<h4>
					<span><input name="rule.discount" id="discount" type="text" size="2"/>&nbsp;<fmt:message key="marketman.promotion.pri" />
					</span><fmt:message key="marketman.promotion.tczk" />:
				</h4>
			</div>
			
			<div id="saleConfigDiv" class="active">
				<div class="dialog_overlay"></div>
				<div class="dialog_win">
					<h2>
						<span  class="dialog_close">×</span>
					</h2>
					<div class="tc_tab">
						<table width="100%" border="0">
							<tr>
								<td width="40%" align="right"><fmt:message key="trademan.rechargecard.saleconfig" />:</td>
								<td>
								<select id="minSaleConfigSelect" multiple="multiple" size="20">
									<c:forEach items="${saleConfigs }" var="item">
										<option value="${item.code }" typeCode="${item.proTypeCode }" typeName="${item.proTypeName}">${item.name }</option>
									</c:forEach>
								</select>
								</td>
							</tr>
							<tr>
								<td width="40%" align="right">&nbsp;</td>
								<td><input id="saleConfigFinishBtn" type="button" value="<fmt:message key="common.btn.ok" />"
									class="diw_but_tc" />
								</td>
							</tr>
						</table>
					</div>
				</div>
				<h4>
					<span><button type="button" id="saleConfigBtn" class="pro_compar_btn" value="<fmt:message key="common.btn.add" />"><fmt:message key="common.btn.add" /></button>
					</span><fmt:message key="trademan.rechargecard.saleconfig" />:
				</h4>
			</div>
			
			<div class="act_tj">
				<table width="100%" border="0" bordercolor="#dddddd" id="configTable">
					<tr class="title">
						<td width="25%"><fmt:message key="common.list.code" /></td>
						<td width="25%"><fmt:message key="marketman.saleconfigman.name" /></td>
						<td width="25%"><fmt:message key="product.producttypevo" /></td>
						<td width="25%"><fmt:message key="common.list.operation" /></td>
					</tr>
				</table>
			</div>
			
			
			
			<div id="areaDiv" class="active">
				<div class="dialog_overlay"></div>
				<div class="dialog_win">
					<h2>
						<span class="dialog_close">×</span>
					</h2>
					<div class="tc_tab">
						<table width="100%" border="0">
							<tr>
								<td width="40%" align="right"><fmt:message key="syssetting.areaconfig" />:</td>
								<td><select id="areaSelect" multiple="multiple" size="20">
									<c:forEach items="${areaConfigs }" var="item">
										<option value="${item.code }">${item.name }</option>
									</c:forEach>
								</select>
								</td>
							</tr>
							<tr>
								<td width="40%" align="right">&nbsp;</td>
								<td><input id="areaFinishBtn" type="button" value="<fmt:message key="common.btn.ok" />"
									class="diw_but_tc" />
								</td>
							</tr>
						</table>
					</div>
				</div>

				<h4>
					<span><button type="button" id="areaBtn" class="pro_compar_pz" value="<fmt:message key="common.btn.add" />"><fmt:message key="common.btn.add" /></button>
					</span><fmt:message key="marketman.promotion.areaconfig" />:<input id="allArea" name="baseInfo.allArea" type="checkbox" value="1" /> <fmt:message key="marketman.promotion.allareaconfig" />
				</h4>
			</div>

			<div class="act_tj">
				<table width="100%" border="0" bordercolor="#dddddd" id="areaTable">
					<tr class="title">
						<td width="34%"><fmt:message key="common.list.code" /></td>
						<td width="34%"><fmt:message key="syssetting.areaconfig" /></td>
						<td width="32%"><fmt:message key="common.list.operation" /></td>
					</tr>
				</table>
				<div style="padding-top: 20px;">
					<input id="finishBtn" type="button" value="<fmt:message key="common.btn.ok" />" class="diw_but" /><input
						 type="button" value="<fmt:message key="common.btn.return" />" class="diw_but" onclick="jump('toMarkerPromotion.do?search.promotionType==${search.promotionType}');"/>
				</div>
			</div>
		</div>
	</div>
	</form>
<script type="text/javascript">
	var timeSelect = new selfOpenDialog({id:"timeSelectDiv",btnId:"timeBtn"});
	
	var saleConfig = new selfOpenDialog({id:"saleConfigDiv",btnId:"saleConfigBtn"});
	
	var saleUnit = new selfOpenDialog({id:"saleUnitDiv",btnId:"saleUnitBtn"});
	
	var areaDiv = new selfOpenDialog({id:"areaDiv",btnId:"areaBtn"});
	
	//加载小时
	$(".hour").each(function(){
		var prefix ;
		for(var i = 0 ; i < 24 ; i++){
			prefix = "0";
			if(i < 10){
				prefix = prefix+i;
			}else{
				prefix = i;
			}
			var option = $("<option value='"+prefix+"'>"+prefix+"</option>");
			$(this).append(option);
		}
	});
	
	//加载分钟
	$(".minute").each(function(){
		var prefix ;
		for(var i = 0 ; i < 60 ; i++){
			prefix = "0";
			if(i < 10){
				prefix = prefix+i;
			}else{
				prefix = i;
			}
			var option = $("<option value='"+prefix+"'>"+prefix+"</option>");
			$(this).append(option);
		}
	});
	
	function jump(url){
		var e=document.createElement("a");
		e.href=url;
		document.body.appendChild(e);
		e.click();
	}
</script>

