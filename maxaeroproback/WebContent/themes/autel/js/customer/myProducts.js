var cusmenu=1;

function WidthCheck(str, maxLen){  
	var w = 0;  
	var tempCount = 0;
	for (var i=0; i<str.value.length; i++) {  
	   var c = str.value.charCodeAt(i);  
	   if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f)) {  
	    w++;    
	   }else {    
	    w+=2;  
	  
	   }   
	   tempCount=w;
	   if (w > maxLen) {  
	   str.value = str.value.substr(0,i);
	   break;
	   }    
	}  

	}
function addRemark(proCode,proSN,remark){
	
	var remarkDesc=$('#remarkDesc').val();
	var OKValue=$('#OKValue').val();
	var CancelValue=$('#CancelValue').val();
	
	var dialog = art.dialog({
	    content: '<p>'+remarkDesc+'</p>'
	    	+ '<input id="demo-labs-input" onchange="WidthCheck(this,40);" onkeyup="WidthCheck(this,40);" maxlength="46" style="width:30em; padding:4px" value="'+remark+'"/>',
	    fixed: true,
	    id: 'Fm7',
	    icon: 'question',
	    title:'',
	    width:'400px',
	    cancelVal:CancelValue,
	    okVal: OKValue,
	    lock:true,
	    ok: function () {
	    	var input = document.getElementById('demo-labs-input');
	        
	    	if (input.value == '') {
	            this.shake && this.shake();// 调用抖动接口
	            input.select();
	            input.focus();
	            return false;
	        } else {
	        	ajaxSubmit(proCode,input.value);
	        	jump(window.location.href);
	            
	        };
	    },
	    cancel: true
	});




}

function ajaxSubmit(proCode,remark){
	
	var resultCode="SUCCESS";
	
	$.ajax({
		url:'front/usercenter/customer/updateProductRemark.do',
		type:"post",
		async:false,
		data:{"regProductInfoVO.proCode":proCode,"regProductInfoVO.remark":remark},
		dataType:'JSON',
		success :function(data)
		{
			var jsonData=data;
			if(jsonData == "SUCCESS")
			{
				resultCode="SUCCESS";
			}
			else
			{
				resultCode="FAIL";
			}
		},
		error :function(){
			resultCode="FAIL";
		}
	});
	return resultCode;
}


/**
 * 我的产品-软件续租按钮点击事件
 * @param proSerialNo
 * @param proName
 * @param saleContractCode
 * @param proCode
 * @returns
 */
function Extend(proSerialNo,proName,saleContractCode,proCode,picPath){
   //	window.location.replace("rentSoftware-1-1.html?serial="+proSerialNo+"&proName="+proName+"&saleContractCode="+saleContractCode+"&proCode="+proCode+"&picPath="+picPath);
   jump("rentSoftware-1-1.html?serial="+proSerialNo+"&proName="+proName+"&saleContractCode="+saleContractCode+"&proCode="+proCode+"&picPath="+picPath);
}


function getSoftwareList(proSerialNo,proName,saleContractCode,proCode,picPath){
	
	jump("softwareList.html?serialNo="+proSerialNo+"&saleCfgCode="+saleContractCode+"&type=1");
	
}

function changePageNo(module){
	var remark=$('#remark').val();
	jump("myProducts-1-1.html?m=1&remark="+remark+"&pageSize="+module);
}

function jump(url){

	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
}

/**
 * 我的产品-软件购买按钮点击事件
 * @param serial
 * @param proName
 * @param saleContractCode
 * @param proCode
 * @returns
 */
function Add(serial,proName,saleContractCode,proCode,picPath){
	//window.location.replace("buySoftware-1-1.html?serial="+serial+"&proName="+proName+"&saleContractCode="+saleContractCode+"&proCode="+proCode+"&picPath="+picPath);
	jump("buySoftware-1-1.html?serial="+serial+"&proName="+proName+"&saleContractCode="+saleContractCode+"&proCode="+proCode+"&picPath="+picPath);
}