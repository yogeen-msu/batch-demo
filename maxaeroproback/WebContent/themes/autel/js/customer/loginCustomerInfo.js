$(document).ready(function()
{
	$("#loginCheck").click(function()
	{
		var userType = $("#userType").val();
		var autelId = $("#autelId").val();
		var password = $("#password").val();
		var imagenum = $("#imagenum").val();
		var $form = $("#loginCustomerInfo");
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var userTypeTip = $("#userTypeTip");
		var autelIdTip = $("#autelIdTip");
		var passwordTip = $("#passwordTip");
		var imagenumTip = $("#imagenumTip");
		
		if(userType == -1)
		{
			userTypeTip.html("请选择用户类型!");
			return;
		}
		
		//验证通过后清除错误提示信息
		userTypeTip.html("");
		
		if(autelId == null || autelId == "")
		{
			autelIdTip.html("登录账号不能为空!");
			return;
		}
		
		autelIdTip.html("");
		
		if(userType == 1)
		{
			if(!regEmail.test(autelId))
			{
				autelIdTip.html("账号必须是你注册时的邮箱账户!");
				return;
			}
		}
		autelIdTip.html("");
		
		if(password == null || password == "")
		{
			passwordTip.html("密码不能为空!");
			return;
		}
		else if(password.length < 6)
		{
			passwordTip.html("密码长度不能少于6位!");
			return;
		}
		
		passwordTip.html("");
		
		if(imagenum == null || imagenum == "")
		{
			imagenumTip.html("验证码不能为空!");
			return;
		}
		
		imagenumTip.html("");
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/checkUserLogin.do',
			type:"post",
			async:false,
			data:{"userInfoVo.userType":userType,"userInfoVo.autelId":autelId,"userInfoVo.userPwd":password,"userInfoVo.imageCode":imagenum},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				var userPwd = jsonData[0].userPwd;
				var actCode = jsonData[0].actCode;
				var imageCode = jsonData[0].imageCode;
				
				if(imageCode == "false")
				{
					imagenumTip.html("您输入的验证码不正确!");
					resultCode = false;
					return;
				}
				
				imagenumTip.innerHTML="";
				
				if(autelId == "false")
				{
					autelIdTip.html("您输入的账号不存在!");
					resultCode = false;
					return;
				}
				
				if(userPwd == "false")
				{
					passwordTip.html("您输入的密码错误!");
					resultCode = false;
					return;
				}
				
				if(actCode == "false")
				{
					autelIdTip.html("您的账号还未激活!");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				autelIdTip.html("系统异常、请联系管理员....");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		
		
		$form.action="loginCustomerInfoResult.html?operationType=4";
		$form.submit();
	});
	
	$("#imageField").click(function()
	{
		jump("regCustomerInfo.html?operationType=1");
	});
});

function enterkey(e)
{
    if (e==13||e==32)
    {
    	$("#loginCheck").click();
    }
}
