<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){

	var autelIdResultTip =$("#autelIdResultTip");
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	
	if($("#autelId").val() == null || $("#autelId").val() == ""){
		$("#autelId").val("<fmt:message key='customerinfo.user.forexample'/>:steve@auteltech.com");
	}
	
	if(regEmail.test($("#autelIdValue").val())){
		$("#autelId").attr("disabled","true");
		$("#checkUpdateCustomerInfo").hide();
	}
	
	$("#checkUpdateCustomerInfo").click(function()
	{
		var code = $("#code").val();
		var autelId = $("#autelId").val();
		var sendEmail = $("#sendEmail").val();
		var actCode = $("#actCode").val();
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_.,&'""()\s]+$/i;
		var regNumber = /^[0-9]\d*$/;
		var regComUsername = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		

		var operationResultTip = $("#operationResultTip");

		
		operationResultTip.html("");
		
		clearAllErrorInfo();
		var isFlag=true;
		
		
	
	
		if(regEmail.test($("#autelIdValue").val()))
		{
			$("#autelIdResult").show();
			$("#autelIdImg").show();
			autelIdResultTip.html("<fmt:message key='accountinformation.firstemail.isemailalready'/>");
			isFlag =false;
		}
		
		if(autelId == null || autelId =="")
		{
			$("#autelIdResult").show();
			$("#autelIdImg").show();
			autelIdResultTip.html("<fmt:message key='accountinformation.firstemail.isnotnull'/>");
			if(isFlag){
				$("#autelId").focus();
			}
			isFlag =false;
		}
		
		if(autelId != null && autelId !="")
		{
			if(!regEmail.test(autelId))
			{
				$("#autelIdResult").show();
				$("#autelIdImg").show();
				autelIdResultTip.html("<fmt:message key='accountinformation.firstemail.illegalcharactercheck'/>");
				if(isFlag){
				 $("#autelId").focus();
				}
				isFlag =false;
			}
			
			operationResultTip.html("");
			
			if(autelId==sendEmail)
			{
				$("#autelIdResult").show();
				$("#autelIdImg").show();
				autelIdResultTip.html("<fmt:message key='accountinformation.firstandsecondemail.isthesame2'/>");
				if(isFlag){
				 $("#autelId").focus();
				}
				isFlag =false;
			}
			
		}
	
		operationResultTip.html("");
		
		var resultCode = true;
		
		if(!isFlag){
			return;
		}
		
		$.ajax({
			url:'front/user/updateCustomerInfoNew.do',
			type:"post",
			async:false,
			data:{"customerInfoEdit.autelId":autelId,"customerInfoEdit.code":code,"customerInfoEdit.actCode":actCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				
				if(result == "1")
				{
					if(autelId != $("#autelIdValue").val()){
						operationResultTip.html("<fmt:message key='accountinformation.update.success'/></br>"+"<fmt:message key='accountinformation.updateFirstEmail.success'/>  "+autelId+"  <fmt:message key='accountinformation.updateFirstEmail.success2'/>");
					}else{
						operationResultTip.html("<fmt:message key='accountinformation.update.success'/>");
					}
					
					resultCode = true;
					return;
				}
				else if(result == "2")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.fail'/>");
					resultCode = false;
					return;
				}
				else if(result == "3")
				{
					$("#autelIdResult").show();
					$("#autelIdImg").show();
					autelIdResultTip.html("<fmt:message key='accountinformation.firstEmailIsUse.name'/>");
					$("#autelId").focus();
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});

});

function clearAllErrorInfo(){
	
	$("#autelIdResult").hide();
	$("#autelIdImg").hide();
	
}