$(document).ready(function(){
	
	$("#resetPasswordCheck").click(function()
	{
		var autelId = $("#autelId").val();
		var userPwd = $("#userPwd").val();
		var confirmPassword = $("#confirmPassword").val();
		var $form = $("#resetPasswordForm");
		var passwordTip = $("#passwordTip");
		var password1Tip = $("#password1Tip");
		
	
		if(userPwd == null || userPwd == "")
		{
			passwordTip.html("新密码不能为空");
			return;
		}
		else if(userPwd.length < 6)
		{
			passwordTip.html("新密码长度不能少于6位!");
			return;
		}
		
		passwordTip.html("");
		
		if(confirmPassword == null || confirmPassword == "")
		{
			password1Tip.html("确认密码不能为空!");
			return;
		}
		else if(confirmPassword.length < 6)
		{
			password1Tip.html("确认长度不能少于6位!");
			return;
		}
		
		password1Tip.html("");
		
		if(userPwd != confirmPassword)
		{
			password1Tip.html("新密码与确认密码不一致!");
			return;
		}
		
		
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/resetPasswordCheck.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				
				if(jsonData[0].result == "false")
				{
					password1Tip.html("对不起，用户账号不存在!");
					resultCode = false;
					return;
				}
				
				if(jsonData[0].actStateResult == "false")
				{
					password1Tip.html("对不起，您用户账号未激活!");
					resultCode = false;
					return;
				}
				
				if(jsonData[0].sendResetPasswordResult == "false")
				{
					password1Tip.html("对不起，此链接失效。超过密码重置时间，请重新重置密码找回操作!");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				password1Tip.html("系统异常、请联系管理员....");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		
		$form.action="resetPasswordResult.html";
		$form.submit();
	});
});
