$(document).ready(function(){
	
	$("#checkQuestionAnswer").click(function()
	{
		var autelId = $("#autelId").val();
		var answerValue = $("#answer").val();
		var $form = $("#selectQuestionForgotFrom");
		var answerTip = $("#answerTip");
		
		if(autelId == null || autelId == "")
		{
			answerTip.html("用户账号不能为空");
			return;
		}
		answerTip.html("");
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/getCustomerInfo.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var answer = jsonData[0].customerInfo.answer;
				
				if(answer != answerValue)
				{
					answerTip.html("您回答问题错误!");
					resultCode = false;
					return;
				}
				
				
				
			},
			error :function(){
				answerTip.html("系统异常、请联系管理员....");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		
		
		$form.action="selectQuestionForgotPasswordResult.html";
		$form.submit();
	});
});