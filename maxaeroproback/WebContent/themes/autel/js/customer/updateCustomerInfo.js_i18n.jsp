<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){
	var firstNameResultTip=$("#firstNameResultTip");
	var middleNameResultTip=$("#middleNameResultTip");
	var lastNameResultTip=$("#lastNameResultTip");
	var secondEmailResultTip =$("#secondEmailResultTip");
	var comUsernameResultTip=$("#comUsernameResultTip");
	var questionCodeResultTip=$("#questionCodeResultTip");
	var answerResultTip=$("#answerResultTip");
	var countryResultTip=$("#countryResultTip");
	var addressResultTip=$("#addressResultTip");
	var cityResultTip=$("#cityResultTip");
	var companyResultTip=$("#companyResultTip");
	var zipCodeResultTip=$("#zipCodeResultTip");
	var languageCodeResultTip=$("#languageCodeResultTip");
	var dayTimeResultTip=$("#dayTimeResultTip");
	var mobileResultTip=$("#mobileResultTip");
	var second = $("#secondEmail").val();
	
	var regEmailTest = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	if(regEmailTest.test($("#autelId").val())){
		$("#isshowEdit").hide();
	}else{
	    $("#isshowEdit").show();
	}
	
	if(second == null || second == ""){
	
		$("#secondEmail").val("<fmt:message key='customerinfo.user.forexample'/>:steve@auteltech.com");
	}
	
	if('${local}'=='en_US')
	{
		$("#middlenametr").show();
		$("#middleNameDiv").show();
	}
	
	$("#checkUpdateCustomerInfo").click(function()
	{
		var autelId = $("#autelId").val();
		var firstName = $("#firstName").val();
		var middleName = $("#middleName").val();
		var lastName = $("#lastName").val();
		var secondEmail = $("#secondEmail").val();
		var comUsername = $("#comUsername").val();
		var languageCode = $("#languageCode").val();
		var address = $("#address").val();
		var country = $("#country").val();
		var company = $("#company").val();
		var zipCode = $("#zipCode").val();
		var city = $("#city").val();
		var questionCode = $("#questionCode").val();
		var answer = $("#answer").val();
		var mobilePhone = $("#mobilePhone").val();
		var mobilePhoneAC = $("#mobilePhoneAC").val();
		var mobilePhoneCC = $("#mobilePhoneCC").val();
		var daytimePhone = $("#dayTimePhone").val();
		var daytimePhoneAC = $("#dayTimePhoneAC").val();
		var daytimePhoneCC = $("#dayTimePhoneCC").val();
		var IsAllowSendEmail = $("#isAllowSendEmail").val();
		var actCode = $("#actCode").val();
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_.,&'""()\s]+$/i;
		var regNumber = /^[0-9]\d*$/;
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regComUsername = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		

		var operationResultTip = $("#operationResultTip");

		
		operationResultTip.html("");
		
		clearAllErrorInfo();
		var isFlag=true;
		if(firstName == null || firstName == "")
		{
			$("#firstNameResult").show();
			$("#firstNameImg").show();
			firstNameResultTip.html("<fmt:message key='accountinformation.firstname.isnotnull'/>");
			if(isFlag){
				 $("#firstName").focus();
			}
			isFlag =false;
		}
		else
		{
			if(!regName.test(firstName))
			{
				$("#firstNameResult").show();
				$("#firstNameImg").show();
				firstNameResultTip.html("<fmt:message key='accountinformation.firstname.illegalcharactercheck'/>");
				if(isFlag){
				 $("#firstName").focus();
				}
				isFlag =false;
			}
		}
		
		
		if(middleName != null && middleName !="")
		{
			if(!regName.test(middleName))
			{
				$("#middleNameResult").show();
				$("#middleNameImg").show();
				middleNameResultTip.html("<fmt:message key='accountinformation.middlename.illegalcharactercheck'/>");
				if(isFlag){
				 $("#middleName").focus();
				}
				isFlag =false;
			}
		}
		
		
		if(lastName == null || lastName == "")
		{
			$("#lastNameResult").show();
			$("#lastNameImg").show();
			lastNameResultTip.html("<fmt:message key='accountinformation.lastname.isnotnull'/>");
			if(isFlag){
				 $("#lastName").focus();
			}
			isFlag =false;
		}
		else
		{
			if(!regName.test(lastName))
			{
				$("#lastNameResult").show();
				$("#lastNameImg").show();
				lastNameResultTip.html("<fmt:message key='accountinformation.lastname.illegalcharactercheck'/>");
				if(isFlag){
				 $("#lastName").focus();
				}
				isFlag =false;
			}
		}
		
	
		if(secondEmail == null || secondEmail =="")
		{
			$("#secondEmailResult").show();
			$("#secondEmailImg").show();
			secondEmailResultTip.html("<fmt:message key='accountinformation.secondemail.isnotnull'/>");
			if(isFlag){
				$("#secondEmail").focus();
			}
			isFlag =false;
		}
		
		if(secondEmail != null && secondEmail !="")
		{
			if(!regEmail.test(secondEmail))
			{
				$("#secondEmailResult").show();
				$("#secondEmailImg").show();
				secondEmailResultTip.html("<fmt:message key='accountinformation.secondemail.illegalcharactercheck'/>");
				if(isFlag){
				 $("#secondEmail").focus();
				}
				isFlag =false;
			}
			
			operationResultTip.html("");
			
			if(secondEmail==autelId)
			{
				$("#secondEmailResult").show();
				$("#secondEmailImg").show();
				secondEmailResultTip.html("<fmt:message key='accountinformation.firstandsecondemail.isthesame'/>");
				if(isFlag){
				 $("#secondEmail").focus();
				}
				isFlag =false;
			}
			
		}
		
		if(questionCode == -1)
		{
			$("#questionCodeResult").show();
			$("#questionCodeImg").show();
			questionCodeResultTip.html("<fmt:message key='accountinformation.selectanswer.name'/>");
			if(isFlag){
				 $("#questionCode").focus();
			}
			isFlag =false;
		}
		
		
		operationResultTip.html("");
		
		if(answer == null || answer == "")
		{
			$("#answerResult").show();
			$("#answerImg").show();
			answerResultTip.html("<fmt:message key='accountinformation.answer.isnotnull'/>");
			if(isFlag){
				 $("#answer").focus();
			}
			isFlag =false;
		}
		else if(!regName.test(answer))
		{
			$("#answerResult").show();
			$("#answerImg").show();
			answerResultTip.html("<fmt:message key='accountinformation.answer.illegalcharactercheck'/>");
			if(isFlag){
				 $("#answer").focus();
			}
			isFlag =false;
		}
		
		if(country == null || country == "")
		{
			$("#countryResult").show();
			$("#countryImg").show();
			countryResultTip.html("<fmt:message key='accountinformation.country.isnotnull'/>");
			if(isFlag){
				 $("#country").focus();
			}
			isFlag =false;
		}
		
		
	
		
	
		
	
		
		operationResultTip.html("");
		
		if(daytimePhoneCC !=null && daytimePhoneCC!="")
		{
			if(!regNumber.test(daytimePhoneCC))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.countrycode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(daytimePhoneAC !=null && daytimePhoneAC!="")
		{
			if(!regNumber.test(daytimePhoneAC))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.areacode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(daytimePhone !=null && daytimePhone!="")
		{
			if(!regNumber.test(daytimePhone))
			{
				$("#dayTimeResult").show();
				$("#dayTimeImg").show();
				dayTimeResultTip.html("<fmt:message key='accountinformation.phone.illegalcharactercheck'/>");
				if(isFlag){
				 $("#dayTimePhoneCC").focus();
				}
				isFlag =false;
			}
		}
	
		operationResultTip.html("");
		
		if(mobilePhoneCC !=null && mobilePhoneCC!="")
		{
			if(!regNumber.test(mobilePhoneCC))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.countrycode.illegalcharactercheck'/>");
				
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(mobilePhoneAC !=null && mobilePhoneAC!="")
		{
			if(!regNumber.test(mobilePhoneAC))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.areacode.illegalcharactercheck'/>");
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		operationResultTip.html("");
		
		if(mobilePhone !=null && mobilePhone!="")
		{
			if(!regNumber.test(mobilePhone))
			{
				$("#mobileResult").show();
				$("#mobileImg").show();
				mobileResultTip.html("<fmt:message key='accountinformation.phone.illegalcharactercheck'/>");
				if(isFlag){
				 $("#mobilePhoneCC").focus();
				}
				isFlag =false;
			}
		}
		
		if(languageCode == -1)
		{
			$("#languageCodeResult").show();
			$("#languageCodeImg").show();
			languageCodeResultTip.html("<fmt:message key='accountinformation.selectlanguage.name'/>");
			if(isFlag){
				 $("#languageCode").focus();
			}
			isFlag =false;
		}
	
		operationResultTip.html("");
		
		var resultCode = true;
		
		if(!isFlag){
			return;
		}
		
		$.ajax({
			url:'front/user/updateCustomerInfo.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId,"userInfoVo.firstName":firstName,"userInfoVo.middleName":middleName,
				"userInfoVo.lastName":lastName,"userInfoVo.comUsername":comUsername,"userInfoVo.secondEmail":secondEmail,"userInfoVo.languageCode":languageCode,
				"userInfoVo.address":address,"userInfoVo.country":country,"userInfoVo.company":company,
				"userInfoVo.zipCode":zipCode,"userInfoVo.city":city,"userInfoVo.questionCode":questionCode,
				"userInfoVo.answer":answer,"userInfoVo.mobilePhone":mobilePhone,"userInfoVo.mobilePhoneAC":mobilePhoneAC,
				"userInfoVo.mobilePhoneCC":mobilePhoneCC,"userInfoVo.daytimePhone":daytimePhone,"userInfoVo.daytimePhoneAC":daytimePhoneAC,
				"userInfoVo.daytimePhoneCC":daytimePhoneCC,"userInfoVo.IsAllowSendEmail":IsAllowSendEmail,"userInfoVo.actCode":actCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				
				if(result == "1")
				{
					if(secondEmail != $("#secondEmailValue").val()){
						operationResultTip.html("<fmt:message key='accountinformation.update.success'/></br>"+"<fmt:message key='accountinformation.updateSecondEmail.success'/>  "+secondEmail+"  <fmt:message key='accountinformation.updateSecondEmail.success2'/>");
					}else{
						operationResultTip.html("<fmt:message key='accountinformation.update.success'/>");
					}
					
					resultCode = true;
					return;
				}
				else if(result == "2")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.fail'/>");
					resultCode = false;
					return;
				}
				else if(result == "3")
				{
					$("#secondEmailResult").show();
					$("#secondEmailImg").show();
					secondEmailResultTip.html("<fmt:message key='accountinformation.secondEmailIsUse.name'/>");
					$("#secondEmail").focus();
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});

});

function setEmail(obj)
{
	if(obj.checked)
	{
		$("#isAllowSendEmail").val(1);
	}
	else
	{
		$("#isAllowSendEmail").val(0);
	}
}

function clearAllErrorInfo(){
	
	$("#firstNameResult").hide();
	$("#firstNameImg").hide();
	
	$("#middleNameResult").hide();
	$("#middleNameImg").hide();
	
	$("#lastNameResult").hide();
	$("#lastNameImg").hide();
	
	$("#secondEmailResult").hide();
	$("#secondEmailImg").hide();
	
	$("#comUsernameResult").hide();
	$("#comUsernameImg").hide();
	
	$("#questionCodeResult").hide();
	$("#questionCodeImg").hide();
	
	$("#answerResult").hide();
	$("#answerImg").hide();
	
	$("#countryResult").hide();
	$("#countryImg").hide();
	
	$("#addressResult").hide();
	$("#addressImg").hide();
	
	$("#cityResult").hide();
	$("#cityImg").hide();
	
	$("#companyResult").hide();
	$("#companyImg").hide();
	
	$("#zipCodeResult").hide();
	$("#zipCodeImg").hide();
	
	$("#languageCodeResult").hide();
	$("#languageCodeImg").hide();
	
	$("#mobileResult").hide();
	$("#mobileImg").hide();
	
	$("#dayTimeResult").hide();
	$("#dayTimeImg").hide();
	
	
}
	
function clearsecondEmailValue(){
	if($("#secondEmail").val()=="<fmt:message key='customerinfo.user.forexample'/>:steve@auteltech.com")
	{
		$("#secondEmail").attr("value","");
	} 
}