<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){
	var secondEmailResultTip =$("#secondEmailResultTip");

	var second = $("#secondEmail").val();
	if(second == null || second == ""){
	
		$("#secondEmail").val("<fmt:message key='customerinfo.user.forexample'/>:steve@auteltech.com");
	}
	
	$("#checkUpdateCustomerInfo").click(function()
	{
		var autelId = $("#autelId").val();
		var code = $("#code").val();
		var secondEmail = $("#secondEmail").val();
		var actCode = $("#actCode").val();
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_.,&'""()\s]+$/i;
		var regNumber = /^[0-9]\d*$/;
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regComUsername = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		

		var operationResultTip = $("#operationResultTip");

		
		operationResultTip.html("");
		
		clearAllErrorInfo();
		var isFlag=true;
		
	
		if(secondEmail == null || secondEmail =="")
		{
			$("#secondEmailResult").show();
			$("#secondEmailImg").show();
			secondEmailResultTip.html("<fmt:message key='accountinformation.secondemail.isnotnull'/>");
			if(isFlag){
				$("#secondEmail").focus();
			}
			isFlag =false;
		}
		
		if(secondEmail != null && secondEmail !="")
		{
			if(!regEmail.test(secondEmail))
			{
				$("#secondEmailResult").show();
				$("#secondEmailImg").show();
				secondEmailResultTip.html("<fmt:message key='accountinformation.secondemail.illegalcharactercheck'/>");
				if(isFlag){
				 $("#secondEmail").focus();
				}
				isFlag =false;
			}
			
			operationResultTip.html("");
			
			if(secondEmail==autelId)
			{
				$("#secondEmailResult").show();
				$("#secondEmailImg").show();
				secondEmailResultTip.html("<fmt:message key='accountinformation.firstandsecondemail.isthesame'/>");
				if(isFlag){
				 $("#secondEmail").focus();
				}
				isFlag =false;
			}
			
		}
	
		operationResultTip.html("");
		
		var resultCode = true;
		
		if(!isFlag){
			return;
		}
		
		$.ajax({
			url:'front/user/updateCustomerInfoNew.do',
			type:"post",
			async:false,
			data:{"customerInfoEdit.secondEmail":secondEmail,"customerInfoEdit.code":code,"customerInfoEdit.actCode":actCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				
				if(result == "1")
				{
					if(secondEmail != $("#secondEmailValue").val()){
						operationResultTip.html("<fmt:message key='accountinformation.update.success'/></br>"+"<fmt:message key='accountinformation.updateSecondEmail.success'/>  "+secondEmail+"  <fmt:message key='accountinformation.updateSecondEmail.success2'/>");
					}else{
						operationResultTip.html("<fmt:message key='accountinformation.update.success'/>");
					}
					
					resultCode = true;
					return;
				}
				else if(result == "2")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.fail'/>");
					resultCode = false;
					return;
				}
				else if(result == "3")
				{
					$("#secondEmailResult").show();
					$("#secondEmailImg").show();
					secondEmailResultTip.html("<fmt:message key='accountinformation.secondEmailIsUse.name'/>");
					$("#secondEmail").focus();
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});

});

function setEmail(obj)
{
	if(obj.checked)
	{
		$("#isAllowSendEmail").val(1);
	}
	else
	{
		$("#isAllowSendEmail").val(0);
	}
}

function clearAllErrorInfo(){
	
	$("#firstNameResult").hide();
	$("#firstNameImg").hide();
	
	$("#middleNameResult").hide();
	$("#middleNameImg").hide();
	
	$("#lastNameResult").hide();
	$("#lastNameImg").hide();
	
	$("#secondEmailResult").hide();
	$("#secondEmailImg").hide();
	
	$("#comUsernameResult").hide();
	$("#comUsernameImg").hide();
	
	$("#questionCodeResult").hide();
	$("#questionCodeImg").hide();
	
	$("#answerResult").hide();
	$("#answerImg").hide();
	
	$("#countryResult").hide();
	$("#countryImg").hide();
	
	$("#addressResult").hide();
	$("#addressImg").hide();
	
	$("#cityResult").hide();
	$("#cityImg").hide();
	
	$("#companyResult").hide();
	$("#companyImg").hide();
	
	$("#zipCodeResult").hide();
	$("#zipCodeImg").hide();
	
	$("#languageCodeResult").hide();
	$("#languageCodeImg").hide();
	
	$("#mobileResult").hide();
	$("#mobileImg").hide();
	
	$("#dayTimeResult").hide();
	$("#dayTimeImg").hide();
	
	
}
	
function clearsecondEmailValue(){
	if($("#secondEmail").val()=="<fmt:message key='customerinfo.user.forexample'/>:steve@auteltech.com")
	{
		$("#secondEmail").attr("value","");
	} 
}