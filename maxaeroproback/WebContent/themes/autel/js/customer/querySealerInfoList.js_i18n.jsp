<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){
	
	$("#querySealerInfoSubmit").click(function()
	{
		var $form = $("#sealerInfoForm");
		var country = $("#country").val();
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_]+$/i;
		var resultTip = $("#resultTip");
		
			//去掉空格
		country = country.replace(/[ ]/g,"");
		$("#country").val(country);
		
		if(country != null && country != '')
		{
			if(!regName.test(country))
			{
				resultTip.show();
				resultTip.html("<fmt:message key='sealerinfo.country.illegalcharactercheck'/>");
				return;
			}
		}
		resultTip.hide();
		resultTip.html("");
		
		$form.action="querySealerInfoList-1-1.html?operationType=5";
		$form.submit();
	});
});