$(document).ready(function(){
	
	$("#checkUpdatePassword").click(function()
	{
		var autelId = $("#autelId").val();
		var id = $("#id").val();
		var oldPassword = $("#oldPassword").val();
		var userPwd = $("#userPwd").val();
		var confirmPassword = $("#confirmPassword").val();
		
		var passwordTip = $("#passwordTip");
		var oldPasswordTip = $("#oldPasswordTip");
		var confirmPasswordTip = $("#confirmPasswordTip");
		
		
		if(oldPassword == null || oldPassword == '')
		{
			oldPasswordTip.html("旧密码不能为空!");
			return;
		}
		else if(oldPassword.length < 6)
		{
			oldPasswordTip.html("旧密码长度不能少于6位!");
			return;
		}
		
		oldPasswordTip.html("");
		
		if(userPwd == null || userPwd == '')
		{
			passwordTip.html("新密码不能为空!");
			return;
		}
		else if(userPwd.length < 6)
		{
			passwordTip.html("新密码长度不能少于6位!");
			return;
		}
		passwordTip.html("");
		
		if(confirmPassword == null || confirmPassword == '')
		{
			confirmPasswordTip.html("确认密码不能为空!");
			return;
		}
		else if(confirmPassword.length < 6)
		{
			confirmPasswordTip.html("确认密码长度不能少于6位!");
			return;
		}
		
		confirmPasswordTip.html("");
		
		if(userPwd != confirmPassword)
		{
			confirmPasswordTip.html("新密码与确认密码不一致!");
			return;
		}
		
		confirmPasswordTip.html("");
	
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/updateCustomerPassword.do',
			type:"post",
			async:false,
			data:{"userInfoVo.userPwd":userPwd,"userInfoVo.oldPassword":oldPassword,"userInfoVo.autelId":autelId,"userInfoVo.id":id},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				var oldPassword = jsonData[0].oldPassword;
				
				if(oldPassword == "false")
				{
					oldPasswordTip.html("旧密码输入不正确!");
					resultCode = false;
					return;
				}
				
				
				if(result == "true")
				{
					confirmPasswordTip.html("修改密码成功!");
					resultCode = true;
				}
				else
				{
					confirmPasswordTip.html("修改密码失败!");
					resultCode = false;
				}
				
			},
			error :function(){
				oldPasswordTip.html("系统异常、请联系管理员....");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});
});