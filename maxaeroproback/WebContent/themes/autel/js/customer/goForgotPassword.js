$(document).ready(function(){
	
	$("#checkAutelId").click(function()
	{
		var autelId = $("#autelId").val();
		var $form = $("#goForgotPasswordForm");
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		
		var usernameTip = $("#usernameTip");
		
		if(autelId == null || autelId == "")
		{
			usernameTip.html("用户账号不能为空!");
			return;
		}
		else if(!regEmail.test(autelId))
		{
			usernameTip.html("用户账号格式不正确!");
			return;
		}
		
		usernameTip.html("");
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/checkAutelIdIsExist.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				
				if(autelId == "false")
				{
					usernameTip.html("您输入的用户账号不存在!");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				usernameTip.html("系统异常、请联系管理员....");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		
		$form.action="selectForgotPassword.html?operationType=11";
		$form.submit();
	});
});
	
	
	
