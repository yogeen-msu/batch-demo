	
	var handle;

	function start()
	{
		var obj = document.getElementById("tip");
		if (parseInt(obj.style.height)==0)
		{ 
			obj.style.display="block";
			handle = setInterval("changeH('up')",2);
		}
		else
		{
			handle = setInterval("changeH('down')",2);
		}
	}

	function changeH(str)
	{
		var obj = document.getElementById("tip");
		if(str=="up")
		{
			if (parseInt(obj.style.height)>200)
			{
				clearInterval(handle);
			}
			else
			{
				obj.style.height=(parseInt(obj.style.height)+8).toString()+"px";
			}
	
		}
	
		if(str=="down")
		{
			if (parseInt(obj.style.height)<8)
			{ 
				clearInterval(handle);
				obj.style.display="none";
			}
			else
			{
				obj.style.height=(parseInt(obj.style.height)-8).toString()+"px";
			}
		}
	}

	function showwin()
	{
		start();
		document.getElementById("shadow").style.display="block";
		document.getElementById("detail").style.display="block";
	}

	function recover()
	{
		document.getElementById("shadow").style.display="none";
		document.getElementById("detail").style.display="none";
	}
	
	function myTimer()
	{
	    start();
	    //window.setTimeout("myTimer()",6000);//设置循环时间
	}

	
	$(document).ready(function()
	{
		var userMessageIsNoDialogReadCount = 0;
		$.ajax({
			url:'front/user/queryDialogMessageCount.do',
			type:"post",
			async:false,
			data:{"userInfoVo.userType":$("#userType").val()},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				userMessageIsNoDialogReadCount = jsonData[0].userMessageIsNoDialogReadCount;
				
			},
			error :function(){
				alert("系统异常、请联系管理员....");
			}
		});
		
		
		if(userMessageIsNoDialogReadCount > 0)
		{
			
			document.getElementById('tip').style.height='0px';
			myTimer();
			//document.getElementById("message_count").innerHTML=messageCount;
			document.getElementById("message_count_id").innerHTML=userMessageIsNoDialogReadCount;
		}
	
	});


	

