<%@ include file="../common.js_i18n.jsp" %>
$(document).ready(function(){
	$("#resetPasswordCheck").click(function()
	{
		var autelId = $("#autelId").val();
		var actCode = $("#actCode").val();
		var userPwd = $("#userPwd").val();
		var confirmPassword = $("#confirmPassword").val();
		var $form = $("#resetPasswordForm");
		var regPwd=new RegExp(/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/);
	
		var passwordErrorInfoTip = $("#passwordErrorInfoTip");
		var confirmPasswordErrorInfoTip = $("#confirmPasswordErrorInfoTip");
		
		
		clearAllErrorInfo();
		
		if(autelId == null || autelId == "")
		{
			$("#confirmPasswordErrorInfo").show();
			$("#confirmPasswordImg").show();
			confirmPasswordErrorInfoTip.html("<fmt:message key='accountinformation.useraccount.isnotnull'/>");
			return;
		}
		
		if(userPwd == null || userPwd == "")
		{
			$("#passwordErrorInfo").show();
			$("#passwordImg").show();
			$("#passwordNull").hide();
			passwordErrorInfoTip.html("<fmt:message key='update.newpassword.isnotnull'/>");
			return;
		}
		else if(userPwd.length < 6)
		{
			$("#passwordErrorInfo").show();
			$("#passwordImg").show();
			$("#passwordNull").hide();
			passwordErrorInfoTip.html("<fmt:message key='update.newpassword.lengthcheck'/>");
			return;
		}
		else if(!regPwd.test(userPwd))
		{
			$("#passwordErrorInfo").show();
			$("#passwordImg").show();
			$("#passwordNull").hide();
			passwordErrorInfoTip.html("<fmt:message key='accountinformation.password.check'/>");
			return;
		}
		
		if(confirmPassword == null || confirmPassword == "")
		{
			$("#confirmPasswordErrorInfo").show();
			$("#confirmPasswordImg").show();
			confirmPasswordErrorInfoTip.html("<fmt:message key='update.confirmpassword.isnotnull'/>");
			return;
		}
		else if(confirmPassword.length < 6)
		{
			$("#confirmPasswordErrorInfo").show();
			$("#confirmPasswordImg").show();
			confirmPasswordErrorInfoTip.html("<fmt:message key='update.confirmpassword.lengthcheck'/>");
			return;
		}
		else if(!regPwd.test(confirmPassword))
		{
			$("#confirmPasswordErrorInfo").show();
			$("#confirmPasswordImg").show();
			confirmPasswordErrorInfoTip.html("<fmt:message key='accountinformation.password.check'/>");
			return;
		}
		
		
		if(userPwd != confirmPassword)
		{
			$("#confirmPasswordErrorInfo").show();
			$("#confirmPasswordImg").show();
			confirmPasswordErrorInfoTip.html("<fmt:message key='update.newpasswordandconfirmpassword.identical'/>");
			return;
		}
		
		
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/resetPasswordCheck.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId,"userInfoVo.actCode":actCode,"type":""},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				
				if(jsonData[0].result == "1")//账户未激活
				{
					$("#confirmPasswordErrorInfo").show();
					$("#confirmPasswordImg").show();
					confirmPasswordErrorInfoTip.html("<fmt:message key='accountinformation.useraccount.activeinvalid'/>");
					resultCode = false;
					return;
				}
				else if(jsonData[0].result == "2")//账户不存在或激活码错误
				{
					$("#confirmPasswordErrorInfo").show();
					$("#confirmPasswordImg").show();
					confirmPasswordErrorInfoTip.html("<fmt:message key='accountinformation.userandactcode.isnotexist'/>!");
					resultCode = false;
					return;
				}
				else if(jsonData[0].result == "3")//链接失效超过24小时
				{
					$("#confirmPasswordErrorInfo").show();
					$("#confirmPasswordImg").show();
					confirmPasswordErrorInfoTip.html("<fmt:message key='accountinformation.userpassword.resetinvalid'/>");
					resultCode = false;
					return;
				}
				else if(jsonData[0].result == "4")//校验通过
				{
					resultCode = true;
					return;
				}
				else if(jsonData[0].result == "5")//密码找回失败
				{
					$("#confirmPasswordErrorInfo").show();
					$("#confirmPasswordImg").show();
					confirmPasswordErrorInfoTip.html("<fmt:message key='common.system.error'/>");
					resultCode=false;
					return;
				}
			},
			error :function()
			{
				$("#confirmPasswordErrorInfo").show();
				$("#confirmPasswordImg").show();
				confirmPasswordErrorInfoTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		
		$form.action="resetPasswordResult.html";
		$form.submit();
	});
});

function clearAllErrorInfo()
{
	$("#passwordErrorInfoTip").html("");
	$("#passwordErrorInfo").hide();
	$("#passwordImg").hide();
	$("#passwordNull").show();
	
	$("#confirmPasswordErrorInfoTip").html("");
	$("#confirmPasswordErrorInfo").hide();
	$("#confirmPasswordImg").hide();

}
