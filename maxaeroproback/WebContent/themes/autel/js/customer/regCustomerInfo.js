$(document).ready(function(){
	
	$("#regCustomerCheck").click(function()
	{
		
		var $form = $("#regCustomerInfoForm");
		var autelId = $("#autelId").val();
		var password = $("#userPwd").val();
		var confirmPassword = $("#userPwd2").val();
		var questionCode = $("#questionCode").val();
		var languageCode = $("#languageCode").val();
		var answer = $("#answer").val();
		var firstName = $("#firstName").val();
		var middleName = $("#middleName").val();
		var lastName = $("#lastName").val();
		var secondEmail = $("#secondEmail").val();
		var country = $("#country").val();
		var comUsername = $("#comUsername").val();
		var agree = $("#agree").val();
		var imagenum = $("#imagenum").val();
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_]+$/i;
		
		var usernameTip = $("#usernameTip");
		var passwordTip = $("#passwordTip");
		var confirmPasswordTip = $("#confirmPasswordTip");
	    var firstNameTip = $("#firstNameTip");
	    var middleNameTip = $("#middleNameTip");
	    var lastNameTip = $("#lastNameTip");
	    var secondEmailTip = $("#secondEmailTip");
	    var comUsernameTip = $("#comUsernameTip");
	    var questionCodeTip = $("#questionCodeTip");
	    var answerTip = $("#answerTip");
	    var languageCodeTip = $("#languageCodeTip");
	    var imagenumCodeTip = $("#imagenumCodeTip");
	    var agreeTip = $("#agreeTip");
	    var countryTip = $("#countryTip");
		
		if(autelId == null || autelId == "")
		{
			usernameTip.html("账号不能为空!");
			return;
		}
		else if(!regEmail.test(autelId))
		{
			usernameTip.html("账号必须是邮箱账户!");
			return;
		}
		
		usernameTip.html("");
		
		if(password == null || password == "")
		{
			passwordTip.html("密码不能为空!");
			return;
		}
		else if(password.length < 6)
		{
			passwordTip.html("密码长度不能少于6位!");
			return;
		}
		
		passwordTip.html("");
		
		if(confirmPassword == null || confirmPassword == "")
		{
			confirmPasswordTip.html("确认密码不能为空!");
			return;
		}
		else if(confirmPassword.length < 6)
		{
			confirmPasswordTip.html("确认长度不能少于6位!");
			return;
		}
		
		confirmPasswordTip.html("");
		
		if(password != confirmPassword)
		{
			confirmPasswordTip.html("密码与确认密码不一致!");
			return;
		}
		
		confirmPasswordTip.html("");
		
		if(firstName != null && firstName != "")
		{
			if(!regName.test(firstName))
			{
				firstNameTip.html("FirstName不能有非法字符!");
				return;
			}
		}
		
		firstNameTip.html("");
		

		if(middleName != null && middleName !="")
		{
			if(!regName.test(middleName))
			{
				middleNameTip.html("middleName不能有非法字符!");
				return;
			}
		}
		
		middleNameTip.html("");
		
		if(lastName != null && lastName != "")
		{
			if(!regName.test(lastName))
			{
				lastNameTip.html("lastName不能有非法字符!");
				return;
			}
		}
	
		
		lastNameTip.html("");
		
		if(secondEmail != null && secondEmail !="")
		{
			if(!regEmail.test(secondEmail))
			{
				secondEmailTip.html("第二邮箱账户格式不正确!");
				return;
			}
			
			secondEmailTip.html("");
		}
		
		if(comUsername == null || comUsername == "")
		{
			comUsernameTip.html("真实姓名不能为空!");
			return;
		}
		else if(!regName.test(comUsername))
		{
			comUsernameTip.html("真实姓名不能有非法字符!");
			return;
		}
		
		comUsernameTip.html("");
		
		if(questionCode == -1)
		{
			questionCodeTip.html("Please Security Question!");
			return;
		}
		
		questionCodeTip.html("");
		
		if(answer == null || answer == "")
		{
			answerTip.html("答案不能为空!");
			return;
		}
		
		answerTip.html("");
		
		if(country == null || country == "")
		{
			countryTip.html("国家不能为空!");
			return;
		}
		
		countryTip.html("");
		
		if(languageCode == -1)
		{
			languageCodeTip.html("Please Choose Preferred Language!");
			return;
		}
		
		languageCodeTip.html("");
		
		if(imagenum == null || imagenum == "")
		{
			imagenumCodeTip.html("验证码不能为空!");
			return;
		}
		
		imagenumCodeTip.html("");
	
		if(agree == 0)
		{
			agreeTip.html("请勾选条款项!");
			return;
		}
		
		agreeTip.html("");
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/regCustomerCheck.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId,"userInfoVo.imageCode":imagenum},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				var imageCode = jsonData[0].imageCode;
				
				if(imageCode == "false")
				{
					imagenumCodeTip.html("您输入的验证码不正确!");
					resultCode = false;
					return;
				}
				
				if(autelId == "true")
				{
					usernameTip.html("您输入的用户账号已注册!");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				usernameTip.html("系统异常、请联系管理员....");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		
		
		$form.action="regCustomerInfoResult.html";
		$form.submit();
	});

});
	
function setEmail(obj)
{
	if(obj.checked)
	{
		$("#isAllowSendEmail").val(1);
	}
	else
	{
		$("#isAllowSendEmail").val(0);
	}
}

function setAgree(obj)
{
	if(obj.checked)
	{
		$("#agree").val(1);
	}
	else
	{
		$("#agree").val(0);
	}
}
	