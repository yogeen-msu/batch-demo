<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){
	var oldPasswordResultTip=$("#oldPasswordResultTip");
	var userPwdResultTip=$("#userPwdResultTip");
	var confirmPasswordResultTip=$("#confirmPasswordResultTip");
	
	$("#checkUpdatePassword").click(function()
	{
		var autelId = $("#autelId").val();
		var id = $("#id").val();
		var oldPassword = $("#oldPassword").val();
		var userPwd = $("#userPwd").val();
		var confirmPassword = $("#confirmPassword").val();
		
		var confirmPasswordTip = $("#confirmPasswordTip");
		var regPwd=new RegExp(/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/);
		
		clearAllErrorInfo();
		var isFlag=true;
		if(oldPassword == null || oldPassword == '')
		{
			$("#oldPasswordResult").show();
			$("#oldPasswordImg").show();
			oldPasswordResultTip.html("<fmt:message key='update.oldpassword.isnotnull'/>");
			isFlag=false;
		}
		else if(oldPassword.length < 6)
		{
			$("#oldPasswordResult").show();
			$("#oldPasswordImg").show();
			oldPasswordResultTip.html("<fmt:message key='update.oldpassword.lengthcheck'/>");
			isFlag=false;
		}
		confirmPasswordTip.html("");
		
		if(userPwd == null || userPwd == '')
		{
			$("#userPwdResult").show();
			$("#userPwdImg").show();
			userPwdResultTip.html("<fmt:message key='update.newpassword.isnotnull'/>");
			isFlag=false;
		}
		else if(userPwd.length < 6)
		{
			$("#userPwdResult").show();
			$("#userPwdImg").show();
			userPwdResultTip.html("<fmt:message key='update.newpassword.lengthcheck'/>");
			isFlag=false;
		}else if(!regPwd.test(userPwd)){
			$("#userPwdResult").show();
			$("#userPwdImg").show();
			userPwdResultTip.html("<fmt:message key='accountinformation.password.check'/>");
			isFlag=false;
  		}
  		
		confirmPasswordTip.html("");
		
		if(confirmPassword == null || confirmPassword == '')
		{
			$("#confirmPasswordResult").show();
			$("#confirmPasswordImg").show();
			confirmPasswordResultTip.html("<fmt:message key='update.confirmpassword.isnotnull'/>");
			isFlag=false;
		}
		else if(confirmPassword.length < 6)
		{
			$("#confirmPasswordResult").show();
			$("#confirmPasswordImg").show();
			confirmPasswordResultTip.html("<fmt:message key='update.confirmpassword.lengthcheck'/>");
			isFlag=false;
		}
		
		confirmPasswordTip.html("");
		
		if(userPwd != confirmPassword)
		{
			$("#confirmPasswordResult").show();
			$("#confirmPasswordImg").show();
			confirmPasswordResultTip.html("<fmt:message key='update.newpasswordandconfirmpassword.identical'/>");
			isFlag=false;
		}
		
		confirmPasswordTip.html("");
	
		
		var resultCode = true;
		if(!isFlag){
			return;
		}
		$.ajax({
			url:'front/user/updateCustomerPassword.do',
			type:"post",
			async:false,
			data:{"userInfoVo.userPwd":userPwd,"userInfoVo.oldPassword":oldPassword,"userInfoVo.autelId":autelId,"userInfoVo.id":id},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				var oldPassword = jsonData[0].oldPassword;
				
				if(oldPassword == "false")
				{
					$("#oldPasswordResult").show();
					$("#oldPasswordImg").show();
					oldPasswordResultTip.html("<fmt:message key='update.oldpassword.iserror'/>");
					resultCode = false;
					return;
				}
				
				
				if(result == "true")
				{
					confirmPasswordTip.html("<fmt:message key='update.password.success'/>");
					resultCode = true;
				}
				else
				{
					confirmPasswordTip.html("<fmt:message key='update.password.fail'/>");
					resultCode = false;
				}
				
			},
			error :function(){
				confirmPasswordTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});
	
    $("#canel").click(function(){
		     $("#oldPasswordImg").hide();
		     $("#oldPasswordResult").hide();
		     $("#confirmPasswordImg").hide();
		     $("#confirmPasswordResult").hide();
		     $("#userPwdImg").hide();
		     $("#userPwdResult").hide();
			clearInfo();
    });
	
	
});

function clearInfo(){
	var confirmPasswordTip = $("#confirmPasswordTip");
	confirmPasswordTip.html("");
}

function clearAllErrorInfo(){	
	$("#oldPasswordResult").hide();
	$("#oldPasswordImg").hide();
	
	$("#userPwdResult").hide();
	$("#userPwdImg").hide();
	
	$("#confirmPasswordResult").hide();
	$("#confirmPasswordImg").hide();
	clearInfo();
}
