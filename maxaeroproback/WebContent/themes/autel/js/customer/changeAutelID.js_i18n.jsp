<%@ include file="../common.js_i18n.jsp" %>
$(document).ready(function(){
	$("#changeAutelCheck").click(function()
	{
		var autelId = $("#autelId").val();
		var productSN = $("#productSN").val();
		var productPwd = $("#productPwd").val();
		var newAutelId = $("#newAutelId").val();
	
		var $form = $("#changeAutelForm");
	    var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;	
		
		var productErrorInfoTip = $("#productErrorInfoTip");
		var passwordErrorInfoTip = $("#passwordErrorInfoTip");
		var newAutelIdErrorInfoTip=  $("#newAutelIdErrorInfoTip");
		var imagenumErrorInfoTip=$("#imagenumErrorInfoTip");
		
		clearAllErrorInfo();
		
		if(productSN == null || productSN == "")
		{
			$("#productErrorInfo").show();
			$("#productSNImg").show();
			productErrorInfoTip.html("<fmt:message key='customer.change.sn.null'/>");
			return;
		}
		
		if(productPwd == null || productPwd == "")
		{
			$("#passwordErrorInfo").show();
			$("#productPwdImg").show();
			passwordErrorInfoTip.html("<fmt:message key='customer.change.pwd.null'/>");
			return;
		}
		
		if(newAutelId == null || newAutelId == "")
		{
			$("#newAutelIdErrorInfo").show();
			$("#newAutelIdImg").show();
			newAutelIdErrorInfoTip.html("<fmt:message key='customerinfo.username.isnotnull'/>");
			return;
		}else{
		  if(!regEmail.test(newAutelId))
			{
			$("#newAutelIdErrorInfo").show();
			$("#newAutelIdImg").show();
			newAutelIdErrorInfoTip.html("<fmt:message key='accountinformation.useraccount.check'/>");
			return;
			}
		
		}
		
		
		var resultCode = true;
		var imagenum = $("#imagenum").val();
		$.ajax({
			url:'front/usercenter/customer/checkProduct.do',
			type:"post",
			async:false,
			data:{"autelId":autelId,"proSerialNo":productSN,"proRegPwd":productPwd,"imagenum":imagenum},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				
				if(jsonData[0].regResult == "notFound")
				{
					$("#productErrorInfo").show();
					$("#productPwdImg").show();
					productErrorInfoTip.html("<fmt:message key='customer.change.sn.notexist'/>");
					resultCode = false;
					return;
				}
				else if(jsonData[0].regResult == "passwordError")
				{
					$("#passwordErrorInfo").show();
			        $("#productPwdImg").show();
					passwordErrorInfoTip.html("<fmt:message key='customer.change.pwd.error'/>");
					resultCode = false;
					return;
				}else if(jsonData[0].regResult == "imagenum"){
				    $("#imagenumErrorInfo").show();
					$("#imagenumImg").show();
					imagenumErrorInfoTip.html("<fmt:message key='customerinfo.usercode.illegalcharactercheck'/>");
					resultCode = false;
					buildRandom();
					return;
				}
				
				else if(jsonData[0].regResult == "productNotForYou")
				{
					$("#productErrorInfo").show();
					$("#productPwdImg").show();
					productErrorInfoTip.html("<fmt:message key='customer.change.sn.error'/>!");
					resultCode = false;
					return;
				}
				
			},
			error :function()
			{
				$("#newAutelIdErrorInfo").show();
				$("#newAutelIdImg").show();
				newAutelIdErrorInfoTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}		
		
		if(checkAutelID(newAutelId)=="true"){
		  $("#newAutelIdErrorInfo").show();
		  $("#newAutelIdImg").show();
		  newAutelIdErrorInfoTip.html("<fmt:message key='customer.change.account.error'/>");
		  resultCode=false;
		  return;
		}
		
		
		$form.action="changeAutelIDResult.html";
		$form.submit();
	});
});

function clearImagenumErrorInfo(){
		$("#imagenumErrorInfo").hide();
		$("#imagenumImg").hide();
}

function checkAutelID2(){
	var autelId = $("#newAutelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var flag;
	if(!regEmail.test(autelId))
	{
		$("#emailError").attr("color","#ea0000");
		return;
	}else{
		$("#emailError").attr("color","");
	}
	
	$.ajax({
			url:'front/user/checkAutelIDIsUseNew.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				flag=autelId;
				if(autelId == "true")
				{
					$("#isUse").attr("color","#ea0000");
				}else{
					$("#isUse").attr("color","");
				}
				
			}
		});
	return flag;
}

function clearautelIdValue(){
	if($("#newAutelId").val()=="<fmt:message key='customerinfo.user.forexample'/>:steve@auteltech.com")
	{
		$("#newAutelId").attr("value","");
	} 
	$("#autelIdImg").hide();
	$("#autelIDResult").hide();
	$("#au_id").show();
	checkAutelID2();
	
}

function onblurAutelId(){
	$("#au_id").hide();
	var autelId = $("#newAutelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var autelIDResultTip=$("#autelIDResultTip");
	
	if(autelId == null || autelId == "")
	{
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		
		autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isnotnull'/>");
	}
	else if(!regEmail.test(autelId))
	{
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.check'/>");
		
	}else if(checkAutelID2()=="true"){
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='customer.change.account.error'/>");
	}else{
		$("#autelIDResult").hide();
		$("#autelIdImg").hide();
		
	}
}



function checkAutelID(autelId){
    var flag;
	$.ajax({
			url:'front/user/checkAutelIDIsUseNew.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				flag=autelId;
			}
		});
	return flag;
}


function clearAllErrorInfo()
{
	$("#productErrorInfoTip").html("");
	$("#passwordErrorInfoTip").html("");
    $("#newAutelIdErrorInfoTip").html("");
	
	$("#productErrorInfo").hide();
	$("#productSNImg").hide();
	$("#passwordErrorInfo").hide();
	$("#productPwdImg").hide();
	$("#newAutelIdErrorInfo").hide();
	$("#newAutelIdImg").hide();

}
