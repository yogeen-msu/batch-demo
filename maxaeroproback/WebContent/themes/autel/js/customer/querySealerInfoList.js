

$(document).ready(function(){
	
	$("#querySealerInfoSubmit").click(function()
	{
		var $form = $("#sealerInfoForm");
		var country = $("#country").val();
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_]+$/i;
		var resultTip = $("#resultTip");
		if(country != null && country != '')
		{
			if(!regName.test(country))
			{
				resultTip.html("国家不能有非法字符!");
				return;
			}
		}
		
		resultTip.html("");
		
		$form.action="querySealerInfoList-1-1.html?operationType=5";
		//form.action="querySealerInfoList-1-1.html?operationType=5&areaCode="+areaCode+"&country="+country;
		$form.submit();
	});
});