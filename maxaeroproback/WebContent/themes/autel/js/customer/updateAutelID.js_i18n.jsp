<%@ include file="../common.js_i18n.jsp" %>

function clearAllErrorInfo(){
}

function clearautelIdValue(){
	if($("#newAutelId").val()=="<fmt:message key='customerinfo.user.forexample'/>:steve@auteltech.com")
	{
		$("#newAutelId").attr("value","");
	} 
	$("#autelIdImg").hide();
	$("#autelIDResult").hide();
	$("#au_id").show();
	checkAutelID();
	
}

function onblurAutelId(){
	$("#au_id").hide();
	var autelId = $("#newAutelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var autelIDResultTip=$("#autelIDResultTip");
	
	if(autelId == null || autelId == "")
	{
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		
		autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isnotnull'/>");
	}
	else if(!regEmail.test(autelId))
	{
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.check'/>");
		
	}else if(checkAutelID()=="true"){
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='customer.change.account.error'/>");
	}else{
		$("#autelIDResult").hide();
		$("#autelIdImg").hide();
		
	}
}

function updateAutel(){
	var oldId=$("#oldId").val();
	var newAutelId=$("#newAutelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var autelIDResultTip=$("#autelIDResultTip");
	if(newAutelId == null || newAutelId == "")
	{
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isnotnull'/>");
		return;
	}
	else if(!regEmail.test(newAutelId))
	{
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.check'/>");
		return;
	}else if(checkAutelID()=="true"){
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='customer.change.account.error'/>");
		return;
	}else{
		$("#autelIDResult").hide();
		$("#autelIdImg").hide();
		
	}
    if(checkAutelID()=="true"){
    	$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='customer.change.account.error'/>");
        return;
    }
    $.ajax({
			url:'front/user/updateAutelId.do',
			type:"post",
			async:false,
			data:{"oldId":oldId,"newAutelId":newAutelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				if(result == "0")
				{
					jump("updateAutelIDResult.html?operationType=23");
				}else if(result == "1"){
					$("#autelIDResult").show();
		            $("#autelIdImg").show();
		            autelIDResultTip.html("<fmt:message key='customer.change.account.error'/>");
				}else if(result == "2"){
				    $("#autelIdImg").show();
					$("#autelIDResult").show();
					autelIDResultTip.html("<fmt:message key='common.system.error'/>");
				}
				
			},
			error :function(){
				$("#autelIDResult").show();
				autelIDResultTip.html("<fmt:message key='common.system.error'/>");
			}
	});

}


function checkAutelID(){
	var autelId = $("#newAutelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var flag;
	if(!regEmail.test(autelId))
	{
		$("#emailError").attr("color","#ea0000");
		return;
	}else{
		$("#emailError").attr("color","");
	}
	
	$.ajax({
			url:'front/user/checkAutelIDIsUseNew.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				flag=autelId;
				if(autelId == "true")
				{
					$("#isUse").attr("color","#ea0000");
				}else{
					$("#isUse").attr("color","");
				}
				
			}
		});
	return flag;
}
