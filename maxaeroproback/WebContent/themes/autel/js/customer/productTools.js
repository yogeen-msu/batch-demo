function downProductTool(downToolPath)
{
	jump("downLoadProductTools.html?operationType=2&downToolPath="+downToolPath);
}

function jump(url){

	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
}

function backDownProductTool(type,code,userType)
{
	//添加客户时查看附件
	if(type == 1)
	{
		jump("queryCustomerComplaintTableDetail.html?operationType=8&code="+code+"&userType="+userType);
	}
	else if(type == 2)//客诉详情回复附件查看
	{
		jump("queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType);
	}
	else
	{
		jump("queryProductToolsList.html?operationType=1");
	}
}

function queryProductTools()
{
	var languageCode = $("#languageCode").val();
	jump("queryProductToolsList.html?operationType=1&languageCode="+languageCode);	
}
