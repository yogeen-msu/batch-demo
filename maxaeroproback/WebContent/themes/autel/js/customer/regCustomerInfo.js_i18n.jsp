
<%@ include file="../common.js_i18n.jsp" %>
$(document).ready(function(){
	
	var autelIDResultTip=$("#autelIDResultTip");
	var userPwdResultTip=$("#userPwdResultTip");
	var userPwd2ResultTip=$("#userPwd2ResultTip");
	var firstNameResultTip=$("#firstNameResultTip");
	var middleNameResultTip=$("#middleNameResultTip");
	var lastNameResultTip=$("#lastNameResultTip");
	var secondEmailResultTip =$("#secondEmailResultTip");
	var comUsernameResultTip=$("#comUsernameResultTip");
	var questionCodeResultTip=$("#questionCodeResultTip");
	var answerResultTip=$("#answerResultTip");
	var countryResultTip=$("#countryResultTip");
	var addressResultTip=$("#addressResultTip");
	var cityResultTip=$("#cityResultTip");
	var companyResultTip=$("#companyResultTip");
	var zipCodeResultTip=$("#zipCodeResultTip");
	var languageCodeResultTip=$("#languageCodeResultTip");
	var imagenumResultTip=$("#imagenumResultTip");
	var agreeResultTip=$("#agreeResultTip");

   var tempCountry=$('#country').val();
   if(tempCountry=='United States'){
		usaProvince();
	}
   
	
	if('${local}'=='en_US')
	{
		$("#middleNameDiv").show();
		$("#middleNameTipDiv").show();
		if($("#languageCode").length > 0){
		 $("#languageCode").find("option").each(function (){
			if($(this).text()=="English") $(this).attr("selected", true);
		 });
		 }
		
	}else{
	    if($("#languageCode").length > 0){
		 $("#languageCode").find("option").each(function (){
			if($(this).text()=="Chinese") $(this).attr("selected", true);
		 });
		 }
	}
	
	$("#regCustomerCheck").click(function()
	{
		var $form = $("#regCustomerInfoForm");
		var autelId = $("#autelId").val();
		var password = $("#userPwd").val();
		var confirmPassword = $("#userPwd2").val();
		var questionCode = $("#questionCode").val();
		var languageCode = $("#languageCode").val();
		var answer = $("#answer").val();
		var firstName = $("#firstName").val();
		var middleName = $("#middleName").val();
		var lastName = $("#lastName").val();
		var secondEmail = $("#secondEmail").val();
		var country = $("#country").val();
		var address = $("#address").val();
		var city = $("#city").val();
		var company = $("#company").val();
		var comUsername = $("#comUsername").val();
		var zipCode = $("#zipCode").val();
		var agree = $("#agree").val();
		var imagenum = $("#imagenum").val();
		var province = $("#province").val();   //20140307 add
		
		
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_]+$/i;
		var regComUsername = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		var regNumber = /^[0-9]\d*$/;
		var regPwd=new RegExp(/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/);
	    var operationResultTip = $("#operationResultTip");
	    operationResultTip.html("");
		
		clearAllErrorInfo();
		
		var isFlag=true;
		
		if(autelId == null || autelId == "")
		{
			$("#autelIDResult").show();
			$("#autelIdImg").show();
			autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isnotnull'/>");
			
			$("#autelId").focus();
			isFlag =false;
		}
		else if(!regEmail.test(autelId))
		{
			$("#autelIDResult").show();
			$("#autelIdImg").show();
			autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.check'/>");
			$("#autelId").focus();
			isFlag =false;
		}
		
		if(password == null || password == "")
		{
			$("#userPwdResult").show();
			$("#userPwdImg").show();
			userPwdResultTip.html("<fmt:message key='customerinfo.userpassword.isnotnull'/>");
			if(isFlag){
					$("#userPwd").focus();
			}
			isFlag =false;
		}
		else if(password.length < 6)
		{
			$("#userPwdResult").show();
			$("#userPwdImg").show();
			userPwdResultTip.html("<fmt:message key='accountinformation.password.lengthcheck'/>");
			if(isFlag){
					$("#userPwd").focus();
			}
			isFlag =false;
		}else{
			if(!regPwd.test(password)){
	   			$("#userPwdResult").show();
				$("#userPwdImg").show();
				userPwdResultTip.html("<fmt:message key='accountinformation.password.check'/>");
				if(isFlag){
					$("#userPwd").focus();
				}
				isFlag =false;
  			}
		}
		
		if(confirmPassword == null || confirmPassword == "")
		{
			$("#userPwd2Result").show();
			$("#userPwd2Img").show();
			userPwd2ResultTip.html("<fmt:message key='update.confirmpassword.isnotnull'/>");
			if(isFlag){
				$("#userPwd2").focus();
			}
			isFlag =false;
		}
		else if(confirmPassword.length < 6)
		{
			$("#userPwd2Result").show();
			$("#userPwd2Img").show();
			userPwd2ResultTip.html("<fmt:message key='update.confirmpassword.lengthcheck'/>");
			if(isFlag){
				$("#userPwd2").focus();
			}
			isFlag =false;
		}
		
		if(password != confirmPassword)
		{
			$("#userPwd2Result").show();
			$("#userPwd2Img").show();
			userPwd2ResultTip.html("<fmt:message key='accountinformation.passwordandconfirmpassword.identical'/>");
			if(isFlag){
				$("#userPwd2").focus();
			}
			isFlag =false;
		}
		
		if(firstName == null || firstName == "")
		{
			$("#firstNameResult").show();
			$("#firstNameImg").show();
			firstNameResultTip.html("<fmt:message key='accountinformation.firstname.isnotnull'/>");
			if(isFlag){
				$("#firstName").focus();
			}
			isFlag =false;
		}
		else
		{
			if(!regName.test(firstName))
			{
				$("#firstNameResult").show();
				$("#firstNameImg").show();
				firstNameResultTip.html("<fmt:message key='accountinformation.firstname.illegalcharactercheck'/>");
				if(isFlag){
					$("#firstName").focus();
				}
				isFlag =false;
			}
		}
		
		if(middleName != null && middleName !="")
		{
			if(!regName.test(middleName))
			{
				$("#middleNameResult").show();
				$("#middleNameImg").show();
				middleNameResultTip.html("<fmt:message key='accountinformation.middlename.illegalcharactercheck'/>");
				if(isFlag){
					$("#middleName").focus();
				}
				isFlag =false;
			}
		}
		
		if(lastName == null || lastName == "")
		{
			$("#lastNameResult").show();
			$("#lastNameImg").show();
			lastNameResultTip.html("<fmt:message key='accountinformation.lastname.isnotnull'/>");
			if(isFlag){
				$("#lastName").focus();
			}
			isFlag =false;
		}
		else
		{
			if(!regName.test(lastName))
			{
				$("#lastNameResult").show();
				$("#lastNameImg").show();
				lastNameResultTip.html("<fmt:message key='accountinformation.lastname.illegalcharactercheck'/>");
				if(isFlag){
					$("#lastName").focus();
				}
				isFlag =false;
			}
		}
	
		
		operationResultTip.html("");
		
		if(secondEmail != null && secondEmail !="")
		{
			if(!regEmail.test(secondEmail))
			{
				$("#secondEmailResult").show();
				$("#secondEmailImg").show();
				secondEmailResultTip.html("<fmt:message key='accountinformation.secondemail.illegalcharactercheck'/>");
				if(isFlag){
					$("#secondEmail").focus();
				}
				isFlag =false;
			}
			
			operationResultTip.html("");
			
			if(secondEmail==autelId)
			{
				$("#secondEmailResult").show();
				$("#secondEmailImg").show();
				secondEmailResultTip.html("<fmt:message key='accountinformation.firstandsecondemail.isthesame'/>");
				if(isFlag){
					$("#secondEmail").focus();
				}
				isFlag =false;
			}
			
		}
		
		if(comUsername == null || comUsername == "")
		{
			$("#comUsernameResult").show();
			$("#comUsernameImg").show();
			comUsernameResultTip.html("<fmt:message key='accountinformation.comusername.isnotnull'/>");
			if(isFlag){
				$("#comUsername").focus();
			}
			isFlag =false;
		}
		else if(!regComUsername.test(comUsername))
		{
			$("#comUsernameResult").show();
			$("#comUsernameImg").show();
			comUsernameResultTip.html("<fmt:message key='accountinformation.comusername.illegalcharacterche'/>");
			if(isFlag){
				$("#comUsername").focus();
			}
			isFlag =false;
		}
		
		operationResultTip.html("");
		
		if(questionCode == -1)
		{
			$("#questionCodeResult").show();
			$("#questionCodeImg").show();
			questionCodeResultTip.html("<fmt:message key='accountinformation.selectanswer.name'/>");
			if(isFlag){
				$("#questionCode").focus();
			}
			isFlag =false;
		}
		
		operationResultTip.html("");
		
		if(answer == null || answer == "")
		{
			$("#answerResult").show();
			$("#answerImg").show();
			answerResultTip.html("<fmt:message key='accountinformation.answer.isnotnull'/>");
			if(isFlag){
				$("#answer").focus();
			}
			isFlag =false;
		}
		
		if(country == null || country == "")
		{
			$("#countryResult").show();
			$("#countryImg").show();
			countryResultTip.html("<fmt:message key='accountinformation.country.isnotnull'/>");
			if(isFlag){
				$("#country").focus();
			}
			isFlag =false;
		}
	
		
	
		
		if(languageCode == -1)
		{
			$("#languageCodeResult").show();
			$("#languageCodeImg").show();
			languageCodeResultTip.html("<fmt:message key='accountinformation.selectlanguage.name'/>");
			if(isFlag){
				$("#languageCode").focus();
			}
			isFlag =false;
		}
		
		if(imagenum == null || imagenum == "")
		{
			$("#imagenumResult").show();
			$("#imagenumImg").show();
			imagenumResultTip.html("<fmt:message key='customerinfo.usercode.isnotnull'/>");
			if(isFlag){
				$("#imagenum").focus();
			}
			isFlag =false;
		}
		
		if(agree == 0)
		{
			$("#agreeResult").show();
			$("#agreeImg").show();
			agreeResultTip.html("<fmt:message key='accountinformation.tiaokuan.select'/>");
			if(isFlag){
				$("#agree").focus();
			}
			isFlag =false;
		}
		if(!isFlag){
			return;
		}
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/regCustomerCheck.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId,"userInfoVo.imageCode":imagenum},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				var imageCode = jsonData[0].imageCode;
				
				if(imageCode == "false")
				{
					$("#imagenumResult").show();
					$("#imagenumImg").show();
					imagenumResultTip.html("<fmt:message key='customerinfo.usercode.illegalcharactercheck'/>");
					resultCode = false;
					$("#imageCode").focus();
					return;
				}else if(autelId == "false1"){
				    $("#autelIDResult").show();
					$("#autelIdImg").show();
					autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isnotnull'/>");
				    window.setTimeout( function(){ jQuery("#autelId").focus(); }, 0);
					return;
				}
				
				if(autelId == "true")
				{
					$("#autelIDResult").show();
					$("#autelIdImg").show();
					autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isreg'/>");
					
					resultCode = false;
					$("#autelId").focus();
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='customer.network.not.valid'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
		
	    $("#regCustomerCheck").attr("disabled","true");
		$form.action="regCustomerInfoResult.html?autelId="+autelId;
		$form.submit();
	});

});
	
function setEmail(obj)
{
	if(obj.checked)
	{
		$("#isAllowSendEmail").val(1);
	}
	else
	{
		$("#isAllowSendEmail").val(0);
	}
	
}

function setAgree(obj)
{
	if(obj.checked)
	{
		$("#agree").val(1);
	}
	else
	{
		$("#agree").val(0);
	}
}

function clearAllErrorInfo(){
	$("#autelIDResult").hide();
	$("#autelIdImg").hide();
	
	
	$("#au_pwd").hide(); 
	
	$("#userPwdResult").hide();
	$("#userPwdImg").hide();
	
	$("#userPwd2Result").hide();
	$("#userPwd2Img").hide();
	
	$("#firstNameResult").hide();
	$("#firstNameImg").hide();
	
	$("#middleNameResult").hide();
	$("#middleNameImg").hide();
	
	$("#lastNameResult").hide();
	$("#lastNameImg").hide();
	
	$("#secondEmailResult").hide();
	$("#secondEmailImg").hide();
	
	$("#comUsernameResult").hide();
	$("#comUsernameImg").hide();
	
	$("#questionCodeResult").hide();
	$("#questionCodeImg").hide();
	
	$("#answerResult").hide();
	$("#answerImg").hide();
	
	$("#countryResult").hide();
	$("#countryImg").hide();
	
	$("#addressResult").hide();
	$("#addressImg").hide();
	
	$("#cityResult").hide();
	$("#cityImg").hide();
	
	$("#companyResult").hide();
	$("#companyImg").hide();
	
	$("#zipCodeResult").hide();
	$("#zipCodeImg").hide();
	
	$("#languageCodeResult").hide();
	$("#languageCodeImg").hide();
	
	$("#imagenumResult").hide();
	$("#imagenumImg").hide();
	
	$("#agreeResult").hide();
	$("#agreeImg").hide();
}

function clearautelIdValue(){
	$("#au_pwd").hide();
	if($("#autelId").val()=="<fmt:message key='customerinfo.user.forexample'/>:steve@auteltech.com")
	{
		$("#autelId").attr("value","");
	} 
	$("#au_id").show();
	checkAutelID();
	<%-- var autelIDResultTip=$("#autelIDResultTip");
	$("#autelIDResult").show();
	$("#autelIdImg").show();
	autelIDResultTip.html("<fmt:message key='accountinformation.autelIDInfo.name'/>"); --%>
}

function onblurAutelId(){
	$("#au_id").hide();
	var autelId = $("#autelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var autelIDResultTip=$("#autelIDResultTip");
	
	if(autelId == null || autelId == "")
	{
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isnotnull'/>");
	    window.setTimeout( function(){ jQuery("#autelId").focus(); }, 0);
		return;
	}
	else if(!regEmail.test(autelId))
	{
		$("#autelIDResult").show();
		$("#autelIdImg").show();
		autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.check'/>");
		 window.setTimeout( function(){ jQuery("#autelId").focus(); }, 0);
		return;
	}else{
	    $.ajax({
			url:'front/user/checkAutelIDIsUse.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var autelId = jsonData[0].autelId;
				
				if(autelId == "true")
				{
				    $("#autelIDResult").show();
					$("#autelIdImg").show();
					autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isreg'/>");
					$("#isUse").attr("color","red");
					 window.setTimeout( function(){ jQuery("#autelId").focus(); }, 0);
					return;
				}else if(autelId == "false1"){
				    $("#autelIDResult").show();
					$("#autelIdImg").show();
					autelIDResultTip.html("<fmt:message key='accountinformation.useraccount.isnotnull'/>");
				    window.setTimeout( function(){ jQuery("#autelId").focus(); }, 0);
					return;
				}
				else if(autelId == "SystemError"){
				    $("#autelIDResult").show();
					$("#autelIdImg").show();
					autelIDResultTip.html("<fmt:message key='system.error.name'/>");
					$("#isUse").attr("color","red");
					 window.setTimeout( function(){ jQuery("#autelId").focus(); }, 0);
					return;
				}
				else{
				   $("#isUse").attr("color","");
					$("#autelIDResult").hide();
					$("#autelIdImg").hide();
				}
			}
		}); 
	}
}

function onbluruserPwdValue(){
	$("#au_pwd").hide();
	var password = $("#userPwd").val();
	var userPwdResultTip=$("#userPwdResultTip");
	var regPwd=new RegExp(/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/);
	if(password == null || password == "")
	{
		$("#userPwdResult").show();
		$("#userPwdImg").show();
		userPwdResultTip.html("<fmt:message key='customerinfo.userpassword.isnotnull'/>");
	}
	else if(password.length < 6)
	{
		$("#userPwdResult").show();
		$("#userPwdImg").show();
		userPwdResultTip.html("<fmt:message key='accountinformation.password.lengthcheck'/>");
	}else if(!regPwd.test(password)){
	   	$("#userPwdResult").show();
		$("#userPwdImg").show();
		userPwdResultTip.html("<fmt:message key='accountinformation.password.check'/>");
	}else{
		$("#userPwdResult").hide();
		$("#userPwdImg").hide();
	}
	
	var confirmPassword = $("#userPwd2").val();
	var userPwd2ResultTip=$("#userPwd2ResultTip");
	if(confirmPassword !=null && confirmPassword!= ""){
		if(password != confirmPassword){
				$("#userPwd2Result").show();
				$("#userPwd2Img").show();
				userPwd2ResultTip.html("<fmt:message key='accountinformation.passwordandconfirmpassword.identical'/>");
		}else{
				$("#userPwd2Result").hide();
				$("#userPwd2Img").hide();
		}
	}
		
}

function userPwdValue(){
	$("#au_id").hide();
	$("#au_pwd").show();
	checkAutelPwd();
	<%-- var userPwdResultTip=$("#userPwdResultTip");
	$("#userPwdResult").show();
	$("#userPwdImg").show();
	userPwdResultTip.html("<fmt:message key='accountinformation.userPwdInfo.name'/>"); --%>
}

function onblurUserPwd2Value(){
		var password = $("#userPwd").val();
		var confirmPassword = $("#userPwd2").val();
		var userPwd2ResultTip=$("#userPwd2ResultTip");
		if(password != confirmPassword){
			$("#userPwd2Result").show();
			$("#userPwd2Img").show();
			userPwd2ResultTip.html("<fmt:message key='accountinformation.passwordandconfirmpassword.identical'/>");
		}else{
			$("#userPwd2Result").hide();
			$("#userPwd2Img").hide();
		}
}


function checkAutelID(){
	var autelId = $("#autelId").val();
	var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	if(!regEmail.test(autelId))
	{
		$("#emailError").attr("color","#ea0000");
		return;
	}else{
		$("#emailError").attr("color","");
	}
}

function checkAutelPwd(){
	var password = $("#userPwd").val();
	var regPwd=new RegExp(/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/);
	if(password.length < 6)
	{
		$("#lengthError").attr("color","#ea0000");
	}else{
		$("#lengthError").attr("color","");
	}
	
	if(!regPwd.test(password)){
	   	$("#numberError").attr("color","#ea0000");
  	}else{
  		$("#numberError").attr("color","");
  	}
}






