<%@ include file="../common.js_i18n.jsp" %>

$(document).ready(function(){
	var languageCodeResultTip=$("#languageCodeResultTip");

	
	$("#checkUpdateCustomerInfo").click(function()
	{
		var languageCode = $("#languageCode").val();
		var code = $("#code").val();
		var actCode = $("#actCode").val();
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_.,&'""()\s]+$/i;
		var regNumber = /^[0-9]\d*$/;
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regComUsername = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		

		var operationResultTip = $("#operationResultTip");

		
		operationResultTip.html("");
		
		clearAllErrorInfo();
		var isFlag=true;
		
		if(languageCode == -1)
		{
			$("#languageCodeResult").show();
			$("#languageCodeImg").show();
			languageCodeResultTip.html("<fmt:message key='accountinformation.selectlanguage.name'/>");
			if(isFlag){
				 $("#languageCode").focus();
			}
			isFlag =false;
		}
	
		operationResultTip.html("");
		
		var resultCode = true;
		
		if(!isFlag){
			return;
		}
		
		$.ajax({
			url:'front/user/updateCustomerInfoNew.do',
			type:"post",
			async:false,
			data:{"customerInfoEdit.languageCode":languageCode,"customerInfoEdit.code":code,"customerInfoEdit.actCode":actCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				
				if(result == "1")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.success'/>");					
					resultCode = true;
					return;
				}
				else if(result == "2")
				{
					operationResultTip.html("<fmt:message key='accountinformation.update.fail'/>");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});

});

function clearAllErrorInfo(){
	
	$("#languageCodeResult").hide();
	$("#languageCodeImg").hide();
	
}

