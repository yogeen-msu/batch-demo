<%@ include file="common.js_i18n.jsp" %> 
/**
 * 软件续租JS
*/

$(document).ready(function(){
		var addtocart = $("#addtocart");
		//全选
		$("input[name^='chkAll']").click(function(){
			
	
			addtocart.html("");
			if($(this).attr("checked")){
				$("input[name='chk']").attr("checked","checked");
			}else{
				$("input[name='chk']").removeAttr("checked");
			}
		});
		
		$("input[name='chk']").click(function(){
			addtocart.html("");
			var flag = false;
		    
		    $("input[name='chk']").each(function() {
		        if($(this).attr("checked")){
		            flag = true;
		        }else{
		            flag = false;
		            return false;
		        }
		    });
		    
		    if(flag){
		    	$("input[name^='chkAll']").attr("checked","checked");       
		    }else{
		    	$("input[name^='chkAll']").attr("checked",flag);        
		    }
		});
});
 

/**
 * 显示充值卡信息输入框
 */
function RenewPaybyCard(){
	var renew=$("#renewPaybyCard").attr("class");
	if(renew=="renew_lan"){
		$("#reChargeCard").show();	
		$("#renewPaybyCard").attr("class","renew");
	}else{
		$("#reChargeCard").hide();	
		$("#renewPaybyCard").attr("class","renew_lan");
	}
	
}

/**
 * 批量加入购物车
 * @param proName
 * @param proSerial
 * @param proCode
 * @param areaCfgCode
 */
function addShoppingCarts(proName,proSerial,proCode,picPath){
	
	var addtocart = $("#addtocart");
	
	addtocart.html("");
	
	var shoppingInfos=new Array();
	
	$("input[name='chk']").each(function() {
        if($(this).attr("checked")){
        	var minSaleUnitCode=$(this).attr("minSaleUnitCode");
        	var areaCfgCode=$(this).attr("areaCfgCode");
        	var $year=$("#rentYear_"+minSaleUnitCode).val();
        	var $isSoft=$(this).attr("isSoft");
        	if($isSoft == 0){
        		var $price=$("#price_"+minSaleUnitCode).attr("price");
            	var $validData=$("#validData_"+minSaleUnitCode).attr("validData");
            	var shoppingInfo=proName+"--"+proSerial+"--"+proCode+"--"+areaCfgCode+"--"+minSaleUnitCode+"--"+$year+"--1--"+picPath+"--"+$isSoft+"--"+$price+"--"+$validData;
            	shoppingInfos.push(shoppingInfo);
        	}else{
        		var shoppingInfo=proName+"--"+proSerial+"--"+proCode+"--"+areaCfgCode+"--"+minSaleUnitCode+"--"+$year+"--1--"+picPath+"--"+$isSoft+"--0--0";
            	shoppingInfos.push(shoppingInfo);
        	}
        	
        	
        }
    });
    
	if(shoppingInfos.length<1){
		addtocart.html("<fmt:message key='product.selecttocart'/>");
		
		return ;
	}

	$.ajax({
		url:'front/usercenter/customer/checkMinSaleUnitIsUpCode.do',
		type:"post",
		data:{"shoppingInfos":shoppingInfos.join(",")},
		dataType:'JSON',
		success:function(data){
			var jsonData=eval(data);
			
			if(jsonData[0].addResult == "isupcode"){
				if(confirm(jsonData[0].jia+"<fmt:message key='addcart.isup.name1'/>"+jsonData[0].yi+"<fmt:message key='addcart.isup.name2'/>"+jsonData[0].yi+"<fmt:message key='addcart.isup.name3'/>")){
					$.ajax({
						url:'front/usercenter/customer/addShoppingCart.do',
						type:"post",
						data:{"shoppingInfos":shoppingInfos.join(",")},
						dataType:'JSON',
						success:function(data){
							var jsonData=eval(data);
							if(jsonData[0].addResult){
								jump("cart.html?way=1");//跳转到购物车页面
							}else{
								addtocart.html("<fmt:message key='product.addtocartfail'/>");
							}
						},
						error:function(){
							addtocart.html("<fmt:message key='system.error.name'/>");
						}
					});
				}else{
					return;
				}
			}else{
				$.ajax({
						url:'front/usercenter/customer/addShoppingCart.do',
						type:"post",
						data:{"shoppingInfos":shoppingInfos.join(",")},
						dataType:'JSON',
						success:function(data){
							var jsonData=eval(data);
							if(jsonData[0].addResult){
								jump("cart.html?way=1");//跳转到购物车页面
							}else{
								addtocart.html("<fmt:message key='product.addtocartfail'/>");
							}
						},
						error:function(){
							addtocart.html("<fmt:message key='system.error.name'/>");
						}
					});
			}
		},
		error:function(){
			addtocart.html("<fmt:message key='system.error.name'/>");
		}
	});
	
}

/**
 * 加入购物车
 * @param proName
 * @param proSerial
 * @param proCode
 * @param minSaleUnit
 * @param areaCfgCode
*/
function addShoppingCart(proName,proSerial,proCode,minSaleUnit,areaCfgCode,picPath,isSoft){
	if(isSoft == 0){
		var $year=$("#rentYear_"+minSaleUnit).val();
		var validData=$("#validData_"+minSaleUnit).attr("validData");
		var price=$("#price_"+minSaleUnit).attr("price");
		jump("cart.html?proName="+proName+"&proSerial="+proSerial+"&" +
				"proCode="+proCode+"&minSaleUnit="+minSaleUnit+"&areaCfgCode="+areaCfgCode+
				"&consumeType=1&rentYear="+$year+"&way=0&pic="+picPath+"&isSoft="+isSoft+"&cfgPrice="+price+"&validData="+validData);
	}else{
		var $year=$("#rentYear_"+minSaleUnit).val();
		jump("cart.html?proName="+proName+"&proSerial="+proSerial+"&" +
				"proCode="+proCode+"&minSaleUnit="+minSaleUnit+"&areaCfgCode="+areaCfgCode+
				"&consumeType=1&rentYear="+$year+"&way=0&pic="+picPath+"&isSoft="+isSoft);
	}
} 

/**
 * 使用充值卡充值
 */
function reChargeCard(proSerial){

	var isCommon = $("#isCommon");
	var isAccount = $("#isAccount");
	
	isAccount.html("");
	isCommon.html("");
	
	var $account=$("#account").val();
	if($account ==""){
		isAccount.html("<fmt:message key='recharge.account.name'/>");
		return;
	}
	
	var $pwd=$("#pwd").val();
	if($pwd ==""){
		isCommon.html("<fmt:message key='recharge.pwd.name'/>");
		return;
	}
	
	if($pwd.length<6){
		isCommon.html("<fmt:message key='recharge.pwdlength.name'/>");
		return;
	}
	$.ajax({
		url:'front/usercenter/customer/toCharge.do',
		type:'post',
		data:{"account":$account,"pwd":$pwd,"proSerial":proSerial},
		dataType:'JSON',
		success:function(data){
			
			var jsonData=eval(data);
			if(jsonData[0].chargeResult == "0"){
				isCommon.html("<fmt:message key='recharge.pwderror.name'/>");
			}else if(jsonData[0].chargeResult == "1"){
				isCommon.html("<fmt:message key='recharge.isuse.name'/>");
			}else if(jsonData[0].chargeResult == "2"){
				isCommon.html("<fmt:message key='recharge.success.name'/>");
				setTimeout("window.location.reload()",2000);
			}else if(jsonData[0].chargeResult == "3"){
				isCommon.html("<fmt:message key='recharge.fail.name'/>");
			}else if(jsonData[0].chargeResult == "4"){
				isCommon.html("<fmt:message key='recharge.sorry1.name'/>");
			}else if(jsonData[0].chargeResult == "5"){
				isCommon.html("<fmt:message key='recharge.sorry2.name'/>");
			}else if(jsonData[0].chargeResult == "6"){
				isCommon.html("<fmt:message key='recharge.sorry3.name'/>");
			}
		},
		error:function(){
			isCommon.html("<fmt:message key='system.error.name'/>");
		}
	});
}

/**
 * 出厂配置软件加入购物车
 * @param proName
 * @param proSerial
 * @param rentYears
 * @param price
 * @param date
 * @param minSaleUnitsMemos
 */
function cfgSoftAddShoppingCart(proName,proSerial,proCode,areaCfgCode,minSaleUnitsMemos){
	var addtocart = $("#addtocart");
	
	addtocart.html("");
	
	var shoppingInfos=new Array();
	var year=$("#rentYear").attr("rentYears");
	var code;
	code=minSaleUnitsMemos.split(",");
	
	$.each(code,function(k,v){
		var minSaleUnitCode=v;
		console.log(minSaleUnitCode);
		shoppingInfo=proName+"-"+proSerial+"-"+proCode+"-"+areaCfgCode+"-"+minSaleUnitCode+"-"+year;
		shoppingInfos.push(shoppingInfo);
	});
	
	$.ajax({
		url:'front/usercenter/customer/addShoppingCart.do',
		type:"post",
		data:{"shoppingInfos":shoppingInfos.join(",")},
		dataType:'JSON',
		success:function(data){
			var jsonData=eval(data);
			if(jsonData[0].result=="true"){
				
				jump("shoppingCart.html?way=1");//跳转到购物车页面
			}else{
				addtocart.html("<fmt:message key='product.addtocartfail' />");
			}
		},
		error:function(){
			addtocart.html("<fmt:message key='system.error.name' />");
		}
	});
	
	
}

function rentYear(rentYear){
	var year=$("#rentYear_"+rentYear);
	if(year.val() == "" || year.val() == "0"){
		year.attr("value",1);
	}else{
		year.attr("value",year.val().replace(/\D+/g,'1'));
	}
}

function clearInfo(){
	var isCommon = $("#isCommon");
	var isAccount = $("#isAccount");
	
	isAccount.html("");
	isCommon.html("");
}

function addtoCart(type,minSaleUnitCode,serialNo){
	var type="1";
	var isSoft="0";

	var areaCfgCode="";
	var picPath="";
	var year="1";
	$.ajax({
		url:'front/usercenter/customer/addtocart2.do',
		type:'post',
		data:{"type":type,"minSaleUnitCode":minSaleUnitCode,"isSoft":isSoft,"serialNo":serialNo,"areaCfgCode":areaCfgCode,"year":year,"picPath":picPath},
		dataType:'JSON', 
		success:function(data){
			var jsonData=eval(data);
			jump("addToCart.html?isSuccess="+jsonData[0].addResult);
		},
		error:function(){
			
		}
	});
}

