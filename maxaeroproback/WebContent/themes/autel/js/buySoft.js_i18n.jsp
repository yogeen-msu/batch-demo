<%@ include file="common.js_i18n.jsp" %> 
/**
 * 软件购买JS
 */
$(document).ready(function(){
	var addtocart = $("#addtocart");
	
	
	//全选
	$("input[name^='chkAll']").click(function(){
		addtocart.html("");
		if($(this).attr("checked")){
			$("input[name='chk']").attr("checked","checked");
		}else{
			$("input[name='chk']").removeAttr("checked");
		}
	});
	
	$("input[name='chk']").click(function(){
		addtocart.html("");
		var flag = false;
	    
	    $("input[name='chk']").each(function() {
	        if($(this).attr("checked")){
	            flag = true;
	        }else{
	            flag = false;
	            return false;
	        }
	    });
	    
	    if(flag){
	    	$("input[name^='chkAll']").attr("checked","checked");       
	    }else{
	    	$("input[name^='chkAll']").attr("checked",flag);        
	    }
	});
		
});

/**
 * 批量加入购物车
 * @param proName
 * @param proSerial
 * @param proCode
 * @param areaCfgCode
 */
function addShoppingCarts(proName,proSerial,proCode,picPath){
	var addtocart = $("#addtocart");
	
	addtocart.html("");
	
	var shoppingInfos=new Array();
	$("input[name='chk']").each(function() {
        if($(this).attr("checked")){
        	var minSaleUnitCode=$(this).attr("minSaleUnitCode");
        	var areaCfgCode=$(this).attr("areaCfgCode");
        	var shoppingInfo=proName+"--"+proSerial+"--"+proCode+"--"+areaCfgCode+"--"+minSaleUnitCode+"--0--0--"+picPath+"--1--0--0";
        	shoppingInfos.push(shoppingInfo);
        }
    });
	if(shoppingInfos.length<1){
		addtocart.html("<fmt:message key='product.selecttocart' />");
		return ;
	}
	
	$.ajax({
		url:'front/usercenter/customer/addShoppingCart.do',
		type:"post",
		data:{"shoppingInfos":shoppingInfos.join(",")},
		dataType:'JSON',
		success:function(data){
			var jsonData=eval(data);
			if(jsonData[0].addResult){
				jump("cart.html?way=1");//跳转到购物车页面
			}else{
				addtocart.html("<fmt:message key='product.addtocartfail' />");
			}
		},
		error:function(){
			addtocart.html("<fmt:message key='system.error.name' />");
		}
	});
}
/**
 * 加入购物车
 * @param proName
 * @param proSerial
 * @param proCode
 * @param minSaleUnit
 * @param areaCfgCode
 */
function addShoppingCart(proName,proSerial,proCode,minSaleUnit,areaCfgCode,picPath){
	
	jump("cart.html?proName="+proName+"&proSerial="+proSerial+"&" +
			"proCode="+proCode+"&minSaleUnit="+minSaleUnit+"&areaCfgCode="+areaCfgCode+
			"&consumeType="+0+"&year=0&way=0&pic="+picPath+"&isSoft=1");

}




