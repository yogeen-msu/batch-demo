<%@ include file="../common.js_i18n.jsp" %>
$(document).ready(function(){
	
	var proTypeCode2=$("#proTypeCode2").val();
	if(proTypeCode2=="-2"){
		  $("#proTypeCode2").css("width","130px");
		   $("#selectProType").append("<span id='inputSpan'>&nbsp;&nbsp;<input id='proTypeCode3'   class='easyui-combobox' value='Please input'  style='width:100px;height:25px;padding-top:3px;font-size:13px;color:#cccccc' type='text' maxlength='120' name='proTypeCode3'> </span>");
		   $("#proTypeCode3").focus();
		}else{
		  $("#proTypeCode2").css("width","240px");
		  if($("#inputSpan")[0]){
		   $("#inputSpan").remove();
		  } 
		}
	
	$("#proTypeCode2").change(function(){
		var typeName= $(this).val();
		if(typeName=="-2"){
		  $("#proTypeCode2").css("width","130px");
		   $("#selectProType").append("<span id='inputSpan'>&nbsp;&nbsp;<input id='proTypeCode3' value='Please input'  class='easyui-combobox'  style='width:100px;height:25px;font-size:13px;padding-top:3px;color:#cccccc' type='text' maxlength='120' name='proTypeCode3'> </span>");
		   
		   $("#proTypeCode3").bind("focus",function(){
  				if($(this).val()=='Please input'){
					$(this).val("");
				}
		   });
		   
		}else{
		  $("#proTypeCode2").css("width","240px");
		  if($("#inputSpan")[0]){
		   $("#inputSpan").remove();
		  } 
		}
		
	});
	
	
	$("#serialNo").blur(function(){
		$.ajax({
				url:'front/sealer/getCustomerInfoBySerialNo.do',
				type:"post",
				async:false,
				data:{"serialNo":$(this).val()},
				dataType:'JSON',
				success :function(data)
				{
				    var jsonData = eval(eval(data));
					$('#customerName').val(jsonData[0].name);
					$('#company').val(jsonData[0].company);
					$('#telphone').val(jsonData[0].daytimePhoneCC+jsonData[0].daytimePhoneAC+jsonData[0].daytimePhone);
					$('#email').val(jsonData[0].autelId);
					$('#address').val(jsonData[0].address+" "+jsonData[0].city+" "+jsonData[0].province+" "+jsonData[0].zipCode);
					$('#proStartDate').val(jsonData[0].proDate);
					$('#proEndDate').val(jsonData[0].warrantyDate);
					$('#updateStartDate').val(jsonData[0].regTime);
					$('#updateEndDate').val(jsonData[0].expTime);
				}
		});
	
	});					
	
	$("#subBtn").click(function()
	{
		var proTypeCode=$('#proTypeCode2').val(); //产品类型
		if(proTypeCode=="-1"){
			proTypeCode="";
		}
		if(proTypeCode=="-2"){
		  if($.trim($('#proTypeCode3').val())=='' || $('#proTypeCode3').val()=='Please input'){
		   	proTypeCode="";
		  }else{
		  	proTypeCode=$('#proTypeCode3').val();
		  }
		}
		
		var serialNo=$("#serialNo").val(); //序列号
		var customerName=$("#customerName").val(); //客户名称
		var company=$("#company").val(); //公司
		var telphone=$("#telphone").val(); //联系电话
		var email=$("#email").val(); //电子邮件
		var address=$("#address").val(); //地址
		
		var reason=$("#reason").val(); //返修原因
		var receiveDate=$("#receiveDate").val(); //签收日期
		var trackingNum=$("#trackingNum").val(); //运单号
		var cableFlag=$("#cableFlag").val(); //是否含连接线
		var connectorFlag=$("#connectorFlag").val(); //是否含连接头
		var otherPart=$("#otherPart").val(); //是否含其它部件
		var solution=$("#solution").val(); //处理方案
		var returnDate=$("#returnDate").val(); //送返日期
		var returnNum=$("#returnNum").val(); //返回单号
		var status=$("#status").val(); //状态
		
		var $form = $("#addCustomerComplaint");

		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var operationResultTip = $("#operationResultTip");
		
		operationResultTip.html("&nbsp;");
		if (proTypeCode == '')
		{
			operationResultTip.html("<fmt:message key='sale.query.customer.protype.null'/>");
			return;
		}
		$('#proTypeCode').val(proTypeCode);
		
		if (serialNo == '')
		{
			operationResultTip.html("<fmt:message key='sealer.complaint.add.productsn'/>");
			return;
		}
		if (customerName == '')
		{
			operationResultTip.html("<fmt:message key='sale.query.customer.name.null'/>");
			return;
		}
		if (company == '')
		{
			operationResultTip.html("<fmt:message key='rga.management.customer.company.null'/>");
			return;
		}
		if (receiveDate == '')
		{
			operationResultTip.html("<fmt:message key='rga.management.receiveDate.null'/>");
			return;
		}
		if (trackingNum == '')
		{
			operationResultTip.html("<fmt:message key='rga.management.trackNum.null'/>");
			return;
		}
		
		$("#subBtn").attr("disabled","true");
		var options = { 
				url : "widget.do?type=rgaManagementWidget&operationType=4&ajax=yes",
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
					if(result=="1") 
					{
						operationResultTip.html("<fmt:message key='rga.management.success'/>");
						window.setTimeout(function(){jump("rgaManagement-1-1.html?operationType=1")},2000);
					} 
					else if(result=="0")
					{
						operationResultTip.html("<fmt:message key='rga.management.fail'/>");
						$("#subBtn").removeAttr("disabled");
						return;
					}
					
				},
				error : function(e) 
				{
					$("#subBtn").removeAttr("disabled");
					operationResultTip.html("<fmt:message key='common.system.error'/>");
				}
		};
        
		$form.ajaxSubmit(options);
		
	
	});
	$("#previosBtn").click(function()
	{
		jump("rgaManagement-1-1.html?operationType=1");
	});
	
});

 function changeF()  
    {  
    alert($('#makeupCoSe').val());
       document.getElementById('makeupCo').value=  
       document.getElementById('makeupCoSe').options[document.getElementById('makeupCoSe').selectedIndex].value;  
    } 