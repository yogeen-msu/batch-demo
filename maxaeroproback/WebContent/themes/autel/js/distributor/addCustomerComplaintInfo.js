
$(document).ready(function(){
	
	$("#subBtn").click(function()
	{
		var complaintPerson = $("#complaintPerson").val();
		var company = $("#company").val();
		var complaintDate = $("#complaintDate").val();
		var phone = $("#phone").val();
		var email = $("#email").val();
		var complaintTitle = $("#complaintTitle").val();
		var userType = $("#userType").val();
		var $form = $("#addCustomerComplaint");

		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_]+$/i;
		
		
		var operationResultTip = $("#operationResultTip");
		
		
		if (complaintPerson == null || complaintPerson == '')
		{
			operationResultTip.html("投诉人不能为空!");
			return;
		}
		
		
		operationResultTip.html("");

		if (company == null || company == '') 
		{
			operationResultTip.html("单位不能为空!");
			return;
		}
		else if(!regName.test(company))
		{
			operationResultTip.html("单位不能有非法字符!");
			return;
		}
		
		operationResultTip.html("");

		if (complaintDate == null || complaintDate == '') 
		{
			operationResultTip.html("投诉日期不能为空!");
			return;
		}
		else
		{
			//获取选择时间
			var arr = complaintDate.split("-");
		    var starttime = new Date(arr[0], arr[1], arr[2]);
		    var starttimes = starttime.getTime();
		   
		    //获取系统当前时间
			var myDate = new Date();
			getYear = myDate.getFullYear() ;
	     	getMonth = myDate.getMonth()+1;
	     	getDay = myDate.getDate();
	     	var date = getYear+"-"+getMonth+"-"+getDay;
	     	
	     	var arr2 = date.split("-");
		    var starttime2 = new Date(arr2[0], arr2[1], arr2[2]);
		    var starttimes2 = starttime2.getTime();
		    
		    if(starttimes>starttimes2)
		    {
		    	operationResultTip.html("投诉日期不能大于系统当前时间!");
				return;
		    }
		}
		
		operationResultTip.html("");

		if (phone == null || phone == '')
		{
			operationResultTip.html("电话不能为空!");
			return;
		}
		
		operationResultTip.html("");

		if (email == null || email == '') {
			operationResultTip.html("电子邮箱不能为空!");
			return;
		}
		else if(!regEmail.test(email))
		{
			operationResultTip.html("邮箱账户不合法!");
			return;
		}
		
		operationResultTip.html("");

		if (complaintTitle == null || complaintTitle == '')
		{
			operationResultTip.html("主题不能为空!");
			return;
		}
		else if(!regName.test(complaintTitle))
		{
			operationResultTip.html("主题不能有非法字符!");
			return;
		}
		
		operationResultTip.html("");
		
		var options = { 
				url : "widget.do?type=customerComplaintInfoWidget&operationType=6&ajax=yes",
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
					if(result=="4") 
					{
						operationResultTip.html("添加客诉成功");
						window.location.href="customerComplaintInfo-1-1.html?operationType=1&userType="+userType;
						return;
					} 
					else if(result=="2")
					{
						operationResultTip.html("对不起，上传文件大小不能超过4M!");
						return;
					}
					else if(result=="1")
					{
						operationResultTip.html("对不起,只支持上传doc,docx,xls,xlsx,txt,rar,zip,gif,gpg,pdf,bmp,gpeg格式的文件！");
						return;
					}
					else if(result=="3")
					{
						operationResultTip.html("客诉回复失败!");
						return;
					}
				},
				error : function(e) 
				{
					operationResultTip.html("系统异常、请联系管理员....");
				}
		};

		$form.ajaxSubmit(options);
	
	});
	$("#previosBtn").click(function()
	{
		var userType = $("#userType").val();
		var complaintType = $("#complaintType").val();
		window.location.href="customerComplaintTypeInfo.html?operationType=7&userType="+userType+"&complaintType="+complaintType;
	});
	
});


