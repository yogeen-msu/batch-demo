<%@ include file="../common.js_i18n.jsp" %> 
$(document).ready(function(){
	
	$("#subBtn").click(function()
	{
		
		var $form = $("#customerComplaintType");
		var complaintType=$("#complaintType").val();
		var complaintTypeTip=$("#complaintTypeTip");
		
		if(complaintType == '' || complaintType == null)
		{
			complaintTypeTip.html("<fmt:message key='customercomplaint.pleaseselect.producttypename'/>");
			return;
		}

		complaintTypeTip.html("");
		
		$form.action="addCustomerComplaintInfo.html?operationType=2";
		$form.submit();
	});
});

//设置选择radio值
function setValue(id)
{
	var complaintTypeCode = $("#complaintTypeCode").val();
	$("#complaintType").val(id);
	if(complaintTypeCode !=null && complaintTypeCode !='')
	{
		for(var i = 0; i < complaintTypeCode.split(",").length;i++)
		{
			if(complaintTypeCode.split(",")[i]==id)
			{
				document.getElementById(id).checked=true;
			}
			else
			{
				document.getElementById(complaintTypeCode.split(",")[i]).checked=false;
			}
		}
	}
	
	
}