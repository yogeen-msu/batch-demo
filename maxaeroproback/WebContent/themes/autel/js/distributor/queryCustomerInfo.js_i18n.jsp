<%@ include file="../common.js_i18n.jsp" %>  
addObj=null;
function queryInfo(){
	var autelID=$('#autelID').val();
	var productSN=$('#productSN').val();
	var city=$('#city').val();
	var province =$('#province').val();
	if(province=="-1"){
		province="";
	}
	$.ajax({
			url:'front/usercenter/customer/queryProduct.do',
			type:"post",
			async:false,
			data:{"autelID":autelID,"productSN":productSN,"city":city,"province":province},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var totalCount = jsonData[0].totalCount;
				var productSerialNo = jsonData[0].productSerialNo;
				var customerCode = jsonData[0].customerCode;
				if(totalCount=="1"){
				    getValue(productSerialNo,customerCode);
				}else{
				
				    var url='selectSN.html?type=S&autelID='+autelID+'&productSN='+productSN+'&city='+encodeURI(city)+'&province='+encodeURI(province);
				 	art.dialog.open(url, {
    				title: '<fmt:message key='saler.customer.query.show'/>',
    				lock:true,
    				cancelVal:'Cancel',
    				width: 600, 
    				height: 400,
    				// 在open()方法中，init会等待iframe加载完毕后执行
    				init: function () {
    					var iframe = this.iframe.contentWindow;
    					var top = art.dialog.top;// 引用顶层页面window对象
    				}, 
    				cancel: true
    				});
				}
				
				
			},
			error :function(){
				alert("<fmt:message key='system.error.name'/>");
				return;
			}
	});
}
function jump(url){
	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
}

function setMessageTab(n)
{
	 var url="";
	 var productSNValue=$('#productSNValue').val();
	 var customerCode=$('#customerCode').val();
	 var spanStr="span"+n;
	 $(".down_menu_lm").each(function(i){
   			if($(this).attr('id')==spanStr){
   				$(this).addClass("SelectedOn");
   			}else{
   				$(this).removeClass("SelectedOn");
   			}
     });
	 
	 if(n=="0"){
	 
	   		url="customerPlatBase.html?type=S2&productSN="+productSNValue+"&code="+customerCode;
	 }
	 if(n=="1"){
	    	url="customerPlatSoft.html?type=S1&productSN="+productSNValue;
	 }
	 if(n=="2"){
	  	url="customerPlatComplaint.html?type=S3&productSN="+productSNValue+"&code="+customerCode;
	 }
	 if(n=="3"){
	 	url="customerPlatlogging.html?type=S4&productSN="+productSNValue+"&code="+customerCode;
	 }
	$("#detail").attr("src",url);
}

