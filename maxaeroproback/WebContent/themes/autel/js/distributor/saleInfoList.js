
$(document).ready(function(){
	
	$("#querySaleInfo").click(function()
	{
		var productName = $("#productName").val();
		var $form = $("#saleInfoForm");
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_]+$/i;
		var productNameTip = $("#productNameTip");
		
		if(productName != null && productName != '')
		{
			if(!regName.test(productName))
			{
				productNameTip.html("产品名称不能有非法字符!");
				return;
			}
		}
		
		productNameTip.html("");
		
		$form.action="saleInfo-1-1.html";
		$form.submit();
	});
});