<%@ include file="../common.js_i18n.jsp" %>
$(document).ready(function(){
	
	$("#fixFlagRadio").click(function(){
		if($('input:checkbox[name="fixFlagRadio"]:checked').attr("checked")==true){
			$("#fixFlag").val("2");
		}else{
		  	$("#fixFlag").val("1");
		}
	
	});
	
	$("#productName").change(function(){
			var SN=$("#productName option:selected").attr("tag");
			$("#productNo").val(SN);
		});
		
		$("#datalogging").change(function(){
			var dataloggingId=$("#datalogging option:selected").attr("tag");
			$("#dataloggingId").val(dataloggingId);
		});
		
	
		$("#carYear").change(function(){
			var carYear=$("#carYear option:selected").val();
			$.ajax({
				url:'front/sealer/getVehicleJson.do',
				type:"post",
				async:false,
				data:{"vehicle.queryType":"2","vehicle.yearName":carYear},
				dataType:'JSON',
				success :function(data)
				{
				    var jsonData = eval(eval(data));
				     $("#vehicleSystem").empty();
				     $("#vehicleSystem").append($("<option>").val("").text("<fmt:message key='product.select'/>"));
					$.each(jsonData,function(i){
						var option = $("<option>").val(jsonData[i].mark).text(jsonData[i].mark);
						$("#vehicleSystem").append(option);
					});
					
				}
		});
		});
		
		
		$("#vehicleSystem").change(function(){
			var carYear=$("#carYear option:selected").val();
			var mark=$("#vehicleSystem option:selected").val();
			$.ajax({
				url:'front/sealer/getVehicleJson.do',
				type:"post",
				async:false,
				data:{"vehicle.queryType":"3","vehicle.yearName":carYear,"vehicle.mark":mark},
				dataType:'JSON',
				success :function(data)
				{
				    var jsonData = eval(eval(data));
				    $("#vehicleType").empty();
				    $("#vehicleType").append( $("<option>").val("").text("<fmt:message key='product.select'/>"));
					$.each(jsonData,function(i){
						var option = $("<option>").val(jsonData[i].model).text(jsonData[i].model);
						$("#vehicleType").append(option);
					});
					
				}
		});
		});
	
	
	$("#vehicleType").change(function(){
			var carYear=$("#carYear option:selected").val();
			var mark=$("#vehicleSystem option:selected").val();
			var vehicleType=$("#vehicleType option:selected").val();
			$.ajax({
				url:'front/sealer/getVehicleJson.do',
				type:"post",
				async:false,
				data:{"vehicle.queryType":"4","vehicle.yearName":carYear,"vehicle.mark":mark,"vehicle.model":vehicleType},
				dataType:'JSON',
				success :function(data)
				{
				    var jsonData = eval(eval(data));
				    if(jsonData.length==1 && jsonData[0].submodel==""){
				      $("#system").change();
				    }else{
				     	$("#system").empty();
				    	$("#system").append( $("<option>").val("").text("<fmt:message key='product.select'/>"));
						$.each(jsonData,function(i){
						var option = $("<option>").val(jsonData[i].submodel).text(jsonData[i].submodel);
						$("#system").append(option);
					});
				    }
				}
		});
		});
		
		
		$("#system").change(function(){
			var carYear=$("#carYear option:selected").val();
			var mark=$("#vehicleSystem option:selected").val();
			var vehicleType=$("#vehicleType option:selected").val();
			var system=$("#system option:selected").val();
			$.ajax({
				url:'front/sealer/getVehicleJson.do',
				type:"post",
				async:false,
				data:{"vehicle.queryType":"5","vehicle.yearName":carYear,"vehicle.mark":mark,"vehicle.model":vehicleType,"vehicle.submodel":system},
				dataType:'JSON',
				success :function(data)
				{
				    var jsonData = eval(eval(data));
				    $("#engineType").empty();
				    $("#engineType").append( $("<option>").val("").text("<fmt:message key='product.select'/>"));
					$.each(jsonData,function(i){
						var option = $("<option>").val(jsonData[i].engine).text(jsonData[i].engine);
						$("#engineType").append(option);
					});
					
				}
		});
		});
		
	
	
	
	
	$("#subBtn").click(function()
	{
		var complaintPerson = $("#complaintPerson").val();
		var company = $("#company").val();
	
		var phone = $("#phone").val();
		var email = $("#email").val();
		var complaintTitle = $("#complaintTitle").val();
		var userType = $("#userType").val();
		var $form = $("#addCustomerComplaint");

		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		var regNumber = /^[0-9]\d*$/;
		var reVIN = /^[A-Za-z0-9]+$/i;
	
		var regsubectName = /^[\u4e00-\u9fa5-A-Za-z0-9_ ]+$/i;
		var operationResultTip = $("#operationResultTip");
		
		
		
		if (complaintPerson == null || complaintPerson == '')
		{
			operationResultTip.html("<fmt:message key='customercomplaint.person.isnotnull'/>");
			return;
		}
		
		
		operationResultTip.html("&nbsp;");

		if (company == null || company == '') 
		{
			operationResultTip.html("<fmt:message key='customercomplaint.company.isnotnull'/>");
			return;
		}
		
		operationResultTip.html("&nbsp;");

		

		if (phone == null || phone == '')
		{
			operationResultTip.html("<fmt:message key='customercomplaint.phone.isnotnull'/>");
			return;
		}
	
		
		operationResultTip.html("&nbsp;");

		if (email == null || email == '') {
			operationResultTip.html("<fmt:message key='customercomplaint.email.isnotnull'/>");
			return;
		}
		else if(!regEmail.test(email))
		{
			operationResultTip.html("<fmt:message key='customercomplaint.email.illegalcharactercheck'/>");
			return;
		}
		
		operationResultTip.html("");

		if (complaintTitle == null || complaintTitle == '')
		{
			operationResultTip.html("<fmt:message key='customercomplaint.title.isnotnull'/>");
			return;
		}
		/*else if(!regsubectName.test(complaintTitle))
		{
			operationResultTip.html("<fmt:message key='customercomplaint.title.illegalcharactercheck'/>");
			return;
		}*/
		operationResultTip.html("&nbsp;");
		/*20140212*/
		var VINCode=$('#carnumber').val();
		var softwareName=$('#softwareName').val();
		var softwareEdition=$('#softwareEdition').val();
		var productNo=$('#productNo').val();
		if(VINCode==null || VINCode==""){
		  	operationResultTip.html("<fmt:message key='customer.complaint.valid.vin'/>");
			return;
		}
		operationResultTip.html("&nbsp;");
		if(productNo==null || productNo==""){
		  	operationResultTip.html("<fmt:message key='sealer.complaint.add.productsn'/>");
			return;
		}
		operationResultTip.html("&nbsp;");
		if(softwareName==null || softwareName==""){
		  	operationResultTip.html("<fmt:message key='customer.complaint.valid.softname'/>");
			return;
		}
		operationResultTip.html("&nbsp;");
		if(softwareEdition==null || softwareEdition==""){
		  	operationResultTip.html("<fmt:message key='customer.complaint.valid.softversion'/>");
			return;
		}
		operationResultTip.html("&nbsp;");
	    
	    if($("#vehicleSystem").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.vehicleSystem'/>");
			return;
	    }	
	 
	    operationResultTip.html("&nbsp;");
	    
	    if($("#vehicleType").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.vehicleType'/>");
			return;
	    }	
	 
		operationResultTip.html("&nbsp;");
		
		if($("#carYear").val()!="" &&!regNumber.test($("#carYear").val())){
		    operationResultTip.html("<fmt:message key='customercomplaint.carYear.illegalcharactercheck'/>");
			return;
		}
		operationResultTip.html("&nbsp;");
		
	    
	    <%-- if($("#diagnosticConnector").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.diagnosticConnector'/>");
			return;
	    }	
		operationResultTip.html(""); --%>
		
		if($("#engineType").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.engineType'/>");
			return;
	    }	
		operationResultTip.html("&nbsp;");
		
		if($("#system").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.system'/>");
			return;
	    }	
		operationResultTip.html("&nbsp;");
		
		
		if($("#carnumber").val()!="" && !reVIN.test($("#carnumber").val())){
		    operationResultTip.html("<fmt:message key='complaint.message.vin'/>");
			return;
		}
		
	<%-- 	if($("#other").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.other'/>");
			return;
	     }	
		operationResultTip.html(""); --%>
		
		if($("#softwareName").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.softwareNamer'/>");
			return;
	     }	
		operationResultTip.html("&nbsp;");
		if($("#softwareEdition").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.softwareEdition'/>");
			return;
	     }	
		
		operationResultTip.html("&nbsp;");
		if($("#adapter").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.adapter'/>");
			return;
	     }	
		operationResultTip.html("&nbsp;");
		if($("#datalogging").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.datalogging'/>");
			return;
	     }	
	     
	    operationResultTip.html("&nbsp;");
		<%-- if($("#testPath").val().indexOf('"')!=-1){
	        operationResultTip.html("<fmt:message key='complaint.message.testPath'/>");
			return;
	     }	 --%>
	     var productSN=$('#productSN').val();
	     var userCode=$('#userCode').val();
		var options = { 
				url : "widget.do?type=customerComplaintInfoWidget&operationType=61&ajax=yes",
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
					if(result=="4") 
					{
						operationResultTip.html("<fmt:message key='customercomplaint.addcomplaint.success'/>");
						
						jump("queryCustomerInfo-1-1.html?m=1&userType=2&type=S3&code="+userCode+"&productSN="+productSN);
						
						return;
					} 
					else if(result=="2")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.uploadfilecheck.name'/>");
						$("#subBtn").removeAttr("disabled");
						return;
					}
					else if(result=="1")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.uploadfiletypecheck.name'/>");
						$("#subBtn").removeAttr("disabled");
						return;
					}
					else if(result=="3")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.addcomplaint.fail'/>");
						$("#subBtn").removeAttr("disabled");
						return;
					}
				},
				error : function(e) 
				{
					$("#subBtn").removeAttr("disabled");
					operationResultTip.html("<fmt:message key='common.system.error'/>");
				}
		};
        
		$form.ajaxSubmit(options);
		$("#subBtn").attr("disabled","true");
	
	});
	$("#previosBtn").click(function()
	{
		var productSN=$('#productSN').val();
	    var userCode=$('#userCode').val();
		jump("queryCustomerInfo-1-1.html?m=1&userType=2&type=S3&code="+userCode+"&productSN="+productSN);
	});
	
});

