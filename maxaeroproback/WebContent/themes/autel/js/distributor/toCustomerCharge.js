

$(document).ready(function(){
	
	
	$("#queryToCustomerCharge").click(function()
	{
		var serialNo = $("#serialNo").val();
		var $form = $("#toCustomerChargeForm");
		var regSerialNo = /^[A-Za-z0-9_]+$/i;
		var serialNoTip = $("#serialNoTip");
		
		if(serialNo !=null && serialNo !='')
		{
			if(!regSerialNo.test(serialNo))
			{
				serialNoTip.html("产品序列号不能包含非法字符!");
				return;
			}
		}
		
		serialNoTip.html("");
		
		$form.action="toCustomerCharge-1-1.html?operationType=1&serialNo="+serialNo;
		$form.submit();
	});
	
	$("#reChargeCard").click(function()
	{
		var reChargeCardSerialNo = $("#reChargeCardSerialNo").val();
		var reChargeCardPassword = $("#reChargeCardPassword").val();
		var proCode = $("#proCode").val();
		var proSerialNo = $("#proSerialNo").val();
		var memberName= $("#memberName").val();
		var reChargeTime= $("#reChargeTime").val();
		var proTypeCode = $("#proTypeCode").val();
		var areaCfgCode =  $("#areaCfgCode").val();
		var saleCfgCode =  $("#saleCfgCode").val();
		var resultTip = $("#resultTip");
		
		if(reChargeCardSerialNo == null || reChargeCardSerialNo == '')
		{
			resultTip.html("充值卡账号不能为空!");
			return;
		}
		
		resultTip.html("");
		
		if(reChargeCardPassword == null || reChargeCardPassword == '')
		{
			resultTip.html("充值卡密码不能为空!");
			return;
		}
		
		resultTip.html("");
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/reChargeCardOperation.do',
			type:"post",
			async:false,
			data:{"reChargeCardVo.reChargeCardSerialNo":reChargeCardSerialNo,
				"reChargeCardVo.reChargeCardPassword":reChargeCardPassword,"reChargeCardVo.proCode":proCode,
				"reChargeCardVo.proSerialNo":proSerialNo,"reChargeCardVo.proTypeCode":proTypeCode,
				"reChargeCardVo.areaCfgCode":areaCfgCode,"reChargeCardVo.saleCfgCode":saleCfgCode,
				"reChargeCardVo.memberName":memberName,
				"reChargeCardVo.reChargeTime":reChargeTime,"reChargeCardVo.memberType":2},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				
				if(jsonData[0].reChargeCardInfo == "false")
				{
					resultTip.html("充值卡账号或密码不正确!");
					resultCode = false;
					return;
				}
				else if(jsonData[0].isActive == "1")
				{
					resultTip.html("充值卡未激活!");
					resultCode = false;
					return;
				}
				else if(jsonData[0].chargeCardIsUse == "2")
				{
					resultTip.html("充值卡已使用!");
					resultCode = false;
					return;
				}
				else if(jsonData[0].valiDate == "false")
				{
					resultTip.html("充值卡已过有效期!");
					resultCode = false;
					return;
				}
				
				if(jsonData[0].proNoReCharge == "false")
				{
					resultTip.html("该产品不能续租!");
					resultCode = false;
					return;
				}
				
				if(jsonData[0].reChargeResult == "false")
				{
					resultTip.html("该充值卡不能使用该产品!");
					resultCode = false;
					return;
				}
				
				if(jsonData[0].result == "true")
				{
					resultTip.html("续租成功!");
					resultCode = true;
				}
				else
				{
					resultTip.html("续租失败!");
					resultCode = false;
				}
				
				
			},
			error :function(){
				resultTip.html("系统异常、请联系管理员....");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}

		window.location.href="toCustomerCharge-1-1.html?operationType=1";
	
	});
});

function querySoftwareInfo(saleCfgCode)
{
	window.location.href="softwareVersionInfo.html?operationType=2&saleCfgCode="+saleCfgCode;
}

	
function xuzuOperation(proCode,proSerialNo,proName,memberName,proTypeCode,areaCfgCode,saleCfgCode)
{
	$("#chongzhi").show();
	$("#proCode").val(proCode);
	$("#proSerialNo").val(proSerialNo);
	$("#proName").val(proName);
	$("#memberName").val(memberName);
	$("#proTypeCode").val(proTypeCode);
	$("#areaCfgCode").val(areaCfgCode);
	$("#saleCfgCode").val(saleCfgCode);
}