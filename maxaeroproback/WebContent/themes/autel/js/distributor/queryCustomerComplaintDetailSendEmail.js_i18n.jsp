<%@ include file="../common.js_i18n.jsp" %> 

//win_complaint控件
var $win_complaint;

//overlay控件
var $overlay;

//close控件
var $close;


var textAreaLength = 2000;

function textAreaOnchange()
{
   var reContent = $("#reContent").val();
   
   
   if(reContent.length > textAreaLength)
   {
     $("#titileResultTip").html("<fmt:message key='customercomplaint.complaintcontent.maxlength'/>");
     $("#titileResultTip").show();
   }
   else 
   {
      $("#titileResultTip").hide();
   
   }
}

function textAreaOnchangeSuspend()
{
  var reContentSuspend = $("#guaqiContent").val();
  if(reContentSuspend.length > textAreaLength)
   {
     $("#SuspendTip").html("<fmt:message key='customercomplaint.complaintcontent.maxlength'/>");
     $("#SuspendTip").show();
   }
   else 
   {
      $("#SuspendTip").hide();
   
   }
   
}

$(document).ready(function()
{
    
	$("#tijiao").click(function()
	{
	    //add by longjiachun begin
	    //for send email params.
	    var code = $("#code").val();
	    var autelId = $("#autelId").val();
	    //add by longjiachun end.
		var $form = $("#replyCustomerComplaint");
		var userType = $("#userType").val();
		var cusComCode = $("#cusComCode").val();
		var reContent = $("#reContent").val();
		var operationResultTip = $("#titileResultTip");
		var operationResult=$("#titileResultTip");
		var titileResultTip=$("#titileResultTip");
		operationResult.show();
		
		
		if(reContent == null || reContent == "")
		{
			$("#success").hide();
			$("#operationResult").hide();
			titileResultTip.html("<fmt:message key='customercomplaint.complaintcontent.isnotnull'/>");
			return;
		}
		else if(reContent.length > textAreaLength)
		{
		   operationResultTip.html("<fmt:message key='customercomplaint.complaintcontent.maxlength'/>");
		   return;
		}
		
		
		titileResultTip.html("");
		
		var resultCode="";
		
		var options = { 
				url : "widget.do?type=customerComplaintInfoWidget&operationType=12&rePersonType=1&ajax=yes" + "&code=" + code+ "&autelId=" + autelId,
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
					if (result=="4") 
					{
						operationResultTip.html("<fmt:message key='customercomplaint.replycomplaint.success'/>");
						//jump("queryCustomerComplaintDetail.html?operationType=3&code="+cusComCode+"&userType="+userType);
						jump("queryCustomerComplaintDetailSendEmail.html?operationType=3&code="+cusComCode+"&userType="+userType);
						return;
					} 
					else if(result=="2")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.uploadfilecheck.name'/>");
						return;
					}
					else if(result=="1")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.uploadfiletypecheck.name'/>");
						return;
					}
					else if(result=="3")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.replycomplaint.fail'/>");
						return;
					}
				},
				error : function(e) 
				{
					operationResultTip.html("<fmt:message key='common.system.error'/>");
				}
		};

		$form.ajaxSubmit(options);
	});
	
	$("#update_id").click(function()
	{
		var $form = $("#replyCustomerComplaint");
		var userType = $("#userType").val();
		var operationResultTip = $("#operationResultTip");
		var cusComCode = $("#cusComCode").val();
		var successDiv = $("#success");
		
		var options = { 
				url : "widget.do?type=customerComplaintInfoWidget&operationType=9&ajax=yes",
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
					if (result) 
					{
						successDiv.show();
						jump("queryCustomerComplaintDetail.html?operationType=3&code="+cusComCode+"&userType="+userType);
						return;
					} 
					else 
					{
						successDiv.show();
						return;
					}
				},
				error : function(e) 
				{
					operationResultTip.html("<fmt:message key='common.system.error'/>");
				}
		};
		

		$form.ajaxSubmit(options);
	});
	
	$("#refresh").click(function()
	{
		var userType = $("#userType").val();
		var cusComCode = $("#cusComCode").val();
		jump("queryCustomerComplaintDetail.html?operationType=3&code="+cusComCode+"&userType="+userType);
	});
	
 	$("#reply_id").click(function()
	{
            $("#guaqikefu_Div").hide();
		    $win_complaint.hide();
		    $("#show_reply_connect").toggle();
    }); 

	


	
	$win_complaint = $("#win_complaint");
	$overlay = $("#overlay");
	$close = $("#close");
	
	$win_complaint.hide();
	$overlay.hide();
		/**
	 * 初始化遮罩层大小
	 */
	if($win_complaint.length > 0){
		$overlay.css("height",$("#customerDiv").height());
	}
	
	
    /**
	 * 弹出分配客服窗口
	 */
	$("#transcomplaint_id").click(function()
	{
	   var showOrHidden = $("#showOrHiddenTranslationDiv").val();
	
	    if(0 == showOrHidden)
	    {
	        document.getElementById("showOrHiddenTranslationDiv").value = 1; 
	        $("#guaqikefu_Div").hide();
		    $("#show_reply_connect").hide();
		
		    $win_complaint.show();
		    $overlay.show();
	    }
	    else if(1 == showOrHidden)
	    {
	        document.getElementById("showOrHiddenTranslationDiv").value = 0; 
	        $win_complaint.hide();
	    }
	   

	});
	
	/**
	 * 关闭分配客服窗口
	 */
	$close.click(function ()
	{
		$win_complaint.hide();
	    $overlay.hide();
	});
	
    /**
	 * 转发客诉
	 */
	$(".selacceptortrans").click(function(){
		var $this = $(this);
		var acceptor = $this.attr("acceptor");
		selAcceptorTrans(acceptor);
	});
	
	/**
	 * 挂起客诉
	 */
	$("#stateGuaqi_id").click(function(){
		
		$win_complaint.hide();
		$("#show_reply_connect").hide();
		$("#guaqikefu_Div").show();
	});
	
	
	/**
	 * 提交挂起客诉原因
	 */
	$("#guaqiButton").click(function(){
		submitSuspend();
	});

});


//提交挂起客诉原因
function submitSuspend()
{
   	var userType = $("#userType").val();
    var cusComCode = $("#cusComCode").val();
	var code = $("#code").val();
	var result = false;
    var complaintState = 2;//guaqiContent
    var reason = $("#guaqiContent").val();
    document.getElementById('guaqiContent2').value = reason;
    
    var reContentSuspend = $("#guaqiContent").val();
    if(reContentSuspend.length > textAreaLength)
   {
     $("#SuspendTip").html("<fmt:message key='customercomplaint.complaintcontent.maxlength'/>");
     $("#SuspendTip").show();
     return;
   }

    var autelId = $("#autelId").val();
    var ajaxUrl = "widget.do?type=customerComplaintInfoWidget&operationType=11&ajax=yes&code="+
                   code+"&complaintState="+complaintState +"&reason="+reason +"&autelId="+autelId ;
	var options = { 
				url : ajaxUrl,
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
				    $("#guaqikefu_Div").hide();
                    //refresh the page.
				    jump("queryCustomerComplaintDetailSendEmail.html?operationType=3&code="+cusComCode+"&userType="+userType);
					if (result) 
					{
				
			           // alert('<fmt:message key="memberman.complaintman.message5" />');
					} 
					else 
					{
						//alert('<fmt:message key="memberman.complaintman.message6" />');
					}
				},
				error : function(e) 
				{
					alert('<fmt:message key="memberman.complaintman.message6" />');
				}
		};
		
    $("#ForWardCustomerComplaint").ajaxSubmit(options);
}


//转发客诉方法
function selAcceptorTrans(acceptor)
{
		var code = $("#code").val();
	    var result = -1;

		var options = { 
				url : "widget.do?type=customerComplaintInfoWidget&operationType=10&ajax=yes&code="+code+"&acceptor="+acceptor ,
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
					if (result) 
					{
						$win_complaint.hide();
	                    $overlay.hide();
			            //alert('<fmt:message key="memberman.complaintman.message5" />');
					} 
					else 
					{
						//alert('<fmt:message key="memberman.complaintman.message6" />');
					}
					
					jump("salerComplaintInfo-1-1.html?m=1&userType=2&operationType=1");
				},
				error : function(e) 
				{
					alert('<fmt:message key="memberman.complaintman.message6" />');
				}
		};
		
		$("#ForWardCustomerComplaint").ajaxSubmit(options);

}

function replyshowdiv()
{
	$("#show_reply_connect").show();
}
function downLoadProductTools(attachment,sourceType)
{
	var cusComCode = $("#cusComCode").val();
	var userType = $("#userType").val();
	var operationResultTip= $("#operationResultTip");
	
	if(attachment == null || attachment =='')
	{
		operationResultTip.html("<fmt:message key='customercomplaint.attachment.isnotnull'/>");
		return;
	}
	
	operationResultTip.html("");
	
		
	if(sourceType==2){
		jump("<fmt:message key='autelsupport.url'/>/attachment/"+attachment);
	}else{
	jump("downLoadProductTools.html?operationType=2&type=2&downToolPath="+attachment+"&userType="+userType+"&code="+cusComCode);
	}

}


function queryComplaintTableDetail(code)
{
	var userType = $("#userType").val();
	jump("queryCustomerComplaintTableDetail.html?operationType=8&code="+code+"&userType="+userType);
}


	
function setAcceptState(obj)
{
	$("#acceptState").val(obj.value);
}





//å³é­åéå®¢æçªå£æ¹æ³
function colseWin()
{	
	$win_tc.hide();
	$overlay.hide();
}

var itemMetaData = new Array();
var selected_color = "#F8B704";	//éä¸çé¢è²
var uselected_color = "gray";    //æªéçé¢è²

function Star(nowindex,startnum,startname){
	this.nowindex = nowindex;
	this.startnum = startnum;
	this.startname = startname;
}

Star.prototype.initstar = function(id)
{
	this.startname = "start"+id+"_starValueHidden";
	var html = '' ;
	
	for (var i = 1; i <= this.startnum; i++) 
	{
		var sid = this.startname + i;
		html+='<span style="font-size:15px;color:gray;" class="'+sid+'" title="'+i+'<fmt:message key='customercomplaint.star.name'/>" ><strong><fmt:message key='mycomplaint.xing.name'/></strong></span>';
	}
	
	html+="&nbsp;&nbsp;</br>";
	var rev_ = $(".rev_"+id);
	rev_.html(html);
	istar(this.startname,this.startnum,this.nowindex);
}

function onmousoutshow(startname,startnum){
	var index = _p(startname).value;
	setstar(startname,startnum,new Number(index))
}

/**
åå§å
*/
function istar(startname,startnum,index){

	   for (var i = 1; i <= index; i++)
	   {
			_p(startname+i).css("color",selected_color);
			_p(startname+i).css("cursor","hand");
		}
		for (var i = (index + 1); i <= startnum; i++) {
			_p(startname+i).css("color",uselected_color);
			_p(startname+i).css("cursor","hand");
		}
}

/*é¼ æ æ»è¿*/
function setstar(rev_,startname,startnum,index)
{
	var vp = $(rev_).parent();
    for (var i = 1; i <= index; i++) {
		_p(startname+i).css("color",selected_color);
		_p(startname+i).css("cursor","hand");
	}
	for (var i = (index + 1); i <= startnum; i++) {
		_p(startname+i).css("color",uselected_color);
		_p(startname+i).css("cursor","hand");
	}
}


function _p(id)
{
	return $("."+id);
}
function _q(id)
{
	return $(id);
}

function clickstar(startname,startnum,index,id) 
{
	_p(startname).value=index;
	
	setstar(null,startname,startnum,index);
	
	var resultCode = false;
	
	$.ajax({
		url:'front/user/updateCustomerComplaintScore.do',
		type:"post",
		async:false,
		data:{"reCustomerComplaintInfoVO.id":id,"reCustomerComplaintInfoVO.complaintScore":index},
		dataType:'JSON',
		success :function(data)
		{
			resultCode = true;
		},
		error :function(){
			alert("<fmt:message key='common.system.error'/>");
		}
	});
	
	if(resultCode == true)
	{
		var userType = $("#userType").val();
		var code = $("#cusComCode").val();
		
		jump("queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType);
	}

}

function back()
{
	//jump("queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType);
	jump("salerComplaintInfo-1-1.html?m=1&userType=2&operationType=1");
	
}

