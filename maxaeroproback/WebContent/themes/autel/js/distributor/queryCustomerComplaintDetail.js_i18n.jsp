<%@ include file="../common.js_i18n.jsp" %> 
$(document).ready(function()
{

	$("#tijiao").click(function()
	{
		var $form = $("#replyCustomerComplaint");
		var userType = $("#userType").val();
		var cusComCode = $("#cusComCode").val();
		var reContent = $("#reContent").val();
		var operationResultTip = $("#titileResultTip");
		var operationResult=$("#titileResultTip");
		var titileResultTip=$("#titileResultTip");
		operationResult.show();
		
		if(reContent == null || reContent == "")
		{
			$("#success").hide();
			$("#operationResult").hide();
			titileResultTip.html("<fmt:message key='customercomplaint.complaintcontent.isnotnull'/>");
			return;
		}
		else if(reContent.length > textAreaLength)
		{
		   operationResultTip.html("<fmt:message key='customercomplaint.complaintcontent.maxlength'/>");
		   return;
		}
		
		titileResultTip.html("");
		
		var resultCode="";
		
		var options = { 
				url : "widget.do?type=customerComplaintInfoWidget&operationType=4&rePersonType=1&ajax=yes",
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
					if (result=="4") 
					{
						operationResultTip.html("<fmt:message key='customercomplaint.replycomplaint.success'/>");
						jump("queryCustomerComplaintDetail.html?operationType=3&code="+cusComCode+"&userType="+userType);
						return;
					} 
					else if(result=="2")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.uploadfilecheck.name'/>");
						return;
					}
					else if(result=="1")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.uploadfiletypecheck.name'/>");
						return;
					}
					else if(result=="3")
					{
						operationResultTip.html("<fmt:message key='customercomplaint.replycomplaint.fail'/>");
						return;
					}
				},
				error : function(e) 
				{
					operationResultTip.html("<fmt:message key='common.system.error'/>");
				}
		};

		$form.ajaxSubmit(options);
	});
	
	$("#update_id").click(function()
	{
		var $form = $("#replyCustomerComplaint");
		var userType = $("#userType").val();
		var operationResultTip = $("#operationResultTip");
		var cusComCode = $("#cusComCode").val();
		var successDiv = $("#success");
		
		var options = { 
				url : "widget.do?type=customerComplaintInfoWidget&operationType=9&ajax=yes",
				type : "POST",
				async:false,
				dataType : "json",
				success : function(result)
				{
					if (result) 
					{
						successDiv.show();
						jump("queryCustomerComplaintDetail.html?operationType=3&code="+cusComCode+"&userType="+userType);
						return;
					} 
					else 
					{
						successDiv.show();
						return;
					}
				},
				error : function(e) 
				{
					operationResultTip.html("<fmt:message key='common.system.error'/>");
				}
		};
		

		$form.ajaxSubmit(options);
	});
	
	$("#refresh").click(function()
	{
		var userType = $("#userType").val();
		var cusComCode = $("#cusComCode").val();
		jump("queryCustomerComplaintDetail.html?operationType=3&code="+cusComCode+"&userType="+userType);
	});
	
	$("#reply_id").click(function()
	{
		$("#show_reply_connect").toggle();
	});

});

function replyshowdiv()
{
	$("#show_reply_connect").show();
}
function downLoadProductTools(attachment,sourceType)
{
	var cusComCode = $("#cusComCode").val();
	var userType = $("#userType").val();
	var operationResultTip= $("#operationResultTip");
	
	if(attachment == null || attachment =='')
	{
		operationResultTip.html("<fmt:message key='customercomplaint.attachment.isnotnull'/>");
		return;
	}
	
	operationResultTip.html("");
	
	if(sourceType==2){
		jump("<fmt:message key='autelsupport.url'/>/attachment/"+attachment);
	}else{
		jump("downLoadProductTools.html?operationType=2&type=2&downToolPath="+attachment+"&userType="+userType+"&code="+cusComCode);
	}
}


function queryComplaintTableDetail(code)
{
	var userType = $("#userType").val();
	jump("queryCustomerComplaintTableDetail.html?operationType=8&code="+code+"&userType="+userType);
}


	
function setAcceptState(obj)
{
	$("#acceptState").val(obj.value);
}





//关闭分配客服窗口方法
function colseWin()
{	
	$win_tc.hide();
	$overlay.hide();
}

var itemMetaData = new Array();
var selected_color = "#F8B704";	//选上的颜色
var uselected_color = "gray";    //未选的颜色

function Star(nowindex,startnum,startname){
	this.nowindex = nowindex;
	this.startnum = startnum;
	this.startname = startname;
}

Star.prototype.initstar = function(id)
{
	this.startname = "start"+id+"_starValueHidden";
	var html = '' ;
	
	for (var i = 1; i <= this.startnum; i++) 
	{
		var sid = this.startname + i;
		html+='<span style="font-size:15px;cursor:pointer;color:gray;" class="'+sid+'" title="'+i+'<fmt:message key='customercomplaint.star.name'/>" onclick="clickstar(\''+this.startname+'\','+this.startnum+','+i+','+id+')"><strong><fmt:message key='mycomplaint.xing.name'/></strong></span>';
	}
	
	html+="&nbsp;&nbsp;</br>";
	var rev_ = $(".rev_"+id);
	rev_.html(html);
	istar(this.startname,this.startnum,this.nowindex);
}

function onmousoutshow(startname,startnum){
	var index = _p(startname).value;
	setstar(startname,startnum,new Number(index))
}

/**
初始化
*/
function istar(startname,startnum,index){

	   for (var i = 1; i <= index; i++)
	   {
			_p(startname+i).css("color",selected_color);
			_p(startname+i).css("cursor","hand");
		}
		for (var i = (index + 1); i <= startnum; i++) {
			_p(startname+i).css("color",uselected_color);
			_p(startname+i).css("cursor","hand");
		}
}

/*鼠标滑过*/
function setstar(rev_,startname,startnum,index)
{
	var vp = $(rev_).parent();
    for (var i = 1; i <= index; i++) {
		_p(startname+i).css("color",selected_color);
		_p(startname+i).css("cursor","hand");
	}
	for (var i = (index + 1); i <= startnum; i++) {
		_p(startname+i).css("color",uselected_color);
		_p(startname+i).css("cursor","hand");
	}
}


function _p(id)
{
	return $("."+id);
}
function _q(id)
{
	return $(id);
}

function clickstar(startname,startnum,index,id) 
{
	_p(startname).value=index;
	
	setstar(null,startname,startnum,index);
	
	var resultCode = false;
	
	$.ajax({
		url:'front/user/updateCustomerComplaintScore.do',
		type:"post",
		async:false,
		data:{"reCustomerComplaintInfoVO.id":id,"reCustomerComplaintInfoVO.complaintScore":index},
		dataType:'JSON',
		success :function(data)
		{
			resultCode = true;
		},
		error :function(){
			alert("<fmt:message key='common.system.error'/>");
		}
	});
	
	if(resultCode == true)
	{
		var userType = $("#userType").val();
		var code = $("#cusComCode").val();
		
		jump("queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType);
	}

}

var textAreaLength = 1000;

function textAreaOnchange()
{
   var reContent = $("#reContent").val();
   
   
   if(reContent.length > textAreaLength)
   {
     $("#titileResultTip").html("<fmt:message key='customercomplaint.complaintcontent.maxlength'/>");
     $("#titileResultTip").show();
   }
   else 
   {
      $("#titileResultTip").hide();
   
   }
}


