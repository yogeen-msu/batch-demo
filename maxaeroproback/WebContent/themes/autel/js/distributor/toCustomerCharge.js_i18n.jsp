
<%@ include file="../common.js_i18n.jsp" %>  
$(document).ready(function(){
	
	
	$("#queryToCustomerCharge").click(function()
	{
		var serialNo = $("#serialNo").val();
		var operationType = $("#operationType").val();
		var $form = $("#toCustomerChargeForm");
		var regSerialNo = /^[A-Za-z0-9_]+$/i;
		var serialNoTip = $("#serialNoTip");
		//å»æç©ºæ ¼
	//	serialNo = serialNo.replace(/[ ]/g,"");
		$("#serialNo").val(serialNo);
		
		var proTypeCode=$('#proTypeCodeQuy').val();
		var expDate=$('#expDate').val();
		
	<%-- 	if(serialNo !=null && serialNo !='')
		{
			if(!regSerialNo.test(serialNo))
			{
				serialNoTip.show();
				serialNoTip.html("<fmt:message key='tocustomercharge.productserno.illegalcharactercheck'/>");
				return;
			}
		} --%>
		
		serialNoTip.hide();
		serialNoTip.html("");
		
		//$form.action="toCustomerCharge-1-1.html";
		$form.attr("action","toCustomerCharge-1-1.html?m=1&operationType="+operationType+"&serialNo="+serialNo+"&proTypeCode="+proTypeCode+"&expDate="+expDate);
		$form.submit();
	});
	
	$("#reChargeCard").click(function()
	{
		var reChargeCardSerialNo = $("#reChargeCardSerialNo").val();
		var reChargeCardPassword = $("#reChargeCardPassword").val();
		var proCode = $("#proCode").val();
		var proSerialNo = $("#proSerialNo").val();
		var memberName= $("#memberName").val();
		var reChargeTime= $("#reChargeTime").val();
		var proTypeCode = $("#proTypeCode").val();
		var areaCfgCode =  $("#areaCfgCode").val();
		var saleCfgCode =  $("#saleCfgCode").val();
		var resultTip = $("#resultTip");
		if(proSerialNo == null || proSerialNo == '')
		{
			resultTip.html("<fmt:message key='renew.card.product.null' />");
			return;
		}
		
		resultTip.html("");
		
		if(reChargeCardPassword == null || reChargeCardPassword == '' || reChargeCardPassword.length!=16)
		{
			resultTip.html("<fmt:message key='renew.card.active.lentgh.error'/>");
			return;
		}
		
		resultTip.html("");
		
		$("#reChargeCard").attr("disabled","true");
	    $.ajax({
			url:'front/user/reChargeCardForUser.do',
			type:"post",
			data:{"reChargeCardVo.memberName":memberName,"reChargeCardVo.proSerialNo":proSerialNo,"reChargeCardVo.reChargeCardPassword":reChargeCardPassword},
			dataType:"JSON",
			success:function(data){
				var jsonData=eval(data);
				if(jsonData[0].result=="1"){
					resultTip.html("<fmt:message key='customerinfo.usercode.illegalcharactercheck'/>"); 
				    $("#reChargeCard").removeAttr("disabled");
				}else if(jsonData[0].result=="2"){ //升级卡错误
					resultTip.html("<fmt:message key='renew.card.active.valid'/>"+" <a style='color:#08c;' onclick='javascript:goSupport()'><fmt:message key='renew.card.active.valid.support'/></a>");
				    $("#reChargeCard").removeAttr("disabled");
				}else if(jsonData[0].result=="3"){ //升级卡没有激活
					resultTip.html("<fmt:message key='renew.card.active.error' />");
				    $("#reChargeCard").removeAttr("disabled");
				}else if(jsonData[0].result=="4"){ //升级卡已经被使用
					resultTip.html("<fmt:message key='renew.card.active.valid'/>"+" <a style='color:#08c;' onclick='javascript:goSupport()'><fmt:message key='renew.card.active.valid.support'/></a>");
					$("#reChargeCard").removeAttr("disabled");
				}else if(jsonData[0].result=="5"){ //升级卡使用区域不匹配
					resultTip.html("<fmt:message key='renew.card.active.area.error' />");
				    $("#reChargeCard").removeAttr("disabled");
				}else if(jsonData[0].result=="7"){ //升级卡使用区域不匹配
					resultTip.html("<fmt:message key='renew.card.active.area.error2' />");
				    $("#reChargeCard").removeAttr("disabled");
				}
				else if(jsonData[0].result=="6"){ //升级卡类型和产品型号不匹配
					resultTip.html("<fmt:message key='renew.card.active.valid'/>"+" <a style='color:#08c;' onclick='javascript:goSupport()'><fmt:message key='renew.card.active.valid.support'/></a>");
				    $("#reChargeCard").removeAttr("disabled");
				}else if(jsonData[0].result=="8"){ //错误次数查过五次
					resultTip.html("<fmt:message key='renew.card.active.error.num'/>");
				    $("#reChargeCard").removeAttr("disabled");
				}else if(jsonData[0].result=="9"){
					imagenumErrorInfoTip.html("<fmt:message key='renew.card.fail'/>");
				    $("#reChargeCard").removeAttr("disabled");
				}else{
					resultTip.html("<fmt:message key='renew.card.success'/>");
					setTimeout(jump("toCustomerCharge-1-1.html?operationType=1"),9000);
				}
			},
			error:function(data){
				$("#isCommonErrorInfo").show();
				imagenumErrorInfoTip.html("<fmt:message key='system.error.name' />");
				$("#reChargeCard").removeAttr("disabled");
			}
	});
		
		<%-- $.ajax({
			url:'front/user/reChargeCardOperation.do',
			type:"post",
			async:false,
			data:{"reChargeCardVo.reChargeCardSerialNo":reChargeCardSerialNo,
				"reChargeCardVo.reChargeCardPassword":reChargeCardPassword,"reChargeCardVo.proCode":proCode,
				"reChargeCardVo.proSerialNo":proSerialNo,"reChargeCardVo.proTypeCode":proTypeCode,
				"reChargeCardVo.areaCfgCode":areaCfgCode,"reChargeCardVo.saleCfgCode":saleCfgCode,
				"reChargeCardVo.memberName":memberName,
				"reChargeCardVo.reChargeTime":reChargeTime,"reChargeCardVo.memberType":2},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				
				if(jsonData[0].reChargeCardInfo == "false")
				{
					resultTip.html("<fmt:message key='tocustomercharge.rechargecardandpassword.iserror'/>");
					resultCode = false;
					return;
				}
				else if(jsonData[0].isActive == "1")
				{
					resultTip.html("<fmt:message key='tocustomercharge.rechargecard.isnotactive'/>");
					resultCode = false;
					return;
				}
				else if(jsonData[0].chargeCardIsUse == "2")
				{
					resultTip.html("<fmt:message key='tocustomercharge.rechargecard.isuse'/>");
					resultCode = false;
					return;
				}
				else if(jsonData[0].valiDate == "false")
				{
					resultTip.html("<fmt:message key='tocustomercharge.rechargecard.periodofvalidity'/>");
					resultCode = false;
					return;
				}
				
				if(jsonData[0].proNoReCharge == "false")
				{
					resultTip.html("<fmt:message key='tocustomercharge.productsoftware.isnottocharge'/>");
					resultCode = false;
					return;
				}
				
				
				if(jsonData[0].result == "true")
				{
					resultTip.html("<fmt:message key='tocustomercharge.success'/>");
					resultCode = true;
				}
				else
				{
					resultTip.html("<fmt:message key='tocustomercharge.fail'/>");
					resultCode = false;
				}
				
				
			},
			error :function(){
				resultTip.html("<fmt:message key='common.system.error'/>");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		} --%>

		
	
	});
});

function querySoftwareInfo(saleCfgCode)
{
	jump("softwareVersionInfo.html?operationType=2&saleCfgCode="+saleCfgCode);
}

function goSupport(){
    var reChargeCardPassword=$("#reChargeCardPassword").val();
    var serialNo=$("#proSerialNo").val();
    var url="cardSupport.html?cardNum="+reChargeCardPassword+"&serialNo="+serialNo;
    var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
}	
	
function xuzuOperation(proCode,proSerialNo,proName,memberName,proTypeCode,areaCfgCode,saleCfgCode)
{
	$("#chongzhi").show();
	$("#proCode").val(proCode);
	$("#proSerialNo").val(proSerialNo);
	$("#proName").val(proName);
	$("#memberName").val(memberName);
	$("#proTypeCode").val(proTypeCode);
	$("#areaCfgCode").val(areaCfgCode);
	$("#saleCfgCode").val(saleCfgCode);
	$("#reChargeCard").removeAttr("disabled");
	var resultTip = $("#resultTip");
	resultTip.html("");
}