<%@ include file="../common.js_i18n.jsp" %>  
$(document).ready(function(){
	
	$("#querySaleInfo").click(function()
	{
		var productName = $("#productName").val();
		var $form = $("#saleInfoForm");
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_]+$/i;
		var productNameTip = $("#productNameTip");
		
		//去掉空格
		productName = productName.replace(/[ ]/g,"");
		$("#productName").val(productName);
		
		if(productName != null && productName != '')
		{
			if(!regName.test(productName))
			{
				productNameTip.show();
				productNameTip.html("<fmt:message key='customercomplaint.product.illegalcharactercheck'/>");
				return;
			}
		}
		productNameTip.hide();
		
		$form.action="saleInfo-1-1.html";
		$form.submit();
	});
});