
<%@ include file="../common.js_i18n.jsp" %>  

$(document).ready(function()
{
	
	$("#queryAttachment").click(function()
	{
		var attachment = $("#attachment").val();
		var cusComCode = $("#cusComCode").val();
		var userType = $("#userType").val();
		var operationResultTip= $("#operationResultTip");
		var sourceType=$("#sourceType").val();
		
		if(attachment == null || attachment =='')
		{
			operationResultTip.html("<fmt:message key='customercomplaint.attachment.isnotnull'/>");
			return;
		}
		
		operationResultTip.html("");
		 if(sourceType=="2"){
		 	jump("<fmt:message key='autelsupport.url'/>/attachment/"+attachment);
		 }else{
		 	jump("downLoadProductTools.html?operationType=2&type=1&downToolPath="+attachment+"&userType="+userType+"&code="+cusComCode);
		 }
	});
});

		
function back()
{
	var code = $("#cusComCode").val();
	var userType = $("#userType").val();
	
	if(2 == userType)
	{
	  jump("queryCustomerComplaintDetailSendEmail.html?operationType=3&code="+code+"&userType="+userType);
	}
	else
	{
	   jump("queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType);
	}
	
	
}