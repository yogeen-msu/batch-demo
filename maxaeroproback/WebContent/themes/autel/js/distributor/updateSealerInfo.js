$(document).ready(function(){
	
	$("#checkUpdateSealerInfo").click(function()
	{
		var autelId = $("#autelId").val();
		var firstName = $("#firstName").val();
		var middleName = $("#middleName").val();
		var lastName = $("#lastName").val();
		var email = $("#email").val();
		var languageCode = $("#languageCode").val();
		var address = $("#address").val();
		var country = $("#country").val();
		var company = $("#company").val();
		var zipCode = $("#zipCode").val();
		var secondEmail = $("#secondEmail").val();
		var city = $("#city").val();
		var mobilePhone = $("#mobilePhone").val();
		var mobilePhoneAC = $("#mobilePhoneAC").val();
		var mobilePhoneCC = $("#mobilePhoneCC").val();
		var daytimePhone = $("#dayTimePhone").val();
		var daytimePhoneAC = $("#dayTimePhoneAC").val();
		var daytimePhoneCC = $("#dayTimePhoneCC").val();
		var IsAllowSendEmail = $("#isAllowSendEmail").val();
		var code = $("#code").val();
		var regEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		var regName = /^[\u4e00-\u9fa5-A-Za-z0-9_]+$/i;
		var regNumber = /^[0-9]\d*$/;
		
		var firstNameTip = $("#firstNameTip");
		var middleNameTip = $("#middleNameTip");
		var lastNameTip = $("#lastNameTip");
		var emailTip = $("#emailTip");
		var secondEmailTip = $("#secondEmailTip");
		var phoneTip = $("#phoneTip");
		var mobilePhoneTip = $("#mobilePhoneTip");
		var languageCodeTip = $("#languageCodeTip");
		var operationResultTip = $("#operationResultTip");
		var countryTip = $("#countryTip");
		
		operationResultTip.html("");
		
		if(firstName != null && firstName != "")
		{
			if(!regName.test(firstName))
			{
				firstNameTip.html("firstName不能有非法字符!");
				return;
			}
		}
		
		
		firstNameTip.html("");
		
		if(middleName != null && middleName != "")
		{
			if(!regName.test(middleName))
			{
				middleNameTip.html("middleName不能有非法字符!");
				return;
			}
		}
		
		
		middleNameTip.html("");
		
		if(lastName != null && lastName != "")
		{
			if(!regName.test(lastName))
			{
				lastNameTip.html("lastName不能有非法字符!");
				return;
			}
		}
	
		
		lastNameTip.html("");
	
		
		if(email == null || email == "")
		{
			emailTip.html("第一邮箱账户不能为空!");
			return;
		}
		else if(!regEmail.test(email))
		{
			emailTip.html("第一邮箱账户格式不正确!");
			return;
		}
		
		emailTip.html("");
		
		if(secondEmail != null && secondEmail !="")
		{
			if(!regEmail.test(secondEmail))
			{
				secondEmailTip.html("第二邮箱账户格式不正确!");
				return;
			}
		}
		
		secondEmailTip.html("");
		
		if(country == null || country == "")
		{
			countryTip.html("国家不能为空!");
			return;
		}
		
		countryTip.html("");
		
		if(daytimePhoneCC !=null && daytimePhoneCC!="")
		{
			if(!regNumber.test(daytimePhoneCC))
			{
				phoneTip.html("电话国家编码只能由数字组成!");
				return;
			}
		}
		
		phoneTip.html("");
		
		if(daytimePhoneAC !=null && daytimePhoneAC!="")
		{
			if(!regNumber.test(daytimePhoneAC))
			{
				phoneTip.html("电话地区编码只能由数字组成!");
				return;
			}
		}
		
		phoneTip.html("");
		
		if(daytimePhone !=null && daytimePhone!="")
		{
			if(!regNumber.test(daytimePhone))
			{
				phoneTip.html("电话只能由数字组成!");
				return;
			}
		}
		
		phoneTip.html("");
		
		if(mobilePhoneCC !=null && mobilePhoneCC!="")
		{
			
			if(!regNumber.test(mobilePhoneCC))
			{
				mobilePhoneTip.html("移动电话国家编码只能由数字组成!");
				return;
			}
		}
		
		mobilePhoneTip.html("");
		
		if(mobilePhoneAC !=null && mobilePhoneAC!="")
		{
			if(!regNumber.test(mobilePhoneAC))
			{
				mobilePhoneTip.html("移动电话地区编码只能由数字组成!");
				return;
			}
		}
		
		mobilePhoneTip.html("");
		
		if(mobilePhone !=null && mobilePhone!="")
		{
			if(!regNumber.test(mobilePhone))
			{
				mobilePhoneTip.html("移动电话只能由数字组成!");
				return;
			}
		}
		
		mobilePhoneTip.html("");
	
		
		if(languageCode == -1)
		{
			languageCodeTip.html("Please Choose Preferred Language!");
			return;
		}
		
		languageCodeTip.html("");
		
		var resultCode = true;
		
		$.ajax({
			url:'front/user/updateSealerInfo.do',
			type:"post",
			async:false,
			data:{"userInfoVo.autelId":autelId,"userInfoVo.firstName":firstName,"userInfoVo.middleName":middleName,
				"userInfoVo.lastName":lastName,"userInfoVo.email":email,"userInfoVo.secondEmail":secondEmail,"userInfoVo.languageCode":languageCode,
				"userInfoVo.address":address,"userInfoVo.country":country,"userInfoVo.company":company,"userInfoVo.code":code,
				"userInfoVo.zipCode":zipCode,"userInfoVo.city":city,"userInfoVo.mobilePhone":mobilePhone,"userInfoVo.mobilePhoneAC":mobilePhoneAC,
				"userInfoVo.mobilePhoneCC":mobilePhoneCC,"userInfoVo.daytimePhone":daytimePhone,"userInfoVo.daytimePhoneAC":daytimePhoneAC,
				"userInfoVo.daytimePhoneCC":daytimePhoneCC,"userInfoVo.IsAllowSendEmail":IsAllowSendEmail},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = eval(data);
				var result = jsonData[0].result;
				
				if(result == "true")
				{
					operationResultTip.html("个人信息修改成功!");
					resultCode = true;
					return;
				}
				else
				{
					operationResultTip.html("个人信息修改失败!");
					resultCode = false;
					return;
				}
				
			},
			error :function(){
				operationResultTip.html("系统异常、请联系管理员....");
				resultCode=false;
			}
		});
		
		if(resultCode == false)
		{
			return;
		}
	});
});

	
	
function setEmail(obj)
{
	if(obj.checked)
	{
		$("#isAllowSendEmail").val(1);
	}
	else
	{
		$("#isAllowSendEmail").val(0);
	}
}
	
	
	
	