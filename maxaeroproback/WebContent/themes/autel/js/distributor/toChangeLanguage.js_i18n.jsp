<%@ include file="../common.js_i18n.jsp" %>  
$(document).ready(function(){
$("#queryToCustomerChange").click(function()
	{
		var serialNo = $("#serialNo").val();
		var operationType = $("#operationType").val();
		var $form = $("#toCustomerChargeForm");
		var productTypeCode=$('#productTypeCode').val();
		$("#serialNo").val(serialNo);
		
		$form.attr("action","toChangeLanguage-1-1.html?m=1&operationType=1&serialNo="+serialNo+"&productTypeCode="+productTypeCode);
		$form.submit();
	});
});


function querySubmit(){
		var serialNo = $("#serialNo").val();
		var operationType = $("#operationType").val();
		var $form = $("#toCustomerChargeForm");
		var productTypeCode=$('#productTypeCode').val();
		$("#serialNo").val(serialNo);
		
		
		$form.attr("action","toChangeLanguage-1-1.html?m=1&operationType=1&serialNo="+serialNo+"&productTypeCode="+productTypeCode);
		$form.submit();
}

function changeOperation(proCode,serialNo,language){
 	changeProduct(language,proCode);
}

function ResetOperation(proCode){
  $.ajax({
			url:'front/sealer/checkCancelProduct.do',
			type:"post",
			async:false,
			data:{"proCode":proCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = data;
				if(jsonData=="NoSession"){
				     jump("login.html");
				}else if(jsonData=="PrductNotRegister"){
				     alert("<fmt:message key='sealer.query.product.reset.notregister'/>");
				     return;
				}else if(jsonData=="TimeOut"){
				     alert("<fmt:message key='sealer.query.product.reset.timeout'/>");
				     return;
				}else if(jsonData=="HadReset"){
					 alert("<fmt:message key='sealer.query.product.reset.hadchanged'/>");
				     return;
				}else if(jsonData=="SUCCESS"){
				     if(confirm("<fmt:message key='sealer.query.product.reset.comfirm'/>")){
				        
				        $.ajax({
						url:'front/sealer/CancelProduct.do',
						type:"post",
						async:false,
						data:{"proCode":proCode},
						dataType:'JSON',
						success :function(data2){
			                var jsonData2 = data2;
			                if(data2=="SUCCESS"){
			                  	alert("<fmt:message key='sealer.query.product.reset.success'/>");
			                  	querySubmit();
			                }else{
			                	alert("<fmt:message key='sealer.query.product.reset.fail'/>");
			                }
						}
						});
				        
				     }
				}
			}
		}); 

}



function changeProduct(language,proCode){

     
      $.ajax({
			url:'front/sealer/checkChangeLanguage.do',
			type:"post",
			async:false,
			data:{"proCode":proCode},
			dataType:'JSON',
			success :function(data)
			{
				var jsonData = data;
				if(jsonData=="TimeOut"){
				     alert("<fmt:message key='sealer.change.language.new.timeout'/>");
				     return;
				}else if(jsonData=="HadChanged"){
				     alert("<fmt:message key='sealer.change.language.new.hadchanged'/>");
				     return;
				}else if(jsonData=="NoSession"){
					 jump("login.html");
				}else if(jsonData=="Success"){
					   var  url="changeLanguage.html?m=1&operationType=2&language="+language+"&proCode="+proCode;
      					    art.dialog.open(url, {
    					    title: '',
	    					lock:true,
		    				cancelVal:'Cancel',
		    				width: 600, 
		    				height: 250,
		    				init: function () {
		    					var iframe = this.iframe.contentWindow;
		    					var top = art.dialog.top;
		    				}, 
		    				cancel: true
		    				});
				}
			}
		}); 
}

	
