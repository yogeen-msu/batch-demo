
$(document).ready(function()
{
	var divValue = document.getElementById("divValue").value; 
	
	//初始化控制选择消息分类div
	if(divValue !=null && divValue !="")
	{
		document.getElementById(divValue).className="hover";
		document.getElementById("all_id").className="";
	}
});

/**
 * 控制选择消息分类div
 * @param n
 * @param msgTypeCode
 */
function setMessageTab(n,msgTypeCode)
{
	if(msgTypeCode != null && msgTypeCode !="" )
	{
		document.getElementById("all_id").className="";
		document.getElementById("span"+n).className="hover";
	}
	
	$("#msgTypeCode").val(msgTypeCode);
	
	queryMessage("span"+n);
}

function queryMessage(divValue)
{
	var form = document.getElementById("myMessageInfoForm");
	var msgTypeCode = $("#msgTypeCode").val();
	var timeRange = $("#timeRange").val();
	var userType = $("#userType").val();
	form.action="myMessageInfo-1-1.html?msgTypeCode="+msgTypeCode+"&userType="+userType+"&timeRange="+timeRange+"&divValue="+divValue;
	form.submit();
}

function setTimeRange()
{
	queryMessage();
}

function queryMessageInfo(code,userType)
{
	window.location.href="queryMyMessageInfo.html?operationType=2&code="+code+"&userType="+userType;
}

function addUserMessageIsRead(userType,messageCode,userCode)
{
	var $form = $("#myMessageInfoForm");
	var resultCode = true;
	
	$.ajax({
		url:'front/user/addUserMessageIsRead.do',
		type:"post",
		async:false,
		data:{"userMessageVo.userType":userType,"userMessageVo.messageCode":messageCode,"userMessageVo.userCode":userCode},
		dataType:'JSON',
		success :function(data)
		{
			var jsonData = eval(data);
			
			if(jsonData[0].result == "false")
			{
				resultCode = false;
				return;
			}
		},
		error :function(){
			alert("系统异常、请联系管理员....");
			resultCode=false;
		}
	});
	
	if(resultCode == false)
	{
		return;
	}
	
	$form.action="myMessageInfo-1-1.html?userType="+userType;
	$form.submit();
}