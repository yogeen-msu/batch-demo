<%@ include file="common.js_i18n.jsp" %> 
$(document).ready(function(){
        $("#giftTypeSelect").change(function(){
            var path=$("#giftTypeSelect option:selected").attr("path");
            if(path != ""){
             $("#imageShow").html("");
             $("<img/>").appendTo("#imageShow").attr("src",path);
            }else{
                $("#imageShow").html("");
            }
        });
});
     function exchange(){
         var proTypeCode = $("#proTypeCode").val();
         var proSerialNo = $("#proSerialNo").val();
         var code = $("#code").val();
         if(proSerialNo == ""){
             $("#inputProSerialNo").html("<fmt:message key='ofn.txt.pn.empty' />");
             return ;
         }
         $("#inputProSerialNo").html("");
//       �ɲ�Ʒ����
         var oProductTypeCode = $("#oProductTypeCode").val();
         if(oProductTypeCode == ""){
            $("#optce").show();
             $("#selectoProductTypeCode").html("<fmt:message key='ofn.txt.opt.empty' />");
             return ;
         }
         $("#selectoProductTypeCode").html("");
         var data = {proTypeCode:proTypeCode,proSerialNo:proSerialNo,oproductTypeCode:oProductTypeCode,code:code};
         if(oProductTypeCode=="MaxiDAS DS708"){
             $(".oprotr").show();
             var oSerialNo = $("#oSerialNo").val();
             if(oSerialNo == ""){
                 $("#inputoSerialNo").html("<fmt:message key='ofn.txt.opn.empty' />");
                 return ;
             }
             $("#inputoSerialNo").html("");
             data.oserialNo = oSerialNo;
             var proRegPwd = $("#proRegPwd").val();
             if(proRegPwd == ""){
                 $("#inputProRegPwd").html("<fmt:message key='ofn.txt.opd.empty' />");
                 return ;
             }
             $("#inputProRegPwd").html("");
             data.proRegPwd = proRegPwd;
         }else{
            var ohterTool = $("#ohterTool").val();
            if(ohterTool == ""){
                 $("#inputohterToole").html("<fmt:message key='ofn.txt.other.tool.empty' />");
                 return ;
             }
             $("#inputohterToole").html("");
             data.oProductTypeCode=ohterTool;
         }
         var giftTypeSelect = $("#giftTypeSelect").val();
         if(giftTypeSelect == ""){
             $("#selectGiftType").html("<fmt:message key='ofn.txt.gift.empty' />");
             return;
         }
        $("#selectGiftType").html("");
        //  var giftType = $("#giftTypeSelect").find("option[value='"+giftTypeSelect+"']").attr("data-giftType");
         data.giftType = giftTypeSelect;
         data.giftProTypeCode = giftTypeSelect;
         
         var consignee = $("#consignee").val();
         if(consignee == ""){
             $("#inputConsignee").html("<fmt:message key='ofn.txt.consignee.empty' />");
             return;
         }
         $("#inputConsignee").html("");
          data.consignee = consignee;
          
         var mailAddress = $("#mailAddress").val();
         if(mailAddress == ""){
             $("#inputMailAddress").html("<fmt:message key='ofn.txt.mail.empty' />");
             return;
         }
         $("#inputMailAddress").html("");
         data.mailAddress = mailAddress;
          var phone = $("#phone").val();
         if(phone == ""){
             $("#inputPhone").html("<fmt:message key='ofn.txt.phone.empty' />");
             return;
         }
         $("#inputPhone").html("");
          data.phone = phone;
         $("#exchangeButton").attr("disabled","true");
         var isCommon = $("#isCommon");
         $.ajax({
             url:'front/usercenter/oldForNew/add.do',
             type:"post",
             data:data,
             dataType:"JSON",
             success:function(result){
                 var jsonData=eval(result);
                 if(jsonData[0].regResult=="notLogin"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='customer.session.not.valid' />");
                 }else if(jsonData[0].regResult=="proNotFind"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.pn.not.find' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="proerror"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.pn.error' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="areaerror"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.area.error' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="timeerror"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.time.error' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="oprouserd"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.oprouserd' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="prouserd"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.prouserd' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="oproAreaError"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.opn.area.error' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="pwderror"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.pwderror' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="proNotReg"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.pro.not.reg' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="regTimeError"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.reg.time.error' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="customerError"){
                     $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.customer.error' />");
                     $("#exchangeButton").removeAttr("disabled");
                 }else if(jsonData[0].regResult=="true"){
                     $("#suss").html("<fmt:message key='ofn.txt.success' />");
                   //  setTimeout(jump("myProducts-1-1.html"),60000);
                 }else{
                    $("#isCommonErrorInfo").show();
                     isCommon.html("<fmt:message key='ofn.txt.fail' />");
                       $("#exchangeButton").removeAttr("disabled");
                 }
                
             },
             error:function(data){
                 $("#isCommonErrorInfo").show();
                 isCommon.html("<fmt:message key='customer.network.not.valid' />");
                 $("#exchangeButton").removeAttr("disabled");
             }
         })
         
     }
     function changeOproType(obj){
        var oProductTypeCode = $(obj).val();
        if(oProductTypeCode=="MaxiDAS DS708"){
            $("#oproop").show();
            $("#oProductTypeTip").html("<fmt:message key='ofn.txt.ds708.opn' />");
               $("#optce").hide();
               $("#selectoProductTypeCode").html("");
                $(".oprotr").show();
                $(".otherTooltr").hide();
        }else if(oProductTypeCode!=""){
            $("#oproop").show();
            $("#oProductTypeTip").html("<fmt:message key='ofn.text.pt.opn' />");
            $("#optce").hide();
            $("#selectoProductTypeCode").html("");
            $(".oprotr").hide();
            $("#oSerialNo").val("");
            $("#inputoSerialNo").html("");
            $("#proRegPwd").val("");
            $("#inputProRegPwd").html("");
             $(".otherTooltr").show();
        }else{
             $(".oprotr").hide();
              $(".otherTooltr").hide();
            $("#oproop").hide();
            $("#optce").show();
            $("#selectoProductTypeCode").html("<fmt:message key='ofn.txt.opt.empty' />");
        }
     }