<%@page import="com.cheriscon.common.model.Language"%>
<%@page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
function jump(url){
	var tmpForm = $("<form  method='post'></form>");
	$(tmpForm).attr("action",url);
	tmpForm.appendTo(document.body).submit();
   
}
<%  
String language = null;
String country = null;

Language languageInfo =(Language)request.getSession().getAttribute("languageInfo");

if(languageInfo == null)
{
	language=request.getLocale().getLanguage();
	if(language.toLowerCase().equals("zh")){
		language="zh";
		country = "CN";
	}else if(language.toLowerCase().equals("de")){ //德语
		language="de";
		country = "DE";
	}else if(language.toLowerCase().equals("es")){ //西班牙语
		language="es";
		country = "LA";
	}else{
		language="en";
		country = "US";
	}
	
}
else
{
	language = languageInfo.getLanguageCode();  
	if(language.equals("zh")){
		language="zh";
		country = "CN";
	}else if(language.toLowerCase().equals("de")){ //德语
		language="de";
		country = "DE";
	}else if(language.toLowerCase().equals("es")){ //西班牙语
		language="es";
		country = "LA";
	}else{
		language="en";
		country = "US";
	}
}
String local =  language +"_"+ country;
request.setAttribute("local",local);
%>
<fmt:setLocale value="${local}" />
<fmt:setBundle basename="backstage" />


