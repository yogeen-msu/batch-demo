<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<c:set var="fieldList" value="${fieldList}" />
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > CMS ><fmt:message key="syssetting.cmsmenu.content.list" /> </div>
<div class="grid">
	<form action="data!list.do" method="post">
	    <input type="hidden" id = "firstIn" name="firstIn" value="${firstIn}"/>
		<input type="hidden" name="catid" value="${catid }"/>
		<div class="toolbar" >
			<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.cmsmenu.content.list" /></h4>
			<div class="sear_table">
				<table width="100%">
					  <tr height="38">    
					  	<c:if test="${languageField != null}">
					  		<td width="111" align="right">${languageField.china_name}:</td>
							<td width="154">
								<input type="hidden" name="languageField.english_name" value="${languageField.english_name}">
								<select name="languageCode" id="languageCode_select">
									<option value=""><fmt:message key="common.sel.choose" /></option>
									<c:forEach var="item" items="${languageList}">
										<option value="${item.code}">${item.name}</option>
									</c:forEach>
								</select>
								<script type="text/javascript">
									var lanCode = "${languageCode}";
									$("#languageCode_select option[value='"+lanCode+"']").attr("selected","selected");
								</script>
							</td>
					  	</c:if>
						<td></td>
						<td width="80px"><fmt:message key="cententman.cms.keyword" /></td>
						<td width="170px" align="left">
							<input type="text" name="searchText" value="${searchText}" />
						</td>
						<td align="left" width="140px">
							<select name="searchField" style="width: 80px;height:24px">
								<c:forEach items="${fieldList}" var="field">
									<c:if test="${field.show_form != 'language_select'}">
										<option value="${field.english_name }">${field.china_name}</option>
									</c:if>
								</c:forEach>
							</select>
						</td>
						<td align="left"><input type="submit" id = "searchButton_id" value=<fmt:message key="common.btn.search" /> class="search_but" /></td>
						<td></td>
					  </tr>
				</table>
			</div>
			<div class="active">
				<h4><span>
					<c:if test="${site.multi_site == 1 }"><input type="button"  id=""importBtn"" value="导入" class="search_but" /></c:if>
					<input type="button"  id="sortBtn" value="<fmt:message key="produdt.js.save" /><fmt:message key="cententman.cms.sort" />" class="search_but" />
					<input type="button" onclick="location.href='data!add.do?catid=${catid}'" value=<fmt:message key="common.btn.add" /> class="search_but" />
				</span><fmt:message key="syssetting.cmsmenu.content.list" /></h4>
			</div>
		</div>
	</form>
	<form id="sortForm" method="post">
		<input type="hidden" name="catid" value="${catid }"/>
		<grid:grid from="webpage">
			<grid:header>
				<grid:cell width="50px">id</grid:cell>
				<th style="width:200px"><fmt:message key="cententman.messageman.title"/></th>
<!--				<th style="width:80px">子栏目</th>-->
				<th style="width:80px"><fmt:message key="cententman.cms.sort" /></th>
				<th style="width:100px"><fmt:message key="cententman.cms.addtime" /></th>
				<th style="width:100px"><fmt:message key="common.list.mod" /></th>
				<th style="width:100px"><fmt:message key="common.list.del" /></th>
			</grid:header> 	
	  		<grid:body item="article">
	  			<grid:cell>${article.id }<input type="hidden" name="ids" value="${article.id }" /> </grid:cell>
	  			<html:field></html:field>
<!--  				<td><c:if test="${article.cat_id!=catid}">${article.cat_name }</c:if></td>-->
  				<td><input type="text" style="width:50px" name="sorts" value="${article.sort }" /></td>
  				<td><html:dateformat pattern="yyyy-MM-dd" time="${article.add_time*1000}"></html:dateformat></td>
	    		<td><a href="data!edit.do?dataid=${article.id }&catid=${catid}"><fmt:message key="common.list.mod" /></a></td>
	    		<td>
	    			<c:if test="${article.sys_lock != 1 }">
	    				<a href="data!delete.do?dataid=${article.id }&catid=${catid}" onclick="javascript:return confirm('<fmt:message key="stmp.message.comfirm3" />');"><fmt:message key="common.list.del" /></a>
					</c:if>
				</td>
			</grid:body>
		</grid:grid>
	</form>
	<div style="clear:both;padding-top:5px;"></div>
	<div id="import_selected"></div>
</div>
<script type="text/javascript">
function updateSort(){
	$.Loading.show('<fmt:message key="syssetting.toolman.message1" />');
	var options = {
			url :"data!updateSort.do?ajax=yes",
			type : "POST",
			dataType : 'json',
			success : function(result) {				
			 	if(result.result==1){
			 		$.Loading.hide();
			 		alert("<fmt:message key='syssetting.toolman.message2' />");
			 		location.reload();
			 	}else{
			 		alert(result.message);
			 	}
			},
			error : function(e) {
				$.Loading.hide();
				alert("<fmt:message key='syssetting.toolman.message3' />");
				}
		};

	$("#sortForm").ajaxSubmit(options);		
}
$(function(){

	/**初始化导入数据对话框**/
	Cop.Dialog.init({id:"import_selected",modal:true,title:"导入数据", width:"600px"});
	
	/**<fmt:message key="cententman.cms.sort" />**/
	$("#sortBtn").click(function(){
		updateSort();
	});

	/**导入按钮点击事件**/
	$("#importBtn").click(function(){

		//抓取导入对话框内容
		$.ajax({
			 type: "GET",
			 url: "${ctx}/cms/admin/data!implist.do?ajax=yes&catid=${catid }",
			 dataType:'html',
			 success: function(result){
				 $("#import_selected").empty().append(result);
		     },
		     error:function(){
				alert("数据列表获取失败");
			 }
		});

		//打开导入对话框
		Cop.Dialog.open("import_selected");
		
	});
});


$(document).ready(function(){
   //alert(125);	
  var value =  Number($("#firstIn").attr("value"));
  //alert("value" + value);
  if(1 == value)
  {
	  $("#searchButton_id").click();
	  //setTi1meout(function(){$("#searchButton_id").click();}, 1000); 
  }
   
});

</script>