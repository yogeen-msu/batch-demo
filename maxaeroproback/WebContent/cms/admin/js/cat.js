var Cat={
	init:function(){
		 var self = this;
		 //保存排序事件
		$("#sortBtn").click(function(){
			self.saveSort();
		});

		//删除类别事件
		$(".delete").click(function(){
			if( confirm(deletemsg) ){
				$.Loading.show(responsemsg);
				self.opBundle=$(this);
				self.doDelete( self.opBundle.attr("catid") );
			}
		});
		
		
	},	doDelete:function(catid){
		var self =this;
		$.ajax({
			 type: "POST",
			 url: "cat!delete.do",
			 data: "ajax=yes&cat_id="+catid,
			 dataType:"json",
			 success: function(result){
				 if(result.result==0){
					$.Loading.hide();
				 	alert(result.message);
			     }else{
			 		self.opBundle.parents("tr").remove();
				    $.Loading.hide();
				 }
			 },
			 error:function(){
				 $.Loading.hide();
				 alert(failmsg);
			 }
		}); 		
	},
	saveSort:function(){
		$.Loading.show(savemsg);
		var options = {
				url :"cat!saveSort.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {				
				 	if(result.result==1){
				 		$.Loading.hide();
				 		alert(success);
				 		location.reload();
				 	}else{
				 		alert(result.message);
				 	}
				},
				error : function(e) {
					$.Loading.hide();
					alert(failmsg);
 				}
 		};
 
		$("form").ajaxSubmit(options);		
	}
	
};