function getCookie(objName) {// 获取指定名称的cookie的值  
    var arrStr = document.cookie.split("; ");  
    for (var i = 0; i < arrStr.length; i++) {  
        var temp = arrStr[i].split("=");  
        if (temp[0] == objName) {  
            return temp[1];  
        }  
    }  
    return null;  
}

//指定当前可用的语言(注意全部用小写，方便比较)
var $lans =['zh-cn','en-us'];

//获取并判断当前语言是否存在
var in18_lan = getCookie("in18_lan"); 		//从cookie获取当前语言环境,后台设置
if(!in18_lan) {		//
	in18_lan = "en-us";  
}else{
	
	//判断当前语言是否为可用的语言，没有则用默认
	var lansLength = $lans.length;
	var $canUse = false;
	while(lansLength--){
		if($lans[lansLength] == in18_lan){
			$canUse = true;
			break;
		}
	}
	
	if(!$canUse){
		in18_lan = "en-us";  
	}
}

//定义全局的语言参数
var In18Lan = {
	validator:[]
};

///////////////////////////////////begin 后台验证/////////////////////////////////////////////
In18Lan.validator['zh-cn'] = {
	validate_fail:'您提交的表单中有无效内容，请检查高亮部分内容。',
	required:'此项为必填',
	string:'',
	is_not_int:'此选项必须为整型数字',
	is_not_float:'此选项必须为浮点型数字',
	is_not_date:'日期格式不正确',
	is_not_email:'email格式不正确',
	is_not_mobile:'手机号码格式不正确',
	is_not_id_card:'cart not valid...',
	is_not_post_code:'邮政编码格式不正确',
	is_not_url:'不是有效的地址格式',
	is_not_tel_num:'电话号码格式不正确'
};
	
In18Lan.validator['en-us'] = {
	validate_fail:'Invalid input. Please re-enter in the highlight.',
	required:'This is required. ',
	string:'',
	is_not_int:'This option must be an integer number',
	is_not_float:'This option must be a floating point number',
	is_not_date:'Date format incorrect',
	is_not_email:'Email format incorrect',
	is_not_mobile:'Phone number format is not correct',
	is_not_id_card:'Cart not valid ...',
	is_not_post_code:'ZIP format is incorrect',
	is_not_url:'is not a valid address format',
	is_not_tel_num:'phone number format incorrect'
};
/////////////////////////////////////end 后台验证/////////////////////////////////////////////////////////
