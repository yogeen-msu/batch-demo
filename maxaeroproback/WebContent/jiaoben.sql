USE [autelproweb]
GO
/****** Object:  Table [dbo].[DT_UserLoginInfo]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_UserLoginInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userCode] [varchar](32) NULL,
	[loginTime] [varchar](250) NULL,
	[userType] [int] NULL,
	[loginIP] [varchar](250) NULL,
	[code] [varchar](32) NULL,
 CONSTRAINT [PK_DT_USERLOGININFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_UserLoginInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_UserLoginInfo] ON
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1, N'cui201301231859400133', N'2013-01-23 20:38:05', 1, N'127.0.0.1', N'uli201301232038050655')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (3, N'cui201301231859400133', N'2013-01-23 20:41:46', 1, N'127.0.0.1', N'uli201301232041460355')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (4, N'cui201301231859400133', N'2013-01-23 20:42:51', 1, N'127.0.0.1', N'uli201301232042510448')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (5, N'cui201301231859400133', N'2013-01-23 20:50:34', 1, N'127.0.0.1', N'uli201301232050340990')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (6, N'cui201301231859400133', N'2013-01-23 20:55:49', 1, N'127.0.0.1', N'uli201301232055490327')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (7, N'cui201301231859400133', N'2013-01-23 20:56:21', 1, N'127.0.0.1', N'uli201301232056210218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (11, N'sei201301161019460562', N'2013-01-23 21:06:30', 2, N'127.0.0.1', N'uli201301232106300556')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (12, N'cui201301231859400133', N'2013-01-24 09:13:46', 1, N'127.0.0.1', N'uli201301240913460296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (13, N'cui201301231859400133', N'2013-01-24 09:15:14', 1, N'127.0.0.1', N'uli201301240915150031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (14, N'sei201301161019460562', N'2013-01-24 09:17:59', 2, N'127.0.0.1', N'uli201301240917590296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (15, N'sei201301161019460562', N'2013-01-24 10:07:08', 2, N'127.0.0.1', N'uli201301241007090046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (16, N'sei201301161019460562', N'2013-01-24 10:13:40', 2, N'127.0.0.1', N'uli201301241013400187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (17, N'cui201301231859400133', N'2013-01-24 11:15:30', 1, N'127.0.0.1', N'uli201301241115310031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (18, N'cui201301231859400133', N'2013-01-24 11:26:15', 1, N'127.0.0.1', N'uli201301241126160015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (19, N'cui201301231859400133', N'2013-01-24 11:30:34', 1, N'127.0.0.1', N'uli201301241130340203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (20, N'cui201301231859400133', N'2013-01-24 11:45:24', 1, N'127.0.0.1', N'uli201301241145240343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (21, N'cui201301231859400133', N'2013-01-24 11:46:47', 1, N'127.0.0.1', N'uli201301241146470656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (22, N'cui201301231859400133', N'2013-01-24 11:59:04', 1, N'127.0.0.1', N'uli201301241159040984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (23, N'cui201301231859400133', N'2013-01-24 14:11:09', 1, N'127.0.0.1', N'uli201301241411090187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (24, N'cui201301231859400133', N'2013-01-24 14:12:37', 1, N'127.0.0.1', N'uli201301241412380000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (25, N'cui201301231859400133', N'2013-01-24 18:49:56', 1, N'127.0.0.1', N'uli201301241849560968')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (26, N'cui201301231859400133', N'2013-01-24 19:02:19', 1, N'127.0.0.1', N'uli201301241902190359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (27, N'cui201301231859400133', N'2013-01-24 19:02:56', 1, N'127.0.0.1', N'uli201301241902560171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (28, N'cui201301231859400133', N'2013-01-24 19:05:25', 1, N'127.0.0.1', N'uli201301241905250593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (29, N'cui201301231859400133', N'2013-01-24 19:07:16', 1, N'127.0.0.1', N'uli201301241907160937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (30, N'sei201301161019460562', N'2013-01-24 19:14:53', 2, N'127.0.0.1', N'uli201301241914530468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (31, N'sei201301161019460562', N'2013-01-24 19:15:30', 2, N'127.0.0.1', N'uli201301241915300187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (32, N'cui201301231859400133', N'2013-01-24 19:16:45', 1, N'127.0.0.1', N'uli201301241916450078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (33, N'cui201301231859400133', N'2013-01-24 19:17:38', 1, N'127.0.0.1', N'uli201301241917380828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (34, N'cui201301231859400133', N'2013-01-24 19:19:32', 1, N'127.0.0.1', N'uli201301241919320359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (35, N'sei201301161019460562', N'2013-01-24 19:20:20', 2, N'127.0.0.1', N'uli201301241920200031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (36, N'sei201301161019460562', N'2013-01-24 19:20:38', 2, N'127.0.0.1', N'uli201301241920380500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (37, N'sei201301161019460562', N'2013-01-24 19:28:42', 2, N'127.0.0.1', N'uli201301241928420515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (38, N'sei201301161019460562', N'2013-01-24 19:30:45', 2, N'127.0.0.1', N'uli201301241930460046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (39, N'sei201301161019460562', N'2013-01-24 19:31:06', 2, N'127.0.0.1', N'uli201301241931060562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (40, N'sei201301161019460562', N'2013-01-24 19:32:23', 2, N'127.0.0.1', N'uli201301241932230531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (41, N'sei201301161019460562', N'2013-01-24 19:34:46', 2, N'127.0.0.1', N'uli201301241934460875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (42, N'sei201301161019460562', N'2013-01-24 19:39:24', 2, N'127.0.0.1', N'uli201301241939240609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (43, N'cui201301231859400133', N'2013-01-24 19:40:30', 1, N'127.0.0.1', N'uli201301241940310000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (44, N'sei201301161019460562', N'2013-01-24 23:33:53', 2, N'127.0.0.1', N'uli201301242333540109')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (45, N'sei201301161019460562', N'2013-01-24 23:40:46', 2, N'127.0.0.1', N'uli201301242340460515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (46, N'sei201301161019460562', N'2013-01-24 23:48:43', 2, N'127.0.0.1', N'uli201301242348430562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (47, N'sei201301161019460562', N'2013-01-24 23:54:11', 2, N'127.0.0.1', N'uli201301242354110593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (48, N'sei201301161019460562', N'2013-01-25 00:05:19', 2, N'127.0.0.1', N'uli201301250005190609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (49, N'sei201301161019460562', N'2013-01-25 00:08:59', 2, N'127.0.0.1', N'uli201301250008590984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (50, N'sei201301161019460562', N'2013-01-25 00:15:29', 2, N'127.0.0.1', N'uli201301250015300031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (51, N'cui201301231020360421', N'2013-01-25 18:55:17', 1, N'127.0.0.1', N'uli201301251855170421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (52, N'cui201301231020360421', N'2013-01-25 19:01:47', 1, N'127.0.0.1', N'uli201301251901470375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (53, N'cui201301231020360421', N'2013-01-25 19:12:07', 1, N'127.0.0.1', N'uli201301251912070359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (54, N'cui201301231020360421', N'2013-01-25 19:16:03', 1, N'127.0.0.1', N'uli201301251916030593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (55, N'cui201301231020360421', N'2013-01-25 20:50:38', 1, N'127.0.0.1', N'uli201301252050390015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (56, N'cui201301231020360421', N'2013-01-25 20:52:12', 1, N'127.0.0.1', N'uli201301252052120718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (57, N'cui201301231020360421', N'2013-01-25 23:46:06', 1, N'0:0:0:0:0:0:0:1', N'uli201301252346060845')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (58, N'cui201301231020360421', N'2013-01-25 23:51:57', 1, N'0:0:0:0:0:0:0:1', N'uli201301252351580041')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (59, N'cui201301231020360421', N'2013-01-25 23:58:56', 1, N'0:0:0:0:0:0:0:1', N'uli201301252358560959')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (60, N'cui201301231020360421', N'2013-01-26 00:05:16', 1, N'0:0:0:0:0:0:0:1', N'uli201301260005160313')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (61, N'cui201301231020360421', N'2013-01-26 00:12:49', 1, N'0:0:0:0:0:0:0:1', N'uli201301260012490465')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (62, N'cui201301231020360421', N'2013-01-26 00:22:30', 1, N'0:0:0:0:0:0:0:1', N'uli201301260022300581')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (63, N'cui201301231020360421', N'2013-01-26 10:34:00', 1, N'127.0.0.1', N'uli201301261034000421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (64, N'cui201301231020360421', N'2013-01-26 10:39:06', 1, N'127.0.0.1', N'uli201301261039060312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (65, N'cui201301231020360421', N'2013-01-26 10:42:48', 1, N'127.0.0.1', N'uli201301261042480578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (66, N'cui201301231020360421', N'2013-01-26 10:44:51', 1, N'127.0.0.1', N'uli201301261044510203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (67, N'cui201301231020360421', N'2013-01-26 15:34:22', 1, N'0:0:0:0:0:0:0:1', N'uli201301261534230210')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (68, N'sei201301161019460562', N'2013-01-27 13:00:32', 2, N'127.0.0.1', N'uli201301271300320174')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (69, N'sei201301161019460562', N'2013-01-27 13:24:08', 2, N'127.0.0.1', N'uli201301271324080283')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (70, N'sei201301161019460562', N'2013-01-27 13:26:58', 2, N'127.0.0.1', N'uli201301271326580299')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (71, N'sei201301161019460562', N'2013-01-27 13:35:01', 2, N'127.0.0.1', N'uli201301271335020221')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (72, N'sei201301161019460562', N'2013-01-27 14:28:55', 2, N'127.0.0.1', N'uli201301271428550174')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (73, N'cui201301271547140111', N'2013-01-27 15:48:59', 1, N'127.0.0.1', N'uli201301271548590471')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (74, N'cui201301271547140111', N'2013-01-27 20:10:21', 1, N'127.0.0.1', N'uli201301272010210625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (75, N'cui201301271547140111', N'2013-01-27 20:17:37', 1, N'127.0.0.1', N'uli201301272017370109')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (76, N'sei201301161019460562', N'2013-01-27 20:20:28', 2, N'127.0.0.1', N'uli201301272020280921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (77, N'cui201301271547140111', N'2013-01-27 21:25:01', 1, N'127.0.0.1', N'uli201301272125010750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (78, N'cui201301271547140111', N'2013-01-27 21:49:19', 1, N'127.0.0.1', N'uli201301272149190640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (79, N'cui201301271547140111', N'2013-01-27 21:54:51', 1, N'127.0.0.1', N'uli201301272154510890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (80, N'cui201301271547140111', N'2013-01-27 22:05:04', 1, N'127.0.0.1', N'uli201301272205040421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (81, N'sei201301161019460562', N'2013-01-27 22:07:26', 2, N'127.0.0.1', N'uli201301272207260171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (82, N'cui201301231020360421', N'2013-01-28 09:12:19', 1, N'127.0.0.1', N'uli201301280912190093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (83, N'cui201301231020360421', N'2013-01-28 09:14:47', 1, N'127.0.0.1', N'uli201301280914470390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (84, N'cui201301231020360421', N'2013-01-28 09:17:22', 1, N'127.0.0.1', N'uli201301280917220906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (85, N'cui201301231020360421', N'2013-01-28 09:25:52', 1, N'127.0.0.1', N'uli201301280925520109')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (86, N'cui201301231020360421', N'2013-01-28 09:26:43', 1, N'127.0.0.1', N'uli201301280926430078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (87, N'cui201301231020360421', N'2013-01-28 09:38:44', 1, N'127.0.0.1', N'uli201301280938440078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (88, N'cui201301231020360421', N'2013-01-28 16:31:40', 1, N'127.0.0.1', N'uli201301281631400562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (89, N'cui201301231020360421', N'2013-01-28 16:38:16', 1, N'127.0.0.1', N'uli201301281638160843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (90, N'cui201301231020360421', N'2013-01-28 16:42:18', 1, N'127.0.0.1', N'uli201301281642180890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (91, N'cui201301231020360421', N'2013-01-28 16:48:20', 1, N'127.0.0.1', N'uli201301281648200156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (92, N'cui201301231020360421', N'2013-01-28 17:02:49', 1, N'127.0.0.1', N'uli201301281702490984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (93, N'cui201301231020360421', N'2013-01-28 17:04:00', 1, N'127.0.0.1', N'uli201301281704010000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (94, N'cui201301231020360421', N'2013-01-28 17:12:24', 1, N'127.0.0.1', N'uli201301281712240656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (95, N'cui201301231020360421', N'2013-01-28 17:15:32', 1, N'127.0.0.1', N'uli201301281715320156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (96, N'cui201301271547140111', N'2013-01-28 18:28:14', 1, N'127.0.0.1', N'uli201301281828140421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (97, N'sei201301161019460562', N'2013-01-28 18:33:10', 2, N'127.0.0.1', N'uli201301281833100484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (98, N'cui201301231020360421', N'2013-01-28 19:18:56', 1, N'127.0.0.1', N'uli201301281918560109')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (99, N'cui201301231020360421', N'2013-01-28 19:21:29', 1, N'127.0.0.1', N'uli201301281921290843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (100, N'cui201301231020360421', N'2013-01-28 19:24:19', 1, N'127.0.0.1', N'uli201301281924190906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (101, N'cui201301231020360421', N'2013-01-28 19:29:37', 1, N'127.0.0.1', N'uli201301281929370218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (102, N'cui201301231020360421', N'2013-01-28 19:33:39', 1, N'127.0.0.1', N'uli201301281933390421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (103, N'cui201301231020360421', N'2013-01-28 19:37:49', 1, N'127.0.0.1', N'uli201301281937490812')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (104, N'cui201301231020360421', N'2013-01-28 19:44:24', 1, N'127.0.0.1', N'uli201301281944240656')
GO
print 'Processed 100 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (105, N'cui201301231020360421', N'2013-01-28 19:46:09', 1, N'127.0.0.1', N'uli201301281946090765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (106, N'cui201301271547140111', N'2013-01-28 19:45:32', 1, N'127.0.0.1', N'uli201301281945320546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (107, N'cui201301231020360421', N'2013-01-28 19:51:05', 1, N'127.0.0.1', N'uli201301281951050671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (108, N'cui201301231020360421', N'2013-01-28 19:54:38', 1, N'127.0.0.1', N'uli201301281954380328')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (109, N'cui201301231020360421', N'2013-01-28 19:58:59', 1, N'127.0.0.1', N'uli201301281958590375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (110, N'cui201301271547140111', N'2013-01-28 19:59:03', 1, N'127.0.0.1', N'uli201301281959030656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (111, N'cui201301231020360421', N'2013-01-28 20:03:27', 1, N'127.0.0.1', N'uli201301282003270625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (112, N'sei201301161019460562', N'2013-01-28 20:03:32', 2, N'127.0.0.1', N'uli201301282003320640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (113, N'cui201301231020360421', N'2013-01-28 20:17:12', 1, N'127.0.0.1', N'uli201301282017120203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (114, N'cui201301271547140111', N'2013-01-28 23:32:11', 1, N'127.0.0.1', N'uli201301282332130000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (115, N'cui201301271547140111', N'2013-01-28 23:35:02', 1, N'127.0.0.1', N'uli201301282335030234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (116, N'sei201301161019460562', N'2013-01-28 23:43:11', 2, N'127.0.0.1', N'uli201301282343110343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (117, N'sei201301161019460562', N'2013-01-28 23:51:00', 2, N'127.0.0.1', N'uli201301282351000859')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (118, N'sei201301161019460562', N'2013-01-28 23:58:43', 2, N'127.0.0.1', N'uli201301282358430093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (119, N'cui201301231020360421', N'2013-01-29 08:45:02', 1, N'127.0.0.1', N'uli201301290845020375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (120, N'cui201301231020360421', N'2013-01-29 09:04:33', 1, N'127.0.0.1', N'uli201301290904330781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (121, N'cui201301231020360421', N'2013-01-29 10:03:58', 1, N'127.0.0.1', N'uli201301291003580421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (122, N'cui201301231020360421', N'2013-01-29 10:08:23', 1, N'127.0.0.1', N'uli201301291008230546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (123, N'cui201301231020360421', N'2013-01-29 10:29:10', 1, N'127.0.0.1', N'uli201301291029100406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (124, N'cui201301231020360421', N'2013-01-29 10:39:16', 1, N'127.0.0.1', N'uli201301291039160937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (125, N'cui201301231020360421', N'2013-01-29 10:43:33', 1, N'127.0.0.1', N'uli201301291043330531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (126, N'cui201301231020360421', N'2013-01-29 10:54:28', 1, N'127.0.0.1', N'uli201301291054280500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (127, N'cui201301231020360421', N'2013-01-29 11:07:49', 1, N'127.0.0.1', N'uli201301291107490875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (128, N'cui201301231020360421', N'2013-01-29 15:29:44', 1, N'127.0.0.1', N'uli201301291529440734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (129, N'cui201301231020360421', N'2013-01-29 15:50:46', 1, N'127.0.0.1', N'uli201301291550460937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (130, N'cui201301231020360421', N'2013-01-29 15:53:07', 1, N'127.0.0.1', N'uli201301291553070437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (131, N'sei201301161019460562', N'2013-01-29 15:53:59', 2, N'127.0.0.1', N'uli201301291553590390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (132, N'cui201301231020360421', N'2013-01-29 15:56:38', 1, N'127.0.0.1', N'uli201301291556380828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (133, N'cui201301231020360421', N'2013-01-29 16:00:06', 1, N'127.0.0.1', N'uli201301291600060281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (134, N'cui201301231020360421', N'2013-01-29 16:06:37', 1, N'127.0.0.1', N'uli201301291606370875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (135, N'cui201301231020360421', N'2013-01-29 16:29:02', 1, N'127.0.0.1', N'uli201301291629020500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (136, N'cui201301231020360421', N'2013-01-29 16:33:11', 1, N'127.0.0.1', N'uli201301291633110140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (137, N'cui201301231020360421', N'2013-01-29 16:44:19', 1, N'127.0.0.1', N'uli201301291644190125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (138, N'cui201301231020360421', N'2013-01-29 16:52:22', 1, N'127.0.0.1', N'uli201301291652220187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (139, N'cui201301271547140111', N'2013-01-29 18:54:38', 1, N'127.0.0.1', N'uli201301291854390000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (140, N'cui201301271547140111', N'2013-01-29 18:59:21', 1, N'127.0.0.1', N'uli201301291859210872')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (141, N'cui201301271547140111', N'2013-01-29 19:05:47', 1, N'127.0.0.1', N'uli201301291905470648')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (142, N'cui201301291919100466', N'2013-01-29 19:20:10', 1, N'127.0.0.1', N'uli201301291920100512')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (143, N'cui201301291919100466', N'2013-01-29 19:21:21', 1, N'127.0.0.1', N'uli201301291921210839')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (144, N'cui201301231020360421', N'2013-01-29 19:25:35', 1, N'127.0.0.1', N'uli201301291925350820')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (145, N'cui201301271547140111', N'2013-01-29 20:23:35', 1, N'127.0.0.1', N'uli201301292023350131')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (146, N'cui201301271547140111', N'2013-01-29 20:27:20', 1, N'127.0.0.1', N'uli201301292027200175')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (147, N'cui201301271547140111', N'2013-01-29 20:29:38', 1, N'127.0.0.1', N'uli201301292029380892')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (148, N'cui201301271547140111', N'2013-01-29 20:32:48', 1, N'127.0.0.1', N'uli201301292032490108')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (149, N'cui201301271547140111', N'2013-01-29 20:35:33', 1, N'127.0.0.1', N'uli201301292035330903')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (150, N'cui201301271547140111', N'2013-01-29 20:55:16', 1, N'127.0.0.1', N'uli201301292055160414')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (151, N'cui201301271547140111', N'2013-01-30 08:46:01', 1, N'127.0.0.1', N'uli201301300846010671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (152, N'sei201301161019460562', N'2013-01-30 08:46:25', 2, N'127.0.0.1', N'uli201301300846250671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (153, N'sei201301161019460562', N'2013-01-30 08:54:16', 2, N'127.0.0.1', N'uli201301300854160656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (154, N'sei201301161019460562', N'2013-01-30 08:56:28', 2, N'127.0.0.1', N'uli201301300856280781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (155, N'cui201301231020360421', N'2013-01-30 09:14:03', 1, N'127.0.0.1', N'uli201301300914030406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (156, N'sei201301161019460562', N'2013-01-30 09:20:47', 2, N'127.0.0.1', N'uli201301300920470437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (157, N'sei201301161019460562', N'2013-01-30 09:41:15', 2, N'127.0.0.1', N'uli201301300941150703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (158, N'sei201301161019460562', N'2013-01-30 10:17:52', 2, N'127.0.0.1', N'uli201301301017520921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (159, N'cui201301231020360421', N'2013-01-30 10:26:50', 1, N'127.0.0.1', N'uli201301301026500906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (160, N'cui201301231020360421', N'2013-01-30 10:28:40', 1, N'127.0.0.1', N'uli201301301028400921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (161, N'sei201301161019460562', N'2013-01-30 10:29:57', 2, N'127.0.0.1', N'uli201301301029570375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (162, N'sei201301161019460562', N'2013-01-30 10:44:58', 2, N'0:0:0:0:0:0:0:1', N'uli201301301044580164')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (163, N'sei201301161019460562', N'2013-01-30 10:53:38', 2, N'0:0:0:0:0:0:0:1', N'uli201301301053380177')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (164, N'cui201301231020360421', N'2013-01-30 10:54:44', 1, N'127.0.0.1', N'uli201301301054440781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (165, N'cui201301231020360421', N'2013-01-30 11:01:25', 1, N'127.0.0.1', N'uli201301301101250437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (166, N'sei201301161019460562', N'2013-01-30 11:04:08', 2, N'0:0:0:0:0:0:0:1', N'uli201301301104080392')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (167, N'cui201301231020360421', N'2013-01-30 11:09:17', 1, N'127.0.0.1', N'uli201301301109170734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (168, N'sei201301161019460562', N'2013-01-30 11:26:42', 2, N'0:0:0:0:0:0:0:1', N'uli201301301126420665')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (169, N'cui201301231020360421', N'2013-01-30 14:57:48', 1, N'127.0.0.1', N'uli201301301457480171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (170, N'cui201301231020360421', N'2013-01-30 15:03:27', 1, N'127.0.0.1', N'uli201301301503270812')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (171, N'cui201301231020360421', N'2013-01-30 15:14:25', 1, N'127.0.0.1', N'uli201301301514250765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (172, N'cui201301231020360421', N'2013-01-30 15:32:39', 1, N'127.0.0.1', N'uli201301301532390656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (173, N'sei201301161019460562', N'2013-01-30 18:13:02', 2, N'127.0.0.1', N'uli201301301813030062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (174, N'sei201301161019460562', N'2013-01-30 18:17:15', 2, N'127.0.0.1', N'uli201301301817150531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (175, N'sei201301161019460562', N'2013-01-30 18:46:12', 2, N'127.0.0.1', N'uli201301301846120125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (176, N'sei201301161019460562', N'2013-01-30 18:54:57', 2, N'127.0.0.1', N'uli201301301854570265')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (177, N'sei201301161019460562', N'2013-01-30 19:01:42', 2, N'127.0.0.1', N'uli201301301901430000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (178, N'cui201301271547140111', N'2013-01-30 19:22:46', 1, N'127.0.0.1', N'uli201301301922460703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (179, N'cui201301271547140111', N'2013-01-30 19:38:29', 1, N'127.0.0.1', N'uli201301301938290218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (180, N'sei201301161019460562', N'2013-01-30 22:04:45', 2, N'0:0:0:0:0:0:0:1', N'uli201301302204450835')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (181, N'sei201301161019460562', N'2013-01-30 22:10:59', 2, N'0:0:0:0:0:0:0:1', N'uli201301302211000052')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (182, N'cui201301271547140111', N'2013-01-30 22:36:50', 1, N'127.0.0.1', N'uli201301302236500312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (183, N'sei201301161019460562', N'2013-01-30 23:16:23', 2, N'0:0:0:0:0:0:0:1', N'uli201301302316230820')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (184, N'sei201301161019460562', N'2013-01-30 23:17:43', 2, N'0:0:0:0:0:0:0:1', N'uli201301302317430097')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (185, N'sei201301161019460562', N'2013-01-31 00:07:40', 2, N'0:0:0:0:0:0:0:1', N'uli201301310007400922')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (186, N'sei201301161019460562', N'2013-01-31 00:08:00', 2, N'0:0:0:0:0:0:0:1', N'uli201301310008000085')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (187, N'cui201301271547140111', N'2013-01-31 08:53:31', 1, N'127.0.0.1', N'uli201301310853310484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (188, N'cui201301271547140111', N'2013-01-31 09:11:26', 1, N'127.0.0.1', N'uli201301310911260437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (189, N'cui201301271547140111', N'2013-01-31 09:16:54', 1, N'127.0.0.1', N'uli201301310916550578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (190, N'sei201301161019460562', N'2013-01-31 09:45:39', 2, N'127.0.0.1', N'uli201301310945390609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (191, N'sei201301161019460562', N'2013-01-31 10:08:06', 2, N'127.0.0.1', N'uli201301311008070015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (192, N'sei201301161019460562', N'2013-01-31 10:20:43', 2, N'127.0.0.1', N'uli201301311020430125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (193, N'sei201301161019460562', N'2013-01-31 10:29:19', 2, N'127.0.0.1', N'uli201301311029190578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (194, N'sei201301161019460562', N'2013-01-31 10:31:40', 2, N'127.0.0.1', N'uli201301311031400468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (195, N'sei201301161019460562', N'2013-01-31 10:43:18', 2, N'127.0.0.1', N'uli201301311043180171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (196, N'sei201301161019460562', N'2013-01-31 10:56:08', 2, N'127.0.0.1', N'uli201301311056080187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (197, N'sei201301161019460562', N'2013-01-31 11:08:59', 2, N'127.0.0.1', N'uli201301311108590828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (198, N'sei201301161019460562', N'2013-01-31 11:18:34', 2, N'127.0.0.1', N'uli201301311118340828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (199, N'sei201301161019460562', N'2013-01-31 11:35:25', 2, N'127.0.0.1', N'uli201301311135250640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (200, N'sei201301161019460562', N'2013-01-31 11:41:34', 2, N'127.0.0.1', N'uli201301311141340078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (201, N'cui201301231020360421', N'2013-01-31 14:27:38', 1, N'127.0.0.1', N'uli201301311427380978')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (202, N'sei201301161019460562', N'2013-01-31 14:41:42', 2, N'127.0.0.1', N'uli201301311441420562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (203, N'cui201301231020360421', N'2013-01-31 14:58:16', 1, N'127.0.0.1', N'uli201301311458160603')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (204, N'sei201301161019460562', N'2013-01-31 15:18:33', 2, N'127.0.0.1', N'uli201301311518330781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (205, N'sei201301161019460562', N'2013-01-31 15:27:03', 2, N'127.0.0.1', N'uli201301311527030734')
GO
print 'Processed 200 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (206, N'sei201301161019460562', N'2013-01-31 15:46:41', 2, N'127.0.0.1', N'uli201301311546410703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (207, N'cui201301231020360421', N'2013-01-31 15:51:57', 1, N'127.0.0.1', N'uli201301311551570978')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (208, N'cui201301231020360421', N'2013-01-31 16:09:09', 1, N'127.0.0.1', N'uli201301311609090978')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (209, N'cui201301271547140111', N'2013-01-31 16:20:03', 1, N'127.0.0.1', N'uli201301311620030562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (210, N'cui201301271547140111', N'2013-01-31 16:32:39', 1, N'127.0.0.1', N'uli201301311632390937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (211, N'cui201301271547140111', N'2013-01-31 16:46:37', 1, N'127.0.0.1', N'uli201301311646380031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (212, N'cui201301271547140111', N'2013-01-31 16:52:54', 1, N'127.0.0.1', N'uli201301311652540859')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (213, N'cui201301271547140111', N'2013-01-31 16:54:43', 1, N'127.0.0.1', N'uli201301311654440031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (214, N'cui201301271547140111', N'2013-01-31 17:03:47', 1, N'127.0.0.1', N'uli201301311703470343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (215, N'cui201301231020360421', N'2013-01-31 17:46:20', 1, N'127.0.0.1', N'uli201301311746200853')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (216, N'cui201301231020360421', N'2013-01-31 17:50:03', 1, N'127.0.0.1', N'uli201301311750030806')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (217, N'cui201301231020360421', N'2013-01-31 17:52:53', 1, N'127.0.0.1', N'uli201301311752530212')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (218, N'cui201301231020360421', N'2013-01-31 17:54:44', 1, N'127.0.0.1', N'uli201301311754440337')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (219, N'sei201301161019460562', N'2013-01-31 20:49:51', 2, N'127.0.0.1', N'uli201301312049510156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (220, N'sei201301161019460562', N'2013-01-31 21:29:25', 2, N'127.0.0.1', N'uli201301312129250640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (221, N'sei201301161019460562', N'2013-01-31 21:35:13', 2, N'127.0.0.1', N'uli201301312135130953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (222, N'cui201301271547140111', N'2013-02-02 19:57:26', 1, N'127.0.0.1', N'uli201302021957260500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (223, N'sei201301161019460562', N'2013-02-02 19:59:30', 2, N'127.0.0.1', N'uli201302021959300453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (224, N'cui201301271547140111', N'2013-02-03 10:28:58', 1, N'127.0.0.1', N'uli201302031028580812')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (225, N'cui201301271547140111', N'2013-02-03 10:29:37', 1, N'127.0.0.1', N'uli201302031029370203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (226, N'cui201301271547140111', N'2013-02-03 10:30:30', 1, N'127.0.0.1', N'uli201302031030300140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (227, N'cui201301271547140111', N'2013-02-03 10:31:13', 1, N'127.0.0.1', N'uli201302031031130234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (228, N'cui201301271547140111', N'2013-02-03 10:31:57', 1, N'127.0.0.1', N'uli201302031031570296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (229, N'cui201301271547140111', N'2013-02-03 10:32:45', 1, N'127.0.0.1', N'uli201302031032450906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (230, N'cui201301271547140111', N'2013-02-03 10:33:20', 1, N'127.0.0.1', N'uli201302031033210000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (231, N'cui201301271547140111', N'2013-02-03 10:33:56', 1, N'127.0.0.1', N'uli201302031033560203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (232, N'cui201301271547140111', N'2013-02-03 10:34:31', 1, N'127.0.0.1', N'uli201302031034310984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (233, N'cui201301271547140111', N'2013-02-03 10:35:09', 1, N'127.0.0.1', N'uli201302031035090203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (234, N'cui201301271547140111', N'2013-02-03 10:37:05', 1, N'127.0.0.1', N'uli201302031037050062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (235, N'cui201301271547140111', N'2013-02-03 10:45:40', 1, N'127.0.0.1', N'uli201302031045400671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (236, N'cui201301271547140111', N'2013-02-03 10:47:57', 1, N'127.0.0.1', N'uli201302031047570734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (237, N'cui201301271547140111', N'2013-02-03 10:49:01', 1, N'127.0.0.1', N'uli201302031049010937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (238, N'cui201301271547140111', N'2013-02-03 10:51:11', 1, N'127.0.0.1', N'uli201302031051110515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (239, N'cui201301271547140111', N'2013-02-03 10:51:44', 1, N'127.0.0.1', N'uli201302031051440250')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (240, N'cui201301271547140111', N'2013-02-03 10:52:31', 1, N'127.0.0.1', N'uli201302031052310125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (241, N'cui201301271547140111', N'2013-02-03 10:53:27', 1, N'127.0.0.1', N'uli201302031053270125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (242, N'cui201301271547140111', N'2013-02-03 10:54:02', 1, N'127.0.0.1', N'uli201302031054020375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (243, N'cui201301271547140111', N'2013-02-03 10:54:36', 1, N'127.0.0.1', N'uli201302031054360718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (244, N'cui201301271547140111', N'2013-02-03 10:56:31', 1, N'127.0.0.1', N'uli201302031056310968')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (245, N'cui201301271547140111', N'2013-02-03 10:57:20', 1, N'127.0.0.1', N'uli201302031057200890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (246, N'cui201301271547140111', N'2013-02-03 10:59:00', 1, N'127.0.0.1', N'uli201302031059000171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (247, N'cui201301271547140111', N'2013-02-03 10:59:42', 1, N'127.0.0.1', N'uli201302031059420765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (248, N'cui201301271547140111', N'2013-02-03 11:01:41', 1, N'127.0.0.1', N'uli201302031101420000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (249, N'cui201301271547140111', N'2013-02-03 11:03:33', 1, N'127.0.0.1', N'uli201302031103330343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (250, N'cui201301271547140111', N'2013-02-03 11:35:40', 1, N'127.0.0.1', N'uli201302031135400875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (251, N'cui201301271547140111', N'2013-02-03 11:36:12', 1, N'127.0.0.1', N'uli201302031136120921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (252, N'cui201301271547140111', N'2013-02-03 11:36:48', 1, N'127.0.0.1', N'uli201302031136480671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (253, N'cui201301271547140111', N'2013-02-03 11:37:20', 1, N'127.0.0.1', N'uli201302031137200062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (254, N'cui201301271547140111', N'2013-02-03 11:38:23', 1, N'127.0.0.1', N'uli201302031138230843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (255, N'cui201301271547140111', N'2013-02-03 11:46:00', 1, N'127.0.0.1', N'uli201302031146000875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (256, N'cui201301271547140111', N'2013-02-03 11:53:51', 1, N'127.0.0.1', N'uli201302031153510859')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (257, N'cui201301271547140111', N'2013-02-03 11:54:40', 1, N'127.0.0.1', N'uli201302031156550296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (258, N'cui201301271547140111', N'2013-02-03 12:02:39', 1, N'127.0.0.1', N'uli201302031202390765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (259, N'cui201301271547140111', N'2013-02-03 12:03:16', 1, N'127.0.0.1', N'uli201302031203160156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (260, N'sei201301161019460562', N'2013-02-03 12:04:34', 2, N'127.0.0.1', N'uli201302031204340125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (261, N'cui201301271547140111', N'2013-02-03 12:05:16', 1, N'127.0.0.1', N'uli201302031205160546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (262, N'cui201301271547140111', N'2013-02-03 12:19:45', 1, N'127.0.0.1', N'uli201302031219450781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (263, N'cui201301271547140111', N'2013-02-03 12:55:59', 1, N'127.0.0.1', N'uli201302031255590140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (264, N'cui201301271547140111', N'2013-02-03 13:00:12', 1, N'127.0.0.1', N'uli201302031300120750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (265, N'cui201301271547140111', N'2013-02-03 13:06:34', 1, N'127.0.0.1', N'uli201302031306340984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (266, N'sei201301161019460562', N'2013-02-06 09:41:18', 2, N'127.0.0.1', N'uli201302060941180437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (267, N'sei201301161019460562', N'2013-02-06 09:44:51', 2, N'127.0.0.1', N'uli201302060944510328')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (268, N'sei201301161019460562', N'2013-02-06 09:56:48', 2, N'127.0.0.1', N'uli201302060956480609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (269, N'sei201301161019460562', N'2013-02-06 10:03:17', 2, N'127.0.0.1', N'uli201302061003170375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (270, N'cui201302061017260203', N'2013-02-06 10:18:09', 1, N'127.0.0.1', N'uli201302061018090562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (271, N'cui201301231020360421', N'2013-03-01 08:31:50', 1, N'127.0.0.1', N'uli201303010831500140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (272, N'cui201301231020360421', N'2013-03-01 08:33:46', 1, N'127.0.0.1', N'uli201303010833460078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (273, N'cti20121228134915123', N'2013-03-01 08:48:55', 1, N'127.0.0.1', N'uli201303010848550968')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (274, N'cui201301231020360421', N'2013-03-01 09:22:27', 1, N'127.0.0.1', N'uli201303010922270812')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (275, N'cui201303031518130324', N'2013-03-03 15:18:59', 1, N'127.0.0.1', N'uli201303031518590721')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (276, N'cui201303031518130324', N'2013-03-03 15:23:33', 1, N'127.0.0.1', N'uli201303031523330069')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (277, N'cui201303031518130324', N'2013-03-03 15:28:26', 1, N'127.0.0.1', N'uli201303031528260523')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (278, N'cti20121228134915123', N'2013-03-03 16:00:23', 1, N'127.0.0.1', N'uli201303031600240053')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (279, N'cti20121228134915123', N'2013-03-03 16:15:02', 1, N'127.0.0.1', N'uli201303031615020129')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (280, N'sei201301161019460562', N'2013-03-03 16:19:06', 2, N'127.0.0.1', N'uli201303031619060535')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (281, N'cti20121228134915123', N'2013-03-04 10:08:28', 1, N'127.0.0.1', N'uli201303041008280281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (282, N'cti20121228134915123', N'2013-03-04 10:24:17', 1, N'127.0.0.1', N'uli201303041024170750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (283, N'cti20121228134915123', N'2013-03-04 10:30:22', 1, N'127.0.0.1', N'uli201303041030220718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (284, N'cti20121228134915123', N'2013-03-04 10:35:13', 1, N'127.0.0.1', N'uli201303041035130437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (285, N'cti20121228134915123', N'2013-03-04 10:54:41', 1, N'127.0.0.1', N'uli201303041054420015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (286, N'cti20121228134915123', N'2013-03-04 11:05:48', 1, N'127.0.0.1', N'uli201303041105480390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (287, N'cti20121228134915123', N'2013-03-04 11:37:51', 1, N'127.0.0.1', N'uli201303041137510184')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (288, N'cti20121228134915123', N'2013-03-04 11:40:59', 1, N'113.106.60.17', N'uli201303041140590602')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (289, N'cti20121228134915123', N'2013-03-04 14:00:18', 1, N'127.0.0.1', N'uli201303041400180562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (290, N'cti20121228134915123', N'2013-03-04 14:06:15', 1, N'127.0.0.1', N'uli201303041406150515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (291, N'sei201301161019460562', N'2013-03-04 14:28:04', 2, N'127.0.0.1', N'uli201303041428050046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (292, N'cui201303032028060718', N'2013-03-04 14:35:45', 1, N'127.0.0.1', N'uli201303041435450296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (293, N'cui201303032028060718', N'2013-03-04 14:45:19', 1, N'127.0.0.1', N'uli201303041445190671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (294, N'cui201303032028060718', N'2013-03-04 14:46:16', 1, N'127.0.0.1', N'uli201303041446160765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (295, N'cui201303032028060718', N'2013-03-04 14:47:44', 1, N'127.0.0.1', N'uli201303041447440953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (296, N'cui201303032028060718', N'2013-03-04 14:49:00', 1, N'127.0.0.1', N'uli201303041449000328')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (297, N'cui201303032028060718', N'2013-03-04 14:54:07', 1, N'127.0.0.1', N'uli201303041454070515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (298, N'cui201303032028060718', N'2013-03-04 14:55:38', 1, N'127.0.0.1', N'uli201303041455380656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (299, N'cui201303032028060718', N'2013-03-04 14:56:30', 1, N'127.0.0.1', N'uli201303041456300890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (300, N'cui201303032028060718', N'2013-03-04 14:57:06', 1, N'127.0.0.1', N'uli201303041457060078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (301, N'cui201303032028060718', N'2013-03-04 14:59:52', 1, N'127.0.0.1', N'uli201303041459520875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (302, N'cui201303032028060718', N'2013-03-04 15:01:30', 1, N'127.0.0.1', N'uli201303041501300875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (303, N'cui201303032028060718', N'2013-03-04 15:02:18', 1, N'127.0.0.1', N'uli201303041502180953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (304, N'cui201303032028060718', N'2013-03-04 15:03:01', 1, N'127.0.0.1', N'uli201303041503010109')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (305, N'cti20121228134915123', N'2013-03-04 15:02:25', 1, N'113.106.60.17', N'uli201303041502250730')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (306, N'cui201303032028060718', N'2013-03-04 15:05:55', 1, N'127.0.0.1', N'uli201303041505550406')
GO
print 'Processed 300 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (307, N'cui201303032028060718', N'2013-03-04 15:07:28', 1, N'127.0.0.1', N'uli201303041507280593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (308, N'cti20121228134915123', N'2013-03-04 15:12:58', 1, N'127.0.0.1', N'uli201303041512580984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (309, N'cti20121228134915123', N'2013-03-04 15:17:18', 1, N'127.0.0.1', N'uli201303041517180906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (310, N'cti20121228134915123', N'2013-03-04 15:18:15', 1, N'127.0.0.1', N'uli201303041518150531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (311, N'cti20121228134915123', N'2013-03-04 15:30:04', 1, N'127.0.0.1', N'uli201303041530040484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (312, N'cti20121228134915123', N'2013-03-04 15:47:41', 1, N'127.0.0.1', N'uli201303041547410734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (313, N'cti20121228134915123', N'2013-03-04 15:49:21', 1, N'127.0.0.1', N'uli201303041549210906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (314, N'cti20121228134915123', N'2013-03-04 16:23:15', 1, N'127.0.0.1', N'uli201303041623150796')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (315, N'cti20121228134915123', N'2013-03-04 16:28:36', 1, N'127.0.0.1', N'uli201303041628360203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (316, N'cti20121228134915123', N'2013-03-04 16:52:13', 1, N'127.0.0.1', N'uli201303041652140000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (317, N'cti20121228134915123', N'2013-03-04 17:02:24', 1, N'127.0.0.1', N'uli201303041702240046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (318, N'cti20121228134915123', N'2013-03-04 17:07:44', 1, N'127.0.0.1', N'uli201303041707440312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (319, N'cui201303032028060718', N'2013-03-04 17:14:31', 1, N'127.0.0.1', N'uli201303041714310234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (320, N'sei201301161019460562', N'2013-03-04 17:15:08', 2, N'127.0.0.1', N'uli201303041715080718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (321, N'sei201301161019460562', N'2013-03-04 17:35:17', 2, N'127.0.0.1', N'uli201303041735170875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (322, N'cui201303032028060718', N'2013-03-04 17:37:22', 1, N'127.0.0.1', N'uli201303041737220625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (323, N'cui201303032028060718', N'2013-03-04 17:51:17', 1, N'127.0.0.1', N'uli201303041751170765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (324, N'cui201303032028060718', N'2013-03-04 18:04:52', 1, N'127.0.0.1', N'uli201303041804520125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (325, N'sei201301161019460562', N'2013-03-04 18:08:40', 2, N'127.0.0.1', N'uli201303041808400218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (326, N'cui201303032028060718', N'2013-03-04 18:31:55', 1, N'127.0.0.1', N'uli201303041831550390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (327, N'cui201303032028060718', N'2013-03-04 18:38:45', 1, N'127.0.0.1', N'uli201303041838460015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (328, N'sei201301161019460562', N'2013-03-04 18:40:56', 2, N'127.0.0.1', N'uli201303041840560906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (329, N'cti20121228134915123', N'2013-03-05 09:09:58', 1, N'127.0.0.1', N'uli201303050909580031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (330, N'cti20121228134915123', N'2013-03-05 09:23:14', 1, N'127.0.0.1', N'uli201303050923140906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (331, N'cti20121228134915123', N'2013-03-05 09:31:10', 1, N'127.0.0.1', N'uli201303050931100203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (332, N'cti20121228134915123', N'2013-03-05 09:34:30', 1, N'127.0.0.1', N'uli201303050934300421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (333, N'cti20121228134915123', N'2013-03-05 09:44:56', 1, N'127.0.0.1', N'uli201303050944560328')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (334, N'cui201301231020360421', N'2013-03-05 09:48:44', 1, N'127.0.0.1', N'uli201303050948440687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (335, N'cui201301231020360421', N'2013-03-05 09:54:18', 1, N'127.0.0.1', N'uli201303050954180062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (336, N'cui201301231020360421', N'2013-03-05 10:00:35', 1, N'127.0.0.1', N'uli201303051000350609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (337, N'cui201301231020360421', N'2013-03-05 10:01:37', 1, N'127.0.0.1', N'uli201303051001370890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (338, N'cui201301231020360421', N'2013-03-05 10:03:26', 1, N'127.0.0.1', N'uli201303051003260937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (339, N'cui201301231020360421', N'2013-03-05 10:30:15', 1, N'127.0.0.1', N'uli201303051030150187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (340, N'cui201301231020360421', N'2013-03-05 10:31:20', 1, N'127.0.0.1', N'uli201303051031210015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (341, N'cui201301231020360421', N'2013-03-05 10:31:59', 1, N'127.0.0.1', N'uli201303051032000000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (342, N'cui201303032028060718', N'2013-03-05 14:48:30', 1, N'127.0.0.1', N'uli201303051448300687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (343, N'cui201303051525520046', N'2013-03-05 15:28:47', 1, N'127.0.0.1', N'uli201303051528470390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (344, N'cui201303051600540968', N'2013-03-05 17:57:09', 1, N'127.0.0.1', N'uli201303051757090156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (345, N'cui201303051600540968', N'2013-03-05 17:59:05', 1, N'127.0.0.1', N'uli201303051759050921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (346, N'cui201301231020360421', N'2013-03-06 23:53:56', 1, N'127.0.0.1', N'uli201303062353560218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (347, N'cui201301231020360421', N'2013-03-06 23:58:03', 1, N'127.0.0.1', N'uli201303062358040031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (348, N'cui201301231020360421', N'2013-03-07 00:00:35', 1, N'127.0.0.1', N'uli201303070000350671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (349, N'cui201303051600540968', N'2013-03-07 00:07:11', 1, N'127.0.0.1', N'uli201303070007110921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (350, N'cui201303051600540968', N'2013-03-07 00:13:54', 1, N'127.0.0.1', N'uli201303070013540671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (351, N'cui201303051600540968', N'2013-03-07 00:15:32', 1, N'127.0.0.1', N'uli201303070015320125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (352, N'cui201303051600540968', N'2013-03-07 00:19:34', 1, N'127.0.0.1', N'uli201303070019340609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (353, N'cui201303051600540968', N'2013-03-07 00:26:09', 1, N'127.0.0.1', N'uli201303070026100000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (354, N'cui201303051600540968', N'2013-03-07 00:31:20', 1, N'127.0.0.1', N'uli201303070031200890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (355, N'cui201303051600540968', N'2013-03-07 00:42:26', 1, N'127.0.0.1', N'uli201303070042270046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (356, N'cui201303051600540968', N'2013-03-07 00:44:08', 1, N'127.0.0.1', N'uli201303070044080921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (357, N'cui201303051600540968', N'2013-03-07 00:44:39', 1, N'127.0.0.1', N'uli201303070044390125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (358, N'sei201301161019460562', N'2013-03-07 00:45:26', 2, N'127.0.0.1', N'uli201303070045260796')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (359, N'cui201303051600540968', N'2013-03-07 00:47:05', 1, N'127.0.0.1', N'uli201303070047050281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (360, N'cui201303051600540968', N'2013-03-07 00:53:08', 1, N'127.0.0.1', N'uli201303070053080203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (361, N'cui201303051600540968', N'2013-03-07 00:57:22', 1, N'127.0.0.1', N'uli201303070057220593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (362, N'sei201301161019460562', N'2013-03-07 00:58:38', 2, N'127.0.0.1', N'uli201303070058380062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (363, N'cui201301231020360421', N'2013-03-07 13:49:23', 1, N'127.0.0.1', N'uli201303071349230953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (364, N'cui201301231020360421', N'2013-03-07 13:54:02', 1, N'127.0.0.1', N'uli201303071354020078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (365, N'cui201301231020360421', N'2013-03-07 13:57:23', 1, N'127.0.0.1', N'uli201303071357230843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (366, N'cui201301231020360421', N'2013-03-07 14:03:11', 1, N'127.0.0.1', N'uli201303071403110812')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (367, N'cui201301231020360421', N'2013-03-07 14:18:22', 1, N'127.0.0.1', N'uli201303071418220078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (368, N'cui201301231020360421', N'2013-03-07 14:40:23', 1, N'127.0.0.1', N'uli201303071440230921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (369, N'cti20121228134915123', N'2013-03-07 14:41:03', 1, N'127.0.0.1', N'uli201303071441030921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (370, N'cui201301231020360421', N'2013-03-07 14:55:02', 1, N'127.0.0.1', N'uli201303071455020250')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (371, N'cui201301231020360421', N'2013-03-07 14:59:03', 1, N'127.0.0.1', N'uli201303071459030421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (372, N'cui201301231020360421', N'2013-03-08 09:31:28', 1, N'127.0.0.1', N'uli201303080931280156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (373, N'cti20121228134915123', N'2013-03-08 09:32:11', 1, N'127.0.0.1', N'uli201303080932110843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (374, N'cti20121228134915123', N'2013-03-08 09:34:12', 1, N'127.0.0.1', N'uli201303080934120546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (375, N'cti20121228134915123', N'2013-03-08 09:38:34', 1, N'127.0.0.1', N'uli201303080938350031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (376, N'cti20121228134915123', N'2013-03-08 09:57:09', 1, N'127.0.0.1', N'uli201303080957090609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (377, N'cti20121228134915123', N'2013-03-08 09:59:47', 1, N'127.0.0.1', N'uli201303080959480015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (378, N'cti20121228134915123', N'2013-03-08 10:05:24', 1, N'127.0.0.1', N'uli201303081005240781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (379, N'cti20121228134915123', N'2013-03-08 10:09:38', 1, N'127.0.0.1', N'uli201303081009380343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (380, N'cui201303051600540968', N'2013-03-08 10:19:10', 1, N'127.0.0.1', N'uli201303081019100421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (381, N'cui201303051600540968', N'2013-03-08 10:30:32', 1, N'127.0.0.1', N'uli201303081030320453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (382, N'cui201303051600540968', N'2013-03-08 10:57:15', 1, N'127.0.0.1', N'uli201303081057150937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (383, N'cui201303051600540968', N'2013-03-08 11:03:26', 1, N'127.0.0.1', N'uli201303081103260546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (384, N'cui201303051600540968', N'2013-03-08 11:15:47', 1, N'127.0.0.1', N'uli201303081115480312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (385, N'cui201303051600540968', N'2013-03-08 11:21:38', 1, N'127.0.0.1', N'uli201303081121380187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (386, N'sei201301161019460562', N'2013-03-08 11:23:26', 2, N'127.0.0.1', N'uli201303081123270031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (387, N'sei201301161019460562', N'2013-03-08 11:28:13', 2, N'127.0.0.1', N'uli201303081128130765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (388, N'sei201301161019460562', N'2013-03-08 11:31:30', 2, N'127.0.0.1', N'uli201303081131300390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (389, N'cui201303051600540968', N'2013-03-08 11:58:35', 1, N'127.0.0.1', N'uli201303081158350906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (390, N'sei201301161019460562', N'2013-03-08 13:58:39', 2, N'127.0.0.1', N'uli201303081358390531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (391, N'sei201301161019460562', N'2013-03-08 14:15:25', 2, N'127.0.0.1', N'uli201303081415250328')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (392, N'sei201301161019460562', N'2013-03-08 14:34:16', 2, N'127.0.0.1', N'uli201303081434170031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (393, N'sei201301161019460562', N'2013-03-08 14:47:30', 2, N'127.0.0.1', N'uli201303081447310046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (394, N'sei201301161019460562', N'2013-03-08 15:20:26', 2, N'127.0.0.1', N'uli201303081520260906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (395, N'cti20121228134915123', N'2013-03-08 17:46:47', 1, N'127.0.0.1', N'uli201303081746470359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (396, N'cti20121228134915123', N'2013-03-08 17:47:24', 1, N'127.0.0.1', N'uli201303081747240453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (397, N'cti20121228134915123', N'2013-03-08 17:48:17', 1, N'127.0.0.1', N'uli201303081748170296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (398, N'cui201303051600540968', N'2013-03-10 10:43:02', 1, N'127.0.0.1', N'uli201303101043030070')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (399, N'cui201303051600540968', N'2013-03-10 11:48:36', 1, N'127.0.0.1', N'uli201303101148370051')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (400, N'cui201303051600540968', N'2013-03-10 11:51:20', 1, N'127.0.0.1', N'uli201303101151200658')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (401, N'cui201303051600540968', N'2013-03-10 13:57:20', 1, N'127.0.0.1', N'uli201303101357200640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (402, N'cui201303051600540968', N'2013-03-10 14:01:28', 1, N'127.0.0.1', N'uli201303101401280527')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (403, N'cui201303051600540968', N'2013-03-10 14:03:55', 1, N'127.0.0.1', N'uli201303101403550260')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (404, N'sei201301161019460562', N'2013-03-10 14:35:32', 2, N'127.0.0.1', N'uli201303101435320641')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (405, N'sei201301161019460562', N'2013-03-10 15:03:53', 2, N'127.0.0.1', N'uli201303101503540088')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (406, N'cui201303051600540968', N'2013-03-10 15:51:44', 1, N'127.0.0.1', N'uli201303101551440844')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (407, N'sei201301161019460562', N'2013-03-10 15:52:04', 2, N'127.0.0.1', N'uli201303101552040063')
GO
print 'Processed 400 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (408, N'sei201301161019460562', N'2013-03-10 16:56:04', 2, N'127.0.0.1', N'uli201303101656040998')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (409, N'sei201301161019460562', N'2013-03-10 17:03:25', 2, N'127.0.0.1', N'uli201303101703250976')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (410, N'sei201301161019460562', N'2013-03-10 17:09:41', 2, N'127.0.0.1', N'uli201303101709410190')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (411, N'sei201301161019460562', N'2013-03-10 17:37:16', 2, N'127.0.0.1', N'uli201303101737170028')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (412, N'sei201301161019460562', N'2013-03-10 17:47:16', 2, N'127.0.0.1', N'uli201303101747160771')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (413, N'sei201301161019460562', N'2013-03-10 22:22:50', 2, N'127.0.0.1', N'uli201303102222500671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (414, N'sei201301161019460562', N'2013-03-10 22:36:22', 2, N'127.0.0.1', N'uli201303102236220843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (415, N'sei201301161019460562', N'2013-03-10 22:51:26', 2, N'127.0.0.1', N'uli201303102251260437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (416, N'sei201301161019460562', N'2013-03-10 22:54:53', 2, N'127.0.0.1', N'uli201303102254530375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (417, N'sei201301161019460562', N'2013-03-10 23:14:06', 2, N'127.0.0.1', N'uli201303102314070015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (418, N'sei201301161019460562', N'2013-03-10 23:16:33', 2, N'127.0.0.1', N'uli201303102316330656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (419, N'sei201301161019460562', N'2013-03-10 23:22:14', 2, N'127.0.0.1', N'uli201303102322140171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (420, N'sei201301161019460562', N'2013-03-10 23:29:45', 2, N'127.0.0.1', N'uli201303102329450250')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (421, N'sei201301161019460562', N'2013-03-10 23:37:57', 2, N'127.0.0.1', N'uli201303102337580375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (422, N'cti20121228134915123', N'2013-03-11 08:43:39', 1, N'127.0.0.1', N'uli201303110843390453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (423, N'cti20121228134915123', N'2013-03-11 08:46:38', 1, N'127.0.0.1', N'uli201303110846380593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (424, N'cti20121228134915123', N'2013-03-11 09:16:38', 1, N'127.0.0.1', N'uli201303110916380328')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (425, N'cti20121228134915123', N'2013-03-11 09:42:34', 1, N'127.0.0.1', N'uli201303110942340953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (426, N'cti20121228134915123', N'2013-03-11 09:46:25', 1, N'127.0.0.1', N'uli201303110946250734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (427, N'cti20121228134915123', N'2013-03-11 09:49:29', 1, N'127.0.0.1', N'uli201303110949300031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (428, N'cti20121228134915123', N'2013-03-11 09:51:15', 1, N'127.0.0.1', N'uli201303110951150078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (429, N'cti20121228134915123', N'2013-03-11 10:17:50', 1, N'127.0.0.1', N'uli201303111017500703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (430, N'cti20121228134915123', N'2013-03-11 10:21:47', 1, N'127.0.0.1', N'uli201303111021470687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (431, N'cti20121228134915123', N'2013-03-11 10:23:27', 1, N'127.0.0.1', N'uli201303111023270062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (432, N'cti20121228134915123', N'2013-03-11 10:40:08', 1, N'127.0.0.1', N'uli201303111040080484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (433, N'cti20121228134915123', N'2013-03-11 10:48:28', 1, N'127.0.0.1', N'uli201303111048280359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (434, N'cti20121228134915123', N'2013-03-11 10:51:31', 1, N'127.0.0.1', N'uli201303111051310703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (435, N'cti20121228134915123', N'2013-03-11 10:53:59', 1, N'127.0.0.1', N'uli201303111053590687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (436, N'cti20121228134915123', N'2013-03-11 10:56:26', 1, N'127.0.0.1', N'uli201303111056260906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (437, N'cti20121228134915123', N'2013-03-11 14:28:01', 1, N'127.0.0.1', N'uli201303111428010890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (438, N'cti20121228134915123', N'2013-03-11 14:35:38', 1, N'127.0.0.1', N'uli201303111435380234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (439, N'cti20121228134915123', N'2013-03-11 14:37:58', 1, N'127.0.0.1', N'uli201303111437580484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (440, N'cti20121228134915123', N'2013-03-11 14:41:38', 1, N'127.0.0.1', N'uli201303111441380953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (441, N'cti20121228134915123', N'2013-03-11 14:48:05', 1, N'127.0.0.1', N'uli201303111448050906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (442, N'cti20121228134915123', N'2013-03-11 14:50:16', 1, N'127.0.0.1', N'uli201303111450160062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (443, N'cti20121228134915123', N'2013-03-11 14:52:14', 1, N'127.0.0.1', N'uli201303111452140500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (444, N'cti20121228134915123', N'2013-03-11 15:09:22', 1, N'127.0.0.1', N'uli201303111509220296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (445, N'cti20121228134915123', N'2013-03-11 15:10:46', 1, N'127.0.0.1', N'uli201303111510460468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (446, N'cti20121228134915123', N'2013-03-11 15:12:00', 1, N'127.0.0.1', N'uli201303111512000062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (447, N'cti20121228134915123', N'2013-03-11 15:26:16', 1, N'127.0.0.1', N'uli201303111526160937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (448, N'cti20121228134915123', N'2013-03-11 15:55:24', 1, N'127.0.0.1', N'uli201303111555240718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (449, N'cti20121228134915123', N'2013-03-11 15:56:14', 1, N'127.0.0.1', N'uli201303111556140875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (450, N'cti20121228134915123', N'2013-03-11 15:57:41', 1, N'127.0.0.1', N'uli201303111557410312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (451, N'cti20121228134915123', N'2013-03-11 16:00:10', 1, N'127.0.0.1', N'uli201303111600100671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (452, N'cti20121228134915123', N'2013-03-11 16:01:39', 1, N'127.0.0.1', N'uli201303111601390312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (453, N'cti20121228134915123', N'2013-03-11 16:07:48', 1, N'127.0.0.1', N'uli201303111607480890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (454, N'cti20121228134915123', N'2013-03-11 16:14:19', 1, N'127.0.0.1', N'uli201303111614190734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (455, N'cti20121228134915123', N'2013-03-11 16:17:24', 1, N'127.0.0.1', N'uli201303111617240703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (456, N'cui201301231020360421', N'2013-03-11 16:51:29', 1, N'127.0.0.1', N'uli201303111651300000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (457, N'cui201301231020360421', N'2013-03-11 16:52:01', 1, N'127.0.0.1', N'uli201303111652010875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (458, N'cti20121228134915123', N'2013-03-11 17:01:41', 1, N'127.0.0.1', N'uli201303111701410093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (459, N'cti20121228134915123', N'2013-03-11 17:12:55', 1, N'127.0.0.1', N'uli201303111712550468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (460, N'sei201301161019460562', N'2013-03-11 18:13:15', 2, N'127.0.0.1', N'uli201303111813150640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (461, N'sei201301161019460562', N'2013-03-11 18:17:02', 2, N'127.0.0.1', N'uli201303111817020500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (462, N'sei201301161019460562', N'2013-03-11 18:17:54', 2, N'127.0.0.1', N'uli201303111817540875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (463, N'sei201301161019460562', N'2013-03-11 18:19:33', 2, N'127.0.0.1', N'uli201303111819330062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (464, N'sei201301161019460562', N'2013-03-11 18:20:05', 2, N'127.0.0.1', N'uli201303111820050421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (465, N'sei201301161019460562', N'2013-03-11 18:22:06', 2, N'127.0.0.1', N'uli201303111822060421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (466, N'sei201301161019460562', N'2013-03-11 18:40:36', 2, N'127.0.0.1', N'uli201303111840360921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (467, N'sei201301161019460562', N'2013-03-11 18:45:38', 2, N'127.0.0.1', N'uli201303111845380156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (468, N'sei201301161019460562', N'2013-03-11 18:50:58', 2, N'127.0.0.1', N'uli201303111850580843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (469, N'cti20121228134915123', N'2013-03-11 19:20:28', 1, N'127.0.0.1', N'uli201303111920280609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (470, N'cti20121228134915123', N'2013-03-11 19:25:43', 1, N'127.0.0.1', N'uli201303111925430781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (471, N'cti20121228134915123', N'2013-03-11 19:31:30', 1, N'127.0.0.1', N'uli201303111931310000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (472, N'cti20121228134915123', N'2013-03-11 19:35:28', 1, N'127.0.0.1', N'uli201303111935280906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (473, N'cti20121228134915123', N'2013-03-11 19:43:17', 1, N'127.0.0.1', N'uli201303111943170270')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (474, N'cui201301231020360421', N'2013-03-11 21:22:41', 1, N'127.0.0.1', N'uli201303112122430046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (475, N'cti20121228134915123', N'2013-03-11 21:25:18', 1, N'0:0:0:0:0:0:0:1', N'uli201303112125180412')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (476, N'cti20121228134915123', N'2013-03-11 21:30:17', 1, N'0:0:0:0:0:0:0:1', N'uli201303112130170449')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (477, N'cui201303051600540968', N'2013-03-11 21:46:03', 1, N'127.0.0.1', N'uli201303112146080687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (478, N'cui201303051600540968', N'2013-03-11 21:52:05', 1, N'127.0.0.1', N'uli201303112152050859')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (479, N'cui201303051600540968', N'2013-03-11 22:18:01', 1, N'127.0.0.1', N'uli201303112218010218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (480, N'cui201303051600540968', N'2013-03-11 23:12:28', 1, N'127.0.0.1', N'uli201303112312280187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (481, N'cui201303051600540968', N'2013-03-11 23:14:06', 1, N'127.0.0.1', N'uli201303112314070046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (482, N'cui201303051600540968', N'2013-03-11 23:35:44', 1, N'127.0.0.1', N'uli201303112335440281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (483, N'cui201303051600540968', N'2013-03-11 23:47:13', 1, N'127.0.0.1', N'uli201303112347140062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (484, N'cui201303051600540968', N'2013-03-12 00:17:20', 1, N'127.0.0.1', N'uli201303120017200171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (485, N'cui201303051600540968', N'2013-03-12 00:19:50', 1, N'127.0.0.1', N'uli201303120019510031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (486, N'cti20121228134915123', N'2013-03-12 08:47:55', 1, N'127.0.0.1', N'uli201303120847550078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (487, N'cti20121228134915123', N'2013-03-12 08:53:19', 1, N'127.0.0.1', N'uli201303120853190781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (488, N'cti20121228134915123', N'2013-03-12 08:59:09', 1, N'127.0.0.1', N'uli201303120859090593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (489, N'cti20121228134915123', N'2013-03-12 09:17:22', 1, N'127.0.0.1', N'uli201303120917220359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (490, N'cti20121228134915123', N'2013-03-12 09:23:51', 1, N'127.0.0.1', N'uli201303120923510281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (491, N'cti20121228134915123', N'2013-03-12 10:09:08', 1, N'127.0.0.1', N'uli201303121009080078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (492, N'cti20121228134915123', N'2013-03-12 10:12:30', 1, N'127.0.0.1', N'uli201303121012300531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (493, N'cti20121228134915123', N'2013-03-12 10:29:13', 1, N'127.0.0.1', N'uli201303121029130828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (494, N'cti20121228134915123', N'2013-03-12 11:10:19', 1, N'127.0.0.1', N'uli201303121110190843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (495, N'cti20121228134915123', N'2013-03-12 11:22:54', 1, N'127.0.0.1', N'uli201303121122540875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (496, N'cti20121228134915123', N'2013-03-12 11:33:19', 1, N'127.0.0.1', N'uli201303121133190656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (497, N'cti20121228134915123', N'2013-03-12 11:40:57', 1, N'127.0.0.1', N'uli201303121140570722')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (498, N'cti20121228134915123', N'2013-03-12 11:45:10', 1, N'127.0.0.1', N'uli201303121145100203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (499, N'cti20121228134915123', N'2013-03-12 14:13:12', 1, N'127.0.0.1', N'uli201303121413120468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (500, N'cti20121228134915123', N'2013-03-12 14:21:03', 1, N'127.0.0.1', N'uli201303121421030500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (501, N'cti20121228134915123', N'2013-03-12 14:24:52', 1, N'127.0.0.1', N'uli201303121424520093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (502, N'cti20121228134915123', N'2013-03-12 14:29:11', 1, N'127.0.0.1', N'uli201303121429110078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (503, N'cti20121228134915123', N'2013-03-12 14:36:52', 1, N'127.0.0.1', N'uli201303121436520984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (504, N'cti20121228134915123', N'2013-03-12 15:00:52', 1, N'127.0.0.1', N'uli201303121500520390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (505, N'cti20121228134915123', N'2013-03-12 15:08:41', 1, N'127.0.0.1', N'uli201303121508410812')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (506, N'cti20121228134915123', N'2013-03-12 15:09:22', 1, N'127.0.0.1', N'uli201303121509220328')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (507, N'cti20121228134915123', N'2013-03-12 15:10:38', 1, N'127.0.0.1', N'uli201303121510380484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (508, N'cti20121228134915123', N'2013-03-12 15:13:35', 1, N'127.0.0.1', N'uli201303121513350921')
GO
print 'Processed 500 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (509, N'cti20121228134915123', N'2013-03-12 15:17:26', 1, N'127.0.0.1', N'uli201303121517260703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (510, N'cti20121228134915123', N'2013-03-12 15:21:12', 1, N'127.0.0.1', N'uli201303121521120218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (511, N'cui201303051600540968', N'2013-03-12 15:41:42', 1, N'127.0.0.1', N'uli201303121541420875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (512, N'cui201303051600540968', N'2013-03-12 15:50:00', 1, N'127.0.0.1', N'uli201303121550000515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (513, N'cui201303051600540968', N'2013-03-12 16:17:31', 1, N'127.0.0.1', N'uli201303121617310890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (514, N'cui201303051600540968', N'2013-03-12 16:27:26', 1, N'127.0.0.1', N'uli201303121627260671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (515, N'sei201301161019460562', N'2013-03-12 16:29:12', 2, N'127.0.0.1', N'uli201303121629120687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (516, N'sei201301161019460562', N'2013-03-12 16:31:23', 2, N'127.0.0.1', N'uli201303121631230156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (517, N'cti20121228134915123', N'2013-03-12 16:35:06', 1, N'127.0.0.1', N'uli201303121635060890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (518, N'cui201303051600540968', N'2013-03-12 16:43:50', 1, N'127.0.0.1', N'uli201303121643500359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (519, N'sei201301161019460562', N'2013-03-12 16:47:02', 2, N'127.0.0.1', N'uli201303121647020203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (520, N'cti20121228134915123', N'2013-03-12 16:49:37', 1, N'127.0.0.1', N'uli201303121649370875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (521, N'cti20121228134915123', N'2013-03-12 16:53:14', 1, N'127.0.0.1', N'uli201303121653140828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (522, N'cui201303051600540968', N'2013-03-12 16:54:01', 1, N'127.0.0.1', N'uli201303121654020015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (523, N'cui201303051600540968', N'2013-03-12 16:56:56', 1, N'127.0.0.1', N'uli201303121656560187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (524, N'cti20121228134915123', N'2013-03-12 17:00:13', 1, N'127.0.0.1', N'uli201303121700130406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (525, N'cti20121228134915123', N'2013-03-12 17:00:50', 1, N'127.0.0.1', N'uli201303121700500156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (526, N'cti20121228134915123', N'2013-03-12 17:01:42', 1, N'127.0.0.1', N'uli201303121701420312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (527, N'cti20121228134915123', N'2013-03-12 17:10:30', 1, N'127.0.0.1', N'uli201303121710300234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (528, N'cti20121228134915123', N'2013-03-12 17:11:13', 1, N'127.0.0.1', N'uli201303121711130750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (529, N'cti20121228134915123', N'2013-03-12 17:12:26', 1, N'127.0.0.1', N'uli201303121712260687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (530, N'cui201303051600540968', N'2013-03-12 17:13:20', 1, N'127.0.0.1', N'uli201303121713200671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (531, N'cti20121228134915123', N'2013-03-12 17:14:16', 1, N'127.0.0.1', N'uli201303121714160171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (532, N'cui201303051600540968', N'2013-03-12 17:14:25', 1, N'127.0.0.1', N'uli201303121714250906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (533, N'cui201303051600540968', N'2013-03-12 17:17:38', 1, N'127.0.0.1', N'uli201303121717380921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (534, N'sei201301161019460562', N'2013-03-12 17:19:40', 2, N'127.0.0.1', N'uli201303121719400421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (535, N'sei201301161019460562', N'2013-03-12 17:20:03', 2, N'127.0.0.1', N'uli201303121720030750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (536, N'cti20121228134915123', N'2013-03-12 17:29:51', 1, N'127.0.0.1', N'uli201303121729520031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (537, N'cti20121228134915123', N'2013-03-12 17:32:09', 1, N'127.0.0.1', N'uli201303121732090593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (538, N'cti20121228134915123', N'2013-03-12 17:36:26', 1, N'127.0.0.1', N'uli201303121736260578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (539, N'cti20121228134915123', N'2013-03-12 17:45:06', 1, N'127.0.0.1', N'uli201303121745060437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (540, N'cti20121228134915123', N'2013-03-12 17:52:30', 1, N'127.0.0.1', N'uli201303121752300796')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (541, N'cui201303051600540968', N'2013-03-12 23:09:13', 1, N'127.0.0.1', N'uli201303122309130531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (542, N'sei201301161019460562', N'2013-03-12 23:15:58', 2, N'127.0.0.1', N'uli201303122315580203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (543, N'cti20121228134915123', N'2013-03-13 09:08:23', 1, N'127.0.0.1', N'uli201303130908230609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (544, N'cti20121228134915123', N'2013-03-13 10:35:09', 1, N'127.0.0.1', N'uli201303131035090171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (545, N'cti20121228134915123', N'2013-03-13 10:49:53', 1, N'127.0.0.1', N'uli201303131049530343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (546, N'cti20121228134915123', N'2013-03-13 10:52:09', 1, N'127.0.0.1', N'uli201303131052090828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (547, N'cti20121228134915123', N'2013-03-13 19:18:05', 1, N'127.0.0.1', N'uli201303131918050125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (548, N'cti20121228134915123', N'2013-03-13 19:24:33', 1, N'127.0.0.1', N'uli201303131924330312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (549, N'', N'2013-03-13 20:35:11', -1, N'127.0.0.1', N'uli201303132035110906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (550, N'cui201303051600540968', N'2013-03-13 20:35:25', 1, N'127.0.0.1', N'uli201303132035250047')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (551, N'cui201303051600540968', N'2013-03-13 20:45:59', 1, N'127.0.0.1', N'uli201303132045590414')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (552, N'cti20121228134915123', N'2013-03-13 20:49:09', 1, N'127.0.0.1', N'uli201303132049090750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (553, N'cui201303051600540968', N'2013-03-13 20:49:46', 1, N'127.0.0.1', N'uli201303132049460442')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (554, N'cui201303051600540968', N'2013-03-13 23:08:26', 1, N'127.0.0.1', N'uli201303132308260562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (555, N'cui201303051600540968', N'2013-03-13 23:17:27', 1, N'127.0.0.1', N'uli201303132317270640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (556, N'cui201303051600540968', N'2013-03-13 23:24:32', 1, N'127.0.0.1', N'uli201303132324320140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (557, N'cti20121228134915123', N'2013-03-14 09:00:33', 1, N'127.0.0.1', N'uli201303140900330437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (558, N'cti20121228134915123', N'2013-03-14 09:10:50', 1, N'127.0.0.1', N'uli201303140910500640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (559, N'cti20121228134915123', N'2013-03-14 09:20:44', 1, N'127.0.0.1', N'uli201303140920440671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (560, N'cui201303051600540968', N'2013-03-14 09:33:58', 1, N'127.0.0.1', N'uli201303140933580843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (561, N'cui201303051600540968', N'2013-03-14 09:36:56', 1, N'127.0.0.1', N'uli201303140936560640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (562, N'cti20121228134915123', N'2013-03-14 09:56:23', 1, N'127.0.0.1', N'uli201303140956230515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (563, N'', N'2013-03-14 09:58:55', -1, N'127.0.0.1', N'uli201303140958550203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (564, N'cti20121228134915123', N'2013-03-14 09:59:38', 1, N'127.0.0.1', N'uli201303140959380890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (565, N'', N'2013-03-14 09:59:58', -1, N'127.0.0.1', N'uli201303140959580734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (566, N'cti20121228134915123', N'2013-03-14 10:02:50', 1, N'127.0.0.1', N'uli201303141002500218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (567, N'cti20121228134915123', N'2013-03-14 10:03:55', 1, N'127.0.0.1', N'uli201303141003550171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (568, N'', N'2013-03-14 10:05:31', -1, N'127.0.0.1', N'uli201303141005310359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (569, N'', N'2013-03-14 10:05:50', -1, N'127.0.0.1', N'uli201303141005500156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (570, N'cti20121228134915123', N'2013-03-14 10:07:01', 1, N'127.0.0.1', N'uli201303141007010187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (571, N'cti20121228134915123', N'2013-03-14 10:10:36', 1, N'127.0.0.1', N'uli201303141010360953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (572, N'cti20121228134915123', N'2013-03-14 10:11:15', 1, N'127.0.0.1', N'uli201303141011150406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (573, N'cui201303051600540968', N'2013-03-14 10:12:15', 1, N'127.0.0.1', N'uli201303141012150656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (574, N'cti20121228134915123', N'2013-03-14 10:18:15', 1, N'127.0.0.1', N'uli201303141018150390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (575, N'cti20121228134915123', N'2013-03-14 10:21:06', 1, N'127.0.0.1', N'uli201303141021060265')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (576, N'cti20121228134915123', N'2013-03-14 10:39:59', 1, N'127.0.0.1', N'uli201303141039590593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (577, N'cti20121228134915123', N'2013-03-14 10:40:56', 1, N'127.0.0.1', N'uli201303141040560046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (578, N'cti20121228134915123', N'2013-03-14 11:21:41', 1, N'127.0.0.1', N'uli201303141121410296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (579, N'cti20121228134915123', N'2013-03-14 11:39:32', 1, N'127.0.0.1', N'uli201303141139320203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (580, N'cti20121228134915123', N'2013-03-14 11:47:53', 1, N'127.0.0.1', N'uli201303141147530718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (581, N'cti20121228134915123', N'2013-03-14 11:48:12', 1, N'127.0.0.1', N'uli201303141148120359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (582, N'cti20121228134915123', N'2013-03-14 15:07:38', 1, N'127.0.0.1', N'uli201303141507380421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (583, N'cti20121228134915123', N'2013-03-14 15:12:08', 1, N'127.0.0.1', N'uli201303141512080859')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (584, N'cti20121228134915123', N'2013-03-14 15:14:05', 1, N'127.0.0.1', N'uli201303141514050546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (585, N'cti20121228134915123', N'2013-03-14 15:16:35', 1, N'127.0.0.1', N'uli201303141516350140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (586, N'cti20121228134915123', N'2013-03-14 15:21:11', 1, N'127.0.0.1', N'uli201303141521110343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (587, N'cti20121228134915123', N'2013-03-14 15:25:00', 1, N'127.0.0.1', N'uli201303141525000046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (588, N'cti20121228134915123', N'2013-03-14 15:26:26', 1, N'127.0.0.1', N'uli201303141526260093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (589, N'cti20121228134915123', N'2013-03-14 15:52:15', 1, N'127.0.0.1', N'uli201303141552150765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (590, N'cti20121228134915123', N'2013-03-14 15:54:05', 1, N'127.0.0.1', N'uli201303141554050140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (591, N'cti20121228134915123', N'2013-03-14 15:58:13', 1, N'127.0.0.1', N'uli201303141558130687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (592, N'cti20121228134915123', N'2013-03-14 16:20:22', 1, N'127.0.0.1', N'uli201303141620220328')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (593, N'cti20121228134915123', N'2013-03-14 16:30:00', 1, N'127.0.0.1', N'uli201303141630000921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (594, N'cti20121228134915123', N'2013-03-14 16:30:30', 1, N'127.0.0.1', N'uli201303141630300437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (595, N'cti20121228134915123', N'2013-03-14 16:31:22', 1, N'127.0.0.1', N'uli201303141631220656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (596, N'cti20121228134915123', N'2013-03-14 16:57:44', 1, N'127.0.0.1', N'uli201303141657440093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (597, N'cti20121228134915123', N'2013-03-14 16:58:38', 1, N'127.0.0.1', N'uli201303141658380968')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (598, N'cti20121228134915123', N'2013-03-14 17:00:37', 1, N'127.0.0.1', N'uli201303141700370234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (599, N'cti20121228134915123', N'2013-03-14 17:01:34', 1, N'127.0.0.1', N'uli201303141701340703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (600, N'cti20121228134915123', N'2013-03-14 17:04:39', 1, N'127.0.0.1', N'uli201303141704390671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (601, N'cti20121228134915123', N'2013-03-14 17:22:27', 1, N'127.0.0.1', N'uli201303141722270296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (602, N'cti20121228134915123', N'2013-03-14 17:36:39', 1, N'127.0.0.1', N'uli201303141736390421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (603, N'cti20121228134915123', N'2013-03-14 17:44:25', 1, N'127.0.0.1', N'uli201303141744250531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (604, N'cti20121228134915123', N'2013-03-14 18:09:11', 1, N'127.0.0.1', N'uli201303141809110500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (605, N'cti20121228134915123', N'2013-03-14 18:12:48', 1, N'127.0.0.1', N'uli201303141812480281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (606, N'cti20121228134915123', N'2013-03-14 18:20:30', 1, N'127.0.0.1', N'uli201303141820300046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (607, N'cti20121228134915123', N'2013-03-14 18:22:31', 1, N'127.0.0.1', N'uli201303141822310281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (608, N'cti20121228134915123', N'2013-03-14 18:54:52', 1, N'127.0.0.1', N'uli201303141854520140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (609, N'cti20121228134915123', N'2013-03-14 19:08:26', 1, N'127.0.0.1', N'uli201303141908260843')
GO
print 'Processed 600 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (610, N'cti20121228134915123', N'2013-03-14 19:15:36', 1, N'127.0.0.1', N'uli201303141915360234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (611, N'cti20121228134915123', N'2013-03-14 19:37:31', 1, N'127.0.0.1', N'uli201303141937310593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (612, N'cti20121228134915123', N'2013-03-14 19:55:07', 1, N'127.0.0.1', N'uli201303141955070312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (613, N'cti20121228134915123', N'2013-03-14 20:06:21', 1, N'127.0.0.1', N'uli201303142006210484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (614, N'cui201303051600540968', N'2013-03-14 23:02:27', 1, N'127.0.0.1', N'uli201303142302270359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (615, N'sei201301161019460562', N'2013-03-14 23:12:35', 2, N'127.0.0.1', N'uli201303142312350609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (616, N'sei201301161019460562', N'2013-03-14 23:13:40', 2, N'127.0.0.1', N'uli201303142313400500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (617, N'cui201303051600540968', N'2013-03-14 23:24:34', 1, N'127.0.0.1', N'uli201303142324340671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (618, N'cui201303051600540968', N'2013-03-14 23:46:19', 1, N'127.0.0.1', N'uli201303142346190437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (619, N'cui201303051600540968', N'2013-03-14 23:51:03', 1, N'127.0.0.1', N'uli201303142351030468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (620, N'cui201303051600540968', N'2013-03-15 00:05:34', 1, N'127.0.0.1', N'uli201303150005340734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (621, N'cti20121228134915123', N'2013-03-15 09:17:25', 1, N'127.0.0.1', N'uli201303150917250921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (622, N'cti20121228134915123', N'2013-03-15 09:17:49', 1, N'127.0.0.1', N'uli201303150917490046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (623, N'cti20121228134915123', N'2013-03-15 09:18:55', 1, N'127.0.0.1', N'uli201303150918550640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (624, N'cti20121228134915123', N'2013-03-15 09:22:00', 1, N'127.0.0.1', N'uli201303150922000671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (625, N'cti20121228134915123', N'2013-03-15 09:47:37', 1, N'127.0.0.1', N'uli201303150947370921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (626, N'cti20121228134915123', N'2013-03-15 09:53:48', 1, N'127.0.0.1', N'uli201303150953480625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (627, N'cti20121228134915123', N'2013-03-15 09:56:07', 1, N'127.0.0.1', N'uli201303150956070078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (628, N'cti20121228134915123', N'2013-03-15 10:00:17', 1, N'127.0.0.1', N'uli201303151000170312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (629, N'cti20121228134915123', N'2013-03-15 10:00:56', 1, N'127.0.0.1', N'uli201303151000560390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (630, N'cui201303051600540968', N'2013-03-15 10:06:33', 1, N'127.0.0.1', N'uli201303151006330218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (631, N'cui201303051600540968', N'2013-03-15 10:51:37', 1, N'127.0.0.1', N'uli201303151051370265')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (632, N'cui201303051600540968', N'2013-03-15 11:14:23', 1, N'127.0.0.1', N'uli201303151114230671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (633, N'cui201303051600540968', N'2013-03-15 11:26:33', 1, N'127.0.0.1', N'uli201303151126330578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (634, N'sei201301161019460562', N'2013-03-15 11:35:23', 2, N'127.0.0.1', N'uli201303151135230718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (635, N'sei201301161019460562', N'2013-03-15 11:36:43', 2, N'127.0.0.1', N'uli201303151136430406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (636, N'cui201303051600540968', N'2013-03-15 11:41:33', 1, N'127.0.0.1', N'uli201303151141330234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (637, N'cui201303051600540968', N'2013-03-15 11:51:54', 1, N'127.0.0.1', N'uli201303151151540937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (638, N'cui201303051600540968', N'2013-03-15 12:07:37', 1, N'127.0.0.1', N'uli201303151207370500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (639, N'cui201303051600540968', N'2013-03-15 12:13:28', 1, N'127.0.0.1', N'uli201303151213280609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (640, N'cti20121228134915123', N'2013-03-15 16:50:46', 1, N'127.0.0.1', N'uli201303151650460046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (641, N'cui201301231020360421', N'2013-03-15 17:10:16', 1, N'127.0.0.1', N'uli201303151710160156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (642, N'cti20121228134915123', N'2013-03-15 17:44:06', 1, N'0:0:0:0:0:0:0:1', N'uli201303151744060355')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (643, N'cti20121228134915123', N'2013-03-15 17:48:31', 1, N'0:0:0:0:0:0:0:1', N'uli201303151748310252')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (644, N'cti20121228134915123', N'2013-03-15 17:54:03', 1, N'127.0.0.1', N'uli201303151754030771')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (645, N'cti20121228134915123', N'2013-03-15 17:57:23', 1, N'127.0.0.1', N'uli201303151757230609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (646, N'cui201303051600540968', N'2013-03-17 10:28:14', 1, N'127.0.0.1', N'uli201303171028140078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (647, N'cti20121228134915123', N'2013-03-17 10:40:02', 1, N'113.106.60.17', N'uli201303171040020829')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (648, N'cti20121228134915123', N'2013-03-17 10:44:27', 1, N'113.106.60.17', N'uli201303171044270919')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (649, N'cti20121228134915123', N'2013-03-17 10:48:48', 1, N'113.106.60.17', N'uli201303171048480775')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (650, N'cti20121228134915123', N'2013-03-17 10:56:03', 1, N'113.106.60.17', N'uli201303171056030848')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (651, N'cti20121228134915123', N'2013-03-17 11:13:17', 1, N'113.106.60.17', N'uli201303171113170256')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (652, N'cui201303051600540968', N'2013-03-17 11:24:08', 1, N'127.0.0.1', N'uli201303171124080750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (653, N'cti20121228134915123', N'2013-03-17 11:25:38', 1, N'113.106.60.17', N'uli201303171125380694')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (654, N'cui201303051600540968', N'2013-03-17 11:28:33', 1, N'127.0.0.1', N'uli201303171128340015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (655, N'cti20121228134915123', N'2013-03-17 11:39:04', 1, N'113.106.60.17', N'uli201303171139040215')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (656, N'cti20121228134915123', N'2013-03-17 11:41:07', 1, N'113.106.60.17', N'uli201303171141070244')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (657, N'cti20121228134915123', N'2013-03-17 11:45:01', 1, N'113.106.60.17', N'uli201303171145010709')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (658, N'cti20121228134915123', N'2013-03-17 11:52:41', 1, N'113.106.60.17', N'uli201303171152410781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (659, N'cti20121228134915123', N'2013-03-17 11:55:14', 1, N'113.106.60.17', N'uli201303171155140951')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (660, N'cti20121228134915123', N'2013-03-17 11:59:28', 1, N'113.106.60.17', N'uli201303171159280729')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (661, N'cti20121228134915123', N'2013-03-17 12:02:29', 1, N'113.106.60.17', N'uli201303171202290414')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (662, N'cti20121228134915123', N'2013-03-17 12:03:13', 1, N'113.106.60.17', N'uli201303171203130351')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (663, N'cui201303051600540968', N'2013-03-17 14:04:00', 1, N'127.0.0.1', N'uli201303171404000828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (664, N'cti20121228134915123', N'2013-03-17 14:04:04', 1, N'113.106.60.17', N'uli201303171404040571')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (665, N'cti20121228134915123', N'2013-03-17 14:05:27', 1, N'0:0:0:0:0:0:0:1', N'uli201303171405280259')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (666, N'cti20121228134915123', N'2013-03-17 14:05:32', 1, N'113.106.60.17', N'uli201303171405320944')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (667, N'cui201303051600540968', N'2013-03-17 14:57:19', 1, N'127.0.0.1', N'uli201303171457190593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (668, N'cui201303051600540968', N'2013-03-17 14:59:38', 1, N'127.0.0.1', N'uli201303171459400000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (669, N'cui201303051600540968', N'2013-03-17 15:06:11', 1, N'127.0.0.1', N'uli201303171506110500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (670, N'cti20121228134915123', N'2013-03-17 15:06:16', 1, N'127.0.0.1', N'uli201303171506160382')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (671, N'cti20121228134915123', N'2013-03-17 15:11:40', 1, N'127.0.0.1', N'uli201303171511410003')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (672, N'sei201301161019460562', N'2013-03-17 15:20:19', 2, N'127.0.0.1', N'uli201303171520190171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (673, N'sei201301161019460562', N'2013-03-17 15:23:38', 2, N'127.0.0.1', N'uli201303171523380968')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (674, N'sei201301161019460562', N'2013-03-17 15:32:02', 2, N'127.0.0.1', N'uli201303171532020078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (675, N'sei201301161019460562', N'2013-03-17 15:44:38', 2, N'127.0.0.1', N'uli201303171544380031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (676, N'sei201301161019460562', N'2013-03-17 15:46:12', 2, N'127.0.0.1', N'uli201303171546120250')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (677, N'sei201301161019460562', N'2013-03-17 15:51:09', 2, N'127.0.0.1', N'uli201303171551090984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (678, N'sei201301161019460562', N'2013-03-17 16:11:38', 2, N'127.0.0.1', N'uli201303171611380203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (679, N'sei201301161019460562', N'2013-03-17 16:16:51', 2, N'127.0.0.1', N'uli201303171616510437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (680, N'cui201303051600540968', N'2013-03-17 16:19:21', 1, N'127.0.0.1', N'uli201303171619210609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (681, N'sei201301161019460562', N'2013-03-17 16:20:13', 2, N'127.0.0.1', N'uli201303171620130046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (682, N'cui201303051600540968', N'2013-03-17 16:31:45', 1, N'127.0.0.1', N'uli201303171631460000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (683, N'sei201301161019460562', N'2013-03-17 16:57:45', 2, N'127.0.0.1', N'uli201303171657450265')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (684, N'cui201303051600540968', N'2013-03-17 17:09:58', 1, N'127.0.0.1', N'uli201303171709580687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (685, N'sei201301161019460562', N'2013-03-17 17:18:55', 2, N'127.0.0.1', N'uli201303171718550937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (686, N'cti20121228134915123', N'2013-03-18 10:29:02', 1, N'127.0.0.1', N'uli201303181029020390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (687, N'cti20121228134915123', N'2013-03-18 10:31:31', 1, N'127.0.0.1', N'uli201303181031310953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (688, N'cti20121228134915123', N'2013-03-18 10:33:45', 1, N'127.0.0.1', N'uli201303181033450343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (689, N'cti20121228134915123', N'2013-03-18 11:37:51', 1, N'127.0.0.1', N'uli201303181137510734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (690, N'cti20121228134915123', N'2013-03-18 14:27:39', 1, N'127.0.0.1', N'uli201303181427390437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (691, N'cti20121228134915123', N'2013-03-18 14:31:11', 1, N'127.0.0.1', N'uli201303181431110187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (692, N'cti20121228134915123', N'2013-03-18 14:36:06', 1, N'127.0.0.1', N'uli201303181436060906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (693, N'cti20121228134915123', N'2013-03-18 14:43:30', 1, N'127.0.0.1', N'uli201303181443300656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (694, N'cti20121228134915123', N'2013-03-18 14:56:04', 1, N'127.0.0.1', N'uli201303181456040906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (695, N'cti20121228134915123', N'2013-03-18 15:29:20', 1, N'127.0.0.1', N'uli201303181529200921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (696, N'cui201303051600540968', N'2013-03-18 16:37:42', 1, N'127.0.0.1', N'uli201303181637420375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (697, N'cui201303051600540968', N'2013-03-18 16:53:56', 1, N'127.0.0.1', N'uli201303181653560625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (698, N'cui201303051600540968', N'2013-03-18 16:57:12', 1, N'127.0.0.1', N'uli201303181657130000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (699, N'cti20121228134915123', N'2013-03-18 17:26:23', 1, N'0:0:0:0:0:0:0:1', N'uli201303181726230799')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (700, N'cui201303051600540968', N'2013-03-18 17:33:46', 1, N'127.0.0.1', N'uli201303181733460593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (701, N'cui201303051600540968', N'2013-03-18 18:01:38', 1, N'127.0.0.1', N'uli201303181801380265')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (702, N'cui201303051600540968', N'2013-03-18 18:09:34', 1, N'127.0.0.1', N'uli201303181809340468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (703, N'cui201303051600540968', N'2013-03-18 22:36:16', 1, N'127.0.0.1', N'uli201303182236170046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (704, N'cui201303051600540968', N'2013-03-18 23:05:23', 1, N'127.0.0.1', N'uli201303182305240046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (705, N'sei201301161019460562', N'2013-03-18 23:11:14', 2, N'127.0.0.1', N'uli201303182311140312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (706, N'sei201301161019460562', N'2013-03-18 23:28:12', 2, N'127.0.0.1', N'uli201303182328130015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (707, N'cui201303051600540968', N'2013-03-18 23:51:24', 1, N'127.0.0.1', N'uli201303182351250078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (708, N'sei201301161019460562', N'2013-03-18 23:52:47', 2, N'127.0.0.1', N'uli201303182352470546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (709, N'cti20121228134915123', N'2013-03-19 08:53:02', 1, N'127.0.0.1', N'uli201303190853020203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (710, N'cti20121228134915123', N'2013-03-19 08:53:24', 1, N'127.0.0.1', N'uli201303190853240171')
GO
print 'Processed 700 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (711, N'cui201303051600540968', N'2013-03-19 08:56:03', 1, N'127.0.0.1', N'uli201303190856030234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (712, N'cti20121228134915123', N'2013-03-19 09:08:04', 1, N'127.0.0.1', N'uli201303190908040906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (713, N'cti20121228134915123', N'2013-03-19 09:15:45', 1, N'127.0.0.1', N'uli201303190915450750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (714, N'cti20121228134915123', N'2013-03-19 09:16:26', 1, N'127.0.0.1', N'uli201303190916260953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (715, N'cui201303051600540968', N'2013-03-19 09:16:21', 1, N'127.0.0.1', N'uli201303190916210937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (716, N'cui201303051600540968', N'2013-03-19 09:18:35', 1, N'127.0.0.1', N'uli201303190918360031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (717, N'cui201303051600540968', N'2013-03-19 09:21:29', 1, N'127.0.0.1', N'uli201303190921290187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (718, N'cui201303051600540968', N'2013-03-19 09:24:13', 1, N'127.0.0.1', N'uli201303190924130765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (719, N'sei201301161019460562', N'2013-03-19 09:24:40', 2, N'127.0.0.1', N'uli201303190924400046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (720, N'sei201301161019460562', N'2013-03-19 09:25:39', 2, N'127.0.0.1', N'uli201303190925390078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (721, N'sei201301161019460562', N'2013-03-19 09:28:21', 2, N'127.0.0.1', N'uli201303190928230625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (722, N'sei201301161019460562', N'2013-03-19 09:29:08', 2, N'127.0.0.1', N'uli201303190929080609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (723, N'sei201301161019460562', N'2013-03-19 09:32:36', 2, N'127.0.0.1', N'uli201303190932360718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (724, N'cti20121228134915123', N'2013-03-19 09:38:13', 1, N'127.0.0.1', N'uli201303190938130515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (725, N'cti20121228134915123', N'2013-03-19 09:39:11', 1, N'127.0.0.1', N'uli201303190939110140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (726, N'cti20121228134915123', N'2013-03-19 09:44:27', 1, N'127.0.0.1', N'uli201303190944270984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (727, N'cti20121228134915123', N'2013-03-19 09:48:47', 1, N'127.0.0.1', N'uli201303190948470062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (728, N'cti20121228134915123', N'2013-03-19 10:13:14', 1, N'127.0.0.1', N'uli201303191013140640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (729, N'cti20121228134915123', N'2013-03-19 10:33:13', 1, N'127.0.0.1', N'uli201303191033130140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (730, N'cti20121228134915123', N'2013-03-19 10:37:36', 1, N'127.0.0.1', N'uli201303191037370000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (731, N'cui201303051600540968', N'2013-03-19 10:55:10', 1, N'127.0.0.1', N'uli201303191055100656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (732, N'cti20121228134915123', N'2013-03-19 11:00:49', 1, N'127.0.0.1', N'uli201303191100490906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (733, N'cti20121228134915123', N'2013-03-19 11:03:44', 1, N'127.0.0.1', N'uli201303191103440343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (734, N'cui201303051600540968', N'2013-03-19 11:07:14', 1, N'127.0.0.1', N'uli201303191107140484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (735, N'cti20121228134915123', N'2013-03-19 11:08:57', 1, N'127.0.0.1', N'uli201303191108570781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (736, N'cti20121228134915123', N'2013-03-19 11:13:44', 1, N'127.0.0.1', N'uli201303191113440625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (737, N'cti20121228134915123', N'2013-03-19 11:16:54', 1, N'127.0.0.1', N'uli201303191116540828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (738, N'cti20121228134915123', N'2013-03-19 11:17:33', 1, N'127.0.0.1', N'uli201303191117330875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (739, N'cui201303051600540968', N'2013-03-19 11:17:43', 1, N'127.0.0.1', N'uli201303191117430953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (740, N'cti20121228134915123', N'2013-03-19 11:18:14', 1, N'127.0.0.1', N'uli201303191118140171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (741, N'cui201303051600540968', N'2013-03-19 11:18:45', 1, N'127.0.0.1', N'uli201303191118450359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (742, N'cti20121228134915123', N'2013-03-19 11:25:32', 1, N'127.0.0.1', N'uli201303191125320593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (743, N'cti20121228134915123', N'2013-03-19 11:51:12', 1, N'127.0.0.1', N'uli201303191151120562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (744, N'cti20121228134915123', N'2013-03-19 11:51:59', 1, N'127.0.0.1', N'uli201303191151590625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (745, N'cti20121228134915123', N'2013-03-19 11:52:27', 1, N'127.0.0.1', N'uli201303191152270437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (746, N'cti20121228134915123', N'2013-03-19 14:29:49', 1, N'127.0.0.1', N'uli201303191429490296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (747, N'cti20121228134915123', N'2013-03-19 14:31:17', 1, N'127.0.0.1', N'uli201303191431170125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (748, N'cti20121228134915123', N'2013-03-19 14:33:52', 1, N'127.0.0.1', N'uli201303191433520781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (749, N'cti20121228134915123', N'2013-03-19 14:44:03', 1, N'127.0.0.1', N'uli201303191444030562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (750, N'cti20121228134915123', N'2013-03-19 15:46:55', 1, N'127.0.0.1', N'uli201303191546550234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (751, N'cui201303051600540968', N'2013-03-19 16:06:14', 1, N'127.0.0.1', N'uli201303191606150078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (752, N'cti20121228134915123', N'2013-03-19 16:28:19', 1, N'127.0.0.1', N'uli201303191628190375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (753, N'cti20121228134915123', N'2013-03-19 16:33:22', 1, N'127.0.0.1', N'uli201303191633220453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (754, N'cti20121228134915123', N'2013-03-19 16:34:58', 1, N'127.0.0.1', N'uli201303191634580437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (755, N'cti20121228134915123', N'2013-03-19 16:49:09', 1, N'127.0.0.1', N'uli201303191649090703')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (756, N'sei201301161019460562', N'2013-03-19 16:50:35', 2, N'127.0.0.1', N'uli201303191650350375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (757, N'cti20121228134915123', N'2013-03-19 16:52:35', 1, N'127.0.0.1', N'uli201303191652350578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (758, N'cti20121228134915123', N'2013-03-19 17:49:41', 1, N'127.0.0.1', N'uli201303191749410625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (759, N'cti20121228134915123', N'2013-03-19 17:51:43', 1, N'127.0.0.1', N'uli201303191751430359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (760, N'cti20121228134915123', N'2013-03-19 18:00:12', 1, N'127.0.0.1', N'uli201303191800120906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (761, N'cti20121228134915123', N'2013-03-19 18:01:43', 1, N'127.0.0.1', N'uli201303191801430984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (762, N'cti20121228134915123', N'2013-03-19 18:06:52', 1, N'127.0.0.1', N'uli201303191806520593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (763, N'cti20121228134915123', N'2013-03-19 18:07:50', 1, N'127.0.0.1', N'uli201303191807500140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (764, N'cti20121228134915123', N'2013-03-19 18:10:35', 1, N'127.0.0.1', N'uli201303191810350890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (765, N'cti20121228134915123', N'2013-03-19 18:11:50', 1, N'127.0.0.1', N'uli201303191811500875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (766, N'cti20121228134915123', N'2013-03-19 18:14:36', 1, N'127.0.0.1', N'uli201303191814360750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (767, N'cti20121228134915123', N'2013-03-19 18:14:55', 1, N'127.0.0.1', N'uli201303191814550406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (768, N'cti20121228134915123', N'2013-03-19 18:17:20', 1, N'127.0.0.1', N'uli201303191817200156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (769, N'cti20121228134915123', N'2013-03-19 18:17:58', 1, N'127.0.0.1', N'uli201303191817580093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (770, N'cti20121228134915123', N'2013-03-19 18:51:39', 1, N'127.0.0.1', N'uli201303191851390906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (771, N'cti20121228134915123', N'2013-03-19 18:55:02', 1, N'127.0.0.1', N'uli201303191855020453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (772, N'cti20121228134915123', N'2013-03-19 19:08:22', 1, N'127.0.0.1', N'uli201303191908220093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (773, N'cti20121228134915123', N'2013-03-19 19:13:31', 1, N'127.0.0.1', N'uli201303191913310140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (774, N'cti20121228134915123', N'2013-03-19 19:17:44', 1, N'127.0.0.1', N'uli201303191917440546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (775, N'cti20121228134915123', N'2013-03-19 19:20:30', 1, N'127.0.0.1', N'uli201303191920300625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (776, N'cti20121228134915123', N'2013-03-19 19:22:27', 1, N'127.0.0.1', N'uli201303191922270750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (777, N'cui201303051600540968', N'2013-03-19 19:28:59', 1, N'127.0.0.1', N'uli201303191928590275')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (778, N'cti20121228134915123', N'2013-03-19 19:51:14', 1, N'127.0.0.1', N'uli201303191951140937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (779, N'cui201303051600540968', N'2013-03-19 20:32:33', 1, N'127.0.0.1', N'uli201303192032330960')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (780, N'cui201303051600540968', N'2013-03-19 20:36:22', 1, N'127.0.0.1', N'uli201303192036220442')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (781, N'cui201303051600540968', N'2013-03-19 20:38:17', 1, N'127.0.0.1', N'uli201303192038170737')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (782, N'cui201303051600540968', N'2013-03-19 23:02:15', 1, N'127.0.0.1', N'uli201303192302150484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (783, N'cui201303051600540968', N'2013-03-19 23:42:44', 1, N'127.0.0.1', N'uli201303192342440531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (784, N'cui201303051600540968', N'2013-03-20 08:59:14', 1, N'127.0.0.1', N'uli201303200859140187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (785, N'cti20121228134915123', N'2013-03-20 09:14:04', 1, N'127.0.0.1', N'uli201303200914040625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (786, N'cti20121228134915123', N'2013-03-20 09:18:57', 1, N'127.0.0.1', N'uli201303200918570078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (787, N'cti20121228134915123', N'2013-03-20 09:37:07', 1, N'127.0.0.1', N'uli201303200937070218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (788, N'cui201303051600540968', N'2013-03-20 09:48:30', 1, N'127.0.0.1', N'uli201303200948300843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (789, N'cti20121228134915123', N'2013-03-20 09:55:07', 1, N'127.0.0.1', N'uli201303200955070375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (790, N'cti20121228134915123', N'2013-03-20 09:56:27', 1, N'127.0.0.1', N'uli201303200956270937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (791, N'cti20121228134915123', N'2013-03-20 10:14:27', 1, N'127.0.0.1', N'uli201303201014270968')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (792, N'cti20121228134915123', N'2013-03-20 10:18:43', 1, N'127.0.0.1', N'uli201303201018430437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (793, N'cti20121228134915123', N'2013-03-20 10:23:20', 1, N'127.0.0.1', N'uli201303201023200609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (794, N'cui201303051600540968', N'2013-03-20 10:28:57', 1, N'127.0.0.1', N'uli201303201028570625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (795, N'cui201303051600540968', N'2013-03-20 10:31:42', 1, N'127.0.0.1', N'uli201303201031420390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (796, N'', N'2013-03-20 10:43:51', -1, N'127.0.0.1', N'uli201303201043510531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (797, N'cti20121228134915123', N'2013-03-20 10:44:45', 1, N'127.0.0.1', N'uli201303201044450406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (798, N'', N'2013-03-20 10:45:50', -1, N'127.0.0.1', N'uli201303201045500796')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (799, N'', N'2013-03-20 10:46:04', -1, N'127.0.0.1', N'uli201303201046040812')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (800, N'cti20121228134915123', N'2013-03-20 10:47:05', 1, N'127.0.0.1', N'uli201303201047050156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (801, N'', N'2013-03-20 10:48:24', -1, N'127.0.0.1', N'uli201303201048240187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (802, N'cti20121228134915123', N'2013-03-20 10:49:47', 1, N'127.0.0.1', N'uli201303201049470453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (803, N'cti20121228134915123', N'2013-03-20 10:55:43', 1, N'127.0.0.1', N'uli201303201055430656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (804, N'cti20121228134915123', N'2013-03-20 10:56:47', 1, N'127.0.0.1', N'uli201303201056470515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (805, N'cui201303051600540968', N'2013-03-20 11:00:24', 1, N'127.0.0.1', N'uli201303201100240781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (806, N'cui201303051600540968', N'2013-03-20 11:01:55', 1, N'127.0.0.1', N'uli201303201101550828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (807, N'cui201303051600540968', N'2013-03-20 11:06:52', 1, N'127.0.0.1', N'uli201303201106520156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (808, N'cti20121228134915123', N'2013-03-20 11:33:17', 1, N'127.0.0.1', N'uli201303201133170209')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (809, N'cti20121228134915123', N'2013-03-20 11:50:00', 1, N'127.0.0.1', N'uli201303201150000593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (810, N'cui201303051600540968', N'2013-03-20 11:58:42', 1, N'127.0.0.1', N'uli201303201158420453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (811, N'cui201303051600540968', N'2013-03-20 13:35:54', 1, N'127.0.0.1', N'uli201303201335540312')
GO
print 'Processed 800 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (812, N'cui201303051600540968', N'2013-03-20 13:37:10', 1, N'127.0.0.1', N'uli201303201337100312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (813, N'cti20121228134915123', N'2013-03-20 13:45:43', 1, N'127.0.0.1', N'uli201303201345430046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (814, N'cui201303051600540968', N'2013-03-20 13:52:59', 1, N'127.0.0.1', N'uli201303201352590671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (815, N'cti20121228134915123', N'2013-03-20 13:56:51', 1, N'127.0.0.1', N'uli201303201356510406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (816, N'sei201301161019460562', N'2013-03-20 14:13:38', 2, N'127.0.0.1', N'uli201303201413380687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (817, N'cti20121228134915123', N'2013-03-20 14:18:26', 1, N'127.0.0.1', N'uli201303201418260937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (818, N'cti20121228134915123', N'2013-03-20 14:23:06', 1, N'127.0.0.1', N'uli201303201423060390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (819, N'cti20121228134915123', N'2013-03-20 14:24:23', 1, N'127.0.0.1', N'uli201303201424230687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (820, N'cti20121228134915123', N'2013-03-20 14:31:28', 1, N'127.0.0.1', N'uli201303201431280578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (821, N'cti20121228134915123', N'2013-03-20 14:35:58', 1, N'127.0.0.1', N'uli201303201435580906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (822, N'cti20121228134915123', N'2013-03-20 14:37:10', 1, N'127.0.0.1', N'uli201303201437100859')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (823, N'cti20121228134915123', N'2013-03-20 14:38:47', 1, N'127.0.0.1', N'uli201303201438470843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (824, N'cti20121228134915123', N'2013-03-20 14:43:24', 1, N'127.0.0.1', N'uli201303201443240515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (825, N'cti20121228134915123', N'2013-03-20 14:44:06', 1, N'127.0.0.1', N'uli201303201444060437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (826, N'cui201303051600540968', N'2013-03-20 14:54:49', 1, N'127.0.0.1', N'uli201303201454490640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (827, N'cui201303051600540968', N'2013-03-20 15:02:19', 1, N'127.0.0.1', N'uli201303201502190734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (828, N'cti20121228134915123', N'2013-03-20 15:12:21', 1, N'127.0.0.1', N'uli201303201512210359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (829, N'cti20121228134915123', N'2013-03-20 15:29:27', 1, N'127.0.0.1', N'uli201303201529270718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (830, N'cui201303051600540968', N'2013-03-20 15:53:20', 1, N'127.0.0.1', N'uli201303201553200750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (831, N'cti20121228134915123', N'2013-03-20 15:54:42', 1, N'127.0.0.1', N'uli201303201554420390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (832, N'cti20121228134915123', N'2013-03-20 16:35:13', 1, N'127.0.0.1', N'uli201303201635130578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (833, N'cti20121228134915123', N'2013-03-20 16:38:25', 1, N'127.0.0.1', N'uli201303201638250671')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (834, N'cui201303051600540968', N'2013-03-20 16:38:56', 1, N'127.0.0.1', N'uli201303201638560828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (835, N'cui201303051600540968', N'2013-03-20 16:55:23', 1, N'127.0.0.1', N'uli201303201655230296')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (836, N'cui201303051600540968', N'2013-03-20 17:11:02', 1, N'127.0.0.1', N'uli201303201711020875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (837, N'cui201303051600540968', N'2013-03-20 17:21:17', 1, N'127.0.0.1', N'uli201303201721170953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (838, N'cui201303051600540968', N'2013-03-20 22:31:31', 1, N'127.0.0.1', N'uli201303202231320048')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (839, N'cui201303051600540968', N'2013-03-20 22:33:06', 1, N'127.0.0.1', N'uli201303202233060204')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (840, N'cui201303051600540968', N'2013-03-20 22:35:13', 1, N'127.0.0.1', N'uli201303202235130376')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (841, N'cui201303051600540968', N'2013-03-20 22:39:21', 1, N'127.0.0.1', N'uli201303202239220173')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (842, N'cui201303051600540968', N'2013-03-20 22:41:09', 1, N'127.0.0.1', N'uli201303202241090361')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (843, N'cui201303051600540968', N'2013-03-20 22:50:04', 1, N'127.0.0.1', N'uli201303202250040517')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (844, N'cui201303051600540968', N'2013-03-21 09:07:34', 1, N'127.0.0.1', N'uli201303210907340796')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (845, N'cti20121228134915123', N'2013-03-21 10:08:12', 1, N'127.0.0.1', N'uli201303211008120078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (846, N'cti20121228134915123', N'2013-03-21 10:14:33', 1, N'127.0.0.1', N'uli201303211014330546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (847, N'cti20121228134915123', N'2013-03-21 10:20:16', 1, N'127.0.0.1', N'uli201303211020160468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (848, N'cti20121228134915123', N'2013-03-21 10:22:37', 1, N'127.0.0.1', N'uli201303211022370906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (849, N'cti20121228134915123', N'2013-03-21 10:28:29', 1, N'127.0.0.1', N'uli201303211028290312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (850, N'cti20121228134915123', N'2013-03-21 10:30:29', 1, N'127.0.0.1', N'uli201303211030290515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (851, N'cti20121228134915123', N'2013-03-21 10:47:21', 1, N'127.0.0.1', N'uli201303211047210468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (852, N'cti20121228134915123', N'2013-03-21 10:49:01', 1, N'127.0.0.1', N'uli201303211049010531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (853, N'cui201303051600540968', N'2013-03-21 16:55:54', 1, N'127.0.0.1', N'uli201303211655540359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (854, N'cui201303051600540968', N'2013-03-21 16:58:21', 1, N'127.0.0.1', N'uli201303211658210250')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (855, N'cui201303051600540968', N'2013-03-21 17:01:03', 1, N'127.0.0.1', N'uli201303211701040015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (856, N'cui201303051600540968', N'2013-03-21 17:22:33', 1, N'127.0.0.1', N'uli201303211722330765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (857, N'cti20121228134915123', N'2013-03-21 18:09:45', 1, N'127.0.0.1', N'uli201303211809450953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (858, N'cti20121228134915123', N'2013-03-21 18:58:29', 1, N'127.0.0.1', N'uli201303211858290721')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (859, N'cti20121228134915123', N'2013-03-21 19:10:15', 1, N'127.0.0.1', N'uli201303211910150843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (860, N'cti20121228134915123', N'2013-03-21 19:11:36', 1, N'127.0.0.1', N'uli201303211911360031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (861, N'cti20121228134915123', N'2013-03-21 19:12:36', 1, N'127.0.0.1', N'uli201303211912360187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (862, N'cti20121228134915123', N'2013-03-21 19:15:09', 1, N'127.0.0.1', N'uli201303211915090890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (863, N'cti20121228134915123', N'2013-03-21 19:17:10', 1, N'127.0.0.1', N'uli201303211917100578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (864, N'cti20121228134915123', N'2013-03-21 19:31:51', 1, N'127.0.0.1', N'uli201303211931510750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (865, N'cti20121228134915123', N'2013-03-21 19:43:03', 1, N'127.0.0.1', N'uli201303211943030468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (866, N'cti20121228134915123', N'2013-03-21 19:45:44', 1, N'127.0.0.1', N'uli201303211945440562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (867, N'cti20121228134915123', N'2013-03-21 19:48:45', 1, N'127.0.0.1', N'uli201303211948450562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (868, N'cti20121228134915123', N'2013-03-21 19:51:25', 1, N'127.0.0.1', N'uli201303211951250390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (869, N'cti20121228134915123', N'2013-03-21 20:03:16', 1, N'127.0.0.1', N'uli201303212003170000')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (870, N'cti20121228134915123', N'2013-03-21 20:04:08', 1, N'127.0.0.1', N'uli201303212004080640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (871, N'cti20121228134915123', N'2013-03-21 20:09:28', 1, N'127.0.0.1', N'uli201303212009280515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (872, N'cti20121228134915123', N'2013-03-21 20:11:22', 1, N'127.0.0.1', N'uli201303212011220906')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (873, N'cti20121228134915123', N'2013-03-21 20:14:36', 1, N'127.0.0.1', N'uli201303212014360718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (874, N'cti20121228134915123', N'2013-03-21 20:21:20', 1, N'127.0.0.1', N'uli201303212021200140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (875, N'cti20121228134915123', N'2013-03-21 20:30:22', 1, N'127.0.0.1', N'uli201303212030220796')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (876, N'cti20121228134915123', N'2013-03-21 20:31:56', 1, N'127.0.0.1', N'uli201303212031560515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (877, N'cti20121228134915123', N'2013-03-21 20:34:00', 1, N'127.0.0.1', N'uli201303212034000078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (878, N'cti20121228134915123', N'2013-03-22 08:44:09', 1, N'127.0.0.1', N'uli201303220844090218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (879, N'cti20121228134915123', N'2013-03-22 08:47:45', 1, N'127.0.0.1', N'uli201303220847450406')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (880, N'cti20121228134915123', N'2013-03-22 08:51:29', 1, N'127.0.0.1', N'uli201303220851290093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (881, N'cti20121228134915123', N'2013-03-22 08:55:12', 1, N'127.0.0.1', N'uli201303220855120359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (882, N'cti20121228134915123', N'2013-03-22 10:09:33', 1, N'127.0.0.1', N'uli201303221009330515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (883, N'cti20121228134915123', N'2013-03-22 10:21:52', 1, N'127.0.0.1', N'uli201303221021520890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (884, N'cti20121228134915123', N'2013-03-22 10:39:11', 1, N'0:0:0:0:0:0:0:1', N'uli201303221039110904')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (885, N'cti20121228134915123', N'2013-03-22 10:53:55', 1, N'127.0.0.1', N'uli201303221053550515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (886, N'cti20121228134915123', N'2013-03-22 11:19:04', 1, N'127.0.0.1', N'uli201303221119040578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (887, N'cti20121228134915123', N'2013-03-22 11:24:09', 1, N'127.0.0.1', N'uli201303221124090093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (888, N'cti20121228134915123', N'2013-03-22 11:28:03', 1, N'127.0.0.1', N'uli201303221128030937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (889, N'cti20121228134915123', N'2013-03-22 11:40:47', 1, N'127.0.0.1', N'uli201303221140470546')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (890, N'cti20121228134915123', N'2013-03-22 11:42:17', 1, N'127.0.0.1', N'uli201303221142170734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (891, N'cti20121228134915123', N'2013-03-22 11:43:24', 1, N'127.0.0.1', N'uli201303221143240718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (892, N'cti20121228134915123', N'2013-03-22 11:58:22', 1, N'127.0.0.1', N'uli201303221158220812')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (893, N'cti20121228134915123', N'2013-03-22 13:43:01', 1, N'127.0.0.1', N'uli201303221343010875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (894, N'cti20121228134915123', N'2013-03-22 15:14:27', 1, N'127.0.0.1', N'uli201303221514270890')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (895, N'cui201303051600540968', N'2013-03-22 15:17:27', 1, N'127.0.0.1', N'uli201303221517270875')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (896, N'sei201301161019460562', N'2013-03-22 15:17:52', 2, N'127.0.0.1', N'uli201303221517520500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (897, N'cti20121228134915123', N'2013-03-23 10:35:11', 1, N'127.0.0.1', N'uli201303231035110281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (898, N'cti20121228134915123', N'2013-03-23 10:40:52', 1, N'127.0.0.1', N'uli201303231040520375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (899, N'cti20121228134915123', N'2013-03-23 10:43:04', 1, N'127.0.0.1', N'uli201303231043040234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (900, N'cti20121228134915123', N'2013-03-23 11:27:35', 1, N'127.0.0.1', N'uli201303231127350921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (901, N'cti20121228134915123', N'2013-03-23 11:47:44', 1, N'127.0.0.1', N'uli201303231147440218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (902, N'cti20121228134915123', N'2013-03-23 11:51:11', 1, N'127.0.0.1', N'uli201303231151110687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (903, N'cti20121228134915123', N'2013-03-23 11:50:11', 1, N'0:0:0:0:0:0:0:1', N'uli201303231150110923')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (904, N'sei201301161019460562', N'2013-03-23 13:26:41', 2, N'127.0.0.1', N'uli201303231326410781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (905, N'cti20121228134915123', N'2013-03-23 13:57:02', 1, N'127.0.0.1', N'uli201303231357020593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (906, N'cti20121228134915123', N'2013-03-23 14:15:55', 1, N'127.0.0.1', N'uli201303231415550312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (907, N'cti20121228134915123', N'2013-03-23 14:28:07', 1, N'113.106.60.17', N'uli201303231428070382')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (908, N'cti20121228134915123', N'2013-03-23 14:31:49', 1, N'127.0.0.1', N'uli201303231431490921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (909, N'cti20121228134915123', N'2013-03-23 14:50:24', 1, N'127.0.0.1', N'uli201303231450240484')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (910, N'cti20121228134915123', N'2013-03-23 15:25:57', 1, N'127.0.0.1', N'uli201303231525570359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (911, N'cti20121228134915123', N'2013-03-23 17:07:39', 1, N'127.0.0.1', N'uli201303231707390718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (912, N'cui201303051600540968', N'2013-03-23 22:38:18', 1, N'127.0.0.1', N'uli201303232238190031')
GO
print 'Processed 900 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (913, N'cui201303051600540968', N'2013-03-23 23:17:18', 1, N'127.0.0.1', N'uli201303232317190062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (914, N'cui201303051600540968', N'2013-03-23 23:33:54', 1, N'127.0.0.1', N'uli201303232333540390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (915, N'cti20121228134915123', N'2013-03-24 15:05:05', 1, N'127.0.0.1', N'uli201303241505050187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (916, N'cti20121228134915123', N'2013-03-24 15:09:52', 1, N'127.0.0.1', N'uli201303241509520093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (917, N'cui201303051600540968', N'2013-03-24 15:08:21', 1, N'127.0.0.1', N'uli201303241508220299')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (918, N'cti20121228134915123', N'2013-03-24 15:13:28', 1, N'127.0.0.1', N'uli201303241513280828')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (919, N'cti20121228134915123', N'2013-03-24 15:19:21', 1, N'127.0.0.1', N'uli201303241519210437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (920, N'cti20121228134915123', N'2013-03-24 15:21:35', 1, N'127.0.0.1', N'uli201303241521350750')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (921, N'cti20121228134915123', N'2013-03-24 15:25:00', 1, N'127.0.0.1', N'uli201303241525000312')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (922, N'cui201303051600540968', N'2013-03-24 15:25:51', 1, N'127.0.0.1', N'uli201303241525510668')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (923, N'cui201303051600540968', N'2013-03-24 15:30:26', 1, N'127.0.0.1', N'uli201303241530260648')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (924, N'cti20121228134915123', N'2013-03-24 15:34:48', 1, N'127.0.0.1', N'uli201303241534480125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (925, N'cti20121228134915123', N'2013-03-24 15:38:03', 1, N'127.0.0.1', N'uli201303241538030078')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (926, N'cti20121228134915123', N'2013-03-24 15:38:30', 1, N'127.0.0.1', N'uli201303241538300437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (927, N'cui201303051600540968', N'2013-03-24 15:37:29', 1, N'127.0.0.1', N'uli201303241537290693')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (928, N'cui201303051600540968', N'2013-03-24 15:46:34', 1, N'127.0.0.1', N'uli201303241546340706')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (929, N'cui201303051600540968', N'2013-03-24 15:49:21', 1, N'127.0.0.1', N'uli201303241549210953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (930, N'cui201303051600540968', N'2013-03-24 15:54:26', 1, N'127.0.0.1', N'uli201303241554260308')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (931, N'cui201303051600540968', N'2013-03-24 16:04:32', 1, N'127.0.0.1', N'uli201303241604320363')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (932, N'cui201303051600540968', N'2013-03-24 16:10:37', 1, N'127.0.0.1', N'uli201303241610370203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (933, N'cui201303051600540968', N'2013-03-24 16:14:09', 1, N'127.0.0.1', N'uli201303241614090561')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (934, N'cui201303051600540968', N'2013-03-24 16:18:24', 1, N'127.0.0.1', N'uli201303241618240847')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (935, N'cui201303051600540968', N'2013-03-24 16:26:02', 1, N'127.0.0.1', N'uli201303241626020102')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (936, N'cui201303051600540968', N'2013-03-24 16:27:19', 1, N'127.0.0.1', N'uli201303241627190663')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (937, N'cui201303051600540968', N'2013-03-24 16:55:23', 1, N'127.0.0.1', N'uli201303241655230766')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (938, N'cui201303051600540968', N'2013-03-24 17:01:38', 1, N'127.0.0.1', N'uli201303241701380199')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (939, N'cui201303051600540968', N'2013-03-24 17:13:00', 1, N'127.0.0.1', N'uli201303241713000127')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (940, N'cti20121228134915123', N'2013-03-24 17:15:54', 1, N'127.0.0.1', N'uli201303241715540140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (941, N'cui201303051600540968', N'2013-03-24 17:14:48', 1, N'127.0.0.1', N'uli201303241714480798')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (942, N'cui201303051600540968', N'2013-03-24 17:16:28', 1, N'127.0.0.1', N'uli201303241716280638')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (943, N'sei201301161019460562', N'2013-03-24 18:03:04', 2, N'127.0.0.1', N'uli201303241803040850')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (944, N'sei201301161019460562', N'2013-03-24 18:07:27', 2, N'127.0.0.1', N'uli201303241807270128')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (945, N'cui201303051600540968', N'2013-03-24 19:13:03', 1, N'127.0.0.1', N'uli201303241913030346')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (946, N'sei201301161019460562', N'2013-03-24 19:13:24', 2, N'127.0.0.1', N'uli201303241913240377')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (947, N'sei201301161019460562', N'2013-03-24 19:16:42', 2, N'127.0.0.1', N'uli201303241916420077')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (948, N'sei201301161019460562', N'2013-03-24 19:18:02', 2, N'127.0.0.1', N'uli201303241918020045')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (949, N'sei201301161019460562', N'2013-03-24 19:24:26', 2, N'127.0.0.1', N'uli201303241924260728')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (950, N'sei201301161019460562', N'2013-03-24 19:29:27', 2, N'127.0.0.1', N'uli201303241929270302')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (951, N'sei201301161019460562', N'2013-03-24 19:39:50', 2, N'127.0.0.1', N'uli201303241939500457')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (952, N'sei201301161019460562', N'2013-03-24 19:49:59', 2, N'127.0.0.1', N'uli201303241950000056')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (953, N'sei201301161019460562', N'2013-03-24 19:57:29', 2, N'127.0.0.1', N'uli201303241957290922')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (954, N'cui201303051600540968', N'2013-03-24 20:17:20', 1, N'127.0.0.1', N'uli201303242017200204')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (955, N'cui201303051600540968', N'2013-03-24 20:24:13', 1, N'127.0.0.1', N'uli201303242024130902')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (956, N'sei201301161019460562', N'2013-03-24 23:14:33', 2, N'127.0.0.1', N'uli201303242314330515')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (957, N'cui201303051600540968', N'2013-03-24 23:16:42', 1, N'127.0.0.1', N'uli201303242316430046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (958, N'cui201303051600540968', N'2013-03-24 23:33:06', 1, N'127.0.0.1', N'uli201303242333060390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (959, N'sei201301161019460562', N'2013-03-24 23:53:15', 2, N'127.0.0.1', N'uli201303242353150390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (960, N'sei201301161019460562', N'2013-03-25 09:20:55', 2, N'127.0.0.1', N'uli201303250920550843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (961, N'sei201301161019460562', N'2013-03-25 09:26:14', 2, N'127.0.0.1', N'uli201303250926150593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (962, N'sei201301161019460562', N'2013-03-25 09:40:29', 2, N'127.0.0.1', N'uli201303250940290218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (963, N'sei201301161019460562', N'2013-03-25 09:53:03', 2, N'127.0.0.1', N'uli201303250953030687')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (964, N'sei201301161019460562', N'2013-03-25 10:18:16', 2, N'127.0.0.1', N'uli201303251018160234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (965, N'cti20121228134915123', N'2013-03-25 10:33:42', 1, N'127.0.0.1', N'uli201303251033420093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (966, N'cui201303051600540968', N'2013-03-25 10:58:21', 1, N'127.0.0.1', N'uli201303251058210625')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (967, N'cui201303051600540968', N'2013-03-25 11:10:32', 1, N'127.0.0.1', N'uli201303251110320031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (968, N'cti20121228134915123', N'2013-03-25 11:20:28', 1, N'113.106.60.17', N'uli201303251120280578')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (969, N'cti20121228134915123', N'2013-03-25 11:23:12', 1, N'127.0.0.1', N'uli201303251123120263')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (970, N'cti20121228134915123', N'2013-03-25 11:33:53', 1, N'113.106.60.17', N'uli201303251133530547')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (971, N'cti20121228134915123', N'2013-03-25 11:47:55', 1, N'113.106.60.17', N'uli201303251147550672')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (972, N'cti20121228134915123', N'2013-03-25 11:49:02', 1, N'113.106.60.17', N'uli201303251149020428')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (973, N'cti20121228134915123', N'2013-03-25 11:49:59', 1, N'113.106.60.17', N'uli201303251149590824')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (974, N'cti20121228134915123', N'2013-03-25 11:51:03', 1, N'113.106.60.17', N'uli201303251151030802')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (975, N'cti20121228134915123', N'2013-03-25 11:53:05', 1, N'113.106.60.17', N'uli201303251153050758')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (976, N'cti20121228134915123', N'2013-03-25 11:57:08', 1, N'113.106.60.17', N'uli201303251157080742')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (977, N'cui201303051600540968', N'2013-03-25 11:58:57', 1, N'127.0.0.1', N'uli201303251158570109')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (978, N'cti20121228134915123', N'2013-03-25 12:31:52', 1, N'113.106.60.17', N'uli201303251231520682')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (979, N'cti20121228134915123', N'2013-03-25 12:32:35', 1, N'113.106.60.17', N'uli201303251232350663')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (980, N'cti20121228134915123', N'2013-03-25 13:41:25', 1, N'113.106.60.17', N'uli201303251341250921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (981, N'cti20121228134915123', N'2013-03-25 13:54:04', 1, N'113.106.60.17', N'uli201303251354040729')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (982, N'cti20121228134915123', N'2013-03-25 13:57:57', 1, N'113.106.60.17', N'uli201303251357570631')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (983, N'cui201303051600540968', N'2013-03-25 14:04:36', 1, N'127.0.0.1', N'uli201303251404360203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (984, N'cti20121228134915123', N'2013-03-25 14:12:35', 1, N'113.106.60.17', N'uli201303251412350338')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (985, N'cti20121228134915123', N'2013-03-25 14:15:13', 1, N'113.106.60.17', N'uli201303251415130822')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (986, N'cti20121228134915123', N'2013-03-25 14:18:02', 1, N'113.106.60.17', N'uli201303251418020492')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (987, N'cti20121228134915123', N'2013-03-25 14:19:53', 1, N'113.106.60.17', N'uli201303251419530648')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (988, N'cti20121228134915123', N'2013-03-25 14:35:20', 1, N'113.106.60.17', N'uli201303251435200909')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (989, N'cti20121228134915123', N'2013-03-25 14:35:58', 1, N'127.0.0.1', N'uli201303251435580501')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (990, N'cti20121228134915123', N'2013-03-25 14:37:07', 1, N'113.106.60.17', N'uli201303251437070872')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (991, N'cti20121228134915123', N'2013-03-25 14:50:15', 1, N'113.106.60.17', N'uli201303251450160026')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (992, N'cti20121228134915123', N'2013-03-25 14:55:38', 1, N'113.106.60.17', N'uli201303251455380399')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (993, N'cui201303051600540968', N'2013-03-25 15:37:50', 1, N'127.0.0.1', N'uli201303251537510015')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (994, N'cti20121228134915123', N'2013-03-25 15:36:35', 1, N'113.106.60.17', N'uli201303251536350290')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (995, N'cti20121228134915123', N'2013-03-25 15:42:56', 1, N'113.106.60.17', N'uli201303251542560877')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (996, N'cui201303051600540968', N'2013-03-25 15:46:12', 1, N'127.0.0.1', N'uli201303251546120656')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (997, N'cui201303051600540968', N'2013-03-25 15:53:24', 1, N'127.0.0.1', N'uli201303251553240531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (998, N'cui201303051600540968', N'2013-03-25 16:12:26', 1, N'127.0.0.1', N'uli201303251612260281')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (999, N'cti20121228134915123', N'2013-03-25 17:14:55', 1, N'113.106.60.17', N'uli201303251714550244')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1000, N'cti20121228134915123', N'2013-03-25 17:16:19', 1, N'113.106.60.17', N'uli201303251716190899')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1001, N'cti20121228134915123', N'2013-03-25 17:18:32', 1, N'113.106.60.17', N'uli201303251718320132')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1002, N'cti20121228134915123', N'2013-03-25 17:31:34', 1, N'113.106.60.17', N'uli201303251731340087')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1003, N'cti20121228134915123', N'2013-03-25 17:35:27', 1, N'113.106.60.17', N'uli201303251735270700')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1004, N'cti20121228134915123', N'2013-03-25 17:36:59', 1, N'127.0.0.1', N'uli201303251736590618')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1005, N'cti20121228134915123', N'2013-03-25 17:54:27', 1, N'127.0.0.1', N'uli201303251754270314')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1006, N'cti20121228134915123', N'2013-03-25 17:57:19', 1, N'127.0.0.1', N'uli201303251757190623')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1007, N'cti20121228134915123', N'2013-03-25 18:01:24', 1, N'127.0.0.1', N'uli201303251801250040')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1008, N'cti20121228134915123', N'2013-03-25 19:22:41', 1, N'127.0.0.1', N'uli201303251922410695')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1009, N'cui201303051600540968', N'2013-03-25 20:01:12', 1, N'127.0.0.1', N'uli201303252001120453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1010, N'cui201303051600540968', N'2013-03-26 22:00:09', 1, N'127.0.0.1', N'uli201303262200090234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1011, N'cui201303051600540968', N'2013-03-26 22:57:57', 1, N'127.0.0.1', N'uli201303262257570562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1012, N'sei201301161019460562', N'2013-03-26 23:01:28', 2, N'127.0.0.1', N'uli201303262301280343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1013, N'cui201301231020360421', N'2013-03-26 23:26:48', 1, N'0:0:0:0:0:0:0:1', N'uli201303262326500183')
GO
print 'Processed 1000 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1014, N'cti20121228134915123', N'2013-03-26 23:27:41', 1, N'0:0:0:0:0:0:0:1', N'uli201303262327410562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1015, N'cti20121228134915123', N'2013-03-27 08:59:25', 1, N'127.0.0.1', N'uli201303270859250463')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1016, N'cti20121228134915123', N'2013-03-27 09:05:05', 1, N'127.0.0.1', N'uli201303270905050972')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1017, N'cti20121228134915123', N'2013-03-27 09:13:24', 1, N'127.0.0.1', N'uli201303270913240603')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1018, N'sei201301161019460562', N'2013-03-27 09:20:14', 2, N'127.0.0.1', N'uli201303270920140939')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1019, N'cti20121228134915123', N'2013-03-27 09:20:44', 1, N'127.0.0.1', N'uli201303270920440423')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1020, N'cti20121228134915123', N'2013-03-27 09:32:02', 1, N'113.106.60.17', N'uli201303270932020691')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1021, N'cti20121228134915123', N'2013-03-27 09:34:41', 1, N'113.106.60.17', N'uli201303270934420001')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1022, N'cti20121228134915123', N'2013-03-27 09:36:29', 1, N'127.0.0.1', N'uli201303270936290186')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1023, N'cti20121228134915123', N'2013-03-27 09:44:44', 1, N'127.0.0.1', N'uli201303270944440348')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1024, N'cti20121228134915123', N'2013-03-27 09:48:19', 1, N'127.0.0.1', N'uli201303270948190126')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1025, N'cti20121228134915123', N'2013-03-27 09:53:29', 1, N'127.0.0.1', N'uli201303270953290057')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1026, N'cti20121228134915123', N'2013-03-27 09:54:26', 1, N'127.0.0.1', N'uli201303270954260697')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1027, N'cti20121228134915123', N'2013-03-27 09:56:34', 1, N'127.0.0.1', N'uli201303270956340663')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1028, N'cti20121228134915123', N'2013-03-27 10:01:44', 1, N'127.0.0.1', N'uli201303271001440860')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1029, N'cti20121228134915123', N'2013-03-27 10:51:29', 1, N'127.0.0.1', N'uli201303271051290209')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1030, N'cti20121228134915123', N'2013-03-27 10:58:03', 1, N'127.0.0.1', N'uli201303271058030248')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1031, N'cui201303051600540968', N'2013-03-27 13:43:41', 1, N'127.0.0.1', N'uli201303271343410093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1032, N'sei201301161019460562', N'2013-03-27 14:16:16', 2, N'127.0.0.1', N'uli201303271416170011')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1033, N'cui201303051600540968', N'2013-03-27 14:36:22', 1, N'127.0.0.1', N'uli201303271436220156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1034, N'cui201303051600540968', N'2013-03-27 15:00:24', 1, N'127.0.0.1', N'uli201303271500240453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1035, N'cui201303051600540968', N'2013-03-27 15:24:55', 1, N'127.0.0.1', N'uli201303271524550187')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1036, N'sei201301161019460562', N'2013-03-27 15:32:16', 2, N'127.0.0.1', N'uli201303271532160796')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1037, N'cui201303051600540968', N'2013-03-27 15:32:55', 1, N'127.0.0.1', N'uli201303271532550968')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1038, N'cui201303051600540968', N'2013-03-27 15:36:36', 1, N'127.0.0.1', N'uli201303271536360437')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1039, N'cui201303051600540968', N'2013-03-27 15:44:40', 1, N'127.0.0.1', N'uli201303271544400218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1040, N'cui201303051600540968', N'2013-03-27 15:50:01', 1, N'127.0.0.1', N'uli201303271550010796')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1041, N'cti20121228134915123', N'2013-03-27 15:49:47', 1, N'127.0.0.1', N'uli201303271549470718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1042, N'cui201303051600540968', N'2013-03-27 15:54:59', 1, N'127.0.0.1', N'uli201303271554590781')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1043, N'cti20121228134915123', N'2013-03-27 20:57:32', 1, N'113.106.60.17', N'uli201303272057320962')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1044, N'cti20121228134915123', N'2013-03-28 09:06:34', 1, N'127.0.0.1', N'uli201303280906340031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1045, N'cti20121228134915123', N'2013-03-28 09:07:50', 1, N'127.0.0.1', N'uli201303280907500953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1046, N'cti20121228134915123', N'2013-03-28 09:43:18', 1, N'127.0.0.1', N'uli201303280943180765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1047, N'cti20121228134915123', N'2013-03-28 10:10:07', 1, N'127.0.0.1', N'uli201303281010070062')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1048, N'cti20121228134915123', N'2013-03-28 11:01:59', 1, N'113.106.60.17', N'uli201303281101590116')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1049, N'cti20121228134915123', N'2013-03-28 13:36:14', 1, N'113.106.60.17', N'uli201303281336140969')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1050, N'cti20121228134915123', N'2013-03-28 13:40:26', 1, N'113.106.60.17', N'uli201303281340260732')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1051, N'cti20121228134915123', N'2013-03-28 13:48:27', 1, N'113.106.60.17', N'uli201303281348270118')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1052, N'cti20121228134915123', N'2013-03-28 13:50:05', 1, N'113.106.60.17', N'uli201303281350050184')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1053, N'cti20121228134915123', N'2013-03-28 13:54:31', 1, N'113.106.60.17', N'uli201303281354310853')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1054, N'cti20121228134915123', N'2013-03-28 13:55:46', 1, N'113.106.60.17', N'uli201303281355460489')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1055, N'cti20121228134915123', N'2013-03-28 14:01:55', 1, N'113.106.60.17', N'uli201303281401550651')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1056, N'cti20121228134915123', N'2013-03-28 14:04:08', 1, N'113.106.60.17', N'uli201303281404080899')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1057, N'cti20121228134915123', N'2013-03-28 14:04:33', 1, N'113.106.60.17', N'uli201303281404330633')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1058, N'cti20121228134915123', N'2013-03-28 14:09:32', 1, N'113.106.60.17', N'uli201303281409320784')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1059, N'cti20121228134915123', N'2013-03-28 15:11:37', 1, N'113.106.60.17', N'uli201303281511370452')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1060, N'cti20121228134915123', N'2013-03-28 15:15:08', 1, N'113.106.60.17', N'uli201303281515080917')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1061, N'cti20121228134915123', N'2013-03-28 15:17:54', 1, N'113.106.60.17', N'uli201303281517540368')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1062, N'cti20121228134915123', N'2013-03-28 15:18:34', 1, N'113.106.60.17', N'uli201303281518340726')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1063, N'cti20121228134915123', N'2013-03-28 15:21:22', 1, N'113.106.60.17', N'uli201303281521220881')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1064, N'cti20121228134915123', N'2013-03-28 15:26:33', 1, N'113.106.60.17', N'uli201303281526330080')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1065, N'cti20121228134915123', N'2013-03-28 15:30:32', 1, N'113.106.60.17', N'uli201303281530320217')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1066, N'cti20121228134915123', N'2013-03-28 15:37:09', 1, N'113.106.60.17', N'uli201303281537090790')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1067, N'cti20121228134915123', N'2013-03-28 16:46:03', 1, N'113.106.60.17', N'uli201303281646030600')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1068, N'cti20121228134915123', N'2013-03-28 16:52:30', 1, N'113.106.60.17', N'uli201303281652300845')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1069, N'cti20121228134915123', N'2013-03-28 16:54:53', 1, N'113.106.60.17', N'uli201303281654530640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1070, N'cti20121228134915123', N'2013-03-28 17:32:32', 1, N'113.106.60.17', N'uli201303281732320176')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1071, N'cti20121228134915123', N'2013-03-28 17:35:55', 1, N'113.106.60.17', N'uli201303281735550422')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1072, N'cti20121228134915123', N'2013-03-28 17:37:48', 1, N'113.106.60.17', N'uli201303281737480749')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1073, N'cti20121228134915123', N'2013-03-28 17:40:11', 1, N'113.106.60.17', N'uli201303281740110685')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1074, N'cti20121228134915123', N'2013-03-28 17:49:34', 1, N'113.106.60.17', N'uli201303281749340302')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1075, N'cti20121228134915123', N'2013-03-28 18:01:24', 1, N'113.106.60.17', N'uli201303281801240371')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1076, N'cti20121228134915123', N'2013-03-28 18:03:06', 1, N'113.106.60.17', N'uli201303281803060120')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1077, N'cti20121228134915123', N'2013-03-28 18:05:28', 1, N'113.106.60.17', N'uli201303281805280556')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1078, N'cti20121228134915123', N'2013-03-28 18:50:48', 1, N'113.106.60.17', N'uli201303281850480646')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1079, N'cti20121228134915123', N'2013-03-28 18:54:19', 1, N'113.106.60.17', N'uli201303281854190190')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1080, N'cti20121228134915123', N'2013-03-28 18:55:22', 1, N'113.106.60.17', N'uli201303281855220408')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1081, N'cti20121228134915123', N'2013-03-28 18:55:56', 1, N'113.106.60.17', N'uli201303281855560423')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1082, N'cti20121228134915123', N'2013-03-28 19:01:19', 1, N'113.106.60.17', N'uli201303281901190107')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1083, N'cti20121228134915123', N'2013-03-28 19:18:03', 1, N'113.106.60.17', N'uli201303281918030672')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1084, N'cti20121228134915123', N'2013-03-28 19:24:05', 1, N'113.106.60.17', N'uli201303281924050777')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1085, N'cti20121228134915123', N'2013-03-28 19:27:17', 1, N'113.106.60.17', N'uli201303281927170962')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1086, N'cti20121228134915123', N'2013-03-28 19:33:50', 1, N'113.106.60.17', N'uli201303281933500425')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1087, N'cti20121228134915123', N'2013-03-28 19:47:58', 1, N'113.106.60.17', N'uli201303281947580961')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1088, N'cti20121228134915123', N'2013-03-28 20:04:37', 1, N'113.106.60.17', N'uli201303282004370995')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1089, N'cti20121228134915123', N'2013-03-29 09:25:50', 1, N'127.0.0.1', N'uli201303290925500765')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1090, N'cti20121228134915123', N'2013-03-29 11:19:03', 1, N'113.106.60.17', N'uli201303291119030252')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1091, N'cti20121228134915123', N'2013-03-29 11:24:45', 1, N'113.106.60.17', N'uli201303291124450279')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1092, N'cti20121228134915123', N'2013-03-29 11:57:18', 1, N'113.106.60.17', N'uli201303291157180066')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1093, N'cti20121228134915123', N'2013-03-29 13:55:34', 1, N'113.106.60.17', N'uli201303291355340975')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1094, N'cti20121228134915123', N'2013-03-29 14:03:24', 1, N'113.106.60.17', N'uli201303291403240501')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1095, N'cti20121228134915123', N'2013-03-29 14:09:33', 1, N'113.106.60.17', N'uli201303291409330636')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1096, N'cti20121228134915123', N'2013-03-29 14:30:01', 1, N'113.106.60.17', N'uli201303291430010089')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1097, N'cti20121228134915123', N'2013-03-29 14:34:57', 1, N'113.106.60.17', N'uli201303291434570351')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1098, N'cti20121228134915123', N'2013-03-29 14:37:01', 1, N'113.106.60.17', N'uli201303291437010537')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1099, N'cti20121228134915123', N'2013-03-29 14:43:12', 1, N'113.106.60.17', N'uli201303291443120407')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1100, N'cti20121228134915123', N'2013-03-29 14:46:38', 1, N'113.106.60.17', N'uli201303291446380827')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1101, N'cti20121228134915123', N'2013-03-29 14:51:01', 1, N'113.106.60.17', N'uli201303291451010698')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1102, N'cti20121228134915123', N'2013-03-29 15:00:59', 1, N'113.106.60.17', N'uli201303291500590503')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1103, N'cti20121228134915123', N'2013-03-29 15:37:05', 1, N'113.106.60.17', N'uli201303291537050725')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1104, N'cti20121228134915123', N'2013-03-29 15:45:32', 1, N'113.106.60.17', N'uli201303291545320360')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1105, N'cti20121228134915123', N'2013-03-29 15:46:13', 1, N'113.106.60.17', N'uli201303291546130172')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1106, N'cui201303051600540968', N'2013-03-29 17:19:11', 1, N'127.0.0.1', N'uli201303291719110718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1107, N'cti20121228134915123', N'2013-03-29 17:58:05', 1, N'113.106.60.17', N'uli201303291758050269')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1108, N'cti20121228134915123', N'2013-03-30 10:50:35', 1, N'113.106.60.17', N'uli201303301050350449')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1109, N'cti20121228134915123', N'2013-03-30 10:56:02', 1, N'113.106.60.17', N'uli201303301056020384')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1110, N'cti20121228134915123', N'2013-03-30 10:56:38', 1, N'113.106.60.17', N'uli201303301056380712')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1111, N'cti20121228134915123', N'2013-03-30 11:06:01', 1, N'113.106.60.17', N'uli201303301106010224')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1112, N'cti20121228134915123', N'2013-03-30 11:08:54', 1, N'113.106.60.17', N'uli201303301108550035')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1113, N'cti20121228134915123', N'2013-03-30 11:10:08', 1, N'113.106.60.17', N'uli201303301110080816')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1114, N'sei201303101427190812', N'2013-03-30 11:11:10', 2, N'127.0.0.1', N'uli201303301111100781')
GO
print 'Processed 1100 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1115, N'cti20121228134915123', N'2013-03-30 11:12:02', 1, N'113.106.60.17', N'uli201303301112020472')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1116, N'cti20121228134915123', N'2013-03-30 11:15:22', 1, N'113.106.60.17', N'uli201303301115220236')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1117, N'cti20121228134915123', N'2013-03-30 11:31:17', 1, N'113.106.60.17', N'uli201303301131170199')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1118, N'cti20121228134915123', N'2013-03-30 11:31:46', 1, N'113.106.60.17', N'uli201303301131460245')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1119, N'cti20121228134915123', N'2013-03-30 11:34:07', 1, N'113.106.60.17', N'uli201303301134070604')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1120, N'cti20121228134915123', N'2013-03-30 11:43:14', 1, N'113.106.60.17', N'uli201303301143140116')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1121, N'cti20121228134915123', N'2013-03-30 11:43:49', 1, N'0:0:0:0:0:0:0:1', N'uli201303301143490676')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1122, N'cti20121228134915123', N'2013-03-30 11:45:46', 1, N'113.106.60.17', N'uli201303301145460709')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1123, N'cti20121228134915123', N'2013-03-30 11:58:52', 1, N'113.106.60.17', N'uli201303301158520454')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1124, N'cti20121228134915123', N'2013-03-30 12:04:34', 1, N'113.106.60.17', N'uli201303301204340217')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1125, N'cti20121228134915123', N'2013-03-30 13:49:07', 1, N'113.106.60.17', N'uli201303301349070370')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1126, N'cti20121228134915123', N'2013-03-30 13:50:20', 1, N'113.106.60.17', N'uli201303301350200339')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1127, N'cti20121228134915123', N'2013-03-30 13:50:53', 1, N'113.106.60.17', N'uli201303301350540026')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1128, N'cti20121228134915123', N'2013-03-30 13:55:32', 1, N'113.106.60.17', N'uli201303301355320180')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1129, N'cti20121228134915123', N'2013-03-30 13:58:08', 1, N'113.106.60.17', N'uli201303301358080054')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1130, N'cui201303051600540968', N'2013-03-30 14:31:25', 1, N'127.0.0.1', N'uli201303301431250838')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1131, N'cti20121228134915123', N'2013-03-30 14:47:54', 1, N'127.0.0.1', N'uli201303301447540265')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1132, N'cti20121228134915123', N'2013-03-30 14:58:42', 1, N'127.0.0.1', N'uli201303301458420500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1133, N'cti20121228134915123', N'2013-03-30 15:33:03', 1, N'113.106.60.17', N'uli201303301533030541')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1134, N'cti20121228134915123', N'2013-03-30 16:04:43', 1, N'113.106.60.17', N'uli201303301604430951')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1135, N'cti20121228134915123', N'2013-03-30 16:08:13', 1, N'113.106.60.17', N'uli201303301608130531')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1136, N'cti20121228134915123', N'2013-03-30 16:11:30', 1, N'113.106.60.17', N'uli201303301611300209')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1137, N'cti20121228134915123', N'2013-03-30 16:13:03', 1, N'113.106.60.17', N'uli201303301613040009')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1138, N'cti20121228134915123', N'2013-03-30 16:17:35', 1, N'113.106.60.17', N'uli201303301617350340')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1139, N'cti20121228134915123', N'2013-03-30 16:21:23', 1, N'113.106.60.17', N'uli201303301621230151')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1140, N'cti20121228134915123', N'2013-03-30 16:22:24', 1, N'113.106.60.17', N'uli201303301622240431')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1141, N'cti20121228134915123', N'2013-03-30 16:32:15', 1, N'113.106.60.17', N'uli201303301632150792')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1142, N'cti20121228134915123', N'2013-03-30 16:38:25', 1, N'113.106.60.17', N'uli201303301638250792')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1143, N'cui201303051600540968', N'2013-03-31 10:03:36', 1, N'127.0.0.1', N'uli201303311003360528')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1144, N'cui201303051600540968', N'2013-03-31 10:04:44', 1, N'127.0.0.1', N'uli201303311004440254')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1145, N'cui201303051600540968', N'2013-03-31 10:07:02', 1, N'127.0.0.1', N'uli201303311007020488')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1146, N'cui201303051600540968', N'2013-03-31 10:23:23', 1, N'127.0.0.1', N'uli201303311023230481')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1147, N'cui201303051600540968', N'2013-03-31 10:26:05', 1, N'127.0.0.1', N'uli201303311026050229')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1148, N'cui201303051600540968', N'2013-03-31 10:27:43', 1, N'127.0.0.1', N'uli201303311027430681')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1149, N'cui201303051600540968', N'2013-03-31 10:28:24', 1, N'127.0.0.1', N'uli201303311028240586')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1150, N'cui201303051600540968', N'2013-03-31 10:29:06', 1, N'127.0.0.1', N'uli201303311029060305')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1151, N'cui201303051600540968', N'2013-03-31 10:30:14', 1, N'127.0.0.1', N'uli201303311030140069')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1152, N'cui201303051600540968', N'2013-03-31 10:31:14', 1, N'127.0.0.1', N'uli201303311031140053')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1153, N'cui201303051600540968', N'2013-03-31 10:32:33', 1, N'127.0.0.1', N'uli201303311032330583')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1154, N'cui201303051600540968', N'2013-03-31 10:33:24', 1, N'127.0.0.1', N'uli201303311033240957')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1155, N'cui201303051600540968', N'2013-03-31 10:37:09', 1, N'127.0.0.1', N'uli201303311037090986')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1156, N'cui201303051600540968', N'2013-03-31 10:39:14', 1, N'127.0.0.1', N'uli201303311039140500')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1157, N'cui201303051600540968', N'2013-03-31 10:42:21', 1, N'127.0.0.1', N'uli201303311042210997')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1158, N'cui201303051600540968', N'2013-03-31 10:47:29', 1, N'127.0.0.1', N'uli201303311047290525')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1159, N'cti20121228134915123', N'2013-03-31 10:51:36', 1, N'0:0:0:0:0:0:0:1', N'uli201303311051360373')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1160, N'cti20121228134915123', N'2013-03-31 10:53:40', 1, N'127.0.0.1', N'uli201303311053400640')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1161, N'cti20121228134915123', N'2013-03-31 10:52:34', 1, N'0:0:0:0:0:0:0:1', N'uli201303311052340831')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1162, N'cui201303051600540968', N'2013-03-31 10:57:48', 1, N'127.0.0.1', N'uli201303311057480517')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1163, N'cti20121228134915123', N'2013-03-31 11:00:15', 1, N'127.0.0.1', N'uli201303311100150921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1164, N'cti20121228134915123', N'2013-03-31 10:59:18', 1, N'0:0:0:0:0:0:0:1', N'uli201303311059180621')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1165, N'cti20121228134915123', N'2013-03-31 11:00:15', 1, N'0:0:0:0:0:0:0:1', N'uli201303311100150343')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1166, N'cui201303051600540968', N'2013-03-31 11:03:06', 1, N'127.0.0.1', N'uli201303311103060857')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1167, N'cti20121228134915123', N'2013-03-31 11:03:18', 1, N'0:0:0:0:0:0:0:1', N'uli201303311103180970')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1168, N'cui201303051600540968', N'2013-03-31 11:05:51', 1, N'127.0.0.1', N'uli201303311105510433')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1169, N'cti20121228134915123', N'2013-03-31 11:07:57', 1, N'127.0.0.1', N'uli201303311107580009')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1170, N'cti20121228134915123', N'2013-03-31 11:15:59', 1, N'127.0.0.1', N'uli201303311115590937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1171, N'cui201303051600540968', N'2013-03-31 11:17:45', 1, N'127.0.0.1', N'uli201303311117450970')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1172, N'cti20121228134915123', N'2013-03-31 11:20:26', 1, N'127.0.0.1', N'uli201303311120260562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1173, N'sei201301161019460562', N'2013-03-31 11:23:42', 2, N'127.0.0.1', N'uli201303311123420966')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1174, N'cti20121228134915123', N'2013-03-31 11:25:21', 1, N'127.0.0.1', N'uli201303311125210046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1175, N'cti20121228134915123', N'2013-03-31 11:25:46', 1, N'127.0.0.1', N'uli201303311125460375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1176, N'cti20121228134915123', N'2013-03-31 11:31:12', 1, N'127.0.0.1', N'uli201303311131120609')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1177, N'cti20121228134915123', N'2013-03-31 11:34:48', 1, N'127.0.0.1', N'uli201303311134480859')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1178, N'cti20121228134915123', N'2013-03-31 11:59:18', 1, N'127.0.0.1', N'uli201303311159180218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1179, N'cti20121228134915123', N'2013-03-31 13:44:23', 1, N'127.0.0.1', N'uli201303311344240031')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1180, N'cti20121228134915123', N'2013-03-31 13:47:46', 1, N'127.0.0.1', N'uli201303311347460937')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1181, N'cti20121228134915123', N'2013-03-31 13:52:12', 1, N'0:0:0:0:0:0:0:1', N'uli201303311352140977')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1182, N'cui201303051600540968', N'2013-03-31 14:31:56', 1, N'127.0.0.1', N'uli201303311431560939')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1183, N'cti20121228134915123', N'2013-03-31 14:32:44', 1, N'127.0.0.1', N'uli201303311432440984')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1184, N'cti20121228134915123', N'2013-03-31 14:35:50', 1, N'127.0.0.1', N'uli201303311435500171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1185, N'cti20121228134915123', N'2013-03-31 14:55:13', 1, N'127.0.0.1', N'uli201303311455130593')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1186, N'cti20121228134915123', N'2013-03-31 15:03:51', 1, N'127.0.0.1', N'uli201303311503510046')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1187, N'cti20121228134915123', N'2013-03-31 15:04:50', 1, N'127.0.0.1', N'uli201303311504500468')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1188, N'cti20121228134915123', N'2013-03-31 15:15:12', 1, N'127.0.0.1', N'uli201303311515120843')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1189, N'cui201303051600540968', N'2013-03-31 15:14:05', 1, N'127.0.0.1', N'uli201303311514050565')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1190, N'cti20121228134915123', N'2013-03-31 15:16:52', 1, N'127.0.0.1', N'uli201303311516520734')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1191, N'cti20121228134915123', N'2013-03-31 15:21:03', 1, N'0:0:0:0:0:0:0:1', N'uli201303311521030431')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1192, N'cui201303051600540968', N'2013-03-31 15:23:40', 1, N'127.0.0.1', N'uli201303311523400480')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1193, N'cui201303051600540968', N'2013-03-31 15:24:35', 1, N'127.0.0.1', N'uli201303311524350776')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1194, N'cui201303051600540968', N'2013-03-31 15:27:49', 1, N'127.0.0.1', N'uli201303311527490898')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1195, N'cti20121228134915123', N'2013-03-31 15:31:53', 1, N'0:0:0:0:0:0:0:1', N'uli201303311531530916')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1196, N'cti20121228134915123', N'2013-03-31 15:32:25', 1, N'0:0:0:0:0:0:0:1', N'uli201303311532250150')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1197, N'cti20121228134915123', N'2013-03-31 15:33:08', 1, N'0:0:0:0:0:0:0:1', N'uli201303311533080693')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1198, N'cui201303051600540968', N'2013-03-31 15:33:39', 1, N'127.0.0.1', N'uli201303311533390363')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1199, N'cui201303051600540968', N'2013-03-31 15:34:41', 1, N'127.0.0.1', N'uli201303311534410987')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1200, N'cui201303051600540968', N'2013-03-31 15:39:07', 1, N'127.0.0.1', N'uli201303311539070452')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1201, N'cti20121228134915123', N'2013-03-31 16:25:31', 1, N'0:0:0:0:0:0:0:1', N'uli201303311625310389')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1202, N'cti20121228134915123', N'2013-03-31 16:50:01', 1, N'0:0:0:0:0:0:0:1', N'uli201303311650010680')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1203, N'cti20121228134915123', N'2013-03-31 16:52:04', 1, N'113.106.60.17', N'uli201303311652040232')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1204, N'cti20121228134915123', N'2013-03-31 16:54:30', 1, N'113.106.60.17', N'uli201303311654300520')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1205, N'cti20121228134915123', N'2013-03-31 16:59:47', 1, N'0:0:0:0:0:0:0:1', N'uli201303311659470932')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1206, N'cti20121228134915123', N'2013-03-31 16:59:39', 1, N'113.106.60.17', N'uli201303311659400024')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1207, N'cti20121228134915123', N'2013-03-31 17:04:00', 1, N'113.106.60.17', N'uli201303311704000674')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1208, N'cti20121228134915123', N'2013-03-31 17:06:04', 1, N'113.106.60.17', N'uli201303311706040953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1209, N'cti20121228134915123', N'2013-03-31 17:11:46', 1, N'0:0:0:0:0:0:0:1', N'uli201303311711460940')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1210, N'cti20121228134915123', N'2013-03-31 17:44:39', 1, N'0:0:0:0:0:0:0:1', N'uli201303311744390545')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1211, N'cti20121228134915123', N'2013-03-31 17:55:03', 1, N'0:0:0:0:0:0:0:1', N'uli201303311755030692')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1212, N'cti20121228134915123', N'2013-03-31 18:08:27', 1, N'0:0:0:0:0:0:0:1', N'uli201303311808270390')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1213, N'cti20121228134915123', N'2013-03-31 18:15:07', 1, N'0:0:0:0:0:0:0:1', N'uli201303311815070988')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1214, N'cti20121228134915123', N'2013-03-31 18:20:21', 1, N'127.0.0.1', N'uli201303311820220007')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1215, N'cti20121228134915123', N'2013-03-31 18:30:36', 1, N'0:0:0:0:0:0:0:1', N'uli201303311830360782')
GO
print 'Processed 1200 total records'
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1216, N'cti20121228134915123', N'2013-03-31 18:40:47', 1, N'0:0:0:0:0:0:0:1', N'uli201303311840470230')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1217, N'cti20121228134915123', N'2013-03-31 19:05:32', 1, N'0:0:0:0:0:0:0:1', N'uli201303311905320244')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1218, N'cti20121228134915123', N'2013-04-01 09:13:17', 1, N'113.106.60.17', N'uli201304010913170550')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1219, N'cti20121228134915123', N'2013-04-01 09:15:53', 1, N'113.106.60.17', N'uli201304010915530845')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1220, N'cti20121228134915123', N'2013-04-01 10:51:13', 1, N'113.106.60.17', N'uli201304011051140084')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1221, N'cti20121228134915123', N'2013-04-01 11:01:35', 1, N'113.106.60.17', N'uli201304011101350561')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1222, N'cti20121228134915123', N'2013-04-01 11:14:29', 1, N'113.106.60.17', N'uli201304011114300004')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1223, N'cti20121228134915123', N'2013-04-01 13:51:10', 1, N'127.0.0.1', N'uli201304011351100125')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1224, N'cti20121228134915123', N'2013-04-01 13:54:23', 1, N'127.0.0.1', N'uli201304011354230921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1225, N'cti20121228134915123', N'2013-04-01 14:08:59', 1, N'127.0.0.1', N'uli201304011408590359')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1226, N'cti20121228134915123', N'2013-04-01 14:27:16', 1, N'113.106.60.17', N'uli201304011427160590')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1227, N'cti20121228134915123', N'2013-04-01 14:37:02', 1, N'113.106.60.17', N'uli201304011437020317')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1228, N'cti20121228134915123', N'2013-04-01 14:40:49', 1, N'113.106.60.17', N'uli201304011440490189')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1229, N'cti20121228134915123', N'2013-04-01 14:42:45', 1, N'113.106.60.17', N'uli201304011442450313')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1230, N'cti20121228134915123', N'2013-04-01 14:55:16', 1, N'113.106.60.17', N'uli201304011455160975')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1231, N'cti20121228134915123', N'2013-04-01 15:13:25', 1, N'113.106.60.17', N'uli201304011513250664')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1232, N'cti20121228134915123', N'2013-04-01 15:14:33', 1, N'113.106.60.17', N'uli201304011514330585')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1233, N'cti20121228134915123', N'2013-04-01 15:32:27', 1, N'113.106.60.17', N'uli201304011532270156')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1234, N'cti20121228134915123', N'2013-04-01 16:34:04', 1, N'113.106.60.17', N'uli201304011634040188')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1235, N'cti20121228134915123', N'2013-04-01 16:49:00', 1, N'113.106.60.17', N'uli201304011649000177')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1236, N'cti20121228134915123', N'2013-04-01 16:58:50', 1, N'113.106.60.17', N'uli201304011658500482')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1237, N'cti20121228134915123', N'2013-04-01 16:59:43', 1, N'113.106.60.17', N'uli201304011659430137')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1238, N'cti20121228134915123', N'2013-04-01 17:03:02', 1, N'113.106.60.17', N'uli201304011703020557')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1239, N'cti20121228134915123', N'2013-04-01 17:03:48', 1, N'113.106.60.17', N'uli201304011703480337')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1240, N'cti20121228134915123', N'2013-04-01 17:12:44', 1, N'113.106.60.17', N'uli201304011712450018')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1241, N'cti20121228134915123', N'2013-04-01 17:15:29', 1, N'113.106.60.17', N'uli201304011715290391')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1242, N'cti20121228134915123', N'2013-04-01 17:20:40', 1, N'113.106.60.17', N'uli201304011720400121')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1243, N'cti20121228134915123', N'2013-04-01 17:22:22', 1, N'113.106.60.17', N'uli201304011722220651')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1244, N'cti20121228134915123', N'2013-04-01 17:28:11', 1, N'113.106.60.17', N'uli201304011728110319')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1245, N'cti20121228134915123', N'2013-04-01 17:56:44', 1, N'113.106.60.17', N'uli201304011756440547')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1246, N'cti20121228134915123', N'2013-04-01 18:04:15', 1, N'113.106.60.17', N'uli201304011804150619')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1247, N'cti20121228134915123', N'2013-04-01 18:09:10', 1, N'0:0:0:0:0:0:0:1', N'uli201304011809100622')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1248, N'cti20121228134915123', N'2013-04-01 18:13:10', 1, N'0:0:0:0:0:0:0:1', N'uli201304011813100637')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1249, N'sei201301161019460562', N'2013-04-01 18:13:58', 2, N'0:0:0:0:0:0:0:1', N'uli201304011813580233')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1250, N'sei201301161019460562', N'2013-04-01 18:58:13', 2, N'0:0:0:0:0:0:0:1', N'uli201304011858130724')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1251, N'cti20121228134915123', N'2013-04-01 19:05:19', 1, N'0:0:0:0:0:0:0:1', N'uli201304011905190767')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1252, N'cti20121228134915123', N'2013-04-01 19:56:05', 1, N'113.106.60.17', N'uli201304011956050361')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1253, N'cti20121228134915123', N'2013-04-01 20:02:45', 1, N'113.106.60.17', N'uli201304012002450109')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1254, N'cti20121228134915123', N'2013-04-01 20:04:00', 1, N'127.0.0.1', N'uli201304012004000574')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1255, N'cti20121228134915123', N'2013-04-01 20:11:57', 1, N'113.106.60.17', N'uli201304012011570777')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1256, N'cti20121228134915123', N'2013-04-01 20:14:38', 1, N'113.106.60.17', N'uli201304012014380388')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1257, N'cti20121228134915123', N'2013-04-01 20:42:02', 1, N'113.106.60.17', N'uli201304012042020425')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1258, N'cti20121228134915123', N'2013-04-01 20:43:52', 1, N'113.106.60.17', N'uli201304012043520876')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1259, N'cti20121228134915123', N'2013-04-01 20:46:28', 1, N'113.106.60.17', N'uli201304012046280327')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1260, N'cti20121228134915123', N'2013-04-01 20:52:41', 1, N'113.106.60.17', N'uli201304012052410823')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1261, N'cti20121228134915123', N'2013-04-01 20:53:47', 1, N'0:0:0:0:0:0:0:1', N'uli201304012053470202')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1262, N'cti20121228134915123', N'2013-04-01 20:58:40', 1, N'113.106.60.17', N'uli201304012058400943')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1263, N'sei201301161019460562', N'2013-04-01 20:59:35', 2, N'113.106.60.17', N'uli201304012059350317')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1264, N'cti20121228134915123', N'2013-04-01 21:03:16', 1, N'0:0:0:0:0:0:0:1', N'uli201304012103160334')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1265, N'cti20121228134915123', N'2013-04-01 21:07:46', 1, N'127.0.0.1', N'uli201304012107460234')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1266, N'cui201303051600540968', N'2013-04-02 07:47:12', 1, N'127.0.0.1', N'uli201304020747120203')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1267, N'sei201301161019460562', N'2013-04-02 08:32:34', 2, N'127.0.0.1', N'uli201304020832340171')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1268, N'cti20121228134915123', N'2013-04-02 08:41:10', 1, N'127.0.0.1', N'uli201304020841100265')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1269, N'cti20121228134915123', N'2013-04-02 09:30:16', 1, N'127.0.0.1', N'uli201304020930160953')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1270, N'cti20121228134915123', N'2013-04-02 09:37:25', 1, N'127.0.0.1', N'uli201304020937250093')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1271, N'cti20121228134915123', N'2013-04-02 09:43:32', 1, N'127.0.0.1', N'uli201304020943320375')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1272, N'cti20121228134915123', N'2013-04-02 10:18:21', 1, N'113.106.60.17', N'uli201304021018210772')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1273, N'cti20121228134915123', N'2013-04-02 11:19:12', 1, N'113.106.60.17', N'uli201304021119130240')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1274, N'cti20121228134915123', N'2013-04-02 11:27:11', 1, N'0:0:0:0:0:0:0:1', N'uli201304021127110519')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1275, N'sei201301161019460562', N'2013-04-02 11:44:50', 2, N'0:0:0:0:0:0:0:1', N'uli201304021144500570')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1276, N'cti20121228134915123', N'2013-04-02 11:47:43', 1, N'0:0:0:0:0:0:0:1', N'uli201304021147430167')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1277, N'sei201301161019460562', N'2013-04-02 12:03:12', 2, N'127.0.0.1', N'uli201304021203120453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1278, N'sei201301161019460562', N'2013-04-02 12:07:30', 2, N'0:0:0:0:0:0:0:1', N'uli201304021207300701')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1279, N'sei201301161019460562', N'2013-04-02 12:13:02', 2, N'127.0.0.1', N'uli201304021213020921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1280, N'cui201303051600540968', N'2013-04-02 12:17:31', 1, N'127.0.0.1', N'uli201304021217310218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1281, N'sei201301161019460562', N'2013-04-02 12:51:40', 2, N'127.0.0.1', N'uli201304021251400562')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1282, N'sei201301161019460562', N'2013-04-02 13:03:20', 2, N'127.0.0.1', N'uli201304021303200218')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1283, N'sei201301161019460562', N'2013-04-02 13:05:14', 2, N'127.0.0.1', N'uli201304021305140421')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1284, N'sei201301161019460562', N'2013-04-02 13:07:50', 2, N'127.0.0.1', N'uli201304021307500921')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1285, N'sei201301161019460562', N'2013-04-02 13:08:43', 2, N'127.0.0.1', N'uli201304021308430718')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1286, N'sei201301161019460562', N'2013-04-02 13:09:53', 2, N'127.0.0.1', N'uli201304021309530453')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1287, N'cui201303051600540968', N'2013-04-02 13:11:12', 1, N'127.0.0.1', N'uli201304021311120140')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1288, N'sei201301161019460562', N'2013-04-02 13:29:46', 2, N'0:0:0:0:0:0:0:1', N'uli201304021329460659')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1289, N'cti20121228134915123', N'2013-04-02 13:52:29', 1, N'113.106.60.17', N'uli201304021352290936')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1290, N'cti20121228134915123', N'2013-04-02 13:58:55', 1, N'113.106.60.17', N'uli201304021358550200')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1291, N'cti20121228134915123', N'2013-04-02 14:02:48', 1, N'113.106.60.17', N'uli201304021402480400')
INSERT [dbo].[DT_UserLoginInfo] ([id], [userCode], [loginTime], [userType], [loginIP], [code]) VALUES (1292, N'cti20121228134915123', N'2013-04-02 17:55:22', 1, N'127.0.0.1', N'uli201304021755220505')
SET IDENTITY_INSERT [dbo].[DT_UserLoginInfo] OFF
/****** Object:  Table [dbo].[DT_User_Message]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_User_Message](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[userType] [int] NULL,
	[userCode] [varchar](32) NOT NULL,
	[messageCode] [varchar](32) NOT NULL,
	[messageIsRead] [int] NULL,
 CONSTRAINT [PK_DT_User_Message] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_User_Message] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_User_Message] ON
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (9, N'umc201303171202550625', 1, N'cui201303051600540968', N'dm0006', 1)
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (10, N'umc201303171203010765', 1, N'cui201303051600540968', N'dm0002', 1)
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (11, N'umc201303182311280718', 2, N'sei201301161019460562', N'dm0005', 1)
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (12, N'umc201303250956150390', 2, N'sei201301161019460562', N'mes201303250955420953', 1)
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (13, N'umc201303271536580984', 1, N'cui201303051600540968', N'mes201303060213440975', 1)
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (14, N'umc201303301453570455', 1, N'cui201303051600540968', N'mes201303301447280285', 1)
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (15, N'umc201304012054580327', 1, N'cti20121228134915123', N'dm0003', 1)
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (16, N'umc201304012055220239', 1, N'cti20121228134915123', N'dm0001', 1)
INSERT [dbo].[DT_User_Message] ([id], [code], [userType], [userCode], [messageCode], [messageIsRead]) VALUES (17, N'umc201304012055490399', 1, N'cti20121228134915123', N'dm0004', 1)
SET IDENTITY_INSERT [dbo].[DT_User_Message] OFF
/****** Object:  Table [dbo].[DT_ToolDownload]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ToolDownload](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[toolName] [nvarchar](2000) NULL,
	[lastVersion] [varchar](250) NULL,
	[releaseDate] [varchar](250) NULL,
	[downloadPath] [varchar](250) NULL,
 CONSTRAINT [PK_DT_TOOLDOWNLOAD] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ToolDownload] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ToolDownload] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tld' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ToolDownload', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_ToolDownload] ON
INSERT [dbo].[DT_ToolDownload] ([id], [code], [toolName], [lastVersion], [releaseDate], [downloadPath]) VALUES (15, N'tdl201301220107150762', N'{"lag201301070020090542":"usa","lag201301070146010554":"DT909测速机"}', N'V11', N'2013-01-22', N'D:/11.txt')
INSERT [dbo].[DT_ToolDownload] ([id], [code], [toolName], [lastVersion], [releaseDate], [downloadPath]) VALUES (17, N'tdl201301222219450605', N'{"lag201301070020090542":"222"}', N'1111', N'2013-01-22', N'D:/11.txt')
INSERT [dbo].[DT_ToolDownload] ([id], [code], [toolName], [lastVersion], [releaseDate], [downloadPath]) VALUES (18, N'tdl201301261554430080', N'{"lag201301070146010554":"dfad"}', N'sdf', N'2013-01-26', N'sdf232')
INSERT [dbo].[DT_ToolDownload] ([id], [code], [toolName], [lastVersion], [releaseDate], [downloadPath]) VALUES (19, N'tdl201304012032110046', N'{"lag201301070146010554":"kj;;khj","lag201301070020090542":"爱爱爱"}', N'aaaa', N'2013-04-01', N'/iiio/uuu')
SET IDENTITY_INSERT [dbo].[DT_ToolDownload] OFF
/****** Object:  Table [dbo].[DT_Smtp]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_Smtp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[host] [varchar](50) NULL,
	[username] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[last_send_time] [bigint] NULL,
	[send_count] [int] NULL,
	[max_count] [int] NULL,
	[mail_from] [varchar](255) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_DT_SMTP] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_Smtp] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DT_SiteLanguage]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SiteLanguage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[stateCode] [varchar](32) NULL,
	[languageCode] [varchar](32) NULL,
	[resourceName] [varchar](250) NULL,
	[attUrl] [varchar](200) NULL,
 CONSTRAINT [PK_DT_SITELANGUAGE] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SiteLanguage] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_SiteLanguage] ON
INSERT [dbo].[DT_SiteLanguage] ([id], [name], [stateCode], [languageCode], [resourceName], [attUrl]) VALUES (7, N'中文', N'zh', N'CN', NULL, N'fs:/attachment/language/autel/autel_CN_zh.properties')
SET IDENTITY_INSERT [dbo].[DT_SiteLanguage] OFF
/****** Object:  Table [dbo].[DT_SaleInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SaleInfo](
	[code] [varchar](32) NOT NULL,
	[proExpand] [varchar](250) NULL,
	[company] [varchar](250) NULL,
	[salesNum] [int] NULL,
	[salesMoney] [numeric](18, 0) NULL,
	[pinBackNum] [int] NULL,
	[pinBackMoney] [numeric](18, 0) NULL,
	[netWeight] [int] NULL,
	[netSales] [numeric](18, 0) NULL,
	[sealerName] [varchar](250) NULL,
	[proName] [varchar](250) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_DT_SaleInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SaleInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SaleInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sIo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SaleInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_SaleInfo] ON
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'324324', N'sssss', N'apple', 30, CAST(30 AS Numeric(18, 0)), 11, CAST(20 AS Numeric(18, 0)), 19, CAST(10 AS Numeric(18, 0)), N'C-0003', N'dddd', 6)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sei201301172111050734', N'zczczc', N'zczczc', 400, CAST(221 AS Numeric(18, 0)), 22, CAST(211 AS Numeric(18, 0)), 378, CAST(10 AS Numeric(18, 0)), N'C-0001', N'sfsf', 9)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201301181124380484', N'xx3w', N'sssss', 1000, CAST(1000 AS Numeric(18, 0)), 50, CAST(50 AS Numeric(18, 0)), 950, CAST(950 AS Numeric(18, 0)), N'C-0003', N'贴纸', 10)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201301181125390625', N'ssssssss', N'aaaaaaa', 111, CAST(111 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 111, CAST(111 AS Numeric(18, 0)), N'C-0003', N'ggggggggg', 11)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201302021641580187', N'ss', N'ss', 1, CAST(3 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 1, CAST(3 AS Numeric(18, 0)), N'C-0003', N'ss', 13)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303031422030796', N'sdf', N's', 23, CAST(3454 AS Numeric(18, 0)), 34534, CAST(34534 AS Numeric(18, 0)), -34511, CAST(-31080 AS Numeric(18, 0)), N'C-0001@tom.com', N'df', 16)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071715190968', N'aaaaaaaa', N'aaaaaaaaaa', 1, CAST(1 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 1, CAST(1 AS Numeric(18, 0)), N'C-0003', N'aaaaaaa', 17)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071715520359', N'dfafd', N'afdf', 1, CAST(1 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 1, CAST(1 AS Numeric(18, 0)), N'C-0003', N'dafaf', 18)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071716230640', N'dfdf', N'dfdf', 1, CAST(1 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 1, CAST(1 AS Numeric(18, 0)), N'C-0003', N'fafa', 19)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071716360187', N'aada', N'adsds', 1, CAST(1 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 1, CAST(1 AS Numeric(18, 0)), N'C-0003', N'jhkjhjh', 20)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071716510718', N'erwrw', N'we', 11, CAST(11 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 11, CAST(11 AS Numeric(18, 0)), N'C-0003', N'yure', 21)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071717070062', N'afsdfa', N'yurt', 11, CAST(11 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 11, CAST(11 AS Numeric(18, 0)), N'C-0003', N'afsff', 22)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071717210234', N'ffafas', N'adad', 3, CAST(3 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 3, CAST(3 AS Numeric(18, 0)), N'C-0003', N'dfagfa', 23)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071717330343', N'fgasgw', N'ssdgsg', 3, CAST(3 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 3, CAST(3 AS Numeric(18, 0)), N'C-0003', N'dgsags', 24)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071717460781', N'fdaa', N'erqa', 2, CAST(2 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 2, CAST(2 AS Numeric(18, 0)), N'C-0003', N'astarf', 25)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071718010328', N'erar', N'arqae', 5, CAST(5 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 5, CAST(5 AS Numeric(18, 0)), N'C-0003', N'sywsts', 26)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071718130812', N'yudsfdsa', N'dgfsd', 4, CAST(4 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 4, CAST(4 AS Numeric(18, 0)), N'C-0003', N'yture', 27)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303071718290234', N'tretr', N'rer', 5, CAST(5 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 5, CAST(5 AS Numeric(18, 0)), N'C-0003', N'tytewr', 28)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201303261532270673', N'agggggggggg', N'aggggggggg', 1, CAST(1 AS Numeric(18, 0)), 0, CAST(0 AS Numeric(18, 0)), 1, CAST(1 AS Numeric(18, 0)), N'C-0003', N'aggggggrrrrrrrrrrrrrr', 35)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201304012150130528', N'product', N'件', 30, CAST(10000 AS Numeric(18, 0)), 1, CAST(100 AS Numeric(18, 0)), 29, CAST(9900 AS Numeric(18, 0)), N'李晶', N'product', 37)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201304012151160981', N'product2', N'件', 30, CAST(10000 AS Numeric(18, 0)), 1, CAST(100 AS Numeric(18, 0)), 29, CAST(9900 AS Numeric(18, 0)), N'李晶', N'product2', 38)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201304012154380369', N'product2', N'件', 30, CAST(10000 AS Numeric(18, 0)), 78, CAST(100 AS Numeric(18, 0)), -48, CAST(9900 AS Numeric(18, 0)), N'李晶', N'product2', 39)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201304012157060742', N'product2', N'件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件', 30, CAST(10000 AS Numeric(18, 0)), 78, CAST(100 AS Numeric(18, 0)), -48, CAST(9900 AS Numeric(18, 0)), N'李晶', N'product2', 40)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201304012157180976', N'product2product2product2product2product2product2product2product2product2product2', N'件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件', 30, CAST(10000 AS Numeric(18, 0)), 78, CAST(100 AS Numeric(18, 0)), -48, CAST(9900 AS Numeric(18, 0)), N'李晶', N'product2', 41)
INSERT [dbo].[DT_SaleInfo] ([code], [proExpand], [company], [salesNum], [salesMoney], [pinBackNum], [pinBackMoney], [netWeight], [netSales], [sealerName], [proName], [id]) VALUES (N'sai201304012157270507', N'product2product2product2product2product2product2product2product2product2product2', N'件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件件', 30, CAST(10000 AS Numeric(18, 0)), 78, CAST(100 AS Numeric(18, 0)), -48, CAST(9900 AS Numeric(18, 0)), N'李晶', N'product2product2product2product2product2', 42)
SET IDENTITY_INSERT [dbo].[DT_SaleInfo] OFF
/****** Object:  Table [dbo].[DT_SellerEmail]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SellerEmail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NOT NULL,
	[sellerEmail] [varchar](250) NOT NULL,
	[partner] [varchar](250) NOT NULL,
	[key] [varchar](250) NOT NULL,
	[payType] [int] NOT NULL,
	[defaultAccount] [int] NOT NULL,
 CONSTRAINT [PK_DT_SellerEmail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SellerEmail] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_SellerEmail] ON
INSERT [dbo].[DT_SellerEmail] ([id], [code], [sellerEmail], [partner], [key], [payType], [defaultAccount]) VALUES (1, N'sle2012012209480523', N'62261234561234', N'2088123456789123', N'123456', 0, 1)
INSERT [dbo].[DT_SellerEmail] ([id], [code], [sellerEmail], [partner], [key], [payType], [defaultAccount]) VALUES (2, N'sle2012012209492022', N'62261234561235', N'2088123456789125', N'123456', 0, 0)
SET IDENTITY_INSERT [dbo].[DT_SellerEmail] OFF
/****** Object:  Table [dbo].[DT_SealerType]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SealerType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
 CONSTRAINT [PK_DT_SEALERTYPE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SealerType] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SealerType] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'slp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SealerType', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'经销商类型表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SealerType'
GO
SET IDENTITY_INSERT [dbo].[DT_SealerType] ON
INSERT [dbo].[DT_SealerType] ([id], [code], [name]) VALUES (1, N'set20130105172215123', N'经销商')
INSERT [dbo].[DT_SealerType] ([id], [code], [name]) VALUES (4, N'set201301161650300359', N'海外销售')
INSERT [dbo].[DT_SealerType] ([id], [code], [name]) VALUES (26, N'set201304011618370859', N'saaaaa')
INSERT [dbo].[DT_SealerType] ([id], [code], [name]) VALUES (27, N'set201304011810060474', N'经销商类型1')
INSERT [dbo].[DT_SealerType] ([id], [code], [name]) VALUES (28, N'set201304011810390661', N'经销商类型1')
INSERT [dbo].[DT_SealerType] ([id], [code], [name]) VALUES (31, N'set201304011835240611', N'uhuih')
SET IDENTITY_INSERT [dbo].[DT_SealerType] OFF
/****** Object:  Table [dbo].[DT_SysUser]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SysUser](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[state] [int] NULL,
	[realname] [varchar](255) NULL,
	[userno] [varchar](255) NULL,
	[remark] [varchar](255) NULL,
	[dateline] [int] NULL,
 CONSTRAINT [PK_DT_SYSUSER] PRIMARY KEY NONCLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SysUser] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0禁用
   1开启' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SysUser', @level2type=N'COLUMN',@level2name=N'state'
GO
/****** Object:  Table [dbo].[DT_SysSetting]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SysSetting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[setValue] [varchar](1000) NULL,
 CONSTRAINT [PK_DT_SYSSETTING] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SysSetting] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SysSetting] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'stt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SysSetting', @level2type=N'COLUMN',@level2name=N'code'
GO
/****** Object:  Table [dbo].[es_user_role]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[es_user_role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[roleid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_user_role] DISABLE CHANGE_TRACKING
GO
SET IDENTITY_INSERT [dbo].[es_user_role] ON
INSERT [dbo].[es_user_role] ([id], [userid], [roleid]) VALUES (16, 2, 2)
INSERT [dbo].[es_user_role] ([id], [userid], [roleid]) VALUES (37, 9, 2)
INSERT [dbo].[es_user_role] ([id], [userid], [roleid]) VALUES (38, 3, 3)
INSERT [dbo].[es_user_role] ([id], [userid], [roleid]) VALUES (45, 1, 1)
INSERT [dbo].[es_user_role] ([id], [userid], [roleid]) VALUES (46, 1, 7)
INSERT [dbo].[es_user_role] ([id], [userid], [roleid]) VALUES (48, 4, 7)
SET IDENTITY_INSERT [dbo].[es_user_role] OFF
/****** Object:  Table [dbo].[es_themeuri]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_themeuri](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[themeid] [int] NULL,
	[uri] [varchar](255) NULL,
	[path] [varchar](255) NULL,
	[deleteflag] [int] NULL,
	[pagename] [varchar](255) NULL,
	[point] [int] NULL,
	[sitemaptype] [int] NULL,
	[keywords] [varchar](255) NULL,
	[description] [ntext] NULL,
	[httpcache] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_themeuri] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_themeuri] ON
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (1, NULL, N'/index.html', N'/index.html', 0, N'首页', 2, 1, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (2, NULL, N'/product-(.*).html', N'/product.html', 0, N'产品展示', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (3, NULL, N'/news-(\d+)-(\d+).html', N'/news/news.html', 0, N'最新动态', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (4, NULL, N'/detail-(.*).html', N'/news/detail.html', 0, N'详细页一', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (5, NULL, N'/search.html', N'/search.html', 0, N'搜索页', 1, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (6, NULL, N'/brand.html', N'/brand.html', 0, N'品牌营销', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (7, NULL, N'/cases-(\\d+)-(\\d+).html', N'/news.html', 0, N'成功案例', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (8, NULL, N'/article-(\\d+)-(\\d+).html', N'/article.html', 0, N'详细页二', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (9, NULL, N'/article2-(\\d+)-(\\d+).html', N'/article2.html', 0, N'详细页三', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (10, NULL, N'/guestbook.html', N'/guestbook.html', 0, N'留言板', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (11, NULL, N'/article1-(\\d+)-(\\d+).html', N'/article1.html', 0, N'详细页四', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (12, NULL, N'/job-(.*).html', N'/job.html', 0, N'人才招聘', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (13, NULL, N'/article3-(\\d+)-(\\d+).html', N'/article3.html', 0, N'详细页五', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (14, NULL, N'/about.html', N'/about.html', 0, N'关于我们', 1, 1, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (15, NULL, N'/solution-(\\d+)-(\\d+).html', N'/solution.html', 0, N'', 1, 0, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (16, NULL, N'/service.html', N'/service.html', 0, N'', 1, 1, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (17, NULL, N'/contact.html', N'/contact.html', 0, N'', 1, 1, NULL, NULL, 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (18, NULL, N'/test.html', N'/test.html', 0, N'测试页面', 0, 0, N'测试页面', N'测试页面', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (19, NULL, N'/account.html', N'/account.html', 0, N'我的账号', 0, 0, N'我的账号', N'我的账号', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (21, NULL, N'/product.html', N'/product.html', 0, N'我的产品', 0, 0, N'我的产品', N'我的产品', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (22, NULL, N'/myDistributorAccount-(.*).html', N'/myDistributorAccount.html', 0, N'我的账户（经销商）', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (23, NULL, N'/rentSoft-(.*).html', N'/rentSoft.html', 0, N'软件续租', 0, 0, N'软件续租', N'软件续租', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (25, NULL, N'/saleInfo-(.*).html', N'/saleInfo.html', 0, N'我的销售信息(经销商)', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (26, NULL, N'/toCustomerCharge-(.*).html', N'/toCustomerCharge.html', 0, N'给客户续租(经销商)', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (27, NULL, N'/customerComplaintInfo-(.*).html', N'/customerComplaintInfo.html', 0, N'我的客诉', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (28, NULL, N'/buySoft-(.*).html', N'/buySoft.html', 0, N'软件购买', 0, 0, N'软件购买', N'软件购买', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (29, NULL, N'/regCustomerInfo.html', N'/regCustomerInfo.html', 0, N'进入用户注册页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (30, NULL, N'/regCustomerInfoResult.html', N'/regCustomerInfoResult.html', 0, N'用户注册操作', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (31, NULL, N'/regProductResult.html', N'/regProductResult.html', 0, N'产品注册结果', 0, 0, N'产品注册结果', N'产品注册结果页面', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (32, NULL, N'/minSaleUnitMemo.html', N'/minSaleUnitMemo.html', 0, N'最小销售单位功能说明', 0, 0, N'最小销售单位功能说明', N'最小销售单位功能说明', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (33, NULL, N'/login.html', N'/login.html', 0, N'用户登陆页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (34, NULL, N'/loginCustomerInfoResult.html', N'/loginCustomerInfoResult.html', 0, N'用户登陆处理', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (35, NULL, N'/updateCustomerInfo.html', N'/updateCustomerInfo.html', 0, N'个人信息维护页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (36, NULL, N'/updateCustomerInfoResult.html', N'/updateCustomerInfoResult.html', 0, N'个人信息维护操作', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (37, NULL, N'/updatePasswordCustomer.html', N'/updatePasswordCustomer.html', 0, N'个人密码修改页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (38, NULL, N'/updatePasswordCustomerResult.html', N'/updatePasswordCustomerResult.html', 0, N'个人密码修改', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (39, NULL, N'/activeCustomerInfo.html', N'/activeCustomerInfo.html', 0, N'激活个人用户', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (40, NULL, N'/shoppingCart.html', N'/shoppingCart.html', 0, N'我的购物车', 0, 0, N'我的购物车', N'我的购物车', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (41, NULL, N'/goForgotPassword.html', N'/goForgotPassword.html', 0, N'找回密码页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (42, NULL, N'/selectForgotPassword.html', N'/selectForgotPassword.html', 0, N'密码方法选择页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (43, NULL, N'/selectMailForgotPassword.html', N'/selectMailForgotPassword.html', 0, N'找回密码选择发送邮件', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (44, NULL, N'/selectQuestionForgotPassword.html', N'/selectQuestionForgotPassword.html', 0, N'找回密码选择回答问题方式', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (45, NULL, N'/shoppingCart.html', N'/shoppingCart.html', 0, N'我的购物车', 0, 0, N'我的购物车', N'我的购物车', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (46, NULL, N'/selectQuestionForgotPasswordResult.html', N'/selectQuestionForgotPasswordResult.html', 0, N'找回密码方式选择回答问题操作', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (47, NULL, N'/updateSealerInfo.html', N'/updateSealerInfo.html', 0, N'进入个人信息页面-经销商', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (48, NULL, N'/updateSealerInfoResult.html', N'/updateSealerInfoResult.html', 0, N'个人信息修改操作-经销商', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (49, NULL, N'/updatePasswordSealer.html', N'/updatePasswordSealer.html', 0, N'进入个人密码修改页面-经销商', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (50, NULL, N'/updatePasswordSealerResult.html', N'/updatePasswordSealerResult.html', 0, N'密码修改操作-经销商', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (51, NULL, N'/deleteShoppingCartResult.html', N'/deleteShoppingCartResult.html', 0, N'删除我的购物车数据', 0, 0, N'删除我的购物车数据', N'删除我的购物车数据', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (52, NULL, N'/addCustomerComplaintInfo.html', N'/addCustomerComplaintInfo.html', 0, N'进入添加客诉页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (53, NULL, N'/pay.html', N'/pay.html', 0, N'支付', 0, 0, N'支付', N'支付', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (54, NULL, N'/queryCustomerComplaintDetail.html', N'/queryCustomerComplaintDetail.html', 0, N'查看客诉详情', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (55, NULL, N'/minSaleUnitMemo.html', N'/minSaleUnitMemo.html', 0, N'最小销售单位说明', 0, 0, N'最小销售单位说明', N'最小销售单位说明', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (56, NULL, N'/myMessageInfo-(.*).html', N'/myMessageInfo.html', 0, N'我的消息', 0, 0, N'我的消息', N'我的消息', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (57, NULL, N'/querySealerInfoList-(.*).html', N'/querySealerInfoList.html', 0, N'查看经销商列表', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (58, NULL, N'/queryProductToolsList.html', N'/queryProductToolsList.html', 0, N'工具下载', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (59, NULL, N'/myOrderInfo-(.*).html', N'/myOrderInfo.html', 0, N'我的订单', 0, 0, N'我的订单', N'我的订单', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (60, NULL, N'/replyCustomerComplaint.html', N'/replyCustomerComplaint.html', 0, N'客诉回复', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (61, NULL, N'/downLoadProductTools.html', N'/downLoadProductTools.html', 0, N'工具下载操作', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (62, NULL, N'/orderInfoPage.html', N'/orderInfoPage.html', 0, N'我的订单', 0, 0, N'我的订单', N'我的订单', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (63, NULL, N'/addCustomerComplaint.html', N'/addCustomerComplaint.html', 0, N'增加客诉', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (64, NULL, N'/resetPassword.html', N'/resetPassword.html', 0, N'进入密码重置操作页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (65, NULL, N'/resetPasswordResult.html', N'/resetPasswordResult.html', 0, N'密码重置操作', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (66, NULL, N'/myAccount.html', N'/myAccount.html', 0, N'我的账号', 0, 0, N'我的账号', N'个人中心-我的账号', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (67, NULL, N'/myProducts-(.*).html', N'/myProducts.html', 0, N'我的产品', 0, 0, N'我的产品', N'个人客户我的产品', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (68, NULL, N'/buySoftWare-(.*).html', N'/buySoftWare.html', 0, N'软件购买', 0, 0, N'软件购买', N'软件购买', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (69, NULL, N'/customerComplaintTypeInfo.html', N'/customerComplaintTypeInfo.html', 0, N'客诉分类页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (70, NULL, N'/rentSoftware-(.*).html', N'/rentSoftware.html', 0, N'软件续租', 0, 0, N'软件续租', N'软件续租', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (71, NULL, N'/myOrders-(.*).html', N'/myOrders.html', 0, N'我的订单', 0, 0, N'我的订单', N'我的订单', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (72, NULL, N'/orderDetailInfo.html', N'/orderDetailInfo.html', 0, N'订单详情', 0, 0, N'订单详情', N'订单详情', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (73, NULL, N'/cart.html', N'/cart.html', 0, N'购物车', 0, 0, N'购物车', N'购物车', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (74, NULL, N'/onlinePay.html', N'/onlinePay.html', 0, N'在线支付', 0, 0, N'在线支付', N'在线支付', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (75, NULL, N'/notify_url.html', N'/notify_url.html', 0, N'支付异步返回页面', 0, 0, N'支付异步返回页面', N'支付异步返回页面', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (76, NULL, N'/return_url.html', N'/return_url.html', 0, N'支付同步返回页面', 0, 0, N'支付同步返回页面', N'支付同步返回页面', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (77, NULL, N'/alipayapi.html', N'/alipayapi.html', 0, N'支付宝即时到账交易接口', 0, 0, N'支付宝即时到账交易接口', N'支付宝即时到账交易接口', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (78, NULL, N'/regProduct.html', N'/regProduct.html', 0, N'注册产品', 0, 0, N'注册产品', N'注册产品', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (82, NULL, N'/loginOut.html', N'/loginOut.html', 0, N'退出系统页面', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (83, NULL, N'/queryCustomerComplaintTableDetail.html', N'/queryCustomerComplaintTableDetail.html', 0, N'客诉表格详情', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (84, NULL, N'/myVendProduct-(.*).html', N'/myVendProduct.html', 0, N'经销商中心 ——我的可卖产品 ', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (85, NULL, N'/myPayDesc.html', N'/myPayDesc.html', 0, N'我的支付说明', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (86, NULL, N'/literature-(\d+)-(\d+).html', N'/news/literature.html', 0, N'资料中心', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (87, NULL, N'/litera-(.*).html', N'/news/litera.html', 0, N'资料详情', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (88, NULL, N'/service-(\d+)-(\d+).html', N'/news/service.html', 0, N'客服服务', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (89, NULL, N'/serdetail-(.*).html', N'/news/serdetail.html', 0, N'客服问题答', 0, 0, N'', N'', 0)
INSERT [dbo].[es_themeuri] ([id], [themeid], [uri], [path], [deleteflag], [pagename], [point], [sitemaptype], [keywords], [description], [httpcache]) VALUES (90, NULL, N'/otherPay.html', N'/otherPay.html', 0, N'其他支付', 0, 0, N'其他支付', N'其他支付', 0)
SET IDENTITY_INSERT [dbo].[es_themeuri] OFF
/****** Object:  Table [dbo].[es_theme]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_theme](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[appid] [varchar](50) NULL,
	[themename] [varchar](50) NULL,
	[path] [varchar](255) NULL,
	[author] [varchar](50) NULL,
	[version] [varchar](50) NULL,
	[deleteflag] [int] NULL,
	[thumb] [varchar](50) NULL,
	[siteid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_theme] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_theme] ON
INSERT [dbo].[es_theme] ([id], [appid], [themename], [path], [author], [version], [deleteflag], [thumb], [siteid]) VALUES (1, NULL, N'蓝色经典', N'001', NULL, NULL, 0, N'preview.png', 0)
INSERT [dbo].[es_theme] ([id], [appid], [themename], [path], [author], [version], [deleteflag], [thumb], [siteid]) VALUES (2, NULL, N'红色激情', N'002', NULL, NULL, 0, N'preview.png', 0)
INSERT [dbo].[es_theme] ([id], [appid], [themename], [path], [author], [version], [deleteflag], [thumb], [siteid]) VALUES (3, NULL, N'绿色情怡', N'003', NULL, NULL, 0, N'preview.png', 0)
INSERT [dbo].[es_theme] ([id], [appid], [themename], [path], [author], [version], [deleteflag], [thumb], [siteid]) VALUES (4, NULL, N'橙色浪漫', N'004', NULL, NULL, 0, N'preview.png', 0)
INSERT [dbo].[es_theme] ([id], [appid], [themename], [path], [author], [version], [deleteflag], [thumb], [siteid]) VALUES (5, NULL, N'蓝色简洁', N'005', NULL, NULL, 0, N'preview.png', 0)
INSERT [dbo].[es_theme] ([id], [appid], [themename], [path], [author], [version], [deleteflag], [thumb], [siteid]) VALUES (6, NULL, N'粉色柔情', N'006', NULL, NULL, 0, N'preview.png', 0)
INSERT [dbo].[es_theme] ([id], [appid], [themename], [path], [author], [version], [deleteflag], [thumb], [siteid]) VALUES (7, NULL, N'autel', N'autel', N'autel', NULL, 0, N'preview.png', 0)
SET IDENTITY_INSERT [dbo].[es_theme] OFF
/****** Object:  Table [dbo].[es_smtp]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_smtp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[host] [varchar](50) NULL,
	[username] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[last_send_time] [bigint] NULL,
	[send_count] [int] NULL,
	[max_count] [int] NULL,
	[mail_from] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_smtp] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_smtp] ON
INSERT [dbo].[es_smtp] ([id], [host], [username], [password], [last_send_time], [send_count], [max_count], [mail_from]) VALUES (1, N'smtp.exmail.qq.com', N'service1@javashop.cn', N'enation752513', 1364539277, 2, 9999, N'service1@javashop.cn')
INSERT [dbo].[es_smtp] ([id], [host], [username], [password], [last_send_time], [send_count], [max_count], [mail_from]) VALUES (2, N'sdf', N'sd', N'sdf', 0, 0, 1, N'dsf')
INSERT [dbo].[es_smtp] ([id], [host], [username], [password], [last_send_time], [send_count], [max_count], [mail_from]) VALUES (4, N'wwwwwwwwww', N'wwwwwwwwwwwwwgggggggdddddddddeeeeeeeeeaaaaaaaaa', N'sdfggfhh', 0, 0, 9999, N'wewew')
INSERT [dbo].[es_smtp] ([id], [host], [username], [password], [last_send_time], [send_count], [max_count], [mail_from]) VALUES (16, N'asdfqvqwerq', N'qwervqewr', N'qwer', 0, 0, 9999, N'qwer')
SET IDENTITY_INSERT [dbo].[es_smtp] OFF
/****** Object:  Table [dbo].[es_site_menu]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_site_menu](
	[menuid] [int] IDENTITY(1,1) NOT NULL,
	[parentid] [int] NULL,
	[name] [varchar](50) NULL,
	[url] [varchar](255) NULL,
	[target] [varchar](255) NULL,
	[sort] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[menuid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_site_menu] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_site_menu] ON
INSERT [dbo].[es_site_menu] ([menuid], [parentid], [name], [url], [target], [sort]) VALUES (1, 0, N'Home', N'myAccount.html?userType=1', N'', 0)
INSERT [dbo].[es_site_menu] ([menuid], [parentid], [name], [url], [target], [sort]) VALUES (2, 0, N'My Account', N'myAccount.html?userType=1', N'', 1)
INSERT [dbo].[es_site_menu] ([menuid], [parentid], [name], [url], [target], [sort]) VALUES (3, 0, N'News', N'news-27-1.html', N'', 2)
INSERT [dbo].[es_site_menu] ([menuid], [parentid], [name], [url], [target], [sort]) VALUES (5, 0, N'Service', N'service-29-1.html', N'', 4)
INSERT [dbo].[es_site_menu] ([menuid], [parentid], [name], [url], [target], [sort]) VALUES (6, 0, N'Literature', N'literature-28-1.html', N'', 3)
SET IDENTITY_INSERT [dbo].[es_site_menu] OFF
/****** Object:  Table [dbo].[es_site]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_site](
	[siteid] [int] IDENTITY(1,1) NOT NULL,
	[parentid] [int] NULL,
	[code] [int] NULL,
	[name] [varchar](255) NULL,
	[domain] [varchar](255) NULL,
	[themeid] [int] NULL,
	[sitelevel] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[siteid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_site] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[es_settings]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_settings](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[cfg_value] [varchar](1000) NULL,
	[cfg_group] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_settings] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_settings] ON
INSERT [dbo].[es_settings] ([id], [code], [cfg_value], [cfg_group]) VALUES (1, NULL, N'{"detail_pic_height":"260","thumbnail_pic_height":"75","album_pic_height":"600","thumbnail_pic_width":"100","album_pic_width":"800","detail_pic_width":"347"}', N'photo')
INSERT [dbo].[es_settings] ([id], [code], [cfg_value], [cfg_group]) VALUES (2, NULL, N'{"anonymous":"0","validcode":"1","pageSize":"5","directShow":"0"}', N'comments')
INSERT [dbo].[es_settings] ([id], [code], [cfg_value], [cfg_group]) VALUES (3, NULL, N'{"state":"open"}', N'widgetCache')
SET IDENTITY_INSERT [dbo].[es_settings] OFF
/****** Object:  Table [dbo].[es_role_auth]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[es_role_auth](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[roleid] [int] NULL,
	[authid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_role_auth] DISABLE CHANGE_TRACKING
GO
SET IDENTITY_INSERT [dbo].[es_role_auth] ON
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (20, 6, 7)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (21, 6, 8)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (26, 3, 3)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (27, 3, 5)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (28, 3, 6)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (29, 3, 7)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (30, 3, 8)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (31, 3, 9)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (40, 2, 9)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (41, 2, 11)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (63, 1, 1)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (64, 1, 13)
INSERT [dbo].[es_role_auth] ([id], [roleid], [authid]) VALUES (65, 7, 13)
SET IDENTITY_INSERT [dbo].[es_role_auth] OFF
/****** Object:  Table [dbo].[es_role]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_role](
	[roleid] [int] IDENTITY(1,1) NOT NULL,
	[rolename] [varchar](255) NULL,
	[rolememo] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[roleid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_role] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_role] ON
INSERT [dbo].[es_role] ([roleid], [rolename], [rolememo]) VALUES (1, N'编辑', N'编辑')
INSERT [dbo].[es_role] ([roleid], [rolename], [rolememo]) VALUES (2, N'客服', N'客服主要管理客诉')
INSERT [dbo].[es_role] ([roleid], [rolename], [rolememo]) VALUES (3, N'角色-测试', N'测试用')
INSERT [dbo].[es_role] ([roleid], [rolename], [rolememo]) VALUES (6, N'角色0001', N'角色0001')
INSERT [dbo].[es_role] ([roleid], [rolename], [rolememo]) VALUES (7, N'管理员', N'管理员')
SET IDENTITY_INSERT [dbo].[es_role] OFF
/****** Object:  Table [dbo].[es_regions]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_regions](
	[region_id] [int] IDENTITY(1,1) NOT NULL,
	[p_region_id] [int] NULL,
	[region_path] [varchar](255) NULL,
	[region_grade] [int] NULL,
	[local_name] [varchar](100) NOT NULL,
	[zipcode] [varchar](20) NULL,
	[cod] [varchar](4) NULL,
PRIMARY KEY CLUSTERED 
(
	[region_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_regions] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_regions] ON
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (1, 0, N',1,', 1, N'北京', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (2, 0, N',2,', 1, N'上海', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (3, 1, N',1,33,', 2, N'密云县', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (4, 1, N',1,34,', 2, N'三河', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (5, 1, N',1,35,', 2, N'海淀区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (6, 1, N',1,36,', 2, N'丰台区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (7, 1, N',1,37,', 2, N'大兴区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (8, 1, N',1,38,', 2, N'通州区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (9, 1, N',1,39,', 2, N'平谷区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (10, 1, N',1,40,', 2, N'怀柔区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (11, 1, N',1,41,', 2, N'东城区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (12, 1, N',1,42,', 2, N'崇文区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (13, 1, N',1,43,', 2, N'石景山区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (14, 1, N',1,44,', 2, N'宣武区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (15, 1, N',1,45,', 2, N'延庆县', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (16, 1, N',1,46,', 2, N'昌平区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (17, 1, N',1,47,', 2, N'门头沟区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (18, 1, N',1,48,', 2, N'西城区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (19, 1, N',1,49,', 2, N'房山区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (20, 1, N',1,50,', 2, N'朝阳区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (21, 1, N',1,51,', 2, N'顺义区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (22, 2, N',2,52,', 2, N'黄浦区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (23, 2, N',2,53,', 2, N'卢湾区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (24, 2, N',2,54,', 2, N'徐汇区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (25, 2, N',2,55,', 2, N'长宁区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (26, 2, N',2,56,', 2, N'静安区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (27, 2, N',2,57,', 2, N'普陀区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (28, 2, N',2,58,', 2, N'闸北区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (29, 2, N',2,59,', 2, N'虹口区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (30, 2, N',2,60,', 2, N'杨浦区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (31, 2, N',2,61,', 2, N'闵行区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (32, 2, N',2,62,', 2, N'宝山区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (33, 2, N',2,63,', 2, N'嘉定区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (34, 2, N',2,64,', 2, N'浦东新区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (35, 2, N',2,65,', 2, N'金山区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (36, 2, N',2,66,', 2, N'松江区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (37, 2, N',2,67,', 2, N'青浦区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (38, 2, N',2,68,', 2, N'南汇区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (39, 2, N',2,69,', 2, N'奉贤区', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (40, 2, N',2,70,', 2, N'崇明县', N'', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (41, 33, N',1,33,451,', 3, N'密云县', N'101505', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (42, 34, N',1,34,452,', 3, N'燕郊', N'101601', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (43, 35, N',1,35,453,', 3, N'海淀区', N'100093', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (44, 36, N',1,36,454,', 3, N'丰台区', N'100072', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (45, 37, N',1,37,455,', 3, N'大兴区', N'100076', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (46, 38, N',1,38,456,', 3, N'通州区', N'101100', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (47, 39, N',1,39,457,', 3, N'平谷区', N'101205', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (48, 40, N',1,40,458,', 3, N'怀柔区', N'101402', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (49, 41, N',1,41,459,', 3, N'东城区', N'100704', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (50, 42, N',1,42,460,', 3, N'崇文区', N'100077', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (51, 43, N',1,43,461,', 3, N'石景山区', N'100043', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (52, 44, N',1,44,462,', 3, N'宣武区', N'100050', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (53, 45, N',1,45,463,', 3, N'延庆县', N'102104', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (54, 46, N',1,46,464,', 3, N'昌平区', N'102200', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (55, 47, N',1,47,465,', 3, N'门头沟区', N'102301', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (56, 48, N',1,48,466,', 3, N'西城区', N'100044', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (57, 49, N',1,49,467,', 3, N'房山区', N'102503', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (58, 50, N',1,50,468,', 3, N'朝阳区', N'100021', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (59, 51, N',1,51,469,', 3, N'顺义区', N'101307', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (60, 52, N',2,52,470,', 3, N'黄浦区', N'200001', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (61, 53, N',2,53,471,', 3, N'卢湾区', N'200023', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (62, 54, N',2,54,472,', 3, N'徐汇区', N'200030', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (63, 55, N',2,55,473,', 3, N'长宁区', N'200050', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (64, 56, N',2,56,474,', 3, N'静安区', N'200041', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (65, 57, N',2,57,475,', 3, N'普陀区', N'200062', N'0')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (66, 58, N',2,58,476,', 3, N'闸北区', N'200070', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (67, 59, N',2,59,477,', 3, N'虹口区', N'200085', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (68, 60, N',2,60,478,', 3, N'杨浦区', N'200090', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (69, 61, N',2,61,479,', 3, N'闵行区', N'200240', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (70, 62, N',2,62,480,', 3, N'宝山区', N'200940', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (71, 63, N',2,63,481,', 3, N'嘉定区', N'201822', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (72, 64, N',2,64,482,', 3, N'浦东新区', N'200122', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (73, 65, N',2,65,483,', 3, N'金山区', N'200540', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (74, 66, N',2,66,484,', 3, N'松江区', N'201600', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (75, 67, N',2,67,485,', 3, N'青浦区', N'201700', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (76, 68, N',2,68,486,', 3, N'南汇区', N'201313', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (77, 69, N',2,69,487,', 3, N'奉贤区', N'201400', N'1')
INSERT [dbo].[es_regions] ([region_id], [p_region_id], [region_path], [region_grade], [local_name], [zipcode], [cod]) VALUES (78, 70, N',2,70,488,', 3, N'崇明县', N'202150', N'1')
SET IDENTITY_INSERT [dbo].[es_regions] OFF
/****** Object:  Table [dbo].[es_menu]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_menu](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[appid] [varchar](50) NULL,
	[pid] [int] NULL,
	[title] [varchar](50) NULL,
	[url] [varchar](255) NULL,
	[target] [varchar](255) NULL,
	[sorder] [int] NULL,
	[menutype] [int] NULL,
	[datatype] [varchar](50) NULL,
	[selected] [int] NULL,
	[deleteflag] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_menu] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_menu] ON
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (1, N'', 0, N'内容管理', N'/cms/admin/cat!listChild.do?parentId=11', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (10, N'', 1, N'全面管理', N'#', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (11, N'', 10, N'文章分类', N'/cms/admin/cat!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (12, N'', 10, N'导航栏目', N'/core/admin/siteMenu!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (20, N'', 1, N'广告管理', N'#', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (21, N'', 20, N'广告列表', N'/core/admin/adv!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (22, N'', 20, N'广告位置', N'/core/admin/adColumn!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (23, N'', 0, N'系统设置', N'/core/admin/product-info!list.do', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (24, N'', 23, N'设置', N'#', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (25, N'', 24, N'文章管理', N'/cms/admin/cat!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (28, N'', 23, N'权限管理', N'', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (29, N'', 28, N'管理员管理', N'/core/admin/user/userAdmin.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (30, N'', 28, N'角色管理', N'/core/admin/role!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (32, N'', 24, N'数据初始化', N'/core/admin/user/userSite!toInitData.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (36, N'', 0, N'浏览网站', N'../', N'_blank', 100, 1, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (37, N'', 0, N'退出', N'/admin/logout.do', N'_self', 50, 1, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (38, NULL, 23, N'工具', N'#', NULL, 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (39, NULL, 38, N'菜单管理', N'/core/admin/menu!list.do', NULL, 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (40, NULL, 38, N'URL映射', N'/core/admin/themeUri!list.do', NULL, 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (42, NULL, 10, N'友情链接', N'/core/admin/friendsLink!list.do', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (43, NULL, 44, N'smtp设置', N'/core/admin/smtp!list.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (44, NULL, 23, N'其他配置', N'#', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (45, NULL, 44, N'短信服务配置', N'/core/admin/phone-message-cfg!list.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (46, NULL, 44, N'网站语言', N'/core/admin/site-language!list.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (47, NULL, 23, N'语言配置', N'#', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (48, NULL, 47, N'基本语言设置', N'/core/admin/language!list.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (49, NULL, 47, N'语言配置', N'/core/admin/language-config!list.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (50, NULL, 0, N'产品管理', N'/autel/product/toProductTree.do', N'', 70, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (51, NULL, 50, N'产品管理', N'#', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (52, NULL, 51, N'产品型号', N'/autel/product/toProductTree.do', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (53, NULL, 23, N'区域配置', N'#', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (54, NULL, 53, N'基本区域设置', N'/core/admin/area-info!list.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (55, NULL, 53, N'区域配置', N'/core/admin/area-config!list.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (57, NULL, 0, N'会员管理', N'	/core/admin/question-info!list.do', N'', 60, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (58, NULL, 57, N'客户管理', N'#', N'', 30, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (59, NULL, 58, N'客户安全问题管理', N'/core/admin/question-info!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (60, NULL, 23, N'出厂产品管理', N'#', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (61, NULL, 60, N'出厂产品信息', N'/core/admin/product-info!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (62, NULL, 60, N'硬件维修记录', N'/core/admin/product-repair-record!list.do', N'', 60, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (63, NULL, 58, N'软件有效期', N'/core/admin/product-software-valid-status!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (64, NULL, 1, N'CMS', N'#', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (65, NULL, 1, N'消息管理', N'#', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (66, NULL, 65, N'消息分类', N'/core/admin/message-type!list.do', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (67, NULL, 65, N'站内消息', N'/core/admin/message!list.do', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (68, NULL, 65, N'短信模板', N'/core/admin/phone-message-template!list.do', N'', 30, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (69, NULL, 65, N'邮件模板', N'/core/admin/email-template!list.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (70, NULL, 64, N'客服中心', N'/cms/admin/cat!listChild.do?parentId=11', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (71, NULL, 64, N'新闻中心', N'/cms/admin/cat!listChild.do?parentId=2', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (72, NULL, 64, N'资料中心', N'/cms/admin/cat!listChild.do?parentId=10', N'', 30, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (73, NULL, 57, N'经销商管理', N'#', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (74, NULL, 73, N'经销商信息管理', N'/autel/member/listsealer.do', N'', 30, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (75, NULL, 0, N'营销管理', N'/autel/market/toMinSaleUnitPage.do', N'', 60, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (76, NULL, 75, N'最小销售单位', N'/autel/market/toMinSaleUnitPage.do', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (78, NULL, 58, N'客户信息管理', N'/autel/member/listcustomer.do', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (79, NULL, 58, N'客户注册产品信息', N'/autel/member/listcustomerpro.do', N'', 30, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (80, NULL, 58, N'客户消费信息', N'/core/admin/product-software-valid-status!listCustomerOrderInfo.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (81, NULL, 23, N'支付设置', N'#', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (82, NULL, 81, N'支付配置', N'/core/admin/payment-cfg!list.do', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (83, NULL, 73, N'经销商类型管理', N'/autel/member/listsealertype.do', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (84, NULL, 58, N'客户产品升级信息', N'/autel/member/listproupgrade.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (85, NULL, 58, N'客户登录信息查询', N'/autel/member/listcustomerlogininfo.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (86, NULL, 75, N'销售配置', N'/autel/market/toSaleConfig.do', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (87, NULL, 73, N'经销商销售信息管理', N'/autel/member/listsaleinfo.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (88, NULL, 86, N'销售契约管理', N'/autel/market/toSaleContractList.do', N'', 6, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (89, NULL, 57, N'客诉管理', N'#', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (90, NULL, 89, N'客诉跟踪', N'/autel/member/listcustomercomplaint.do', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (91, NULL, 75, N'营销活动管理', N'#', N'', 55, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (92, NULL, 91, N'限时抢购', N'/core/admin/toMarkerPromotion.do?search.promotionType=1', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (93, NULL, 23, N'工具管理', N'#', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (94, NULL, 93, N'下载工具', N'/core/admin/tool-download!list.do', N'', 50, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (95, NULL, 89, N'测试包管理', N'/autel/member/listtestpack.do', N'', 30, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (96, NULL, 0, N'交易管理', N'/autel/trade/listorderinfo.do', N'', 60, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (97, NULL, 96, N'日志管理', N'#', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (98, NULL, 97, N'付款日志', N'/autel/trade/listorderinfo.do', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (99, NULL, 58, N'客户软件信息查询', N'/autel/member/listcustomersoft.do', N'', 40, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (100, NULL, 91, N'套餐优惠', N'/core/admin/toMarkerPromotion.do?search.promotionType=3', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (101, NULL, 76, N'最小销售单位管理', N'/autel/market/toMinSaleUnitPage.do', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (102, NULL, 86, N'销售配置管理', N'/autel/market/toSaleConfig.do', N'', 5, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (103, NULL, 91, N'越早越便宜', N'/core/admin/toMarkerPromotion.do?search.promotionType=2', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (104, NULL, 91, N'过期续租', N'/core/admin/toMarkerPromotion.do?search.promotionType=4', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (105, NULL, 97, N'充值卡使用日志', N'/autel/trade/listrechargerecord.do', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (106, NULL, 96, N'支付卡管理', N'#', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (107, NULL, 106, N'充值卡类型管理', N'/autel/trade/listrechargecardtype.do', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (108, NULL, 0, N'订单管理', N'/core/admin/toOrderInfoList.do', N'', 55, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (109, NULL, 110, N'订单列表', N'/core/admin/toOrderInfoList.do', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (110, NULL, 108, N'订单管理', N'#', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (111, NULL, 106, N'充值卡信息管理', N'/autel/trade/listrechargecardinfo.do', N'', 10, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (112, NULL, 110, N'订单确认', N'/core/admin/toOrderConfirmList.do', N'', 0, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (116, NULL, 89, N'客诉权限管理', N'/autel/member/listcomplaintrole.do', N'', 20, 2, NULL, 0, 0)
INSERT [dbo].[es_menu] ([id], [appid], [pid], [title], [url], [target], [sorder], [menutype], [datatype], [selected], [deleteflag]) VALUES (117, NULL, 89, N'客诉来源管理', N'/autel/member/listcomplaintsource.do', N'', 5, 2, NULL, 0, 0)
SET IDENTITY_INSERT [dbo].[es_menu] OFF
/****** Object:  Table [dbo].[es_index_item]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_index_item](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NULL,
	[url] [varchar](255) NULL,
	[sort] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_index_item] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_index_item] ON
INSERT [dbo].[es_index_item] ([id], [title], [url], [sort]) VALUES (1, N'站点信息统计', N'/core/admin/indexItem!base.do', 1)
INSERT [dbo].[es_index_item] ([id], [title], [url], [sort]) VALUES (2, N'分类消息统计', N'/core/admin/message!statistics.do', 4)
INSERT [dbo].[es_index_item] ([id], [title], [url], [sort]) VALUES (3, N'流量统计信息', N'/core/admin/indexItem!access.do', 3)
INSERT [dbo].[es_index_item] ([id], [title], [url], [sort]) VALUES (5, N'您管理的客诉信息', N'/autel/main/indexItemCustomerComplaint.do', 2)
SET IDENTITY_INSERT [dbo].[es_index_item] OFF
/****** Object:  Table [dbo].[es_guestbook]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_guestbook](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NULL,
	[content] [ntext] NULL,
	[parentid] [int] NULL,
	[dateline] [bigint] NULL,
	[issubject] [smallint] NULL,
	[username] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[qq] [varchar](255) NULL,
	[tel] [varchar](255) NULL,
	[sex] [smallint] NULL,
	[ip] [varchar](255) NULL,
	[area] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_guestbook] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[es_friends_link]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_friends_link](
	[link_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[url] [varchar](100) NULL,
	[logo] [varchar](255) NULL,
	[sort] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[link_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_friends_link] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_friends_link] ON
INSERT [dbo].[es_friends_link] ([link_id], [name], [url], [logo], [sort]) VALUES (2, N'1112', N'111', N'fs:/attachment/friendslink/201303292116115094.jpg', 1)
SET IDENTITY_INSERT [dbo].[es_friends_link] OFF
/****** Object:  Table [dbo].[es_data_model]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_data_model](
	[model_id] [int] IDENTITY(1,1) NOT NULL,
	[china_name] [varchar](255) NULL,
	[english_name] [varchar](255) NULL,
	[add_time] [bigint] NULL,
	[project_name] [varchar](255) NULL,
	[brief] [varchar](400) NULL,
	[if_audit] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[model_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_data_model] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_data_model] ON
INSERT [dbo].[es_data_model] ([model_id], [china_name], [english_name], [add_time], [project_name], [brief], [if_audit]) VALUES (1, N'普通文章', N'car_article', 1284636354948, NULL, N'', 0)
SET IDENTITY_INSERT [dbo].[es_data_model] OFF
/****** Object:  Table [dbo].[es_data_field]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_data_field](
	[field_id] [int] IDENTITY(1,1) NOT NULL,
	[china_name] [varchar](255) NULL,
	[english_name] [varchar](255) NULL,
	[data_type] [int] NULL,
	[data_size] [varchar](20) NULL,
	[show_form] [varchar](255) NULL,
	[show_value] [varchar](400) NULL,
	[add_time] [bigint] NULL,
	[model_id] [int] NULL,
	[save_value] [ntext] NULL,
	[is_validate] [smallint] NULL,
	[taxis] [int] NULL,
	[dict_id] [int] NULL,
	[is_show] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[field_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_data_field] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_data_field] ON
INSERT [dbo].[es_data_field] ([field_id], [china_name], [english_name], [data_type], [data_size], [show_form], [show_value], [add_time], [model_id], [save_value], [is_validate], [taxis], [dict_id], [is_show]) VALUES (1, N'标题', N'title', 1, NULL, N'input', NULL, 1284636743486, 1, N'', 1, 1, NULL, 1)
INSERT [dbo].[es_data_field] ([field_id], [china_name], [english_name], [data_type], [data_size], [show_form], [show_value], [add_time], [model_id], [save_value], [is_validate], [taxis], [dict_id], [is_show]) VALUES (2, N'图片', N'img', 1, NULL, N'image', NULL, 1284636765602, 1, N'', 0, 3, NULL, 0)
INSERT [dbo].[es_data_field] ([field_id], [china_name], [english_name], [data_type], [data_size], [show_form], [show_value], [add_time], [model_id], [save_value], [is_validate], [taxis], [dict_id], [is_show]) VALUES (3, N'内容', N'content', 2, NULL, N'richedit', NULL, 1284636783994, 1, N'', 0, 4, NULL, 0)
INSERT [dbo].[es_data_field] ([field_id], [china_name], [english_name], [data_type], [data_size], [show_form], [show_value], [add_time], [model_id], [save_value], [is_validate], [taxis], [dict_id], [is_show]) VALUES (7, N'语言', N'language_code', NULL, NULL, N'language_select', NULL, 1358255757727, 1, NULL, 1, 2, NULL, 1)
SET IDENTITY_INSERT [dbo].[es_data_field] OFF
/****** Object:  Table [dbo].[es_data_cat]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_data_cat](
	[cat_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[parent_id] [int] NULL,
	[cat_path] [varchar](255) NULL,
	[cat_order] [int] NULL,
	[model_id] [int] NULL,
	[if_audit] [int] NULL,
	[url] [varchar](255) NULL,
	[detail_url] [varchar](255) NULL,
	[tositemap] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[cat_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_data_cat] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_data_cat] ON
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (2, N'新闻中心', 0, N'0|2|', 20, 1, NULL, N'news-2-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (10, N'资料中心', 0, N'0|10|', 30, 1, NULL, N'solution-13-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (11, N'客服中心', 0, N'0|11|', 10, 1, NULL, N'service.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (27, N'Company News', 2, N'0|2|27|', 10, 1, NULL, N'newslist-27-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (28, N'EURO & US Cars', 10, N'0|10|28|', 10, 1, NULL, N'literature-28-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (29, N'FAQ', 11, N'0|11|29|', 10, 1, NULL, N'newslist-29-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (30, N'Industry news', 2, N'0|2|30|', 20, 1, NULL, N'newslist-30-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (31, N'Product Registration', 2, N'0|2|31|', 30, 1, NULL, N'newslist-31-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (32, N'SW Update Release', 2, N'0|2|32|', 40, 1, NULL, N'newslist-32-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (33, N'Latest Announcement', 2, N'0|2|33|', 50, 1, NULL, N'newslist-33-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (35, N'Other', 2, N'0|2|35|', 50, 1, NULL, N'newslist-35-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (36, N'Chinese Cars', 10, N'0|10|36|', 20, 1, NULL, N'literature-36-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (37, N'JP & KR Cars', 10, N'0|10|37|', 30, 1, NULL, N'literature-37-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (38, N'Middle East Carts', 10, N'0|10|38|', 40, 1, NULL, N'literature-38-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (39, N'Other', 10, N'0|10|39|', 50, 1, NULL, N'literature-39-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (40, N'Supported Models', 11, N'0|11|40|', 20, 1, NULL, N'newslist-40-1.html', N'', 0)
INSERT [dbo].[es_data_cat] ([cat_id], [name], [parent_id], [cat_path], [cat_order], [model_id], [if_audit], [url], [detail_url], [tositemap]) VALUES (41, N'Oline Complain Service', 11, N'0|11|41|', 30, 1, NULL, N'newslist-41-1.html', N'', 0)
SET IDENTITY_INSERT [dbo].[es_data_cat] OFF
/****** Object:  Table [dbo].[es_component]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_component](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[componentid] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[install_state] [smallint] NULL,
	[enable_state] [smallint] NULL,
	[version] [varchar](50) NULL,
	[author] [varchar](255) NULL,
	[javashop_version] [varchar](50) NULL,
	[description] [ntext] NULL,
	[error_message] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_component] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_component] ON
INSERT [dbo].[es_component] ([id], [componentid], [name], [install_state], [enable_state], [version], [author], [javashop_version], [description], [error_message]) VALUES (1, N'cmsCoreComponent', N'CMS核心组件', 1, 1, N'1.0', N'javashop', N'3.0.0', N'', NULL)
INSERT [dbo].[es_component] ([id], [componentid], [name], [install_state], [enable_state], [version], [author], [javashop_version], [description], [error_message]) VALUES (2, N'baseComponent', N'base应用基础组件', 1, 1, N'1.0', N'javashop', N'3.0.0', N'', NULL)
INSERT [dbo].[es_component] ([id], [componentid], [name], [install_state], [enable_state], [version], [author], [javashop_version], [description], [error_message]) VALUES (7, N'cmsExtendComponent', N'CMS扩展组件', 1, 1, N'1.0', N'autel', N'3.0.0', N'', NULL)
INSERT [dbo].[es_component] ([id], [componentid], [name], [install_state], [enable_state], [version], [author], [javashop_version], [description], [error_message]) VALUES (8, N'paymentComponent', N'支付组件', 1, 1, N'1.0', N'autel', N'3.0.0', N'', NULL)
SET IDENTITY_INSERT [dbo].[es_component] OFF
/****** Object:  Table [dbo].[es_car_article]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_car_article](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sort] [int] NULL,
	[add_time] [bigint] NULL,
	[hit] [bigint] NULL,
	[able_time] [bigint] NULL,
	[state] [int] NULL,
	[user_id] [bigint] NULL,
	[cat_id] [int] NULL,
	[is_commend] [int] NULL,
	[title] [varchar](255) NULL,
	[img] [varchar](255) NULL,
	[content] [ntext] NULL,
	[sys_lock] [int] NULL,
	[lastmodified] [bigint] NULL,
	[page_title] [varchar](255) NULL,
	[page_keywords] [varchar](255) NULL,
	[page_description] [ntext] NULL,
	[site_code] [int] NULL,
	[siteidlist] [varchar](255) NULL,
	[language_code] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_car_article] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_car_article] ON
INSERT [dbo].[es_car_article] ([id], [sort], [add_time], [hit], [able_time], [state], [user_id], [cat_id], [is_commend], [title], [img], [content], [sys_lock], [lastmodified], [page_title], [page_keywords], [page_description], [site_code], [siteidlist], [language_code]) VALUES (62, 0, 1363702741, 0, NULL, NULL, NULL, 26, NULL, N'222', N'', N'<p>
	2222</p>
', 0, 1363702741, N'', N'', N'', 100000, NULL, N'lag201301070020090542')
INSERT [dbo].[es_car_article] ([id], [sort], [add_time], [hit], [able_time], [state], [user_id], [cat_id], [is_commend], [title], [img], [content], [sys_lock], [lastmodified], [page_title], [page_keywords], [page_description], [site_code], [siteidlist], [language_code]) VALUES (63, 10, 1364396273, 47, NULL, NULL, NULL, 27, NULL, N'外汇储备库总规模1000亿美元 中国或出资410亿美元', N'', N'<p>
	央视3月27日《东方时空》节目播出&ldquo;祖马证实组建金砖银行将投资4.5万亿美元&rdquo;，以下为文字实录：</p>
<p>
	张羽：董倩，我们刚刚知道，会议目前已经就金砖国家开发银行达成了初步的意向，我们也想知道，要真正建立起金砖银行还面临着很多问题，他具体落实还有多远？</p>
<p>
	董倩：昨天我记得我们两个连线的过程中也说到了德班在外面正在下着大雨，今天的报纸也把这个大雨和这次德班会议上的一系列协议联系了起来，一天半的 时间非常有限，但是多边和双边的协议签署了很多，今天早上有报纸说，这些协议就好像昨天的大雨一样多，但是大家最关注的能不能成立金砖银行，细节如何落 实，以及如何到底能不能成立这个外汇资金储备库的问题，应该说直到我们刚才还在听的领导人记者会之前还没有一个到底怎么样的这么一个确切的说法。</p>
<p>
	我告诉你一个非常有意思的信息，今天早上我们刚刚报了国际会议中心有一位中国代表团的成员，因为他早上起来参与了今天的早餐会，他说，俄罗斯总统普 京在早餐会上释放了这样的一个信息，普京总统的原话是这样的，今天在德班峰会上，我们将正式签署成立开发银行的协议，他的这样的一个释放信息应该说是让很 多人都有点出乎意料。</p>
<p>
	在刚才大概20分钟以前，祖马总统在领导人会议结束之后的记者招待会上证实了这个消息，他说，<strong>未来五年金砖五国将会有4.5万亿美元的投资，主要是花在五国还有其他发展中国家的基础设施建设上</strong>。应该说他们都是大人物，为什么这么说？因为大人物做的是求同的事情，同有了，接下来还会有一些分歧，也就是意，这些意接下来就要这些财长和央行行长的去面对了。</p>
<p>
	我们在采访的过程中注意到，俄罗斯财长说了这么一番话，他说，对于细节的敲定我们会加快进程，在今年4月份的G20峰会上我们会继续讨论，印度的财长说，出资选址这个问题一年以后才会定下，而且资金人员治理结构这些问题非常复杂也非常细致，我们要很谨慎的去面对。</p>
<p>
	我们就看为什么这个问题这么难达成一个共识，现在我们看到有些外媒说，对于金砖银行到底启动资金是多少？有外媒报道说，俄罗斯认为启动资金应该每一 个国家都相同，就是100亿美元这样的出资，但是你知道五个国家经济体量是非常不一样的，中国的经济体量是南非的20倍，是俄罗斯和巴西的4倍，在这个问 题上是不是每个国家出资都相同，因此在这个问题上各方是存在分歧的。</p>
<p>
	具体还有一些关于外汇储备库的建立，刚才祖马总统也说，他们已经初步达成了一个共识，就是在未来要推进这个资金储备库的建设，<strong>未来几年要形成一个1000亿美元的资金池。同样外媒也把这个问题细致了一下，比如说中国将在其中占到410亿美元的出资，南非至少是50亿美元，其他三国都各占180亿美元</strong>，当然这个消息还没有得到证实。</p>
', 0, 1364821295, N'祖马证实组建金砖银行将投资4.5万亿美元', N'1000亿美元 外汇储备', N'张羽：董倩，我们刚刚知道，会议目前已经就金砖国家开发银行达成了初步的意向，我们也想知道，要真正建立起金砖银行还面临着很多问题，他具体落实还有多远？', 100000, NULL, N'lag201301070146010554')
INSERT [dbo].[es_car_article] ([id], [sort], [add_time], [hit], [able_time], [state], [user_id], [cat_id], [is_commend], [title], [img], [content], [sys_lock], [lastmodified], [page_title], [page_keywords], [page_description], [site_code], [siteidlist], [language_code]) VALUES (64, 10, 1364490745, 11, NULL, NULL, NULL, 28, NULL, N'习近平谈访俄:不是痛并快乐着 是累并快乐着', N'', N'<p>
	　原标题：中俄之间的特殊&ldquo;熊抱&rdquo;</p>
<p>
	　　一天半时间，约20场正式活动，习近平说，他是&ldquo;累并快乐着&rdquo;。中国新任国家元首将首访定在俄罗斯，以及俄方做出的种种安排，都在凸显中俄关系的高水平和特殊性</p>
<p>
	　　本刊记者/田冰(发自莫斯科)</p>
<p>
	　　当地时间3月22日11时55分许，中国国家主席习近平乘坐的专机抵达莫斯科伏努科沃2号机场。习近平和夫人彭丽媛受到俄远东地区发展部长伊沙耶夫的热情迎接。俄政府在机场举行了迎接仪式，习近平检阅了俄三军仪仗队，并观看了分列式。</p>
<p>
	　　按外交礼仪惯例，国事访问，东道主国家会派出部长或副部长等高官到机场接机。俄方派伊沙耶夫接机，或许暗含深意。</p>
<p>
	　　2010年，同样是早春时节，时任国家副主席习近平将访俄第一站选择在俄罗斯远东地区的滨海边疆区。伊沙耶夫长期在远东地区任要职，目前担任俄 远东地区发展部长，同时兼任俄总统驻远东联邦区全权代表。而中国东北地区与俄罗斯远东及东西伯利亚地区合作，是习近平此访重要议题之一。</p>
<p>
	　　&ldquo;他希望能与习近平性格相似&rdquo;</p>
<p>
	　　俄罗斯主要市场化报纸之一《生意人报》刊登了标题为&ldquo;中国&mdash;克里姆林宫仪式&rdquo;的文章。该报在描述&ldquo;习普会&rdquo;时透露，普京在克里姆林宫接待中国领 导人，这本身就是一种极高的礼遇。因为一段时间以来，普京越来越少、而且只在极为必要的情况下，才会离开新奥加廖沃。新奥加廖沃是俄罗斯总统郊外官邸，距 离莫斯科数十公里。</p>
<p>
	　　&ldquo;但昨日(3月22日)，除了向无名烈士墓献花活动外，与中国国家主席到访有关的所有活动都安排在克里姆林宫举行。正式欢迎仪式在大克里姆林宫最有名的乔治大厅举行，欢迎仪式和迎接阵容非常豪华，就像金碧辉煌的乔治大厅本身一样。&rdquo;</p>
<p>
	　　据《中国新闻周刊》记者观察，俄方参加欢迎仪式和会谈的政府官员约有30人，其中有3名副总理(戈洛杰茨、德沃尔科维奇、罗戈津)、总统助理乌 沙科夫和外交、国防、经济、农业、能源等各部部长。此外，由上届政府副总理转任俄罗斯石油公司总裁的谢钦、俄天然气工业股份公司总裁米勒等大公司高层也位 列其中。这在俄方接待外国元首历史上极为少见。</p>
<p>
	　　在《生意人报》笔下，习近平给人留下的印象是：&ldquo;身形略显壮硕，衣着非常得体&rdquo;，&ldquo;脸上写满温厚与友善&rdquo;。</p>
<p>
	　　在会见中，习近平回忆与普京的交往，&ldquo;3年前我应你的邀请首次访俄，那次访问给我留下了非常美好和深刻的印象。去年6月，我们又在北京再次相见。此外，我们还数次相互通信，不久前我们进行了电话交谈。&rdquo;</p>
<p>
	　　当习近平说他们两人&ldquo;很能谈得来，性格很相似&rdquo;时，普京会心一笑。</p>
<p>
	　　&ldquo;他(普京)希望能与习近平性格相似。&rdquo;《生意人报》写道。</p>
<p>
	　　除了最高规格的礼宾安排外，俄方在活动项目的选择上也做足了功课，有些活动是普京亲自主动提议的，如邀请习近平参观俄国防部，会见俄罗斯汉学 家、学习汉语的学生和媒体代表等。这些安排，除了体现俄方欲显示&ldquo;最大诚意&rdquo;之外，也彰显中俄关系的高水平和特殊性，以及两国最高层之间的默契和友好。</p>
<p>
	　　3月23日下午3时许，习近平抵达俄国防部大楼前，俄国防部长绍伊古在此迎接，并举行了隆重的欢迎仪式。这是一次非常特殊的安排&mdash;&mdash;习近平是第一位到访俄国防部并参观俄联邦武装力量作战指挥中心的外国元首。</p>
<p>
	　　在俄联邦武装力量作战指挥中心，正在指挥中心值勤的俄陆海空军军官全体起立，向习近平敬礼。俄军总参谋长格拉西莫夫向习近平报告说，&ldquo;尊敬的主席先生，您是我们为之打开俄联邦武装力量作战指挥中心这扇门的首位外国领导人。&rdquo;</p>
<p>
	　　一天半时间，约20场正式活动。习近平同普京举行会谈并共同出席俄罗斯中国旅游年开幕式，会见梅德韦杰夫总理、联邦委员会主席马特维延科以及国 家杜马主席纳雷什金等俄方领导人，在莫斯科国际关系学院发表演讲，出席中共六大会址纪念馆建馆启动仪式，参观俄国防部，会见俄罗斯汉学家、学习汉语的学生 及媒体代表。双方发表由两国元首签署的联合声明，并共同见证双方经贸、能源、投资、地方、人文、环保等诸多领域达成合作协议的签字仪式。</p>
<p>
	　　《中国新闻周刊》记者观察到，甫下飞机，从当地时间22日下午3点多到晚上10点40分，习近平访俄首日与普京共同出席活动持续了7个多小时。 &ldquo;会谈非常富有成效，取得了巨大成功。&rdquo;中国驻俄使馆一位工作人员告诉《中国新闻周刊》。而因为会谈时间拉长，以至于当晚在克里姆林宫大礼堂举行的俄罗斯 中国旅游年开幕式比原定时间推迟了近两小时。</p>
<p>
	　　习近平一行访俄的次日，一天的日程从早晨9点开始，至晚上会见俄罗斯汉学家、学习汉语的学生及媒体代表为结束。最后这一项活动持续至晚上10点多，比预计时间又延长了40多分钟。</p>
<p>
	　　据中国驻俄使馆工作人员透露，几乎每场活动都有延长，但是因行程安排紧密，能压缩的只有中间非常短暂的休息时间。使馆方面曾考虑到行程安排太密集，根本没有时间休息，欲压缩一些活动的时间，&ldquo;但是习近平主席一点都不打折扣&rdquo;。</p>
', 0, 1364490745, N'中俄之间的特殊“熊抱”', N'习近平', N'一天半时间，约20场正式活动，习近平说，他是“累并快乐着”。中国新任国家元首将首访定在俄罗斯，以及俄方做出的种种安排，都在凸显中俄关系的高水平和特殊性', 100000, NULL, N'lag201301070020090542')
INSERT [dbo].[es_car_article] ([id], [sort], [add_time], [hit], [able_time], [state], [user_id], [cat_id], [is_commend], [title], [img], [content], [sys_lock], [lastmodified], [page_title], [page_keywords], [page_description], [site_code], [siteidlist], [language_code]) VALUES (65, 10, 1364491676, 9, NULL, NULL, NULL, 29, NULL, N'支付帮助', N'', N'<dl>
	<dt>
		<strong>Q:</strong> download a model upgrade package the upgrade tool shows don&#39;t come out, how should be thandI do?<span class="cus_span">[2013-01-10 16:30:11]</span></dt>
	<dt>
		<strong>Answer:</strong> the possible reasons and the corresponding solution has the following two kinds: 1, the model upgrade procedures and other models to upgrade procedure is not the same position, copy to the same position can; 2, the model upgrade program download no t completely, can download again.</dt>
</dl>
', 0, 1364491899, N'', N'', N'', 100000, NULL, N'lag201301070146010554')
INSERT [dbo].[es_car_article] ([id], [sort], [add_time], [hit], [able_time], [state], [user_id], [cat_id], [is_commend], [title], [img], [content], [sys_lock], [lastmodified], [page_title], [page_keywords], [page_description], [site_code], [siteidlist], [language_code]) VALUES (66, 0, 1364821131, 0, NULL, NULL, NULL, 27, NULL, N'ssssssssssssssssssssssssssssssss', N'', N'<p>
	sssssssssssssssssssssssssssssssssssssssssssssssssssss</p>
', 0, 1364821131, N'adsf', N'sadff', N'sdfdsafasdfsadfsa', 100000, NULL, N'lag201301070020090542')
SET IDENTITY_INSERT [dbo].[es_car_article] OFF
/****** Object:  Table [dbo].[es_auth_action]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_auth_action](
	[actid] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[type] [varchar](255) NULL,
	[objvalue] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[actid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_auth_action] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_auth_action] ON
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (1, N'编辑', N'menu', N'1,64,70,71,72,65,66,67,68,69,20,21,22,10,42,11,12')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (3, N'栏目内容', N'menu', N'23,44,43,45,46,1,10,42,11,12')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (4, N'外观效果', N'menu', N'16,17,18,19,20,21,22')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (5, N'系统设置', N'menu', N'23,44,43,45,46,24,25,26,27,28,29,30,31,32,38,41,39,40')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (6, N'访问统计', N'menu', N'33,34,35')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (7, N'产品管理', N'menu', N'50,51,52')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (8, N'会员管理', N'menu', N'57,73,83,74,87,89,90,95,58,78,79,84,85,99,63,59,80')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (9, N'kefu', N'menu', N'57,89,90,116,95')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (11, N'客服', N'menu', N'57,89,90,116,95')
INSERT [dbo].[es_auth_action] ([actid], [name], [type], [objvalue]) VALUES (13, N'管理员', N'menu', N'23,60,61,62,53,54,55,44,43,45,46,47,48,49,93,94,81,82,24,25,32,28,29,30,38,39,40,1,64,70,71,72,65,66,67,68,69,10,42,11,12,20,21,22,108,110,109,112,75,76,101,86,102,88,91,92,100,103,104,96,97,98,105,106,111,107,57,73,83,74,87,89,117,90,116,95,58,78,79,84,85,99,80,59,63,50,51,52')
SET IDENTITY_INSERT [dbo].[es_auth_action] OFF
/****** Object:  Table [dbo].[es_adv]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_adv](
	[aid] [bigint] IDENTITY(1,1) NOT NULL,
	[acid] [bigint] NULL,
	[atype] [int] NULL,
	[begintime] [bigint] NULL,
	[endtime] [bigint] NULL,
	[isclose] [int] NULL,
	[attachment] [varchar](50) NULL,
	[atturl] [varchar](255) NULL,
	[url] [varchar](255) NULL,
	[aname] [varchar](255) NULL,
	[clickcount] [int] NULL,
	[linkman] [varchar](50) NULL,
	[company] [varchar](255) NULL,
	[contact] [varchar](255) NULL,
	[disabled] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[aid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_adv] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_adv] ON
INSERT [dbo].[es_adv] ([aid], [acid], [atype], [begintime], [endtime], [isclose], [attachment], [atturl], [url], [aname], [clickcount], [linkman], [company], [contact], [disabled]) VALUES (1, 1, NULL, 1272643200000, 1433001600000, 0, NULL, N'http://localhost:8080/javaweb/statics/attachment/adv/201009211255055900.jpg', N'../index.html', N'首页轮换1', 1, N'', N'', N'', N'false')
INSERT [dbo].[es_adv] ([aid], [acid], [atype], [begintime], [endtime], [isclose], [attachment], [atturl], [url], [aname], [clickcount], [linkman], [company], [contact], [disabled]) VALUES (2, 1, NULL, 1272643200000, 1433001600000, 0, NULL, N'http://localhost:8080/javaweb/statics/attachment/adv/201009211254109510.jpg', N'../index.html', N'首页轮换3', 4, N'', N'', N'', N'false')
INSERT [dbo].[es_adv] ([aid], [acid], [atype], [begintime], [endtime], [isclose], [attachment], [atturl], [url], [aname], [clickcount], [linkman], [company], [contact], [disabled]) VALUES (3, 1, NULL, 1284652800000, 1600272000000, 0, NULL, N'fs:/attachment/adv/201009211253500869.jpg', N'../index.html', N'首页轮换2', 1, N'', N'', N'', N'false')
INSERT [dbo].[es_adv] ([aid], [acid], [atype], [begintime], [endtime], [isclose], [attachment], [atturl], [url], [aname], [clickcount], [linkman], [company], [contact], [disabled]) VALUES (4, 2, NULL, 1285776000000, 1569772800000, 0, NULL, N'http://localhost:8080/javashop/statics/attachment/adv/201009301515295573.jpg', N'../index.html', N'蓝色经典广告图片1', 5, N'', N'', N'', N'false')
INSERT [dbo].[es_adv] ([aid], [acid], [atype], [begintime], [endtime], [isclose], [attachment], [atturl], [url], [aname], [clickcount], [linkman], [company], [contact], [disabled]) VALUES (5, 4, NULL, 1312214400000, 2574518400000, 0, NULL, N'fs:/attachment/adv/201108021410470508.jpg', N'../index.html', N'橙色激情广告图片', 0, N'', N'', N'', N'false')
INSERT [dbo].[es_adv] ([aid], [acid], [atype], [begintime], [endtime], [isclose], [attachment], [atturl], [url], [aname], [clickcount], [linkman], [company], [contact], [disabled]) VALUES (6, 3, NULL, 1312214400000, 2574518400000, 0, NULL, N'fs:/attachment/adv/201108021411479293.jpg', N'../index.html', N'绿色情怡广告图片', 1, N'', N'', N'', N'false')
INSERT [dbo].[es_adv] ([aid], [acid], [atype], [begintime], [endtime], [isclose], [attachment], [atturl], [url], [aname], [clickcount], [linkman], [company], [contact], [disabled]) VALUES (7, 5, NULL, 1313424000000, 1629043200000, 0, NULL, N'http://localhost:8080/javashop/statics/attachment/adv/201108222137587830.jpg', N'../index.html', N'粉色广告图片一', 1, N'', N'', N'', N'false')
INSERT [dbo].[es_adv] ([aid], [acid], [atype], [begintime], [endtime], [isclose], [attachment], [atturl], [url], [aname], [clickcount], [linkman], [company], [contact], [disabled]) VALUES (8, 5, NULL, 1313424000000, 1629043200000, 0, NULL, N'fs:/attachment/adv/201108222138413409.jpg', N'../index.html', N'粉色广告图片二', 0, N'', N'', N'', N'false')
SET IDENTITY_INSERT [dbo].[es_adv] OFF
/****** Object:  Table [dbo].[es_adminuser]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_adminuser](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[state] [int] NULL,
	[realname] [varchar](255) NULL,
	[userno] [varchar](255) NULL,
	[userdept] [varchar](255) NULL,
	[remark] [varchar](255) NULL,
	[dateline] [int] NULL,
	[founder] [smallint] NULL,
	[siteid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_adminuser] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_adminuser] ON
INSERT [dbo].[es_adminuser] ([userid], [username], [password], [state], [realname], [userno], [userdept], [remark], [dateline], [founder], [siteid]) VALUES (1, N'admin', N'21232f297a57a5a743894ae4a801fc3', 1, N'admin', N'', N'', N'', NULL, 0, NULL)
INSERT [dbo].[es_adminuser] ([userid], [username], [password], [state], [realname], [userno], [userdept], [remark], [dateline], [founder], [siteid]) VALUES (2, N'order', N'70a17ffa722a3985b86d30b034ad6d7', 1, N'order', N'', N'', N'', NULL, 0, NULL)
INSERT [dbo].[es_adminuser] ([userid], [username], [password], [state], [realname], [userno], [userdept], [remark], [dateline], [founder], [siteid]) VALUES (3, N'developer', N'5e8edd851d2fdfbd7415232c67367cc3', 1, N'developer', N'', N'', N'', NULL, 0, NULL)
INSERT [dbo].[es_adminuser] ([userid], [username], [password], [state], [realname], [userno], [userdept], [remark], [dateline], [founder], [siteid]) VALUES (4, N'pda', N'e4a1c788249df994eaedb833d434', 1, N'pda', N'', N'', N'', NULL, 0, NULL)
INSERT [dbo].[es_adminuser] ([userid], [username], [password], [state], [realname], [userno], [userdept], [remark], [dateline], [founder], [siteid]) VALUES (9, N'czy', N'a92bfc3ced5f1511333292f739647d43', 1, N'czy', N'', N'', N'', NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[es_adminuser] OFF
/****** Object:  Table [dbo].[es_admintheme]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_admintheme](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[appid] [varchar](50) NULL,
	[siteid] [int] NULL,
	[themename] [varchar](50) NULL,
	[path] [varchar](255) NULL,
	[userid] [int] NULL,
	[author] [varchar](50) NULL,
	[version] [varchar](50) NULL,
	[framemode] [int] NULL,
	[deleteflag] [int] NULL,
	[thumb] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_admintheme] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_admintheme] ON
INSERT [dbo].[es_admintheme] ([id], [appid], [siteid], [themename], [path], [userid], [author], [version], [framemode], [deleteflag], [thumb]) VALUES (1, NULL, NULL, N'默认模板', N'default', NULL, N'enation', N'1.0', 0, 0, N'preview.png')
SET IDENTITY_INSERT [dbo].[es_admintheme] OFF
/****** Object:  Table [dbo].[es_adcolumn]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_adcolumn](
	[acid] [int] IDENTITY(1,1) NOT NULL,
	[cname] [varchar](255) NULL,
	[width] [varchar](50) NULL,
	[height] [varchar](50) NULL,
	[description] [varchar](255) NULL,
	[anumber] [bigint] NULL,
	[atype] [int] NULL,
	[arule] [bigint] NULL,
	[disabled] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[acid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_adcolumn] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[es_adcolumn] ON
INSERT [dbo].[es_adcolumn] ([acid], [cname], [width], [height], [description], [anumber], [atype], [arule], [disabled]) VALUES (1, N'红色模板广告位', N'731px', N'260px', NULL, NULL, 0, NULL, N'false')
INSERT [dbo].[es_adcolumn] ([acid], [cname], [width], [height], [description], [anumber], [atype], [arule], [disabled]) VALUES (2, N'蓝色经典广告位', N'1002px', N'252px', NULL, NULL, 0, NULL, N'false')
INSERT [dbo].[es_adcolumn] ([acid], [cname], [width], [height], [description], [anumber], [atype], [arule], [disabled]) VALUES (3, N'绿色情怡广告位', N'968px', N'365px', NULL, NULL, 0, NULL, N'false')
INSERT [dbo].[es_adcolumn] ([acid], [cname], [width], [height], [description], [anumber], [atype], [arule], [disabled]) VALUES (4, N'橙色浪漫广告位', N'990px', N'246px', NULL, NULL, 0, NULL, N'false')
INSERT [dbo].[es_adcolumn] ([acid], [cname], [width], [height], [description], [anumber], [atype], [arule], [disabled]) VALUES (5, N'粉色童话广告位', N'980px', N'290px', NULL, NULL, 0, NULL, N'false')
SET IDENTITY_INSERT [dbo].[es_adcolumn] OFF
/****** Object:  Table [dbo].[es_access]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[es_access](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ip] [varchar](255) NULL,
	[url] [varchar](1000) NULL,
	[page] [varchar](255) NULL,
	[area] [varchar](255) NULL,
	[access_time] [int] NULL,
	[stay_time] [int] NULL,
	[point] [int] NULL,
	[membername] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[es_access] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DT_ProductType]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ProductType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[proSerialPicPath] [varchar](1000) NULL,
	[picPath] [varchar](1000) NOT NULL,
 CONSTRAINT [PK_DT_PRODUCTTYPE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ProductType] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ProductType] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'prt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ProductType', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'prt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ProductType'
GO
SET IDENTITY_INSERT [dbo].[DT_ProductType] ON
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (28, N'prt201301071535520790', N'TS501_EN', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"6f50572829284434acd8e96dfe77fa83","picPath":"statics/attachment/language/autel/4127d732f26340d59b4e7439d8ae2c2c.jpg"}]', N'statics/attachment/language/autel/f101ec50da1441f2a8deac04192e6af1.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (30, N'prt201301081510220618', N'DS708', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"d9d62fa461e846528740d9a1a2599ddc","picPath":"statics/attachment/language/autel/5fd0498206c14e4794f96c9705d15f9c.jpg"}]', N'statics/attachment/language/autel/9687fbae636045509301ad84bfc353b5.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (33, N'prt201301121755410241', N'VAG505_en', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"83118c6bdecf419485a0e58b24e91333","picPath":"statics/attachment/language/autel/a28aeb60b6aa4cd9b5ebdf82552ebbaf.jpg"}]', N'statics/attachment/language/autel/370dedb668f34a6b96943267ab9f859a.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (34, N'prt201301121756510710', N'OLS301_en', N'[{"code":"","languageCode":"lag201301070146010554","languageName":"","picId":"15b3baec094e4c129d1d1c19d2a5f119","picPath":"statics/attachment/language/autel/d8de254ac13048fca8cb76bced44a126.jpg"}]', N'statics/attachment/language/autel/28706d2668344580a5fd8952d586fc33.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (35, N'prt201301261537230766', N'EBS301_en', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"dce591b3e01b483f9b2d044e5e248dcb","picPath":"statics/attachment/language/autel/a070d4fa1f80494abc2b5ee04a46747a.jpg"}]', N'statics/attachment/language/autel/5c86498b64734ea782755ca7f9687892.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (36, N'prt201301301142520522', N'AL609', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"e205af33bf8a42abb553079fdc7f19b2","picPath":"statics/attachment/language/autel/17a751ab2ec647debad65f58e8a756b4.jpg"}]', N'statics/attachment/language/autel/6dea16ee0e514a42bccbbd53cb163abd.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (41, N'prt201303062244140942', N'AL619_EN', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"ed5b043a9d9246e0962b2a2056d6610e","picPath":"statics/attachment/language/autel/2de4fe5f9f844b6e88a24d44748034ca.jpg"}]', N'statics/attachment/language/autel/a6c7d74bfd404f7da85887f12b93784a.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (43, N'prt201303062258250082', N'MOT_Pro_EN', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"846bf12d56af4efa8af499c68b447ebd","picPath":"statics/attachment/language/autel/deb90c83178b480e928a63dcf6d5cccc.jpg"}]', N'statics/attachment/language/autel/9d6c6babb4504454aefadc3bb7ad50c6.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (44, N'prt201303062304290138', N'Check_Pro_EN', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"b084ddbcfa6a4dea8b099c8cb839e051","picPath":"statics/attachment/language/autel/92597a5d560947249aa1309c8629ecfd.jpg"}]', N'statics/attachment/language/autel/5334d9e99a9c47588bb4a7ec12fcd739.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (45, N'prt201303062305310621', N'MD802_EN_3', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"bf2b8931e7154a9192b0aec81c3e17aa","picPath":"statics/attachment/language/autel/fcf67bf3efc14e45bc8f7d90f33dbd5a.jpg"}]', N'statics/attachment/language/autel/c52f422e3a184a589991e3cf0eec1695.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (46, N'prt201303062306080995', N'MD802_EN_2', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"66f174b39eab412e8b39334ffe84cc6a","picPath":"statics/attachment/language/autel/e685c36721f14796956fae8e99389abf.jpg"}]', N'statics/attachment/language/autel/f8d7ca34e9264fa5af5d781b934bfa63.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (47, N'prt201303081717190162', N'MD802_EN_1', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"4e846a4fc3cd4635a6469c7244ef09ec","picPath":"statics/attachment/language/autel/3d1fbe5764a845189672392c109ee6ce.jpg"}]', N'statics/attachment/language/autel/906bbce6588949c3b377b6c2d7e68c16.jpg')
INSERT [dbo].[DT_ProductType] ([id], [code], [name], [proSerialPicPath], [picPath]) VALUES (50, N'prt201303231459010678', N'MD802_EN_0', N'[{"code":"","languageCode":"lag201301070020090542","languageName":"","picId":"d6125fb0d6d24458a01332f36dabfbe6","picPath":"statics/attachment/language/autel/2871b48be1a842dba4a703e5d0e5ef96.jpg"}]', N'statics/attachment/language/autel/8e14e42551b54ed2bb2838491a373b15.jpg')
SET IDENTITY_INSERT [dbo].[DT_ProductType] OFF
/****** Object:  Table [dbo].[DT_ProductRepairRecord]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ProductRepairRecord](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[proCode] [varchar](32) NULL,
	[repDate] [varchar](250) NULL,
	[repType] [varchar](25) NULL,
	[repContent] [ntext] NULL,
 CONSTRAINT [PK_DT_ProductRepairRecord] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ProductRepairRecord] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_ProductRepairRecord] ON
INSERT [dbo].[DT_ProductRepairRecord] ([id], [code], [proCode], [repDate], [repType], [repContent]) VALUES (2, N'prr201303171738180331', N'pro201301071637123457', N'2013-03-15', N'111', N'adminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadmin')
INSERT [dbo].[DT_ProductRepairRecord] ([id], [code], [proCode], [repDate], [repType], [repContent]) VALUES (3, N'prr201303171739230640', N'pro201301071637123457', N'2013-03-31', N'123', N'123')
INSERT [dbo].[DT_ProductRepairRecord] ([id], [code], [proCode], [repDate], [repType], [repContent]) VALUES (4, N'prr201303212115470991', N'pro201301090108080139', N'2013-03-21', N'fgf', N'sdfsd')
INSERT [dbo].[DT_ProductRepairRecord] ([id], [code], [proCode], [repDate], [repType], [repContent]) VALUES (5, N'prr201303212116030147', N'pro201301261637110688', N'2013-03-05', N'ASAS', N'SDDSD')
INSERT [dbo].[DT_ProductRepairRecord] ([id], [code], [proCode], [repDate], [repType], [repContent]) VALUES (6, N'prr201303212116280334', N'pro201303072123110636', N'2013-03-05', N'DFD', N'SDFSDF')
INSERT [dbo].[DT_ProductRepairRecord] ([id], [code], [proCode], [repDate], [repType], [repContent]) VALUES (7, N'prr201303241657450484', N'pro201301071637123456', N'2013-03-24', N'133', N'666')
SET IDENTITY_INSERT [dbo].[DT_ProductRepairRecord] OFF
/****** Object:  Table [dbo].[DT_QuestionInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_QuestionInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[questionDesc] [varchar](250) NULL,
 CONSTRAINT [PK_DT_QUESTIONINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_QuestionInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_QuestionInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'qio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_QuestionInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_QuestionInfo] ON
INSERT [dbo].[DT_QuestionInfo] ([id], [code], [questionDesc]) VALUES (1, N'qui20121228134915123', N'今天天气怎么样？fff')
INSERT [dbo].[DT_QuestionInfo] ([id], [code], [questionDesc]) VALUES (2, N'qui201301090006120438', N'出生地址sdffsd')
INSERT [dbo].[DT_QuestionInfo] ([id], [code], [questionDesc]) VALUES (4, N'qui201301100902010948', N'cc')
INSERT [dbo].[DT_QuestionInfo] ([id], [code], [questionDesc]) VALUES (6, N'qui201301112351410320', N'你是哪里人')
INSERT [dbo].[DT_QuestionInfo] ([id], [code], [questionDesc]) VALUES (7, N'qui201301112355560820', N'你是哪里人')
INSERT [dbo].[DT_QuestionInfo] ([id], [code], [questionDesc]) VALUES (8, N'qui201301112358200398', N'你是哪里人')
INSERT [dbo].[DT_QuestionInfo] ([id], [code], [questionDesc]) VALUES (9, N'qui201304011807450820', N'什么是客户安全问题？~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~！！！')
INSERT [dbo].[DT_QuestionInfo] ([id], [code], [questionDesc]) VALUES (10, N'qui201304011809000209', N'什么是客户安全问题？~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~！！！')
SET IDENTITY_INSERT [dbo].[DT_QuestionInfo] OFF
/****** Object:  Table [dbo].[DT_Role]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_Role](
	[roleid] [int] IDENTITY(1,1) NOT NULL,
	[rolename] [varchar](255) NULL,
	[rolememo] [varchar](255) NULL,
 CONSTRAINT [PK_DT_ROLE] PRIMARY KEY NONCLUSTERED 
(
	[roleid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_Role] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DT_MarketPromotionSaleConfig]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MarketPromotionSaleConfig](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[productTypeCode] [varchar](32) NULL,
	[saleConfigCode] [varchar](32) NULL,
	[saleConfigName] [varchar](250) NULL,
	[marketPromotionCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_MarketPromotionSaleConfig] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MarketPromotionSaleConfig] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionSaleConfig] ON
INSERT [dbo].[DT_MarketPromotionSaleConfig] ([id], [code], [productTypeCode], [saleConfigCode], [saleConfigName], [marketPromotionCode]) VALUES (1, N'mpc201303191446500430', N'prt201301081510220618', N'scf201301181516540901', N'销售配置12', N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionSaleConfig] ([id], [code], [productTypeCode], [saleConfigCode], [saleConfigName], [marketPromotionCode]) VALUES (2, N'mpc201303210959010450', N'prt201301121755410241', N'scf201301181240180212', N'销售配置11', N'mpb201303210959000875')
INSERT [dbo].[DT_MarketPromotionSaleConfig] ([id], [code], [productTypeCode], [saleConfigCode], [saleConfigName], [marketPromotionCode]) VALUES (3, N'mpc201303211013570350', N'prt201301301142520522', N'scf201303031350340170', N'config101', N'mpb201303210959000875')
INSERT [dbo].[DT_MarketPromotionSaleConfig] ([id], [code], [productTypeCode], [saleConfigCode], [saleConfigName], [marketPromotionCode]) VALUES (4, N'mpc201303211013570421', N'prt201301261537230766', N'scf201301261556180297', N'saleconfig', N'mpb201303210959000875')
INSERT [dbo].[DT_MarketPromotionSaleConfig] ([id], [code], [productTypeCode], [saleConfigCode], [saleConfigName], [marketPromotionCode]) VALUES (5, N'mpc201303211013570482', N'prt201301081510220618', N'scf201301181527090943', N'销售配置3', N'mpb201303210959000875')
INSERT [dbo].[DT_MarketPromotionSaleConfig] ([id], [code], [productTypeCode], [saleConfigCode], [saleConfigName], [marketPromotionCode]) VALUES (6, N'mpc201303211013570573', N'prt201301081510220618', N'scf201301181516540901', N'销售配置12', N'mpb201303210959000875')
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionSaleConfig] OFF
/****** Object:  Table [dbo].[DT_MarketPromotionBaseInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MarketPromotionBaseInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[promotionName] [varchar](250) NULL,
	[promotionType] [int] NULL,
	[status] [int] NULL,
	[creator] [varchar](250) NULL,
	[createtime] [varchar](250) NULL,
	[allSaleUnit] [int] NULL,
	[allArea] [int] NULL,
 CONSTRAINT [PK_DT_MARKETPROMOTIONBASEINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MarketPromotionBaseInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_MarketPromotionBaseInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'abi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_MarketPromotionBaseInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionBaseInfo] ON
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (6, N'mpb201301240927510370', N'营销活动一', 1, 2, N'admin', N'201301240927510370', 1, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (7, N'mpb201301241021590454', N'营销活动二', 1, 2, N'admin', N'201301241021590456', 0, 1)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (8, N'mpb201301241028560270', N'营销活动二三', 1, 2, N'admin', N'201301241028560270', 1, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (9, N'mpb201301241030350094', N'套餐优惠1', 3, 0, N'admin', N'201301241030350094', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (10, N'mpb201301241034570246', N'优惠一', 1, 2, N'admin', N'201301241034570247', 1, 1)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (11, N'mpb201301241035550046', N'优惠2', 1, 2, N'admin', N'201301241035550046', 1, 1)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (12, N'mpb201301241037540891', N'优惠三', 1, 2, N'admin', N'201301241037540891', 1, 1)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (14, N'mpb201301241043290523', N'套餐21', 3, 0, N'admin', N'201301241043290523', 0, 1)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (15, N'mpb201301241458000340', N'越早越便宜1UU', 2, 0, N'admin', N'201301241458000342', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (17, N'mpb201301261601570445', N'huodong', 1, 2, N'admin', N'201301261601570445', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (18, N'mpb201301261603350975', N'huodong', 2, 2, N'admin', N'201301261603350976', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (19, N'mpb201301261604310039', N'huodong', 4, 0, N'admin', N'201301261604310039', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (20, N'mpb201301261605110648', N'huodong', 3, 2, N'admin', N'201301261605110648', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (21, N'mpb201301281633310078', N'123', 1, 2, N'admin', N'201301281633310078', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (22, N'mpb201303031355310878', N'huodong1', 1, 2, N'admin', N'201303031355310878', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (23, N'mpb201303031359450522', N'zaopianyi1', 2, 2, N'admin', N'201303031359450522', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (24, N'mpb201303031402250314', N'埃保常', 4, 2, N'admin', N'201303031402250314', 0, 1)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (25, N'mpb201303080941290515', N'abc', 1, 2, N'admin', N'201303080941290515', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (26, N'mpb201303111925210703', N'1', 1, 2, N'admin', N'201303111925210703', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (27, N'mpb201303130859510703', N'1', 2, 0, N'admin', N'201303130859510703', 1, 1)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (28, N'mpb201303130905590781', N'2', 1, 2, N'admin', N'201303130905590781', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (29, N'mpb201303130907020156', N'111', 2, 2, N'admin', N'201303130907020156', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (30, N'mpb201303130907540312', N'123', 4, 1, N'admin', N'201303130907540312', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (31, N'mpb201303191446490703', N'qw', 1, 0, N'admin', N'201303191446490703', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (32, N'mpb201303210959000875', N'dd', 1, 2, N'admin', N'201303210959000875', 0, 0)
INSERT [dbo].[DT_MarketPromotionBaseInfo] ([id], [code], [promotionName], [promotionType], [status], [creator], [createtime], [allSaleUnit], [allArea]) VALUES (33, N'mpb201303281917460235', N'ccc', 2, 1, N'admin', N'201303281917460235', 1, 1)
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionBaseInfo] OFF
/****** Object:  Table [dbo].[DT_PhoneMessageTemplate]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_PhoneMessageTemplate](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[languageCode] [varchar](32) NULL,
	[title] [varchar](250) NULL,
	[type] [int] NULL,
	[content] [text] NULL,
	[status] [int] NULL,
	[useTime] [varchar](250) NULL,
 CONSTRAINT [PK_DT_PHONEMESSAGETEMPLATE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_PhoneMessageTemplate] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_PhoneMessageTemplate] ON
INSERT [dbo].[DT_PhoneMessageTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime]) VALUES (1, N'lag201301070146010554', N'中文_密码短信_2', 2, N'中文_密码短信中文_密码短信中文_密码短信', 1, N'2013-01-14')
INSERT [dbo].[DT_PhoneMessageTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime]) VALUES (2, N'lag201301070146010554', N'英文_付款_短信', 6, N'英文_付款_短信', 0, NULL)
INSERT [dbo].[DT_PhoneMessageTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime]) VALUES (3, N'lag201301070146010554', N'中文_密码短信_1', 2, N'中文_密码短信_1', 0, N'2013-01-14')
SET IDENTITY_INSERT [dbo].[DT_PhoneMessageTemplate] OFF
/****** Object:  Table [dbo].[DT_PhoneMessageCfg]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_PhoneMessageCfg](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[config] [ntext] NULL,
	[biref] [ntext] NULL,
	[status] [int] NULL,
	[useTime] [varchar](250) NULL,
 CONSTRAINT [PK_DT_PHONEMESSAGECFG] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_PhoneMessageCfg] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_PhoneMessageCfg] ON
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (1, N'asdf', N'22', N'112222', 1, NULL)
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (3, N'短信1', N'木木木木木木要', N'木木木木木木要木木木木木木要', 0, NULL)
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (10, N'11111', N'1111', N'111', 0, NULL)
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (5, N'名称1', N'手机短信配置内容', N'手机短信配置内容描述sss', 0, NULL)
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (6, N'dfad', N'DDDDDDDD!@#$%^                 ', N'                      ', 0, NULL)
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (7, N'POOI098-89-09=-0-=', N'            JHJHJK', N'HHJKKH', 0, NULL)
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (8, N'8UJOJKL', N'JHJ                                            LKHKJH', N'JKK ', 0, NULL)
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (9, N'AAAAAAAA', N'FFFFFFFF', N'', 0, NULL)
INSERT [dbo].[DT_PhoneMessageCfg] ([id], [name], [config], [biref], [status], [useTime]) VALUES (11, N'2', N'2', N'2', 1, NULL)
SET IDENTITY_INSERT [dbo].[DT_PhoneMessageCfg] OFF
/****** Object:  Table [dbo].[DT_PaymentLog]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_PaymentLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[orderCode] [varchar](32) NULL,
	[orderType] [int] NULL,
	[pruductType] [varchar](32) NULL,
	[softType] [varchar](32) NULL,
	[softLanguage] [varchar](32) NULL,
	[softArea] [varchar](32) NULL,
	[saleCode] [varchar](32) NULL,
	[payTime] [varchar](250) NULL,
	[paymen] [decimal](20, 2) NULL,
	[menmberCode] [int] NULL,
	[menberType] [int] NULL,
 CONSTRAINT [PK_DT_PAYMENTLOG] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_PaymentLog] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_PaymentLog] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pyl' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_PaymentLog', @level2type=N'COLUMN',@level2name=N'code'
GO
/****** Object:  Table [dbo].[DT_PaymentCfg]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_PaymentCfg](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[config] [ntext] NULL,
	[biref] [ntext] NULL,
	[pay_fee] [decimal](20, 2) NULL,
	[type] [varchar](255) NULL,
 CONSTRAINT [PK_DT_PAYMENTCFG] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_PaymentCfg] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_PaymentCfg] ON
INSERT [dbo].[DT_PaymentCfg] ([id], [name], [config], [biref], [pay_fee], [type]) VALUES (6, N'汇率配置', N'{"rate":"6.5"}', N'', NULL, N'ratePlugin')
INSERT [dbo].[DT_PaymentCfg] ([id], [name], [config], [biref], [pay_fee], [type]) VALUES (4, N'支付宝即时到帐接口', N'{"callback_encoding":"UTF-8","return_encoding":"UTF-8","partner":"2088901200022525","seller_email":"LeiJingLing@auteltech.net","key":"ulmkoeztguzkjndf6gucjf8k7176rjs1"}', N'&lt;p>
	支付宝即时支付&lt;/p>
', NULL, N'alipayDirectPlugin')
SET IDENTITY_INSERT [dbo].[DT_PaymentCfg] OFF
/****** Object:  Table [dbo].[DT_PayLog]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_PayLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[out_trade_no] [varchar](32) NOT NULL,
	[trade_no] [varchar](32) NOT NULL,
	[trade_status] [varchar](50) NOT NULL,
	[result] [int] NOT NULL,
 CONSTRAINT [PK_DT_PayLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_PayLog] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_PayLog] ON
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (1, N'ori201303281803170667', N'2013032845899753', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (2, N'ori201303281850550974', N'2013032845989953', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (3, N'ori201303281850550974', N'2013032845989953', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (4, N'ori201303281537160165', N'2013032845580853', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (5, N'ori201303281734000503', N'2013032845856653', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (6, N'ori201303281749420662', N'2013032845874053', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (7, N'ori201303281850550974', N'2013032845989953', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (8, N'ori201303281803170667', N'2013032845899753', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (9, N'ori201303281646140350', N'2013032845737353', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (10, N'ori201303281850550974', N'2013032845989953', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (11, N'ori201303281652360767', N'2013032845752353', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (12, N'ori201303281654590812', N'2013032845758153', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (13, N'ori201303281734000503', N'2013032845856653', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (14, N'ori201303281749420662', N'2013032845874053', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (15, N'ori201303281801310856', N'2013032845895453', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (16, N'ori201303281803170667', N'2013032845899753', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (17, N'ori201303281805370040', N'2013032845903653', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (18, N'ori201303281850550974', N'2013032845989953', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (19, N'ori201303281521300052', N'2013032845550253', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (20, N'ori201303281526410314', N'2013032845556253', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (21, N'ori201303281530530545', N'2013032845566353', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (22, N'ori201303281537160165', N'2013032845580853', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (23, N'ori201303281646140350', N'2013032845737353', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (24, N'ori201303281652360767', N'2013032845752353', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (25, N'ori201303281654590812', N'2013032845758153', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (26, N'ori201303281734000503', N'2013032845856653', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (27, N'ori201303281749420662', N'2013032845874053', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (28, N'ori201303281801310856', N'2013032845895453', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (29, N'ori201303281803170667', N'2013032845899753', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (30, N'ori201303281805370040', N'2013032845903653', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (31, N'ori201303281850550974', N'2013032845989953', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (32, N'ori201303281521300052', N'2013032845550253', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (33, N'ori201303281526410314', N'2013032845556253', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (34, N'ori201303281530530545', N'2013032845566353', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (35, N'ori201303281537160165', N'2013032845580853', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (36, N'ori201303281734000503', N'2013032845856653', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (37, N'ori201303281749420662', N'2013032845874053', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (38, N'ori201303281801310856', N'2013032845895453', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (39, N'ori201303281803170667', N'2013032845899753', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (40, N'ori201303281805370040', N'2013032845903653', N'TRADE_SUCCESS', 0)
INSERT [dbo].[DT_PayLog] ([id], [out_trade_no], [trade_no], [trade_status], [result]) VALUES (41, N'ori201303281850550974', N'2013032845989953', N'TRADE_SUCCESS', 0)
SET IDENTITY_INSERT [dbo].[DT_PayLog] OFF
/****** Object:  Table [dbo].[DT_MessageType]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MessageType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
 CONSTRAINT [PK_DT_MESSAGETYPE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MessageType] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_MessageType] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ntt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_MessageType', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_MessageType] ON
INSERT [dbo].[DT_MessageType] ([id], [code], [name]) VALUES (1, N'mst201301140124030006', N'公司新闻')
INSERT [dbo].[DT_MessageType] ([id], [code], [name]) VALUES (2, N'mst201301140125160639', N'公告')
SET IDENTITY_INSERT [dbo].[DT_MessageType] OFF
/****** Object:  Table [dbo].[DT_LanguageConfig]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_LanguageConfig](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
 CONSTRAINT [PK_DT_LANGUAGECONFIG] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_LanguageConfig] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_LanguageConfig] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'lcg' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_LanguageConfig', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_LanguageConfig] ON
INSERT [dbo].[DT_LanguageConfig] ([id], [code], [name]) VALUES (4, N'lcf201301070156330030', N'中文')
INSERT [dbo].[DT_LanguageConfig] ([id], [code], [name]) VALUES (5, N'lcf201304061806050273', N'英文')
SET IDENTITY_INSERT [dbo].[DT_LanguageConfig] OFF
/****** Object:  Table [dbo].[DT_Language]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_Language](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[countryCode] [varchar](20) NULL,
	[languageCode] [varchar](20) NULL,
 CONSTRAINT [PK_DT_LANGUAGE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_Language] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_Language] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'lag' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_Language', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_Language] ON
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (2, N'lag201301070020090542', N'英文', N'US', N'en')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (3, N'lag201301070146010554', N'简体中文', N'CN', N'zh')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (4, N'lag201301261639170385', N'繁体中文', N'HK', N'hk')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (12, N'lag201303212159590222', N'西班牙语', N'ES', N'es')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (13, N'lag201303212200140379', N'葡萄牙语', N'PT', N'pt')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (14, N'lag201303212200420925', N'德语', N'DE', N'de')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (15, N'lag201303212201380190', N'法语', N'FR', N'fr')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (16, N'lag201303212202100971', N'荷兰语', N'NL', N'nl')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (17, N'lag201304061805000399', N'波兰语', N'PL', N'pl')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (18, N'lag201304061805120127', N'俄语', N'RU', N'ru')
INSERT [dbo].[DT_Language] ([ID], [code], [name], [countryCode], [languageCode]) VALUES (19, N'lag201304061805270583', N'日语', N'JP', N'jp')
SET IDENTITY_INSERT [dbo].[DT_Language] OFF
/****** Object:  Table [dbo].[DT_EmailTemplate]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_EmailTemplate](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[languageCode] [varchar](32) NULL,
	[title] [varchar](32) NULL,
	[type] [int] NULL,
	[content] [text] NULL,
	[status] [int] NULL,
	[useTime] [varchar](25) NULL,
	[code] [varchar](32) NULL,
 CONSTRAINT [PK_DT_EMAILTEMPLATE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_EmailTemplate] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_EmailTemplate] ON
INSERT [dbo].[DT_EmailTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime], [code]) VALUES (1, N'lag201301070020090542', N'用户注册', 1, N'&lt;p>
	您好：&lt;strong>${autelId}&lt;/strong>&lt;/p>
&lt;p>
	&nbsp;&nbsp;&nbsp; 谢谢您注册道通会员，请立即激活您的账户：&lt;br />
	&lt;br />
	&nbsp;&nbsp;&nbsp;&lt;span style="color:#ff0000;"> ${activeUrl}&lt;/span>&lt;br />
	&lt;br />
	&nbsp;&nbsp;&nbsp; 基于安全考虑，以上链接24小时内有效，并在成功激活后立即失效！&lt;/p>
', 0, N'2013-03-19', N'etp201301140020090542')
INSERT [dbo].[DT_EmailTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime], [code]) VALUES (3, N'lag201301070146010554', N'付款_中文', 4, N'<p>
	付款_中文付款_中文付款_中文</p>
', 1, N'2013-03-29', N'etp201301140020093523')
INSERT [dbo].[DT_EmailTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime], [code]) VALUES (5, N'lag201301070146010554', N'注册_中文_01', 1, N'<p>
	注册_中文_01注册_中文_01注册_中文_01</p>
', 1, N'2013-03-05', N'etp201301140022521341')
INSERT [dbo].[DT_EmailTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime], [code]) VALUES (6, N'lag201301070020090542', N'密码找回邮件', 2, N'&lt;p>
	您好：${autelId}&lt;/p>
&lt;p>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please click on this link to reset your password and activate your account.&lt;br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${resetPasswordUrl}&lt;/p>
&lt;p>
	&nbsp;&lt;/p>
&lt;p>
	基于安全考虑，以上链接24小时内有效，并在成功设置后立即失效！&lt;/p>
&lt;p>
	&nbsp;&lt;/p>
', 1, NULL, N'etp')
INSERT [dbo].[DT_EmailTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime], [code]) VALUES (7, N'lag201301070020090542', N'客诉回复_英文', 1, N'&lt;p>
	${autelId},&lt;/p>
&lt;p>
	Hello,&lt;/p>
&lt;p>
	&lt;span style="color:#ff0000;">${reContent}&lt;/span>&lt;br />
	&lt;br />
	${rePerson}&lt;br />
	&lt;br />
	HELP&nbsp;US&nbsp;HELP&nbsp;YOU:&nbsp;If&nbsp;you&nbsp;are&nbsp;not&nbsp;satisfied&nbsp;with&nbsp;this&nbsp;response,&nbsp;please&nbsp;forward&nbsp;this&nbsp;e-mail&nbsp;to&nbsp;our&nbsp;quality&nbsp;assurance&nbsp;department&nbsp;at&nbsp;&lt;a href="mailto:support@auteltech.com">support@auteltech.com&lt;/a>.&lt;br />
	&lt;br />
	NOTE:&nbsp;Is&nbsp;this&nbsp;an&nbsp;urgent&nbsp;request?&nbsp;You&nbsp;may&nbsp;wish&nbsp;to&nbsp;contact&nbsp;us&nbsp;by&nbsp;calling&nbsp;1-888-927-8493&nbsp;or&nbsp;starting&nbsp;a&nbsp;live&nbsp;chat&nbsp;at&nbsp;&lt;a href="http://www.auteltech.com/">http://www.auteltech.com&lt;/a>&nbsp;.&nbsp;You&nbsp;can&nbsp;view&nbsp;a&nbsp;history&nbsp;of&nbsp;your&nbsp;tickets&nbsp;by&nbsp;logging&nbsp;in&nbsp;at&nbsp;&lt;a href="http://support.auteltech.com/">http://support.auteltech.com&lt;/a>&nbsp;&lt;br />
	&lt;br />
	Ticket&nbsp;Details&lt;/p>
&lt;p>
	&nbsp;&lt;/p>
&lt;p>
	Ticket&nbsp;ID: ${customerComplaintCode}&lt;br />
	Department:&nbsp;Support&lt;br />
	Status:&nbsp;Awaiting&nbsp;Customer&nbsp;Reply&lt;/p>
', 1, N'2013-03-29', N'etp')
INSERT [dbo].[DT_EmailTemplate] ([id], [languageCode], [title], [type], [content], [status], [useTime], [code]) VALUES (8, N'lag201301070020090542', N'新增客诉-英文', 7, N'&lt;p>
	${autelId}&lt;/p>
&lt;p>
	Thank you for contacting us. Your ticket is currently in the &quot;General&quot; queue.&lt;/p>
&lt;p>
	For your records, the details of the ticket are listed below. When replying, please make sure that the ticket ID is kept in the subject line to ensure that your replies are tracked appropriately.&lt;br />
	&lt;br />
	&lt;strong>Ticket ID: &lt;/strong>${ticketId}&lt;br />
	&lt;strong>标题: &lt;/strong>${subject}&lt;br />
	&lt;strong>客诉分类: &lt;/strong>${complaintType}&lt;br />
	&lt;br />
	You can check the status of or reply to this ticket online at: &lt;a href="https://support.auteltech.com/index.php?/Tickets/Ticket/View/PMH-187-76346">https://support.auteltech.com/index.php?/Tickets/Ticket/View/PMH-187-76346&lt;/a>&lt;/p>
&lt;p>
	&nbsp;&lt;/p>
&lt;p>
	&nbsp;&lt;/p>
&lt;p>
	Kind regards,&lt;/p>
&lt;p>
	&lt;em>NOTE:&lt;/em> Is this an urgent request? You may wish to contact us by calling 1-888-927-8493 or starting a live chat at &lt;a href="http://www.auteltech.com">http://www.auteltech.com&lt;/a>. Please feel free to email us at support@auteltech.com if you feel that you aren&#39;t receiving a proper/timely response to your support request.&lt;br />
	You can view a history of your tickets by logging in at &lt;a href="http://www.auteltech.com">http://www.auteltech.com&lt;/a>.&lt;br />
	Autel&lt;/p>
', 1, N'2013-03-11', N'etp')
SET IDENTITY_INSERT [dbo].[DT_EmailTemplate] OFF
/****** Object:  Table [dbo].[DT_DataModel]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_DataModel](
	[model_id] [int] IDENTITY(1,1) NOT NULL,
	[china_name] [varchar](255) NULL,
	[english_name] [varchar](255) NULL,
	[add_time] [bigint] NULL,
	[project_name] [varchar](255) NULL,
	[brief] [varchar](400) NULL,
	[if_audit] [int] NULL,
 CONSTRAINT [PK_DT_DATAMODEL] PRIMARY KEY NONCLUSTERED 
(
	[model_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_DataModel] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:否,1是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_DataModel', @level2type=N'COLUMN',@level2name=N'if_audit'
GO
/****** Object:  Table [dbo].[DT_DataField]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_DataField](
	[field_id] [int] IDENTITY(1,1) NOT NULL,
	[china_name] [varchar](255) NULL,
	[english_name] [varchar](255) NULL,
	[data_type] [int] NULL,
	[data_size] [varchar](20) NULL,
	[show_form] [varchar](255) NULL,
	[show_value] [varchar](400) NULL,
	[add_time] [bigint] NULL,
	[model_id] [int] NULL,
	[save_value] [varchar](1) NULL,
	[is_validate] [int] NULL,
	[taxis] [int] NULL,
	[dict_id] [int] NULL,
	[is_show] [int] NULL,
 CONSTRAINT [PK_DT_DATAFIELD] PRIMARY KEY NONCLUSTERED 
(
	[field_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_DataField] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:字符串
   2:整数
   3:浮点数
   4:文本
   5:日期
   6:图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_DataField', @level2type=N'COLUMN',@level2name=N'data_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:文本域
   2:文本区域
   3:单选按钮
   4:复选框
   5:列表菜单
   6:FCK文本编辑器
   7:图片 
   8:日期
   9:颜色选择器
   10:数据字典' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_DataField', @level2type=N'COLUMN',@level2name=N'show_form'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'逗号隔开' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_DataField', @level2type=N'COLUMN',@level2name=N'show_value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'逗号隔开,与展现值要品要匹配' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_DataField', @level2type=N'COLUMN',@level2name=N'save_value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0为不显示
   1为显示' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_DataField', @level2type=N'COLUMN',@level2name=N'is_show'
GO
/****** Object:  Table [dbo].[DT_DataCat]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_DataCat](
	[cat_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[parent_id] [int] NULL,
	[cat_path] [varchar](255) NULL,
	[cat_order] [int] NULL,
	[model_id] [int] NULL,
	[if_audit] [int] NULL,
	[url] [varchar](255) NULL,
	[detail_url] [varchar](255) NULL,
	[tositemap] [int] NULL,
 CONSTRAINT [PK_DT_DATACAT] PRIMARY KEY NONCLUSTERED 
(
	[cat_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_DataCat] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0：否，1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_DataCat', @level2type=N'COLUMN',@level2name=N'if_audit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0：否，1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_DataCat', @level2type=N'COLUMN',@level2name=N'tositemap'
GO
/****** Object:  Table [dbo].[DT_AreaConfig]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_AreaConfig](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
 CONSTRAINT [PK_DT_AREACONFIG] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_AreaConfig] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_AreaConfig] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'arc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_AreaConfig', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_AreaConfig] ON
INSERT [dbo].[DT_AreaConfig] ([id], [code], [name]) VALUES (2, N'acf201301080125020811', N'欧洲区域')
INSERT [dbo].[DT_AreaConfig] ([id], [code], [name]) VALUES (3, N'acf201301080130400725', N'亚太区')
INSERT [dbo].[DT_AreaConfig] ([id], [code], [name]) VALUES (7, N'acf201303212143110189', N'中国')
INSERT [dbo].[DT_AreaConfig] ([id], [code], [name]) VALUES (8, N'acf201304061800510152', N'美国区域')
INSERT [dbo].[DT_AreaConfig] ([id], [code], [name]) VALUES (9, N'acf201304061801140669', N'澳洲区域')
INSERT [dbo].[DT_AreaConfig] ([id], [code], [name]) VALUES (10, N'acf201304061801440888', N'南美区域')
SET IDENTITY_INSERT [dbo].[DT_AreaConfig] OFF
/****** Object:  Table [dbo].[cop_userdetail]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cop_userdetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[bussinessscope] [ntext] NULL,
	[regaddress] [varchar](255) NULL,
	[regdate] [bigint] NULL,
	[corpscope] [int] NULL,
	[corpdescript] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[cop_userdetail] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cop_user]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cop_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NULL,
	[companyname] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[address] [varchar](255) NULL,
	[legalrepresentative] [varchar](50) NULL,
	[linkman] [varchar](50) NULL,
	[tel] [varchar](50) NULL,
	[mobile] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[logofile] [varchar](255) NULL,
	[licensefile] [varchar](255) NULL,
	[defaultsiteid] [int] NULL,
	[deleteflag] [int] NULL,
	[usertype] [int] NULL,
	[createtime] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cop_user] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[cop_user] ON
INSERT [dbo].[cop_user] ([id], [username], [companyname], [password], [address], [legalrepresentative], [linkman], [tel], [mobile], [email], [logofile], [licensefile], [defaultsiteid], [deleteflag], [usertype], [createtime]) VALUES (1, N'admin', N'单机版用户', N'21232f297a57a5a743894ae4a801fc3', N'在这里输入详细地址', NULL, N'在这里输入联系人信息', N'010-12345678', N'13888888888', N'youmail@domain.com', NULL, NULL, NULL, 0, 1, NULL)
SET IDENTITY_INSERT [dbo].[cop_user] OFF
/****** Object:  Table [dbo].[cop_sitedomain]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cop_sitedomain](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[domain] [varchar](255) NULL,
	[domaintype] [int] NULL,
	[siteid] [int] NULL,
	[userid] [int] NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cop_sitedomain] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[cop_sitedomain] ON
INSERT [dbo].[cop_sitedomain] ([id], [domain], [domaintype], [siteid], [userid], [status]) VALUES (1, N'localhost', 0, 1, 1, 0)
SET IDENTITY_INSERT [dbo].[cop_sitedomain] OFF
/****** Object:  Table [dbo].[cop_site]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cop_site](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[sitename] [varchar](255) NULL,
	[productid] [varchar](50) NULL,
	[descript] [ntext] NULL,
	[icofile] [varchar](255) NULL,
	[logofile] [varchar](255) NULL,
	[deleteflag] [int] NULL,
	[keywords] [varchar](255) NULL,
	[themepath] [varchar](50) NULL,
	[adminthemeid] [int] NULL,
	[themeid] [int] NULL,
	[point] [int] NULL,
	[createtime] [bigint] NULL,
	[lastlogin] [bigint] NULL,
	[lastgetpoint] [bigint] NULL,
	[logincount] [int] NULL,
	[bkloginpicfile] [varchar](255) NULL,
	[bklogofile] [varchar](255) NULL,
	[sumpoint] [bigint] NULL,
	[sumaccess] [bigint] NULL,
	[title] [varchar](255) NULL,
	[username] [varchar](255) NULL,
	[usersex] [int] NULL,
	[usertel] [varchar](50) NULL,
	[usermobile] [varchar](50) NULL,
	[usertel1] [varchar](50) NULL,
	[useremail] [varchar](50) NULL,
	[state] [int] NULL,
	[qqlist] [varchar](255) NULL,
	[msnlist] [varchar](255) NULL,
	[wwlist] [varchar](255) NULL,
	[tellist] [varchar](255) NULL,
	[worktime] [varchar](255) NULL,
	[siteon] [int] NULL,
	[closereson] [varchar](255) NULL,
	[copyright] [varchar](1000) NULL,
	[icp] [varchar](255) NULL,
	[address] [varchar](255) NULL,
	[zipcode] [varchar](50) NULL,
	[qq] [int] NULL,
	[msn] [int] NULL,
	[ww] [int] NULL,
	[tel] [int] NULL,
	[wt] [int] NULL,
	[linkman] [varchar](255) NULL,
	[linktel] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[multi_site] [smallint] NULL,
	[relid] [varchar](255) NULL,
	[sitestate] [smallint] NULL,
	[isimported] [smallint] NULL,
	[imptype] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[cop_site] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[cop_site] ON
INSERT [dbo].[cop_site] ([id], [userid], [sitename], [productid], [descript], [icofile], [logofile], [deleteflag], [keywords], [themepath], [adminthemeid], [themeid], [point], [createtime], [lastlogin], [lastgetpoint], [logincount], [bkloginpicfile], [bklogofile], [sumpoint], [sumaccess], [title], [username], [usersex], [usertel], [usermobile], [usertel1], [useremail], [state], [qqlist], [msnlist], [wwlist], [tellist], [worktime], [siteon], [closereson], [copyright], [icp], [address], [zipcode], [qq], [msn], [ww], [tel], [wt], [linkman], [linktel], [email], [multi_site], [relid], [sitestate], [isimported], [imptype]) VALUES (1, 1, N'道通产品网站', N'company', N'道通产品网站', N'http://localhost:8080/javashop/statics/images/default/favicon.ico', N'http://localhost:8080/javashop/statics/images/default/logo.gif', 0, N'道通产品网站', N'autel', 1, 7, 1000, 1352900016, 1365316189, 0, 38, NULL, NULL, 0, 0, N'道通产品网站', N'admin', 1, N'', N'', N'', N'88@88.com', 0, N'', N'', N'', N'', N'', 0, N'', N'道通产品网站', N'', N'admin', N'888888', 0, 0, 0, 0, 0, N'888888', N'888888', N'88@88.com', 0, NULL, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[cop_site] OFF
/****** Object:  Table [dbo].[cop_reply]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cop_reply](
	[replyid] [int] IDENTITY(1,1) NOT NULL,
	[askid] [int] NULL,
	[content] [ntext] NULL,
	[username] [varchar](255) NULL,
	[dateline] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[replyid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[cop_reply] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cop_data_log]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cop_data_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [ntext] NULL,
	[url] [varchar](255) NULL,
	[pics] [ntext] NULL,
	[sitename] [varchar](255) NULL,
	[domain] [varchar](255) NULL,
	[logtype] [varchar](50) NULL,
	[optype] [varchar](50) NULL,
	[dateline] [int] NULL,
	[userid] [bigint] NULL,
	[siteid] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[cop_data_log] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[cop_data_log] ON
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (1, N'id:57<br>cat_id:24<br>content:<p>
	111111111111111</p>
<br>title:1111111111111<br>sort:1<br>page_description:<br>page_keywords:<br>page_title:1<br>', NULL, N'fs:/attachment/cms/201301150032436257.jpg|fs:/attachment/cms/201301150032436257.jpg', N'道通产品网站', N'localhost', N'文章', N'修改', 1358181177, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (2, N'sort:3<br>img:http://www.baidu.com/img/baidu_sylogo1.gif<br>page_keywords:222222222222<br>content:<p>
	22222222222222</p>
<br>cat_id:20<br>id:58<br>language_code:lag201301070146010554<br>title:222222222222222<br>page_description:22222222<br>page_title:222222222222<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1358257225, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (3, N'sort:33<br>img:http://www.baidu.com/img/baidu_sylogo1.gif<br>page_keywords:222222222222<br>content:<p>
	222222222222222222</p>
<br>cat_id:20<br>id:59<br>language_code:lag201301070020090542<br>title:222222222222222<br>page_description:22222<br>page_title:222222222222<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1358257265, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (4, N'sort:34543<br>img:http://<br>page_keywords:<br>content:<br>cat_id:20<br>id:60<br>language_code:lag201301070020090542<br>title:435345<br>page_description:<br>page_title:<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1358762811, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (5, N'sort:0<br>img:<br>page_keywords:<br>content:<p>
	342234234234</p>
<br>cat_id:3<br>id:61<br>language_code:lag201301070020090542<br>title:234234234<br>page_description:<br>page_title:<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1358763767, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (6, N'sort:0<br>img:<br>page_keywords:<br>content:<p>
	2222</p>
<br>cat_id:26<br>id:62<br>language_code:lag201301070020090542<br>title:222<br>page_description:<br>page_title:<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1363702742, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (7, N'sort:10<br>img:<br>page_keywords:1000亿美元 外汇储备<br>content:<p>
	央视3月27日《东方时空》节目播出&ldquo;祖马证实组建金砖银行将投资4.5万亿美元&rdquo;，以下为文字实录：</p>
<p>
	张羽：董倩，我们刚刚知道，会议目前已经就金砖国家开发银行达成了初步的意向，我们也想知道，要真正建立起金砖银行还面临着很多问题，他具体落实还有多远？</p>
<p>
	董倩：昨天我记得我们两个连线的过程中也说到了德班在外面正在下着大雨，今天的报纸也把这个大雨和这次德班会议上的一系列协议联系了起来，一天半的 时间非常有限，但是多边和双边的协议签署了很多，今天早上有报纸说，这些协议就好像昨天的大雨一样多，但是大家最关注的能不能成立金砖银行，细节如何落 实，以及如何到底能不能成立这个外汇资金储备库的问题，应该说直到我们刚才还在听的领导人记者会之前还没有一个到底怎么样的这么一个确切的说法。</p>
<p>
	我告诉你一个非常有意思的信息，今天早上我们刚刚报了国际会议中心有一位中国代表团的成员，因为他早上起来参与了今天的早餐会，他说，俄罗斯总统普 京在早餐会上释放了这样的一个信息，普京总统的原话是这样的，今天在德班峰会上，我们将正式签署成立开发银行的协议，他的这样的一个释放信息应该说是让很 多人都有点出乎意料。</p>
<p>
	在刚才大概20分钟以前，祖马总统在领导人会议结束之后的记者招待会上证实了这个消息，他说，<strong>未来五年金砖五国将会有4.5万亿美元的投资，主要是花在五国还有其他发展中国家的基础设施建设上</strong>。应该说他们都是大人物，为什么这么说？因为大人物做的是求同的事情，同有了，接下来还会有一些分歧，也就是意，这些意接下来就要这些财长和央行行长的去面对了。</p>
<p>
	我们在采访的过程中注意到，俄罗斯财长说了这么一番话，他说，对于细节的敲定我们会加快进程，在今年4月份的G20峰会上我们会继续讨论，印度的财长说，出资选址这个问题一年以后才会定下，而且资金人员治理结构这些问题非常复杂也非常细致，我们要很谨慎的去面对。</p>
<p>
	我们就看为什么这个问题这么难达成一个共识，现在我们看到有些外媒说，对于金砖银行到底启动资金是多少？有外媒报道说，俄罗斯认为启动资金应该每一 个国家都相同，就是100亿美元这样的出资，但是你知道五个国家经济体量是非常不一样的，中国的经济体量是南非的20倍，是俄罗斯和巴西的4倍，在这个问 题上是不是每个国家出资都相同，因此在这个问题上各方是存在分歧的。</p>
<p>
	具体还有一些关于外汇储备库的建立，刚才祖马总统也说，他们已经初步达成了一个共识，就是在未来要推进这个资金储备库的建设，<strong>未来几年要形成一个1000亿美元的资金池。同样外媒也把这个问题细致了一下，比如说中国将在其中占到410亿美元的出资，南非至少是50亿美元，其他三国都各占180亿美元</strong>，当然这个消息还没有得到证实。</p>
<br>cat_id:27<br>id:63<br>language_code:lag201301070146010554<br>title:外汇储备库总规模1000亿美元 中国或出资410亿美元<br>page_description:张羽：董倩，我们刚刚知道，会议目前已经就金砖国家开发银行达成了初步的意向，我们也想知道，要真正建立起金砖银行还面临着很多问题，他具体落实还有多远？<br>page_title:祖马证实组建金砖银行将投资4.5万亿美元<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1364396275, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (8, N'sort:10<br>img:<br>page_keywords:习近平<br>content:<p>
	　原标题：中俄之间的特殊&ldquo;熊抱&rdquo;</p>
<p>
	　　一天半时间，约20场正式活动，习近平说，他是&ldquo;累并快乐着&rdquo;。中国新任国家元首将首访定在俄罗斯，以及俄方做出的种种安排，都在凸显中俄关系的高水平和特殊性</p>
<p>
	　　本刊记者/田冰(发自莫斯科)</p>
<p>
	　　当地时间3月22日11时55分许，中国国家主席习近平乘坐的专机抵达莫斯科伏努科沃2号机场。习近平和夫人彭丽媛受到俄远东地区发展部长伊沙耶夫的热情迎接。俄政府在机场举行了迎接仪式，习近平检阅了俄三军仪仗队，并观看了分列式。</p>
<p>
	　　按外交礼仪惯例，国事访问，东道主国家会派出部长或副部长等高官到机场接机。俄方派伊沙耶夫接机，或许暗含深意。</p>
<p>
	　　2010年，同样是早春时节，时任国家副主席习近平将访俄第一站选择在俄罗斯远东地区的滨海边疆区。伊沙耶夫长期在远东地区任要职，目前担任俄 远东地区发展部长，同时兼任俄总统驻远东联邦区全权代表。而中国东北地区与俄罗斯远东及东西伯利亚地区合作，是习近平此访重要议题之一。</p>
<p>
	　　&ldquo;他希望能与习近平性格相似&rdquo;</p>
<p>
	　　俄罗斯主要市场化报纸之一《生意人报》刊登了标题为&ldquo;中国&mdash;克里姆林宫仪式&rdquo;的文章。该报在描述&ldquo;习普会&rdquo;时透露，普京在克里姆林宫接待中国领 导人，这本身就是一种极高的礼遇。因为一段时间以来，普京越来越少、而且只在极为必要的情况下，才会离开新奥加廖沃。新奥加廖沃是俄罗斯总统郊外官邸，距 离莫斯科数十公里。</p>
<p>
	　　&ldquo;但昨日(3月22日)，除了向无名烈士墓献花活动外，与中国国家主席到访有关的所有活动都安排在克里姆林宫举行。正式欢迎仪式在大克里姆林宫最有名的乔治大厅举行，欢迎仪式和迎接阵容非常豪华，就像金碧辉煌的乔治大厅本身一样。&rdquo;</p>
<p>
	　　据《中国新闻周刊》记者观察，俄方参加欢迎仪式和会谈的政府官员约有30人，其中有3名副总理(戈洛杰茨、德沃尔科维奇、罗戈津)、总统助理乌 沙科夫和外交、国防、经济、农业、能源等各部部长。此外，由上届政府副总理转任俄罗斯石油公司总裁的谢钦、俄天然气工业股份公司总裁米勒等大公司高层也位 列其中。这在俄方接待外国元首历史上极为少见。</p>
<p>
	　　在《生意人报》笔下，习近平给人留下的印象是：&ldquo;身形略显壮硕，衣着非常得体&rdquo;，&ldquo;脸上写满温厚与友善&rdquo;。</p>
<p>
	　　在会见中，习近平回忆与普京的交往，&ldquo;3年前我应你的邀请首次访俄，那次访问给我留下了非常美好和深刻的印象。去年6月，我们又在北京再次相见。此外，我们还数次相互通信，不久前我们进行了电话交谈。&rdquo;</p>
<p>
	　　当习近平说他们两人&ldquo;很能谈得来，性格很相似&rdquo;时，普京会心一笑。</p>
<p>
	　　&ldquo;他(普京)希望能与习近平性格相似。&rdquo;《生意人报》写道。</p>
<p>
	　　除了最高规格的礼宾安排外，俄方在活动项目的选择上也做足了功课，有些活动是普京亲自主动提议的，如邀请习近平参观俄国防部，会见俄罗斯汉学 家、学习汉语的学生和媒体代表等。这些安排，除了体现俄方欲显示&ldquo;最大诚意&rdquo;之外，也彰显中俄关系的高水平和特殊性，以及两国最高层之间的默契和友好。</p>
<p>
	　　3月23日下午3时许，习近平抵达俄国防部大楼前，俄国防部长绍伊古在此迎接，并举行了隆重的欢迎仪式。这是一次非常特殊的安排&mdash;&mdash;习近平是第一位到访俄国防部并参观俄联邦武装力量作战指挥中心的外国元首。</p>
<p>
	　　在俄联邦武装力量作战指挥中心，正在指挥中心值勤的俄陆海空军军官全体起立，向习近平敬礼。俄军总参谋长格拉西莫夫向习近平报告说，&ldquo;尊敬的主席先生，您是我们为之打开俄联邦武装力量作战指挥中心这扇门的首位外国领导人。&rdquo;</p>
<p>
	　　一天半时间，约20场正式活动。习近平同普京举行会谈并共同出席俄罗斯中国旅游年开幕式，会见梅德韦杰夫总理、联邦委员会主席马特维延科以及国 家杜马主席纳雷什金等俄方领导人，在莫斯科国际关系学院发表演讲，出席中共六大会址纪念馆建馆启动仪式，参观俄国防部，会见俄罗斯汉学家、学习汉语的学生 及媒体代表。双方发表由两国元首签署的联合声明，并共同见证双方经贸、能源、投资、地方、人文、环保等诸多领域达成合作协议的签字仪式。</p>
<p>
	　　《中国新闻周刊》记者观察到，甫下飞机，从当地时间22日下午3点多到晚上10点40分，习近平访俄首日与普京共同出席活动持续了7个多小时。 &ldquo;会谈非常富有成效，取得了巨大成功。&rdquo;中国驻俄使馆一位工作人员告诉《中国新闻周刊》。而因为会谈时间拉长，以至于当晚在克里姆林宫大礼堂举行的俄罗斯 中国旅游年开幕式比原定时间推迟了近两小时。</p>
<p>
	　　习近平一行访俄的次日，一天的日程从早晨9点开始，至晚上会见俄罗斯汉学家、学习汉语的学生及媒体代表为结束。最后这一项活动持续至晚上10点多，比预计时间又延长了40多分钟。</p>
<p>
	　　据中国驻俄使馆工作人员透露，几乎每场活动都有延长，但是因行程安排紧密，能压缩的只有中间非常短暂的休息时间。使馆方面曾考虑到行程安排太密集，根本没有时间休息，欲压缩一些活动的时间，&ldquo;但是习近平主席一点都不打折扣&rdquo;。</p>
<br>cat_id:28<br>id:64<br>language_code:lag201301070020090542<br>title:习近平谈访俄:不是痛并快乐着 是累并快乐着<br>page_description:一天半时间，约20场正式活动，习近平说，他是“累并快乐着”。中国新任国家元首将首访定在俄罗斯，以及俄方做出的种种安排，都在凸显中俄关系的高水平和特殊性<br>page_title:中俄之间的特殊“熊抱”<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1364490745, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (9, N'sort:10<br>img:<br>page_keywords:<br>content:<dl>
	<dt>
		<strong>Q:</strong> download a model upgrade package the upgrade tool shows don&#39;t come out, how should be thandI do?<span class="cus_span">[2013-01-10 16:30:11]</span></dt>
	<dt>
		<strong>Answer:</strong> the possible reasons and the corresponding solution has the following two kinds: 1, the model upgrade procedures and other models to upgrade procedure is not the same position, copy to the same position can; 2, the model upgrade program download no t completely, can download again.</dt>
</dl>
<br>cat_id:11<br>id:65<br>language_code:lag201301070146010554<br>title:支付帮助<br>page_description:<br>page_title:<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1364491677, 1, 1)
INSERT [dbo].[cop_data_log] ([id], [content], [url], [pics], [sitename], [domain], [logtype], [optype], [dateline], [userid], [siteid]) VALUES (10, N'sort:0<br>img:<br>page_keywords:sadff<br>content:<p>
	sssssssssssssssssssssssssssssssssssssssssssssssssssss</p>
<br>cat_id:27<br>id:66<br>language_code:lag201301070020090542<br>title:ssssssssssssssssssssssssssssssss<br>page_description:sdfdsafasdfsadfsa<br>page_title:adsf<br>', NULL, N'', N'道通产品网站', N'localhost', N'文章', N'修改', 1364821132, 1, 1)
SET IDENTITY_INSERT [dbo].[cop_data_log] OFF
/****** Object:  Table [dbo].[cop_ask]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cop_ask](
	[askid] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NULL,
	[content] [ntext] NULL,
	[dateline] [int] NULL,
	[isreply] [smallint] NULL,
	[userid] [int] NULL,
	[siteid] [int] NULL,
	[domain] [varchar](255) NULL,
	[sitename] [varchar](255) NULL,
	[username] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[askid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[cop_ask] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cop_app]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cop_app](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[appid] [varchar](50) NULL,
	[app_name] [varchar](50) NULL,
	[author] [varchar](50) NULL,
	[descript] [ntext] NULL,
	[deployment] [int] NULL,
	[path] [varchar](255) NULL,
	[authorizationcode] [varchar](50) NULL,
	[installuri] [varchar](255) NULL,
	[deleteflag] [int] NULL,
	[version] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[cop_app] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[cop_app] ON
INSERT [dbo].[cop_app] ([id], [appid], [app_name], [author], [descript], [deployment], [path], [authorizationcode], [installuri], [deleteflag], [version]) VALUES (3, N'base', N'base应用', NULL, N'base应用', 0, N'/core', NULL, NULL, 0, N'2.2.0')
INSERT [dbo].[cop_app] ([id], [appid], [app_name], [author], [descript], [deployment], [path], [authorizationcode], [installuri], [deleteflag], [version]) VALUES (4, N'cms', N'cms应用', NULL, N'cms应用', 0, N'/cms', NULL, NULL, 0, N'2.2.0')
SET IDENTITY_INSERT [dbo].[cop_app] OFF
/****** Object:  Table [dbo].[DT_Authority]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_Authority](
	[actid] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[type] [varchar](255) NULL,
	[objvalue] [varchar](1) NULL,
 CONSTRAINT [PK_DT_AUTHORITY] PRIMARY KEY NONCLUSTERED 
(
	[actid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_Authority] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DT_AreaInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_AreaInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NOT NULL,
	[name] [varchar](250) NULL,
	[continent] [int] NULL,
 CONSTRAINT [PK_DT_AREAINFO] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_AreaInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_AreaInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'are' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_AreaInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_AreaInfo] ON
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (1, N'ain201301080109020633', N'中国', 1)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (2, N'ain201301080113090078', N'日本', 1)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (3, N'ain201301080113230346', N'美国', 4)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (4, N'ain201301080113340350', N'巴西', 3)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (5, N'ain201301080113430654', N'澳大利亚', 6)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (6, N'ain201301080113570249', N'西班牙', 3)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (7, N'ain201301080114090232', N'法国', 2)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (10, N'ain201301081156360937', N'新加坡', 1)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (18, N'ain201303211929030370', N'印度', 1)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (13, N'ain201303081553490087', N'韩国', 1)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (19, N'ain201303211929300932', N'泰国', 1)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (21, N'ain201304061756510950', N'德国', 2)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (22, N'ain201304061757020054', N'英国', 2)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (23, N'ain201304061759080264', N'俄罗斯', 2)
INSERT [dbo].[DT_AreaInfo] ([id], [code], [name], [continent]) VALUES (24, N'ain201304061802040029', N'墨西哥', 3)
SET IDENTITY_INSERT [dbo].[DT_AreaInfo] OFF
/****** Object:  Table [dbo].[DT_CustomerComplaintType]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_CustomerComplaintType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
 CONSTRAINT [PK_DT_CustomerComplaintType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_CustomerComplaintType] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DT_CustomerComplaintType] ON [dbo].[DT_CustomerComplaintType] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DT_CustomerComplaintType] ON
INSERT [dbo].[DT_CustomerComplaintType] ([id], [code]) VALUES (1, N'cut201303311910160359')
INSERT [dbo].[DT_CustomerComplaintType] ([id], [code]) VALUES (6, N'cut201304021404040240')
INSERT [dbo].[DT_CustomerComplaintType] ([id], [code]) VALUES (7, N'cut201304021421560178')
INSERT [dbo].[DT_CustomerComplaintType] ([id], [code]) VALUES (8, N'cut201304021426030053')
SET IDENTITY_INSERT [dbo].[DT_CustomerComplaintType] OFF
/****** Object:  Table [dbo].[DT_CustomerComplaintSource]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_CustomerComplaintSource](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
 CONSTRAINT [PK_DT_CustomerComplaintSource] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_CustomerComplaintSource] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DT_CustomerComplaintSource] ON [dbo].[DT_CustomerComplaintSource] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DT_CustomerComplaintSource] ON
INSERT [dbo].[DT_CustomerComplaintSource] ([id], [code], [name]) VALUES (2, N'cus201303311646450921', N'产品网站')
INSERT [dbo].[DT_CustomerComplaintSource] ([id], [code], [name]) VALUES (3, N'cus201303311647360484', N'DS-908系统')
SET IDENTITY_INSERT [dbo].[DT_CustomerComplaintSource] OFF
/****** Object:  Table [dbo].[DT_CustomerComplaintInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_CustomerComplaintInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[complaintTitle] [varchar](250) NULL,
	[complaintPerson] [varchar](250) NULL,
	[company] [varchar](250) NULL,
	[complaintDate] [varchar](250) NULL,
	[phone] [varchar](250) NULL,
	[email] [varchar](250) NULL,
	[vehicleSystem] [varchar](250) NULL,
	[vehicleType] [varchar](250) NULL,
	[diagnosticConnector] [varchar](250) NULL,
	[carYear] [varchar](250) NULL,
	[carnumber] [varchar](250) NULL,
	[engineType] [varchar](250) NULL,
	[system] [varchar](250) NULL,
	[other] [varchar](255) NULL,
	[productName] [varchar](250) NULL,
	[productNo] [varchar](250) NULL,
	[softwareName] [varchar](250) NULL,
	[softwareEdition] [varchar](250) NULL,
	[testPath] [varchar](255) NULL,
	[complaintContent] [varchar](2000) NULL,
	[emergencyLevel] [int] NULL,
	[attachment] [varchar](250) NULL,
	[complaintState] [int] NULL,
	[complaintEffective] [int] NULL,
	[userType] [int] NULL,
	[userCode] [varchar](32) NULL,
	[acceptState] [int] NULL,
	[acceptor] [varchar](32) NULL,
	[complaintType] [int] NULL,
 CONSTRAINT [PK_DT_CUSTOMERCOMPLAINTINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_CustomerComplaintInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_CustomerComplaintInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cci' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_CustomerComplaintInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_CustomerComplaintInfo] ON
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (39, N'cuc201301311503490781', N'12312', N'黎明', N'apple', N'2013-01-31 15:03:49', N'0823432', N'yy@qq.com', N'A4', N'M5', N'A', N'2013', N'234324', N'MYY', N'sum', N'other', N'001', N'002', N'003', N'033', N'23423', N'23423423', 2, N'/attachment/complaintinfo/201301311503495675.htm', 4, 2, 2, N'sei201301161019460562', 4, NULL, 1)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (40, N'cuc201301311712450703', N'12312', N'黎明', N'太阳公司', N'2013-01-31 17:12:45', N'234324', N'yy@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 1, N'/attachment/complaintinfo/201301311712135007.htm', 1, 1, 1, N'cui201301271547140111', 1, N'order', 2)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (41, N'cuc201301312105350562', N'机器出问题', N'234', N'a', N'2013-01-31 21:05:35', N'12312', N'aa@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 1, NULL, 1, 1, 2, N'sei201301161019460562', 1, N'admin', 3)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (42, N'cuc201303031530120411', N'按时打算的', N'太太团', N'werewolf', N'2013-03-03 15:30:12', N'213123', N'ww@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 1, N'/attachment/complaintinfo/201303031530125531.xls', 1, 1, 1, N'cui201303031518130324', NULL, NULL, 4)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (43, N'cuc201303041504480742', N'aaaaaaaaa', N'wwwwww8', N'aaaa', N'2013-03-04 15:04:48', N'1234454', N'40345949@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 1, N'/attachment/complaintinfo/201303041504486404.txt', 1, 1, 1, N'cti20121228134915123', NULL, NULL, 1)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (44, N'cuc201303111846070671', N'1111', N'李四we', N'autel', N'2013-03-11 18:46:04', N'86075588888881', N'C-0001@tom.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 3, NULL, 1, 1, 2, N'sei201301161019460562', NULL, NULL, 4)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (45, N'cuc201303112147310250', N'机器出问题了', N'TAE', N'苹果公司', N'2013-03-11 21:47:31', N'324234', N'527690766@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 2, NULL, 1, 1, 1, N'cui201303051600540968', NULL, NULL, 3)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (46, N'cuc201303112152440687', N'宝马发动机出问题了', N'TAE', N'苹果公司', N'2013-03-11 21:52:44', N'18665396791', N'527690766@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 1, NULL, 1, 1, 1, N'cui201303051600540968', NULL, NULL, 2)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (47, N'cuc201303112155430484', N'123213', N'TAE', N'苹果公司', N'2013-03-11 21:55:43', N'18665396791', N'527690766@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 1, NULL, 1, 1, 1, N'cui201303051600540968', NULL, NULL, 1)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (48, N'cuc201303112200540265', N'123213', N'TAE', N'苹果公司', N'2013-03-11 22:00:54', N'18665396791', N'527690766@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 1, NULL, 1, 1, 1, N'cui201303051600540968', NULL, NULL, 1)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (49, N'cuc201303151104330687', N'12312', N'TAE', N'太阳公司', N'2013-03-15 11:04:33', N'234324', N'527690766@qq.com', N'', N'', N'', N'', N'', N'', N'12345678965432109', N'', N'', N'', N'', N'', N'', N'', 1, NULL, 1, 1, 1, N'cui201303051600540968', NULL, NULL, 4)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (50, N'cuc201303151116110921', N'奔驰车出故障了', N'TAE', N'太阳公司', N'2013-03-15 11:16:06', N'234324', N'527690766@qq.com', N'M234', N'C', N'12312312', N'2013', N'23423423', N'YY', N'12344456788991011', N'other', N'DS708', N'002', N'DS901', N'033', N'd:/sdfsd', N'水电费谁地方水电费收费的收复失地', 3, N'/attachment/complaintinfo/201303151115579727.png', 2, 2, 1, N'cui201303051600540968', NULL, NULL, 3)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (51, N'cuc201303171618190187', N'sdfsdfsdfsdfsd', N'李四we', N'autel', N'2013-03-17 16:18:19', N'34234234', N'C-0001@tom.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 1, NULL, 1, 1, 2, N'sei201301161019460562', NULL, NULL, 1)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (52, N'cuc201303201442320859', N'78999999999999999999999999999999999999999999999999', N'TAs', N'autel', N'2013-03-20 14:42:32', N'94555', N'C-0001@tom.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'5675675666666666666666666666767777777777777777777777777777777777777777777777777777777777', 1, NULL, 1, 1, 2, N'sei201301161019460562', NULL, NULL, 1)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (53, N'cuc201303241709570161', N'宝马5出问题了', N'TAE', N'Apple', N'2013-03-24 17:09:57', N'86075589191111', N'527690766@qq.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', 3, N'/attachment/complaintinfo/201303241709570448.JPG', 1, 1, 1, N'cui201303051600540968', NULL, NULL, 1)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (54, N'cuc201303262259180765', N'奥迪A8上市了', N'TAE', N'Apple', N'2013-03-26 22:59:18', N'86075589191111', N'527690766@qq.com', N'A8', N'A8', N'111', N'2013', N'2013', N'111', N'NaN', N'2222', N'啊啊啊', N'222', N'  啊', N'22', N'22', N'随碟附送地方随碟附送地方', 3, N'/attachment/complaintinfo/201303262259184826.txt', 1, 1, 1, N'cui201303051600540968', NULL, NULL, 3)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (55, N'cuc201304021812070296', N'fff', N'王五', N'autel', N'2013-04-02 18:12:07', N'111111', N'wwwwww8@126.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'fff
f
f
f
f
f

f
f

f
f

f
f
f

f
f
f
f
f

f', 1, NULL, 1, 1, 1, N'cti20121228134915123', NULL, NULL, 1)
INSERT [dbo].[DT_CustomerComplaintInfo] ([id], [code], [complaintTitle], [complaintPerson], [company], [complaintDate], [phone], [email], [vehicleSystem], [vehicleType], [diagnosticConnector], [carYear], [carnumber], [engineType], [system], [other], [productName], [productNo], [softwareName], [softwareEdition], [testPath], [complaintContent], [emergencyLevel], [attachment], [complaintState], [complaintEffective], [userType], [userCode], [acceptState], [acceptor], [complaintType]) VALUES (56, N'cuc201304021812100015', N'fff', N'王五', N'autel', N'2013-04-02 18:12:09', N'111111', N'wwwwww8@126.com', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'fff
f
f
f
f
f

f
f

f
f

f
f
f

f
f
f
f
f

f', 1, NULL, 1, 1, 1, N'cti20121228134915123', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[DT_CustomerComplaintInfo] OFF
/****** Object:  Table [dbo].[DT_ComplaintAdminuserInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DT_ComplaintAdminuserInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[complaintType] [int] NOT NULL,
	[adminuserUserid] [int] NOT NULL,
 CONSTRAINT [PK_DT_ComplaintAdminuserInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ComplaintAdminuserInfo] DISABLE CHANGE_TRACKING
GO
SET IDENTITY_INSERT [dbo].[DT_ComplaintAdminuserInfo] ON
INSERT [dbo].[DT_ComplaintAdminuserInfo] ([id], [complaintType], [adminuserUserid]) VALUES (1, 1, 2)
INSERT [dbo].[DT_ComplaintAdminuserInfo] ([id], [complaintType], [adminuserUserid]) VALUES (2, 2, 2)
INSERT [dbo].[DT_ComplaintAdminuserInfo] ([id], [complaintType], [adminuserUserid]) VALUES (3, 3, 2)
INSERT [dbo].[DT_ComplaintAdminuserInfo] ([id], [complaintType], [adminuserUserid]) VALUES (4, 4, 4)
SET IDENTITY_INSERT [dbo].[DT_ComplaintAdminuserInfo] OFF
/****** Object:  Table [dbo].[DT_AreaConfigDetail]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_AreaConfigDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[areaCode] [varchar](32) NULL,
	[areaCfgCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_AREACONFIGDETAIL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_AreaConfigDetail] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_AreaConfigDetail] ON
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (59, N'ain201301080114090232', N'acf201301080125020811')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (60, N'ain201304061756510950', N'acf201301080125020811')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (61, N'ain201304061757020054', N'acf201301080125020811')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (62, N'ain201304061759080264', N'acf201301080125020811')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (64, N'ain201301080109020633', N'acf201303212143110189')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (65, N'ain201301080113230346', N'acf201304061800510152')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (66, N'ain201301080113430654', N'acf201304061801140669')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (67, N'ain201301080113090078', N'acf201301080130400725')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (68, N'ain201301081156360937', N'acf201301080130400725')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (69, N'ain201303081553490087', N'acf201301080130400725')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (70, N'ain201303211929030370', N'acf201301080130400725')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (71, N'ain201303211929300932', N'acf201301080130400725')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (74, N'ain201301080113340350', N'acf201304061801440888')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (75, N'ain201301080113570249', N'acf201304061801440888')
INSERT [dbo].[DT_AreaConfigDetail] ([id], [areaCode], [areaCfgCode]) VALUES (76, N'ain201304061802040029', N'acf201304061801440888')
SET IDENTITY_INSERT [dbo].[DT_AreaConfigDetail] OFF
/****** Object:  Table [dbo].[DT_MinSaleUnit]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MinSaleUnit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[proTypeCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_MINSALEUNIT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MinSaleUnit] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_MinSaleUnit] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'mss' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_MinSaleUnit', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnit] ON
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (1, N'msu201301121637152342', N'prt201303062304290138')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (2, N'msu201301121637123457', N'prt201301081510220618')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (4, N'msu201301151652010229', N'prt201301121755410241')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (5, N'msu201301161426500468', N'prt201303062306080995')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (7, N'msu201301181526170539', N'prt201303081717190162')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (9, N'msu201301261546410851', N'prt201303231459010678')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (10, N'msu201303031203220658', N'prt201301081510220618')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (11, N'msu201304061823360237', N'prt201301081510220618')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (12, N'msu201304061840100520', N'prt201301081510220618')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (13, N'msu201304061843380949', N'prt201301081510220618')
INSERT [dbo].[DT_MinSaleUnit] ([ID], [code], [proTypeCode]) VALUES (14, N'msu201304061844090023', N'prt201301081510220618')
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnit] OFF
/****** Object:  Table [dbo].[DT_Message]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_Message](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[msgTypeCode] [varchar](32) NULL,
	[languageCode] [varchar](32) NULL,
	[title] [varchar](255) NULL,
	[sort] [int] NULL,
	[creator] [varchar](250) NULL,
	[creatTime] [varchar](250) NULL,
	[status] [int] NULL,
	[isDialog] [int] NULL,
	[content] [text] NULL,
	[userType] [int] NULL,
	[code] [varchar](32) NULL,
 CONSTRAINT [PK_DT_MESSAGE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_Message] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_Message] ON
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (1, N'mst201301140125160639', N'lag201301070146010554', N'1111111111', 1, N'1111', N'2013-01-14 16:57:39', 1, 1, N'<p>
	1111111111111</p>
', 1, N'dm0001')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (2, N'mst201301140124030006', N'lag201301070146010554', N'2222222', 2, N'2222', N'2013-01-14 16:57:55', 1, 1, N'<p>
	22222222</p>
', 2, N'dm0002')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (4, N'mst201301140124030006', N'lag201301070146010554', N'fsdf', 1, N'sadf', N'2013-01-24 19:10:39', 1, 1, N'<p>
	sdafasdfsd</p>
', 1, N'dm0003')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (5, N'mst201301140125160639', N'lag201301070146010554', N'12312', 1, N'123', N'2013-01-24 19:16:02', 1, 1, N'<p>
	21312321</p>
', 1, N'dm0004')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (6, N'mst201301140124030006', N'lag201301070020090542', N'test', 1, N'sdf', N'2013-01-27 20:32:18', 1, 1, N'<p>
	sdfsdfsdfs</p>
', 2, N'dm0005')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (7, N'mst201301140124030006', N'lag201301070020090542', N'111', 4, N'11', N'2013-03-03 15:26:17', 1, 1, N'&lt;p>
	qweqwewqeqeqweqw&lt;/p>
', 1, N'dm0006')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (8, N'mst201301140125160639', N'lag201301070020090542', N'测试数据test', 1, N'admin', N'2013-03-04 14:40:23', 1, 1, N'&lt;p>
	测试数据test&lt;/p>
', 1, N'dm0002')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (9, N'mst201301140124030006', N'lag201301070020090542', N'111', 2, N'111', N'2013-03-06 02:13:44', 1, 1, N'&lt;p>
	232323&lt;/p>
', 1, N'mes201303060213440975')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (10, N'mst201301140125160639', N'lag201301070020090542', N'宝马5', 1, N'admin', N'2013-03-25 09:55:42', 1, 1, N'&lt;p>
	宝马5系成功突破100W台&lt;/p>
', 2, N'mes201303250955420953')
INSERT [dbo].[DT_Message] ([id], [msgTypeCode], [languageCode], [title], [sort], [creator], [creatTime], [status], [isDialog], [content], [userType], [code]) VALUES (11, N'mst201301140124030006', N'lag201301070020090542', N'test', 1, N'1', N'2013-03-30 14:47:28', 1, 1, N'&lt;p>
	是打发士大夫说的士大夫&lt;/p>
', 1, N'mes201303301447280285')
SET IDENTITY_INSERT [dbo].[DT_Message] OFF
/****** Object:  Table [dbo].[DT_MarketPromotionTimesInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MarketPromotionTimesInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[startTime] [varchar](250) NULL,
	[endTime] [varchar](250) NULL,
	[marketPromotionCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_MARKETPROMOTIONTIMESINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MarketPromotionTimesInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_MarketPromotionTimesInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ati' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_MarketPromotionTimesInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionTimesInfo] ON
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (9, N'mpt201301240927510630', N'2013-01-01 01:00:00', N'2013-01-04 03:00:00', N'mpb201301240927510370')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (10, N'mpt201301240927510751', N'2013-01-05 01:04:00', N'2013-01-06 08:03:00', N'mpb201301240927510370')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (11, N'mpt201301241021590710', N'2013-01-01 00:00:00', N'2013-01-11 00:00:00', N'mpb201301241021590454')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (12, N'mpt201301241021590861', N'2013-01-12 00:00:00', N'2013-01-14 03:00:00', N'mpb201301241021590454')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (13, N'mpt201301241028560510', N'2013-01-01 00:00:00', N'2013-01-02 00:00:00', N'mpb201301241028560270')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (14, N'mpt201301241030350330', N'2013-01-01 00:00:00', N'2013-01-03 02:00:00', N'mpb201301241030350094')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (15, N'mpt201301241034570680', N'2013-01-02 00:00:00', N'2013-01-11 00:00:00', N'mpb201301241034570246')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (16, N'mpt201301241035550300', N'2013-01-01 00:00:00', N'2013-01-11 00:00:00', N'mpb201301241035550046')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (17, N'mpt201301241037550160', N'2013-01-24 00:00:00', N'2013-01-25 00:00:00', N'mpb201301241037540891')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (19, N'mpt201301241043290810', N'2013-01-02 00:00:00', N'2013-01-12 00:00:00', N'mpb201301241043290523')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (21, N'mpt201301241559440380', N'2013-01-22 03:00:00', N'2013-01-23 00:00:00', N'mpb201301241458000340')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (24, N'mpt201301261601570830', N'2013-01-01 00:00:00', N'2013-01-31 00:00:00', N'mpb201301261601570445')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (25, N'mpt201301261601580011', N'2013-02-01 00:00:00', N'2013-02-02 00:00:00', N'mpb201301261601570445')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (26, N'mpt201301261603360540', N'2013-01-01 00:00:00', N'2013-01-11 00:00:00', N'mpb201301261603350975')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (27, N'mpt201301261604310590', N'2013-01-01 00:00:00', N'2013-01-11 00:00:00', N'mpb201301261604310039')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (28, N'mpt201301261605120040', N'2013-01-02 00:00:00', N'2013-01-25 00:00:00', N'mpb201301261605110648')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (29, N'mpt201301281633310210', N'2013-01-28 00:00:00', N'2013-01-28 23:00:00', N'mpb201301281633310078')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (31, N'mpt201303031359460110', N'2013-03-04 00:00:00', N'2013-03-06 00:00:00', N'mpb201303031359450522')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (32, N'mpt201303031402250870', N'2013-03-05 00:00:00', N'2013-03-08 00:00:00', N'mpb201303031402250314')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (33, N'mpt201303031613130510', N'2013-03-02 00:00:00', N'2013-03-04 00:00:00', N'mpb201303031355310878')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (34, N'mpt201303080941290640', N'2013-03-07 00:00:00', N'2013-03-09 00:00:00', N'mpb201303080941290515')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (35, N'mpt201303111925210950', N'2013-03-10 00:00:00', N'2013-03-13 00:00:00', N'mpb201303111925210703')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (36, N'mpt201303130859520060', N'2013-03-13 00:00:00', N'2013-03-15 00:00:00', N'mpb201303130859510703')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (37, N'mpt201303130906000040', N'2013-03-13 00:00:00', N'2013-03-15 00:00:00', N'mpb201303130905590781')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (38, N'mpt201303130907020400', N'2013-03-13 00:00:00', N'2013-03-31 00:00:00', N'mpb201303130907020156')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (39, N'mpt201303130907540510', N'2013-03-13 00:00:00', N'2013-03-31 00:00:00', N'mpb201303130907540312')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (40, N'mpt201303191446490850', N'2013-03-19 00:00:00', N'2013-03-21 00:00:00', N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (44, N'mpt201303211013400930', N'2013-03-20 00:00:00', N'2013-03-22 00:00:00', N'mpb201303210959000875')
INSERT [dbo].[DT_MarketPromotionTimesInfo] ([id], [code], [startTime], [endTime], [marketPromotionCode]) VALUES (45, N'mpt201303281917460450', N'2013-03-28 00:00:00', N'2013-03-31 00:00:00', N'mpb201303281917460235')
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionTimesInfo] OFF
/****** Object:  Table [dbo].[DT_MarketPromotionSoftwareRule]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MarketPromotionSoftwareRule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[productTypeCode] [varchar](32) NULL,
	[minSaleUnitCode] [varchar](32) NULL,
	[minSaleUnitName] [varchar](250) NULL,
	[costPrice] [numeric](18, 0) NULL,
	[marketPromotionCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_MARKETPROMOTIONSOFTWARER] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MarketPromotionSoftwareRule] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_MarketPromotionSoftwareRule] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'psr' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_MarketPromotionSoftwareRule', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionSoftwareRule] ON
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (10, N'mps201301241027160320', N'prt201301081510220618', N'msu201301161426500468', N'sale-1', NULL, N'mpb201301241021590454')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (11, N'mps201301241027160471', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', CAST(359 AS Numeric(18, 0)), N'mpb201301241021590454')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (12, N'mps201301241030350450', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', CAST(359 AS Numeric(18, 0)), N'mpb201301241030350094')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (13, N'mps201301241030350571', N'prt201301081510220618', N'msu201301161426500468', N'sale-1', NULL, N'mpb201301241030350094')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (17, N'mps201301241043290930', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', CAST(359 AS Numeric(18, 0)), N'mpb201301241043290523')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (20, N'mps201301241602220830', N'prt201301081510220618', N'msu201301161426500468', N'sale-1', NULL, N'mpb201301241458000340')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (21, N'mps201301241602220991', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', CAST(359 AS Numeric(18, 0)), N'mpb201301241458000340')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (25, N'mps201301261601580180', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201301261601570445')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (26, N'mps201301261603360720', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201301261603350975')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (27, N'mps201301261604310760', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201301261604310039')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (28, N'mps201301261605120210', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201301261605110648')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (32, N'mps201301282031420370', N'prt201301071535520790', N'msu201301121637152342', N'销售单位说明002', NULL, N'mpb201301281633310078')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (33, N'mps201303031356070330', N'prt201301301142520522', N'msu201303031203220658', N'softunit', NULL, N'mpb201303031355310878')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (34, N'mps201303031359460310', N'prt201301301142520522', N'msu201303031203220658', N'softunit', NULL, N'mpb201303031359450522')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (35, N'mps201303031402260070', N'prt201301301142520522', N'msu201303031203220658', N'softunit', NULL, N'mpb201303031402250314')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (36, N'mps201303031402260251', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201303031402250314')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (37, N'mps201303031402260432', N'prt201301121756510710', N'msu201301232008190461', N'35', NULL, N'mpb201303031402250314')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (38, N'mps201303031402260643', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', NULL, N'mpb201303031402250314')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (39, N'mps201303031402260824', N'prt201301081510220618', N'msu201301161426500468', N'sale-1', NULL, N'mpb201303031402250314')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (40, N'mps201303031402270015', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303031402250314')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (41, N'mps201303031402270236', N'prt201301071535520790', N'msu201301121637152342', N'销售单位说明002', NULL, N'mpb201303031402250314')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (42, N'mps201303031613140150', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303031355310878')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (43, N'mps201303080941290700', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303080941290515')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (44, N'mps201303111927000370', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303111925210703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (45, N'mps201303111927000451', N'prt201301071535520790', N'msu201301121637152342', N'销售单位说明002', NULL, N'mpb201303111925210703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (46, N'mps201303111927000532', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', NULL, N'mpb201303111925210703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (47, N'mps201303130906000120', N'prt201301301142520522', N'msu201303031203220658', N'softunit', NULL, N'mpb201303130905590781')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (48, N'mps201303130906000201', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201303130905590781')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (49, N'mps201303130906000282', N'prt201301121756510710', N'msu201301232008190461', N'35', NULL, N'mpb201303130905590781')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (50, N'mps201303130906000343', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', NULL, N'mpb201303130905590781')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (51, N'mps201303130906000404', N'prt201301081510220618', N'msu201301161426500468', N'sale-1', NULL, N'mpb201303130905590781')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (52, N'mps201303130906000465', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303130905590781')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (53, N'mps201303130906000536', N'prt201301071535520790', N'msu201301121637152342', N'销售单位说明002', NULL, N'mpb201303130905590781')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (54, N'mps201303130907020480', N'prt201301301142520522', N'msu201303031203220658', N'softunit', NULL, N'mpb201303130907020156')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (55, N'mps201303130907020541', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201303130907020156')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (56, N'mps201303130907020602', N'prt201301121756510710', N'msu201301232008190461', N'35', NULL, N'mpb201303130907020156')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (57, N'mps201303130907020673', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', NULL, N'mpb201303130907020156')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (58, N'mps201303130907020734', N'prt201301081510220618', N'msu201301161426500468', N'sale-1', NULL, N'mpb201303130907020156')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (59, N'mps201303130907020795', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303130907020156')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (60, N'mps201303130907020876', N'prt201301071535520790', N'msu201301121637152342', N'销售单位说明002', NULL, N'mpb201303130907020156')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (61, N'mps201303130907540570', N'prt201301301142520522', N'msu201303031203220658', N'softunit', NULL, N'mpb201303130907540312')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (62, N'mps201303130907540641', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201303130907540312')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (63, N'mps201303130907540702', N'prt201301121756510710', N'msu201301232008190461', N'35', NULL, N'mpb201303130907540312')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (64, N'mps201303130907540763', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', NULL, N'mpb201303130907540312')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (65, N'mps201303130907540844', N'prt201301081510220618', N'msu201301161426500468', N'sale-1', NULL, N'mpb201303130907540312')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (66, N'mps201303130907540905', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303130907540312')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (67, N'mps201303130907540966', N'prt201301071535520790', N'msu201301121637152342', N'销售单位说明002', NULL, N'mpb201303130907540312')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (68, N'mps201303191446490930', N'prt201301301142520522', N'msu201303031203220658', N'softunit', NULL, N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (69, N'mps201303191446500001', N'prt201301261537230766', N'msu201301261546410851', N'sale1', NULL, N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (70, N'mps201303191446500062', N'prt201301121756510710', N'msu201301232008190461', N'35', NULL, N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (71, N'mps201303191446500123', N'prt201301081510220618', N'msu201301181526170539', N'销售单位说明002BNK', NULL, N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (72, N'mps201303191446500204', N'prt201301081510220618', N'msu201301161426500468', N'sale-1', NULL, N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (73, N'mps201303191446500265', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (74, N'mps201303191446500346', N'prt201301071535520790', N'msu201301121637152342', N'销售单位说明002', NULL, N'mpb201303191446490703')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (75, N'mps201303210959010290', N'prt201301081510220618', N'msu201301121637123457', N'销售单位说明001', NULL, N'mpb201303210959000875')
INSERT [dbo].[DT_MarketPromotionSoftwareRule] ([id], [code], [productTypeCode], [minSaleUnitCode], [minSaleUnitName], [costPrice], [marketPromotionCode]) VALUES (77, N'mps201303281935260190', NULL, N'all', NULL, NULL, N'mpb201303281917460235')
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionSoftwareRule] OFF
/****** Object:  Table [dbo].[DT_MarketPromotionAreaRule]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MarketPromotionAreaRule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[marketPromotionCode] [varchar](32) NULL,
	[areaCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_MARKETPROMOTIONAREARULE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MarketPromotionAreaRule] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_MarketPromotionAreaRule] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'par' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_MarketPromotionAreaRule', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionAreaRule] ON
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (9, N'mpa201301240927520120', N'mpb201301240927510370', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (10, N'mpa201301241028560630', N'mpb201301241028560270', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (11, N'mpa201301241040070030', N'mpb201301241030350094', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (14, N'mpa201301241602230130', N'mpb201301241458000340', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (16, N'mpa201301261601580360', N'mpb201301261601570445', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (17, N'mpa201301261601580531', N'mpb201301261601570445', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (18, N'mpa201301261603360900', N'mpb201301261603350975', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (19, N'mpa201301261603370081', N'mpb201301261603350975', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (20, N'mpa201301261604310940', N'mpb201301261604310039', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (21, N'mpa201301261604320141', N'mpb201301261604310039', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (22, N'mpa201301261605120390', N'mpb201301261605110648', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (23, N'mpa201301261605120591', N'mpb201301261605110648', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (28, N'mpa201301282031420430', N'mpb201301281633310078', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (29, N'mpa201303031359460500', N'mpb201303031359450522', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (30, N'mpa201303031613140470', N'mpb201303031355310878', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (31, N'mpa201303031613140791', N'mpb201303031355310878', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (32, N'mpa201303080941290780', N'mpb201303080941290515', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (33, N'mpa201303111927130620', N'mpb201303111925210703', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (34, N'mpa201303111927130701', N'mpb201303111925210703', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (35, N'mpa201303130906000590', N'mpb201303130905590781', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (36, N'mpa201303130906000671', N'mpb201303130905590781', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (37, N'mpa201303130907020930', N'mpb201303130907020156', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (38, N'mpa201303130907030001', N'mpb201303130907020156', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (39, N'mpa201303130907550040', N'mpb201303130907540312', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (40, N'mpa201303130907550091', N'mpb201303130907540312', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (41, N'mpa201303191446500510', N'mpb201303191446490703', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (42, N'mpa201303191446500591', N'mpb201303191446490703', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (43, N'mpa201303210959010540', N'mpb201303210959000875', N'acf201301080125020811')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (44, N'mpa201303210959010641', N'mpb201303210959000875', N'acf201301080130400725')
INSERT [dbo].[DT_MarketPromotionAreaRule] ([id], [code], [marketPromotionCode], [areaCode]) VALUES (46, N'mpa201303281935260362', N'mpb201303281917460235', N'all')
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionAreaRule] OFF
/****** Object:  Table [dbo].[DT_CustomerInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_CustomerInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[firstName] [varchar](250) NULL,
	[middleName] [varchar](250) NULL,
	[lastName] [varchar](250) NULL,
	[userPwd] [varchar](250) NULL,
	[regTime] [varchar](250) NULL,
	[address] [varchar](250) NULL,
	[company] [varchar](250) NULL,
	[zipCode] [varchar](250) NULL,
	[comUsername] [varchar](250) NULL,
	[actState] [int] NULL,
	[actCode] [varchar](32) NULL,
	[autelId] [varchar](250) NOT NULL,
	[isAllowSendEmail] [int] NULL,
	[lastLoginTime] [varchar](250) NULL,
	[answer] [varchar](250) NULL,
	[questionCode] [varchar](32) NULL,
	[city] [varchar](250) NULL,
	[daytimePhone] [varchar](250) NULL,
	[daytimePhoneCC] [varchar](250) NULL,
	[daytimePhoneAC] [varchar](250) NULL,
	[daytimePhoneExtCode] [varchar](250) NULL,
	[mobilePhone] [varchar](250) NULL,
	[mobilePhoneCC] [varchar](250) NULL,
	[mobilePhoneAC] [varchar](250) NULL,
	[country] [varchar](250) NULL,
	[languageCode] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[secondEmail] [varchar](250) NULL,
	[sendActiveTime] [varchar](250) NULL,
	[sendResetPasswordTime] [varchar](250) NULL,
 CONSTRAINT [PK_DT_CUSTOMERINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_CustomerInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_CustomerInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_CustomerInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'加密' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_CustomerInfo', @level2type=N'COLUMN',@level2name=N'userPwd'
GO
SET IDENTITY_INSERT [dbo].[DT_CustomerInfo] ON
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (9, N'cti20121228134915123', N'王', N'五', N'', N'601B77DBE966684A', NULL, N'', N'autel', N'518000', NULL, 1, N'jhihdjdfkldjfssdsd', N'wwwwww8@126.com', 1, N'2013-04-02 12:07:06', NULL, NULL, N'', N'', N'', N'', N'', N'11111111111', N'', N'', N'', N'lag201301070146010554', N'王五', N'', NULL, NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (30, N'cui201301121425320671', N'李', N'四', N'', N'', N'2013-01-12 14:25:31', N'深圳市南山区', N'Apple', N'518000', N'TAE', 0, NULL, N'llll@126.com', 1, N'2013-01-12 14:25:31', N'还不错', N'qui20121228134915123', NULL, NULL, NULL, NULL, NULL, N'11111111', NULL, NULL, NULL, N'lag201301070146010554', N'李四', NULL, NULL, NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (49, N'cui201301231020360421', N'cao', N'zhi', N'yong', N'6E1268F4867007EC', N'2013-01-23 10:20:36', N'bantian', N'broadengrate', N'518000', N'chad', 1, N'841cdcae7f6a4fa1baef3a863bb01e15', N'chad@163.com', 1, N'2013-01-23 10:20:36', N'good', N'qui20121228134915123', N'shenzheng', NULL, NULL, NULL, NULL, N'sdf', NULL, NULL, N'china', N'lag201301070146010554', N'caozhiyong', NULL, NULL, NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (57, N'cui201301291919100466', N'zhang', N'sheng', N'aa', N'6E1268F4867007EC', N'2013-01-29 19:19:10', N'', N'', N'', N'zs', 1, N'4886dd3140644816bbdbced6354dfa74', N'zhangsheng011@126.com', 1, N'2013-01-29 19:19:10', N'湖北', N'qui201301112351410320', N'', N'', N'', N'', N'', N'', N'', N'', N'中国', N'lag201301070146010554', N'', NULL, NULL, NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (60, N'cui201303031510050808', N'y', N'y', N'y', N'6E1268F4867007EC', N'2013-03-03 15:10:05', N'', N'', N'', N'yyy', 0, N'cf0ccf1890da4fd383039f08e0017b71', N'zhangsheng@broadengate.com', 1, N'2013-03-03 15:10:05', N'湖北', N'qui201301112351410320', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'lag201301070020090542', NULL, NULL, NULL, NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (64, N'cui201303041831170343', N'a', N'b', N'c', N'6E1268F4867007EC', N'2013-03-04 18:31:17', N'', N'', N'', N'TAE', 0, N'cc293ee5a266420aa116605e8b940ddf', N'peng@126.com', 1, N'2013-03-04 18:31:17', N'撒', N'qui20121228134915123', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'lag201301070020090542', N'abc', N'33@qq.com', NULL, NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (74, N'cui201303051600540968', N'T', N'A', N'E', N'6E1268F4867007EC', N'2013-03-05 16:00:54', N'南山市', N'Apple', N'518000', N'TAE', 1, N'b90c340f09d84f3bbba328cbe40fcb18', N'527690766@qq.com', 1, N'2013-04-02 08:32:24', N'湖南', N'qui20121228134915123', N'深圳', N'89191111', N'86', N'0755', NULL, N'1364565464565', N'86', N'0755', N'中国', N'lag201301070020090542', N'TAE', N'TAE@126.com', N'2013-03-05 16:00:54', N'2013-03-27 15:31:28')
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (76, N'cui201303141354090703', N'T', N'A', N'E', N'6E1268F4867007EC', N'2013-03-14 13:54:09', N'', N'', N'', N'TAE', 0, N'eb070967f5b84b6c85841c73c68509ae', N'sum@qq.com', 1, N'2013-03-14 13:54:09', N'湖南', N'qui201301112351410320', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'中国', N'lag201301070020090542', N'TAE', N'', N'2013-03-14 13:54:09', NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (78, N'cui201303141358270500', N'T', N'A', N'E', N'6E1268F4867007EC', N'2013-03-14 13:58:27', N'', N'', N'', N'TAE', 0, N'ce6a2593790e4e9a984fa75f440f6f15', N'sum@qq.com', 1, N'2013-03-14 13:58:27', N'湖南', N'qui201301112351410320', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'中国', N'lag201301070020090542', N'TAE', N'', N'2013-03-14 13:58:27', NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (84, N'cui201303142141120421', N'yang', N'ping', N'gui', N'6E1268F4867007EC', N'2013-03-14 21:41:12', N'', N'', N'', N'yangpinggui', 0, N'7465a16232c2473abcd1482e3aeb8c10', N'wanglaoshi@qq.com', 1, N'2013-03-14 21:41:12', N'shanghai', N'qui20121228134915123', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'2312', N'lag201301070020090542', N'yangpinggui', N'', N'2013-03-14 21:41:12', NULL)
INSERT [dbo].[DT_CustomerInfo] ([id], [code], [firstName], [middleName], [lastName], [userPwd], [regTime], [address], [company], [zipCode], [comUsername], [actState], [actCode], [autelId], [isAllowSendEmail], [lastLoginTime], [answer], [questionCode], [city], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [country], [languageCode], [name], [secondEmail], [sendActiveTime], [sendResetPasswordTime]) VALUES (86, N'cui201303191638230546', N'T', N'A', N'E', N'6E1268F4867007EC', N'2013-03-19 16:38:23', N'', N'', N'', N'TAE', 1, N'81d8f960d40b40eaaf4cafe55a50b428', N'yangpingguitae@126.com', 1, N'2013-03-19 16:38:23', N'湖南', N'qui201301112351410320', N'', N'', N'', N'', N'', N'', N'', N'', N'中国', N'lag201301070020090542', N'TAE', N'', N'2013-03-19 16:38:23', NULL)
SET IDENTITY_INSERT [dbo].[DT_CustomerInfo] OFF
/****** Object:  Table [dbo].[DT_CustomerComplaintTypeName]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_CustomerComplaintTypeName](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[complaintTypeCode] [varchar](32) NULL,
	[languageCode] [varchar](32) NULL,
	[name] [varchar](250) NULL,
 CONSTRAINT [PK_DT_CustomerComplaintTypeName] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_CustomerComplaintTypeName] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_CustomerComplaintTypeName] ON
INSERT [dbo].[DT_CustomerComplaintTypeName] ([id], [complaintTypeCode], [languageCode], [name]) VALUES (14, N'cut201304021404040240', N'lag201301070146010554', N'硬件问题')
INSERT [dbo].[DT_CustomerComplaintTypeName] ([id], [complaintTypeCode], [languageCode], [name]) VALUES (16, N'cut201303311910160359', N'lag201301070146010554', N'产品问题')
INSERT [dbo].[DT_CustomerComplaintTypeName] ([id], [complaintTypeCode], [languageCode], [name]) VALUES (17, N'cut201304021421560178', N'lag201301070146010554', N'其它问题')
INSERT [dbo].[DT_CustomerComplaintTypeName] ([id], [complaintTypeCode], [languageCode], [name]) VALUES (18, N'cut201304021426030053', N'lag201301070146010554', N'软件问题')
SET IDENTITY_INSERT [dbo].[DT_CustomerComplaintTypeName] OFF
/****** Object:  Table [dbo].[DT_MarketPromotionRule]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MarketPromotionRule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[marketPromotionCode] [varchar](32) NULL,
	[conditionType] [int] NULL,
	[discount] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DT_MARKETPROMOTIONRULE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MarketPromotionRule] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_MarketPromotionRule] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_MarketPromotionRule', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionRule] ON
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (6, N'mpr201301240927510513', N'mpb201301240927510370', 1, CAST(9 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (7, N'mpr201301241021590583', N'mpb201301241021590454', 1, CAST(7 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (8, N'mpr201301241028560393', N'mpb201301241028560270', 1, CAST(5 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (9, N'mpr201301241030350217', N'mpb201301241030350094', 0, CAST(3 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (10, N'mpr201301241034570547', N'mpb201301241034570246', 0, CAST(2 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (11, N'mpr201301241035550169', N'mpb201301241035550046', 0, CAST(2 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (12, N'mpr201301241037550016', N'mpb201301241037540891', 0, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (14, N'mpr201301241043290673', N'mpb201301241043290523', 0, CAST(2 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (15, N'mpr201301241458000708', N'mpb201301241458000340', 1, CAST(5 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (17, N'mpr201301261601570633', N'mpb201301261601570445', 0, CAST(9 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (18, N'mpr201301261603360164', N'mpb201301261603350975', 1, CAST(5 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (19, N'mpr201301261604310242', N'mpb201301261604310039', 1, CAST(5 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (20, N'mpr201301261605110835', N'mpb201301261605110648', 0, CAST(2 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (21, N'mpr201301281633310156', N'mpb201301281633310078', 0, CAST(5 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (22, N'mpr201303031355320081', N'mpb201303031355310878', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (23, N'mpr201303031359450725', N'mpb201303031359450522', 1, NULL)
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (24, N'mpr201303031402250517', N'mpb201303031402250314', 1, NULL)
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (25, N'mpr201303080941290578', N'mpb201303080941290515', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (26, N'mpr201303111925210875', N'mpb201303111925210703', 0, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (27, N'mpr201303130859510875', N'mpb201303130859510703', 1, NULL)
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (28, N'mpr201303130905590984', N'mpb201303130905590781', 0, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (29, N'mpr201303130907020218', N'mpb201303130907020156', 1, NULL)
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (30, N'mpr201303130907540375', N'mpb201303130907540312', 1, NULL)
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (31, N'mpr201303191446490781', N'mpb201303191446490703', 1, CAST(2 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (32, N'mpr201303210959010078', N'mpb201303210959000875', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[DT_MarketPromotionRule] ([id], [code], [marketPromotionCode], [conditionType], [discount]) VALUES (33, N'mpr201303281917460297', N'mpb201303281917460235', 1, NULL)
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionRule] OFF
/****** Object:  Table [dbo].[DT_SaleConfig]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SaleConfig](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[proTypeCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_SALECONFIG] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SaleConfig] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SaleConfig] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ssc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SaleConfig', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_SaleConfig] ON
INSERT [dbo].[DT_SaleConfig] ([id], [code], [name], [proTypeCode]) VALUES (6, N'scf201301181240180212', N'销售配置11', N'prt201301121755410241')
INSERT [dbo].[DT_SaleConfig] ([id], [code], [name], [proTypeCode]) VALUES (7, N'scf201301181516540901', N'销售配置标', N'prt201301081510220618')
INSERT [dbo].[DT_SaleConfig] ([id], [code], [name], [proTypeCode]) VALUES (8, N'scf201301181527090943', N'销售配置最大', N'prt201301081510220618')
INSERT [dbo].[DT_SaleConfig] ([id], [code], [name], [proTypeCode]) VALUES (9, N'scf201301261556180297', N'saleconfig', N'prt201301261537230766')
INSERT [dbo].[DT_SaleConfig] ([id], [code], [name], [proTypeCode]) VALUES (10, N'scf201303031350340170', N'config101', N'prt201301301142520522')
INSERT [dbo].[DT_SaleConfig] ([id], [code], [name], [proTypeCode]) VALUES (11, N'scf201303231435080187', N'config2', N'prt201301301142520522')
SET IDENTITY_INSERT [dbo].[DT_SaleConfig] OFF
/****** Object:  Table [dbo].[DT_RoleAuth]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DT_RoleAuth](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[roleid] [int] NULL,
	[actid] [int] NULL,
	[authid] [int] NULL,
 CONSTRAINT [PK_DT_ROLEAUTH] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_RoleAuth] DISABLE CHANGE_TRACKING
GO
/****** Object:  Table [dbo].[DT_ReCustomerComplaintInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ReCustomerComplaintInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[customerComplaintCode] [varchar](32) NULL,
	[reContent] [varchar](255) NULL,
	[reDate] [varchar](250) NULL,
	[rePerson] [varchar](250) NULL,
	[attachment] [varchar](250) NULL,
	[rePersonType] [int] NULL,
	[complaintScore] [int] NULL,
 CONSTRAINT [PK_DT_RECUSTOMERCOMPLAINTINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ReCustomerComplaintInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ReCustomerComplaintInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'rcc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ReCustomerComplaintInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'后台管理员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ReCustomerComplaintInfo', @level2type=N'COLUMN',@level2name=N'rePerson'
GO
SET IDENTITY_INSERT [dbo].[DT_ReCustomerComplaintInfo] ON
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (33, N'rec201301311519410375', N'cuc201301311503490781', N'123213', N'2013-01-31 15:19:30', N'C-0001@tom.com', N'/attachment/complaintinfo/201301311519265624.htm', 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (34, N'rec201301311524230828', N'cuc201301311503490781', N'234234', N'2013-01-31 15:20:53', N'C-0001@tom.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (35, N'rec201301311527540234', N'cuc201301311503490781', N'333', N'2013-01-31 15:27:42', N'C-0001@tom.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (36, N'rec201301311529380937', N'cuc201301311503490781', N'', N'2013-01-31 15:28:44', N'C-0001@tom.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (37, N'rec201301311547340140', N'cuc201301311503490781', N'43534', N'2013-01-31 15:47:33', N'C-0001@tom.com', N'/attachment/complaintinfo/201301311547339504.png', 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (38, N'rec201301312052000187', N'cuc201301311503490781', N'', N'2013-01-31 20:51:58', N'C-0001@tom.com', N'/attachment/complaintinfo/201301312050468464.zip', 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (39, N'rec201302021731120765', N'cuc201301311712450703', N'ssssss', N'2013-02-02 17:31:12', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (40, N'rec201302041628540765', N'cuc201301311712450703', N'', N'2013-02-04 16:28:54', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (41, N'rec201302041634080781', N'cuc201301312105350562', N'aaaaa', N'2013-02-04 16:34:08', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (42, N'rec201302041634200093', N'cuc201301312105350562', N'bbbbb', N'2013-02-04 16:34:20', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (43, N'rec201302041635060718', N'cuc201301312105350562', N'cccc', N'2013-02-04 16:35:06', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (47, N'rec201302041700060046', N'cuc201301312105350562', N'aaaavvvvvvvvvvv', N'2013-02-04 17:00:05', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (48, N'rec201302041700100296', N'cuc201301312105350562', N'', N'2013-02-04 17:00:10', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (50, N'rec201302041708060703', N'cuc201301312105350562', N'fffffffffffffffkkkkkkkkkkkkk', N'2013-02-04 17:08:06', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (52, N'rec201303031530530579', N'cuc201303031530120411', N'我单位切尔', N'2013-03-03 15:30:53', N'527690766@qq.com', N'/attachment/complaintinfo/201303031530539751.pdf', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (53, N'rec201303041442140112', N'cuc201303031530120411', N'要不得', N'2013-03-04 14:42:14', N'0001', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (54, N'rec201303051408110171', N'cuc201303041504480742', N'你好', N'2013-03-05 14:08:11', N'0001', N'/attachment/complaintinfo/201303051408115723.txt', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (55, N'rec201303051422060515', N'cuc201303041504480742', N'sss', N'2013-03-05 14:22:06', N'0001', N'/attachment/complaintinfo/201303051422065067.txt', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (56, N'rec201303051511460140', N'cuc201303041504480742', N'ffffff', N'2013-03-05 15:11:46', N'0001', N'/attachment/complaintinfo/201303051511463296.xls', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (57, N'rec201303061646290140', N'cuc201301311712450703', N'收到', N'2013-03-06 16:46:29', N'order', N'/attachment/complaintinfo/201303061646281820.txt', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (67, N'rec201303071441330812', N'cuc201303041504480742', N'aa', N'2013-03-07 14:41:33', N'wwwwww8@126.com', N'/attachment/complaintinfo/201303071441334876.txt', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (68, N'rec201303071442530546', N'cuc201303041504480742', N'2222', N'2013-03-07 14:42:53', N'wwwwww8@126.com', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (74, N'rec201303081554340343', N'cuc201301312105350562', N'35345345', N'2013-03-08 15:54:34', N'C-0001@tom.com', N'/attachment/complaintinfo/201303081554340292.bak', 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (75, N'rec201303081909270375', N'cuc201303041504480742', N'sssssss', N'2013-03-08 19:09:27', N'admin', N'/attachment/complaintinfo/201303081909274610.txt', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (76, N'rec201303081920370812', N'cuc201303041504480742', N'fdaass', N'2013-03-08 19:20:37', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (77, N'rec201303081955310125', N'cuc201303041504480742', N'dddd', N'2013-03-08 19:55:31', N'admin', N'/attachment/complaintinfo/201303081955313226.xls', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (78, N'rec201303081955480218', N'cuc201303041504480742', N'q', N'2013-03-08 19:55:48', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (79, N'rec201303101425500625', N'cuc201303041504480742', N'好', N'2013-03-10 14:25:50', N'admin', N'/attachment/complaintinfo/201303101425507632.txt', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (80, N'rec201303101637500937', N'cuc201303041504480742', N'me', N'2013-03-10 16:37:50', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (81, N'rec201303101701140500', N'cuc201303041504480742', N'abab', N'2013-03-10 17:01:14', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (82, N'rec201303101710120515', N'cuc201303041504480742', N'sdsd', N'2013-03-10 17:10:12', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (83, N'rec201303101735530375', N'cuc201303041504480742', N'yyyyy', N'2013-03-10 17:35:53', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (84, N'rec201303101800010937', N'cuc201303041504480742', N'OK，我会尽快处理的', N'2013-03-10 18:00:01', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (85, N'rec201303171110520625', N'cuc201303151116110921', NULL, N'2013-03-17 11:10:52', N'527690766@qq.com', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (86, N'rec201303171111090984', N'cuc201303151116110921', NULL, N'2013-03-17 11:11:09', N'527690766@qq.com', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (87, N'rec201303171111340906', N'cuc201303151116110921', NULL, N'2013-03-17 11:11:34', N'527690766@qq.com', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (88, N'rec201303171122440796', N'cuc201303151116110921', N'67867867867', N'2013-03-17 11:22:44', N'527690766@qq.com', N'/attachment/complaintinfo/201303171122447706.txt', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (89, N'rec201303171124560734', N'cuc201303151116110921', N'很好', N'2013-03-17 11:24:56', N'527690766@qq.com', N'/attachment/complaintinfo/201303171124356975.sql', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (90, N'rec201303171618430265', N'cuc201303171618190187', N'34534534543', N'2013-03-17 16:18:43', N'C-0001@tom.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (91, N'rec201303201444190687', N'cuc201303201442320859', N'', N'2013-03-20 14:44:19', N'C-0001@tom.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (92, N'rec201303201451020656', N'cuc201303201442320859', N'', N'2013-03-20 14:51:02', N'C-0001@tom.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (93, N'rec201303201451430578', N'cuc201303201442320859', N'', N'2013-03-20 14:51:43', N'C-0001@tom.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (94, N'rec201303201453520687', N'cuc201303201442320859', N'yyyy', N'2013-03-20 14:53:52', N'C-0001@tom.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (95, N'rec201303232346170921', N'cuc201303151116110921', N'我认为二位', N'2013-03-23 23:46:12', N'527690766@qq.com', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (96, N'rec201303232347060343', N'cuc201303151116110921', N'qq群', N'2013-03-23 23:47:05', N'527690766@qq.com', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (97, N'rec201303232350110000', N'cuc201303151104330687', N'111', N'2013-03-23 23:50:10', N'527690766@qq.com', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (98, N'rec201303241712530578', N'cuc201303241709570161', N'aaaa', N'2013-03-24 17:12:53', N'pda', NULL, 2, 5)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (99, N'rec201303241714340296', N'cuc201303241709570161', N'sssss', N'2013-03-24 17:14:34', N'pda', NULL, 2, 3)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (100, N'rec201303241715030328', N'cuc201303241709570161', N'aaaacccc', N'2013-03-24 17:15:03', N'pda', NULL, 2, 3)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (101, N'rec201303242017490422', N'cuc201303241709570161', N'TAETAE', N'2013-03-24 20:17:49', N'527690766@qq.com', NULL, 1, 1)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (102, N'rec201303251145490640', N'cuc201303241709570161', N'我们', N'2013-03-25 11:45:49', N'527690766@qq.com', NULL, 1, 3)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (103, N'rec201303251146110265', N'cuc201303241709570161', N'水电费', N'2013-03-25 11:46:11', N'527690766@qq.com', NULL, 1, 1)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (104, N'rec201303251616570781', N'cuc201303112200540265', N'111', N'2013-03-25 16:16:57', N'527690766@qq.com', NULL, 1, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (105, N'rec201303270934450656', N'cuc201303241709570161', N'ssss', N'2013-03-27 09:34:45', N'admin', N'/attachment/complaintinfo/201303270934458965.png', NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (106, N'rec201303291031510500', N'cuc201303262259180765', N'aaaaa', N'2013-03-29 10:31:51', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (107, N'rec201303291440320671', N'cuc201303262259180765', N'好', N'2013-03-29 14:40:32', N'admin', NULL, NULL, NULL)
INSERT [dbo].[DT_ReCustomerComplaintInfo] ([id], [code], [customerComplaintCode], [reContent], [reDate], [rePerson], [attachment], [rePersonType], [complaintScore]) VALUES (108, N'rec201303291441160359', N'cuc201303241709570161', N'时尚', N'2013-03-29 14:41:16', N'admin', N'/attachment/complaintinfo/201303291441167185.xls', NULL, NULL)
SET IDENTITY_INSERT [dbo].[DT_ReCustomerComplaintInfo] OFF
/****** Object:  Table [dbo].[DT_ProductTypeTools]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ProductTypeTools](
	[productTypeCode] [varchar](32) NULL,
	[toolsCode] [varchar](32) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ProductTypeTools] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201301121756510710', N'tdl201301222219450605')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201301071535520790', N'tdl201301220107150762')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201301081510220618', N'tdl201301222219450605')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201301121756510710', N'tdl201301261554430080')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201301121756510710', N'tdl201301220107150762')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201301081510220618', N'tdl201304012032110046')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201303062244140942', N'tdl201304012032110046')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201303062258250082', N'tdl201304012032110046')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201303062305310621', N'tdl201304012032110046')
INSERT [dbo].[DT_ProductTypeTools] ([productTypeCode], [toolsCode]) VALUES (N'prt201303231459010678', N'tdl201304012032110046')
/****** Object:  Table [dbo].[DT_LanguageConfigDetail]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_LanguageConfigDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[languageCode] [varchar](32) NULL,
	[languageCfgCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_LANGUAGECONFIGDETAIL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_LanguageConfigDetail] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_LanguageConfigDetail] ON
INSERT [dbo].[DT_LanguageConfigDetail] ([id], [languageCode], [languageCfgCode]) VALUES (49, N'lag201301070146010554', N'lcf201301070156330030')
INSERT [dbo].[DT_LanguageConfigDetail] ([id], [languageCode], [languageCfgCode]) VALUES (50, N'lag201301070020090542', N'lcf201304061806050273')
SET IDENTITY_INSERT [dbo].[DT_LanguageConfigDetail] OFF
/****** Object:  Table [dbo].[DT_UserRole]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DT_UserRole](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[roleid] [int] NULL,
 CONSTRAINT [PK_DT_USERROLE] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_UserRole] DISABLE CHANGE_TRACKING
GO
/****** Object:  Table [dbo].[DT_SoftwareType]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SoftwareType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[proTypeCode] [varchar](32) NULL,
	[carSignPath] [varchar](1000) NULL,
 CONSTRAINT [PK_DT_SOFTWARETYPE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SoftwareType] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SoftwareType] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sft' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SoftwareType', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_SoftwareType] ON
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (1, N'45345', N'Aop', N'prt201301071535520790', N'vv')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (3, N'swt201301101726140193', N'ABARTH', N'prt201301081510220618', N'statics/attachment/language/autel/fcd67c0c24e84b16b5da69ffc16ba0c2.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (4, N'swt201301101729310963', N'AUDI', N'prt201301081510220618', N'statics/attachment/language/autel/7c8d846d6753449bab556d0a33509c16.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (5, N'swt201301101732330395', N'LANDROVER', N'prt201301081510220618', N'statics/attachment/language/autel/b4f5dc78408e4fb8966b4cde19ee51c9.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (6, N'swt201301101734340723', N'FORD_US', N'prt201301081510220618', N'statics/attachment/language/autel/daa544afe2904a90aebf04762d013331.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (9, N'swt201301101738110674', N'CHRYSLER', N'prt201301081510220618', N'statics/attachment/language/autel/0f976e310c2641bca41dcb1a1c06ca1f.jpg')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (10, N'swt201301151406190296', N'GM', N'prt201301081510220618', N'statics/attachment/language/autel/7dc5cd76a027493aad34c2e262371e26.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (11, N'swt201301151406480796', N'OBDII', N'prt201301081510220618', N'statics/attachment/language/autel/6e372f4f2506447f89ff1d2ae93623c4.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (12, N'swt201301151406540812', N'firmware', N'prt201301081510220618', N'statics/attachment/language/autel/69c129f6adb24221bb60b2e838f84f9e.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (15, N'swt201301151407320578', N'sys', N'prt201301081510220618', N'statics/attachment/language/autel/4a394c6c1daf44a98b5c9f1b9dbaf1b9.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (27, N'swt201301241933490694', N'estsete', N'prt201301121756510710', N'sfd')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (28, N'swt201301261538110331', N'Sf101', N'prt201301261537230766', N'/a/b')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (29, N'swt201301261544040290', N'V1.0', N'prt201301261537230766', N'a/b/c')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (36, N'swt201301301750240751', N'vte-2', N'prt201301301142520522', N'f:\\vte22')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (38, N'swt201301301754350059', N'vte-4', N'prt201301301142520522', N'f:\\vte')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (39, N'swt201301311010340335', N'vv-1', N'prt201301261537230766', N'statics/attachment/language/autel/1314cb8c292f44329247c417aca84bc7.jpg')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (41, N'swt201303031138450628', N'soft1001', N'prt201301301142520522', N'soft1001')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (42, N'swt201303231412530312', N'maxidas', N'prt201301071535520790', N'/123')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (43, N'swt201303231431420488', N'AUDI', N'prt201303081717190162', N'statics/attachment/language/autel/4f4e7cbd0c884a5a9a80060809d390e2.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (47, N'swt201303231435550282', N'HOLDEN', N'prt201303081717190162', N'statics/attachment/language/autel/6ec8f6f2db7b49e881c7931a32ce86ee.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (48, N'swt201303241940590799', N'AU FORD', N'prt201303081717190162', N'statics/attachment/language/autel/dbfe03f198bb4dad9c3bd4b5db76e860.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (49, N'swt201303241948050052', N'OBDII', N'prt201303081717190162', N'statics/attachment/language/autel/281e25bd2c9c4a20a31f58bd384c7c97.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (50, N'swt201303262326390344', N'SYS', N'prt201303081717190162', N'statics/attachment/language/autel/1ae69090b20e4f989fcd317987216d1f.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (51, N'swt201304061550380205', N'BMW', N'prt201303231459010678', N'statics/attachment/language/autel/e6493814a96b4f17bb939947c9de97ae.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (52, N'swt201304061551120489', N'BENZ', N'prt201303231459010678', N'statics/attachment/language/autel/16a19f7a82364f6c81b8359331821cca.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (53, N'swt201304061601130329', N'SYS', N'prt201303231459010678', N'statics/attachment/language/autel/344073beb07c493d898ef9b2e14a57d0.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (54, N'swt201304061603230819', N'OBDII', N'prt201303231459010678', N'statics/attachment/language/autel/28e8bfbed4104f22a67941d67009f13b.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (55, N'swt201304061603430414', N'AU FORD', N'prt201303231459010678', N'statics/attachment/language/autel/e6f17ea6bf434093ab092e0fdbcf5023.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (56, N'swt201304061604090549', N'HOLDEN', N'prt201303231459010678', N'statics/attachment/language/autel/d18812ec114f46e1ba04e7e6fbd570c8.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (57, N'swt201304061604210612', N'ACURA', N'prt201303231459010678', N'statics/attachment/language/autel/e3d1f8da84284e2ea9b574fa5d69180a.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (58, N'swt201304061605010971', N'DAEWOO', N'prt201303231459010678', N'statics/attachment/language/autel/b62808f94dd64e1cabf450970753ee40.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (59, N'swt201304061605160154', N'AUDI', N'prt201303231459010678', N'statics/attachment/language/autel/23429343d8824dbe87b45a5fb086dbef.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (60, N'swt201304061605290630', N'ABARTH', N'prt201303231459010678', N'statics/attachment/language/autel/a5a1628342dd4ab98c1488b23033b553.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (61, N'swt201304061605420596', N'ALFA', N'prt201303231459010678', N'statics/attachment/language/autel/338dcc92c5774882bacb55b497e11ad4.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (62, N'swt201304061606450626', N'CHRYSLER', N'prt201303231459010678', N'statics/attachment/language/autel/5a47215f6345483fa74dea78d0da5bf5.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (63, N'swt201304061607080664', N'GM', N'prt201303231459010678', N'statics/attachment/language/autel/4670befcc4d14796893b1175b41b97c0.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (64, N'swt201304061607250707', N'BENTLEY', N'prt201303231459010678', N'statics/attachment/language/autel/160569dd6e2a4d3b9f7481f9d7a245f8.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (65, N'swt201304061607440169', N'BUGATTI', N'prt201303231459010678', N'statics/attachment/language/autel/df5074a3e0d9461fb8e496fa2e84fa29.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (66, N'swt201304061608000896', N'CITROEN', N'prt201303231459010678', N'statics/attachment/language/autel/d02cd2cc46434caa9f94b24d8a1a3eaf.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (67, N'swt201304061608160156', N'DACIA', N'prt201303231459010678', N'statics/attachment/language/autel/d2b8710f75444f3391cb00aef62c157f.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (68, N'swt201304061642350538', N'ABARTH', N'prt201303081717190162', N'statics/attachment/language/autel/a98b39c479e249199b37ace6a44b0051.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (69, N'swt201304061642510549', N'ALFA', N'prt201303081717190162', N'statics/attachment/language/autel/6f0240d6ad95417cbb644b6394552bf3.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (70, N'swt201304061643070630', N'CHRYSLER', N'prt201303081717190162', N'statics/attachment/language/autel/3e5385b29d634869b4ddd4ca153ba295.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (71, N'swt201304061643200499', N'GM', N'prt201303081717190162', N'statics/attachment/language/autel/ee7a6bec79524b7cab2b4ebec6818dda.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (72, N'swt201304061644500667', N'SYS', N'prt201303062306080995', N'statics/attachment/language/autel/39e48cef24c94ddf95b763b91b44133e.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (73, N'swt201304061647120386', N'OBDII', N'prt201303062306080995', N'statics/attachment/language/autel/bcac4245067c4923a2dd88a48670aa94.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (74, N'swt201304061647310972', N'AU FORD', N'prt201303062306080995', N'statics/attachment/language/autel/f2df02fe5b1e458ebb0019ca27f74bdc.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (75, N'swt201304061647490662', N'ISUZU', N'prt201303062306080995', N'statics/attachment/language/autel/c2e744badac94ed28c94716fbee5bd54.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (76, N'swt201304061710160809', N'ALFA', N'prt201301081510220618', N'statics/attachment/language/autel/7e48b9d6c4e841ed8dfce85ad8c79409.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (77, N'swt201304061710270401', N'BENZ', N'prt201301081510220618', N'statics/attachment/language/autel/8fac9e9e3b95480d95abc20e6f5ad4de.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (78, N'swt201304061710390718', N'BMW', N'prt201301081510220618', N'statics/attachment/language/autel/4619c6e7ed1249bdbf4c2b0a258fab23.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (79, N'swt201304061710530938', N'BENTLEY', N'prt201301081510220618', N'statics/attachment/language/autel/9ef1d5afee554e9cb7772ac5b8059f50.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (80, N'swt201304061711120135', N'BUGATTI', N'prt201301081510220618', N'statics/attachment/language/autel/b3dbcb4dc6bf4deeb5428dc135ba5799.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (81, N'swt201304061711280590', N'CITROEN', N'prt201301081510220618', N'statics/attachment/language/autel/a125732d220f42f7bf240b7d394e57c4.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (82, N'swt201304061711450331', N'DACIA', N'prt201301081510220618', N'statics/attachment/language/autel/dd09796098424d37bf1750004d26c6d4.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (83, N'swt201304061712010604', N'EUFORD', N'prt201301081510220618', N'statics/attachment/language/autel/4757f7c10b6c494ca869485fe891ac53.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (84, N'swt201304061712140207', N'FIAT', N'prt201301081510220618', N'statics/attachment/language/autel/033d68c59c3b4620974fd7b9aa8f4ba6.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (85, N'swt201304061712290668', N'JAGUAR', N'prt201301081510220618', N'statics/attachment/language/autel/0c427dfbaf8e49edaa7854d46af1cf6c.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (86, N'swt201304061712440475', N'LANCIA', N'prt201301081510220618', N'statics/attachment/language/autel/90bc5cd205bb41b782e05338496e6f2f.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (87, N'swt201304061713110210', N'MINI', N'prt201301081510220618', N'statics/attachment/language/autel/ca99b241edc04b9ba1b66766584506aa.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (88, N'swt201304061713330135', N'MAYBACH', N'prt201301081510220618', N'statics/attachment/language/autel/efd90eb93a0846b3be597e42aa52ce0a.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (89, N'swt201304061713470184', N'OPEL', N'prt201301081510220618', N'statics/attachment/language/autel/de945ae7080449e1b43017c05e9b4c22.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (90, N'swt201304061714020515', N'PEUGEOT', N'prt201301081510220618', N'statics/attachment/language/autel/42365c2d32c64c1c8153b32e4eeeed30.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (91, N'swt201304061714140231', N'PORSCHE', N'prt201301081510220618', N'statics/attachment/language/autel/a8fc7f9c2cf547648aa8b17eb181cafe.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (92, N'swt201304061714290473', N'RENAULT', N'prt201301081510220618', N'statics/attachment/language/autel/26dfc2b3771e42c4b5f0344119e29220.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (93, N'swt201304061714420152', N'SAAB', N'prt201301081510220618', N'statics/attachment/language/autel/980e2e39c5af494aa89617f8d11c35a3.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (94, N'swt201304061714560620', N'SMART', N'prt201301081510220618', N'statics/attachment/language/autel/c23ed0c5755743e1a6bbbc7ca8c89a12.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (95, N'swt201304061717010268', N'SEAT', N'prt201301081510220618', N'statics/attachment/language/autel/a0b5e6b58d1b46f3818373150c531326.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (96, N'swt201304061717160783', N'SKODA', N'prt201301081510220618', N'statics/attachment/language/autel/552dc6929ccb4669b31202c76648f39e.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (97, N'swt201304061718100905', N'SPRINTER', N'prt201301081510220618', N'statics/attachment/language/autel/cd2ee5bbadda4200b11f6a1e95892810.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (98, N'swt201304061718430926', N'VW', N'prt201301081510220618', N'statics/attachment/language/autel/473f0b6c3a794ec3bfab8dd6249d1b9d.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (99, N'swt201304061719030063', N'VAUXHALL', N'prt201301081510220618', N'statics/attachment/language/autel/4ccc0805190746afa9927f613cb6efdf.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (100, N'swt201304061719190174', N'VOLVO', N'prt201301081510220618', N'statics/attachment/language/autel/f774741807d649c089e63d3c7e223fbb.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (101, N'swt201304061719380174', N'ACURA', N'prt201301081510220618', N'statics/attachment/language/autel/87d8d5981f5947ae8c3f41b1cbd57fdb.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (102, N'swt201304061719590748', N'DAEWOO', N'prt201301081510220618', N'statics/attachment/language/autel/34bb2ece85da41e0a5b124984a160a66.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (103, N'swt201304061720160256', N'HONDA', N'prt201301081510220618', N'statics/attachment/language/autel/1172b3622d74492d93b352ec66f7769d.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (104, N'swt201304061720300158', N'HYUNDAI', N'prt201301081510220618', N'statics/attachment/language/autel/5b189f10b53c41aba4cdd8f7f616ed90.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (105, N'swt201304061720430638', N'ISUZU', N'prt201301081510220618', N'statics/attachment/language/autel/f11a332fb4fe4ff380b2eaeeb918013c.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (106, N'swt201304061721030187', N'INFINITI', N'prt201301081510220618', N'statics/attachment/language/autel/774e40e0581a47b988e80e12bbaf0fbc.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (107, N'swt201304061721160892', N'KIA', N'prt201301081510220618', N'statics/attachment/language/autel/b3ca1b55e3784cd390a30db76e1d4e9f.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (108, N'swt201304061721280336', N'LEXUS', N'prt201301081510220618', N'statics/attachment/language/autel/9c68adce8d72425d97e78864814dea6b.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (109, N'swt201304061738530282', N'MAZDA', N'prt201301081510220618', N'statics/attachment/language/autel/72b703fc240a4217bd775788057f86f4.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (110, N'swt201304061739120332', N'MITSUBISHI', N'prt201301081510220618', N'statics/attachment/language/autel/c1bf0b8b9ef0442b8d52dd9f0812639e.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (111, N'swt201304061739280076', N'NISSAN', N'prt201301081510220618', N'statics/attachment/language/autel/e55f106f065d443b85b6d60de583b1cc.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (112, N'swt201304061739420867', N'SCION', N'prt201301081510220618', N'statics/attachment/language/autel/3d5d8e143d1b4f10862d9bb0fe6e527f.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (113, N'swt201304061739560902', N'SUBARU', N'prt201301081510220618', N'statics/attachment/language/autel/9fe0da438d86429b91a467fd32596d0a.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (114, N'swt201304061740300226', N'SUZUKI', N'prt201301081510220618', N'statics/attachment/language/autel/78b52935d4394adea181b620c3259c62.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (115, N'swt201304061741090009', N'TOYOTA', N'prt201301081510220618', N'statics/attachment/language/autel/d045008d8e8b42f18aa91275574b1853.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (116, N'swt201304061741530762', N'一汽轿车', N'prt201301081510220618', N'statics/attachment/language/autel/d9ebee6b263049b0aaadbcb74ccbd9ea.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (117, N'swt201304061749480405', N'Chery', N'prt201301081510220618', N'statics/attachment/language/autel/b4ea291a723d4a938d400a013c7d804c.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (118, N'swt201304061750070170', N'BYD', N'prt201301081510220618', N'statics/attachment/language/autel/5381ebdb49484851b4d377d3a48bc9a0.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (119, N'swt201304061753340058', N'Great Wall', N'prt201301081510220618', N'statics/attachment/language/autel/fcec502902a043248fe1d9ba7c478466.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (120, N'swt201304061842530764', N'AU Ford', N'prt201301081510220618', N'statics/attachment/language/autel/28962cf9e41244bc8fa0ff9e33c56eb2.png')
INSERT [dbo].[DT_SoftwareType] ([id], [code], [name], [proTypeCode], [carSignPath]) VALUES (121, N'swt201304061843120859', N'Holden', N'prt201301081510220618', N'statics/attachment/language/autel/f2d532be4ef744c0af6a0af3fc5439c2.png')
SET IDENTITY_INSERT [dbo].[DT_SoftwareType] OFF
/****** Object:  Table [dbo].[DT_SealerInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SealerInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[autelId] [varchar](250) NOT NULL,
	[userPwd] [varchar](250) NULL,
	[firstName] [varchar](250) NULL,
	[middleName] [varchar](250) NULL,
	[lastName] [varchar](250) NULL,
	[daytimePhone] [varchar](250) NULL,
	[daytimePhoneCC] [varchar](250) NULL,
	[daytimePhoneAC] [varchar](250) NULL,
	[daytimePhoneExtCode] [varchar](250) NULL,
	[mobilePhone] [varchar](250) NULL,
	[mobilePhoneCC] [varchar](250) NULL,
	[mobilePhoneAC] [varchar](250) NULL,
	[regTime] [varchar](250) NULL,
	[city] [varchar](250) NULL,
	[address] [varchar](250) NULL,
	[company] [varchar](250) NULL,
	[zipCode] [varchar](250) NULL,
	[email] [varchar](250) NULL,
	[isAllowSendEmail] [int] NULL,
	[lastLoginTime] [varchar](250) NULL,
	[country] [varchar](250) NULL,
	[languageCode] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[secondEmail] [varchar](250) NULL,
 CONSTRAINT [PK_DT_SealerInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SealerInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SealerInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'use' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SealerInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'加密' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SealerInfo', @level2type=N'COLUMN',@level2name=N'userPwd'
GO
SET IDENTITY_INSERT [dbo].[DT_SealerInfo] ON
INSERT [dbo].[DT_SealerInfo] ([id], [code], [autelId], [userPwd], [firstName], [middleName], [lastName], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [regTime], [city], [address], [company], [zipCode], [email], [isAllowSendEmail], [lastLoginTime], [country], [languageCode], [name], [secondEmail]) VALUES (1, N'sei20121228134915123', N'C-0003', N'A406F372E608BA46', N'w', N'a', N'an', N'23423432', N'86', N'010', N'', N'156456546546', N'86', N'0', NULL, N'北京', N'北京', N'太阳公司', N'100000', N'tt@qq.com', 1, NULL, N'中国', N'lag201301070146010554', N'aaaaaaaaaa', NULL)
INSERT [dbo].[DT_SealerInfo] ([id], [code], [autelId], [userPwd], [firstName], [middleName], [lastName], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [regTime], [city], [address], [company], [zipCode], [email], [isAllowSendEmail], [lastLoginTime], [country], [languageCode], [name], [secondEmail]) VALUES (4, N'sei201301161019460562', N'C-0001@tom.com', N'6E1268F4867007EC', N'T', N'A', N's', N'', N'', N'', N'', N'', N'', N'', N'2013-01-16 10:19:46', N'深圳', N'深圳南山', N'autel', N'518000', N'yy@qq.com', 1, N'2013-03-27 15:32:35', N'中国', N'lag201301070146010554', N'TAs', N'34534@qq.com')
INSERT [dbo].[DT_SealerInfo] ([id], [code], [autelId], [userPwd], [firstName], [middleName], [lastName], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [regTime], [city], [address], [company], [zipCode], [email], [isAllowSendEmail], [lastLoginTime], [country], [languageCode], [name], [secondEmail]) VALUES (37, N'sei201304011746040890', N'c-0001', N'5437EFDE63ACD6C6', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'2013-04-01 17:46:04', N'', N'', N'', N'', N'aaa@126.com', 0, N'2013-04-01 17:46:04', N'', N'lag201301070146010554', N'aaaaa', N'')
INSERT [dbo].[DT_SealerInfo] ([id], [code], [autelId], [userPwd], [firstName], [middleName], [lastName], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [regTime], [city], [address], [company], [zipCode], [email], [isAllowSendEmail], [lastLoginTime], [country], [languageCode], [name], [secondEmail]) VALUES (38, N'sei201304011845080510', N'111111111111', N'167D85A4A77C4458', N'', N'', N'', N'2832', N'86', N'755', N'8888', N'18682321330', N'86', N'755', N'2013-04-01 18:45:08', N'中国深圳中国深圳', N'中国深圳中国深圳', N'中国深圳中国深圳', N'中国深圳中国深圳', N'kfjdsfjdkfjk@163.com', 1, N'2013-04-01 18:45:08', N'中国', N'lag201301070146010554', N'aaaaa', N'kdj@126.com')
INSERT [dbo].[DT_SealerInfo] ([id], [code], [autelId], [userPwd], [firstName], [middleName], [lastName], [daytimePhone], [daytimePhoneCC], [daytimePhoneAC], [daytimePhoneExtCode], [mobilePhone], [mobilePhoneCC], [mobilePhoneAC], [regTime], [city], [address], [company], [zipCode], [email], [isAllowSendEmail], [lastLoginTime], [country], [languageCode], [name], [secondEmail]) VALUES (40, N'sei201304012121180676', N'李晶', N'BEF735D9AFFB03B8', N'', N'', N'', N'3232', N'86', N'0755', N'4343', N'', N'', N'', N'2013-04-01 21:21:18', N'深圳', N'中国广东深圳', N'深圳公司', N'ssssssss', N'453466@123.com', 1, N'2013-04-01 21:21:18', N'中国——2', N'lag201301070146010554', N'李晶', N'4534@123.com')
SET IDENTITY_INSERT [dbo].[DT_SealerInfo] OFF
/****** Object:  UserDefinedFunction [dbo].[f_str]    Script Date: 04/07/2013 14:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[f_str]
(
	@code varchar(10)
)
RETURNS varchar(100)
AS
BEGIN
DECLARE @re varchar(100)
SET @re=''
SELECT @re=@re+','+CAST(endTime as varchar)+ CAST(startTime as varchar)
FROM (select mpt.startTime,mpt.endTime,mpt.marketPromotionCode,mpb.promotionName,mpb.status from DT_MarketPromotionTimesInfo mpt left join DT_MarketPromotionBaseInfo mpb on mpt.marketPromotionCode = mpb.code) td
WHERE marketPromotionCode = @code
RETURN(STUFF(@re,1,1,''))
END
GO
/****** Object:  Table [dbo].[DT_SaleContract]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SaleContract](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[sealerCode] [varchar](32) NULL,
	[sealerTypeCode] [varchar](32) NULL,
	[proTypeCode] [varchar](32) NULL,
	[reChargePrice] [numeric](18, 0) NULL,
	[areaCfgCode] [varchar](32) NULL,
	[languageCfgCode] [varchar](32) NULL,
	[saleCfgCode] [varchar](32) NULL,
	[maxSaleCfgCode] [varchar](32) NULL,
	[contractDate] [varchar](255) NULL,
	[expirationTime] [varchar](255) NULL,
 CONSTRAINT [PK_DT_SALECONTRACT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SaleContract] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SaleContract] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sct' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SaleContract', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_SaleContract] ON
INSERT [dbo].[DT_SaleContract] ([id], [code], [name], [sealerCode], [sealerTypeCode], [proTypeCode], [reChargePrice], [areaCfgCode], [languageCfgCode], [saleCfgCode], [maxSaleCfgCode], [contractDate], [expirationTime]) VALUES (1, N'sac201301071639123456', N'契约00011', N'sei201301161019460562', N'set201301161650300359', N'prt201301071535520790', CAST(300 AS Numeric(18, 0)), N'acf201301080125020811', N'lcf201301070156330030', N'scf201301181516540901', N'scf201301181527090943', NULL, NULL)
INSERT [dbo].[DT_SaleContract] ([id], [code], [name], [sealerCode], [sealerTypeCode], [proTypeCode], [reChargePrice], [areaCfgCode], [languageCfgCode], [saleCfgCode], [maxSaleCfgCode], [contractDate], [expirationTime]) VALUES (3, N'sac201301261557100656', N'salecontact', N'sei20121228134915123', N'set20130105172215123', N'prt201301261537230766', CAST(1000 AS Numeric(18, 0)), N'acf201301080125020811', N'lcf201301070156330030', N'scf201301261556180297', N'scf201301261556180297', NULL, NULL)
INSERT [dbo].[DT_SaleContract] ([id], [code], [name], [sealerCode], [sealerTypeCode], [proTypeCode], [reChargePrice], [areaCfgCode], [languageCfgCode], [saleCfgCode], [maxSaleCfgCode], [contractDate], [expirationTime]) VALUES (4, N'sac201303031351290924', N'sale101', N'sei20121228134915123', N'set20130105172215123', N'prt201301301142520522', CAST(101 AS Numeric(18, 0)), N'acf201301080125020811', N'lcf201301070156330030', N'scf201303031350340170', N'scf201303231435080187', NULL, NULL)
INSERT [dbo].[DT_SaleContract] ([id], [code], [name], [sealerCode], [sealerTypeCode], [proTypeCode], [reChargePrice], [areaCfgCode], [languageCfgCode], [saleCfgCode], [maxSaleCfgCode], [contractDate], [expirationTime]) VALUES (5, N'sac201304012124340267', N'销售契约', N'sei201304012121180676', N'set201304011810060474', N'prt201303081717190162', CAST(38 AS Numeric(18, 0)), N'acf201301080125020811', N'lcf201301070156330030', N'scf201301181516540901', N'scf201301181527090943', N'2013-04-03', N'2013-04-27')
INSERT [dbo].[DT_SaleContract] ([id], [code], [name], [sealerCode], [sealerTypeCode], [proTypeCode], [reChargePrice], [areaCfgCode], [languageCfgCode], [saleCfgCode], [maxSaleCfgCode], [contractDate], [expirationTime]) VALUES (6, N'sac201304012125570875', N'销售契约', N'sei201304012121180676', N'set201304011810390661', N'prt201303081717190162', CAST(38 AS Numeric(18, 0)), N'acf201301080130400725', N'lcf201301070156330030', N'scf201301181240180212', N'scf201301181527090943', N'2013-04-03', N'2013-04-30')
INSERT [dbo].[DT_SaleContract] ([id], [code], [name], [sealerCode], [sealerTypeCode], [proTypeCode], [reChargePrice], [areaCfgCode], [languageCfgCode], [saleCfgCode], [maxSaleCfgCode], [contractDate], [expirationTime]) VALUES (7, N'sac201304012128470967', N'销售契约', N'sei201304012121180676', N'set201304011810390661', N'prt201303081717190162', CAST(38 AS Numeric(18, 0)), N'acf201301080130400725', N'lcf201301070156330030', N'scf201301181240180212', N'scf201301181527090943', N'2013-04-03', N'2013-04-30')
SET IDENTITY_INSERT [dbo].[DT_SaleContract] OFF
/****** Object:  Table [dbo].[DT_SoftwareVersion]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SoftwareVersion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[versionName] [varchar](250) NULL,
	[softwareTypeCode] [varchar](32) NULL,
	[releaseDate] [varchar](250) NULL,
	[releaseState] [int] NULL,
	[createDate] [varchar](250) NULL,
 CONSTRAINT [PK_DT_SOFTWAREVERSION] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SoftwareVersion] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SoftwareVersion] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'svs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SoftwareVersion', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_SoftwareVersion] ON
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (30, N'swv201301121747140995', N'v2U', N'swt201301101726140193', N'201301141407390275', 0, N'201301121747140995')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (31, N'swv201301121747370548', N'vt3', N'swt201301101729310963', N'201301141408220272', 1, N'201301121747370547')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (39, N'swv201303031142380754', N'v1', N'swt201303031138450628', NULL, 0, N'201303031142380754')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (40, N'swv201303031146400332', N'v2', N'swt201303031138450628', NULL, 0, N'201303031146400332')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (41, N'swv201303042041590564', N'abc', N'swt201301301754350059', NULL, 0, N'201303042041590564')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (43, N'swv201303051625150796', N'V2.0', N'swt201301301750240751', NULL, 0, N'201303051625150796')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (44, N'swv201303051625460953', N'V1.01', N'swt201301301750240751', N'201303062057190394', 1, N'201303051625460953')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (50, N'swv201303231537420078', N'v8-软件1-版本001', N'swt201303231431420488', N'201303231630590803', 0, N'201303231537420078')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (55, N'swv201303231544080933', N'v8-软件1-版本002', N'swt201303231431420488', NULL, 0, N'201303231544080933')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (56, N'swv201303311444070515', N'aaaa', N'swt201303231431420488', NULL, 0, N'201303311444070515')
INSERT [dbo].[DT_SoftwareVersion] ([id], [code], [versionName], [softwareTypeCode], [releaseDate], [releaseState], [createDate]) VALUES (57, N'swv201304061558440531', N'V1.0', N'swt201304061550380205', NULL, 0, N'201304061558440531')
SET IDENTITY_INSERT [dbo].[DT_SoftwareVersion] OFF
/****** Object:  Table [dbo].[DT_ReChargeCardType]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ReChargeCardType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[price] [varchar](250) NULL,
	[proTypeCode] [varchar](32) NULL,
	[saleCfgCode] [varchar](32) NULL,
	[areaCfgCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_RECHARGECARDTYPE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ReChargeCardType] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ReChargeCardType] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'rct' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ReChargeCardType', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_ReChargeCardType] ON
INSERT [dbo].[DT_ReChargeCardType] ([id], [code], [name], [price], [proTypeCode], [saleCfgCode], [areaCfgCode]) VALUES (3, N'rct201301242027120796', N'宝马001', N'100', N'prt201301301142520522', N'scf201301181527090943', N'acf201301080125020811')
INSERT [dbo].[DT_ReChargeCardType] ([id], [code], [name], [price], [proTypeCode], [saleCfgCode], [areaCfgCode]) VALUES (5, N'rct201301250905220828', N'宝马003', N'300', N'prt201301071535520790', N'scf201301181516540901', N'acf201301080125020811')
INSERT [dbo].[DT_ReChargeCardType] ([id], [code], [name], [price], [proTypeCode], [saleCfgCode], [areaCfgCode]) VALUES (9, N'rct201302041232200890', N'www', N'01', N'prt201301301142520522', N'scf201301181527090943', N'acf201301080125020811')
INSERT [dbo].[DT_ReChargeCardType] ([id], [code], [name], [price], [proTypeCode], [saleCfgCode], [areaCfgCode]) VALUES (13, N'rct201302051413090593', N'ssss', N'0', N'prt201301261537230766', N'scf201301261556180297', N'acf201301080125020811')
INSERT [dbo].[DT_ReChargeCardType] ([id], [code], [name], [price], [proTypeCode], [saleCfgCode], [areaCfgCode]) VALUES (14, N'rct201302051559070890', N'adesssss', N'0', N'prt201301301142520522', N'scf201301181527090943', N'acf201301080125020811')
INSERT [dbo].[DT_ReChargeCardType] ([id], [code], [name], [price], [proTypeCode], [saleCfgCode], [areaCfgCode]) VALUES (15, N'rct201302051559070891', N'v668999', N'111', N'prt201301071535520790', N'scf201303031350340170', N'acf201301080125020811')
SET IDENTITY_INSERT [dbo].[DT_ReChargeCardType] OFF
/****** Object:  Table [dbo].[DT_MarketPromotionDiscountRule]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MarketPromotionDiscountRule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[conditionDesc] [varchar](250) NULL,
	[discount] [numeric](18, 0) NULL,
	[marketPromotionRuleCode] [varchar](32) NULL,
	[betweenMonth] [int] NOT NULL,
	[endMonth] [int] NOT NULL,
 CONSTRAINT [PK_DT_MARKETPROMOTIONDISCOUNTR] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MarketPromotionDiscountRule] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_MarketPromotionDiscountRule] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pdr' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_MarketPromotionDiscountRule', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionDiscountRule] ON
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (3, N'mpd201301241559440000', N'到期前5至6个月续租', CAST(4 AS Numeric(18, 0)), N'mpr201301241458000708', 5, 6)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (7, N'mpd201301261603360340', N'到期前3至10个月续租', CAST(9 AS Numeric(18, 0)), N'mpr201301261603360164', 3, 10)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (8, N'mpd201301261604310410', N'过期3至6个月续租', CAST(7 AS Numeric(18, 0)), N'mpr201301261604310242', 3, 6)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (9, N'mpd201303031359450910', N'到期前1至3个月续租', CAST(5 AS Numeric(18, 0)), N'mpr201303031359450725', 1, 3)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (10, N'mpd201303031402250700', N'过期1至5个月续租', CAST(3 AS Numeric(18, 0)), N'mpr201303031402250517', 1, 5)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (11, N'mpd201303130859510930', N'到期前21至2个月续租', CAST(2 AS Numeric(18, 0)), N'mpr201303130859510875', 21, 2)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (12, N'mpd201303130859520001', N'到期前1至2个月续租', CAST(2 AS Numeric(18, 0)), N'mpr201303130859510875', 1, 2)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (13, N'mpd201303130907020280', N'到期前1至3个月续租', CAST(1 AS Numeric(18, 0)), N'mpr201303130907020218', 1, 3)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (14, N'mpd201303130907020341', N'到期前3至6个月续租', CAST(1 AS Numeric(18, 0)), N'mpr201303130907020218', 3, 6)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (15, N'mpd201303130907540450', N'过期1至3个月续租', CAST(1 AS Numeric(18, 0)), N'mpr201303130907540375', 1, 3)
INSERT [dbo].[DT_MarketPromotionDiscountRule] ([id], [code], [conditionDesc], [discount], [marketPromotionRuleCode], [betweenMonth], [endMonth]) VALUES (16, N'mpd201303281917460370', N'到期前1至2个月续租', CAST(9 AS Numeric(18, 0)), N'mpr201303281917460297', 1, 2)
SET IDENTITY_INSERT [dbo].[DT_MarketPromotionDiscountRule] OFF
/****** Object:  Table [dbo].[DT_OrderInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_OrderInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[orderMoney] [varchar](250) NULL,
	[orderDate] [varchar](250) NULL,
	[orderState] [int] NULL,
	[orderPayState] [int] NULL,
	[recConfirmState] [int] NULL,
	[discountMoney] [varchar](250) NULL,
	[bankNumber] [varchar](250) NULL,
	[userCode] [varchar](32) NULL,
	[code] [varchar](32) NULL,
	[processState] [int] NULL,
	[payDate] [varchar](250) NULL,
	[recConfirmDate] [varchar](250) NULL,
 CONSTRAINT [PK_DT_ORDERINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_OrderInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_OrderInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'oio' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_OrderInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_OrderInfo] ON
INSERT [dbo].[DT_OrderInfo] ([id], [orderMoney], [orderDate], [orderState], [orderPayState], [recConfirmState], [discountMoney], [bankNumber], [userCode], [code], [processState], [payDate], [recConfirmDate]) VALUES (151, N'349.0', N'2013-03-30 13:50:36', 0, 0, 0, N'0.0', NULL, N'cti20121228134915123', N'ori201303301350360557', 1, NULL, NULL)
INSERT [dbo].[DT_OrderInfo] ([id], [orderMoney], [orderDate], [orderState], [orderPayState], [recConfirmState], [discountMoney], [bankNumber], [userCode], [code], [processState], [payDate], [recConfirmDate]) VALUES (152, N'450.0', N'2013-03-30 15:33:23', 0, 0, 0, N'0.0', NULL, N'cti20121228134915123', N'ori201303301533230885', 1, NULL, NULL)
INSERT [dbo].[DT_OrderInfo] ([id], [orderMoney], [orderDate], [orderState], [orderPayState], [recConfirmState], [discountMoney], [bankNumber], [userCode], [code], [processState], [payDate], [recConfirmDate]) VALUES (153, N'101.0', N'2013-03-31 11:25:30', 1, 0, 0, N'0.0', NULL, N'cti20121228134915123', N'ori201303311125300921', 1, NULL, NULL)
INSERT [dbo].[DT_OrderInfo] ([id], [orderMoney], [orderDate], [orderState], [orderPayState], [recConfirmState], [discountMoney], [bankNumber], [userCode], [code], [processState], [payDate], [recConfirmDate]) VALUES (154, N'101.0', N'2013-03-31 11:34:35', 0, 0, 0, N'0.0', NULL, N'cti20121228134915123', N'ori201303311134350015', 1, NULL, NULL)
INSERT [dbo].[DT_OrderInfo] ([id], [orderMoney], [orderDate], [orderState], [orderPayState], [recConfirmState], [discountMoney], [bankNumber], [userCode], [code], [processState], [payDate], [recConfirmDate]) VALUES (155, N'349.0', N'2013-03-31 14:37:33', 1, 0, 0, N'0.0', NULL, N'cti20121228134915123', N'ori201303311437330687', 1, NULL, NULL)
INSERT [dbo].[DT_OrderInfo] ([id], [orderMoney], [orderDate], [orderState], [orderPayState], [recConfirmState], [discountMoney], [bankNumber], [userCode], [code], [processState], [payDate], [recConfirmDate]) VALUES (156, N'101.0', N'2013-03-31 15:03:39', 1, 0, 0, N'0.0', NULL, N'cti20121228134915123', N'ori201303311503390828', 1, NULL, NULL)
INSERT [dbo].[DT_OrderInfo] ([id], [orderMoney], [orderDate], [orderState], [orderPayState], [recConfirmState], [discountMoney], [bankNumber], [userCode], [code], [processState], [payDate], [recConfirmDate]) VALUES (157, N'349.0', N'2013-04-01 09:13:32', 1, 0, 0, N'0.0', NULL, N'cti20121228134915123', N'ori201304010913320159', 1, NULL, NULL)
INSERT [dbo].[DT_OrderInfo] ([id], [orderMoney], [orderDate], [orderState], [orderPayState], [recConfirmState], [discountMoney], [bankNumber], [userCode], [code], [processState], [payDate], [recConfirmDate]) VALUES (158, N'100.0', N'2013-04-01 20:02:40', 1, 0, 0, N'0.0', NULL, N'cti20121228134915123', N'ori201304012002400045', 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DT_OrderInfo] OFF
/****** Object:  Table [dbo].[DT_MinSaleUnitSoftwareDetail]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MinSaleUnitSoftwareDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minSaleUnitCode] [varchar](32) NULL,
	[softwareTypeCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_MINSALEUNITSOFTWAREDETAI] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MinSaleUnitSoftwareDetail] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ON
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (1, N'msu201301121637152342', N'swt201301151406190296')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (8, N'msu201301161426500468', N'45345')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (11, N'msu201301161426500468', N'swt201301101729310963')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (13, N'msu201301161426500468', N'swt201301101738110674')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (15, N'msu201301161426500468', N'swt201301101734340723')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (16, N'msu201301121637123457', N'swt201301101726140193')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (17, N'msu201301181526170539', N'swt201301151407320578')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (18, N'msu201301181526170539', N'swt201301151406540812')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (19, N'msu201301181526170539', N'swt201301151406190296')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (20, N'msu201301181526170539', N'swt201301101732330395')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (21, N'msu201301181526170539', N'swt201301101726140193')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (22, N'msu201301261546410851', N'swt201301261544040290')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (23, N'msu201301261546410851', N'swt201301261538110331')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (24, N'msu201301261546410851', N'swt201301241933490694')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (34, N'msu201304061823360237', N'swt201301101732330395')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (35, N'msu201304061823360237', N'swt201301101729310963')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (36, N'msu201304061823360237', N'swt201301101726140193')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (37, N'msu201304061823360237', N'swt201304061710160809')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (38, N'msu201304061823360237', N'swt201304061710270401')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (39, N'msu201304061823360237', N'swt201304061710390718')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (40, N'msu201304061823360237', N'swt201304061710530938')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (41, N'msu201304061823360237', N'swt201304061711120135')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (42, N'msu201304061823360237', N'swt201304061711280590')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (43, N'msu201304061823360237', N'swt201304061711450331')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (44, N'msu201304061823360237', N'swt201304061712010604')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (45, N'msu201304061823360237', N'swt201304061712140207')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (46, N'msu201304061823360237', N'swt201304061712290668')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (47, N'msu201304061823360237', N'swt201304061712440475')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (48, N'msu201304061823360237', N'swt201304061713330135')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (49, N'msu201304061823360237', N'swt201304061713110210')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (50, N'msu201304061823360237', N'swt201304061713470184')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (51, N'msu201304061823360237', N'swt201304061714020515')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (52, N'msu201304061823360237', N'swt201304061714290473')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (53, N'msu201304061823360237', N'swt201304061714420152')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (54, N'msu201304061823360237', N'swt201304061714560620')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (55, N'msu201304061823360237', N'swt201304061717010268')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (56, N'msu201304061823360237', N'swt201304061717160783')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (57, N'msu201304061823360237', N'swt201304061718100905')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (58, N'msu201304061823360237', N'swt201304061718430926')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (59, N'msu201304061823360237', N'swt201304061719030063')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (60, N'msu201304061823360237', N'swt201304061719190174')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (61, N'msu201303031203220658', N'swt201301101738110674')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (62, N'msu201303031203220658', N'swt201301101734340723')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (63, N'msu201303031203220658', N'swt201301151406190296')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (64, N'msu201304061840100520', N'swt201304061719380174')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (65, N'msu201304061840100520', N'swt201304061719590748')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (66, N'msu201304061840100520', N'swt201304061720160256')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (67, N'msu201304061840100520', N'swt201304061720300158')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (68, N'msu201304061840100520', N'swt201304061721030187')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (69, N'msu201304061840100520', N'swt201304061720430638')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (70, N'msu201304061840100520', N'swt201304061721160892')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (71, N'msu201304061840100520', N'swt201304061721280336')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (72, N'msu201304061840100520', N'swt201304061738530282')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (73, N'msu201304061840100520', N'swt201304061739120332')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (74, N'msu201304061840100520', N'swt201304061739280076')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (75, N'msu201304061840100520', N'swt201304061739420867')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (76, N'msu201304061840100520', N'swt201304061739560902')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (77, N'msu201304061840100520', N'swt201304061740300226')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (78, N'msu201304061840100520', N'swt201304061741090009')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (79, N'msu201304061843380949', N'swt201304061842530764')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (80, N'msu201304061843380949', N'swt201304061843120859')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (81, N'msu201304061844090023', N'swt201304061750070170')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (82, N'msu201304061844090023', N'swt201304061753340058')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (83, N'msu201304061844090023', N'swt201304061749480405')
INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] ([id], [minSaleUnitCode], [softwareTypeCode]) VALUES (84, N'msu201304061844090023', N'swt201301151406540812')
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnitSoftwareDetail] OFF
/****** Object:  Table [dbo].[DT_MinSaleUnitSaleCfgDetail]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MinSaleUnitSaleCfgDetail](
	[minSaleUnitCode] [varchar](32) NULL,
	[saleCfgCode] [varchar](32) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_DT_MinSaleUnitSaleCfgDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MinSaleUnitSaleCfgDetail] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ON
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301121637123457', N'scf201301181240180212', 9)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301261546410851', N'scf201301261556180297', 14)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201303031203220658', N'scf201303031350340170', 15)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201303031203220658', N'scf201303231435080187', 17)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301181526170539', N'scf201303231435080187', 18)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301121637123457', N'scf201303231435080187', 19)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301121637152342', N'scf201303231435080187', 20)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301261546410851', N'scf201301181516540901', 21)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301261546410851', N'scf201301181527090943', 22)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301161426500468', N'scf201301181527090943', 23)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201303031203220658', N'scf201301181527090943', 24)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301181526170539', N'scf201301181527090943', 25)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301121637123457', N'scf201301181527090943', 26)
INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] ([minSaleUnitCode], [saleCfgCode], [id]) VALUES (N'msu201301121637152342', N'scf201301181527090943', 27)
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnitSaleCfgDetail] OFF
/****** Object:  Table [dbo].[DT_MinSaleUnitPrice]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MinSaleUnitPrice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minSaleUnitCode] [varchar](32) NULL,
	[price] [varchar](250) NULL,
	[areaCfgCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_MINSALEUNITPRICE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MinSaleUnitPrice] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnitPrice] ON
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (1, N'msu201301121637152342', N'658', N'acf201301080125020811')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (2, N'msu201301121637123457', N'500', N'acf201301080125020811')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (5, N'msu201301121637152342', N'340', N'acf201301080130400725')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (6, N'msu201301161426500468', N'320', N'acf201301080125020811')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (7, N'msu201301181526170539', N'349', N'acf201301080130400725')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (8, N'msu201301181526170539', N'349', N'acf201301080125020811')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (9, N'msu201301121637123457', N'2222', N'acf201301080130400725')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (11, N'msu201301261546410851', N'1000', N'acf201301080125020811')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (12, N'msu201303031203220658', N'100', N'acf201301080130400725')
INSERT [dbo].[DT_MinSaleUnitPrice] ([id], [minSaleUnitCode], [price], [areaCfgCode]) VALUES (13, N'msu201303031203220658', N'100', N'acf201301080125020811')
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnitPrice] OFF
/****** Object:  Table [dbo].[DT_MinSaleUnitMemo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_MinSaleUnitMemo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minSaleUnitCode] [varchar](32) NULL,
	[languageCode] [varchar](32) NULL,
	[memo] [varchar](250) NULL,
	[name] [varchar](250) NULL,
 CONSTRAINT [PK_DT_MINSALEUNITMEMO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_MinSaleUnitMemo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnitMemo] ON
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (1, N'msu201301121637123457', N'lag201301070146010554', N'销售单位说明001', N'MD802_EN_3(全系统带数据流)')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (2, N'msu201301121637152342', N'lag201301070146010554', N'销售单位说明002', N'Check  Pro_EN')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (4, N'msu201301151652010229', N'lag201301070020090542', N'', N'en')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (6, N'msu201301121637123457', N'lag201301070020090542', N'test', N'test')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (12, N'msu201301181526170539', N'lag201301070146010554', NULL, N'MD802_EN_1(全系统)')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (15, N'msu201301261546410851', N'lag201301070020090542', N'abcd', N'abc')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (16, N'msu201301261546410851', N'lag201301070146010554', N'sale1', N'MD802_EN_0(四系统)')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (17, N'msu201301161426500468', N'lag201301070020090542', N'sale-1', N'sale-1')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (19, N'msu201301161426500468', N'lag201301070146010554', N'销售-1', N'MD802_EN_2(四系统带数据流)')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (27, N'msu201304061823360237', N'lag201301070146010554', N'DS708欧洲车', N'DS708欧洲车')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (28, N'msu201303031203220658', N'lag201301070146010554', N'DS708美国车', N'DS708美国车')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (29, N'msu201304061840100520', N'lag201301070146010554', N'DS708亚洲车', N'DS708亚洲车')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (30, N'msu201304061843380949', N'lag201301070146010554', N'DS708澳洲车', N'DS708澳洲车')
INSERT [dbo].[DT_MinSaleUnitMemo] ([id], [minSaleUnitCode], [languageCode], [memo], [name]) VALUES (31, N'msu201304061844090023', N'lag201301070146010554', N'DS708中国车', N'DS708中国车')
SET IDENTITY_INSERT [dbo].[DT_MinSaleUnitMemo] OFF
/****** Object:  Table [dbo].[DT_OrderMinSaleUnitDetail]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_OrderMinSaleUnitDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[orderCode] [varchar](32) NULL,
	[productSerialNo] [varchar](32) NULL,
	[minSaleUnitCode] [varchar](32) NULL,
	[consumeType] [int] NULL,
	[minSaleUnitPrice] [numeric](18, 2) NULL,
	[year] [int] NULL,
	[picPath] [varchar](1000) NOT NULL,
	[isSoft] [int] NOT NULL,
 CONSTRAINT [PK_DT_ORDERMINSALEUNITDETAIL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_OrderMinSaleUnitDetail] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_OrderMinSaleUnitDetail] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'omc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_OrderMinSaleUnitDetail', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_OrderMinSaleUnitDetail] ON
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (58, N'omu201303301350360573', N'ori201303301350360557', N'789456123', N'msu201301181526170539', 0, CAST(349.00 AS Numeric(18, 2)), 0, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 1)
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (59, N'omu201303301533230916', N'ori201303301533230885', N'789456123', N'scf201303031350340170', 1, CAST(101.00 AS Numeric(18, 2)), 1, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 0)
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (60, N'omu201303301533240025', N'ori201303301533230885', N'789456123', N'msu201301181526170539', 0, CAST(349.00 AS Numeric(18, 2)), 0, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 1)
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (61, N'omu201303311125300937', N'ori201303311125300921', N'789456123', N'scf201303031350340170', 1, CAST(101.00 AS Numeric(18, 2)), 1, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 0)
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (62, N'omu201303311134350031', N'ori201303311134350015', N'789456123', N'scf201303031350340170', 1, CAST(101.00 AS Numeric(18, 2)), 1, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 0)
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (63, N'omu201303311437330703', N'ori201303311437330687', N'789456123', N'msu201301181526170539', 0, CAST(349.00 AS Numeric(18, 2)), 0, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 1)
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (64, N'omu201303311503390828', N'ori201303311503390828', N'789456123', N'scf201303031350340170', 1, CAST(101.00 AS Numeric(18, 2)), 3, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 0)
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (65, N'omu201304010913320191', N'ori201304010913320159', N'789456123', N'msu201301181526170539', 0, CAST(349.00 AS Numeric(18, 2)), 0, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 1)
INSERT [dbo].[DT_OrderMinSaleUnitDetail] ([id], [code], [orderCode], [productSerialNo], [minSaleUnitCode], [consumeType], [minSaleUnitPrice], [year], [picPath], [isSoft]) VALUES (66, N'omu201304012002400107', N'ori201304012002400045', N'111111', N'msu201303031203220658', 0, CAST(100.00 AS Numeric(18, 2)), 0, N'http://113.106.60.20:8086/autelproweb/statics/attachment/language/autel/57a72a6b53ee48d8924c34e0c3302f07.jpg', 1)
SET IDENTITY_INSERT [dbo].[DT_OrderMinSaleUnitDetail] OFF
/****** Object:  Table [dbo].[DT_LanguagePack]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_LanguagePack](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[softwareVersionCode] [varchar](32) NULL,
	[downloadPath] [varchar](250) NULL,
	[testPath] [varchar](250) NULL,
	[languageTypeCode] [varchar](32) NULL,
	[memo] [varchar](250) NULL,
	[title] [varchar](250) NULL,
 CONSTRAINT [PK_DT_LANGUAGEPACK] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_LanguagePack] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_LanguagePack] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'lpk' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_LanguagePack', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_LanguagePack] ON
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (11, N'lpk201301121747150309', N'swv201301121747140995', N'tet1', N'F:\\te', N'lag201301070146010554', N'te', N'test')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (12, N'lpk201301121747370882', N'swv201301121747370548', N'', N'', N'lag201301070146010554', N'', N'')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (23, N'lpk201303031146400800', N'swv201303031146400332', N'/c', N'/c', N'lag201301070146010554', N'abc', N'abc')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (24, N'lpk201303042041590986', N'swv201303042041590564', N'/english', N'/abc', N'lag201301070020090542', N'sdfds', N'asdfd')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (26, N'lpk201303051625160218', N'swv201303051625150796', N'\f\fe\dfdf.war', N'\f\fe\dfdf.war', N'lag201301070020090542', N'fff', N'fff')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (27, N'lpk201303051625470375', N'swv201303051625460953', N'\f\fe\dfdf.war', N'\f\fe\dfdf.war', N'lag201301070020090542', N'fff', N'fff')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (34, N'lpk201303231537420172', N'swv201303231537420078', N'test', N'可测车型文档路径', N'lag201301070020090542', N'test', N'升级公告标题')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (39, N'lpk201303231544090042', N'swv201303231544080933', N'语言包相对路径', N'可测车型文档路径', N'lag201303212201380190', N'版本说明', N'升级公告标题')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (40, N'lpk201303311444070781', N'swv201303311444070515', N'yyy', N'yyyy', N'lag201301070020090542', N'yyyy', N'yyy')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (41, N'lpk201303311444070937', N'swv201303311444070515', N'zzzzzz', N'zzzzzzzz', N'lag201301070146010554', N'zzzzzzzz', N'zzzzzzz')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (42, N'lpk201304061558440888', N'swv201304061558440531', N'\d\d\r.rar', N'\d\d\r.rar', N'lag201301070020090542', N'fffff
f
f
f
f
f
f
', N'fff')
INSERT [dbo].[DT_LanguagePack] ([id], [code], [softwareVersionCode], [downloadPath], [testPath], [languageTypeCode], [memo], [title]) VALUES (43, N'lpk201304061558450015', N'swv201304061558440531', N'\d\d\r.rar', N'\d\d\r.rar', N'lag201301070146010554', N'fffff
f
f
f
f
f
f
', N'fff')
SET IDENTITY_INSERT [dbo].[DT_LanguagePack] OFF
/****** Object:  Table [dbo].[DT_ProductInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ProductInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[serialNo] [varchar](250) NULL,
	[saleContractCode] [varchar](32) NOT NULL,
	[regPwd] [varchar](250) NULL,
	[proDate] [varchar](250) NULL,
	[regStatus] [int] NULL,
	[regTime] [varchar](250) NULL,
	[noRegReason] [varchar](250) NULL,
	[externInfo1] [varchar](250) NULL,
	[externInfo2] [varchar](250) NULL,
	[externInfo3] [varchar](250) NULL,
	[externInfo4] [varchar](250) NULL,
	[externInfo5] [varchar](250) NULL,
	[proTypeCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_PRODUCTINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ProductInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ProductInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_2] ON [dbo].[DT_ProductInfo] 
(
	[serialNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ProductInfo', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_ProductInfo] ON
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (11, N'pro201301071637123456', N'123456789', N'sac201303031351290924', N'123456', N'2013-01-01', 1, N'2013-01-29 11:08:50', N'ssss', N'eert', N'', N'ertert', N'', N'', N'prt201301081510220618')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (13, N'pro201301071637123457', N'1234567890', N'sac201303031351290924', N'123456', N'2013-03-15', 1, N'2013-01-29 10:57:44', N'ssss', N'11111111', N'', N'', N'', N'', N'prt201301071535520790')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (18, N'pro201301090108080139', N'23234234', N'sac201301071639123456', N'test', N'2012-02-12', 1, NULL, N'gghh', N'123', N'123', N'123', N'', N'', N'prt201301071535520790')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (20, N'pro201301092319290815', N'111111111', N'sac201301071639123456', N'12313', N'2013-01-18', 1, NULL, N'fewsfdfd', N'23423', N'', N'', N'', N'', N'prt201301081510220618')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (27, N'pro201301261637110688', N'asd', N'sac201301261557100656', N'sfdas', N'2013-01-03', 0, NULL, N'', N'llllllllllllllllllllllll', N'', N'', N'', N'', N'prt201301261537230766')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (28, N'pro201301311041220921', N'123456999', N'sac201301071639123456', N'xdsasasaa', N'2013-01-31', 1, N'2013-04-01 19:18:36', N'', N'', N'', N'', N'', N'', N'prt201301301142520522')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (29, N'pro201303031536420710', N'123456', N'sac201301071639123456', N'123456', N'2013-03-02', 1, N'2013-03-04 10:31:14', NULL, N'', N'', N'', N'', N'', N'prt201301301142520522')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (30, N'pro201303050939180265', N'987654', N'sac201303031351290924', N'123456', N'2013-03-02', 1, N'2013-03-05 09:51:21', NULL, N'', N'', N'', N'', N'', N'prt201301301142520522')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (31, N'pro201303050952360906', N'111', N'sac201303031351290924', N'111', N'2013-03-05', 1, N'2013-03-05 09:53:50', NULL, N'', N'', N'', N'', N'', N'prt201301301142520522')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (32, N'pro201303072123110636', N'0000000000000099999', N'sac201301071639123456', N'77777777777', N'2013-03-31', NULL, NULL, NULL, N'11112!!!!!!!', N'@@@@@@@@@@', N'########', N'$$$$$$$$', N'%^&×()_+-=', N'prt201303062305310621')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (33, N'pro201303072133410894', N'787wqe~!@#$%^&×()_+=-', N'sac201303031351290924', N'+_)()_(×(&^&%^&%$', N'2013-02-31', NULL, NULL, NULL, N'999999', N'000000', N'iuuuuuuuuuuuuuuu', N'mkk,.kl;k;lkl;', N'                ', N'prt201303062258250082')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (34, N'pro201303081720150473', N'asffffff', N'sac201303031351290924', N'1111111', N'2013-03-15', NULL, NULL, NULL, N'', N'', N'', N'', N'', N'prt201303081717190162')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (35, N'pro201303082152590151', N'dddddddddddddddddddddddddddddddd', N'sac201301261557100656', N'dddddddddddddddddddddd', N'2013-03-15', NULL, NULL, NULL, N'', N'', N'', N'', N'', N'prt201303081717190162')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (36, N'pro201303082156480171', N'11111111111111111111111111111111', N'sac201301261557100656', N'1111111111111111111111', N'2013-03-08', NULL, NULL, NULL, N'333333333333333333333333333333', N'444444444444444444444444444444', N'111111111111111111111111111111', N'222222222222222222222222222222', N'222225555555555555555555555555', N'prt201303081717190162')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (37, N'pro201303140003460877', N'33333', N'sac201303031351290924', N'33234324', N'2013-03-15', NULL, NULL, NULL, N'', N'', N'', N'', N'', N'prt201303062258250082')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (38, N'pro201303140016500406', N'2222222222222222', N'sac201301261557100656', N'222222222222', N'2013-03-16', 1, N'2013-03-05 09:51:21', NULL, N'1111111111111111', N'4444444444444444', N'4444444444444444', N'4444444444444444', N'4444444444444444', N'prt201303062306080995')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (39, N'pro201303171434250618', N'2222', N'sac201301261557100656', N'222222222222', N'2013-03-09', 1, N'2013-03-17', NULL, N'', N'', N'', N'', N'', N'prt201303081717190162')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (40, N'pro201303231415130843', N'789456123', N'sac201303031351290924', N'789456123', N'2013-03-23', 1, N'2013-03-23 14:24:44', NULL, N'', N'', N'', N'', N'', N'prt201301071535520790')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (41, N'pro201303231646300869', N'22222222', N'sac201301261557100656', N'22222222222', N'2013-03-01', NULL, NULL, NULL, N'', N'', N'', N'', N'', N'prt201303231459010678')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (42, N'pro201304011924280001', N'1234567', N'sac201301071639123456', N'111111', N'2013-04-18', 1, N'2013-04-01 19:25:01', NULL, N'', N'', N'', N'', N'', N'prt201303231459010678')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (43, N'pro201304011952430919', N'111111', N'sac201301071639123456', N'111111', N'2013-04-02', 1, N'2013-04-01 19:53:30', NULL, N'', N'', N'', N'', N'', N'prt201301071535520790')
INSERT [dbo].[DT_ProductInfo] ([id], [code], [serialNo], [saleContractCode], [regPwd], [proDate], [regStatus], [regTime], [noRegReason], [externInfo1], [externInfo2], [externInfo3], [externInfo4], [externInfo5], [proTypeCode]) VALUES (44, N'pro201304020928120234', N'321654', N'sac201303031351290924', N'321654', N'2013-04-10', 1, N'2013-04-02 09:32:52', NULL, N'', N'', N'', N'', N'', N'prt201303231459010678')
SET IDENTITY_INSERT [dbo].[DT_ProductInfo] OFF
/****** Object:  Table [dbo].[DT_ReChargeCardInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ReChargeCardInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[reChargeCardTypeCode] [varchar](32) NULL,
	[serialNo] [varchar](250) NULL,
	[reChargePwd] [varchar](250) NULL,
	[validDate] [varchar](250) NULL,
	[isUse] [int] NULL,
	[isActive] [int] NULL,
 CONSTRAINT [PK_DT_RECHARGECARDINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ReChargeCardInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ReChargeCardInfo] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DT_ReChargeCardInfo] ON
INSERT [dbo].[DT_ReChargeCardInfo] ([id], [code], [reChargeCardTypeCode], [serialNo], [reChargePwd], [validDate], [isUse], [isActive]) VALUES (2, N'rco201301242027120796', N'rct201301242027120796', N'123456789', N'lererelkd', N'2013-01-25', 2, 2)
INSERT [dbo].[DT_ReChargeCardInfo] ([id], [code], [reChargeCardTypeCode], [serialNo], [reChargePwd], [validDate], [isUse], [isActive]) VALUES (7, N'rco201301242027120800', N'rct201301242027120796', N'123456794', N'lererelkd', N'2013-01-25', 1, 2)
INSERT [dbo].[DT_ReChargeCardInfo] ([id], [code], [reChargeCardTypeCode], [serialNo], [reChargePwd], [validDate], [isUse], [isActive]) VALUES (72, N'rco201302191350090890', N'rct201301242027120796', N'123456001', N'XDFDFDFEF', N'2013-01-25', 2, 2)
INSERT [dbo].[DT_ReChargeCardInfo] ([id], [code], [reChargeCardTypeCode], [serialNo], [reChargePwd], [validDate], [isUse], [isActive]) VALUES (76, N'rco201303081720290250', N'rct201302051413090593', N'123456005', N'XDFDFDFEF', N'2014-01-05', 1, 2)
INSERT [dbo].[DT_ReChargeCardInfo] ([id], [code], [reChargeCardTypeCode], [serialNo], [reChargePwd], [validDate], [isUse], [isActive]) VALUES (97, N'rco201303081957330468', N'rct201302041232200890', N'123456003', N'XDFDFDFEF', N'2014-01-03', 1, 2)
INSERT [dbo].[DT_ReChargeCardInfo] ([id], [code], [reChargeCardTypeCode], [serialNo], [reChargePwd], [validDate], [isUse], [isActive]) VALUES (100, N'rco201303271640370968', N'rct201301250905220828', N'123456002', N'XDFDFDFEF', N'2014-01-02', 1, 1)
INSERT [dbo].[DT_ReChargeCardInfo] ([id], [code], [reChargeCardTypeCode], [serialNo], [reChargePwd], [validDate], [isUse], [isActive]) VALUES (101, N'rco201303271640390156', N'rct201301250905220828', N'123456004', N'XDFDFDFEF', N'2014-01-04', 1, 2)
INSERT [dbo].[DT_ReChargeCardInfo] ([id], [code], [reChargeCardTypeCode], [serialNo], [reChargePwd], [validDate], [isUse], [isActive]) VALUES (102, N'rco201303271640390157', N'rct201302051559070891', N'11111111', N'123456789', N'2014-3-27', 2, 2)
SET IDENTITY_INSERT [dbo].[DT_ReChargeCardInfo] OFF
/****** Object:  Table [dbo].[DT_SoftwarePack]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_SoftwarePack](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[softwareVersionCode] [varchar](32) NULL,
	[downloadPath] [varchar](250) NULL,
 CONSTRAINT [PK_DT_SOFTWAREPACK] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_SoftwarePack] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_SoftwarePack] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'spk' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_SoftwarePack', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_SoftwarePack] ON
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (8, N'swp201301121747150153', N'swv201301121747140995', N'testU')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (9, N'swp201301121747370723', N'swv201301121747370548', N'vt3')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (17, N'swp201303031142390222', N'swv201303031142380754', N'v1')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (18, N'swp201303031146400566', N'swv201303031146400332', N'v2')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (19, N'swp201303042041590799', N'swv201303042041590564', N'/abc')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (21, N'swp201303051625160015', N'swv201303051625150796', N'\f\f\asdf.war')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (22, N'swp201303051625470156', N'swv201303051625460953', N'\f\f\asdf.war')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (28, N'swp201303231537420125', N'swv201303231537420078', N'iii')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (33, N'swp201303231544080995', N'swv201303231544080933', N'cess')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (34, N'swp201303311444070656', N'swv201303311444070515', N'aaaa')
INSERT [dbo].[DT_SoftwarePack] ([id], [code], [softwareVersionCode], [downloadPath]) VALUES (35, N'swp201304061558440729', N'swv201304061558440531', N'\a\b\d.rar')
SET IDENTITY_INSERT [dbo].[DT_SoftwarePack] OFF
/****** Object:  Table [dbo].[DT_ShoppingCart]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ShoppingCart](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[customerCode] [varchar](32) NOT NULL,
	[minSaleUnitCode] [varchar](32) NOT NULL,
	[areaCfgCode] [varchar](32) NOT NULL,
	[proName] [varchar](250) NOT NULL,
	[proSerial] [varchar](32) NOT NULL,
	[proCode] [varchar](32) NOT NULL,
	[year] [int] NULL,
	[type] [int] NOT NULL,
	[picPath] [varchar](1000) NOT NULL,
	[isSoft] [int] NOT NULL,
	[cfgPrice] [numeric](18, 2) NULL,
	[validData] [varchar](32) NULL,
 CONSTRAINT [PK_DT_SHOPPINGCART] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ShoppingCart] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0、续租；1、购买' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ShoppingCart', @level2type=N'COLUMN',@level2name=N'type'
GO
SET IDENTITY_INSERT [dbo].[DT_ShoppingCart] ON
INSERT [dbo].[DT_ShoppingCart] ([id], [customerCode], [minSaleUnitCode], [areaCfgCode], [proName], [proSerial], [proCode], [year], [type], [picPath], [isSoft], [cfgPrice], [validData]) VALUES (29, N'cti20121228134915123', N'scf201303031350340170', N'acf201301080125020811', N'v10', N'321654', N'pro201304020928120234', 1, 1, N'http://localhost:8081/autelproweb/statics/attachment/language/autel/3c98f92129c445d7a2f7a7495fa7c3da.gif', 0, CAST(101.00 AS Numeric(18, 2)), N'2014/04/02')
SET IDENTITY_INSERT [dbo].[DT_ShoppingCart] OFF
/****** Object:  Table [dbo].[DT_ReChargeRecord]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ReChargeRecord](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[proCode] [varchar](32) NULL,
	[reChargeCardCode] [varchar](32) NULL,
	[reChargeTime] [varchar](250) NULL,
	[memberCode] [varchar](32) NULL,
	[memberType] [int] NULL,
 CONSTRAINT [PK_DT_RECHARGERECORD] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ReChargeRecord] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ReChargeRecord] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DT_ReChargeRecord] ON
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (1, N'rcr201301250027130484', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (4, N'rcr201303071500130343', N'pro201303050939180265', N'rco201302191350090890', N'2013-03-07 15:00:10', N'cui201301231020360421', 1)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (5, N'rcr201303071500390343', N'pro201303050939180265', N'rco201302191350090890', N'2013-03-07 15:00:37', N'cui201301231020360421', 1)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (6, N'rcr201303071500440750', N'pro201303050952360906', N'rco201302191350090890', N'2013-03-07 15:00:42', N'cui201301231020360421', 1)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (7, N'rcr201303081427580546', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (8, N'rcr201303081430090984', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (9, N'rcr201303081434370984', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (10, N'rcr201303081527040828', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (11, N'rcr201303081529470296', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (14, N'rcr201303101437420312', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (12, N'rcr201303081531140781', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (13, N'rcr201303081532210000', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (15, N'rcr201303101504430853', NULL, NULL, N'1', NULL, 2)
INSERT [dbo].[DT_ReChargeRecord] ([id], [code], [proCode], [reChargeCardCode], [reChargeTime], [memberCode], [memberType]) VALUES (16, N'rcr201303281011320265', N'pro201303231415130843', N'rco201303271640390157', N'2013-03-28 10:11:29', N'cti20121228134915123', 1)
SET IDENTITY_INSERT [dbo].[DT_ReChargeRecord] OFF
/****** Object:  Table [dbo].[DT_ProductUpgradeRecord]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ProductUpgradeRecord](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[proCode] [varchar](32) NULL,
	[softwareTypeCode] [varchar](32) NULL,
	[versionCode] [varchar](32) NULL,
	[upgradeTime] [varchar](250) NULL,
 CONSTRAINT [PK_DT_PRODUCTUPGRADERECORD] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ProductUpgradeRecord] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ProductUpgradeRecord] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pru' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ProductUpgradeRecord', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_ProductUpgradeRecord] ON
INSERT [dbo].[DT_ProductUpgradeRecord] ([id], [code], [proCode], [softwareTypeCode], [versionCode], [upgradeTime]) VALUES (3, N'xxx201301071637123456', N'pro201301071637123456', N'swt201301101726140193', N'swv201301121746260742', N'2013-01-01 12:00:56')
INSERT [dbo].[DT_ProductUpgradeRecord] ([id], [code], [proCode], [softwareTypeCode], [versionCode], [upgradeTime]) VALUES (4, N'xxx201301071637123457', N'pro201301071637123456', N'swt201301101726140193', N'swv201301121747140995', N'2013-01-01 14:00:56')
SET IDENTITY_INSERT [dbo].[DT_ProductUpgradeRecord] OFF
/****** Object:  Table [dbo].[DT_ProductSoftwareValidStatus]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_ProductSoftwareValidStatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[proCode] [varchar](32) NULL,
	[validDate] [varchar](250) NULL,
	[minSaleUnitCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_PRODUCTSOFTWAREVALIDSTAT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_ProductSoftwareValidStatus] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_ProductSoftwareValidStatus] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'prv' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_ProductSoftwareValidStatus', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_ProductSoftwareValidStatus] ON
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (23, N'psv201301291057580109', N'pro201301071637123457', N'2013-5-5', N'msu201301121637123457')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (24, N'psv201301291058030671', N'pro201301071637123457', N'2013-5-5', N'msu201301161426500468')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (25, N'psv201301291108500140', N'pro201301071637123456', N'2013-5-5', N'msu201301121637123457')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (26, N'psv201301291108500203', N'pro201301071637123456', N'2013-5-5', N'msu201301161426500468')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (27, N'psv201303041031290328', N'pro201303031536420710', N'2013-5-5', N'msu201301161426500468')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (28, N'psv201303050950160375', N'pro201303050939180265', N'2013-5-5', N'msu201303031203220658')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (29, N'psv201303050951210562', N'pro201303050939180265', N'2013-5-5', N'msu201303031203220658')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (30, N'psv201303050953500921', N'pro201303050952360906', N'2013-5-5', N'msu201303031203220658')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (31, N'psv201303231424440468', N'pro201303231415130843', N'2013-5-5', N'msu201303031203220658')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (32, N'psv201304011913580592', N'pro201301311041220921', N'2014-04-01', N'msu201301161426500468')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (33, N'psv201304011914480891', N'pro201301311041220921', N'2014-04-01', N'msu201301161426500468')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (34, N'psv201304011915570480', N'pro201301311041220921', N'2014-04-01', N'msu201301161426500468')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (35, N'psv201304011918360572', N'pro201301311041220921', N'2014-04-01', N'msu201301161426500468')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (36, N'psv201304011925020104', N'pro201304011924280001', N'2014-04-01', N'msu201301161426500468')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (37, N'psv201304011953300860', N'pro201304011952430919', N'2014-04-01', N'msu201301261546410851')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (38, N'psv201304020930520375', N'pro201304020928120234', N'2014-04-02', N'msu201303031203220658')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (39, N'psv201304020933060140', N'pro201304020928120234', N'2014-04-02', N'msu201303031203220658')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (40, N'psv201304020938200937', N'pro201304020928120234', N'2014-04-02', N'msu201303031203220658')
INSERT [dbo].[DT_ProductSoftwareValidStatus] ([id], [code], [proCode], [validDate], [minSaleUnitCode]) VALUES (41, N'psv201304020944210828', N'pro201304020928120234', N'2014-04-02', N'msu201303031203220658')
SET IDENTITY_INSERT [dbo].[DT_ProductSoftwareValidStatus] OFF
/****** Object:  Table [dbo].[DT_CustomerProInfo]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_CustomerProInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[customerCode] [varchar](32) NULL,
	[proCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_CUSTOMERPROINFO] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_CustomerProInfo] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DT_CustomerProInfo] ON [dbo].[DT_CustomerProInfo] 
(
	[proCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DT_CustomerProInfo] ON
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (6, N'cti20121228134915123', N'pro201301071637123456')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (13, N'cti20121228134915123', N'pro201301071637123457')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (15, N'cti20121228134915123', N'pro201301092319290815')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (19, N'cui201301121425320671', N'pro201301090108080139')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (27, N'cti20121228134915123', N'pro201303031536420710')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (28, N'cui201301231020360421', N'pro201303050939180265')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (30, N'cui201301231020360421', N'pro201303050952360906')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (31, N'cti20121228134915123', N'pro201303231415130843')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (32, N'cti20121228134915123', N'pro201301311041220921')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (36, N'cti20121228134915123', N'pro201304011924280001')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (37, N'cti20121228134915123', N'pro201304011952430919')
INSERT [dbo].[DT_CustomerProInfo] ([id], [customerCode], [proCode]) VALUES (38, N'cti20121228134915123', N'pro201304020928120234')
SET IDENTITY_INSERT [dbo].[DT_CustomerProInfo] OFF
/****** Object:  Table [dbo].[DT_TestPack]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_TestPack](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[name] [varchar](250) NULL,
	[softwareTypeCode] [varchar](32) NULL,
	[releaseDate] [varchar](250) NULL,
	[downloadPath] [varchar](250) NULL,
	[proCode] [varchar](32) NULL,
 CONSTRAINT [PK_DT_TESTPACK] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_TestPack] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_TestPack] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'svs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_TestPack', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_TestPack] ON
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (2, N'tpk201301261044250046', N'宝马（测试001）', N'swt201301101726140193', N'2013-01-26 10:44:25', N'sfdodf/ssrer.tt', N'pro201301071637123456')
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (7, N'tpk201301302231390125', N'hhhhhhh', N'swt201301101726140193', N'2013-01-30 22:31:39', N'hhhhhhhhh', N'pro201301071637123456')
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (14, N'tpk201302011842320968', N'jjjjjjjj', N'swt201301101726140193', N'2013-02-01 18:42:32', N'jjjjjjjjjjj', N'pro201301071637123457')
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (15, N'tpk201302012308180296', N'sssswwwww', N'swt201301101726140193', N'2013-02-01 23:08:18', N'rrrrrrr', N'pro201301071637123456')
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (16, N'tpk201302021058490437', N'测试002', N'swt201301101726140193', N'2013-02-02 10:58:49', N'tttttt/aaaaa', N'pro201301071637123456')
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (18, N'tpk201302021119410203', N'测试3', N'swt201301101726140193', N'2013-02-02 11:19:41', N'hhhhhh', N'pro201301071637123456')
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (19, N'tpk201302021302280296', N'aaa', N'swt201301101726140193', N'2013-02-02 13:02:28', N'aaaa', N'pro201301071637123457')
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (20, N'tpk201303031459350319', N'bcd', N'45345', N'2013-03-03 14:59:35', N'sdf', N'pro201301071637123456')
INSERT [dbo].[DT_TestPack] ([id], [code], [name], [softwareTypeCode], [releaseDate], [downloadPath], [proCode]) VALUES (21, N'tpk201303061429400203', N'dfdf', N'swt201301151407320578', N'2013-03-06 14:29:40', N'aaaaddddddd', N'pro201301071637123456')
SET IDENTITY_INSERT [dbo].[DT_TestPack] OFF
/****** Object:  Table [dbo].[DT_TestPackLanguagePack]    Script Date: 04/07/2013 14:53:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DT_TestPackLanguagePack](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](32) NULL,
	[testPackCode] [varchar](32) NULL,
	[downloadPath] [varchar](250) NULL,
	[testFilePath] [varchar](250) NULL,
	[languageTypeCode] [varchar](32) NULL,
	[memo] [varchar](250) NULL,
 CONSTRAINT [PK_DT_TESTPACKLANGUAGEPACK] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DT_TestPackLanguagePack] DISABLE CHANGE_TRACKING
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [Index_1] ON [dbo].[DT_TestPackLanguagePack] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'lpk' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DT_TestPackLanguagePack', @level2type=N'COLUMN',@level2name=N'code'
GO
SET IDENTITY_INSERT [dbo].[DT_TestPackLanguagePack] ON
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (3, N'tpl201301261044250375', N'tpk201301261044250046', N'fdfdf.dd', N'ererer.doc', N'lag201301070020090542', N'test001')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (4, N'tpl201301261044250515', N'tpk201301261044250046', N'sfdfdf.dd', N'dewerer.doc', N'lag201301070146010554', N'测试001')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (8, N'tpl201301302231390484', N'tpk201301302231390125', N'hhhh', N'hhhhh', N'lag201301070020090542', N'hhhhhh')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (9, N'tpl201301302231390843', N'tpk201301302231390125', N'hhhh', N'hhhhh', N'lag201301070146010554', N'hhhhhh测试')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (10, N'tpl201301302231400218', N'tpk201301302231390125', N'hhhh', N'hhhhh', N'lag201301261639170385', N'hhhhhrtt')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (18, N'tpl201302011842330421', N'tpk201302011842320968', N'ssss', N'sssss', N'lag201301070020090542', N'ssss')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (19, N'tpl201302012308180828', N'tpk201302012308180296', N'ssss', N'ssss', N'lag201301070020090542', N'sssss')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (20, N'tpl201302021058490578', N'tpk201302021058490437', N'test/sss', N'sssss', N'lag201301070020090542', N'')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (21, N'tpl201302021058490687', N'tpk201302021058490437', N'test/sss', N'sssss', N'lag201301070146010554', N'测试')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (23, N'tpl201302021119410390', N'tpk201302021119410203', N'hh', N'hh', N'lag201301070020090542', N'hhh')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (24, N'tpl201302021302280421', N'tpk201302021302280296', N'aa', N'a', N'lag201301070020090542', N'a')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (25, N'tpl201303031459350600', N'tpk201303031459350319', N'/ad', N'sdf', N'lag201301070020090542', N'sdf')
INSERT [dbo].[DT_TestPackLanguagePack] ([id], [code], [testPackCode], [downloadPath], [testFilePath], [languageTypeCode], [memo]) VALUES (26, N'tpl201303061429400390', N'tpk201303061429400203', N'sf', N'ds', N'lag201301070020090542', N'ff')
SET IDENTITY_INSERT [dbo].[DT_TestPackLanguagePack] OFF
/****** Object:  Default [DF__cop_app__appid__09FE775D]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT (NULL) FOR [appid]
GO
/****** Object:  Default [DF__cop_app__app_nam__0AF29B96]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT (NULL) FOR [app_name]
GO
/****** Object:  Default [DF__cop_app__author__0BE6BFCF]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT (NULL) FOR [author]
GO
/****** Object:  Default [DF__cop_app__deploym__0CDAE408]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT ((1)) FOR [deployment]
GO
/****** Object:  Default [DF__cop_app__path__0DCF0841]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT (NULL) FOR [path]
GO
/****** Object:  Default [DF__cop_app__authori__0EC32C7A]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT (NULL) FOR [authorizationcode]
GO
/****** Object:  Default [DF__cop_app__install__0FB750B3]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT (NULL) FOR [installuri]
GO
/****** Object:  Default [DF__cop_app__deletef__10AB74EC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT ((0)) FOR [deleteflag]
GO
/****** Object:  Default [DF__cop_app__version__119F9925]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_app] ADD  DEFAULT (NULL) FOR [version]
GO
/****** Object:  Default [DF__cop_ask__title__16644E42]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_ask] ADD  DEFAULT (NULL) FOR [title]
GO
/****** Object:  Default [DF__cop_ask__datelin__1758727B]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_ask] ADD  DEFAULT (NULL) FOR [dateline]
GO
/****** Object:  Default [DF__cop_ask__isreply__184C96B4]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_ask] ADD  DEFAULT (NULL) FOR [isreply]
GO
/****** Object:  Default [DF__cop_ask__userid__1940BAED]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_ask] ADD  DEFAULT (NULL) FOR [userid]
GO
/****** Object:  Default [DF__cop_ask__siteid__1A34DF26]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_ask] ADD  DEFAULT (NULL) FOR [siteid]
GO
/****** Object:  Default [DF__cop_ask__domain__1B29035F]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_ask] ADD  DEFAULT (NULL) FOR [domain]
GO
/****** Object:  Default [DF__cop_ask__sitenam__1C1D2798]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_ask] ADD  DEFAULT (NULL) FOR [sitename]
GO
/****** Object:  Default [DF__cop_ask__usernam__1D114BD1]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_ask] ADD  DEFAULT (NULL) FOR [username]
GO
/****** Object:  Default [DF__cop_data_lo__url__21D600EE]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_data_log] ADD  DEFAULT (NULL) FOR [url]
GO
/****** Object:  Default [DF__cop_data___siten__22CA2527]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_data_log] ADD  DEFAULT (NULL) FOR [sitename]
GO
/****** Object:  Default [DF__cop_data___domai__23BE4960]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_data_log] ADD  DEFAULT (NULL) FOR [domain]
GO
/****** Object:  Default [DF__cop_data___logty__24B26D99]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_data_log] ADD  DEFAULT (NULL) FOR [logtype]
GO
/****** Object:  Default [DF__cop_data___optyp__25A691D2]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_data_log] ADD  DEFAULT (NULL) FOR [optype]
GO
/****** Object:  Default [DF__cop_data___datel__269AB60B]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_data_log] ADD  DEFAULT (NULL) FOR [dateline]
GO
/****** Object:  Default [DF__cop_data___useri__278EDA44]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_data_log] ADD  DEFAULT (NULL) FOR [userid]
GO
/****** Object:  Default [DF__cop_data___sitei__2882FE7D]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_data_log] ADD  DEFAULT (NULL) FOR [siteid]
GO
/****** Object:  Default [DF__cop_reply__askid__2D47B39A]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_reply] ADD  DEFAULT (NULL) FOR [askid]
GO
/****** Object:  Default [DF__cop_reply__usern__2E3BD7D3]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_reply] ADD  DEFAULT (NULL) FOR [username]
GO
/****** Object:  Default [DF__cop_reply__datel__2F2FFC0C]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_reply] ADD  DEFAULT (NULL) FOR [dateline]
GO
/****** Object:  Default [DF__cop_site__userid__33F4B129]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [userid]
GO
/****** Object:  Default [DF__cop_site__sitena__34E8D562]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [sitename]
GO
/****** Object:  Default [DF__cop_site__produc__35DCF99B]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [productid]
GO
/****** Object:  Default [DF__cop_site__icofil__36D11DD4]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [icofile]
GO
/****** Object:  Default [DF__cop_site__logofi__37C5420D]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [logofile]
GO
/****** Object:  Default [DF__cop_site__delete__38B96646]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [deleteflag]
GO
/****** Object:  Default [DF__cop_site__keywor__39AD8A7F]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [keywords]
GO
/****** Object:  Default [DF__cop_site__themep__3AA1AEB8]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [themepath]
GO
/****** Object:  Default [DF__cop_site__admint__3B95D2F1]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [adminthemeid]
GO
/****** Object:  Default [DF__cop_site__themei__3C89F72A]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [themeid]
GO
/****** Object:  Default [DF__cop_site__point__3D7E1B63]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [point]
GO
/****** Object:  Default [DF__cop_site__create__3E723F9C]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [createtime]
GO
/****** Object:  Default [DF__cop_site__lastlo__3F6663D5]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [lastlogin]
GO
/****** Object:  Default [DF__cop_site__lastge__405A880E]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [lastgetpoint]
GO
/****** Object:  Default [DF__cop_site__loginc__414EAC47]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [logincount]
GO
/****** Object:  Default [DF__cop_site__bklogi__4242D080]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [bkloginpicfile]
GO
/****** Object:  Default [DF__cop_site__bklogo__4336F4B9]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [bklogofile]
GO
/****** Object:  Default [DF__cop_site__sumpoi__442B18F2]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [sumpoint]
GO
/****** Object:  Default [DF__cop_site__sumacc__451F3D2B]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [sumaccess]
GO
/****** Object:  Default [DF__cop_site__title__46136164]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [title]
GO
/****** Object:  Default [DF__cop_site__userna__4707859D]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [username]
GO
/****** Object:  Default [DF__cop_site__userse__47FBA9D6]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [usersex]
GO
/****** Object:  Default [DF__cop_site__userte__48EFCE0F]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [usertel]
GO
/****** Object:  Default [DF__cop_site__usermo__49E3F248]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [usermobile]
GO
/****** Object:  Default [DF__cop_site__userte__4AD81681]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [usertel1]
GO
/****** Object:  Default [DF__cop_site__userem__4BCC3ABA]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [useremail]
GO
/****** Object:  Default [DF__cop_site__state__4CC05EF3]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((1)) FOR [state]
GO
/****** Object:  Default [DF__cop_site__qqlist__4DB4832C]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('25106942:客户服务!comma;52560956:技术支持') FOR [qqlist]
GO
/****** Object:  Default [DF__cop_site__msnlis__4EA8A765]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [msnlist]
GO
/****** Object:  Default [DF__cop_site__wwlist__4F9CCB9E]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [wwlist]
GO
/****** Object:  Default [DF__cop_site__tellis__5090EFD7]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [tellist]
GO
/****** Object:  Default [DF__cop_site__workti__51851410]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('9:00到18:00') FOR [worktime]
GO
/****** Object:  Default [DF__cop_site__siteon__52793849]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('0') FOR [siteon]
GO
/****** Object:  Default [DF__cop_site__closer__536D5C82]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [closereson]
GO
/****** Object:  Default [DF__cop_site__copyri__546180BB]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('Copyright &copy; 2010-2012 本公司版权所有') FOR [copyright]
GO
/****** Object:  Default [DF__cop_site__icp__5555A4F4]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('京ICP备05037293号') FOR [icp]
GO
/****** Object:  Default [DF__cop_site__addres__5649C92D]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('北京市某区某街某号') FOR [address]
GO
/****** Object:  Default [DF__cop_site__zipcod__573DED66]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('000000') FOR [zipcode]
GO
/****** Object:  Default [DF__cop_site__qq__5832119F]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((1)) FOR [qq]
GO
/****** Object:  Default [DF__cop_site__msn__592635D8]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [msn]
GO
/****** Object:  Default [DF__cop_site__ww__5A1A5A11]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [ww]
GO
/****** Object:  Default [DF__cop_site__tel__5B0E7E4A]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [tel]
GO
/****** Object:  Default [DF__cop_site__wt__5C02A283]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((1)) FOR [wt]
GO
/****** Object:  Default [DF__cop_site__linkma__5CF6C6BC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('刘先生') FOR [linkman]
GO
/****** Object:  Default [DF__cop_site__linkte__5DEAEAF5]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('010-61750491') FOR [linktel]
GO
/****** Object:  Default [DF__cop_site__email__5EDF0F2E]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ('enation@126.com') FOR [email]
GO
/****** Object:  Default [DF__cop_site__multi___5FD33367]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [multi_site]
GO
/****** Object:  Default [DF__cop_site__relid__60C757A0]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT (NULL) FOR [relid]
GO
/****** Object:  Default [DF__cop_site__sitest__61BB7BD9]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [sitestate]
GO
/****** Object:  Default [DF__cop_site__isimpo__62AFA012]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [isimported]
GO
/****** Object:  Default [DF__cop_site__imptyp__63A3C44B]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_site] ADD  DEFAULT ((0)) FOR [imptype]
GO
/****** Object:  Default [DF__cop_sited__domai__68687968]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_sitedomain] ADD  DEFAULT (NULL) FOR [domain]
GO
/****** Object:  Default [DF__cop_sited__domai__695C9DA1]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_sitedomain] ADD  DEFAULT ((0)) FOR [domaintype]
GO
/****** Object:  Default [DF__cop_sited__sitei__6A50C1DA]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_sitedomain] ADD  DEFAULT (NULL) FOR [siteid]
GO
/****** Object:  Default [DF__cop_sited__useri__6B44E613]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_sitedomain] ADD  DEFAULT (NULL) FOR [userid]
GO
/****** Object:  Default [DF__cop_sited__statu__6C390A4C]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_sitedomain] ADD  DEFAULT ((0)) FOR [status]
GO
/****** Object:  Default [DF__cop_user__userna__70FDBF69]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [username]
GO
/****** Object:  Default [DF__cop_user__compan__71F1E3A2]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [companyname]
GO
/****** Object:  Default [DF__cop_user__passwo__72E607DB]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [password]
GO
/****** Object:  Default [DF__cop_user__addres__73DA2C14]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [address]
GO
/****** Object:  Default [DF__cop_user__legalr__74CE504D]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [legalrepresentative]
GO
/****** Object:  Default [DF__cop_user__linkma__75C27486]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [linkman]
GO
/****** Object:  Default [DF__cop_user__tel__76B698BF]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [tel]
GO
/****** Object:  Default [DF__cop_user__mobile__77AABCF8]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [mobile]
GO
/****** Object:  Default [DF__cop_user__email__789EE131]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [email]
GO
/****** Object:  Default [DF__cop_user__logofi__7993056A]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [logofile]
GO
/****** Object:  Default [DF__cop_user__licens__7A8729A3]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [licensefile]
GO
/****** Object:  Default [DF__cop_user__defaul__7B7B4DDC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [defaultsiteid]
GO
/****** Object:  Default [DF__cop_user__delete__7C6F7215]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT ((0)) FOR [deleteflag]
GO
/****** Object:  Default [DF__cop_user__userty__7D63964E]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [usertype]
GO
/****** Object:  Default [DF__cop_user__create__7E57BA87]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_user] ADD  DEFAULT (NULL) FOR [createtime]
GO
/****** Object:  Default [DF__cop_userd__useri__031C6FA4]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_userdetail] ADD  DEFAULT (NULL) FOR [userid]
GO
/****** Object:  Default [DF__cop_userd__regad__041093DD]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_userdetail] ADD  DEFAULT (NULL) FOR [regaddress]
GO
/****** Object:  Default [DF__cop_userd__regda__0504B816]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_userdetail] ADD  DEFAULT (NULL) FOR [regdate]
GO
/****** Object:  Default [DF__cop_userd__corps__05F8DC4F]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[cop_userdetail] ADD  DEFAULT ((0)) FOR [corpscope]
GO
/****** Object:  Default [DF__DT_DataCa__tosit__3631FF56]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_DataCat] ADD  DEFAULT ((0)) FOR [tositemap]
GO
/****** Object:  Default [DF__DT_Smtp__send_co__0EE3280B]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_Smtp] ADD  DEFAULT ((0)) FOR [send_count]
GO
/****** Object:  Default [DF__es_access__ip__0ABD916C]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_access] ADD  DEFAULT (NULL) FOR [ip]
GO
/****** Object:  Default [DF__es_access__url__0BB1B5A5]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_access] ADD  DEFAULT (NULL) FOR [url]
GO
/****** Object:  Default [DF__es_access__page__0CA5D9DE]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_access] ADD  DEFAULT (NULL) FOR [page]
GO
/****** Object:  Default [DF__es_access__area__0D99FE17]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_access] ADD  DEFAULT (NULL) FOR [area]
GO
/****** Object:  Default [DF__es_access__acces__0E8E2250]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_access] ADD  DEFAULT (NULL) FOR [access_time]
GO
/****** Object:  Default [DF__es_access__stay___0F824689]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_access] ADD  DEFAULT (NULL) FOR [stay_time]
GO
/****** Object:  Default [DF__es_access__point__10766AC2]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_access] ADD  DEFAULT ((0)) FOR [point]
GO
/****** Object:  Default [DF__es_access__membe__116A8EFB]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_access] ADD  DEFAULT ('0') FOR [membername]
GO
/****** Object:  Default [DF__es_adcolu__cname__162F4418]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adcolumn] ADD  DEFAULT (NULL) FOR [cname]
GO
/****** Object:  Default [DF__es_adcolu__width__17236851]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adcolumn] ADD  DEFAULT (NULL) FOR [width]
GO
/****** Object:  Default [DF__es_adcolu__heigh__18178C8A]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adcolumn] ADD  DEFAULT (NULL) FOR [height]
GO
/****** Object:  Default [DF__es_adcolu__descr__190BB0C3]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adcolumn] ADD  DEFAULT (NULL) FOR [description]
GO
/****** Object:  Default [DF__es_adcolu__anumb__19FFD4FC]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adcolumn] ADD  DEFAULT (NULL) FOR [anumber]
GO
/****** Object:  Default [DF__es_adcolu__atype__1AF3F935]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adcolumn] ADD  DEFAULT (NULL) FOR [atype]
GO
/****** Object:  Default [DF__es_adcolu__arule__1BE81D6E]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adcolumn] ADD  DEFAULT (NULL) FOR [arule]
GO
/****** Object:  Default [DF__es_adcolu__disab__1CDC41A7]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adcolumn] ADD  DEFAULT ('false') FOR [disabled]
GO
/****** Object:  Default [DF__es_admint__appid__21A0F6C4]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT (NULL) FOR [appid]
GO
/****** Object:  Default [DF__es_admint__sitei__22951AFD]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT (NULL) FOR [siteid]
GO
/****** Object:  Default [DF__es_admint__theme__23893F36]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT (NULL) FOR [themename]
GO
/****** Object:  Default [DF__es_adminth__path__247D636F]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT (NULL) FOR [path]
GO
/****** Object:  Default [DF__es_admint__useri__257187A8]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT (NULL) FOR [userid]
GO
/****** Object:  Default [DF__es_admint__autho__2665ABE1]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT (NULL) FOR [author]
GO
/****** Object:  Default [DF__es_admint__versi__2759D01A]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT (NULL) FOR [version]
GO
/****** Object:  Default [DF__es_admint__frame__284DF453]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT ((0)) FOR [framemode]
GO
/****** Object:  Default [DF__es_admint__delet__2942188C]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT ((0)) FOR [deleteflag]
GO
/****** Object:  Default [DF__es_admint__thumb__2A363CC5]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_admintheme] ADD  DEFAULT (NULL) FOR [thumb]
GO
/****** Object:  Default [DF__es_adminu__usern__2EFAF1E2]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [username]
GO
/****** Object:  Default [DF__es_adminu__passw__2FEF161B]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [password]
GO
/****** Object:  Default [DF__es_adminu__state__30E33A54]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [state]
GO
/****** Object:  Default [DF__es_adminu__realn__31D75E8D]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [realname]
GO
/****** Object:  Default [DF__es_adminu__usern__32CB82C6]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [userno]
GO
/****** Object:  Default [DF__es_adminu__userd__33BFA6FF]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [userdept]
GO
/****** Object:  Default [DF__es_adminu__remar__34B3CB38]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [remark]
GO
/****** Object:  Default [DF__es_adminu__datel__35A7EF71]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [dateline]
GO
/****** Object:  Default [DF__es_adminu__found__369C13AA]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [founder]
GO
/****** Object:  Default [DF__es_adminu__sitei__379037E3]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adminuser] ADD  DEFAULT (NULL) FOR [siteid]
GO
/****** Object:  Default [DF__es_adv__acid__3C54ED00]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [acid]
GO
/****** Object:  Default [DF__es_adv__atype__3D491139]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [atype]
GO
/****** Object:  Default [DF__es_adv__begintim__3E3D3572]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [begintime]
GO
/****** Object:  Default [DF__es_adv__endtime__3F3159AB]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [endtime]
GO
/****** Object:  Default [DF__es_adv__isclose__40257DE4]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [isclose]
GO
/****** Object:  Default [DF__es_adv__attachme__4119A21D]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [attachment]
GO
/****** Object:  Default [DF__es_adv__atturl__420DC656]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [atturl]
GO
/****** Object:  Default [DF__es_adv__url__4301EA8F]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [url]
GO
/****** Object:  Default [DF__es_adv__aname__43F60EC8]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [aname]
GO
/****** Object:  Default [DF__es_adv__clickcou__44EA3301]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT ((0)) FOR [clickcount]
GO
/****** Object:  Default [DF__es_adv__linkman__45DE573A]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [linkman]
GO
/****** Object:  Default [DF__es_adv__company__46D27B73]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [company]
GO
/****** Object:  Default [DF__es_adv__contact__47C69FAC]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT (NULL) FOR [contact]
GO
/****** Object:  Default [DF__es_adv__disabled__48BAC3E5]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_adv] ADD  DEFAULT ('false') FOR [disabled]
GO
/****** Object:  Default [DF__es_auth_ac__name__4D7F7902]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_auth_action] ADD  DEFAULT (NULL) FOR [name]
GO
/****** Object:  Default [DF__es_auth_ac__type__4E739D3B]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_auth_action] ADD  DEFAULT (NULL) FOR [type]
GO
/****** Object:  Default [DF__es_car_art__sort__53385258]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT ((0)) FOR [sort]
GO
/****** Object:  Default [DF__es_car_ar__add_t__542C7691]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [add_time]
GO
/****** Object:  Default [DF__es_car_arti__hit__55209ACA]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [hit]
GO
/****** Object:  Default [DF__es_car_ar__able___5614BF03]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [able_time]
GO
/****** Object:  Default [DF__es_car_ar__state__5708E33C]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [state]
GO
/****** Object:  Default [DF__es_car_ar__user___57FD0775]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [user_id]
GO
/****** Object:  Default [DF__es_car_ar__cat_i__58F12BAE]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [cat_id]
GO
/****** Object:  Default [DF__es_car_ar__is_co__59E54FE7]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [is_commend]
GO
/****** Object:  Default [DF__es_car_ar__title__5AD97420]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [title]
GO
/****** Object:  Default [DF__es_car_arti__img__5BCD9859]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [img]
GO
/****** Object:  Default [DF__es_car_ar__sys_l__5CC1BC92]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT ((0)) FOR [sys_lock]
GO
/****** Object:  Default [DF__es_car_ar__lastm__5DB5E0CB]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [lastmodified]
GO
/****** Object:  Default [DF__es_car_ar__page___5EAA0504]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [page_title]
GO
/****** Object:  Default [DF__es_car_ar__page___5F9E293D]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [page_keywords]
GO
/****** Object:  Default [DF__es_car_ar__site___60924D76]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT ((100000)) FOR [site_code]
GO
/****** Object:  Default [DF__es_car_ar__sitei__618671AF]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_car_article] ADD  DEFAULT (NULL) FOR [siteidlist]
GO
/****** Object:  Default [DF__es_compon__compo__664B26CC]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_component] ADD  DEFAULT (NULL) FOR [componentid]
GO
/****** Object:  Default [DF__es_compone__name__673F4B05]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_component] ADD  DEFAULT (NULL) FOR [name]
GO
/****** Object:  Default [DF__es_compon__insta__68336F3E]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_component] ADD  DEFAULT ((0)) FOR [install_state]
GO
/****** Object:  Default [DF__es_compon__enabl__69279377]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_component] ADD  DEFAULT ((0)) FOR [enable_state]
GO
/****** Object:  Default [DF__es_compon__versi__6A1BB7B0]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_component] ADD  DEFAULT (NULL) FOR [version]
GO
/****** Object:  Default [DF__es_compon__autho__6B0FDBE9]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_component] ADD  DEFAULT (NULL) FOR [author]
GO
/****** Object:  Default [DF__es_compon__javas__6C040022]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_component] ADD  DEFAULT (NULL) FOR [javashop_version]
GO
/****** Object:  Default [DF__es_data_ca__name__70C8B53F]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT (NULL) FOR [name]
GO
/****** Object:  Default [DF__es_data_c__paren__71BCD978]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT (NULL) FOR [parent_id]
GO
/****** Object:  Default [DF__es_data_c__cat_p__72B0FDB1]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT (NULL) FOR [cat_path]
GO
/****** Object:  Default [DF__es_data_c__cat_o__73A521EA]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT (NULL) FOR [cat_order]
GO
/****** Object:  Default [DF__es_data_c__model__74994623]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT (NULL) FOR [model_id]
GO
/****** Object:  Default [DF__es_data_c__if_au__758D6A5C]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT (NULL) FOR [if_audit]
GO
/****** Object:  Default [DF__es_data_cat__url__76818E95]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT (NULL) FOR [url]
GO
/****** Object:  Default [DF__es_data_c__detai__7775B2CE]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT (NULL) FOR [detail_url]
GO
/****** Object:  Default [DF__es_data_c__tosit__7869D707]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_cat] ADD  DEFAULT ((0)) FOR [tositemap]
GO
/****** Object:  Default [DF__es_data_f__china__7D2E8C24]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [china_name]
GO
/****** Object:  Default [DF__es_data_f__engli__7E22B05D]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [english_name]
GO
/****** Object:  Default [DF__es_data_f__data___7F16D496]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [data_type]
GO
/****** Object:  Default [DF__es_data_f__data___000AF8CF]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [data_size]
GO
/****** Object:  Default [DF__es_data_f__show___00FF1D08]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [show_form]
GO
/****** Object:  Default [DF__es_data_f__show___01F34141]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [show_value]
GO
/****** Object:  Default [DF__es_data_f__add_t__02E7657A]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [add_time]
GO
/****** Object:  Default [DF__es_data_f__model__03DB89B3]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [model_id]
GO
/****** Object:  Default [DF__es_data_f__is_va__04CFADEC]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [is_validate]
GO
/****** Object:  Default [DF__es_data_f__taxis__05C3D225]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [taxis]
GO
/****** Object:  Default [DF__es_data_f__dict___06B7F65E]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [dict_id]
GO
/****** Object:  Default [DF__es_data_f__is_sh__07AC1A97]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_field] ADD  DEFAULT (NULL) FOR [is_show]
GO
/****** Object:  Default [DF__es_data_m__china__15FA39EE]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_model] ADD  DEFAULT (NULL) FOR [china_name]
GO
/****** Object:  Default [DF__es_data_m__engli__16EE5E27]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_model] ADD  DEFAULT (NULL) FOR [english_name]
GO
/****** Object:  Default [DF__es_data_m__add_t__17E28260]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_model] ADD  DEFAULT (NULL) FOR [add_time]
GO
/****** Object:  Default [DF__es_data_m__proje__18D6A699]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_model] ADD  DEFAULT (NULL) FOR [project_name]
GO
/****** Object:  Default [DF__es_data_m__brief__19CACAD2]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_model] ADD  DEFAULT (NULL) FOR [brief]
GO
/****** Object:  Default [DF__es_data_m__if_au__1ABEEF0B]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_data_model] ADD  DEFAULT (NULL) FOR [if_audit]
GO
/****** Object:  Default [DF__es_friends__name__1F83A428]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_friends_link] ADD  DEFAULT (NULL) FOR [name]
GO
/****** Object:  Default [DF__es_friends___url__2077C861]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_friends_link] ADD  DEFAULT (NULL) FOR [url]
GO
/****** Object:  Default [DF__es_friends__logo__216BEC9A]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_friends_link] ADD  DEFAULT (NULL) FOR [logo]
GO
/****** Object:  Default [DF__es_friends__sort__226010D3]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_friends_link] ADD  DEFAULT (NULL) FOR [sort]
GO
/****** Object:  Default [DF__es_guestb__title__2724C5F0]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [title]
GO
/****** Object:  Default [DF__es_guestb__paren__2818EA29]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [parentid]
GO
/****** Object:  Default [DF__es_guestb__datel__290D0E62]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [dateline]
GO
/****** Object:  Default [DF__es_guestb__issub__2A01329B]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [issubject]
GO
/****** Object:  Default [DF__es_guestb__usern__2AF556D4]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [username]
GO
/****** Object:  Default [DF__es_guestb__email__2BE97B0D]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [email]
GO
/****** Object:  Default [DF__es_guestbook__qq__2CDD9F46]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [qq]
GO
/****** Object:  Default [DF__es_guestboo__tel__2DD1C37F]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [tel]
GO
/****** Object:  Default [DF__es_guestboo__sex__2EC5E7B8]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [sex]
GO
/****** Object:  Default [DF__es_guestbook__ip__2FBA0BF1]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [ip]
GO
/****** Object:  Default [DF__es_guestbo__area__30AE302A]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_guestbook] ADD  DEFAULT (NULL) FOR [area]
GO
/****** Object:  Default [DF__es_index___title__3572E547]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_index_item] ADD  DEFAULT (NULL) FOR [title]
GO
/****** Object:  Default [DF__es_index_it__url__36670980]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_index_item] ADD  DEFAULT (NULL) FOR [url]
GO
/****** Object:  Default [DF__es_index_i__sort__375B2DB9]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_index_item] ADD  DEFAULT (NULL) FOR [sort]
GO
/****** Object:  Default [DF__es_menu__appid__3C1FE2D6]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT (NULL) FOR [appid]
GO
/****** Object:  Default [DF__es_menu__pid__3D14070F]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT (NULL) FOR [pid]
GO
/****** Object:  Default [DF__es_menu__title__3E082B48]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT (NULL) FOR [title]
GO
/****** Object:  Default [DF__es_menu__url__3EFC4F81]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT (NULL) FOR [url]
GO
/****** Object:  Default [DF__es_menu__target__3FF073BA]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT (NULL) FOR [target]
GO
/****** Object:  Default [DF__es_menu__sorder__40E497F3]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT ((50)) FOR [sorder]
GO
/****** Object:  Default [DF__es_menu__menutyp__41D8BC2C]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT (NULL) FOR [menutype]
GO
/****** Object:  Default [DF__es_menu__datatyp__42CCE065]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT (NULL) FOR [datatype]
GO
/****** Object:  Default [DF__es_menu__selecte__43C1049E]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT ((0)) FOR [selected]
GO
/****** Object:  Default [DF__es_menu__deletef__44B528D7]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_menu] ADD  DEFAULT ((0)) FOR [deleteflag]
GO
/****** Object:  Default [DF__es_region__p_reg__4979DDF4]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_regions] ADD  DEFAULT (NULL) FOR [p_region_id]
GO
/****** Object:  Default [DF__es_region__regio__4A6E022D]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_regions] ADD  DEFAULT (NULL) FOR [region_path]
GO
/****** Object:  Default [DF__es_region__regio__4B622666]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_regions] ADD  DEFAULT (NULL) FOR [region_grade]
GO
/****** Object:  Default [DF__es_region__zipco__4C564A9F]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_regions] ADD  DEFAULT (NULL) FOR [zipcode]
GO
/****** Object:  Default [DF__es_regions__cod__4D4A6ED8]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_regions] ADD  DEFAULT (NULL) FOR [cod]
GO
/****** Object:  Default [DF__es_role__rolenam__520F23F5]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_role] ADD  DEFAULT (NULL) FOR [rolename]
GO
/****** Object:  Default [DF__es_role__rolemem__5303482E]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_role] ADD  DEFAULT (NULL) FOR [rolememo]
GO
/****** Object:  Default [DF__es_role_a__rolei__57C7FD4B]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_role_auth] ADD  DEFAULT (NULL) FOR [roleid]
GO
/****** Object:  Default [DF__es_role_a__authi__58BC2184]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_role_auth] ADD  DEFAULT (NULL) FOR [authid]
GO
/****** Object:  Default [DF__es_setting__code__5D80D6A1]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_settings] ADD  DEFAULT (NULL) FOR [code]
GO
/****** Object:  Default [DF__es_settin__cfg_v__5E74FADA]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_settings] ADD  DEFAULT (NULL) FOR [cfg_value]
GO
/****** Object:  Default [DF__es_settin__cfg_g__5F691F13]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_settings] ADD  DEFAULT (NULL) FOR [cfg_group]
GO
/****** Object:  Default [DF__es_site__parenti__642DD430]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site] ADD  DEFAULT (NULL) FOR [parentid]
GO
/****** Object:  Default [DF__es_site__code__6521F869]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site] ADD  DEFAULT (NULL) FOR [code]
GO
/****** Object:  Default [DF__es_site__name__66161CA2]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site] ADD  DEFAULT (NULL) FOR [name]
GO
/****** Object:  Default [DF__es_site__domain__670A40DB]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site] ADD  DEFAULT (NULL) FOR [domain]
GO
/****** Object:  Default [DF__es_site__themeid__67FE6514]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site] ADD  DEFAULT (NULL) FOR [themeid]
GO
/****** Object:  Default [DF__es_site__sitelev__68F2894D]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site] ADD  DEFAULT (NULL) FOR [sitelevel]
GO
/****** Object:  Default [DF__es_site_m__paren__6DB73E6A]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site_menu] ADD  DEFAULT (NULL) FOR [parentid]
GO
/****** Object:  Default [DF__es_site_me__name__6EAB62A3]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site_menu] ADD  DEFAULT (NULL) FOR [name]
GO
/****** Object:  Default [DF__es_site_men__url__6F9F86DC]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site_menu] ADD  DEFAULT (NULL) FOR [url]
GO
/****** Object:  Default [DF__es_site_m__targe__7093AB15]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site_menu] ADD  DEFAULT (NULL) FOR [target]
GO
/****** Object:  Default [DF__es_site_me__sort__7187CF4E]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_site_menu] ADD  DEFAULT (NULL) FOR [sort]
GO
/****** Object:  Default [DF__es_smtp__host__764C846B]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_smtp] ADD  DEFAULT (NULL) FOR [host]
GO
/****** Object:  Default [DF__es_smtp__usernam__7740A8A4]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_smtp] ADD  DEFAULT (NULL) FOR [username]
GO
/****** Object:  Default [DF__es_smtp__passwor__7834CCDD]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_smtp] ADD  DEFAULT (NULL) FOR [password]
GO
/****** Object:  Default [DF__es_smtp__last_se__7928F116]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_smtp] ADD  DEFAULT (NULL) FOR [last_send_time]
GO
/****** Object:  Default [DF__es_smtp__send_co__7A1D154F]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_smtp] ADD  DEFAULT ((0)) FOR [send_count]
GO
/****** Object:  Default [DF__es_smtp__max_cou__7B113988]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_smtp] ADD  DEFAULT (NULL) FOR [max_count]
GO
/****** Object:  Default [DF__es_smtp__mail_fr__7C055DC1]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_smtp] ADD  DEFAULT (NULL) FOR [mail_from]
GO
/****** Object:  Default [DF__es_theme__appid__00CA12DE]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_theme] ADD  DEFAULT (NULL) FOR [appid]
GO
/****** Object:  Default [DF__es_theme__themen__01BE3717]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_theme] ADD  DEFAULT (NULL) FOR [themename]
GO
/****** Object:  Default [DF__es_theme__path__02B25B50]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_theme] ADD  DEFAULT (NULL) FOR [path]
GO
/****** Object:  Default [DF__es_theme__author__03A67F89]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_theme] ADD  DEFAULT (NULL) FOR [author]
GO
/****** Object:  Default [DF__es_theme__versio__049AA3C2]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_theme] ADD  DEFAULT (NULL) FOR [version]
GO
/****** Object:  Default [DF__es_theme__delete__058EC7FB]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_theme] ADD  DEFAULT ((0)) FOR [deleteflag]
GO
/****** Object:  Default [DF__es_theme__thumb__0682EC34]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_theme] ADD  DEFAULT (NULL) FOR [thumb]
GO
/****** Object:  Default [DF__es_theme__siteid__0777106D]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_theme] ADD  DEFAULT (NULL) FOR [siteid]
GO
/****** Object:  Default [DF__es_themeu__theme__0C3BC58A]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT (NULL) FOR [themeid]
GO
/****** Object:  Default [DF__es_themeuri__uri__0D2FE9C3]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT (NULL) FOR [uri]
GO
/****** Object:  Default [DF__es_themeur__path__0E240DFC]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT (NULL) FOR [path]
GO
/****** Object:  Default [DF__es_themeu__delet__0F183235]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT ((0)) FOR [deleteflag]
GO
/****** Object:  Default [DF__es_themeu__pagen__100C566E]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT (NULL) FOR [pagename]
GO
/****** Object:  Default [DF__es_themeu__point__11007AA7]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT (NULL) FOR [point]
GO
/****** Object:  Default [DF__es_themeu__sitem__11F49EE0]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT ((0)) FOR [sitemaptype]
GO
/****** Object:  Default [DF__es_themeu__keywo__12E8C319]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT (NULL) FOR [keywords]
GO
/****** Object:  Default [DF__es_themeu__httpc__13DCE752]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_themeuri] ADD  DEFAULT ((0)) FOR [httpcache]
GO
/****** Object:  Default [DF__es_user_r__useri__18A19C6F]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_user_role] ADD  DEFAULT (NULL) FOR [userid]
GO
/****** Object:  Default [DF__es_user_r__rolei__1995C0A8]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[es_user_role] ADD  DEFAULT (NULL) FOR [roleid]
GO
/****** Object:  Check [CK_DT_SellerEmail]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SellerEmail]  WITH CHECK ADD  CONSTRAINT [CK_DT_SellerEmail] CHECK  (([partner] like '2088[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[DT_SellerEmail] CHECK CONSTRAINT [CK_DT_SellerEmail]
GO
/****** Object:  ForeignKey [FK_DT_AREAC_REFERENCE_DT_AREAC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_AreaConfigDetail]  WITH CHECK ADD  CONSTRAINT [FK_DT_AREAC_REFERENCE_DT_AREAC] FOREIGN KEY([areaCfgCode])
REFERENCES [dbo].[DT_AreaConfig] ([code])
GO
ALTER TABLE [dbo].[DT_AreaConfigDetail] CHECK CONSTRAINT [FK_DT_AREAC_REFERENCE_DT_AREAC]
GO
/****** Object:  ForeignKey [FK_DT_AREAC_REFERENCE_DT_AREAI]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_AreaConfigDetail]  WITH CHECK ADD  CONSTRAINT [FK_DT_AREAC_REFERENCE_DT_AREAI] FOREIGN KEY([areaCode])
REFERENCES [dbo].[DT_AreaInfo] ([code])
GO
ALTER TABLE [dbo].[DT_AreaConfigDetail] CHECK CONSTRAINT [FK_DT_AREAC_REFERENCE_DT_AREAI]
GO
/****** Object:  ForeignKey [FK_ComplaintAdminuser_adminuser]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ComplaintAdminuserInfo]  WITH CHECK ADD  CONSTRAINT [FK_ComplaintAdminuser_adminuser] FOREIGN KEY([adminuserUserid])
REFERENCES [dbo].[es_adminuser] ([userid])
GO
ALTER TABLE [dbo].[DT_ComplaintAdminuserInfo] CHECK CONSTRAINT [FK_ComplaintAdminuser_adminuser]
GO
/****** Object:  ForeignKey [FK_CustomerComplaintTypeName_ComplaintType]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_CustomerComplaintTypeName]  WITH CHECK ADD  CONSTRAINT [FK_CustomerComplaintTypeName_ComplaintType] FOREIGN KEY([complaintTypeCode])
REFERENCES [dbo].[DT_CustomerComplaintType] ([code])
GO
ALTER TABLE [dbo].[DT_CustomerComplaintTypeName] CHECK CONSTRAINT [FK_CustomerComplaintTypeName_ComplaintType]
GO
/****** Object:  ForeignKey [FK_DT_CustomerComplaintTypeName_DT_Language]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_CustomerComplaintTypeName]  WITH CHECK ADD  CONSTRAINT [FK_DT_CustomerComplaintTypeName_DT_Language] FOREIGN KEY([languageCode])
REFERENCES [dbo].[DT_Language] ([code])
GO
ALTER TABLE [dbo].[DT_CustomerComplaintTypeName] CHECK CONSTRAINT [FK_DT_CustomerComplaintTypeName_DT_Language]
GO
/****** Object:  ForeignKey [FK_CUSTOMERINFO_LANGUAGE]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_CustomerInfo]  WITH CHECK ADD  CONSTRAINT [FK_CUSTOMERINFO_LANGUAGE] FOREIGN KEY([languageCode])
REFERENCES [dbo].[DT_Language] ([code])
GO
ALTER TABLE [dbo].[DT_CustomerInfo] CHECK CONSTRAINT [FK_CUSTOMERINFO_LANGUAGE]
GO
/****** Object:  ForeignKey [FK_DT_CustomerInfo_DT_QuestionInfo]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_CustomerInfo]  WITH CHECK ADD  CONSTRAINT [FK_DT_CustomerInfo_DT_QuestionInfo] FOREIGN KEY([questionCode])
REFERENCES [dbo].[DT_QuestionInfo] ([code])
GO
ALTER TABLE [dbo].[DT_CustomerInfo] CHECK CONSTRAINT [FK_DT_CustomerInfo_DT_QuestionInfo]
GO
/****** Object:  ForeignKey [DT_UserProInfo_proserial]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_CustomerProInfo]  WITH CHECK ADD  CONSTRAINT [DT_UserProInfo_proserial] FOREIGN KEY([proCode])
REFERENCES [dbo].[DT_ProductInfo] ([code])
GO
ALTER TABLE [dbo].[DT_CustomerProInfo] CHECK CONSTRAINT [DT_UserProInfo_proserial]
GO
/****** Object:  ForeignKey [DT_UserProInfo_userID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_CustomerProInfo]  WITH CHECK ADD  CONSTRAINT [DT_UserProInfo_userID] FOREIGN KEY([customerCode])
REFERENCES [dbo].[DT_CustomerInfo] ([code])
GO
ALTER TABLE [dbo].[DT_CustomerProInfo] CHECK CONSTRAINT [DT_UserProInfo_userID]
GO
/****** Object:  ForeignKey [DT_LanguageConfigDetail_LanguageConfigID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_LanguageConfigDetail]  WITH CHECK ADD  CONSTRAINT [DT_LanguageConfigDetail_LanguageConfigID] FOREIGN KEY([languageCfgCode])
REFERENCES [dbo].[DT_LanguageConfig] ([code])
GO
ALTER TABLE [dbo].[DT_LanguageConfigDetail] CHECK CONSTRAINT [DT_LanguageConfigDetail_LanguageConfigID]
GO
/****** Object:  ForeignKey [DT_LanguageConfigDetail_LanguageID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_LanguageConfigDetail]  WITH CHECK ADD  CONSTRAINT [DT_LanguageConfigDetail_LanguageID] FOREIGN KEY([languageCode])
REFERENCES [dbo].[DT_Language] ([code])
GO
ALTER TABLE [dbo].[DT_LanguageConfigDetail] CHECK CONSTRAINT [DT_LanguageConfigDetail_LanguageID]
GO
/****** Object:  ForeignKey [DT_LanguagePack_languageTypeID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_LanguagePack]  WITH CHECK ADD  CONSTRAINT [DT_LanguagePack_languageTypeID] FOREIGN KEY([languageTypeCode])
REFERENCES [dbo].[DT_Language] ([code])
GO
ALTER TABLE [dbo].[DT_LanguagePack] CHECK CONSTRAINT [DT_LanguagePack_languageTypeID]
GO
/****** Object:  ForeignKey [FK_DT_Software_ID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_LanguagePack]  WITH CHECK ADD  CONSTRAINT [FK_DT_Software_ID] FOREIGN KEY([softwareVersionCode])
REFERENCES [dbo].[DT_SoftwareVersion] ([code])
GO
ALTER TABLE [dbo].[DT_LanguagePack] CHECK CONSTRAINT [FK_DT_Software_ID]
GO
/****** Object:  ForeignKey [FK_DT_DT_ParticipateIn_Area_Rule_PromotionAtivityID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MarketPromotionAreaRule]  WITH CHECK ADD  CONSTRAINT [FK_DT_DT_ParticipateIn_Area_Rule_PromotionAtivityID] FOREIGN KEY([marketPromotionCode])
REFERENCES [dbo].[DT_MarketPromotionBaseInfo] ([code])
GO
ALTER TABLE [dbo].[DT_MarketPromotionAreaRule] CHECK CONSTRAINT [FK_DT_DT_ParticipateIn_Area_Rule_PromotionAtivityID]
GO
/****** Object:  ForeignKey [FK_DT_PARTI_REFERENCE_DT_PROMO1]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MarketPromotionDiscountRule]  WITH CHECK ADD  CONSTRAINT [FK_DT_PARTI_REFERENCE_DT_PROMO1] FOREIGN KEY([marketPromotionRuleCode])
REFERENCES [dbo].[DT_MarketPromotionRule] ([code])
GO
ALTER TABLE [dbo].[DT_MarketPromotionDiscountRule] CHECK CONSTRAINT [FK_DT_PARTI_REFERENCE_DT_PROMO1]
GO
/****** Object:  ForeignKey [DT_PromotionActivity_Rule_PromotionActivity_Basic_Info]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MarketPromotionRule]  WITH CHECK ADD  CONSTRAINT [DT_PromotionActivity_Rule_PromotionActivity_Basic_Info] FOREIGN KEY([marketPromotionCode])
REFERENCES [dbo].[DT_MarketPromotionBaseInfo] ([code])
GO
ALTER TABLE [dbo].[DT_MarketPromotionRule] CHECK CONSTRAINT [DT_PromotionActivity_Rule_PromotionActivity_Basic_Info]
GO
/****** Object:  ForeignKey [FK_DT_DT_ParticipateIn_SoftWare_Rule_PromotionAtivityID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MarketPromotionSoftwareRule]  WITH CHECK ADD  CONSTRAINT [FK_DT_DT_ParticipateIn_SoftWare_Rule_PromotionAtivityID] FOREIGN KEY([marketPromotionCode])
REFERENCES [dbo].[DT_MarketPromotionBaseInfo] ([code])
GO
ALTER TABLE [dbo].[DT_MarketPromotionSoftwareRule] CHECK CONSTRAINT [FK_DT_DT_ParticipateIn_SoftWare_Rule_PromotionAtivityID]
GO
/****** Object:  ForeignKey [DT_PromotionActivity_TimeSlot_Info_pomotionActivityID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MarketPromotionTimesInfo]  WITH CHECK ADD  CONSTRAINT [DT_PromotionActivity_TimeSlot_Info_pomotionActivityID] FOREIGN KEY([marketPromotionCode])
REFERENCES [dbo].[DT_MarketPromotionBaseInfo] ([code])
GO
ALTER TABLE [dbo].[DT_MarketPromotionTimesInfo] CHECK CONSTRAINT [DT_PromotionActivity_TimeSlot_Info_pomotionActivityID]
GO
/****** Object:  ForeignKey [FK_DT_MESSA_REFERENCE_DT_LANGU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_Message]  WITH CHECK ADD  CONSTRAINT [FK_DT_MESSA_REFERENCE_DT_LANGU] FOREIGN KEY([languageCode])
REFERENCES [dbo].[DT_Language] ([code])
GO
ALTER TABLE [dbo].[DT_Message] CHECK CONSTRAINT [FK_DT_MESSA_REFERENCE_DT_LANGU]
GO
/****** Object:  ForeignKey [FK_DT_MESSA_REFERENCE_DT_MESSA]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_Message]  WITH CHECK ADD  CONSTRAINT [FK_DT_MESSA_REFERENCE_DT_MESSA] FOREIGN KEY([msgTypeCode])
REFERENCES [dbo].[DT_MessageType] ([code])
GO
ALTER TABLE [dbo].[DT_Message] CHECK CONSTRAINT [FK_DT_MESSA_REFERENCE_DT_MESSA]
GO
/****** Object:  ForeignKey [FK_DT_MINSA_REFERENCE_DT_PRODU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MinSaleUnit]  WITH CHECK ADD  CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_PRODU] FOREIGN KEY([proTypeCode])
REFERENCES [dbo].[DT_ProductType] ([code])
GO
ALTER TABLE [dbo].[DT_MinSaleUnit] CHECK CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_PRODU]
GO
/****** Object:  ForeignKey [DT_MinSealSoftwareConfigCon_MinSealSoftwareConfigID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MinSaleUnitMemo]  WITH CHECK ADD  CONSTRAINT [DT_MinSealSoftwareConfigCon_MinSealSoftwareConfigID] FOREIGN KEY([minSaleUnitCode])
REFERENCES [dbo].[DT_MinSaleUnit] ([code])
GO
ALTER TABLE [dbo].[DT_MinSaleUnitMemo] CHECK CONSTRAINT [DT_MinSealSoftwareConfigCon_MinSealSoftwareConfigID]
GO
/****** Object:  ForeignKey [DT_MinSealSoftwareConfigPrice_MinSealSoftwareConfig]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MinSaleUnitPrice]  WITH CHECK ADD  CONSTRAINT [DT_MinSealSoftwareConfigPrice_MinSealSoftwareConfig] FOREIGN KEY([minSaleUnitCode])
REFERENCES [dbo].[DT_MinSaleUnit] ([code])
GO
ALTER TABLE [dbo].[DT_MinSaleUnitPrice] CHECK CONSTRAINT [DT_MinSealSoftwareConfigPrice_MinSealSoftwareConfig]
GO
/****** Object:  ForeignKey [FK_DT_MINSA_REFERENCE_DT_AREAC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MinSaleUnitPrice]  WITH CHECK ADD  CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_AREAC] FOREIGN KEY([areaCfgCode])
REFERENCES [dbo].[DT_AreaConfig] ([code])
GO
ALTER TABLE [dbo].[DT_MinSaleUnitPrice] CHECK CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_AREAC]
GO
/****** Object:  ForeignKey [FK_DT_MINSA_REFERENCE_DT_MINSA]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MinSaleUnitSaleCfgDetail]  WITH CHECK ADD  CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_MINSA] FOREIGN KEY([minSaleUnitCode])
REFERENCES [dbo].[DT_MinSaleUnit] ([code])
GO
ALTER TABLE [dbo].[DT_MinSaleUnitSaleCfgDetail] CHECK CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_MINSA]
GO
/****** Object:  ForeignKey [FK_DT_MINSA_REFERENCE_DT_SALEC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MinSaleUnitSaleCfgDetail]  WITH CHECK ADD  CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_SALEC] FOREIGN KEY([saleCfgCode])
REFERENCES [dbo].[DT_SaleConfig] ([code])
GO
ALTER TABLE [dbo].[DT_MinSaleUnitSaleCfgDetail] CHECK CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_SALEC]
GO
/****** Object:  ForeignKey [DT_MinSealSoftwareConfigDetail_MinSealSoftwareConfigID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MinSaleUnitSoftwareDetail]  WITH CHECK ADD  CONSTRAINT [DT_MinSealSoftwareConfigDetail_MinSealSoftwareConfigID] FOREIGN KEY([minSaleUnitCode])
REFERENCES [dbo].[DT_MinSaleUnit] ([code])
GO
ALTER TABLE [dbo].[DT_MinSaleUnitSoftwareDetail] CHECK CONSTRAINT [DT_MinSealSoftwareConfigDetail_MinSealSoftwareConfigID]
GO
/****** Object:  ForeignKey [FK_DT_MINSA_REFERENCE_DT_SOFTW]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_MinSaleUnitSoftwareDetail]  WITH CHECK ADD  CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_SOFTW] FOREIGN KEY([softwareTypeCode])
REFERENCES [dbo].[DT_SoftwareType] ([code])
GO
ALTER TABLE [dbo].[DT_MinSaleUnitSoftwareDetail] CHECK CONSTRAINT [FK_DT_MINSA_REFERENCE_DT_SOFTW]
GO
/****** Object:  ForeignKey [FK_DT_ORDER_REFERENCE_DT_CUSTO]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_OrderInfo]  WITH CHECK ADD  CONSTRAINT [FK_DT_ORDER_REFERENCE_DT_CUSTO] FOREIGN KEY([userCode])
REFERENCES [dbo].[DT_CustomerInfo] ([code])
GO
ALTER TABLE [dbo].[DT_OrderInfo] CHECK CONSTRAINT [FK_DT_ORDER_REFERENCE_DT_CUSTO]
GO
/****** Object:  ForeignKey [DT_Order_MinSealSoftwareConfig_Info_orderNo]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_OrderMinSaleUnitDetail]  WITH CHECK ADD  CONSTRAINT [DT_Order_MinSealSoftwareConfig_Info_orderNo] FOREIGN KEY([orderCode])
REFERENCES [dbo].[DT_OrderInfo] ([code])
GO
ALTER TABLE [dbo].[DT_OrderMinSaleUnitDetail] CHECK CONSTRAINT [DT_Order_MinSealSoftwareConfig_Info_orderNo]
GO
/****** Object:  ForeignKey [FK_DT_DT_ProductInfo_productTypeCoding]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ProductInfo]  WITH CHECK ADD  CONSTRAINT [FK_DT_DT_ProductInfo_productTypeCoding] FOREIGN KEY([proTypeCode])
REFERENCES [dbo].[DT_ProductType] ([code])
GO
ALTER TABLE [dbo].[DT_ProductInfo] CHECK CONSTRAINT [FK_DT_DT_ProductInfo_productTypeCoding]
GO
/****** Object:  ForeignKey [FK_DT_SealContract_ID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ProductInfo]  WITH CHECK ADD  CONSTRAINT [FK_DT_SealContract_ID] FOREIGN KEY([saleContractCode])
REFERENCES [dbo].[DT_SaleContract] ([code])
GO
ALTER TABLE [dbo].[DT_ProductInfo] CHECK CONSTRAINT [FK_DT_SealContract_ID]
GO
/****** Object:  ForeignKey [DT_ProductSoftwareValidStatus_proserialNo]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ProductSoftwareValidStatus]  WITH CHECK ADD  CONSTRAINT [DT_ProductSoftwareValidStatus_proserialNo] FOREIGN KEY([proCode])
REFERENCES [dbo].[DT_ProductInfo] ([code])
GO
ALTER TABLE [dbo].[DT_ProductSoftwareValidStatus] CHECK CONSTRAINT [DT_ProductSoftwareValidStatus_proserialNo]
GO
/****** Object:  ForeignKey [FK_DT_DT_ProductSoftwareValidStatus_MinSealSoftwareConfigID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ProductSoftwareValidStatus]  WITH CHECK ADD  CONSTRAINT [FK_DT_DT_ProductSoftwareValidStatus_MinSealSoftwareConfigID] FOREIGN KEY([minSaleUnitCode])
REFERENCES [dbo].[DT_MinSaleUnit] ([code])
GO
ALTER TABLE [dbo].[DT_ProductSoftwareValidStatus] CHECK CONSTRAINT [FK_DT_DT_ProductSoftwareValidStatus_MinSealSoftwareConfigID]
GO
/****** Object:  ForeignKey [FK_DT_PRODU_DT_PRODUC_DT_PRODU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ProductTypeTools]  WITH CHECK ADD  CONSTRAINT [FK_DT_PRODU_DT_PRODUC_DT_PRODU] FOREIGN KEY([productTypeCode])
REFERENCES [dbo].[DT_ProductType] ([code])
GO
ALTER TABLE [dbo].[DT_ProductTypeTools] CHECK CONSTRAINT [FK_DT_PRODU_DT_PRODUC_DT_PRODU]
GO
/****** Object:  ForeignKey [FK_DT_PRODU_REFERENCE_DT_TOOLD]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ProductTypeTools]  WITH CHECK ADD  CONSTRAINT [FK_DT_PRODU_REFERENCE_DT_TOOLD] FOREIGN KEY([toolsCode])
REFERENCES [dbo].[DT_ToolDownload] ([code])
GO
ALTER TABLE [dbo].[DT_ProductTypeTools] CHECK CONSTRAINT [FK_DT_PRODU_REFERENCE_DT_TOOLD]
GO
/****** Object:  ForeignKey [DT_ProductUpRecord_proserialNo]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ProductUpgradeRecord]  WITH CHECK ADD  CONSTRAINT [DT_ProductUpRecord_proserialNo] FOREIGN KEY([proCode])
REFERENCES [dbo].[DT_ProductInfo] ([code])
GO
ALTER TABLE [dbo].[DT_ProductUpgradeRecord] CHECK CONSTRAINT [DT_ProductUpRecord_proserialNo]
GO
/****** Object:  ForeignKey [FK_DT_PRODU_REFERENCE_DT_SOFTW]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ProductUpgradeRecord]  WITH CHECK ADD  CONSTRAINT [FK_DT_PRODU_REFERENCE_DT_SOFTW] FOREIGN KEY([softwareTypeCode])
REFERENCES [dbo].[DT_SoftwareType] ([code])
GO
ALTER TABLE [dbo].[DT_ProductUpgradeRecord] CHECK CONSTRAINT [FK_DT_PRODU_REFERENCE_DT_SOFTW]
GO
/****** Object:  ForeignKey [FK_RECHARGECARDINFO_DT_RECHA]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ReChargeCardInfo]  WITH CHECK ADD  CONSTRAINT [FK_RECHARGECARDINFO_DT_RECHA] FOREIGN KEY([reChargeCardTypeCode])
REFERENCES [dbo].[DT_ReChargeCardType] ([code])
GO
ALTER TABLE [dbo].[DT_ReChargeCardInfo] CHECK CONSTRAINT [FK_RECHARGECARDINFO_DT_RECHA]
GO
/****** Object:  ForeignKey [FK_RECHARGECARDTYPE_DT_AREAC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ReChargeCardType]  WITH CHECK ADD  CONSTRAINT [FK_RECHARGECARDTYPE_DT_AREAC] FOREIGN KEY([areaCfgCode])
REFERENCES [dbo].[DT_AreaConfig] ([code])
GO
ALTER TABLE [dbo].[DT_ReChargeCardType] CHECK CONSTRAINT [FK_RECHARGECARDTYPE_DT_AREAC]
GO
/****** Object:  ForeignKey [FK_RECHARGECARDTYPE_DT_PRODU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ReChargeCardType]  WITH CHECK ADD  CONSTRAINT [FK_RECHARGECARDTYPE_DT_PRODU] FOREIGN KEY([proTypeCode])
REFERENCES [dbo].[DT_ProductType] ([code])
GO
ALTER TABLE [dbo].[DT_ReChargeCardType] CHECK CONSTRAINT [FK_RECHARGECARDTYPE_DT_PRODU]
GO
/****** Object:  ForeignKey [FK_RECHARGECARDTYPE_DT_SALEC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ReChargeCardType]  WITH CHECK ADD  CONSTRAINT [FK_RECHARGECARDTYPE_DT_SALEC] FOREIGN KEY([saleCfgCode])
REFERENCES [dbo].[DT_SaleConfig] ([code])
GO
ALTER TABLE [dbo].[DT_ReChargeCardType] CHECK CONSTRAINT [FK_RECHARGECARDTYPE_DT_SALEC]
GO
/****** Object:  ForeignKey [FK_RECHARGERECORD_DT_PRODU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ReChargeRecord]  WITH CHECK ADD  CONSTRAINT [FK_RECHARGERECORD_DT_PRODU] FOREIGN KEY([proCode])
REFERENCES [dbo].[DT_ProductInfo] ([code])
GO
ALTER TABLE [dbo].[DT_ReChargeRecord] CHECK CONSTRAINT [FK_RECHARGERECORD_DT_PRODU]
GO
/****** Object:  ForeignKey [FK_RECHARGERECORD_DT_RECHA]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ReChargeRecord]  WITH CHECK ADD  CONSTRAINT [FK_RECHARGERECORD_DT_RECHA] FOREIGN KEY([reChargeCardCode])
REFERENCES [dbo].[DT_ReChargeCardInfo] ([code])
GO
ALTER TABLE [dbo].[DT_ReChargeRecord] CHECK CONSTRAINT [FK_RECHARGERECORD_DT_RECHA]
GO
/****** Object:  ForeignKey [FK_REPLY_CUSTOMER_COMPLAINT_ID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ReCustomerComplaintInfo]  WITH CHECK ADD  CONSTRAINT [FK_REPLY_CUSTOMER_COMPLAINT_ID] FOREIGN KEY([customerComplaintCode])
REFERENCES [dbo].[DT_CustomerComplaintInfo] ([code])
GO
ALTER TABLE [dbo].[DT_ReCustomerComplaintInfo] CHECK CONSTRAINT [FK_REPLY_CUSTOMER_COMPLAINT_ID]
GO
/****** Object:  ForeignKey [FK_DT_ROLEA_REFERENCE_DT_AUTHO]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_RoleAuth]  WITH CHECK ADD  CONSTRAINT [FK_DT_ROLEA_REFERENCE_DT_AUTHO] FOREIGN KEY([actid])
REFERENCES [dbo].[DT_Authority] ([actid])
GO
ALTER TABLE [dbo].[DT_RoleAuth] CHECK CONSTRAINT [FK_DT_ROLEA_REFERENCE_DT_AUTHO]
GO
/****** Object:  ForeignKey [FK_DT_ROLEA_REFERENCE_DT_ROLE]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_RoleAuth]  WITH CHECK ADD  CONSTRAINT [FK_DT_ROLEA_REFERENCE_DT_ROLE] FOREIGN KEY([roleid])
REFERENCES [dbo].[DT_Role] ([roleid])
GO
ALTER TABLE [dbo].[DT_RoleAuth] CHECK CONSTRAINT [FK_DT_ROLEA_REFERENCE_DT_ROLE]
GO
/****** Object:  ForeignKey [DT_SealSoftwareConfig_productTypeID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SaleConfig]  WITH CHECK ADD  CONSTRAINT [DT_SealSoftwareConfig_productTypeID] FOREIGN KEY([proTypeCode])
REFERENCES [dbo].[DT_ProductType] ([code])
GO
ALTER TABLE [dbo].[DT_SaleConfig] CHECK CONSTRAINT [DT_SealSoftwareConfig_productTypeID]
GO
/****** Object:  ForeignKey [DT_SealContract_productTypeID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SaleContract]  WITH CHECK ADD  CONSTRAINT [DT_SealContract_productTypeID] FOREIGN KEY([proTypeCode])
REFERENCES [dbo].[DT_ProductType] ([code])
GO
ALTER TABLE [dbo].[DT_SaleContract] CHECK CONSTRAINT [DT_SealContract_productTypeID]
GO
/****** Object:  ForeignKey [FK_DT_SALEC_REFERENCE_DT_AREAC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SaleContract]  WITH CHECK ADD  CONSTRAINT [FK_DT_SALEC_REFERENCE_DT_AREAC] FOREIGN KEY([areaCfgCode])
REFERENCES [dbo].[DT_AreaConfig] ([code])
GO
ALTER TABLE [dbo].[DT_SaleContract] CHECK CONSTRAINT [FK_DT_SALEC_REFERENCE_DT_AREAC]
GO
/****** Object:  ForeignKey [FK_DT_SALEC_REFERENCE_DT_LANGU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SaleContract]  WITH CHECK ADD  CONSTRAINT [FK_DT_SALEC_REFERENCE_DT_LANGU] FOREIGN KEY([languageCfgCode])
REFERENCES [dbo].[DT_LanguageConfig] ([code])
GO
ALTER TABLE [dbo].[DT_SaleContract] CHECK CONSTRAINT [FK_DT_SALEC_REFERENCE_DT_LANGU]
GO
/****** Object:  ForeignKey [FK_DT_SALEC_REFERENCE_DT_SALEC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SaleContract]  WITH CHECK ADD  CONSTRAINT [FK_DT_SALEC_REFERENCE_DT_SALEC] FOREIGN KEY([saleCfgCode])
REFERENCES [dbo].[DT_SaleConfig] ([code])
GO
ALTER TABLE [dbo].[DT_SaleContract] CHECK CONSTRAINT [FK_DT_SALEC_REFERENCE_DT_SALEC]
GO
/****** Object:  ForeignKey [FK_DT_SealerType_ID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SaleContract]  WITH CHECK ADD  CONSTRAINT [FK_DT_SealerType_ID] FOREIGN KEY([sealerTypeCode])
REFERENCES [dbo].[DT_SealerType] ([code])
GO
ALTER TABLE [dbo].[DT_SaleContract] CHECK CONSTRAINT [FK_DT_SealerType_ID]
GO
/****** Object:  ForeignKey [FK_SALECONTRACT_SEALERINFO]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SaleContract]  WITH CHECK ADD  CONSTRAINT [FK_SALECONTRACT_SEALERINFO] FOREIGN KEY([sealerCode])
REFERENCES [dbo].[DT_SealerInfo] ([code])
GO
ALTER TABLE [dbo].[DT_SaleContract] CHECK CONSTRAINT [FK_SALECONTRACT_SEALERINFO]
GO
/****** Object:  ForeignKey [FK_SEALERINFO_LANGUAGE]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SealerInfo]  WITH CHECK ADD  CONSTRAINT [FK_SEALERINFO_LANGUAGE] FOREIGN KEY([languageCode])
REFERENCES [dbo].[DT_Language] ([code])
GO
ALTER TABLE [dbo].[DT_SealerInfo] CHECK CONSTRAINT [FK_SEALERINFO_LANGUAGE]
GO
/****** Object:  ForeignKey [FK_DT_SHOPP_SHOPPINGC_DT_AREAC]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ShoppingCart]  WITH CHECK ADD  CONSTRAINT [FK_DT_SHOPP_SHOPPINGC_DT_AREAC] FOREIGN KEY([areaCfgCode])
REFERENCES [dbo].[DT_AreaConfig] ([code])
GO
ALTER TABLE [dbo].[DT_ShoppingCart] CHECK CONSTRAINT [FK_DT_SHOPP_SHOPPINGC_DT_AREAC]
GO
/****** Object:  ForeignKey [FK_DT_SHOPP_SHOPPINGC_DT_PRODU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_ShoppingCart]  WITH CHECK ADD  CONSTRAINT [FK_DT_SHOPP_SHOPPINGC_DT_PRODU] FOREIGN KEY([proCode])
REFERENCES [dbo].[DT_ProductInfo] ([code])
GO
ALTER TABLE [dbo].[DT_ShoppingCart] CHECK CONSTRAINT [FK_DT_SHOPP_SHOPPINGC_DT_PRODU]
GO
/****** Object:  ForeignKey [FK_DT_SOFTWAREVERSION_ID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SoftwarePack]  WITH CHECK ADD  CONSTRAINT [FK_DT_SOFTWAREVERSION_ID] FOREIGN KEY([softwareVersionCode])
REFERENCES [dbo].[DT_SoftwareVersion] ([code])
GO
ALTER TABLE [dbo].[DT_SoftwarePack] CHECK CONSTRAINT [FK_DT_SOFTWAREVERSION_ID]
GO
/****** Object:  ForeignKey [DT_SoftwareType_productTypeID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SoftwareType]  WITH CHECK ADD  CONSTRAINT [DT_SoftwareType_productTypeID] FOREIGN KEY([proTypeCode])
REFERENCES [dbo].[DT_ProductType] ([code])
GO
ALTER TABLE [dbo].[DT_SoftwareType] CHECK CONSTRAINT [DT_SoftwareType_productTypeID]
GO
/****** Object:  ForeignKey [FK_DT_SoftwareType_ID]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_SoftwareVersion]  WITH CHECK ADD  CONSTRAINT [FK_DT_SoftwareType_ID] FOREIGN KEY([softwareTypeCode])
REFERENCES [dbo].[DT_SoftwareType] ([code])
GO
ALTER TABLE [dbo].[DT_SoftwareVersion] CHECK CONSTRAINT [FK_DT_SoftwareType_ID]
GO
/****** Object:  ForeignKey [FK_DT_TESTP_REFERENCE_DT_PRODU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_TestPack]  WITH CHECK ADD  CONSTRAINT [FK_DT_TESTP_REFERENCE_DT_PRODU] FOREIGN KEY([proCode])
REFERENCES [dbo].[DT_ProductInfo] ([code])
GO
ALTER TABLE [dbo].[DT_TestPack] CHECK CONSTRAINT [FK_DT_TESTP_REFERENCE_DT_PRODU]
GO
/****** Object:  ForeignKey [FK_DT_TESTP_REFERENCE_DT_SOFTW]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_TestPack]  WITH CHECK ADD  CONSTRAINT [FK_DT_TESTP_REFERENCE_DT_SOFTW] FOREIGN KEY([softwareTypeCode])
REFERENCES [dbo].[DT_SoftwareType] ([code])
GO
ALTER TABLE [dbo].[DT_TestPack] CHECK CONSTRAINT [FK_DT_TESTP_REFERENCE_DT_SOFTW]
GO
/****** Object:  ForeignKey [FK_DT_TESTP_REFERENCE_DT_LANGU]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_TestPackLanguagePack]  WITH CHECK ADD  CONSTRAINT [FK_DT_TESTP_REFERENCE_DT_LANGU] FOREIGN KEY([languageTypeCode])
REFERENCES [dbo].[DT_Language] ([code])
GO
ALTER TABLE [dbo].[DT_TestPackLanguagePack] CHECK CONSTRAINT [FK_DT_TESTP_REFERENCE_DT_LANGU]
GO
/****** Object:  ForeignKey [FK_DT_TESTP_REFERENCE_DT_TESTP]    Script Date: 04/07/2013 14:53:36 ******/
ALTER TABLE [dbo].[DT_TestPackLanguagePack]  WITH CHECK ADD  CONSTRAINT [FK_DT_TESTP_REFERENCE_DT_TESTP] FOREIGN KEY([testPackCode])
REFERENCES [dbo].[DT_TestPack] ([code])
GO
ALTER TABLE [dbo].[DT_TestPackLanguagePack] CHECK CONSTRAINT [FK_DT_TESTP_REFERENCE_DT_TESTP]
GO
/****** Object:  ForeignKey [FK_dt_user_role_userID]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[DT_UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dt_user_role_userID] FOREIGN KEY([userid])
REFERENCES [dbo].[DT_SysUser] ([userid])
GO
ALTER TABLE [dbo].[DT_UserRole] CHECK CONSTRAINT [FK_dt_user_role_userID]
GO
/****** Object:  ForeignKey [FK_DT_USERR_REFERENCE_DT_ROLE]    Script Date: 04/07/2013 14:53:37 ******/
ALTER TABLE [dbo].[DT_UserRole]  WITH CHECK ADD  CONSTRAINT [FK_DT_USERR_REFERENCE_DT_ROLE] FOREIGN KEY([roleid])
REFERENCES [dbo].[DT_Role] ([roleid])
GO
ALTER TABLE [dbo].[DT_UserRole] CHECK CONSTRAINT [FK_DT_USERR_REFERENCE_DT_ROLE]
GO
