<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<div class="leg_topri">
	<fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.otherconfig" /> > <fmt:message key="syssetting.otherconfig.smtp" /> > 
	<c:if test="${isedit==1 }" ><fmt:message key="syssetting.otherconfig.modsmtp" /></c:if>
	<c:if test="${isedit==0 }" ><fmt:message key="syssetting.otherconfig.addsmtp" /></c:if>
</div>
<c:if test="${isedit==1 }" >
<form method="post" action="smtp!saveEdit.do" class="validate" >
<input type="hidden" name="smtp.id" value="${smtp.id }"/>
</c:if>
<c:if test="${isedit==0 }" >
<form method="post" action="smtp!saveAdd.do" class="validate" >
</c:if>
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;
	<c:if test="${isedit==1 }" ><fmt:message key="syssetting.otherconfig.modsmtp" /></c:if>
	<c:if test="${isedit==0 }" ><fmt:message key="syssetting.otherconfig.addsmtp" /></c:if>
	</h4>
		
	<table cellspacing="1" cellpadding="3" width="100%" class="form-table">
		<tr>
			<th><label class="text">Host:</label></th>
			<td>
			<input type="text" name="smtp.host" id="name" maxlength="25" value="${smtp.host }"  isrequired="true" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.authorityman.username" />:</label></th>
			<td>
			<input type="text" name="smtp.username"  size="40" maxlength="40"   value="${smtp.username }"  isrequired="true"  />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="memberman.sealerinfoman.passwd" />:</label></th>
			<td>
			<input type="text" name="smtp.password"  size="40" maxlength="40"    value="${smtp.password }"  isrequired="true"  />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.stmp.port" />:</label></th>
			<td>
			<input type="text" name="smtp.port"  size="40" maxlength="20"    value="${smtp.port }"  isrequired="true" dataType="int"  />
			</td>
		</tr>
		<tr>
			<th><label class="text">From:</label></th>
			<td>
			<input type="hidden"" name="smtp.max_count"  value="9999"/>
			<input type="text" name="smtp.mail_from"  size="40"   value="${smtp.mail_from }"  maxlength="40"  isrequired="true"  />
			</td>
		</tr>
		
			
		<c:if test="${isedit==1 }" >
		
		<tr>
			<th><label class="text"><fmt:message key="syssetting.otherconfig.hassend" /></label></th>
			<td>${smtp.send_count }<fmt:message key="syssetting.otherconfig.mailunion" /></td>
		</tr>
		
		</c:if>
		
	</table>

</div>
<div class="submitlist" align="center">
<table>
	<tr>
		<td>
			<input type="submit" name="submit" value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
			<input name="reset" type="button" onclick="location.href='smtp!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
		</td>
	</tr>
</table>
</div>
</form>

<script type="text/javascript">
	$(function() {
		$("form.validate").validate();
	});
</script>