<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="js/Smtp.js"></script>
<script type="text/javascript">
var message1 = '<fmt:message key="stmp.message.comfirm1" />';
var message2 = '<fmt:message key="stmp.message.comfirm2" />';
var message3 = '<fmt:message key="stmp.message.comfirm3" />';
</script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.otherconfig" /> > <fmt:message key="syssetting.otherconfig.smtp" /></div>
<div class="grid">
	
	<div class="toolbar" >
		<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.otherconfig.smtp" /></h4>
		<div class="active">
			<h4>
				<span>
					<input type="button" onclick="location.href='smtp!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" />
					<input type="button" id="delBtn" value=<fmt:message key="common.list.del" /> class="search_but" />
				</span>
				&nbsp;
			</h4>
		</div>
	</div>


	<form method="POST">
		<grid:grid from="smtpList">

			<grid:header>
				<grid:cell width="50px">
					<input type="checkbox"  id="toggleChk" />
				</grid:cell>
				<grid:cell width="180px">Host</grid:cell>
				<grid:cell width="180px"><fmt:message key="syssetting.authorityman.username" /></grid:cell>
				<grid:cell ><fmt:message key="memberman.sealerinfoman.passwd" /></grid:cell>
				<grid:cell ><fmt:message key="syssetting.otherconfig.stmp.port" /></grid:cell>
				<grid:cell ><fmt:message key="syssetting.otherconfig.smtptodaymegnum" /></grid:cell>
				<grid:cell >From</grid:cell>
				<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell>
			</grid:header>

			<grid:body item="smtp">
				<grid:cell>
					<input type="checkbox" name="idAr" value="${smtp.id }" />
				</grid:cell>
				<grid:cell>
					${fn:substring(smtp.host,0,20) }
					<c:if test="${fn:length(smtp.host) > 20 }">...</c:if>
				</grid:cell>
				<grid:cell>
					${fn:substring(smtp.username,0,20) }
					<c:if test="${fn:length(smtp.username) > 20 }">...</c:if>
				</grid:cell>
				<grid:cell>	
					${fn:substring(smtp.password,0,20) }
					<c:if test="${fn:length(smtp.password) > 20 }">...</c:if>
				</grid:cell>
				<grid:cell>${smtp.port}</grid:cell>		
				<grid:cell>${smtp.send_count}</grid:cell>		
				<grid:cell> 
					${fn:substring(smtp.mail_from,0,20) }
					<c:if test="${fn:length(smtp.mail_from) > 20 }">...</c:if>
				</grid:cell>							
				<grid:cell>
					<a href="smtp!edit.do?id=${smtp.id }"><fmt:message key="common.list.mod" /></a>
					&nbsp;&nbsp;
					<a class="delete" deleteItemId="${smtp.id }" ><fmt:message key="common.list.del" /></a>
				</grid:cell>
			</grid:body>

		</grid:grid>
	</form>
	<div style="clear: both; padding-top: 5px;"></div>
</div>