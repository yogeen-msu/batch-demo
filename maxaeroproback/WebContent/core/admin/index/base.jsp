<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
      <tbody>        
          <tr>
            <th width="80px" align="right"><span><fmt:message key="homepage.site.sitename" />:&nbsp;&nbsp;</span></th>
            <td><span><fmt:message key="${site.sitename}" /></span></td>
          </tr>
          <tr>
            <th align="right"><span><fmt:message key="homepage.site.lastlogin" />:&nbsp;&nbsp;</span></th>
            <td><span><html:dateformat time="${site.lastlogin*1000}" pattern="yyyy-MM-dd HH:mm:ss"></html:dateformat></span></td>
          </tr>
          <tr>
            <th align="right"><span><fmt:message key="homepage.site.createtime" />:&nbsp;&nbsp;</span></th>
            <td><span>2013-07-01 00:00:00</span></td>
          </tr>
          <tr>
            <th style="height:21px"><span>&nbsp;</span></th>
            <td><span>&nbsp;</span></td>
          </tr>          
        </tbody>
 </table>