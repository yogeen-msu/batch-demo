<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
      <tbody>        
          <tr>
            <th><span><fmt:message key="homepage.flow.visittoday" />:</span></th>
            <td><span>${ access.todayaccess}<fmt:message key="homepage.flow.frequency" /></span></td>
          </tr>
          <tr>
            <th><span><fmt:message key="homepage.flow.thismonth.visit" />:</span></th>
            <td><span>${ access.monthaccess}<fmt:message key="homepage.flow.frequency" /></span></td>
            
          </tr>
          <tr>
            <th><span><fmt:message key="homepage.flow.cumulative" />:</span></th>
            <td><span>${ access.sumaccess}<fmt:message key="homepage.flow.frequency" /></span></td>
          </tr>
          <tr>
            <th><span><fmt:message key="homepage.flow.today.consumption" />:</span></th>
            <td><span>${ access.todaypoint}</span></td>
          </tr>
          <tr>
            <th><span><fmt:message key="homepage.flow.thismonth.consumption" />:</span></th>
            <td><span>${ access.monthpoint}</span></td>
            
          </tr>
          <tr>
            <th><span><fmt:message key="homepage.flow.cumula.consumption" />:</span></th>
            <td><span>${ access.sumpoint}</span></td>
          </tr>          
        </tbody>
</table>