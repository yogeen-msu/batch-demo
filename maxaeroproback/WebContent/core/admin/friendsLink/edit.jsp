<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp" %>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.overallman" /> > <fmt:message key="cententman.overallman.friendlink" /> > <fmt:message key="cententman.overallman.editfriendlink" /></div>
 <form class="validate" method="post" action="friendsLink!editSave.do" name="theForm" id="theForm" enctype="multipart/form-data">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.overallman.editfriendlink" /> </h4>
	
 <input type="hidden" name="friendsLink.link_id" value="${friendsLink.link_id }" />
  <input type="hidden" name="oldpic" value="${friendsLink.logo }" />
   <table cellspacing="1" cellpadding="3" width="100%" class="form-table">
     <tr>
       <th><label class="text"><fmt:message key="cententman.overallman.friendlinkname" />:</label></th>
       <td  ><input type="text" name="friendsLink.name" maxlength="60" value="${friendsLink.name }"  dataType="string" isrequired="true" />       </td>
      </tr>
      <tr>
       <th><label class="text"><fmt:message key="cententman.overallman.friendlinkaddress" />:</label></th>
       <td  ><input type="text" name="friendsLink.url" maxlength="60" value="${friendsLink.url }"  dataType="string" isrequired="true" /></td>
      </tr>
      <tr>
       <th><label class="text"><fmt:message key="cententman.cms.sort" />:</label></th>
       <td  ><input type="text" name="friendsLink.sort" maxlength="60" value="${friendsLink.sort }"  dataType="int" /></td>
      </tr>
      <tr>
       <th><label class="text"><fmt:message key="cententman.overallman.friendlinkpic" />:</label></th>
       <td><input type="file" name="pic" id="pic" size="45"/>
           <span class="notice-span"  id="warn_pic"><fmt:message key="cententman.overallman.uploadpic" /></span></td>
      </tr>
      <c:if test="${friendsLink.logo!=null }">
	     <tr>
	       <th>&nbsp;</td>
	       <td> 
	       <img src="${friendsLink.logo }" />	       </td>
	     </tr> 
     </c:if>
 
   </table>
</div>
   <div class="submitlist" align="center">
 <table>
 <tr><td >
  <input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
  <input name="reset" type="button" onclick="location.href='friendsLink!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
   </td>
   </tr>
 </table>
</div>
 </form>
 <script type="text/javascript">
$("form.validate").validate();
</script>