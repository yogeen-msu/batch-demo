<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp" %>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.overallman" /> > <fmt:message key="cententman.overallman.friendlink" /> > <fmt:message key="cententman.overallman.addfriendlink" /></div>

 <form class="validate" method="post" action="friendsLink!addSave.do" name="theForm" id="theForm" enctype="multipart/form-data">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.overallman.addfriendlink" /> </h4>
	
   <table cellspacing="1" cellpadding="3" width="100%" class="form-table">
     <tr>
       <td  class="label"><label class="text"><fmt:message key="cententman.overallman.friendlinkname" />:</label></td>
       <td  ><input type="text" name="friendsLink.name" maxlength="60"  dataType="string" isrequired="true" />       </td>
      </tr>
      <tr>
       <td  class="label"><label class="text"><fmt:message key="cententman.overallman.friendlinkaddress" />:</label></td>
       <td  ><input type="text" name="friendsLink.url" maxlength="60"  dataType="string" isrequired="true" /></td>
      </tr>
      <tr>
       <td  class="label"><label class="text"><fmt:message key="cententman.cms.sort" />:</label></td>
       <td  ><input type="text" name="friendsLink.sort" maxlength="60" dataType="int"  /></td>
      </tr>
      <tr>
       <td  class="label"><label class="text"><fmt:message key="cententman.overallman.friendlinkpic" />:</label></td>
       <td><input type="file" name="pic" id="pic" size="45"/>
           <span class="notice-span"  id="warn_pic"><fmt:message key="cententman.overallman.uploadpic" /></span></td>
      </tr>
   </table>
</div>
   <div class="submitlist" align="center">
 <table>
 <tr><td >
  <input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
    <input name="reset" type="button" onclick="location.href='friendsLink!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
   </td>
   </tr>
 </table>
</div>
 </form>
 <script type="text/javascript">
$("form.validate").validate();
</script>