<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp"%>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.overallman" /> > <fmt:message key="cententman.overallman.friendlink" /></div>

<div class="grid">
	<div class="toolbar" >
		<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="cententman.overallman.friendlink" /></h4>
		<div class="active">
			<h4>
				<span>
					<input type="button" onclick="location.href='friendsLink!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" />
				</span>
				&nbsp;
			</h4>
		</div>
	</div>
	
	
	
<form method="POST">
<grid:grid from="listLink">
	<grid:header>
	<grid:cell width="50px"><input type="checkbox" id="toggleChk" /></grid:cell>
	<grid:cell width="250px"><fmt:message key="cententman.overallman.friendlinkname" /></grid:cell> 
	<grid:cell><fmt:message key="cententman.overallman.friendlinkaddress" /></grid:cell>
	<grid:cell><fmt:message key="cententman.cms.sort" /></grid:cell> 
	<grid:cell width="100px"><fmt:message key="common.list.operation" /></grid:cell> 
	</grid:header>

  <grid:body item="link">
  		<grid:cell><input type="checkbox" name="id" value="${link.link_id }" />${link.link_id } </grid:cell>
        <grid:cell>${link.name } </grid:cell>
        <grid:cell>${link.url} </grid:cell>
        <grid:cell>${link.sort }</grid:cell>  
        <grid:cell>
			<a href="friendsLink!edit.do?link_id=${link.link_id }" ><fmt:message key="common.list.mod" /></a> 
			<a href="friendsLink!delete.do?id=${link.link_id }" linkid="${link.link_id}"><fmt:message key="common.list.del" /></a>
 		</grid:cell>					
  </grid:body>  
  
</grid:grid>  
</form>	
</div>
<script type="text/javascript">
$(function(){
	$(".delete").parent().click(function(){	
		
		
		if( !confirm('<fmt:message key="cententman.overallman.removecheck" />') ){//"确定删除此链接吗?"
			return false;
		}
		var id = $(this).attr("linkid");  
		$.Loading.show('<fmt:message key="syssetting.toolman.urimapping.removing" />'); //'正在删除，请稍侯...'
		$.ajax({
			url:'friendsLink!delete.do?id='+id+'&ajax=yes',
			type:'get',
			dataType:'json',
			success:function(result){
				$.Loading.hide();
				if(result.result==0){
					//alert("删除成功");
					alert('<fmt:message key="market.promotion.delSuccess" />');
					location.reload();
				}else{
					alert(result.message);
				}						
			},
			error:function(){
				alert("error");
			}	
		});
		return false;
	});
});
</script>

