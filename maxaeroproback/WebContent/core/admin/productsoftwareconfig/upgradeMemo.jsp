<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/taglibsframe.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="marketman.minusalenit" /><fmt:message key="marketman.minusalenitman.softwareman" /></title>
<script type="text/javascript" src="../../autel/js/common/jquery-1.7.2.min.js"></script>
<link href="../../autel/main/css/right.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$("#sumbitBtn").click(function(){
		
		
		var a=[];
		$("#picTable tr").each(function(i){
			if($(this).attr("id")=="picPathTR"){
			 var name=  $($(this).find("input[name='picPath']")).val();
			 var code=  $($(this).find("select[name='languageSelect']")).val();
			 a.push({ "languageCode": code,"name":name}); 
			}
		});
		
		
		$("#jsonStr").val(JSON.stringify(a));
		if(a.length==0){
			alert("<fmt:message key='sale.product.addjsp.serial.msg' />");
			return;
		}
		$("#configForm").submit();
	});
	
	$("#addProductTr").click(function(){
		
		var $win_complaint= $("#win_complaint");;
		var $overlay = $("#overlay");
		var $close = $("#close");
		 $win_complaint.show();
		 $overlay.show();
		
	});
	
	$("#finsh").click(function(){
		var unitCode = $("#unitCode").val();
		//éæ©çoption
		var $selectOption = $("#productType option:selected");
		var size = $selectOption.size();
		if(size ==0){
			alert("<fmt:message key='marketman.js.xzSoftwaretype' />");
			return false;
		}
		$selectOption.each(function(){
			var value= $(this).val();
			var text = $(this).text();
			var html="<tr id='ProductTR'>";
			html += "<td>"+value+"</td>";
			html += "<td>"+text+"</td>";
			html +="<td class='caoz'>";
			html +="<a href='#' onclick='cancel(this);'>"+"<fmt:message key='common.list.del' />"+"</a></td>";
			html +="</tr>";
			$("#productTable").append(html);
		});
		
		var oWin = $("#win_complaint");
		var oLay = $("#overlay");
		oWin.hide();
		oLay.hide();
		$selectOption.remove();
	});
	
	
	$("#addPicTr").click(function(){
		var $table = $("#picTable");
		$.ajax({
			url:'../../autel/sealserProduct/getLanguageJson.do?ajax=yes',
			type:"post",
			
			dataType:'JSON',
			success :function(data){
				if(data != null){
					var selectHtml = buildSelect(data,"");
					if(selectHtml!=0){
					var tr = "<tr id='picPathTR' languageCode='' picId=''>";
					tr += "<td>"+selectHtml+"</td>";
					tr += "<td>";
					tr += "<input type='text' class='text-left' id='picPath' name='picPath' style='width:80%;' value='' languageName='' picPath='' maxlength='1000'/>";  
					tr += "</td>";
					tr += "<td class='caoz'>";
					tr += "&nbsp;&nbsp;";
					tr += "<a href='#' onclick='cancel(this);'>"+"<fmt:message key='common.list.del' />"+"</a>";
					tr += "</td>";
					tr += "</tr>";
					$table.append(tr);
					}
				}
				
			},
			error :function(){
				alert("<fmt:message key='common.js.system.error' />");
			}
		});
	});
});

function cancel(event){
	var $tr = $(event).closest("tr");
	$tr.remove();
}

function jump(url){
	var e=document.createElement("a");
	e.href=url;
	document.body.appendChild(e);
	e.click();
}
function buildSelect(data,option){
	var jsonData = eval(data);
	var select = "<select id='languageSelect' class='act_zt' name='languageSelect'>";
	if(option != ""){
		select += option;
	}
	var code="";
	$("#picTable tr").each(function(i){
		if($(this).attr("id")=="picPathTR"){
		 code=$($(this).find("select[name='languageSelect']")).val()+","+code;
		}
	});
	var num=0;
	for(var i = 0 ; i < jsonData.length; i ++){
		if(code.indexOf(jsonData[i].code)==-1){
		select += "<option value='"+jsonData[i].code+"'>"+jsonData[i].name+"</option>";
		num++;
		}
	}
	select += "</select>";
	if(num==0){
		return num;
	}
	return select;
}

function colseWin()
{
	$("#win_complaint").hide();
	$("#overlay").hide();
}

</script>
</head>

<body>
<form action="productSoftwareConfig!saveUpgradeMemo.do" class="validate" method="post" id="configForm">
<input type="hidden" name="jsonStr" id="jsonStr" value=""/>
<input type="hidden" name="productSoftwareConfigCode" id="productSoftwareConfigCode" value="${productSoftwareConfig.code}"/>
    	<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:平台管理&gt; 虚拟目录管理&gt; 消息推送维护</div>
		<div class="right_tab">
			<div class="sear">
				<h4 class="sear_tj">
					&nbsp;维护消息推送名称
				</h4>
				<div class="sear_table">
					<table width="100%" border="0">
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr height="38">
							<td width="150" align="right">虚拟目录code:</td>
							<td>&nbsp;&nbsp;<input id="ConfigCode" name="ConfigCode" style="width: 300px" type="text" value="${productSoftwareConfig.code}" disabled dataType="string" isrequired="true"/></td>
							<td></td>
						</tr>
						
						<tr height="38">
							<td width="150" align="right">虚拟目录名称:</td>
							<td>&nbsp;&nbsp;<input id="productSoftwareConfigName" name="productSoftwareConfigName" style="width: 300px" type="text" value="${productSoftwareConfig.name}" disabled dataType="string" isrequired="true"/></td>
							<td></td>
						</tr>
						
						
						<tr height="5">

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
					
				
				
				<div class="active">
					<div id="overlay"></div>
					<h4>
						<span><button id="addPicTr" type="button" class="search_but"><fmt:message key="saleConfig.add.jsp.button.add" /></button>
						</span>维护推送名称
					</h4>
				</div>
			</div>
			<div class="act">
				<table width="100%" border="0" bordercolor="#dddddd" id="picTable">
				
					<tr class="title">
						<td width="20%"><fmt:message key="memberman.complaintman.language" /></td>
						<td width="50%">推送名称</td>
						<td width="30%"><fmt:message key="common.list.operation" /></td>
					</tr>
					
					<c:forEach items="${configMemo }" var="memo">
			         <tr id='picPathTR'>
				     <td>
				     <select id='languageSelect' class='act_zt' name='languageSelect' readonly="true">
				      <c:forEach items="${language}" var="item">
								<option value="${item.code}" <c:if test="${memo.languageCode == item.code}"> selected="selected"</c:if>>${item.name}</option>
								</c:forEach>
				     </select></td>
				     <td ><input type='text' class='text-left' id='picPath' name='picPath' style='width:80%;' value='${memo.name}' languageName='' picPath='' maxlength='1000'/></td>
				     <td class="caoz">&nbsp;&nbsp;
				     <a href="###" onclick="cancel(this)"><fmt:message key="common.list.del" /></a></td>
			         </tr>
		           </c:forEach>
					
				</table>
				</div>
				<div style="padding-top: 20px;padding-left:40%">
					<input id="sumbitBtn" type="button" value="<fmt:message key="saleConfig.add.jsp.button.save" />" class="diw_but" /><input
						type="button" onclick="jump('productSoftwareConfig!list.do')" value="<fmt:message key="common.btn.return" />" class="diw_but" />
				</div>
			
		</div>
	</form>
</body>
</html>


