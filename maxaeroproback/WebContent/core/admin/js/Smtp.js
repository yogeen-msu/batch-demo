var Smtp=$.extend({},Cop.Grid,{
	init:function(){
		var self =this;
		$("#delBtn").click(function(){self.doDelete();});
		$("#toggleChk").click(function(){
			self.toggleSelected(this.checked);}
		);
		$(".delete").click(function(){self.deleteItem($(this).attr("deleteItemId"));});
		
	},
	doDelete:function(){
		
		if(!this.checkIdSeled()){
			alert(message1);
			return ;
		}
		if(confirm(message2)){
			this.deletePost("smtp!delete.do");
		}
	},
	deleteItem:function(id){
		if(confirm(message3)){
			$("input[name='idAr'][value='"+id+"']").attr("checked","checked");
			this.deletePost("smtp!delete.do?idAr="+id);
		}
	}
	
});

$(function(){
	Smtp.opation("idChkName","idAr");
	Smtp.init();
});