
var SealerDataAuth  ={
	init:function(){
		var self  = this;
		Cop.Dialog.init({id:"addDlg",modal:false,title:msg1,height:"300px",width:"450px"});
		Cop.Dialog.init({id:"editDlg",modal:false,title:msg2,height:"300px",width:"450px"});
		$(".addBtn").click(function(){
			self.openAddDlg($(this).attr("parentcode"));
		});
		$(".editBtn").click(function(){
			self.openEditDlg($(this).attr("sealerDataAuthCode"));
		});
		$(".delBtn").click(function(){
			if( confirm(msg3) ){
				self.deletesealerDataAuth($(this).attr("sealerDataAuthCode"));
			}
		});
	}, 
	
	/**
	 * 打开添加对话框
	 */
	openAddDlg:function(parentcode){
		var self  = this;
		$("#addDlg").load("sealerDataAuth!add.do?ajax=yes&sealerDataAuth.parentcode="+parentcode,function(){
			$("#saveAddBtn").click(function(){
				self.add();
			});
		});
		Cop.Dialog.open("addDlg");
	},
	/**
	 * 打开编辑对话框
	 */
	openEditDlg:function(sealerDataAuthCode){
		var self  = this;
		$("#editDlg").load("sealerDataAuth!edit.do?ajax=yes&sealerDataAuth.code="+sealerDataAuthCode,function(){
			$("#saveEditBtn").click(function(){
				self.edit();
			});
		});
		Cop.Dialog.open("editDlg");
	}
	,
	edit:function(){
		if( !$("#editForm").checkall() ){
			return ;
		}
		$.Loading.show(msg4);
		var options = {
				url :"sealerDataAuth!saveEdit.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {	
					$.Loading.hide();
				 	if(result.result==1){
				 		updateLeftMenu("update",$("#name").val(),$("#sealerDataAuthCode").val(),"");
				 		alert(msg5);
				 		location.reload();
				 	}else if(result.result==2){				 		
				 		alert(msg10);
				 	}else if(result.result==4){				 		
				 		alert(msg12);
				 	}else{
				 		alert(result.message);
				 	}
				 	
				},
				error : function(e) {
					$.Loading.hide();
					alert(msg6);
 				}
 		};
 
		$("#editForm").ajaxSubmit(options);			
	}
	,
	/**
	 * 保存添加
	 */
	add:function(){
		if( !$("#addForm").checkall() ){
			return ;
		}
		$.Loading.show(msg4);
		var options = {
				url :"sealerDataAuth!saveAdd.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {
					$.Loading.hide();
				 	if(result.result==1){
				 		updateLeftMenu("add",$("#name").val(),result.code,$("#parentcode").val());
				 		alert(msg7);
				 		location.reload();
				 	}else if(result.result==2){
				 		alert(msg10);
				 	}else{
				 		alert(result.message);
				 	}
				 	
				},
				error : function(e) {
					$.Loading.hide();
					alert(msg6);
 				}
 		};
 
		$("#addForm").ajaxSubmit(options);
	},
	deletesealerDataAuth:function(sealerDataAuthCode){
		$.Loading.show(msg8);
		$.ajax({
			url:'sealerDataAuth!delete.do?ajax=yes&sealerDataAuth.code='+sealerDataAuthCode,
			type:'post',
			dataType:'json',
			success:function(result){
				$.Loading.hide();
			 	if(result.result==1){
				 	updateLeftMenu("del","",sealerDataAuthCode,"");
			 		alert(msg9);
			 		location.reload();
			 	}else if(result.result==3){
				 		alert(msg11);
				 }else{
			 		alert(result.message);
			 	}			
			},
			error:function(){
				$.Loading.hide();
				alert(msg6);
			}
		});
	}
}; 

/**
 * 维护左边树形菜单
 * @param actType   操作类型
 * @param nodeName	节点名称
 * @param nodeCode	节点编码
 */
function updateLeftMenu(actType,nodeName,nodeCode,parentcode){
	if(actType == "update" || actType == "del"){
		$($(window.parent.document).find("#left_menu_content")).find("a").each(function(i,item){
			if($(this).attr("code") == nodeCode){
				if(actType == "update"){
					$(this).html(nodeName);
					return false;
				}else if(actType == "del"){
					if ($(this).parent().siblings().length > 0)
					{
						$(this).parent().remove();
					}
					else
					{
						$(this).parent().parent().remove();
					}
				}
			}
		});
	}else if(actType == "add"){
		var $img = $('<img width="9" height="15" src="../adminthemes/default/images/leg_08.gif" name="m1">');
		var $a = $("<a code='"+nodeCode+"' href='../autel/product/toSoftwareType.do?sealerDataAuth.parentcode="+nodeCode+"' onclick='return adminUiLoad(this);'>" + nodeName + "</a>");
		var $img2 = $('<img width="9" height="15" src="../adminthemes/default/images/leg_08.gif" name="m1">');
		var $a2 = $("<a code='"+nodeCode+"' href='../autel/product/toSoftwareType.do?sealerDataAuth.parentcode="+nodeCode+"' onclick='return adminUiLoad(this);'>" + nodeName + "</a>");
		var $dt = $("<dt></dt>");
		var $dd = $("<dd></dd>");
		$dt.append($img).append($a);
		$dd.append($img2).append($a2);
		
		$($(window.parent.document).find("#left_menu_content")).find("a").each(function(i,item){
			if($(this).attr("code") == parentcode){
				var $dl = $(this).parent().children("dl");
				if($dl.length > 0){
					if($dl.children("dt").length > 0){
						$dl.children("dt:first").before($dt);
					}
					else
					{
						$dl.children("dd:first").before($dt);
					}
				}else{
					$dl = $("<dl></dl>");
					$dl.append($dd);
				}
				$(this).parent().append($dl);
			}
		});
	}
	
}



