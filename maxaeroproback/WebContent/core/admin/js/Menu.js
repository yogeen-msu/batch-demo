var Menu  ={
	init:function(){
		var self  = this;
		Cop.Dialog.init({id:"addDlg",modal:false,title:msg1,height:"300px",width:"450px"});
		Cop.Dialog.init({id:"editDlg",modal:false,title:msg2,height:"300px",width:"450px"});
		$(".addBtn").click(function(){
			self.openAddDlg($(this).attr("parentid"));
		});
		$(".editBtn").click(function(){
			self.openEditDlg($(this).attr("menuid"));
		});
		$(".delBtn").click(function(){
			if( confirm(msg3) ){
				self.deleteMenu($(this).attr("menuid"));
			}
		});		
		$(".sortBtn").click(function(){
			 self.sort();
		});				
	}, 
	
	/**
	 * 打开添加对话框
	 */
	openAddDlg:function(pid){
		var self  = this;
		$("#addDlg").load("menu!add.do?ajax=yes&parentid="+pid,function(){
			$("#saveAddBtn").click(function(){
				self.add();
			});
		});
		Cop.Dialog.open("addDlg");
	},
	/**
	 * 打开编辑对话框
	 */
	openEditDlg:function(id){
		var self  = this;
		$("#editDlg").load("menu!edit.do?ajax=yes&id="+id,function(){
			$("#saveEditBtn").click(function(){
				self.edit();
			});
		});
		Cop.Dialog.open("editDlg");
	}
	,
	edit:function(){
		if( !$("#editForm").checkall() ){
			return ;
		}
		$.Loading.show(msg4);
		var options = {
				url :"menu!saveEdit.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {	
					$.Loading.hide();
				 	if(result.result==1){
				 		
				 		alert(msg5);
				 		location.reload();
				 	}else{
				 		alert(result.message);
				 	}
				 	
				},
				error : function(e) {
					$.Loading.hide();
					alert(msg6);
 				}
 		};
 
		$("#editForm").ajaxSubmit(options);			
	}
	,
	/**
	 * 保存添加
	 */
	add:function(){
		if( !$("#addForm").checkall() ){
			return ;
		}
		$.Loading.show(msg4);
		var options = {
				url :"menu!saveAdd.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {	
					$.Loading.hide();
				 	if(result.result==1){
				 		
				 		alert(msg7);
				 		location.reload();
				 	}else{
				 		alert(result.message);
				 	}
				 	
				},
				error : function(e) {
					$.Loading.hide();
					alert(msg6);
 				}
 		};
 
		$("#addForm").ajaxSubmit(options);			
	},
	deleteMenu:function(id){
		$.Loading.show(msg8);
		$.ajax({
			url:'menu!delete.do?ajax=yes&id='+id,
			type:'post',
			dataType:'json',
			success:function(result){
				$.Loading.hide();
			 	if(result.result==1){
			 		alert(msg9);
			 		location.reload();
			 	}else{
			 		alert(result.message);
			 	}			
			},
			error:function(){
				$.Loading.hide();
				alert(msg6);
			}
		});
	},
	sort:function(){
		$.Loading.show(msg4);
		var options = {
				url :"menu!updateSort.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {	
					$.Loading.hide();
				 	if(result.result==1){
				 		alert(msg5);
				 		location.reload();
				 	}else{
				 		alert(result.message);
				 	}
				 	
				},
				error : function(e) {
					$.Loading.hide();
					alert(msg6);
 				}
 		};
 
		$("#sortForm").ajaxSubmit(options);				
	}
};