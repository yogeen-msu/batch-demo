<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<style>
.input table th{
width:80px
}
</style>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" /> > <fmt:message key="syssetting.toolman" /> > <fmt:message key="syssetting.toolman.urlmapping" /></div>
<div class="grid">

	<div class="toolbar" >
		<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="syssetting.toolman.urlmapping" /></h4>
		<div class="active">
			<h4>
				<span>
					<input type="button" id="addBtn" value=<fmt:message key="common.btn.add" /> class="search_but" />
				  <!--  	<input type="button" id="saveBtn" value=<fmt:message key="common.list.mod" /> class="search_but" /> -->
				</span>
				&nbsp;
			</h4>
		</div>
	</div>
	
	
	<form method="POST" id="editform">
	
	<grid:grid  from="uriList">
	
		<grid:header>
			<grid:cell width="50">ID</grid:cell> 
			<grid:cell width="250">URL</grid:cell> 
			<grid:cell width="200">Path</grid:cell> 
			<grid:cell width="150"><fmt:message key="syssetting.toolman.pagename" /></grid:cell>
<!--			<grid:cell width="100">积分</grid:cell>-->
			<grid:cell ><fmt:message key="syssetting.toolman.cache" /></grid:cell>
			 <grid:cell><fmt:message key="common.list.operation" /></grid:cell>
		</grid:header>
		
	
	  <grid:body item="uri">
	        <grid:cell>${uri.id}<input type="hidden" value="${uri.id}"  name="ids" /> </grid:cell>
	        <grid:cell><input type="text" name="uri" value="${uri.uri }" style="width:200px" /> </grid:cell>
	        <grid:cell><input type="text" name="path" value="${uri.path}" style="width:150px" /> </grid:cell> 
	        <grid:cell><input type="text" name="pagename" value="${uri.pagename}" style="width:150px" /></grid:cell> 
<!--	        <grid:cell><input type="text" name="point" value="${uri.point}" style="width:50px" /></grid:cell>-->
	        <grid:cell>
	        <input type="radio" name="httpcache_${uri.id}" value="1" <c:if test="${uri.httpcache==1}" >checked="checked"</c:if> /><fmt:message key="common.yes" />&nbsp;&nbsp;
	        <input type="radio" name="httpcache_${uri.id}" value="0" <c:if test="${uri.httpcache==0}" >checked</c:if> /><fmt:message key="common.no" />
	         </grid:cell>
	        
	        <grid:cell>
		        <a href="javascript:;" uriid="${uri.id }" ><span class="delete"><fmt:message key="common.list.del" /></span></a>
		        &nbsp;&nbsp;
		        <a href="javascript:;" ><span class="modify" uriid="${uri.id }" ><fmt:message key="common.list.mod" /></span></a>
	        </grid:cell>
	  </grid:body> 
	  
	</grid:grid>  
	</form>	 
	<div style="clear:both;padding-top:5px;"></div>
</div>

<div id="adddlg">
</div>
<div id="editdlg">
</div>

<script type="text/javascript">
var ThemeUri = {
	init:function(){
		var self  = this;
		var addNewUri = '<fmt:message key="syssetting.toolman.addmapping" />';
		var updateUri = '<fmt:message key="syssetting.toolman.modmapping" />';
		Cop.Dialog.init({id:"adddlg",modal:false,title:addNewUri,width:500});
		Cop.Dialog.init({id:"editdlg",modal:false,title:updateUri,width:500});
		
		$("#addBtn").click(function(){
			$("#adddlg").load("themeUri!add.do?ajax=yes",function(){
				$("#adddlg .submitBtn").click(function(){
					self.add();
				});		
			});
			Cop.Dialog.open("adddlg");
		});

		$(".modify").click(function(){
			var id  = $(this).attr("uriid");
			$("#editdlg").load("themeUri!edit.do?ajax=yes&id="+id,function(){
				$("#editdlg .submitBtn").click(function(){
					self.edit();
				});		
			});
			Cop.Dialog.open("editdlg");			
		});


		$("#saveBtn").click(function(){
			self.save();
		});
		$(".delete").parent().click(function(){
			
			var removeTip = '<fmt:message key="syssetting.toolman.urimapping.removecheck" />';
			if( !confirm(removeTip) ){
				return ;
			}
			
			var id = $(this).attr("uriid");
			//$.Loading.show('正在删除，请稍侯...');
			$.Loading.show('<fmt:message key="syssetting.toolman.urimapping.removing" />');
			$.ajax({
				url:'themeUri!delete.do?id='+id+'&ajax=yes',
				type:'get',
				dataType:'json',
				success:function(result){
					$.Loading.hide();
					if(result.result==1){
						//alert("删除成功");
						alert('<fmt:message key="market.promotion.delSuccess" />');
						
						location.reload();
					}else{
						alert(result.message);
					}						
				},
				error:function(){
					$.Loading.hide();
					alert("error");
				}
				
			});
		});
	},
	add:function(){
		//$.Loading.show('正在添加，请稍侯...');
		$.Loading.show('<fmt:message key="syssetting.toolman.urimapping.adding" />');
		var options = {
				url : "themeUri!saveAdd.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {	
					Cop.Dialog.close("adddlg");
					$.Loading.hide();
					//alert("添加成功");
					alert('<fmt:message key="market.mainsale.message3" />');
					if(result.result==1){
						location.reload();
					}else{
						alert(result.message);
					}
				},
				error : function(e) {
					$.Loading.hide();
					alert("error");
				}
			};

			$('#adddlg form').ajaxSubmit(options);			
	},
	save:function(){
		if( !this.checkEdit() ){
			return ;
		}
		//$.Loading.show('正在保存，请稍侯...');
		$.Loading.show('<fmt:message key="syssetting.toolman.urimapping.saving" />');
		var options = {
				url : "themeUri!batchEdit.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {	
					$.Loading.hide();
					//alert("保存成功");
					alert('<fmt:message key="syssetting.toolman.urimapping.savesuccess" />');
					
					if(result.result==1){
						location.reload();
					}else{
						alert(result.message);
					}
				},
				error : function(e) {
					$.Loading.hide();
					alert("error");
				}
			};

			$('#editform').ajaxSubmit(options);
	},
	edit:function(){
		//$.Loading.show('正在修改，请稍侯...');
		$.Loading.show('<fmt:message key="syssetting.toolman.urimapping.updating" />');
		var options = {
				url : "themeUri!saveEdit.do?ajax=yes",
				type : "POST",
				dataType : 'json',
				success : function(result) {	
					Cop.Dialog.close("editdlg");
					$.Loading.hide();
					
					if(result.result==1){
						//alert("修改成功");
						alert('<fmt:message key="market.mainsale.message4" />');
						
						location.reload();
					}else{
						alert(result.message);
					}
				},
				error : function(e) {
					$.Loading.hide();
					alert("error");
				}
			};

			$('#editdlg form').ajaxSubmit(options);			
	},
	checkEdit:function(){
		var result = true;
		$("[name='uri']").each(function(){
			if( $.trim( $(this).val() )=='' ){
				result = false;
			}
		});
		var notempty = '<fmt:message key="common.notempty" />'; //不能为空
		if(!result){ alert("uri"+notempty); return result; }	 

		$("[name='path']").each(function(){
			if( $.trim( $(this).val() )=='' ){
				result = false;
			}
		});
		if(!result){ alert("path"+notempty); return result; }

		$("[name='pagename']").each(function(){
			if( $.trim( $(this).val() )=='' ){
				result = false;
			}
		});
		var tip = '<fmt:message key="syssetting.toolman.pagename" />'+notempty;
		if(!result){ alert(tip); return result; }
		
		$("[name='point']").each(function(){
			var point  = $.trim( $(this).val() );
			
			if( point=='' || parseInt(point) != point ){
				result = false;
			}
		});
		tip = '<fmt:message key="syssetting.toolman.urimapping.pointempty" />';//积分不能为空，且必须是数字
		if(!result){ alert(tip); return result; }		

		return true;	
	}
		
};
$(function(){
	ThemeUri.init();
});
</script>
