<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>	
<div id="addcontent">
<div class="input">
<form >
<input type="hidden" name="themeUri.id" value="${ themeUri.id}" />
<table cellspacing="1" cellpadding="3" width="100%" class="form-table">
	<tr>
		<th><label class="text">URL:</label></th>
		<td>
		<input type="text" name="themeUri.uri" value="${ themeUri.uri}"  dataType="string" isrequired="true" />
		</td>
	</tr>
	<tr>
		<th><label class="text"><fmt:message key="syssetting.toolman.modelfile" />:</label></th>
		<td><input type="text" name="themeUri.path" value="${themeUri.path }" dataType="string" isrequired="true"/> </td>
	</tr>
	<tr>
		<th><label class="text"><fmt:message key="syssetting.toolman.httpcache"/>: </label></th>
		<td>
		<input type="radio" name="themeUri.httpcache" value="1"  <c:if test="${themeUri.httpcache==1}" >checked="true"</c:if> ><fmt:message key="common.yes" />&nbsp;&nbsp;
		<input type="radio" name="themeUri.httpcache" value="0"  <c:if test="${themeUri.httpcache==null || themeUri.httpcache==0}" >checked="true"</c:if> ><fmt:message key="common.no" />
		</td>
	</tr>	
	<tr>
		<th><label class="text"><fmt:message key="syssetting.toolman.pagename" />:</label></th>
		<td><input type="text" name="themeUri.pagename"  value="${ themeUri.pagename}"  dataType="string" isrequired="true"/> 
		</td>
	</tr>
	<!-- 
	<tr>
		<th><label class="text">积分</label></th>
		<td><input type="text" name="themeUri.point" value="${themeUri.point }"  dataType="string" required="int" value="0"/> </td>
	</tr>
	 -->
	<tr>
		<th><label class="text"><fmt:message key="syssetting.toolman.keywords" />:</label></th>
		<td><input type="text" name="themeUri.keywords" value="${ themeUri.keywords}"   dataType="string" isrequired="true"/> 
		</td>
	</tr>
	<tr>
		<th><label class="text"><fmt:message key="syssetting.otherconfig.smsconfigdesc"/>:</label></th>
		<td> 
			<textarea name="themeUri.description"  style="width:250px;height:100px"  >${themeUri.description }</textarea>
		</td>
	</tr>	
</table>
</form>
</div>

</div>

<div class="footContent" >
<div style="width: 200px; height: 40px; margin: 0pt auto;"
	class="mainFoot">
<table style="margin: 0pt auto; width: auto;">
	<tbody>
		<tr>
			<td><b class="save">
			<button class="submitBtn" ><fmt:message key="produdt.js.save"></fmt:message></button>
			</b></td>
		</tr>
	</tbody>
</table>
</div>
</div> 
