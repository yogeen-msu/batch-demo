<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<style>
.code_img{
height:22px;
vertical-align:middle;
width:60px;
}
</style>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="menu.system.setting" />  > <fmt:message key="syssetting.cmsmenu.setting" /> > <fmt:message key="syssetting.cmsmenu.initdata" /> </div>

<div class="input" >
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="syssetting.cmsmenu.initdata" />  </h4>
	<br/>
	<div style="color:red">&nbsp;&nbsp;&nbsp;<fmt:message key="syssetting.cmsmenu.initdata.tip" /></div>
	<br/>
   <table cellspacing="1" cellpadding="3" width="100%" class="form-table" >
     <tr> 
       <th ><img class="code_img" id="code_img" src="../../../validcode.do?vtype=initdata"></th>
       <td><input type="text" name="vcode" id="vcode" style="width:80px" />&nbsp;&nbsp;<fmt:message key="syssetting.cmsmenu.initdata.inputtext" />
	   </td>
     </tr>
   
   </table>
</div>
<div class="submitlist" align="center">
	 <table>
	 <tr><td >
	       <input name="button" id="initBtn" type="button"  value=<fmt:message key="syssetting.cmsmenu.initdata.btn" /> class="submitBtn"/>
	   </td>
	   </tr>
	 </table>
	</div> 
<script>
function doInit(){
	var vcode  = $("#vcode").val();
	if($.trim(vcode)==''){
		
		alert('<fmt:message key="syssetting.cmsmenu.initdata.inittip" />');
		//alert("请输入验证码以确认初始化");
		return ;
	}	
	
	//$.Loading.show('正在初始化，请稍侯...'); 
	$.Loading.show('<fmt:message key="syssetting.cmsmenu.initdata.running" />'); 
	$.ajax({
		url:'userSite!initData.do?ajax=yes',
		data:'vcode='+vcode,
		type:'POST',
		dataType:'json',
		success:function(result){
			$.Loading.hide();
			if(result.result==1){
				//alert("初始化成功，请重新启动以便请除缓存！");
				alert('<fmt:message key="syssetting.cmsmenu.initdata.success" />');
			}else{
				alert(result.message);
			}
		},
		error:function(e){
			$.Loading.hide();
			alert("error:"+e);
		}
	});
	
}
$(function(){
	$("#initBtn").click(function(){
		doInit();
	});
})
</script>