<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>

<script type="text/javascript" src="../js/DomainOperator.js">
</script>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<div class="input">
<form  class="validate"  action="userSite!editSave.do" method="post" name="theForm" id="theForm" enctype="multipart/form-data">
<div class="tableform">
<h4>网站基本设置</h4>
<div style="position: static;" class="division">
<table cellspacing="0" cellpadding="0" border="0" width="100%"
	class="shop-setting">
	<tbody>
		<tr>
			<th>网站名称:<input type="hidden" name="copSite.id"  value="${copSite.id }" /></th>
			<td>
			<input type="text" name="copSite.sitename" 
			value="${copSite.sitename }" dataType="string" isrequired="true" style="width:325px"/></td>
		</tr>
		<tr>
			<th>网站标题:</th>
			<td>
			<input type="text" name="copSite.title" 
			value="${copSite.title }" dataType="string" isrequired="true" style="width:325px"/></td>
		</tr>
		<tr>
			<th><label class="text">META_KEYWORDS:<br/>
				(页面关键词):</label></th>
			<td>
			<input type="text" name="copSite.keywords" 
			value="${copSite.keywords }" style="width:325px"/></td>
		</tr>
		<tr>
			<th><label class="text">META_DESCRIPTION:<br/>
					(页面描述)</label></th>
			<td>
			<textarea name="copSite.descript" style="width:405px"/>${copSite.descript }</textarea></td>
		</tr>
		<tr>
			<th>版权声明:</th>
			<td>
			<textarea name="copSite.copyright" style="width:405px"/><c:out value="${copSite.copyright }" escapeXml="true"/></textarea></td>
		</tr>
	</tbody>
</table>
</div>
<h4>网站Logo设置</h4>
<div style="position: static;" class="division">
<table cellspacing="0" cellpadding="0" border="0" width="100%"
	class="shop-setting">
	<tbody>
		<tr>
			<th>网站前台Logo:</th>
			<td width=250>
			<div id="sitelogo" style="float: none; position: static;">
			<c:if test="${copSite.logofile!=null }">
			<img style="float: none; position: static;" src="${copSite.logofile }" />&nbsp;
			</c:if>
			<div></div>
			<input type="file" name="cologo" id="cologo" size="10" style="width:145px"/> </div>
			</td>
			<th>网站后台Logo:</th>
			<td width=250>
			<div id="sitelogo" style="float: none; position: static;">
			<c:if test="${copSite.bklogofile!=null }">
			<img style="float: none; position: static;" src="${copSite.bklogofile }" />&nbsp;
			</c:if>
			<div></div>
			<input type="file" name="bklogo" id="bklogo" size="10" style="width:145px"/> </div>
			</td>
		</tr>
		<tr>
			<th>网站登录界面Logo:</th>
			<td width=250>
			<div id="sitelogo" style="float: none; position: static;">
			<c:if test="${copSite.bkloginpicfile!=null }">
			<img style="float: none; position: static;" src="${copSite.bkloginpicfile }" />&nbsp;
			</c:if>
			<div></div>
			<input type="file" name="bkloginpic" id="bkloginpic" size="10" style="width:145px"/> </div>
			</td>
			<th>网站ico文件:</th>
			<td width=250>
			<div id="sitelogo" style="float: none; position: static;">
			<c:if test="${copSite.icofile!=null }">
			<img style="float: none; position: static;" src="${copSite.icofile }" />&nbsp;
			</c:if>
			<div></div>
			<input type="file" name="ico" id="ico" size="10" style="width:145px"/> </div>
			</td>
		</tr>
	</tbody>
</table>
</div>
<table cellspacing="0" cellpadding="0" border="0"
	class="shop-setting">
	<tr>
		<td width="400px"><h4>站长信息&nbsp;&nbsp;<span class="help_icon" helpid="site_userinfo"></span></h4></td>
		<td><h4>在线客服&nbsp;&nbsp;<span class="help_icon" helpid="site_state"></span></h4></td>
	</tr>	
</table>
<div class="division" style="height: 207px">
<table cellspacing="0" cellpadding="0" border="0"
	class="shop-setting">
	<tbody>
		<tr>
			<th style="width: 100px">站长姓名:</th>
			<td style="width:230px">
			<input type="text" name="copSite.username" 
			value="${copSite.username }" dataType="string" isrequired="true" style="width: 133px"></td>
			<th style="width: 102px">是否开启:</th>
			<td>
			<input type="radio" name="copSite.state" namespace="state" id="copsitekfon0"
				default="1" <c:if test="${copSite.state==1 }">checked="checked"</c:if> value="1"><label
				for="copsitekfon0">是</label>&nbsp;<input type="radio"
				name="copSite.state" 
				 namespace="state" value="0" <c:if test="${copSite.state==0 }">checked="checked"</c:if> id="copsitekfon1"><label
				for="copsitekfon1">否</label></td>
		</tr>
		<tr>
			<th style="width: 100px">性别:</th>
			<td style="width:230px"><input type="radio" name="copSite.usersex" namespace="usersex" id="copsiteusersex0"
				default="0" <c:if test="${copSite.usersex==0 }">checked="checked"</c:if> value="0"><label
				for="copsiteusersex0">男</label>&nbsp;<input type="radio"
				name="copSite.usersex" id="copsiteusersex1"
				 namespace="usersex" value="1" <c:if test="${copSite.usersex!=0 }">checked="checked"</c:if> ><label
				for="copsiteusersex1">女</label></td>
			<th style="width: 102px;">QQ:</th>
			<td>
			<input style="float:left" type="checkbox" value="1" name="copSite.qq" <c:if test="${copSite.qq == 1 }">checked</c:if> />&nbsp;&nbsp;
			<input type="text" vtype="text"
				name="copSite.qqlist" value="${copSite.qqlist }" style="width: 143px;">&nbsp;&nbsp;<span class="help_icon" helpid="qq_format"></span></td>
		</tr>

		<tr>
			<th style="width: 100px">电话:</th>
			<td style="width:230px">
			<input type="text" vtype="text"
				name="copSite.usertel" value="${copSite.usertel }" style="width: 133px">  
			</td>
			<th style="width: 102px">MSN:</th>
			<td>
			<input style="float:left" type="checkbox" value="1" name="copSite.msn" <c:if test="${copSite.msn == 1 }">checked</c:if> />&nbsp;&nbsp;
			<input type="text" vtype="text"
				name="copSite.msnlist" value="${copSite.msnlist }" style="width: 143px;">&nbsp;&nbsp;<span class="help_icon" helpid="msn_format"></span></td>
		</tr>
		<tr>
			<th style="width: 100px">手机:</th>
			<td style="width:230px">
			<input type="text" vtype="text"
				name="copSite.usermobile" value="${copSite.usermobile }" style="width: 133px"></td>
			<th style="width: 102px">旺旺:</th>
			<td>
			<input style="float:left" type="checkbox" value="1" name="copSite.ww" <c:if test="${copSite.ww == 1 }">checked</c:if> />&nbsp;&nbsp;
			<input type="text" vtype="text"
				name="copSite.wwlist" value="${copSite.wwlist }" style="width: 143px;">&nbsp;&nbsp; <span class="help_icon" helpid="ww_format"></span> 
			</td>
		</tr>
		<tr>
			<th style="width: 100px">其它电话:</th>
			<td style="width:230px">
			<input type="text" vtype="text"
				name="copSite.usertel1" value="${copSite.usertel1 }" style="width: 133px"></td>
			<th style="width: 102px">电话:</th>
			<td>
			<input type="checkbox" value="1" name="copSite.tel" <c:if test="${copSite.tel == 1 }">checked</c:if> />&nbsp;&nbsp;
			<input type="text" vtype="text"
				name="copSite.tellist" value="${copSite.tellist }" style="width: 163px"></td>
		</tr>
		<tr>
			<th style="width: 100px">电子邮件:</th>
			<td style="width:230px">
			<input type="text" vtype="text" isrequired="true" datatype="email"
				name="copSite.useremail" value="${copSite.useremail }" style="width: 133px"></td>
			<th style="width: 102px">营业时间:</th>
			<td>
			<input type="checkbox" value="1" name="copSite.wt" <c:if test="${copSite.wt == 1 }">checked</c:if> />&nbsp;&nbsp;
			<input type="text" vtype="text"
				name="copSite.worktime" value="${copSite.worktime }" style="width: 163px"></td>
		</tr>

	</tbody>
</table>
	

</div>
<h4>其它设置</h4>
<div class="division">
<table cellspacing="0" cellpadding="0" border="0" width="100%"
	class="shop-setting">
	<tbody>
		<tr>
			<th>备案号:</th>
			<td><input type="text" vtype="text"
				name="copSite.icp" value="${copSite.icp }" style="width: 176px"><span class="notice-inline">此处填写您在工信部备案管理网站申请的备案编号，详请登录<a
				target="_blank" href="http://www.miibeian.gov.cn">官方网站</a></span></td>
		</tr>
		<tr>
			<th>详细地址:</th>
			<td><span id="el-e927a-51407"><input type="text" vtype="text" isrequired="true" dataType="string"
				name="copSite.address" value="${copSite.address }" style="width: 519px"></span></td>
		</tr>
		<tr>
			<th style="height: 34px">邮政编码:</th>
			<td style="height: 34px">
			<input type="text" vtype="text" isrequired="true" dataType="zipcode"
				name="copSite.zipcode" id="el-e927a-babcf4" value="${copSite.zipcode }"
				autocomplete="off"></td>
		</tr>
		<tr>
			<th style="height: 34px">联系人:</th>
			<td style="height: 34px">
			<input type="text" vtype="text" isrequired="true" dataType="string"
				name="copSite.linkman" id="el-e927a-babcf4" value="${copSite.linkman }"
				autocomplete="off"></td>
		</tr>
		<tr>
			<th style="height: 34px">联系电话:</th>
			<td style="height: 34px">
			<input type="text" vtype="text" isrequired="true" dataType="string"
				name="copSite.linktel" value="${copSite.linktel }"
				autocomplete="off"></td>
		</tr>
		<tr>
			<th style="height: 34px">电子邮件:</th>
			<td style="height: 34px">
			<input type="text" vtype="text" isrequired="true" dataType="email"
				name="copSite.email" value="${copSite.email }"
				autocomplete="off"></td>
		</tr>
		<tr>
			<th>开启站点:</th>
			<td>
			<input type="radio" name="copSite.siteon" namespace="siteon" id="copsiteon0"
				default="0" <c:if test="${copSite.siteon==0 }">checked="checked"</c:if> value="0"><label
				for="copsiteon0">是</label>&nbsp;<input type="radio"
				name="copSite.siteon" id="copsiteon1"
				 namespace="siteon" value="1" <c:if test="${copSite.siteon!=0 }">checked="checked"</c:if> ><label
				for="copsiteon1">否</label></td>
		</tr>
		<tr>
			<th>关闭原因:</th>
			<td>
			<textarea style="width:519px;height:200px" name="copSite.closereson">${copSite.closereson }</textarea>
			</td>
		</tr>		
	</tbody>
</table>
</div>
<div class="submitlist" align="center">
 <table>
 <tr><td >
  <input name="submit" type="submit"	  value="保存设置" class="submitBtn" />
   </td>
   </tr>
 </table>
</div>
</div>
</form>
</div>




<script type="text/javascript">
$("form.validate").validate();
</script>



	


