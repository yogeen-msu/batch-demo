<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
	
<div class="input">
<%@ include file="account_header.jsp" %>
<%@ include file="info_header.jsp" %>
<form method="post" action="userDetail!save.do" name="theForm"
	id="theForm">
<table class="form-table">
	<tr>
		<th> <input type="hidden"
			name="copUserDetail.userid" id="copUserDetail.userid"
			value="${userid }" /><label class="text">经营范围:</label></th>
		<td><input type="text" name="copUserDetail.bussinessscope"
			id="copUserDetail.bussinessscope"
			value="${copUserDetail.bussinessscope }" 
			dataType="string" isrequired="true" style="width:260px;" /></td>
	</tr>
	<tr>
		<th><label class="text">注册地:</label></th>
		<td><input type="text" name="copUserDetail.regaddress"
			id="copUserDetail.regaddress" value="${copUserDetail.regaddress }"
			 dataType="string" isrequired="true" style="width:260px;" /></td>
	</tr>
	<tr>
		<th><label class="text">注册日期:</label></th>
		<td><input type="text" name="copUserDetail.regdate"
			id="copUserDetail.regdate" value="${copUserDetail.regdate }"
			 dataType="string" isrequired="true" style="width:260px;"/></td>
	</tr>
	<tr>
		<th><label class="text">公司规模:</label></th>
		<td><select name="copUserDetail.corpscope"
			id="copUserDetail.corpscope">
			<option value="0"
				<c:if test="${copUserDetail.corpscope==0 }"> selected</c:if> >未知</option>
			<option value="1"
				<c:if test="${copUserDetail.corpscope==1 }"> selected</c:if> >10人以下</option>
			<option value="2"
				<c:if test="${copUserDetail.corpscope==2 }"> selected</c:if> >11-50人</option>
			<option value="3"
				<c:if test="${copUserDetail.corpscope==3 }"> selected</c:if> >51-100人</option>
			<option value="4"
				<c:if test="${copUserDetail.corpscope==4 }"> selected</c:if> >101-500人</option>
			<option value="5"
				<c:if test="${copUserDetail.corpscope==5 }"> selected</c:if> >501-1000人</option>
			<option value="6"
				<c:if test="${copUserDetail.corpscope==6 }"> selected</c:if> >1000人以上</option>
		</select></td>
	</tr>
	<tr>
		<th><label class="text">公司简介:</label></th>
		<td><fck:editor basePath="${ctx}/editor/fckeditor/" startupFocus="false"
							id="copUserDetail.corpdescript" width="98%" height="300" toolbarSet="Default">
						${copUserDetail.corpdescript}</fck:editor></td>
	</tr>
 
</table>
<div class="submitlist" align="center">
 <table>
 <tr><td >
  <input name="submit" type="submit"	  value="确定" class="submitBtn" />
   </td>
   </tr>
 </table>
</div>  
</form>
</div>