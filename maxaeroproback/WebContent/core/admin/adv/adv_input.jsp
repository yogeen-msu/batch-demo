<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp" %>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck" %>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.advertman" /> > <fmt:message key="cententman.advertman.adertlist" />   > <fmt:message key="cententman.advertman.add" /></div>
<form class="validate" method="post" action="adv!addSave.do" name="theForm" id="theForm" enctype="multipart/form-data">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.advertman.add" /> </h4>

	<table class="form-table">
		<tr>
			<th><label class="text"><fmt:message key="cententman.advertman.adertname" />:</label></th>
			<td>
				<input type="text" name="adv.aname" maxlength="60" value=""  dataType="string" isrequired="true" />
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.advertman.adertloction" />:</label></th>
			<td>
				<select name="adv.acid">
		       		<c:forEach var="item" items="${adColumnList }">
		       	  		<option value="${item.acid }">${item.cname }</option>
		       		</c:forEach>
		       </select>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.advertman.issart" /></label></th>
			<td>
				<input type="radio" name="adv.isclose" value="0" checked /><fmt:message key="cententman.advertman.start" />&nbsp;&nbsp; 
				<input type="radio" name="adv.isclose" value="1" /><fmt:message key="cententman.advertman.close" />&nbsp;&nbsp;
				<span class="help_icon" helpid="ad_isclose"></span>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="marketman.promotion.starttime" />:</label></th>
			<td>
				<input type="text" name="mstartdate" maxlength="60" value=""  dataType="date" isrequired="true" class="dateinput"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="marketman.promotion.endtime" />:</label></th>
			<td>
				<input type="text" name="menddate" maxlength="60" value=""  dataType="date" isrequired="true" class="dateinput"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.advertman.link" />:</label></th>
			<td>
				<input type="text" name="adv.url" maxlength="60" value=""  dataType="string"/>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.advertman.uploadadvertfile" />:</label></th>
			<td>
				<input type="file" name="pic" id="pic" size="45"/>&nbsp;&nbsp;<span class="help_icon" helpid="ad_file"></span>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.advertman.unitname" />:</label></th>
			<td>
				<input type="text" name="adv.company" value=""  dataType="string" />&nbsp;&nbsp;<span class="help_icon" helpid="ad_link"></span>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.advertman.linkmen" />:</label></th>
			<td>
				<input type="text" name="adv.linkman" value=""  dataType="string" />&nbsp;&nbsp;<span class="help_icon" helpid="ad_link"></span>
			</td>
		</tr>
		<tr>
			<th><label class="text"><fmt:message key="cententman.advertman.linktype" />:</label></th>
			<td>
				<input type="text" name="adv.contact" value=""  dataType="string" />&nbsp;&nbsp;<span class="help_icon" helpid="ad_link"></span>
			</td>
		</tr>
		
	</table>

</div>
	<div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input name="reset" type="button" onclick="location.href='adv!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>

 <script type="text/javascript">
$("form.validate").validate();
</script>
