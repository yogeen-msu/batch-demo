<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp" %>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.advertman" /> > <fmt:message key="cententman.advertman.adertloction" />   > <fmt:message key="cententman.advertman.add" /></div>
 <form class="validate" method="post" action="adColumn!addSave.do" name="theForm" id="theForm">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.advertman.addlocation" /> </h4>
   <table cellspacing="1" cellpadding="3" width="100%" class="form-table">
     <tr>
       <td  class="label"><label class="text"><fmt:message key="cententman.advertman.advname" />:</label></td>
       <td  ><input type="text" name="adColumn.cname" maxlength="60" value=""  dataType="string" isrequired="true" />&nbsp;&nbsp;<span class="help_icon" helpid="adcolumn_name"></span>       </td>
      </tr>
	 <tr>
       <td  class="label"><label class="text"><fmt:message key="cententman.advertman.type" />:</label></td>
       <td  ><select name="adColumn.atype">
       			<option value=0><fmt:message key="syssetting.cmsmenu.type.image" /></option>
       			<option value=1>Flash</option> 
             </select>      
       </td>
      </tr>      
      <tr>
       <td  class="label"><label class="text"><fmt:message key="cententman.advertman.width" />:</label></td>
       <td  ><input type="text" name="adColumn.width" maxlength="60" value=""  dataType="string" isrequired="true" />&nbsp;&nbsp;<span class="help_icon" helpid="adcolumn_width"></span></td>
      </tr>
      <tr>
       <td  class="label"><label class="text"><fmt:message key="cententman.advertman.height" />:</label></td>
       <td  ><input type="text" name="adColumn.height" maxlength="60" value=""  dataType="string" isrequired="true" />&nbsp;&nbsp;<span class="help_icon" helpid="adcolumn_height"></span></td>
      </tr>
   </table>
 </div>
   <div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input type="button" onclick="location.href='adColumn!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>
 <script type="text/javascript">
$("form.validate").validate();
</script>
