<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp" %>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck" %>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.advertman" /> > <fmt:message key="cententman.advertman.adertlist" />   > <fmt:message key="cententman.advertman.adertedit" /></div>
 <form class="validate" method="post" action="adv!editSave.do" name="theForm" id="theForm" enctype="multipart/form-data">
<div class="input">
	<h4 class="title"><div class="icon">&nbsp;</div>&nbsp;<fmt:message key="cententman.advertman.adertedit" /></h4>

   <table cellspacing="1" cellpadding="3" width="100%" class="form-table">
     <tr>
       <th style="width:140px"><label class="text"><input type="hidden" name="adv.aid" value="${advid }"/><fmt:message key="cententman.advertman.adertname" />:</label></th>
       <td><input type="text" name="adv.aname" value="${adv.aname }" maxlength="60" dataType="string" isrequired="true" />&nbsp;&nbsp;<span class="help_icon" helpid="ad_name"></span>       </td>
      </tr>
      <tr>
       <th style="width:140px"><label class="text"><fmt:message key="cententman.advertman.adertloction" />:</label></th>
       <td>
       <select name="adv.acid">
       	<c:forEach var="item" items="${adColumnList }">
       	  <option value="${item.acid }"  <c:if test="${item.acid == adv.acid }">selected</c:if>>${item.cname }</option>
       	</c:forEach>
       </select>&nbsp;&nbsp;<span class="help_icon" helpid="adc_name"></span>
       </td>
      </tr>
	<tr>
		<th style="width:140px"><label class="text"><fmt:message key="cententman.advertman.issart" /></label></th>
		<td><input type="radio" name="adv.isclose" value="0"
			<c:if test="${adv.isclose == 0 }">checked</c:if> /><fmt:message key="cententman.advertman.start" />&nbsp;&nbsp; <input type="radio"
			name="adv.isclose" value="1" <c:if test="${adv.isclose == 1 }">checked</c:if>/><fmt:message key="cententman.advertman.close" />&nbsp;&nbsp;<span class="help_icon" helpid="ad_isclose"></span></td>
	</tr>
	
	<tr>
       <th style="width:140px"><label class="text"><fmt:message key="marketman.promotion.starttime" />:</label></th>
       <td  ><input type="text" name="mstartdate" value="<html:dateformat pattern="yyyy-MM-dd" time="${adv.begintime }"></html:dateformat>" maxlength="60" dataType="date" isrequired="true" class="dateinput"/></td>
      </tr>
      <tr>
       <th style="width:140px"><label class="text"><fmt:message key="marketman.promotion.endtime" />:</label></th>
       <td  ><input type="text" name="menddate" value="<html:dateformat pattern="yyyy-MM-dd" time="${adv.endtime }"></html:dateformat>" maxlength="60"  dataType="date" isrequired="true" class="dateinput"/></td>
      </tr>
      <tr>
       <th style="width:140px"><label class="text"><fmt:message key="cententman.advertman.link" />:</label></th>
       <td  ><input type="text" name="adv.url" value="${adv.url }" maxlength="60" dataType="string"/></td>
      </tr>
       <tr>
       <th style="width:140px"><label class="text"><fmt:message key="cententman.advertman.uploadadvertfile" />:</label></th>
       <td  ><input type="file" name="pic" id="pic" size="45"/>&nbsp;&nbsp;<span class="help_icon" helpid="ad_file"></span></td>
      </tr>
      
		     <c:if test="${adv.atturl !=null }">
			     <tr>
			       <th style="width:140px">&nbsp;</th>
			       <td> 
			       <img src="${adv.atturl  }"  /></td>
			     </tr>
		     </c:if>      
      <tr>
       <th style="width:140px"><label class="text"><fmt:message key="cententman.advertman.unitname" />:</label></th>
       <td  ><input type="text" name="adv.company" value="${adv.company }" maxlength="60" dataType="string" />&nbsp;&nbsp;<span class="help_icon" helpid="ad_link"></span></td>
      </tr>
      
      <tr>
       <th style="width:140px"><label class="text"><fmt:message key="cententman.advertman.linkmen" />:</label></th>
       <td  ><input type="text" name="adv.linkman" value="${adv.linkman }" maxlength="60" dataType="string" />&nbsp;&nbsp;<span class="help_icon" helpid="ad_link"></span></td>
      </tr>
      
      <tr>
       <th style="width:140px"><label class="text"><fmt:message key="cententman.advertman.linktype" />:</label></th>
       <td  ><input type="text" name="adv.contact" value="${adv.contact }" maxlength="60" dataType="string" />&nbsp;&nbsp;<span class="help_icon" helpid="ad_link"></span></td>
      </tr>
     
 
   </table>
</div>
   <div class="submitlist" align="center">
		<table>
			<tr><td >
				<input name="submit" type="submit"	  value=<fmt:message key="common.btn.finish" /> class="submitBtn" />
				<input type="button" onclick="location.href='adv!list.do'" value=<fmt:message key="common.btn.return" /> class="submitBtn" />
			</td></tr>
		</table>
	</div>
</form>
<script type="text/javascript">
	$("form.validate").validate();
	$(function(){
		if($("img").width()>600){$("img").attr("width","600");}
	});
</script>