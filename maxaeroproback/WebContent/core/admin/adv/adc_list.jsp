<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="js/AdColumn.js"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.advertman" /> > <fmt:message key="cententman.advertman.adertloction" /></div>

<div class="grid">
	<div class="toolbar" >
		<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="cententman.advertman.adertloction" /></h4>
		<div class="active">
			<h4>
				<span>
					<input type="button" onclick="location.href='adColumn!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" />
					<input type="button" id="delBtn" value=<fmt:message key="common.list.del" /> class="search_but" />
				</span>
				&nbsp;
			</h4>
		</div>
	</div>
	
<form method="POST">
<grid:grid  from="webpage">
	<grid:header>
	<grid:cell width="50px"><input type="checkbox" id="toggleChk" /></grid:cell>
	<grid:cell width="250px"><fmt:message key="cententman.messageman.title" /></grid:cell> 
	<grid:cell><fmt:message key="cententman.advertman.type" /></grid:cell>
	<grid:cell ><fmt:message key="cententman.advertman.width" /></grid:cell>
	<grid:cell ><fmt:message key="cententman.advertman.height" /></grid:cell> 
	<grid:cell  width="100px"><fmt:message key="common.list.operation" /></grid:cell> 
	</grid:header>
  <grid:body item="adColumn">
  		<grid:cell><input type="checkbox" name="id" value="${adColumn.acid }" />${adColumn.acid } </grid:cell>
        <grid:cell>${adColumn.cname }</grid:cell>
        <grid:cell><c:if test="${adColumn.atype == 0 }"><fmt:message key="syssetting.cmsmenu.type.image" /></c:if><c:if test="${adColumn.atype == 1 }">Flash</c:if></grid:cell>
        <grid:cell>${adColumn.width} </grid:cell>
        <grid:cell>${adColumn.height }</grid:cell>  
        <grid:cell><a href="adColumn!edit.do?acid=${adColumn.acid}" ><fmt:message key="cententman.advertman.edit" /></grid:cell> 
  </grid:body>
</grid:grid>  
</form>	
</div>