<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/commons/taglibs.jsp"%>
<script type="text/javascript" src="js/Adv.js"></script>
<div class="leg_topri"><fmt:message key="common.nva.currentloaction" />:<fmt:message key="cententman.cms.contentmanager" /> > <fmt:message key="cententman.advertman" /> > <fmt:message key="cententman.advertman.adertlist" /></div>

<form method="get" action="adv!list.do">
<div class="grid">
	<div class="toolbar" >
		<h4 class="sear_tj"><div class="icon">&nbsp;</div><fmt:message key="cententman.advertman.adertlist" /></h4>
		<div class="sear_table">
			<table width="100%">
				<tr height="38">    
					<td width="110px" align="right"><fmt:message key="cententman.advertman.adertname" />:</td>
					<td align="left" style="width:300px;">
						&nbsp;&nbsp;
						<input type="text" name="advname" value="${advname }" />
					</td>
					<td width="50px"></td>
					<td width="120px"><fmt:message key="cententman.advertman.belongadertloction" />:</td>
					<td align="left">
						<select name="acid" id="acid">
							<option value=""><fmt:message key="orderman.list.all" /></option>
							<c:forEach items="${adColumnList}" var="adc">
								<option value="${adc.acid}">${adc.cname }</option>
							</c:forEach>
						</select> 
					</td>
					<td align="center"><input type="submit" value=<fmt:message key="common.btn.search" /> class="search_but" /></td>
				  </tr>
			</table>
		</div>
		<div class="active">
			<h4>
				<span>
					<input type="button" onclick="location.href='adv!add.do'" value=<fmt:message key="common.btn.add" /> class="search_but" />
					<input type="button" id="delBtn" value=<fmt:message key="common.list.del" /> class="search_but" />
				</span>
				<fmt:message key="cententman.advertman.adertlist" />
			</h4>
		</div>
	</div>
	
<grid:grid  from="webpage">
	<grid:header>
	<grid:cell width="50px"><input type="checkbox" id="toggleChk" /></grid:cell>
	<grid:cell width="250px"><fmt:message key="cententman.advertman.adertname" /></grid:cell>
	<grid:cell sort="cname"><fmt:message key="cententman.advertman.belongadertloction" /></grid:cell>
	<grid:cell sort="company"><fmt:message key="cententman.advertman.unitname" /></grid:cell> 
	<grid:cell sort="begintime"><fmt:message key="marketman.promotion.starttime" /></grid:cell>
	<grid:cell sort="endtime"><fmt:message key="marketman.promotion.endtime" /></grid:cell> 
	<grid:cell sort="isclose"><fmt:message key="syssetting.authorityman.state" /></grid:cell>
	<grid:cell sort="clickcount"><fmt:message key="cententman.advertman.clickrate" /></grid:cell>
	<grid:cell  width="100px"><fmt:message key="common.list.operation" /></grid:cell> 
	</grid:header>

  <grid:body item="adv">
  		<grid:cell><input type="checkbox" name="id" value="${adv.aid }" />${adv.aid } </grid:cell>
        <grid:cell>${adv.aname } </grid:cell>
        <grid:cell>${adv.cname } </grid:cell>
        <grid:cell>${adv.company }</grid:cell>
        <grid:cell><html:dateformat pattern="yyyy-MM-dd" time="${adv.begintime }"></html:dateformat> </grid:cell>
        <grid:cell><html:dateformat pattern="yyyy-MM-dd" time="${adv.endtime }"></html:dateformat></grid:cell>
        <grid:cell>
        	<c:if test="${adv.isclose == 0 }"><fmt:message key="marketman.promotion.start" />&nbsp;<input type="button" class="stop" advid="${adv.aid }" value=<fmt:message key="cententman.messageman.stop" /> /></c:if>
        	<c:if test="${adv.isclose == 1 }"><fmt:message key="cententman.messageman.nostart" />&nbsp;<input type="button" class="start" advid="${adv.aid}" value=<fmt:message key="cententman.advertman.start" /> /></c:if>
        </grid:cell> 
        <grid:cell>${adv.clickcount }</grid:cell>  
        <grid:cell>
        	<a href="adv!edit.do?advid=${adv.aid}" ><fmt:message key="cententman.advertman.edit" /></a>  
        </grid:cell> 
  </grid:body>  
  
</grid:grid>  

</div>
</form>	
<script>
$(function(){
	$("#acid").val("${acid}");
});
</script>
