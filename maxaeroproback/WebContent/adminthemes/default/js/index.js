var data2='[{"virtualDirVos":[{"code":"prt201311261342470937","name":"ttt","virtualDirVos":[{"code":"swt201311261351430531","name":"aaaaaaaaa"},{"code":"swt201311261351570421","name":"bbbbbbbbbb","virtualDirVos":[{"code":"swt201311261424460562","name":"1111111","virtualDirVos":[{"code":"swt201311261424460562","name":"2222222"},{"code":"swt201311261424460562","name":"333333","virtualDirVos":[]}]}]}]},{"code":"prt201311261352330562","name":"uuuuuu","virtualDirVos":[{"code":"swt201311261424460562","name":"vvvvv","virtualDirVos":[{"code":"swt201311261424460562","name":"1111111"}]}]},{"code":"prt201311261425210781","name":"wwwww","virtualDirVos":[]}]}]';

/**
 * 后台界面构建js
 * @author kingapex
 */
var BackendUi={
	menu:undefined,
	init:function(menu){
		Cop.AdminUI.init({wrapper:$("#right_content")});
		
		$(".desktop a").click(function(){
			Cop.AdminUI.load($(this));
			return false;
		});
		
		
		this.menu =menu;
		this.autoHeight();
		var self =this;
		$(window).resize(function(){self.autoHeight();});
	},
	disMenu:function(){
		this.disSysMenu();
		this.disAppMenu();
	},
	
	/**
	 * 显示系统菜单
	 */
	disSysMenu:function(){
		var self =this;
		var menu = this.menu;
		$.each(menu.sys,function(k,v){
			var link = self.createLink(v);
			 $("<li/>").appendTo( $(".sysmenu>ul") ).append(link);
			 if(v.target!='_blank'){
				link.click(function(){
						Cop.AdminUI.load($(this));
						return false;
					});
			 }
		});		
	},
	/**
	 * 显示应用菜单
	 */
	disAppMenu:function(){
		var self=this;
		var menu = this.menu;
		var i=0;
		$.each(menu.app,function(k,v){
			if(founder ==1 && (v.id==237 || v.id==244 ||v.id==266)){}else{
				var link ;
				if(v.url.indexOf("Tree") != -1){
					link = $("<a target='"+v.target+"'>" + v.text + "</a>");
				}else{
					
					var children = v.children;
					$.each(children,function(i,c){
						$.each(c.children,function(j,w){
							v.url=w.url;
						});
					});
					
				if(v.url.indexOf("/autelproweb")!=-1){
					v.url="../"+v.url.substring(13);
				}
					
					link = $("<a target='"+v.target+"' href='"+v.url+"' >" + v.text + "</a>");
				}
				$("<li><span></span></li>").appendTo($(".appmenu>ul")).children("span").append(link);
				
				var children = v.children;
				var showTree = false;
				var delId = 0;
				if(v.url.indexOf("Tree") != -1){
					$.each(children,function(i,o){
						if(o.text == '平台管理'){
							showTree = true;
							delId = i;
						}
					});
				}
				
				if(showTree) children.splice(delId,1);
				
				if(v.url.indexOf("Tree") != -1){
					link.click(function(){
						var $this = $(this);
						$.ajax({
							url:"../autel/product/bulidVirtualDirTree.do?ajax=yes",
							type:"post",
							dataType:"json",
							success :function(data){
								var treeJson = eval(data);
								if(treeJson){
									self.disAppProductChildren(children,treeJson,$this);
								}
							}
						});
						$(".appmenu li").find("a").removeClass("mous_on");
						$(this).addClass("mous_on");
					});
				}else{
					link.click(function(){
						if(children)
							self.disAppChildren(children);
						Cop.AdminUI.load($(this));
						$(".appmenu li").removeClass("current");
						$(this).parent().parent().addClass("current");
						$(".appmenu li").find("a").removeClass("mous_on");
						$(this).addClass("mous_on");
						return false;
					});
				}
			

				if(i==0){
					var href= link.attr("href");
					var target=link.attr("target");
					link.attr("href",app_path+"/core/admin/index.do");
					link.removeAttr("target");
					link.click();
					link.attr("href",href);
					link.attr("target",target);
				}
				i++;
			}
		});			
	},
    
	/**
	 * 动态显示产品管理子菜单
	 */
	disAppProductChildren:function(temp,children,event){
		var self= this;
		var leftMenu = $("#left_menu_content");
		leftMenu.empty();
		/**
		if(temp.length>0){
			if(temp.length>1){
			temp.splice(0,1);
			}
			self.disAppChildren(temp);
		}
		**/
		self.disAppChildren(temp);
		
		var menuFolder = $("<div class='lanm'></div>");
		var $divImg = $("<div></div>");
		var $a = $("<a code='' href='../autel/product/search.do' style='color: #B74033;'>" +"&nbsp;"+ children[0].name + "</a>");
		var $img = $("<img name='img1' width='35' height='15' src='../adminthemes/default/images/leg_09.gif'>");
		$divImg.append($img);
		$divImg.append($a);
		var folder = $($divImg);
		$img.click(function(){
			if($(this).attr("name") == "img1"){
				$(this).attr("name","img");
				$(this).attr("src","../adminthemes/default/images/leg_10.gif");
			}else{
				$(this).attr("src","../adminthemes/default/images/leg_09.gif");
				$(this).attr("name","img1");
			}
			$(this).closest("div").next().next().toggle();
		});
		
		
		$a.click(function(){
			Cop.AdminUI.load($(this));
			$(".appmenu li").removeClass("current");
			event.parent().parent().addClass("current");
			return false;
		});
		$a.click();
		menuFolder.append(folder);
		leftMenu.append(menuFolder);
				
		if(children[0].virtualDirVos != null && children[0].virtualDirVos.length > 0)
		{
			self.disDirChildren(null,menuFolder,children[0].virtualDirVos);
		}
	
	},
	/**
	 * 显示虚拟目录的子菜单
	 */
	disDirChildren:function($dtImg,$dt,children){
		var self= this;
		if(children){			
			var $dl = $("<dl></dl>");
			$dt.append($dl);
			$.each(children,function(ks,t){
				var softWareTypelink = self.createProductLink(t,"../autel/product/toSoftwareType.do?productSoftwareConfig.parentCode=" +t.code);
				var $dtImg2 = $("<img name='m1' width='9' height='15' src='../adminthemes/default/images/leg_08.gif'>");
				
				$dtImg2.click(function(){
					if($(this).attr("name") == "m1"){
						$(this).attr("src","../adminthemes/default/images/leg_11.gif");
						$(this).attr("name","m");
					}else{
						$(this).attr("src","../adminthemes/default/images/leg_08.gif");
						$(this).attr("name","m1");
					}
					$(this).next().next().toggle();
				});
				
				if(ks < children.length - 1){
					var $dt2 = $("<dt></dt>").append($dtImg2).append(softWareTypelink);
				}else{
					var $dt2 = $("<dd></dd>").append($dtImg2).append(softWareTypelink);
				}
				
				$dl.append($dt2);
				
				if(t.virtualDirVos != null && t.virtualDirVos.length > 0)
				{
					self.disDirChildren($dtImg2,$dt2,t.virtualDirVos);
				}
				
				softWareTypelink.click(function(){
					Cop.AdminUI.load($(this));
					$("#left_menu_content").find(".left_on").removeClass("left_on");
					$(this).addClass("left_on");
					return false;
				});
			});
			//默认树折叠
			if(children.length >0 && $dtImg != null){
				$dtImg.click();
			}
		}
	},
	/**
	 * 显示应用的子菜单
	 */
	disAppChildren:function(children){
		var self= this;
		var leftMenu = $("#left_menu_content");
		leftMenu.empty();
		$.each(children,function(k,v){
			var menuFolder = $("<div class='lanm'></div>");
			var $divImg = $("<div class='folder' >"+this.text+"</div>");
			var folder = $($divImg);
			
			$divImg.click(function(){
				if($(this).attr("class") == "folder"){
					$(this).attr("class","folder1");
				}else{
					$(this).attr("class","folder");
				}
				$(this).next().toggle();
			});
			
			var menuItem = $(" <dl></dl>");
			menuFolder.append(folder);
			menuFolder.append(menuItem);
			leftMenu.append(menuFolder);
			if(this.children){
				$.each(this.children,function(k,v){
					var link = self.createLink(v);
					if(k < this.children.length){
						menuItem.append($("<dt></dt>").append(link));
					}else{
						menuItem.append($("<dd></dd>").append(link));
					}
					
					link.click(function(){
						Cop.AdminUI.load($(this));
						$("#left_menu_content").find(".left_on").removeClass("left_on");
						$(this).addClass("left_on");
						return false;
					});
				});
			}
			
			//默认树折叠
			$divImg.click();
			
		});
		
	},
	createProductLink:function(v,url){
		var link = $("<a href='"+ url +"' code='"+v.code+"'>" + v.name + "</a>");
		return link;
	},
	createLink:function(v){
		if(v.url.indexOf("/autelproweb")!=-1){
			v.url="../"+v.url.substring(13);
		}
		var link = $("<a  target='"+v.target+"' href='"+ v.url +"' >" + v.text + "</a>");
		return link;
	},
	autoHeight:function(){
		var height= $(window).height();
		$("#leftMenus").height(height-100);
		$("#right_content").height(height-50);
	}

};

$(function(){
	BackendUi.init(menu);
	BackendUi.disMenu();
});

function adminUiLoad(event){
	Cop.AdminUI.load($(event));
	return false;
}


