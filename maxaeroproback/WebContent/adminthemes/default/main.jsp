<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<%@ include file="/commons/taglibs.jsp"%>
<title><fmt:message key="${title}" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="image/x-icon" href="${ico}" rel="icon" />
<link type="image/x-icon" href="${ico}" rel="bookmark" />
<script type="text/javascript" src="menu.do"></script>
<script>
var founder= ${user.founder};
</script>
<script type="text/javascript" src="${staticserver }/js/language.js"></script>
<script type="text/javascript" src="${staticserver }/js/common/common.js"></script>
<script type="text/javascript" src="${staticserver }/js/admin/jquery.timers-1.2.js"></script>
<%-- <script type="text/javascript" src="${staticserver }/js/admin/short-msg.js"></script> --%>
<script type="text/javascript" src="${staticserver }/js/admin/cop-min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/editor/ckeditor362/ckeditor.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/editor/ckeditor362/adapters/jquery.js"></script>
<link href="${context}/css/style.css" rel="stylesheet" type="text/css" />
<link href="${context}/css/global.css" rel="stylesheet" type="text/css" />
<link href="${context}/css/grid.css" rel="stylesheet" type="text/css" />
<link href="${context}/css/input.css" rel="stylesheet" type="text/css" />
<link href="${context}/css/validate.css" rel="stylesheet" type="text/css" />
<link href="${context}/css/dialog.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${context}/js/index.js"></script>
</head>
<body>
	<div id="head">
		<div class="top clearfix">
	    	<div class="toplogo"><img src="${context}/images/logo.png"  width="242" height="78" class="IE6png" /><fmt:message key="common.info.title" /></div>
	        <div class="amo">
	        	<ul class="clearfix">
	            	<li style="width:67px;"><img src="${context}/images/out.png" width="15" height="15" /><a href="../autel-cqc/logout.do"  target="_top"><fmt:message key="common.info.outsys" /></a></li>
	            	<!-- <li style="width:83px;"><img src="${context}/images/zx.png" width="15" height="15" /><a href="#">系统帮助</a></li>
	                <li style="width:83px;"><img src="${context}/images/zx.png" width="15" height="15" /><a href="/autelproweb/admin/logout.do" target="_top">系统注销</a></li>
	                 -->
	            	<li style="width:150px;"><fmt:message key="common.info.welcome" />: ${user.username }</li>                                         
	            </ul>
	      	</div>
	    </div>
	    <!--
	   <div class="sysmenu">
	    	<ul class="clearfix">
	    		<li><a href='javascript:;' id='cache_btn'></a></li>
	        </ul>
    	</div>
    	--><div class="appmenu">
			<ul>
			</ul>
		</div>
	</div>
	
	<div id="leftMenus" >
		<div class="<fmt:message key="site.left.style" />"></div>
		<div id="left_menu_content" class="left_menu">
        </div>
	</div>
	<div id="right_content" ></div>
</body>
</html>