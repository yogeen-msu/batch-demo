<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/commons/taglibs.jsp"%>
<title><fmt:message key="${title}" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="image/x-icon" href="${ico}" rel="icon" />
<link type="image/x-icon" href="${ico}" rel="bookmark" />
<script type="text/javascript" src="${staticserver }/js/common/jquery-1.3.2.js"></script>
<SCRIPT  src="${staticserver }/js/common/jquery-form-2.33.js" type="text/javascript"></SCRIPT>
<script type="text/javascript" src="${staticserver }/js/admin/Cop.SSO.js.jsp"></script>
<link href="${context}/css/css.css" rel="stylesheet" type="text/css" />

</head>
<body class="body_all">
<form>
	<div class="login">
    	<h4><fmt:message key="common.info.title" /></h4>
        <table width="505" border="0">
  <tr height="36">
    <td width="158" align="right"><strong><fmt:message key="login.username" />:</strong>&nbsp;&nbsp;</td>
    <td><input name="username" id="username" type="text" class="input1"></td>
  </tr>
  <tr height="36">
    <td align="right"><strong><fmt:message key="login.password" />:</strong>&nbsp;&nbsp;</td>
    <td><input name="password" id="password" type="password" class="input1"/></td>
  </tr>
  <tr height="36">
    <td align="right"><strong><fmt:message key="login.captch" />:</strong>&nbsp;&nbsp;</td>
    <td><input name="valid_code" id="valid_code" type="text" class="input2" />&nbsp;&nbsp;&nbsp;&nbsp;<img id="code_img" class="remember_login_name" width="69" height="27" />&nbsp;&nbsp;&nbsp;<span class="try"><a onclick="javascript:buildRandom();"><fmt:message key="accountinformation.user.regclickchange"/></a></span></td>
  </tr>
  <tr height="36">
    <td>&nbsp;</td>
    <td><input name="login_btn" id="login_btn" <fmt:message key="accountinformation.login.name" /> type="button" class="butt1" value="Login in" /></td>
  </tr>
</table>

    </div>
    </form>
</body>
<script>
$(function(){
	var bkloginpicfile = '${bkloginpicfile}';
	if(bkloginpicfile!=''){
		$(".logo").css("background","transparent url(${bkloginpicfile}) no-repeat scroll 0 0");
	}
	if($("#username").val()){$("#psdinput").focus();}
});

function buildRandom()
{
	$("#code_img").attr("src","../validcode.do?vtype=admin&rmd="+new Date().getTime() );
}
</script>

</html>
