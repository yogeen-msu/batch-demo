package com.cheriscon.surface.usercenter.component;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

/**
 * 客诉信息控制器实现类-平板端
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Component("surfaceComplaintWidget")
public class SurfaceComplaintWidget  extends RequestParamWidget
{
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;
	
	@Resource 
	private IProductInfoService productInfoService;
	
	
	@Resource
	private ICustomerInfoService customerInfoService;
	
	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		
		String operationType = params.get("operationType");
		
		if (operationType == null) 
		{
			return;
		}
		
		if(Integer.parseInt(operationType) == 1)// 查询我的客诉记录信息
		{
			queryComplaintInfoList(params, request);
		} 
	}
	
	/**
	 * 查询我的客诉信息记录
	 * @param params
	 * @param request
	 */
	protected void queryComplaintInfoList(Map<String, String> params,HttpServletRequest request)
	{
		String timeRange = request.getParameter("timeRange");
		String serialNo = request.getParameter("serialNo");
		String usercode="";
		
		
		
		//默认查询最近3个月
		if (timeRange == null || timeRange.equals("")) 
		{
			timeRange = String.valueOf(FrontConstant.LATELY_THREE_MONTH);
		}
		
		//判断该产品序列号是否注册、如没注册必须强制该用户进行注册
		if(productInfoService.getBySerialNo(serialNo) == null)
		{
			//response.sendRedirect(requestUrl);
			return;
		}
		
		if(StringUtil.isEmpty(serialNo))
		{
			serialNo="S/N10000001";
		}
		
		
		try
		{
			
			CustomerInfo customerInfo = customerInfoService.getCustomerAutelIdBySerialNo(serialNo);
			
			if(customerInfo != null)
			{
				usercode = customerInfo.getCode();
			}
			
			List<CustomerComplaintInfo> complaintList = customerComplaintInfoService
							.queryCustomerComplaintInfoList(Integer.parseInt(timeRange), usercode);
			
			this.putData("complaintList", complaintList);
			this.putData("timeRange",timeRange);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
