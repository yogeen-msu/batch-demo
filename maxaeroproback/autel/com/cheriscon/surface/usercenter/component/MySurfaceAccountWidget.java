package com.cheriscon.surface.usercenter.component;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.user.service.ICustomerInfoService;

/**
 * 平板端我的账户控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-5-4
 */
@Component("mySurfaceAccountWidget")
public class MySurfaceAccountWidget extends RequestParamWidget
{
	@Resource
	private ICustomerInfoService customerInfoService;
	
	@Resource 
	private IProductInfoService productInfoService;
	
	
	@Override
	protected void display(Map<String, String> params) 
	{
		String serialNo = params.get("serialNo");
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		HttpServletResponse response  = ThreadContextHolder.getHttpResponse();
		
		//判断该产品序列号是否注册、如没注册必须强制该用户进行注册
		if(productInfoService.getBySerialNo(serialNo) == null)
		{
			//response.sendRedirect(requestUrl);
			return;
		}
		
		if(StringUtil.isEmpty(serialNo))
		{
			serialNo="S/N10000001";
		}
		
		try 
		{
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoBySerialNo(serialNo);
			SessionUtil.setLoginUserInfo(request, customerInfo);
			this.putData("customerInfo", customerInfo);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
