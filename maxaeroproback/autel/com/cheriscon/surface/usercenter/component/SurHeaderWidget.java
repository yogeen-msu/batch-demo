package com.cheriscon.surface.usercenter.component;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.user.service.ICustomerInfoService;

/**
 * 
 * @author yangpinggui
 * @version 创建时间：2013-1-10
 */
@Component("surHeaderWidget")
public class SurHeaderWidget extends RequestParamWidget
{
	@Resource
	private ICustomerInfoService customerInfoService;
	
	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String serialNo = request.getParameter("serialNo");
		
		if(StringUtil.isEmpty(serialNo))
		{
			serialNo="S/N10000001";
		}
		
		try 
		{
			CustomerInfo customerInfo = customerInfoService.getCustomerAutelIdBySerialNo(serialNo);
			
			if(customerInfo != null)
			{
				this.putData("autelId",customerInfo.getAutelId());
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
