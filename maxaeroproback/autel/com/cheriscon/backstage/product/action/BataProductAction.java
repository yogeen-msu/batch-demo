/**
 */
package com.cheriscon.backstage.product.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.product.service.IBataProductService;
import com.cheriscon.backstage.product.vo.BetaProduct;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;

/**
 * @ClassName: BataProductAction
 * @Description: bata产品管理
 * @author chenqichuan
 * @date 2013-9-12 下午02:38:03
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/bataProduct")
public class BataProductAction extends WWAction {

	private static final long serialVersionUID = -8182781630555532764L;

	private String jsonData;
	private String jsonStr;
	private BetaProduct bataProduct;
	private String code;
	private List<ProductForSealer> productTypeList;
	private String saleContractCode; // 销售契约编号
	private String batchSerialNo; // 需要更换契约的bataProduct serialNo

	private SaleContract contract;
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IProductForSealerService productForSealerService;
	@Resource
	private IBataProductService bataProductService;
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private IAdminUserManager adminUserManager;
	@Resource
	private IProductContractLogService productContractService;
	@Resource
	private IProductSoftwareValidStatusService validService;
	@Resource
	private ISaleConfigService saleConfigService;

	// 进入列表页面
	@Action(value = "list", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/bataProduct_list.jsp") })
	public String list() throws Exception {
		this.webpage = bataProductService.pageBataProduct(bataProduct,
				this.getPage(), this.getPageSize());
		return SUCCESS;
	}

	// 进入新增页面
	@Action(value = "add", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/bataProduct_add.jsp") })
	public String add() throws Exception {
		return SUCCESS;
	}

	// 保存bata产品
	@Action(value = "save")
	@SuppressWarnings("unchecked")
	public String save() throws Exception {
		try {
			this.bataProductService.addBataProduct(bataProduct);
			this.msgs.add(getText("syssetting.toolman.urimapping.savesuccess"));
			this.urls.put(getText("market.mainsale.message5"), "list.do");
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "list.do");
		}
		return MESSAGE;
	}

	/**
	 * 获取所有销售契约
	 * 
	 * @return
	 */
	@Action(value = "selectsaleContract", results = { @Result(name = "selectsaleContract", location = "/autel/backstage/market/sale_contract_select_list3.jsp") })
	public String selectsaleContract() {

		try {
			productTypeList = productForSealerService.getProductForSealerList();
			this.webpage = saleContractService.pageSaleContract(contract,
					this.getPage(), 8);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "selectsaleContract";
	}

	// 修改产品契约
	@Action(value = "saveEditContract", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String saveEditContract() {

		List<String> successList = new ArrayList<String>(); // 修改成功的的产品序列号
		List<String> failList = new ArrayList<String>(); // 修改失败的产品序列号
		Map<String,String> map=new HashMap<String,String>();

		try {
			// 修改销售契约
			SaleContract contract = saleContractService
					.getSaleContractByCode(saleContractCode);
			if (contract == null) {
				map.put("result", "0");
				map.put("message", "选择的销售契约错误");
			}
			String[] seriaArr = batchSerialNo.trim().split(",");
			HttpServletRequest request = ThreadContextHolder.getHttpRequest();
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			String ip = FrontConstant.getIpAddr(request);
			String validDate = "";
			for (int i = 0; i < seriaArr.length; i++) {
				String serialNo = seriaArr[i].trim();
				ProductInfo pinfo = productInfoService.getBySerialNo(serialNo);
				// 在这里进行判断，产品是否注册，
				if (pinfo.getRegStatus() != FrontConstant.PRODUCT_REGSTATUS_YES) {
					ProductContractChangeLog log = new ProductContractChangeLog();
					log.setNewContract(saleContractCode);
					log.setOldContract(pinfo.getSaleContractCode());
					log.setOperatorIp(ip);
					log.setOperatorTime(DateUtil.toString(new Date(),
							"yyyy-MM-dd hh:mm:ss"));
					log.setOperatorUser(user.getUsername());
					log.setProductSN(serialNo);
					// 1.保存日志
					productContractService.saveLog(log);
					pinfo.setProTypeCode(contract.getProTypeCode());
					pinfo.setSaleContractCode(contract.getCode());
					pinfo.setSaleContractName(contract.getName());
					try {
						productInfoService.updateProductInfo(pinfo);
						successList.add(serialNo);
					} catch (Exception e) {
						failList.add(serialNo);
					}
				} else {
					List<ProductSoftwareValidStatus> list = validService
							.getProductSoftwareValidStatusList(pinfo.getCode());
					for (int j = 0; j < list.size(); j++) {
						ProductSoftwareValidStatus valid = list.get(j);
						validDate = valid.getValidDate();
						ProductContractChangeLog log = new ProductContractChangeLog();
						log.setNewContract(saleContractCode);
						log.setOldContract(pinfo.getSaleContractCode());
						log.setOldMinSaleUnit(valid.getMinSaleUnitCode());
						log.setOperatorIp(ip);
						log.setOperatorTime(DateUtil.toString(new Date(),
								"yyyy-MM-dd hh:mm:ss"));
						log.setOperatorUser(user.getUsername());
						log.setProductSN(serialNo);
						// 1.保存日志
						productContractService.saveLog(log);
						// 2.删除有效期
						validService.delProductSoftwareValidStatusById(valid
								.getId());
					}
					List<MinSaleUnit> listUnit = saleConfigService
							.queryMinSaleUnits(contract.getSaleCfgCode());
					if (listUnit == null || listUnit.size() == 0) {
						map.put("result", "0");
						map.put("message", "选择的销售契约标准销售配置没有最小销售单位");
					}
					// 更新销售配置
					pinfo.setProTypeCode(contract.getProTypeCode());
					pinfo.setSaleContractCode(saleContractCode);
					productInfoService.updateProductInfo(pinfo);

					// 4.插入新的标准销售配置
					for (int k = 0; k < listUnit.size(); k++) {
						MinSaleUnit unit = listUnit.get(k);
						ProductSoftwareValidStatus valid = new ProductSoftwareValidStatus();
						valid.setMinSaleUnitCode(unit.getCode());
						valid.setProCode(pinfo.getCode());
						valid.setValidDate(validDate);
						validService.saveProductSoftwareValidStatus(valid);
					}
					successList.add(serialNo);
				}
				map.put("result", "1");

			}
		} catch (Exception e) {
			map.put("result", "0");
			map.put("message",  e.getMessage());
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	@Action(value = "checkSerialNo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkSerialNo() throws Exception {
		String serialNo = this.getRequest().getParameter("serialNo");
		Map<String, String> map = new HashMap<String, String>();
		String result = "0"; // 1 表示产品序列号已经存在，2，表示产品序列号不存在，
		bataProduct = bataProductService.getBataProductBySerialNo(serialNo);
		if (null != bataProduct) {
			result = "1";
		}
		if (result.equals("0")) {
			com.cheriscon.common.model.ProductInfo info = productInfoService
					.getBySerialNo(serialNo);
			if (null == info) {
				result = "2";
			} else {
				result = info.getCode();
			}
		}

		map.put("result", result);
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	// 删除bata产品
	@Action(value = "delete")
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			this.bataProductService.deleteBataProduct(code);
			this.msgs.add(getText("common.js.del"));
			this.urls.put(getText("market.mainsale.message5"), "list.do");
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("product.info.usingnotremove"));
			this.urls.put(getText("common.btn.return"), "list.do");
		}
		return MESSAGE;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}

	public BetaProduct getBataProduct() {
		return bataProduct;
	}

	public void setBataProduct(BetaProduct bataProduct) {
		this.bataProduct = bataProduct;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<ProductForSealer> getProductTypeList() {
		return productTypeList;
	}

	public void setProductTypeList(List<ProductForSealer> productTypeList) {
		this.productTypeList = productTypeList;
	}

	public SaleContract getContract() {
		return contract;
	}

	public void setContract(SaleContract contract) {
		this.contract = contract;
	}

	public String getSaleContractCode() {
		return saleContractCode;
	}

	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}

	public String getBatchSerialNo() {
		return batchSerialNo;
	}

	public void setBatchSerialNo(String batchSerialNo) {
		this.batchSerialNo = batchSerialNo;
	}

}
