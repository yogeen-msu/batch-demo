/**
 */
package com.cheriscon.backstage.product.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.product.service.ILanguagePackService;
import com.cheriscon.backstage.product.service.IProductSoftwareConfigService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.service.ISoftwareTypeService;
import com.cheriscon.backstage.product.service.ISoftwareVersionService;
import com.cheriscon.backstage.product.vo.SoftwareVersionPageVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.common.model.ProductSoftwareConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SoftwarePack;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.model.SoftwareVersion;
import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.Theme;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.utils.UploadUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.FileUtil;

/**ge
 * @ClassName: SoftwareVersionAction
 * @Description: 软件版本信息action
 * @author shaohu
 * @date 2013-1-10 下午06:06:40
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/product")
public class SoftwareVersionAction extends WWAction {

	private static final long serialVersionUID = -1104637361693396320L;

	@Resource
	private IProductTypeService productTypeService;

	@Resource
	private IProductSoftwareConfigService productSoftwareConfigService;

	@Resource
	private ISoftwareTypeService softwareTypeService;

	@Resource
	private ISoftwareVersionService softwareVersionService;

	@Resource
	private ILanguageService languageService;
	
	@Resource
	private ILanguagePackService languagePackService;
	
//	@Resource
//	private IThemeManager themeManager;

	private SoftwareType softwareType;

	private ProductType productType;

	private ProductSoftwareConfig productSoftwareConfig;

	private SoftwareVersion softwareVersion;

	private SoftwarePack softwarePack;

	private LanguagePack languagePack;

	private List<LanguagePack> languagePacks;

	private List<Language> languages;

	private SoftwareVersionPageVo versionPageVo;

	private Integer state;

	private String code;

	private String jsonData;
	
	private String versionCodes;
	
	private String returnPage;
	
	private String langugagStr;
	
//	private File testPath;	//文件
	
//	private String testPathFileName;	//文件名称
	


	public String getLangugagStr() {
		return langugagStr;
	}

	public void setLangugagStr(String langugagStr) {
		this.langugagStr = langugagStr;
	}

	public String getReturnPage() {
		return returnPage;
	}

	public void setReturnPage(String returnPage) {
		this.returnPage = returnPage;
	}

	/**
	 * @return the testPath
	 */
//	public File getTestPath() {
//		return testPath;
//	}
//
//	/**
//	 * @param testPath the testPath to set
//	 */
//	public void setTestPath(File testPath) {
//		this.testPath = testPath;
//	}
//
//	/**
//	 * @return the testPathFileName
//	 */
//	public String getTestPathFileName() {
//		return testPathFileName;
//	}
//
//	/**
//	 * @param testPathFileName the testPathFileName to set
//	 */
//	public void setTestPathFileName(String testPathFileName) {
//		this.testPathFileName = testPathFileName;
//	}

	public SoftwareType getSoftwareType() {
		return softwareType;
	}

	public void setSoftwareType(SoftwareType softwareType) {
		this.softwareType = softwareType;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public ProductSoftwareConfig getProductSoftwareConfig() {
		return productSoftwareConfig;
	}

	public void setProductSoftwareConfig(ProductSoftwareConfig productSoftwareConfig) {
		this.productSoftwareConfig = productSoftwareConfig;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public SoftwareVersion getSoftwareVersion() {
		return softwareVersion;
	}

	public void setSoftwareVersion(SoftwareVersion softwareVersion) {
		this.softwareVersion = softwareVersion;
	}

	public List<LanguagePack> getLanguagePacks() {
		return languagePacks;
	}

	public void setLanguagePacks(List<LanguagePack> languagePacks) {
		this.languagePacks = languagePacks;
	}

	public SoftwarePack getSoftwarePack() {
		return softwarePack;
	}

	public void setSoftwarePack(SoftwarePack softwarePack) {
		this.softwarePack = softwarePack;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public SoftwareVersionPageVo getVersionPageVo() {
		return versionPageVo;
	}

	public void setVersionPageVo(SoftwareVersionPageVo versionPageVo) {
		this.versionPageVo = versionPageVo;
	}

	public LanguagePack getLanguagePack() {
		return languagePack;
	}

	public void setLanguagePack(LanguagePack languagePack) {
		this.languagePack = languagePack;
	}

	/**
	 * @return the versionCodes
	 */
	public String getVersionCodes() {
		return versionCodes;
	}

	/**
	 * @param versionCodes the versionCodes to set
	 */
	public void setVersionCodes(String versionCodes) {
		this.versionCodes = versionCodes;
	}

	/**
	 * @Title: toSoftwareVersion
	 * @Description: 跳转到软件版本列表页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toSoftwareVersion", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/software_version_list.jsp") })
	public String toSoftwareVersion() throws Exception {
		if (null != softwareType && StringUtils.isNotBlank(softwareType.getCode().trim()) && softwareType.getCode().indexOf(",") != -1) {
			softwareType.setCode(softwareType.getCode().split(",")[0].trim());
		}
		softwareType = softwareTypeService.getSoftwareTypeByCode(softwareType
				.getCode());
		productType = productTypeService.getProductTypeByCode(softwareType
				.getProTypeCode());
		
		productSoftwareConfig = productSoftwareConfigService.getProductSoftwareConfigByCode(softwareType.getCode());
		
		languages = languageService.queryAllLanguage();
		if (versionPageVo == null) {
			versionPageVo = new SoftwareVersionPageVo();
			versionPageVo.setSoftwareTypeCode(softwareType.getCode());
		}
        
		this.webpage = softwareVersionService.pageSoftwareVersion(
				versionPageVo, this.getPage(), this.getPageSize());

		if (null == softwareVersion) {
			softwareVersion = new SoftwareVersion();
			softwareVersion.setSoftwareTypeCode(softwareType.getCode());
		}

		return SUCCESS;
	}
	
	
	/**
	 * @Title: toSoftwareVersion2
	 * @Description: 跳转到软件版本列表页面for 刘研群
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toSoftwareVersion2", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/software_version_list2.jsp") })
	public String toSoftwareVersion2() throws Exception {
		if (null != softwareType && StringUtils.isNotBlank(softwareType.getCode().trim()) && softwareType.getCode().indexOf(",") != -1) {
			softwareType.setCode(softwareType.getCode().split(",")[0].trim());
		}
		softwareType = softwareTypeService.getSoftwareTypeByCode(softwareType
				.getCode());
		productType = productTypeService.getProductTypeByCode(softwareType
				.getProTypeCode());
		
		productSoftwareConfig = productSoftwareConfigService.getProductSoftwareConfigByCode(softwareType.getCode());
		
		languages = languageService.queryAllLanguage();
		if (versionPageVo == null) {
			versionPageVo = new SoftwareVersionPageVo();
			versionPageVo.setSoftwareTypeCode(softwareType.getCode());
		}
        
		this.webpage = softwareVersionService.pageSoftwareVersion(
				versionPageVo, this.getPage(), this.getPageSize());

		if (null == softwareVersion) {
			softwareVersion = new SoftwareVersion();
			softwareVersion.setSoftwareTypeCode(softwareType.getCode());
		}

		return SUCCESS;
	}

	/**
	 * @Title: toAddSoftwareVersion
	 * @Description: 跳转到添加软件版本的页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toAddSoftwareVersion", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/software_version_add.jsp") })
	public String toAddSoftwareVersion() throws Exception {
		this.softwareType = this.softwareTypeService.getSoftwareTypeByCode(softwareType.getCode());
		languages = languageService.queryAllLanguage();
		return SUCCESS;
	}
	
	@Action(value = "toBatchAddSoftwareVersion", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/software_version_batch_add.jsp") })
	public String toBatchAddSoftwareVersion() throws Exception {
		String [] arr=langugagStr.split(",");
		languages = languageService.queryAllLanguage();
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<languages.size();j++){
				Language l=languages.get(j);
				if(l.getName().equals(arr[i])){
					languages.remove(j);
				}
				
			}
			
		}
		
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: addSoftwareVersion
	 * @Description: 添加软件版本
	 * @param
	 * @return String
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "addSoftwareVersion")
	public String addSoftwareVersion() throws Exception {

		try{
			
			softwareVersionService.addSoftwareVersionDetail(softwareVersion,
					softwarePack, languagePacks);

			this.msgs.add(getText("market.mainsale.message3"));
			this.urls.put(
					getText("market.mainsale.message5"),
					"toSoftwareVersion.do?softwareType.code="+softwareType.getCode()+"&returnPage="+returnPage);
//			softwareType = softwareTypeService.getSoftwareTypeByCode(softwareType
//					.getCode());
			
//			productType = productTypeService.getProductTypeByCode(softwareType
//					.getProTypeCode());
//			languages = languageService.queryLanguage();
			
		}catch(Exception e){
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(
					getText("common.btn.return"),
					"toAddSoftwareVersion.do?softwareType.code="+softwareType.getCode()+"&returnPage="+returnPage);
		}
		return MESSAGE;
//		
//		if (versionPageVo == null) {
//			versionPageVo = new SoftwareVersionPageVo();
//			versionPageVo.setSoftwareTypeCode(softwareType.getCode());
//		}
//
//		this.webpage = softwareVersionService.pageSoftwareVersion(
//				versionPageVo, this.getPage(), this.getPageSize());
//
//		if (null == softwareVersion) {
//			softwareVersion = new SoftwareVersion();
//			softwareVersion.setSoftwareTypeCode(softwareType.getCode());
//		}
//		return SUCCESS;
	}

	/**
	 * 
	 * @Title: releaseVersion
	 * @Description: 软件版本发布
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "releaseVersion", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String releaseVersion() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			softwareVersionService.releaseSoftwareVersion(code, state);
			map.put("flag", "true");
		} catch (Exception e) {
			map.put("flag", "false");
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: delSoftwareVersion
	 * @Description: 删除软件版本
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "delSoftwareVersion", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String delSoftwareVersion() throws Exception {
		
		Map<String,String> map = new HashMap<String,String>();
		try{
			this.softwareVersionService.delSoftwareVersion(softwareVersion.getCode());
			map.put("flag","true");
		}catch(Exception e){
			e.printStackTrace();
			map.put("flag","false");
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
		
//		try {
//			this.softwareVersionService.delSoftwareVersion(softwareVersion.getCode());
//			this.msgs.add(getText("market.promotion.delSuccess"));
//			this.urls.put(getText("market.mainsale.message5"),
//					"toSoftwareVersion.do?versionPageVo.softwareTypeCode="+softwareType
//					.getCode()+"&softwareType.code="+softwareType
//					.getCode());
//		} catch (Exception e) {
//			e.printStackTrace();
//			this.msgs.add(getText("common.system.error"));
//			this.urls.put(getText("common.btn.return"), "toSoftwareVersion.do?versionPageVo.softwareTypeCode="+softwareType
//					.getCode()+"&softwareType.code="+softwareType
//					.getCode());
//		}
//		return MESSAGE;
	}

	/**
	 * 
	* @Title: batchDelSoftwareVersion
	* @Description: 批量删除软件版本
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "batchDelSoftwareVersion")
	public String batchDelSoftwareVersion() throws Exception {

		try {
			this.softwareVersionService.batchDelSoftwareVersion(versionCodes);
			this.msgs.add(getText("market.promotion.delSuccess"));
			this.urls.put(getText("market.mainsale.message5"), "toSoftwareVersion.do?versionPageVo.softwareTypeCode=" + softwareType.getCode()
					+ "&softwareType.code=" + softwareType.getCode()+"&page="+returnPage);
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toSoftwareVersion.do?versionPageVo.softwareTypeCode=" + softwareType.getCode()
					+ "&softwareType.code=" + softwareType.getCode()+"&page="+returnPage);
		}
		return MESSAGE;

	}
	
	/**
	 * 
	* @Title: batchDelPublishVersion
	* @Description: 批量删除软件发布
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "batchDelPublishVersion")
	public String batchDelPublishVersion() throws Exception {

		try {
			softwareVersionService.releaseSoftwareVersion(code, state);
			this.msgs.add(getText("取消成功"));
			this.urls.put(getText("market.mainsale.message5"), "toSoftwareVersion.do?versionPageVo.softwareTypeCode=" + softwareType.getCode()
					+ "&softwareType.code=" + softwareType.getCode()+"&page="+returnPage);
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toSoftwareVersion.do?versionPageVo.softwareTypeCode=" + softwareType.getCode()
					+ "&softwareType.code=" + softwareType.getCode()+"&page="+returnPage);
		}
		return MESSAGE;

	}
	
	
	/**
	 * 
	 * @Title: delLanguagepack
	 * @Description: 删除语言包
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "delLanguagepack", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String delLanguagepack() throws Exception {
		
		Map<String,String> map = new HashMap<String,String>();
		try{
			this.languagePackService.delLanguagePack(languagePack.getLanguagePackCode());
			map.put("flag","true");
		}catch(Exception e){
			e.printStackTrace();
			map.put("flag","false");
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
		
//		try {
//			this.languagePackService.delLanguagePack(languagePack.getLanguagePackCode());
//			this.msgs.add(getText("market.promotion.delSuccess"));
//			this.urls.put(getText("market.mainsale.message5"),
//					"toSoftwareVersion.do?versionPageVo.softwareTypeCode="+softwareType
//					.getCode()+"&softwareType.code="+softwareType
//					.getCode());
//		} catch (Exception e) {
//			e.printStackTrace();
//			this.msgs.add(getText("common.system.error"));
//			this.urls.put(getText("common.btn.return"), "toSoftwareVersion.do?versionPageVo.softwareTypeCode="+softwareType
//					.getCode()+"&softwareType.code="+softwareType
//					.getCode());
//		}
//		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: updateSoftversion
	* @Description: 修改软件版本
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "updateSoftversion", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateSoftversion() throws Exception{
		Map<String,String> map = new HashMap<String,String>();
		try {
			this.softwareVersionService.updateSoftversion(softwareVersion, softwarePack);
			map.put("flag", "true");
		} catch (Exception e) {
			e.printStackTrace();
			map.put("flag", "false");
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: updateLanguagePack
	* @Description: 修改语言包
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "updateLanguagePack")
	public String updateLanguagePack() throws Exception{
		try {
//			if(testPath != null){
//				CopSite site  = CopContext.getContext().getCurrentSite();
//				Theme theme = themeManager.getTheme( site.getThemeid());
//				String randName = UUID.randomUUID().toString().replaceAll("-", "");
//				String fileName =randName+"."+ testPathFileName.split("\\.")[1];
//				String filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/language/"+theme.getPath()+"/";
//				filePath += fileName;
//				if(!UploadUtil.fileExists(filePath)){
//					FileUtil.createFile(testPath, filePath);
//					languagePack.setTestPath("statics/attachment/language/"
//							+ theme.getPath() + "/" + fileName);
//				}
//			}
			this.languagePackService.updateLanguagePack(languagePack);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("common.btn.return"), "toSoftwareVersion.do?softwareType.code="+code+"&returnPage="+returnPage);
		} catch (Exception e) {
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toSoftwareVersion.do?softwareType.code="+code+"&returnPage="+returnPage);
		}
		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: addLanguagePack
	* @Description: 添加语言包
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "addLanguagePack")
	public String addLanguagePack() throws Exception{
		
		try {
//			if(testPath != null){
//				CopSite site  = CopContext.getContext().getCurrentSite();
//				Theme theme = themeManager.getTheme( site.getThemeid());
//				String randName = UUID.randomUUID().toString().replaceAll("-", "");
//				String fileName =randName+"."+ testPathFileName.split("\\.")[1];
//				String filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/language/"+theme.getPath()+"/";
//				filePath += fileName;
//				if(!UploadUtil.fileExists(filePath)){
//					FileUtil.createFile(testPath, filePath);
//					languagePack.setTestPath("statics/attachment/language/"
//							+ theme.getPath() + "/" + fileName);
//				}
//			}
			this.languagePackService.addLanguagePack(languagePack);
			this.msgs.add(getText("market.mainsale.message3"));
			this.urls.put(getText("common.btn.return"), "toSoftwareVersion.do?softwareType.code="+code+"&returnPage="+returnPage);
		} catch (Exception e) {
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toSoftwareVersion.do?softwareType.code="+code+"&returnPage="+returnPage);
		}
		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: getAvailableLanguage
	* @Description: 获取可用的语言数据
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "getAvailableLanguage", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
			public String getAvailableLanguage() throws Exception{
//		Map<String,String> map = new HashMap<String,String>();
		
		try {
			this.languages = this.softwareVersionService.getAvailableLanguage(this.softwareVersion.getCode());
//			map.put("flag", "true");
		} catch (Exception e) {
			e.printStackTrace();
//			map.put("flag", "false");
		}
		jsonData = JSONArray.fromObject(this.languages).toString();
		return SUCCESS;
	}
}
