/**
* @ClassName: ProductTreeAction
* @Description: TODO
* @author shaohu
* @date 2013-1-8 下午06:53:31
*
*/ 
package com.cheriscon.backstage.product.action;


import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.product.component.IProductTreeComponent;
import com.cheriscon.backstage.product.vo.ProductTreeParam;
import com.cheriscon.framework.action.WWAction;

/**
 * @ClassName: ProductTreeAction
 * @Description: 产品管理树
 * @author shaohu
 * @date 2013-1-8 下午06:53:31
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/product")
public class ProductTreeAction extends WWAction {

	private static final long serialVersionUID = -124256164160223221L;

	@Resource
	private IProductTreeComponent treeComponent;
	
	private String jsonData;
	
	private ProductTreeParam treeParam;
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public ProductTreeParam getTreeParam() {
		return treeParam;
	}

	public void setTreeParam(ProductTreeParam treeParam) {
		this.treeParam = treeParam;
	}
	/**
	* @Title: toProductTree
	* @Description: 产品管理
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toProductTree", results = { @Result(name = SUCCESS, location="/autel/backstage/product/product_tree.jsp") })
	public String toProductTree() throws Exception{
		return SUCCESS;
	}
	
	
	/**
	* @Title: toTreeLeft
	* @Description: 产品管理树左边
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toTreeLeft", results = { @Result(name = SUCCESS, location="/autel/backstage/product/tree_left.jsp") })
	public String toTreeLeft() throws Exception{
		return SUCCESS;
	}
	
	@Action(value = "bulidProductTree", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String bulidProductTree() throws Exception{
		jsonData = treeComponent.bulidProductTree(getText("product.rootName"));
		return SUCCESS;
	}

	/**
	* @Title: bulidVirtualDirTree
	* @Description: 构建虚拟目录树
	* @return String    
	* @throws
	* @author pengdongan
	* @date 2013-11-26
	 */
	@Action(value = "bulidVirtualDirTree", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String bulidVirtualDirTree() throws Exception{
		jsonData = treeComponent.bulidVirtualDirTree(getText("product.rootName"));
		return SUCCESS;
	}
	
	/**
	 * 获取产品树数据
	 * @return
	 * @throws Exception
	 */
	@Action(value = "getProductTree", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getProductTree() throws Exception{
		jsonData = treeComponent.getProductTree(treeParam);
		return SUCCESS;
	}
}
