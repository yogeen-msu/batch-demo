/**
*/ 
package com.cheriscon.backstage.product.action;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.product.service.IProductSoftwareConfigService;
import com.cheriscon.backstage.product.service.IProductTypeConfigService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.service.ISoftwareNameService;
import com.cheriscon.backstage.product.service.ISoftwareTypeService;
import com.cheriscon.backstage.product.service.ISoftwareVersionService;
import com.cheriscon.backstage.product.vo.ProductTypeConfig;
import com.cheriscon.backstage.product.vo.SoftwareName;
import com.cheriscon.backstage.product.vo.SoftwareVersionSearch;
import com.cheriscon.backstage.product.vo.SoftwareVersionVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.util.CallProcedureUtil;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductSoftwareConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.Theme;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.utils.UploadUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.FileUtil;

/**
 * @ClassName: SoftwareTypeAction
 * @Description: 软件类型控制类
 * @author shaohu
 * @date 2013-1-9 下午03:24:40
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/product")
public class SoftwareTypeAction extends WWAction{

	private static final long serialVersionUID = -980516855101962995L;
	
	@Resource
	private IProductTypeService productTypeService;
	
	@Resource
	private ISoftwareTypeService softwareTypeService;
	
	@Resource
	private ISoftwareVersionService versionService;
	
	@Resource
	private ILanguageService  languageService;
	
	@Resource
	private IThemeManager themeManager;
	
	@Resource
	private ISoftwareNameService softwareNameService;
	
	@Resource
	private IProductSoftwareConfigService productSoftwareConfigService;
	
	private File carSign;
	
	private String carSignFileName;
	
	/**
	 * 产品型号
	 */
	private ProductType productType;
	
	private SoftwareType softwareType;
	
	private SoftwareVersionSearch search;
	
	public List<SoftwareVersionVo> softwareVersionVos;
	
	private List<Language> language;
	
	private List<SoftwareName> config;
	
	public String jsonData;
	
	private Integer productTypeNameID;
	
	private SoftwareName softwareName;
	
	private String returnPage;
	
	private ProductSoftwareConfig productSoftwareConfig;
	
	private ProductSoftwareConfig productSoftwareConfigParent;
	
	private List<ProductSoftwareConfig> productSoftwareConfigList;
	
	private List<SoftwareName> softwareNameList;
	
	private String langugagStr;

	public String getLangugagStr() {
		return langugagStr;
	}

	public void setLangugagStr(String langugagStr) {
		this.langugagStr = langugagStr;
	}

	public List<SoftwareName> getSoftwareNameList() {
		return softwareNameList;
	}

	public void setSoftwareNameList(List<SoftwareName> softwareNameList) {
		this.softwareNameList = softwareNameList;
	}

	public List<ProductSoftwareConfig> getProductSoftwareConfigList() {
		return productSoftwareConfigList;
	}

	public void setProductSoftwareConfigList(
			List<ProductSoftwareConfig> productSoftwareConfigList) {
		this.productSoftwareConfigList = productSoftwareConfigList;
	}

	public ProductSoftwareConfig getProductSoftwareConfig() {
		return productSoftwareConfig;
	}

	public void setProductSoftwareConfig(ProductSoftwareConfig productSoftwareConfig) {
		this.productSoftwareConfig = productSoftwareConfig;
	}

	public ProductSoftwareConfig getProductSoftwareConfigParent() {
		return productSoftwareConfigParent;
	}

	public void setProductSoftwareConfigParent(
			ProductSoftwareConfig productSoftwareConfigParent) {
		this.productSoftwareConfigParent = productSoftwareConfigParent;
	}

	public String getReturnPage() {
		return returnPage;
	}

	public void setReturnPage(String returnPage) {
		this.returnPage = returnPage;
	}

	public SoftwareName getSoftwareName() {
		return softwareName;
	}

	public void setSoftwareName(SoftwareName softwareName) {
		this.softwareName = softwareName;
	}

	public Integer getProductTypeNameID() {
		return productTypeNameID;
	}

	public void setProductTypeNameID(Integer productTypeNameID) {
		this.productTypeNameID = productTypeNameID;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public SoftwareType getSoftwareType() {
		return softwareType;
	}

	public void setSoftwareType(SoftwareType softwareType) {
		this.softwareType = softwareType;
	}

	public SoftwareVersionSearch getSearch() {
		return search;
	}

	public void setSearch(SoftwareVersionSearch search) {
		this.search = search;
	}

	public List<SoftwareVersionVo> getSoftwareVersionVos() {
		return softwareVersionVos;
	}

	public void setSoftwareVersionVos(List<SoftwareVersionVo> softwareVersionVos) {
		this.softwareVersionVos = softwareVersionVos;
	}

	public File getCarSign() {
		return carSign;
	}

	public void setCarSign(File carSign) {
		this.carSign = carSign;
	}

	public String getCarSignFileName() {
		return carSignFileName;
	}

	public void setCarSignFileName(String carSignFileName) {
		this.carSignFileName = carSignFileName;
	}
	
	@Action(value = "refreshBySoftCode")
	public String refreshBySoftCode() throws Exception{
		try{
			CallProcedureUtil.call("pro_InitMinSaleSoftPackSearchBySoftCode",softwareType.getCode());
			msgs.add("操作成功");
		}catch(Exception e){
			msgs.add("操作失败，请联系管理员");
		}
		this.urls.put("返回", "toSoftwareType.do?productSoftwareConfig.parentCode="+productSoftwareConfig.getParentCode());
		return MESSAGE;
	}

	@Action(value="toSoftwareType", results = { @Result(name = SUCCESS, location="/autel/backstage/product/software_type_list.jsp") })
	public String toSoftwareType() throws Exception{
		
		String returnPage=getRequest().getParameter("returnPage");
		if(returnPage!=null){
			if(page==0 || page==1){
			this.page=Integer.parseInt(returnPage);
			this.setPage(page);
			}
		}
		
		if(productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode()) != null)
		{
			productSoftwareConfigParent = productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode());
		}
		
		if(productSoftwareConfigService.getRootDirByCode(productSoftwareConfig.getParentCode()) != null)
		{
			this.productType = this.productTypeService.getProductTypeByCode(productSoftwareConfigService.getRootDirByCode(productSoftwareConfig.getParentCode()).getCode().trim());
		}
		if(productSoftwareConfig.getParentCode()!=null && productSoftwareConfig.getParentCode().indexOf(",")!=-1){
			productSoftwareConfig.setParentCode(productSoftwareConfig.getParentCode().substring(0, productSoftwareConfig.getParentCode().indexOf(",")));
		}
		
		this.webpage = softwareTypeService.pageSoftwareTypePageNew(productSoftwareConfig.getParentCode(),softwareType, this.getPage(), this.getPageSize());
		
		return SUCCESS;
	}
	
	
	@Action(value="querySoftwareType", results = { @Result(name = SUCCESS, location="/autel/backstage/product/software_type_list.jsp") })
	public String querySoftwareType() throws Exception{
		
		String returnPage=getRequest().getParameter("returnPage");
		if(returnPage!=null){
			if(page==0 || page==1){
			this.page=Integer.parseInt(returnPage);
			this.setPage(page);
			}
		}
		
		if(productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode()) != null)
		{
			productSoftwareConfigParent = productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode());
		}
		
		if(productSoftwareConfigService.getRootDirByCode(productSoftwareConfig.getParentCode()) != null)
		{
			this.productType = this.productTypeService.getProductTypeByCode(productSoftwareConfigService.getRootDirByCode(productSoftwareConfig.getParentCode()).getCode().trim());
		}
		if(productSoftwareConfig.getParentCode()!=null && productSoftwareConfig.getParentCode().indexOf(",")!=-1){
			productSoftwareConfig.setParentCode(productSoftwareConfig.getParentCode().substring(0, productSoftwareConfig.getParentCode().indexOf(",")));
		}
		
		this.webpage = softwareTypeService.querySoftwareTypePageNew(productType.getCode(),softwareType, this.getPage(), this.getPageSize());
		
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: toAddSoftwareType
	* @Description: 跳转到添加功能软件页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value="toAddSoftwareType", results = { @Result(name = SUCCESS, location="/autel/backstage/product/software_type_add.jsp") })
	public String toAddSoftwareType() throws Exception{
		String productTypeCode = productSoftwareConfigService.getRootDirByCode(productSoftwareConfig.getParentCode()).getCode();
		this.productType = this.productTypeService.getProductTypeByCode(productTypeCode.trim());
		productSoftwareConfigList = productSoftwareConfigService.listProductSoftwareConfigLevel(productTypeCode.trim());
		return SUCCESS;
	}

	
	/**
	 * 
	* @Title: toProductName
	* @Description: 跳转到软件名称管理页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value="toProductName", results = { @Result(name = SUCCESS, location="/autel/backstage/product/software_type_name.jsp") })
	public String toProductName() throws Exception{
		config=softwareNameService.getProductTypeConfig(softwareType.getCode());
		softwareType = softwareTypeService.getSoftwareTypeByCode(softwareType.getCode());		
		productSoftwareConfig = productSoftwareConfigService.getProductSoftwareConfigByCode(softwareType.getCode());		
		language=languageService.queryAllLanguage();
		for(int i=0;i<language.size();i++){
			String languageCode=language.get(i).getCode();
			for(int j=0;j<config.size();j++){
				String temp=config.get(j).getLanguageCode();
				if(languageCode.equals(temp)){
					language.remove(i);
					i--;
				}
			}
		}
		return SUCCESS;
	}
	
	/**
	 * 查询未添加的产品型号图片的语言
	 * @return
	 * @throws Exception
	 */
	@Action(value = "deleteLanguageNameJson", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String deleteLanguageNameJson() throws Exception {
		softwareNameService.delete(softwareName.getCode());
		return SUCCESS;
	}
	
	/**
	 * 更新语言名称
	 * @return
	 * @throws Exception
	 */
	@Action(value = "updateLanguageNameJson", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateLanguageNameJson() throws Exception {
		softwareNameService.updateName(softwareName.getCode(),softwareName.getSoftwareName());
		return SUCCESS;
	}
	@Action(value="saveSoftwareName")
	public String saveSoftwareName() throws Exception{
		return MESSAGE;
	}
	/**
	 * 查询未添加的产品型号图片的语言
	 * @return
	 * @throws Exception
	 */
	@Action(value = "saveLanguageNameJson", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/product_list.jsp" ) })
	public String saveLanguageNameJson() throws Exception { 
		try{
			softwareNameService.save(softwareNameList);
			this.msgs.add("操作成功");
			this.urls.put(getText("common.btn.return"), "toProductName.do?softwareType.code="+softwareNameList.get(0).getSoftwareCode());
		}catch(Exception e){
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("common.btn.return"), "toProductName.do?softwareType.code="+softwareNameList.get(0).getSoftwareCode());
		}
		return MESSAGE;
	}
	
	@Action(value = "batchAddLanguageName", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/software_name_batch_add.jsp" ) })
	public String batchAddLanguageName() throws Exception { 
		String [] arr=langugagStr.split(",");
		language = languageService.queryAllLanguage();
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<language.size();j++){
				Language l=language.get(j);
				if(l.getName().equals(arr[i])){
					language.remove(j);
				}
				
			}
			
		}
		
		return SUCCESS;
	}
	
	
	/**
	 * 查询未添加的产品型号图片的语言
	 * @return
	 * @throws Exception
	 */
	@Action(value = "queryLanguageNameJson", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryLanguageNameJson() throws Exception {
		jsonData = softwareTypeService.queryLanguageNameJson(softwareType.getCode());
		return SUCCESS;
	}
	
	
	
//	@Action(value = "addSoftwareType",results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@SuppressWarnings("unchecked")
	@Action(value = "addSoftwareType")
	public String addSoftwareType() throws Exception {
//		Map<String,String> map = new HashMap<String,String>();
//		try{
//			SoftwareType type = softwareTypeService
//				.getSoftwareTypeByName(softwareType.getName());
//			// 为空表示名称未被使用可以添加
//			if (null == type) {
//				softwareTypeService.addSoftwareType(softwareType);
//				map.put("flag", "1");
//			}else{
//				map.put("flag", "2");
//			}
//		}catch(Exception ex){
//			ex.printStackTrace();
//			map.put("flag", "0");
//		}
//		this.jsonData = JSONArray.fromObject(map).toString();
//		return SUCCESS;

		
		try{
			if (null != softwareType) {
				if (StringUtils.isNotBlank(softwareType.getName())) {
					SoftwareType type = softwareTypeService.getSoftwareTypeByName(softwareType.getName(), softwareType.getProTypeCode());
					// 为空表示名称未被使用可以添加
					if (null == type) {
//						if(carSign != null){
//							CopSite site  = CopContext.getContext().getCurrentSite();
//							Theme theme = themeManager.getTheme( site.getThemeid());
//							String randName = UUID.randomUUID().toString().replaceAll("-", "");
//							String fileName =randName+"."+ carSignFileName.split("\\.")[1];
//							String filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/language/"+theme.getPath()+"/";
//							filePath += fileName;
//							if(!UploadUtil.fileExists(filePath)){
//								FileUtil.createFile(carSign, filePath);
//								String path = UploadUtil.upload(carSign,carSignFileName, "carico");
//								softwareType.setCarSignPath(path);
//								softwareType.setCarSignPath("statics/attachment/language/"
//										+ theme.getPath() + "/" + fileName);
//								productType.setPicPath(CopSetting.IMG_SERVER_DOMAIN + "/attachment/language/"
//										+ theme.getPath() + "/" + fileName);
//							}

						productSoftwareConfigService.addSoftwareType(softwareType,productSoftwareConfig);
						
						this.msgs.add(getText("product.softwaretype.add"));
//						this.urls.put(getText(getText("common.btn.return")), "toAddSoftwareType.do?productSoftwareConfig.parentCode="+productSoftwareConfig.getParentCode());
						this.urls.put(
							getText("market.mainsale.message5"),
							"toSoftwareType.do?productSoftwareConfig.parentCode="
							+ productSoftwareConfig.getParentCode());
//						}else{
//							this.msgs.add(getText("product.softwaretype.message3"));
//							this.urls.put(getText("common.btn.return"), "toAddSoftwareType.do?productSoftwareConfig.parentCode="+productSoftwareConfig.getParentCode());
//						}
						
					} else {
						this.msgs.add(getText("product.softwaretype.message1"));
						this.urls.put(getText("common.btn.return"), "toAddSoftwareType.do?productSoftwareConfig.parentCode="+productSoftwareConfig.getParentCode());
					}
				}else{
//					if(StringUtils.isBlank(softwareType.getName())){
						this.msgs.add(getText("product.softwaretype.message2"));
//					}
//					if(StringUtils.isBlank(softwareType.getCarSignPath())){
//						this.msgs.add(getText("product.softwaretype.message3"));
//					}
					this.urls.put(getText("common.btn.return"), "toAddSoftwareType.do?productSoftwareConfig.parentCode="+productSoftwareConfig.getParentCode());
				}
				
			}else{
				this.msgs.add(getText("product.softwaretype.message1"));
				this.msgs.add(getText("product.softwaretype.message3"));
				this.urls.put(getText("common.btn.return"), "toAddSoftwareType.do?productSoftwareConfig.parentCode="+productSoftwareConfig.getParentCode());
			}
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toAddSoftwareType.do?productSoftwareConfig.parentCode="+productSoftwareConfig.getParentCode());
		}
		return MESSAGE;
	}
	
	
	/**
	 * 
	* @Title: toReleaseSoftwareversion
	* @Description: 跳转到软件版本发布页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toReleaseSoftwareversion", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/software_version_release.jsp") })
	public String toReleaseSoftwareversion() throws Exception{
		
		if(productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode()) != null)
		{
			productSoftwareConfigParent = productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode());
		}
		
		if(productSoftwareConfigService.getRootDirByCode(productSoftwareConfig.getParentCode()) != null)
		{
			this.productType = this.productTypeService.getProductTypeByCode(productSoftwareConfigService.getRootDirByCode(productSoftwareConfig.getParentCode()).getCode().trim());
		}
		
		if(search == null)
		{
			search = new SoftwareVersionSearch();
		}

		search.setParentCode(productSoftwareConfig.getParentCode());
		
		this.webpage = versionService.pageSoftwareVersionRelease(search,this.getPage(), this.getPageSize());
		return SUCCESS;
	}
	
	
	/**
	 * 
	* @Title: toReleaseSoftwareversionNew
	* @Description: 跳转到软件版本发布页面（新），跳转到该产品目录下的所有未发布和已发布的列表-刘妍群20150204
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toReleaseSoftwareversionNew", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/software_version_release.jsp") })
	public String toReleaseSoftwareversionNew() throws Exception{
		if(productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode()) != null)
		{
			productSoftwareConfigParent = productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode());
		}
		
		this.productType = this.productTypeService.getProductTypeByCode(productType.getCode());
		
		if(search == null)
		{
			search = new SoftwareVersionSearch();
		}

		search.setProductTypeCode(productType.getCode());
		
		this.webpage = versionService.pageSoftwareVersionReleaseNew(search,this.getPage(), this.getPageSize());
		return SUCCESS;
	}
	
	
	@Action(value = "deleteSoftwareType", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String deleteSoftwareType() throws Exception{
		Map<String,String> map = new HashMap<String,String>();
		try{
			productSoftwareConfigService.delSoftwareType(softwareType);
			
			map.put("flag","true");
		}catch(Exception e){
			e.printStackTrace();
			map.put("flag","false");
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	/**
	 * 跳转到修改页面
	* @Title: toUpdataSoftwareType
	* @Description: 跳转到修改页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toUpdataSoftwareType", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/software_type_update.jsp") })
	public String toUpdataSoftwareType() throws Exception{
		softwareType = softwareTypeService.getSoftwareTypeByCode(softwareType.getCode());		
		productType = productTypeService.getProductTypeByCode(softwareType.getProTypeCode());
		productSoftwareConfig = productSoftwareConfigService.getProductSoftwareConfigByCode(softwareType.getCode());		
		productSoftwareConfigList = productSoftwareConfigService.listProductSoftwareConfigLevel(softwareType.getProTypeCode().trim());
		
		return SUCCESS;
	}
	
	
	/**
	 * 
	* @Title: updateSoftwareType
	* @Description: 修改软件类型数据
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
//	@Action(value = "updateSoftwareType", results = { @Result(name = SUCCESS, location="/autel/backstage/product/software_type_list.jsp" )})
	@Action(value = "updateSoftwareType")
	public String updateSoftwareType() throws Exception {
		try {
			
			if(StringUtils.isBlank(softwareType.getName().trim())){
				this.msgs.add(getText("product.softwaretype.message1"));
				this.urls.put(getText("common.btn.return"), "toUpdataSoftwareType.do?softwareType.code="+softwareType.getCode()+"&page="+returnPage);
				return MESSAGE;
			}
//			if(carSign == null){
//				this.msgs.add(getText("product.softwaretype.message3"));
//				this.urls.put(getText("common.btn.return"), "toUpdataSoftwareType.do?softwareType.code="+softwareType.getCode());
//				return MESSAGE;
//			}
			
//			if(carSign != null){
//				CopSite site  = CopContext.getContext().getCurrentSite();
//				Theme theme = themeManager.getTheme( site.getThemeid());
//				String randName = UUID.randomUUID().toString().replaceAll("-", "");
//				String fileName =randName+"."+ carSignFileName.split("\\.")[1];
//				String filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/language/"+theme.getPath()+"/";
//				filePath += fileName;
//				if(!UploadUtil.fileExists(filePath)){
//					FileUtil.createFile(carSign, filePath);
//					String path = UploadUtil.upload(carSign,carSignFileName, "carico");
//					softwareType.setCarSignPath(path);
//					softwareType.setCarSignPath("statics/attachment/language/"
//							+ theme.getPath() + "/" + fileName);
//				}
//			}
			softwareTypeService.updateSoftwareType(softwareType);
			
			//修改虚拟目录对象
			productSoftwareConfig.setCode(softwareType.getCode());
			productSoftwareConfig.setName(softwareType.getName());
			productSoftwareConfigService.update(productSoftwareConfig);
			
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(
					getText("market.mainsale.message5"),
					"toSoftwareType.do?productSoftwareConfig.parentCode="
							+ productSoftwareConfig.getParentCode()+"&page="+returnPage);
		} catch (Exception e) {
			this.msgs.add(getText("common.system.error"));
			this.urls.put(
					getText("market.mainsale.message5"),
					"toSoftwareType.do?productSoftwareConfig.parentCode="
							+ productSoftwareConfig.getParentCode()+"&page="+returnPage);
		}
		return MESSAGE;
		// softwareTypeService.updateSoftwareType(softwareType);
		// softwareType.setName(null);
		// this.webpage = softwareTypeService.pageSoftwareTypePage(softwareType,
		// this.getPage(), this.getPageSize());
		// return SUCCESS;
	}
	
	public List<Language> getLanguage() {
		return language;
	}

	public void setLanguage(List<Language> language) {
		this.language = language;
	}

	public List<SoftwareName> getConfig() {
		return config;
	}

	public void setConfig(List<SoftwareName> config) {
		this.config = config;
	}
}
