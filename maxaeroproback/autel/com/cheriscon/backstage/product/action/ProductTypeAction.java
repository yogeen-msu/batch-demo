package com.cheriscon.backstage.product.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.product.service.IProductSoftwareConfigService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.vo.ProductTypeSerialPicVo;
import com.cheriscon.backstage.product.vo.ProductTypeVo;
import com.cheriscon.backstage.product.vo.SoftwareVersionSearch;
import com.cheriscon.backstage.util.CallProcedureUtil;
import com.cheriscon.common.model.ProductSoftwareConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.framework.action.WWAction;

/**
 * @remark 产品型号action
 * @date 2013-1-4
 * @author sh
 */
@ParentPackage("json_default")
@Namespace("/autel/product")
@ExceptionMappings({ @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class ProductTypeAction extends WWAction {

	private static final long serialVersionUID = -644859255038113322L;

	@Resource
	private IProductTypeService productTypeService;
	
	@Resource
	private IProductSoftwareConfigService productSoftwareConfigService;
	
	private ProductTypeVo productTypeVo;
	
	private ProductSoftwareConfig productSoftwareConfig;

	private List<ProductType> productTypes;

	private ProductType productType;
	
	private String jsonData;
	
	private String action = "NON";	//操作

	private List<ProductTypeSerialPicVo> productTypeSerialPicVos;

	private ProductTypeSerialPicVo productTypeSerialPicVo;

	public ProductTypeVo getProductTypeVo() {
		return productTypeVo;
	}

	public void setProductTypeVo(ProductTypeVo productTypeVo) {
		this.productTypeVo = productTypeVo;
	}

	public ProductSoftwareConfig getProductSoftwareConfig() {
		return productSoftwareConfig;
	}

	public void setProductSoftwareConfig(ProductSoftwareConfig productSoftwareConfig) {
		this.productSoftwareConfig = productSoftwareConfig;
	}

	public List<ProductType> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductType> productTypes) {
		this.productTypes = productTypes;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public List<ProductTypeSerialPicVo> getProductTypeSerialPicVos() {
		return productTypeSerialPicVos;
	}

	public void setProductTypeSerialPicVos(
			List<ProductTypeSerialPicVo> productTypeSerialPicVos) {
		this.productTypeSerialPicVos = productTypeSerialPicVos;
	}

	public ProductTypeSerialPicVo getProductTypeSerialPicVo() {
		return productTypeSerialPicVo;
	}

	public void setProductTypeSerialPicVo(
			ProductTypeSerialPicVo productTypeSerialPicVo) {
		this.productTypeSerialPicVo = productTypeSerialPicVo;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Action(value = "refreshProduct")
	public String refreshProduct() throws Exception{
		try{
			CallProcedureUtil.call("pro_InitMinSaleSoftPackSearch");
			msgs.add("操作成功");
		}catch(Exception e){
			msgs.add("操作失败，请联系管理员");
		}
		this.urls.put("返回", "search.do");
		return MESSAGE;
	}
	
	
	@Action(value = "refreshByProType")
	public String refreshByProType() throws Exception{
		try{
			CallProcedureUtil.call("pro_InitMinSaleSoftPackSearchByProduct",productType.getCode());
			msgs.add("操作成功");
		}catch(Exception e){
			msgs.add("操作失败，请联系管理员");
		}
		this.urls.put("返回", "search.do");
		return MESSAGE;
	}
	
	/**
	 * 产品型号分页查询
	 * @return
	 * @throws Exception
	 */
	@Action(value = "search", results = { @Result(name = "product_list", location = "/autel/backstage/product/product_list.jsp") })
	public String search() throws Exception {
		try {
			//productTypes = productTypeService.search(productTypeVo);
			this.webpage = productTypeService.searchPage(productTypeVo, this.getPage(), this.getPageSize());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product_list";
	}

	/**
	 * 保存产品型号
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "saveProductType", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/product_list.jsp") })
	public String saveProductType() throws Exception {
		
		try{
			
			boolean bool = productTypeService.isRepeat(null, productType.getName());
			if(bool){
				this.msgs.add(getText("product.producttypevo.repeatName"));
				this.urls.put(getText("common.btn.return"), "search.do");
				return MESSAGE;
			}

			productSoftwareConfigService.addProductType(productType, productSoftwareConfig);
			
			this.msgs.add(getText("market.mainsale.message3"));
			this.urls.put(getText("common.btn.return"), "toSerialPicPath.do?action=add&productType.name="+productType.getName()+"&productType.code="+productType.getCode());
		}catch(Exception e){
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "search.do");
		}
		return MESSAGE;
	}

	/**
	 * 修改产品型号
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "updateProductType", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/product_list.jsp" ) })
	public String updateProductType() throws Exception {
		try {
			
			boolean bool = productTypeService.isRepeat(productType.getCode(), productType.getName());
			if(bool){
				this.msgs.add(getText("product.producttypevo.repeatName"));
				this.urls.put(getText("common.btn.return"), "search.do");
				return MESSAGE;
			}
			
			productSoftwareConfigService.updateProductType(productType, productSoftwareConfig);
			
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("common.btn.return"), "search.do?action=update&productType.name="+productType.getName()+"&productType.code="+productType.getCode());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "search.do");
		}
		return MESSAGE;
	}

	/**
	 * 删除产品型号
	 * @return
	 * @throws Exception
	 */
	@Action(value = "delProductType", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String delProductType() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		
		if(productSoftwareConfigService.listProductSoftwareConfigByParentCode(productType.getCode()).size() > 0)
		{
			map.put("flag", "false");
		}
		else
		{
			productSoftwareConfigService.delProductType(productType);

			map.put("flag", "true");
		}
		
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 跳转到产品型号图片管理页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toSerialPicPath", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/product_serial_pic_list.jsp") })
	public String toSerialPicPath() throws Exception {
		productType = productTypeService.getProductTypeByCode(productType.getCode());
		productTypeSerialPicVos = productTypeService
				.getProductTypeSerialList(productType.getCode());
		return SUCCESS;
	}

	/**
	 * 查询未添加的产品型号图片的语言
	 * @return
	 * @throws Exception
	 */
	@Action(value = "queryLanguageJson", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryLanguageJson() throws Exception {
		jsonData = productTypeService.queryLanguageJson(productType.getCode());
		return SUCCESS;
	}

	/**
	 * 添加产品型号语言图片
	 * @return
	 * @throws Exception
	 */
	@Action(value = "addSerialPicPath", results = { @Result(name = SUCCESS, location = "/autel/backstage/product/product_serial_pic_list.jsp") })
	public String addSerialPicPath() throws Exception {
		productTypeService
				.addSerialPicPath(productType, productTypeSerialPicVo);
		productType = productTypeService.getProductTypeByCode(productType.getCode());
		productTypeSerialPicVos = productTypeService
				.getProductTypeSerialList(productType.getCode());
		return SUCCESS;
	}
	
	/**
	 * 删除产品型号语言图片
	 * @return
	 * @throws Exception
	 */
	@Action(value = "delProductPic", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String delProductPic() throws Exception{
		productTypeService.delPicByLanguageCode(productType.getCode(),productTypeSerialPicVo.getLanguageCode());
		Map<String, String> map = new HashMap<String, String>();
		map.put("flag", "true");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	/**
	 * 修改对应的语言图片
	 * @return
	 * @throws Exception
	 */
	@Action(value = "updateLanguagePic",results = { @Result(name = SUCCESS, location = "/autel/backstage/product/product_serial_pic_list.jsp") })
	public String updateLanguagePic() throws Exception{
		productTypeService.updateLanguagePic(productType,productTypeSerialPicVo);
		productType = productTypeService.getProductTypeByCode(productType.getCode());
		productTypeSerialPicVos = productTypeService
				.getProductTypeSerialList(productType.getCode());
		return SUCCESS;
	}
}
