package com.cheriscon.backstage.product.service;

import java.util.List;

import com.cheriscon.common.model.ProductSoftwareConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.cop.resource.model.Menu;

public interface IProductSoftwareConfigService {

	/**
	 * 添加软件
	 * 
	 * @author pengdongan
	 * @data 2013-12-05
	 */
	public String addSoftwareType(SoftwareType softwareType,ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 修改软件
	 * 
	 * @author pengdongan
	 * @data 2013-12-05
	 */
	public void updateSoftwareType(SoftwareType softwareType,ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 删除软件
	 * 
	 * @author pengdongan
	 * @data 2013-12-05
	 */
	public void delSoftwareType(SoftwareType softwareType) throws Exception;

	/**
	 * 添加产品类型
	 * 
	 * @author pengdongan
	 * @data 2013-12-05
	 */
	public String addProductType(ProductType productType,ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 修改产品类型
	 * 
	 * @author pengdongan
	 * @data 2013-12-05
	 */
	public void updateProductType(ProductType productType,ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 删除产品类型
	 * 
	 * @author pengdongan
	 * @data 2013-12-05
	 */
	public void delProductType(ProductType productType) throws Exception;

	/**
	 * 查询所有的虚拟目录
	 * @return
	 * @throws Exception
	 */
	public List<ProductSoftwareConfig> listAll() throws Exception;

	/**
	 * 查询所有的虚拟目录不含软件
	 * @return
	 * @throws Exception
	 */
	public List<ProductSoftwareConfig> listVirtualDirAll() throws Exception;

	/**
	 * 添加虚拟目录
	 * 
	 * @param productSoftwareConfig
	 * @return 虚拟目录编码
	 * @throws Exception
	 */
	public String add(ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 添加虚拟目录(对应产品、软件对象)
	 * 
	 * @param productSoftwareConfig
	 * @return 虚拟目录编码
	 * @throws Exception
	 */
	public String addForProSoft(ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 
	 * @param productSoftwareConfig
	 * @throws Exception
	 */
	public void update(ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 根据虚拟目录编码查询虚拟目录对象
	 * 
	 * @param code
	 *            虚拟目录编码
	 * @return
	 * @throws Exception
	 */
	public ProductSoftwareConfig getProductSoftwareConfigByCode(String code) throws Exception;

	/**
	 * 根据虚拟目录编码查询上级虚拟目录对象
	 * 
	 * @param code
	 *            虚拟目录编码
	 * @return
	 * @throws Exception
	 */
	public ProductSoftwareConfig getParentDirByCode(String code) throws Exception;

	/**
	 * 根据虚拟目录编码查询顶级虚拟目录对象
	 * 
	 * @param code
	 *            虚拟目录编码
	 * @return
	 * @throws Exception
	 */
	public ProductSoftwareConfig getRootDirByCode(String code) throws Exception;

	/**
	 * 删除虚拟目录对象
	 * 
	 * @param productSoftwareConfig
	 * @return
	 * @throws Exception
	 */
	public int delete(ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 根据虚拟目录编码删除虚拟目录对象
	 * 
	 * @param productSoftwareConfig
	 * @return
	 * @throws Exception
	 */
	public void delProductSoftwareConfig(String productSoftwareConfigCode) throws Exception;

	
	
	/**
	 * 读取某虚拟目录列表并形成Tree格式
	 * @param productSoftwareConfigCode 要读取的顶级目录code ,0为读取所有目录
	 * @return
	 */
	public List<ProductSoftwareConfig> getProductSoftwareConfigTree(String productSoftwareConfigCode) throws Exception;

	
	
	/**
	 * 根据新产品名称查询虚拟目录列表并形成Tree格式
	 * @param productSoftwareConfigName
	 * @return
	 */
	public List<ProductSoftwareConfig> getTreeByProductName(String productName) throws Exception;

	/**
	 * 读取某虚拟目录列表,层级排列
	 * @param productSoftwareConfigCode 要读取的顶级目录code ,0为读取所有目录
	 * @return
	 */
	public List<ProductSoftwareConfig> listProductSoftwareConfigLevel(String productSoftwareConfigCode) throws Exception;

	/**
	 * 添加修改虚拟目录时判断是否有相同的目录名称
	 * 
	 * @param productSoftwareConfig
	 * @return
	 * @throws Exception
	 */
	public boolean isSameName(ProductSoftwareConfig productSoftwareConfig) throws Exception;

	/**
	 * 根据parentCode查询的虚拟目录
	 * 
	 * @param parentCode
	 * @return
	 * @throws Exception
	 */
	public List<ProductSoftwareConfig> listProductSoftwareConfigByParentCode(String parentCode) throws Exception;
	
	/**
	 * 读取产品类型和最小销售单位并形成Tree格式
	 * @param productSoftwareConfigCode 要读取的顶级目录code ,0为读取所有目录
	 * @return
	 */
	public List<ProductSoftwareConfig> getProductMinSaleTree(String languageCode) throws Exception;

	
}
