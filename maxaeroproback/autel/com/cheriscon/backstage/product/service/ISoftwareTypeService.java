/**
 * @ClassName: ISoftwareTypeService
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-9 上午09:13:42
 *
 */
package com.cheriscon.backstage.product.service;

import java.util.List;

import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: ISoftwareTypeService
 * @Description: 软件类型管理
 * @author shaohu
 * @date 2013-1-9 上午09:13:42
 * 
 */
public interface ISoftwareTypeService {

	/**
	 * 
	* @Title: querySoftwareTypePage
	* @Description: 根据软件类型名称 查询数据
	* @param    softwareType 产品型号
	* @return Page    
	* @throws
	 */
	public Page pageSoftwareTypePage(SoftwareType softwareType,int pageNo, int pageSize) throws Exception;

	/**
	 * 
	* @Title: pageSoftwareTypePageNew
	* @Description: 根据软件类型名称 查询数据
	* @param    productSoftwareConfigCode 虚拟目录code
	* @param    softwareType
	* @return Page    
	* @throws
	 */
	public Page pageSoftwareTypePageNew(String productSoftwareConfigCode,SoftwareType softwareType,int pageNo, int pageSize) throws Exception;
	
	
	
	/**
	 * 
	* @Title: pageSoftwareTypePageNew
	* @Description: 根据软件类型名称 查询数据
	* @param    productTypeCode 产品类型
	* @param    softwareType
	* @return Page    
	* @throws
	 */
	public Page querySoftwareTypePageNew(String productTypeCode,SoftwareType softwareType,int pageNo, int pageSize) throws Exception;
	
	
	/**
	 * 
	* @Title: querySoftwareTypePage
	* @Description: 根据产品类型和软件名称查询软件
	* @param    
	* @return Page    
	* @throws
	 */
	public Page pageSoftwarePage(SoftwareType softwareType, int pageNo,int pageSize) throws Exception;
	
	
	/**
	 * @Title: listAll
	 * @Description: 查询所有软件类型
	 * @param
	 * @return List<SoftwareType>
	 * @throws
	 */
	public List<SoftwareType> listAll() throws Exception;

	/**
	 * @Title: getSoftwareTypeByProTypeCode
	 * @Description: 根据产品型号编码、查询软件类型信息
	 * @param productTypeCode
	 * @return List<SoftwareType>
	 * @throws
	 */
	public List<SoftwareType> getSoftwareTypeByProTypeCode(
			String productTypeCode) throws Exception;
	
	/**
	 * 
	* @Title: getSoftwareTypeByName
	* @Description: 根据功能软件类型名称和产品型号编码查询功能软件
	* @param    
	* @return SoftwareType    
	* @throws
	 */
	public SoftwareType getSoftwareTypeByName(String name,String proTypeCode) throws Exception;
	
	/**
	 * 
	* @Title: addSoftwareType
	* @Description: 添加功能软件类型信息
	* @param    
	* @return String    
	* @throws
	 */
	public String addSoftwareType(SoftwareType softwareType) throws Exception;
	
	/**
	 * 
	* @Title: getSoftwareTypeByCode
	* @Description: 根据软件类型code 查询软件类型
	* @param    code 软件类型code
	* @return SoftwareType    
	* @throws
	 */
	public SoftwareType getSoftwareTypeByCode(String code) throws Exception;
	
	
	/**
	 * 
	* @Title: delSoftwareType
	* @Description: 根据软件类型code 删除软件类型
	* @param    code 软件类型code
	* @return void    
	* @throws
	 */
	public void delSoftwareType(String code) throws Exception;
	
	/**
	 * 
	* @Title: updateSoftwareType
	* @Description: 修改软件类型数据
	* @param    softwareType
	* @return void    
	* @throws
	 */
	public void updateSoftwareType(SoftwareType softwareType) throws Exception;
	
	/**
	 * 
	* @Title: queryLanguageNameJson
	* @Description: 查询当前产品还未添加的语言包
	* @param    softwareType
	* @return void    
	* @throws
	 */
	public String queryLanguageNameJson(String code) throws Exception;
}
