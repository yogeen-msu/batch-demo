package com.cheriscon.backstage.product.service;

import java.util.List;

import com.cheriscon.backstage.product.vo.ProductTypeSerialPicVo;
import com.cheriscon.backstage.product.vo.ProductTypeVo;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.framework.database.Page;

public interface IProductTypeService {
	
	/**
	 * 分页查询产品型号
	* @Title: searchPage
	* @Description: 分页查询产品型号
	* @param    
	* @return Page    
	* @throws
	 */
	public Page searchPage(ProductTypeVo productTypeVo, int pageNo,
			int pageSize) throws Exception ;
	
	/**
	 * 
	 * @param productTypeVo
	 *            产品对象业务对象
	 * @return
	 * @throws Exception
	 */
	public List<ProductType> search(ProductTypeVo productTypeVo)
			throws Exception;

	/**
	 * 查询所有的产品型号
	 * @return
	 * @throws Exception
	 */
	public List<ProductType> listAll() throws Exception;

	/*
	 * public Page search(ProductTypeVo productTypeVo, int page, int pageSize)
	 * throws Exception;
	 */

	/**
	 * 添加产品型号
	 * 
	 * @param productType
	 * @return 产品型号编码
	 * @throws Exception
	 */
	public String add(ProductType productType) throws Exception;

	/**
	 * 
	 * @param productType
	 * @throws Exception
	 */
	public void update(ProductType productType) throws Exception;

	/**
	 * 根据产品型号编码查询产品型号对象
	 * 
	 * @param code
	 *            产品型号编码
	 * @return
	 * @throws Exception
	 */
	public ProductType getProductTypeByCode(String code) throws Exception;

	/**
	 * 根据产品型号编码删除产品型号对象
	 * 
	 * @param productType
	 * @return
	 * @throws Exception
	 */
	public int delete(ProductType productType) throws Exception;

	/**
	 * 获取当前产品型号未添加图片的语言数据
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public String queryLanguageJson(String code) throws Exception;

	/**
	 * 根据产品型号code获取 路径图片
	 * 
	 * @param code
	 *            产品型号code
	 * @return 产品型号code
	 * @throws Exception
	 */
	public List<ProductTypeSerialPicVo> getProductTypeSerialList(String code)
			throws Exception;

	/**
	 * 添加序列化图片路径
	 * 
	 * @param productType
	 *            产品型号对象
	 * @param serialPicVo
	 *            产品型号序列化图片
	 * @return
	 * @throws Exception
	 */
	public String addSerialPicPath(ProductType productType,
			ProductTypeSerialPicVo serialPicVo) throws Exception;

	/**
	 * 删除产品型号对应语言图片
	 * 
	 * @param code
	 *            产品型号
	 * @param languageCode
	 *            语言code
	 * @return
	 * @throws Exception
	 */
	public int delPicByLanguageCode(String code, String languageCode)
			throws Exception;

	/**
	 * 修改产品对应语言图片
	 * 
	 * @param productType
	 *            产品型号
	 * @param serialPicVo
	 *            产品序列化图片
	 * @return
	 * @throws Exception
	 */
	public int updateLanguagePic(ProductType productType,
			ProductTypeSerialPicVo serialPicVo) throws Exception;
	
	
	/**
	 * 查询经销商的可卖产品列表
	 * @param sealerCode
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	public Page queryProductTypeList(String sealerCode, int pageNo,
			int pageSize) throws Exception ;
	
	/**
	 * 查询经销商的可卖产品记录数
	 * @param sealerCode
	 * @return
	 * @throws Exception
	 */
	public int queryProductTypeCount(String sealerCode) throws Exception ;
	
	/**
	 * 
	* @Title: isRepeat
	* @Description: 判断名称是否重复
	* @param    
	* @return boolean true 名称重复 , false 不重复   
	* @throws
	 */
	public boolean isRepeat(String code,String productName) throws Exception ;
}
