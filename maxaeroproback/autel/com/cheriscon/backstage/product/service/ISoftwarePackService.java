/**
*/ 
package com.cheriscon.backstage.product.service;

import com.cheriscon.common.model.SoftwarePack;

/**
 * @ClassName: ISoftwarePack
 * @Description: 软件基础包业务类
 * @author shaohu
 * @date 2013-1-11 下午04:09:54
 * 
 */
public interface ISoftwarePackService {

	/**
	 * 
	* @Title: addSoftwarePack
	* @Description: 添加软件基础包信息
	* @param	softwarePack 软件基础包信息
	* @return void    
	* @throws
	 */
	public void addSoftwarePack(SoftwarePack softwarePack) throws Exception;
	
	/**
	 * 
	* @Title: delSoftwarePackByVersionCode
	* @Description: 根据软件版本编码删除软件基础语言包
	* @param   versionCode 软件版本编码 
	* @return void    
	* @throws
	 */
	public void delSoftwarePackByVersionCode(String versionCode) throws Exception;
	
	/**
	 * 
	* @Title: delSoftwarePack
	* @Description: 根据语言包编码删除语言包
	* @param    
	* @return void    
	* @throws
	 */
	public void delSoftwarePack(String code) throws Exception;
	
	/**
	 * 
	* @Title: updateSoftwarePack
	* @Description: 修改软件基础包
	* @param    
	* @return void    
	* @throws
	 */
	public void updateSoftwarePack(SoftwarePack softwarePack) throws Exception;
	
}
