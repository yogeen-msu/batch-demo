/**
 */
package com.cheriscon.backstage.product.service;

import java.util.List;

import com.cheriscon.backstage.product.vo.SoftwareName;
import com.cheriscon.backstage.product.vo.SoftwareVersionPageVo;
import com.cheriscon.backstage.product.vo.SoftwareVersionSearch;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.common.model.SoftwarePack;
import com.cheriscon.common.model.SoftwareVersion;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: ISoftwareVersionService
 * @Description: 软件版本信息业务类
 * @author shaohu
 * @date 2013-1-10 下午07:37:32
 * 
 */
public interface ISoftwareVersionService {

	/**
	 * 
	* @Title: pageSoftwareVersion
	* @Description: 软件版本名称
	* @param    
	* @return Page    
	* @throws
	 */
	public Page pageSoftwareVersion(SoftwareVersionPageVo versionPageVo,int pageNo,int pageSize) throws Exception;
	
	/**
	 * 
	* @Title: addSoftwareVersionDetail
	* @Description: 添加软件版本
	* @param    softwareVersion	软件版本信息
	* @param softwarePack	软件基础包信息
	* @param languagePacks	软件语言包信息
	* @return void    
	* @throws
	 */
	public void addSoftwareVersionDetail(SoftwareVersion softwareVersion,
			SoftwarePack softwarePack, List<LanguagePack> languagePacks)
			throws Exception;
	
	/**
	 * 
	* @Title: delSoftwareVersion
	* @Description: 删除软件版本
	* @param  code 软件版本编码  
	* @return void    
	* @throws
	 */
	public void delSoftwareVersion(String code) throws Exception;
	
	/**
	 * 
	* @Title: delSoftwareVersion
	* @Description: 批量删除软件版本
	* @param  code 软件版本编码  
	* @return void    
	* @throws
	 */
	public void batchDelSoftwareVersion(String code) throws Exception;
	
	/**
	 * 
	* @Title: releaseSoftwareVersion
	* @Description: 发布软件版本
	* @param    code 软件版本编码
	* @param releaseState 发布状态
	* @return void    
	* @throws
	 */
	public void releaseSoftwareVersion(String code,Integer releaseState) throws Exception;
	
	/**
	 * 
	* @Title: pageSoftwareVersionRelease
	* @Description: 版本发布数据
	* @param    
	* @return List<SoftwareVersionVo>    
	* @throws
	 */
	public Page pageSoftwareVersionRelease(SoftwareVersionSearch search,int pageNo, int pageSize) throws Exception;
	
	/**
	 * 
	* @Title: pageSoftwareVersionRelease
	* @Description: 版本发布数据
	* @param    
	* @return List<SoftwareVersionVo>    
	* @throws
	 */
	public Page pageSoftwareVersionReleaseNew(SoftwareVersionSearch search,int pageNo, int pageSize) throws Exception;
	
	/**
	 * 
	* @Title: updateSoftversion
	* @Description: TODO
	* @param    
	* @return void    
	* @throws
	 */
	public  void updateSoftversion(SoftwareVersion softwareVersion,SoftwarePack softwarePack) throws Exception;
	
	/**
	 * 
	* @Title: getAvailableLanguage
	* @Description: 获取版本对应可用语言
	* @param    
	* @return List<Language>    
	* @throws
	 */
	public List<Language> getAvailableLanguage(String vsersion) throws Exception;
	
	/**
	 * 根据软件类型code查询软件版本信息
	 * @param softTypeCode
	 * @return
	 * @throws Exception
	 */
	public List<SoftwareVersion> getSoftwareVersionBySoftTypeCode(String softTypeCode) throws Exception;
	
	//根据产品code查询当前产品的最新版本
    public List<SoftwareVersion> querySoftVersionByProCode(String proCode) throws Exception;
	
}
