package com.cheriscon.backstage.product.service;

import com.cheriscon.backstage.product.vo.BetaProduct;
import com.cheriscon.framework.database.Page;

public interface IBataProductService {
	
	/**
	 * 分页查询bata产品
	 * 
	 **/
	public Page pageBataProduct(BetaProduct bata,int pageNo, int pageSize) throws Exception;
	
	
	/**
	 * 增加Bata产品
	 * 
	 **/
   public void addBataProduct(BetaProduct bata) throws Exception;
   
   
   /**
	 * 删除Bata产品
	 * 
	 **/
  public void deleteBataProduct(String code) throws Exception;
  
  /**
   * 根据serialNo查询bata产品
   * **/
  public BetaProduct getBataProductBySerialNo(String serialNo) throws Exception;
   
   
}
