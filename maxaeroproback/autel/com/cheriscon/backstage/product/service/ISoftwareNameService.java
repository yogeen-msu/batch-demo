package com.cheriscon.backstage.product.service;

import java.util.List;

import com.cheriscon.backstage.product.vo.ProductTypeConfig;
import com.cheriscon.backstage.product.vo.SoftwareName;

public interface ISoftwareNameService {
	
	public List<SoftwareName> getProductTypeConfig(String Code) throws Exception;
	
	public void save(SoftwareName config) throws Exception;
	
	public void delete(String code)throws Exception;

	public void updateName(String code,String name)throws Exception;
	
	public void save(List<SoftwareName> softwareNameList) throws Exception;
}
