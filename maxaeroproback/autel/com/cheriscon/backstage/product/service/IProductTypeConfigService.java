package com.cheriscon.backstage.product.service;

import java.util.List;

import com.cheriscon.backstage.product.vo.ProductTypeConfig;

public interface IProductTypeConfigService {
	
	public List<ProductTypeConfig> getProductTypeConfig(String proTypeCode) throws Exception;
	
	public void save(ProductTypeConfig config) throws Exception;
	
	public void delete(String code)throws Exception;

}
