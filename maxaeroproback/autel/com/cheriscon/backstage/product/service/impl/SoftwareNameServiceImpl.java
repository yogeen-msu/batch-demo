package com.cheriscon.backstage.product.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.product.service.IProductTypeConfigService;
import com.cheriscon.backstage.product.service.ISoftwareNameService;
import com.cheriscon.backstage.product.vo.ProductTypeConfig;
import com.cheriscon.backstage.product.vo.SoftwareName;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
@SuppressWarnings("rawtypes")
@Service
public class SoftwareNameServiceImpl extends BaseSupport implements ISoftwareNameService {

	@Override
	public List<SoftwareName> getProductTypeConfig(String proTypeCode)
			throws Exception {
		String sql="select a.code as code,a.softwareCode as softwareCode,a.languageCode as languageCode,a.softwareName as softwareName ,b.name as languageName from DT_SoftwareName a,DT_Language b where a.languageCode=b.code and a.softwareCode=?";
		return this.daoSupport.queryForList(sql.toString(), SoftwareName.class,proTypeCode);
	}

	@Override
	public void save(SoftwareName config) throws Exception {
		this.daoSupport.insert("DT_SoftwareName", config);

	}

	public void save(List<SoftwareName> softwareNameList) throws Exception{
		String sql="delete from DT_SoftwareName where softwareCode=?";
		this.daoSupport.execute(sql, softwareNameList.get(0).getSoftwareCode());
		for(int i=0;i<softwareNameList.size();i++){
			SoftwareName name=softwareNameList.get(i);
			String code = DBUtils.generateCode(DBConstant.PRODUCT_NAME_LANGUAGE_CODE);
			name.setCode(code);
			this.daoSupport.insert("DT_SoftwareName", name);
		}
				
	}
	
	
	@Override
	public void delete(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from DT_SoftwareName where code = ?");
		this.daoSupport.execute(sql.toString(),code);
	}
	@Override
	public void updateName(String code,String name)throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("update DT_SoftwareName set softwareName=? where code = ?");
		this.daoSupport.execute(sql.toString(), name,code);
	}

}
