package com.cheriscon.backstage.product.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.product.service.IProductTypeConfigService;
import com.cheriscon.backstage.product.vo.ProductTypeConfig;
import com.cheriscon.cop.sdk.database.BaseSupport;
@SuppressWarnings("rawtypes")
@Service
public class ProductTypeConfigServiceImpl extends BaseSupport implements IProductTypeConfigService {

	@Override
	public List<ProductTypeConfig> getProductTypeConfig(String proTypeCode)
			throws Exception {
		String sql="select a.code as code,a.proTypeCode as proTypeCode,a.languageCode as languageCode,a.name as name ,b.name as languageName from DT_ProductTypeConfig a,DT_Language b where a.languageCode=b.code and a.proTypeCode=?";
		return this.daoSupport.queryForList(sql.toString(), ProductTypeConfig.class,proTypeCode);
	}

	@Override
	public void save(ProductTypeConfig config) throws Exception {
		this.daoSupport.insert("DT_ProductTypeConfig", config);

	}

	@Override
	public void delete(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from DT_ProductTypeConfig where code = ?");
		this.daoSupport.execute(sql.toString(),code);
	}

}
