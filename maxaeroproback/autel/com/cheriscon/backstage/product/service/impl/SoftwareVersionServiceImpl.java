/**
 */
package com.cheriscon.backstage.product.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.product.constant.ProductConstant;
import com.cheriscon.backstage.product.mapper.SoftwareVersionVoMapper;
import com.cheriscon.backstage.product.service.ILanguagePackService;
import com.cheriscon.backstage.product.service.ISoftwareNameService;
import com.cheriscon.backstage.product.service.ISoftwarePackService;
import com.cheriscon.backstage.product.service.ISoftwareTypeService;
import com.cheriscon.backstage.product.service.ISoftwareVersionService;
import com.cheriscon.backstage.product.vo.SoftwareName;
import com.cheriscon.backstage.product.vo.SoftwareVersionPageData;
import com.cheriscon.backstage.product.vo.SoftwareVersionPageVo;
import com.cheriscon.backstage.product.vo.SoftwareVersionResult;
import com.cheriscon.backstage.product.vo.SoftwareVersionSearch;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.common.model.Message;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.model.SoftwarePack;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.model.SoftwareVersion;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

/**
 * @ClassName: SoftwareVersionServiceImpl
 * @Description: 软件版本信息业务类
 * @author shaohu
 * @date 2013-1-10 下午07:38:00
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class SoftwareVersionServiceImpl extends BaseSupport implements
		ISoftwareVersionService {

	private static String TABLE_NAME = "DT_SoftwareVersion";

	@Resource
	private ISoftwarePackService softwarePackService;

	@Resource
	private IMessageService messageService;
	
	@Resource
	private ILanguagePackService languagePackService;
	
	@Resource
	private ILanguageService languageService;
	
    @Resource
    private ISoftwareTypeService softwareTypeService;
    
    @Resource
    private ISoftwareNameService softwareNameService;
	

	/**
	 * <p>
	 * Title: addSoftwareVersion
	 * </p>
	 * <p>
	 * Description: 添加软件版本
	 * </p>
	 * 
	 * @param softwareVersion
	 * @param softwarePack
	 * @param languagePacks
	 * @throws Exception
	 */
	@Override
	@Transactional
	public void addSoftwareVersionDetail(SoftwareVersion softwareVersion,
			SoftwarePack softwarePack, List<LanguagePack> languagePacks)
			throws Exception {
		// 版本创建时间
		String createDate = DateUtil.toString(new Date(), "yyyyMMddHHmmssSSSS");
		String code = DBUtils.generateCode(DBConstant.SOFTWARE_VERSION_CODE);
		softwareVersion.setCode(code);
		softwareVersion.setCreateDate(createDate);

		softwareVersion.setReleaseState(ProductConstant.NOT_RELEASE);
		this.daoSupport.insert(TABLE_NAME, softwareVersion);
		softwarePack.setSoftwareVersionCode(code);
		this.softwarePackService.addSoftwarePack(softwarePack);

		if (null != languagePacks && languagePacks.size() > 0) {
			for (LanguagePack languagePack : languagePacks) {
				languagePack.setSoftwareVersionCode(code);
				this.languagePackService.addLanguagePack(languagePack);
				/*
				Message message = new Message();
				
				//消息类型Code
				//message.setMsgTypeCode("");
				//标题
				message.setTitle(languagePack.getTitle());
				//语言编码
				message.setLanguageCode(languagePack.getLanguageTypeCode());
				//语言名称
				message.setLanguageName(languagePack.getLanguageName());
				//排序
				message.setSort(1);
				//0: 未发布   1:已发布
				message.setStatus(0);
				//0: 未弹出    1:弹出
				message.setIsDialog(1);
				//用户类型     1：个人用户 2：经销商
				message.setUserType(1);
				message.setCreatTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
				this.messageService.saveMessage(message);
				 */
			}
		}

	}

	/**
	 * 
	 * @Title: addSoftwareVersion
	 * @Description: 添加软件版本信息
	 * @param
	 * @return void
	 * @throws
	 */
	public void addSoftwareVersion(SoftwareVersion softwareVersion)
			throws Exception {
		// 版本创建时间
		String createDate = DateUtil.toString(new Date(), "yyyyMMddHHmmssSSSS");
		String code = DBUtils.generateCode(DBConstant.SOFTWARE_VERSION_CODE);
		softwareVersion.setCode(code);
		softwareVersion.setCreateDate(createDate);
		softwareVersion.setReleaseState(ProductConstant.NOT_RELEASE);
		this.daoSupport.insert(TABLE_NAME, softwareVersion);
	}

	/**
	 * <p>
	 * Title: delSoftwareVersion
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param code
	 * @throws Exception
	 */
	@Override
	@Transactional
	public void delSoftwareVersion(String code) throws Exception {

		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code=?");

		this.softwarePackService.delSoftwarePackByVersionCode(code);
		this.languagePackService.delLanguagePackByVersionCode(code);
		this.daoSupport.execute(sql.toString(), code);

	}
	
	@Transactional
	public void batchDelSoftwareVersion(String codes) throws Exception {
		if(StringUtils.isNotBlank(codes)){
			String [] c = codes.split(",");
			for(String s : c){
				StringBuffer sql = new StringBuffer();
				sql.append("delete from ").append(TABLE_NAME).append(" where code=?");
				
				this.softwarePackService.delSoftwarePackByVersionCode(s);
				this.languagePackService.delLanguagePackByVersionCode(s);
				this.daoSupport.execute(sql.toString(), s);
			}
		}
		

	}
	
	
	/**
	 * <p>
	 * Title: releaseSoftwareVersion
	 * </p>
	 * <p>
	 * Description: releaseState 为1时，修改为发布状态 并添加发布时间 releaseState 为0时 修改发布状态
	 * </p>
	 * 
	 * @param code
	 *            版本编码
	 * @param releaseState
	 *            发布状态
	 * @throws Exception
	 */
	@Override
	@Transactional
	public void releaseSoftwareVersion(String code, Integer releaseState)
			throws Exception {
		String releaseDate = DateUtil
				.toString(new Date(), "yyyyMMddHHmmssSSSS");
		StringBuffer sql = new StringBuffer();

		// StringBuffer condition = new StringBuffer();

		String[] codes = code.trim().split(" ");

		// for(int i = 0 ; i < codes.length ; i ++){
		// condition.append("'").append(codes[i]).append("'").append(" ");
		// }
		// String conditionSql = condition.toString().trim().replaceAll(" ",
		// ",");
		for (String c : codes) {
			sql = new StringBuffer();
			
			SoftwareVersion version=this.getSoftwareVersionByCode(c);
			SoftwareType softType=softwareTypeService.getSoftwareTypeByCode(version.getSoftwareTypeCode());
			// 改为未发布
			if (releaseState == ProductConstant.NOT_RELEASE) {
				sql.append("update ").append(TABLE_NAME)
						.append(" set releaseState = ? ")
						.append("where code = ?");
				this.daoSupport.execute(sql.toString(), releaseState, c.trim());
				
				messageService.delMessageBySoftCodeAndVersion(softType.getCode());
				
			} else {
				
				sql.append("update ").append(TABLE_NAME)
						.append(" set releaseDate=? ").append(",")
						.append(" releaseState=? ").append("where code = ?");
				this.daoSupport.execute(sql.toString(), releaseDate,
						releaseState, c.trim());
				if(softType.getSoftType().equals("1")){  //付费诊断程序发送推送消息
					this.updateVersionTitle(version, c); 
					this.publishMessage(version.getSoftwareTypeCode(), c, releaseDate);
				}
				
			}
		}
	}

	public boolean updateVersionTitle(SoftwareVersion version,String softwareVersionCode) throws Exception{
		String dateStr=DateUtil.toString(new Date(), "YYYY-MM-dd");
		
		List<SoftwareName> list=softwareNameService.getProductTypeConfig(version.getSoftwareTypeCode());
		for(int i=0 ;i<list.size();i++){
			SoftwareName softName=list.get(i);
			String title=softName.getSoftwareName().trim()+" " +version.getVersionName().trim()+" " +dateStr;
			String sql="update DT_LanguagePack set title=? where softwareVersionCode=? and languageTypeCode=?";
			this.daoSupport.execute(sql, title,version.getCode(),softName.getLanguageCode());
		}
		
		return true;
		
	}
	
	
	/**
	 * @Title: publishMessage
	 * @Description: 将升级信息发布到站内消息,通过软件类型Code进行关联
	 * 1.每条功能软件只写一个版本，因此先删除已经有的消息，在添加
	 * 
	 * @param softwareTypeCode 软件类型Code，versionCode 软件版本Code，state状态
	 * @return boolean
	 * @throws
	 * 
	 * ***/
	public boolean publishMessage(String softwareTypeCode,String versionCode,String state) throws Exception{
		
		boolean flag=false;
		try{
			int returnNum=messageService.delMessageBySoftCodeAndVersion(softwareTypeCode);
			if(returnNum==0){
				//1.需要查询出中文(lag201304221103300175)和英文(lag201304221104540500)升级公告和更新内容,增加西班牙语和德语20151520
				List<LanguagePack> languagePack=languagePackService.getLanguagePackByVersionCode(versionCode);
				for(int i=0;i<languagePack.size();i++){
					LanguagePack pack=languagePack.get(i);
					if(!StringUtil.isEmpty(pack.getMemo())){
					
						if(pack.getLanguageTypeCode().equals("lag201304221103300175") || pack.getLanguageTypeCode().equals("lag201304221104540500")
								|| pack.getLanguageTypeCode().equals("lag201305051503240411") ||
								pack.getLanguageTypeCode().equals("lag201305051504580071")){
							Message message=new Message();
							message.setContent(pack.getMemo());
							message.setTitle(pack.getTitle());
							message.setCreator("admin");
							message.setIsDialog(FrontConstant.USER_MESSAGE_IS_YES_DIALOG); //是否弹出消息
							message.setLanguageCode(pack.getLanguageTypeCode());
							message.setMsgTypeCode("mst201304251043350375"); //消息类型
							message.setSoftwareTypeCode(softwareTypeCode);
							message.setSort(0);
							message.setStatus(FrontConstant.SOFTWARE_VERSION_RELEASE_IS_STATE);  //消息发布状态
							message.setUserType(FrontConstant.CUSTOMER_USER_TYPE); //个人客户
							message.setVersionCode(versionCode);
							messageService.saveMessage(message);
						}
					}
				}
			
			}else{
				
				flag=true;
			}
		}catch(Exception e){
			flag=false;
		}
		
		return flag;
	}
	
	
	/**
	 * <p>
	 * Title: pageSoftwareVersionRelease
	 * </p>
	 * <p>
	 * Description: 分页查询
	 * </p>
	 * 
	 * @param search
	 * @return
	 * @throws Exception
	 */
	public Page pageSoftwareVersionRelease(SoftwareVersionSearch search,
			int pageNo, int pageSize) throws Exception {

		StringBuffer sql = new StringBuffer();

		sql.append("select ds.name,tsv.* from DT_PRODUCT_SOFTWARE_CONFIG dp right join DT_SoftwareType ds on dp.code = ds.code" +
				" right join (select t.code versionCode ,t.versionName,t.softwareTypeCode,t.releaseState from DT_SoftwareVersion t" +
				" right join (select d.softwareTypeCode ,MAX(id) id from DT_SoftwareVersion d  group by d.softwareTypeCode) td on t.id = td.id) tsv" +
				" on ds.code = tsv.softwareTypeCode");
		sql.append(" where dp.parentCode = ? ");
		
		//输入框输入跳转页数时，获取参数重复，产生","
		if(!StringUtil.isEmpty(search.getParentCode()) ){
			if(search.getParentCode().indexOf(",")>-1){
				search.setParentCode(search.getParentCode().substring(0, search.getParentCode().indexOf(",")));
			}
			
		}
		
		if (null != search) {
			// 功能软件名称
			if (StringUtils.isNotBlank(search.getName())) {
				sql.append(" and ").append(
						" ds.name like '%" + search.getName() + "%'");
			}
			// 发布状态
			if (search.getReleaseState() != null) {
				sql.append(" and ").append(
						" releaseState = " + search.getReleaseState());
			}
		}
		sql.append(" order by ds.name desc");
		return daoSupport.queryForPage(sql.toString(), pageNo, pageSize,
				new SoftwareVersionVoMapper(), search.getParentCode());
		// /return daoSupport.queryForPage(sql.toString(), new
		// SoftwareVersionVoMapper(),search.getProductTypeCode());
	}
	
	
	public Page pageSoftwareVersionReleaseNew(SoftwareVersionSearch search,int pageNo, int pageSize) throws Exception{
		StringBuffer sql = new StringBuffer();

		sql.append("select ds.name,tsv.* from DT_PRODUCT_SOFTWARE_CONFIG dp right join DT_SoftwareType ds on dp.code = ds.code" +
				" right join (select t.code versionCode ,t.versionName,t.softwareTypeCode,t.releaseState from DT_SoftwareVersion t" +
				" right join (select d.softwareTypeCode ,MAX(id) id from DT_SoftwareVersion d  group by d.softwareTypeCode) td on t.id = td.id) tsv" +
				" on ds.code = tsv.softwareTypeCode");
		sql.append(" where ds.proTypeCode = ? ");
		
		//输入框输入跳转页数时，获取参数重复，产生","
		/*if(!StringUtil.isEmpty(search.getParentCode()) ){
			if(search.getParentCode().indexOf(",")>-1){
				search.setParentCode(search.getParentCode().substring(0, search.getParentCode().indexOf(",")));
			}
			
		}*/
		
		if (null != search) {
			// 功能软件名称
			if (StringUtils.isNotBlank(search.getName())) {
				sql.append(" and ").append(
						" ds.name like '%" + search.getName() + "%'");
			}
			// 发布状态
			if (search.getReleaseState() != null) {
				sql.append(" and ").append(
						" releaseState = " + search.getReleaseState());
			}
		}
		sql.append(" order by ds.name desc");
		return daoSupport.queryForPage(sql.toString(), pageNo, pageSize,
				new SoftwareVersionVoMapper(), search.getProductTypeCode());
		
	}

	/**
	 * <p>
	 * Title: pageSoftwareVersion
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param versionName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page pageSoftwareVersion(SoftwareVersionPageVo versionPageVo, int pageNo, int pageSize)
			throws Exception {
		pageSize=10;
		StringBuffer versionCountSql = new StringBuffer();
		versionCountSql.append("select * from DT_SoftwareVersion where 1=1 ");
		StringBuffer pageSql = new StringBuffer();
		pageSql.append("select ta.*,dp.code as languagePackCode ,dp.downloadPath,dp.testPath,dp.languageTypeCode,dp.memo,dp.title,dg.name as languagename from (")
				.append("select * from ( select  dv.*,dc.code as softwarePackCode , dc.downloadPath as softPackPath from DT_SoftwareVersion as dv left join DT_SoftwarePack dc on dv.code = dc.softwareVersionCode where 1=1 ");
		if(null != versionPageVo){
			if (StringUtils.isNotBlank(versionPageVo.getVersionName())) {
				versionCountSql.append(" and versionName like '%")
						.append(versionPageVo.getVersionName().trim()).append("%'");
				pageSql.append(" and versionName like '%")
						.append(versionPageVo.getVersionName().trim()).append("%'");
			}
			if(StringUtils.isNotBlank(versionPageVo.getSoftwareTypeCode())){
				versionCountSql.append(" and softwareTypeCode = ").append("'").append(versionPageVo.getSoftwareTypeCode().trim()).append("'");
				pageSql.append(" and softwareTypeCode = ").append("'").append(versionPageVo.getSoftwareTypeCode().trim()).append("'");
				
			}
		}

		pageSql.append(" order by dv.id desc) tb ");
		pageSql.append(") ta")
				.append(" left join ")
				.append(" DT_LanguagePack dp  on dp.softwareVersionCode = ta.code left join DT_Language dg on dg.code = dp.languageTypeCode order by ta.id desc");

		List<SoftwareVersionResult> versionResults = this.daoSupport
				.queryForList(pageSql.toString(), SoftwareVersionResult.class);
		versionCountSql.append(" order by id desc");
		List<SoftwareVersion> list = this.daoSupport.queryForList(
				versionCountSql.toString(), SoftwareVersion.class);

		Page page = new Page(pageNo, list.size(), 10,
				this.convertPageData(versionResults));

		return page;
	}

	@SuppressWarnings("unchecked")
	public List<SoftwareVersionPageData> convertPageData(List<SoftwareVersionResult> versionResults)
			throws Exception {

		List<SoftwareVersionPageData> result = new ArrayList<SoftwareVersionPageData>();

		Map versionMap = new HashMap();
		Map languagePackMap = new HashMap();

		List<LanguagePack> packs = null;

		for (SoftwareVersionResult v : versionResults) {
			String code = v.getCode();
			if (!versionMap.containsKey(code)) {
				SoftwareVersion version = new SoftwareVersion();
				BeanUtils.copyProperties(v, version);
				versionMap.put(code, version);

			}

			LanguagePack languagePack = new LanguagePack();
			BeanUtils.copyProperties(v, languagePack);
			if (languagePackMap.containsKey(code)) {
				packs = (List<LanguagePack>) languagePackMap.get(code);
			} else {
				packs = new ArrayList<LanguagePack>();
			}
			if(StringUtils.isNotBlank(languagePack.getLanguageName()) ){
				packs.add(languagePack);
			}
			languagePackMap.put(code, packs);
		}

		Set keys = versionMap.keySet();

		for (Object key : keys) {
			SoftwareVersionPageData data = new SoftwareVersionPageData();
			data.setSoftwareVersion((SoftwareVersion) versionMap.get(key));
			data.setLanguagePacks((List<LanguagePack>) languagePackMap.get(key));
			result.add(data);
		}
		
		
		int size = result.size();
		if(size > 0 ){
			Integer [] ids = new Integer[size];
			Map<Integer,SoftwareVersionPageData> map = new HashMap<Integer,SoftwareVersionPageData>();
			for(int i=0 ; i< size ; i ++){
				SoftwareVersionPageData data = result.get(i);
				SoftwareVersion version = result.get(i).getSoftwareVersion();
				ids[i] = version.getId();
				map.put(version.getId(), data);
			}
			Arrays.sort(ids);
			if(ids.length > 0 ){
				result.clear();
				int len = ids.length ;
				for(int i = len -1 ; i>=0 ; i-- ){
					result.add(map.get(ids[i]));
				}
			}
		}
		
		return result;
	}

	/**
	  * <p>Title: updateSoftversion</p>
	  * <p>Description: 修改软件版本 </p>
	  * @param softwareVersion
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void updateSoftversion(SoftwareVersion softwareVersion,SoftwarePack softwarePack)
			throws Exception {
		this.daoSupport.update(TABLE_NAME, softwareVersion, " code='"+softwareVersion.getCode()+"'");
		this.softwarePackService.updateSoftwarePack(softwarePack);
	}

	/**
	  * <p>Title: getAvailableLanguage</p>
	  * <p>Description: </p>
	  * @param vsersion
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<Language> getAvailableLanguage(String vsersionCode) throws Exception {
		List<Language> list = new ArrayList<Language>();
		List<Language> languages = languageService.queryAllLanguage();
		if(null == languages || languages.size() == 0){
			return list;
		}
		
	    List<LanguagePack> languagePacks =	languagePackService.getLanguagePackByVersionCode(vsersionCode);
	    if(null == languagePacks || languagePacks.size() == 0){
	    	return languages;
	    }
	    
	    Map<String , LanguagePack> map = this.convertListToMap(languagePacks);
	    
	    for(Language language : languages){
			if(!map.containsKey(language.getCode()))
				list.add(language);
		}
		return list;
	    
	}
	
	private Map<String , LanguagePack> convertListToMap(List<LanguagePack> languagePacks) throws Exception{
		Map<String , LanguagePack> map = new HashMap<String , LanguagePack>();
		for(LanguagePack languagePack : languagePacks){
			map.put(languagePack.getLanguageTypeCode(), languagePack);
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SoftwareVersion> getSoftwareVersionBySoftTypeCode(
			String softTypeCode) throws Exception 
	{
		StringBuffer versionCountSql = new StringBuffer();
		versionCountSql.append("select * from DT_SoftwareVersion where softwareTypeCode='").append(softTypeCode).append("'");
		
		return this.daoSupport.queryForList(versionCountSql.toString(), SoftwareVersion.class);
	}
	
	public List<SoftwareVersion> querySoftVersionByProCode(String proCode) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select e.softwareTypeCode as softwareTypeCode,") ;
		sql.append(" (select versionName from DT_SoftwareVersion ") ; 
		sql.append(" where releaseState=1 and e.softwareTypeCode=softwareTypeCode  order by releaseDate desc limit 0,1) as versionName") ;
		sql.append(" from dt_productinfo a ,") ;
		sql.append(" DT_SaleContract b,") ;
		sql.append(" DT_SaleConfig c ,") ;
		sql.append(" DT_MinSaleUnitSaleCfgDetail d,") ;
		sql.append(" DT_MinSaleUnitSoftwareDetail e,") ;
		sql.append(" DT_SoftwareType f") ;
		sql.append(" where a.saleContractCode=b.code") ;
		sql.append(" and b.saleCfgCode=c.code") ;
		sql.append(" and c.code=d.saleCfgCode") ;
		sql.append(" and d.minSaleUnitCode=e.minSaleUnitCode") ;
		sql.append(" and e.softwareTypeCode=f.code") ;
		sql.append(" and a.code=?") ;
		sql.append(" order by f.name ") ;
		
		@SuppressWarnings("unchecked")
		List<SoftwareVersion> list=this.daoSupport.queryForList(sql.toString(), SoftwareVersion.class, proCode);
		return list;
	}

	public SoftwareVersion getSoftwareVersionByCode(String code) throws Exception{
		String sql="select * from DT_SoftwareVersion where code=?";
		return (SoftwareVersion) this.daoSupport.queryForObject(sql, SoftwareVersion.class, code);
		
	}
	
}
