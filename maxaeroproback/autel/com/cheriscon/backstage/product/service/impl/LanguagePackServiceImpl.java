package com.cheriscon.backstage.product.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.product.service.ILanguagePackService;
import com.cheriscon.backstage.product.vo.SoftwareName;
import com.cheriscon.backstage.product.vo.VehicleTypeMapper;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.model.SoftwareVersion;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.constant.FrontConstant;

/**
 * @Description: 软件语言包业务类
 * @author shaohu
 * @date 2013-1-10 下午07:44:04
 * 
 */
@Service
public class LanguagePackServiceImpl extends BaseSupport<LanguagePack> implements ILanguagePackService {

	private final static String TABLE_NAME = "DT_LanguagePack";
	
//	@Resource private ILanguageService languageService;
	
	/**
	  * <p>Title: listAll</p>
	  * <p>Description: 查询软件语言包的集合</p>
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<LanguagePack> listAll() throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME);
		return this.daoSupport.queryForList(sql.toString(), LanguagePack.class);
	}
	
	public Page searchPage(Map<String,Object> params,int pageNo,int pageSize) throws Exception {
		
		
		
		
		Language language = (Language)CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
		Locale locale = null;
		String carSignPath="";
		
		 String filePath=FrontConstant.getProperValue("download.url.other") ;
		
		if (language != null) {
			locale = new Locale(language.getLanguageCode(),language.getCountryCode());
			if(language.getLanguageCode().toLowerCase().equals("zh")){
				filePath=FrontConstant.getProperValue("download.url.china");
				carSignPath="old";
			}else{
				carSignPath="new";
			}
			
		} else {
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			String languageCode =request.getLocale().getLanguage();
			if(languageCode.toLowerCase().equals("zh")){
				locale = new Locale("zh", "CN");
				filePath=FrontConstant.getProperValue("download.url.china");
				carSignPath="old";
			}else{
				locale = new Locale("en", "US");
				carSignPath="new";
			}
		}
		
		/*StringBuffer newVersionSql = new StringBuffer();
		newVersionSql.append("select tb3.id id,tb3.softwareTypeCode softwareTypeCode,tb3.releaseState releaseState,tb3.versionName versionName,tb3.code code");
		newVersionSql.append(" from "+swvTbl+" tb3 inner join (");
		newVersionSql.append(" select  tb1.softwareTypeCode as stc,MAX(releaseDate) as rd");
		newVersionSql.append(" from "+swvTbl + " tb1 group by tb1.softwareTypeCode) tb2 ");
		newVersionSql.append(" on tb3.softwareTypeCode = tb2.stc and tb3.releaseDate = tb2.rd");
		
		StringBuffer sql = new StringBuffer();
		sql.append("select ");
		sql.append(" swt.id no ,swt.carSignPath path,swv.versionName vname,c.softwareName sname,pt.name proName,lpk.testPath testPath  ");
		sql.append(" from "+swtTbl+" swt");
		sql.append(" inner join (select a.softwareCode,a.softwareName from DT_SoftwareName a,dt_Language b where a.languageCode=b.code and  b.countryCode = '"+locale.getCountry()+"' and b.languageCode='"+locale.getLanguage()+"') c on  c.softwareCode=swt.code");
		//sql.append(" left join "+ptTbl+" pt on swt.proTypeCode = pt.code ");
		sql.append(" inner join( select b.productTypeCode as code, a.name as name from dt_productforsealer a ,dt_productforsealerconfig b where a.code=b.productforsealercode and  a.code='"+params.get("proTypeCode")+"') pt on swt.proTypeCode = pt.code ");
		sql.append(" left join ("+newVersionSql.toString()+") swv on swv.softwareTypeCode=swt.code ");
		sql.append(" left join "+lapkTbl+" lpk on lpk.softwareVersionCode=swv.code ");
		sql.append(" left join "+lanTbl+" lan on lpk.languageTypeCode = lan.code ");
		sql.append(" where 1=1 ");*/
		
		StringBuffer sql = new StringBuffer();
		if(carSignPath.equals("new")){
			sql.append("select a.id as rowNum,a.newCarSignPath as path , c.versionName as vname, n.softwareName as sname, ");
		}else{
			sql.append("select a.id as rowNum,a.carSignPath as path , c.versionName as vname, n.softwareName as sname, ");
		}
		
		sql.append(" p.name as proName, b.testPath as testPath,  m.name  as functionName ");
		sql.append(" from DT_SoftwareType a inner join DT_SoftwareVersion c on a.code=c.softwareTypeCode and a.softType='1'  ");
		sql.append(" inner join DT_LanguagePack b on c.code=b.softwareVersionCode");
		sql.append(" inner join DT_SoftWareName n on a.code=n.softwareCode");
		sql.append(" inner join DT_Language l on l.code=b.languageTypeCode and n.languageCode=l.code   and l.countryCode='"+locale.getCountry()+"'  and l.languageCode='"+locale.getLanguage()+"'  ");
		sql.append(" inner join  dt_product_software_config g on g.code=a.code");
		sql.append(" left outer join dt_product_software_config_memo m on  g.parentCode=m.softConfigCode and m.languageCode=l.code ");
		sql.append(" inner join");
		sql.append(" (select d.softwareTypeCode ,max(d.id) releaseId from DT_SoftwareVersion d where d.releaseState=1  and d.softwareTypeCode in ");
		sql.append(" ( select distinct c.softwareTypeCode from DT_SaleConfig a ,DT_MinSaleUnitSaleCfgDetail b,DT_MinSaleUnitSoftwareDetail c");
		sql.append(" where a.code=b.saleCfgCode and b.minSaleUnitCode=c.minSaleUnitCode and a.proTypeCode='"+params.get("proTypeCode")+"' ) group by d.softwareTypeCode  ) e ");
		sql.append(" on c.id=e.releaseId and c.softwareTypeCode=e.softwareTypeCode  ");
		sql.append(" inner join dt_productforsealer p");
		sql.append(" where 1=1 ");
		sql.append(" and  p.code='"+params.get("proTypeCode")+"'");
		if(pageSize != Integer.MAX_VALUE && params != null){
			
			if(params.get("softName") != null){	//软件名称
				sql.append(" and n.softwareName like '%"+params.get("softName")+"%' ");
			}
		}
		
		
		sql.append(" order by n.softwareName   ");
		
		return daoSupport.queryForPage(sql.toString(), pageNo, pageSize,new VehicleTypeMapper(filePath),new Object[]{});
	}

	/**
	  * <p>Title: addLanguagePack</p>
	  * <p>Description: 添加软件语言包</p>
	  * @param languagePack
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void addLanguagePack(LanguagePack languagePack) throws Exception {
		String code = DBUtils.generateCode(DBConstant.LANGUAGE_PACK_CODE);
		languagePack.setCode(code);
		this.daoSupport.insert(TABLE_NAME, languagePack);
	}

	/**
	  * <p>Title: delLanguagePack</p>
	  * <p>Description: 删除软件语言包</p>
	  * @param code
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void delLanguagePack(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code = ?");
		this.daoSupport.execute(sql.toString(),code);
	}

	/**
	  * <p>Title: delLanguagePackByVersionCode</p>
	  * <p>Description: </p>
	  * @param versionCode
	  * @throws Exception
	  */ 
	@Override
	public void delLanguagePackByVersionCode(String versionCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append( " where softwareVersionCode = ?");
		this.daoSupport.execute(sql.toString(),versionCode);
	}

	/**
	  * <p>Title: updateLanguagePack</p>
	  * <p>Description: </p>
	  * @param languagePack
	  * @throws Exception
	  */ 
	@Override
	public void updateLanguagePack(LanguagePack languagePack) throws Exception {
		this.daoSupport.update(TABLE_NAME, languagePack, " code='"+languagePack.getCode().trim()+"'");
	}

	/**
	  * <p>Title: getLanguagePackByVersionCode</p>
	  * <p>Description: </p>
	  * @param versionCode
	  * @throws Exception
	  */ 
	@Override
	public List<LanguagePack> getLanguagePackByVersionCode(String versionCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where softwareVersionCode = ?");;
		return this.daoSupport.queryForList(sql.toString(), LanguagePack.class,versionCode);
	}
	

}
