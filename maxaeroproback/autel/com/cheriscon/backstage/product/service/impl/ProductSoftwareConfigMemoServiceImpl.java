package com.cheriscon.backstage.product.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.product.service.IProductSoftwareConfigMemoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ProductSoftwareConfigMemo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
@Service
public class ProductSoftwareConfigMemoServiceImpl extends BaseSupport<ProductSoftwareConfigMemo> implements
		IProductSoftwareConfigMemoService {

	@Override
	public void addProductSoftwareConfigMemo(ProductSoftwareConfigMemo memo)
			throws Exception {
		String code = DBUtils.generateCode(DBConstant.SOFTWARE_CONFIG_MEMO_CODE);
		memo.setCode(code);
		this.daoSupport.insert("dt_product_software_config_memo", memo);
		
	}

	@Override
	public void delProductSoftwareConfigMemo(String softConfigCode)
			throws Exception {
		String sql="delete from dt_product_software_config_memo where softConfigCode=?";
		this.daoSupport.execute(sql, softConfigCode);
		
	}

	@Override
	public List<ProductSoftwareConfigMemo> getProductSoftwareConfigMemo(
			String softConfigCode) throws Exception {
		StringBuffer sql=new StringBuffer("select a.code,a.name,b.code as languageCode,b.name as languageName from dt_product_software_config_memo a ");
		sql.append("left outer join dt_language b on a.languageCode=b.code where softConfigCode=?");
		return this.daoSupport.queryForList(sql.toString(), ProductSoftwareConfigMemo.class, softConfigCode);
	}

	

}
