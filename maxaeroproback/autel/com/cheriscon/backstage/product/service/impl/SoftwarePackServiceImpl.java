/**
*/ 
package com.cheriscon.backstage.product.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.product.service.ISoftwarePackService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.SoftwarePack;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: SoftwarePackServiceImpl
 * @Description: 软件基础语言包业务实现类
 * @author shaohu
 * @date 2013-1-11 下午04:10:52
 * 
 */
@Service
public class SoftwarePackServiceImpl extends BaseSupport<SoftwarePack> implements ISoftwarePackService {

	private final static String TABLE_NAME = "DT_SoftwarePack"; 
	
	/**
	  * <p>Title: addSoftwarePack</p>
	  * <p>Description: 添加软件基础包信息 </p>
	  * @param softwarePack
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void addSoftwarePack(SoftwarePack softwarePack) throws Exception {
		String code = DBUtils.generateCode(DBConstant.SOFTWARE_PACK_CODE);
		softwarePack.setCode(code);
		this.daoSupport.insert(TABLE_NAME, softwarePack);
	}

	/**
	  * <p>Title: delSoftwarePackByVersionCode</p>
	  * <p>Description: 根据软件版本编码删除软件基础语言包</p>
	  * @param versionCode
	  * @throws Exception
	  */ 
	@Override
	public void delSoftwarePackByVersionCode(String versionCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where softwareVersionCode=?");
		this.daoSupport.execute(sql.toString(), versionCode);
	}

	/**
	  * <p>Title: delSoftwarePack</p>
	  * <p>Description: </p>
	  * @param code
	  * @throws Exception
	  */ 
	@Override
	public void delSoftwarePack(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code=?");
		this.daoSupport.execute(sql.toString(), code);
		
	}

	/**
	  * <p>Title: updateSoftwarePack</p>
	  * <p>Description: </p>
	  * @param softwarePack
	  * @throws Exception
	  */ 
	@Override
	public void updateSoftwarePack(SoftwarePack softwarePack) throws Exception {
		this.daoSupport.update(TABLE_NAME, softwarePack, " code='"+softwarePack.getCode().trim()+"'");
	}

	
}
