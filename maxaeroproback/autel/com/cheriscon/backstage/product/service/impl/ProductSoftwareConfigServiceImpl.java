package com.cheriscon.backstage.product.service.impl;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.vo.MinSaleUnitSearch;
import com.cheriscon.backstage.member.service.ITestPackLanguagePackService;
import com.cheriscon.backstage.product.service.IProductSoftwareConfigService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.service.ISoftwareTypeService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.ProductSoftwareConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @remark 虚拟目录service
 * @date 2013-11-26
 * @author pengdongan
 */
@Service
public class ProductSoftwareConfigServiceImpl extends BaseSupport implements
		IProductSoftwareConfigService {
	
	@Resource
	private ISoftwareTypeService softwareTypeService;
	
	@Resource
	private IProductTypeService productTypeService;
	
	@Override
	public List<ProductSoftwareConfig> listAll() throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_PRODUCT_SOFTWARE_CONFIG order by name asc");
		return this.daoSupport.queryForList(sql.toString(),ProductSoftwareConfig.class);
	}
	
	@Override
	public List<ProductSoftwareConfig> listVirtualDirAll() throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_PRODUCT_SOFTWARE_CONFIG where SoftwareFlag = 0 order by name asc");
		return this.daoSupport.queryForList(sql.toString(),ProductSoftwareConfig.class);
	}

	
	/**
	 * 添加虚拟目录
	 * 
	 * @param productSoftwareConfig
	 *            虚拟目录
	 * @return	虚拟目录编码
	 * @throws Exception
	 */
	public String add(ProductSoftwareConfig productSoftwareConfig) throws Exception {
		String code = DBUtils.generateCode(DBConstant.PRODUCT_SOFTWARE_CONFIG_CODE);
		productSoftwareConfig.setCode(code);
		this.daoSupport.insert("DT_PRODUCT_SOFTWARE_CONFIG", productSoftwareConfig);
		return code;
	}

	
	/**
	 * 添加虚拟目录(对应产品、软件对象)
	 * 
	 * @param productSoftwareConfig
	 *            虚拟目录
	 * @return	虚拟目录编码
	 * @throws Exception
	 */
	public String addForProSoft(ProductSoftwareConfig productSoftwareConfig) throws Exception {
		String code = productSoftwareConfig.getCode();
		this.daoSupport.insert("DT_PRODUCT_SOFTWARE_CONFIG", productSoftwareConfig);
		return code;
	}
	
	@Override
	public void update(ProductSoftwareConfig productSoftwareConfig) throws Exception {
		this.daoSupport.update("DT_PRODUCT_SOFTWARE_CONFIG", productSoftwareConfig, "code='"+productSoftwareConfig.getCode()+"'");
	}
	
	@Override
	@Transactional
	public String addSoftwareType(SoftwareType softwareType,ProductSoftwareConfig productSoftwareConfig) throws Exception {
		String softwareTypeCode = softwareTypeService.addSoftwareType(softwareType);
		
		//添加虚拟目录对象
		productSoftwareConfig.setSoftwareFlag(1);
		productSoftwareConfig.setName(softwareType.getName());
		productSoftwareConfig.setCode(softwareTypeCode);
		this.addForProSoft(productSoftwareConfig);
		
		return softwareTypeCode;
	}
	
	@Override
	@Transactional
	public void updateSoftwareType(SoftwareType softwareType,ProductSoftwareConfig productSoftwareConfig) throws Exception {
		softwareTypeService.updateSoftwareType(softwareType);
		
		//修改虚拟目录对象
		productSoftwareConfig.setCode(softwareType.getCode());
		productSoftwareConfig.setName(softwareType.getName());
		this.update(productSoftwareConfig);
	}
	
	@Override
	@Transactional
	public void delSoftwareType(SoftwareType softwareType) throws Exception {
		softwareTypeService.delSoftwareType(softwareType.getCode());
		
		//删除虚拟目录对象
		this.delProductSoftwareConfig(softwareType.getCode());		
	}
	
	@Override
	@Transactional
	public String addProductType(ProductType productType,ProductSoftwareConfig productSoftwareConfig) throws Exception {
		
		String productTypeCode = productTypeService.add(productType);
		
		//添加虚拟目录对象
		if(productSoftwareConfig == null)
		{
			productSoftwareConfig = new ProductSoftwareConfig();
		}
		
		productSoftwareConfig.setSoftwareFlag(0);
		productSoftwareConfig.setName(productType.getName());
		productSoftwareConfig.setCode(productTypeCode);
		productSoftwareConfig.setParentCode("0");
		this.addForProSoft(productSoftwareConfig);
		
		return productTypeCode;
	}
	
	@Override
	@Transactional
	public void updateProductType(ProductType productType,ProductSoftwareConfig productSoftwareConfig) throws Exception {
		
		productTypeService.update(productType);
		
		//修改虚拟目录对象
		if(productSoftwareConfig == null)
		{
			productSoftwareConfig = new ProductSoftwareConfig();
		}
		productSoftwareConfig.setCode(productType.getCode());
		productSoftwareConfig.setName(productType.getName());
		this.update(productSoftwareConfig);
	}
	
	@Override
	@Transactional
	public void delProductType(ProductType productType) throws Exception {
		productTypeService.delete(productType);
		
		//删除虚拟目录对象
		this.delProductSoftwareConfig(productType.getCode());
	}
	
	@Override
	public ProductSoftwareConfig getProductSoftwareConfigByCode(String code) throws Exception {
		return (ProductSoftwareConfig) this.daoSupport.queryForObject(
				"select * from DT_PRODUCT_SOFTWARE_CONFIG where code=?", ProductSoftwareConfig.class, code.trim());
	}
	
	@Override
	public ProductSoftwareConfig getParentDirByCode(String code) throws Exception {
		ProductSoftwareConfig productSoftwareConfig = getProductSoftwareConfigByCode(code.trim());
		
		if(productSoftwareConfig != null && productSoftwareConfig.getParentCode() != null)
		{
			productSoftwareConfig = getProductSoftwareConfigByCode(productSoftwareConfig.getParentCode().trim());
		}
		
		return productSoftwareConfig;
	}
	
	@Override
	public ProductSoftwareConfig getRootDirByCode(String code) throws Exception {
		ProductSoftwareConfig parentProductSoftwareConfig = new ProductSoftwareConfig();
		parentProductSoftwareConfig = getProductSoftwareConfigByCode(code);
		
		for (int i = 0; i < 100; i++)
		{
			if(parentProductSoftwareConfig != null && getParentDirByCode(parentProductSoftwareConfig.getCode()) != null)
			{
				parentProductSoftwareConfig = getParentDirByCode(parentProductSoftwareConfig.getCode());
			}
			else
			{
				break;
			}
			
		}
		return parentProductSoftwareConfig;
	}

	@Override
	public int delete(ProductSoftwareConfig productSoftwareConfig) throws Exception {
		String sql = "delete from DT_PRODUCT_SOFTWARE_CONFIG where code= '" + productSoftwareConfig.getCode() + "'";
		this.daoSupport.execute(sql);
		return 0;
	}

	@Override
	public void delProductSoftwareConfig(String productSoftwareConfigCode) throws Exception {
		String sql = "delete from DT_PRODUCT_SOFTWARE_CONFIG where code= ?";
		this.daoSupport.execute(sql, productSoftwareConfigCode);
	}


	@Override
	public List<ProductSoftwareConfig> getTreeByProductName(String productName) throws Exception {
		
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_PRODUCT_SOFTWARE_CONFIG where parentCode='0' and SoftwareFlag=0 ");
		
		if(StringUtils.isNotBlank(productName))
		{
			sql.append(" and name like '%" + productName.trim() + "%'");
		}

		sql.append(" order by name asc");
		
		List<ProductSoftwareConfig> productSoftwareConfigs = this.daoSupport.queryForList(sql.toString(),ProductSoftwareConfig.class);
		

		List<ProductSoftwareConfig> productSoftwareConfigParentList = new ArrayList<ProductSoftwareConfig>();
		
		
		for (ProductSoftwareConfig productSoftwareConfig : productSoftwareConfigs) {

			List<ProductSoftwareConfig> productSoftwareConfigList = this.getProductSoftwareConfigTree(productSoftwareConfig.getCode());
			
			productSoftwareConfigParentList.add(productSoftwareConfig);
			productSoftwareConfig.setChildren(productSoftwareConfigList);
		}
		
		return productSoftwareConfigParentList;
	}



	public List<ProductSoftwareConfig> getProductSoftwareConfigTree(String productSoftwareConfigCode) throws Exception {
		if(productSoftwareConfigCode==null)throw new IllegalArgumentException("productSoftwareConfigCode argument is null");
		List<ProductSoftwareConfig> productSoftwareConfigList  = this.listVirtualDirAll();
		List<ProductSoftwareConfig> topProductSoftwareConfigList  = new ArrayList<ProductSoftwareConfig>();
		
		for(ProductSoftwareConfig productSoftwareConfig :productSoftwareConfigList){
			if(productSoftwareConfig.getParentCode().equals(productSoftwareConfigCode)){
				List<ProductSoftwareConfig> children = this.getChildren(productSoftwareConfigList, productSoftwareConfig.getCode());
				productSoftwareConfig.setChildren(children);
				topProductSoftwareConfigList.add(productSoftwareConfig);
			}
		}
		return topProductSoftwareConfigList;
	}
	
	private List<ProductSoftwareConfig> getChildren(List<ProductSoftwareConfig> productSoftwareConfigList ,String parentCode) throws Exception {
		List<ProductSoftwareConfig> children =new ArrayList<ProductSoftwareConfig>();
		for(ProductSoftwareConfig productSoftwareConfig :productSoftwareConfigList){
			if(productSoftwareConfig.getParentCode().equals(parentCode)){
			 	productSoftwareConfig.setChildren(this.getChildren(productSoftwareConfigList, productSoftwareConfig.getCode()));
				children.add(productSoftwareConfig);
			}
		}
		return children;
	}


	public List<ProductSoftwareConfig> listProductSoftwareConfigLevel(String productSoftwareConfigCode) throws Exception {
		List<ProductSoftwareConfig> topProductSoftwareConfigList  = getProductSoftwareConfigTree(productSoftwareConfigCode);
		
		List<ProductSoftwareConfig> productSoftwareConfigs = new ArrayList<ProductSoftwareConfig>();
		
		for(ProductSoftwareConfig productSoftwareConfig :topProductSoftwareConfigList){
			productSoftwareConfig.setLevel(0);
			productSoftwareConfigs.add(productSoftwareConfig);
			
			if(productSoftwareConfig.getHasChildren()){
				buildProductSoftwareConfigLevel(productSoftwareConfigs,0,productSoftwareConfig.getChildren());
			}
		}
		return productSoftwareConfigs;
	}
	
	private void buildProductSoftwareConfigLevel(List<ProductSoftwareConfig> productSoftwareConfigs,int level,List<ProductSoftwareConfig> children) throws Exception {
		level++;
		
		for(ProductSoftwareConfig productSoftwareConfig :children){
			productSoftwareConfig.setLevel(level);
			productSoftwareConfigs.add(productSoftwareConfig);
			
			if(productSoftwareConfig.getHasChildren()){
				buildProductSoftwareConfigLevel(productSoftwareConfigs,0,productSoftwareConfig.getChildren());
			}
		}
		
	}

	@Override
	public boolean isSameName(ProductSoftwareConfig productSoftwareConfig) throws Exception {
		if(productSoftwareConfig.getCode() == null) productSoftwareConfig.setCode("");
		
		List<ProductSoftwareConfig> productSoftwareConfigs = this.daoSupport.queryForList(
				"select * from DT_PRODUCT_SOFTWARE_CONFIG where code<>? and parentCode=? and name=? and SoftwareFlag=0",
				ProductSoftwareConfig.class, productSoftwareConfig.getCode(),productSoftwareConfig.getParentCode(),productSoftwareConfig.getName());
		
		if(productSoftwareConfigs.size()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public List<ProductSoftwareConfig> listProductSoftwareConfigByParentCode(String parentCode) throws Exception {
		return (List<ProductSoftwareConfig>) this.daoSupport.queryForList(
				"select * from DT_PRODUCT_SOFTWARE_CONFIG where parentCode=?", ProductSoftwareConfig.class, parentCode.trim());
	}
	
	public List<ProductSoftwareConfig> getProductMinSaleTree(String languageCode) throws Exception{
		List<ProductSoftwareConfig> productSoftwareConfigList  = this.listProductSoftwareConfigByParentCode("0");
		List<ProductSoftwareConfig> topProductSoftwareConfigList  = new ArrayList<ProductSoftwareConfig>();
		
		for(ProductSoftwareConfig productSoftwareConfig :productSoftwareConfigList){
				
			String sql="select a.code,b.name from  DT_MinSaleUnit a,DT_MinSaleUnitMemo b  where a.code=b.minSaleUnitCode and a.proTypeCode=? and b.languageCode=?";
			List<MinSaleUnitSearch> children=this.daoSupport.queryForList(sql, MinSaleUnit.class, productSoftwareConfig.getCode(),languageCode);
			productSoftwareConfig.setMinList(children);
			topProductSoftwareConfigList.add(productSoftwareConfig);
		}
		return topProductSoftwareConfigList;
	}
}
