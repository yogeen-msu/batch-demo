package com.cheriscon.backstage.product.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.vo.ProductTypeSerialPicVo;
import com.cheriscon.backstage.product.vo.ProductTypeVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.JSONHelpUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @remark 产品型号service
 * @date 2012-1-4
 * @author shaohu
 */
@Service
public class ProductTypeServiceImpl extends BaseSupport<ProductType> implements
		IProductTypeService {

	@Resource
	private ILanguageService languageService;
	
	/**
	  * <p>Title: searchPage</p>
	  * <p>Description: </p>
	  * @param productTypeVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page searchPage(ProductTypeVo productTypeVo, int pageNo, int pageSize)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_ProductType where 1=1 ");
		if (null != productTypeVo) {
			if (StringUtils.isNotBlank(productTypeVo.getProductName())) {
				sql.append(" and name like");
				sql.append("'%");
				sql.append(productTypeVo.getProductName().trim());
				sql.append("%'");
			}
		}
		sql.append(" order by id desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, ProductType.class);
	}
	
	@Override
	public List<ProductType> search(ProductTypeVo productTypeVo)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_ProductType where 1=1");
		if (null != productTypeVo) {
			if (StringUtils.isNotBlank(productTypeVo.getProductName())) {
				sql.append(" and name like");
				sql.append("'%");
				sql.append(productTypeVo.getProductName().trim());
				sql.append("%'");
			}
		}
		sql.append(" order by id desc");
		return this.daoSupport.queryForList(sql.toString(),ProductType.class);
	}
	
	@Override
	public List<ProductType> listAll() throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_ProductType where 1 = 1 order by name asc");
		return this.daoSupport.queryForList(sql.toString(),ProductType.class);
	}
	/*@Override
	public Page search(ProductTypeVo productTypeVo,int page,
			int pageSize)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_ProductType where 1=1");
		if (null != productTypeVo) {
			if (StringUtils.isNotBlank(productTypeVo.getProductName())) {
				sql.append(" and name =");
				sql.append("'");
				sql.append(productTypeVo.getProductName());
				sql.append("'");
			}
		}
		return this.daoSupport.queryForPage(sql.toString(), page, pageSize, new RowMapper(){

			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public Object mapRow(ResultSet rs, int c) throws SQLException {
				Map data = new HashMap();
				data.put("id", rs.getString("id"));
				data.put("code", rs.getString("code"));
				data.put("name", rs.getString("name"));
				data.put("proSerialPicPath", rs.getString("proSerialPicPath"));
				return data;
			}
		});
	}*/

	
	/**
	 * 添加产品型号
	 * 
	 * @param productType
	 *            产品型号
	 * @return	产品型号编码
	 * @throws Exception
	 */
	public String add(ProductType productType) throws Exception {
		String code = DBUtils.generateCode(DBConstant.PRODUCT_TYPE_CODE);
		productType.setCode(code);
		this.daoSupport.insert("DT_ProductType", productType);
		return code;
//		StringBuffer sql = new StringBuffer();
//		sql.append("insert into DT_ProductType(code,name) values('"
//				+ code + "','" + productType.getName() + "')");
//		this.daoSupport.execute(sql.toString());
//		return code;
	}
	
	
	/**
	 * 添加序列化图片路径
	 * @param productType	产品型号对象
	 * @param serialPicVo	产品型号序列化图片
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String addSerialPicPath(ProductType productType,
			ProductTypeSerialPicVo serialPicVo) throws Exception {
		try{
			List<ProductTypeSerialPicVo> list = new ArrayList<ProductTypeSerialPicVo>();
			list.add(serialPicVo);
			
			productType = this.getProductTypeByCode(productType.getCode());
			if(StringUtils.isNotBlank(productType.getProSerialPicPath())){
//			List tlist = JSONHelpUtils.jsonToList(productType.getProSerialPicPath(),
//					ProductTypeSerialPicVo.class.getName());
				List tlist = JSONHelpUtils.getList4Json(productType.getProSerialPicPath(),ProductTypeSerialPicVo.class);
				if(tlist.size() != 0){
					list.addAll(tlist);
				}
			}
			
			String serialPicPathJson =  JSONArray.fromObject(list).toString();
			productType.setProSerialPicPath(serialPicPathJson);
			this.update(productType);
			return productType.getCode();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public void update(ProductType productType) throws Exception {
		
		String code = productType.getCode();
		StringBuffer sql = new StringBuffer();
		sql.append("update DT_ProductType set ");
		boolean bool = false;
		if(StringUtils.isNotBlank(productType.getName())){
			bool = true;
			sql.append("name='"+productType.getName()+"'");
		}
		if(StringUtils.isNotBlank(productType.getPicPath())){
			if(bool)
				sql.append(",");
			bool = true;
			sql.append("picPath='"+productType.getPicPath()+"'");
		}
		if(StringUtils.isNotBlank(productType.getProSerialPicPath())){
			if(bool)
				sql.append(",");
			sql.append("proSerialPicPath='"+productType.getProSerialPicPath()+"'");
		}
		
		sql.append(" where code='"+code+"'");
		//sql.append("update DT_ProductType set name='"+productType.getName()+"' where code='"+code+"'");
		this.daoSupport.execute(sql.toString());
		
//		this.daoSupport.update("DT_ProductType", productType, " code='"+productType.getCode()+"'");
		
	}
	
	@Override
	public ProductType getProductTypeByCode(String code) throws Exception {
		return (ProductType) this.daoSupport.queryForObject(
				"select * from DT_ProductType where code=?", ProductType.class,
				code.trim());
	}

	@Override
	public int delete(ProductType productType) throws Exception {
		String sql = "delete from DT_ProductType where code= '" + productType.getCode() + "'";
		this.daoSupport.execute(sql);
		return 0;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ProductTypeSerialPicVo> getProductTypeSerialList(String code) throws Exception{
		ProductType productType = this.getProductTypeByCode(code);
		if(StringUtils.isBlank(productType.getProSerialPicPath())){
			return null;
		}
//		List list = JSONHelpUtils.jsonToList(productType.getProSerialPicPath(),
//				ProductTypeSerialPicVo.class.getName());
		List list = new ArrayList();
		try{
			list = JSONHelpUtils.getList4Json(productType.getProSerialPicPath(),ProductTypeSerialPicVo.class);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		if(list.size() == 0 ){
			return list;
		}else{
			//设置语言名称
			List<Language> languages = languageService.queryLanguage();
			Map<String, Language> map = this.languageListToMap(languages);
			for (Object o : list) {
				ProductTypeSerialPicVo t = (ProductTypeSerialPicVo) o;
				Language language = map.get(t.getLanguageCode());
				t.setLanguageName(language.getName());
			}
			
		}
		return list;
	}
	
	/**
	 * 删除产品型号对应语言图片
	 * @param code 产品型号
	 * @param languageCode 语言code
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int delPicByLanguageCode(String code,String languageCode) throws Exception{
		ProductType productType = this.getProductTypeByCode(code);
//		List list = JSONHelpUtils.jsonToList(productType.getProSerialPicPath(),
//				ProductTypeSerialPicVo.class.getName());
		List list = JSONHelpUtils.getList4Json(productType.getProSerialPicPath(),ProductTypeSerialPicVo.class);
		List<ProductTypeSerialPicVo> copyList = new ArrayList<ProductTypeSerialPicVo>();
		copyList.addAll(list);
		for (Object o : list) {
			ProductTypeSerialPicVo t = (ProductTypeSerialPicVo) o;
			if(StringUtils.equals(languageCode.trim(), t.getLanguageCode().trim())){
				copyList.remove(t);
				break;
			}
		}
		//存在语言对应的图片
		if(copyList.size() != 0){
			String serialPicPathJson = JSONArray.fromObject(copyList).toString();
			productType.setProSerialPicPath(serialPicPathJson);
			this.update(productType);
		}else{
			StringBuffer sql = new StringBuffer();
			sql.append("update DT_ProductType set proSerialPicPath= null where code='"+code+"'");
			this.daoSupport.execute(sql.toString());
		}
		return 0;
	}

	/**
	 * 获取当前产品型号未添加图片的语言数据
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public String queryLanguageJson(String code) throws Exception{
		List<Language> languages = languageService.queryLanguage();
		List<ProductTypeSerialPicVo> vList = this.getProductTypeSerialList(code);
		Map<String, Language> map = this.languageListToMap(languages);
		
		if(null == vList || vList.size() == 0){
			return JSONArray.fromObject(languages).toString();
		}
		
		for(ProductTypeSerialPicVo v : vList){
			map.remove(v.getLanguageCode());
		}
		
		//map为空，则说明所有语言已经添加
		if(map.size() == 0){
			return null;
		}
		
		List<Language> tLanguages = new ArrayList<Language>();
		Set<String> keys = map.keySet();
		for(String key : keys){
			tLanguages.add(map.get(key));
		}
		return JSONArray.fromObject(tLanguages).toString();
	}
	
	/**
	 * 语言list转为map
	 * @param languages	语言集合
	 * @return	返回code为键、语言为值的map对象
	 * @throws Exception
	 */
	public Map<String, Language> languageListToMap(List<Language> languages)
			throws Exception {
		Map<String, Language> map = new HashMap<String, Language>();
		for (Language language : languages) {
			map.put(language.getCode(), language);
		}
		return map;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public int updateLanguagePic(ProductType productType,
			ProductTypeSerialPicVo serialPicVo) throws Exception {
		productType = this.getProductTypeByCode(productType.getCode());
//		List list = JSONHelpUtils.jsonToList(productType.getProSerialPicPath(),
//				ProductTypeSerialPicVo.class.getName());
		
		List list = JSONHelpUtils.getList4Json(productType.getProSerialPicPath(),ProductTypeSerialPicVo.class);
		
		List<ProductTypeSerialPicVo> copyList = new ArrayList<ProductTypeSerialPicVo>();
		copyList.addAll(list);
		
		for (Object o : list) {
			ProductTypeSerialPicVo t = (ProductTypeSerialPicVo) o;
			if(StringUtils.equals(t.getPicId().trim(),serialPicVo.getPicId().trim())){
				copyList.remove(t);
				t.setLanguageCode(serialPicVo.getLanguageCode());
				t.setPicPath(serialPicVo.getPicPath());
				copyList.add(t);
				break;
			}
		}
		String serialPicPathJson = JSONArray.fromObject(copyList).toString();
		productType.setProSerialPicPath(serialPicPathJson);
		this.update(productType);
		return 0;
	}

	@Override
	public Page queryProductTypeList(String sealerCode, int pageNo, int pageSize)
			throws Exception 
	{
		StringBuffer querySql = new StringBuffer();
		
		//String newDate = DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD);
		
		querySql.append("select a.code as productCode,a.name as productName,a.picPath,e.name as areaName,b.contractDate,b.expirationTime ")
		        .append("from DT_ProductType a ,DT_SaleContract b,")
		        .append("DT_AreaConfig e where a.code = b.proTypeCode  and b.areaCfgCode = e.code and b.sealerCode ='")
		        .append(sealerCode).append("' order by b.contractDate desc ");
		
				
		return this.daoSupport.queryForPage(querySql.toString(), pageNo, pageSize,
				new RowMapper()
		{

			@Override
			public ProductType mapRow(ResultSet rs, int arg1) throws SQLException 
			{
				ProductType productType = new ProductType();
				productType.setProductCode(rs.getString("productCode"));
				productType.setName(rs.getString("productName"));
				productType.setPicPath(rs.getString("picPath"));
				productType.setAreaName(rs.getString("areaName"));
				productType.setContractDate(rs.getString("contractDate"));
				productType.setExpirationTime(rs.getString("expirationTime"));
				return productType;
			}
		});
	}

	@Override
	public int queryProductTypeCount(String sealerCode) throws Exception
	{
		
		StringBuffer sql = new StringBuffer();
		sql.append("select count(*) from  DT_ProductType a ,DT_SaleContract b, DT_AreaConfig e ")
		   .append(" where a.code = b.proTypeCode  and b.areaCfgCode = e.code and b.sealerCode='")
		   .append(sealerCode).append("'");
		
		return this.daoSupport.queryForInt(sql.toString());
	}

	/**
	  * <p>Title: isRepeat</p>
	  * <p>Description: </p>
	  * @param id
	  * @param productName
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public boolean isRepeat(String code, String productName) throws Exception {
		StringBuffer sql = new StringBuffer();
		List<ProductType> list = new ArrayList<ProductType>();
		if(StringUtils.isBlank(code)){ 
			sql.append("select * from DT_ProductType where name=?");
			list = this.daoSupport.queryForList(sql.toString(), ProductType.class, productName);
		}else{
			sql.append("select * from DT_ProductType where name=? and code !=?");
			list = this.daoSupport.queryForList(sql.toString(), ProductType.class, productName,code);
		}
		if(list.size() > 0 ){
			return true;
		}
		return false;
	}

	
}
