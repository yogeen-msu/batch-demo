package com.cheriscon.backstage.product.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.product.service.IBataProductService;
import com.cheriscon.backstage.product.vo.BetaProduct;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class bataProductServiceImpl extends BaseSupport<BetaProduct> implements IBataProductService {

	private final static String TABLE_NAME = "DT_BetaProduct";
	
	@Override
	public Page pageBataProduct(BetaProduct bata, int pageNo, int pageSize)
			throws Exception {
		StringBuffer sql=new StringBuffer();
	//	sql.append(" select id,autelId,proTypeName,serialNo from (");
		sql.append("select t.code as code, t.id as id, c.autelId as autelId,s.name as proTypeName,p.serialNo as serialNo ,ps.code as saleCode,ps.name as saleName"
				+ " from DT_BetaProduct t ");
		/*sql.append(" left join (select p.serialNo,p.code as proCode,p.proTypeCode, sale.code as saleCode ,sale.name as saleName from dt_productinfo p "
				+ "  left join DT_saleContract  sale on p.salecontractCode =sale.code ) ps "
				+ "  on t.proCode=ps.proCode ");
		sql.append(" left join  DT_ProductForSealer s on s.code=ps.proTypeCode ");
		sql.append(" left join DT_CustomerProInfo r on r.proCode=ps.proCode ");
		sql.append(" left join dt_customerinfo c on r.customerCode=c.code");*/
		/**
		 * 优化查询 原sql执行太慢
		 * */
		sql.append(" left join dt_productInfo p on t.proCode=p.code");
		sql.append(" left join DT_saleContract ps on ps.code=p.saleContractCode");
		sql.append(" left join  DT_ProductForSealer s on s.code=ps.proTypeCode ");
		sql.append(" left join DT_CustomerProInfo r on r.proCode=p.code ");
		sql.append(" left join dt_customerinfo c on r.customerCode=c.code");
		sql.append(" where 1=1");
		if(bata!=null){
			if(StringUtils.isNotBlank(bata.getSerialNo())){
				sql.append(" and t.serialNo like '%"+bata.getSerialNo()+"%'");
			}
		}
		sql.append(" order by t.id desc");
		return daoSupport.queryForPage(sql.toString(), pageNo, pageSize,BetaProduct.class);
	}

	@Override
	public void addBataProduct(BetaProduct bata) throws Exception {
	
		String code = DBUtils.generateCode(DBConstant.BATA_PRODUCT_CODE);
		bata.setCode(code);
		this.daoSupport.insert(TABLE_NAME, bata);
	}

	@Override
	public void deleteBataProduct(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code = ?");
		this.daoSupport.execute(sql.toString(),code);

	}
	@Override
	public BetaProduct getBataProductBySerialNo(String serialNo) throws Exception{
		StringBuffer sql= new StringBuffer();
		sql.append("select * from dt_betaproduct where serialNo=?");
        return	this.daoSupport.queryForObject(sql.toString(), BetaProduct.class, serialNo);
	}
}
