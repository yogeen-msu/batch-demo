package com.cheriscon.backstage.product.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.product.mapper.SoftwareTypeMapper;
import com.cheriscon.backstage.product.service.IProductTypeConfigService;
import com.cheriscon.backstage.product.service.ISoftwareNameService;
import com.cheriscon.backstage.product.service.ISoftwareTypeService;
import com.cheriscon.backstage.product.vo.ProductTypeConfig;
import com.cheriscon.backstage.product.vo.ProductTypeSerialPicVo;
import com.cheriscon.backstage.product.vo.SoftwareName;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: SoftwareTypeServiceImpl
 * @Description: 软件类型业务类
 * @author shaohu
 * @date 2013-1-9 上午09:14:15
 */
@Service
public class SoftwareTypeServiceImpl extends BaseSupport<SoftwareType> implements ISoftwareTypeService {

	@Resource
	private ILanguageService languageService;
	@Resource
	private ISoftwareNameService softwareNameService;
	/**
	* <p>Title: listAll</p>
	* <p>Description:查询所有软件类型 </p>
	* @return
	* @throws Exception
	*/ 
	@Override
	public List<SoftwareType> listAll()
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_SoftwareType order by name asc");
		return this.daoSupport.queryForList(sql.toString(), SoftwareType.class);
	}

	/**
	  * <p>Title: getSoftwareTypeByProTypeCode</p>
	  * <p>Description: 根据产品型号code查询功能软件类型</p>
	  * @param productTypeCode
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<SoftwareType> getSoftwareTypeByProTypeCode(
			String productTypeCode) throws Exception {
		
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from DT_SoftwareType where proTypeCode=?");
			return this.daoSupport.queryForList(sql.toString(), SoftwareType.class,productTypeCode);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public SoftwareType getSoftwareTypeByName(String name,String proTypeCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_SoftwareType where name=? and proTypeCode =?");
		return this.daoSupport.queryForObject(sql.toString(), SoftwareType.class, name,proTypeCode);
	}

	/**
	  * <p>Title: addSoftwareType</p>
	  * <p>Description: 添加功能软件类型信息</p>
	  * @param softwareType
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public String addSoftwareType(SoftwareType softwareType) throws Exception {
		StringBuffer sql = new StringBuffer();
		String softwareTypeCode = DBUtils.generateCode(DBConstant.SOFTWARE_TYPE_CODE);
		softwareType.setCode(softwareTypeCode);
		sql.append("insert into DT_SoftwareType(code,name,proTypeCode,carSignPath,newCarSignPath,softType) values(?,?,?,?,?,?)");
		this.daoSupport.execute(sql.toString(), softwareType.getCode(),
				softwareType.getName(), softwareType.getProTypeCode(),
				softwareType.getCarSignPath(),softwareType.getNewCarSignPath(),softwareType.getSoftType());
		
		return softwareTypeCode;
	}

	/**
	  * <p>Title: getSoftwareTypeByCode</p>
	  * <p>Description: 根据软件类型的code 查询软件类型</p>
	  * @param code
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public SoftwareType getSoftwareTypeByCode(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_SoftwareType where code=?");
		return this.daoSupport.queryForObject(sql.toString(), SoftwareType.class, code);
	}

	/**
	  * <p>Title: delSoftwareType</p>
	  * <p>Description: 删除软件类型 </p>
	  * @param code
	  * @throws Exception
	  */ 
	@Override
	public void delSoftwareType(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from DT_SoftwareType where code = ?");
		this.daoSupport.execute(sql.toString(), code);
	}

	/**
	  * <p>Title: pageSoftwareTypePage</p>
	  * <p>Description: </p>
	  * @param softwareType
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageSoftwareTypePage(SoftwareType softwareType, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_SoftwareType where proTypeCode = ? ");
		if (StringUtils.isNotBlank(softwareType.getName())) {
			sql.append("and").append(" name like ")
					.append("'%" + softwareType.getName().trim() + "%'")
					.append(" order by id desc");
			Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
					pageSize, new SoftwareTypeMapper(),
					softwareType.getProTypeCode());
			return page;
		} else {
			sql.append(" order by id desc");
			Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
					pageSize, new SoftwareTypeMapper(),
					softwareType.getProTypeCode());
			return page;
		}
	}

	/**
	  * <p>Title: pageSoftwareTypePageNew</p>
	  * <p>Description: </p>
	  * @param productSoftwareConfigCode 虚拟目录code
	  * @param softwareType
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageSoftwareTypePageNew(String productSoftwareConfigCode,SoftwareType softwareType, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select ds.* from DT_PRODUCT_SOFTWARE_CONFIG dp");
		sql.append(" inner join DT_SoftwareType ds on dp.code=ds.code");
		sql.append(" where dp.SoftwareFlag = 1 and dp.parentCode = ? ");
		
		if (softwareType !=null && StringUtils.isNotBlank(softwareType.getName())) {
			sql.append(" and ds.name like '%" + softwareType.getName().trim() + "%'");
		}

		sql.append(" order by ds.id desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, new SoftwareTypeMapper(), productSoftwareConfigCode);
		return page;
	}
	
	@Override
	public Page querySoftwareTypePageNew(String productTypeCode,SoftwareType softwareType, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select ds.* from DT_ProductType dp");
		sql.append(" inner join DT_SoftwareType ds on dp.code=ds.proTypeCode");
		sql.append(" where  dp.code = ? ");
		
		if (softwareType !=null && StringUtils.isNotBlank(softwareType.getName())) {
			sql.append(" and ds.name like '%" + softwareType.getName().trim() + "%'");
		}

		sql.append(" order by ds.id desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, new SoftwareTypeMapper(), productTypeCode);
		return page;
	}

	public Page pageSoftwarePage(SoftwareType softwareType, int pageNo,int pageSize) throws Exception{
		
		StringBuffer sql = new StringBuffer();
		sql.append("select a.code as code, b.name as proTypeName,a.name as name from DT_SoftwareType a ,DT_ProductType b where a.proTypeCode=b.code ");
		if(softwareType!= null  && StringUtils.isNotBlank(softwareType.getProTypeCode())){
			sql.append(" and a.proTypeCode='").append(softwareType.getProTypeCode()).append("'");
		}
		if(softwareType!= null  && StringUtils.isNotBlank(softwareType.getName()) ){
			sql.append(" and a.name like '%").append(softwareType.getName()).append("%'");
		}
		
		sql.append(" order by b.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,pageSize);
		return page;
	}
	
	/**
	  * <p>Title: updateSoftwareType</p>
	  * <p>Description: </p>
	  * @throws Exception
	  */ 
	@Override
	public void updateSoftwareType(SoftwareType softwareType) throws Exception {
		this.daoSupport.update("DT_SoftwareType", softwareType, " code='"
				+ softwareType.getCode().trim() + "'");
	}
	
	
	public String queryLanguageNameJson(String code) throws Exception{
		List<Language> languages = languageService.queryAllLanguage();
		List<SoftwareName> vList = softwareNameService.getProductTypeConfig(code);
		Map<String, Language> map = this.languageListToMap(languages);
		
		if(null == vList || vList.size() == 0){
			return JSONArray.fromObject(languages).toString();
		}
		
		for(SoftwareName v : vList){
			map.remove(v.getLanguageCode());
		}
		
		//map为空，则说明所有语言已经添加
		if(map.size() == 0){
			return null;
		}
		
		List<Language> tLanguages = new ArrayList<Language>();
		Set<String> keys = map.keySet();
		for(String key : keys){
			tLanguages.add(map.get(key));
		}
		return JSONArray.fromObject(tLanguages).toString();
	}
	/**
	 * 语言list转为map
	 * @param languages	语言集合
	 * @return	返回code为键、语言为值的map对象
	 * @throws Exception
	 */
	public Map<String, Language> languageListToMap(List<Language> languages)
			throws Exception {
		Map<String, Language> map = new HashMap<String, Language>();
		for (Language language : languages) {
			map.put(language.getCode(), language);
		}
		return map;
	}

}
