package com.cheriscon.backstage.product.service;

import java.util.List;

import com.cheriscon.common.model.ProductSoftwareConfigMemo;

public interface IProductSoftwareConfigMemoService {
	
	
	/**
	 * 增加ProductSoftwareConfigMemo
	 * 
	 **/
   public void addProductSoftwareConfigMemo(ProductSoftwareConfigMemo memo) throws Exception;
   
   
   /**
	 * 删除ProductSoftwareConfigMemo
	 * 
	 **/
  public void delProductSoftwareConfigMemo(String softConfigCode) throws Exception;
  
  /**
   * 根据serialNo查询bata产品
   * **/
  public List<ProductSoftwareConfigMemo> getProductSoftwareConfigMemo(String softConfigCode) throws Exception;
   
   
}
