/**
*/ 
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

/**
 * @ClassName: SoftwareVersionVo
 * @Description: 软件版本发布业务实体
 * @author shaohu
 * @date 2013-1-12 下午04:26:01
 * 
 */
public class SoftwareVersionVo implements Serializable{

	private static final long serialVersionUID = -6236859271972662669L;
	
	/**
	 * 功能软件名称
	 */
	private String name;
	
	/**
	 * 版本编码
	 */
	private String versionCode;
	
	/**
	 * 版本名称
	 */
	private String versionName;
	
	/**
	 * 软件类型编码
	 */
	private String softwareTypeCode;
	
	/**
	 * 发布状态
	 */
	private Integer releaseState;
	
	public SoftwareVersionVo(Integer id, String name, String versionCode,
			String versionName, String softwareTypeCode, Integer releaseState) {
		super();
		this.name = name;
		this.versionCode = versionCode;
		this.versionName = versionName;
		this.softwareTypeCode = softwareTypeCode;
		this.releaseState = releaseState;
	}

	public SoftwareVersionVo() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}

	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}

	public Integer getReleaseState() {
		return releaseState;
	}

	public void setReleaseState(Integer releaseState) {
		this.releaseState = releaseState;
	}
	

}
