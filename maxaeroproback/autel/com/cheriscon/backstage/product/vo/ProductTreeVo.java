/**
*/ 
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;
import java.util.List;

import com.cheriscon.common.model.ProductType;

/**
 * @ClassName: ProductTreeVo
 * @Description: 产品树业务对象
 * @author shaohu
 * @date 2013-1-29 下午02:22:03
 * 
 */
public class ProductTreeVo implements Serializable{

	private static final long serialVersionUID = -742730399861234813L;
	
	private String text;
	private String url;
	private List<ProductType> productTypes;
	public ProductTreeVo() {
		super();
	}
	public ProductTreeVo(String text, String url, List<ProductType> productTypes) {
		super();
		this.text = text;
		this.url = url;
		this.productTypes = productTypes;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public List<ProductType> getProductTypes() {
		return productTypes;
	}
	public void setProductTypes(List<ProductType> productTypes) {
		this.productTypes = productTypes;
	}
	
	
}
