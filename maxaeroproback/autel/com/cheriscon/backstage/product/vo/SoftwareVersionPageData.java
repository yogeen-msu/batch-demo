/**
 */
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;
import java.util.List;

import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.common.model.SoftwareVersion;

/**
 * @ClassName: SoftwareVersionPageData
 * @Description: 软件版本分页对象
 * @author shaohu
 * @date 2013-1-26 上午11:52:14
 * 
 */
@SuppressWarnings("serial")
public class SoftwareVersionPageData implements Serializable {

	private SoftwareVersion softwareVersion;

	private List<LanguagePack> languagePacks;

	public SoftwareVersionPageData() {
		super();
	}

	public SoftwareVersionPageData(SoftwareVersion softwareVersion,
			List<LanguagePack> languagePacks) {
		super();
		this.softwareVersion = softwareVersion;
		this.languagePacks = languagePacks;
	}

	public SoftwareVersion getSoftwareVersion() {
		return softwareVersion;
	}

	public void setSoftwareVersion(SoftwareVersion softwareVersion) {
		this.softwareVersion = softwareVersion;
	}

	public List<LanguagePack> getLanguagePacks() {
		return languagePacks;
	}

	public void setLanguagePacks(List<LanguagePack> languagePacks) {
		this.languagePacks = languagePacks;
	}

}
