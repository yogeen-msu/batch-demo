package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;

public class SoftwareName  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 746486637312411636L;
	
	private Integer id;
	private String code;
	private String softwareCode;  //
	private String languageCode; //语言编码
	private String softwareName; //产品名称
	private String languageName; //语言名称
	
	public Integer getId() {
		return id;
	}
	public String getCode() {
		return code;
	}
	public String getSoftwareCode() {
		return softwareCode;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public String getSoftwareName() {
		return softwareName;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setSoftwareCode(String softwareCode) {
		this.softwareCode = softwareCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public void setSoftwareName(String softwareName) {
		this.softwareName = softwareName;
	}

	
	@NotDbField
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	

}
