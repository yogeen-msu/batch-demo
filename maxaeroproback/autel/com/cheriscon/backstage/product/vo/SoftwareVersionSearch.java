/**
*/ 
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

/**
 * @ClassName: SoftwareVersionSearch
 * @Description: 软件版本搜索类
 * @author shaohu
 * @date 2013-1-12 下午04:48:51
 * 
 */
public class SoftwareVersionSearch implements Serializable{

	private static final long serialVersionUID = -4337040214570341976L;
	
	/**
	 * 产品型号编码
	 */
	private String productTypeCode;
	
	/**
	 * 上级目录编码
	 */
	private String parentCode;
	
	/**
	 * 功能软件名称
	 */
	private String name;
	
	/**
	 * 发布状态
	 */
	private Integer releaseState;

	public SoftwareVersionSearch(String productTypeCode, String name,
			Integer releaseState) {
		super();
		this.productTypeCode = productTypeCode;
		this.name = name;
		this.releaseState = releaseState;
	}

	public SoftwareVersionSearch() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getReleaseState() {
		return releaseState;
	}

	public void setReleaseState(Integer releaseState) {
		this.releaseState = releaseState;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	

}
