/**
* @ClassName: ProductTree
* @Description: TODO
* @author shaohu
* @date 2013-1-8 下午05:57:30
*
*/ 
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

/**
 * @ClassName: ProductTree
 * @Description: 产品树
 * @author shaohu
 * @date 2013-1-8 下午05:57:30
 * 
 */
public class ProductTree implements Serializable {

	private static final long serialVersionUID = -5267889511650984906L;
	
	private String id;
	
	private String name ;
	
	private String pId;
	
	//false 折叠   true 展开
	private boolean open = false;
	
	//true 表示是父节点 false 表示不是父节点
	private boolean isParent;
	
	private String target="mainProductFrame";
	
	private String url;

	public ProductTree() {
		super();
	}

	public boolean isOpen() {
		return open;
	}


	public void setOpen(boolean open) {
		this.open = open;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
