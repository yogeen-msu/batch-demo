package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

/**
 * @remark	产品型号业务对象
 * @date 2013-1-5
 * @author sh
 *
 */
public class ProductTypeVo implements Serializable{

	private static final long serialVersionUID = -4295118113698042398L;
	
	/**
	 * 产品名称
	 */
	private String productName;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
}
