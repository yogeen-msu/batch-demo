package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

public class BetaProduct  implements Serializable{
	
	/**
	 * Bata产品Vo，主要用于来关联出厂产品
	 * 
	 */
	private static final long serialVersionUID = -2245271117607273049L;
	
	private Integer id;
	
	private String code;
	
	private String proCode;  //对应ProductInfo中code
	
	private String serialNo; //对应ProductInfo中serialNo
	
	private String proTypeName; //产品名称

	private String autelId; //产品对应的用户
	
	private String saleCode;//销售契约code
	
	private String saleName;//销售契约name
	
	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getProCode() {
		return proCode;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}
	
}
