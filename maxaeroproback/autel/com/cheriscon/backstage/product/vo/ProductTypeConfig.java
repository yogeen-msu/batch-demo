package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;

public class ProductTypeConfig  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 746486637312411636L;
	
	private Integer id;
	private String code;
	private String proTypeCode;  //内部产品编码
	private String languageCode; //语言编码
	private String name; //产品名称
	private String languageName; //语言名称
	
	@NotDbField
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getId() {
		return id;
	}
	public String getProTypeCode() {
		return proTypeCode;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public String getName() {
		return name;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public void setName(String name) {
		this.name = name;
	}

}
