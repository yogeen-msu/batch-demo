package com.cheriscon.backstage.product.vo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.utils.UploadUtil;

public class VehicleTypeMapper implements RowMapper {

	private String downPath;
	public VehicleTypeMapper(String testPath){
		this.downPath=testPath;
	}
	
	@Override
	public Object mapRow(ResultSet result, int arg1) throws SQLException {
		VehicleType vehicleType = new VehicleType();
		vehicleType.setRowNum(result.getInt("rowNum"));
		/*vehicleType.setNo(result.getInt("no"));*/
//		vehicleType.setPath(UploadUtil.replacePath(result.getString("path")));
		String path = result.getString("path");
		if(StringUtils.isNotEmpty(path)){
			path = path.replaceAll(CopSetting.FILE_STORE_PREFIX, "http://pro.auteltech.com/statics");
		}
		vehicleType.setPath(path);
		vehicleType.setSname(result.getString("sname"));
		vehicleType.setVname(result.getString("vname"));
		vehicleType.setProName(result.getString("proName"));
		String testPath=result.getString("testPath");
		
		if(testPath!=null){
			testPath=downPath+testPath.replace("\\", "/");
		}
		
		vehicleType.setTestPath(testPath);
		vehicleType.setFunctionName(result.getString("functionName"));
		return vehicleType;
	}

}
