/**
*/ 
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

/**
 * @ClassName: SoftwareVersionPageVo
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-26 下午01:12:01
 * 
 */
public class SoftwareVersionPageVo implements Serializable{
	private static final long serialVersionUID = -822905958832051775L;
	private String versionName;
	/**
	 * 功能软件编码
	 */
	private String softwareTypeCode;
	
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}
	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}
	
	
}
