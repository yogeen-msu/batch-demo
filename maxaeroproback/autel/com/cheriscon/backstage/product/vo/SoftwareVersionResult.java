/**
 */
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

/**
 * @ClassName: SoftwareVersionResult
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-26 上午11:53:10
 * 
 */
public class SoftwareVersionResult implements Serializable {

	private static final long serialVersionUID = 4099586913776602918L;
	private Integer id;
	private String code;
	/**
	 * 软件类型Code
	 */
	private String softwareTypeCode;
	/**
	 * 版本名称
	 */
	private String versionName;
	/**
	 * 发布日期
	 */
	private String releaseDate;
	/**
	 * 发布状态 0表示未发布 1表示发布
	 */
	private Integer releaseState;

	/**
	 * 版本创建时间
	 */
	private String createDate;

	/**
	 * 基础包编码
	 */
	private String softwarePackCode;

	/**
	 * 基础包路径
	 */
	private String softPackPath;

	/**
	 * 语言编码
	 */
	private String languageTypeCode;
	/**
	 * 语言包编码
	 */
	private String languagePackCode;

	/**
	 * 升级公告标题
	 */
	private String title;

	/**
	 * 下载路径
	 */
	private String downloadPath;
	/**
	 * 可测车型文档路径
	 */
	private String testPath;
	/**
	 * 语言包说明
	 */
	private String memo;

	/**
	 * 语言名称
	 */
	private String languageName;

	public SoftwareVersionResult() {
		super();
	}

	public SoftwareVersionResult(Integer id, String code,
			String softwareTypeCode, String versionName, String releaseDate,
			Integer releaseState, String createDate, String softwarePackCode,
			String softPackPath, String languageTypeCode,
			String languagePackCode, String title, String downloadPath,
			String testPath, String memo, String languageName) {
		super();
		this.id = id;
		this.code = code;
		this.softwareTypeCode = softwareTypeCode;
		this.versionName = versionName;
		this.releaseDate = releaseDate;
		this.releaseState = releaseState;
		this.createDate = createDate;
		this.softwarePackCode = softwarePackCode;
		this.softPackPath = softPackPath;
		this.languageTypeCode = languageTypeCode;
		this.languagePackCode = languagePackCode;
		this.title = title;
		this.downloadPath = downloadPath;
		this.testPath = testPath;
		this.memo = memo;
		this.languageName = languageName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}

	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Integer getReleaseState() {
		return releaseState;
	}

	public void setReleaseState(Integer releaseState) {
		this.releaseState = releaseState;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getSoftwarePackCode() {
		return softwarePackCode;
	}

	public void setSoftwarePackCode(String softwarePackCode) {
		this.softwarePackCode = softwarePackCode;
	}

	public String getSoftPackPath() {
		return softPackPath;
	}

	public void setSoftPackPath(String softPackPath) {
		this.softPackPath = softPackPath;
	}

	public String getLanguageTypeCode() {
		return languageTypeCode;
	}

	public void setLanguageTypeCode(String languageTypeCode) {
		this.languageTypeCode = languageTypeCode;
	}

	public String getLanguagePackCode() {
		return languagePackCode;
	}

	public void setLanguagePackCode(String languagePackCode) {
		this.languagePackCode = languagePackCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getTestPath() {
		return testPath;
	}

	public void setTestPath(String testPath) {
		this.testPath = testPath;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

}
