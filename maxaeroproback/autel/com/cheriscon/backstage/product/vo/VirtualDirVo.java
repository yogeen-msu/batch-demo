/**
*/ 
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: VirtualDirVo
 * @Description: 虚拟目录树业务对象
 * @author pengdongan
 * @date 2013-11-26
 * 
 */
public class VirtualDirVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2443033434290666017L;
	
	private String code;
	private String name;
	private List<VirtualDirVo> virtualDirVos;
	
	public VirtualDirVo() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<VirtualDirVo> getVirtualDirVos() {
		return virtualDirVos;
	}

	public void setVirtualDirVos(List<VirtualDirVo> virtualDirVos) {
		this.virtualDirVos = virtualDirVos;
	}	
	
}
