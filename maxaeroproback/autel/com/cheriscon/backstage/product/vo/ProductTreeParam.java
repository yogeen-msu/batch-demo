/**
* @ClassName: ProductTreeVo
* @Description: TODO
* @author shaohu
* @date 2013-1-8 下午06:57:36
*
*/ 
package com.cheriscon.backstage.product.vo;

import java.io.Serializable;

/**
 * @ClassName: ProductTreeParam
 * @Description: 产品树异步参数
 * @author shaohu
 * @date 2013-1-8 下午06:57:36
 * 
 */
public class ProductTreeParam implements Serializable{

	private static final long serialVersionUID = 6891877017095412639L;

	private String id ;
	
	private String code;

	public ProductTreeParam() {
		super();
	}

	public ProductTreeParam(String id, String code) {
		super();
		this.id = id;
		this.code = code;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.code = id;
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
