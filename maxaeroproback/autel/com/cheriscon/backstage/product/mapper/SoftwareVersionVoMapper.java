/**
*/ 
package com.cheriscon.backstage.product.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.backstage.product.vo.SoftwareVersionVo;

/**
 * @ClassName: SoftwareVersionVoMapper
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-12 下午04:39:31
 * 
 */
public class SoftwareVersionVoMapper implements RowMapper{

	/**
	  * <p>Title: mapRow</p>
	  * <p>Description: </p>
	  * @param arg0
	  * @param arg1
	  * @return
	  * @throws SQLException
	  */ 
	@Override
	public Object mapRow(ResultSet rs, int arg1) throws SQLException {
		SoftwareVersionVo v = new SoftwareVersionVo();
		v.setName(rs.getString("name"));
		v.setReleaseState(rs.getInt("releaseState"));
		v.setSoftwareTypeCode(rs.getString("softwareTypeCode"));
		v.setVersionCode(rs.getString("versionCode"));
		v.setVersionName(rs.getString("versionName"));
		return v;
	}

}
