/**
*/ 
package com.cheriscon.backstage.product.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.cop.sdk.utils.UploadUtil;

/**
 * @ClassName: SoftwareTypeMapper
 * @Description: 软件类型mapper
 * @author shaohu
 * @date 2013-1-10 下午08:11:07
 * 
 */
public class SoftwareTypeMapper implements RowMapper {

	/**
	 * <p>Title: mapRow</p>
	 * <p>Description: </p>
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 */
	@Override
	public Object mapRow(ResultSet result, int arg1) throws SQLException {
		SoftwareType softwareType = new SoftwareType();
		softwareType.setId(result.getInt("id"));
		softwareType.setCode(result.getString("code"));
		softwareType.setName(result.getString("name"));
		softwareType.setProTypeCode(result.getString("proTypeCode"));
//		softwareType.setCarSignPath(result.getString("carSignPath"));
		String carSignPath  = result.getString("carSignPath");
		String newCarSignPath  = result.getString("newCarSignPath");
		if(carSignPath!=null){
			carSignPath  =UploadUtil.replacePath(carSignPath); 
			newCarSignPath=UploadUtil.replacePath(newCarSignPath); 
			softwareType.setCarSignPath(carSignPath);
			softwareType.setNewCarSignPath(newCarSignPath);
		}
		return softwareType;
	}

}
