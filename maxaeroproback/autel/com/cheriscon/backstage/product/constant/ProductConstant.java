package com.cheriscon.backstage.product.constant;

public final class ProductConstant {
	
	/**
	 * 产品树的根id
	 */
	public final static String ROOT_ID = "1";
	
	/**
	 * 已经发布
	 */
	public final static int HAS_RELEASE	= 1;
	
	/**
	 *  未发布
	 */
	public final static int NOT_RELEASE	= 0;
}
