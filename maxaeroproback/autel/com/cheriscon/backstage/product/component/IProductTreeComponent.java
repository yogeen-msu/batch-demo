/**
* @ClassName: IProductTreeComponent
* @Description: TODO
* @author shaohu
* @date 2013-1-8 下午07:13:13
*
*/ 
package com.cheriscon.backstage.product.component;

import com.cheriscon.backstage.product.vo.ProductTreeParam;

/**
 * @ClassName: IProductTreeComponent
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-8 下午07:13:13
 * 
 */
public interface IProductTreeComponent {
	
	public String getProductTree(ProductTreeParam treeParam) throws Exception;
	
	public String bulidProductTree(String rootName) throws Exception;
	
	/**
	* @Title: bulidVirtualDirTree
	* @Description: 构建虚拟目录树
	* @return String    
	* @throws
	* @author pengdongan
	* @date 2013-11-26
	 */
	public String bulidVirtualDirTree(String rootName) throws Exception;
}
