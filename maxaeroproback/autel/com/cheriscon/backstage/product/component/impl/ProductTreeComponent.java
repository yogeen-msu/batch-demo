/**
 * @ClassName: ProductTreeComponent
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-8 下午06:18:59
 *
 */
package com.cheriscon.backstage.product.component.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.cheriscon.backstage.product.component.IProductTreeComponent;
import com.cheriscon.backstage.product.constant.ProductConstant;
import com.cheriscon.backstage.product.service.IProductSoftwareConfigService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.service.ISoftwareTypeService;
import com.cheriscon.backstage.product.vo.ProductTree;
import com.cheriscon.backstage.product.vo.ProductTreeParam;
import com.cheriscon.backstage.product.vo.ProductTreeVo;
import com.cheriscon.backstage.product.vo.VirtualDirVo;
import com.cheriscon.common.model.ProductSoftwareConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.utils.JSONHelpUtils;

/**
 * @ClassName: ProductTreeComponent
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-8 下午06:18:59
 * 
 */
@Component
public class ProductTreeComponent implements IProductTreeComponent {

	@Resource
	private IProductTypeService productTypeService;

	@Resource
	private ISoftwareTypeService typeService;
	
	@Resource
	private IProductSoftwareConfigService productSoftwareConfigService;

	public String getProductTree(ProductTreeParam treeParam) throws Exception {
		// 第一次访问参数为空
		if (null == treeParam || StringUtils.isBlank(treeParam.getCode())) {
			return this.initProductTreeRoot();
		} else if (StringUtils.equals(treeParam.getCode(),
				ProductConstant.ROOT_ID)) {
			List<ProductType> productTypes = productTypeService.listAll();
			return this.getProductTypeJson(productTypes, treeParam.getCode());
		} else {
			List<SoftwareType> softwareTypes = typeService
					.getSoftwareTypeByProTypeCode(treeParam.getCode());
			return this.getSoftwareTypeJson(softwareTypes, treeParam.getCode());
		}
	}

	/**
	 * 初始化根节点
	 * 
	 * @return
	 * @throws Exception
	 */
	private String initProductTreeRoot() throws Exception {
		List<Object> list = new ArrayList<Object>();
		ProductTree tree = new ProductTree();
		tree.setId(ProductConstant.ROOT_ID);
		tree.setName("产品管理");
		// 没有叶子节点
		tree.setIsParent(false);
		List<ProductType> productTypes = productTypeService.listAll();
		if (null != productTypes && productTypes.size() != 0) {
			// 有叶子节点
			tree.setIsParent(true);
		}
		// tree.setOpen(true);
		tree.setpId("0");
		tree.setUrl("search.do");
		list.add(tree);
		return JSONHelpUtils.getJsonString4JavaPOJOList(list);
	}

	/**
	 * 
	 * @Title: getProductTypeJson
	 * @Description: 根据pid和产品类型数据 将数据转换为JSON格式
	 * @param productTypes
	 *            产品型号数据
	 * @param pId
	 *            父id
	 * @return String 产品类型JSON数据
	 * @throws
	 */
	private String getProductTypeJson(List<ProductType> productTypes, String pId)
			throws Exception {

		if (null == productTypes || productTypes.size() == 0)
			return null;
		List<Object> list = new ArrayList<Object>();
		for (ProductType t : productTypes) {
			ProductTree tree = new ProductTree();
			tree.setId(t.getCode());
			tree.setName(t.getName());
			tree.setpId(pId);
			// 跳转到软件类型
			tree.setUrl("toSoftwareType.do?productType.name=" + t.getName()+"&productType.code="+t.getCode());
			tree.setIsParent(false);
			List<SoftwareType> softwareTypes = typeService
					.getSoftwareTypeByProTypeCode(t.getCode());
			if (null != softwareTypes && softwareTypes.size() != 0) {
				tree.setIsParent(true);
			}
			list.add(tree);
		}
		return JSONHelpUtils.getJsonString4JavaPOJOList(list);
	}

	/**
	 * 
	 * @Title: getSoftwareTypeJson
	 * @Description: 根据pid和软件类型数据 将数据转换为JSON
	 * @param softwareTypes
	 *            软件类型数据
	 * @param pId
	 *            父id
	 * @return String
	 * @throws
	 */
	private String getSoftwareTypeJson(List<SoftwareType> softwareTypes,
			String pId) throws Exception {
		if (null == softwareTypes || softwareTypes.size() == 0)
			return null;
		List<Object> list = new ArrayList<Object>();
		
		for (SoftwareType softwareType : softwareTypes) {
			ProductTree tree = new ProductTree();
			tree.setId(softwareType.getCode());
			tree.setName(softwareType.getName());
			tree.setpId(pId);
			tree.setUrl("toSoftwareVersion.do?softwareType.code=" + softwareType.getCode());
			tree.setIsParent(false);
			list.add(tree);
		}
		return JSONHelpUtils.getJsonString4JavaPOJOList(list);
	}
	
	/**
	 * 
	* @Title: bulidProductTree
	* @Description: 获得产品树
	* @param    
	* @return void    
	* @throws
	 */
	public String bulidProductTree(String rootName) throws Exception{
		ProductTreeVo treeVo = new ProductTreeVo();
		treeVo.setUrl("search.do");
		treeVo.setText(rootName);
		
		List<ProductType> productTypes = productTypeService.listAll();
		
		Map<String,List<SoftwareType>> map = new HashMap<String,List<SoftwareType>>();
		
		if(null != productTypes && productTypes.size() > 0){
			
			//获取所有的功能软件
			List<SoftwareType> softwareTypes = typeService.listAll();
			if(null != softwareTypes && softwareTypes.size() > 0){
				List<SoftwareType> temp = null;
				for(SoftwareType softwareType : softwareTypes){
					if(map.containsKey(softwareType.getProTypeCode())){
						temp = map.get(softwareType.getProTypeCode());
					}else{
						temp = new ArrayList<SoftwareType>();
					}
					temp.add(softwareType);
					map.put(softwareType.getProTypeCode(), temp);
				}
			}
			
			for(ProductType productType : productTypes){
				productType.setSoftwareTypes(map.get(productType.getCode()));
			}
		}
		treeVo.setProductTypes(productTypes);
		List<Object> list = new ArrayList<Object>();
		list.add(treeVo);
		return JSONHelpUtils.getJsonString4JavaPOJOList(list);
	}


	/**
	* @Title: bulidVirtualDirTree
	* @Description: 构建虚拟目录树
	* @return String    
	* @throws
	* @author pengdongan
	* @date 2013-11-26
	 */
	public String bulidVirtualDirTree(String rootName) throws Exception{
		
		List<ProductSoftwareConfig> productSoftwareConfigs = productSoftwareConfigService.listVirtualDirAll();
		
		VirtualDirVo virtualDirVo = new VirtualDirVo();
		
		virtualDirVo.setCode("0");
		virtualDirVo.setName(rootName);
		
		bulidVirtualDir(virtualDirVo,productSoftwareConfigs);

		List<Object> list = new ArrayList<Object>();
		list.add(virtualDirVo);
		return JSONHelpUtils.getJsonString4JavaPOJOList(list);
	}


	/**
	* @Title: bulidVirtualDir
	* @Description: 构建虚拟目录
	* @return String    
	* @throws
	* @author pengdongan
	* @date 2013-11-26
	 */
	private void bulidVirtualDir(VirtualDirVo virtualDirVo,List<ProductSoftwareConfig> productSoftwareConfigs) throws Exception {
		
		for (ProductSoftwareConfig productSoftwareConfig : productSoftwareConfigs) {
			if(productSoftwareConfig.getParentCode().equals(virtualDirVo.getCode()))
			{
				VirtualDirVo virtualDirVo2 = new VirtualDirVo();
				virtualDirVo2.setCode(productSoftwareConfig.getCode());
				virtualDirVo2.setName(productSoftwareConfig.getName());
				
				bulidVirtualDir(virtualDirVo2,productSoftwareConfigs);
				
				if(virtualDirVo.getVirtualDirVos() != null)
				{
					virtualDirVo.getVirtualDirVos().add(virtualDirVo2);
				}else {
					List<VirtualDirVo> virtualDirVos = new ArrayList<VirtualDirVo>();
					virtualDirVos.add(virtualDirVo2);
					virtualDirVo.setVirtualDirVos(virtualDirVos);
				}
			}
		}
		
	}
}
