package com.cheriscon.backstage.order.action;



import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.order.service.IOrderInfoLogService;

import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderInfoLog;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * @remark	订单修改日志action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/autel/order")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class OrderInfoLogAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IAdminUserManager adminUserManager;

	@Resource
	private IOrderInfoService orderInfoService;

	@Resource
	private IOrderInfoLogService orderInfoLogService;

	@Resource
	private ISealerInfoService sealerInfoService;
	
	private OrderInfoLog orderInfoLog;
	
	private List<OrderInfoLog> orderInfoLogs;
	
	private List<SealerInfo> sealerInfos;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private OrderInfoLog orderInfoLogSel;
	private OrderInfoLog orderInfoLogAdd;
	private OrderInfoLog orderInfoLogEdit;
	
	private String jsonData;
	
	
	//获取订单修改日志列表
	public void list(){
		try{
			this.webpage = orderInfoLogService.pageOrderInfoLogPage(orderInfoLogSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listorderinfolog", results = { @Result(name = "listorderinfolog", location = "/autel/backstage/order/order_log_list.jsp") })
	public String listOrderInfoLog() throws Exception{
		list();
		return "listorderinfolog";
	}

	@Action(value = "addorderinfolog", results = { @Result(name = "addorderinfolog", location = "/autel/backstage/order/order_log_add.jsp") })
	public String addOrderInfoLog() throws Exception{
		return "addorderinfolog";
	}

//	@Action(value = "insertorderinfolog", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertorderinfolog")
	public String insertOrderInfoLog() throws Exception{
		int result = -1;
		try{
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			
			String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			orderInfoLogAdd.setUpdateMoney(String.format("%.2f", Float.parseFloat(orderInfoLogAdd.getUpdateMoney())));
			orderInfoLogAdd.setOrderMoney(orderInfoService.queryOrderInfoByCode(orderInfoLogAdd.getOrderCode()).getOrderMoney());
			orderInfoLogAdd.setUpdatePerson(user.getUsername());
			orderInfoLogAdd.setUpdateDate(dateStr);
			orderInfoLogService.insertOrderInfoLogDetail(orderInfoLogAdd);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.addSuccess"));
		}
		else
		{
			this.msgs.add(getText("common.js.addFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listorderinfolog.do");
		
		return MESSAGE;
	}

	@Action(value = "editorderinfolog", results = { @Result(name = "editorderinfolog", location = "/autel/backstage/order/order_log_edit.jsp") })
	public String editOrderInfoLog() throws Exception{
		try{
			this.orderInfoLogEdit = orderInfoLogService.getOrderInfoLogByCode(code);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editorderinfolog";
	}

//	@Action(value = "updateorderinfolog", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updateorderinfolog")
	public String updateOrderInfoLog() throws Exception{
		int result = -1;
		
		try{

			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			
			String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			orderInfoLogEdit.setUpdateMoney(String.format("%.2f", Float.parseFloat(orderInfoLogEdit.getUpdateMoney())));
			orderInfoLogEdit.setUpdatePerson(user.getUsername());
			orderInfoLogEdit.setUpdateDate(dateStr);
			
			orderInfoLogService.updateOrderInfoLogDetail(orderInfoLogEdit);
			result = 0;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.modSuccess"));
		}
		else
		{
			this.msgs.add(getText("common.js.modFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listorderinfolog.do");
		
		return MESSAGE;
	}

//	@Action(value = "delorderinfolog", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delorderinfolog")
	public String delOrderInfoLog() throws Exception{
		int result = -1;
		try{
			orderInfoLogService.delOrderInfoLogDetail(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.del"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("common.js.nonDel"));
		}
		else
		{
			this.msgs.add(getText("common.js.delFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listorderinfolog.do");
		
		return MESSAGE;
	}

	/**
	 * 验证是否未支付有效订单
	 * @return
	 * @throws Exception
	 */
	@Action(value = "checkordernopay", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkOrderNopay() throws Exception {
		int result = -1;
		OrderInfo orderInfo = orderInfoService.queryOrderInfoByCode(orderInfoLog.getOrderCode());
		if(orderInfo != null && orderInfo.getOrderState()==FrontConstant.ORDER_STATE_YES_DONE && orderInfo.getOrderPayState()==FrontConstant.ORDER_PAY_STATE_NO) result = 0;
		
		this.jsonData = JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}

	/**
	 * 验证订单号有效性
	 * @return
	 * @throws Exception
	 */
	@Action(value = "checkordercode", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkorderCode() throws Exception {
		int result = -1;
		if(orderInfoService.queryOrderInfoByCode(orderInfoLog.getOrderCode()) !=null) result = 0;
		
		this.jsonData = JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}

	/**
	 * 得到订单价格
	 * @return
	 * @throws Exception
	 */
	@Action(value = "getordermoney", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getOrderMoney() throws Exception {
		String orderMoney = "";
		orderMoney = orderInfoService.queryOrderInfoByCode(orderInfoLog.getOrderCode()).getOrderMoney();
		//this.jsonData = JSONArray.fromObject(orderMoney).toString();
		this.jsonData = "["+orderMoney+"]";
		
		return SUCCESS;
	}

	public OrderInfoLog getOrderInfoLog() {
		return orderInfoLog;
	}

	public void setOrderInfoLog(OrderInfoLog orderInfoLog) {
		this.orderInfoLog = orderInfoLog;
	}

	public List<OrderInfoLog> getOrderInfoLogs() {
		return orderInfoLogs;
	}

	public void setOrderInfoLogs(List<OrderInfoLog> orderInfoLogs) {
		this.orderInfoLogs = orderInfoLogs;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public OrderInfoLog getOrderInfoLogSel() {
		return orderInfoLogSel;
	}

	public void setOrderInfoLogSel(OrderInfoLog orderInfoLogSel) {
		this.orderInfoLogSel = orderInfoLogSel;
	}

	public OrderInfoLog getOrderInfoLogAdd() {
		return orderInfoLogAdd;
	}

	public void setOrderInfoLogAdd(OrderInfoLog orderInfoLogAdd) {
		this.orderInfoLogAdd = orderInfoLogAdd;
	}

	public OrderInfoLog getOrderInfoLogEdit() {
		return orderInfoLogEdit;
	}

	public void setOrderInfoLogEdit(OrderInfoLog orderInfoLogEdit) {
		this.orderInfoLogEdit = orderInfoLogEdit;
	}

	public List<SealerInfo> getSealerInfos() {
		return sealerInfos;
	}

	public void setSealerInfos(List<SealerInfo> sealerInfos) {
		this.sealerInfos = sealerInfos;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

}	
