package com.cheriscon.backstage.order.action;



import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.order.service.IOrderPayStatusLogService;

import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderPayStatusLog;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * @remark	订单支付状态修改日志action
 * @author pengdongan
 * @date   2013-5-30
 */
@ParentPackage("json_default")
@Namespace("/autel/order")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class OrderPayStatusLogAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IAdminUserManager adminUserManager;

	@Resource
	private IOrderInfoService orderInfoService;

	@Resource
	private IOrderPayStatusLogService orderPayStatusLogService;
	
	private OrderPayStatusLog orderPayStatusLog;
	
	private List<OrderPayStatusLog> orderPayStatusLogs;
	
	private List<SealerInfo> sealerInfos;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private OrderPayStatusLog orderPayStatusLogSel;
	private OrderPayStatusLog orderPayStatusLogAdd;
	private OrderPayStatusLog orderPayStatusLogEdit;
	
	private String jsonData;
	
	
	//获取订单支付状态修改日志列表
	public void list(){
		try{
			this.webpage = orderPayStatusLogService.pageOrderPayStatusLogPage(orderPayStatusLogSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listorderpaystatuslog", results = { @Result(name = "listorderpaystatuslog", location = "/autel/backstage/order/order_paystatus_list.jsp") })
	public String listOrderPayStatusLog() throws Exception{
		list();
		return "listorderpaystatuslog";
	}

	@Action(value = "addorderpaystatuslog", results = { @Result(name = "addorderpaystatuslog", location = "/autel/backstage/order/order_paystatus_add.jsp") })
	public String addOrderPayStatusLog() throws Exception{
		return "addorderpaystatuslog";
	}

//	@Action(value = "insertorderpaystatuslog", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertorderpaystatuslog")
	public String insertOrderPayStatusLog() throws Exception{
		int result = -1;
		try{
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			
			String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			orderPayStatusLogAdd.setUpdatePerson(user.getUsername());
			orderPayStatusLogAdd.setUpdateDate(dateStr);
			orderPayStatusLogService.insertOrderPayStatusLogDetail(orderPayStatusLogAdd);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.addSuccess"));
		}
		else
		{
			this.msgs.add(getText("common.js.addFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listorderpaystatuslog.do");
		
		return MESSAGE;
	}

	/**
	 * 验证是否未支付有效订单
	 * @return
	 * @throws Exception
	 */
	@Action(value = "paystatuscheckordernopay", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String paystatusCheckOrderNopay() throws Exception {
		int result = -1;
		OrderInfo orderInfo = orderInfoService.queryOrderInfoByCode(orderPayStatusLog.getOrderCode());
		if(orderInfo != null && orderInfo.getOrderState()==FrontConstant.ORDER_STATE_YES_DONE && orderInfo.getOrderPayState()==FrontConstant.ORDER_PAY_STATE_NO) result = 0;
		
		this.jsonData = JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}

	/**
	 * 验证订单号有效性
	 * @return
	 * @throws Exception
	 */
	@Action(value = "paystatuscheckordercode", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String paystatusCheckorderCode() throws Exception {
		int result = -1;
		if(orderInfoService.queryOrderInfoByCode(orderPayStatusLog.getOrderCode()) !=null) result = 0;
		
		this.jsonData = JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}

	public OrderPayStatusLog getOrderPayStatusLog() {
		return orderPayStatusLog;
	}

	public void setOrderPayStatusLog(OrderPayStatusLog orderPayStatusLog) {
		this.orderPayStatusLog = orderPayStatusLog;
	}

	public List<OrderPayStatusLog> getOrderPayStatusLogs() {
		return orderPayStatusLogs;
	}

	public void setOrderPayStatusLogs(List<OrderPayStatusLog> orderPayStatusLogs) {
		this.orderPayStatusLogs = orderPayStatusLogs;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public OrderPayStatusLog getOrderPayStatusLogSel() {
		return orderPayStatusLogSel;
	}

	public void setOrderPayStatusLogSel(OrderPayStatusLog orderPayStatusLogSel) {
		this.orderPayStatusLogSel = orderPayStatusLogSel;
	}

	public OrderPayStatusLog getOrderPayStatusLogAdd() {
		return orderPayStatusLogAdd;
	}

	public void setOrderPayStatusLogAdd(OrderPayStatusLog orderPayStatusLogAdd) {
		this.orderPayStatusLogAdd = orderPayStatusLogAdd;
	}

	public OrderPayStatusLog getOrderPayStatusLogEdit() {
		return orderPayStatusLogEdit;
	}

	public void setOrderPayStatusLogEdit(OrderPayStatusLog orderPayStatusLogEdit) {
		this.orderPayStatusLogEdit = orderPayStatusLogEdit;
	}

	public List<SealerInfo> getSealerInfos() {
		return sealerInfos;
	}

	public void setSealerInfos(List<SealerInfo> sealerInfos) {
		this.sealerInfos = sealerInfos;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

}	
