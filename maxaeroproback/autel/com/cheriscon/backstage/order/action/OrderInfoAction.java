/**
*/ 
package com.cheriscon.backstage.order.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.order.service.IOrderInfoBackService;
import com.cheriscon.backstage.order.vo.OrderSearch;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * @ClassName: OrderInfoAction
 * @Description: 订单管理控制类
 * @author shaohu
 * @date 2013-1-25 下午03:11:05
 * 
 */
@ParentPackage("json_default")
@Namespace("/core/admin")
public class OrderInfoAction extends WWAction{

	private static final long serialVersionUID = 1059928697909659903L;
	
	@Resource
	private IOrderInfoBackService orderInfoBackService;
	
	@Resource
	private ILanguageService languageService;
	
	private String code;
	
	private String proCode;
	
	private String year;
	
	private OrderSearch orderSearch;
	
	private OrderInfo orderInfo;
	
	private String orderCodes;
	
	private String jsonData;
	
	public OrderSearch getOrderSearch() {
		return orderSearch;
	}

	public void setOrderSearch(OrderSearch orderSearch) {
		this.orderSearch = orderSearch;
	}

	public String getOrderCodes() {
		return orderCodes;
	}

	public void setOrderCodes(String orderCodes) {
		this.orderCodes = orderCodes;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 
	* @Title: toOrderInfoList
	* @Description: 调整到订单列表
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toOrderInfoList", results = { @Result(name = SUCCESS, location = "/autel/backstage/order/order_list.jsp") })
	public String toOrderInfoList() throws Exception{
		if(null == orderSearch){
			orderSearch = new OrderSearch();
			orderSearch.setOrderState(1);
			orderSearch.setOrderPayState(1);
			orderSearch.setRecConfirmState(1);
			orderSearch.setProcessState(0);
		}
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
//		String languageCode = "lag201301070146010554";
		this.webpage = orderInfoBackService.pageOrderInfo(orderSearch, this.getPage(), this.getPageSize(), language.getCode());
		return SUCCESS;
	}
	
/*	@Action(value = "exprotOrderExcel", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public void exprotOrderExcel() throws Exception{
		if(null == orderSearch){
			orderSearch = new OrderSearch();
		}
		HttpServletResponse response = ThreadContextHolder.getHttpResponse();
		String languageCode = "lag201301070146010554";
		orderInfoBackService.exprotOrderExcel(orderSearch, languageCode);
		FileInputStream fin = null;
		Map<String, String> map = new HashMap<String, String>();
		try {  
			File outputFile = new File("F://c.xls");  
	        response.reset();// 清空输出流  
//	        response.setContentType("application/vnd.ms-excel");// 定义输出类型  
	        response.setHeader("Content-disposition", "attachment; filename="  
	            + new SimpleDateFormat("yyyyMMdd_HHmmssSSS")  
	                      .format(new Date()) +".xls");// 设定输出文件头  
	        response.setContentType("application/octet-stream");
	        // 读取文件并且输出  
	        fin = new FileInputStream(outputFile);  
	        byte[] tempBytes = new byte[2048];  
	        while (fin.read(tempBytes) != -1) {  
	            response.getOutputStream().write(tempBytes);  
	        }  
	        map.put("flag", "true");
	    } catch (IOException e) {  
	    	map.put("flag", "false");
	        e.printStackTrace();  
	    }finally{
	    	if(fin != null){
	    		fin.close();
	    	}
	    response.getOutputStream().flush();
///    	response.getOutputStream().close();  
	    }
		
		
		
		jsonData = JSONArray.fromObject(map).toString();
//		return SUCCESS;
	}
	*/
	/**
	 * 
	* @Title: toOrderInfoList
	* @Description: 调整到订单确认列表
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toOrderConfirmList", results = { @Result(name = SUCCESS, location = "/autel/backstage/order/orderConfirm_list.jsp") })
	public String toOrderConfirmList() throws Exception{
		if(null == orderSearch){
			orderSearch = new OrderSearch();
			orderSearch.setOrderState(1);
			orderSearch.setOrderPayState(1);
			orderSearch.setRecConfirmState(0);
		}
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		
//		String languageCode = "lag201301070146010554";
		this.webpage = orderInfoBackService.pageOrderInfo(orderSearch, this.getPage(), this.getPageSize(), language.getCode());
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: updateRenewal
	* @Description: 更新产品有效期
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "processState")
	public String processState() throws Exception{
//		Map<String,String> map = new HashMap<String,String>();
		try {
			this.orderInfoBackService.processState(code);
			this.msgs.add(getText("orderman.js.updatevalidateSuccess"));
			this.urls.put(
					getText("orderman.list"),
					"toOrderInfoList.do");
//			map.put("flag", "true");
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(
					getText("common.btn.return"),
					"toOrderInfoList.do");
//			map.put("flag", "false");
		}
		return MESSAGE;
//		jsonData = JSONArray.fromObject(map).toString();
//		this.render(jsonData, "text/x-json;charset=UTF-8");
	}
//	public void processState() throws Exception{
//		Map<String,String> map = new HashMap<String,String>();
//		try {
//			this.orderInfoBackService.processState(code);
//			map.put("flag", "true");
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("flag", "false");
//		}
//		jsonData = JSONArray.fromObject(map).toString();
//		this.render(jsonData, "text/x-json;charset=UTF-8");
//	}
	
	/**
	 * 
	* @Title: orderConfirm
	* @Description: 订单确认
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "orderConfirm" )
	public String orderConfirm() throws Exception{
//		Map<String,String> map = new HashMap<String,String>();
//		try{
//			this.orderInfoBackService.orderConfirm(orderCodes);
//			map.put("flag", "1");
//		}catch(Exception e){
//			map.put("flag", "0");
//			throw e;
//		}
//		this.jsonData =JSONArray.fromObject(map).toString();
		try{
			this.orderInfoBackService.orderConfirm(orderCodes);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(
					getText("market.mainsale.message5"),
					"toOrderConfirmList.do");
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(
					getText("common.btn.return"),
					"toOrderConfirmList.do");
		}
		return MESSAGE;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "updateBankNumber" )
	public String updateBankNumber() throws Exception{
		try{
			this.orderInfoBackService.updateBankNumber(orderInfo);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(
					getText("market.mainsale.message5"),
					"toOrderConfirmList.do");
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(
					getText("common.btn.return"),
					"toOrderConfirmList.do");
		}
		return MESSAGE;
	}

}
