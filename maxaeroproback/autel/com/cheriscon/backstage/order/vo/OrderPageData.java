/**
*/ 
package com.cheriscon.backstage.order.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderMinSaleUnitDetail;

/**
 * @ClassName: OrderResult
 * @Description: 订单分页对象
 * @author shaohu
 * @date 2013-1-25 下午02:54:34
 * 
 */
public class OrderPageData implements Serializable{
	
	private static final long serialVersionUID = 7451623405180649063L;

	private OrderInfo orderInfo;
	
	private List<OrderMinSaleUnitDetail> details = new ArrayList<OrderMinSaleUnitDetail>(); //订单最小销售单表详细

	public OrderPageData() {
		super();
	}

	public OrderPageData(OrderInfo orderInfo, List<OrderMinSaleUnitDetail> details) {
		super();
		this.orderInfo = orderInfo;
		this.details = details;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public List<OrderMinSaleUnitDetail> getDetails() {
		return details;
	}

	public void setDetails(List<OrderMinSaleUnitDetail> details) {
		this.details = details;
	}
	
	
}
