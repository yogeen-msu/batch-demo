/**
*/ 
package com.cheriscon.backstage.order.vo;

import java.io.Serializable;

/**
 * @ClassName: OrderInfoVo
 * @Description: 订单信息vo
 * @author shaohu
 * @date 2013-1-25 下午01:53:55
 * 
 */
public class OrderInfoVo implements Serializable{

	private static final long serialVersionUID = -4339861170625493012L;
	private Integer id;
	private String code;
	private String userCode;				//用户code
	private String orderMoney;				//订单金额
	private String orderDate;				//下单日期
	private Integer orderState;				//订单状态
	private Integer orderPayState;			//订单支付状态（0：未支付 1：已支付）
	private Integer recConfirmState;		//收款确认状态（0：未确认 1：已确认）
	private String discountMoney;			//优惠金额
	private String bankNumber;				//银行流水号
	private String customerName ;//用户名称
	private Integer processState; //处理状态 1：已处理  0：未处理
	private String payDate;//支付成功时间
	private String recConfirmDate;//确认收款时间
	
	private String detailCode;	//订单销售单位表编码
	private String orderCode; 					// 订单code
	private String productSerialNo; 			// 产品序列号
	private String minSaleUnitCode; 			// 最小销售单位Code
	private Integer consumeType;				// 消费类型（0：购买 1：续费）
	private Double minSaleUnitPrice; 				// 最小销售单位价格
	private String saleName;	//最小销售单位名称
	private Integer year;	//续租年限
	
	private String proName;	//最小销售单位名称
	private String proCode;//产品code
	private String picPath;//产品图片路径
	private Integer isSoft;	

	public OrderInfoVo() {
		super();
	}
	
	

	public OrderInfoVo(Integer id, String code, String userCode,
			String orderMoney, String orderDate, Integer orderState,
			Integer orderPayState, Integer recConfirmState,
			String discountMoney, String bankNumber, String customerName,
			Integer processState, String payDate, String recConfirmDate,
			String detailCode, String orderCode, String productSerialNo,
			String minSaleUnitCode, Integer consumeType,
			Double minSaleUnitPrice, String saleName, Integer year,
			String proName, String proCode, String picPath, Integer isSoft) {
		super();
		this.id = id;
		this.code = code;
		this.userCode = userCode;
		this.orderMoney = orderMoney;
		this.orderDate = orderDate;
		this.orderState = orderState;
		this.orderPayState = orderPayState;
		this.recConfirmState = recConfirmState;
		this.discountMoney = discountMoney;
		this.bankNumber = bankNumber;
		this.customerName = customerName;
		this.processState = processState;
		this.payDate = payDate;
		this.recConfirmDate = recConfirmDate;
		this.detailCode = detailCode;
		this.orderCode = orderCode;
		this.productSerialNo = productSerialNo;
		this.minSaleUnitCode = minSaleUnitCode;
		this.consumeType = consumeType;
		this.minSaleUnitPrice = minSaleUnitPrice;
		this.saleName = saleName;
		this.year = year;
		this.proName = proName;
		this.proCode = proCode;
		this.picPath = picPath;
		this.isSoft = isSoft;
	}



	public String getPayDate() {
		return payDate;
	}



	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}



	public String getRecConfirmDate() {
		return recConfirmDate;
	}



	public void setRecConfirmDate(String recConfirmDate) {
		this.recConfirmDate = recConfirmDate;
	}



	public Integer getIsSoft() {
		return isSoft;
	}

	public void setIsSoft(Integer isSoft) {
		this.isSoft = isSoft;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(String orderMoney) {
		this.orderMoney = orderMoney;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getOrderState() {
		return orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	public Integer getOrderPayState() {
		return orderPayState;
	}

	public void setOrderPayState(Integer orderPayState) {
		this.orderPayState = orderPayState;
	}

	public Integer getRecConfirmState() {
		return recConfirmState;
	}

	public void setRecConfirmState(Integer recConfirmState) {
		this.recConfirmState = recConfirmState;
	}

	public String getDiscountMoney() {
		return discountMoney;
	}

	public void setDiscountMoney(String discountMoney) {
		this.discountMoney = discountMoney;
	}

	public String getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(String bankNumber) {
		this.bankNumber = bankNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDetailCode() {
		return detailCode;
	}

	public void setDetailCode(String detailCode) {
		this.detailCode = detailCode;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getProductSerialNo() {
		return productSerialNo;
	}

	public void setProductSerialNo(String productSerialNo) {
		this.productSerialNo = productSerialNo;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	public Integer getConsumeType() {
		return consumeType;
	}

	public void setConsumeType(Integer consumeType) {
		this.consumeType = consumeType;
	}
	
	public Double getMinSaleUnitPrice() {
		return minSaleUnitPrice;
	}

	public void setMinSaleUnitPrice(Double minSaleUnitPrice) {
		this.minSaleUnitPrice = minSaleUnitPrice;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	
}
