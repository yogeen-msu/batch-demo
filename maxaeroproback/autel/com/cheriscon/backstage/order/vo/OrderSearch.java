/**
*/ 
package com.cheriscon.backstage.order.vo;

import java.io.Serializable;

/**
 * @ClassName: OrderSearch
 * @Description: 订单列表查询
 * @author shaohu
 * @date 2013-1-25 上午10:18:49
 * 
 */
public class OrderSearch implements Serializable{
	
	private static final long serialVersionUID = 4549192402736479224L;
	/**
	 * 订单编号
	 */
	private String orderCode;
	/**
	 * 用户名
	 */
	private String customerName;
	
	/**
	 * 产品序列号
	 */
	private String productSerialNo;
	
	/**
	 * 下单开始时间
	 */
	private String startTime;
	/**
	 * 下单结束时间
	 */
	private String endTime;
	/**
	 * 订单状态  0已取消,1有效
	 */
	private Integer orderState;
	
	private Integer processState; //处理状态 1：已处理  0：未处理
	
	/**
	 * 订单支付状态  0：未支付 1：已支付
	 */
	private Integer orderPayState;
	
	/**
	 * 收款确认状态  0：未确认 1：已确认
	 */
	private Integer recConfirmState;

	public OrderSearch() {
		super();
	}

	public OrderSearch(String orderCode, String customerName,
			String productSerialNo, String startTime, String endTime,
			Integer orderState, Integer processState, Integer orderPayState,
			Integer recConfirmState) {
		super();
		this.orderCode = orderCode;
		this.customerName = customerName;
		this.productSerialNo = productSerialNo;
		this.startTime = startTime;
		this.endTime = endTime;
		this.orderState = orderState;
		this.processState = processState;
		this.orderPayState = orderPayState;
		this.recConfirmState = recConfirmState;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProductSerialNo() {
		return productSerialNo;
	}

	public void setProductSerialNo(String productSerialNo) {
		this.productSerialNo = productSerialNo;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getOrderState() {
		return orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	public Integer getOrderPayState() {
		return orderPayState;
	}

	public void setOrderPayState(Integer orderPayState) {
		this.orderPayState = orderPayState;
	}

	public Integer getRecConfirmState() {
		return recConfirmState;
	}

	public void setRecConfirmState(Integer recConfirmState) {
		this.recConfirmState = recConfirmState;
	}

	public Integer getProcessState() {
		return processState;
	}

	public void setProcessState(Integer processState) {
		this.processState = processState;
	}
	
}
