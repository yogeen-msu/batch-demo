/**
 */
package com.cheriscon.backstage.order.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.market.service.IMinSaleUnitSaleCfgDetailService;
import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.order.service.IOrderInfoBackService;
import com.cheriscon.backstage.order.vo.OrderInfoVo;
import com.cheriscon.backstage.order.vo.OrderPageData;
import com.cheriscon.backstage.order.vo.OrderSearch;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.constant.OrderEnum;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.MinSaleUnitSaleCfgDetail;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderMinSaleUnitDetail;
import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * @ClassName: OrderInfoBackServiceImpl
 * @Description: 订单信息业务实现类
 * @author shaohu
 * @date 2013-1-25 上午11:16:44
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class OrderInfoBackServiceImpl extends BaseSupport implements
		IOrderInfoBackService {

	private final static String TABLE_NAME = "DT_OrderInfo";
	
	@Resource
	private IProductSoftwareValidStatusService productSoftwareValidStatusService;
	
	@Resource
	private IProductInfoService productInfoService;
	
	@Resource
	private IMinSaleUnitSaleCfgDetailService cfgDetailService;
	
	@Resource
	private ISaleConfigService configService;

	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private ILanguageService languageService;
	@Resource
	private IEmailTemplateService emailTemplateService; 
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IOrderInfoService service;
	@Resource
	private ICustomerInfoService customerInfoService;
	/**
	
	/**
	 * <p>
	 * Title: PageOrderInfo
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param search
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page pageOrderInfo(OrderSearch search, int pageNo, int pageSize,
			String languageCode) throws Exception {
		StringBuffer orderCountSql = new StringBuffer();
		orderCountSql
				.append("select di.*,dt.autelID as customerName from DT_OrderInfo as di  left join DT_CustomerInfo as dt on di.userCode = dt.code where 1=1 ");
		StringBuffer pageSql = new StringBuffer();
		pageSql.append("select * from (")
				.append("select * from ( select di.*,dt.autelID as customerName from DT_OrderInfo as di  left join DT_CustomerInfo as dt on di.userCode = dt.code where 1=1 ");
		if (null != search) {
			if (StringUtils.isNotBlank(search.getOrderCode())) {
				orderCountSql.append(" and di.code like '%")
						.append(search.getOrderCode().trim()).append("%'");
				pageSql.append(" and di.code like '%")
						.append(search.getOrderCode().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(search.getCustomerName())) {
				orderCountSql.append(" and dt.name like '%")
						.append(search.getCustomerName().trim()).append("%'");
				pageSql.append(" and dt.name like '%")
						.append(search.getCustomerName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(search.getStartTime())) {
				orderCountSql.append(" and di.orderDate >= ")
						.append("'"+search.getStartTime().trim() + " 00:00:00")
						.append("'");
				pageSql.append(" and di.orderDate >= ")
						.append("'"+search.getStartTime().trim() + " 00:00:00")
						.append("'");
			}
			if (StringUtils.isNotBlank(search.getEndTime())) {
				orderCountSql.append(" and di.orderDate <= ")
						.append("'"+search.getEndTime().trim() + " 00:00:00")
						.append("'");
				pageSql.append(" and di.orderDate <= ")
						.append("'"+search.getEndTime().trim() + " 00:00:00")
						.append("'");
			}
			if (null != search.getOrderState()) {
				orderCountSql.append(" and di.orderState = ").append(
						search.getOrderState());
				pageSql.append(" and di.orderState = ").append(
						search.getOrderState());
			}
			if (null != search.getOrderPayState()) {
				orderCountSql.append(" and di.orderPayState = ").append(
						search.getOrderPayState());
				pageSql.append(" and di.orderPayState = ").append(
						search.getOrderPayState());
			}
			if (null != search.getRecConfirmState()) {
				orderCountSql.append(" and di.recConfirmState = ").append(
						search.getRecConfirmState());
				pageSql.append(" and di.recConfirmState = ").append(
						search.getRecConfirmState());
			}
			if (null != search.getProcessState()) {
				orderCountSql.append(" and di.processState = ").append(
						search.getProcessState());
				pageSql.append(" and di.processState = ").append(
						search.getProcessState());
			}
			
		}
		orderCountSql.append(" order by di.id desc");
		
		pageSql.append(" order by di.id desc limit ? , ? ) tb ");
		pageSql.append(") ta")
				.append(" left join ")
				.append("(select dom.code as detailCode,dom.orderCode,dom.productSerialNo,dom.consumeType,dom.minSaleUnitPrice,dom.year,dmm.name as saleName,dom.minSaleUnitCode from DT_OrderMinSaleUnitDetail dom left join DT_MinSaleUnitMemo dmm on dom.minSaleUnitCode = dmm.minSaleUnitCode and dmm.languageCode=?) tc on ta.code = tc.orderCode ");

		List<OrderInfoVo> orderInfoVos = this.daoSupport.queryForList(
				pageSql.toString(), OrderInfoVo.class, (pageNo - 1) * pageSize
						, pageSize, languageCode);

		List<OrderInfoVo> list = this.daoSupport.queryForList(
				orderCountSql.toString(), OrderInfoVo.class);

		Page page = new Page(pageNo, list.size(), pageSize,
				this.convertPageData(orderInfoVos));

		return page;
	}

	/**
	 * 
	 * @Title: convertPageData
	 * @Description: 将查询结果转换为可控显示的分页数据
	 * @param
	 * @return List<OrderInfo>
	 * @throws
	 */
	@SuppressWarnings({ "unchecked" })
	public List<OrderPageData> convertPageData(List<OrderInfoVo> orderInfoVos)
			throws Exception {

		List<OrderPageData> result = new ArrayList<OrderPageData>();

		Map orderMap = new HashMap();
		Map detailMap = new HashMap();

		List<OrderMinSaleUnitDetail> details = null;

		for (OrderInfoVo v : orderInfoVos) {
			String code = v.getCode();
			if (!orderMap.containsKey(code)) {
				OrderInfo info = new OrderInfo();
				BeanUtils.copyProperties(v, info);
				orderMap.put(code, info);

			}

			OrderMinSaleUnitDetail detail = new OrderMinSaleUnitDetail();
			BeanUtils.copyProperties(v, detail);
			//如果是销售配置
			if(StringUtils.startsWith(detail.getMinSaleUnitCode(), DBConstant.SALE_CONFIG_CODE)){
				SaleConfig saleConfig = configService.getSaleConfigByCode(detail.getMinSaleUnitCode().trim());
				detail.setSaleName(saleConfig.getName().trim());
			}
			if (detailMap.containsKey(code)) {
				details = (List<OrderMinSaleUnitDetail>) detailMap.get(code);
			} else {
				details = new ArrayList<OrderMinSaleUnitDetail>();
			}
			details.add(detail);
			detailMap.put(code, details);
		}
		orderMap = mapByKey(orderMap);
		
		Object[] unsort_key = orderMap.keySet().toArray();
		Arrays.sort(unsort_key);
		
		if(unsort_key.length > 0){
			int len = unsort_key.length;
			for (int i = len-1; i >= 0; i--) {
				OrderPageData data = new OrderPageData();
				data.setOrderInfo((OrderInfo) orderMap.get(unsort_key[i].toString()));
				data.setDetails((List<OrderMinSaleUnitDetail>) detailMap.get(unsort_key[i].toString()));
				result.add(data);
			}
		}else{
			return null;
		}
		
		
//		Set keys = orderMap.keySet();
//
//		for (Object key : keys) {
//			OrderPageData data = new OrderPageData();
//			data.setOrderInfo((OrderInfo) orderMap.get(key));
//			data.setDetails((List<OrderMinSaleUnitDetail>) detailMap.get(key));
//			result.add(data);
//		}

		return result;
	}

	/**
	 * <p>
	 * Title: orderConfirm
	 * </p>
	 * <p>
	 * Description: 订单确认
	 * </p>
	 * 
	 * @param orderCodes
	 * @throws Exception
	 */
	@Override
	public void orderConfirm(String orderCodes) throws Exception {
		String[] codes = orderCodes.split(",");
		StringBuffer code = new StringBuffer();
		for (String s : codes) {
			code.append("'").append(s.trim()).append("'").append(" ");
		}
		
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		StringBuffer sql = new StringBuffer();
		sql.append("update ").append(TABLE_NAME)
				.append(" set recConfirmState=1 , recConfirmDate =? ").append(" where code in (")
				.append(code.toString().trim().replaceAll(" ", ","))
				.append(")");
		this.daoSupport.execute(sql.toString(),date);
	}

	/**
	  * <p>Title: updateBankNumber</p>
	  * <p>Description: </p>
	  * @param orderInfo
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	public void updateBankNumber(OrderInfo orderInfo) throws Exception {
		this.daoSupport.update(TABLE_NAME, orderInfo, " code='"+orderInfo.getCode().trim()+"'");
	}

	@SuppressWarnings("unchecked")
	public List<OrderMinSaleUnitDetail> getOrderDetailByOrderCode(String code) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append("DT_OrderMinSaleUnitDetail").append(" where orderCode=?");
		return (List<OrderMinSaleUnitDetail>) this.daoSupport.queryForList(sql.toString(), OrderMinSaleUnitDetail.class, code);
	}
	
	/**
	  * <p>Title: processState</p>
	  * <p>Description: </p>
	  * @param proCode
	  * @param year
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void processState(String code)
			throws Exception {
		String[] codes = code.trim().split(",");
		
		Calendar calendar = Calendar.getInstance(); 
		calendar.setTime(new Date());
		calendar.add(Calendar.YEAR, 1);
		String validDate = DateUtil.toString(calendar.getTime(), "yyyy-MM-dd");
		
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		
		for (int i = 0; i < codes.length; i++) {
			if (StringUtils.isBlank(codes[i]))
				break;
			StringBuffer sql = new StringBuffer();
			
			sql.append("update DT_OrderInfo set processState = 1,recConfirmState=1,processDate=?,recConfirmDate=? where code = ?");
			List<OrderMinSaleUnitDetail> details = this.getOrderDetailByOrderCode(codes[i].trim());
			this.daoSupport.execute(sql.toString(),date,date,codes[i]);
			if(null != details && details.size() > 0){
				for(OrderMinSaleUnitDetail detail :details){
					if(StringUtils.isNotBlank(detail.getProductSerialNo()) && detail.getYear() != null){
//						续费更新产品有效期
						if(detail.getConsumeType() == 1){
							ProductInfo productInfo = productInfoService.getBySerialNo(detail.getProductSerialNo().trim());
							String tCode = detail.getMinSaleUnitCode().trim();
							if(StringUtils.startsWith(tCode, DBConstant.SALE_CONFIG_CODE)){
								List<MinSaleUnitSaleCfgDetail> cfgDetails = cfgDetailService.getMinSaleUnitSaleCfgListByCode(tCode);
								for(MinSaleUnitSaleCfgDetail cfgDetail : cfgDetails){
									this.productSoftwareValidStatusService.updateRenewal(productInfo.getCode().trim(),cfgDetail.getMinSaleUnitCode(), detail.getYear().toString());
								}
							}else{
								this.productSoftwareValidStatusService.updateRenewal(productInfo.getCode().trim(),tCode, detail.getYear().toString());
							}
//							购买更新产品有效期
						}else{
							
							ProductInfo productInfo = productInfoService.getBySerialNo(detail.getProductSerialNo().trim());
							
							String tCode = detail.getMinSaleUnitCode().trim();
							if(StringUtils.startsWith(tCode, DBConstant.SALE_CONFIG_CODE)){
								List<MinSaleUnitSaleCfgDetail> cfgDetails = cfgDetailService.getMinSaleUnitSaleCfgListByCode(tCode);
								for(MinSaleUnitSaleCfgDetail cfgDetail : cfgDetails){
									ProductSoftwareValidStatus validStatus = new ProductSoftwareValidStatus();
									validStatus.setProCode(productInfo.getCode());
									validStatus.setMinSaleUnitCode(cfgDetail.getMinSaleUnitCode());
									validStatus.setValidDate(validDate);
									this.productSoftwareValidStatusService.saveProductSoftwareValidStatus(validStatus);
								}
							}else{
								ProductSoftwareValidStatus validStatus = new ProductSoftwareValidStatus();
								validStatus.setProCode(productInfo.getCode());
								validStatus.setMinSaleUnitCode(detail.getMinSaleUnitCode().trim());
								validStatus.setValidDate(validDate);
								this.productSoftwareValidStatusService.saveProductSoftwareValidStatus(validStatus);
							}
							//saveProductSoftwareValidStatus
						}
					}
				}
			}
			
			//TODO 发送邮件
			OrderInfo info=service.queryOrderInfoByCode(codes[i].trim());
			if(info!=null){
				
			CustomerInfo customerInfo =customerInfoService.getCustomerByCode(info.getUserCode());
			Smtp smtp = smtpManager.getCurrentSmtp( );
			EmailModel emailModel = new EmailModel();
			emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
			emailModel.getData().put("password", smtp.getPassword());
			emailModel.getData().put("host", smtp.getHost());
			emailModel.getData().put("port", smtp.getPort());
			
			String userName="";
			if(customerInfo!=null && customerInfo.getName()!=null){
			  userName=customerInfo.getName();
			}
			String requestUrl =FreeMarkerPaser.getBundleValue("autelpro.url");
			String url=requestUrl+"/orderDetailInfo.html?orderCode="+info.getCode();
			emailModel.getData().put("autelId", customerInfo.getAutelId());
			emailModel.getData().put("userName", userName); //用户姓名
			emailModel.getData().put("orderCode", info.getCode()); //订单编号
			emailModel.getData().put("orderDate", info.getOrderDate()); //订单时间
			emailModel.getData().put("orderMoney", info.getOrderMoney()); //订单金额
			emailModel.getData().put("url", url); //支付链接
			
			
			
			Language language = languageService.getByCode(customerInfo.getLanguageCode());
			if(language != null)
			{
				emailModel.setTitle(FreeMarkerPaser.getBundleValue("cententman.messageman.order.result.mailtitle",
						language.getLanguageCode(),language.getCountryCode()));
			}
			else 
			{
				emailModel.setTitle(FreeMarkerPaser.getBundleValue("cententman.messageman.order.result.mailtitle"));
			}
			emailModel.getData().put("orderStatus", FreeMarkerPaser.getBundleValue("orderman.list.dealed",
					language.getLanguageCode(),language.getCountryCode())); //订单状态
			
			emailModel.setTo(customerInfo.getAutelId());
			EmailTemplate template = emailTemplateService.
					getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_ORDER_RESULT,customerInfo.getLanguageCode());
			
			if(template != null)
			{
				emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
			}
			
			emailProducer.send(emailModel);
			}
			
		}

	}
	
	
	/**
	 * 
	* @Title: countOrderByState
	* @Description: 根据状态查询各状态订单数据
	* @param    
	* @return Integer    
	* @throws
	 */
	public Integer countOrderByState(String stateFlag , Integer state) {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select COUNT(*) from DT_OrderInfo di where 1=1 ");
			if(StringUtils.equals(stateFlag, OrderEnum.ORDER_STATE.toString())){
				sql.append(" and di.orderState = ? ");
			}else if(StringUtils.equals(stateFlag, OrderEnum.ORDER_PAY_STATE.toString())){
				sql.append(" and di.orderPayState = ? ");
			}else if(StringUtils.equals(stateFlag, OrderEnum.REC_CONFIRM_STATE.toString())){
				sql.append(" and di.recConfirmState = ? ");
			}else if(StringUtils.equals(stateFlag, OrderEnum.PROCESS_STATE.toString())){
				sql.append(" and di.processState = ? ");
			}
			
			return this.daoSupport.queryForInt(sql.toString(), state);
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	} 

	/**
	 * 
	* @Title: mapByKey
	* @Description: map排序
	* @param    
	* @return Map<String,OrderInfo>    
	* @throws
	 */
	private static Map<String, OrderInfo> mapByKey(Map<String, OrderInfo> unsort_map) {
		Map<String, OrderInfo> result = new TreeMap<String, OrderInfo>();

		Object[] unsort_key = unsort_map.keySet().toArray();
		Arrays.sort(unsort_key);

		int len = unsort_key.length;
		
		for (int i = len-1; i >= 0; i--) {
			result.put(unsort_key[i].toString(), unsort_map.get(unsort_key[i]));
		}
		return result;
	}

	
//	public void exprotOrderExcel(OrderSearch search,String languageCode)throws Exception {
//		ExportExcel<OrderInfoVo> exportExcel = new ExportExcel<OrderInfoVo>();
//		String[] headers = { "id", "code", "userCode", "orderMoney", "orderDate","orderState","orderPayState","recConfirmState","discountMoney","bankNumber","customerName","processState","payDate","recConfirmDate","detailCode","orderCode","minSaleUnitCode","consumeType","minSaleUnitPrice","saleName" ,"year"};
//		
//		try {
//			List<OrderInfoVo> infoVos = this.getOrderInfoList(search, languageCode);
//			OutputStream out = new FileOutputStream("F://c.xls");
//			exportExcel.exportExcel( headers, infoVos, out);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
//	public List<OrderInfoVo> getOrderInfoList(OrderSearch search,String languageCode){
//		StringBuffer pageSql = new StringBuffer();
//		pageSql.append("select * from (")
//				.append("select * from ( select ROW_NUMBER() Over(order by di.id desc) as rowNum,di.*,dt.name as customerName from DT_OrderInfo as di  left join DT_CustomerInfo as dt on di.userCode = dt.code where 1=1 ");
//		if (null != search) {
//			if (StringUtils.isNotBlank(search.getOrderCode())) {
//				pageSql.append(" and di.code like '%")
//						.append(search.getOrderCode().trim()).append("%'");
//			}
//			if (StringUtils.isNotBlank(search.getCustomerName())) {
//				pageSql.append(" and dt.name like '%")
//						.append(search.getCustomerName().trim()).append("%'");
//			}
//			if (StringUtils.isNotBlank(search.getStartTime())) {
//				pageSql.append(" and di.orderDate >= ")
//						.append("'"+search.getStartTime().trim() + " 00:00:00")
//						.append("'");
//			}
//			if (StringUtils.isNotBlank(search.getEndTime())) {
//				pageSql.append(" and di.orderDate <= ")
//						.append("'"+search.getEndTime().trim() + " 00:00:00")
//						.append("'");
//			}
//			if (null != search.getOrderState()) {
//				pageSql.append(" and di.orderState = ").append(
//						search.getOrderState());
//			}
//			if (null != search.getOrderPayState()) {
//				pageSql.append(" and di.orderPayState = ").append(
//						search.getOrderPayState());
//			}
//			if (null != search.getRecConfirmState()) {
//				pageSql.append(" and di.recConfirmState = ").append(
//						search.getRecConfirmState());
//			}
//			if (null != search.getProcessState()) {
//				pageSql.append(" and di.processState = ").append(
//						search.getProcessState());
//			}
//			
//		}
//		pageSql.append(" ) tb where rowNum between ? AND ? ");
//		pageSql.append(") ta")
//				.append(" left join ")
//				.append("(select dom.code as detailCode,dom.orderCode,dom.productSerialNo,dom.consumeType,dom.minSaleUnitPrice,dom.year,dmm.name as saleName from DT_OrderMinSaleUnitDetail dom left join DT_MinSaleUnitMemo dmm on dom.minSaleUnitCode = dmm.minSaleUnitCode and dmm.languageCode=?) tc on ta.code = tc.orderCode ");
//
//		List<OrderInfoVo> orderInfoVos = this.daoSupport.queryForList(
//				pageSql.toString(), OrderInfoVo.class, 0, 100000, languageCode);
//		return orderInfoVos;
//	}
	public void payErrorLog(PayErrorLog errorLog){
		
		this.daoSupport.insert("DT_PayErrorLog", errorLog);	
		
	}
}
