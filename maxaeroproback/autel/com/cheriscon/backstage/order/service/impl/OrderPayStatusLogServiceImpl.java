package com.cheriscon.backstage.order.service.impl;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.order.service.IOrderPayStatusLogService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderPayStatusLog;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * 订单支付状态修改日志业务逻辑实现类
 * @author pengdongan
 * @version 创建时间：2013-5-30
 */
@SuppressWarnings("rawtypes")
@Service
public class OrderPayStatusLogServiceImpl extends BaseSupport implements IOrderPayStatusLogService
{


	@Resource
	private IOrderInfoService orderInfoService;
	
	/**
	 * 添加订单支付状态修改日志
	 * @param orderPayStatusLog
	 * 订单支付状态修改日志
	 * @throws Exception
	 */
	@Transactional
	public int insertOrderPayStatusLog(OrderPayStatusLog orderPayStatusLog) throws Exception {
		orderPayStatusLog.setCode(DBUtils.generateCode(DBConstant.ORD_PAYSTATUS_LOG_CODE));
		this.daoSupport.insert("DT_OrderPayStatusLog", orderPayStatusLog);
		return 0;
	}

	@Override
	public OrderPayStatusLog getOrderPayStatusLogByCode(String code) throws Exception {
		String sql = "select * from DT_OrderPayStatusLog where code = ?";
		
		return (OrderPayStatusLog) this.daoSupport.queryForObject(sql, OrderPayStatusLog.class, code);
	}

	@Override
	public OrderPayStatusLog getOrderPayStatusLogByOrderCode(String orderCode) throws Exception {
		String sql = "select * from DT_OrderPayStatusLog where orderCode = ?";
		
		return (OrderPayStatusLog) this.daoSupport.queryForObject(sql, OrderPayStatusLog.class, orderCode);
	}

	
	/**
	 * 添加订单支付状态修改日志详情
	 * @param orderPayStatusLog
	 * 订单支付状态修改日志
	 * @throws Exception
	 */
	@Transactional
	public int insertOrderPayStatusLogDetail(OrderPayStatusLog orderPayStatusLog) throws Exception {
		this.insertOrderPayStatusLog(orderPayStatusLog);
		
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setCode(orderPayStatusLog.getOrderCode());
		orderInfo.setOrderPayState(1);
		orderInfoService.updateOrderInfo(orderInfo);
		
		return 0;
	}



	/**
	  * 订单支付状态修改日志分页显示方法
	  * @param OrderPayStatusLog
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	public Page pageOrderPayStatusLogPage(OrderPayStatusLog orderPayStatusLog, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.autelId,c.name userName");
		sql.append(" from DT_OrderPayStatusLog a");
		sql.append(" left join DT_OrderInfo b on a.orderCode=b.code");
		sql.append(" left join DT_CustomerInfo c on b.userCode=c.code");
		sql.append(" where 1=1");
		
		if (null != orderPayStatusLog) {
			if (StringUtils.isNotBlank(orderPayStatusLog.getOrderCode())) {
				sql.append(" and a.orderCode like '%");
				sql.append(orderPayStatusLog.getOrderCode().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(orderPayStatusLog.getAutelId())) {
				sql.append(" and c.autelId like '%");
				sql.append(orderPayStatusLog.getAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(orderPayStatusLog.getBeginDate())) {
				sql.append(" and a.updateDate >= '");
				sql.append(orderPayStatusLog.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(orderPayStatusLog.getEndDate())) {
				sql.append(" and a.updateDate < cast('");
				sql.append(orderPayStatusLog.getEndDate().trim());
				sql.append("' as datetime)+1");
			}
		}

		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, OrderPayStatusLog.class);
		return page;
	}

}
