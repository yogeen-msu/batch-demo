package com.cheriscon.backstage.order.service.impl;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ITestPackLanguagePackService;
import com.cheriscon.backstage.order.service.IOrderInfoLogService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderInfoLog;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * 订单修改日志业务逻辑实现类
 * @author pengdongan
 * @version 创建时间：2013-4-28
 */
@SuppressWarnings("rawtypes")
@Service
public class OrderInfoLogServiceImpl extends BaseSupport implements IOrderInfoLogService
{


	@Resource
	private IOrderInfoService orderInfoService;
	
	/**
	 * 添加订单修改日志
	 * @param orderInfoLog
	 * 订单修改日志
	 * @throws Exception
	 */
	@Transactional
	public int insertOrderInfoLog(OrderInfoLog orderInfoLog) throws Exception {
		orderInfoLog.setCode(DBUtils.generateCode(DBConstant.ORD_LOG_CODE));
		this.daoSupport.insert("DT_OrderInfoLog", orderInfoLog);
		return 0;
	}
	
	/**
	 * 更新订单修改日志
	 * @param orderInfoLog
	 * 订单修改日志
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public int updateOrderInfoLog(OrderInfoLog orderInfoLog) throws Exception {
		this.daoSupport.update("DT_OrderInfoLog", orderInfoLog, "code='"+orderInfoLog.getCode()+"'");
		return 0;
	}

	@Override
	public OrderInfoLog getOrderInfoLogByCode(String code) throws Exception {
		String sql = "select * from DT_OrderInfoLog where code = ?";
		
		return (OrderInfoLog) this.daoSupport.queryForObject(sql, OrderInfoLog.class, code);
	}

	@Override
	public OrderInfoLog getOrderInfoLogByOrderCode(String orderCode) throws Exception {
		String sql = "select * from DT_OrderInfoLog where orderCode = ?";
		
		return (OrderInfoLog) this.daoSupport.queryForObject(sql, OrderInfoLog.class, orderCode);
	}

	@Transactional
	public int delOrderInfoLog(String code) throws Exception {
		String sql = "delete from DT_OrderInfoLog where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delOrderInfoLogs(String codes) throws Exception {
		String sql = "delete from DT_OrderInfoLog where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}

	
	/**
	 * 添加订单修改日志详情
	 * @param orderInfoLog
	 * 订单修改日志
	 * @throws Exception
	 */
	@Transactional
	public int insertOrderInfoLogDetail(OrderInfoLog orderInfoLog) throws Exception {
		OrderInfoLog orderInfoLog2 = this.getOrderInfoLogByOrderCode(orderInfoLog.getOrderCode());
		if(orderInfoLog2 != null)
		{
			orderInfoLog.setCode(orderInfoLog2.getCode());
			orderInfoLog.setOrderMoney(orderInfoLog2.getOrderMoney());
			this.updateOrderInfoLog(orderInfoLog);
		}
		else
		{
			this.insertOrderInfoLog(orderInfoLog);
		}
		
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setCode(orderInfoLog.getOrderCode());
		orderInfo.setOrderMoney(orderInfoLog.getUpdateMoney());
		orderInfoService.updateOrderInfo(orderInfo);
		
		return 0;
	}
	
	/**
	 * 更新订单修改日志详情
	 * @param orderInfoLog
	 * 订单修改日志
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public int updateOrderInfoLogDetail(OrderInfoLog orderInfoLog) throws Exception {
		
		this.updateOrderInfoLog(orderInfoLog);
		
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setCode(orderInfoLog.getOrderCode());
		orderInfo.setOrderMoney(orderInfoLog.getUpdateMoney());
		orderInfoService.updateOrderInfo(orderInfo);
		
		return 0;
	}

	@Transactional
	public int delOrderInfoLogDetail(String code) throws Exception {
		OrderInfoLog orderInfoLog = this.getOrderInfoLogByCode(code);
		if(orderInfoLog != null)
		{
			OrderInfo orderInfo = new OrderInfo();
			orderInfo.setCode(orderInfoLog.getOrderCode());
			orderInfo.setOrderMoney(orderInfoLog.getOrderMoney());
			orderInfoService.updateOrderInfo(orderInfo);
	
			this.delOrderInfoLog(code);
		}
		
		return 0;
	}



	/**
	  * 订单修改日志分页显示方法
	  * @param OrderInfoLog
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	public Page pageOrderInfoLogPage(OrderInfoLog orderInfoLog, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.autelId,c.name userName");
		sql.append(" from DT_OrderInfoLog a");
		sql.append(" left join DT_OrderInfo b on a.orderCode=b.code");
		sql.append(" left join DT_CustomerInfo c on b.userCode=c.code");
		sql.append(" where 1=1");
		
		if (null != orderInfoLog) {
			if (StringUtils.isNotBlank(orderInfoLog.getOrderCode())) {
				sql.append(" and a.orderCode like '%");
				sql.append(orderInfoLog.getOrderCode().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(orderInfoLog.getAutelId())) {
				sql.append(" and c.autelId like '%");
				sql.append(orderInfoLog.getAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(orderInfoLog.getBeginDate())) {
				sql.append(" and a.updateDate >= '");
				sql.append(orderInfoLog.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(orderInfoLog.getEndDate())) {
				sql.append(" and a.updateDate < cast('");
				sql.append(orderInfoLog.getEndDate().trim());
				sql.append("' as datetime)+1");
			}
		}

		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, OrderInfoLog.class);
		return page;
	}

}
