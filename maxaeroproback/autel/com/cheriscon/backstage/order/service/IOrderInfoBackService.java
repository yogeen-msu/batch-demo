/**
*/ 
package com.cheriscon.backstage.order.service;

import com.cheriscon.backstage.order.vo.OrderSearch;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: IOrderInfoService
 * @Description: 订单信息业务接口类
 * @author shaohu
 * @date 2013-1-25 上午11:15:37
 * 
 */
public interface IOrderInfoBackService {
	
	/**
	 * 
	* @Title: PageOrderInfo
	* @Description: 分页查询订单数据
	* @param    
	* @return Page    
	* @throws
	 */
	public Page pageOrderInfo(OrderSearch search , int pageNo,int pageSize,String languageCode) throws Exception;
	
	/**
	 * 
	* @Title: orderConfirm
	* @Description: 订单确认
	* @param    
	* @return void    
	* @throws
	 */
	public void orderConfirm(String orderCodes) throws Exception;
	
	/**
	 *
	* @Title: updateBankNumber
	* @Description:  修改流水号
	* @param    
	* @return void    
	* @throws
	 */
	public void updateBankNumber(OrderInfo orderInfo) throws Exception;
	
	/**
	 * 
	* @Title: processState
	* @Description: 更新产品有效期
	* @param    
	* @return void    
	* @throws
	 */
	public void processState(String code) throws Exception;
	
	
	/**
	 * 
	* @Title: countOrderByState
	* @Description: 根据订单查询各种订单的数量
	* @param    
	* @return Integer    
	* @throws
	 */
	public Integer countOrderByState(String stateFlag , Integer state);
	
	
	/**
	 * 
	* @Title: payErrorLog
	* @Description: 支付失败记录日志
	* @param    
	* @return void    
	* @throws
	 */
	public void payErrorLog(PayErrorLog errorLog);
	
}
