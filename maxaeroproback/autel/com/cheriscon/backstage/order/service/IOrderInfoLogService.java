package com.cheriscon.backstage.order.service;

import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.OrderInfoLog;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.framework.database.Page;

/**
 * 订单修改日志业务逻辑实现接口类
 * @author pengdongan
 * @version 创建时间：2013-4-28
 */
public interface IOrderInfoLogService 
{	
	/**
	 * 添加订单修改日志对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertOrderInfoLog(OrderInfoLog orderInfoLog) throws Exception;
	
	/**
	 * 更新订单修改日志对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateOrderInfoLog(OrderInfoLog orderInfoLog) throws Exception;
	
	/**
	 * 根据编码查询订单修改日志对象
	 * @param code	订单修改日志code
	 * @return
	 * @throws Exception
	 */
	public OrderInfoLog getOrderInfoLogByCode(String code) throws Exception;
	
	/**
	 * 根据订单编码查询订单修改日志对象
	 * @param code	订单修改日志code
	 * @return
	 * @throws Exception
	 */	
	public OrderInfoLog getOrderInfoLogByOrderCode(String orderCode) throws Exception;
	
	/**
	 * 根据编码删除订单修改日志对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delOrderInfoLog(String code) throws Exception;
	
	/**
	 * 根据删除多个订单修改日志对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delOrderInfoLogs(String codes) throws Exception;

	
	/**
	 * 添加订单修改日志详情
	 * @param orderInfoLog
	 * 订单修改日志
	 * @throws Exception
	 */
	public int insertOrderInfoLogDetail(OrderInfoLog orderInfoLog) throws Exception;
	
	/**
	 * 更新订单修改日志详情
	 * @param orderInfoLog
	 * 订单修改日志
	 * @throws Exception
	 */
	public int updateOrderInfoLogDetail(OrderInfoLog orderInfoLog) throws Exception;

	public int delOrderInfoLogDetail(String code) throws Exception;

	/**
	  * 订单修改日志分页显示方法
	  * @param OrderInfoLog
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageOrderInfoLogPage(OrderInfoLog orderInfoLog, int pageNo,
			int pageSize) throws Exception;

}
