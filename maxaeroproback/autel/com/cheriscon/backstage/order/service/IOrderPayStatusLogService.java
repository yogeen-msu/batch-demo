package com.cheriscon.backstage.order.service;


import com.cheriscon.common.model.OrderPayStatusLog;
import com.cheriscon.framework.database.Page;

/**
 * 订单支付状态修改日志业务逻辑实现接口类
 * @author pengdongan
 * @version 创建时间：2013-5-30
 */
public interface IOrderPayStatusLogService 
{	
	/**
	 * 添加订单支付状态修改日志对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertOrderPayStatusLog(OrderPayStatusLog orderPayStatusLog) throws Exception;
	
	/**
	 * 根据编码查询订单支付状态修改日志对象
	 * @param code	订单支付状态修改日志code
	 * @return
	 * @throws Exception
	 */
	public OrderPayStatusLog getOrderPayStatusLogByCode(String code) throws Exception;
	
	/**
	 * 根据订单编码查询订单支付状态修改日志对象
	 * @param code	订单支付状态修改日志code
	 * @return
	 * @throws Exception
	 */	
	public OrderPayStatusLog getOrderPayStatusLogByOrderCode(String orderCode) throws Exception;

	
	/**
	 * 添加订单支付状态修改日志详情
	 * @param orderPayStatusLog
	 * 订单支付状态修改日志
	 * @throws Exception
	 */
	public int insertOrderPayStatusLogDetail(OrderPayStatusLog orderPayStatusLog) throws Exception;

	/**
	  * 订单支付状态修改日志分页显示方法
	  * @param OrderPayStatusLog
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageOrderPayStatusLogPage(OrderPayStatusLog orderPayStatusLog, int pageNo,
			int pageSize) throws Exception;

}
