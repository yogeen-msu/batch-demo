package com.cheriscon.backstage.member.service;

import java.io.File;
import java.util.List;

import com.cheriscon.backstage.member.vo.SealerInfoVo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.framework.database.Page;

/**
 * @remark 经销商信息service接口
 * @author pengdongan
 * @date 2013-01-14
 * 
 */

public interface ISealerInfoService {
	
	/**
	 * 
	 * @param sealerInfoVo	经销商信息业务对象
	 * @return	
	 * @throws Exception
	 */
	public List<SealerInfoVo> listSealer(SealerInfoVo sealerInfoVo) throws Exception;
	


	
	/**
	 * 保存excel中经销商信息
	 * @throws Exception
	 * @author pengdongan
	 */
	public int saveCreateSealer(File sealerFile) throws Exception;

	
	/**
	 * 添加经销商信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertSealer(SealerInfo sealerInfo) throws Exception;
	
	
	/**
	 * 添加经销商信息权限信息
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public void insertSealerAuth(String authCode,String sealerCode) throws Exception;
	
	/**
	 * 添加经销商信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertSealer2(SealerInfo sealerInfo) throws Exception;
	
	/**
	 * 更新经销商信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateSealer(SealerInfo sealerInfo) throws Exception;
	
	/**
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public void updateSealer(String code,String lastLoginTime) throws Exception;
	
	/**
	 * 根据编码查询经销商信息对象
	 * @param code	经销商信息code
	 * @return
	 * @throws Exception
	 */
	public SealerInfo getSealerByCode(String code) throws Exception;
	
	/**
	 * 根据autelId查询经销商信息对象
	 * @param autelId	经销商信息autelId
	 * @return
	 * @throws Exception
	 */
	public SealerInfo getSealerByAutelId(String autelId) throws Exception;
	
	/**
	 * 根据编码删除经销商信息对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delSealer(String code) throws Exception;
	
	/**
	 * 根据删除多个经销商信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delSealers(String codes) throws Exception;
	
	/**
	 * 根据经销商账号、密码查询经销商信息对象
	 * @param autelId
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public SealerInfo getSealerInfoByAutelIdAndPassword(String autelId,String password)
		throws Exception;

	/**
	  * 经销商信息分页显示方法
	  * @param SealerInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageSealerInfoVoPage(SealerInfoVo sealerInfoVo, int pageNo,
			int pageSize) throws Exception;
	
	
	/**
	  * 经销商信息分页显示方法2
	  * @param SealerInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageSealerInfoVoPage2(SealerInfoVo sealerInfoVo, int pageNo,
			int pageSize) throws Exception;
	
	/**
	 * 根据区域code、国家名称查看经销商信息列表
	 * @param areaCode
	 * @param country
	 * @return
	 * @throws Exception
	 */
	public Page querySealerInfoPage(String areaCode , String country ,int pageNo,int pageSize) 
			throws Exception;

	
	
	public List<SealerInfo> querySealerInfo() throws Exception;
}
