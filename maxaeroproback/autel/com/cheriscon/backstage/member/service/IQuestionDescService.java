package com.cheriscon.backstage.member.service;



import java.util.List;

import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.QuestionDesc;
import com.cheriscon.framework.database.Page;

/**
 * 问题描述业务逻辑实现接口类
 * @author pengdongan
 * @date 2013-01-24
 */
public interface IQuestionDescService 
{	
	/**
	 * 查询所有问题描述对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<QuestionDesc> listAll() throws Exception;
	/**
	 * 按语言查询所有问题描述对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<QuestionDesc> listQuestionDescByLanguageCode(String languageCode) throws Exception;

	/**
	 * 根据客诉类型查询问题描述对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<QuestionDesc> queryByQuestionCode(String questionCode) throws Exception;
	
	/**
	 * 添加问题描述对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertQuestionDesc(QuestionDesc questionDesc) throws Exception;
	
	/**
	 * 更新问题描述对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateQuestionDesc(QuestionDesc questionDesc) throws Exception;
	
	public void updateQuestionDescs(
			List<QuestionDesc> questionDescs, List<QuestionDesc> delQuestionDescs)
			throws Exception;
	
	/**
	 * 根据编码查询问题描述对象
	 * @param code	问题描述code
	 * @return
	 * @throws Exception
	 */
	public QuestionDesc getQuestionDescByCode(String code) throws Exception;
	
	/**
	 * 
	 * @param questionCode
	 * @param languageCode
	 * @return
	 * @throws Exception
	 */
	public QuestionDesc getQuestionDesc(String questionCode,String languageCode)
			throws Exception;
	
	
	/**
	 * 根据ID删除问题描述对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int delQuestionDesc(int id) throws Exception;
	
	/**
	 * 根据类型删除问题描述对象
	 * @param questionCode
	 * @return
	 * @throws Exception
	 */
	public int delQuestionDescByQuestionCode(String questionCode) throws Exception;

	/**
	  * 问题描述分页显示方法
	  * @param QuestionDesc
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageQuestionDescPage(QuestionDesc questionDesc, int pageNo,
			int pageSize) throws Exception;

	/**
	  * <p>Title: getAvailableLanguage</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @return
	  * @throws Exception
	  */
	public List<Language> getAvailableLanguage(String questionCode)
			throws Exception;
	

}
