package com.cheriscon.backstage.member.service;



import java.util.List;

import com.cheriscon.common.model.CustomerComplaintType;
import com.cheriscon.framework.database.Page;

/**
 * 客诉类型业务逻辑实现接口类
 * @author pengdongan
 * @date 2013-01-24
 */
public interface ICustomerComplaintTypeService 
{	
	/**
	 * 查询所有客诉类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<CustomerComplaintType> listAll() throws Exception;
	
	/**
	 * 按语言查询所有客诉类型对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	public List<CustomerComplaintType> listCustomerComplaintTypeByLanguageCode(String languageCode) throws Exception;
	
	/**
	 * 添加客诉类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertCustomerComplaintType(CustomerComplaintType customerComplaintType) throws Exception;
	
	/**
	 * 更新客诉类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateCustomerComplaintType(CustomerComplaintType customerComplaintType) throws Exception;
	
	/**
	 * 根据编码查询客诉类型对象
	 * @param code	客诉类型code
	 * @return
	 * @throws Exception
	 */
	public CustomerComplaintType getCustomerComplaintTypeByCode(String code) throws Exception;
	
	/**
	 * 根据编码删除客诉类型对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delCustomerComplaintType(String code) throws Exception;
	
	/**
	 * 根据编码删除客诉类型及其名称
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delCustomerComplaintTypeDetail(String code) throws Exception;
	
	/**
	 * 根据删除多个客诉类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delCustomerComplaintTypes(String codes) throws Exception;

	/**
	  * 客诉类型分页显示方法
	  * @param CustomerComplaintType
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerComplaintTypePage(CustomerComplaintType customerComplaintType, int pageNo,
			int pageSize) throws Exception;
	

	/**
	 * 根据编码查询客诉类型对象
	 * @param code	客诉类型code
	 * @return
	 * @throws Exception
	 */
	public CustomerComplaintType getComplaintTypeByLanguage(String language,String code) throws Exception;
	
	
}
