package com.cheriscon.backstage.member.service;

import java.io.File;
import java.util.List;

import com.cheriscon.backstage.member.vo.SealerInfoVo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.database.Page;

/**
 * @remark 查询客诉转换service接口
 * @author pengdongan
 * @date 2013-07-08
 * 
 */

public interface IQueryTranslateUser {
	
	List<AdminUser> listByRoleId2(int roleid);
}
