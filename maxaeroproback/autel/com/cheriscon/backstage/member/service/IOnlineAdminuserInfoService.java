package com.cheriscon.backstage.member.service;



import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.common.model.OnlineAdminuserInfo;
import com.cheriscon.framework.database.Page;

/**
 * 在线客服权限接口类
 * @author pengdongan
 * @date 2013-06-05
 */
public interface IOnlineAdminuserInfoService
{

	/**
	  * 在线客服权限分页显示方法
	  * @param OnlineAdminuserInfo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageOnlineAdminuserInfoPage(OnlineAdminuserInfo onlineAdminuserInfo, int pageNo,
			int pageSize) throws Exception;
	
	/**
	 * 分配客服
	 * @param code,acceptor
	 * @throws Exception
	 */
	public int allotAcceptor(String code,String acceptor) throws Exception;

	/**
	 * 清除相关记录
	 */
	public int delOnlineAdminuserInfoByOnlineTypeCode(String code) throws Exception;

	
	/**
	 * 添加信息
	 * @param onlineAdminuserInfo
	 * 经销商信息
	 * @throws Exception
	 */
	public int insertOnlineAdminuserInfo(OnlineAdminuserInfo onlineAdminuserInfo) throws Exception;
	
	/**
	 * 更新信息
	 * @param onlineAdminuserInfo
	 * 经销商信息
	 * @throws Exception
	 */
	public int updateOnlineAdminuserInfo(OnlineAdminuserInfo onlineAdminuserInfo) throws Exception;
	
	
	public OnlineAdminuserInfo getOnlineAdminuserInfoByonlineTypeCode(String onlineTypeCode) throws Exception;
	

}
