package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.backstage.member.vo.SdkAppVO;
import com.cheriscon.framework.database.Page;

/**
 * sdk app逻辑处理接口
 * 
 * @author autel
 * 
 */
public interface ISdkAppService {

	/**
	 * 查詢sdk app集合(未分页)
	 * 
	 * @param sdkAppVO
	 * @return Page
	 */
	public List<SdkAppVO> querySdkAppList(SdkAppVO sdkAppVO);

	/**
	 * 查詢sdk app集合
	 * 
	 * @param sdkAppVO
	 * @param pageNo
	 * @param pageSize
	 * @return Page
	 */
	public Page querySdkAppList(SdkAppVO sdkAppVO, int pageNo, int pageSize);

	/**
	 * 查詢sdk app详情
	 * 
	 * @return
	 */
	public SdkAppVO getSdkAppDetails(SdkAppVO sdkAppVO);

	/**
	 * 添加sdk app
	 * 
	 * @param sdkAppVO
	 * @return boolean
	 */
	public boolean addSdkAppList(SdkAppVO sdkAppVO);

	/**
	 * 修改sdk app
	 * 
	 * @param sdkAppVO
	 * @return boolean
	 */
	public boolean updateSdkAppList(SdkAppVO sdkAppVO);

	/**
	 * 删除sdk app
	 * 
	 * @param sdkAppVO
	 * @return boolean
	 */
	public boolean deleteSdkAppList(SdkAppVO sdkAppVO);

	/**
	 * 查詢查询用户sdk app协议详情
	 * 
	 * @param sdkAppVO
	 * @return sdkAppVO
	 */
	public SdkAppVO getCustomerAgreementDetails(SdkAppVO sdkAppVO);

	/**
	 * 用户同意sdk app协议详情
	 * 
	 * @param sdkAppVO
	 * @return boolean
	 */
	public boolean saveCustomerAgreementDetails(SdkAppVO sdkAppVO);

}
