package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.backstage.member.vo.SealerTypeVo;
import com.cheriscon.common.model.SealerType;
import com.cheriscon.framework.database.Page;

/**
 * @remark 经销商类型service接口
 * @author pengdongan
 * @date 2013-01-14
 * 
 */

public interface ISealerTypeService {

	
	/**
	 * 
	 * @param sealerType	经销商类型对象
	 * @return	
	 * @throws Exception
	 */
	public List<SealerType> querySealerType() throws Exception;
	
	/**
	 * 
	 * @param sealerTypeVo	经销商类型业务对象
	 * @return	
	 * @throws Exception
	 */
	public List<SealerTypeVo> listSealerType(SealerTypeVo sealerTypeVo) throws Exception;
	

	

	
	/**
	 * 添加经销商类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertSealerType(SealerType sealerType) throws Exception;
	
	/**
	 * 更新经销商类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateSealerType(SealerType sealerType) throws Exception;
	
	/**
	 * 根据编码查询经销商类型对象
	 * @param code	经销商类型code
	 * @return
	 * @throws Exception
	 */
	public SealerType getSealerTypeByCode(String code) throws Exception;
	
	/**
	 * 根据名称查询经销商类型对象
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public SealerType getSealerTypeByName(String name) throws Exception;
	
	/**
	 * 根据编码删除经销商类型对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delSealerType(String code) throws Exception;
	
	/**
	 * 根据删除多个经销商类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delSealerTypes(String codes) throws Exception;

	/**
	  * 经销商类型分页显示方法
	  * @param SealerTypeVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageSealerTypeVoPage(SealerTypeVo sealerTypeVo, int pageNo,
			int pageSize) throws Exception;
	
}
