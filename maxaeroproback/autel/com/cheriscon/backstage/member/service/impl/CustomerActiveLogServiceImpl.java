package com.cheriscon.backstage.member.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ICustomerActiveLogService;
import com.cheriscon.common.model.CustomerActiveLog;
import com.cheriscon.cop.sdk.database.BaseSupport;
@Service
public class CustomerActiveLogServiceImpl extends BaseSupport<CustomerActiveLog> implements ICustomerActiveLogService {

	@Override
	public void saveLog(CustomerActiveLog log) throws Exception {
		
		this.daoSupport.insert("DT_CustomerActiveLog", log);
	}

}
