package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ICustomerComplaintSourceService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerComplaintSource;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客诉来源service实现
 * @author pengdongan
 * @date 2013-03-31
 * 
 */
@Service
public class CustomerComplaintSourceServiceImpl extends BaseSupport implements ICustomerComplaintSourceService {


	
	
	public List<CustomerComplaintSource> queryCustomerComplaintSource()
	throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.* from DT_CustomerComplaintSource a");
		return (List<CustomerComplaintSource>) this.daoSupport.queryForList(sql.toString(),CustomerComplaintSource.class);
	}


	
	
	public List<CustomerComplaintSource> listCustomerComplaintSource(CustomerComplaintSource customerComplaintSource)
	throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.* from DT_CustomerComplaintSource a");
		return (List<CustomerComplaintSource>) this.daoSupport.queryForList(sql.toString(),CustomerComplaintSource.class);
	}
	
	/**
	 * 更新客诉来源
	 * @param customerComplaintSource
	 * 客诉来源
	 * @throws Exception
	 */
	@Transactional
	public int insertCustomerComplaintSource(CustomerComplaintSource customerComplaintSource) throws Exception {
		customerComplaintSource.setCode(DBUtils.generateCode(DBConstant.CUC_SOURCE_CODE));
		this.daoSupport.insert("DT_CustomerComplaintSource", customerComplaintSource);
		return 0;
	}
	
	/**
	 * 添加客诉来源
	 * @param customerComplaintSource
	 * 客诉来源
	 * @throws Exception
	 */
	@Transactional
	public int updateCustomerComplaintSource(CustomerComplaintSource customerComplaintSource) throws Exception {
		this.daoSupport.update("DT_CustomerComplaintSource", customerComplaintSource, "code='"+customerComplaintSource.getCode()+"'");
		return 0;
	}

	public CustomerComplaintSource getCustomerComplaintSourceByCode(String code) throws Exception {
		String sql = "select * from DT_CustomerComplaintSource where code = ?";
		
		return (CustomerComplaintSource) this.daoSupport.queryForObject(sql, CustomerComplaintSource.class, code);
	}

	public CustomerComplaintSource getCustomerComplaintSourceByName(String name) throws Exception {
		String sql = "select * from DT_CustomerComplaintSource where name = ? limit 0,1";
		
		return (CustomerComplaintSource) this.daoSupport.queryForObject(sql, CustomerComplaintSource.class, name);
	}

	@Transactional
	public int delCustomerComplaintSource(String code) throws Exception {
		String sql = "delete from DT_CustomerComplaintSource where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delCustomerComplaintSources(String codes) throws Exception {
		String sql = "delete from DT_CustomerComplaintSource where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}



	/**
	  * 客诉来源分页显示方法
	  * @param CustomerComplaintSource
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerComplaintSourcePage(CustomerComplaintSource customerComplaintSource, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		
		sql.append("select a.* from DT_CustomerComplaintSource a");
		sql.append(" order by a.id desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, CustomerComplaintSource.class);
		return page;
	}

}
