package com.cheriscon.backstage.member.service.impl;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.SealerInfoVo;
import com.cheriscon.backstage.system.service.ICountryCodeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IStateProvinceService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CountryCode;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.StateProvince;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.DesModule;
import com.cheriscon.common.utils.ExcelUtils;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;

/**
 * @remark 经销商信息service实现
 * @author pengdongan
 * @date 2013-01-07
 * 
 */
@Service
public class SealerInfoServiceImpl extends BaseSupport implements ISealerInfoService {
	@Resource
	private ILanguageService languageService;
	@Resource
	private ICountryCodeService countryCodeService;
	@Resource
	private IStateProvinceService stateProvinceService;
	
	@Transactional
	public List<SealerInfoVo> listSealer(SealerInfoVo sealerInfoVo)
	throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select distinct a.id,a.autelId,a.code,a.firstName,a.middleName,a.lastName,a.name,a.email,a.secondEmail," +
				"a.userPwd,a.daytimePhoneCC,a.daytimePhoneAC,a.daytimePhone,a.daytimePhoneExtCode,a.regTime,f.name areaCfgName,a.name sealerName");
		sql.append(" from DT_SealerInfo a");
		sql.append(" left join DT_SaleContract d on a.code=d.sealercode");
		sql.append(" left join DT_AreaConfig f on d.areaCfgCode=f.code");
		sql.append(" where 1=1");
		
		if (null != sealerInfoVo) {
			if (StringUtils.isNotBlank(sealerInfoVo.getSealerName())) {
				sql.append(" and a.name like '%");
				sql.append(sealerInfoVo.getSealerName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(sealerInfoVo.getAreaCfgName())) {
				sql.append(" and f.name like '%");
				sql.append(sealerInfoVo.getAreaCfgName().trim());
				sql.append("%'");
			}
		}
		return (List<SealerInfoVo>) this.daoSupport.queryForList(sql.toString(),SealerInfoVo.class);
	}
	
	/**
	 * 保存excel中经销商信息
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public int saveCreateSealer(File sealerFile) throws Exception {

		if (sealerFile != null) {
			SealerInfo sealerInfo = new SealerInfo();
//			sealerInfo.setIsUse(1);
//			sealerInfo.setIsActive(1);
			List<Language> languages = languageService.queryLanguage();
			List<CountryCode> countryCodes = countryCodeService.getCountryCodeList();
			List<StateProvince> stateProvinces = stateProvinceService.getStateProvinceList();
			
			ExcelUtils excelUtils = new ExcelUtils(sealerFile, 0);
			List<List<String>> resultList = excelUtils.readSimpleExcel(0,13);
			
			String emailCheck = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(emailCheck);
			
			for (List<String> list : resultList) {
				
				if (StringUtils.isBlank(list.get(0))) return 1;
				if (StringUtils.isBlank(list.get(1))) return 2;
				
				Language language = (Language) getListTClass(languages, "name", list.get(2));
				if (null == language) return 3;
				list.set(2, language.getCode());
				
				
				if (StringUtils.isBlank(list.get(3)) || StringUtils.isBlank(list.get(4))) return 4;
				if (!regex.matcher(list.get(5)).matches()) return 5;
				if (StringUtils.isNotBlank(list.get(6)) && !regex.matcher(list.get(6)).matches()) return 6;
				
				CountryCode countryCode = (CountryCode) getListTClass(countryCodes, "country", list.get(8));
				if (null == countryCode) return 7;
				list.set(8, countryCode.getCode());
				
				StateProvince stateProvince = (StateProvince) getListTClass(stateProvinces, "state", list.get(7));
				if (!"none".equals(countryCode.getState()) && null == stateProvince) return 8;
				list.set(7, stateProvince.getCode());
				
				if (StringUtils.isBlank(list.get(9))) return 9;
				if (StringUtils.isBlank(list.get(10))) return 10;
				if (StringUtils.isBlank(list.get(11))) return 11;
				if (StringUtils.isBlank(list.get(12))) return 12;
				if (StringUtils.isBlank(list.get(13))) return 13;
			}
			
			
			
			for (List<String> list : resultList) {
				sealerInfo.setRegTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
				sealerInfo.setIsAllowSendEmail(1);
				
				sealerInfo.setAutelId(list.get(0));
				sealerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(list.get(1),null));
				sealerInfo.setLanguageCode(list.get(2));
				sealerInfo.setFirstName(list.get(3));
				sealerInfo.setLastName(list.get(4));
				sealerInfo.setName(list.get(3)+list.get(4));
				sealerInfo.setEmail(list.get(5));
				sealerInfo.setSecondEmail(list.get(6));
				sealerInfo.setStateCode(list.get(7));
				sealerInfo.setCountryCode(list.get(8));
				sealerInfo.setCompany(list.get(9));
				sealerInfo.setAddress(list.get(10));
				sealerInfo.setCity(list.get(11));
				sealerInfo.setZipCode(list.get(12));
				sealerInfo.setTelephone(list.get(13));
				
				if(this.getSealerByAutelId(sealerInfo.getAutelId()) == null)
					this.insertSealer(sealerInfo);
			}
		}
		
		return 0;
	}
	
	/**
	 * 根据list中的一个键值获取对象
	 * @param list
	 * @param field
	 * @param eqField
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Object getListTClass(List list,String field,String eqField) {
		if (StringUtils.isEmpty(eqField))
			return null;
		for (int i = 0; i < list.size(); i++) {
			JSONObject c = JSONObject.fromObject(list.get(i));
			if(c.containsKey(field) && c.get(field).toString().toUpperCase().equals(eqField.toUpperCase())){
				return list.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 更新经销商信息
	 * @param sealerInfo
	 * 经销商信息
	 * @throws Exception
	 */
	@Transactional
	public int insertSealer(SealerInfo sealerInfo) throws Exception {
		sealerInfo.setCode(DBUtils.generateCode(DBConstant.SEALERINFO_CODE));
		sealerInfo.setStatus(1);
		this.daoSupport.insert("DT_SealerInfo", sealerInfo);
		return 0;
	}
	
	public void insertSealerAuth(String authCode,String sealerCode) throws Exception{
		
		String sql="insert into DT_SealerAuthDetail(sealerCode,authCode) values('"+sealerCode+"','"+authCode+"')";
		this.daoSupport.execute(sql, null);
	}
	
	/**
	 * 更新经销商信息
	 * @param sealerInfo
	 * 经销商信息
	 * @throws Exception
	 */
	@Transactional
	public int insertSealer2(SealerInfo sealerInfo) throws Exception {
		
		this.daoSupport.insert("DT_SealerInfo", sealerInfo);
		return 0;
	}
	
	/**
	 * 添加经销商信息
	 * @param sealerInfo
	 * 经销商信息
	 * @throws Exception
	 */
	@Transactional
	public int updateSealer(SealerInfo sealerInfo) throws Exception {
		this.daoSupport.update("DT_SealerInfo", sealerInfo, "code='"+sealerInfo.getCode()+"'");
		return 0;
	}
	
	@Override
	public void updateSealer(String code, String lastLoginTime) throws Exception
	{
		String sql = "update DT_SealerInfo set lastLoginTime=? where code= ?";
		this.daoSupport.execute(sql,lastLoginTime,code);
	}

	public SealerInfo getSealerByCode(String code) throws Exception {
		String sql = "select * from DT_SealerInfo where code = ?";
		
		return (SealerInfo) this.daoSupport.queryForObject(sql, SealerInfo.class, code);
	}

	@Transactional
	public int delSealer(String code) throws Exception {
		String sql = "delete from DT_SealerInfo where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delSealers(String codes) throws Exception {
		String sql = "delete from DT_SealerInfo where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}

	@Override
	public SealerInfo getSealerInfoByAutelIdAndPassword(String autelId,
			String password) throws Exception 
	{
		StringBuffer sql = new StringBuffer();
		sql.append("select s.*,a.authCode as auth from DT_SealerInfo s left join DT_SealerDataAuthDeails a on  s.autelId=a.sealerCode where s.autelId='").append(autelId)
			.append("' and s.userPwd='").append(password).append("'")
		    .append("  and s.status=1");
		return (SealerInfo) this.daoSupport.queryForObject(sql.toString(), SealerInfo.class);
	}



	/**
	  * 经销商信息分页显示方法
	  * @param SealerInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageSealerInfoVoPage(SealerInfoVo sealerInfoVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		/*sql.append("select * from (");*/
		sql.append("select  a.id,a.autelId,a.code,a.firstName,a.middleName,a.lastName,a.name,a.email,a.secondEmail,a.telephone," +
			"a.userPwd,a.daytimePhoneCC,a.daytimePhoneAC,a.daytimePhone,a.daytimePhoneExtCode,a.regTime,a.name sealerName");
		sql.append(" from DT_SealerInfo a");
	/*	sql.append(" left join DT_SaleContract d on a.code=d.sealercode");
		sql.append(" left join DT_AreaConfig f on d.areaCfgCode=f.code");*/
		sql.append(" where 1=1");
		
		if (null != sealerInfoVo) {
			if (StringUtils.isNotBlank(sealerInfoVo.getSealerName())) {
				sql.append(" and a.name like '%");
				sql.append(sealerInfoVo.getSealerName().trim());
				sql.append("%'");
			}
			/*if (StringUtils.isNotBlank(sealerInfoVo.getAutelId())) {
				sql.append(" and a.autelId like '%");
				sql.append(sealerInfoVo.getAutelId().trim());
				sql.append("%'");
			}*/
			if (StringUtils.isNotBlank(sealerInfoVo.getAutelId())) {
				sql.append(" and a.autelID like '%");
				sql.append(sealerInfoVo.getAutelId().trim());
				sql.append("%'");
			}
		}

	/*	sql.append(") m");*/
		sql.append(" order by a.id desc");
			Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
					pageSize, SealerInfoVo.class);
			return page;
	}

	
	@Override
	public Page pageSealerInfoVoPage2(SealerInfoVo sealerInfoVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ");
		sql.append("DT_SealerInfo a");
		
		sql.append(" where 1=1");
		
		if (null != sealerInfoVo) {
			if (StringUtils.isNotBlank(sealerInfoVo.getAutelId())) {
				sql.append(" and a.autelID like '%");
				sql.append(sealerInfoVo.getAutelId().trim());
				sql.append("%'");
			}
		}
       sql.append(" order by  id desc");
		
			Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
					pageSize, SealerInfoVo.class);
			return page;
	}
	

	@Override
	public SealerInfo getSealerByAutelId(String autelId) throws Exception 
	{
		String sql = "select s.*,a.authCode as auth from DT_SealerInfo s left   join DT_SealerDataAuthDeails a on  s.autelId=a.sealerCode where   s.autelId = ?";
		
		return (SealerInfo) this.daoSupport.queryForObject(sql, SealerInfo.class, autelId);
	}
	
	
	public List<SealerInfo> querySealerInfo() {
		String sql = "select lan.* from DT_SealerInfo lan order by id asc";
		return this.daoSupport.queryForList(sql, SealerInfo.class, new Object[]{});
	}

	@Override
	public Page querySealerInfoPage(String areaCode, String country,int pageNo,int pageSize)
			throws Exception 
	{
		StringBuffer sql = new StringBuffer();

		sql.append("select * from (select  distinct a.id, a.country,a.name sealerName,a.daytimePhone,a.address,d.name as areaName ");
		sql.append(" from DT_SealerInfo a");
		sql.append(" ,DT_SaleContract b,DT_AreaConfig d where a.code = b.sealercode and b.areaCfgCode = d.code");
		
		
		if (StringUtils.isNotBlank(areaCode)) 
		{
			sql.append(" and b.areaCfgCode='").append(areaCode).append("'");
		}
		
		if (StringUtils.isNotBlank(country))
		{
			sql.append(" and a.country like '%").append(country).append("%'");
		}


		sql.append(" ) m order by m.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
					pageSize, new RowMapper()
		{

						@Override
						public SealerInfo mapRow(ResultSet rs, int arg1)
								throws SQLException {

							SealerInfo sealerInfo = new SealerInfo();
							sealerInfo.setCountry(rs.getString("country"));
							sealerInfo.setSealerName(rs.getString("sealerName"));
							sealerInfo.setDaytimePhone(rs.getString("daytimePhone"));
							sealerInfo.setAddress(rs.getString("address"));
							sealerInfo.setAreaName(rs.getString("areaName"));
							return sealerInfo;
						}
		});
		
		return page;
	}



}
