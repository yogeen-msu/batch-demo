package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ICustomerSoftwareService;
import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客户软件信息service实现
 * @author pengdongan
 * @date 2013-01-07
 * 
 */
@Service
public class CustomerSoftwareServiceImpl extends BaseSupport implements ICustomerSoftwareService {


	
	@Transactional
	public List<CustomerSoftwareVo> listSealer(CustomerSoftwareVo customerSoftwareVo)
	throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select d.autelId customerAutelId,d.name customerName,");
		sql.append("e.name minSaleUnitName,b.serialNo proSerialNo,h.name areaCfgName,g.autelId sealerAutelId,");
		sql.append("g.name sealerName,a.validDate proSoftValidDate");
		sql.append(" from DT_ProductSoftwareValidStatus a");
		sql.append(" left join DT_ProductInfo b on a.proCode=b.code");
		sql.append(" left join DT_CustomerProInfo c on b.code=c.proCode");
		sql.append(" left join DT_CustomerInfo d on c.customerCode=d.code");
		sql.append(" left join DT_MinSaleUnitMemo e on a.minSaleUnitCode=e.minSaleUnitCode and e.languageCode=?");
		sql.append(" left join DT_SaleContract f on b.saleContractCode=f.code");
		sql.append(" left join DT_SealerInfo g on f.sealerCode=g.code");
		sql.append(" left join DT_AreaConfig h on f.areaCfgCode=h.code");
		sql.append(" where 1=1");
		
		if (null != customerSoftwareVo) {
			if (StringUtils.isNotBlank(customerSoftwareVo.getCustomerAutelId())) {
				sql.append(" and d.autelId like '%");
				sql.append(customerSoftwareVo.getCustomerAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerSoftwareVo.getSealerAutelId())) {
				sql.append(" and g.autelId like '%");
				sql.append(customerSoftwareVo.getSealerAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerSoftwareVo.getProSerialNo())) {
				sql.append(" and b.serialNo like '%");
				sql.append(customerSoftwareVo.getProSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerSoftwareVo.getAreaCfgName())) {
				sql.append(" and h.name like '%");
				sql.append(customerSoftwareVo.getAreaCfgName().trim());
				sql.append("%'");
			}			
		}		
//
//		
//		if (StringUtils.isNotBlank(customerSoftwareVo.getAdminLanguageCode())) {
//			sql.append(" and e.languageCode = '");
//			sql.append(customerSoftwareVo.getAdminLanguageCode().trim());
//			sql.append("'");
//		}
		

		sql.append(" order by a.id desc");
		
		return (List<CustomerSoftwareVo>) this.daoSupport.queryForList(sql.toString(),CustomerSoftwareVo.class, customerSoftwareVo.getAdminLanguageCode().trim());
	}
	
	/**
	 * 根据编码查询客户软件信息对象
	 * @param code	客户软件信息code
	 * @return
	 * @throws Exception
	 */

	public CustomerSoftwareVo getSealerByCode(String code) throws Exception {
		String sql = "select * from DT_ProductUpgradeRecord where code = ?";
		
		return (CustomerSoftwareVo) this.daoSupport.queryForObject(sql, CustomerSoftwareVo.class, code);
	}


	/**
	  * 客户软件信息分页显示方法
	  * @param CustomerSoftwareVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerSoftwareVoPage(CustomerSoftwareVo customerSoftwareVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
//        sql.append("select * from (");
		sql.append("select distinct d.autelId customerAutelId,d.name customerName,");
		sql.append("b.serialNo proSerialNo,h.name areaCfgName,g.autelId sealerAutelId,");
		sql.append("g.name sealerName,a.validDate proSoftValidDate,l.name as softLanguage,f.name as saleContract,b.regTime as proRegisterDate");
		sql.append(" from DT_ProductSoftwareValidStatus a");
		sql.append(" left join DT_ProductInfo b on a.proCode=b.code");
		sql.append(" left join DT_CustomerProInfo c on b.code=c.proCode");
		sql.append(" left join DT_CustomerInfo d on c.customerCode=d.code");
		sql.append(" left join DT_MinSaleUnitMemo e on a.minSaleUnitCode=e.minSaleUnitCode and e.languageCode=?");
		sql.append(" left join DT_SaleContract f on b.saleContractCode=f.code");
		sql.append(" left join DT_SealerInfo g on f.sealerCode=g.code");
		sql.append(" left join DT_AreaConfig h on f.areaCfgCode=h.code");
		sql.append(" left join DT_LanguageConfig l on f.languageCfgCode=l.code");
		sql.append(" where 1=1");
		
		if (null != customerSoftwareVo) {
			if (StringUtils.isNotBlank(customerSoftwareVo.getProCode())) {
				sql.append(" and b.code = '");
				sql.append(customerSoftwareVo.getProCode().trim());
				sql.append("'");
			}
			
			if (StringUtils.isNotBlank(customerSoftwareVo.getCustomerAutelId())) {
				sql.append(" and d.autelId like '%");
				sql.append(customerSoftwareVo.getCustomerAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerSoftwareVo.getSealerAutelId())) {
				sql.append(" and g.autelId like '%");
				sql.append(customerSoftwareVo.getSealerAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerSoftwareVo.getProSerialNo())) {
				sql.append(" and b.serialNo like '%");
				sql.append(customerSoftwareVo.getProSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerSoftwareVo.getAreaCfgName())) {
				sql.append(" and h.name like '%");
				sql.append(customerSoftwareVo.getAreaCfgName().trim());
				sql.append("%'");
			}			
		}		

		
//		if (StringUtils.isNotBlank(customerSoftwareVo.getAdminLanguageCode())) {
//			sql.append(" and e.languageCode = '");
//			sql.append(customerSoftwareVo.getAdminLanguageCode().trim());
//			sql.append("'");
//		}

//		sql.append(" ) a order by a.customerAutelId desc");
		sql.append("  order by d.autelId desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, CustomerSoftwareVo.class, customerSoftwareVo.getAdminLanguageCode().trim());
		return page;
	}

}
