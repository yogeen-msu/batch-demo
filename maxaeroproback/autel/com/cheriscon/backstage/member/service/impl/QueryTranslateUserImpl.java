package com.cheriscon.backstage.member.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IQueryTranslateUser;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.database.BaseJdbcDaoSupport;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

/**
 * 客诉信息(1:个人用户 2：经销商用户)业务逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Service
public class QueryTranslateUserImpl extends BaseSupport<AdminUser> 
		implements IQueryTranslateUser
{

	
    public List<AdminUser> listByRoleId2(int roleid){
		
		String sql  = "select u.* from es_adminuser u ,es_user_role ur where ur.userid=u.userid and ur.roleid=?";
		
		
		return this.daoSupport.queryForList(sql, AdminUser.class, roleid);
		
	}
}
