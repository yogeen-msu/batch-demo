package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ISdkAppService;
import com.cheriscon.backstage.member.vo.SdkAppVO;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * sdk app逻辑实现类
 * 
 * @author autel
 * 
 */
@Service
public class SdkAppServiceImpl extends BaseSupport<SdkAppVO> implements
		ISdkAppService {

	private static Logger logger = Logger.getLogger(SdkAppServiceImpl.class);

	/**
	 * 查詢sdk app集合(未分页)
	 * 
	 * @param sdkAppVO
	 * @return sdaAppList
	 */
	public List<SdkAppVO> querySdkAppList(SdkAppVO sdkAppVO) {
		StringBuffer sql = new StringBuffer();
		sql.append("select id, app_key, app_name, software_platform, package_name, category, description,status,customer_Id,create_date,last_update_date");
		sql.append(" from es_sdk_app_t");
		sql.append(" where 1=1");
		if (null != sdkAppVO) {
			if (StringUtils.isNotBlank(sdkAppVO.getStatus())) {
				sql.append(" and status = '");
				sql.append(sdkAppVO.getStatus().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(sdkAppVO.getCustomerId())) {
				sql.append(" and customer_Id = '");
				sql.append(sdkAppVO.getCustomerId().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(sdkAppVO.getSoftwarePlatform())
					&& !"-1".equals(sdkAppVO.getSoftwarePlatform())) {
				sql.append(" and software_platform = '");
				sql.append(sdkAppVO.getSoftwarePlatform().trim());
				sql.append("'");
			}
		}
		sql.append(" order by last_update_date desc");
		logger.info("querySdkAppList sql:" + sql.toString());
		List<SdkAppVO> sdaAppList = this.daoSupport.queryForList(
				sql.toString(), SdkAppVO.class);
		return sdaAppList;
	}

	/**
	 * 查詢sdk app集合
	 * 
	 * @param sdkAppVO
	 * @param pageNo
	 * @param pageSize
	 * @return Page
	 */
	public Page querySdkAppList(SdkAppVO sdkAppVO, int pageNo, int pageSize) {
		StringBuffer sql = new StringBuffer();
		sql.append("select e.id, e.app_key, e.app_name, e.software_platform, e.package_name, e.category, e.description,e.status,e.customer_Id,d.autelId");
		sql.append(",date_format(create_date, '%Y-%m-%d %H:%i:%s') as create_date");
		sql.append(",date_format(last_update_date, '%Y-%m-%d %H:%i:%s') as last_update_date");
		sql.append(" from es_sdk_app_t e,DT_CustomerInfo d");
		sql.append(" where e.customer_Id = d.id");
		if (null != sdkAppVO) {
			if (StringUtils.isNotBlank(sdkAppVO.getStatus())) {
				sql.append(" and e.status = '");
				sql.append(sdkAppVO.getStatus().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(sdkAppVO.getCustomerId())) {
				sql.append(" and e.customer_Id = '");
				sql.append(sdkAppVO.getCustomerId().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(sdkAppVO.getSoftwarePlatform())
					&& !"-1".equals(sdkAppVO.getSoftwarePlatform())) {
				sql.append(" and e.software_platform = '");
				sql.append(sdkAppVO.getSoftwarePlatform().trim());
				sql.append("'");
			}
		}
		sql.append(" order by last_update_date desc");
		logger.info("querySdkAppList sql:" + sql.toString());
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, SdkAppVO.class);
		return page;
	}

	/**
	 * 查詢sdk app详情
	 * 
	 * @param id
	 * @return SdkAppVO
	 */
	public SdkAppVO getSdkAppDetails(SdkAppVO sdkAppVO) {
		StringBuffer sql = new StringBuffer();
		sql.append("select id, app_key, app_name, software_platform, package_name, category, description,status,customer_Id,create_date,last_update_date");
		sql.append(" from es_sdk_app_t");
		sql.append(" where 1=1");
		if (null != sdkAppVO) {
			if (StringUtils.isNotBlank(sdkAppVO.getId())) {
				sql.append(" and id = '");
				sql.append(sdkAppVO.getId().trim());
				sql.append("'");
			}
		}
		logger.info("getSdkAppDetails sql:" + sql.toString());
		List<SdkAppVO> queryForList = this.daoSupport.queryForList(
				sql.toString(), SdkAppVO.class);
		if (null != queryForList && queryForList.size() > 0) {
			return queryForList.get(0);
		}
		return null;
	}

	/**
	 * 添加sdk app
	 * 
	 * @param sdkAppVO
	 * @return boolean
	 */
	public boolean addSdkAppList(SdkAppVO sdkAppVO) {
		boolean result = true;
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO es_sdk_app_t (app_key, app_name, software_platform, package_name, category, description,status,customer_Id,create_date,last_update_date)");
		sql.append(" VALUES (?, ?, ?, ?, ?, ?, '1', ?, sysdate(), sysdate())");
		logger.info("getSdkAppDetails sql:" + sql.toString());
		try {
			this.daoSupport.execute(sql.toString(), sdkAppVO.getAppKey(),
					sdkAppVO.getAppName(), sdkAppVO.getSoftwarePlatform(),
					sdkAppVO.getPackageName(), sdkAppVO.getCategory(),
					sdkAppVO.getDescription(), sdkAppVO.getCustomerId());
		} catch (Exception e) {
			result = false;
			logger.error("addSdkAppList error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 修改sdk app
	 * 
	 * @param sdkAppVO
	 * @return boolean
	 */
	public boolean updateSdkAppList(SdkAppVO sdkAppVO) {
		boolean result = true;
		StringBuffer sql = new StringBuffer();
		sql.append("update es_sdk_app_t set");
		if (StringUtils.isNotBlank(sdkAppVO.getAppName())) {
			sql.append(" app_name = ?,");
		}
		if (StringUtils.isNotBlank(sdkAppVO.getDescription())) {
			sql.append(" description = ?,");
		}
		sql.append(" last_update_date = sysdate() where id=?");
		logger.info("getSdkAppDetails sql:" + sql.toString());
		try {
			this.daoSupport.execute(sql.toString(), sdkAppVO.getAppName(),
					sdkAppVO.getDescription(), sdkAppVO.getId());
		} catch (Exception e) {
			result = false;
			logger.error("updateSdkAppList error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 删除sdk app
	 * 
	 * @param sdkAppVO
	 * @return boolean
	 */
	public boolean deleteSdkAppList(SdkAppVO sdkAppVO) {
		boolean result = true;
		StringBuffer sql = new StringBuffer();
		sql.append("update es_sdk_app_t set status = 0,last_update_date = sysdate() where id=? ");
		logger.info("getSdkAppDetails sql:" + sql.toString());
		try {
			this.daoSupport.execute(sql.toString(), sdkAppVO.getId());
		} catch (Exception e) {
			result = false;
			logger.error("deleteSdkAppList error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 查詢查询用户sdk app协议详情
	 * 
	 * @return String
	 */
	public SdkAppVO getCustomerAgreementDetails(SdkAppVO sdkAppVO) {
		StringBuffer sql = new StringBuffer();
		sql.append("select id,customer_Id,sign");
		sql.append(" from es_sdk_customer_agreement_t");
		sql.append(" where 1=1");
		if (null != sdkAppVO) {
			if (StringUtils.isNotBlank(sdkAppVO.getCustomerId())) {
				sql.append(" and customer_Id = '");
				sql.append(sdkAppVO.getCustomerId().trim());
				sql.append("'");
			}
		}
		logger.info("getCustomerAgreementDetails sql:" + sql.toString());
		List<SdkAppVO> queryForList = this.daoSupport.queryForList(
				sql.toString(), SdkAppVO.class);
		if (null != queryForList && queryForList.size() > 0) {
			return queryForList.get(0);
		}
		return null;
	}

	/**
	 * 用户同意sdk app协议详情
	 * 
	 * @return String
	 */
	public boolean saveCustomerAgreementDetails(SdkAppVO sdkAppVO) {
		boolean result = true;
		StringBuffer sql = new StringBuffer();
		SdkAppVO existsVo = getCustomerAgreementDetails(sdkAppVO);
		if (null != existsVo) {
			sql.append("update es_sdk_customer_agreement_t set sign=1 where customer_Id=?");
		} else {
			sql.append("insert into es_sdk_customer_agreement_t (customer_Id,sign) values(?,1) ");
		}
		logger.info("saveCustomerAgreementDetails sql:" + sql.toString());
		try {
			this.daoSupport.execute(sql.toString(), sdkAppVO.getCustomerId());
		} catch (Exception e) {
			result = false;
			logger.error("saveCustomerAgreementDetails error." + e.getMessage());
		}
		return result;
	}
}
