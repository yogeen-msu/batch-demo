package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ICustomerInfoVoService;
import com.cheriscon.backstage.member.vo.CustomerInfoVo;
import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderMinSaleUnitDetail;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客户信息service实现
 * @author pengdongan
 * @date 2013-01-07
 * 
 */
@Service
public class CustomerInfoVoServiceImpl extends BaseSupport<CustomerInfoVo> implements
		ICustomerInfoVoService {


	
	@Transactional
	public List<CustomerInfoVo> listCustomer(CustomerInfoVo customerInfoVo)
	throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select distinct a.code,a.firstName,a.middleName,a.lastName,a.name,a.secondEmail,a.userPwd,a.mobilePhone,a.regTime,a.actCode,a.actState,a.autelId,");
		sql.append("e.autelId sealerAutelId,f.name areaCfgName");
		sql.append(" from DT_CustomerInfo a");
		sql.append(" left join DT_CustomerProInfo b on a.code=b.customerCode");
		sql.append(" left join DT_ProductInfo c on b.proCode=c.code");
		sql.append(" left join DT_SaleContract d on c.saleContractCode=d.code");
		sql.append(" left join DT_SealerInfo e on d.sealerCode=e.code");
		sql.append(" left join DT_AreaConfig f on d.areaCfgCode=f.code");
		sql.append(" where 1=1");
		
		if (null != customerInfoVo) {
			if (customerInfoVo.getActState() != null) {
				sql.append(" and a.actState = ");
				sql.append(customerInfoVo.getActState().toString());
			}
			if (StringUtils.isNotBlank(customerInfoVo.getAutelId())) {
				sql.append(" and a.autelId like '%");
				sql.append(customerInfoVo.getAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getSealerAutelId())) {
				sql.append(" and e.autelId like '%");
				sql.append(customerInfoVo.getSealerAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getAreaCfgName())) {
				sql.append(" and f.name like '%");
				sql.append(customerInfoVo.getAreaCfgName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getStartTime())) {
				sql.append(" and a.regTime >= '");
				sql.append(customerInfoVo.getStartTime().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getEndTime())) {
				sql.append(" and a.regTime < cast('");
				sql.append(customerInfoVo.getEndTime().trim());
				sql.append("' as datetime)+1");
			}
		}
		
		return this.daoSupport.queryForList(sql.toString(),CustomerInfoVo.class);
	}

	/**
	 * 查询客户消费记录
	 * @param customerInfoVo
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page queryCustomerOrderInfo(CustomerInfoVo customerInfoVo, int pageNo, int pageSize){
		String oriTbl = DBUtils.getTableName(OrderInfo.class);
		String omsudTbl = DBUtils.getTableName(OrderMinSaleUnitDetail.class);
		String pifTbl = DBUtils.getTableName(ProductInfo.class);
		String sctTbl = DBUtils.getTableName(SaleContract.class);
		String sealTbl = DBUtils.getTableName(SealerInfo.class);
		String cusTbl = DBUtils.getTableName(CustomerInfo.class);
		String acfTbl = DBUtils.getTableName(AreaConfig.class);
		String msumTbl = DBUtils.getTableName(MinSaleUnitMemo.class);
		
		StringBuffer sql = new StringBuffer("select ");
		sql.append(" ori.id as id,ori.code as code, omsud.consumeType as consumeType,ori.userCode as userCode,cus.firstName as firstName,cus.middleName as middleName,cus.lastName as lastName,  ");
		sql.append(" seal.autelId as sealAutelId,cus.autelId as autelId,acf.name as areaCfgName,acf.code as areaCfgCode,pif.serialNo as serialNo, case omsud.consumeType when 1 then sct.name else msum.name end as minSaleUnitName,omsud.minSaleUnitPrice as orderMoney, ");
		sql.append(" ori.orderDate as orderDate,ori.orderState as orderState,ori.orderPayState as orderPayState,ori.recConfirmState as recConfirmState ");
		sql.append(" from "+oriTbl+"  ori ");
		sql.append(" inner join "+omsudTbl+" omsud on omsud.orderCode = ori.code ");
		sql.append(" inner join "+pifTbl+" pif on pif.serialNo = omsud.productSerialNo");
		sql.append(" inner join "+sctTbl+" sct on sct.code = pif.saleContractCode ");
		sql.append(" inner join "+sealTbl+" seal on seal.code = sct.sealerCode ");
		sql.append(" inner join "+cusTbl+" cus on cus.code = ori.userCode ");
		sql.append(" inner join "+acfTbl+" acf on acf.code = sct.areaCfgCode ");
		sql.append(" left join "+msumTbl+" msum on msum.minSaleUnitCode =omsud.minSaleUnitCode and msum.languageCode=cus.languageCode ");
		
		sql.append(" where 1=1 ");
		if(customerInfoVo != null){	
			if(StringUtils.isNotEmpty(customerInfoVo.getCode())){		//用户ID
				sql.append(" and  cus.code = '"+customerInfoVo.getCode().trim()+"'");
			}
			if(StringUtils.isNotEmpty(customerInfoVo.getAutelId())){		//用户ID
				sql.append(" and  cus.autelId like '%"+customerInfoVo.getAutelId()+"%' ");
			}
			
			if(customerInfoVo != null && customerInfoVo.getConsumeType() > -1){		//消费类型
				sql.append(" and  omsud.consumeType ="+customerInfoVo.getConsumeType()+" ");
			}
			
			if(StringUtils.isNotEmpty(customerInfoVo.getAreaCfgName())){		//区域配置
				sql.append(" and  acf.name like '%"+customerInfoVo.getAreaCfgName()+"%' ");
			}
			
			
			if(StringUtils.isNotEmpty(customerInfoVo.getSealAutelId())){			//经销商ID
				sql.append(" and  seal.autelId like '%"+customerInfoVo.getSealAutelId()+"%' ");
			}
			
			if(StringUtils.isNotEmpty(customerInfoVo.getStartTime())){			//开始时间
				sql.append(" and  ori.orderDate >= '"+customerInfoVo.getStartTime()+"' ");
			}
			
			if(StringUtils.isNotEmpty(customerInfoVo.getEndTime())){			//结束时间
				sql.append(" and  ori.orderDate <= '"+customerInfoVo.getEndTime()+"' ");
			}
		}
		
		sql.append(" order by ori.orderDate desc,ori.id asc ");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,CustomerInfoVo.class);
	}



	public Page pageCustomerInfoVoPage2(String type,CustomerInfoVo customerInfoVo, int pageNo,
			int pageSize) throws Exception{
		
StringBuffer sql = new StringBuffer();
		
		sql.append("select * from (");

		sql.append("select distinct a.id,a.code,a.firstName,a.middleName,a.lastName,a.name,a.secondEmail,a.userPwd,a.mobilePhone,a.regTime,a.actCode,a.actState,a.autelId,");
		sql.append("e.autelId sealerAutelId,f.name areaCfgName");
		sql.append(" from DT_CustomerInfo a");
		sql.append(" inner join DT_CustomerProInfo b on a.code=b.customerCode");
		sql.append(" inner join DT_ProductInfo c on b.proCode=c.code");
		sql.append(" inner join DT_SaleContract d on c.saleContractCode=d.code");
		sql.append(" inner join DT_SealerInfo e on d.sealerCode=e.code");
		sql.append(" inner join DT_AreaConfig f on d.areaCfgCode=f.code");
		sql.append(" where 1=1");
		
		if (null != customerInfoVo) {
			if (customerInfoVo.getActState() != null) {
				sql.append(" and a.actState = ");
				sql.append(customerInfoVo.getActState().toString());
			}
			if (StringUtils.isNotBlank(customerInfoVo.getAutelId())) {
				sql.append(" and a.autelId like '%");
				sql.append(customerInfoVo.getAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getSealerAutelId())) {
				sql.append(" and e.autelId like '%");
				sql.append(customerInfoVo.getSealerAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getAreaCfgName())) {
				sql.append(" and f.name like '%");
				sql.append(customerInfoVo.getAreaCfgName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getStartTime())) {
				sql.append(" and a.regTime >= '");
				sql.append(customerInfoVo.getStartTime().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getEndTime())) {
				sql.append(" and a.regTime < cast('");
				sql.append(customerInfoVo.getEndTime().trim());
				sql.append("' as datetime)+1");
			}
			if (type.equals("1")) {
				sql.append(" and f.name='China_Area'");
			}
			if (type.equals("2")) {
				sql.append(" and f.name!='China_Area'");
			}
		}
		
		sql.append(") m");

		sql.append(" order by m.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, CustomerInfoVo.class);
		return page;
		
	}

	/**
	  * 客户信息分页显示方法
	  * @param CustomerInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerInfoVoPage(CustomerInfoVo customerInfoVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		
//		sql.append("select * from (");

		sql.append("select distinct a.id,a.code,a.firstName,a.middleName,a.lastName,a.name,a.secondEmail,a.userPwd,a.mobilePhone,a.regTime,a.actCode,a.actState,a.autelId,");
		sql.append("e.autelId sealerAutelId,f.name areaCfgName,c.serialNo as serialNo");
		sql.append(" from DT_CustomerInfo a");
		sql.append(" left join DT_CustomerProInfo b on a.code=b.customerCode");
		sql.append(" left join DT_ProductInfo c on b.proCode=c.code");
		sql.append(" left join DT_SaleContract d on c.saleContractCode=d.code");
		sql.append(" left join DT_SealerInfo e on d.sealerCode=e.code");
		sql.append(" left join DT_AreaConfig f on d.areaCfgCode=f.code");
		sql.append(" where 1=1");
		
		if (null != customerInfoVo) {
			if (customerInfoVo.getActState() != null) {
				sql.append(" and a.actState = ");
				sql.append(customerInfoVo.getActState().toString());
			}
			if (StringUtils.isNotBlank(customerInfoVo.getAutelId())) {
				sql.append(" and a.autelId like '%");
				sql.append(customerInfoVo.getAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getSealerAutelId())) {
				sql.append(" and e.autelId like '%");
				sql.append(customerInfoVo.getSealerAutelId().trim());
				sql.append("%'");
			}
			
			if (StringUtils.isNotBlank(customerInfoVo.getSerialNo())) {
				sql.append(" and c.serialNo like '%");
				sql.append(customerInfoVo.getSerialNo().trim());
				sql.append("%'");
			}
			
			if (StringUtils.isNotBlank(customerInfoVo.getAreaCfgName())) {
				sql.append(" and f.name like '%");
				sql.append(customerInfoVo.getAreaCfgName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getStartTime())) {
				sql.append(" and a.regTime >= '");
				sql.append(customerInfoVo.getStartTime().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(customerInfoVo.getEndTime())) {
				sql.append(" and a.regTime < cast('");
				sql.append(customerInfoVo.getEndTime().trim());
				sql.append("' as datetime)+1");
			}
		}
		
//		sql.append(") m");
//
//		sql.append(" order by m.id desc");
		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, CustomerInfoVo.class);
		return page;
	}
	

}
