package com.cheriscon.backstage.member.service.impl;

import java.io.ByteArrayInputStream;

import java.io.ByteArrayOutputStream;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.cheriscon.backstage.member.service.IExportExcelService;
import com.cheriscon.backstage.member.vo.CustomerInfoVo;
import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.backstage.member.vo.ProUpgradeRecordVo;
import com.cheriscon.backstage.trade.vo.OrderInfoVo;
import com.cheriscon.backstage.trade.vo.RechargeRecordVo;
import com.cheriscon.common.model.ReChargeRecord;

import jxl.Workbook;

import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;

import jxl.write.WritableWorkbook;

public class ExportExcelServiceImpl implements IExportExcelService {

	// 将OutputStream转化为InputStream
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	WritableWorkbook wwb;

	@Override
	public InputStream getRechargeRecordExcelInputStream(
			List<RechargeRecordVo> rechargeRecord) {
		try {
			RechargeRecordVo record = null;
			wwb = Workbook.createWorkbook(out);
			WritableSheet ws = wwb.createSheet("Sheet1", 0);
			WritableCellFormat firstFormat = new WritableCellFormat();

			// 设置背景色
			firstFormat.setBackground(Colour.YELLOW);
			ws.addCell(new Label(0, 0, "升级卡序列号", firstFormat));
			ws.addCell(new Label(1, 0, "产品序列号", firstFormat));
			ws.addCell(new Label(2, 0, "产品类型", firstFormat));
			ws.addCell(new Label(3, 0, "产品注册用户", firstFormat));
			ws.addCell(new Label(4, 0, "升级时间", firstFormat));
			ws.addCell(new Label(5, 0, "升级卡销售经销商", firstFormat));

			// 设置显示长度.
			ws.setColumnView(0, 25);
			ws.setColumnView(1, 25);
			ws.setColumnView(2, 30);
			ws.setColumnView(3, 30);
			ws.setColumnView(4, 30);
			ws.setColumnView(5, 20);

			for (int j = 0; j < rechargeRecord.size(); j++) {
				record = rechargeRecord.get(j);

				if (record != null) {

					ws.addCell(new Label(0, j + 1, ""
							+ record.getReChargeCardSerialNo() != null ? record
							.getReChargeCardSerialNo() : ""));

					ws.addCell(new Label(1, j + 1,
							"" + record.getProSerialNo() != null ? record
									.getProSerialNo() : ""));

					ws.addCell(new Label(2, j + 1,
							"" + record.getProTypeName() != null ? record
									.getProTypeName() : ""));

					ws.addCell(new Label(3, j + 1, ""
							+ record.getMemberAutelId() != null ? record
							.getMemberAutelId() : ""));

					ws.addCell(new Label(4, j + 1, ""
							+ record.getReChargeTime() != null ? record
							.getReChargeTime() : ""));

					ws.addCell(new Label(5, j + 1, ""
							+ record.getSealerAutelId() != null ? record
							.getSealerAutelId() : ""));

				}
			}
			wwb.write();
			wwb.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ByteArrayInputStream(out.toByteArray());
	}

	@Override
	public InputStream getCustomerInfoExcelInputStream(
			List<CustomerInfoVo> customerInfoVos) {
		CustomerInfoVo customerInfoVo;
		String userActState = "";
		try {

			wwb = Workbook.createWorkbook(out);

			WritableSheet ws = wwb.createSheet("Sheet1", 0);

			// WritableFont font = new WritableFont(WritableFont.ARIAL,20,WritableFont.BOLD,false,UnderlineStyle.NO_UNDERLINE,Colour.RED);
			WritableCellFormat firstFormat = new WritableCellFormat();

			// 设置背景色
			firstFormat.setBackground(Colour.YELLOW);

			ws.addCell(new Label(0, 0, "用户ID", firstFormat));
			ws.addCell(new Label(1, 0, "用户名", firstFormat));
			ws.addCell(new Label(2, 0, "手机", firstFormat));
			ws.addCell(new Label(3, 0, "经销商", firstFormat));
			ws.addCell(new Label(4, 0, "区域配置", firstFormat));
			ws.addCell(new Label(5, 0, "注册时间", firstFormat));
			ws.addCell(new Label(6, 0, "用户状态", firstFormat));

			// 设置显示长度.
			ws.setColumnView(0, 25);
			ws.setColumnView(1, 15);
			ws.setColumnView(2, 15);
			ws.setColumnView(3, 15);
			ws.setColumnView(4, 20);
			ws.setColumnView(5, 20);
			ws.setColumnView(6, 20);

			for (int j = 0; j < customerInfoVos.size(); j++) {
				customerInfoVo = customerInfoVos.get(j);

				if (customerInfoVo != null) {
					if (customerInfoVo.getActState() == null) {
						userActState = "";
					} else if (customerInfoVo.getActState() == 0) {
						userActState = "未激活";
					} else if (customerInfoVo.getActState() == 1) {
						userActState = "已激活";
					} else {
						userActState = "其它";
					}

					ws.addCell(new Label(
							0,
							j + 1,
							"" + customerInfoVo.getAutelId() != null ? customerInfoVo
									.getAutelId() : ""));

					ws.addCell(new Label(1, j + 1, ""
							+ customerInfoVo.getName() != null ? customerInfoVo
							.getName() : ""));

					ws.addCell(new Label(
							2,
							j + 1,
							"" + customerInfoVo.getMobilePhone() != null ? customerInfoVo
									.getMobilePhone() : ""));

					ws.addCell(new Label(
							3,
							j + 1,
							"" + customerInfoVo.getSealerAutelId() != null ? customerInfoVo
									.getSealerAutelId() : ""));

					ws.addCell(new Label(
							4,
							j + 1,
							"" + customerInfoVo.getAreaCfgName() != null ? customerInfoVo
									.getAreaCfgName() : ""));

					ws.addCell(new Label(
							5,
							j + 1,
							"" + customerInfoVo.getRegTime() != null ? customerInfoVo
									.getRegTime() : ""));

					ws.addCell(new Label(6, j + 1, "" + userActState));
				}
			}

			wwb.write();

			wwb.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	@Override
	public InputStream getCustomerProductExcelInputStream(
			List<CustomerProductVo> customerProductVos) {
		CustomerProductVo customerProductVo;
		String proRegStatus;
		try {

			wwb = Workbook.createWorkbook(out);

			WritableSheet ws = wwb.createSheet("Sheet1", 0);

			// WritableFont font = new WritableFont(WritableFont.ARIAL,20,WritableFont.BOLD,false,UnderlineStyle.NO_UNDERLINE,Colour.RED);
			WritableCellFormat firstFormat = new WritableCellFormat();

			// 设置背景色
			firstFormat.setBackground(Colour.YELLOW);

			ws.addCell(new Label(0, 0, "用户ID", firstFormat));
			ws.addCell(new Label(1, 0, "用户名", firstFormat));
			ws.addCell(new Label(2, 0, "产品序列号", firstFormat));
			ws.addCell(new Label(3, 0, "区域配置", firstFormat));
			ws.addCell(new Label(4, 0, "经销商ID", firstFormat));
			ws.addCell(new Label(5, 0, "出厂时期", firstFormat));
			ws.addCell(new Label(6, 0, "产品注册时间", firstFormat));
			ws.addCell(new Label(7, 0, "绑定状态", firstFormat));

			// 设置显示长度.
			ws.setColumnView(0, 25);
			ws.setColumnView(1, 15);
			ws.setColumnView(2, 15);
			ws.setColumnView(3, 15);
			ws.setColumnView(4, 20);
			ws.setColumnView(5, 20);
			ws.setColumnView(6, 20);
			ws.setColumnView(7, 20);

			for (int j = 0; j < customerProductVos.size(); j++) {
				customerProductVo = customerProductVos.get(j);

				if (customerProductVo != null) {
					if (customerProductVo.getProRegStatus() == null) {
						proRegStatus = "";
					} else if (customerProductVo.getProRegStatus() == 1) {
						proRegStatus = "绑定中";
					} else if (customerProductVo.getProRegStatus() == 2) {
						proRegStatus = "已解绑";
					} else {
						proRegStatus = "未绑定";
					}

					ws.addCell(new Label(
							0,
							j + 1,
							"" + customerProductVo.getAutelId() != null ? customerProductVo
									.getAutelId() : ""));

					ws.addCell(new Label(
							1,
							j + 1,
							"" + customerProductVo.getName() != null ? customerProductVo
									.getName() : ""));

					ws.addCell(new Label(
							2,
							j + 1,
							"" + customerProductVo.getProSerialNo() != null ? customerProductVo
									.getProSerialNo() : ""));

					ws.addCell(new Label(
							3,
							j + 1,
							"" + customerProductVo.getAreaCfgName() != null ? customerProductVo
									.getAreaCfgName() : ""));

					ws.addCell(new Label(
							4,
							j + 1,
							"" + customerProductVo.getSealerAutelId() != null ? customerProductVo
									.getSealerAutelId() : ""));

					ws.addCell(new Label(
							5,
							j + 1,
							"" + customerProductVo.getProDate() != null ? customerProductVo
									.getProDate() : ""));

					ws.addCell(new Label(
							6,
							j + 1,
							"" + customerProductVo.getProRegTime() != null ? customerProductVo
									.getProRegTime() : ""));

					ws.addCell(new Label(7, j + 1, "" + proRegStatus));
				}
			}

			wwb.write();

			wwb.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	@Override
	public InputStream getProUpgradeRecordExcelInputStream(
			List<ProUpgradeRecordVo> proUpgradeRecordVos) {
		ProUpgradeRecordVo proUpgradeRecordVo;
		try {

			wwb = Workbook.createWorkbook(out);

			WritableSheet ws = wwb.createSheet("Sheet1", 0);

			// WritableFont font = new WritableFont(WritableFont.ARIAL,20,WritableFont.BOLD,false,UnderlineStyle.NO_UNDERLINE,Colour.RED);
			WritableCellFormat firstFormat = new WritableCellFormat();

			// 设置背景色
			firstFormat.setBackground(Colour.YELLOW);

			ws.addCell(new Label(0, 0, "用户ID", firstFormat));
			ws.addCell(new Label(1, 0, "用户名", firstFormat));
			ws.addCell(new Label(2, 0, "产品序列号", firstFormat));
			ws.addCell(new Label(3, 0, "功能软件", firstFormat));
			ws.addCell(new Label(4, 0, "升级时间", firstFormat));

			// 设置显示长度.
			ws.setColumnView(0, 25);
			ws.setColumnView(1, 15);
			ws.setColumnView(2, 15);
			ws.setColumnView(3, 15);
			ws.setColumnView(4, 20);

			for (int j = 0; j < proUpgradeRecordVos.size(); j++) {
				proUpgradeRecordVo = proUpgradeRecordVos.get(j);

				if (proUpgradeRecordVo != null) {

					ws.addCell(new Label(
							0,
							j + 1,
							"" + proUpgradeRecordVo.getAutelId() != null ? proUpgradeRecordVo
									.getAutelId() : ""));

					ws.addCell(new Label(
							1,
							j + 1,
							"" + proUpgradeRecordVo.getCustomerName() != null ? proUpgradeRecordVo
									.getCustomerName() : ""));

					ws.addCell(new Label(
							2,
							j + 1,
							"" + proUpgradeRecordVo.getProSerialNo() != null ? proUpgradeRecordVo
									.getProSerialNo() : ""));

					ws.addCell(new Label(
							3,
							j + 1,
							"" + proUpgradeRecordVo.getSoftwareTypeName() != null ? proUpgradeRecordVo
									.getSoftwareTypeName() : ""));

					ws.addCell(new Label(
							4,
							j + 1,
							"" + proUpgradeRecordVo.getUpgradeTime() != null ? proUpgradeRecordVo
									.getUpgradeTime() : ""));
				}
			}

			wwb.write();

			wwb.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	@Override
	public InputStream getCustomerLoginInfoExcelInputStream(
			List<CustomerLoginInfoVo> customerLoginInfoVos) {
		CustomerLoginInfoVo customerLoginInfoVo;
		try {

			wwb = Workbook.createWorkbook(out);

			WritableSheet ws = wwb.createSheet("Sheet1", 0);

			// WritableFont font = new WritableFont(WritableFont.ARIAL,20,WritableFont.BOLD,false,UnderlineStyle.NO_UNDERLINE,Colour.RED);
			WritableCellFormat firstFormat = new WritableCellFormat();

			// 设置背景色
			firstFormat.setBackground(Colour.YELLOW);

			ws.addCell(new Label(0, 0, "用户ID", firstFormat));
			ws.addCell(new Label(1, 0, "用户名", firstFormat));
			ws.addCell(new Label(2, 0, "登录IP", firstFormat));
			ws.addCell(new Label(3, 0, "登录时间", firstFormat));

			// 设置显示长度.
			ws.setColumnView(0, 25);
			ws.setColumnView(1, 15);
			ws.setColumnView(2, 15);
			ws.setColumnView(3, 20);

			for (int j = 0; j < customerLoginInfoVos.size(); j++) {
				customerLoginInfoVo = customerLoginInfoVos.get(j);

				if (customerLoginInfoVo != null) {

					ws.addCell(new Label(
							0,
							j + 1,
							"" + customerLoginInfoVo.getCustomerAutelId() != null ? customerLoginInfoVo
									.getCustomerAutelId() : ""));

					ws.addCell(new Label(
							1,
							j + 1,
							"" + customerLoginInfoVo.getCustomerName() != null ? customerLoginInfoVo
									.getCustomerName() : ""));

					ws.addCell(new Label(
							2,
							j + 1,
							"" + customerLoginInfoVo.getLoginIP() != null ? customerLoginInfoVo
									.getLoginIP() : ""));

					ws.addCell(new Label(
							3,
							j + 1,
							"" + customerLoginInfoVo.getLoginTime() != null ? customerLoginInfoVo
									.getLoginTime() : ""));
				}
			}

			wwb.write();

			wwb.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	@Override
	public InputStream getCustomerSoftwareExcelInputStream(
			List<CustomerSoftwareVo> customerSoftwareVos) {
		CustomerSoftwareVo customerSoftwareVo;
		try {

			wwb = Workbook.createWorkbook(out);

			WritableSheet ws = wwb.createSheet("Sheet1", 0);

			// WritableFont font = new WritableFont(WritableFont.ARIAL,20,WritableFont.BOLD,false,UnderlineStyle.NO_UNDERLINE,Colour.RED);
			WritableCellFormat firstFormat = new WritableCellFormat();

			// 设置背景色
			firstFormat.setBackground(Colour.YELLOW);

			ws.addCell(new Label(0, 0, "用户ID", firstFormat));
			ws.addCell(new Label(1, 0, "用户名", firstFormat));
			ws.addCell(new Label(2, 0, "最小销售单位名称", firstFormat));
			ws.addCell(new Label(3, 0, "产品序列号", firstFormat));
			ws.addCell(new Label(4, 0, "区域配置", firstFormat));
			ws.addCell(new Label(5, 0, "经销商ID", firstFormat));
			ws.addCell(new Label(6, 0, "到期时间", firstFormat));

			// 设置显示长度.
			ws.setColumnView(0, 25);
			ws.setColumnView(1, 15);
			ws.setColumnView(2, 15);
			ws.setColumnView(3, 15);
			ws.setColumnView(4, 20);
			ws.setColumnView(5, 20);
			ws.setColumnView(6, 20);

			for (int j = 0; j < customerSoftwareVos.size(); j++) {
				customerSoftwareVo = customerSoftwareVos.get(j);

				if (customerSoftwareVo != null) {

					ws.addCell(new Label(
							0,
							j + 1,
							"" + customerSoftwareVo.getCustomerAutelId() != null ? customerSoftwareVo
									.getCustomerAutelId() : ""));

					ws.addCell(new Label(
							1,
							j + 1,
							"" + customerSoftwareVo.getCustomerName() != null ? customerSoftwareVo
									.getCustomerName() : ""));

					ws.addCell(new Label(
							2,
							j + 1,
							"" + customerSoftwareVo.getMinSaleUnitName() != null ? customerSoftwareVo
									.getMinSaleUnitName() : ""));

					ws.addCell(new Label(
							3,
							j + 1,
							"" + customerSoftwareVo.getProSerialNo() != null ? customerSoftwareVo
									.getProSerialNo() : ""));

					ws.addCell(new Label(
							4,
							j + 1,
							"" + customerSoftwareVo.getAreaCfgName() != null ? customerSoftwareVo
									.getAreaCfgName() : ""));

					ws.addCell(new Label(
							5,
							j + 1,
							"" + customerSoftwareVo.getSealerAutelId() != null ? customerSoftwareVo
									.getSealerAutelId() : ""));

					ws.addCell(new Label(
							6,
							j + 1,
							"" + customerSoftwareVo.getProSoftValidDate() != null ? customerSoftwareVo
									.getProSoftValidDate() : ""));
				}
			}

			wwb.write();

			wwb.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	@Override
	public InputStream getOrderInfoExcelInputStream(
			List<OrderInfoVo> orderInfoVos) {
		OrderInfoVo orderInfoVo;
		String orderState = "";
		String orderPayState = "";
		String recConfirmState = "";
		String processState = "";

		try {

			wwb = Workbook.createWorkbook(out);

			WritableSheet ws = wwb.createSheet("Sheet1", 0);

			// WritableFont font = new WritableFont(WritableFont.ARIAL,20,WritableFont.BOLD,false,UnderlineStyle.NO_UNDERLINE,Colour.RED);
			WritableCellFormat firstFormat = new WritableCellFormat();

			// 设置背景色
			firstFormat.setBackground(Colour.YELLOW);

			ws.addCell(new Label(0, 0, "订单号", firstFormat));
			ws.addCell(new Label(1, 0, "付款会员", firstFormat));
			ws.addCell(new Label(2, 0, "产品型号", firstFormat));
			ws.addCell(new Label(3, 0, "产品序列号", firstFormat));
			ws.addCell(new Label(4, 0, "下单时间", firstFormat));
			ws.addCell(new Label(5, 0, "交易金额", firstFormat));
			ws.addCell(new Label(6, 0, "订单状态", firstFormat));
			ws.addCell(new Label(7, 0, "支付状态", firstFormat));
			ws.addCell(new Label(8, 0, "支付时间", firstFormat));
			ws.addCell(new Label(9, 0, "paypal流水号", firstFormat));
			/*
			 * ws.addCell(new Label(10, 0, "收款确认状态",firstFormat));
			 * ws.addCell(new Label(11, 0, "收款确认时间",firstFormat));
			 * ws.addCell(new Label(12, 0, "处理状态",firstFormat)); ws.addCell(new
			 * Label(13, 0, "处理时间",firstFormat));
			 */

			// 设置显示长度.
			ws.setColumnView(0, 25);
			ws.setColumnView(1, 20);
			ws.setColumnView(2, 15);
			ws.setColumnView(3, 15);
			ws.setColumnView(4, 20);
			ws.setColumnView(5, 10);
			ws.setColumnView(6, 10);
			ws.setColumnView(7, 10);
			ws.setColumnView(8, 20);
			ws.setColumnView(9, 15);
			/*
			 * ws.setColumnView(10, 10); ws.setColumnView(11, 20);
			 * ws.setColumnView(12, 10); ws.setColumnView(13, 20);
			 */

			for (int j = 0; j < orderInfoVos.size(); j++) {
				orderInfoVo = orderInfoVos.get(j);

				if (orderInfoVo != null) {
					if (orderInfoVo.getOrderState() == null) {
						orderState = "";
					} else if (orderInfoVo.getOrderState() == 0) {
						orderState = "已取消";
					} else if (orderInfoVo.getOrderState() == 1) {
						orderState = "有效";
					}

					if (orderInfoVo.getOrderPayState() == null) {
						orderPayState = "";
					} else if (orderInfoVo.getOrderPayState() == 0) {
						orderPayState = "未支付";
					} else if (orderInfoVo.getOrderPayState() == 1) {
						orderPayState = "已支付";
					}

					if (orderInfoVo.getRecConfirmState() == null) {
						recConfirmState = "";
					} else if (orderInfoVo.getRecConfirmState() == 0) {
						recConfirmState = "未确认";
					} else if (orderInfoVo.getRecConfirmState() == 1) {
						recConfirmState = "已确认";
					}

					if (orderInfoVo.getProcessState() == null) {
						processState = "";
					} else if (orderInfoVo.getProcessState() == 0) {
						processState = "未处理";
					} else if (orderInfoVo.getProcessState() == 1) {
						processState = "已处理";
					}

					ws.addCell(new Label(0, j + 1,
							"" + orderInfoVo.getCode() != null ? orderInfoVo
									.getCode() : ""));

					ws.addCell(new Label(1, j + 1, ""
							+ orderInfoVo.getAutelId() != null ? orderInfoVo
							.getAutelId() : ""));

					ws.addCell(new Label(
							2,
							j + 1,
							"" + orderInfoVo.getProTypeName() != null ? orderInfoVo
									.getProTypeName() : ""));

					ws.addCell(new Label(
							3,
							j + 1,
							"" + orderInfoVo.getProductSerialNo() != null ? orderInfoVo
									.getProductSerialNo() : ""));

					ws.addCell(new Label(4, j + 1, ""
							+ orderInfoVo.getOrderDate() != null ? orderInfoVo
							.getOrderDate() : ""));

					ws.addCell(new Label(5, j + 1, ""
							+ orderInfoVo.getMinSaleUnitPrice() != null ? orderInfoVo
							.getMinSaleUnitPrice() : ""));

					ws.addCell(new Label(6, j + 1, "" + orderState));

					ws.addCell(new Label(7, j + 1, "" + orderPayState));

					ws.addCell(new Label(8, j + 1, ""
							+ orderInfoVo.getPayDate() != null ? orderInfoVo
							.getPayDate() : ""));

					ws.addCell(new Label(9, j + 1, ""
							+ orderInfoVo.getPayNumber() != null ? orderInfoVo
							.getPayNumber() : ""));

					/*
					 * ws.addCell(new Label(10, j+1, "" + recConfirmState));
					 * 
					 * ws.addCell(new Label(11, j+1, "" +
					 * orderInfoVo.getRecConfirmDate() != null ?
					 * orderInfoVo.getRecConfirmDate():""));
					 * 
					 * ws.addCell(new Label(12, j+1, "" + processState));
					 * 
					 * ws.addCell(new Label(13, j+1, "" +
					 * orderInfoVo.getProcessDate() != null ?
					 * orderInfoVo.getProcessDate():""));
					 */
				}
			}

			wwb.write();

			wwb.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	@Override
	public InputStream getComplaintStatExcelInputStream(List<Map> lMaps) {
		Map map;
		try {

			wwb = Workbook.createWorkbook(out);

			WritableSheet ws = wwb.createSheet("Sheet1", 0);

			// WritableFont font = new WritableFont(WritableFont.ARIAL,20,WritableFont.BOLD,false,UnderlineStyle.NO_UNDERLINE,Colour.RED);
			WritableCellFormat firstFormat = new WritableCellFormat();

			// 设置背景色
			firstFormat.setBackground(Colour.YELLOW);

			ws.addCell(new Label(0, 0, "客服ID", firstFormat));
			ws.addCell(new Label(1, 0, "五颗星", firstFormat));
			ws.addCell(new Label(2, 0, "四颗星", firstFormat));
			ws.addCell(new Label(3, 0, "三颗星", firstFormat));
			ws.addCell(new Label(4, 0, "二颗星", firstFormat));
			ws.addCell(new Label(5, 0, "一颗星", firstFormat));
			ws.addCell(new Label(6, 0, "默认", firstFormat));
			ws.addCell(new Label(7, 0, "总数", firstFormat));

			// 设置显示长度.
			ws.setColumnView(0, 20);
			ws.setColumnView(1, 15);
			ws.setColumnView(2, 15);
			ws.setColumnView(3, 15);
			ws.setColumnView(4, 15);
			ws.setColumnView(5, 15);
			ws.setColumnView(6, 15);
			ws.setColumnView(7, 15);

			for (int j = 0; j < lMaps.size(); j++) {
				map = lMaps.get(j);

				if (map != null) {

					ws.addCell(new Label(0, j + 1,
							"" + map.get("rePerson") != null ? map.get(
									"rePerson").toString() : ""));

					ws.addCell(new Label(1, j + 1,
							"" + map.get("fivePoint") != null ? map.get(
									"fivePoint").toString() : ""));

					ws.addCell(new Label(2, j + 1,
							"" + map.get("fourPoint") != null ? map.get(
									"fourPoint").toString() : ""));

					ws.addCell(new Label(3, j + 1,
							"" + map.get("threePoint") != null ? map.get(
									"threePoint").toString() : ""));

					ws.addCell(new Label(4, j + 1,
							"" + map.get("twoPoint") != null ? map.get(
									"twoPoint").toString() : ""));

					ws.addCell(new Label(5, j + 1,
							"" + map.get("onePoint") != null ? map.get(
									"onePoint").toString() : ""));

					ws.addCell(new Label(6, j + 1,
							"" + map.get("defaultPoint") != null ? map.get(
									"defaultPoint").toString() : ""));

					ws.addCell(new Label(7, j + 1,
							"" + map.get("total") != null ? map.get("total")
									.toString() : ""));
				}
			}

			wwb.write();

			wwb.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return new ByteArrayInputStream(out.toByteArray());
	}

}