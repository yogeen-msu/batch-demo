package com.cheriscon.backstage.member.service.impl;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.cheriscon.backstage.member.service.ITestPackLanguagePackService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.TestPackLanguagePack;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @Description: 测试包语言包业务类
 * @author pengdongan
 * @date 2013-01-22
 * 
 */
@Service
public class TestPackLanguagePackServiceImpl extends BaseSupport<TestPackLanguagePack> implements ITestPackLanguagePackService {

	private final static String TABLE_NAME = "DT_TestPackLanguagePack";
	
	/**
	  * <p>Title: listAll</p>
	  * <p>Description: 查询测试包语言包的集合</p>
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<TestPackLanguagePack> listAll() throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME);
		return this.daoSupport.queryForList(sql.toString(), TestPackLanguagePack.class);
	}

	/**
	  * <p>Title: addTestPackLanguagePack</p>
	  * <p>Description: 添加测试包语言包</p>
	  * @param testPackLanguagePack
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public int addTestPackLanguagePack(TestPackLanguagePack testPackLanguagePack) throws Exception {
		String code = DBUtils.generateCode(DBConstant.TEST_PACK_LANGUAGE_PACK);
		testPackLanguagePack.setCode(code);
		this.daoSupport.insert(TABLE_NAME, testPackLanguagePack);
		return 0;
	}

	
	@Override
	@Transactional
	public int updateTestPackLanguagePack(TestPackLanguagePack testPackLanguagePack) throws Exception {
		
		this.daoSupport.update(TABLE_NAME, testPackLanguagePack, "code='"+testPackLanguagePack.getCode()+"'");
		return 0;
	}
	/**
	  * <p>Title: delTestPackLanguagePack</p>
	  * <p>Description: 删除测试包语言包</p>
	  * @param code
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public int delTestPackLanguagePack(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code = ?");
		this.daoSupport.execute(sql.toString(),code);
		return 0;
	}

	/**
	  * <p>Title: delTestPackLanguagePackByTestPackCode</p>
	  * <p>Description: </p>
	  * @param testPackCode
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public int delTestPackLanguagePackByTestPackCode(String testPackCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where testPackCode = ?");
		this.daoSupport.execute(sql.toString(),testPackCode);
		return 0;
	}

	/**
	 * 
	* @Title: listLanguagePackByTestPackCode
	* @Description: 根据测试包编码查询测试包语言包集合
	* @param    
	* @return List<TestPackLanguagePack>    
	* @throws
	 */
	@Override
	public List<TestPackLanguagePack> listLanguagePackByTestPackCode(String testPackCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select a.*,b.name languageTypeName")
			.append(" from DT_TestPackLanguagePack a")
			.append(" left join DT_Language b on a.languageTypeCode=b.code")
			.append(" where a.testPackCode = ?");
		return this.daoSupport.queryForList(sql.toString(), TestPackLanguagePack.class, testPackCode);
	}

}
