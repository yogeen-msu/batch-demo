package com.cheriscon.backstage.member.service.impl;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IComplaintAdminuserInfoService;

import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ComplaintAdminuserInfo;
import com.cheriscon.common.model.ComplaintAdminuserInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.DBUtils;

import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;


/**
 * 客诉权限实现类
 * @author pengdongan
 * @date 2013-01-18
 */
@Service
public class ComplaintAdminuserInfoServiceImpl extends BaseSupport<ComplaintAdminuserInfo> implements IComplaintAdminuserInfoService
{

	/**
	  * 客诉权限分页显示方法
	  * @param ComplaintAdminuserInfo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageComplaintAdminuserInfoPage(ComplaintAdminuserInfo complaintAdminuserInfo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.id,a.code complaintTypeCode,c.adminUserid,b.name complaintTypeName,s.username emailName");
		sql.append(" from DT_CustomerComplaintType a");
		sql.append(" left join DT_CustomerComplaintTypeName b on a.code=b.complaintTypeCode and b.languageCode=?");
		sql.append(" left join DT_ComplaintAdminuserInfo c on a.code=c.complaintTypeCode");
		sql.append(" left join es_smtp s on s.id=c.emailid");
		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, ComplaintAdminuserInfo.class,complaintAdminuserInfo.getLanguageCode());
		return page;
	}

	/**
	 * 分配客服
	 */
	@Transactional
	public int allotAcceptor(String code,String acceptor) throws Exception
	{
		String sql = "update DT_ComplaintAdminuserInfo set adminUserid=? where complaintType= ?";
		this.daoSupport.execute(sql,acceptor,code);
		return 0;
		
	}
	
	/**
	 * 添加信息
	 * @param complaintAdminuserInfo
	 * 经销商信息
	 * @throws Exception
	 */
	@Transactional
	public int insertComplaintAdminuserInfo(ComplaintAdminuserInfo complaintAdminuserInfo) throws Exception {
		this.daoSupport.insert("DT_ComplaintAdminuserInfo", complaintAdminuserInfo);
		return 0;
	}
	
	/**
	 * 更新信息
	 * @param complaintAdminuserInfo
	 * 经销商信息
	 * @throws Exception
	 */
	@Transactional
	public int updateComplaintAdminuserInfo(ComplaintAdminuserInfo complaintAdminuserInfo) throws Exception {
		this.daoSupport.update("DT_ComplaintAdminuserInfo", complaintAdminuserInfo, "complaintTypeCode='"+complaintAdminuserInfo.getComplaintTypeCode()+"'");
		return 0;
	}

	public ComplaintAdminuserInfo getComplaintAdminuserInfoBycomplaintTypeCode(String complaintTypeCode) throws Exception {
		String sql = "select * from DT_ComplaintAdminuserInfo where complaintTypeCode = ?";
		
		return (ComplaintAdminuserInfo) this.daoSupport.queryForObject(sql, ComplaintAdminuserInfo.class, complaintTypeCode);
	}

	public ComplaintAdminuserInfo getComplaintAdminuserInfoBySealer(String sealer) throws Exception{
        
		String sql = "select * from DT_ComplaintAdminuserInfo where adminUserid = ?";
		List<ComplaintAdminuserInfo> list=this.daoSupport.queryForList(sql,ComplaintAdminuserInfo.class,sealer);
		if(list==null || list.size()==0){
			return null;
		}else{
			return list.get(0);
		}
	}
	
}
