package com.cheriscon.backstage.member.service.impl;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IOnlineAdminuserInfoService;

import com.cheriscon.common.model.OnlineAdminuserInfo;

import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;


/**
 * 在线客服权限实现类
 * @author pengdongan
 * @date 2013-06-05
 */
@Service
public class OnlineAdminuserInfoServiceImpl extends BaseSupport<OnlineAdminuserInfo> implements IOnlineAdminuserInfoService
{

	/**
	  * 在线客服权限分页显示方法
	  * @param OnlineAdminuserInfo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageOnlineAdminuserInfoPage(OnlineAdminuserInfo onlineAdminuserInfo, int pageNo,
			int pageSize) throws Exception {
		
		StringBuffer conditionSql = new StringBuffer();

		conditionSql.append("select a.id,a.code onlineTypeCode,IFNULL(c.adminUserid,'') adminUserid,IFNULL(b.name,'') onlineTypeName");
		conditionSql.append(" from DT_OnlineServiceType a");
		conditionSql.append(" left join DT_OnlineServiceTypeName b on a.code=b.onlineTypeCode and b.languageCode=?");
		conditionSql.append(" left join DT_OnlineAdminuserInfo c on a.code=c.onlineTypeCode");
		
		StringBuffer sql = new StringBuffer();

		sql.append("select * from (");
		sql.append("select onlineTypeCode,onlineTypeName,td.adminUserids adminUserids from ("+conditionSql+") AS ta ,");
		sql.append(" (SELECT group_concat(adminUserid) adminUserids FROM ("+conditionSql+") AS tb group by onlineTypeCode) td ");
		sql.append(" GROUP BY onlineTypeCode,onlineTypeName,adminUserids");
		sql.append(") tc order by tc.onlineTypeCode desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, OnlineAdminuserInfo.class,onlineAdminuserInfo.getLanguageCode(),onlineAdminuserInfo.getLanguageCode());
		return page;
	}

	/**
	 * 分配客服
	 */
	@Transactional
	public int allotAcceptor(String code,String acceptor) throws Exception
	{
		String sql = "update DT_OnlineAdminuserInfo set adminUserid=? where onlineTypeCode= ?";
		this.daoSupport.execute(sql,acceptor,code);
		return 0;
		
	}

	/**
	 * 清除相关记录
	 */
	@Transactional
	public int delOnlineAdminuserInfoByOnlineTypeCode(String code) throws Exception
	{
		String sql = "delete from DT_OnlineAdminuserInfo where onlineTypeCode= ?";
		this.daoSupport.execute(sql,code);
		return 0;
		
	}
	
	/**
	 * 添加信息
	 * @param onlineAdminuserInfo
	 * 经销商信息
	 * @throws Exception
	 */
	@Transactional
	public int insertOnlineAdminuserInfo(OnlineAdminuserInfo onlineAdminuserInfo) throws Exception {
		this.daoSupport.insert("DT_OnlineAdminuserInfo", onlineAdminuserInfo);
		return 0;
	}
	
	/**
	 * 更新信息
	 * @param onlineAdminuserInfo
	 * 经销商信息
	 * @throws Exception
	 */
	@Transactional
	public int updateOnlineAdminuserInfo(OnlineAdminuserInfo onlineAdminuserInfo) throws Exception {
		this.daoSupport.update("DT_OnlineAdminuserInfo", onlineAdminuserInfo, "onlineTypeCode='"+onlineAdminuserInfo.getOnlineTypeCode()+"'");
		return 0;
	}

	public OnlineAdminuserInfo getOnlineAdminuserInfoByonlineTypeCode(String onlineTypeCode) throws Exception {
		String sql = "select * from DT_OnlineAdminuserInfo where onlineTypeCode = ?";
		
		return (OnlineAdminuserInfo) this.daoSupport.queryForObject(sql, OnlineAdminuserInfo.class, onlineTypeCode);
	}

}
