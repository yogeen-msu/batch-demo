package com.cheriscon.backstage.member.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeLogService;
import com.cheriscon.common.model.ProductSoftwareValidChangeLog;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class ProductSoftwareValidChangeLogServiceImpl extends
		BaseSupport<ProductSoftwareValidChangeLog> implements
		IProductSoftwareValidChangeLogService {

	@Override
	public boolean saveProductSoftwareValidStatus(
			ProductSoftwareValidChangeLog log) {
		this.daoSupport.insert("DT_ProductSoftwareValidChangeLog", log);
		return true;
	}

	@Override
	public Page pageProductSoftwareValidStatus(
			ProductSoftwareValidChangeLog log, int pageNo, int pageSize) {

		StringBuffer sql = new StringBuffer(
				"select distinct productSN,oldDate,newDate,operatorUser,SUBSTRING(operatorTime,1,10) as operatorTime,operatorIp,operType "
				+ " from dt_productsoftwarevalidchangelog where 1=1 ");
		if (log != null && StringUtils.isNotBlank(log.getProductSN())) {
			sql.append(" and ProductSN like '%" + log.getProductSN() + "%' ");
		}
		sql.append("order by id desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}

}
