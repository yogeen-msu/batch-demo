package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ICustomerLoginInfoService;
import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.sun.org.apache.xpath.internal.operations.And;

/**
 * @remark 客户登录信息service实现
 * @author pengdongan
 * @date 2013-01-07
 * 
 */
@Service
public class CustomerLoginInfoServiceImpl extends BaseSupport implements ICustomerLoginInfoService {


	
	@Transactional
	public List<CustomerLoginInfoVo> listSealer(CustomerLoginInfoVo customerLoginInfoVo)
	throws Exception {
		StringBuffer sql = new StringBuffer();



		sql.append("select distinct a.id, case userType when 1 then b.autelId when 2 then e.autelId else '' end customerAutelId," +
				"case userType when 1 then b.name when 2 then e.name else '' end customerName," +
				"a.loginIP,a.loginTime");
		sql.append(" from DT_UserLoginInfo a");
		sql.append(" left join DT_CustomerInfo b on a.userCode=b.code and a.userType=1");
		sql.append(" left join DT_CustomerProInfo c on b.code=c.customerCode");
		sql.append(" left join DT_ProductInfo d on c.proCode=d.code");
		sql.append(" left join DT_SealerInfo e on a.userCode=e.code and a.userType=2");
		sql.append(" where 1=1");
		
		if (null != customerLoginInfoVo) {
			if (StringUtils.isNotBlank(customerLoginInfoVo.getCustomerAutelId())) {
				sql.append(" and ((");
				sql.append("a.userType=1 and b.autelId like '%");
				sql.append(customerLoginInfoVo.getCustomerAutelId().trim());
				sql.append("%'");
				sql.append(") or (");
				sql.append("a.userType=2 and e.autelId like '%");
				sql.append(customerLoginInfoVo.getCustomerAutelId().trim());
				sql.append("%'");
				sql.append("))");
			}
			if (StringUtils.isNotBlank(customerLoginInfoVo.getProSerialNo())) {
				sql.append(" and d.serialNo like '%");
				sql.append(customerLoginInfoVo.getProSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerLoginInfoVo.getBeginDate())) {
				sql.append(" and a.loginTime >= '");
				sql.append(customerLoginInfoVo.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(customerLoginInfoVo.getEndDate())) {
				sql.append(" and a.loginTime < cast('");
				sql.append(customerLoginInfoVo.getEndDate().trim());
				sql.append("' as datetime)+1");
			}
		}

		sql.append(" order by a.id desc");
		
		return (List<CustomerLoginInfoVo>) this.daoSupport.queryForList(sql.toString(),CustomerLoginInfoVo.class);
	}
	
	/**
	 * 根据编码查询客户登录信息对象
	 * @param code	客户登录信息code
	 * @return
	 * @throws Exception
	 */

	public CustomerLoginInfoVo getSealerByCode(String code) throws Exception {
		String sql = "select * from DT_UserLoginInfo where code = ?";
		
		return (CustomerLoginInfoVo) this.daoSupport.queryForObject(sql, CustomerLoginInfoVo.class, code);
	}

	
	/**
	 * 清空客户登录信息对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	
	@Transactional
	public int clearCustomerLoginInfo() throws Exception {
		String sql = "delete from DT_UserLoginInfo";
		this.daoSupport.execute(sql);
		return 0;
	}


	/**
	  * 客户登录信息分页显示方法
	  * @param CustomerLoginInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerLoginInfoVoPage(CustomerLoginInfoVo customerLoginInfoVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		
//		sql.append("select * from (");

		sql.append("select distinct a.id, case userType when 1 then b.autelId when 2 then e.autelId else '' end customerAutelId," +
				"case userType when 1 then b.name when 2 then e.name else '' end customerName," +
				"a.loginIP,a.loginTime");
		sql.append(" from DT_UserLoginInfo a");
		sql.append(" left join DT_CustomerInfo b on a.userCode=b.code and a.userType=1");
		sql.append(" left join DT_CustomerProInfo c on b.code=c.customerCode");
		sql.append(" left join DT_ProductInfo d on c.proCode=d.code");
		sql.append(" left join DT_SealerInfo e on a.userCode=e.code and a.userType=2");
		sql.append(" where 1=1");
		
		if (null != customerLoginInfoVo) {
			
			if (StringUtils.isNotBlank(customerLoginInfoVo.getCode())) {
				sql.append(" and a.userCode = '");
				sql.append(customerLoginInfoVo.getCode().trim());
				sql.append("'");
			}
			
			if (StringUtils.isNotBlank(customerLoginInfoVo.getCustomerAutelId())) {
				sql.append(" and ((");
				sql.append("a.userType=1 and b.autelId like '%");
				sql.append(customerLoginInfoVo.getCustomerAutelId().trim());
				sql.append("%'");
				sql.append(") or (");
				sql.append("a.userType=2 and e.autelId like '%");
				sql.append(customerLoginInfoVo.getCustomerAutelId().trim());
				sql.append("%'");
				sql.append("))");
			}
			if (StringUtils.isNotBlank(customerLoginInfoVo.getProSerialNo())) {
				sql.append(" and d.serialNo like '%");
				sql.append(customerLoginInfoVo.getProSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(customerLoginInfoVo.getBeginDate())) {
				sql.append(" and a.loginTime >= '");
				sql.append(customerLoginInfoVo.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(customerLoginInfoVo.getEndDate())) {
				sql.append(" and a.loginTime < cast('");
				sql.append(customerLoginInfoVo.getEndDate().trim());
				sql.append("' as datetime)+1");
			}
		}
		
//		sql.append(") m");
//
//		sql.append(" order by m.id desc");
		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, CustomerLoginInfoVo.class);
		return page;
	}

}
