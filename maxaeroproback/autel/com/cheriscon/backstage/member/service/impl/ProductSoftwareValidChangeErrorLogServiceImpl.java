package com.cheriscon.backstage.member.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeErrorLogService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidChangeErrorLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;

@Service
public class ProductSoftwareValidChangeErrorLogServiceImpl extends
		BaseSupport<ProductSoftwareValidChangeErrorLog> implements IProductSoftwareValidChangeErrorLogService {
	
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IProductSoftwareValidStatusService validService;
	@Resource
	private ISaleConfigService saleConfigService;
	
	@Resource
	private IAdminUserManager adminUserManager;

	@Override
	@Transactional
	public void saveErrorLog(ProductSoftwareValidChangeErrorLog log) {
		this.daoSupport.insert("dt_ProductSoftwareValidChangeErrorLog", log);

	}

	@Override
	public Page pageErrorLog(ProductSoftwareValidChangeErrorLog log,
			int pageNo, int pageSize) {
		StringBuffer sql = new StringBuffer(
				"select l.* ,p.serialNo as productSN"
				).append( " from dt_ProductSoftwareValidChangeErrorLog l left join dt_productInfo p on l.proCode=p.code " 
				).append("   where 1=1 ");
		if (log != null && StringUtils.isNotBlank(log.getProductSN())) {
			sql.append(" and p.serialNo like '%" + log.getProductSN() + "%' ");
		}
		if (log != null && StringUtils.isNotBlank(log.getIsUpdate())) {
			sql.append(" and l.isUpdate = '" + log.getIsUpdate() + "' ");
		}
		sql.append("order by id desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}

	@Override
	@Transactional
	public boolean updateSoftwareValid(String proCode) {
		ProductInfo productInfo = productInfoService.getByProCode(proCode);
		if(productInfo == null){
			return false;
		}
		try {
			SaleContract contract = saleContractService
					.getSaleContractByCode(productInfo.getSaleContractCode());
			if(contract == null){
				return false;
			}
			if(productInfo.getRegStatus() != FrontConstant.PRODUCT_REGSTATUS_YES){
				String upsql = "update dt_ProductSoftwareValidChangeErrorLog set isUpdate='1' where proCode='"+proCode+"'";
				this.daoSupport.execute(upsql);
				return true;
			}else{
				List<ProductSoftwareValidStatus> list = validService
						.getProductSoftwareValidStatusList(productInfo.getCode());
				List<MinSaleUnit> listUnit = saleConfigService
						.queryMinSaleUnits(contract.getSaleCfgCode());
				if(listUnit == null ||listUnit.size()==0){
					return false;
				}
				
				String validDate = null;
//				删除有效期
				for (int j = 0; j < list.size(); j++) {
					ProductSoftwareValidStatus valid = list.get(j);
					validDate = valid.getValidDate();
					// 2.删除有效期
					validService.delProductSoftwareValidStatusById(valid
							.getId());
				}
//				添加有效期
				for (int k = 0; k < listUnit.size(); k++) {
					MinSaleUnit unit = listUnit.get(k);
					ProductSoftwareValidStatus valid = new ProductSoftwareValidStatus();
					valid.setMinSaleUnitCode(unit.getCode());
					valid.setProCode(productInfo.getCode());
					valid.setValidDate(validDate);
					validService.saveProductSoftwareValidStatus(valid);
				}
				HttpServletRequest request = ThreadContextHolder.getHttpRequest();
				AdminUser user = adminUserManager.getCurrentUser();
				user = adminUserManager.get(user.getUserid());
				String ip = FrontConstant.getIpAddr(request);
				String upsql = "update dt_ProductSoftwareValidChangeErrorLog set isUpdate='1',operatorUser='"+user.getUsername()
						+"',operatorIp='"+ip+"',operatorTime='"+DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss")+"' where isUpdate='0' and proCode='"+proCode+"'";
				this.daoSupport.execute(upsql);
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return false;
	}

}
