package com.cheriscon.backstage.member.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IQuestionDescService;
import com.cheriscon.backstage.member.service.IQuestionInfoDescService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.QuestionInfo;
import com.cheriscon.common.model.QuestionDesc;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 安全问题业务逻辑实现类
 * @author pengdongan
 * @date 2013-01-24
 */
@Service
public class QuestionInfoDescServiceImpl extends BaseSupport<QuestionInfo> implements IQuestionInfoDescService
{

	@Resource
	private IQuestionDescService questionDescService;
	
	/**
	 * 查询所有安全问题对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<QuestionInfo> listAll() throws Exception {
		String sql = "select * from DT_QuestionInfo order by id desc";
		
		return this.daoSupport.queryForList(sql, QuestionInfo.class);
	}
	
	/**
	 * 按语言查询所有安全问题对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<QuestionInfo> listQuestionInfoByLanguageCode(String languageCode) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.questionDesc questionDesc");
		sql.append(" from DT_QuestionInfo a");
		sql.append(" left join DT_QuestionDesc c on a.code=c.questionCode and c.languageCode=?");
		sql.append(" order by a.id desc");
		
		return this.daoSupport.queryForList(sql.toString(), QuestionInfo.class, languageCode);
	}
	
	
	/**
	 * 添加安全问题
	 * @param questionInfo
	 * 安全问题
	 * @throws Exception
	 */
	@Transactional
	public int insertQuestionInfo(QuestionInfo questionInfo) throws Exception {
		questionInfo.setCode(DBUtils.generateCode(DBConstant.QUESTIONINFO_CODE));
		
		QuestionDesc questionDesc =new QuestionDesc();
		questionDesc.setQuestionCode(questionInfo.getCode());
		questionDesc.setLanguageCode(questionInfo.getAdminLanguageCode());
		questionDesc.setQuestionDesc(questionInfo.getQuestionDesc());
		
		this.daoSupport.insert("DT_QuestionInfo", questionInfo);
		questionDescService.insertQuestionDesc(questionDesc);
		
		return 0;
	}
	
	/**
	 * 更新安全问题
	 * @param questionInfo
	 * 安全问题
	 * @throws Exception
	 */
	@Transactional
	public int updateQuestionInfo(QuestionInfo questionInfo) throws Exception {
		this.daoSupport.update("DT_QuestionInfo", questionInfo, "code='"+questionInfo.getCode()+"'");
		return 0;
	}

	public QuestionInfo getQuestionInfoByCode(String code) throws Exception {
		String sql = "select * from DT_QuestionInfo where code = ?";
		
		return (QuestionInfo) this.daoSupport.queryForObject(sql, QuestionInfo.class, code);
	}

	@Transactional
	public int delQuestionInfo(String code) throws Exception {
		String sql = "delete from DT_QuestionInfo where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delQuestionInfoDetail(String code) throws Exception {		
		questionDescService.delQuestionDescByQuestionCode(code);

		this.delQuestionInfo(code);
		return 0;
	}

	@Transactional
	public int delQuestionInfos(String codes) throws Exception {
		String sql = "delete from DT_QuestionInfo where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}



	/**
	  * 安全问题分页显示方法
	  * @param QuestionInfo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageQuestionInfoPage(QuestionInfo questionInfo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.questionDesc questionDesc");
		sql.append(" from DT_QuestionInfo a");
		sql.append(" left join DT_QuestionDesc c on a.code=c.questionCode and c.languageCode=?");
		sql.append(" where 1=1");
		
		if (null != questionInfo) {
			if (StringUtils.isNotBlank(questionInfo.getQuestionDesc())) {
				sql.append(" and c.questionDesc like '%");
				sql.append(questionInfo.getQuestionDesc().trim());
				sql.append("%'");
			}
		}		

		
//		if (StringUtils.isNotBlank(questionInfo.getAdminLanguageCode())) {
//			sql.append(" and c.languageCode = '");
//			sql.append(questionInfo.getAdminLanguageCode().trim());
//			sql.append("'");
//		}
		
		
		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, QuestionInfo.class,questionInfo.getAdminLanguageCode().trim());
		return page;
	}

}
