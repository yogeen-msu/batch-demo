package com.cheriscon.backstage.member.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IOnlineServiceTypeNameService;
import com.cheriscon.backstage.member.service.IOnlineServiceTypeService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.OnlineServiceType;
import com.cheriscon.common.model.OnlineServiceTypeName;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 在线客服类型业务逻辑实现类
 * @author pengdongan
 * @date 2013-06-02
 */
@Service
public class OnlineServiceTypeServiceImpl extends BaseSupport<OnlineServiceType> implements IOnlineServiceTypeService
{

	@Resource
	private IOnlineServiceTypeNameService onlineTypeNameService;
	
	/**
	 * 查询所有在线客服类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<OnlineServiceType> listAll() throws Exception {
		String sql = "select * from DT_OnlineServiceType order by id desc";
		
		return this.daoSupport.queryForList(sql, OnlineServiceType.class);
	}
	
	/**
	 * 按语言查询所有在线客服类型对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<OnlineServiceType> listOnlineServiceTypeByLanguageCode(String languageCode) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.name name");
		sql.append(" from DT_OnlineServiceType a");
		sql.append(" left join DT_OnlineServiceTypeName c on a.code=c.onlineTypeCode and c.languageCode=?");
		sql.append(" order by a.id desc");
		
		return this.daoSupport.queryForList(sql.toString(), OnlineServiceType.class, languageCode);
	}
	
	
	/**
	 * 添加在线客服类型
	 * @param onlineServiceType
	 * 在线客服类型
	 * @throws Exception
	 */
	@Transactional
	public int insertOnlineServiceType(OnlineServiceType onlineServiceType) throws Exception {
		onlineServiceType.setCode(DBUtils.generateCode(DBConstant.ONL_TYPE_CODE));
		
		OnlineServiceTypeName onlineServiceTypeName =new OnlineServiceTypeName();
		onlineServiceTypeName.setOnlineTypeCode(onlineServiceType.getCode());
		onlineServiceTypeName.setLanguageCode(onlineServiceType.getAdminLanguageCode());
		onlineServiceTypeName.setName(onlineServiceType.getName());
		
		this.daoSupport.insert("DT_OnlineServiceType", onlineServiceType);
		onlineTypeNameService.insertOnlineServiceTypeName(onlineServiceTypeName);
		
		return 0;
	}
	
	/**
	 * 更新在线客服类型
	 * @param onlineServiceType
	 * 在线客服类型
	 * @throws Exception
	 */
	@Transactional
	public int updateOnlineServiceType(OnlineServiceType onlineServiceType) throws Exception {
		this.daoSupport.update("DT_OnlineServiceType", onlineServiceType, "code='"+onlineServiceType.getCode()+"'");
		return 0;
	}

	public OnlineServiceType getOnlineServiceTypeByCode(String code) throws Exception {
		String sql = "select * from DT_OnlineServiceType where code = ?";
		
		return (OnlineServiceType) this.daoSupport.queryForObject(sql, OnlineServiceType.class, code);
	}

	@Transactional
	public int delOnlineServiceType(String code) throws Exception {
		String sql = "delete from DT_OnlineServiceType where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delOnlineServiceTypeDetail(String code) throws Exception {		
		onlineTypeNameService.delOnlineServiceTypeNameByTypeCode(code);

		this.delOnlineServiceType(code);
		return 0;
	}

	@Transactional
	public int delOnlineServiceTypes(String codes) throws Exception {
		String sql = "delete from DT_OnlineServiceType where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}



	/**
	  * 在线客服类型分页显示方法
	  * @param OnlineServiceType
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageOnlineServiceTypePage(OnlineServiceType onlineServiceType, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.name name");
		sql.append(" from DT_OnlineServiceType a");
		sql.append(" left join DT_OnlineServiceTypeName c on a.code=c.onlineTypeCode and c.languageCode=?");
		sql.append(" where 1=1");
		
		if (null != onlineServiceType) {
			if (StringUtils.isNotBlank(onlineServiceType.getName())) {
				sql.append(" and c.name like '%");
				sql.append(onlineServiceType.getName().trim());
				sql.append("%'");
			}
		}		

		
//		if (StringUtils.isNotBlank(onlineServiceType.getAdminLanguageCode())) {
//			sql.append(" and c.languageCode = '");
//			sql.append(onlineServiceType.getAdminLanguageCode().trim());
//			sql.append("'");
//		}
		
		
		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, OnlineServiceType.class,onlineServiceType.getAdminLanguageCode().trim());
		return page;
	}

}
