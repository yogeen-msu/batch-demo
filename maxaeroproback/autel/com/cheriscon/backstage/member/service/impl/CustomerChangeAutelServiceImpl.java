package com.cheriscon.backstage.member.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ICustomerChangeAutelService;
import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class CustomerChangeAutelServiceImpl extends BaseSupport<CustomerChangeAutel> implements
		ICustomerChangeAutelService {

	@Override
	public Page getCustomerChangeAutels(
			CustomerChangeAutel customerChangeAutel, int pageNo, int pageSize)
			throws Exception {
		
		StringBuffer sql= new StringBuffer();
		sql.append("select a.*,b.autelId as oldAutelId,b.actState as oldActState "
				+" from dt_customerchangeautel a ,dt_customerinfo b where a.customerCode=b.code ");
		if(customerChangeAutel!=null){
			if(!StringUtil.isEmpty(customerChangeAutel.getOldAutelId())){
				sql.append(" and b.autelId like '%"+customerChangeAutel.getOldAutelId()+"%' ");
			}
			if(!StringUtil.isEmpty(customerChangeAutel.getNewAutelId())){
				sql.append(" and a.newAutelId like '%"+customerChangeAutel.getNewAutelId()+"%' ");
			}
			if(customerChangeAutel.getActState()>-1){
				sql.append(" and a.actstate = "+customerChangeAutel.getActState()+" ");
			}
		}		
		sql.append(" order by a.id ");
		 
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, CustomerChangeAutel.class);
	}

	@Transactional
	public void updateCustomerChangeAutelStatus(CustomerChangeAutel customerChangeAutel)
			throws Exception {
		String sql1="update dt_customerchangeautel set actstate=1 where customerCode =? ";
		String sql2="update dt_customerinfo set actstate=1, autelid=? where code =? " ;
		this.daoSupport.execute(sql1, customerChangeAutel.getCustomerCode());
		this.daoSupport.execute(sql2, customerChangeAutel.getNewAutelId(),customerChangeAutel.getCustomerCode());
	}

}
