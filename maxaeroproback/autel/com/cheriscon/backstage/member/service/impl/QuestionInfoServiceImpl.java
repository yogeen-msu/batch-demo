package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IQuestionInfoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.QuestionInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class QuestionInfoServiceImpl extends BaseSupport<QuestionInfo> implements IQuestionInfoService {
	
	@Transactional
	public boolean saveQuestionInfo(QuestionInfo questionInfo) {
		String tableName = DBUtils.getTableName(QuestionInfo.class);
		questionInfo.setCode(DBUtils.generateCode(DBConstant.QUESTIONINFO_CODE));
		this.daoSupport.insert(tableName, questionInfo);
		return true;
	}

	@Transactional
	public boolean updateQuestionInfo(QuestionInfo questionInfo) {
		String tableName = DBUtils.getTableName(QuestionInfo.class);
		this.daoSupport.update(tableName,questionInfo,"id="+questionInfo.getId());
		return true;
	}

	@Transactional
	public boolean delQuestionInfoById(int id) {
		String tableName = DBUtils.getTableName(QuestionInfo.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	public QuestionInfo getById(int id){
		String tableName = DBUtils.getTableName(QuestionInfo.class);
		String sql = "select * from " + tableName + " where id=?";
		return this.daoSupport.queryForObject(sql, QuestionInfo.class, id);
	}
	
	public List<QuestionInfo> queryQuestionInfo() {
		String tableName = DBUtils.getTableName(QuestionInfo.class);
		String sql = "select lan.* from " + tableName +" lan order by id asc";
		return this.daoSupport.queryForList(sql, QuestionInfo.class, new Object[]{});
	}

	public Page pageQuestionInfo(int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(QuestionInfo.class);
		String sql = "select tb.* from " + tableName +" tb order by id asc";
		Page page = this.daoSupport.queryForPage(sql, pageNo, pageSize,QuestionInfo.class,new Object[]{});
		return page;
	}

	@Override
	public QuestionInfo getByCode(String code) 
	{
		String tableName = DBUtils.getTableName(QuestionInfo.class);
		String sql = "select * from " + tableName + " where code=?";
		return this.daoSupport.queryForObject(sql, QuestionInfo.class, code);
	}

}
