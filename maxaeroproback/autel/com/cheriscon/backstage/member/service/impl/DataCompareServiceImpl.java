package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.IDataCompareService;
import com.cheriscon.common.model.DataCompare;
import com.cheriscon.cop.sdk.database.BaseSupport;

@Service
public class DataCompareServiceImpl extends BaseSupport<DataCompare> implements
		IDataCompareService {

	@Override
	public List<DataCompare> getOldLanguages(String serialNo) throws Exception {

		String sql = " select distinct DT_Language.languageCode as oldLanguageCode from DT_ProductInfo "
				+ " join DT_SaleContract on DT_ProductInfo.saleContractCode = DT_SaleContract.code "
				+ " join DT_LanguageConfig on DT_LanguageConfig.code = DT_SaleContract.languageCfgCode "
				+ " join DT_LanguageConfigDetail on DT_LanguageConfigDetail.languageCfgCode = DT_LanguageConfig.code "
				+ " join DT_Language on DT_Language.code = DT_LanguageConfigDetail.languageCode "
				+ " where DT_ProductInfo.serialNo=? order by DT_Language.languageCode desc ";

		return this.daoSupport.queryForList(sql, DataCompare.class, serialNo);
	}

	@Override
	public List<DataCompare> getNewLanguages(String serialNo) throws Exception {

		String sql = "select distinct language as newLanguageCode from DT_ProductMinSaleSearch where productSn=? order by language desc ";
		return this.daoSupport.queryForList(sql, DataCompare.class, serialNo);
	}

	@Override
	public List<DataCompare> getNewtSoftwares(String serialNo) throws Exception {

		String sql = "select  DT_MinSaleSoftPackSearch.softCode as carId , "
				+ " DT_SoftWareName.softwareName as carName , "
				+ " DT_MinSaleSoftPackSearch.language as languageCode , "
				+ " DT_MinSaleSoftPackSearch.softVersion as versionName , "
				+ " DT_MinSaleSoftPackSearch.baseSoftPackPath as baseDownloadPath , "
				+ " DT_MinSaleSoftPackSearch.languagePackPath as lgDownloadPath  "
				//+ " row_number() over(partition by DT_MinSaleSoftPackSearch.softCode order by softVersion desc) priority "
				+ " from  DT_ProductMinSaleSearch  "
				+ " join  DT_MinSaleSoftPackSearch  on  DT_MinSaleSoftPackSearch.minSaleCode  =  DT_ProductMinSaleSearch.minSaleCode "
				+ " and  DT_ProductMinSaleSearch.language  =  DT_MinSaleSoftPackSearch.language "
				+ " and  DT_MinSaleSoftPackSearch.softType  =  '1'  "
				+ " and  (DT_MinSaleSoftPackSearch.softReleaseState  = '1') " /*
																			 * 若是beta用户则条件为and
																			 * (
																			 * DT_MinSaleSoftPackSearch
																			 * .
																			 * softReleaseState
																			 * =
																			 * '1'
																			 * or
																			 * DT_MinSaleSoftPackSearch
																			 * .
																			 * softReleaseState
																			 * =
																			 * '0'
																			 * )
																			 */
				+ " and ( (DT_MinSaleSoftPackSearch.productSn is null ) OR (DT_MinSaleSoftPackSearch.productSn =  ?))  "
				+ " join  DT_ProductInfo  on  DT_ProductInfo.serialNo  =  DT_ProductMinSaleSearch.productSn  "
				+ " left   join  DT_SoftWareName  on  DT_SoftWareName.softwareCode  =  DT_MinSaleSoftPackSearch.softCode "
				+ " and  DT_SoftWareName.languageCode  = "
				+ " (select DT_Language.code from DT_Language where DT_Language.languageCode = 'en' limit 0,1) "
				+ " where   DT_ProductMinSaleSearch.productSn  = ?  and   DT_ProductMinSaleSearch.validDate  > date_format(NOW(),'%Y-%m-%d') "
				+ " order by  DT_SoftWareName.softwareName ,DT_MinSaleSoftPackSearch.softVersion desc";

		return this.daoSupport.queryForList(sql, DataCompare.class, serialNo,
				serialNo);
	}

	@Override
	public List<DataCompare> getOldSoftwares(String serialNo) throws Exception {

		String sql = "select DT_SoftwareType.code as carId, DT_SoftWareName.softwareName as carName, "
				+ " DT_Language.languageCode, "
				+ " DT_SoftwareVersion.versionName, "
				+ " DT_LanguagePack.downloadPath as lgDownloadPath, "
				+ " DT_SoftwarePack.downloadPath as baseDownloadPath  "
				//+ " row_number() over(partition by DT_SoftwareType.code, DT_LanguagePack.languageTypeCode order by releaseDate desc) priority "
				+ " from DT_LanguagePack "
				+ " join DT_Language on DT_Language.code = DT_LanguagePack.languageTypeCode "
				+ " join DT_SoftwareVersion on DT_LanguagePack.softwareVersionCode =  DT_SoftwareVersion.code "
				+ " join DT_SoftwarePack on DT_SoftwarePack.softwareVersionCode =  DT_SoftwareVersion.code "
				+ " join DT_SoftwareType on  DT_SoftwareType.code = DT_SoftwareVersion.softwareTypeCode "
				+ " join DT_MinSaleUnitSoftwareDetail on DT_MinSaleUnitSoftwareDetail.softwareTypeCode = DT_SoftwareType.code "
				+ " join DT_MinSaleUnit on DT_MinSaleUnit.code = DT_MinSaleUnitSoftwareDetail.minSaleUnitCode "
				+ " left join DT_SoftWareName on DT_SoftWareName.softwareCode = DT_SoftwareType.code "
				+ " and  DT_SoftWareName.languageCode  =  "
				+ " (select DT_Language.code from DT_Language where DT_Language.languageCode = 'en' limit 0,1) "
				+ " join DT_ProductSoftwareValidStatus on DT_ProductSoftwareValidStatus.minSaleUnitCode = DT_MinSaleUnit.code "
				+ " join DT_ProductInfo on DT_ProductInfo.code = DT_ProductSoftwareValidStatus.proCode  "
				+ " join DT_SaleContract on DT_SaleContract.code = DT_ProductInfo.saleContractCode  "
				+ " join DT_LanguageConfig on DT_LanguageConfig.code = DT_SaleContract.languageCfgCode "
				+ " join DT_LanguageConfigDetail on DT_LanguageConfigDetail.languageCfgCode = DT_LanguageConfig.code  "
				+ " and  DT_Language.code = DT_LanguageConfigDetail.languageCode "
				+ " where serialNo = '"
				+ serialNo
				+ "' and validDate > date_format(NOW(),'%Y-%m-%d') and DT_SoftwareType.softType = '1' "
				+ " and (DT_SoftwareVersion.releaseState = '1' ) "
				+ " order by DT_SoftWareName.softwareName, DT_SoftwareVersion.versionName desc";

		return this.daoSupport.queryForList(sql, DataCompare.class);

	}

	@Override
	public List<DataCompare> getNewMinSaleCodes(String serialNo)
			throws Exception {

		String sql = "select p.minSaleCode ,(select name from DT_MinSaleUnitMemo where languagecode='lag201304221104540500' and minsaleunitcode=p.minSaleCode ) as newName "
				+ " from DT_ProductMinSaleSearch p "
				+ " where p.productSn=? order by p.minSaleCode desc ";
		return this.daoSupport.queryForList(sql, DataCompare.class, serialNo);
	}

	@Override
	public List<DataCompare> getOldMinSaleCodes(String serialNo)
			throws Exception {

		String sql="select a.minSaleUnitCode ,(select name from DT_MinSaleUnitMemo where languagecode='lag201304221104540500' and minsaleunitcode=a.minSaleUnitCode ) as oldName "
				+ " from DT_ProductSoftwareValidStatus a ,DT_ProductInfo b "
                + " where a.proCode=b.code and b.serialNo=? order by minSaleUnitCode desc ";
		return this.daoSupport.queryForList(sql, DataCompare.class, serialNo);
	}

	
    public void insertProductMinSaleSearch(String serialNo) throws Exception{
    	String delSql="delete from DT_ProductMinSaleSearch where productSn=?";
    	this.daoSupport.execute(delSql, serialNo);
    	
    	StringBuffer insertSql=new StringBuffer("insert into DT_ProductMinSaleSearch(productSn, productTypeCode, minSaleCode, minSaleName,language, validDate) ");
    	insertSql.append(" select DT_ProductInfo.serialNo,DT_ProductForSealerConfig.productTypeCode, DT_MinSaleUnit.code,  ");
    	insertSql.append("DT_MinSaleUnitMemo.name, DT_Language.languageCode, ");
    	insertSql.append("DT_ProductSoftwareValidStatus.validDate ");
    	insertSql.append("from DT_ProductInfo ");
    	insertSql.append("join DT_ProductForSealerConfig on DT_ProductForSealerConfig.productForSealerCode = DT_ProductInfo.proTypeCode ");
    	insertSql.append("join DT_ProductSoftwareValidStatus on DT_ProductSoftwareValidStatus.proCode = DT_ProductInfo.code ");
    	insertSql.append("join DT_MinSaleUnit on DT_MinSaleUnit.code = DT_ProductSoftwareValidStatus.minSaleUnitCode ");
    	insertSql.append("join DT_SaleContract on DT_SaleContract.code = DT_ProductInfo.saleContractCode  ");
    	insertSql.append("join DT_LanguageConfig on DT_LanguageConfig.code = DT_SaleContract.languageCfgCode  ");
    	insertSql.append("join DT_LanguageConfigDetail on DT_LanguageConfigDetail.languageCfgCode = DT_LanguageConfig.code  ");
    	insertSql.append("join DT_Language on DT_Language.code = DT_LanguageConfigDetail.languageCode   ");
    	insertSql.append("left join DT_MinSaleUnitMemo on DT_MinSaleUnitMemo.minSaleUnitCode = DT_MinSaleUnit.code ");
    	insertSql.append("and DT_MinSaleUnitMemo.languageCode = DT_Language.code ");
    	insertSql.append("where DT_ProductInfo.serialNo =?");
    	this.daoSupport.execute(insertSql.toString(),serialNo);
    	
    }
	
	
}
