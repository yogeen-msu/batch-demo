package com.cheriscon.backstage.member.service.impl;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ICustomerChangeAutelIdLogService;
import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;
import com.cheriscon.common.model.CustomerChangeAutelIdLog;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class CustomerChangeAutelIdLogServiceImpl extends  BaseSupport<CustomerChangeAutelIdLog> implements
		ICustomerChangeAutelIdLogService {

	@Override
	public Page pageCustomerChangeAutelPage(CustomerChangeAutelIdLog log,
			int pageNo, int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_CustomerChangeAutelIdLog where 1=1");
		if(null != log){
			if(!StringUtil.isBlank(log.getOldAutelId())){
				sql.append(" and oldAutelId like'%").append(log.getOldAutelId().trim()).append("%'");
			}
			if(!StringUtil.isBlank(log.getNewAutelId())){
				sql.append(" and newAutelId like'%").append(log.getNewAutelId().trim()).append("%'");
			}
		}
		sql.append(" order by id desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, CustomerChangeAutelIdLog.class);
		return page;
	}

	@Override
	public void saveLog(CustomerChangeAutelIdLog log) throws Exception {
		
        this.daoSupport.insert("DT_CustomerChangeAutelIdLog", log);
        
        String updateSql="update DT_CustomerProInfo set customerCode=? where proCode=?";
        this.daoSupport.execute(updateSql, log.getNewCustomerCode(),log.getProCode());
        
	}

}
