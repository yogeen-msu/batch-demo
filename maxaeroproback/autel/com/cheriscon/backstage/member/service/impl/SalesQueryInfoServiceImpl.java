/**
 * 
 */
package com.cheriscon.backstage.member.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ISalesQueryInfoService;
import com.cheriscon.backstage.member.vo.PreSalesSupportVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.SalesQueryInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.vo.SalesQueryInfoVo;

/**
 * @author A13045
 *
 */
@Service
public class SalesQueryInfoServiceImpl extends BaseSupport<SalesQueryInfo> implements ISalesQueryInfoService {


	@Override
	public Page pageSalesQuery(String auth,SalesQueryInfo salesQueryInfo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql=new StringBuffer("");
		sql.append("select a.id,a.code,a.createUser,d.name as createUserName," +
				"date_format(a.createDate,'%m/%d/%Y') as createDate ," +
				"a.customerName,a.email,a.telphone,a.address,a.proTypeCode,a.openRemark,a.status,a.serialNo," +
				"a.closeRemark,a.closeUser,a.closeDate," +
				"b.name as proTypeName " +
				"from DT_SalesQueryInfo a,dt_productForSealer b,DT_SealerDataAuthDeails c,dt_sealerInfo d where a.proTypeCode=b.code ");
		sql.append(" and a.createUser=c.sealerCode");
		sql.append(" and a.createUser=d.autelId");
		sql.append(" and c.authCode like'").append(auth).append("%'");
		if(null != salesQueryInfo ){
			if(!StringUtil.isEmpty(salesQueryInfo.getProTypeCode()) && !salesQueryInfo.getProTypeCode().equals("-1")){
				sql.append(" and b.code='").append(salesQueryInfo.getProTypeCode().trim()).append("'");
			}
			if(!StringUtil.isEmpty(salesQueryInfo.getCustomerName())){
				sql.append(" and a.customerName like '%").append(salesQueryInfo.getCustomerName().trim()).append("%'");
			}
			if(!StringUtil.isEmpty(salesQueryInfo.getStatus()) && !salesQueryInfo.getStatus().equals("-1")){
				sql.append(" and a.status='").append(salesQueryInfo.getStatus().trim()).append("'");
			}
		}
		sql.append(" order by a.id desc");
		
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}
	
	public Page pageSalesQuery(SalesQueryInfoVo salesQueryInfoVo) throws Exception {
		StringBuffer sql=new StringBuffer("");
		sql.append("select a.id,a.code,a.createUser,d.name as createUserName," +
				"date_format(a.createDate,'%m/%d/%Y') as createDate ," +
				"a.customerName,a.email,a.telphone,a.address,a.proTypeCode,a.openRemark,a.status,a.serialNo," +
				"a.closeRemark,a.closeUser,a.closeDate," +
				"b.name as proTypeName " +
				"from DT_SalesQueryInfo a,dt_productForSealer b,DT_SealerDataAuthDeails c,dt_sealerInfo d where a.proTypeCode=b.code ");
		sql.append(" and a.createUser=c.sealerCode");
		sql.append(" and a.createUser=d.autelId");
		sql.append(" and c.authCode like'").append(salesQueryInfoVo.getAuth()).append("%'");
		if(null != salesQueryInfoVo){
			if(!StringUtil.isEmpty(salesQueryInfoVo.getProTypeCode()) && !salesQueryInfoVo.getProTypeCode().equals("-1")){
				sql.append(" and b.code='").append(salesQueryInfoVo.getProTypeCode().trim()).append("'");
			}
			if(!StringUtil.isEmpty(salesQueryInfoVo.getCustomerName())){
				sql.append(" and a.customerName like '%").append(salesQueryInfoVo.getCustomerName().trim()).append("%'");
			}
			if(!StringUtil.isEmpty(salesQueryInfoVo.getStatus()) && !salesQueryInfoVo.getStatus().equals("-1")){
				sql.append(" and a.status='").append(salesQueryInfoVo.getStatus().trim()).append("'");
			}
		}
		sql.append(" order by a.id desc");
		
		return this.daoSupport.queryForPage(sql.toString(), salesQueryInfoVo.getPage(), salesQueryInfoVo.getPageSize());
	}
	
	@Override
	public void saveSalesQuery(SalesQueryInfo salesQueryInfo) throws Exception {
		salesQueryInfo.setCode(DBUtils.generateCode(DBConstant.SALESQUERY_CODE));
		salesQueryInfo.setStatus(DBConstant.SALES_QUERY_STATUS_OPEN);
		this.daoSupport.insert("DT_SalesQueryInfo", salesQueryInfo);
	}

	@Override
	public void updateSalesQuery(SalesQueryInfo salesQueryInfo)
			throws Exception {
		this.daoSupport.update("DT_SalesQueryInfo", salesQueryInfo, "id="+salesQueryInfo.getId()+"");

	}

	@Override
	public void deleteSalesQueryInfo(String code) throws Exception {
		String sql = "delete from DT_salesQueryInfo where code= ?";
		this.daoSupport.execute(sql,code);

	}

	public SalesQueryInfo getSalesQueryInfoByCode(String code) throws Exception{
		String sql="select * from DT_salesQueryInfo where code=?";
		return this.daoSupport.queryForObject(sql, SalesQueryInfo.class, code);
	}
}
