package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ISdkAppDownloadService;
import com.cheriscon.backstage.member.vo.SdkAppDownloadVO;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * sdk app download逻辑实现类
 * 
 * @author autel
 * 
 */
@Service
public class SdkAppDownloadServiceImpl extends BaseSupport<SdkAppDownloadVO>
		implements ISdkAppDownloadService {

	private static Logger logger = Logger
			.getLogger(SdkAppDownloadServiceImpl.class);

	/**
	 * 分页 查詢sdk app download信息
	 * 
	 * @return list
	 */
	public Page querySdkAppDownloadsList(int pageSize, int pageNo) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("id as id,");
		sql.append("sdk_category AS sdkCategory,");
		sql.append("sdk_version AS sdkVersion,");
		sql.append("sdk_download_url AS sdkDownloadUrl,");
		sql.append("category_type AS categoryType,");
		sql.append("release_notes AS releaseNotes,");
		sql.append("release_notes_version AS releaseNotesVersion,");
		sql.append("release_notes_download_url AS releaseNotesDownloadUrl,");
		sql.append("status as status,");
		sql.append("date_format(create_date, '%Y-%m-%d %H:%i:%s') AS createDate,");
		sql.append("date_format(last_update_date, '%Y-%m-%d %H:%i:%s') AS lastUpdateDate");
		sql.append(" FROM es_sdk_app_download_t");
		sql.append(" where `status` = 1");
		logger.info("querySdkAppDownloadsList sql:" + sql.toString());
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize);
		return page;
	}

	/**
	 * 查詢sdk app download明细信息
	 * 
	 * @return list
	 */
	public SdkAppDownloadVO querySdkAppDownloadsInfoById(String id) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("id as id,");
		sql.append("sdk_category AS sdkCategory,");
		sql.append("sdk_version AS sdkVersion,");
		sql.append("sdk_download_url AS sdkDownloadUrl,");
		sql.append("category_type AS categoryType,");
		sql.append("release_notes AS releaseNotes,");
		sql.append("release_notes_version AS releaseNotesVersion,");
		sql.append("release_notes_download_url AS releaseNotesDownloadUrl,");
		sql.append("status as status,");
		sql.append("date_format(create_date, '%Y-%m-%d %H:%i:%s') AS createDate,");
		sql.append("date_format(last_update_date, '%Y-%m-%d %H:%i:%s') AS lastUpdateDate");
		sql.append(" FROM es_sdk_app_download_t");
		sql.append(" where id = " + id);
		logger.info("querySdkAppDownloadsInfo sql:" + sql.toString());
		List<SdkAppDownloadVO> list = this.daoSupport.queryForList(
				sql.toString(), SdkAppDownloadVO.class);
		if (null != list && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 查詢sdk app download信息
	 * 
	 * @return list
	 */
	public List<SdkAppDownloadVO> querySdkAppDownloadsInfo() {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("s.id as id,");
		sql.append("s.sdk_category AS sdkCategory,");
		sql.append("s.sdk_version AS sdkVersion,");
		sql.append("s.sdk_download_url AS sdkDownloadUrl,");
		sql.append("s.category_type AS categoryType,");
		sql.append("s.release_notes AS releaseNotes,");
		sql.append("s.release_notes_version AS releaseNotesVersion,");
		sql.append("s.release_notes_download_url AS releaseNotesDownloadUrl,");
		sql.append("s.status as status,");
		sql.append("date_format(s.create_date, '%Y-%m-%d %H:%i:%s') AS createDate,");
		sql.append("date_format(s.last_update_date, '%Y-%m-%d %H:%i:%s') AS lastUpdateDate");
		sql.append(" from (SELECT max(id) as id FROM es_sdk_app_download_t where status = 1 group by category_type ) t left join es_sdk_app_download_t as s on t.id=s.id ");
		logger.info("querySdkAppDownloadsInfo sql:" + sql.toString());
		List<SdkAppDownloadVO> list = this.daoSupport.queryForList(
				sql.toString(), SdkAppDownloadVO.class);
		return list;
	}

	/**
	 * 添加sdk app download信息
	 * 
	 * @return boolean
	 */
	public boolean addSdkAppDownloads(SdkAppDownloadVO sdkAppDownLoadVo) {
		boolean result = true;
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO es_sdk_app_download_t (sdk_category,sdk_version,sdk_download_url,category_type,release_notes,release_notes_version,release_notes_download_url,status,create_date,last_update_date)");
		sql.append(" VALUES (?, ?, ?, ?,?, ?, ?, '1', sysdate(), sysdate())");
		logger.info("addSdkAppDownloads sql:" + sql.toString());
		try {
			this.daoSupport.execute(sql.toString(),
					sdkAppDownLoadVo.getSdkCategory(),
					sdkAppDownLoadVo.getSdkVersion(),
					sdkAppDownLoadVo.getSdkDownloadUrl(),
					sdkAppDownLoadVo.getCategoryType(),
					sdkAppDownLoadVo.getReleaseNotes(),
					sdkAppDownLoadVo.getReleaseNotesVersion(),
					sdkAppDownLoadVo.getReleaseNotesDownloadUrl());
		} catch (Exception e) {
			result = false;
			logger.error("addSdkAppDownloads error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 修改sdk app download信息
	 * 
	 * @return boolean
	 */
	public boolean updateSdkAppDownloads(SdkAppDownloadVO sdkAppDownLoadVo) {
		boolean result = true;
		StringBuffer sql = new StringBuffer();
		sql.append("update es_sdk_app_download_t set sdk_category = ?,");
		sql.append("sdk_version = ?,");
		sql.append("sdk_download_url = ?,");
		sql.append("category_type = ?,");
		sql.append("release_notes = ?,");
		sql.append("release_notes_version = ?,");
		sql.append("release_notes_download_url = ?,");
		sql.append("last_update_date = sysdate() where id= ? ");
		logger.info("updateSdkAppDownloads sql:" + sql.toString());
		try {
			this.daoSupport.execute(sql.toString(),
					sdkAppDownLoadVo.getSdkCategory(),
					sdkAppDownLoadVo.getSdkVersion(),
					sdkAppDownLoadVo.getSdkDownloadUrl(),
					sdkAppDownLoadVo.getCategoryType(),
					sdkAppDownLoadVo.getReleaseNotes(),
					sdkAppDownLoadVo.getReleaseNotesVersion(),
					sdkAppDownLoadVo.getReleaseNotesDownloadUrl(),
					sdkAppDownLoadVo.getId());
		} catch (Exception e) {
			result = false;
			logger.error("updateSdkAppDownloads error." + e.getMessage());
		}
		return result;
	}

	/**
	 * 删除sdk app download信息
	 * 
	 * @return boolean
	 */
	public boolean deleteSdkAppDownloads(SdkAppDownloadVO sdkAppDownLoadVo) {
		boolean result = true;
		StringBuffer sql = new StringBuffer();
		sql.append("update es_sdk_app_download_t set status = 0,last_update_date = sysdate() where id=? ");
		logger.info("deleteSdkAppDownloads sql:" + sql.toString());
		try {
			this.daoSupport.execute(sql.toString(), sdkAppDownLoadVo.getId());
		} catch (Exception e) {
			result = false;
			logger.error("deleteSdkAppDownloads error." + e.getMessage());
		}
		return result;
	}

}
