package com.cheriscon.backstage.member.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.CustomerComplaintTypeName;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.CustomerComplaintTypeName;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 客诉类型名称业务逻辑实现类
 * @author pengdongan
 * @date 2013-01-24
 */
@Service
public class CustomerComplaintTypeNameServiceImpl extends BaseSupport<CustomerComplaintTypeName> implements ICustomerComplaintTypeNameService
{
	@Resource
	private ILanguageService languageService;
	
	/**
	 * 查询所有客诉类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CustomerComplaintTypeName> listAll() throws Exception {
		String sql = "select * from DT_CustomerComplaintTypeName order by id desc";
		
		return this.daoSupport.queryForList(sql, CustomerComplaintTypeName.class);
	}
	/**
	 * 按语言查询所有客诉类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CustomerComplaintTypeName> listComplaintTypeNameByLanguageCode(String languageCode) throws Exception {
		String sql = "select * from DT_CustomerComplaintTypeName where languageCode =? order by name";
		
		return this.daoSupport.queryForList(sql, CustomerComplaintTypeName.class, languageCode);
	}
	public List<CustomerComplaintTypeName> listComplaintTypeNameByLanguageCode1(String languageCode){
		if(languageCode.contains("zh")||languageCode.contains("ZH")){
			languageCode="zh";
		}
		if(languageCode.contains("en")||languageCode.contains("EN")){
			languageCode="en";
		}
		
		StringBuffer sql=new StringBuffer();
		sql.append("select a.id,a.complaintTypeCode,a.languageCode,a.name from DT_CustomerComplaintTypeName a,DT_Language b where  a.languageCode=b.code  and b.languageCode='")
		.append(languageCode).append("' order by a.id desc");
		return this.daoSupport.queryForList(sql.toString(), CustomerComplaintTypeName.class);
	}

	/**
	 * 根据客诉类型查询客诉类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CustomerComplaintTypeName> queryByTypeCode(String typeCode) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,b.name languageName");
		sql.append(" from DT_CustomerComplaintTypeName a");
		sql.append(" left join DT_Language b on a.languageCode=b.code");
		sql.append(" where a.complaintTypeCode = ? order by a.id desc");
		
		return this.daoSupport.queryForList(sql.toString(), CustomerComplaintTypeName.class, typeCode);
	}
	
	
	/**
	 * 添加客诉类型名称
	 * @param customerComplaintTypeName
	 * 客诉类型名称
	 * @throws Exception
	 */
	@Transactional
	public int insertCustomerComplaintTypeName(CustomerComplaintTypeName customerComplaintTypeName) throws Exception {
		this.daoSupport.insert("DT_CustomerComplaintTypeName", customerComplaintTypeName);
		return 0;
	}
	
	/**
	 * 更新客诉类型名称
	 * @param customerComplaintTypeName
	 * 客诉类型名称
	 * @throws Exception
	 */
	@Transactional
	public int updateCustomerComplaintTypeName(CustomerComplaintTypeName customerComplaintTypeName) throws Exception {
		this.daoSupport.update("DT_CustomerComplaintTypeName", customerComplaintTypeName, "complaintTypeCode='"+customerComplaintTypeName.getComplaintTypeCode()+"'");
		return 0;
	}

	@Override
	@Transactional
	public void updateCustomerComplaintTypeNames(
			List<CustomerComplaintTypeName> customerComplaintTypeNames, List<CustomerComplaintTypeName> delCustomerComplaintTypeNames)
			throws Exception {
		//删除
		if(null != delCustomerComplaintTypeNames && delCustomerComplaintTypeNames.size() > 0){
			for(CustomerComplaintTypeName customerComplaintTypeName : delCustomerComplaintTypeNames){
				this.delCustomerComplaintTypeName(customerComplaintTypeName.getId());
			}
		}
		//添加
		if(null != customerComplaintTypeNames && customerComplaintTypeNames.size() > 0){
			for(CustomerComplaintTypeName customerComplaintTypeName : customerComplaintTypeNames){
				this.insertCustomerComplaintTypeName(customerComplaintTypeName);
			}
		}
	}

	public CustomerComplaintTypeName getCustomerComplaintTypeNameByCode(int id) throws Exception {
		String sql = "select * from DT_CustomerComplaintTypeName where id = ?";
		
		return (CustomerComplaintTypeName) this.daoSupport.queryForObject(sql, CustomerComplaintTypeName.class, id);
	}

	@Transactional
	public int delCustomerComplaintTypeName(int id) throws Exception {
		String sql = "delete from DT_CustomerComplaintTypeName where id= ?";
		this.daoSupport.execute(sql,id);
		return 0;
	}

	@Transactional
	public int delCustomerComplaintTypeNameByTypeCode(String complaintTypeCode) throws Exception {
		String sql = "delete from DT_CustomerComplaintTypeName where complaintTypeCode= ?";
		this.daoSupport.execute(sql,complaintTypeCode);
		return 0;
	}



	/**
	  * 客诉类型名称分页显示方法
	  * @param CustomerComplaintTypeName
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerComplaintTypeNamePage(CustomerComplaintTypeName customerComplaintTypeName, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.name name");
		sql.append(" from DT_CustomerComplaintTypeName a");
		sql.append(" left join DT_CustomerComplaintTypeNameName c on a.code=c.complaintTypeCode");
		sql.append(" where 1=1");
		
//		if (null != customerComplaintTypeName) {
//			if (StringUtils.isNotBlank(customerComplaintTypeName.getName())) {
//				sql.append(" and c.name like '%");
//				sql.append(customerComplaintTypeName.getName().trim());
//				sql.append("%'");
//			}
//		}		
//
//		
//		if (StringUtils.isNotBlank(customerComplaintTypeName.getAdminLanguageCode())) {
//			sql.append(" and c.languageCode = '");
//			sql.append(customerComplaintTypeName.getAdminLanguageCode().trim());
//			sql.append("'");
//		}
		
		
		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, CustomerComplaintTypeName.class);
		return page;
	}


	@Override
	public CustomerComplaintTypeName getCustomerComplaintTypeNameByCode(
			String code) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public CustomerComplaintTypeName getCustomerComplaintTypeName(
			String complaintTypeCode, String languageCode) throws Exception 
	{
		String sql = "select * from DT_CustomerComplaintTypeName where complaintTypeCode=? and languageCode =? ";
		
		return this.daoSupport.queryForObject(sql, CustomerComplaintTypeName.class, complaintTypeCode,languageCode);
	}

	/**
	  * <p>Title: getAvailableLanguage</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<Language> getAvailableLanguage(String typeCode)
			throws Exception {
		List<Language> list = new ArrayList<Language>();
		List<Language> languages = languageService.queryLanguage();
		if(null == languages || languages.size() == 0){
			return list;
		}
		List<CustomerComplaintTypeName> memos = this.queryByTypeCode(typeCode);
		if(null == memos || memos.size() == 0){
			return languages;
		}
		Map<String , CustomerComplaintTypeName> map = this.convertListToMap(memos);
		for(Language language : languages){
			if(!map.containsKey(language.getCode()))
				list.add(language);
		}
		return list;
	}
	
	private Map<String , CustomerComplaintTypeName> convertListToMap(List<CustomerComplaintTypeName> memos) throws Exception{
		Map<String , CustomerComplaintTypeName> map = new HashMap<String , CustomerComplaintTypeName>();
		for(CustomerComplaintTypeName memo : memos){
			map.put(memo.getLanguageCode(), memo);
		}
		return map;
	}

}
