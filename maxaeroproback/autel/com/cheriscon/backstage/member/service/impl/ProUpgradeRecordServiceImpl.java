package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IProUpgradeRecordService;
import com.cheriscon.backstage.member.vo.ProUpgradeRecordVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客户产品升级信息service实现
 * @author pengdongan
 * @date 2013-01-07
 * 
 */
@Service
public class ProUpgradeRecordServiceImpl extends BaseSupport implements IProUpgradeRecordService {


	
	@Transactional
	public List<ProUpgradeRecordVo> listSealer(ProUpgradeRecordVo proUpgradeRecordVo)
	throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,d.autelId autelId,d.name customerName,");
		sql.append("b.serialNo proSerialNo,e.name softwareTypeName");
		sql.append(" from DT_ProductUpgradeRecord a");
		sql.append(" left join DT_ProductInfo b on a.proCode=b.code");
		sql.append(" left join DT_CustomerProInfo c on b.code=c.proCode");
		sql.append(" left join DT_CustomerInfo d on c.customerCode=d.code");
		sql.append(" left join DT_SoftwareType e on a.softwareTypeCode=e.code");
		sql.append(" where 1=1");
		
		if (null != proUpgradeRecordVo) {
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getAutelId())) {
				sql.append(" and d.autelId like '%");
				sql.append(proUpgradeRecordVo.getAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getProSerialNo())) {
				sql.append(" and b.serialNo like '%");
				sql.append(proUpgradeRecordVo.getProSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getBeginDate())) {
				sql.append(" and a.upgradeTime >= '");
				sql.append(proUpgradeRecordVo.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getEndDate())) {
				sql.append(" and a.upgradeTime < cast('");
				sql.append(proUpgradeRecordVo.getEndDate().trim());
				sql.append("' as datetime)+1");
			}
		}
		return (List<ProUpgradeRecordVo>) this.daoSupport.queryForList(sql.toString(),ProUpgradeRecordVo.class);
	}
	
	/**
	 * 根据编码查询客户产品升级信息对象
	 * @param code	客户产品升级信息code
	 * @return
	 * @throws Exception
	 */

	public ProUpgradeRecordVo getSealerByCode(String code) throws Exception {
		String sql = "select * from DT_ProductUpgradeRecord where code = ?";
		
		return (ProUpgradeRecordVo) this.daoSupport.queryForObject(sql, ProUpgradeRecordVo.class, code);
	}

	
	/**
	 * 清空客户产品升级信息对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	
	@Transactional
	public int clearProUpgradeRecord() throws Exception {
		String sql = "delete from DT_ProductUpgradeRecord";
		this.daoSupport.execute(sql);
		return 0;
	}


	/**
	  * 客户产品升级信息分页显示方法
	  * @param ProUpgradeRecordVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageProUpgradeRecordVoPage(ProUpgradeRecordVo proUpgradeRecordVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		
		sql.append("select * from (");

		sql.append("select a.*,d.autelId autelId,d.name customerName,");
		sql.append("b.serialNo proSerialNo,e.name softwareTypeName");
		sql.append(" from DT_ProductUpgradeRecord a");
		sql.append(" left join DT_ProductInfo b on a.proCode=b.code");
		sql.append(" left join DT_CustomerProInfo c on b.code=c.proCode");
		sql.append(" left join DT_CustomerInfo d on c.customerCode=d.code");
		sql.append(" left join DT_SoftwareType e on a.softwareTypeCode=e.code");
		sql.append(" where 1=1");
		
		if (null != proUpgradeRecordVo) {
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getProCode())) {
				sql.append(" and a.proCode = '");
				sql.append(proUpgradeRecordVo.getProCode().trim());
				sql.append("'");
			}
			
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getAutelId())) {
				sql.append(" and d.autelId like '%");
				sql.append(proUpgradeRecordVo.getAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getProSerialNo())) {
				sql.append(" and b.serialNo like '%");
				sql.append(proUpgradeRecordVo.getProSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getSoftwareTypeName())) {
				sql.append(" and e.name like '%");
				sql.append(proUpgradeRecordVo.getSoftwareTypeName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getBeginDate())) {
				sql.append(" and a.upgradeTime >= '");
				sql.append(proUpgradeRecordVo.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(proUpgradeRecordVo.getEndDate())) {
				sql.append(" and a.upgradeTime < cast('");
				sql.append(proUpgradeRecordVo.getEndDate().trim());
				sql.append("' as datetime)+1");
			}
		}
		
		sql.append(") m");

		sql.append(" order by m.id desc");
			Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
					pageSize, ProUpgradeRecordVo.class);
			return page;
	}

}
