package com.cheriscon.backstage.member.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.IReChargeCardErrorLogService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ReChargeCardErrorLog;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;

@Service
public class ReChargeCardErrorLogServiceImpl extends BaseSupport<ReChargeCardErrorLog> implements
		IReChargeCardErrorLogService {

	@Override
	public Page pageReChargeCardError(ReChargeCardErrorLog log, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append(" select a.code as code,a.customerCode as customerCode,a.createTime as createTime,a.status as status  " );
		sql.append(" ,a.remark as remark,a.updateUser as updateUser,a.updateTime as updateTime,b.autelId as autelId" );
		sql.append(" from  DT_ReChargeCardErrorLog a left outer join DT_CustomerInfo b on a.customerCode=b.code where 1=1");
		if(null != log){
			if(!StringUtil.isEmpty(log.getAutelId())){
				sql.append(" and b.autelId like '"+StringUtil.isEmpty(log.getAutelId())+"'");
			}
			if(!StringUtil.isEmpty(log.getStatus().toString())){
				sql.append(" and a.status="+log.getStatus()+"");
			}
		}
		sql.append(" order by a.customerCode desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, ReChargeCardErrorLog.class);
	}

	@Override
	public void saveErrorLog(ReChargeCardErrorLog log) throws Exception {
		log.setCode(DBUtils.generateCode(DBConstant.RECHARGE_CARD_ERROR_LOG));
		this.daoSupport.insert("DT_ReChargeCardErrorLog", log);

	}

	@Override
	public void updateErrorLog(String updateUser,String UpdateTime,String customerCode) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("update DT_ReChargeCardErrorLog set status=1,updateTime=?,updateUser=? where customerCode=?");
		this.daoSupport.execute(sql.toString(), UpdateTime,updateUser,customerCode);

	}

	@Override
	public ReChargeCardErrorLog getErrorLogByCode(String code) throws Exception {
		StringBuffer sql=new StringBuffer("select * from DT_ReChargeCardErrorLog where code=?");
		return this.daoSupport.queryForObject(sql.toString(), ReChargeCardErrorLog.class, code);
	}
	
	@Override
	public int getErrorLogCount(String customerCode) throws Exception{
		StringBuffer sql=new StringBuffer();
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd");
		sql.append("select count(customerCode) from DT_ReChargeCardErrorLog where status=0 and customerCode=? and type='1' and createTime like '"+date+"%'  ");
		return this.daoSupport.queryForInt(sql.toString(), customerCode);
		
	}

}
