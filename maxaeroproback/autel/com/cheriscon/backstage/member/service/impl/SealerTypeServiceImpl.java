package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ISealerTypeService;
import com.cheriscon.backstage.member.vo.SealerTypeVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.SealerType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @remark 经销商类型service实现
 * @author pengdongan
 * @date 2013-01-07
 * 
 */
@Service
public class SealerTypeServiceImpl extends BaseSupport implements ISealerTypeService {


	
	
	public List<SealerType> querySealerType()
	throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.* from DT_SealerType a");
		return (List<SealerType>) this.daoSupport.queryForList(sql.toString(),SealerType.class);
	}


	
	
	public List<SealerTypeVo> listSealerType(SealerTypeVo sealerTypeVo)
	throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.* from DT_SealerType a");
		return (List<SealerTypeVo>) this.daoSupport.queryForList(sql.toString(),SealerTypeVo.class);
	}
	
	/**
	 * 更新经销商类型
	 * @param sealerType
	 * 经销商类型
	 * @throws Exception
	 */
	@Transactional
	public int insertSealerType(SealerType sealerType) throws Exception {
		sealerType.setCode(DBUtils.generateCode(DBConstant.SEALERTYPE_CODE));
		this.daoSupport.insert("DT_SealerType", sealerType);
		return 0;
	}
	
	/**
	 * 添加经销商类型
	 * @param sealerType
	 * 经销商类型
	 * @throws Exception
	 */
	@Transactional
	public int updateSealerType(SealerType sealerType) throws Exception {
		this.daoSupport.update("DT_SealerType", sealerType, "code='"+sealerType.getCode()+"'");
		return 0;
	}

	public SealerType getSealerTypeByCode(String code) throws Exception {
		String sql = "select * from DT_SealerType where code = ?";
		
		return (SealerType) this.daoSupport.queryForObject(sql, SealerType.class, code);
	}

	public SealerType getSealerTypeByName(String name) throws Exception {
		String sql = "select * from DT_SealerType where name = ? limit 0,1";
		
		return (SealerType) this.daoSupport.queryForObject(sql, SealerType.class, name);
	}

	@Transactional
	public int delSealerType(String code) throws Exception {
		String sql = "delete from DT_SealerType where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delSealerTypes(String codes) throws Exception {
		String sql = "delete from DT_SealerType where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}



	/**
	  * 经销商类型分页显示方法
	  * @param SealerTypeVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageSealerTypeVoPage(SealerTypeVo sealerTypeVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		
		sql.append("select a.* from DT_SealerType a");
		sql.append(" order by a.id desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, SealerTypeVo.class);
		return page;
	}

}
