package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthDetail;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;
@Service
public class SealerAuthServiceImpl extends BaseSupport<SealerAuthVO> implements ISealerAuthService {

	@Override
	public List<SealerAuthVO> list(String sealerCode) {
		
		StringBuffer sql=new StringBuffer();
		sql.append(" select a.code as code, a.url as url,a.sealermenu as sealermenu,a.name as name from dt_sealerauth a,dt_sealerauthdetail b");
		sql.append(" where a.code=b.authcode ");
		sql.append(" and b.sealerCode='"+sealerCode+"'");
		sql.append(" order by a.sortNum asc");
		List<SealerAuthVO> leftMenuList  = this.daoSupport.queryForList(sql.toString(),SealerAuthVO.class);
	    return leftMenuList;
	}

	public Page pageAuthVoDetail(SealerAuthVO sealerAuthVo,int pageNo,int pageSize) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select b.id as id, a.code as code,a.Name as name,c.autelId as autelId from dt_sealerauth a,DT_SealerAuthDetail b,DT_SealerInfo c");
		sql.append(" where a.code=b.authCode");
		sql.append(" and b.sealerCode=c.code");
		if(null!=sealerAuthVo){
			if(!StringUtil.isEmpty(sealerAuthVo.getAutelId())){
				sql.append(" and c.autelId like '%").append(sealerAuthVo.getAutelId().trim()).append("%'");
			}
		}
		sql.append(" order by c.autelId");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}
	
	public List<SealerAuthVO> listSealerAuthVO() throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select * from dt_sealerauth order by sortNum");
		return this.daoSupport.queryForList(sql.toString(), SealerAuthVO.class);
	}
	
	@Override
	public Page pageSealerAuthVoPage(SealerAuthVO sealerAuthVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("select * from dt_sealerauth order by sortNum");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}

	@Override
	public void saveAuth(SealerAuthVO sealerAuthVo) throws Exception {
		this.daoSupport.insert("DT_SealerAuth", sealerAuthVo)	;	
	}

	@Override
	public void updateAuth(SealerAuthVO sealerAuthVo) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("update DT_SealerAuth set url=?,sealermenu=?,name=?,sortnum=? where code=?");
		this.daoSupport.execute(sql.toString(), 
				sealerAuthVo.getUrl(),
				sealerAuthVo.getSealermenu(),
				sealerAuthVo.getName(),
				sealerAuthVo.getSortNum(),
				sealerAuthVo.getCode());
	}

	@Override
	public void deleteAuth(String code) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("delete from DT_SealerAuth where code=?");
		this.daoSupport.execute(sql.toString(), code);
		
	}

	@Override
	public SealerAuthVO getSealerAuthById(String code) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("select * from DT_SealerAuth where code=?");
		return this.daoSupport.queryForObject(sql.toString(), SealerAuthVO.class, code);
	}

	public void saveAuthDetail(String autelId,String sealerAuthDesc) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select count(1) from DT_SealerAuth a ,dt_sealerauthdetail b");
		sql.append(" where a.code=b.authCode");
		sql.append(" and b.sealerCode=?");
		int num=this.daoSupport.queryForInt(sql.toString(), autelId);
		if(num!=0){
			String delSql="delete from dt_sealerauthdetail where sealerCode=?";
			this.daoSupport.execute(delSql, autelId);
		}
		String[] str=sealerAuthDesc.split(",");
		for(int i=0;i<str.length;i++){
			SealerAuthDetail d=new SealerAuthDetail();
			d.setAuthCode(str[i].trim());
			d.setSealerCode(autelId);
			this.daoSupport.insert("dt_sealerauthdetail", d);
		}
		
	}

	public void delAuthDetailById(Integer id) throws Exception{
		String sql="delete from dt_sealerauthdetail where id=?" ;
		this.daoSupport.execute(sql, id);		
	}
	
}
