package com.cheriscon.backstage.member.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeService;
import com.cheriscon.backstage.member.service.ITestPackLanguagePackService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerComplaintType;
import com.cheriscon.common.model.CustomerComplaintTypeName;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 客诉类型业务逻辑实现类
 * @author pengdongan
 * @date 2013-01-24
 */
@Service
public class CustomerComplaintTypeServiceImpl extends BaseSupport<CustomerComplaintType> implements ICustomerComplaintTypeService
{

	@Resource
	private ICustomerComplaintTypeNameService complaintTypeNameService;
	
	/**
	 * 查询所有客诉类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CustomerComplaintType> listAll() throws Exception {
		String sql = "select * from DT_CustomerComplaintType order by id desc";
		
		return this.daoSupport.queryForList(sql, CustomerComplaintType.class);
	}
	
	/**
	 * 按语言查询所有客诉类型对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CustomerComplaintType> listCustomerComplaintTypeByLanguageCode(String languageCode) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.name name");
		sql.append(" from DT_CustomerComplaintType a");
		sql.append(" left join DT_CustomerComplaintTypeName c on a.code=c.complaintTypeCode and c.languageCode=?");
		sql.append(" order by a.id desc");
		
		return this.daoSupport.queryForList(sql.toString(), CustomerComplaintType.class, languageCode);
	}
	
	public CustomerComplaintType getComplaintTypeByLanguage(String language,String code) throws Exception{
		
		StringBuffer sql = new StringBuffer();
		sql.append("select a.*,c.name name");
		sql.append(" from DT_CustomerComplaintType a");
		sql.append(" left join DT_CustomerComplaintTypeName c on a.code=c.complaintTypeCode and c.languageCode=?");
		sql.append(" where a.code=?");
		return this.daoSupport.queryForObject(sql.toString(), CustomerComplaintType.class, language,code);
	}
	
	/**
	 * 添加客诉类型
	 * @param customerComplaintType
	 * 客诉类型
	 * @throws Exception
	 */
	@Transactional
	public int insertCustomerComplaintType(CustomerComplaintType customerComplaintType) throws Exception {
		customerComplaintType.setCode(DBUtils.generateCode(DBConstant.CUC_TYPE_CODE));
		
		CustomerComplaintTypeName customerComplaintTypeName =new CustomerComplaintTypeName();
		customerComplaintTypeName.setComplaintTypeCode(customerComplaintType.getCode());
		customerComplaintTypeName.setLanguageCode(customerComplaintType.getAdminLanguageCode());
		customerComplaintTypeName.setName(customerComplaintType.getName());
		
		this.daoSupport.insert("DT_CustomerComplaintType", customerComplaintType);
		complaintTypeNameService.insertCustomerComplaintTypeName(customerComplaintTypeName);
		
		return 0;
	}
	
	/**
	 * 更新客诉类型
	 * @param customerComplaintType
	 * 客诉类型
	 * @throws Exception
	 */
	@Transactional
	public int updateCustomerComplaintType(CustomerComplaintType customerComplaintType) throws Exception {
		this.daoSupport.update("DT_CustomerComplaintType", customerComplaintType, "code='"+customerComplaintType.getCode()+"'");
		return 0;
	}

	public CustomerComplaintType getCustomerComplaintTypeByCode(String code) throws Exception {
		String sql = "select * from DT_CustomerComplaintType where code = ?";
		
		return (CustomerComplaintType) this.daoSupport.queryForObject(sql, CustomerComplaintType.class, code);
	}

	@Transactional
	public int delCustomerComplaintType(String code) throws Exception {
		String sql = "delete from DT_CustomerComplaintType where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delCustomerComplaintTypeDetail(String code) throws Exception {		
		complaintTypeNameService.delCustomerComplaintTypeNameByTypeCode(code);

		this.delCustomerComplaintType(code);
		return 0;
	}

	@Transactional
	public int delCustomerComplaintTypes(String codes) throws Exception {
		String sql = "delete from DT_CustomerComplaintType where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}



	/**
	  * 客诉类型分页显示方法
	  * @param CustomerComplaintType
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageCustomerComplaintTypePage(CustomerComplaintType customerComplaintType, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.name name");
		sql.append(" from DT_CustomerComplaintType a");
		sql.append(" left join DT_CustomerComplaintTypeName c on a.code=c.complaintTypeCode and c.languageCode=?");
		sql.append(" where 1=1");
		
		if (null != customerComplaintType) {
			if (StringUtils.isNotBlank(customerComplaintType.getName())) {
				sql.append(" and c.name like '%");
				sql.append(customerComplaintType.getName().trim());
				sql.append("%'");
			}
		}		

		
//		if (StringUtils.isNotBlank(customerComplaintType.getAdminLanguageCode())) {
//			sql.append(" and c.languageCode = '");
//			sql.append(customerComplaintType.getAdminLanguageCode().trim());
//			sql.append("'");
//		}
		
		
		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, CustomerComplaintType.class,customerComplaintType.getAdminLanguageCode().trim());
		return page;
	}

}
