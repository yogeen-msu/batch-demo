package com.cheriscon.backstage.member.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import com.cheriscon.backstage.member.service.IVehicleService;
import com.cheriscon.common.model.Vehicle;
import com.cheriscon.cop.sdk.database.BaseSupport;


/**
 * @remark 车型选择service实现
 * @author chenqichuan
 * @date 2014-02-24
 * 
 */
@Service
public class VehicleServiceImpl extends BaseSupport<Vehicle> implements
		IVehicleService {

	@Override
	public List<Vehicle> getVehicleList(Vehicle vehicle)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		List<Vehicle> list = null;
		if (vehicle.getQueryType().equals("1")) { // 查询年份
			sql.append("select distinct(yearName) from DT_VEHICLE order by yearName desc");
			list = this.daoSupport.queryForList(sql.toString(), Vehicle.class);
		} else if (vehicle.getQueryType().equals("2")) { // 品牌
			sql.append("select distinct(mark) from DT_VEHICLE  where yearName = ? order by mark asc");
			list = this.daoSupport.queryForList(sql.toString(), Vehicle.class,
					vehicle.getYearName());
		} else if (vehicle.getQueryType().equals("3")) { // 型号
			sql.append("select distinct(model) from DT_VEHICLE  where yearName = ? and mark = ? order by model asc");
			list = this.daoSupport.queryForList(sql.toString(), Vehicle.class,
					vehicle.getYearName(), vehicle.getMark());
		} else if (vehicle.getQueryType().equals("4")) { // 子型号
			sql.append("select distinct(submodel) from DT_VEHICLE  where yearName = ? and mark = ? and model=? order by submodel asc");
			list = this.daoSupport.queryForList(sql.toString(), Vehicle.class,
					vehicle.getYearName(), vehicle.getMark(),
					vehicle.getModel());
		} else if (vehicle.getQueryType().equals("5")) { // 发动机
			sql.append("select distinct(engine) from DT_VEHICLE  where yearName = ? and mark = ? and model=? and submodel=? order by engine asc");
			list = this.daoSupport.queryForList(sql.toString(), Vehicle.class,
					vehicle.getYearName(), vehicle.getMark(),
					vehicle.getModel(), vehicle.getSubmodel());
		}

		return list;
	}

}
