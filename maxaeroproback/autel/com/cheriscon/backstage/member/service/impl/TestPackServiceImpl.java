package com.cheriscon.backstage.member.service.impl;



import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ITestPackService;
import com.cheriscon.backstage.member.vo.TestPackVo;

import com.cheriscon.backstage.member.service.ITestPackLanguagePackService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.TestPackLanguagePack;

import com.cheriscon.common.model.TestPack;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;

/**
 * 测试包业务逻辑实现类
 * @author pengdongan
 * @version 创建时间：2013-01-22
 */
@Service
public class TestPackServiceImpl extends BaseSupport implements ITestPackService
{

	
	@Resource
	private ITestPackLanguagePackService testPackLanguagePackService;

	/**
	 * 添加测试包全整信息
	 * @param TestPack ,List<TestPackLanguagePack>
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional
	public int insertTestPackDetail(TestPack testPack,List<TestPackLanguagePack> testPackLanguagePacks) throws Exception {
		String code = this.insertTestPack(testPack);
		
		if(null != testPackLanguagePacks && testPackLanguagePacks.size() > 0){
			for(TestPackLanguagePack testPackLanguagePack : testPackLanguagePacks){
				testPackLanguagePack.setTestPackCode(code);
				this.testPackLanguagePackService.addTestPackLanguagePack(testPackLanguagePack);
			}
		}
		return 0;
		
	}
	
	
	public int updateTestPackDetail(TestPack testPackVo) throws Exception{
		
		TestPack test=getTestPackByCode(testPackVo.getCode());
		test.setDownloadPath(testPackVo.getDownloadPath());
		//test.setName(testPackVo.getName());
		test.setProCode(testPackVo.getProCode());
		test.setSoftwareTypeCode(testPackVo.getSoftwareTypeCode());
		test.setVersionName(testPackVo.getVersionName());
		this.daoSupport.update("DT_TestPack", test, "id="+test.getId()+"");
		return 0;
	}
	
	/**
	 * 添加测试包
	 * @param testPack
	 * 测试包
	 * @throws Exception
	 */
	@Transactional
	public String insertTestPack(TestPack testPack) throws Exception {
		//版本创建时间
		String releaseDate = DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss");
		String code = DBUtils.generateCode(DBConstant.TEST_PACK);
		testPack.setCode(code);
		testPack.setReleaseDate(releaseDate);
		this.daoSupport.insert("DT_TestPack", testPack);
		return code;
	}

	public TestPack getTestPackByCode(String code) throws Exception {
		String sql = "select * from DT_TestPack where code = ?";
		
		return (TestPack) this.daoSupport.queryForObject(sql, TestPack.class, code);
	}

	@Transactional
	public int delTestPack(String code) throws Exception {
		String sql = "delete from DT_TestPack where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delTestPackDetail(String code) throws Exception {		
		this.testPackLanguagePackService.delTestPackLanguagePackByTestPackCode(code);

		this.delTestPack(code);
		return 0;
	}



	/**
	  * 测试包分页显示方法
	  * @param TestPackVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageTestPackVoPage(TestPackVo testPackVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,b.serialNo proSerialNo,e.name softwareName,d.name customerName,f.name proTypeName");
		sql.append(" from DT_TestPack a");
		sql.append(" left join DT_ProductInfo b on a.proCode=b.code");
		sql.append(" left join DT_CustomerProInfo c on a.proCode=c.proCode");
		sql.append(" left join DT_CustomerInfo d on c.customerCode=d.code");
		sql.append(" left join DT_SoftwareType e on a.softwareTypeCode=e.code");
		sql.append(" left join DT_ProductType f on b.proTypeCode=f.code");
		sql.append(" where 1=1");
		
		if (null != testPackVo) {
			if (StringUtils.isNotBlank(testPackVo.getVersionName())) {
				sql.append(" and a.versionName like '%");
				sql.append(testPackVo.getVersionName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(testPackVo.getProTypeName())) {
				sql.append(" and f.name like '%");
				sql.append(testPackVo.getProTypeName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(testPackVo.getProSerialNo())) {
				sql.append(" and a.proCode like '%");
				sql.append(testPackVo.getProSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(testPackVo.getSoftwareName())) {
				sql.append(" and e.name like '%");
				sql.append(testPackVo.getSoftwareName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(testPackVo.getCustomerName())) {
				sql.append(" and d.name like '%");
				sql.append(testPackVo.getCustomerName().trim());
				sql.append("%'");
			}
		}

		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, TestPackVo.class);
		return page;
	}

	

	/**
	 * 根据编码查询测试包业务对象
	 * @param code	测试包code
	 * @return
	 * @throws Exception
	 */
	@Override
	public TestPackVo getTestPackVoByCode(String code) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select  a.*,b.serialNo proSerialNo,e.name softwareName,d.name customerName,f.name proTypeName");
		sql.append(" from DT_TestPack a");
		sql.append(" left join DT_ProductInfo b on a.proCode=b.code");
		sql.append(" left join DT_CustomerProInfo c on a.proCode=c.proCode");
		sql.append(" left join DT_CustomerInfo d on c.customerCode=d.code");
		sql.append(" left join DT_SoftwareType e on a.softwareTypeCode=e.code");
		sql.append(" left join DT_ProductType f on b.proTypeCode=f.code");
		sql.append(" where a.code = ? limit 0,1");
		
		return (TestPackVo) this.daoSupport.queryForObject(sql.toString(), TestPackVo.class, code);
	}

}
