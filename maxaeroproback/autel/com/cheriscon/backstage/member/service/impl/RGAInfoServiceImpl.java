/**
 * 
 */
package com.cheriscon.backstage.member.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.member.service.IRGAInfoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.RGAInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.vo.RGAInfoVo;

/**
 * @author A13045
 *
 */
@Service
public class RGAInfoServiceImpl extends BaseSupport<RGAInfo> implements IRGAInfoService {


	@Override
	public Page pageRGAQuery(String auth,RGAInfo info, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql=new StringBuffer("");
		sql.append("select a.code as code,a.createUser as createUser ,d.name as createUserName,");
		sql.append("date_format(a.createDate, '%m/%d/%Y') as createDate,");
		sql.append("a.serialNo as serialNo,a.status as status,DATEDIFF(NOW() ,createDate) as dayNum");
		/*sql.append(",b.name as proTypeName");*/
		sql.append(" from DT_RGAInfo a,DT_SealerDataAuthDeails c,dt_sealerInfo d " );
		/*sql.append(" where a.proTypeCode=b.code ");*/
		sql.append(" where a.createUser=d.autelId");
		sql.append(" and a.createUser=c.sealerCode");
		sql.append(" and c.authCode like'").append(auth).append("%'");
		if(null != info ){
			if(!StringUtil.isEmpty(info.getProTypeCode()) && !info.getProTypeCode().equals("-1")){
				sql.append(" and a.proTypeCode like'%").append(info.getProTypeCode().trim()).append("%'");
			}
			if(!StringUtil.isEmpty(info.getSerialNo())){
				sql.append(" and a.serialNo like '%").append(info.getSerialNo().trim()).append("%'");
			}
			if(!StringUtil.isEmpty(info.getCode())){
				sql.append(" and a.code like '%").append(info.getCode().trim()).append("%'");
			}
		}
		sql.append(" order by a.id desc");
		
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}
	
	public Page pageRGAQuery(RGAInfoVo rgaInfoVo) throws Exception {
		StringBuffer sql = new StringBuffer("");
		sql.append("select a.code as code,a.createUser as createUser ,d.name as createUserName,");
		sql.append("date_format(a.createDate, '%m/%d/%Y') as createDate,");
		sql.append("a.serialNo as serialNo,a.status as status,DATEDIFF(NOW() ,createDate) as dayNum");
		/*sql.append(",b.name as proTypeName");*/
		sql.append(" from DT_RGAInfo a,DT_SealerDataAuthDeails c,dt_sealerInfo d " );
		/*sql.append(" where a.proTypeCode=b.code ");*/
		sql.append(" where a.createUser=d.autelId");
		sql.append(" and a.createUser=c.sealerCode");
		sql.append(" and c.authCode like'").append(rgaInfoVo.getAuth()).append("%'");
		if(null != rgaInfoVo ){
			if(!StringUtil.isEmpty(rgaInfoVo.getProTypeCode()) && !rgaInfoVo.getProTypeCode().equals("-1")){
				sql.append(" and a.proTypeCode like'%").append(rgaInfoVo.getProTypeCode().trim()).append("%'");
			}
			if(!StringUtil.isEmpty(rgaInfoVo.getSerialNo())){
				sql.append(" and a.serialNo like '%").append(rgaInfoVo.getSerialNo().trim()).append("%'");
			}
			if(!StringUtil.isEmpty(rgaInfoVo.getCode())){
				sql.append(" and a.code like '%").append(rgaInfoVo.getCode().trim()).append("%'");
			}
		}
		sql.append(" order by a.id desc");
		
		return this.daoSupport.queryForPage(sql.toString(), rgaInfoVo.getPageNo(), rgaInfoVo.getPageSize());
	}
	
	@Override
	public void saveRGAInfo(RGAInfo info) throws Exception {
		info.setCode(getMaxNum());
		this.daoSupport.insert("DT_RGAInfo", info);
	}

	public String getMaxNum() throws Exception{
		String sql="select * from dt_rgainfo order by id desc limit 0,1";
		RGAInfo info=this.daoSupport.queryForObject(sql, RGAInfo.class);
		String DateStr=DateUtil.toString(new Date(), "yyyyMMdd");
		String RGACODE="";
		if(info==null){
			RGACODE= "RGA"+DateStr+"-"+"00001";
		}else{
			
			String code=info.getCode();
			int startLenth=code.indexOf("-");
			String yyyymmdd=code.substring(3, startLenth);
			if(DateStr.equals(yyyymmdd)){
				String lastCode=code.substring(startLenth+1);
				lastCode=String.valueOf(Integer.valueOf(lastCode)+1);
				RGACODE="RGA"+DateStr+"-"+this.addZeroForNum(lastCode, 5);
			}else{
				RGACODE="RGA"+DateStr+"-"+"00001";
			}
			
		}
		return RGACODE;
	}
	
	public  String addZeroForNum(String str, int strLength) {
		int strLen = str.length();
		if (strLen < strLength) {
			while (strLen < strLength) {
			StringBuffer sb = new StringBuffer();
			sb.append("0").append(str);
			str = sb.toString();
			strLen = str.length();
			}
		}

			return str;
		}
	
	
	@Override
	public void updateRGAInfo(RGAInfo info)
			throws Exception {
		this.daoSupport.update("DT_RGAInfo", info, "id="+info.getId()+"");

	}

	@Override
	public void deleteRGAInfo(String code) throws Exception {
		String sql = "delete from DT_RGAInfo where code= ?";
		this.daoSupport.execute(sql,code);

	}

	public RGAInfo getRGAInfoByCode(String code) throws Exception{
		String sql="select a.*,d.name as createUserName from DT_RGAInfo a,dt_sealerInfo d where  a.createUser=d.autelId and a.code=?";
		return this.daoSupport.queryForObject(sql, RGAInfo.class, code);
	}
}
