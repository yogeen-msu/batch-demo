package com.cheriscon.backstage.member.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IOnlineServiceTypeNameService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.OnlineServiceTypeName;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 在线客服类型名称业务逻辑实现类
 * @author pengdongan
 * @date 2013-06-02
 */
@Service
public class OnlineServiceTypeNameServiceImpl extends BaseSupport<OnlineServiceTypeName> implements IOnlineServiceTypeNameService
{
	@Resource
	private ILanguageService languageService;
	
	/**
	 * 查询所有在线客服类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<OnlineServiceTypeName> listAll() throws Exception {
		String sql = "select * from DT_OnlineServiceTypeName order by id desc";
		
		return this.daoSupport.queryForList(sql, OnlineServiceTypeName.class);
	}
	/**
	 * 按语言查询所有在线客服类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<OnlineServiceTypeName> listOnlineTypeNameByLanguageCode(String languageCode) throws Exception {
		String sql = "select * from DT_OnlineServiceTypeName where languageCode =? order by id desc";
		
		return this.daoSupport.queryForList(sql, OnlineServiceTypeName.class, languageCode);
	}

	/**
	 * 根据在线客服类型查询在线客服类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<OnlineServiceTypeName> queryByTypeCode(String typeCode) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,b.name languageName");
		sql.append(" from DT_OnlineServiceTypeName a");
		sql.append(" left join DT_Language b on a.languageCode=b.code");
		sql.append(" where a.onlineTypeCode = ? order by a.id desc");
		
		return this.daoSupport.queryForList(sql.toString(), OnlineServiceTypeName.class, typeCode);
	}
	
	
	/**
	 * 添加在线客服类型名称
	 * @param onlineServiceTypeName
	 * 在线客服类型名称
	 * @throws Exception
	 */
	@Transactional
	public int insertOnlineServiceTypeName(OnlineServiceTypeName onlineServiceTypeName) throws Exception {
		this.daoSupport.insert("DT_OnlineServiceTypeName", onlineServiceTypeName);
		return 0;
	}
	
	/**
	 * 更新在线客服类型名称
	 * @param onlineServiceTypeName
	 * 在线客服类型名称
	 * @throws Exception
	 */
	@Transactional
	public int updateOnlineServiceTypeName(OnlineServiceTypeName onlineServiceTypeName) throws Exception {
		this.daoSupport.update("DT_OnlineServiceTypeName", onlineServiceTypeName, "onlineTypeCode='"+onlineServiceTypeName.getOnlineTypeCode()+"'");
		return 0;
	}

	@Override
	@Transactional
	public void updateOnlineServiceTypeNames(
			List<OnlineServiceTypeName> onlineServiceTypeNames, List<OnlineServiceTypeName> delOnlineServiceTypeNames)
			throws Exception {
		//删除
		if(null != delOnlineServiceTypeNames && delOnlineServiceTypeNames.size() > 0){
			for(OnlineServiceTypeName onlineServiceTypeName : delOnlineServiceTypeNames){
				this.delOnlineServiceTypeName(onlineServiceTypeName.getId());
			}
		}
		//添加
		if(null != onlineServiceTypeNames && onlineServiceTypeNames.size() > 0){
			for(OnlineServiceTypeName onlineServiceTypeName : onlineServiceTypeNames){
				this.insertOnlineServiceTypeName(onlineServiceTypeName);
			}
		}
	}

	public OnlineServiceTypeName getOnlineServiceTypeNameByCode(int id) throws Exception {
		String sql = "select * from DT_OnlineServiceTypeName where id = ?";
		
		return (OnlineServiceTypeName) this.daoSupport.queryForObject(sql, OnlineServiceTypeName.class, id);
	}

	@Transactional
	public int delOnlineServiceTypeName(int id) throws Exception {
		String sql = "delete from DT_OnlineServiceTypeName where id= ?";
		this.daoSupport.execute(sql,id);
		return 0;
	}

	@Transactional
	public int delOnlineServiceTypeNameByTypeCode(String onlineTypeCode) throws Exception {
		String sql = "delete from DT_OnlineServiceTypeName where onlineTypeCode= ?";
		this.daoSupport.execute(sql,onlineTypeCode);
		return 0;
	}



	/**
	  * 在线客服类型名称分页显示方法
	  * @param OnlineServiceTypeName
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageOnlineServiceTypeNamePage(OnlineServiceTypeName onlineServiceTypeName, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,c.name name");
		sql.append(" from DT_OnlineServiceTypeName a");
		sql.append(" left join DT_OnlineServiceTypeNameName c on a.code=c.onlineTypeCode");
		sql.append(" where 1=1");
		
//		if (null != onlineServiceTypeName) {
//			if (StringUtils.isNotBlank(onlineServiceTypeName.getName())) {
//				sql.append(" and c.name like '%");
//				sql.append(onlineServiceTypeName.getName().trim());
//				sql.append("%'");
//			}
//		}		
//
//		
//		if (StringUtils.isNotBlank(onlineServiceTypeName.getAdminLanguageCode())) {
//			sql.append(" and c.languageCode = '");
//			sql.append(onlineServiceTypeName.getAdminLanguageCode().trim());
//			sql.append("'");
//		}
		
		
		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, OnlineServiceTypeName.class);
		return page;
	}


	@Override
	public OnlineServiceTypeName getOnlineServiceTypeNameByCode(
			String code) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public OnlineServiceTypeName getOnlineServiceTypeName(
			String onlineTypeCode, String languageCode) throws Exception 
	{
		String sql = "select * from DT_OnlineServiceTypeName where onlineTypeCode=? and languageCode =? ";
		
		return this.daoSupport.queryForObject(sql, OnlineServiceTypeName.class, onlineTypeCode,languageCode);
	}

	/**
	  * <p>Title: getAvailableLanguage</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<Language> getAvailableLanguage(String typeCode)
			throws Exception {
		List<Language> list = new ArrayList<Language>();
		List<Language> languages = languageService.queryLanguage();
		if(null == languages || languages.size() == 0){
			return list;
		}
		List<OnlineServiceTypeName> memos = this.queryByTypeCode(typeCode);
		if(null == memos || memos.size() == 0){
			return languages;
		}
		Map<String , OnlineServiceTypeName> map = this.convertListToMap(memos);
		for(Language language : languages){
			if(!map.containsKey(language.getCode()))
				list.add(language);
		}
		return list;
	}
	
	private Map<String , OnlineServiceTypeName> convertListToMap(List<OnlineServiceTypeName> memos) throws Exception{
		Map<String , OnlineServiceTypeName> map = new HashMap<String , OnlineServiceTypeName>();
		for(OnlineServiceTypeName memo : memos){
			map.put(memo.getLanguageCode(), memo);
		}
		return map;
	}

}
