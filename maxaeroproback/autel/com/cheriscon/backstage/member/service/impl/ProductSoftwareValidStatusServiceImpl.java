package com.cheriscon.backstage.member.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeErrorLogService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeLogService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.vo.ProductSoftwareValidStatusMapper;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.constant.BackageConstant;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidChangeErrorLog;
import com.cheriscon.common.model.ProductSoftwareValidChangeLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

@Service
public class ProductSoftwareValidStatusServiceImpl extends BaseSupport<ProductSoftwareValidStatus> 
			implements IProductSoftwareValidStatusService {
	
	@Resource
	private IProductInfoService productInfoService; 
	@Resource 
	private IProductSoftwareValidChangeLogService productSoftwareValidChangeLogService;
	@Resource
	private IProductSoftwareValidChangeErrorLogService productSoftwareValidChangeErrorLogService;
	
	@Transactional
	public boolean saveProductSoftwareValidStatus(ProductSoftwareValidStatus productSoftwareValidStatus) {
		String tableName = DBUtils.getTableName(ProductSoftwareValidStatus.class);
		productSoftwareValidStatus.setCode(DBUtils.generateCode(DBConstant.PRO_SOFTWARE_VALID_STATUS));
		this.daoSupport.insert(tableName, productSoftwareValidStatus);
		return true;
	}

	@Transactional
	public boolean updateValidDate(ProductSoftwareValidStatus softwareValidStatus) {
		try{
		String tableName = DBUtils.getTableName(ProductSoftwareValidStatus.class);
		StringBuffer update = new StringBuffer("update " + tableName + " set ");
		update.append(" validDate=? ");
		update.append(" where id=? ");
		this.daoSupport.execute(update.toString(),new Object[]{softwareValidStatus.getValidDate(),softwareValidStatus.getId()});
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}
	
	@Transactional
	public boolean updateValidDate(String validDate,String proCode)
			throws Exception 
	{
		String sql ="update DT_ProductSoftwareValidstatus set validDate=? where proCode=? ";
		this.daoSupport.execute(sql,new Object[]{validDate,proCode});
		return true;
	}

	@Transactional
	public boolean delProductSoftwareValidStatusById(int id) {
		String tableName = DBUtils.getTableName(ProductSoftwareValidStatus.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	public Page pageProductSoftwareValidStatus(ProductSoftwareValidStatus validStatus, int pageNo, int pageSize) {
	    StringBuffer sql=new StringBuffer();
	    sql.append("select pro.serialNo as pif_serialNo");
	    sql.append(",b.name as sct_sealName");
	    sql.append(",(select distinct validDate from DT_ProductSoftwareValidStatus a,DT_MinSaleUnitSaleCfgDetail d where a.proCode=pro.code and a.minSaleUnitCode=d.minSaleUnitCode  and d.saleCfgCode=c.code ) as validDate");
	    sql.append(",cust.autelid as cus_autelid");
	    sql.append(",sealer.autelId as seal_autelid");
	    sql.append(" ,area.name as acf_name");
	    sql.append(" from ");
	    sql.append(" dt_productinfo pro");
	    sql.append(" left join DT_SaleContract b on b.code=pro.saleContractCode");
	    sql.append(" left join DT_SaleConfig c on c.code=b.saleCfgCode");
	  //  sql.append(" left join DT_MinSaleUnitSaleCfgDetail m on m.saleCfgCode=c.code");
	    sql.append(" left join DT_CustomerProInfo custPro on custPro.proCode=pro.code");
	    sql.append(" left join DT_CustomerInfo cust on cust.code=custpro.customerCode");
	    sql.append(" left join DT_SealerInfo sealer on sealer.code=b.sealerCode");
	    sql.append(" left join DT_AreaConfig area on area.code=b.areaCfgCode");
		
		sql.append(" where 1=1 and pro.regStatus='1'");
		
		if(validStatus != null){
			//客户ID
			if(StringUtils.isNotEmpty(validStatus.getCusAutelid())){	
				sql.append(" and cust.autelId like '%"+validStatus.getCusAutelid()+"%'");
			}
			
			//产品序列号
			if(validStatus.getProductInfo() != null && !StringUtil.isEmpty(validStatus.getProductInfo().getSerialNo())){	
				sql.append(" and pro.serialNo like '%"+validStatus.getProductInfo().getSerialNo()+"%'");
			}
			
			//经销商ID
			if(StringUtils.isNotEmpty(validStatus.getSealAutelid())){	
				sql.append(" and sealer.autelId like '%"+validStatus.getSealAutelid()+"%'");
			}
			
			//区域配置
			if(validStatus.getAreaConfig() != null && !StringUtil.isEmpty(validStatus.getAreaConfig().getName())){	
				sql.append(" and area.name like '%"+validStatus.getAreaConfig().getName()+"%'");
			}
		}
		
		sql.append(" order by pro.serialNo asc ");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,new ProductSoftwareValidStatusMapper(),new Object[]{});
		return page;
	}

	@Override
	public ProductSoftwareValidStatus getProductSoftwareValidStatus(
			String proCode,String minSaleUnitCode) throws Exception
	{
		String sql = "select * from DT_ProductSoftwareValidStatus where proCode='"+proCode+"' and minSaleUnitCode='"+minSaleUnitCode+"'";
		return this.daoSupport.queryForObject(sql, ProductSoftwareValidStatus.class);
	}

	/**
	  * <p>Title: updateRenewal</p>
	  * <p>Description: </p>
	  * @param proCode
	  * @param year
	  * @throws Exception
	  */ 
	@Override
	public void updateRenewal(String proCode,String minSaleCode, String year) throws Exception {
			ProductSoftwareValidStatus softwareValidStatus =this.getProductSoftwareValidStatus(proCode,minSaleCode);
			//默认有效等于当前时间
			String validDate = null;
			String oldDate=null;
			if(null != softwareValidStatus && StringUtils.isNotBlank(softwareValidStatus.getValidDate())){
				validDate = softwareValidStatus.getValidDate();
				oldDate=softwareValidStatus.getValidDate();
				Date date = DateUtil.toDate(softwareValidStatus.getValidDate().trim(), null);
				Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), null), null);
				//当前时间大于有效期时间(已经过期)
				if(currentDate.after(date)){
					validDate  = DateUtil.toString(currentDate,null);
				}
			}else{
				ProductSoftwareValidChangeErrorLog errorLog = new ProductSoftwareValidChangeErrorLog();
				errorLog.setProCode(proCode);
				errorLog.setMinSaleUnit(minSaleCode);
				errorLog.setCreateTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
				errorLog.setRemark("没有找到对应的数据");
				errorLog.setIsUpdate("0");
				productSoftwareValidChangeErrorLogService.saveErrorLog(errorLog);
				return ;
			}
			StringBuffer sql = new StringBuffer();                               
			sql.append("update DT_ProductSoftwareValidStatus set validDate = DATE_ADD(DATE_FORMAT(?,'%Y-%m-%d'),INTERVAL ? YEAR) where proCode = ? and minSaleUnitCode=?");
			this.daoSupport.execute(sql.toString(), validDate,Integer.valueOf(year),proCode,minSaleCode);
			
			//20140829 add chenqichuan
			ProductInfo product=productInfoService.getByProCode(proCode);
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			String OperDate=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			ProductSoftwareValidChangeLog log=new ProductSoftwareValidChangeLog();
			log.setMinSaleUnit(minSaleCode);
			log.setNewDate(DateUtil.dateAddYear(validDate,year));
			log.setOldDate(oldDate);
			log.setOperatorIp(FrontConstant.getIpAddr(request));
			log.setOperatorTime(OperDate);
			
			/*Object obj=SessionUtil.getLoginUserInfo(request);*/
			String operateUser="";
			/*if(obj.getClass().equals(CustomerInfo.class)){
				operateUser=((CustomerInfo)obj).getAutelId();
			}
			if(obj.getClass().equals(SealerInfo.class)){
				operateUser=((SealerInfo)obj).getAutelId();
			}*/
			
			log.setOperatorUser(operateUser);
			log.setOperType(BackageConstant.PRODUCT_VALID_CHANGE_ONLINE);
			log.setProductSN(product.getSerialNo());
			productSoftwareValidChangeLogService.saveProductSoftwareValidStatus(log);
			
			
	}

	@Override
	public List<ProductSoftwareValidStatus> getProductSoftwareValidStatusList(
			String proCode) throws Exception 
	{
		String sql="select * from DT_ProductSoftwareValidStatus where proCode='"+proCode+"'";
		return this.daoSupport.queryForList(sql, ProductSoftwareValidStatus.class);
	}
	@Override
	public List<ProductSoftwareValidStatus> getProductSoftwareValidList(String productSN)throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append(" select a.* from DT_ProductSoftwareValidStatus a,");
		sql.append(" DT_ProductInfo b,");
		sql.append(" DT_SaleContract c,");
		sql.append(" DT_SaleConfig e ,");
		sql.append(" DT_MinSaleUnitSaleCfgDetail d");
		sql.append(" where a.proCode=b.code");
		sql.append(" and b.saleContractCode=c.code");
		sql.append(" and c.saleCfgCode =e.code");
		sql.append(" and e.code=d.saleCfgCode");
		sql.append(" and a.minSaleUnitCode=d.minSaleUnitCode");
		sql.append(" and b.serialNo='"+productSN+"'");
		return this.daoSupport.queryForList(sql.toString(), ProductSoftwareValidStatus.class);
	}
	@Override
	public void deleteMoreMinCode(String proCode) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append(" delete from DT_ProductSoftwareValidStatus where proCode=? and minSaleUnitCode not in( ");
		sql.append(" select d.minSaleUnitCode from dt_productinfo a ,DT_SaleContract b ,DT_SaleConfig c,DT_MinSaleUnitSaleCfgDetail d ");
		sql.append(" where a.saleContractCode=b.code and b.saleCfgCode=c.code and c.code=d.saleCfgCode and  ");
		sql.append(" a.code=? )");
		this.daoSupport.execute(sql.toString(),proCode,proCode);
	}
	@Transactional
	public void deleteByProCode(String proCode) throws Exception{
		StringBuffer sql=new StringBuffer("delete from DT_ProductSoftwareValidStatus where proCode=?");
		this.daoSupport.execute(sql.toString(),proCode);
	}
}
