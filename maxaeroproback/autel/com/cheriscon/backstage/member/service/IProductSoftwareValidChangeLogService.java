package com.cheriscon.backstage.member.service;


import com.cheriscon.common.model.ProductSoftwareValidChangeLog;
import com.cheriscon.framework.database.Page;

public interface IProductSoftwareValidChangeLogService {

	/**
	 * 增加
	 * 
	 * @param productSoftwareValidStatus
	 */
	public boolean saveProductSoftwareValidStatus(
			ProductSoftwareValidChangeLog log);



	/**
	 * 翻页查询所有软件有效
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageProductSoftwareValidStatus(
			ProductSoftwareValidChangeLog log, int pageNo, int pageSize);

}
