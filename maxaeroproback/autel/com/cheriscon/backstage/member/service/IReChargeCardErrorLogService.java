package com.cheriscon.backstage.member.service;


import com.cheriscon.common.model.ReChargeCardErrorLog;
import com.cheriscon.framework.database.Page;

public interface IReChargeCardErrorLogService {
	
	/**
	 * 续租错误日志对象
	 * @param ReChargeCardErrorLog	
	 * @return	Page
	 * @throws Exception
	 */
	public Page pageReChargeCardError(ReChargeCardErrorLog log, int pageNo,int pageSize) throws Exception;
			
	

	/**
	 * 保存续租错误日志
	 * @param ReChargeCardErrorLog	
	 * @return	
	 * @throws Exception
	 */
	public void saveErrorLog(ReChargeCardErrorLog log) throws Exception;
	
	
	/**
	 * 更新续租错误日志
	 * @param customerCode	
	 * @return	
	 * @throws Exception
	 */
	public void updateErrorLog(String updateUser,String UpdateTime,String customerCode) throws Exception;
	
	
	/**
	 * 根据编码获得对象
	 * @param code	
	 * @return	ReChargeCardErrorLog
	 * @throws Exception
	 */
	public ReChargeCardErrorLog getErrorLogByCode(String code) throws Exception;
	

	/**
	 * 根据用户编码查询用户错误记录次数
	 * @param code	
	 * @return	ReChargeCardErrorLog
	 * @throws Exception
	 */
	public int getErrorLogCount(String customerCode) throws Exception;
	
}
