package com.cheriscon.backstage.member.service;

import com.cheriscon.common.model.ProductSoftwareValidChangeErrorLog;
import com.cheriscon.framework.database.Page;

public interface IProductSoftwareValidChangeErrorLogService {
	
	public void saveErrorLog(ProductSoftwareValidChangeErrorLog log);
	
	public Page pageErrorLog(ProductSoftwareValidChangeErrorLog log,int pageNo, int pageSize);
	public boolean updateSoftwareValid(String proCode);

}
