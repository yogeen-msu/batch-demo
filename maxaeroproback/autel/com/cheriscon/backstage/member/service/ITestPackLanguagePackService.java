package com.cheriscon.backstage.member.service;



import java.util.List;

import com.cheriscon.common.model.TestPackLanguagePack;

/**
 * @ClassName: ITestPackLanguagePackService
 * @Description: 测试包语言包业务类
 * @author pengdongan
 * @date 2013-01-22
 * 
 */
public interface ITestPackLanguagePackService {
	
	/**
	 * 
	* @Title: listAll
	* @Description: 查询测试包语言包集合
	* @param    
	* @return List<TestPackLanguagePack>    
	* @throws
	 */
	public List<TestPackLanguagePack> listAll() throws Exception;
	
	/**
	 * 
	* @Title: addTestPackLanguagePack
	* @Description: 添加测试包语言包
	* @param    测试包语言包 对象
	* @return void    
	* @throws
	 */
	public int addTestPackLanguagePack(TestPackLanguagePack testPackLanguagePack) throws Exception;

	
	/**
	 * 
	* @Title: addTestPackLanguagePack
	* @Description: 更新测试包语言包
	* @param    测试包语言包 对象
	* @return void    
	* @throws
	 */
	public int updateTestPackLanguagePack(TestPackLanguagePack testPackLanguagePack) throws Exception;

	
	/**
	 * 
	* @Title: delTestPackLanguagePack
	* @Description: 根据测试包语言包code 删除测试包语言包
	* @param    code  测试包语言包code
	* @return void    
	* @throws
	 */
	public int delTestPackLanguagePack(String code)  throws Exception;
	
	/**
	 * 
	* @Title: delTestPackLanguagePackByTestPackCode
	* @Description: 根据测试包编码删除测试包语言包
	* @param   testPackCode 测试包编码
	* @return void    
	* @throws
	 */
	public int delTestPackLanguagePackByTestPackCode(String testPackCode) throws Exception;

	
	/**
	 * 
	* @Title: listLanguagePackByTestPackCode
	* @Description: 根据测试包编码查询测试包语言包集合
	* @param    
	* @return List<TestPackLanguagePack>    
	* @throws
	 */
	public List<TestPackLanguagePack> listLanguagePackByTestPackCode(String testPackCode) throws Exception;
}

