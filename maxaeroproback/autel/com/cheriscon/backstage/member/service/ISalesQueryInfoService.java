package com.cheriscon.backstage.member.service;

import com.cheriscon.common.model.SalesQueryInfo;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.distributor.vo.SalesQueryInfoVo;

public interface ISalesQueryInfoService {

	
	//分页展示
	public Page pageSalesQuery(String auth,SalesQueryInfo salesQueryInfo, int pageNo,int pageSize) throws Exception;
	
	public void saveSalesQuery(SalesQueryInfo salesQueryInfo) throws Exception;
	
	public void updateSalesQuery(SalesQueryInfo salesQueryInfo) throws Exception;
	
	public void deleteSalesQueryInfo(String code) throws Exception;
	
	public SalesQueryInfo getSalesQueryInfoByCode(String code) throws Exception;
	
	public Page pageSalesQuery(SalesQueryInfoVo salesQueryInfoVo) throws Exception;
}
