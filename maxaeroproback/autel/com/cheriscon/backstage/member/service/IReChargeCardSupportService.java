package com.cheriscon.backstage.member.service;

import com.cheriscon.common.model.ReChargeCardSupport;
import com.cheriscon.framework.database.Page;

public interface IReChargeCardSupportService {
	
	/**
	 * 续租支持对象
	 * @param ReChargeCardSupport	
	 * @return	Page
	 * @throws Exception
	 */
	public Page pageReChargeCardSupport(ReChargeCardSupport support, int pageNo,int pageSize) throws Exception;
			
	

	/**
	 * 保存续租错误日志
	 * @param ReChargeCardSupport	
	 * @return	
	 * @throws Exception
	 */
	public void saveCardSupport(ReChargeCardSupport support) throws Exception;
	
	
	/**
	 * 更新支持状态
	 * @param code	
	 * @return	
	 * @throws Exception
	 */
	public void updateCardSupportStatus(String code) throws Exception;
	
	
	/**
	 * 根据编码获得对象
	 * @param code	
	 * @return	ReChargeCardErrorLog
	 * @throws Exception
	 */
	public ReChargeCardSupport getErrorLogByCode(String code) throws Exception;
	

}
