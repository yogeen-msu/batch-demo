package com.cheriscon.backstage.member.service;

import java.util.List;


import com.cheriscon.common.model.CustomerComplaintSource;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客诉来源service接口
 * @author pengdongan
 * @date 2013-03-31
 * 
 */

public interface ICustomerComplaintSourceService {

	
	/**
	 * 
	 * @param customerComplaintSource	客诉来源对象
	 * @return	
	 * @throws Exception
	 */
	public List<CustomerComplaintSource> queryCustomerComplaintSource() throws Exception;
	
	/**
	 * 
	 * @param customerComplaintSource	客诉来源业务对象
	 * @return	
	 * @throws Exception
	 */
	public List<CustomerComplaintSource> listCustomerComplaintSource(CustomerComplaintSource customerComplaintSource) throws Exception;
	

	

	
	/**
	 * 添加客诉来源对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertCustomerComplaintSource(CustomerComplaintSource customerComplaintSource) throws Exception;
	
	/**
	 * 更新客诉来源对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateCustomerComplaintSource(CustomerComplaintSource customerComplaintSource) throws Exception;
	
	/**
	 * 根据编码查询客诉来源对象
	 * @param code	客诉来源code
	 * @return
	 * @throws Exception
	 */
	public CustomerComplaintSource getCustomerComplaintSourceByCode(String code) throws Exception;
	
	/**
	 * 根据名称查询客诉来源对象
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public CustomerComplaintSource getCustomerComplaintSourceByName(String name) throws Exception;
	
	/**
	 * 根据编码删除客诉来源对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delCustomerComplaintSource(String code) throws Exception;
	
	/**
	 * 根据删除多个客诉来源对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delCustomerComplaintSources(String codes) throws Exception;

	/**
	  * 客诉来源分页显示方法
	  * @param CustomerComplaintSource
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerComplaintSourcePage(CustomerComplaintSource customerComplaintSource, int pageNo,
			int pageSize) throws Exception;
	
}
