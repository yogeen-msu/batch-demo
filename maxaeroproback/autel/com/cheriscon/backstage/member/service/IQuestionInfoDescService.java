package com.cheriscon.backstage.member.service;



import java.util.List;

import com.cheriscon.common.model.QuestionInfo;
import com.cheriscon.framework.database.Page;

/**
 * 安全问题业务逻辑实现接口类
 * @author pengdongan
 * @date 2013-01-24
 */
public interface IQuestionInfoDescService 
{	
	/**
	 * 查询所有安全问题对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<QuestionInfo> listAll() throws Exception;
	
	/**
	 * 按语言查询所有安全问题对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	public List<QuestionInfo> listQuestionInfoByLanguageCode(String languageCode) throws Exception;
	
	/**
	 * 添加安全问题对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertQuestionInfo(QuestionInfo questionInfo) throws Exception;
	
	/**
	 * 更新安全问题对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateQuestionInfo(QuestionInfo questionInfo) throws Exception;
	
	/**
	 * 根据编码查询安全问题对象
	 * @param code	安全问题code
	 * @return
	 * @throws Exception
	 */
	public QuestionInfo getQuestionInfoByCode(String code) throws Exception;
	
	/**
	 * 根据编码删除安全问题对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delQuestionInfo(String code) throws Exception;
	
	/**
	 * 根据编码删除安全问题及其名称
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delQuestionInfoDetail(String code) throws Exception;
	
	/**
	 * 根据删除多个安全问题对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delQuestionInfos(String codes) throws Exception;

	/**
	  * 安全问题分页显示方法
	  * @param QuestionInfo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageQuestionInfoPage(QuestionInfo questionInfo, int pageNo,
			int pageSize) throws Exception;
	

}
