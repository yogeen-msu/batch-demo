package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.backstage.member.vo.CustomerInfoVo;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客户信息service接口
 * @author pengdongan
 * @date 2013-01-07
 * 
 */

public interface ICustomerInfoVoService {
	
	/**
	 * 
	 * @param customerInfoVo	客户信息业务对象
	 * @return	
	 * @throws Exception
	 */
	public List<CustomerInfoVo> listCustomer(CustomerInfoVo customerInfoVo) throws Exception;
	

	/**
	 * 查询客户消费记录
	 * @param customerInfoVo
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page queryCustomerOrderInfo(CustomerInfoVo customerInfoVo, int pageNo, int pageSize);



	/**
	  * 客户信息分页显示方法
	  * @param CustomerInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerInfoVoPage(CustomerInfoVo customerInfoVo, int pageNo,
			int pageSize) throws Exception;
	
	/**
	  * 根据国内国外客户进行区分
	  * @param CustomerInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @type pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerInfoVoPage2(String type,CustomerInfoVo customerInfoVo, int pageNo,
			int pageSize) throws Exception;
	
}
