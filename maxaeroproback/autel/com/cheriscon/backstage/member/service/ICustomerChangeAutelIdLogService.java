package com.cheriscon.backstage.member.service;

import com.cheriscon.common.model.CustomerChangeAutelIdLog;
import com.cheriscon.framework.database.Page;

public interface ICustomerChangeAutelIdLogService {
	
	public Page pageCustomerChangeAutelPage(CustomerChangeAutelIdLog log, int pageNo,
			int pageSize) throws Exception;
	
	public void saveLog(CustomerChangeAutelIdLog log) throws Exception;

}
