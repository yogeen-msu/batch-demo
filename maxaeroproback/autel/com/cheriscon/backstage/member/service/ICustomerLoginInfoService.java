package com.cheriscon.backstage.member.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客户登录信息service接口
 * @author pengdongan
 * @date 2013-01-14
 * 
 */

public interface ICustomerLoginInfoService {
	
	/**
	 * 
	 * @param customerLoginInfoVo	客户登录信息业务对象
	 * @return	
	 * @throws Exception
	 */
	public List<CustomerLoginInfoVo> listSealer(CustomerLoginInfoVo customerLoginInfoVo) throws Exception;
	

	
	/**
	 * 根据编码查询客户登录信息对象
	 * @param code	客户登录信息code
	 * @return
	 * @throws Exception
	 */
	public CustomerLoginInfoVo getSealerByCode(String code) throws Exception;

	
	/**
	 * 清空客户登录信息对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	public int clearCustomerLoginInfo() throws Exception;

	/**
	  * 客户登录信息分页显示方法
	  * @param CustomerLoginInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerLoginInfoVoPage(CustomerLoginInfoVo customerLoginInfoVo, int pageNo,
			int pageSize) throws Exception;
	
}
