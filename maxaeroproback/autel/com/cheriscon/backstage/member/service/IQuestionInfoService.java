package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.common.model.QuestionInfo;
import com.cheriscon.framework.database.Page;

public interface IQuestionInfoService {
	
	/**
	 * 增加
	 * @param questionInfo
	 */
	public boolean saveQuestionInfo(QuestionInfo questionInfo);
	
	/**
	 * 修改
	 * @param questionInfo
	 */
	public boolean updateQuestionInfo(QuestionInfo questionInfo);
	
	/**
	 * 删除
	 */
	public boolean delQuestionInfoById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public QuestionInfo getById(int id);
	
	
	/**
	 * 根据code查询
	 * @param code
	 * @return
	 */
	public QuestionInfo getByCode(String code);
	
	/**
	 * 查询出所有安全问题
	 * @return
	 */
	public List<QuestionInfo> queryQuestionInfo();
	
	/**
	 * 翻页查询所有安全问题
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageQuestionInfo(int pageNo, int pageSize);

}
