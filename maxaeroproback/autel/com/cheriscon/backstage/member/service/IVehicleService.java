package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.common.model.Vehicle;

public interface IVehicleService {

	
	public List<Vehicle> getVehicleList(Vehicle vehicle) throws Exception;
}
