package com.cheriscon.backstage.member.service;

import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.framework.database.Page;


public interface ICustomerChangeAutelService {

    public Page getCustomerChangeAutels(CustomerChangeAutel customerChangeAutel,int pageNo,int pageSize) throws Exception;
    
    public void updateCustomerChangeAutelStatus(CustomerChangeAutel customerChangeAutel) throws Exception;
}
