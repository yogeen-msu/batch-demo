package com.cheriscon.backstage.member.service;


import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.common.model.ComplaintAdminuserInfo;
import com.cheriscon.framework.database.Page;

/**
 * 客诉权限接口类
 * @author pengdongan
 * @date 2013-01-18
 */
public interface IComplaintAdminuserInfoService
{

	/**
	  * 客诉权限分页显示方法
	  * @param ComplaintAdminuserInfo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageComplaintAdminuserInfoPage(ComplaintAdminuserInfo complaintAdminuserInfo, int pageNo,
			int pageSize) throws Exception;
	
	/**
	 * 分配客服
	 * @param code,acceptor
	 * @throws Exception
	 */
	public int allotAcceptor(String code,String acceptor) throws Exception;

	
	/**
	 * 添加信息
	 * @param complaintAdminuserInfo
	 * 经销商信息
	 * @throws Exception
	 */
	public int insertComplaintAdminuserInfo(ComplaintAdminuserInfo complaintAdminuserInfo) throws Exception;
	
	/**
	 * 更新信息
	 * @param complaintAdminuserInfo
	 * 经销商信息
	 * @throws Exception
	 */
	public int updateComplaintAdminuserInfo(ComplaintAdminuserInfo complaintAdminuserInfo) throws Exception;
	
	
	public ComplaintAdminuserInfo getComplaintAdminuserInfoBycomplaintTypeCode(String complaintTypeCode) throws Exception;
	
	public ComplaintAdminuserInfo getComplaintAdminuserInfoBySealer(String sealer) throws Exception;
}
