package com.cheriscon.backstage.member.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.vo.ProUpgradeRecordVo;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客户产品升级信息service接口
 * @author pengdongan
 * @date 2013-01-14
 * 
 */

public interface IProUpgradeRecordService {
	
	/**
	 * 
	 * @param proUpgradeRecordVo	客户产品升级信息业务对象
	 * @return	
	 * @throws Exception
	 */
	public List<ProUpgradeRecordVo> listSealer(ProUpgradeRecordVo proUpgradeRecordVo) throws Exception;
	

	
	/**
	 * 根据编码查询客户产品升级信息对象
	 * @param code	客户产品升级信息code
	 * @return
	 * @throws Exception
	 */
	public ProUpgradeRecordVo getSealerByCode(String code) throws Exception;

	/**
	  * 客户产品升级信息分页显示方法
	  * @param ProUpgradeRecordVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageProUpgradeRecordVoPage(ProUpgradeRecordVo proUpgradeRecordVo, int pageNo,
			int pageSize) throws Exception;

	
	/**
	 * 清空客户产品升级信息对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	
	public int clearProUpgradeRecord() throws Exception;
	
}
