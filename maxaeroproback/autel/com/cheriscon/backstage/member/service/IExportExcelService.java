package com.cheriscon.backstage.member.service;

import java.io.InputStream;   
import java.util.List;
import java.util.Map;

import com.cheriscon.backstage.member.vo.CustomerInfoVo;
import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.backstage.member.vo.ProUpgradeRecordVo;
import com.cheriscon.backstage.trade.vo.OrderInfoVo;
import com.cheriscon.backstage.trade.vo.RechargeRecordVo;
import com.cheriscon.common.model.ReChargeRecord;

public interface IExportExcelService {   

    InputStream getCustomerInfoExcelInputStream(List<CustomerInfoVo> customerInfoVos);
    InputStream getCustomerProductExcelInputStream(List<CustomerProductVo> customerProductVos);
    InputStream getProUpgradeRecordExcelInputStream(List<ProUpgradeRecordVo> proUpgradeRecordVos);
    InputStream getCustomerLoginInfoExcelInputStream(List<CustomerLoginInfoVo> customerLoginInfoVos);
    InputStream getCustomerSoftwareExcelInputStream(List<CustomerSoftwareVo> customerSoftwareVos);
    InputStream getOrderInfoExcelInputStream(List<OrderInfoVo> orderInfoVos);
    InputStream getComplaintStatExcelInputStream(List<Map> lMaps);
    InputStream getRechargeRecordExcelInputStream(List<RechargeRecordVo> rechargeRecord);
}
