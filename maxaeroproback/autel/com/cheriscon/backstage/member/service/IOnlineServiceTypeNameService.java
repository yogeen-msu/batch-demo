package com.cheriscon.backstage.member.service;



import java.util.List;

import com.cheriscon.common.model.OnlineServiceTypeName;
import com.cheriscon.common.model.Language;
import com.cheriscon.framework.database.Page;

/**
 * 在线客服类型名称业务逻辑实现接口类
 * @author pengdongan
 * @date 2013-06-02
 */
public interface IOnlineServiceTypeNameService 
{	
	/**
	 * 查询所有在线客服类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<OnlineServiceTypeName> listAll() throws Exception;
	/**
	 * 按语言查询所有在线客服类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<OnlineServiceTypeName> listOnlineTypeNameByLanguageCode(String languageCode) throws Exception;

	/**
	 * 根据在线客服类型查询在线客服类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<OnlineServiceTypeName> queryByTypeCode(String typeCode) throws Exception;
	
	/**
	 * 添加在线客服类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertOnlineServiceTypeName(OnlineServiceTypeName onlineServiceTypeName) throws Exception;
	
	/**
	 * 更新在线客服类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateOnlineServiceTypeName(OnlineServiceTypeName onlineServiceTypeName) throws Exception;
	
	public void updateOnlineServiceTypeNames(
			List<OnlineServiceTypeName> onlineServiceTypeNames, List<OnlineServiceTypeName> delOnlineServiceTypeNames)
			throws Exception;
	
	/**
	 * 根据编码查询在线客服类型名称对象
	 * @param code	在线客服类型名称code
	 * @return
	 * @throws Exception
	 */
	public OnlineServiceTypeName getOnlineServiceTypeNameByCode(String code) throws Exception;
	
	/**
	 * 
	 * @param onlineTypeCode
	 * @param languageCode
	 * @return
	 * @throws Exception
	 */
	public OnlineServiceTypeName getOnlineServiceTypeName(String onlineTypeCode,String languageCode)
			throws Exception;
	
	
	/**
	 * 根据ID删除在线客服类型名称对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int delOnlineServiceTypeName(int id) throws Exception;
	
	/**
	 * 根据类型删除在线客服类型名称对象
	 * @param onlineTypeCode
	 * @return
	 * @throws Exception
	 */
	public int delOnlineServiceTypeNameByTypeCode(String onlineTypeCode) throws Exception;

	/**
	  * 在线客服类型名称分页显示方法
	  * @param OnlineServiceTypeName
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageOnlineServiceTypeNamePage(OnlineServiceTypeName onlineServiceTypeName, int pageNo,
			int pageSize) throws Exception;

	/**
	  * <p>Title: getAvailableLanguage</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @return
	  * @throws Exception
	  */
	public List<Language> getAvailableLanguage(String typeCode)
			throws Exception;
	

}
