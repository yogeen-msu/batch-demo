package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.backstage.member.vo.SdkAppDownloadVO;
import com.cheriscon.framework.database.Page;

/**
 * sdk app download逻辑处理接口
 * 
 * @author autel
 * 
 */
public interface ISdkAppDownloadService {

	/**
	 * 分页 查詢sdk app download信息
	 * 
	 * @return list
	 */
	public Page querySdkAppDownloadsList(int pageSize, int pageNo);

	/**
	 * 查詢sdk app download明细信息
	 * 
	 * @return list
	 */
	public SdkAppDownloadVO querySdkAppDownloadsInfoById(String id);

	/**
	 * 查詢sdk app download信息
	 * 
	 * @return list
	 */
	public List<SdkAppDownloadVO> querySdkAppDownloadsInfo();

	/**
	 * 添加sdk app download信息
	 * 
	 * @return boolean
	 */
	public boolean addSdkAppDownloads(SdkAppDownloadVO sdkAppDownLoadVo);

	/**
	 * 修改sdk app download信息
	 * 
	 * @return boolean
	 */
	public boolean updateSdkAppDownloads(SdkAppDownloadVO sdkAppDownLoadVo);

	/**
	 * 删除sdk app download信息
	 * 
	 * @return boolean
	 */
	public boolean deleteSdkAppDownloads(SdkAppDownloadVO sdkAppDownLoadVo);
}
