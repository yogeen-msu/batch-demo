package com.cheriscon.backstage.member.service;



import java.util.List;

import com.cheriscon.common.model.OnlineServiceType;
import com.cheriscon.framework.database.Page;

/**
 * 在线客服类型业务逻辑实现接口类
 * @author pengdongan
 * @date 2013-06-02
 */
public interface IOnlineServiceTypeService 
{	
	/**
	 * 查询所有在线客服类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<OnlineServiceType> listAll() throws Exception;
	
	/**
	 * 按语言查询所有在线客服类型对象
	 * @param 
	 * @return
	 * @throws Exception
	 */
	public List<OnlineServiceType> listOnlineServiceTypeByLanguageCode(String languageCode) throws Exception;
	
	/**
	 * 添加在线客服类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertOnlineServiceType(OnlineServiceType onlineServiceType) throws Exception;
	
	/**
	 * 更新在线客服类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateOnlineServiceType(OnlineServiceType onlineServiceType) throws Exception;
	
	/**
	 * 根据编码查询在线客服类型对象
	 * @param code	在线客服类型code
	 * @return
	 * @throws Exception
	 */
	public OnlineServiceType getOnlineServiceTypeByCode(String code) throws Exception;
	
	/**
	 * 根据编码删除在线客服类型对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delOnlineServiceType(String code) throws Exception;
	
	/**
	 * 根据编码删除在线客服类型及其名称
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delOnlineServiceTypeDetail(String code) throws Exception;
	
	/**
	 * 根据删除多个在线客服类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delOnlineServiceTypes(String codes) throws Exception;

	/**
	  * 在线客服类型分页显示方法
	  * @param OnlineServiceType
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageOnlineServiceTypePage(OnlineServiceType onlineServiceType, int pageNo,
			int pageSize) throws Exception;
	

}
