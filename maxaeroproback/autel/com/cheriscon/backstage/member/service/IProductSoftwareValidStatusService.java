package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.framework.database.Page;

public interface IProductSoftwareValidStatusService {

	/**
	 * 增加
	 * 
	 * @param productSoftwareValidStatus
	 */
	public boolean saveProductSoftwareValidStatus(
			ProductSoftwareValidStatus productSoftwareValidStatus);

	/**
	 * 修改有效期
	 * 
	 * @param softwareValidStatus
	 */
	public boolean updateValidDate(
			ProductSoftwareValidStatus softwareValidStatus);

	/**
	 * 根据产品code、最小销售单位code修改对应有效期
	 * 
	 * @param validDate
	 * @param proCode
	 * @param minSaleUnitCode
	 * @return
	 * @throws Exception
	 */
	public boolean updateValidDate(String validDate,String proCode)
			throws Exception;

	/**
	 * 删除
	 */
	public boolean delProductSoftwareValidStatusById(int id);

	/**
	 * 根据产品code、minSaleUnitCode查询产品软件有效状态对象
	 * 
	 * @param proCode
	 * @param minSaleUnitCode
	 * @return
	 * @throws Exception
	 */
	public ProductSoftwareValidStatus getProductSoftwareValidStatus(
			String proCode,String minSaleUnitCode) throws Exception;

	/**
	 * 翻页查询所有软件有效
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageProductSoftwareValidStatus(
			ProductSoftwareValidStatus validStatus, int pageNo, int pageSize);

	/**
	 * 
	* @Title: updateRenewal
	* @Description: 产品续期
	* @param    
	* @return void    
	* @throws
	 */
	public void updateRenewal(String proCode,String minSaleCode, String year) throws Exception;
	
	/**
	 * 
	 * @param proCode
	 * @return
	 * @throws Exception
	 */
	public List<ProductSoftwareValidStatus> getProductSoftwareValidStatusList(String proCode)throws Exception;
	
	/**
	 * 
	 * @param productSN 产品序列号
	 * @return
	 * @throws Exception
	 */
	public List<ProductSoftwareValidStatus> getProductSoftwareValidList(String productSN)throws Exception;

	public void deleteMoreMinCode(String proCode) throws Exception;
	
	public void deleteByProCode(String proCode) throws Exception;

}
