package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客户软件信息service接口
 * @author pengdongan
 * @date 2013-01-14
 * 
 */

public interface ICustomerSoftwareService {
	
	/**
	 * 
	 * @param customerSoftwareVo	客户软件信息业务对象
	 * @return	
	 * @throws Exception
	 */
	public List<CustomerSoftwareVo> listSealer(CustomerSoftwareVo customerSoftwareVo) throws Exception;
	

	
	/**
	 * 根据编码查询客户软件信息对象
	 * @param code	客户软件信息code
	 * @return
	 * @throws Exception
	 */
	public CustomerSoftwareVo getSealerByCode(String code) throws Exception;

	/**
	  * 客户软件信息分页显示方法
	  * @param CustomerSoftwareVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerSoftwareVoPage(CustomerSoftwareVo customerSoftwareVo, int pageNo,
			int pageSize) throws Exception;
	
}
