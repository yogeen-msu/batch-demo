package com.cheriscon.backstage.member.service;



import java.util.List;

import com.cheriscon.backstage.member.vo.TestPackVo;
import com.cheriscon.common.model.TestPackLanguagePack;
import com.cheriscon.common.model.TestPack;
import com.cheriscon.framework.database.Page;

/**
 * 测试包业务逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public interface ITestPackService 
{


	
	/**
	 * 添加测试包完整信息
	 * @param TestPack ,List<LanguagePack>
	 * @return
	 * @throws Exception
	 */
	public int insertTestPackDetail(TestPack testPack,List<TestPackLanguagePack> testPackLanguagePacks) throws Exception;

	
	/**
	 * 添加测试包对象
	 * @param TestPack
	 * @return
	 * @throws Exception
	 */
	public String insertTestPack(TestPack testPack) throws Exception;
	
	/**
	 * 根据编码查询测试包对象
	 * @param code	测试包code
	 * @return
	 * @throws Exception
	 */
	public TestPack getTestPackByCode(String code) throws Exception;
	
	/**
	 * 根据编码删除测试包对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delTestPack(String code) throws Exception;

	/**
	  * 测试包分页显示方法
	  * @param TestPackVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageTestPackVoPage(TestPackVo testPackVo, int pageNo,
			int pageSize) throws Exception;

	/**
	 * 根据编码查询测试包业务对象
	 * @param code	测试包code
	 * @return
	 * @throws Exception
	 */
	public TestPackVo getTestPackVoByCode(String code) throws Exception;

	/**
	 * 根据编码删除测试包完整信息
	 * @param code	测试包code
	 * @return
	 * @throws Exception
	 */
	public int delTestPackDetail(String code) throws Exception;
	
	/**
	 * 根据编码更新测试信息
	 * @param code	
	 * @return
	 * @throws Exception
	 */
	public int updateTestPackDetail(TestPack testPackVo) throws Exception;
}
