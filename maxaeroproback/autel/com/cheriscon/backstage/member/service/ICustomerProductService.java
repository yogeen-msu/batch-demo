package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.common.model.CustomerProInfo;
import com.cheriscon.framework.database.Page;

/**
 * @remark 客户注册产品信息service接口
 * @author pengdongan
 * @date 2013-01-10
 * 
 */

public interface ICustomerProductService {
	
	/**
	 * 
	 * @param customerInfoVo	客户注册产品信息业务对象
	 * @return	
	 * @throws Exception
	 */
	public List<CustomerProductVo> listCustomerPro(CustomerProductVo customerProductVo) throws Exception;
	
	/**
	 * 解绑客户注册产品
	 * @param code
	 * @return int	
	 * @throws Exception
	 */
	public int noRegPro(String code,String proNoRegReason,String createUser) throws Exception;	
	/**
	  * 客户注册产品分页显示方法
	  * @param CustomerProductVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerProductVoPage(CustomerProductVo customerProductVo, int pageNo,
			int pageSize) throws Exception;
	
	public int queryProductArea(String areaCode,String userCode) throws Exception;
	
	/**
	 * 
	 * @param serialNo	产品序列号
	 * @return	
	 * @throws Exception
	 */
	public CustomerProductVo getCustomerPro(String serialNo) throws Exception;
	
	public String getCustomerproinfo(String proCode) throws Exception;
	
	public List<CustomerProductVo> queryCustomerProductVo(CustomerProductVo customerProductVo) throws Exception;
}
