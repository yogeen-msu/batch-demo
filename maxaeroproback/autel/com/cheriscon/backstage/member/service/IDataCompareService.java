package com.cheriscon.backstage.member.service;

import java.util.List;

import com.cheriscon.common.model.DataCompare;

public interface IDataCompareService {
	
    public List<DataCompare> getNewLanguages(String serialNo) throws Exception;
    
    public List<DataCompare> getOldLanguages(String serialNo) throws Exception;
    
    public List<DataCompare> getNewMinSaleCodes(String serialNo) throws Exception;
    
    public List<DataCompare> getOldMinSaleCodes(String serialNo) throws Exception;
    
    public List<DataCompare> getOldSoftwares(String serialNo) throws Exception;
    
    public List<DataCompare> getNewtSoftwares(String serialNo) throws Exception;
    
    
    public void insertProductMinSaleSearch(String serialNo) throws Exception;
}
