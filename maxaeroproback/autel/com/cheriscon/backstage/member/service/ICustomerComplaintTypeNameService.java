package com.cheriscon.backstage.member.service;



import java.util.List;

import com.cheriscon.common.model.CustomerComplaintTypeName;
import com.cheriscon.common.model.Language;
import com.cheriscon.framework.database.Page;

/**
 * 客诉类型名称业务逻辑实现接口类
 * @author pengdongan
 * @date 2013-01-24
 */
public interface ICustomerComplaintTypeNameService 
{	
	/**
	 * 查询所有客诉类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<CustomerComplaintTypeName> listAll() throws Exception;
	/**
	 * 按语言查询所有客诉类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<CustomerComplaintTypeName> listComplaintTypeNameByLanguageCode(String languageCode) throws Exception;
	
	public List<CustomerComplaintTypeName> listComplaintTypeNameByLanguageCode1(String languageCode);
	
	/**
	 * 根据客诉类型查询客诉类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<CustomerComplaintTypeName> queryByTypeCode(String typeCode) throws Exception;
	
	/**
	 * 添加客诉类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertCustomerComplaintTypeName(CustomerComplaintTypeName customerComplaintTypeName) throws Exception;
	
	/**
	 * 更新客诉类型名称对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateCustomerComplaintTypeName(CustomerComplaintTypeName customerComplaintTypeName) throws Exception;
	
	public void updateCustomerComplaintTypeNames(
			List<CustomerComplaintTypeName> customerComplaintTypeNames, List<CustomerComplaintTypeName> delCustomerComplaintTypeNames)
			throws Exception;
	
	/**
	 * 根据编码查询客诉类型名称对象
	 * @param code	客诉类型名称code
	 * @return
	 * @throws Exception
	 */
	public CustomerComplaintTypeName getCustomerComplaintTypeNameByCode(String code) throws Exception;
	
	/**
	 * 
	 * @param complaintTypeCode
	 * @param languageCode
	 * @return
	 * @throws Exception
	 */
	public CustomerComplaintTypeName getCustomerComplaintTypeName(String complaintTypeCode,String languageCode)
			throws Exception;
	
	
	/**
	 * 根据ID删除客诉类型名称对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int delCustomerComplaintTypeName(int id) throws Exception;
	
	/**
	 * 根据类型删除客诉类型名称对象
	 * @param complaintTypeCode
	 * @return
	 * @throws Exception
	 */
	public int delCustomerComplaintTypeNameByTypeCode(String complaintTypeCode) throws Exception;

	/**
	  * 客诉类型名称分页显示方法
	  * @param CustomerComplaintTypeName
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageCustomerComplaintTypeNamePage(CustomerComplaintTypeName customerComplaintTypeName, int pageNo,
			int pageSize) throws Exception;

	/**
	  * <p>Title: getAvailableLanguage</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @return
	  * @throws Exception
	  */
	public List<Language> getAvailableLanguage(String typeCode)
			throws Exception;
	

}
