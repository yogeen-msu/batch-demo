package com.cheriscon.backstage.member.service;

import com.cheriscon.common.model.CustomerActiveLog;

public interface ICustomerActiveLogService {
	

	public void saveLog(CustomerActiveLog log) throws Exception;
}
