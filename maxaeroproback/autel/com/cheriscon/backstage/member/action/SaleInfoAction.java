package com.cheriscon.backstage.member.action;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.dao.DataIntegrityViolationException;

import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.SaleInfoVo;

import com.cheriscon.common.model.SaleInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;
import com.cheriscon.front.usercenter.distributor.service.ISaleInfoService;

/**
 * @remark	经销商销售信息action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class SaleInfoAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private ISaleInfoService saleInfoService;

	@Resource
	private ISealerInfoService sealerInfoService;
	
	private SaleInfo saleInfo;
	
	private SaleInfoVo saleInfoVo;
	
	private List<SaleInfoVo> saleInfoVos;
	
	private List<SealerInfo> sealerInfos;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private SaleInfoVo saleInfoVoSel;
	private SaleInfo saleInfoAdd;
	private SaleInfo saleInfoEdit;
	
	private String jsonData;
	
	private File saleInfoFile;
	
	private int result;
	
	
	//获取经销商销售信息列表
	public void list(){
		try{
			this.webpage = saleInfoService.pageSaleInfoVoPage(saleInfoVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listsaleinfo", results = { @Result(name = "listsaleinfo", location = "/autel/backstage/member/sale_info_list.jsp") })
	public String listSaleInfo() throws Exception{
		list();
		return "listsaleinfo";
	}

	@Action(value = "createsaleinfo", results = { @Result(name = "createsaleinfo", location = "/autel/backstage/member/sale_info_create.jsp") })
	public String createSaleInfo() throws Exception{
		return "createsaleinfo";
	}

	/**
	 * 将充值卡信息导入数据库
	 * @return
	 * @throws Exception
	 */
//	@Action(value = "savecreatesaleinfo", results = { @Result(name = "savecreatesaleinfo", location = "/autel/backstage/member/iframe_result.jsp")})
	@Action(value = "savecreatesaleinfo")
	public String saveCreateSaleInfo() throws Exception{
		this.result = -1;
		if(saleInfoFile != null)
		{
			try{
				this.result = saleInfoService.saveCreateSaleInfo(saleInfoFile);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		//return "savecreatesale";
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.saleinfoman.message31"));
		}		
		else if(result == 1)
		{
			this.msgs.add(getText("memberman.saleinfoman.message33"));
		}		
		else if(result == 2)
		{
			this.msgs.add(getText("memberman.saleinfoman.message34"));
		}		
		else if(result == 3)
		{
			this.msgs.add(getText("memberman.saleinfoman.message35"));
		}		
		else if(result == 4)
		{
			this.msgs.add(getText("memberman.saleinfoman.message36"));
		}		
		else if(result == 5)
		{
			this.msgs.add(getText("memberman.saleinfoman.message37"));
		}		
		else if(result == 6)
		{
			this.msgs.add(getText("memberman.saleinfoman.message38"));
		}		
		else if(result == 7)
		{
			this.msgs.add(getText("memberman.saleinfoman.message39"));
		}		
		else if(result == 8)
		{
			this.msgs.add(getText("memberman.saleinfoman.message40"));
		}
		else
		{
			this.msgs.add(getText("memberman.saleinfoman.message32"));
		}

		this.urls.put(getText("memberman.saleinfoman.message20"),"listsaleinfo.do");
		
		return MESSAGE;
	}

	@Action(value = "addsaleinfo", results = { @Result(name = "addsaleinfo", location = "/autel/backstage/member/sale_info_add.jsp") })
	public String addSaleInfo() throws Exception{
		this.sealerInfos = sealerInfoService.querySealerInfo();
		return "addsaleinfo";
	}

//	@Action(value = "insertsaleinfo", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertsaleinfo")
	public String insertSaleInfo() throws Exception{
		int result = -1;
		try{
			if(saleInfoAdd.getSalesNum() == null) saleInfoAdd.setSalesNum(0);
			if(saleInfoAdd.getSalesMoney() == null) saleInfoAdd.setSalesMoney(Long.parseLong("0"));
			if(saleInfoAdd.getPinBackNum() == null) saleInfoAdd.setPinBackNum(0);
			if(saleInfoAdd.getPinBackMoney() == null) saleInfoAdd.setPinBackMoney(Long.parseLong("0"));
			
			saleInfoAdd.setNetWeight(saleInfoAdd.getSalesNum()-saleInfoAdd.getPinBackNum());
			saleInfoAdd.setNetSales(saleInfoAdd.getSalesMoney()-saleInfoAdd.getPinBackMoney());
			saleInfoService.insertSaleInfo(saleInfoAdd);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.saleinfoman.message21"));
		}
		else
		{
			this.msgs.add(getText("memberman.saleinfoman.message22"));
		}

		this.urls.put(getText("memberman.saleinfoman.message20"),"listsaleinfo.do");
		
		return MESSAGE;
	}

	@Action(value = "editsaleinfo", results = { @Result(name = "editsaleinfo", location = "/autel/backstage/member/sale_info_edit.jsp") })
	public String editSaleInfo() throws Exception{
		try{
			this.saleInfoEdit = saleInfoService.getSaleInfoByCode(code);
			this.sealerInfos = sealerInfoService.querySealerInfo();
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editsaleinfo";
	}

//	@Action(value = "updatesaleinfo", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updatesaleinfo")
	public String updateSaleInfo() throws Exception{
		int result = -1;
		
		try{
			saleInfo = saleInfoService.getSaleInfoByCode(saleInfoEdit.getCode());
			if(saleInfo != null)
			{
				if(saleInfoEdit.getSalesNum() == null) saleInfoEdit.setSalesNum(0);
				if(saleInfoEdit.getSalesMoney() == null) saleInfoEdit.setSalesMoney(Long.parseLong("0"));
				if(saleInfoEdit.getPinBackNum() == null) saleInfoEdit.setPinBackNum(0);
				if(saleInfoEdit.getPinBackMoney() == null) saleInfoEdit.setPinBackMoney(Long.parseLong("0"));
				
				saleInfo.setProName(saleInfoEdit.getProName());
				saleInfo.setProExpand(saleInfoEdit.getProExpand());
				saleInfo.setCompany(saleInfoEdit.getCompany());
				saleInfo.setSalesNum(saleInfoEdit.getSalesNum());
				saleInfo.setSalesMoney(saleInfoEdit.getSalesMoney());
				saleInfo.setPinBackNum(saleInfoEdit.getPinBackNum());
				saleInfo.setPinBackMoney(saleInfoEdit.getPinBackMoney());
				saleInfo.setSealerName(saleInfoEdit.getSealerName());
				saleInfo.setNetWeight(saleInfoEdit.getSalesNum()-saleInfoEdit.getPinBackNum());
				saleInfo.setNetSales(saleInfoEdit.getSalesMoney()-saleInfoEdit.getPinBackMoney());
			
				saleInfoService.updateSaleInfo(saleInfo);
				result = 0;
			}
			else
			{
				result = 1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.saleinfoman.message23"));
		}
		else
		{
			this.msgs.add(getText("memberman.saleinfoman.message24"));
		}

		this.urls.put(getText("memberman.saleinfoman.message20"),"listsaleinfo.do");
		
		return MESSAGE;
	}

//	@Action(value = "delsaleinfo", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delsaleinfo")
	public String delSaleInfo() throws Exception{
		int result = -1;
		try{
			saleInfoService.delSaleInfo(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.saleinfoman.message25"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.saleinfoman.message26"));
		}
		else
		{
			this.msgs.add(getText("memberman.saleinfoman.message27"));
		}

		this.urls.put(getText("memberman.saleinfoman.message20"),"listsaleinfo.do");
		
		return MESSAGE;
	}

//	@Action(value = "delsaleinfos", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delsaleinfos")
	public String delSaleInfos() throws Exception{
		int result = -1;
		try{
			saleInfoService.delSaleInfos(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.saleinfoman.message25"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.saleinfoman.message26"));
		}
		else
		{
			this.msgs.add(getText("memberman.saleinfoman.message27"));
		}

		this.urls.put(getText("memberman.saleinfoman.message20"),"listsaleinfo.do");
		
		return MESSAGE;
	}
	
	
	public SaleInfo getSaleInfo() {
		return saleInfo;
	}

	public void setSaleInfo(SaleInfo saleInfo) {
		this.saleInfo = saleInfo;
	}

	public SaleInfoVo getSaleInfoVo() {
		return saleInfoVo;
	}

	public void setSaleInfoVo(SaleInfoVo saleInfoVo) {
		this.saleInfoVo = saleInfoVo;
	}

	public List<SaleInfoVo> getSaleInfoVos() {
		return saleInfoVos;
	}

	public void setSaleInfoVos(List<SaleInfoVo> saleInfoVos) {
		this.saleInfoVos = saleInfoVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public SaleInfoVo getSaleInfoVoSel() {
		return saleInfoVoSel;
	}

	public void setSaleInfoVoSel(SaleInfoVo saleInfoVoSel) {
		this.saleInfoVoSel = saleInfoVoSel;
	}

	public SaleInfo getSaleInfoAdd() {
		return saleInfoAdd;
	}

	public void setSaleInfoAdd(SaleInfo saleInfoAdd) {
		this.saleInfoAdd = saleInfoAdd;
	}

	public SaleInfo getSaleInfoEdit() {
		return saleInfoEdit;
	}

	public void setSaleInfoEdit(SaleInfo saleInfoEdit) {
		this.saleInfoEdit = saleInfoEdit;
	}

	public List<SealerInfo> getSealerInfos() {
		return sealerInfos;
	}

	public void setSealerInfos(List<SealerInfo> sealerInfos) {
		this.sealerInfos = sealerInfos;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public File getSaleInfoFile() {
		return saleInfoFile;
	}

	public void setSaleInfoFile(File saleInfoFile) {
		this.saleInfoFile = saleInfoFile;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

}	
