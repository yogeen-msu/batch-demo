package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ICustomerSoftwareService;
import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.backstage.system.service.ILanguageService;

import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * @remark	客户软件信息action
 * @author pengdongan
 * @date   2013-01-07
 */
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerSoftwareAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private ICustomerSoftwareService customerSoftwareService;

	@Resource
	private ILanguageService languageService;
	
	private CustomerSoftwareVo customerSoftwareVo;
	
	private List<CustomerSoftwareVo> customerSoftwareVos;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private CustomerSoftwareVo customerSoftwareVoSel;
	
	
	//获取客户软件信息列表
	public void list(){
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			customerSoftwareVoSel = customerSoftwareVoSel != null ? customerSoftwareVoSel : new CustomerSoftwareVo();
			customerSoftwareVoSel.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			this.webpage = customerSoftwareService.pageCustomerSoftwareVoPage(customerSoftwareVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listcustomersoft", results = { @Result(name = "listcustomersoft", location = "/autel/backstage/member/customer_soft_list.jsp") })
	public String listCustomerSoft() throws Exception{
		if(customerSoftwareVoSel!=null){
			list();
		}
		return "listcustomersoft";
	}

	public CustomerSoftwareVo getCustomerSoftwareVo() {
		return customerSoftwareVo;
	}

	public void setCustomerSoftwareVo(CustomerSoftwareVo customerSoftwareVo) {
		this.customerSoftwareVo = customerSoftwareVo;
	}

	public List<CustomerSoftwareVo> getCustomerSoftwareVos() {
		return customerSoftwareVos;
	}

	public void setCustomerSoftwareVos(List<CustomerSoftwareVo> customerSoftwareVos) {
		this.customerSoftwareVos = customerSoftwareVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public CustomerSoftwareVo getCustomerSoftwareVoSel() {
		return customerSoftwareVoSel;
	}

	public void setCustomerSoftwareVoSel(CustomerSoftwareVo customerSoftwareVoSel) {
		this.customerSoftwareVoSel = customerSoftwareVoSel;
	}

}	
