package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.backstage.system.service.ICustomerProInfoHistoryService;
import com.cheriscon.common.model.CustomerProInfoHistory;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;

/**
 * @remark	客户信息action
 * @author pengdongan
 * @date   2013-01-10
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerProductAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private ICustomerProductService customerProductService;
	
	@Resource
	private IAdminUserManager adminUserManager;
	
	@Resource
	private ICustomerProInfoHistoryService customerProInfoHistoryServiceImpl;
	
	private CustomerProductVo customerProductVo;
	
	private List<CustomerProductVo> customerProductVos;
	
	//查询、修改、删除参数
	private CustomerProductVo customerProductVoSel;

	private String jsonData;
	private String code;
	private String proNoRegReason;
    private String customerCode;
	
	
	//获取用户信息列表
	public void list(){
		try{
			this.webpage = customerProductService.pageCustomerProductVoPage(customerProductVoSel, this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listcustomerpro", results = { @Result(name = "listcustomerpro", location = "/autel/backstage/member/customer_product_list.jsp") })
	public String listCustomerPro() throws Exception{
		if(customerProductVoSel!=null){
			list();
		}
		return "listcustomerpro";
	}

	/**
	 * 解绑用户产品
	 * @return
	 * @throws Exception
	 */
	@Action(value = "noRegPro", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String noRegPro() throws Exception {
		int result = -1;
		try {
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			customerProductService.noRegPro(code,proNoRegReason,user.getUsername());
			
			CustomerProInfoHistory customerProInfoHistory = new CustomerProInfoHistory();
			customerProInfoHistory.setProCode(code);
			customerProInfoHistory.setCreateUser(user.getUsername());
			customerProInfoHistory.setCustomerCode(customerCode);
			customerProInfoHistoryServiceImpl.add(customerProInfoHistory);
			result = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jsonData =  JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}

	public CustomerProductVo getCustomerProductVo() {
		return customerProductVo;
	}

	public void setCustomerProductVo(CustomerProductVo customerProductVo) {
		this.customerProductVo = customerProductVo;
	}

	public List<CustomerProductVo> getCustomerProductVos() {
		return customerProductVos;
	}

	public void setCustomerProductVos(List<CustomerProductVo> customerProductVos) {
		this.customerProductVos = customerProductVos;
	}

	public CustomerProductVo getCustomerProductVoSel() {
		return customerProductVoSel;
	}

	public void setCustomerProductVoSel(CustomerProductVo customerProductVoSel) {
		this.customerProductVoSel = customerProductVoSel;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProNoRegReason() {
		return proNoRegReason;
	}

	public void setProNoRegReason(String proNoRegReason) {
		this.proNoRegReason = proNoRegReason;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

}	
