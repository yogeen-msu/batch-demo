package com.cheriscon.backstage.member.action;



import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.IOnlineServiceTypeNameService;
import com.cheriscon.backstage.member.service.IOnlineServiceTypeService;
import com.cheriscon.backstage.system.service.ILanguageService;

import com.cheriscon.common.model.OnlineServiceType;
import com.cheriscon.common.model.OnlineServiceTypeName;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	在线客服类型action
 * @author pengdongan
 * @date   2013-06-02
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class OnlineServiceTypeAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6693265756340979658L;

	@Resource
	private IAdminUserManager adminUserManager;

	@Resource
	private IOnlineServiceTypeService onlineServiceTypeService;
	@Resource
	private IOnlineServiceTypeNameService onlineServiceTypeNameService;
	@Resource
	private ILanguageService languageService;
	
	private OnlineServiceType onlineServiceType;
	
	private List<OnlineServiceType> onlineServiceTypes;
	
	private List<OnlineServiceTypeName> onlineServiceTypeNames;
	
	private List<Language> languages;

	private AdminUser adminUser;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private OnlineServiceType onlineServiceTypeSel;
	private OnlineServiceType onlineServiceTypeAdd;
	private OnlineServiceType onlineServiceTypeEdit;
	
	
	private String hostUrl;
	
	/**
	 * 需删除的名称
	 */
	private List<OnlineServiceTypeName> delOnlineServiceTypeNames;
	
	private String jsonData;
	
	
	//获取在线客服类型列表
	public void list(){
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			onlineServiceTypeSel = onlineServiceTypeSel != null ? onlineServiceTypeSel : new OnlineServiceType();
			onlineServiceTypeSel.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			this.webpage = onlineServiceTypeService.pageOnlineServiceTypePage(onlineServiceTypeSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listonlineservicetype", results = { @Result(name = "listonlineservicetype", location = "/autel/backstage/member/online_type_list.jsp") })
	public String listOnlineServiceType() throws Exception{
		list();
		return "listonlineservicetype";
	}

	@Action(value = "onlineadmin", results = { @Result(name = "onlineadmin", location = "/autel/backstage/member/onlineadmin.jsp") })
	public String onlineAdmin() throws Exception{
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			
			String requestUrl = request.getRequestURL().toString();
			String contextPath = request.getContextPath();
			
			hostUrl = requestUrl.split(contextPath)[0];
			
			adminUser = adminUserManager.getCurrentUser();
			adminUser = adminUserManager.get(adminUser.getUserid());
		}catch(Exception e){
			e.printStackTrace();
		}
		return "onlineadmin";
	}

	@Action(value = "addonlineservicetype", results = { @Result(name = "addonlineservicetype", location = "/autel/backstage/member/online_type_add.jsp") })
	public String addOnlineServiceType() throws Exception{
		return "addonlineservicetype";
	}

//	@Action(value = "insertonlineservicetype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertonlineservicetype")
	public String insertOnlineServiceType() throws Exception{
		int result = -1;
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			onlineServiceTypeAdd.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			onlineServiceTypeService.insertOnlineServiceType(onlineServiceTypeAdd);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.onlinetypeman.message21"));
		}
		else
		{
			this.msgs.add(getText("memberman.onlinetypeman.message22"));
		}

		this.urls.put(getText("memberman.onlinetypeman.message20"),"listonlineservicetype.do");
		
		return MESSAGE;
	}

	@Action(value = "editonlineservicetype", results = { @Result(name = "editonlineservicetype", location = "/autel/backstage/member/online_type_edit.jsp") })
	public String editOnlineServiceType() throws Exception{
		try{
			this.onlineServiceType = this.onlineServiceTypeService.getOnlineServiceTypeByCode(code);
			this.onlineServiceTypeNames = this.onlineServiceTypeNameService.queryByTypeCode(code);
			this.languages = this.onlineServiceTypeNameService.getAvailableLanguage(code);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editonlineservicetype";
	}

//	@Action(value = "updateonlineservicetype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updateonlineservicetype")
	public String updateOnlineServiceType() throws Exception{
		int result = -1;
		
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			onlineServiceTypeEdit.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			onlineServiceTypeService.updateOnlineServiceType(onlineServiceTypeEdit);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.onlinetypeman.message23"));
		}
		else
		{
			this.msgs.add(getText("memberman.onlinetypeman.message24"));
		}

		this.urls.put(getText("memberman.onlinetypeman.message20"),"listonlineservicetype.do");
		
		return MESSAGE;
	}

	@Action(value = "updateOnlineServiceTypeName")
	public String updateOnlineServiceTypeName() throws Exception {
		int result = -1;
		try {
			onlineServiceTypeNameService.updateOnlineServiceTypeNames(onlineServiceTypeNames,
					delOnlineServiceTypeNames);
			result = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.onlinetypeman.message23"));
		}
		else
		{
			this.msgs.add(getText("memberman.onlinetypeman.message24"));
		}

		this.urls.put(getText("memberman.onlinetypeman.message20"),"listonlineservicetype.do");

		return MESSAGE;
	}

//	@Action(value = "delonlineservicetype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delonlineservicetype")
	public String delOnlineServiceType() throws Exception{
		int result = -1;
		try{
			onlineServiceTypeService.delOnlineServiceTypeDetail(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.onlinetypeman.message25"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.onlinetypeman.message26"));
		}
		else
		{
			this.msgs.add(getText("memberman.onlinetypeman.message26"));
		}

		this.urls.put(getText("memberman.onlinetypeman.message20"),"listonlineservicetype.do");
		
		return MESSAGE;
	}

//	@Action(value = "delonlineservicetypes", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delonlineservicetypes")
	public String delOnlineServiceTypes() throws Exception{
		int result = -1;
		try{
			onlineServiceTypeService.delOnlineServiceTypes(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.onlinetypeman.message25"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.onlinetypeman.message26"));
		}
		else
		{
			this.msgs.add(getText("memberman.onlinetypeman.message27"));
		}

		this.urls.put(getText("memberman.onlinetypeman.message20"),"listonlineservicetype.do");
		
		return MESSAGE;
	}
	
	
	public OnlineServiceType getOnlineServiceType() {
		return onlineServiceType;
	}

	public void setOnlineServiceType(OnlineServiceType onlineServiceType) {
		this.onlineServiceType = onlineServiceType;
	}

	public List<OnlineServiceType> getOnlineServiceTypes() {
		return onlineServiceTypes;
	}

	public void setOnlineServiceTypes(List<OnlineServiceType> onlineServiceTypes) {
		this.onlineServiceTypes = onlineServiceTypes;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public OnlineServiceType getOnlineServiceTypeSel() {
		return onlineServiceTypeSel;
	}

	public void setOnlineServiceTypeSel(OnlineServiceType onlineServiceTypeSel) {
		this.onlineServiceTypeSel = onlineServiceTypeSel;
	}

	public OnlineServiceType getOnlineServiceTypeAdd() {
		return onlineServiceTypeAdd;
	}

	public void setOnlineServiceTypeAdd(OnlineServiceType onlineServiceTypeAdd) {
		this.onlineServiceTypeAdd = onlineServiceTypeAdd;
	}

	public OnlineServiceType getOnlineServiceTypeEdit() {
		return onlineServiceTypeEdit;
	}

	public void setOnlineServiceTypeEdit(OnlineServiceType onlineServiceTypeEdit) {
		this.onlineServiceTypeEdit = onlineServiceTypeEdit;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public List<OnlineServiceTypeName> getOnlineServiceTypeNames() {
		return onlineServiceTypeNames;
	}

	public void setOnlineServiceTypeNames(
			List<OnlineServiceTypeName> onlineServiceTypeNames) {
		this.onlineServiceTypeNames = onlineServiceTypeNames;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public List<OnlineServiceTypeName> getDelOnlineServiceTypeNames() {
		return delOnlineServiceTypeNames;
	}

	public void setDelOnlineServiceTypeNames(
			List<OnlineServiceTypeName> delOnlineServiceTypeNames) {
		this.delOnlineServiceTypeNames = delOnlineServiceTypeNames;
	}

	public AdminUser getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(AdminUser adminUser) {
		this.adminUser = adminUser;
	}

	public String getHostUrl() {
		return hostUrl;
	}

	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}

}	
