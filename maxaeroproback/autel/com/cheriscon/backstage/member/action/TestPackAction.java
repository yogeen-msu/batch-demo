package com.cheriscon.backstage.member.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ITestPackLanguagePackService;
import com.cheriscon.backstage.member.service.ITestPackService;
import com.cheriscon.backstage.member.vo.TestPackVo;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.service.ISoftwareTypeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.util.CallProcedureUtil;

import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SoftwareType;

import com.cheriscon.common.model.TestPack;

import com.cheriscon.common.model.TestPackLanguagePack;
import com.cheriscon.framework.action.WWAction;

/**
 * @remark	测试包action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class TestPackAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2812954469726046995L;
	

	@Resource
	private ITestPackService testPackService;
	
	@Resource
	private IProductTypeService productTypeService;

	@Resource
	private ITestPackLanguagePackService testPackLanguagePackService;

	@Resource
	private ILanguageService languageService;

	@Resource
	private ISoftwareTypeService softwareTypeService;

	@Resource
	private IProductInfoService productInfoService;
	
	private TestPack testPack;
	
	private TestPackVo testPackVo;
	
	private List<TestPackVo> testPackVos;
	
	private List<Language> languages;
	
	private List<SoftwareType> softwareTypes;

	private List<TestPackLanguagePack> testPackLanguagePacks;
	
	private List<ProductType> productTypeList ;
	
	private TestPackLanguagePack testPackLanguagePack;
	
	public TestPackLanguagePack getTestPackLanguagePack() {
		return testPackLanguagePack;
	}

	public void setTestPackLanguagePack(TestPackLanguagePack testPackLanguagePack) {
		this.testPackLanguagePack = testPackLanguagePack;
	}

	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private TestPackVo testPackVoSel;
	private TestPack testPackAdd;
	private TestPack testPackEdit;
	private ProductInfo productInfo;
	private SoftwareType softType;
	private String name;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SoftwareType getSoftType() {
		return softType;
	}

	public void setSoftType(SoftwareType softType) {
		this.softType = softType;
	}

	private String jsonData;
	
	
	@Action(value = "refresh")
	public String refresh() throws Exception{
		try{
			CallProcedureUtil.call("pro_InitTestPackSearch");
			msgs.add("操作成功");
		}catch(Exception e){
			msgs.add("操作失败，请联系管理员");
		}
		this.urls.put("返回", "listtestpack.do");
		return MESSAGE;
	}
	
	//获取测试包列表
	public void list(){
		try{
			this.webpage = testPackService.pageTestPackVoPage(testPackVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listtestpack", results = { @Result(name = "listtestpack", location = "/autel/backstage/member/test_pack_list.jsp") })
	public String listTestPack() throws Exception{
		list();
		return "listtestpack";
	}

	@Action(value = "addtestpack", results = { @Result(name = "addtestpack", location = "/autel/backstage/member/test_pack_add.jsp") })
	public String addTestPack() throws Exception{
		this.languages = languageService.queryAllLanguage();
		this.softwareTypes = softwareTypeService.listAll();
		return "addtestpack";
	}

	@Action(value = "selectProductTypeValue", results = { @Result(name = "selectProductTypeValue", location = "/autel/backstage/member/product_select.jsp") })
	public String selectProductTypeValue() {
		try {
			this.webpage= softwareTypeService.pageSoftwarePage(softType,this.getPage(), this.getPageSize());
		    this.productTypeList = productTypeService.listAll();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "selectProductTypeValue";
	}
	
	
	
	
	/**
	 * 
	 * @Title: insertTestPack
	 * @Description: 添加测试包
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "inserttestpack")
	public String insertTestPack() throws Exception {

		try{
			//productInfo = productInfoService.getBySerialNo(testPackVo.getProSerialNo());
		
				testPackAdd.setProCode(testPackVo.getProSerialNo());
				testPackService.insertTestPackDetail(testPackAdd,testPackLanguagePacks);
				this.msgs.add(getText("common.js.addSuccess"));
				this.urls.put(getText("product.info.list"), "listtestpack.do");
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		

		return MESSAGE;
	}

	@Action(value = "updatetestpack")
	public String updatetestpack() throws Exception {

		try{
			//productInfo = productInfoService.getBySerialNo(testPackVo.getProSerialNo());
		
			    testPackAdd.setCode(testPackVo.getCode());
				testPackAdd.setProCode(testPackVo.getProSerialNo());
				testPackService.updateTestPackDetail(testPackAdd);
				this.msgs.add(getText("common.js.modSuccess"));
				this.urls.put(getText("product.info.list"), "listtestpack.do");
			
		}catch(Exception e){
			e.printStackTrace();
		}

		return MESSAGE;
	}
	
	//更新语言测试包
	@Action(value = "updateLanguagePack", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public  String updateLanguagePack() throws Exception{
		String result="";
		testPackLanguagePackService.updateTestPackLanguagePack(testPackLanguagePack);
		this.jsonData = JSONArray.fromObject(result).toString();
		return SUCCESS;
	}
	
	
	//更新语言测试包
		@Action(value = "deleteLanguagePack", results = { @Result(name = SUCCESS, type = "json", params = {
				"root", "jsonData" }) })
		public  String deleteLanguagePack() throws Exception{
			String result="";
			testPackLanguagePackService.delTestPackLanguagePack(testPackLanguagePack.getCode());
			this.jsonData = JSONArray.fromObject(result).toString();
			return SUCCESS;
		}
	
	  //更新语言测试包
	@Action(value = "insertLanguagePack", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public  String insertLanguagePack() throws Exception{
		String result="";
		testPackLanguagePackService.addTestPackLanguagePack(testPackLanguagePack);
		this.jsonData = JSONArray.fromObject(result).toString();
		return SUCCESS;
	}
	
	/**
	 * 验证产品序列号有效性
	 * @return
	 * @throws Exception
	 */
	@Action(value = "checkproserialno", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkProSerialNo() throws Exception {
		String SerialNo=productInfo.getSerialNo();
		String str[] =SerialNo.split(",");
		List<String> list=new ArrayList<String>();
		for(int i=0;i<str.length;i++){
			productInfo=productInfoService.getBySerialNo(str[i]);
			if(productInfo==null){
				list.add(str[i]);
			}
		}
		this.jsonData = JSONArray.fromObject(list).toString();
		
		return SUCCESS;
	}

	/**
	 * 按产品序列号获取功能软件列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "getsoftbyprono", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getSoftByProNo() throws Exception {
		ProductInfo productInfo_t;
		String proTypeCode;
		List<SoftwareType> softwareTypes = new ArrayList<SoftwareType>();
		productInfo_t = productInfoService.getBySerialNo(productInfo.getSerialNo());
		if(productInfo_t !=null)
		{
			proTypeCode = productInfo_t.getProTypeCode();
			softwareTypes = softwareTypeService.getSoftwareTypeByProTypeCode(proTypeCode);
		}

		Map<String,Object> map = new HashMap<String,Object>();
		map.put("softwareTypes", softwareTypes);
		
		this.jsonData = JSONArray.fromObject(map).toString();
		
		return SUCCESS;
	}

	/**
	 * 查看测试包详情
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showtestpack", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String showTestPack() throws Exception {
		testPackVo = testPackService.getTestPackVoByCode(code);
		testPackLanguagePacks = testPackLanguagePackService.listLanguagePackByTestPackCode(code);
		
		//String testPackVoJson = JSONArray.fromObject(testPackVo).toString();
		//String testPackLanguagePacksJson = JSONArray.fromObject(testPackLanguagePacks).toString();		
		//this.jsonData =  "{'testPackVo':'"+testPackVoJson+"','testPackLanguagePacks':'"+testPackLanguagePacksJson+"'}";
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("testPackVo", testPackVo);
		map.put("testPackLanguagePacks", testPackLanguagePacks);
		
		this.jsonData = JSONArray.fromObject(map).toString();
		
		return SUCCESS;
	}

	
	
	@Action(value = "editTestPack", results = { @Result(name = "editTestPack", location = "/autel/backstage/member/test_pack_edit.jsp") })
	public String editTestPack() throws Exception {
		testPackVo = testPackService.getTestPackVoByCode(code);
		testPackLanguagePacks = testPackLanguagePackService.listLanguagePackByTestPackCode(code);
		this.languages = languageService.queryAllLanguage();
		this.softwareTypes = softwareTypeService.listAll();
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("testPackVo", testPackVo);
		map.put("testPackLanguagePacks", testPackLanguagePacks);
		
		this.jsonData = JSONArray.fromObject(map).toString();
		
		return "editTestPack";
	}

	/**
	 * 
	 * @Title: delTestPack
	 * @Description: 删除测试包
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "deltestpack", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String delTestPack() throws Exception{
		
		int result = -1;
		
		try{
			testPackService.delTestPackDetail(code);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.jsonData = JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}
	
	
	public TestPack getTestPack() {
		return testPack;
	}

	public void setTestPack(TestPack testPack) {
		this.testPack = testPack;
	}

	public TestPackVo getTestPackVo() {
		return testPackVo;
	}

	public void setTestPackVo(TestPackVo testPackVo) {
		this.testPackVo = testPackVo;
	}

	public List<TestPackVo> getTestPackVos() {
		return testPackVos;
	}

	public void setTestPackVos(List<TestPackVo> testPackVos) {
		this.testPackVos = testPackVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public TestPackVo getTestPackVoSel() {
		return testPackVoSel;
	}

	public void setTestPackVoSel(TestPackVo testPackVoSel) {
		this.testPackVoSel = testPackVoSel;
	}

	public TestPack getTestPackAdd() {
		return testPackAdd;
	}

	public void setTestPackAdd(TestPack testPackAdd) {
		this.testPackAdd = testPackAdd;
	}

	public TestPack getTestPackEdit() {
		return testPackEdit;
	}

	public void setTestPackEdit(TestPack testPackEdit) {
		this.testPackEdit = testPackEdit;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public List<TestPackLanguagePack> getTestPackLanguagePacks() {
		return testPackLanguagePacks;
	}

	public void setTestPackLanguagePacks(
			List<TestPackLanguagePack> testPackLanguagePacks) {
		this.testPackLanguagePacks = testPackLanguagePacks;
	}

	public List<SoftwareType> getSoftwareTypes() {
		return softwareTypes;
	}

	public void setSoftwareTypes(List<SoftwareType> softwareTypes) {
		this.softwareTypes = softwareTypes;
	}

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	public List<ProductType> getProductTypeList() {
		return productTypeList;
	}

	public void setProductTypeList(List<ProductType> productTypeList) {
		this.productTypeList = productTypeList;
	}

}	
