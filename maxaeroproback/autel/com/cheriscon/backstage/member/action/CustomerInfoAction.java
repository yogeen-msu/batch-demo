package com.cheriscon.backstage.member.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.dao.DataIntegrityViolationException;

import com.cheriscon.app.base.core.model.AuthAction;
import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ICustomerActiveLogService;
import com.cheriscon.backstage.member.service.ICustomerChangeAutelIdLogService;
import com.cheriscon.backstage.member.service.ICustomerInfoVoService;
import com.cheriscon.backstage.member.vo.CustomerInfoVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerActiveLog;
import com.cheriscon.common.model.CustomerChangeAutelIdLog;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.utils.DesModule;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;

/**
 * @remark	客户信息action
 * @author pengdongan
 * @date   2013-01-07
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerInfoAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private ICustomerInfoVoService customerInfoVoService;

	@Resource
	private ICustomerInfoService customerInfoService;

	@Resource
	private ILanguageService languageService;
	
	@Resource
	private IAdminUserManager adminUserManager;
	
	@Resource
	private ICustomerActiveLogService customerActiveService;
	
	@Resource
	private ICustomerChangeAutelIdLogService custmerChangeAutelIdService;
	@Resource
	private IProductInfoService productInfoService;
	
	private CustomerInfo customerInfo;
	
	private CustomerInfoVo customerInfoVo;
	
	private List<CustomerInfoVo> customerInfoVos;
	
	private List<Language> languages;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private CustomerInfoVo customerInfoVoSel;
	private CustomerInfo customerInfoEdit;
	private CustomerChangeAutelIdLog changeAutelIdLog;
	


	private String jsonData;
	
	private String languageCodeCN;
	
	InputStream is;
	InputStream excelStream;
	
	
	//获取用户信息列表
	public void list(){
		try{
			
			AdminUser user = adminUserManager.getCurrentUser();
			List<AuthAction> list=user.getAuthList();   //网站资料录入  //
			String name="";
			for(int i=0;i<list.size();i++){
				AuthAction a=list.get(i);
				name=a.getName();
			}
			if(name.equals("网站资料录入")){
				if(customerInfoVoSel!=null){
					this.webpage = customerInfoVoService.pageCustomerInfoVoPage2("2",customerInfoVoSel, this.getPage(), this.getPageSize());
				}
			}else if(name.equals("国内售后")){
				if(customerInfoVoSel!=null){
					this.webpage = customerInfoVoService.pageCustomerInfoVoPage2("1",customerInfoVoSel, this.getPage(), this.getPageSize());
				}
			}else{
				this.webpage = customerInfoVoService.pageCustomerInfoVoPage(customerInfoVoSel, this.getPage(), this.getPageSize());
			}
			}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listcustomer", results = { @Result(name = "listcustomer", location = "/autel/backstage/member/customer_info_list.jsp") })
	public String listCustomer() throws Exception{
		if(customerInfoVoSel!=null){
			list();
		}
		return "listcustomer";
	}
	
	@Action(value = "updatePwd", results = { @Result(name = "updatePwd", location = "/autel/backstage/member/customer_update_pwd.jsp") })
	public String updatePwd() throws Exception{
		customerInfoEdit = customerInfoService.getCustomerByCode(code);
		return "updatePwd";
	}
    
	@Action(value = "changeAutel", results = { @Result(name = "changeAutel", location = "/autel/backstage/member/customer_change_list.jsp") })
	public String changeAutel() throws Exception{
		
		this.webpage =custmerChangeAutelIdService.pageCustomerChangeAutelPage(changeAutelIdLog, this.getPage(), this.getPageSize());
		return "changeAutel";
	}
	
	
	@Action(value = "addchangeAutelLog", results = { @Result(name = "addchangeAutelLog", location = "/autel/backstage/member/customer_change_add.jsp") })
	public String addchangeAutelLog() throws Exception{
		return "addchangeAutelLog";
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "saveChangeAutelLog")
	public String saveChangeAutelLog() throws Exception{
		
		AdminUser user = adminUserManager.getCurrentUser();
		user = adminUserManager.get(user.getUserid());
		CustomerInfo oldCust=customerInfoService.getCustBySerialNo(changeAutelIdLog.getProductSn().trim());
		if(null == oldCust){
			this.msgs.add("产品不存在或者没有注册！");
			this.urls.put("返回","javascript:window.history.go(-1)");
			return MESSAGE;
		}
		
		ProductInfo info=productInfoService.getBySerialNo(changeAutelIdLog.getProductSn());
		
		CustomerInfo newCust=customerInfoService.getCustomerInfoByAutelId(changeAutelIdLog.getNewAutelId().trim());
		if(null == newCust){
			this.msgs.add("New Autel ID 不存在！");
			this.urls.put("返回","javascript:window.history.go(-1)");
			return MESSAGE;
		}
		
		if(newCust.getActState()==0){
			this.msgs.add("New Autel ID 没有激活！");
			this.urls.put("返回","javascript:window.history.go(-1)");
			return MESSAGE;	
		}
		
		changeAutelIdLog.setOldCustomerCode(oldCust.getCode());
		changeAutelIdLog.setOldAutelId(oldCust.getAutelId());
		changeAutelIdLog.setNewAutelId(newCust.getAutelId());
		changeAutelIdLog.setNewCustomerCode(newCust.getCode());
		changeAutelIdLog.setOperatorUser(user.getUsername());
		changeAutelIdLog.setOperatorDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
		changeAutelIdLog.setOperatorIp(FrontConstant.getIpAddr(getRequest()));
		changeAutelIdLog.setProCode(info.getCode());
		custmerChangeAutelIdService.saveLog(changeAutelIdLog);
		
		
		
		this.msgs.add("操作成功！");
		this.urls.put("返回列表","changeAutel.do");
		return MESSAGE;
	}
	
	@Action(value = "getCustomerInfoBySerialNo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getCustomerInfoBySerialNo() throws Exception {
		
		CustomerInfo info=customerInfoService.getCustBySerialNo2(changeAutelIdLog.getProductSn());
		if(null != info ){
			 this.jsonData = info.getAutelId();
		}
		return SUCCESS;
	}
	
	
	
	@Action(value = "savePwd")
	public String savePwd() throws Exception{
		//customerInfoEdit = customerInfoService.getCustomerByCode(customerInfoEdit.getCode());
		int result = -1;
		try{
			
			customerInfoEdit.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(customerInfoEdit.getUserPwd(),null));
			customerInfoService.updateCustomerPwd(customerInfoEdit.getCode(),customerInfoEdit.getUserPwd());
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.customman.message24"));
		}
		else
		{
			this.msgs.add(getText("memberman.customman.message25"));
		}

		this.urls.put(getText("memberman.customman.message20"),"listActiveCustomer.do");
		
		return MESSAGE;
	}
	
	
	@Action(value = "listActiveCustomer", results = { @Result(name = "listActiveCustomer", location = "/autel/backstage/member/customer_active_list.jsp") })
	public String listActiveCustomer() throws Exception{
//		if(customerInfoVoSel==null){
//			customerInfoVoSel =new CustomerInfoVo();
//			customerInfoVoSel.setActCode(actCode)
//		}
		if(customerInfoVoSel!=null){
			this.webpage = customerInfoVoService.pageCustomerInfoVoPage(customerInfoVoSel, this.getPage(), this.getPageSize());
		}
		return "listActiveCustomer";
	}
	
    
	@Action(value = "editcustomer", results = { @Result(name = "editcustomer", location = "/autel/backstage/member/customer_info_edit.jsp") })
	public String editcustomer() throws Exception{
		try{
			customerInfoEdit = customerInfoService.getCustomerByCode(code);
			this.languages = languageService.queryLanguage();
			languageCodeCN = languageService.getByLocale(Locale.SIMPLIFIED_CHINESE).getCode();
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editcustomer";
	}

//	@Action(value = "updatecustomer", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updatecustomer")
	public String updatecustomer() throws Exception{
		int result = -1;

		try{
			String languageCodeCN = languageService.getByLocale(Locale.SIMPLIFIED_CHINESE).getCode();
			
			if(!customerInfoEdit.getLanguageCode().equals(languageCodeCN))
			{
				customerInfoEdit.setName(customerInfoEdit.getFirstName()
					+customerInfoEdit.getMiddleName()+customerInfoEdit.getLastName());
			}
			
			if(StringUtils.isNotBlank(customerInfoEdit.getUserPwd()))
			{
				//customerInfoEdit.setUserPwd(DesModule.getInstance().encrypt(customerInfoEdit.getUserPwd()));
				// add by A16043 20160824  特殊字符处理，放置sql注入
				String userpwd = customerInfoEdit.getUserPwd();
				userpwd=userpwd.replaceAll("\\'", "‘");
				userpwd=userpwd.replaceAll("--", "－－");
				userpwd=userpwd.replaceAll("\\*", "×");
			 	//防止跨站点脚本攻击的过滤
				userpwd=userpwd.replaceAll("<", "&lt;");
				// end 20160824
				customerInfoEdit.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(userpwd,null));
			}
			else
			{
				customerInfoEdit.setUserPwd(null);
			}
			
			if(customerInfoEdit.getIsAllowSendEmail() == null)
			{
				customerInfoEdit.setIsAllowSendEmail(0);
			}
		
			customerInfoService.updateCustomer(customerInfoEdit);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.customman.message24"));
		}
		else
		{
			this.msgs.add(getText("memberman.customman.message25"));
		}

		this.urls.put(getText("memberman.customman.message20"),"listcustomer.do");
		
		return MESSAGE;
	}

//	@Action(value = "delcustomer", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delcustomer")
	public String delcustomer() throws Exception{
		int result = -1;
		try{
			boolean flag=getRule();
			if(flag!=true){
				result=9;
			}else{
				customerInfoService.delCustomer(code);
				result = 0;
			}
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.customman.message26"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.customman.message27"));
		}
		else if(result == 9)
		{
			this.msgs.add("您没有权限删除");
		}
		else
		{
			this.msgs.add(getText("memberman.customman.message28"));
		}

		this.urls.put(getText("memberman.customman.message20"),"listcustomer.do");
		
		return MESSAGE;
	}

//	@Action(value = "delcustomers", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delcustomers")
	public String delcustomers() throws Exception{
		int result = -1;
		try{
			boolean flag=getRule();
			if(flag!=true){
				result=9;
			}else{
				selectedcodes = selectedcodes.replace("‘", "'");
				customerInfoService.delCustomers(selectedcodes);
				result = 0;
			}
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.customman.message26"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.customman.message27"));
		}
		else if(result == 9)
		{
			this.msgs.add(getText("您没有权限删除"));
		}
		else
		{
			this.msgs.add(getText("memberman.customman.message28"));
		}

		this.urls.put(getText("memberman.customman.message20"),"listcustomer.do");
		
		return MESSAGE;
	}
	
	@Action(value = "activecustomer")
	public String activecustomer() throws Exception{
		int result = -1;
		try{
			customerInfo=customerInfoService.getCustomerByCode(code);
			customerInfo.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
			customerInfoService.updateCustomer(customerInfo);
			
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			
			CustomerActiveLog log=new CustomerActiveLog();
			log.setActiveUser(user.getUsername());
			log.setActiveTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			log.setCustomerCode(code);
			customerActiveService.saveLog(log);
			
			
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}				
		if(result == 0)
		{
			this.msgs.add(getText("customer.active.success"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.customman.message27"));
		}
		else
		{
			this.msgs.add(getText("customer.active.fail"));
		}

		this.urls.put(getText("common.js.returnlist"),"listActiveCustomer.do");
		
		return MESSAGE;
	}
	
	public boolean getRule(){
		AdminUser user = adminUserManager.getCurrentUser();
		List<AuthAction> list=user.getAuthList();
		boolean flag=false;
		for(int i=0;i<list.size();i++){
			AuthAction a=list.get(i);
			if(a.getName().equals("管理员")){
				flag=true;
			}
		}
		return flag;
	}
	
	
	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public CustomerInfoVo getCustomerInfoVo() {
		return customerInfoVo;
	}

	public void setCustomerInfoVo(CustomerInfoVo customerInfoVo) {
		this.customerInfoVo = customerInfoVo;
	}

	public List<CustomerInfoVo> getCustomerInfoVos() {
		return customerInfoVos;
	}

	public void setCustomerInfoVos(List<CustomerInfoVo> customerInfoVos) {
		this.customerInfoVos = customerInfoVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public CustomerInfoVo getCustomerInfoVoSel() {
		return customerInfoVoSel;
	}

	public void setCustomerInfoVoSel(CustomerInfoVo customerInfoVoSel) {
		this.customerInfoVoSel = customerInfoVoSel;
	}

	public CustomerInfo getCustomerInfoEdit() {
		return customerInfoEdit;
	}

	public void setCustomerInfoEdit(CustomerInfo customerInfoEdit) {
		this.customerInfoEdit = customerInfoEdit;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public InputStream getExcelStream() {
		return excelStream;
	}

	public void setExcelStream(InputStream excelStream) {
		this.excelStream = excelStream;
	}

	public String getLanguageCodeCN() {
		return languageCodeCN;
	}

	public void setLanguageCodeCN(String languageCodeCN) {
		this.languageCodeCN = languageCodeCN;
	}
	public CustomerChangeAutelIdLog getChangeAutelIdLog() {
		return changeAutelIdLog;
	}

	public void setChangeAutelIdLog(CustomerChangeAutelIdLog changeAutelIdLog) {
		this.changeAutelIdLog = changeAutelIdLog;
	}
}	
