package com.cheriscon.backstage.member.action;



import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ICustomerComplaintSourceService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeService;
import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.backstage.system.service.ILanguageService;

import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.CustomerComplaintSource;
import com.cheriscon.common.model.CustomerComplaintType;
import com.cheriscon.common.model.CustomerComplaintTypeName;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	客诉类型action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerComplaintTypeAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6693265756340979658L;

	@Resource
	private ICustomerComplaintTypeService customerComplaintTypeService;
	@Resource
	private ICustomerComplaintTypeNameService customerComplaintTypeNameService;
	@Resource
	private ILanguageService languageService;
	
	private CustomerComplaintType customerComplaintType;
	
	private List<CustomerComplaintType> customerComplaintTypes;
	
	private List<CustomerComplaintTypeName> customerComplaintTypeNames;
	
	private List<Language> languages;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private CustomerComplaintType customerComplaintTypeSel;
	private CustomerComplaintType customerComplaintTypeAdd;
	private CustomerComplaintType customerComplaintTypeEdit;
	
	/**
	 * 需删除的名称
	 */
	private List<CustomerComplaintTypeName> delCustomerComplaintTypeNames;
	
	private String jsonData;
	
	
	//获取客诉类型列表
	public void list(){
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			customerComplaintTypeSel = customerComplaintTypeSel != null ? customerComplaintTypeSel : new CustomerComplaintType();
			customerComplaintTypeSel.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			this.webpage = customerComplaintTypeService.pageCustomerComplaintTypePage(customerComplaintTypeSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listcustomercomplainttype", results = { @Result(name = "listcustomercomplainttype", location = "/autel/backstage/member/complaint_type_list.jsp") })
	public String listCustomerComplaintType() throws Exception{
		list();
		return "listcustomercomplainttype";
	}

	@Action(value = "addcustomercomplainttype", results = { @Result(name = "addcustomercomplainttype", location = "/autel/backstage/member/complaint_type_add.jsp") })
	public String addCustomerComplaintType() throws Exception{
		return "addcustomercomplainttype";
	}

//	@Action(value = "insertcustomercomplainttype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertcustomercomplainttype")
	public String insertCustomerComplaintType() throws Exception{
		int result = -1;
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			customerComplaintTypeAdd.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			customerComplaintTypeService.insertCustomerComplaintType(customerComplaintTypeAdd);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.complainttypeman.message21"));
		}
		else
		{
			this.msgs.add(getText("memberman.complainttypeman.message22"));
		}

		this.urls.put(getText("memberman.complainttypeman.message20"),"listcustomercomplainttype.do");
		
		return MESSAGE;
	}

	@Action(value = "editcustomercomplainttype", results = { @Result(name = "editcustomercomplainttype", location = "/autel/backstage/member/complaint_type_edit.jsp") })
	public String editCustomerComplaintType() throws Exception{
		try{
			this.customerComplaintType = this.customerComplaintTypeService.getCustomerComplaintTypeByCode(code);
			this.customerComplaintTypeNames = this.customerComplaintTypeNameService.queryByTypeCode(code);
			this.languages = this.customerComplaintTypeNameService.getAvailableLanguage(code);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editcustomercomplainttype";
	}

//	@Action(value = "updatecustomercomplainttype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updatecustomercomplainttype")
	public String updateCustomerComplaintType() throws Exception{
		int result = -1;
		
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			customerComplaintTypeEdit.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			customerComplaintTypeService.updateCustomerComplaintType(customerComplaintTypeEdit);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.complainttypeman.message23"));
		}
		else
		{
			this.msgs.add(getText("memberman.complainttypeman.message24"));
		}

		this.urls.put(getText("memberman.complainttypeman.message20"),"listcustomercomplainttype.do");
		
		return MESSAGE;
	}

	@Action(value = "updateCustomerComplaintTypeName")
	public String updateCustomerComplaintTypeName() throws Exception {
		int result = -1;
		try {
			customerComplaintTypeNameService.updateCustomerComplaintTypeNames(customerComplaintTypeNames,
					delCustomerComplaintTypeNames);
			result = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.complainttypeman.message23"));
		}
		else
		{
			this.msgs.add(getText("memberman.complainttypeman.message24"));
		}

		this.urls.put(getText("memberman.complainttypeman.message20"),"listcustomercomplainttype.do");

		return MESSAGE;
	}

//	@Action(value = "delcustomercomplainttype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delcustomercomplainttype")
	public String delCustomerComplaintType() throws Exception{
		int result = -1;
		try{
			customerComplaintTypeService.delCustomerComplaintTypeDetail(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.complainttypeman.message25"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.complainttypeman.message26"));
		}
		else
		{
			this.msgs.add(getText("memberman.complainttypeman.message26"));
		}

		this.urls.put(getText("memberman.complainttypeman.message20"),"listcustomercomplainttype.do");
		
		return MESSAGE;
	}

//	@Action(value = "delcustomercomplainttypes", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delcustomercomplainttypes")
	public String delCustomerComplaintTypes() throws Exception{
		int result = -1;
		try{
			customerComplaintTypeService.delCustomerComplaintTypes(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.complainttypeman.message25"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.complainttypeman.message26"));
		}
		else
		{
			this.msgs.add(getText("memberman.complainttypeman.message27"));
		}

		this.urls.put(getText("memberman.complainttypeman.message20"),"listcustomercomplainttype.do");
		
		return MESSAGE;
	}
	
	
	public CustomerComplaintType getCustomerComplaintType() {
		return customerComplaintType;
	}

	public void setCustomerComplaintType(CustomerComplaintType customerComplaintType) {
		this.customerComplaintType = customerComplaintType;
	}

	public List<CustomerComplaintType> getCustomerComplaintTypes() {
		return customerComplaintTypes;
	}

	public void setCustomerComplaintTypes(List<CustomerComplaintType> customerComplaintTypes) {
		this.customerComplaintTypes = customerComplaintTypes;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public CustomerComplaintType getCustomerComplaintTypeSel() {
		return customerComplaintTypeSel;
	}

	public void setCustomerComplaintTypeSel(CustomerComplaintType customerComplaintTypeSel) {
		this.customerComplaintTypeSel = customerComplaintTypeSel;
	}

	public CustomerComplaintType getCustomerComplaintTypeAdd() {
		return customerComplaintTypeAdd;
	}

	public void setCustomerComplaintTypeAdd(CustomerComplaintType customerComplaintTypeAdd) {
		this.customerComplaintTypeAdd = customerComplaintTypeAdd;
	}

	public CustomerComplaintType getCustomerComplaintTypeEdit() {
		return customerComplaintTypeEdit;
	}

	public void setCustomerComplaintTypeEdit(CustomerComplaintType customerComplaintTypeEdit) {
		this.customerComplaintTypeEdit = customerComplaintTypeEdit;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public List<CustomerComplaintTypeName> getCustomerComplaintTypeNames() {
		return customerComplaintTypeNames;
	}

	public void setCustomerComplaintTypeNames(
			List<CustomerComplaintTypeName> customerComplaintTypeNames) {
		this.customerComplaintTypeNames = customerComplaintTypeNames;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public List<CustomerComplaintTypeName> getDelCustomerComplaintTypeNames() {
		return delCustomerComplaintTypeNames;
	}

	public void setDelCustomerComplaintTypeNames(
			List<CustomerComplaintTypeName> delCustomerComplaintTypeNames) {
		this.delCustomerComplaintTypeNames = delCustomerComplaintTypeNames;
	}

}	
