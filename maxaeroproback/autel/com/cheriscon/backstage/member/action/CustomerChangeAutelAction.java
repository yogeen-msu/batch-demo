package com.cheriscon.backstage.member.action;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ICustomerActiveLogService;
import com.cheriscon.backstage.member.service.ICustomerChangeAutelService;
import com.cheriscon.common.model.CustomerActiveLog;
import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/member")
@Results({
	@Result(name="list" ,location="/autel/backstage/customerChangeAutel/customerChangeAutel_list.jsp")
})
@ExceptionMappings({ @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerChangeAutelAction extends WWAction {

	@Resource
	private ICustomerChangeAutelService customerChangeAutelService;
	@Resource
	private ICustomerActiveLogService customerActiveLogService;
	@Resource
	private IAdminUserManager adminUserManager;
	
	private CustomerChangeAutel customerChangeAutel;
	
	public String list() {
		try {
			if(customerChangeAutel!=null){
				this.webpage=customerChangeAutelService.getCustomerChangeAutels(customerChangeAutel, this.getPage(), this.getPageSize());
			}
		} catch (Exception e) {
		}
		return "list";
	}
	
	public String active(){
		try {
			customerChangeAutelService.updateCustomerChangeAutelStatus(customerChangeAutel);
			CustomerActiveLog customerActiveLog =new CustomerActiveLog();
			customerActiveLog.setActiveTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			customerActiveLog.setCustomerCode(customerChangeAutel.getCustomerCode());
			customerActiveLog.setActiveUser(adminUserManager.getCurrentUser().getUsername());
			customerActiveLogService.saveLog(customerActiveLog);
			this.msgs.add("激活成功");
			this.urls.put("返回列表", "customer-change-autel!list.do");
		} catch (Exception e) {
			this.msgs.add("激活失败");
			this.urls.put("返回列表", "customer-change-autel!list.do");
		}
		return MESSAGE;
	}
	
	public CustomerChangeAutel getCustomerChangeAutel() {
		return customerChangeAutel;
	}
	public void setCustomerChangeAutel(CustomerChangeAutel customerChangeAutel) {
		this.customerChangeAutel = customerChangeAutel;
	}
	
}
