package com.cheriscon.backstage.member.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ICustomerInfoVoService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeLogService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.vo.CustomerInfoVo;
import com.cheriscon.common.constant.BackageConstant;
import com.cheriscon.common.model.ProductSoftwareValidChangeLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/member/softwareValidStatus_list.jsp"),
	@Result(name = "list_customer_order_info", location = "/autel/backstage/member/customer_order_info_list.jsp")
})
public class ProductSoftwareValidStatusAction extends WWAction{
	
	private int id;
	private String validDate;		//最小销售单位
	private ProductSoftwareValidStatus softwareValidStatus;
	private CustomerInfoVo customerInfoVo;
	private String productSN;
	public String getProductSN() {
		return productSN;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	@Resource
	private ICustomerInfoVoService customerInfoVoService;
	@Resource
	private IProductSoftwareValidStatusService productSoftwareValidStatusService; 
	
	@Resource
	private IProductSoftwareValidChangeLogService ProductSoftwareValidChangeLogService;
	@Resource
	private IAdminUserManager adminUserManager;
	/**
	 * 显示列表
	 * @return
	 */
	public String list() {
		if(softwareValidStatus!=null){
			this.webpage = productSoftwareValidStatusService.pageProductSoftwareValidStatus(softwareValidStatus,this.getPage(), this.getPageSize());
		}
		return "list";
	}
	
	@SuppressWarnings("unchecked")
	public String changeValidDate() throws Exception {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		try{
			//1.根据产品序列号取出标准销售配置中有效期
			List<ProductSoftwareValidStatus> list=productSoftwareValidStatusService.getProductSoftwareValidList(productSN);
			if(list==null || list.size()==0){
				this.json="{result:0}";
			}else{
				for(int i=0 ;i<list.size();i++){
					ProductSoftwareValidStatus valid=list.get(i);
					
					ProductSoftwareValidChangeLog log=new ProductSoftwareValidChangeLog();
					AdminUser user = adminUserManager.getCurrentUser();
					user = adminUserManager.get(user.getUserid());
					String ip=FrontConstant.getIpAddr(request);
					log.setProductSN(productSN);
					log.setMinSaleUnit(valid.getMinSaleUnitCode());
					log.setNewDate(validDate);
					log.setOldDate(valid.getValidDate());
					log.setOperatorIp(ip);
					log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
					log.setOperatorUser(user.getUsername());
					log.setOperType(BackageConstant.PRODUCT_VALID_CHANGE_SUPPORT);
					ProductSoftwareValidChangeLogService.saveProductSoftwareValidStatus(log);
					
					valid.setValidDate(validDate);
					productSoftwareValidStatusService.updateValidDate(valid);
					
					this.json="{result:1}";
				}
				
			}
		}catch(Exception e){
			this.json="{result:0}";
		}
		
		return JSON_MESSAGE;
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	public String editSave() throws Exception {
		try{
			softwareValidStatus = new ProductSoftwareValidStatus();
			softwareValidStatus.setId(id);
			softwareValidStatus.setValidDate(validDate);
			productSoftwareValidStatusService.updateValidDate(softwareValidStatus);
			this.json="{result:1,date:'"+validDate+"',id:"+id+"}";
		}catch(RuntimeException e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}
		return JSON_MESSAGE;
	}
	
	
	/**
	 * 查询客户消费记录 
	 * @return
	 */
	public String listCustomerOrderInfo(){
		if(customerInfoVo!=null){
			this.webpage =  customerInfoVoService.queryCustomerOrderInfo(customerInfoVo,this.getPage(),this.getPageSize());
		}
		return "list_customer_order_info";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

	public ProductSoftwareValidStatus getSoftwareValidStatus() {
		return softwareValidStatus;
	}

	public void setSoftwareValidStatus(
			ProductSoftwareValidStatus softwareValidStatus) {
		this.softwareValidStatus = softwareValidStatus;
	}

	public CustomerInfoVo getCustomerInfoVo() {
		return customerInfoVo;
	}

	public void setCustomerInfoVo(CustomerInfoVo customerInfoVo) {
		this.customerInfoVo = customerInfoVo;
	}

}
