package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.dao.DataIntegrityViolationException;

import com.cheriscon.backstage.member.service.ISealerTypeService;
import com.cheriscon.backstage.member.vo.SealerTypeVo;

import com.cheriscon.common.model.SealerType;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	经销商类型action
 * @author pengdongan
 * @date   2013-01-07
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class SealerTypeAction extends WWAction{
	
	private static final long serialVersionUID = -644859255038113322L;

	@Resource
	private ISealerTypeService sealerTypeService;
	
	private SealerType sealerType;
	
	private SealerTypeVo sealerTypeVo;
	
	private List<SealerTypeVo> sealerTypeVos;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private SealerTypeVo sealerTypeVoSel;
	private SealerType sealerTypeAdd;
	private SealerType sealerTypeEdit;
	
	private String jsonData;
	
	
	//获取经销商类型列表
	public void list(){
		try{
			this.webpage = sealerTypeService.pageSealerTypeVoPage(sealerTypeVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listsealertype", results = { @Result(name = "listsealertype", location = "/autel/backstage/member/sealer_type_list.jsp") })
	public String listSealerType() throws Exception{
		list();
		return "listsealertype";
	}

	@Action(value = "addsealertype", results = { @Result(name = "addsealertype", location = "/autel/backstage/member/sealer_type_add.jsp") })
	public String addSealerType() throws Exception{
		return "addsealertype";
	}

//	@Action(value = "insertsealertype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertsealertype")
	public String insertSealerType() throws Exception{
		int result = -1;
		try{
			sealerType = sealerTypeService.getSealerTypeByName(sealerTypeAdd.getName());
			if(sealerType == null)
			{
				sealerTypeService.insertSealerType(sealerTypeAdd);
				result = 0;
			}
			else {
				result = 2;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealertypeman.message21"));
		}
		else if(result == 2)
		{
			this.msgs.add(getText("memberman.sealertypeman.message22"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealertypeman.message23"));
		}

		this.urls.put(getText("memberman.sealertypeman.message20"),"listsealertype.do");
		
		return MESSAGE;
	}

	@Action(value = "editsealertype", results = { @Result(name = "editsealertype", location = "/autel/backstage/member/sealer_type_edit.jsp") })
	public String editSealerType() throws Exception{
		try{
			this.sealerTypeEdit = sealerTypeService.getSealerTypeByCode(code);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editsealertype";
	}

//	@Action(value = "updatesealertype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updatesealertype")
	public String updateSealerType() throws Exception{
		int result = -1;
		
		try{
			sealerType = sealerTypeService.getSealerTypeByCode(sealerTypeEdit.getCode());

			if(sealerType != null)
			{
				sealerType.setCode(sealerTypeEdit.getCode());
				sealerType.setName(sealerTypeEdit.getName());
				sealerTypeService.updateSealerType(sealerType);
				result = 0;
			}
			else
			{
				result = 1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealertypeman.message24"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealertypeman.message25"));
		}

		this.urls.put(getText("memberman.sealertypeman.message20"),"listsealertype.do");
		
		return MESSAGE;
	}

//	@Action(value = "delsealertype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delsealertype")
	public String delSealerType() throws Exception{
		int result = -1;
		try{
			sealerTypeService.delSealerType(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealertypeman.message26"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.sealertypeman.message27"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealertypeman.message28"));
		}

		this.urls.put(getText("memberman.sealertypeman.message20"),"listsealertype.do");
		
		return MESSAGE;
	}

//	@Action(value = "delsealers", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delsealers")
	public String delSealerTypes() throws Exception{
		int result = -1;
		try{
			sealerTypeService.delSealerTypes(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealertypeman.message26"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.sealertypeman.message27"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealertypeman.message28"));
		}

		this.urls.put(getText("memberman.sealertypeman.message20"),"listsealertype.do");
		
		return MESSAGE;
	}
	
	
	public SealerType getSealerType() {
		return sealerType;
	}

	public void setSealerType(SealerType sealerType) {
		this.sealerType = sealerType;
	}

	public SealerTypeVo getSealerTypeVo() {
		return sealerTypeVo;
	}

	public void setSealerTypeVo(SealerTypeVo sealerTypeVo) {
		this.sealerTypeVo = sealerTypeVo;
	}

	public List<SealerTypeVo> getSealerTypeVos() {
		return sealerTypeVos;
	}

	public void setSealerTypeVos(List<SealerTypeVo> sealerTypeVos) {
		this.sealerTypeVos = sealerTypeVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public SealerTypeVo getSealerTypeVoSel() {
		return sealerTypeVoSel;
	}

	public void setSealerTypeVoSel(SealerTypeVo sealerTypeVoSel) {
		this.sealerTypeVoSel = sealerTypeVoSel;
	}

	public SealerType getSealerTypeAdd() {
		return sealerTypeAdd;
	}

	public void setSealerTypeAdd(SealerType sealerTypeAdd) {
		this.sealerTypeAdd = sealerTypeAdd;
	}

	public SealerType getSealerTypeEdit() {
		return sealerTypeEdit;
	}

	public void setSealerTypeEdit(SealerType sealerTypeEdit) {
		this.sealerTypeEdit = sealerTypeEdit;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

}	
