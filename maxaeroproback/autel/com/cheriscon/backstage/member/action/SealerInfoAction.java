package com.cheriscon.backstage.member.action;

import java.io.File;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.SealerInfoVo;
import com.cheriscon.backstage.system.service.ICountryCodeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IStateProvinceService;
import com.cheriscon.common.model.CountryCode;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.StateProvince;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;
import com.cheriscon.framework.util.DateUtil;

/**
 * @remark	经销商信息action
 * @author pengdongan
 * @date   2013-01-07
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class SealerInfoAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private ISealerInfoService sealerInfoService;

	@Resource
	private ILanguageService languageService;
	
	@Resource
	private ICountryCodeService countryCodeService;
	
	@Resource
	private IStateProvinceService stateProvinceService;
	
	private SealerInfo sealerInfo;
	
	private SealerInfoVo sealerInfoVo;
	
	private List<SealerInfoVo> sealerInfoVos;
	
	private List<Language> languages;
	
	private List<CountryCode> countryCodes;
	
	private List<StateProvince> stateProvinces;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private SealerInfoVo sealerInfoVoSel;
	private SealerInfo sealerInfoAdd;
	private SealerInfo sealerInfoEdit;
	
	private String jsonData;
	
	private File sealerFile;
	
	private int result;
	
	private String languageCodeCN;
	
	
	//获取经销商信息列表
	public void list(){
		try{
			this.webpage = sealerInfoService.pageSealerInfoVoPage(sealerInfoVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listsealer", results = { @Result(name = "listsealer", location = "/autel/backstage/member/sealer_info_list.jsp") })
	public String listSealer() throws Exception{
		list();
		return "listsealer";
	}

	@Action(value = "createsealerinfo", results = { @Result(name = "createsealerinfo", location = "/autel/backstage/member/sealer_info_create.jsp") })
	public String createSealerInfo() throws Exception{
		return "createsealerinfo";
	}

	/**
	 * 将充值卡信息导入数据库
	 * @return
	 * @throws Exception
	 */
//	@Action(value = "savecreatesealer", results = { @Result(name = "savecreatesealer", location = "/autel/backstage/member/iframe_result.jsp")})
	@Action(value = "savecreatesealer")
	public String saveCreateSealer() throws Exception{
		this.result = -1;
		if(sealerFile != null)
		{
			try{
				this.result = sealerInfoService.saveCreateSealer(sealerFile);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		//return "savecreatesealer";
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message31"));
		}		
		else if(result == 1)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message33"));
		}		
		else if(result == 2)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message34"));
		}		
		else if(result == 3)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message35"));
		}		
		else if(result == 4)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message36"));
		}		
		else if(result == 5)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message37"));
		}		
		else if(result == 6)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message38"));
		}
		else if(result == 7)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message39"));
		}
		else if(result == 8)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message40"));
		}
		else if(result == 9)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message41"));
		}
		else if(result == 10)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message42"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message43"));
		}
		else if(result == 12)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message44"));
		}
		else if(result == 13)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message45"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealerinfoman.message32"));
		}

		this.urls.put(getText("memberman.sealerinfoman.message20"),"listsealer.do");
		
		return MESSAGE;
	}

	@Action(value = "addsealer", results = { @Result(name = "addsealer", location = "/autel/backstage/member/sealer_info_add.jsp") })
	public String addsealer() throws Exception{
		languageCodeCN = languageService.getByLocale(Locale.SIMPLIFIED_CHINESE).getCode();
		this.languages = languageService.queryLanguage();
		try {
			this.countryCodes = countryCodeService.getCountryCodeList();
			this.stateProvinces = stateProvinceService.getStateProvinceList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "addsealer";
	}

//	@Action(value = "insertsealer", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertsealer")
	public String insertsealer() throws Exception{
		int result = -1;
		try{
			sealerInfo = sealerInfoService.getSealerByAutelId(sealerInfoAdd.getAutelId());
			String languageCodeCN = languageService.getByLocale(Locale.SIMPLIFIED_CHINESE).getCode();
			
			if(sealerInfo == null)
			{
				sealerInfoAdd.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(sealerInfoAdd.getUserPwd(),null));
				sealerInfoAdd.setRegTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
				sealerInfoAdd.setLastLoginTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
	
				if(StringUtils.isEmpty(sealerInfoAdd.getName()))
				{
					sealerInfoAdd.setName(sealerInfoAdd.getFirstName()+sealerInfoAdd.getLastName());
				}
				
				
				if(sealerInfoAdd.getIsAllowSendEmail() == null)
				{
					sealerInfoAdd.setIsAllowSendEmail(0);
				}
				
				if(StringUtils.isNotEmpty(sealerInfoAdd.getCountry())){
					sealerInfoAdd.setCountryCode(sealerInfoAdd.getCountry().split(";")[0]);
//					sealerInfoAdd.setCountry(sealerInfoAdd.getCountry().split(";")[1]);
				}
				
				if(StringUtils.isNotEmpty(sealerInfoAdd.getProvince())){
					sealerInfoAdd.setStateCode(sealerInfoAdd.getProvince().split(";")[0]);
//					sealerInfoAdd.setProvince(sealerInfoAdd.getProvince().split(";")[1]);
				}
				
				sealerInfoService.insertSealer(sealerInfoAdd);
				result = 0;
			}
			else
			{
				result = 2;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message21"));
		}
		else if(result == 2)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message22"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealerinfoman.message23"));
		}

		this.urls.put(getText("memberman.sealerinfoman.message20"),"listsealer.do");
		
		return MESSAGE;
	}

	@Action(value = "editsealer", results = { @Result(name = "editsealer", location = "/autel/backstage/member/sealer_info_edit.jsp") })
	public String editsealer() throws Exception{
		try{
			this.sealerInfoEdit = sealerInfoService.getSealerByCode(code);
			this.languages = languageService.queryLanguage();
			languageCodeCN = languageService.getByLocale(Locale.SIMPLIFIED_CHINESE).getCode();
			this.countryCodes = countryCodeService.getCountryCodeList();
			this.stateProvinces = stateProvinceService.getStateProvinceList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editsealer";
	}
	
//	@Action(value = "updatesealer", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updatesealer")
	public String updatesealer() throws Exception{
		int result = -1;
		
		String languageCodeCN = languageService.getByLocale(Locale.SIMPLIFIED_CHINESE).getCode();
		
		try{
				
			if(StringUtils.isNotBlank(sealerInfoEdit.getUserPwd()))
			{
				sealerInfoEdit.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(sealerInfoEdit.getUserPwd(),null));
			}
			else
			{
				sealerInfoEdit.setUserPwd(null);
			}
	
			if(StringUtils.isEmpty(sealerInfoEdit.getName()))
			{
				sealerInfoEdit.setName(sealerInfoEdit.getFirstName()+sealerInfoEdit.getLastName());
			}
			
			if(sealerInfoEdit.getIsAllowSendEmail() == null)
			{
				sealerInfoEdit.setIsAllowSendEmail(0);
			}
			
			if(StringUtils.isNotEmpty(sealerInfoEdit.getCountry())){
				sealerInfoEdit.setCountryCode(sealerInfoEdit.getCountry().split(";")[0]);
//				sealerInfoEdit.setCountry(sealerInfoEdit.getCountry().split(";")[1]);
			}
			
			if(StringUtils.isNotEmpty(sealerInfoEdit.getProvince())){
				sealerInfoEdit.setStateCode(sealerInfoEdit.getProvince().split(";")[0]);
//				sealerInfoEdit.setProvince(sealerInfoEdit.getProvince().split(";")[1]);
			}
			
			sealerInfoService.updateSealer(sealerInfoEdit);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
//		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message24"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealerinfoman.message25"));
		}

		this.urls.put(getText("memberman.sealerinfoman.message20"),"listsealer.do");
		
		return MESSAGE;
	}

//	@Action(value = "delsealer", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delsealer")
	public String delsealer() throws Exception{
		int result = -1;
		try{
			sealerInfoService.delSealer(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message26"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message27"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealerinfoman.message28"));
		}

		this.urls.put(getText("memberman.sealerinfoman.message20"),"listsealer.do");
		
		return MESSAGE;
	}

//	@Action(value = "delsealers", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	public String delsealers() throws Exception{
		int result = -1;
		try{
			selectedcodes = selectedcodes.replace("‘", "'");
			sealerInfoService.delSealers(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;

		if(result == 0)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message26"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.sealerinfoman.message27"));
		}
		else
		{
			this.msgs.add(getText("memberman.sealerinfoman.message28"));
		}

		this.urls.put(getText("memberman.sealerinfoman.message20"),"listsealer.do");
		
		return MESSAGE;
	}
	
	/*@SuppressWarnings({ "unchecked", "static-access" })
	@Action(value = "toNexternal")
	public String toNexternal() throws Exception {
		SealerInfo sealerInfo = sealerInfoService.getSealerByCode(code);
		String nexternalUrl = FreeMarkerPaser.getBundleValue("nexternal.url");
		
		StringBuffer params = new StringBuffer();
		params.append("LAST_NAME=" +sealerInfo.getLastName());
		params.append("&FIRST_NAME=" +sealerInfo.getFirstName());
		params.append("&EMAIL=" +sealerInfo.getEmail());
		params.append("&ADDRESS1=" +sealerInfo.getAddress());
		params.append("&CITY=" +sealerInfo.getCity());
		params.append("&COMPANY_NAME=" +sealerInfo.getCompany());
		params.append("&STATE=" +sealerInfo.getStateCode());
		params.append("&POSTAL_CODE=" +sealerInfo.getZipCode());
		params.append("&COUNTRY=" +sealerInfo.getCountryCode());
		params.append("&ADDRESS_TYPE=0");
		params.append("&PHONE_NUM=" +sealerInfo.getTelephone());
		params.append("&Match1=EMAIL");
		params.append("&Mode=Display-Only");
		
		HttpPost httpPost = new HttpPost(nexternalUrl);
		StringEntity entity = new StringEntity(params.toString(), "UTF-8");
		entity.setContentType("application/x-www-form-urlencoded");
		httpPost.setEntity(entity);
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse response = httpClient.execute(httpPost);
		String result = EntityUtils.toString(response.getEntity(), "UTF-8");
		if(StringUtils.isNotEmpty(result) && this.SUCCESS.equals(result.toLowerCase())){
			this.msgs.add(getText("common.js.sycnSuccess"));
		}else{
			this.msgs.add(getText("common.js.sycnFail"));
		}
		this.urls.put(getText("memberman.sealerinfoman.message20"),"listsealer.do");
		return MESSAGE;
	}*/
	
	@Action(value = "toNexternal")
	public void toNexternal() throws Exception  {
		SealerInfo sealerInfo = sealerInfoService.getSealerByCode(code);
		String nexternalUrl = FreeMarkerPaser.getBundleValue("nexternal.url");

		StringBuffer outhtml = new StringBuffer();
		outhtml.append("<html><body>");
		outhtml.append("<form name=\"Customer\" method=\"post\" action=\""+nexternalUrl+"\">");
		outhtml.append("<input type=\"hidden\" name=\"LAST_NAME\" value=\""+sealerInfo.getLastName()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"FIRST_NAME\" value=\""+sealerInfo.getFirstName()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"EMAIL\" value=\""+sealerInfo.getEmail()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"ADDRESS1\" value=\""+sealerInfo.getAddress()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"CITY\" value=\""+sealerInfo.getCity()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"COMPANY_NAME\" value=\""+sealerInfo.getCompany()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"STATE\" value=\""+sealerInfo.getStateCode()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"POSTAL_CODE\" value=\""+sealerInfo.getZipCode()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"COUNTRY\" value=\""+sealerInfo.getCountryCode()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"ADDRESS_TYPE\" value=\"0\"/>");
		outhtml.append("<input type=\"hidden\" name=\"PHONE_NUM\" value=\""+sealerInfo.getTelephone()+"\"/>");
		outhtml.append("<input type=\"hidden\" name=\"Match1\" value=\"EMAIL\"/>");
		outhtml.append("<input type=\"hidden\" name=\"Mode\" value=\"Display-Only\"/>");
		outhtml.append("</form>");
		outhtml.append("<script type='text/javascript'>");
		outhtml.append("Customer.submit();");
		outhtml.append("</script>");
		outhtml.append("</body></html>");
		PrintWriter writer = getResponse().getWriter();
		writer.write(outhtml.toString());
		writer.flush();
		writer.close();
	}
	
	public SealerInfo getSealerInfo() {
		return sealerInfo;
	}

	public void setSealerInfo(SealerInfo sealerInfo) {
		this.sealerInfo = sealerInfo;
	}

	public SealerInfoVo getSealerInfoVo() {
		return sealerInfoVo;
	}

	public void setSealerInfoVo(SealerInfoVo sealerInfoVo) {
		this.sealerInfoVo = sealerInfoVo;
	}

	public List<SealerInfoVo> getSealerInfoVos() {
		return sealerInfoVos;
	}

	public void setSealerInfoVos(List<SealerInfoVo> sealerInfoVos) {
		this.sealerInfoVos = sealerInfoVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public SealerInfoVo getSealerInfoVoSel() {
		return sealerInfoVoSel;
	}

	public void setSealerInfoVoSel(SealerInfoVo sealerInfoVoSel) {
		this.sealerInfoVoSel = sealerInfoVoSel;
	}

	public SealerInfo getSealerInfoAdd() {
		return sealerInfoAdd;
	}

	public void setSealerInfoAdd(SealerInfo sealerInfoAdd) {
		this.sealerInfoAdd = sealerInfoAdd;
	}

	public SealerInfo getSealerInfoEdit() {
		return sealerInfoEdit;
	}

	public void setSealerInfoEdit(SealerInfo sealerInfoEdit) {
		this.sealerInfoEdit = sealerInfoEdit;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public File getSealerFile() {
		return sealerFile;
	}

	public void setSealerFile(File sealerFile) {
		this.sealerFile = sealerFile;
	}

	public String getLanguageCodeCN() {
		return languageCodeCN;
	}

	public void setLanguageCodeCN(String languageCodeCN) {
		this.languageCodeCN = languageCodeCN;
	}

	public List<CountryCode> getCountryCodes() {
		return countryCodes;
	}

	public void setCountryCodes(List<CountryCode> countryCodes) {
		this.countryCodes = countryCodes;
	}

	public List<StateProvince> getStateProvinces() {
		return stateProvinces;
	}

	public void setStateProvinces(List<StateProvince> stateProvinces) {
		this.stateProvinces = stateProvinces;
	}
}	
