package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.IOnlineAdminuserInfoService;
import com.cheriscon.backstage.system.service.ILanguageService;

import com.cheriscon.common.model.OnlineAdminuserInfo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * @remark	客诉信息action
 * @author pengdongan
 * @date   2013-06-05
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class OnlineRoleAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IAdminUserManager adminUserManager;
	
	@Resource
	private IOnlineAdminuserInfoService onlineAdminuserInfoService;

	@Resource
	private ILanguageService languageService;
	
	private OnlineAdminuserInfo onlineAdminuserInfo;
	
	private List<AdminUser> customerServiceUsers;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;

	private String[] acts;
	private String jsonData;

	
	private int result;

	@Action(value = "listonlinerole", results = { @Result(name = "listonlinerole", location = "/autel/backstage/member/online_role_list.jsp") })
	public String listComplaintInfoRole() throws Exception{
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			onlineAdminuserInfo = new OnlineAdminuserInfo();
			onlineAdminuserInfo.setLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			this.webpage = onlineAdminuserInfoService.pageOnlineAdminuserInfoPage(onlineAdminuserInfo,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			this.customerServiceUsers = adminUserManager.listByRoleId(2,1);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "listonlinerole";
	}

	/**
	 * 分配客服
	 * @return
	 * @throws Exception
	 */
	@Action(value = "allotacceptor", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String allotAcceptor() throws Exception {
		int result = -1;
		try {
//			onlineAdminuserInfoService.delOnlineAdminuserInfoByOnlineTypeCode(code);
			onlineAdminuserInfoService.delOnlineAdminuserInfoByOnlineTypeCode(code);
			for (String act : acts) {
				onlineAdminuserInfo = new OnlineAdminuserInfo();
				onlineAdminuserInfo.setOnlineTypeCode(code);
				onlineAdminuserInfo.setAdminUserid(act);
				onlineAdminuserInfoService.insertOnlineAdminuserInfo(onlineAdminuserInfo);
			}
			
			result = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.jsonData =  JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public List<AdminUser> getCustomerServiceUsers() {
		return customerServiceUsers;
	}

	public void setCustomerServiceUsers(List<AdminUser> customerServiceUsers) {
		this.customerServiceUsers = customerServiceUsers;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public OnlineAdminuserInfo getOnlineAdminuserInfo() {
		return onlineAdminuserInfo;
	}

	public void setOnlineAdminuserInfo(
			OnlineAdminuserInfo onlineAdminuserInfo) {
		this.onlineAdminuserInfo = onlineAdminuserInfo;
	}

	public String[] getActs() {
		return acts;
	}

	public void setActs(String[] acts) {
		this.acts = acts;
	}

}	
