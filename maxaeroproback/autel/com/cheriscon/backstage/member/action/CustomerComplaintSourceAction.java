package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ICustomerComplaintSourceService;

import com.cheriscon.common.model.CustomerComplaintSource;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	客诉来源action
 * @author pengdongan
 * @date   2013-01-07
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerComplaintSourceAction extends WWAction{
	
	private static final long serialVersionUID = -644859255038113322L;

	@Resource
	private ICustomerComplaintSourceService customerComplaintsourceService;
	
	private CustomerComplaintSource customerComplaintsource;
	
	private List<CustomerComplaintSource> customerComplaintSources;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private CustomerComplaintSource customerComplaintSourceSel;
	private CustomerComplaintSource customerComplaintsourceAdd;
	private CustomerComplaintSource customerComplaintsourceEdit;
	
	private String jsonData;
	
	
	//获取客诉来源列表
	public void list(){
		try{
			this.webpage = customerComplaintsourceService.pageCustomerComplaintSourcePage(customerComplaintSourceSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listcomplaintsource", results = { @Result(name = "listcomplaintsource", location = "/autel/backstage/member/complaint_source_list.jsp") })
	public String listCustomerComplaintSource() throws Exception{
		list();
		return "listcomplaintsource";
	}

	@Action(value = "addcomplaintsource", results = { @Result(name = "addcomplaintsource", location = "/autel/backstage/member/complaint_source_add.jsp") })
	public String addCustomerComplaintSource() throws Exception{
		return "addcomplaintsource";
	}

//	@Action(value = "insertcomplaintsource", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertcomplaintsource")
	public String insertCustomerComplaintSource() throws Exception{
		int result = -1;
		try{
			customerComplaintsource = customerComplaintsourceService.getCustomerComplaintSourceByName(customerComplaintsourceAdd.getName());
			if(customerComplaintsource == null)
			{
				customerComplaintsourceService.insertCustomerComplaintSource(customerComplaintsourceAdd);
				result = 0;
			}
			else {
				result = 2;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add("客诉来源添加成功");
		}
		else if(result == 2)
		{
			this.msgs.add("这个客诉来源已存在，添加失败");
		}
		else
		{
			this.msgs.add("客诉来源添加失败");
		}

		this.urls.put("客诉来源列表","listcomplaintsource.do");
		
		return MESSAGE;
	}

	@Action(value = "editcomplaintsource", results = { @Result(name = "editcomplaintsource", location = "/autel/backstage/member/complaint_source_edit.jsp") })
	public String editCustomerComplaintSource() throws Exception{
		try{
			this.customerComplaintsourceEdit = customerComplaintsourceService.getCustomerComplaintSourceByCode(code);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editcomplaintsource";
	}

//	@Action(value = "updatecomplaintsource", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updatecomplaintsource")
	public String updateCustomerComplaintSource() throws Exception{
		int result = -1;
		
		try{
			customerComplaintsource = customerComplaintsourceService.getCustomerComplaintSourceByCode(customerComplaintsourceEdit.getCode());

			if(customerComplaintsource != null)
			{
				customerComplaintsource.setCode(customerComplaintsourceEdit.getCode());
				customerComplaintsource.setName(customerComplaintsourceEdit.getName());
				customerComplaintsourceService.updateCustomerComplaintSource(customerComplaintsource);
				result = 0;
			}
			else
			{
				result = 1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add("客诉来源修改成功");
		}
		else
		{
			this.msgs.add("客诉来源修改失败");
		}

		this.urls.put("客诉来源列表","listcomplaintsource.do");
		
		return MESSAGE;
	}

//	@Action(value = "delcomplaintsource", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delcomplaintsource")
	public String delCustomerComplaintSource() throws Exception{
		int result = -1;
		try{
			customerComplaintsourceService.delCustomerComplaintSource(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add("客诉来源删除成功");
		}
		else if(result == 11)
		{
			this.msgs.add("可能已被使用，不允许删除");
		}
		else
		{
			this.msgs.add("客诉来源删除失败");
		}

		this.urls.put("客诉来源列表","listcomplaintsource.do");
		
		return MESSAGE;
	}

//	@Action(value = "delcomplaintsources", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delcomplaintsources")
	public String delCustomerComplaintSources() throws Exception{
		int result = -1;
		try{
			customerComplaintsourceService.delCustomerComplaintSources(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add("客诉来源删除成功");
		}
		else if(result == 11)
		{
			this.msgs.add("可能已被使用，不允许删除");
		}
		else
		{
			this.msgs.add("客诉来源删除失败");
		}

		this.urls.put("客诉来源列表","listcomplaintsource.do");
		
		return MESSAGE;
	}
	
	
	public CustomerComplaintSource getCustomerComplaintSource() {
		return customerComplaintsource;
	}

	public void setCustomerComplaintSource(CustomerComplaintSource customerComplaintsource) {
		this.customerComplaintsource = customerComplaintsource;
	}

	public List<CustomerComplaintSource> getCustomerComplaintSources() {
		return customerComplaintSources;
	}

	public void setCustomerComplaintSources(List<CustomerComplaintSource> customerComplaintSources) {
		this.customerComplaintSources = customerComplaintSources;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public CustomerComplaintSource getCustomerComplaintSourceSel() {
		return customerComplaintSourceSel;
	}

	public void setCustomerComplaintSourceSel(CustomerComplaintSource customerComplaintSourceSel) {
		this.customerComplaintSourceSel = customerComplaintSourceSel;
	}

	public CustomerComplaintSource getCustomerComplaintSourceAdd() {
		return customerComplaintsourceAdd;
	}

	public void setCustomerComplaintSourceAdd(CustomerComplaintSource customerComplaintsourceAdd) {
		this.customerComplaintsourceAdd = customerComplaintsourceAdd;
	}

	public CustomerComplaintSource getCustomerComplaintSourceEdit() {
		return customerComplaintsourceEdit;
	}

	public void setCustomerComplaintSourceEdit(CustomerComplaintSource customerComplaintsourceEdit) {
		this.customerComplaintsourceEdit = customerComplaintsourceEdit;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

}	
