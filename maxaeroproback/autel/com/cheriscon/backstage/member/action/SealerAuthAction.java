package com.cheriscon.backstage.member.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;



import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthDetail;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;

/**
 * @remark	测试包action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/core/admin")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class SealerAuthAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2812954469726046995L;
	

	@Resource
	private ISealerAuthService sealerAuthService;
	@Resource
	private ISealerInfoService sealerInfoService;
	
	private SealerAuthVO sealerAuth;
	
	private SealerAuthDetail detail;
	
	

	private List<SealerAuthVO> listSealerAuthVO;
	

	private String jsonData;
	
	private String sealerAuthDesc;
	
	@Action(value = "listSealerAuth", results = { @Result(name = "listSealerAuth", location = "/autel/backstage/member/sealer_auth_detail.jsp") })
	public String listSealerAuth() throws Exception{
		this.webpage = sealerAuthService.pageAuthVoDetail(sealerAuth, this.getPage(), this.getPageSize());
		return "listSealerAuth";
	}
	
	
	//获取权限列表
	public void list(){
		try{
			this.webpage = sealerAuthService.pageSealerAuthVoPage(sealerAuth,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "sealerAuth", results = { @Result(name = "sealerAuth", location = "/autel/backstage/member/sealer_auth_list.jsp") })
	public String sealerAuth() throws Exception{
		list();
		return "sealerAuth";
	}

	@Action(value = "editSealerAuth", results = { @Result(name = "editSealerAuth", location = "/autel/backstage/member/sealer_auth_edit.jsp") })
	public String editSealerAuth() throws Exception{
		sealerAuth=sealerAuthService.getSealerAuthById(sealerAuth.getCode());
		return "editSealerAuth";
	}
	
	@Action(value = "addSealerAuth", results = { @Result(name = "addSealerAuth", location = "/autel/backstage/member/sealer_auth_add.jsp") })
	public String addSealerAuth() throws Exception{
		return "addSealerAuth";
	}
	@Action(value = "addSealerAuthDetail", results = { @Result(name = "addSealerAuthDetail", location = "/autel/backstage/member/sealer_auth_detail_add.jsp") })
	public String addSealerAuthDetail() throws Exception{
		listSealerAuthVO=sealerAuthService.listSealerAuthVO();
		return "addSealerAuthDetail";
	}
	@SuppressWarnings("unchecked")
	@Action(value = "saveSealerAuthDetail")
	public String saveSealerAuthDetail() throws Exception{
		SealerInfo info =sealerInfoService.getSealerByAutelId(sealerAuth.getAutelId());
		if(info==null){
			this.msgs.add("经销商账号不存在，请重新输入");
			this.urls.put("返回", "javascript:history.go(-1);");
			return MESSAGE;
		}
		try{
			sealerAuthService.saveAuthDetail(info.getCode(), sealerAuthDesc);
			this.msgs.add("操作成功");
			this.urls.put("返回列表", "listSealerAuth.do");
		}catch(Exception e){
			this.msgs.add("操作失败");
			this.urls.put("返回列表", "javascript:history.go(-1);");
		}
		return MESSAGE;
	}
	
	
	@SuppressWarnings("unchecked")
	@Action(value = "delAuthDetailByCode")
	public String delAuthDetailByCode() throws Exception{
		try{
			sealerAuthService.delAuthDetailById(detail.getId());
			this.msgs.add("操作成功");
			this.urls.put("返回列表", "listSealerAuth.do");
		}catch(Exception e){
			this.msgs.add("操作失败");
			this.urls.put("返回列表", "listSealerAuth.do");
		}
		return MESSAGE;
	}
	
	
	@SuppressWarnings("unchecked")
	@Action(value = "saveSealerAuth")
	public String saveSealerAuth() throws Exception{
		try{
			SealerAuthVO temp=sealerAuthService.getSealerAuthById(sealerAuth.getCode());
			if(temp!=null){
				this.msgs.add("Code已经存在，请重新输入");
				this.urls.put(getText("common.js.returnlist"), "sealerAuth.do");
				return MESSAGE;
			}
			sealerAuthService.saveAuth(sealerAuth);
			this.msgs.add(getText("common.js.addSuccess"));
			this.urls.put(getText("common.js.returnlist"), "sealerAuth.do");
		}catch(Exception e){
			this.msgs.add("增加失败");
			this.urls.put(getText("common.js.returnlist"), "sealerAuth.do");
			return MESSAGE;
		}
		return MESSAGE;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "saveSealerAuthForUpdate")
	public String saveSealerAuthForUpdate() throws Exception{
		try{
			sealerAuthService.updateAuth(sealerAuth);
			this.msgs.add(getText("修改成功"));
			this.urls.put(getText("common.js.returnlist"), "sealerAuth.do");
		}catch(Exception e){
			this.msgs.add("修改失败");
			this.urls.put(getText("common.js.returnlist"), "sealerAuth.do");
		}
		return MESSAGE;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "delSealerAuth")
	public String delSealerAuth() throws Exception{
		try{
			sealerAuthService.deleteAuth(sealerAuth.getCode());
			this.msgs.add(getText("common.js.addSuccess"));
			this.urls.put(getText("common.js.returnlist"), "sealerAuth.do");
		}catch(Exception e){
			this.msgs.add("删除失败");
			this.urls.put(getText("common.js.returnlist"), "sealerAuth.do");
		}
		return MESSAGE;
	}
	

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	public SealerAuthVO getSealerAuth() {
		return sealerAuth;
	}

	public void setSealerAuth(SealerAuthVO sealerAuth) {
		this.sealerAuth = sealerAuth;
	}
	public List<SealerAuthVO> getListSealerAuthVO() {
		return listSealerAuthVO;
	}

	public void setListSealerAuthVO(List<SealerAuthVO> listSealerAuthVO) {
		this.listSealerAuthVO = listSealerAuthVO;
	}
	public String getSealerAuthDesc() {
		return sealerAuthDesc;
	}

	public void setSealerAuthDesc(String sealerAuthDesc) {
		this.sealerAuthDesc = sealerAuthDesc;
	}
	public SealerAuthDetail getDetail() {
		return detail;
	}

	public void setDetail(SealerAuthDetail detail) {
		this.detail = detail;
	}

}	
