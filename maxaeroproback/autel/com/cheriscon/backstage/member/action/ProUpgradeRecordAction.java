package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.IProUpgradeRecordService;
import com.cheriscon.backstage.member.vo.ProUpgradeRecordVo;

import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	客户产品升级信息action
 * @author pengdongan
 * @date   2013-01-07
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class ProUpgradeRecordAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IProUpgradeRecordService proUpgradeRecordService;
	
	private ProUpgradeRecordVo proUpgradeRecordVo;
	
	private List<ProUpgradeRecordVo> proUpgradeRecordVos;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private ProUpgradeRecordVo proUpgradeRecordVoSel;
	
	
	//获取客户产品升级信息列表
	public void list(){
		try{
			this.webpage = proUpgradeRecordService.pageProUpgradeRecordVoPage(proUpgradeRecordVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listproupgrade", results = { @Result(name = "listproupgrade", location = "/autel/backstage/member/proupgrade_record_list.jsp") })
	public String listProUpgrade() throws Exception{
		list();
		return "listproupgrade";
	}
	
	@Action(value = "clearproupgrade")
	public String clearproupgrade() throws Exception{
		int result = -1;
		try{
			proUpgradeRecordService.clearProUpgradeRecord();
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.customman.upgrademessage26"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.customman.upgrademessage27"));
		}
		else
		{
			this.msgs.add(getText("memberman.customman.upgrademessage28"));
		}

		this.urls.put(getText("memberman.customman.upgrademessage20"),"listproupgrade.do");
		
		return MESSAGE;
	}

	public ProUpgradeRecordVo getProUpgradeRecordVo() {
		return proUpgradeRecordVo;
	}

	public void setProUpgradeRecordVo(ProUpgradeRecordVo proUpgradeRecordVo) {
		this.proUpgradeRecordVo = proUpgradeRecordVo;
	}

	public List<ProUpgradeRecordVo> getProUpgradeRecordVos() {
		return proUpgradeRecordVos;
	}

	public void setProUpgradeRecordVos(List<ProUpgradeRecordVo> proUpgradeRecordVos) {
		this.proUpgradeRecordVos = proUpgradeRecordVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public ProUpgradeRecordVo getProUpgradeRecordVoSel() {
		return proUpgradeRecordVoSel;
	}

	public void setProUpgradeRecordVoSel(ProUpgradeRecordVo proUpgradeRecordVoSel) {
		this.proUpgradeRecordVoSel = proUpgradeRecordVoSel;
	}

}	
