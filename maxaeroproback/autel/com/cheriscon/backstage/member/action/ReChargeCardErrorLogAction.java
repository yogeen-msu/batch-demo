package com.cheriscon.backstage.member.action;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.IReChargeCardErrorLogService;
import com.cheriscon.common.model.ReChargeCardErrorLog;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;

/**
 * @remark	续租卡输入错误日志action
 * @author chenqichuan
 * @date   2013-09-28
 */
@ParentPackage("json_default")
@Namespace("/autel/errorlog")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class ReChargeCardErrorLogAction extends WWAction {


	private static final long serialVersionUID = -4633752303890697706L;

	@Resource
	private IReChargeCardErrorLogService chargeCardErrorLogService;
	@Resource
	private IAdminUserManager adminUserManager;
	
	//查询、修改、删除参数
	private String customerCode;
	private String jsonData;
	private ReChargeCardErrorLog errorlog;
	


	//查询错误日志列表
	@Action(value = "listCardErrorLog", results = { @Result(name = "listCardErrorLog", location = "/autel/backstage/member/recharge_error_list.jsp") })
	public String listCardErrorLog() throws Exception{
		this.webpage = chargeCardErrorLogService.pageReChargeCardError(errorlog,this.getPage(), this.getPageSize());
	    return "listCardErrorLog";
	}

	//处理错误信息
	@SuppressWarnings("unchecked")
	@Action(value = "activecustomer")
	public String updateErrorLog() throws Exception{
		try{
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			String updateTime=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss");
			chargeCardErrorLogService.updateErrorLog(user.getUsername(), updateTime, customerCode);
		this.msgs.add(getText("common.js.modSuccess"));
		}catch(Exception e){
			this.msgs.add(getText("common.js.modFail"));	
		}
		this.urls.put(getText("common.js.returnlist"),"listCardErrorLog.do");
		return MESSAGE;
	}
	
	
	
	public String getCustomerCode() {
		return customerCode;
	}
	public String getJsonData() {
		return jsonData;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	public ReChargeCardErrorLog getErrorlog() {
		return errorlog;
	}
	public void setErrorlog(ReChargeCardErrorLog errorlog) {
		this.errorlog = errorlog;
	}
}
