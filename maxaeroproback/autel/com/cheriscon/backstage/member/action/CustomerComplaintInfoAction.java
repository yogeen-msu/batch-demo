package com.cheriscon.backstage.member.action;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.AuthAction;
import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.member.service.IComplaintAdminuserInfoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.system.service.ILanguageService;

import com.cheriscon.common.model.ComplaintAdminuserInfo;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerComplaintType;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.utils.UploadUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;

/**
 * @remark	客诉信息action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerComplaintInfoAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IAdminUserManager adminUserManager;

	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;

	@Resource
	private ICustomerComplaintInfoVoService customerComplaintInfoVoService;

	@Resource
	private IReCustomerComplaintInfoService reCustomerComplaintInfoService;
	
	@Resource
	private IComplaintAdminuserInfoService complaintAdminuserInfoService;

	@Resource
	private ICustomerInfoService customerInfoService;

	@Resource
	private ISealerInfoService sealerInfoService;

	@Resource
	private ILanguageService languageService;
	
	@Resource
	private ISmtpManager smtpManager;

	@Resource
	private EmailProducer emailProducer;
	
	@Resource
	private IEmailTemplateService emailTemplateService; 
	
	@Resource
	private ICustomerComplaintTypeNameService customerComplaintTypeNameService;
	
	@Resource
	private ICustomerComplaintTypeService customerComplaintTypeService;
	
	
	
	private CustomerComplaintInfo customerComplaintInfo;
	
	private CustomerComplaintInfoVo customerComplaintInfoVo;
	
	private ReCustomerComplaintInfo reCustomerComplaintInfo;
	
	private ComplaintAdminuserInfo complaintAdminuserInfo;
	
	private List<ReCustomerComplaintInfo> reCustomerComplaintInfos;
	
	private List<CustomerComplaintInfoVo> customerComplaintInfoVos;
	
	private List<AdminUser> customerServiceUsers;
	
	private List<Map> lMaps;
	private List<Smtp> smtp;
	
	private String autelproUrl;
	
	public List<Smtp> getSmtp() {
		return smtp;
	}

	public void setSmtp(List<Smtp> smtp) {
		this.smtp = smtp;
	}

	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private CustomerComplaintInfoVo customerComplaintInfoVoSel;
	
	private CustomerComplaintType customerComplaintType;
	
	public CustomerComplaintType getCustomerComplaintType() {
		return customerComplaintType;
	}

	public void setCustomerComplaintType(CustomerComplaintType customerComplaintType) {
		this.customerComplaintType = customerComplaintType;
	}

	private ReCustomerComplaintInfo reCustomerComplaintInfoAdd;

	private String acceptor;
	private String jsonData;
	
	private File attachment;
	private String attachmentFileName;

	private AdminUser user;

	
	private int result;
	
	
	//获取客诉信息列表
	public void list(){
		try{
			user = adminUserManager.getCurrentUser();
			List<AuthAction> list=user.getAuthList();
			boolean flag=false;
			for(int i=0;i<list.size();i++){
				AuthAction a=list.get(i);
				if(a.getName().equals("管理员") || a.getName().equals("网站资料录入")){
					flag=true;
				}
			}
		//	user = adminUserManager.get(user.getUserid());
			if(customerComplaintInfoVoSel==null){
				customerComplaintInfoVoSel=new CustomerComplaintInfoVo();
				customerComplaintInfoVoSel.setComplaintState(new Integer(1));
			}
			if(flag)
			{
				this.webpage = customerComplaintInfoVoService.pageCustomerComplaintInfoVoPage(customerComplaintInfoVoSel,this.getPage(), this.getPageSize());
			}
			else
			{
				
				this.webpage = customerComplaintInfoVoService.pageCustomerComplaintInfoVoUser(user,customerComplaintInfoVoSel,this.getPage(), this.getPageSize());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			this.customerServiceUsers = adminUserManager.listByRoleId(2);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listcustomercomplaintstat", results = { @Result(name = "listcustomercomplaintstat", location = "/autel/backstage/member/complaint_stat_list.jsp") })
	public String listCustomerComplaintStat() throws Exception{

		try{
			this.webpage = customerComplaintInfoVoService.pageCustomerComplaintStatPage(customerComplaintInfoVoSel,this.getPage(), this.getPageSize());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			this.customerServiceUsers = adminUserManager.listByRoleId(2);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "listcustomercomplaintstat";
	}

	@Action(value = "listcustomercomplaint", results = { @Result(name = "listcustomercomplaint", location = "/autel/backstage/member/customer_complaint_list.jsp") })
	public String listCustomerComplaintInfo() throws Exception{
		list();
		return "listcustomercomplaint";
	}

	@Action(value = "listcomplaintrole", results = { @Result(name = "listcomplaintrole", location = "/autel/backstage/member/complaint_role_list.jsp") })
	public String listComplaintInfoRole() throws Exception{
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			complaintAdminuserInfo = new ComplaintAdminuserInfo();
			complaintAdminuserInfo.setLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			this.webpage = complaintAdminuserInfoService.pageComplaintAdminuserInfoPage(complaintAdminuserInfo,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			this.customerServiceUsers = adminUserManager.listByRoleId(2);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "listcomplaintrole";
	}

	@Action(value = "editcomplaintrole", results = { @Result(name = "editcomplaintrole", location = "/autel/backstage/member/complaint_role_edit.jsp") })
	public String editComplaintInfoRole() throws Exception{
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		this.customerComplaintType = this.customerComplaintTypeService.getComplaintTypeByLanguage(languageService.getByLocale(request.getLocale()).getCode(),code);
		
		complaintAdminuserInfo=complaintAdminuserInfoService.getComplaintAdminuserInfoBycomplaintTypeCode(code);
		
		this.customerServiceUsers = adminUserManager.listByRoleId(2);
		this.smtp=smtpManager.list();
		return "editcomplaintrole";
	}
	
	@Action(value = "savecomplaintrole")
	public String saveComplaintInfoRole() throws Exception{
		
		if(complaintAdminuserInfoService.getComplaintAdminuserInfoBycomplaintTypeCode(complaintAdminuserInfo.getComplaintTypeCode()) == null)
			complaintAdminuserInfoService.insertComplaintAdminuserInfo(complaintAdminuserInfo);
	    else
	    	complaintAdminuserInfoService.updateComplaintAdminuserInfo(complaintAdminuserInfo);
		
		this.msgs.add(getText("common.js.addSuccess"));
		this.urls.put(getText("common.js.returnlist"), "listcomplaintrole.do");
		return MESSAGE;
	}
	
	
//	/**
//	 * 分配客服
//	 * @return
//	 * @throws Exception
//	 */
//	@Action(value = "allotacceptor", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
//	public String allotAcceptor() throws Exception {
//		int result = -1;
//		try {
//			complaintAdminuserInfo = new ComplaintAdminuserInfo();
//			complaintAdminuserInfo.setComplaintTypeCode(code);
//			complaintAdminuserInfo.setAdminUserid(acceptor);
//			
//			if(complaintAdminuserInfoService.getComplaintAdminuserInfoBycomplaintTypeCode(code) == null)
//				complaintAdminuserInfoService.insertComplaintAdminuserInfo(complaintAdminuserInfo);
//			else
//				complaintAdminuserInfoService.updateComplaintAdminuserInfo(complaintAdminuserInfo);
//			
//			//complaintAdminuserInfoService.allotAcceptor(code,acceptor);
//			result = 0;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		this.jsonData =  JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
//	}

	/**
	 * 转发客诉
	 * @return
	 * @throws Exception
	 */
	@Action(value = "transcomplaint", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String transcomplaint() throws Exception {
		int result = -1;
		try {
			
			customerComplaintInfoService.allotAcceptor(code, acceptor);

			result = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.jsonData =  JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}

	/**
	 * 修改客诉有效性
	 * @return
	 * @throws Exception
	 */
	@Action(value = "modicomplainteffective", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String modiComplaintEffective() throws Exception {
		int result = -1;
		try {
			customerComplaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
			result = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.jsonData =  JSONArray.fromObject(result).toString();
		
		return SUCCESS;
	}

	/**
	 * 修改客诉状态
	 * @return
	 * @throws Exception
	 */
	@Action(value = "modicomplaintstate", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String modiComplaintState() throws Exception {
		int result = -1;
		try {
			customerComplaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
			
			if(customerComplaintInfo.getComplaintState().equals(new Integer(2))&& !reCustomerComplaintInfoAdd.getReContent().trim().equals("")){
			user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			reCustomerComplaintInfoAdd.setCustomerComplaintCode(customerComplaintInfo.getCode());
			reCustomerComplaintInfoAdd.setReDate(dateStr);
			reCustomerComplaintInfoAdd.setRePerson(user.getUsername());
			reCustomerComplaintInfoAdd.setRePersonType(2);
			reCustomerComplaintInfoService.addReCustomerComplaintInfo(reCustomerComplaintInfoAdd);
			}
			result = 0;
			
			if(result==0)
			{
				try {
					//发邮件////////////////////////////////////////////////////////
					Smtp smtp = smtpManager.getCurrentSmtp( );
					EmailModel emailModel = new EmailModel();
					emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
					emailModel.getData().put("password", smtp.getPassword());
					emailModel.getData().put("host", smtp.getHost());
					String reContent = reCustomerComplaintInfoAdd.getReContent();
					customerComplaintInfo = customerComplaintInfoService.getCustomerComplaintInfoByCode(customerComplaintInfo.getCode());
					code=customerComplaintInfo.getCode();
					String email = customerComplaintInfo.getEmail();
					String languageCode = "";
					if(customerComplaintInfo.getUserType() == FrontConstant.CUSTOMER_USER_TYPE)
					{
						languageCode = customerInfoService.getCustomerByCode(customerComplaintInfo.getUserCode()).getLanguageCode();
					}
					else if(customerComplaintInfo.getUserType() == FrontConstant.DISTRIBUTOR_USER_TYPE)
					{
						languageCode = sealerInfoService.getSealerByCode(customerComplaintInfo.getUserCode()).getLanguageCode();
					}
					String userType = customerComplaintInfo.getUserType().toString();

					String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
					String customerComplaintUrl =requestUrl+"/queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType;
					
					Language lanuage=languageService.getByCode(languageCode);
					String status=FrontConstant.getComplaintStatus(lanuage.getCountryCode(), customerComplaintInfo.getComplaintState());
					
					CustomerInfo info =customerInfoService.getCustomerByCode(customerComplaintInfo.getUserCode());
					String userName="";
					if(info!=null && info.getName()!=null){
					  userName=info.getName();
					}
					
					emailModel.getData().put("autelId", email);
					emailModel.getData().put("userName", userName);
					emailModel.getData().put("reContent", reContent);
					emailModel.getData().put("customerComplaintCode", code);
					emailModel.getData().put("subject", customerComplaintInfo.getComplaintTitle());
					emailModel.getData().put("status", status);
					emailModel.getData().put("rePerson", user.getUsername());
					emailModel.getData().put("url", customerComplaintUrl);
					emailModel.setTitle("[SUPPORT #"+code+"]: "+customerComplaintInfo.getComplaintTitle());
					
					emailModel.setTo(email);
					
					
					EmailTemplate template = emailTemplateService.
							getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REPLY_COMPLAINT_SUCCESS,languageCode);
					
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
					emailProducer.send(emailModel);
			
				} catch (Exception e) {
					this.result = 4;
					e.printStackTrace();
				}
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.jsonData =  JSONArray.fromObject(result).toString();
		return SUCCESS;
	}

	/**
	 * 查看客诉详情
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showcomplaint", results = { @Result(name = "showcomplaint", location = "/autel/backstage/member/customer_complaint_show.jsp") })
	public String showComplaint() throws Exception{
		try{
			this.customerComplaintInfo = customerComplaintInfoService.getCustomerComplaintInfoByCode(code);
			this.webpage = reCustomerComplaintInfoService.pageReCustomerComplaintInfoPage(code, this.getPage(), this.getPageSize());
			this.customerServiceUsers = adminUserManager.listByRoleId(2);
			autelproUrl = FreeMarkerPaser.getBundleValue("autelpro.url");
		}catch(Exception e){
			e.printStackTrace();
		}
		return "showcomplaint";
	}

	/**
	 * 添加客诉回复
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "addrecomplaint", results = { @Result(name = "recomplaint_result", location = "/autel/backstage/member/iframe_result.jsp") })
	public String addRecomplaint() throws Exception{
		this.result = 0;
		try{
			//**
			if (attachment != null) {
				try {
					if (FileUtil.isAllowUp(attachmentFileName)) {
						String path = UploadUtil.publish(this.attachment,this.attachmentFileName, "complaintinfo");
						path = path.replaceAll(CopSetting.FILE_STORE_PREFIX,"");
						reCustomerComplaintInfoAdd.setAttachment(path);
					}
					else {
						this.result = 1;
					}
					
				} catch (Exception e) {
					this.result = 2;
					e.printStackTrace();
				}
			}
			//*/
			
			if(result==0)
			{
				try {
					user = adminUserManager.getCurrentUser();
					user = adminUserManager.get(user.getUserid());
					
					String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
					
					CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();
					customerComplaintInfo.setCode(code);
					customerComplaintInfo.setComplaintState(3);
					customerComplaintInfo.setLastReDate(dateStr);
					customerComplaintInfo.setLastRePerson(user.getUsername());
					customerComplaintInfo.setTimeouts(0);
					
					customerComplaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
					
					reCustomerComplaintInfoAdd.setCustomerComplaintCode(code);
					reCustomerComplaintInfoAdd.setReDate(dateStr);
					reCustomerComplaintInfoAdd.setRePerson(user.getUsername());
					reCustomerComplaintInfoAdd.setRePersonType(2);
					
					reCustomerComplaintInfoService.addReCustomerComplaintInfo(reCustomerComplaintInfoAdd);
			
				} catch (Exception e) {
					this.result = 3;
					e.printStackTrace();
				}
			}
			

			
			if(result==0)
			{
				try {
					//发邮件////////////////////////////////////////////////////////
					
					Smtp smtp = smtpManager.getCurrentSmtp( );
					
					EmailModel emailModel = new EmailModel();
					emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
					emailModel.getData().put("password", smtp.getPassword());
					emailModel.getData().put("host", smtp.getHost());
					
//					HttpServletRequest request = this.getRequest();
//					
//					String requestUrl = request.getRequestURL().toString();
//					String contextPath = request.getContextPath();
//
//					
//					String activeUrl = requestUrl.split(contextPath)[0]+ contextPath;
					
					String reContent = reCustomerComplaintInfoAdd.getReContent();
					customerComplaintInfo = customerComplaintInfoService.getCustomerComplaintInfoByCode(code);
					String email = customerComplaintInfo.getEmail();
					String languageCode = "";
					if(customerComplaintInfo.getUserType() == FrontConstant.CUSTOMER_USER_TYPE)
					{
						languageCode = customerInfoService.getCustomerByCode(customerComplaintInfo.getUserCode()).getLanguageCode();
					}
					else if(customerComplaintInfo.getUserType() == FrontConstant.DISTRIBUTOR_USER_TYPE)
					{
						languageCode = sealerInfoService.getSealerByCode(customerComplaintInfo.getUserCode()).getLanguageCode();
					}
					String userType = customerComplaintInfo.getUserType().toString();
					
					String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
					String customerComplaintUrl = requestUrl+ "/queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType;
					
					Language lanuage=languageService.getByCode(languageCode);
					String status=FrontConstant.getComplaintStatus(lanuage.getCountryCode(), customerComplaintInfo.getComplaintState());
					
					CustomerInfo info =customerInfoService.getCustomerByCode(customerComplaintInfo.getUserCode());
					String userName="";
					if(info!=null && info.getName()!=null){
						userName=info.getName();
					}
					
					emailModel.getData().put("autelId", email);
					emailModel.getData().put("userName", userName);
					emailModel.getData().put("reContent", reContent);
					emailModel.getData().put("customerComplaintCode", code);
					emailModel.getData().put("subject", customerComplaintInfo.getComplaintTitle());
					emailModel.getData().put("status", status);
					emailModel.getData().put("rePerson", user.getUsername());
					emailModel.getData().put("url", customerComplaintUrl);
					emailModel.setTitle("[SUPPORT #"+code+"]: "+customerComplaintInfo.getComplaintTitle());
					
					emailModel.setTo(email);
					
					
					EmailTemplate template = emailTemplateService.
							getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REPLY_COMPLAINT_SUCCESS,languageCode);
					
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
					emailProducer.send(emailModel);
			
				} catch (Exception e) {
					this.result = 4;
					e.printStackTrace();
				}
			}
		}catch(Exception e){
			this.result = -1;
			e.printStackTrace();
		}
		
//		try{
//			this.customerComplaintInfo = customerComplaintInfoService.getCustomerComplaintInfoByCode(code);
//			this.webpage = reCustomerComplaintInfoService.pageReCustomerComplaintInfoPage(code, this.getPage(), this.getPageSize());
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		
		
		return "recomplaint_result";
	}
	
	public CustomerComplaintInfo getCustomerComplaintInfo() {
		return customerComplaintInfo;
	}

	public void setCustomerComplaintInfo(CustomerComplaintInfo customerComplaintInfo) {
		this.customerComplaintInfo = customerComplaintInfo;
	}

	public CustomerComplaintInfoVo getCustomerComplaintInfoVo() {
		return customerComplaintInfoVo;
	}

	public void setCustomerComplaintInfoVo(CustomerComplaintInfoVo customerComplaintInfoVo) {
		this.customerComplaintInfoVo = customerComplaintInfoVo;
	}

	public List<CustomerComplaintInfoVo> getCustomerComplaintInfoVos() {
		return customerComplaintInfoVos;
	}

	public void setCustomerComplaintInfoVos(List<CustomerComplaintInfoVo> customerComplaintInfoVos) {
		this.customerComplaintInfoVos = customerComplaintInfoVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public CustomerComplaintInfoVo getCustomerComplaintInfoVoSel() {
		return customerComplaintInfoVoSel;
	}

	public void setCustomerComplaintInfoVoSel(CustomerComplaintInfoVo customerComplaintInfoVoSel) {
		this.customerComplaintInfoVoSel = customerComplaintInfoVoSel;
	}

	public ReCustomerComplaintInfo getReCustomerComplaintInfo() {
		return reCustomerComplaintInfo;
	}

	public void setReCustomerComplaintInfo(
			ReCustomerComplaintInfo reCustomerComplaintInfo) {
		this.reCustomerComplaintInfo = reCustomerComplaintInfo;
	}

	public ReCustomerComplaintInfo getReCustomerComplaintInfoAdd() {
		return reCustomerComplaintInfoAdd;
	}

	public void setReCustomerComplaintInfoAdd(
			ReCustomerComplaintInfo reCustomerComplaintInfoAdd) {
		this.reCustomerComplaintInfoAdd = reCustomerComplaintInfoAdd;
	}

	public List<AdminUser> getCustomerServiceUsers() {
		return customerServiceUsers;
	}

	public void setCustomerServiceUsers(List<AdminUser> customerServiceUsers) {
		this.customerServiceUsers = customerServiceUsers;
	}

	public String getAcceptor() {
		return acceptor;
	}

	public void setAcceptor(String acceptor) {
		this.acceptor = acceptor;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public List<ReCustomerComplaintInfo> getReCustomerComplaintInfos() {
		return reCustomerComplaintInfos;
	}

	public void setReCustomerComplaintInfos(
			List<ReCustomerComplaintInfo> reCustomerComplaintInfos) {
		this.reCustomerComplaintInfos = reCustomerComplaintInfos;
	}

	public File getAttachment() {
		return attachment;
	}

	public void setAttachment(File attachment) {
		this.attachment = attachment;
	}

	public String getAttachmentFileName() {
		return attachmentFileName;
	}

	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public ComplaintAdminuserInfo getComplaintAdminuserInfo() {
		return complaintAdminuserInfo;
	}

	public void setComplaintAdminuserInfo(
			ComplaintAdminuserInfo complaintAdminuserInfo) {
		this.complaintAdminuserInfo = complaintAdminuserInfo;
	}

	public List<Map> getlMaps() {
		return lMaps;
	}

	public void setlMaps(List<Map> lMaps) {
		this.lMaps = lMaps;
	}

	public String getAutelproUrl() {
		return autelproUrl;
	}

	public void setAutelproUrl(String autelproUrl) {
		this.autelproUrl = autelproUrl;
	}

}	
