package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import oracle.sql.DATE;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.ICustomerInfoVoService;
import com.cheriscon.backstage.member.service.ICustomerLoginInfoService;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.service.ICustomerSoftwareService;
import com.cheriscon.backstage.member.service.IProUpgradeRecordService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.member.vo.CustomerInfoVo;
import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.backstage.member.vo.ProUpgradeRecordVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductRepairRecordService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductRepairRecord;
import com.cheriscon.common.utils.DesModule;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.cop.resource.model.AdminUser;

import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.DBRuntimeException;
import com.cheriscon.front.user.service.ICustomerInfoService;

/**
 * @remark 客户综合信息查询
 * @author chenqichuan
 * @date 2013-06-21
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings({ @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerAction extends WWAction {

	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private ICustomerLoginInfoService customerLoginInfoService;
	@Resource
	private ICustomerProductService customerProductService;
	@Resource
	private ICustomerComplaintInfoVoService customerComplaintInfoVoService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ICustomerInfoVoService customerInfoVoService;
	@Resource
	private IProUpgradeRecordService proUpgradeRecordService;
	@Resource
	private IProductRepairRecordService productRepairRecordService; 
	@Resource
	private ICustomerSoftwareService customerSoftwareService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private IAdminUserManager adminUserManager;

	
	private List<CustomerLoginInfoVo> customerLoginInfoVos;

	// 查询、修改、删除参数

	private CustomerLoginInfoVo customerLoginInfoVoSel;
	private CustomerProductVo customerProductVoSel;
	private CustomerInfo customerInfoEdit;
	private CustomerInfoVo customerInfoVo;
	private CustomerComplaintInfoVo customerComplaintInfoVoSel;
	private ProUpgradeRecordVo proUpgradeRecordVoSel;
	private CustomerSoftwareVo customerSoftwareVoSel;
	private CustomerLoginInfoVo customerLoginInfoVo;
	private Language language;
	private List<AdminUser> customerServiceUsers;
	
	public List<AdminUser> getCustomerServiceUsers() {
		return customerServiceUsers;
	}
	public void setCustomerServiceUsers(List<AdminUser> customerServiceUsers) {
		this.customerServiceUsers = customerServiceUsers;
	}
	public Language getLanguage() {
		return language;
	}
	public void setLanguage(Language language) {
		this.language = language;
	}
	public ICustomerProductService getCustomerProductService() {
		return customerProductService;
	}
	public void setCustomerProductService(
			ICustomerProductService customerProductService) {
		this.customerProductService = customerProductService;
	}

	public CustomerLoginInfoVo getCustomerLoginInfoVo() {
		return customerLoginInfoVo;
	}

	public void setCustomerLoginInfoVo(CustomerLoginInfoVo customerLoginInfoVo) {
		this.customerLoginInfoVo = customerLoginInfoVo;
	}

	public List<CustomerLoginInfoVo> getCustomerLoginInfoVos() {
		return customerLoginInfoVos;
	}

	public void setCustomerLoginInfoVos(
			List<CustomerLoginInfoVo> customerLoginInfoVos) {
		this.customerLoginInfoVos = customerLoginInfoVos;
	}

	public CustomerLoginInfoVo getCustomerLoginInfoVoSel() {
		return customerLoginInfoVoSel;
	}

	public void setCustomerLoginInfoVoSel(
			CustomerLoginInfoVo customerLoginInfoVoSel) {
		this.customerLoginInfoVoSel = customerLoginInfoVoSel;
	}

	public CustomerProductVo getCustomerProductVoSel() {
		return customerProductVoSel;
	}

	public void setCustomerProductVoSel(CustomerProductVo customerProductVoSel) {
		this.customerProductVoSel = customerProductVoSel;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public CustomerSoftwareVo getCustomerSoftwareVoSel() {
		return customerSoftwareVoSel;
	}

	public void setCustomerSoftwareVoSel(CustomerSoftwareVo customerSoftwareVoSel) {
		this.customerSoftwareVoSel = customerSoftwareVoSel;
	}

	public ProUpgradeRecordVo getProUpgradeRecordVoSel() {
		return proUpgradeRecordVoSel;
	}

	public ProductRepairRecord getProductRepairRecord() {
		return productRepairRecord;
	}

	public void setProUpgradeRecordVoSel(ProUpgradeRecordVo proUpgradeRecordVoSel) {
		this.proUpgradeRecordVoSel = proUpgradeRecordVoSel;
	}

	public void setProductRepairRecord(ProductRepairRecord productRepairRecord) {
		this.productRepairRecord = productRepairRecord;
	}

	private ProductRepairRecord productRepairRecord;
	
	public CustomerComplaintInfoVo getCustomerComplaintInfoVoSel() {
		return customerComplaintInfoVoSel;
	}

	public void setCustomerComplaintInfoVoSel(
			CustomerComplaintInfoVo customerComplaintInfoVoSel) {
		this.customerComplaintInfoVoSel = customerComplaintInfoVoSel;
	}

	public CustomerInfoVo getCustomerInfoVo() {
		return customerInfoVo;
	}

	public void setCustomerInfoVo(CustomerInfoVo customerInfoVo) {
		this.customerInfoVo = customerInfoVo;
	}

	public CustomerInfo getCustomerInfoEdit() {
		return customerInfoEdit;
	}

	public void setCustomerInfoEdit(CustomerInfo customerInfoEdit) {
		this.customerInfoEdit = customerInfoEdit;
	}

	private String jsonData;
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	// 获取客户产品信息
	public void listProductInfo() {
		try {
			this.webpage = customerProductService.pageCustomerProductVoPage(
					customerProductVoSel, this.getPage(), this.getPageSize());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 根据条件查询客户以及产品信息
	@Action(value = "listCustomerProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String listCustomerProduct() {

		try {
			this.webpage = customerProductService.pageCustomerProductVoPage(
					customerProductVoSel, this.getPage(), this.getPageSize());
		} catch (Exception e) {

			e.printStackTrace();
		}
		this.jsonData = JSONArray.fromObject(webpage).toString();
		return SUCCESS;
	}
	
	@Action(value = "chooseCustomer", results = { @Result(name = "chooseCustomer", location = "/autel/backstage/member/chooseCustomer.jsp") })
	public String chooseCustomer() throws Exception{
		this.webpage = customerProductService.pageCustomerProductVoPage(customerProductVoSel, this.getPage(), this.getPageSize());
		//this.jsonData = JSONArray.fromObject(webpage).toString();
		return "chooseCustomer";
	}

	// 进入页面
	@Action(value = "viewCustomerProInfo", results = { @Result(name = "viewCustomerProInfo", location = "/autel/backstage/member/customer_all_info.jsp") })
	public String viewCustomerProInfo() throws Exception {
		return "viewCustomerProInfo";
	}

	// 根据用户code查询客户信息
	@Action(value = "getCustomerInfo", results = { @Result(name = "getCustomerInfo", location = "/autel/backstage/member/customerinfo.jsp") })
	public String getCustomerInfo() throws Exception {
		customerInfoEdit = customerInfoService.getCustomerByCode(code);
		if(customerInfoEdit!=null && customerInfoEdit.getLanguageCode()!=null){
			language=languageService.getByCode(customerInfoEdit.getLanguageCode());
			customerInfoEdit.setLanguageCode(language.getName());
		}
		return "getCustomerInfo";
	}

	// 根据用户code查询产品信息
	@Action(value = "getProductInfo", results = { @Result(name = "getProductInfo", location = "/autel/backstage/member/productInfo.jsp") })
	public String getProductInfo() throws Exception {
		this.webpage = customerProductService.pageCustomerProductVoPage(
				customerProductVoSel, this.getPage(), this.getPageSize());
		return "getProductInfo";
	}

	@Action(value = "getCustomerLoginInfo", results = { @Result(name = "getCustomerLoginInfo", location = "/autel/backstage/member/loginInfo.jsp") })
	public String getCustomerLoginInfo() throws Exception {
		this.webpage = customerLoginInfoService.pageCustomerLoginInfoVoPage(
				customerLoginInfoVoSel, this.getPage(), this.getPageSize());
		return "getCustomerLoginInfo";
	}

	@Action(value = "getCustomerConsume", results = { @Result(name = "getCustomerConsume", location = "/autel/backstage/member/customerconsume.jsp") })
	public String getCustomerConsume() throws Exception {
		customerInfoVo.setConsumeType(-1);
		this.webpage = customerInfoVoService.queryCustomerOrderInfo(
				customerInfoVo, this.getPage(), this.getPageSize());
		return "getCustomerConsume";
	}
	
	@Action(value = "getCustomerComplaint", results = { @Result(name = "getCustomerComplaint", location = "/autel/backstage/member/customercomplaint.jsp") })
	public String getCustomerComplaint() throws Exception{
		this.webpage = customerComplaintInfoVoService.pageCustomerComplaintInfoVoPage(customerComplaintInfoVoSel,this.getPage(), this.getPageSize());
		this.customerServiceUsers = adminUserManager.listByRoleId(2);
		return "getCustomerComplaint";
	}
	
	@Action(value = "getProUpgrade", results = { @Result(name = "getProUpgrade", location = "/autel/backstage/member/productUpgrade.jsp") })
	public String getProUpgrade() throws Exception{
		this.webpage = proUpgradeRecordService.pageProUpgradeRecordVoPage(proUpgradeRecordVoSel,this.getPage(), this.getPageSize());
		return "getProUpgrade";
	}
	
	@Action(value = "getProSoftware", results = { @Result(name = "getProSoftware", location = "/autel/backstage/member/productSoftware.jsp") })
	public String getProSoftware() throws Exception{
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		customerSoftwareVoSel = customerSoftwareVoSel != null ? customerSoftwareVoSel : new CustomerSoftwareVo();
		customerSoftwareVoSel.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
		this.webpage = customerSoftwareService.pageCustomerSoftwareVoPage(customerSoftwareVoSel,this.getPage(), this.getPageSize());
		return "getProSoftware";
	}
	
	@Action(value = "getProService", results = { @Result(name = "getProService", location = "/autel/backstage/member/productService.jsp") })
	public String getProService() throws Exception{
		this.webpage = productRepairRecordService.pageProductRepairRecord(productRepairRecord,this.getPage(), this.getPageSize());
		return "getProService";
	}
	
	@Action(value = "ViewRepair", results = { @Result(name = "ViewRepair", location = "/autel/backstage/member/viewService.jsp") })	
	public String ViewRepair() throws Exception{
		productRepairRecord = productRepairRecordService.getById(productRepairRecord.getId());
		return "ViewRepair";
		
	}
	
	@Action(value = "updateCustomerPassword")	
	public void updateCustomerPassword() throws Exception{
	   List<CustomerInfo> list=customerInfoService.updatePassWordList();
	   if(list!=null && list.size()>0){
		   int j=0;
		   for(int i=0;i<list.size();i++){
		   CustomerInfo info=list.get(i);
		   info.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(info.getUserPwd(),null));
		   customerInfoService.updateCustomer(info);
		   System.out.println("更新的数据为:"+info.getCode());
		   j++;
		   }
	   }
		
	}
	
	
	@Action(value = "bank", results = { @Result(name = "bank", location = "/autel/backstage/member/bank.jsp") })	
	public String bank() throws Exception{
		return "bank";
		
	}
	
}
