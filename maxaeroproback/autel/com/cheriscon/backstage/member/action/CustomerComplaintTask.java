package com.cheriscon.backstage.member.action;

import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import javax.annotation.Resource;

import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.impl.CustomerComplaintInfoVoServiceImpl;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.sun.org.apache.bcel.internal.generic.NEW;

public class CustomerComplaintTask extends TimerTask {

	@Resource
	private ICustomerComplaintInfoVoService customerComplaintInfoVoService;
	
	private List<CustomerComplaintInfoVo> customerComplaintInfoVos;

  public void run() {
    System.out.println("ok " + (new Date()));
    // TODO 此处添加具体任务代码
    SendEmail48();
  }
  
  public void SendEmail48() {
	  try {
		  customerComplaintInfoVos = customerComplaintInfoVoService.getComplaintInfoVosTimeout(3, 48);
		  for (CustomerComplaintInfoVo customerComplaintInfoVo : customerComplaintInfoVos) {
			  System.out.println(customerComplaintInfoVo.getComplaintPerson());
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	  
  }

}