package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ICustomerLoginInfoService;
import com.cheriscon.backstage.member.vo.CustomerLoginInfoVo;

import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	客户登录信息action
 * @author pengdongan
 * @date   2013-01-07
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CustomerLoginInfoAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private ICustomerLoginInfoService customerLoginInfoService;
	
	private CustomerLoginInfoVo customerLoginInfoVo;
	
	private List<CustomerLoginInfoVo> customerLoginInfoVos;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private CustomerLoginInfoVo customerLoginInfoVoSel;
	
	
	//获取客户登录信息列表
	public void list(){
		try{
			this.webpage = customerLoginInfoService.pageCustomerLoginInfoVoPage(customerLoginInfoVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listcustomerlogininfo", results = { @Result(name = "listcustomerlogininfo", location = "/autel/backstage/member/customer_logininfo_list.jsp") })
	public String listCustomerLoginInfo() throws Exception{
		if(customerLoginInfoVoSel!=null){
			list();
		}
		return "listcustomerlogininfo";
	}
	
	@Action(value = "clearcustomerlogin")
	public String clearcustomerlogin() throws Exception{
		int result = -1;
		try{
			customerLoginInfoService.clearCustomerLoginInfo();
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.customman.cusloginmessage26"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.customman.cusloginmessage27"));
		}
		else
		{
			this.msgs.add(getText("memberman.customman.cusloginmessage28"));
		}

		this.urls.put(getText("memberman.customman.cusloginmessage20"),"listcustomerlogininfo.do");
		
		return MESSAGE;
	}

	public CustomerLoginInfoVo getCustomerLoginInfoVo() {
		return customerLoginInfoVo;
	}

	public void setCustomerLoginInfoVo(CustomerLoginInfoVo customerLoginInfoVo) {
		this.customerLoginInfoVo = customerLoginInfoVo;
	}

	public List<CustomerLoginInfoVo> getCustomerLoginInfoVos() {
		return customerLoginInfoVos;
	}

	public void setCustomerLoginInfoVos(List<CustomerLoginInfoVo> customerLoginInfoVos) {
		this.customerLoginInfoVos = customerLoginInfoVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public CustomerLoginInfoVo getCustomerLoginInfoVoSel() {
		return customerLoginInfoVoSel;
	}

	public void setCustomerLoginInfoVoSel(CustomerLoginInfoVo customerLoginInfoVoSel) {
		this.customerLoginInfoVoSel = customerLoginInfoVoSel;
	}

}	
