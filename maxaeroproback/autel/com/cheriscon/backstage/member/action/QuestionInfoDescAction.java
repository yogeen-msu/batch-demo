package com.cheriscon.backstage.member.action;



import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.ICustomerComplaintSourceService;
import com.cheriscon.backstage.member.service.IQuestionDescService;
import com.cheriscon.backstage.member.service.IQuestionInfoDescService;
import com.cheriscon.backstage.member.service.IQuestionInfoService;
import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.backstage.system.service.ILanguageService;

import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.CustomerComplaintSource;
import com.cheriscon.common.model.QuestionInfo;
import com.cheriscon.common.model.QuestionDesc;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	安全问题action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/autel/member")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class QuestionInfoDescAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6693265756340979658L;

	@Resource
	private IQuestionInfoDescService questionInfoDescService;
	@Resource
	private IQuestionDescService questionDescService;
	@Resource
	private ILanguageService languageService;
	
	private QuestionInfo questionInfo;
	
	private List<QuestionInfo> questionInfos;
	
	private List<QuestionDesc> questionDescs;
	
	private List<Language> languages;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private QuestionInfo questionInfoSel;
	private QuestionInfo questionInfoAdd;
	private QuestionInfo questionInfoEdit;
	
	/**
	 * 需删除的名称
	 */
	private List<QuestionDesc> delQuestionDescs;
	
	private String jsonData;
	
	
	//获取安全问题列表
	public void list(){
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			questionInfoSel = questionInfoSel != null ? questionInfoSel : new QuestionInfo();
			questionInfoSel.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			this.webpage = questionInfoDescService.pageQuestionInfoPage(questionInfoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listquestioninfo", results = { @Result(name = "listquestioninfo", location = "/autel/backstage/member/question_info_list.jsp") })
	public String listQuestionInfo() throws Exception{
		list();
		return "listquestioninfo";
	}

	@Action(value = "addquestioninfo", results = { @Result(name = "addquestioninfo", location = "/autel/backstage/member/question_info_add.jsp") })
	public String addQuestionInfo() throws Exception{
		return "addquestioninfo";
	}

//	@Action(value = "insertquestioninfo", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertquestioninfo")
	public String insertQuestionInfo() throws Exception{
		int result = -1;
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			questionInfoAdd.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			questionInfoDescService.insertQuestionInfo(questionInfoAdd);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.addSuccess"));
		}
		else
		{
			this.msgs.add(getText("common.js.addFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listquestioninfo.do");
		
		return MESSAGE;
	}

	@Action(value = "editquestioninfo", results = { @Result(name = "editquestioninfo", location = "/autel/backstage/member/question_info_edit.jsp") })
	public String editQuestionInfo() throws Exception{
		try{
			this.questionInfo = this.questionInfoDescService.getQuestionInfoByCode(code);
			this.questionDescs = this.questionDescService.queryByQuestionCode(code);
			this.languages = this.questionDescService.getAvailableLanguage(code);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editquestioninfo";
	}

//	@Action(value = "updatequestioninfo", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updatequestioninfo")
	public String updateQuestionInfo() throws Exception{
		int result = -1;
		
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			questionInfoEdit.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			questionInfoDescService.updateQuestionInfo(questionInfoEdit);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.modSuccess"));
		}
		else
		{
			this.msgs.add(getText("common.js.modFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listquestioninfo.do");
		
		return MESSAGE;
	}

	@Action(value = "updateQuestionDesc")
	public String updateQuestionDesc() throws Exception {
		int result = -1;
		try {
			questionDescService.updateQuestionDescs(questionDescs,
					delQuestionDescs);
			result = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.modSuccess"));
		}
		else
		{
			this.msgs.add(getText("common.js.modFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listquestioninfo.do");

		return MESSAGE;
	}

//	@Action(value = "delquestioninfo", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delquestioninfo")
	public String delQuestionInfo() throws Exception{
		int result = -1;
		try{
			questionInfoDescService.delQuestionInfoDetail(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.del"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("common.js.nonDel"));
		}
		else
		{
			this.msgs.add(getText("common.js.delFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listquestioninfo.do");
		
		return MESSAGE;
	}

//	@Action(value = "delquestioninfos", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delquestioninfos")
	public String delQuestionInfos() throws Exception{
		int result = -1;
		try{
			questionInfoDescService.delQuestionInfos(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("common.js.del"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("common.js.nonDel"));
		}
		else
		{
			this.msgs.add(getText("common.js.delFail"));
		}

		this.urls.put(getText("market.mainsale.message5"),"listquestioninfo.do");
		
		return MESSAGE;
	}
	
	
	public QuestionInfo getQuestionInfo() {
		return questionInfo;
	}

	public void setQuestionInfo(QuestionInfo questionInfo) {
		this.questionInfo = questionInfo;
	}

	public List<QuestionInfo> getQuestionInfos() {
		return questionInfos;
	}

	public void setQuestionInfos(List<QuestionInfo> questionInfos) {
		this.questionInfos = questionInfos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public QuestionInfo getQuestionInfoSel() {
		return questionInfoSel;
	}

	public void setQuestionInfoSel(QuestionInfo questionInfoSel) {
		this.questionInfoSel = questionInfoSel;
	}

	public QuestionInfo getQuestionInfoAdd() {
		return questionInfoAdd;
	}

	public void setQuestionInfoAdd(QuestionInfo questionInfoAdd) {
		this.questionInfoAdd = questionInfoAdd;
	}

	public QuestionInfo getQuestionInfoEdit() {
		return questionInfoEdit;
	}

	public void setQuestionInfoEdit(QuestionInfo questionInfoEdit) {
		this.questionInfoEdit = questionInfoEdit;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public List<QuestionDesc> getQuestionDescs() {
		return questionDescs;
	}

	public void setQuestionDescs(
			List<QuestionDesc> questionDescs) {
		this.questionDescs = questionDescs;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public List<QuestionDesc> getDelQuestionDescs() {
		return delQuestionDescs;
	}

	public void setDelQuestionDescs(
			List<QuestionDesc> delQuestionDescs) {
		this.delQuestionDescs = delQuestionDescs;
	}

}	
