package com.cheriscon.backstage.member.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.member.service.IQuestionInfoService;
import com.cheriscon.common.model.QuestionInfo;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/member/questionInfo_list.jsp"),
	@Result(name="add", location="/autel/backstage/member/questionInfo_add.jsp"),
	@Result(name="edit", location="/autel/backstage/member/questionInfo_edit.jsp")
})
public class QuestionInfoAction extends WWAction{
	
	private int id;
	private QuestionInfo questionInfo;
	
	private List<QuestionInfo> questionInfoList;
	
	@Resource
	private IQuestionInfoService questionInfoService; 
	
	// 显示列表
	public String list() {
		this.webpage = questionInfoService.pageQuestionInfo(this.getPage(), this.getPageSize());
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		
		questionInfoService.saveQuestionInfo(questionInfo);
		this.msgs.add("成功");
		this.urls.put("安全问题列表", "question-info!list.do");
		
		return MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		questionInfo = questionInfoService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		
		questionInfoService.updateQuestionInfo(questionInfo);
		this.msgs.add("成功");
		this.urls.put("安全问题列表", "question-info!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			questionInfoService.delQuestionInfoById(id);
			msgs.add("安全问题删除成功");
		} catch (RuntimeException e) {
			msgs.add("安全问题删除失败:" + e.getMessage());
		}

		this.urls.put("安全问题列表", "question-info!list.do");
		return MESSAGE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public QuestionInfo getQuestionInfo() {
		return questionInfo;
	}

	public void setQuestionInfo(QuestionInfo questionInfo) {
		this.questionInfo = questionInfo;
	}

	public List<QuestionInfo> getQuestionInfoList() {
		return questionInfoList;
	}

	public void setQuestionInfoList(List<QuestionInfo> questionInfoList) {
		this.questionInfoList = questionInfoList;
	}

}
