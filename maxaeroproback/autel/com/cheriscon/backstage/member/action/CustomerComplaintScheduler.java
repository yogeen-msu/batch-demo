package com.cheriscon.backstage.member.action;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.cheriscon.app.base.core.model.MultiSite;
import com.cheriscon.app.base.core.service.IMultiSiteManager;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

public class CustomerComplaintScheduler {

	@Resource
	private ICustomerComplaintInfoVoService customerComplaintInfoVoService;

	@Resource
	private ICustomerComplaintInfoService complaintInfoService;

	@Resource
	private ICustomerInfoService customerInfoService;

	@Resource
	private ISealerInfoService sealerInfoService;

	@Resource
	private ICustomerComplaintTypeNameService customerComplaintTypeNameService;
	
	@Resource
	private ISmtpManager smtpManager;
	
	@Resource
	private ILanguageService languageService;

	@Resource
	private EmailProducer emailProducer;
	
	@Resource
	private IEmailTemplateService emailTemplateService; 
	
	private List<CustomerComplaintInfoVo> customerComplaintInfoVos;

  public void customerComplaintTask48() {
	  System.out.println("====开始执行超时客诉脚本=="+DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss")); 
	  sendEmail(3,48);
  }
  
  
  public void customerComplaintTask72() {
	  System.out.println("====开始执行关闭客诉提醒邮件脚本=="+DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		sendEmail(3,72);
	  }
  
  public void customerComplaintTask96() {
	    System.out.println("====开始执行关闭客诉脚本=="+DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		closeComplanit(3,96);
	  }
  
  public void closeComplanit(int complaintState,int hours) {
	  try {
		  customerComplaintInfoVos = customerComplaintInfoVoService.getComplaintInfoVosTimeout(complaintState, hours);
		  for (CustomerComplaintInfoVo customerComplaintInfoVo : customerComplaintInfoVos) {
				try {
					//超时关闭客诉////////////////////////////////////////////////////////
					CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();
					customerComplaintInfo.setCode(customerComplaintInfoVo.getCode());
					customerComplaintInfo.setComplaintState(4);
					customerComplaintInfo.setTimeouts(hours);
					//关闭日期
					customerComplaintInfo.setCloseDate(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
					
					complaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
  }
  
	private void loadDefaultContext() {
		CopContext context = new CopContext();
		CopSite site = new CopSite();
		site.setUserid(1);
		site.setId(1);
		site.setThemeid(1);
		context.setCurrentSite(site);
		CopContext.setContext(context);

		ISiteManager siteManager = SpringContextHolder.getBean("siteManager");
		site = siteManager.get("localhost");
		context.setCurrentSite(site);
		CopContext.setContext(context);
	}
  
  public void sendEmail(int complaintState,int hours) {
	  try {
		  customerComplaintInfoVos = customerComplaintInfoVoService.getComplaintInfoVosTimeout(complaintState, hours);
		  for (CustomerComplaintInfoVo customerComplaintInfoVo : customerComplaintInfoVos) {
				try {
					System.out.println("====code==="+customerComplaintInfoVo.getCode());
					Integer time=customerComplaintInfoVo.getTimeouts()==null?0:customerComplaintInfoVo.getTimeouts();
					if(time<hours){
					loadDefaultContext();			//订时任务中没办法获取到request 对象 实体化默认的Context
					
					//超时设定////////////////////////////////////////////////////////
					CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();
					customerComplaintInfo.setCode(customerComplaintInfoVo.getCode());
					customerComplaintInfo.setTimeouts(hours);
					complaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
					
					EmailModel emailModel = new EmailModel();
					emailModel.setType(customerComplaintInfoVo.getAcceptor());
					String email = customerComplaintInfoVo.getEmail();
					String code = customerComplaintInfoVo.getCode();
					String userType = customerComplaintInfoVo.getUserType().toString();
					String customerComplaintUrl = "queryCustomerComplaintDetail.html?operationType=3&code="+code+"&userType="+userType;
					String languageCode = "";
					if(customerComplaintInfoVo.getUserType() == FrontConstant.CUSTOMER_USER_TYPE)
					{
						languageCode = customerInfoService.getCustomerByCode(customerComplaintInfoVo.getUserCode()).getLanguageCode();
					}
					else if(customerComplaintInfoVo.getUserType() == FrontConstant.DISTRIBUTOR_USER_TYPE)
					{
						languageCode = sealerInfoService.getSealerByCode(customerComplaintInfoVo.getUserCode()).getLanguageCode();
					}
					Language lanuage=languageService.getByCode(languageCode);
					String status=FrontConstant.getComplaintStatus(lanuage.getCountryCode(), customerComplaintInfoVo.getComplaintState());
					
					CustomerInfo info =customerInfoService.getCustomerByCode(customerComplaintInfoVo.getUserCode());
					String userName="";
					if(info!=null && info.getName()!=null){
					  userName=info.getName();
					}
					
					emailModel.getData().put("autelId", email);
					emailModel.getData().put("userName", userName);
					emailModel.getData().put("ticktId", code);
					emailModel.getData().put("subject", customerComplaintInfoVo.getComplaintTitle());
					emailModel.getData().put("status", status);
					emailModel.getData().put("url", customerComplaintUrl);
					
					emailModel.setTitle("[SUPPORT #"+code+"]: "+customerComplaintInfoVo.getComplaintTitle());
					emailModel.setTo(email);
					int emailTemplateType=0;
					if(hours==48) emailTemplateType=CTConsatnt.EMAIL_TEMPLATE_TYEP_COMPLAINT_TIMEOUT_ALERT;
					if(hours==72) emailTemplateType=CTConsatnt.EMAIL_TEMPLATE_TYEP_COMPLAINT_CLOSE_ALERT;
					EmailTemplate template = emailTemplateService.
							getUseTemplateByTypeAndLanguage(emailTemplateType,languageCode);
					
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
					if(FrontConstant.getPropertValue("autelproweb.email.url").equals("http://pro.auteltech.com")){
						emailProducer.send(emailModel);
					}
	
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
  }
}