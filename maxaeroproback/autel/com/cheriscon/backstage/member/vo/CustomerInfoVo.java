package com.cheriscon.backstage.member.vo;

import java.io.Serializable;

/**
 * @remark	客户信息业务对象
 * @author pengdongan
 * @date 2013-01-07
 *
 */
public class CustomerInfoVo implements Serializable{

	private static final long serialVersionUID = 5262012424617749020L;
	
	private Integer id;					//客户信息ID
	private String code;				//客户信息code
	private String autelId;				//AutelId
	private String firstName;			//第一个名字
	private String middleName;			//中间名字
	private String lastName;			//最后名字
	private String name;				//用户名
	private String secondEmail;			//secondEmail
	private String userPwd;				//用户密码
	private String mobilePhone;			//手机号
	private String regTime;				//注册时间
	private String languageCode;		//语言code
	private String address;				//地址
	private String country;				//国家
	private String company;				//公司
	private String zipCode;				//邮编
	private String questionCode;		//安全问题code
	private String answer;				//安全问题答案
	private String comUsername;			//论坛名字
	private Integer actState;			//激活状态
	private String actCode;				//激活码
	private Integer isAllowSendEmail;	//是否允许发邮件
	private String lastLoginTime;		//最后登录时间

	private String saleContractCode;	//销售契约
	private String sealerCode;			//经销商code
	private String sealerAutelId;		//经销商AutelId，登录用
	private String areaCfgCode;			//区域配置Code
	private String areaCfgName;			//区域配置名称
	
	
	///2012.01.16 yinhongbiao 增加
	private String orderCode;			//订单编号
	private int consumeType;			//消费类型
	private String userCode;			//用户编号
	private String sealerName;			//经销商名称
	private String serialNo;			//产品序列号
	private String minSaleUnitName;		// 最小销售单位名称
	private String orderMoney;			//消费价格
	private String orderDate;			//消费时间
	private String orderState;			//订单状态
	private String orderPayState;		//订单支付状态
	private Integer recConfirmState;		//收款确认状态（0：未确认 1：已确认）
	
	private String startTime;			//开始时间
	private String endTime;				//结束时间
	private String sealAutelId;				//AutelId
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAutelId() {
		return autelId;
	}
	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecondEmail() {
		return secondEmail;
	}
	public void setSecondEmail(String secondEmail) {
		this.secondEmail = secondEmail;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getQuestionCode() {
		return questionCode;
	}
	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getComUsername() {
		return comUsername;
	}
	public void setComUsername(String comUsername) {
		this.comUsername = comUsername;
	}
	public Integer getActState() {
		return actState;
	}
	public void setActState(Integer actState) {
		this.actState = actState;
	}
	public String getActCode() {
		return actCode;
	}
	public void setActCode(String actCode) {
		this.actCode = actCode;
	}
	public Integer getIsAllowSendEmail() {
		return isAllowSendEmail;
	}
	public void setIsAllowSendEmail(Integer isAllowSendEmail) {
		this.isAllowSendEmail = isAllowSendEmail;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getSaleContractCode() {
		return saleContractCode;
	}
	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}
	public String getSealerCode() {
		return sealerCode;
	}
	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}
	public String getSealerAutelId() {
		return sealerAutelId;
	}
	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}
	public String getAreaCfgCode() {
		return areaCfgCode;
	}
	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}
	public String getAreaCfgName() {
		return areaCfgName;
	}
	public void setAreaCfgName(String areaCfgName) {
		this.areaCfgName = areaCfgName;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public int getConsumeType() {
		return consumeType;
	}
	public void setConsumeType(int consumeType) {
		this.consumeType = consumeType;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getMinSaleUnitName() {
		return minSaleUnitName;
	}
	public void setMinSaleUnitName(String minSaleUnitName) {
		this.minSaleUnitName = minSaleUnitName;
	}
	public String getOrderMoney() {
		return orderMoney;
	}
	public void setOrderMoney(String orderMoney) {
		this.orderMoney = orderMoney;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderState() {
		return orderState;
	}
	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}
	public String getSealerName() {
		return sealerName;
	}
	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getSealAutelId() {
		return sealAutelId;
	}
	public void setSealAutelId(String sealAutelId) {
		this.sealAutelId = sealAutelId;
	}
	public String getOrderPayState() {
		return orderPayState;
	}
	public void setOrderPayState(String orderPayState) {
		this.orderPayState = orderPayState;
	}
	public Integer getRecConfirmState() {
		return recConfirmState;
	}
	public void setRecConfirmState(Integer recConfirmState) {
		this.recConfirmState = recConfirmState;
	}
	
}
