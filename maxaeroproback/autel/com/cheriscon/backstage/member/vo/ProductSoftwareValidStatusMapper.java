package com.cheriscon.backstage.member.vo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.CustomerProInfo;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;

/**
 * 软件有效期状态
 * @author yinhb
 */
public class ProductSoftwareValidStatusMapper implements RowMapper {

	@Override
	public ProductSoftwareValidStatus mapRow(ResultSet result, int arg1) throws SQLException {
		
		//最小销售单位说明信息
//		MinSaleUnitMemo minSaleUnitMemo = new MinSaleUnitMemo();	
//		minSaleUnitMemo.setName(result.getString("msum_name"));
		
		//产品信息
		ProductInfo productInfo = new ProductInfo();
		productInfo.setSerialNo(result.getString("pif_serialNo"));
		
		//销售契约
		SaleContract saleContract = new SaleContract();
		//saleContract.setSealerCode(result.getString("sct_sealerName"));
		saleContract.setName(result.getString("sct_sealName"));
		//区域配置
		AreaConfig areaConfig = new AreaConfig();
		areaConfig.setName(result.getString("acf_name"));
		
		//客户产品
//		CustomerProInfo customerProInfo = new CustomerProInfo();
//		customerProInfo.setCustomerCode(result.getString("cpif_customerCode"));
		
		ProductSoftwareValidStatus validStatus = new ProductSoftwareValidStatus();
	//	validStatus.setId(result.getInt("id"));
	//	validStatus.setCode(result.getString("code"));
	//	validStatus.setProCode(result.getString("proCode"));
		validStatus.setValidDate(result.getString("validDate"));
	//	validStatus.setMinSaleUnitCode("minSaleUnitCode");
		
		validStatus.setCusAutelid(result.getString("cus_autelid"));				//客户ID
		validStatus.setSealAutelid(result.getString("seal_autelid"));			//经销商ID
		
		
	//	validStatus.setMinSaleUnitMemo(minSaleUnitMemo);
		validStatus.setProductInfo(productInfo);
		validStatus.setSaleContract(saleContract);
		validStatus.setAreaConfig(areaConfig);
	//	validStatus.setCustomerProInfo(customerProInfo);
		
		return validStatus;
	}
}
