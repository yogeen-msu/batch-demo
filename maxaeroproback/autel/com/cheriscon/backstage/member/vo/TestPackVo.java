package com.cheriscon.backstage.member.vo;



/**
 * 
* @ClassName: TestPackVo
* @Description: 测试包信息业务对象
* @author pengdongan
* @date 2013-01-22
*
 */
public class TestPackVo implements java.io.Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3892492837259135950L;
	
	
	private Integer id;
	/**
	 * 软件类型Code
	 */
	private String softwareTypeCode;
	
	private String code;
	/**
	 * 版本名称
	 */
	private String name;
	/**
	 * 发布日期
	 */
	private String releaseDate;
	
	/**
	 * 基础包下载路径
	 */
	private String downloadPath;
	
	/**
	 * 产品code
	 */
	private String proCode;
	
	/**
	 * 产品序列号
	 */
	private String proSerialNo;
	
	/**
	 * 功能软件名称
	 */
	private String softwareName;
	
	/**
	 * 用户名称
	 */
	private String customerName;
	
	/**
	 * 产品型号名称
	 */
	private String proTypeName;
	
	/**
	 * 版本名称
	 * **/
	
	private String versionName;
	

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public TestPackVo() {
		super();
	}

	public TestPackVo(Integer id, String softwareTypeCode, String code,
			String name, String releaseDate, String downloadPath,
			String proCode, String proSerialNo, String softwareName,
			String customerName, String proTypeName) {
		super();
		this.id = id;
		this.softwareTypeCode = softwareTypeCode;
		this.code = code;
		this.name = name;
		this.releaseDate = releaseDate;
		this.downloadPath = downloadPath;
		this.proCode = proCode;
		this.proSerialNo = proSerialNo;
		this.softwareName = softwareName;
		this.customerName = customerName;
		this.proTypeName = proTypeName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}

	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getSoftwareName() {
		return softwareName;
	}

	public void setSoftwareName(String softwareName) {
		this.softwareName = softwareName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

}