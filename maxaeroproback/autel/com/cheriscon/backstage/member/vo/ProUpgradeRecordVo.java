package com.cheriscon.backstage.member.vo;

import java.io.Serializable;

/**
 * @remark	产品升级记录业务对象
 * @date 2013-1-16
 * @author pengdongan
 *
 */
public class ProUpgradeRecordVo implements Serializable{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8921097753792739719L;

	private Integer id;					//产品升级记录id
	
	private String code;				//产品升级记录code
	
	private String proCode;				//产品code
	
	private String softwareTypeCode;	//功能软件code
	
	private String versionCode;			//软件版本code
	
	private String upgradeTime;			//升级时间
	
	private String autelId;				//客户ID
	
	private String customerName;		//客户名
	
	private String proSerialNo;			//产品序列号
	
	private String softwareTypeName;	//功能软件名称
	
	private String beginDate;			//开始日期
	
	private String endDate;				//结束日期

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}

	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getUpgradeTime() {
		return upgradeTime;
	}

	public void setUpgradeTime(String upgradeTime) {
		this.upgradeTime = upgradeTime;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getSoftwareTypeName() {
		return softwareTypeName;
	}

	public void setSoftwareTypeName(String softwareTypeName) {
		this.softwareTypeName = softwareTypeName;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}	
	
}
