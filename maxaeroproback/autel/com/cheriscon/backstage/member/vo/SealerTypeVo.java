package com.cheriscon.backstage.member.vo;

import java.io.Serializable;

/**
 * @remark	经销商类型业务对象
 * @date 2013-1-5
 * @author pengdongan
 *
 */
public class SealerTypeVo implements Serializable{
	
	private static final long serialVersionUID = -741636032511982563L;

	
	//经销商类型id
	private Integer id;
	//经销商类型code
	private String code;
	//经销商类型名称
	private String name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
