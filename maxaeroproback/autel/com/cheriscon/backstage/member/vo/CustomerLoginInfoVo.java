package com.cheriscon.backstage.member.vo;

import java.io.Serializable;

/**
 * @remark	客户登录信息业务对象
 * @date 2013-1-16
 * @author pengdongan
 *
 */
public class CustomerLoginInfoVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8725839664751524397L;
	
	
	private String customerAutelId;		//客户ID
	
	private String customerName;		//客户名
	
	private String proSerialNo;			//产品序列号
	
	private String loginIP;				//登录IP
	
	private String loginTime;			//登录时间
	
	private String beginDate;			//开始日期
	
	private String endDate;				//结束日期
	
	private String code;                //用户code

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCustomerAutelId() {
		return customerAutelId;
	}

	public void setCustomerAutelId(String customerAutelId) {
		this.customerAutelId = customerAutelId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getLoginIP() {
		return loginIP;
	}

	public void setLoginIP(String loginIP) {
		this.loginIP = loginIP;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}	
	
}
