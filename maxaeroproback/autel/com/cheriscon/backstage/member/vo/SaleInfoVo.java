package com.cheriscon.backstage.member.vo;



/**
 * @remark 经销商销售信息业务对象
 * @author pengdongan
 * @date 2013-01-17
 * 
 */

public class SaleInfoVo  implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2892656672462499649L;
	
	private Integer id;				//ID
	private String code;			//销售信息code
	private String proName;			//产品名称（和产品型号不同）
	private String proExpand;		//产品规格
	private String company;			//单位
	private Integer salesNum;		//销货数量
	private Long salesMoney;		//销货金额
	private Integer pinBackNum;		//销退数量
	private Long pinBackMoney;		//销退金额
	private Integer netWeight;		//销货净量
	private Long netSales;			//销货净额
	private String sealerName;		//经销商名称（就是经销商ID、登录帐号）
	
	public SaleInfoVo() {
		super();
	}

	public SaleInfoVo(Integer id, String code, String proName, String sealerName, String proExpand,
			String company, Integer salesNum, Long salesMoney, Integer pinBackNum, Long pinBackMoney,
			Integer netWeight, Long netSales) {
		this.id = id;
		this.code = code;
		this.proName = proName;
		this.sealerName = sealerName;
		this.proExpand = proExpand;
		this.company = company;
		this.salesNum = salesNum;
		this.salesMoney = salesMoney;
		this.pinBackNum = pinBackNum;
		this.pinBackMoney = pinBackMoney;
		this.netWeight = netWeight;
		this.netSales = netSales;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public String getProExpand() {
		return proExpand;
	}

	public void setProExpand(String proExpand) {
		this.proExpand = proExpand;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Integer getSalesNum() {
		return salesNum;
	}

	public void setSalesNum(Integer salesNum) {
		this.salesNum = salesNum;
	}

	public Long getSalesMoney() {
		return salesMoney;
	}

	public void setSalesMoney(Long salesMoney) {
		this.salesMoney = salesMoney;
	}

	public Integer getPinBackNum() {
		return pinBackNum;
	}

	public void setPinBackNum(Integer pinBackNum) {
		this.pinBackNum = pinBackNum;
	}

	public Long getPinBackMoney() {
		return pinBackMoney;
	}

	public void setPinBackMoney(Long pinBackMoney) {
		this.pinBackMoney = pinBackMoney;
	}

	public Integer getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Integer netWeight) {
		this.netWeight = netWeight;
	}

	public Long getNetSales() {
		return netSales;
	}

	public void setNetSales(Long netSales) {
		this.netSales = netSales;
	}

	public String getSealerName() {
		return sealerName;
	}

	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}

}