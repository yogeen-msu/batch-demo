package com.cheriscon.backstage.member.vo;

import java.io.Serializable;

/**
 * sdk app object
 * 
 * @author autel
 * 
 */
@SuppressWarnings("serial")
public class SdkAppDownloadVO implements Serializable {

	private String id;// 主键
	private String sdkCategory;// sdk category
	private String sdkVersion;// 版本;
	private String sdkDownloadUrl;// 下载路径;
	private String categoryType;// 类别
	private String releaseNotes;// release notes;
	private String releaseNotesVersion;// 版本;
	private String releaseNotesDownloadUrl;// 下载路径;
	private String status;// 状态
	private String createDate;// 创建时间
	private String lastUpdateDate;// 最后修改时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSdkCategory() {
		return sdkCategory;
	}

	public void setSdkCategory(String sdkCategory) {
		this.sdkCategory = sdkCategory;
	}

	public String getSdkVersion() {
		return sdkVersion;
	}

	public void setSdkVersion(String sdkVersion) {
		this.sdkVersion = sdkVersion;
	}

	public String getSdkDownloadUrl() {
		return sdkDownloadUrl;
	}

	public void setSdkDownloadUrl(String sdkDownloadUrl) {
		this.sdkDownloadUrl = sdkDownloadUrl;
	}

	public String getReleaseNotes() {
		return releaseNotes;
	}

	public void setReleaseNotes(String releaseNotes) {
		this.releaseNotes = releaseNotes;
	}

	public String getReleaseNotesVersion() {
		return releaseNotesVersion;
	}

	public void setReleaseNotesVersion(String releaseNotesVersion) {
		this.releaseNotesVersion = releaseNotesVersion;
	}

	public String getReleaseNotesDownloadUrl() {
		return releaseNotesDownloadUrl;
	}

	public void setReleaseNotesDownloadUrl(String releaseNotesDownloadUrl) {
		this.releaseNotesDownloadUrl = releaseNotesDownloadUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

}
