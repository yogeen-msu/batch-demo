package com.cheriscon.backstage.member.vo;

import java.io.Serializable;

/**
 * @remark	客户软件信息业务对象
 * @date 2013-1-16
 * @author pengdongan
 *
 */
public class CustomerSoftwareVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8511564128583437480L;
	
	private String customerAutelId;		//客户ID
	
	private String customerName;		//客户名
	
	private String minSaleUnitName;		//最小销售单位名称
	
	private String proSerialNo;			//产品序列号
	
	private String areaCfgName;			//区域配置名称
	
	private String sealerAutelId;		//经销商ID
	
	private String sealerName;			//经销商名称
	
	private String proSoftValidDate;	//产品软件到期时间
	
	private String adminLanguageCode;	//管理员首选语言code
	
	private String proCode; //产品code
	
	private String softLanguage; //产品出厂配置语言
	
	private String saleContract; //销售配置名称
	
	private String proRegisterDate; //产品注册日期

	public String getProRegisterDate() {
		return proRegisterDate;
	}

	public void setProRegisterDate(String proRegisterDate) {
		this.proRegisterDate = proRegisterDate;
	}

	public String getSaleContract() {
		return saleContract;
	}

	public void setSaleContract(String saleContract) {
		this.saleContract = saleContract;
	}

	public String getSoftLanguage() {
		return softLanguage;
	}

	public void setSoftLanguage(String softLanguage) {
		this.softLanguage = softLanguage;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getCustomerAutelId() {
		return customerAutelId;
	}

	public void setCustomerAutelId(String customerAutelId) {
		this.customerAutelId = customerAutelId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMinSaleUnitName() {
		return minSaleUnitName;
	}

	public void setMinSaleUnitName(String minSaleUnitName) {
		this.minSaleUnitName = minSaleUnitName;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getAreaCfgName() {
		return areaCfgName;
	}

	public void setAreaCfgName(String areaCfgName) {
		this.areaCfgName = areaCfgName;
	}

	public String getSealerAutelId() {
		return sealerAutelId;
	}

	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}

	public String getSealerName() {
		return sealerName;
	}

	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}

	public String getProSoftValidDate() {
		return proSoftValidDate;
	}

	public void setProSoftValidDate(String proSoftValidDate) {
		this.proSoftValidDate = proSoftValidDate;
	}

	public String getAdminLanguageCode() {
		return adminLanguageCode;
	}

	public void setAdminLanguageCode(String adminLanguageCode) {
		this.adminLanguageCode = adminLanguageCode;
	}
	
}
