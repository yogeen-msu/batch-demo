package com.cheriscon.backstage.system.model;

public class AutelCheckVo {

	private String productName; //产品名称
	private int productNum;  //产品数量
	/*
	 * 用于统计订单
	 */
	private int ordersNum;
	private Double ordersMoney;
	/*
	 * 用于统计升级卡
	 */
	private int isUseTotalNum;  //未使用的升级卡总和
 	private int usedTotalNum;   //已经使用的升级卡总和
 	private int totalNum;
 	private String queryType; //查询类型
 	private String proType ;//产品类型
 	private String proRegStatus; //产品注册状态
 	
 	/*
 	 * 用于统计经销商产品升级情况
 	 * */
 	private String sealerName; //经销商名称
 	private String sealerCode;//经销商编码
 	
 	
	public String getSealerCode() {
		return sealerCode;
	}
	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}
	public String getSealerName() {
		return sealerName;
	}
	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}
	public String getProRegStatus() {
		return proRegStatus;
	}
	public void setProRegStatus(String proRegStatus) {
		this.proRegStatus = proRegStatus;
	}
	public String getProType() {
		return proType;
	}
	public void setProType(String proType) {
		this.proType = proType;
	}
	public String getQueryType() {
		return queryType;
	}
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	public String getProductName() {
		return productName;
	}
	public int getProductNum() {
		return productNum;
	}
	public int getOrdersNum() {
		return ordersNum;
	}
	public Double getOrdersMoney() {
		return ordersMoney;
	}
	public int getIsUseTotalNum() {
		return isUseTotalNum;
	}
	public int getUsedTotalNum() {
		return usedTotalNum;
	}
	public int getTotalNum() {
		return totalNum;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public void setProductNum(int productNum) {
		this.productNum = productNum;
	}
	public void setOrdersNum(int ordersNum) {
		this.ordersNum = ordersNum;
	}
	public void setOrdersMoney(Double ordersMoney) {
		this.ordersMoney = ordersMoney;
	}
	public void setIsUseTotalNum(int isUseTotalNum) {
		this.isUseTotalNum = isUseTotalNum;
	}
	public void setUsedTotalNum(int usedTotalNum) {
		this.usedTotalNum = usedTotalNum;
	}
	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}
	


}
