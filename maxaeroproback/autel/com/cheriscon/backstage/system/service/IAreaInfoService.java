package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.AreaInfo;
import com.cheriscon.framework.database.Page;

public interface IAreaInfoService {
	
	/**
	 * 增加
	 * @param areaInfo
	 */
	public boolean saveAreaInfo(AreaInfo areaInfo);
	
	/**
	 * 修改
	 * @param areaInfo
	 */
	public boolean updateAreaInfo(AreaInfo areaInfo);
	
	/**
	 * 删除
	 */
	public boolean delAreaInfoById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public AreaInfo getById(int id);
	
	/**
	 * 查询出所有区域
	 * @return
	 */
	public List<AreaInfo> queryAreaInfo();
	
	/**
	 * 查询没有分配的区域
	 * @return
	 */
	public List<AreaInfo> queryNotSelectAreaInfo();
	
	/**
	 * 翻页查询所有区域
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageAreaInfo(int pageNo, int pageSize);
	
	/**
	 * 判断新区域是否存在
	 * @param areaInfo
	 * @return
	 */
	public List<AreaInfo> getByNewAreaInfo(AreaInfo areaInfo);

}
