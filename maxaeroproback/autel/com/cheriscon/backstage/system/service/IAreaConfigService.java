package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.framework.database.Page;

public interface IAreaConfigService {
	
	/**
	 * 增加
	 * @param areaConfig
	 */
	public boolean saveAreaConfig(AreaConfig areaConfig);
	
	/**
	 * 修改
	 * @param areaConfig
	 */
	public boolean updateAreaConfig(AreaConfig areaConfig);
	
	/**
	 * 删除
	 */
	public boolean delAreaConfigById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public AreaConfig getById(int id);
	
	/**
	 * 翻页查询所有区域配置
	 * @param areaConfig
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageAreaConfig(AreaConfig areaConfig, int pageNo, int pageSize);
	
	/**
	 * 
	* @Title: listAll
	* @Description: 查询所有区域配置信息
	* @author shaohu
	* @param    
	* @return List<AreaConfig>    
	* @throws
	 */
	public List<AreaConfig> listAll() throws Exception;

	/**
	 * 判断新区域配置是否存在
	 * @param areaInfo
	 * @return
	 */
	public List<AreaConfig> getByNewAreaConfig(AreaConfig areaConfig);
}
