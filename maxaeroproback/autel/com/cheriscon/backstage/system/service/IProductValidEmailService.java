package com.cheriscon.backstage.system.service;

import com.cheriscon.common.model.ProductValidEmail;

public interface IProductValidEmailService {

	public void saveProductValidEmail(ProductValidEmail productValidEmail) throws Exception;
    
}
