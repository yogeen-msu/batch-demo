package com.cheriscon.backstage.system.service;

import com.cheriscon.common.model.SaleContractAuth;
import com.cheriscon.framework.database.Page;

public interface ISaleContractAuthService {

	public void addSaleContractAuth(SaleContractAuth auth) throws Exception;
	
	public Page getSaleContractAuthPage(SaleContractAuth auth, int pageNo,int pageSize) throws Exception ;
	
	public void delSaleContractAuthById(Integer id) throws Exception;
	
	public int getSaleContractAuth(SaleContractAuth auth) throws Exception;
	
}
