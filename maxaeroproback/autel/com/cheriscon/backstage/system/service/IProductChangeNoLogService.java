package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.ProductChangeNoLog;
import com.cheriscon.framework.database.Page;



public interface IProductChangeNoLogService {

	public void saveLog(ProductChangeNoLog log) throws Exception;
	
	public Page getProductContractChangeNoLogs(ProductChangeNoLog productChangeNoLog, int pageNo,int pageSize) throws Exception ;
	public List<ProductChangeNoLog> queryProChangeNologs(ProductChangeNoLog productChangeNoLog) throws Exception ;
}
