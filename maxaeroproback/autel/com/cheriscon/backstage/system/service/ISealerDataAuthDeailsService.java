package com.cheriscon.backstage.system.service;


import java.util.List;

import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.framework.database.Page;


public interface ISealerDataAuthDeailsService {

	public SealerDataAuthDeails getDataAuth(String authCode) throws Exception;
	
	public void insertAuth(SealerDataAuthDeails auth);
	
	public Page  getAuthSealer(String sealerName,String authCode,String sealerCode,int pageNo, int pageSize) throws Exception ;
	
	public List<SealerDataAuthDeails>  getAuthSealerList(String authCode,String sealerCode) throws Exception ;
	
	public SealerDataAuthDeails getDataAuthBySealerCode(String sealerCode) throws Exception;
}
