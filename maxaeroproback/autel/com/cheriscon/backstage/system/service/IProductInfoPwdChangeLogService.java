package com.cheriscon.backstage.system.service;

import com.cheriscon.framework.database.Page;

public interface IProductInfoPwdChangeLogService {

	public Page getProductInfoPwdChangeLogs(String procode,int pageNo,
			int pageSize)
			throws Exception;

}
