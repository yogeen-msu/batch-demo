package com.cheriscon.backstage.system.service;

import com.cheriscon.app.base.core.model.Role;
import com.cheriscon.app.base.core.service.auth.IRoleManager;
import com.cheriscon.framework.database.Page;


/**
 * @remark 角色信息接口
 * @author pengdongan
 * @date 2013-01-30
 * 
 */

public interface IRoleAutelService extends IRoleManager {
	
	/**
	 * 分页查询所有角色
	 * @param adminUser
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageRolepage(Role role, int pageNo, int pageSize);

}
