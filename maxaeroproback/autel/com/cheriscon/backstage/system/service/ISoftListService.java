package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.SoftList;
import com.cheriscon.front.usercenter.distributor.vo.SoftListVo;

public interface ISoftListService {

	public List<SoftList> getSoftListByProductSn(String productSn) throws Exception;
	
	public List<SoftList> getSoftList(SoftList record,String languageCode,String queryType);
	
	public List<SoftList> getSoftList(SoftListVo softListVo);
	
}
