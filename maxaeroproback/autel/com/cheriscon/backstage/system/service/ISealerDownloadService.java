package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.SealerDownload;
import com.cheriscon.framework.database.Page;

public interface ISealerDownloadService {
	/**
	 * 分页查询经销商资料
	 * @param sealerDownload
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageSealerDownload(SealerDownload sealerDownload, int pageNo, int pageSize);
	/**
	 * 查询经销商资料文件夹
	 * @param fileName
	 * @return
	 */
	public List<SealerDownload> getSealerDownloadFiles();
	/**
	 * 查询全部经销商资料
	 * @return
	 */
	public List<SealerDownload> getSealerDownloadList(String fileType);
	/**
	 * 获取经销商资料
	 * @param id
	 * @return
	 */
	public SealerDownload getSealerDownload(int id);
	/**
	 * 保存经销商资料
	 * @param sealerDownload
	 */
	public void saveSealerDownload(SealerDownload sealerDownload) throws Exception;
	/**
	 * 修改经销商资料
	 * @param sealerDownload
	 */
	public void updateSealerDownload(SealerDownload sealerDownload) throws Exception;
	/**
	 * 删除经销商资料
	 * @param id
	 */
	public void deleteSealerDownload(int id) throws Exception;
}
