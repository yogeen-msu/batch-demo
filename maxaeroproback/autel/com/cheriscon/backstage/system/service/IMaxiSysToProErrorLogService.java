package com.cheriscon.backstage.system.service;

import com.cheriscon.common.model.MaxiSysToProErrorLog;

public interface IMaxiSysToProErrorLogService {
	public void insertLog(MaxiSysToProErrorLog log);
}
