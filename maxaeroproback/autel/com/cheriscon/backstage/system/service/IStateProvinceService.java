package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.StateProvince;
import com.cheriscon.framework.database.Page;

public interface IStateProvinceService {
	/**
	 * 分页查询国家编码
	 * @param StateProvince
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageStateProvince(StateProvince state, int pageNo, int pageSize);
	/**
	 * 获取全部国家编码
	 * @param state
	 * @return
	 */
	public List<StateProvince> getStateProvinceList();
	/**
	 * 获取国家编码
	 * @param id
	 * @return
	 */
	public StateProvince getStateProvince(int id);
	/**
	 * 获取国家编码
	 * @param id
	 * @return
	 */
	public List<StateProvince> getStateProvince(String state);
	/**
	 * 新增保存国家编码
	 * @param StateProvince
	 * @return
	 */
	public boolean saveStateProvince(StateProvince state);
	/**
	 * 修改国家编码
	 * @param StateProvince
	 * @return
	 */
	public boolean updateStateProvince(StateProvince state);
	/**
	 * 删除国家编码
	 * @param id
	 * @return
	 */
	public boolean deleteStateProvince(int id);
}
