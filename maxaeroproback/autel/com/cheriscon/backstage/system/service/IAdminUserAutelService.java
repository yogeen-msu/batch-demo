package com.cheriscon.backstage.system.service;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.database.Page;


/**
 * @remark 管理员信息接口
 * @author pengdongan
 * @date 2013-01-30
 * 
 */

public interface IAdminUserAutelService extends IAdminUserManager {
	
	/**
	 * 分页查询所有管理员
	 * @param adminUser
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageAdminUserpage(AdminUser adminUser, int pageNo, int pageSize);

}
