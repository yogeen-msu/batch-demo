package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.DataLogging;
import com.cheriscon.common.model.DataLoggingContent;
import com.cheriscon.common.model.UpgradeRecord;

public interface IDataLogService {

	public List<DataLogging> getDataLogList(String productSN) throws Exception;
	
	public DataLogging getDataLogDetails(String id) throws Exception;
	
	public List<DataLoggingContent> getDataLogContent(String datalogId) throws Exception;
}
