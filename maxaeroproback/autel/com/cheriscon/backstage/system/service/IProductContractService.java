package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.UpWarningInfo;


public interface IProductContractService {
	
	/***
	 * 
	 * 查询当前升级区域在美国地区，未处理的预警信息
	 * @return List<UpWarningInfo>
	 */
	public List<UpWarningInfo> queryList();
	
	//更新预警信息状态
	public boolean updateFlag(Integer id);
	
	

}
