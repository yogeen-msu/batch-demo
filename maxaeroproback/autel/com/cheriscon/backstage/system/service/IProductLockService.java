package com.cheriscon.backstage.system.service;

import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.framework.database.Page;

public interface IProductLockService {
	public Page getProductLocks(String serialNO, int pageNo, int pageSize)
			throws Exception;

	public void addProductLock(ProductInfo productInfo) throws Exception;
}
