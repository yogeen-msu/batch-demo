package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.ToolDownload;
import com.cheriscon.framework.database.Page;

public interface IToolDownloadService {
	
	/**
	 * 增加
	 * @param toolDownload		下载工具
	 * @param proTypeCode		关联的产品型号
	 * @return
	 */
	public boolean saveToolDownload(ToolDownload toolDownload,String [] proTypeCode);
	
	/**
	 * 修改
	 * @param toolDownload
	 */
	public boolean updateToolDownload(ToolDownload toolDownload,String [] proTypeCode);
	
	/**
	 * 删除
	 */
	public boolean delToolDownloadByCode(String code);
	
	/**
	 * 根据Code查询
	 * @param id
	 * @return
	 */
	public ToolDownload getByCode(String code);
	
	/**
	 * 查询出该产品型号下的下载工具
	 * @return
	 */
	public List<ToolDownload> queryToolDownloadByProductType(String productTypeCode);

	/**
	 * 翻页查询所有下载工具
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageToolDownload(ToolDownload toolDownload,int pageNo, int pageSize);

}
