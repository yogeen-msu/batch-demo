package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.CountryCode;
import com.cheriscon.framework.database.Page;

public interface ICountryCodeService {
	/**
	 * 分页查询国家编码
	 * @param countryCode
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageCountryCode(CountryCode countryCode, int pageNo, int pageSize);
	/**
	 * 获取全部国家编码
	 * @return
	 */
	public List<CountryCode> getCountryCodeList();
	/**
	 * 获取国家编码
	 * @param id
	 * @return
	 */
	public CountryCode getCountryCode(int id);
	/**
	 * 获取国家编码
	 * @param id
	 * @return
	 */
	public List<CountryCode> getCountryCode(String country);
	/**
	 * 新增保存国家编码
	 * @param countryCode
	 * @return
	 */
	public boolean saveCountryCode(CountryCode countryCode);
	/**
	 * 修改国家编码
	 * @param countryCode
	 * @return
	 */
	public boolean updateCountryCode(CountryCode countryCode);
	/**
	 * 删除国家编码
	 * @param id
	 * @return
	 */
	public boolean deleteCountryCode(int id);
}
