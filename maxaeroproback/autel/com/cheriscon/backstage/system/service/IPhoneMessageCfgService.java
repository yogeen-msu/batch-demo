package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.PhoneMessageCfg;

public interface IPhoneMessageCfgService {
	
	/**
	 * 增加
	 * @param phoneMessageCfg
	 */
	public boolean savePhoneMessageCfg(PhoneMessageCfg phoneMessageCfg);
	
	/**
	 * 修改
	 * @param phoneMessageCfg
	 */
	public boolean updatePhoneMessageCfg(PhoneMessageCfg phoneMessageCfg);
	
	/**
	 * 删除
	 * @param id
	 * @return  操作结果   0：成功   1：失败   2：正在使用
	 */
	public int delPhoneMessageCfgById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public PhoneMessageCfg getById(int id);
	
	/**
	 * 获取使用中的配置
	 * @return
	 */
	public PhoneMessageCfg getUseConfig();
	
	/**
	 * 翻页查询所有短信配置
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<PhoneMessageCfg> listPhoneMessageCfg();

}
