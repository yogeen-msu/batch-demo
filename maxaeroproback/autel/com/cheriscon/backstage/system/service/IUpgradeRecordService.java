package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.UpgradeRecord;

public interface IUpgradeRecordService {

	public List<UpgradeRecord> getUpgradeRecordList(UpgradeRecord record,String languageCode,String queryType) throws Exception;
}
