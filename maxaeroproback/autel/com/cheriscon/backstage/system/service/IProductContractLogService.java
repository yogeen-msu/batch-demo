package com.cheriscon.backstage.system.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.cheriscon.common.model.EmailLog;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.framework.database.Page;
public interface IProductContractLogService {
	
	/**
	 * 
	 * 新增日志
	 * */
	public void saveLog(ProductContractChangeLog log) throws Exception;
	
	public List<ProductContractChangeLog> queryBySerialNo(String serialNo) throws Exception;
	
	public List<ProductContractChangeLog> queryLogForMaxiSys() throws Exception;
	
	public void updateSendEmailFlag(ProductContractChangeLog log) throws Exception;
	
	public Page getProductContractChangeLogs(ProductContractChangeLog log, int pageNo,int pageSize) throws Exception ;
		 
}
