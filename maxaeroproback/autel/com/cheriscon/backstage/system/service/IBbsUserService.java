package com.cheriscon.backstage.system.service;

import com.cheriscon.common.model.BbsUser;

public interface IBbsUserService {
	
    public void add(BbsUser bbsUser) throws Exception;
    
}
