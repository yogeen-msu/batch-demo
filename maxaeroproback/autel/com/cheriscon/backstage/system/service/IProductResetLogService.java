package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.ProductResetLog;
import com.cheriscon.framework.database.Page;

public interface IProductResetLogService {
	
	/**
	 * 
	 * 新增重置
	 * */
	public void saveLog(ProductResetLog log) throws Exception;
	
	
	/**
	 * 查询列表
	 * **/
	public Page pageProductReset(ProductResetLog log,int pageNo, int pageSize) throws Exception;
	
	
	/**
	 * 查询列表
	 * **/
	public ProductResetLog getProductReset(String code) throws Exception;
	
	
	/**
	 * 查询列表
	 * **/
	public List<ProductResetLog> getProductResetByProCode(String proCode) throws Exception;
	/**
	 * 替换或系统调用
	 * @param log
	 * @return
	 */
	public List<ProductResetLog> getProReset(ProductResetLog log);
	

}
