package com.cheriscon.backstage.system.service;

import com.cheriscon.common.model.UpLoginIpInfo;
import com.cheriscon.framework.database.Page;

public interface IUpLoginIpInfoService {

	public Page pageUpLoginIpInfo(UpLoginIpInfo info,int pageNo, int pageSize);
}
