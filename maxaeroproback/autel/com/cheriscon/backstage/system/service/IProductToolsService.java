package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.ProductTypeTools;

/**
 * 工具下载业务逻辑接口类
 * @author yangpinggui 2013-1-20
 * 
 */
public interface IProductToolsService {
	/**
	 * 查询工具下载信息记录
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	public List<ProductTypeTools> queryProductToolList(final String languageCode) throws Exception;
	
	
}
