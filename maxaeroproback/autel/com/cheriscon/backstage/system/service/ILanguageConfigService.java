package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.LanguageConfig;
import com.cheriscon.framework.database.Page;

public interface ILanguageConfigService {
	
	/**
	 * 增加
	 * @param languageConfig
	 */
	public boolean saveLanguageConfig(LanguageConfig languageConfig);
	
	/**
	 * 修改
	 * @param languageConfig
	 */
	public boolean updateLanguageConfig(LanguageConfig languageConfig);
	
	/**
	 * 删除
	 */
	public boolean delLanguageConfigById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public LanguageConfig getById(int id);
	
	/**
	 * 翻页查询所有软件语言
	 * @param languageConfig
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageLanguageConfig(LanguageConfig languageConfig, int pageNo, int pageSize);
	
	
	/**
	 * 
	* @Title: listAll
	* @author shaohu
	* @Description: 查询语言配置list数据
	* @param    
	* @return List<LanguageConfig>    
	* @throws
	 */
	public List<LanguageConfig> listAll() throws Exception;

	public List<LanguageConfig> getByNewLanguageConfig(LanguageConfig languageConfig);
	
	
	/**
	 * 
	* @Title: queryBySealerCode
	* @author chenqichuan
	* @Description: 根据经销商编码查询当前契约下有那些语言配置的契约
	* @param    proTypeCode产品类型 ，sealerCode经销商code
	* @return List<LanguageConfig>    
	* @throws
	 */
	public List<LanguageConfig> queryBySealerCode(String proTypeCode,String sealerCode);
}
