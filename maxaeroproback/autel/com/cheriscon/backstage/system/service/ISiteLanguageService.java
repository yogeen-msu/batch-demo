package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.SiteLanguage;

public interface ISiteLanguageService {
	
	/**
	 * 增加
	 * @param language
	 */
	public boolean saveLanguage(SiteLanguage language);
	
	/**
	 * 修改
	 * @param language
	 */
	public boolean updateLanguage(SiteLanguage language);
	
	/**
	 * 删除
	 */
	public boolean delLanguageById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public SiteLanguage getById(int id);
	
	/**
	 * 查询所有网站支持的语言
	 * @return
	 */
	public List<SiteLanguage> listLanguage();

	public List<SiteLanguage> getByNewSiteLanguage(SiteLanguage siteLanguage);
}
