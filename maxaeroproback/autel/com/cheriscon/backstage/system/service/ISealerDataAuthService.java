package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.SealerDataAuth;

public interface ISealerDataAuthService {

	public List<SealerDataAuth> getSealerDataAuths(String parentCode ,String name)
			throws Exception;

	public SealerDataAuth getSealerDataAuthByCode(String code) throws Exception;

	public void addSealerDataAuth(SealerDataAuth sealerDataAuth)
			throws Exception;

	public void editSealerDataAuth(SealerDataAuth sealerDataAuth)
			throws Exception;

	public List<SealerDataAuth> getTreeBySealerDataAuthName(String name)
			throws Exception;

	public List<SealerDataAuth> getSealerDataAuthTree(String code)
			throws Exception;
	
	public void deleteSealerDataAuth(String code) throws Exception;

}
