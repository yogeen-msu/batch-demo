package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

@Service
public class ProductContractLogServiceImpl extends
		BaseSupport<ProductContractChangeLog> implements
		IProductContractLogService {

	@Override
	public void saveLog(ProductContractChangeLog log) throws Exception {
		String tableName = DBUtils.getTableName(ProductContractChangeLog.class);
		this.daoSupport.insert(tableName, log);

	}

	public List<ProductContractChangeLog> queryBySerialNo(String serialNo)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_ProductContractChangeLog where productSN=?");

		return this.daoSupport.queryForList(sql.toString(),
				ProductContractChangeLog.class, serialNo);
	}

	public List<ProductContractChangeLog> queryLogForMaxiSys() throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append(" select distinct a.productSn,a.sendemailFlag   from ");
		sql.append(" DT_ProductContractChangeLog a,");
		sql.append(" DT_SaleContract b,");
		sql.append(" DT_SaleContract c,");
		sql.append(" dt_productinfo d ");
		sql.append(" where a.oldContract=b.code ");
		sql.append(" and a.newContract =c.code ");
		sql.append(" and a.productSN=d.serialNo ");
		sql.append(" and b.proTypeCode='"
				+ FrontConstant.getProperValue("product.type.maxisys") + "' ");
		sql.append(" and c.proTypeCode='"
				+ FrontConstant.getProperValue("product.type.maxisyspro")
				+ "' ");
		sql.append(" and a.sendemailFlag='N' ");
		sql.append(" and d.regStatus=1 ");

		return this.daoSupport.queryForList(sql.toString(),
				ProductContractChangeLog.class);
	}

	public void updateSendEmailFlag(ProductContractChangeLog log)
			throws Exception {
		String sql="update DT_ProductContractChangeLog set sendemailFlag='Y' where productSn=? ";
		this.daoSupport.execute(sql, log.getProductSN());
	}

	@Override
	public Page getProductContractChangeLogs(ProductContractChangeLog log,
			int pageNo, int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer("select * from ("
				+"select distinct a.productsn,b.name as oldcontract,c.name as newcontract,"
						+ " a.operatoruser,SUBSTRING(operatorTime,1,10) as operatorTime ,a.operatorip,a.sendemailflag from DT_ProductContractChangeLog a "
						+ "left join dt_salecontract b on a.oldcontract = b.code LEFT JOIN  dt_salecontract c on a.newcontract = c.code ");

		sql.append("where 1=1");
		if (log != null) {
			if (!StringUtil.isEmpty(log.getProductSN())) {
				sql.append(" and a.productSN like '%" + log.getProductSN()
						+ "%' ");
			}
			if (!StringUtil.isEmpty(log.getNewContract())) {
				sql.append(" and c.name like '%" + log.getNewContract() + "%' ");
			}
			if (!StringUtil.isEmpty(log.getOldContract())) {
				sql.append(" and b.name like '%" + log.getOldContract() + "%' ");
			}
		}
		sql.append("   ");
		sql.append(" ) m order by m.operatorTime desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,
				ProductContractChangeLog.class, new Object[] {});
	}
}
