package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.mapper.LanguageConfigMapper;
import com.cheriscon.backstage.system.service.ILanguageConfigService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.LanguageConfig;
import com.cheriscon.common.model.LanguageConfigDetail;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class LanguageConfigServiceImpl extends BaseSupport<LanguageConfig> implements ILanguageConfigService {
	
	@Transactional
	public boolean saveLanguageConfig(LanguageConfig languageConfig) {
		
		//语言配置
		String tableName = DBUtils.getTableName(LanguageConfig.class);
		languageConfig.setCode(DBUtils.generateCode(DBConstant.LANGUAGE_CONFIG_CODE));
		this.daoSupport.insert(tableName, languageConfig);
		
		//配置言详情
		String detailTableName = DBUtils.getTableName(LanguageConfigDetail.class);
		if(languageConfig.getLanguageList() != null){
			for (Language language : languageConfig.getLanguageList()) {
				LanguageConfigDetail detail = new LanguageConfigDetail();
				detail.setLanguageCfgCode(languageConfig.getCode());
				detail.setLanguageCode(language.getCode());
				this.daoSupport.insert(detailTableName, detail);
			}
		}
		
		return true;
	}

	@Transactional
	public boolean updateLanguageConfig(LanguageConfig languageConfig) {
		String tableName = DBUtils.getTableName(LanguageConfig.class);
		this.daoSupport.update(tableName,languageConfig,"id="+languageConfig.getId());
		
		//配置言详情    删除原来的，增加现有的
		String detailTableName = DBUtils.getTableName(LanguageConfigDetail.class);
		StringBuffer delDetailSql = new StringBuffer("delete from "+ detailTableName);
		delDetailSql.append(" where languageCfgCode=?");
		this.daoSupport.execute(delDetailSql.toString(),languageConfig.getCode());
		
		if(languageConfig.getLanguageList() != null){
			for (Language language : languageConfig.getLanguageList()) {
				LanguageConfigDetail detail = new LanguageConfigDetail();
				detail.setLanguageCfgCode(languageConfig.getCode());
				detail.setLanguageCode(language.getCode());
				this.daoSupport.insert(detailTableName, detail);
			}
		}
		
		return true;
	}

	@Transactional
	public boolean delLanguageConfigById(int id) {
		
		LanguageConfig config = getById(id);
		//删除配置言详情 项
		String detailTableName = DBUtils.getTableName(LanguageConfigDetail.class);
		StringBuffer delDetailSql = new StringBuffer("delete from "+ detailTableName);
		delDetailSql.append(" where languageCfgCode=?");
		this.daoSupport.execute(delDetailSql.toString(),config.getCode());
		
		//删除配置言详情
		String tableName = DBUtils.getTableName(LanguageConfig.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public LanguageConfig getById(int id){
		String tableName = DBUtils.getTableName(LanguageConfig.class);
		String sql = "select * from " + tableName + " where id=?";
		LanguageConfig config = this.daoSupport.queryForObject(sql, LanguageConfig.class, id);
		
		String detailTableName = DBUtils.getTableName(LanguageConfigDetail.class);
		String lanTableName  = DBUtils.getTableName(Language.class);
		StringBuffer configItemBuffer = new StringBuffer("select");
		configItemBuffer.append(" lc.*,lan.id lid,lan.code lcode,lan.name lname ");
		configItemBuffer.append(" from " + detailTableName +" lc ");
		configItemBuffer.append(" left join " + lanTableName+" lan on lc.languageCode=lan.code " );
		configItemBuffer.append(" where lc.languageCfgCode='"+config.getCode()+"'");
		List languageList = this.daoSupport.queryForList(configItemBuffer.toString(),new LanguageConfigMapper());
		config.setLanguageList(languageList);
		
		return config;
	}

	public Page pageLanguageConfig(LanguageConfig languageConfig, int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(LanguageConfig.class);
		String select = "select lc.* from " + tableName +" lc ";
		StringBuffer term = new StringBuffer();
		
		if(languageConfig != null && !StringUtil.isEmpty(languageConfig.getName())){
			if(term.length()>0){
				term.append(" and ");
			}else{
				term.append(" where ");
			}
			term.append(" lc.name like '%"+languageConfig.getName().trim()+"%' ");
		}
		StringBuffer sql = new StringBuffer(select);
		sql.append(term);
		sql.append(" order by id asc ");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,LanguageConfig.class,new Object[]{});
		return page;
	}

	/**
	  * <p>Title: listAll</p>
	  * <p>Description: </p>
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<LanguageConfig> listAll() throws Exception {
		String tableName = DBUtils.getTableName(LanguageConfig.class);
		String select = "select lc.* from " + tableName +" lc order by lc.id desc";
		return this.daoSupport.queryForList(select.toString(), LanguageConfig.class);
	}
	
	public List<LanguageConfig> getByNewLanguageConfig(LanguageConfig languageConfig){
		String tableName = DBUtils.getTableName(LanguageConfig.class);
		String sql = "select * from " + tableName +" where name=?";
		return this.daoSupport.queryForList(sql,LanguageConfig.class,new Object[]{languageConfig.getName().replace(" ","")});
	}

	public List<LanguageConfig> queryBySealerCode(String proTypeCode,String sealerCode){
		
		StringBuffer sql=new StringBuffer();
		sql.append("select distinct a.code,a.name from DT_LanguageConfig a ,DT_SaleContract b");
		sql.append(" where a.code=b.languageCfgCode");
		sql.append(" and b.proTypeCode=?");
		sql.append(" and b.sealerCode=?");
		//sql.append(" and b.isshow=1");
		sql.append(" order by a.name asc");
		
		return this.daoSupport.queryForList(sql.toString(),LanguageConfig.class,new Object[]{proTypeCode,sealerCode});
	}
}
