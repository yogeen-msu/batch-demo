package com.cheriscon.backstage.system.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.system.service.IToolDownloadService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.ProductTypeTools;
import com.cheriscon.common.model.ToolDownload;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.cop.sdk.utils.UploadUtil;
import com.cheriscon.framework.database.Page;

@Service
public class ToolDownloadServiceImpl extends BaseSupport implements IToolDownloadService {
	
	@Transactional
	public boolean saveToolDownload(ToolDownload toolDownload,String [] proTypeCode) {
		String tableName = DBUtils.getTableName(ToolDownload.class);
		toolDownload.setCode(DBUtils.generateCode(DBConstant.TOOLDOWNLOAD_CODE));
		toolDownload.setReleaseDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
		this.daoSupport.insert(tableName, toolDownload);
		
		//工具关联产品
		if(proTypeCode != null && proTypeCode.length > 0){
			String realTbl = DBUtils.getTableName(ProductTypeTools.class);
			for (String code : proTypeCode) {
				ProductTypeTools proType = new ProductTypeTools();
				proType.setToolsCode(toolDownload.getCode());
				proType.setProductTypeCode(code);
				this.daoSupport.insert(realTbl, proType);
			}
		}
		
		return true;
	}

	@Transactional
	public boolean updateToolDownload(ToolDownload toolDownload,String [] proTypeCode) {
		String tableName = DBUtils.getTableName(ToolDownload.class);
		ToolDownload tool = getByCode(toolDownload.getCode());
		//修改下载工具
		if(StringUtils.isNotEmpty(toolDownload.getDownloadPath())){
			UploadUtil.deleteFile(UploadUtil.replacePath(tool.getDownloadPath()));
			tool.setDownloadPath(toolDownload.getDownloadPath());
		}
		
		tool.setLastVersion(toolDownload.getLastVersion());
		tool.setReleaseDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
		tool.setToolName(toolDownload.getToolName());
		this.daoSupport.update(tableName,toolDownload,"id="+toolDownload.getId());
		
		String proTypeTbl = DBUtils.getTableName(ProductTypeTools.class);
		StringBuffer sql = new StringBuffer();
		
		//删除原有的关系 
		sql.append("delete from "+proTypeTbl+" where toolsCode=?");
		this.daoSupport.execute(sql.toString(),toolDownload.getCode());
		
		if(proTypeCode != null && proTypeCode.length > 0){
			for (String code : proTypeCode) {
				ProductTypeTools proType = new ProductTypeTools();
				proType.setToolsCode(toolDownload.getCode());
				proType.setProductTypeCode(code);
				this.daoSupport.insert(proTypeTbl, proType);
			}
		}
		
		return true;
	}

	@Transactional
	public boolean delToolDownloadByCode(String code){
		//删除中间表
		String prtTbl = DBUtils.getTableName(ProductTypeTools.class);
		this.daoSupport.execute("delete from "+ prtTbl +" where toolsCode=?",code);
		
		String tableName = DBUtils.getTableName(ToolDownload.class);
		this.daoSupport.execute("delete from "+ tableName + " where code=?",code);
		
		return true;
	}
	
	public ToolDownload getByCode(String code){
		String tableName = DBUtils.getTableName(ToolDownload.class);
		String sql = "select * from " + tableName + " where code=?";
		ToolDownload toolDownload = (ToolDownload)this.daoSupport.queryForObject(sql, ToolDownload.class,code);
		
		//获取关联的产品型号
		String prtToolTbl = DBUtils.getTableName(ProductTypeTools.class);
		String prtTbl = DBUtils.getTableName(ProductForSealer.class);
		StringBuffer proSql = new StringBuffer();
		proSql.append("select distinct prt.* from "+prtTbl+" prt ");
		proSql.append(" left join "+prtToolTbl+" prttl on prttl.productTypeCode=prt.code");
		proSql.append(" where prttl.toolsCode=?");
		List<ProductType> proTypeList = this.daoSupport.queryForList(proSql.toString(),ProductType.class,code);
		toolDownload.setProductTypeList(proTypeList);
		
		return toolDownload;
	}
	

	@Override
	public List<ToolDownload> queryToolDownloadByProductType(String productTypeCode) {
		String prtTbl = DBUtils.getTableName(ProductTypeTools.class);
		String tableName = DBUtils.getTableName(ToolDownload.class);
		StringBuffer sql = new StringBuffer("select * from " + tableName +" tbl ");
		sql.append(" left join " + prtTbl +" prt on prt.toolsCode=tbl.code ");
		sql.append(" where prt.productTypeCode=? ");
		return this.daoSupport.queryForList(sql.toString(),ToolDownload.class,productTypeCode);
	}

	@Override
	public Page pageToolDownload(ToolDownload toolDownload, int pageNo,int pageSize) {
		String prtToolTbl = DBUtils.getTableName(ProductTypeTools.class);
		String prtTbl = DBUtils.getTableName(ProductType.class);
		String tableName = DBUtils.getTableName(ToolDownload.class);
		
		
		
		StringBuffer sql = new StringBuffer("select ");
		sql.append(" tbl.* from " + tableName +" tbl ");
//		sql.append(" inner join " + prtToolTbl + " prt on prt.toolsCode=tbl.code ");
//		sql.append(" inner join " + prtTbl + " prtTb on prtTb.code=prt.productTypeCode ");
		sql.append(" where 1=1 ");
		if(toolDownload != null){
			if(StringUtils.isNotEmpty(toolDownload.getCode())){	//工具编码
				sql.append(" and tbl.code = '"+toolDownload.getCode()+"' ");	
			}
			
			//适用产品型号
			if (StringUtils.isNotEmpty(toolDownload.getProductTypeCode())) {
				String tempSql = "select distinct prtl.toolsCode as toolsCode  from "+prtToolTbl+" prtl where prtl.productTypeCode=?";
				List<Map> toolsCodeList = this.daoSupport.queryForList(tempSql,toolDownload.getProductTypeCode().trim());
				if(toolsCodeList != null && toolsCodeList.size()>0){
					StringBuffer codes = new StringBuffer();
					for (Map m : toolsCodeList) {
						codes.append("'"+m.get("toolsCode")).append("',");
					}
					codes.deleteCharAt(codes.length()-1);
					sql.append(" and tbl.code in ("+codes+") ");
				}else{
					sql.append(" and tbl.code = '-1'");		//没有则为空
				}
				
			}
//			if(StringUtils.isNotEmpty(toolDownload.getProductTypeCode())){
//				sql.append(" and prtTb.name like '%"+toolDownload.getProductTypeCode()+"%' ");
//			}
			
			if(StringUtils.isNotEmpty(toolDownload.getStartDate())){	//开始日期
				sql.append(" and tbl.releaseDate >= '"+toolDownload.getStartDate()+"' ");	
			}
			
			if(StringUtils.isNotEmpty(toolDownload.getStartDate())){	//结束日期
				sql.append(" and tbl.releaseDate <= '"+toolDownload.getEndDate()+"' ");	
			}
		}
		sql.append(" order by tbl.code desc ");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,ToolDownload.class,new Object[]{});
	}

}
