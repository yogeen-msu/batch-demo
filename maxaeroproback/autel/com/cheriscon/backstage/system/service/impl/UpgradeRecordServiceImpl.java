package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IUpgradeRecordService;
import com.cheriscon.common.model.UpgradeRecord;
import com.cheriscon.cop.sdk.database.BaseSupport;

@Service
public class UpgradeRecordServiceImpl extends BaseSupport<UpgradeRecord> implements IUpgradeRecordService {

	@Override
	public List<UpgradeRecord> getUpgradeRecordList(UpgradeRecord record,String languageCode,String queryType)
			throws Exception {
		
		StringBuffer sql=new StringBuffer(); 
		sql.append("select a.productSN as productSN,a.upgradeVersion as upgradeVersion,date_format(a.upgradeTime,'%m/%d/%Y') as upgradeTime,b.SoftwareName as softName");
		
		sql.append(" from t_UpgradeRecord a,DT_SoftWareName b ,DT_Language c ");
		sql.append(" where a.softCode=b.softwareCode and b.languageCode=c.code");
		sql.append(" and productSN='").append(record.getProductSN()).append("'");
		sql.append(" and c.code='").append(languageCode).append("'");
		if(StringUtils.isNotBlank(record.getSoftName())){
			sql.append(" and b.SoftwareName like '%").append(record.getSoftName().trim()).append("%'");
		}
		if(queryType.equals("1")){
			sql.append(" limit 0,20 ");
		}
		return this.daoSupport.queryForList(sql.toString(), UpgradeRecord.class);
	}

}
