package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.ISoftListService;
import com.cheriscon.common.model.SoftList;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.usercenter.distributor.vo.SoftListVo;

@Service
public class SoftListServiceImpl extends BaseSupport<SoftList> implements ISoftListService {

	@Override
	public List<SoftList> getSoftListByProductSn(String productSn) throws Exception {
		
		StringBuffer sql=new StringBuffer();
		sql.append("select * from t_softList where lastOpCode='update' and productSn=?");
		return this.daoSupport.queryForList(sql.toString(), SoftList.class,productSn);
	}

	public List<SoftList> getSoftList(SoftList record,String languageCode,String queryType){
		StringBuffer sql=new StringBuffer();
		if(queryType.equals("1")){
			sql.append("select a.serialNo as productSN,");
			sql.append(" (select versionName from DT_SoftwareVersion where softwareTypeCode=e.softwareTypeCode and releaseState=1 order by releaseDate desc limit 0,1) as softSysVersion ,");
			sql.append(" f.softVersion as softVersion ,");
			sql.append(" f.lastOpCode as lastOpCode ,");
			sql.append(" s.softwareName as softName,");
			sql.append(" date_format(f.syncTime,'%m/%d/%Y')  as syncTime");
		}else{
			sql.append("select a.serialNo as productSN,");
			sql.append(" (select versionName from DT_SoftwareVersion where softwareTypeCode=e.softwareTypeCode and releaseState=1 order by releaseDate desc limit 0,1) as softSysVersion ,");
			sql.append(" f.softVersion as softVersion ,");
			sql.append(" f.lastOpCode as lastOpCode ,");
			sql.append(" s.softwareName as softName,");
			sql.append(" date_format(f.syncTime,'%m/%d/%Y') as syncTime");
		}
		sql.append(" from ");
		sql.append("dt_productinfo a, ");
		sql.append("DT_SaleContract b, ");
		sql.append("DT_SaleConfig c, ");
		sql.append("DT_MinSaleUnitSaleCfgDetail d, ");
		sql.append("DT_MinSaleUnitSoftwareDetail e  ");
		sql.append("left join  t_SoftList f on e.softwareTypeCode=f.softCode and f.productSN='"+record.getProductSN()+"', ");
		sql.append("DT_SoftWareName s ,DT_Language l ");
		sql.append("where a.saleContractCode=b.code ");
		sql.append("and b.saleCfgCode=c.code ");
		sql.append("and c.code=d.saleCfgCode ");
		sql.append("and d.minSaleUnitCode=e.minSaleUnitCode ");
		sql.append("and s.softwareCode=e.softwareTypeCode ");
		sql.append("and s.languageCode=l.code ");
		sql.append("and l.code='").append(languageCode).append("' ");
		sql.append(" and a.serialNo='").append(record.getProductSN()).append("'");
		if(StringUtils.isNotBlank(record.getSoftName())){
			sql.append(" and s.SoftwareName like '%").append(record.getSoftName().trim()).append("%'");
		}
		sql.append(" order by s.SoftwareName");
		return this.daoSupport.queryForList(sql.toString(), SoftList.class);
	}
	
	public List<SoftList> getSoftList(SoftListVo softListVo){
		StringBuffer sql=new StringBuffer();
		if(softListVo.getQueryType().equals("1")){
			sql.append("select a.serialNo as productSN,");
			sql.append(" (select versionName from DT_SoftwareVersion where softwareTypeCode=e.softwareTypeCode and releaseState=1 order by releaseDate desc limit 0,1) as softSysVersion ,");
			sql.append(" f.softVersion as softVersion ,");
			sql.append(" f.lastOpCode as lastOpCode ,");
			sql.append(" s.softwareName as softName,");
			sql.append(" date_format(f.syncTime,'%m/%d/%Y')  as syncTime");
		}else{
			sql.append("select a.serialNo as productSN,");
			sql.append(" (select versionName from DT_SoftwareVersion where softwareTypeCode=e.softwareTypeCode and releaseState=1 order by releaseDate desc limit 0,1) as softSysVersion ,");
			sql.append(" f.softVersion as softVersion ,");
			sql.append(" f.lastOpCode as lastOpCode ,");
			sql.append(" s.softwareName as softName,");
			sql.append(" date_format(f.syncTime,'%m/%d/%Y') as syncTime");
		}
		sql.append(" from ");
		sql.append("dt_productinfo a, ");
		sql.append("DT_SaleContract b, ");
		sql.append("DT_SaleConfig c, ");
		sql.append("DT_MinSaleUnitSaleCfgDetail d, ");
		sql.append("DT_MinSaleUnitSoftwareDetail e  ");
		sql.append("left join  t_SoftList f on e.softwareTypeCode=f.softCode and f.productSN='"+softListVo.getProductSN()+"', ");
		sql.append("DT_SoftWareName s ,DT_Language l ");
		sql.append("where a.saleContractCode=b.code ");
		sql.append("and b.saleCfgCode=c.code ");
		sql.append("and c.code=d.saleCfgCode ");
		sql.append("and d.minSaleUnitCode=e.minSaleUnitCode ");
		sql.append("and s.softwareCode=e.softwareTypeCode ");
		sql.append("and s.languageCode=l.code ");
		sql.append("and l.code='").append(softListVo.getLanguageCode()).append("' ");
		sql.append(" and a.serialNo='").append(softListVo.getProductSN()).append("'");
		if(StringUtils.isNotBlank(softListVo.getSoftName())){
			sql.append(" and s.SoftwareName like '%").append(softListVo.getSoftName().trim()).append("%'");
		}
		sql.append(" order by s.SoftwareName");
		return this.daoSupport.queryForList(sql.toString(), SoftList.class);
	}
}
