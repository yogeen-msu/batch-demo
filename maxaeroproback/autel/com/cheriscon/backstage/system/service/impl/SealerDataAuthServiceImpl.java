package com.cheriscon.backstage.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.ISealerDataAuthService;
import com.cheriscon.common.model.SealerDataAuth;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.util.StringUtil;

@Service
public class SealerDataAuthServiceImpl extends BaseSupport<SealerDataAuth>
		implements ISealerDataAuthService {

	@Override
	public List<SealerDataAuth> getSealerDataAuths(String parentCode,
			String name) throws Exception {

		StringBuffer sql = new StringBuffer("select * from DT_SealerDataAuth ");
		sql.append(" where 1=1 ");
		if (!StringUtil.isEmpty(parentCode)) {
			sql.append(" and parentcode ='" + parentCode.trim() + "'");
		}

		if (StringUtils.isNotBlank(name)) {
			sql.append(" and name like '%" + name.trim() + "%'");
		}
		sql.append(" order by code desc");
		return this.daoSupport.queryForList(sql.toString(),
				SealerDataAuth.class);
	}

	@Override
	public List<SealerDataAuth> getTreeBySealerDataAuthName(String name)
			throws Exception {

		List<SealerDataAuth> sealerDataAuths = getSealerDataAuths("0", name);

		List<SealerDataAuth> sealerDataAuthParents = new ArrayList<SealerDataAuth>();

		for (SealerDataAuth sealerDataAuth : sealerDataAuths) {

			List<SealerDataAuth> sealerDataAuthList = this
					.getSealerDataAuthTree(sealerDataAuth.getCode());

			sealerDataAuthParents.add(sealerDataAuth);
			sealerDataAuth.setChildren(sealerDataAuthList);
		}

		return sealerDataAuthParents;
	}

	@Override
	public List<SealerDataAuth> getSealerDataAuthTree(String code)
			throws Exception {
		if (code == null)
			throw new IllegalArgumentException("code argument is null");
		List<SealerDataAuth> sealerDataAuths = this.getSealerDataAuths(null,
				null);
		List<SealerDataAuth> topSealerDataAuths = new ArrayList<SealerDataAuth>();

		for (SealerDataAuth sealerDataAuth : sealerDataAuths) {
			if (sealerDataAuth.getParentcode().equals(code)) {
				List<SealerDataAuth> children = this.getChildren(
						sealerDataAuths, sealerDataAuth.getCode());
				sealerDataAuth.setChildren(children);
				topSealerDataAuths.add(sealerDataAuth);
			}
		}
		return topSealerDataAuths;
	}

	private List<SealerDataAuth> getChildren(
			List<SealerDataAuth> sealerDataAuths, String parentCode)
			throws Exception {
		List<SealerDataAuth> children = new ArrayList<SealerDataAuth>();
		for (SealerDataAuth sealerDataAuth : sealerDataAuths) {
			if (sealerDataAuth.getParentcode().equals(parentCode)) {
				sealerDataAuth.setChildren(this.getChildren(sealerDataAuths,
						sealerDataAuth.getCode()));
				children.add(sealerDataAuth);
			}
		}
		return children;
	}

	@Override
	public SealerDataAuth getSealerDataAuthByCode(String code) throws Exception {

		String sql = "select * from DT_SealerDataAuth where code =? ";
		return this.daoSupport.queryForObject(sql, SealerDataAuth.class, code);
	}

	@Transactional
	public void addSealerDataAuth(SealerDataAuth sealerDataAuth)
			throws Exception {
		List<SealerDataAuth> sealerDataAuths = getSealerDataAuths(
				sealerDataAuth.getParentcode(), null);
		if (sealerDataAuths.isEmpty()) {
			if("0".equals(sealerDataAuth.getParentcode())){
				sealerDataAuth.setCode("0001");
			}else{
				sealerDataAuth.setCode(sealerDataAuth.getParentcode()+",1");
			}
			
		} else {
			String bortherCode = sealerDataAuths.get(0).getCode();
			String stardIndex;
			int endIndex;
			if (bortherCode.contains(",")) {
				stardIndex = bortherCode.substring(0,
						bortherCode.lastIndexOf(","));
				endIndex = Integer.parseInt(bortherCode.substring(bortherCode
						.lastIndexOf(",") + 1));
				sealerDataAuth.setCode(stardIndex + "," + (++endIndex));
			} else {
				endIndex = Integer.parseInt(bortherCode);
				sealerDataAuth.setCode("" + (++endIndex));
			}

		}

		this.daoSupport.insert("DT_SealerDataAuth", sealerDataAuth);
	}

	@Transactional
	public void editSealerDataAuth(SealerDataAuth sealerDataAuth)
			throws Exception {
		this.daoSupport.update("DT_SealerDataAuth", sealerDataAuth, " code='"
				+ sealerDataAuth.getCode() + "'");
	}

	@Transactional
	public void deleteSealerDataAuth(String code) throws Exception {
		
		 String sql="delete from DT_SealerDataAuth where code =?";
		 this.daoSupport.execute(sql,code);
		 
	}

}
