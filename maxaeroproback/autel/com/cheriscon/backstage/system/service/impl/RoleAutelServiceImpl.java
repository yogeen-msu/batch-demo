package com.cheriscon.backstage.system.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.app.base.core.model.Role;
import com.cheriscon.app.base.core.service.auth.impl.RoleManager;
import com.cheriscon.backstage.system.service.IRoleAutelService;
import com.cheriscon.framework.database.Page;



/**
 * @remark 角色信息service实现
 * @author pengdongan
 * @date 2013-01-30
 * 
 */
@Service
public class RoleAutelServiceImpl extends RoleManager implements IRoleAutelService {



	/**
	  * 角色信息分页显示方法
	  * @param role
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageRolepage(Role role, int pageNo, int pageSize){
		StringBuffer sql = new StringBuffer();

		sql.append("select * from es_role");
		sql.append(" where 1=1");
		
		if (null != role) {
			if (StringUtils.isNotBlank(role.getRolename())) {
				sql.append(" and rolename like '%");
				sql.append(role.getRolename().trim());
				sql.append("%'");
			}
		}

		sql.append(" order by roleid desc");
			Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
					pageSize, Role.class);
			return page;
	}
}
