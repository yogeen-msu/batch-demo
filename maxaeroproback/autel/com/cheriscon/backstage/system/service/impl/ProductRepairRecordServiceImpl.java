package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.system.service.IProductRepairRecordService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductRepairRecord;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class ProductRepairRecordServiceImpl extends BaseSupport<ProductRepairRecord> implements IProductRepairRecordService {
	
	@Transactional
	public boolean saveProductRepairRecord(ProductRepairRecord productRepairRecord) {
		String tableName = DBUtils.getTableName(ProductRepairRecord.class);
		productRepairRecord.setCode(DBUtils.generateCode(DBConstant.PRODUCTREPAIRRECORD_CODE));
		this.daoSupport.insert(tableName, productRepairRecord);
		return true;
	}

	@Transactional
	public boolean updateProductRepairRecord(ProductRepairRecord productRepairRecord) {
		String tableName = DBUtils.getTableName(ProductRepairRecord.class);
		this.daoSupport.update(tableName,productRepairRecord,"id="+productRepairRecord.getId());
		return true;
	}

	@Transactional
	public boolean delProductRepairRecordById(int id) {
		String tableName = DBUtils.getTableName(ProductRepairRecord.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	public ProductRepairRecord getById(int id){
		String productRepairRecordTbl = DBUtils.getTableName(ProductRepairRecord.class);
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
		
		StringBuffer sql = new StringBuffer("select ");
		sql.append(" prr.*,pif.proTypeCode proTypeCode,pt.name proTypeName,pif.serialNo proSerialNo,pif.proDate proDate ");
		sql.append(" from " +  productRepairRecordTbl + " prr ");
		sql.append(" left join "+productInfoTbl + " pif on  prr.proCode = pif.code ");
		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
		sql.append(" where prr.id=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductRepairRecord.class, id);
	}
	
	
	public List<ProductRepairRecord> queryProductRepairRecord() {
		String productRepairRecordTbl = DBUtils.getTableName(ProductRepairRecord.class);
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
		
		StringBuffer sql = new StringBuffer("select ");
		sql.append(" prr.*,pif.proTypeCode proTypeCode,pt.name proTypeName,pif.serialNo proSerialNo,pif.proDate proDate ");
		sql.append(" from " +  productRepairRecordTbl + " prr ");
		sql.append(" left join "+productInfoTbl + " pif on  prr.proCode = pif.code ");
		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
		
		sql.append(" order by prr.id asc ");
		
		return this.daoSupport.queryForList(sql.toString(), ProductRepairRecord.class, new Object[]{});
	}

	public Page pageProductRepairRecord(ProductRepairRecord productRepairRecord,int pageNo, int pageSize) {
		String productRepairRecordTbl = DBUtils.getTableName(ProductRepairRecord.class);
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
		
		StringBuffer sql = new StringBuffer("select ");
		sql.append(" prr.*,pif.proTypeCode proTypeCode,pt.name proTypeName,pif.serialNo proSerialNo,pif.proDate proDate ");
		sql.append(" from " +  productRepairRecordTbl + " prr ");
		sql.append(" left join "+productInfoTbl + " pif on  prr.proCode = pif.code ");
		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
		
		StringBuffer term = new StringBuffer();
		if(productRepairRecord != null){
			//产品编号
			if(!StringUtil.isEmpty(productRepairRecord.getProCode())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" prr.proCode = '"+productRepairRecord.getProCode()+"' ");
			}
			
			//产品型号
			if(!StringUtil.isEmpty(productRepairRecord.getProTypeCode())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" pif.proTypeCode = '"+productRepairRecord.getProTypeCode()+"' ");
			}
			
			//产品序列号
			if(!StringUtil.isEmpty(productRepairRecord.getProSerialNo())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				
				term.append(" pif.serialNo like '%"+productRepairRecord.getProSerialNo().trim()+"%' ");
			}
			
			sql.append(term);
		}
		sql.append(" order by prr.id asc ");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,ProductRepairRecord.class,new Object[]{});
		return page;
	}

}
