package com.cheriscon.backstage.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.ICountryCodeService;
import com.cheriscon.common.model.CountryCode;
import com.cheriscon.common.model.StateProvince;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class CountryCodeServiceImpl extends BaseSupport<CountryCode> implements
		ICountryCodeService {
	
	public Page pageCountryCode(CountryCode countryCode, int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(CountryCode.class);
		String select = "select * from " + tableName + " where 1=1 ";
		List<Object> list = new ArrayList<Object>();
		if(countryCode != null){
			if(!StringUtil.isEmpty(countryCode.getCountry())){
				select += " and country like ? ";
				list.add("%"+countryCode.getCountry().trim()+"%");
			}
		}
		select += " order by id asc ";
		
		Page page = this.daoSupport.queryForPage(select, pageNo, pageSize,CountryCode.class,list.toArray());
		return page;
	}
	
	public List<CountryCode> getCountryCodeList(){
		String tableName = DBUtils.getTableName(CountryCode.class);
		String tableName2 = DBUtils.getTableName(StateProvince.class);
		
		String sql = "select c.country,c.code,case count(s.id) when 0 then 'none' when 1 then max(s.state) else '' end state from " + tableName + " c ";
		sql += " left join "+ tableName2 +" s on c.id = s.countryId group by c.id,c.code,c.country";
		return this.daoSupport.queryForList(sql, CountryCode.class);
	}
	
	public CountryCode getCountryCode(int id) {
		String tableName = DBUtils.getTableName(CountryCode.class);
		String sql = "select * from " + tableName +" where id = ?";
		return this.daoSupport.queryForObject(sql, CountryCode.class, id);
	}
	
	public List<CountryCode> getCountryCode(String country){
		String tableName = DBUtils.getTableName(CountryCode.class);
		String sql = "select * from " + tableName +" where country = ?";
		return this.daoSupport.queryForList(sql, CountryCode.class, country);
	}
	
	public boolean saveCountryCode(CountryCode countryCode) {
		String tableName = DBUtils.getTableName(CountryCode.class);
		this.daoSupport.insert(tableName, countryCode);
		return true;
	}
	
	public boolean updateCountryCode(CountryCode countryCode) {
		String tableName = DBUtils.getTableName(CountryCode.class);
		this.daoSupport.update(tableName, countryCode, "id="+countryCode.getId());
		return true;
	}
	
	public boolean deleteCountryCode(int id) {
		String tableName = DBUtils.getTableName(CountryCode.class);
		String sql = "delete from " + tableName +" where id = ?";
		this.daoSupport.execute(sql, id);
		return true;
	}
}
