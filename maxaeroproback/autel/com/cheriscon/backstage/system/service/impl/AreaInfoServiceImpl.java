package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.IAreaInfoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.AreaConfigDetail;
import com.cheriscon.common.model.AreaInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class AreaInfoServiceImpl extends BaseSupport<AreaInfo> implements IAreaInfoService {
	
	@Transactional
	public boolean saveAreaInfo(AreaInfo areaInfo) {
		String tableName = DBUtils.getTableName(AreaInfo.class);
		areaInfo.setCode(DBUtils.generateCode(DBConstant.AREAINFO_CODE));
		this.daoSupport.insert(tableName, areaInfo);
		return true;
	}

	@Transactional
	public boolean updateAreaInfo(AreaInfo areaInfo) {
		String tableName = DBUtils.getTableName(AreaInfo.class);
		this.daoSupport.update(tableName,areaInfo,"id="+areaInfo.getId());
		return true;
	}

	@Transactional
	public boolean delAreaInfoById(int id) {
		String tableName = DBUtils.getTableName(AreaInfo.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	public AreaInfo getById(int id){
		String tableName = DBUtils.getTableName(AreaInfo.class);
		String sql = "select * from " + tableName + " where id=?";
		return this.daoSupport.queryForObject(sql, AreaInfo.class, id);
	}
	
	public List<AreaInfo> queryAreaInfo() {
		String tableName = DBUtils.getTableName(AreaInfo.class);
		String sql = "select ain.* from " + tableName +" ain order by id asc";
		return this.daoSupport.queryForList(sql, AreaInfo.class, new Object[]{});
	}
	
	public List<AreaInfo> queryNotSelectAreaInfo(){
		String tableName = DBUtils.getTableName(AreaInfo.class);
		//String acfTable = DBUtils.getTableName(AreaConfigDetail.class);
		StringBuffer sql = new StringBuffer();
		sql.append("select ain.* from ").append(tableName+" ain");
		/*sql.append(" where code not in( ");
		sql.append(" select acd.areaCode from ").append(acfTable+"  acd group by areaCode");
		sql.append(" ) order by id asc");*/
		sql.append(" order by id asc");
		return this.daoSupport.queryForList(sql.toString(), AreaInfo.class, new Object[]{});
	}

	public Page pageAreaInfo(int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(AreaInfo.class);
		String sql = "select ain.* from " + tableName +" ain order by id asc";
		Page page = this.daoSupport.queryForPage(sql, pageNo, pageSize,AreaInfo.class,new Object[]{});
		return page;
	}

	public List<AreaInfo> getByNewAreaInfo(AreaInfo areaInfo){
		String tableName = DBUtils.getTableName(AreaInfo.class);
		String sql = "select * from " + tableName +" ain where name=? and continent=?";
		return this.daoSupport.queryForList(sql,AreaInfo.class,new Object[]{areaInfo.getName().replace(" ",""),areaInfo.getContinent()});
	}
}
