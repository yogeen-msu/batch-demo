package com.cheriscon.backstage.system.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.IProductForProService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ProductForPro;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

@Service
public class ProductForProServiceImpl extends BaseSupport<ProductForPro>
		implements IProductForProService {

	@Override
	public Page getProductForPros(ProductForPro productForPro, int pageNo,
			int pageSize) throws Exception {

		StringBuffer sql = new StringBuffer();
		sql.append("select a.* from DT_PRODUCT_FOR_PRO a");
		sql.append(" where 1=1 ");
		if (productForPro != null) {
			if (!StringUtil.isEmpty(productForPro.getSerialNo())) {
				sql.append(" and a.serialNo like '%" + productForPro.getSerialNo()
						+ "%' ");
			}
			if (!StringUtil.isEmpty(productForPro.getProCode())) {
				sql.append(" and a.proCode like '%" + productForPro.getProCode()
						+ "%' ");
			}
			if (productForPro.getType()>-1) {
				sql.append(" and a.type ='" + productForPro.getType()
						+ "' ");
			}
		}
		sql.append(" order by a.id desc ");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,
				ProductForPro.class);
	}

	
	public Page getProductForProsForBand(ProductForPro productForPro, int pageNo,
			int pageSize) throws Exception{
		
		StringBuffer sql = new StringBuffer();
		sql.append("select a.* from DT_PRODUCT_FOR_PRO a");
		sql.append(" where 1=1 ");
		if (productForPro != null) {
			if (!StringUtil.isEmpty(productForPro.getSerialNo())) {
				sql.append(" and a.serialNo = '" + productForPro.getSerialNo()
						+ "' ");
			}
			if (!StringUtil.isEmpty(productForPro.getProCode())) {
				sql.append(" and a.proCode = '" + productForPro.getProCode()
						+ "' ");
			}
		}
		sql.append(" and a.type ='2' ");
		sql.append(" order by a.id desc ");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,
				ProductForPro.class);
	}
	
	
	@Override
	public ProductForPro getProductForProByCode(String code) throws Exception {
		
		String sql ="select a.* from DT_PRODUCT_FOR_PRO a where a.code= ?";
		return this.daoSupport.queryForObject(sql, ProductForPro.class, code);
	}
	
	@Override
	public ProductForPro getProductForProById(int id) throws Exception {
		
		String sql ="select a.* from DT_PRODUCT_FOR_PRO a where a.id= ?";
		return this.daoSupport.queryForObject(sql, ProductForPro.class, id);
	}

	@Transactional
	public void addProductForPro(ProductForPro productForPro) throws Exception {
		productForPro.setCode(DBUtils
				.generateCode(DBConstant.PRODUCTFORPRO_CODE));
		productForPro
				.setRegStatus(FrontConstant.PRODUCTFORPRO_STATUS_BOUND_NO);
//		productForPro.setRegTime(DateUtil.toString(new Date(),
//				"yyyy-MM-dd HH:mm:ss"));

		this.daoSupport.insert("DT_PRODUCT_FOR_PRO", productForPro);
	}

	@Transactional
	public void deleteProductForProById(int id) throws Exception {
		String sql = "delete from DT_PRODUCT_FOR_PRO where id =? ";
		this.daoSupport.execute(sql, id);
	}

	@Transactional
	public void updateProductForPro(ProductForPro productForPro)
			throws Exception {
		/* StringBuffer sql=new StringBuffer("update DT_PRODUCT_FOR_PRO set ");
		 sql.append(" type =? ,");
		 sql.append(" regPwd =? ,");
		 sql.append(" proDate =? ");
		 sql.append(" where id=? ");
		 this.daoSupport.execute(sql.toString(),productForPro.getType(),
				 productForPro.getRegPwd(),productForPro.getProDate(),productForPro.getId());*/
		
		/*ProductForPro editProductForPro=getProductForProById(productForPro.getId());
		editProductForPro.setProCode(productForPro.getProCode());
		editProductForPro.setReason(productForPro.getReason());
		editProductForPro.setProDate(productForPro.getProDate());
		editProductForPro.setRegPwd(productForPro.getRegPwd());
		editProductForPro.setType(productForPro.getType());*/
		
		
		this.daoSupport.update("DT_PRODUCT_FOR_PRO", productForPro, "id = "+productForPro.getId() );
	}
	
	@Transactional
	public void updateProductForProByMap(Map<String, Object> fields)
			throws Exception {
		
		this.daoSupport.update("DT_PRODUCT_FOR_PRO", fields, "id = "+fields.get("id") );
	}

	@Override
	public ProductForPro getProductForProBySerialNo(String serialNo)
			throws Exception {
		String sql ="select a.* from DT_PRODUCT_FOR_PRO a where a.serialNo= ?";
		return this.daoSupport.queryForObject(sql, ProductForPro.class, serialNo);
	}

}
