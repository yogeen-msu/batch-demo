package com.cheriscon.backstage.system.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.component.AbstractPaymentPlugin;
import com.cheriscon.backstage.system.component.PaymentPluginBundle;
import com.cheriscon.backstage.system.service.IPaymentCfgService;
import com.cheriscon.common.model.PaymentCfg;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.ObjectNotFoundException;
import com.cheriscon.framework.plugin.IPlugin;
import com.cheriscon.framework.util.StringUtil;

/**
 * 支付方式管理
 * @author kingapex
 *2010-4-4下午02:26:19
 */
@Service
public class PaymentCfgServiceImpl extends BaseSupport<PaymentCfg> implements IPaymentCfgService{
	
	@Resource
	private PaymentPluginBundle paymentPluginBundle;			//支付插件桩
 
	public List<PaymentCfg> list() {
		String tableName = DBUtils.getTableName(PaymentCfg.class);
		String sql = "select * from "+tableName + " order by id ";
		return this.daoSupport.queryForList(sql, PaymentCfg.class);
	}

	
	public PaymentCfg get(Integer id) {
		String tableName = DBUtils.getTableName(PaymentCfg.class);
		String sql = "select * from "+tableName+" where id=?";
		PaymentCfg payment =this.daoSupport.queryForObject(sql, PaymentCfg.class, id);
		return payment;
	}

	public PaymentCfg getByPluginId(String pluginid) {
		String tableName = DBUtils.getTableName(PaymentCfg.class);
		String sql = "select * from "+tableName+" where type=?";
		PaymentCfg payment =this.daoSupport.queryForObject(sql, PaymentCfg.class, pluginid);
		return payment;
	}
	
	
	public Double countPayPrice(Integer id) {
		return 0D;
	}

	
	public void add(String name, String type, String biref,Map<String, String> configParmas) {
		if(StringUtil.isEmpty(name)) throw new IllegalArgumentException("payment name is  null");
		if(StringUtil.isEmpty(type)) throw new IllegalArgumentException("payment type is  null");
		if(configParmas == null) throw new IllegalArgumentException("configParmas  is  null");
		
		PaymentCfg payCfg = new PaymentCfg();
		payCfg.setName(name);
		payCfg.setType(type);
		payCfg.setBiref(biref);
		payCfg.setConfig( JSONObject.fromObject(configParmas).toString());
		
		String tableName = DBUtils.getTableName(PaymentCfg.class);
		this.daoSupport.insert(tableName, payCfg);
	}

	
	public Map<String, String> getConfigParams(Integer paymentId) {
		PaymentCfg payment =this.get(paymentId);
		String config  = payment.getConfig();
		if(null == config ) return new HashMap<String,String>();
		JSONObject jsonObject = JSONObject.fromObject( config );  
		Map itemMap = (Map)jsonObject.toBean(jsonObject, Map.class);
		return itemMap;
	}

	
	public Map<String, String> getConfigParams(String pluginid) {
		PaymentCfg payment =this.getByPluginId(pluginid);
		String config  = payment.getConfig();
		if(null == config ) return new HashMap<String,String>();
		JSONObject jsonObject = JSONObject.fromObject( config );  
		Map itemMap = (Map)jsonObject.toBean(jsonObject, Map.class);
		return itemMap;
	}	

	
	public void edit(Integer paymentId, String name,String type, String biref,
			Map<String, String> configParmas) {
		
		if(StringUtil.isEmpty(name)) throw new IllegalArgumentException("payment name is  null");
		if(configParmas == null) throw new IllegalArgumentException("configParmas  is  null");
		
		PaymentCfg payCfg = new PaymentCfg();
		payCfg.setName(name);
		payCfg.setBiref(biref);
		payCfg.setType(type);
		payCfg.setConfig( JSONObject.fromObject(configParmas).toString());	
		
		String tableName = DBUtils.getTableName(PaymentCfg.class);
		this.daoSupport.update(tableName, payCfg, "id="+ paymentId);
	}

	
	public void delete(Integer[] idArray) {
		if(idArray==null || idArray.length==0) return;
		
		String idStr = StringUtil.arrayToString(idArray, ",");
		String tableName = DBUtils.getTableName(PaymentCfg.class);
		String sql  ="delete from "+tableName+" where id in("+idStr+")";
		this.daoSupport.execute(sql);
	}

	
	
	public List<IPlugin> listAvailablePlugins() {
		return this.paymentPluginBundle.getPluginList();
	}

	
	public String getPluginInstallHtml(String pluginId, Integer paymentId) {
		IPlugin installPlugin = null;
		List<IPlugin> plguinList = this.listAvailablePlugins();
		for (IPlugin plugin : plguinList) {
			if (plugin instanceof AbstractPaymentPlugin) {
				if (((AbstractPaymentPlugin) plugin).getId().equals(pluginId)) {
					installPlugin = plugin;
					break;
				}
			}
		}

		if (installPlugin == null)
			throw new ObjectNotFoundException("plugin[" + pluginId
					+ "] not found!");
		FreeMarkerPaser fp = FreeMarkerPaser.getInstance();
		fp.setClz(installPlugin.getClass());

		if (paymentId != null) {
			Map<String, String> params = this.getConfigParams(paymentId);
			Iterator<String> keyIter = params.keySet().iterator();

			while (keyIter.hasNext()) {
				String key = keyIter.next();
				String value = params.get(key);
				fp.putData(key, value);
			}
		}
		return fp.proessPageContent();
	}

}
