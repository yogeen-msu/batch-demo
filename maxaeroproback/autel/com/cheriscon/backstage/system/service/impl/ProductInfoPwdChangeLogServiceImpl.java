package com.cheriscon.backstage.system.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IProductInfoPwdChangeLogService;
import com.cheriscon.common.model.ProductInfoPwdChangeLog;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class ProductInfoPwdChangeLogServiceImpl extends
		BaseSupport<ProductInfoPwdChangeLog> implements
		IProductInfoPwdChangeLogService {

	@Override
	public Page getProductInfoPwdChangeLogs(String procode, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer(
				"select a.id,b.serialno as procode ,a.oldpwd,a.newpwd,a.operatoruser,a.operatordate "
						+ "from DT_ProductInfoPwdChangeLog a LEFT JOIN  dt_productinfo b on a.procode = b.code");
		sql.append(" where 1=1");
		if (!StringUtil.isEmpty(procode)) {
			sql.append(" and b.serialno  like '%" + procode.trim() + "%'");
		}
		sql.append(" order by a.id desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,
				ProductInfoPwdChangeLog.class, new Object[] {});
	}
}
