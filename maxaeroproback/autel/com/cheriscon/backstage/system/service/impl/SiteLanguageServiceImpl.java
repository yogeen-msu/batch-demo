package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.ISiteLanguageService;
import com.cheriscon.common.model.SiteLanguage;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

@Service
public class SiteLanguageServiceImpl extends BaseSupport<SiteLanguage> implements ISiteLanguageService {
	
	@Transactional
	public boolean saveLanguage(SiteLanguage language) {
		String tableName = DBUtils.getTableName(SiteLanguage.class);
		this.daoSupport.insert(tableName, language);
		return true;
	}

	@Transactional
	public boolean updateLanguage(SiteLanguage language) {
		String tableName = DBUtils.getTableName(SiteLanguage.class);
		this.daoSupport.update(tableName,language,"id="+language.getId());
		return true;
	}

	@Transactional
	public boolean delLanguageById(int id) {
		String tableName = DBUtils.getTableName(SiteLanguage.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	public SiteLanguage getById(int id){
		String tableName = DBUtils.getTableName(SiteLanguage.class);
		String sql = "select * from " + tableName + " where id=?";
		return this.daoSupport.queryForObject(sql, SiteLanguage.class, id);
	}

	public List<SiteLanguage> listLanguage() {
		String tableName = DBUtils.getTableName(SiteLanguage.class);
		StringBuffer sql = new StringBuffer("select s.* from "+ tableName +" s ");
		return this.daoSupport.queryForList(sql.toString(), SiteLanguage.class, new Object[]{});
	}
	
	public List<SiteLanguage> getByNewSiteLanguage(SiteLanguage siteLanguage){
		String tableName = DBUtils.getTableName(SiteLanguage.class);
		String sql = "select * from " + tableName +" where stateCode=? and languageCode=?";
		return this.daoSupport.queryForList(sql,SiteLanguage.class,new Object[]{siteLanguage.getStateCode().trim(),siteLanguage.getLanguageCode().trim()});
	}
}
