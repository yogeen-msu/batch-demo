package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IProductResetLogService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductRepairRecord;
import com.cheriscon.common.model.ProductResetLog;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
@Service
public class ProductResetLogServiceImpl extends BaseSupport<ProductResetLog> implements IProductResetLogService {

	@Override
	public void saveLog(ProductResetLog log) throws Exception {
		
		//重置的话，先要删除和用户产品关系表中的数据，然后删除有效期表，然后更新产品信息
		this.daoSupport.execute("delete from DT_CustomerProInfo where proCode=?", log.getProCode());
		this.daoSupport.execute("delete from DT_ProductSoftwareValidStatus where proCode=?", log.getProCode());
		this.daoSupport.execute("update DT_ProductInfo set regStatus=0,regTime='' where code=?", log.getProCode());
		
		String tableName = DBUtils.getTableName(ProductResetLog.class);
		log.setCode(DBUtils.generateCode(DBConstant.PRODUCT_RESET_CODE));
		this.daoSupport.insert(tableName, log);

	}

	@Override
	public Page pageProductReset(ProductResetLog log, int pageNo, int pageSize) {
		StringBuffer sql=new StringBuffer();
		sql.append("select a.code as code,b.serialNo as serialNo,a.resetUserName as resetUserName,a.resetDate as resetDate,a.regDate as regDate,a.oldUserCode as oldUserCode,a.validDate as validDate  from DT_ProductResetLog a  left join DT_ProductInfo b on a.proCode=b.code ");
		if(null!=log){
			if(!StringUtil.isEmpty(log.getSerialNo())){
				sql.append(" where b.serialNo like '%"+log.getSerialNo()+"%'");
			}
		}
		sql.append(" order by a.resetDate desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,ProductResetLog.class,new Object[]{});
		return page;
	}
	public ProductResetLog getProductReset(String code) throws Exception{
		
		StringBuffer sql=new StringBuffer();
		sql.append("select a.code as code,b.serialNo as serialNo,a.resetUser as resetUser,a.resetDate as resetDate,a.resetReason as resetReason,a.oldUserCode as oldUserCode,a.validDate as validDate  from DT_ProductResetLog a  left join DT_ProductInfo b on a.proCode=b.code");
		sql.append(" where a.code=?");
		return this.daoSupport.queryForObject(sql.toString(), ProductResetLog.class,code);
	}
	
	/**
	 * 查询列表
	 * **/
	public List<ProductResetLog> getProductResetByProCode(String proCode) throws Exception{
		String sql="select * from DT_ProductResetLog where proCode=?";
		return this.daoSupport.queryForList(sql, ProductResetLog.class, proCode);
	}
	
	/**
	 * 退换货系统调用
	 */
	public List<ProductResetLog> getProReset(ProductResetLog log) {
		StringBuffer sql=new StringBuffer();
		sql.append("select a.code as code,b.serialNo as serialNo,a.resetUserName as resetUserName,a.resetDate as resetDate,a.regDate as regDate,a.oldUserCode as oldUserCode,a.validDate as validDate  from DT_ProductResetLog a  left join DT_ProductInfo b on a.proCode=b.code ");
		if(null!=log){
			if(!StringUtil.isEmpty(log.getSerialNo())){
				sql.append(" where b.serialNo like '%"+log.getSerialNo()+"%'");
			}
		}
		sql.append(" order by a.resetDate desc");
		return this.daoSupport.queryForList(sql.toString(), ProductResetLog.class, new Object[]{});
	}
}
