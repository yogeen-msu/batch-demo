package com.cheriscon.backstage.system.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.ISaleContractAuthService;
import com.cheriscon.common.model.SaleContractAuth;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class SaleContractAuthServiceImpl extends BaseSupport<SaleContractAuth> implements ISaleContractAuthService {


	@Override
	public void addSaleContractAuth(SaleContractAuth auth) throws Exception {
		
		this.daoSupport.insert("DT_SaleContractAuth", auth);

	}

	@Override
	public Page getSaleContractAuthPage(SaleContractAuth auth, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append(" select b.id as id, a.code as saleContractCode,a.name as saleContractName,c.username as userName from dt_salecontract a ,dt_salecontractauth b,es_adminuser c");
		sql.append(" where a.code =b.salecontractCode ");
		sql.append(" and b.userId=c.userid ");
		if(auth!=null){
			if(auth.getSaleContractName()!=null){
				sql.append(" and a.name like '%").append(auth.getSaleContractName().trim()).append("%'");
			}
			if(auth.getUserName()!=null){
				sql.append(" and c.username like '%").append(auth.getUserName().trim()).append("%'");
			}
		}
		sql.append(" order by b.id desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}

	@Override
	public void delSaleContractAuthById(Integer id) throws Exception {

		this.daoSupport.execute("delete from DT_SaleContractAuth where id=?", id);
	}

	@Override
	public int getSaleContractAuth(SaleContractAuth auth) throws Exception {
		String sql="select count(1) from DT_SaleContractAuth where saleContractCode=? and userId=?";
		return this.daoSupport.queryForInt(sql, auth.getSaleContractCode(),auth.getUserId());
	}

}
