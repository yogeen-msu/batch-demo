package com.cheriscon.backstage.system.service.impl;

import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class LanguageServiceImpl extends BaseSupport<Language> implements ILanguageService {
	
	@Transactional
	public boolean saveLanguage(Language language) {
		String tableName = DBUtils.getTableName(Language.class);
		language.setCode(DBUtils.generateCode(DBConstant.LANGUAGE_CODE));
		this.daoSupport.insert(tableName, language);
		return true;
	}

	@Transactional
	public boolean updateLanguage(Language language) {
		String tableName = DBUtils.getTableName(Language.class);
		this.daoSupport.update(tableName,language,"id="+language.getId());
		return true;
	}

	@Transactional
	public boolean delLanguageById(int id) {
		String tableName = DBUtils.getTableName(Language.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	public Language getById(int id){
		String tableName = DBUtils.getTableName(Language.class);
		String sql = "select * from " + tableName + " where id=?";
		return this.daoSupport.queryForObject(sql, Language.class, id);
	}
	
	public Language getByCode(String code){
		String tableName = DBUtils.getTableName(Language.class);
		String sql = "select * from " + tableName + " where code=? ";
		return this.daoSupport.queryForObject(sql, Language.class,code);
	}
	
	public Language getByLocale(Locale locale){
		try {
			String tableName = DBUtils.getTableName(Language.class);
			String sql = "select * from " + tableName + " where countryCode=? and languageCode=? and isshow=1";
			List<Language> languageList = daoSupport.queryForList(sql, Language.class,locale.getCountry(),locale.getLanguage());
			if(languageList.size() != 0){
				return languageList.get(0);
			}else{
				locale = new Locale("zh","CN");	//查询中文
				languageList = daoSupport.queryForList(sql, Language.class,locale.getCountry(),locale.getLanguage());
				return languageList.get(0);
			}
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public List<Language> queryLanguage() {
		String tableName = DBUtils.getTableName(Language.class);
		String sql = "select lan.* from " + tableName +" lan where isshow=1 order by id asc";
		return this.daoSupport.queryForList(sql, Language.class, new Object[]{});
	}
	
	public List<Language> queryAllLanguage(){
		String tableName = DBUtils.getTableName(Language.class);
		String sql = "select lan.* from " + tableName +" lan where 1=1 order by id asc";
		return this.daoSupport.queryForList(sql, Language.class, new Object[]{});	
	}
	
	public List<Language> queryNotSelectLanguage(){
		String tableName = DBUtils.getTableName(Language.class);
		//String cdTble = DBUtils.getTableName(LanguageConfigDetail.class);
		StringBuffer sql = new StringBuffer();
		sql.append("select tbl.* from ").append(tableName+" tbl");
		/*sql.append(" where code not in( ");
		sql.append(" select cd.languageCode from ").append(cdTble+" cd group by languageCode");
		sql.append(" ) order by id asc");*/
		sql.append(" order by id asc");
		return this.daoSupport.queryForList(sql.toString(),Language.class, new Object[]{});
	}

	public Page pageLanguage(int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(Language.class);
		String sql = "select lan.* from " + tableName +" lan order by id asc";
		Page page = this.daoSupport.queryForPage(sql, pageNo, pageSize,Language.class,new Object[]{});
		return page;
	}

	public List<Language> getByNewLanguage(Language language){
		String tableName = DBUtils.getTableName(Language.class);
		String sql = "select * from " + tableName +" where name=?";
		return this.daoSupport.queryForList(sql,Language.class,new Object[]{language.getName().replace(" ","")});
	}

	@Override
	public void updateIsShowState(int id, int isShow)
	{
		String sql ="update DT_Language set isshow="+isShow+" where id="+id;
		this.daoSupport.execute(sql);
	}

	/**
	 * A16043 20160523  通过语言配置Code查找对应的基础语言信息,由于此接口是自己调用，未对外公开，故可以规避sql注入风险
	 * @param languageConfigCode
	 * @return
	 */
	@Override
	public List<Language> queryByLanguageCfgCode(String languageConfigCode) {
		 String sql="select f.* from dt_language f ,dt_languageconfigdetail t where f.code = t.languageCode  and t.languageCfgCode='"+languageConfigCode+"'";
		 return this.daoSupport.queryForList(sql, Language.class, new Object[]{});
	}
}
