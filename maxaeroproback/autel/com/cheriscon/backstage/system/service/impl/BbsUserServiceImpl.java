package com.cheriscon.backstage.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.IBbsUserService;
import com.cheriscon.common.model.BbsUser;
import com.cheriscon.cop.sdk.database.BaseSupport;

@Service
public class BbsUserServiceImpl extends BaseSupport<BbsUser> implements
		IBbsUserService {

	@Transactional
	public void add(BbsUser bbsUser) throws Exception {
		this.daoSupport.insert("tbl_bbs_user", bbsUser);
	}

}
