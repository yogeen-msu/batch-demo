package com.cheriscon.backstage.system.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.app.base.core.service.auth.impl.AdminUserManagerImpl;
import com.cheriscon.backstage.system.service.IAdminUserAutelService;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.database.Page;



/**
 * @remark 管理员信息service实现
 * @author pengdongan
 * @date 2013-01-30
 * 
 */
@Service
public class AdminUserAutelServiceImpl extends AdminUserManagerImpl implements IAdminUserAutelService {



	/**
	  * 管理员信息分页显示方法
	  * @param adminUser
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageAdminUserpage(AdminUser adminUser, int pageNo, int pageSize){
		StringBuffer sql = new StringBuffer();

		sql.append("select * from es_adminuser");
		sql.append(" where 1=1");
		
		if (null != adminUser) {
			if (StringUtils.isNotBlank(adminUser.getUsername())) {
				sql.append(" and username like '%");
				sql.append(adminUser.getUsername().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(adminUser.getRealname())) {
				sql.append(" and realname like '%");
				sql.append(adminUser.getRealname().trim());
				sql.append("%'");
			}
		}

		sql.append(" order by userid desc");
			Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
					pageSize, AdminUser.class);
			return page;
	}
}
