package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IProductChangeNoLogService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ProductChangeNoLog;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class ProductChangeNoLogServiceImpl extends BaseSupport<ProductChangeNoLog>  implements IProductChangeNoLogService {

	@Override
	public void saveLog(ProductChangeNoLog log) throws Exception {
		//1.先将基础信息存入日志表
		String code =DBUtils.generateCode(DBConstant.PRODUCT_CHANGE_CODE);
		log.setCode(code);
		this.daoSupport.insert("DT_ProductChangeNoLog", log);
		//2.删除原产品信息
		this.daoSupport.execute("delete from DT_CustomerProInfo where proCode=?", log.getOldProCode());
		this.daoSupport.execute("delete from DT_ProductSoftwareValidStatus where proCode=?", log.getOldProCode());
		this.daoSupport.execute("update DT_ProductInfo set regStatus=0,regTime='' where code=?", log.getOldProCode());
	}

	@Override
	public Page getProductContractChangeNoLogs(ProductChangeNoLog productChangeNoLog, int pageNo,
			int pageSize) throws Exception {
		
		StringBuffer sql=new StringBuffer();
		sql.append("select plog.id, plog.code,cfo.autelid as customercode,opfo.serialno as oldprocode,npfo.serialno as newprocode,"
				+ " plog.validdate,plog.regdate,plog.createdate,sfo.autelid as createuser,plog.createip "
				+ " from DT_ProductChangeNoLog plog "
				+" LEFT JOIN dt_customerinfo cfo on plog.customercode=cfo.code "
				+" LEFT JOIN dt_productinfo opfo on plog.oldprocode=opfo.code "
				+" LEFT JOIN dt_productinfo npfo on plog.newprocode=npfo.code "
				+" LEFT JOIN dt_sealerinfo sfo  on plog.createuser=sfo.code") ;
		
		/*sql.append("select plog.id, plog.code, "
			+ " (select autelid from dt_customerinfo where code=plog.customercode ) as customercode, "
			+ " (select serialno from dt_productinfo where code=plog.oldprocode) as oldprocode, "
			+ " (select serialno from dt_productinfo where code=plog.newprocode) as newprocode,"
			+ " plog.validdate,plog.regdate,plog.createdate, "
			+ " (select autelid from dt_sealerinfo where code=plog.createuser) as createuser,"
			+ " plog.createip from DT_ProductChangeNoLog plog");*/
		
		sql.append(" where 1=1 ");
		
		if(productChangeNoLog!=null){
			if(!StringUtil.isEmpty(productChangeNoLog.getCustomerCode())){
				sql.append(" and cfo.autelid like '%"+productChangeNoLog.getCustomerCode().trim()+"%' ");
			}
			if(!StringUtil.isEmpty(productChangeNoLog.getOldProCode())){
				sql.append(" and opfo.serialno like '%"+productChangeNoLog.getOldProCode().trim()+"%' ");
			}
			if(!StringUtil.isEmpty(productChangeNoLog.getNewProCode())){
				sql.append(" and npfo.serialno like '%"+productChangeNoLog.getNewProCode().trim()+"%' ");
			}
		}
		sql.append(" order by plog.id desc ");
		
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,ProductChangeNoLog.class,new Object[]{});
	}
	
	public List<ProductChangeNoLog> queryProChangeNologs(ProductChangeNoLog productChangeNoLog) throws Exception {
		
		StringBuffer sql=new StringBuffer();
		sql.append("select plog.id, plog.code,cfo.autelid as customercode,opfo.serialno as oldprocode,npfo.serialno as newprocode,"
				+ " plog.validdate,plog.regdate,plog.createdate,sfo.autelid as createuser,plog.createip "
				+ " from DT_ProductChangeNoLog plog "
				+" LEFT JOIN dt_customerinfo cfo on plog.customercode=cfo.code "
				+" LEFT JOIN dt_productinfo opfo on plog.oldprocode=opfo.code "
				+" LEFT JOIN dt_productinfo npfo on plog.newprocode=npfo.code "
				+" LEFT JOIN dt_sealerinfo sfo  on plog.createuser=sfo.code") ;
		sql.append(" where 1=1 ");
		
		if(productChangeNoLog!=null){
			if(!StringUtil.isEmpty(productChangeNoLog.getCustomerCode())){
				sql.append(" and cfo.autelid like '%"+productChangeNoLog.getCustomerCode().trim()+"%' ");
			}
			if(!StringUtil.isEmpty(productChangeNoLog.getOldProCode())){
				sql.append(" and opfo.serialno like '%"+productChangeNoLog.getOldProCode().trim()+"%' ");
			}
			if(!StringUtil.isEmpty(productChangeNoLog.getNewProCode())){
				sql.append(" and npfo.serialno like '%"+productChangeNoLog.getNewProCode().trim()+"%' ");
			}
		}
		sql.append(" order by plog.id desc ");
		
		return this.daoSupport.queryForList(sql.toString(), ProductChangeNoLog.class, new Object[]{});
	}

}
