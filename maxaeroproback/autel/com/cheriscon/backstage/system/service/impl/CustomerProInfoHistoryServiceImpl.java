package com.cheriscon.backstage.system.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.ICustomerProInfoHistoryService;
import com.cheriscon.common.model.CustomerProInfo;
import com.cheriscon.common.model.CustomerProInfoHistory;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;

@Service
public class CustomerProInfoHistoryServiceImpl extends
		BaseSupport implements
		ICustomerProInfoHistoryService {

	@Transactional
	public void add(CustomerProInfoHistory customerProInfoHistory)
			throws Exception {
		customerProInfoHistory.setCreateDate(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		this.daoSupport.insert("DT_CustomerProInfoHistory", customerProInfoHistory);
	}

	@Override
	@Transactional
	public void rebanding(Integer id,String updateTime,String updateUser) throws Exception {
		
		try{
			String sql="select * from DT_CustomerProInfoHistory where id=?";
			CustomerProInfoHistory his=(CustomerProInfoHistory) this.daoSupport.queryForObject(sql, CustomerProInfoHistory.class, id);
			
			CustomerProInfo proInfo =new CustomerProInfo();
			proInfo.setCustomerCode(his.getCustomerCode());
			proInfo.setProCode(his.getProCode());
			this.daoSupport.insert("DT_CustomerProInfo", proInfo);
			
			String updateSql="update DT_CustomerProInfoHistory set updateDate='"+updateTime+"',updateUser='"+updateUser+"',status=1 where id="+id;
			this.daoSupport.execute(updateSql);
			
			String updateSql2="update DT_ProductInfo set regstatus=1 where code='"+his.getProCode()+"'";
			this.daoSupport.execute(updateSql2);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
