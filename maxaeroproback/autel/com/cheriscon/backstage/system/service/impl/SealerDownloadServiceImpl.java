package com.cheriscon.backstage.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.cheriscon.backstage.system.service.ISealerDownloadService;
import com.cheriscon.common.model.SealerDownload;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;

@Service
public class SealerDownloadServiceImpl extends BaseSupport<SealerDownload>
		implements ISealerDownloadService {

	@Override
	public Page pageSealerDownload(SealerDownload sealerDownload, int pageNo,
			int pageSize) {
		String tableName = DBUtils.getTableName(SealerDownload.class);
		String sql = "select * from " + tableName + " where 1=1 ";
		List<Object> list = new ArrayList<Object>();
		if(null != sealerDownload){
			if(StringUtils.isNotEmpty(sealerDownload.getName())){
				sql += " and name like ?";
				list.add("%"+sealerDownload.getName()+"%");
			}
		}
		return this.daoSupport.queryForPage(sql, pageNo, pageSize, list.toArray());
	}
	
	@Override
	public List<SealerDownload> getSealerDownloadFiles() {
		String tableName = DBUtils.getTableName(SealerDownload.class);
		String sql = "select type from " + tableName + " group by type";
		return this.daoSupport.queryForList(sql, SealerDownload.class);
	}
	
	@Override
	public List<SealerDownload> getSealerDownloadList(String fileType) {
		String tableName = DBUtils.getTableName(SealerDownload.class);
		String sql = "select * from " + tableName + " where type = ?";
		return this.daoSupport.queryForList(sql, SealerDownload.class, fileType);
	}
	
	@Override
	public SealerDownload getSealerDownload(int id) {
		String tableName = DBUtils.getTableName(SealerDownload.class);
		String sql = "select * from " + tableName + " where id = ?";
		return this.daoSupport.queryForObject(sql, SealerDownload.class, id);
	}

	@Transactional
	public void saveSealerDownload(SealerDownload sealerDownload)
			throws Exception {
		String tableName = DBUtils.getTableName(SealerDownload.class);
		sealerDownload.setReleaseDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
		this.daoSupport.insert(tableName, sealerDownload);
	}

	@Transactional
	public void updateSealerDownload(SealerDownload sealerDownload)
			throws Exception {
		String tableName = DBUtils.getTableName(SealerDownload.class);
		sealerDownload.setReleaseDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
		this.daoSupport.update(tableName, sealerDownload, "id="+sealerDownload.getId());
	}

	@Transactional
	public void deleteSealerDownload(int id) throws Exception {
		String tableName = DBUtils.getTableName(SealerDownload.class);
		String sql = "delete from " + tableName +" where id = ?";
		this.daoSupport.execute(sql, id);
	}
}
