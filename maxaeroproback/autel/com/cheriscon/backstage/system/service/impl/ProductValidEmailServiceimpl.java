package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IProductValidEmailService;
import com.cheriscon.common.model.EmailLog;
import com.cheriscon.common.model.ProductValidEmail;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

@Service
public class ProductValidEmailServiceimpl extends BaseSupport<ProductValidEmail> implements IProductValidEmailService {

	@Override
	public void saveProductValidEmail(ProductValidEmail productValidEmail)
			throws Exception {
		this.daoSupport.insert("DT_ProductValidEmail", productValidEmail);
	}
}
