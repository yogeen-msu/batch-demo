package com.cheriscon.backstage.system.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.model.AutelCheckVo;
import com.cheriscon.backstage.system.service.IAutelCheckService;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.util.StringUtil;

@Service
public class AutelCheckServiceImpl extends BaseSupport<AutelCheckVo> implements IAutelCheckService {

	/**
	  * 获取今日订单数量，金额
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public  AutelCheckVo getTodayMoney() throws Exception{
		StringBuffer sql=new StringBuffer();
		String nowDate=DateUtil.toString(new Date(), "YYYY-MM-dd");
		sql.append("select count(id) as ordersNum ,sum(orderMoney) as ordersMoney from dt_orderInfo where orderPayState=1");
		sql.append(" and payDate>='"+nowDate+"'");
		
		return this.daoSupport.queryForObject(sql.toString(), AutelCheckVo.class);
	}
	
	/**
	  * 获取订单数量，金额
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public  AutelCheckVo getTotalMoney() throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select count(id) as ordersNum ,sum(orderMoney) as ordersMoney from dt_orderInfo where orderPayState=1");
		return this.daoSupport.queryForObject(sql.toString(), AutelCheckVo.class);
	}

	/**
	  * 获取每年中每个月订单数量，金额
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public List<AutelCheckVo> getMoneyByYear(String year) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select MONTH(payDate) as payDate, count(1) as ordersNum,sum(orderMoney) as ordersMoney from dt_orderInfo ");
		sql.append(" where orderPayState=1");
		sql.append(" and payDate>='"+year+"'");
		sql.append(" group by MONTH(payDate)");
		return this.daoSupport.queryForList(sql.toString(), AutelCheckVo.class);
	}
	public List<AutelCheckVo> getCardUseNum(int useFlag) throws Exception{

		StringBuffer sql= new StringBuffer();
		sql.append("select b.name as reChargeCardTypeName,count(1) as totalNum from dt_rechargecardinfo a ,dt_rechargecardtype b");
		sql.append(" where a.reChargeCardTypeCode=b.code");
		sql.append(" and a.isUse=? ");
		sql.append(" group by b.name");
		
		return this.daoSupport.queryForList(sql.toString(), AutelCheckVo.class, useFlag);
	}
	
	
	public int getCardIsUseNum(int useFlag) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select count(1) as TotalNum from dt_rechargecardinfo where isuse=? group by isuse");
		return this.daoSupport.queryForInt(sql.toString(),useFlag);
	}
	public List<AutelCheckVo> getProductNum(AutelCheckVo autelCheck) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select b.name as productName ,count(1) as productNum from dt_productinfo a ,dt_productforsealer b ");
		sql.append(" where a.proTypeCode=b.`code` ");
		if(autelCheck!=null && !StringUtil.isEmpty(autelCheck.getProRegStatus())){
			sql.append(" and a.regStatus='").append(autelCheck.getProRegStatus().trim()).append("'");
		}
		sql.append(" group by b.name");
		return this.daoSupport.queryForList(sql.toString(), AutelCheckVo.class);
	}
	public int  getProductStatus(int status) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select count(id) as productNum from dt_productinfo where regstatus=?");
		return this.daoSupport.queryForInt(sql.toString(), status);
	}
	public int getProductValid() throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select count(1) from dt_productinfo p where EXISTS( ");
		sql.append("select 1 from dt_productsoftwarevalidstatus a where a.proCode =p.`code`");
		sql.append("and a.validdate<?) order by serialno");
		String nowDate=DateUtil.toString(new Date(), "yyyy-MM-dd");
		return this.daoSupport.queryForInt(sql.toString(), nowDate);
	}
	public List<AutelCheckVo> getProductNumByArea(AutelCheckVo autelCheck) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select c.name as productName ,count(1) as productNum from dt_productinfo a ,dt_salecontract b,dt_areaconfig c ");
		sql.append(" where a.saleContractCode=b.`code` ");
		sql.append(" and b.areaCfgCode=c.`code` ");
		if(autelCheck!=null && !StringUtil.isEmpty(autelCheck.getProType())){
			sql.append(" and a.proTypeCode='").append(autelCheck.getProType().trim()).append("'");
		}
		if(autelCheck!=null && !StringUtil.isEmpty(autelCheck.getProRegStatus())){
			sql.append(" and a.regStatus='").append(autelCheck.getProRegStatus().trim()).append("'");
		}
		
		sql.append(" group by c.name");
		return this.daoSupport.queryForList(sql.toString(), AutelCheckVo.class);
		}
	public List<AutelCheckVo> getProductNumByLanguage(AutelCheckVo autelCheck) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select replace(c.name,'&','+') as productName ,count(1) as productNum from dt_productinfo a ,dt_salecontract b,dt_languageconfig c ");
		sql.append(" where a.saleContractCode=b.`code` ");
		sql.append(" and b.languageCfgCode=c.`code` ");
		if(autelCheck!=null && !StringUtil.isEmpty(autelCheck.getProType())){
			sql.append(" and a.proTypeCode='").append(autelCheck.getProType().trim()).append("'");
		}
		if(autelCheck!=null && !StringUtil.isEmpty(autelCheck.getProRegStatus())){
			sql.append(" and a.regStatus='").append(autelCheck.getProRegStatus().trim()).append("'");
		}
		sql.append(" group by c.name");
		return this.daoSupport.queryForList(sql.toString(), AutelCheckVo.class);
		
	}
}
