package com.cheriscon.backstage.system.service.impl;


import java.util.List;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class SealerDataAuthDeailsServiceImpl extends BaseSupport<SealerDataAuthDeails> implements
		ISealerDataAuthDeailsService {

	@Override
	public SealerDataAuthDeails getDataAuth(String authCode) throws Exception {
		String sql="select * from DT_SealerDataAuthDeails where authCode=? limit 0,1";
		return this.daoSupport.queryForObject(sql, SealerDataAuthDeails.class, authCode);
	}
	public SealerDataAuthDeails getDataAuthBySealerCode(String sealerCode) throws Exception{
		String sql="select * from DT_SealerDataAuthDeails where sealerCode=? limit 0,1";
		return this.daoSupport.queryForObject(sql, SealerDataAuthDeails.class, sealerCode);
	}
	
	
	public List<SealerDataAuthDeails>  getAuthSealerList(String authCode,String sealerCode) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("select a.sealerCode as sealerCode ,b.name as sealerName ");
		sql.append(" from DT_SealerDataAuthDeails a,dt_sealerInfo b ");
		sql.append(" where a.sealerCode=b.autelId ");
		sql.append(" and a.authCode like '"+authCode+"%'  ");
		sql.append(" and a.sealerCode!='"+sealerCode+"'  ");
		sql.append(" and b.status=1;") ;
		return this.daoSupport.queryForList(sql.toString(), SealerDataAuthDeails.class);
	}

	public Page getAuthSealer(String sealerName,String authCode,String sealerCode,int pageNo, int pageSize) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("select a.sealerCode as sealerCode ,b.name as sealerName ,");
		sql.append(" (select count(1) from DT_CustomerComplaintInfo c where c.acceptor=b.autelId and c.complaintState=1) as openNum,");
		sql.append(" (select count(1) from DT_CustomerComplaintInfo c where c.acceptor=b.autelId and c.complaintState=4) as closeNum");		
		sql.append(" from DT_SealerDataAuthDeails a,dt_sealerInfo b  ");
		sql.append(" where a.sealerCode=b.autelId and a.authCode like '"+authCode+"%' and a.sealerCode!='"+sealerCode+"'");
		if(!StringUtil.isBlank(sealerName)){
			sql.append(" and a.sealerCode like '%"+sealerName+"%'");
		}
		sql.append(" and b.status=1");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}
	
	public void insertAuth(SealerDataAuthDeails auth){
		this.daoSupport.insert("DT_SealerDataAuthDeails", auth);
	}
}
