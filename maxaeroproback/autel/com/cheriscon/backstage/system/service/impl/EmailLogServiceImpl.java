package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IEmailLogService;
import com.cheriscon.common.model.EmailLog;
import com.cheriscon.common.model.ToolDownload;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class EmailLogServiceImpl extends BaseSupport<EmailLog> implements
		IEmailLogService {

	@Override
	public void saveEmailLog(EmailLog log) throws Exception {
		this.daoSupport.insert("DT_EmailLog", log);
	}

	@Override
	public void updateEmailLog(EmailLog log) throws Exception {
		this.daoSupport.update("DT_EmailLog", log, "id=" + log.getId());
	}

	public List<EmailLog> listEmailLog() throws Exception{
		String sql="select a.* from DT_EmailLog a where countNum>0 and countNum<6";
		return this.daoSupport.queryForList(sql, EmailLog.class);
	}
	
	public Page getEmailLogs(EmailLog emailLog, int pageNo,int pageSize) throws Exception{
		StringBuffer sql= new StringBuffer("select a.* from DT_EmailLog a ");
		sql.append("where 1=1");
		if(emailLog!=null)
		{
			if(StringUtils.isNotEmpty(emailLog.getToUser())){	 
				sql.append(" and a.toUser like '%"+emailLog.getToUser().trim()+"%' ");	
			}
		}
		sql.append(" order by a.id desc ,a.countnum desc ");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,EmailLog.class,new Object[]{});
	}
}
