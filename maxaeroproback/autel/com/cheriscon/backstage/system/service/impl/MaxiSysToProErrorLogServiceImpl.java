package com.cheriscon.backstage.system.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IMaxiSysToProErrorLogService;
import com.cheriscon.common.model.MaxiSysToProErrorLog;
import com.cheriscon.cop.sdk.database.BaseSupport;

@Service
public class MaxiSysToProErrorLogServiceImpl extends BaseSupport<MaxiSysToProErrorLog> implements IMaxiSysToProErrorLogService {

	public MaxiSysToProErrorLogServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void insertLog(MaxiSysToProErrorLog log) {
        this.daoSupport.insert("dt_MaxiSysToProErrorLog", log);
	}

}
