package com.cheriscon.backstage.system.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.service.IProductLockService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class ProductLockServiceImpl extends BaseSupport<ProductInfo> implements
		IProductLockService {

	@Override
	public Page getProductLocks(String serialNO, int pageNo, int pageSize)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select chy.id,c.autelId as customerCode,pfo.serialno as serialNo,pfo.noregreason as noRegReason,pfo.noRegDate,pfo.createUser "
				+ " from DT_ProductInfo pfo LEFT JOIN DT_CustomerProInfoHistory chy on pfo.code=chy.proCode left join dt_customerinfo c on chy.customerCode=c.code");
		sql.append(" where pfo.regstatus='2' ");
		if (!StringUtil.isEmpty(serialNO)) {
			sql.append(" and pfo.serialno like '%" + serialNO.trim() + "%'");
		}
		sql.append(" order by pfo.noRegDate desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, ProductInfo.class, new Object[] {});
		return page;
	}

	@Transactional
	public void addProductLock(ProductInfo productInfo) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append(" update dt_productinfo set regstatus='2' ,noRegReason=? ,noRegDate=?,"
				+ " createUser=? where serialno =? ");

		this.daoSupport.execute(sql.toString(), productInfo.getNoRegReason(),
				DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"),
				productInfo.getCreateUser(), productInfo.getSerialNo());
	}
}
