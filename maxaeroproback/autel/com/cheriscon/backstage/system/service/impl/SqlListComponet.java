package com.cheriscon.backstage.system.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.IDBRouter;

@Component
public  class SqlListComponet extends BaseSupport {
	@Resource
	private IDBRouter baseDBRouter;
	
	@Resource private ILanguageService languageService;
	
	public List list(String sql,String localParam ,String languageCode){
		if(StringUtils.isNotEmpty(localParam)){	//本地语言不为空
			//增加语言环境查询参数
			Language languages = (Language)CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
			 sql = filterSql(sql);
			Locale locale = null;

			if (languages != null) {
				locale = new Locale(languages.getLanguageCode(),
						languages.getCountryCode());
			} else {
				locale = CopContext.getDefaultLocal();
			}
			
			Language language = languageService.getByCode(languageCode);
			if(language != null){
				if(sql.indexOf("where") != -1){
					String [] tsql = sql.split("where");
					sql =tsql[0]+" where "+ (localParam +" = '"+language.getCode()+"'") + " and " + tsql[1];
				}else{
					String pattern = "order(\\s*)by";
					Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
					Matcher m = p.matcher(sql);
					if(m.find()) {
						String orderBy  = m.group(0);
						String [] tsql = sql.split(orderBy);
						sql = tsql[0]+" where "+ (localParam +" = '"+language.getCode()+"'") + " " + orderBy + " " + tsql[1];
					}else{
						sql += " where " + (localParam +" = '"+language.getCode()+"'");
					}
				}
			}
		}
		
		List list = this.daoSupport.queryForList(sql);
		return list;
	}

	private String filterSql(String sql ){
		 
		String pattern = "#(.*?)#";
 
		Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
		Matcher m = p.matcher(sql);
		while(m.find()) {
				String tb  = m.group(0);
				 
				String newTb  = tb.replaceAll("#", "");
				newTb = this.baseDBRouter.getTableName(newTb);
				sql = sql.replaceAll(tb, newTb);
		}		
		return sql;
	}
}
