package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IProductContractService;
import com.cheriscon.common.model.UpWarningInfo;
import com.cheriscon.cop.sdk.database.BaseSupport;
@Service
public class ProductContractServiceImpl extends BaseSupport implements IProductContractService {

	@Override
	public List<UpWarningInfo> queryList() {
		StringBuffer sql=new StringBuffer("");
		sql.append("select * from t_upWarningInfo where flag=1 and curArea='acf_North_America' and updateNum>5  ");
		return this.daoSupport.queryForList(sql.toString(),UpWarningInfo.class );
	}

	@Override
	public boolean updateFlag(Integer id) {
		String sql="update t_upWarningInfo set flag=0 where warningId=?";
		this.daoSupport.execute(sql, id);
		return true;
	}

}
