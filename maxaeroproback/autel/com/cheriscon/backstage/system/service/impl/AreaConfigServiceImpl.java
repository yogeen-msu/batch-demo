package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.system.mapper.AreaConfigMapper;
import com.cheriscon.backstage.system.service.IAreaConfigService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.AreaConfigDetail;
import com.cheriscon.common.model.AreaInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class AreaConfigServiceImpl extends BaseSupport<AreaConfig> implements IAreaConfigService {
	
	@Transactional
	public boolean saveAreaConfig(AreaConfig areaConfig) {
		
		//语言配置
		String tableName = DBUtils.getTableName(AreaConfig.class);
		areaConfig.setCode(DBUtils.generateCode(DBConstant.AREAINFO_CONFIG_CODE));
		this.daoSupport.insert(tableName, areaConfig);
		
		//配置言详情
		String detailTableName = DBUtils.getTableName(AreaConfigDetail.class);
		if(areaConfig.getAreaInfoList() != null){
			for (AreaInfo areaInfo : areaConfig.getAreaInfoList()) {
				AreaConfigDetail detail = new AreaConfigDetail();
				detail.setAreaCfgCode(areaConfig.getCode());
				detail.setAreaCode(areaInfo.getCode());
				this.daoSupport.insert(detailTableName, detail);
			}
		}
		
		return true;
	}

	@Transactional
	public boolean updateAreaConfig(AreaConfig areaConfig) {
		String tableName = DBUtils.getTableName(AreaConfig.class);
		this.daoSupport.update(tableName,areaConfig,"id="+areaConfig.getId());
		
		//配置言详情    删除原来的，增加现有的
		String detailTableName = DBUtils.getTableName(AreaConfigDetail.class);
		StringBuffer delDetailSql = new StringBuffer("delete from "+ detailTableName);
		delDetailSql.append(" where areaCfgCode=?");
		this.daoSupport.execute(delDetailSql.toString(),areaConfig.getCode());
		
		if(areaConfig.getAreaInfoList() != null){
			for (AreaInfo areaInfo : areaConfig.getAreaInfoList()) {
				AreaConfigDetail detail = new AreaConfigDetail();
				detail.setAreaCfgCode(areaConfig.getCode());
				detail.setAreaCode(areaInfo.getCode());
				this.daoSupport.insert(detailTableName, detail);
			}
		}
		
		return true;
	}

	@Transactional
	public boolean delAreaConfigById(int id) {
		
		AreaConfig config = getById(id);
		//删除配置言详情 项
		String detailTableName = DBUtils.getTableName(AreaConfigDetail.class);
		StringBuffer delDetailSql = new StringBuffer("delete from "+ detailTableName);
		delDetailSql.append(" where areaCfgCode=?");
		this.daoSupport.execute(delDetailSql.toString(),config.getCode());
		
		//删除配置言详情
		String tableName = DBUtils.getTableName(AreaConfig.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public AreaConfig getById(int id){
		String tableName = DBUtils.getTableName(AreaConfig.class);
		String sql = "select * from " + tableName + " where id=?";
		AreaConfig config = this.daoSupport.queryForObject(sql, AreaConfig.class, id);
		
		String areaConfigDetailTbn = DBUtils.getTableName(AreaConfigDetail.class);
		String areaInfoTbn  = DBUtils.getTableName(AreaInfo.class);
		StringBuffer configItemBuffer = new StringBuffer("select");
		configItemBuffer.append(" acd.*,aif.id aid,aif.code acode,aif.name aname ");
		configItemBuffer.append(" from " + areaConfigDetailTbn +" acd ");
		configItemBuffer.append(" left join " + areaInfoTbn+" aif on acd.areaCode=aif.code " );
		configItemBuffer.append(" where acd.areaCfgCode='"+config.getCode()+"'");
		List areaInfoList = this.daoSupport.queryForList(configItemBuffer.toString(),new AreaConfigMapper());
		config.setAreaInfoList(areaInfoList);
		
		return config;
	}

	public Page pageAreaConfig(AreaConfig areaConfig, int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(AreaConfig.class);
		String select = "select acd.* from " + tableName +" acd ";
		StringBuffer term = new StringBuffer();
		
		if(areaConfig != null && !StringUtil.isEmpty(areaConfig.getName())){
			if(term.length()>0){
				term.append(" and ");
			}else{
				term.append(" where ");
			}
			term.append(" acd.name like '%"+areaConfig.getName().trim()+"%' ");
		}
		StringBuffer sql = new StringBuffer(select);
		sql.append(term);
		sql.append(" order by id asc ");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,AreaConfig.class,new Object[]{});
		return page;
	}

	/**
	  * <p>Title: listAll</p>
	  * <p>Description: </p>
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<AreaConfig> listAll() throws Exception {
		String tableName = DBUtils.getTableName(AreaConfig.class);
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(tableName).append(" order by name asc ");
		return this.daoSupport.queryForList(sql.toString(), AreaConfig.class);
	}
	
	public List<AreaConfig> getByNewAreaConfig(AreaConfig areaConfig){
		String tableName = DBUtils.getTableName(AreaConfig.class);
		String sql = "select * from " + tableName +" ain where name=?";
		return this.daoSupport.queryForList(sql,AreaConfig.class,new Object[]{areaConfig.getName().replace(" ","")});
	}

}
