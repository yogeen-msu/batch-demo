package com.cheriscon.backstage.system.service.impl;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IUpLoginIpInfoService;
import com.cheriscon.common.model.UpLoginIpInfo;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@Service
public class UpLoginIpInfoServiceImpl extends BaseSupport<UpLoginIpInfo> implements IUpLoginIpInfoService {

	@Override
	public Page pageUpLoginIpInfo(UpLoginIpInfo info, int pageNo, int pageSize) {
		
		StringBuffer sql=new StringBuffer();
		sql.append("select a.productSn as productSn,a.loginStampStr as loginStampStr,a.ipStr as ip,d.country as ipStr,c.name as saleContract,c.code as saleContractCode from t_upLoginIpInfo a ");
		sql.append(" left outer join t_upCountryInfo d on a.countryId=d.countryId ,");
		sql.append(" dt_productinfo b,DT_SaleContract c where a.productSn=b.serialNo");
		sql.append(" and b.saleContractCode=c.code");
		if(!StringUtil.isBlank(info.getProductSn())){
			sql.append(" and a.productSn like'%").append(info.getProductSn().trim()).append("%'");
		}
       sql.append(" order by a.ipInforId desc ");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,UpLoginIpInfo.class,new Object[]{});
		return page;
	}

}
