package com.cheriscon.backstage.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IProductToolsService;
import com.cheriscon.common.model.ProductTypeTools;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * 工具下载业务逻辑实现类
 * 
 * @author yangpinggui 2013-1-20
 * 
 */
@Service
public class ProductToolsServiceImpl extends BaseSupport<ProductTypeTools>
		implements IProductToolsService {


	
	@Override
	public List<ProductTypeTools> queryProductToolList(final String languageCode) throws Exception {
		StringBuffer querySql = new StringBuffer();

		querySql.append(
				"select d.toolName,a.name as productTypeName,d.lastVersion,d.releaseDate,")
				.append(" d.downloadPath from dt_productForSealer a, dt_productTypeTools b,dt_tooldownload d")
				.append(" where a.code = b.productTypeCode and d.code = b.toolsCode order by d.releaseDate desc");
		
		
		return this.daoSupport.queryForList(querySql.toString(),ProductTypeTools.class);
	}
	


}
