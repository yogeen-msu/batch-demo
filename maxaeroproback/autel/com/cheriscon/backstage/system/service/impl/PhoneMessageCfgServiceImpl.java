package com.cheriscon.backstage.system.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.system.service.IPhoneMessageCfgService;
import com.cheriscon.common.model.PhoneMessageCfg;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;

@Service
public class PhoneMessageCfgServiceImpl extends BaseSupport<PhoneMessageCfg> implements IPhoneMessageCfgService {
	
	@Transactional
	public boolean savePhoneMessageCfg(PhoneMessageCfg phoneMessageCfg) {
		String tableName = DBUtils.getTableName(PhoneMessageCfg.class);
		if(phoneMessageCfg.getStatus() == null){
			phoneMessageCfg.setStatus(CTConsatnt.CT_STATUS_NOT_USE);
		}
		this.daoSupport.insert(tableName, phoneMessageCfg);
		return true;
	}

	@Transactional
	public boolean updatePhoneMessageCfg(PhoneMessageCfg phoneMessageCfg) {
		String tableName = DBUtils.getTableName(PhoneMessageCfg.class);
		PhoneMessageCfg template = getById(phoneMessageCfg.getId());
		template.setName(phoneMessageCfg.getName());
		template.setBiref(phoneMessageCfg.getBiref());
		template.setConfig(phoneMessageCfg.getConfig());
		template.setStatus(phoneMessageCfg.getStatus());
		if(template.getStatus().intValue() == CTConsatnt.CT_STATUS_USE){
			template.setUseTime(DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss"));
		}
		
		this.daoSupport.update(tableName,phoneMessageCfg,"id="+phoneMessageCfg.getId());
		return true;
	}
	

	@Transactional
	public int delPhoneMessageCfgById(int id) {
		String tableName = DBUtils.getTableName(PhoneMessageCfg.class);
		PhoneMessageCfg template = getById(id);
		if(template.getStatus() == CTConsatnt.CT_STATUS_USE){	//下在使用不能删除
			return CTConsatnt.CT_IS_USE;
		}
		
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		
		this.daoSupport.execute(sql.toString(),id);
		return CTConsatnt.CT_SUCCESS;
	}
	
	public PhoneMessageCfg getById(int id){
		String tableName = DBUtils.getTableName(PhoneMessageCfg.class);
		String sql = "select * from " + tableName + " where id=?";
		return this.daoSupport.queryForObject(sql, PhoneMessageCfg.class, id);
	}
	
	public PhoneMessageCfg getUseConfig(){
		PhoneMessageCfg template = null;
		try {
			String tableName = DBUtils.getTableName(PhoneMessageCfg.class);
			StringBuffer sql = new StringBuffer();
			sql.append("select tb.* from "  + tableName +" tb where 1=1 ");
			sql.append(" and tb.status="+CTConsatnt.CT_STATUS_USE+" order by tb.useTime desc ");			//正在使用
			List<PhoneMessageCfg> list = this.daoSupport.queryForList(sql.toString(), PhoneMessageCfg.class,new Object[]{});
			if(list.size() > 0){
				template = list.get(0);
			}
		} catch (Exception e) {
		}
		return template;
	}
	
	public List<PhoneMessageCfg> listPhoneMessageCfg(){
		String tableName = DBUtils.getTableName(PhoneMessageCfg.class);
		List<PhoneMessageCfg> phoneMessageList =this.daoSupport.queryForList("select * from "  + tableName,PhoneMessageCfg.class,new Object[]{});
		return phoneMessageList;
	}

}
