package com.cheriscon.backstage.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.system.service.IStateProvinceService;
import com.cheriscon.common.model.CountryCode;
import com.cheriscon.common.model.StateProvince;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

@Service
public class StateProvinceServiceImpl extends BaseSupport<StateProvince> implements IStateProvinceService {
	
	public Page pageStateProvince(StateProvince state, int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(StateProvince.class);
		String tableName2 = DBUtils.getTableName(CountryCode.class);
		
		StringBuffer sql = new StringBuffer();
		List<Object> list = new ArrayList<Object>();
		sql.append("select s.id,s.state,s.code,s.countryId,c.country from " + tableName + " s ");
		sql.append("left join "+tableName2+" c on s.countryId = c.id");
		sql.append(" where 1=1");
		
		if(null != state){
			if(!StringUtil.isEmpty(state.getState())){
				sql.append(" and s.state like ? ");
				list.add("%" + state.getState() + "%");
			}
			
			if(null != state.getCountryId()){
				sql.append(" and s.countryId = ? ");
				list.add(state.getCountryId());
			}
		}
		
		sql.append(" order by s.id asc ");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,StateProvince.class, list.toArray());
		return page;
	}
	
	public List<StateProvince> getStateProvinceList() {
		String tableName = DBUtils.getTableName(StateProvince.class);
		String tableName2 = DBUtils.getTableName(CountryCode.class);
		
		String sql = "select s.id,s.state,s.code,s.countryId,c.country from " + tableName + " s ";
		sql += "left join "+tableName2+" c on s.countryId = c.id";
		return this.daoSupport.queryForList(sql, StateProvince.class);
	}
	
	public StateProvince getStateProvince(int id) {
		String tableName = DBUtils.getTableName(StateProvince.class);
		String sql = "select * from " + tableName +" where id = ?";
		return this.daoSupport.queryForObject(sql, StateProvince.class, id);
	}
	
	public List<StateProvince> getStateProvince(String state){
		String tableName = DBUtils.getTableName(StateProvince.class);
		String sql = "select * from " + tableName +" where state = ?";
		return this.daoSupport.queryForList(sql, StateProvince.class, state);
	}
	
	public boolean saveStateProvince(StateProvince state) {
		String tableName = DBUtils.getTableName(StateProvince.class);
		this.daoSupport.insert(tableName, state);
		return true;
	}
	
	public boolean updateStateProvince(StateProvince state) {
		String tableName = DBUtils.getTableName(StateProvince.class);
		this.daoSupport.update(tableName, state, "id="+state.getId());
		return true;
	}
	
	public boolean deleteStateProvince(int id) {
		String tableName = DBUtils.getTableName(StateProvince.class);
		String sql = "delete from " + tableName +" where id = ?";
		this.daoSupport.execute(sql, id);
		return true;
	}
}
