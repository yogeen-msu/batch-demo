package com.cheriscon.backstage.system.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductInfoPwdChangeLog;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

@Service
public class ProductInfoServiceImpl extends BaseSupport<ProductInfo> implements IProductInfoService {
	
	@Resource
	private IAdminUserManager adminUserManager;
	@Transactional
	public boolean saveProductInfo(ProductInfo productInfo) {
		String tableName = DBUtils.getTableName(ProductInfo.class);
		productInfo.setCode(DBUtils.generateCode(DBConstant.PRODUCTINFO_CODE));
		productInfo.setSerialNo(productInfo.getSerialNo().toUpperCase());
		productInfo.setRegStatus(FrontConstant.PRODUCT_REGSTATUS_NO);
		this.daoSupport.insert(tableName, productInfo);
		return true;
	}

	
	@Transactional
	public boolean updateProductInfo(ProductInfo productInfo) {
		String tableName = DBUtils.getTableName(ProductInfo.class);
		
		ProductInfo dbProduct = getById(productInfo.getId());
		/*if(!productInfo.getRegPwd().equals(dbProduct.getRegPwd())){
			ProductInfoPwdChangeLog log=new ProductInfoPwdChangeLog();
			log.setNewPwd(productInfo.getRegPwd());
			log.setOldPwd(dbProduct.getRegPwd());
			log.setProCode(dbProduct.getCode());
			log.setOperatorDate(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			log.setOperatorUser(user.getUsername());
			this.daoSupport.insert("DT_ProductInfoPwdChangeLog", log);
		}*/
		
		
		dbProduct.setProTypeCode(productInfo.getProTypeCode());
//		dbProduct.setSerialNo(productInfo.getSerialNo());
		dbProduct.setSaleContractCode(productInfo.getSaleContractCode());
		//dbProduct.setRegPwd(productInfo.getRegPwd());
		dbProduct.setAricraftSerialNumber(productInfo.getAricraftSerialNumber());		
		dbProduct.setImuSerialNumber(productInfo.getImuSerialNumber());
		dbProduct.setRemoteControlSerialNumber(productInfo.getRemoteControlSerialNumber());
		dbProduct.setBatterySerialNumber(productInfo.getBatterySerialNumber());
		dbProduct.setGimbalMac(productInfo.getGimbalMac());
		dbProduct.setRemoteControlMac(productInfo.getRemoteControlMac());
		
		dbProduct.setProDate(productInfo.getProDate());
		dbProduct.setRegStatus(productInfo.getRegStatus());
		dbProduct.setRegTime(productInfo.getRegTime());
		dbProduct.setExternInfo1(productInfo.getExternInfo1());
		dbProduct.setExternInfo2(productInfo.getExternInfo2());
		dbProduct.setExternInfo3(productInfo.getExternInfo3());
		dbProduct.setExternInfo4(productInfo.getExternInfo4());
		dbProduct.setExternInfo5(productInfo.getExternInfo5());
		dbProduct.setExternInfo6(productInfo.getExternInfo6());
		//dbProduct.setRemark(productInfo.getRemark());
		
	
		
		
		this.daoSupport.update(tableName,dbProduct,"id="+dbProduct.getId());
		return true;
	}


	@Transactional
	public boolean delProductInfoById(int id) {
		String tableName = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(),id);
		return true;
	}
	
	public ProductInfo getById(int id){
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
		String saleContractTbl = DBUtils.getTableName(SaleContract.class);
		
		StringBuffer sql = new StringBuffer("select pif.*,pt.name proTypeName,sc.name saleContractName from "  +  productInfoTbl + " pif ");
		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
		sql.append(" left join "+saleContractTbl + " sc on pif.saleContractCode = sc.code");
		sql.append(" where pif.id=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, id);
	}
	
	public ProductInfo getBySerialNo(String serialNo){
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.serialNo=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo);
	}
	
	public ProductInfo getBySerialNo2(String serialNo){
		StringBuffer sql = new StringBuffer("select a.*,b.name as proTypeName  from dt_productinfo a ,DT_ProductForSealer b where a.proTypeCode=b.code");
		sql.append(" and a.serialNo=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo);
	}
	
	public ProductInfo getBySerialNo3(String serialNo){
		StringBuffer sql = new StringBuffer("select a.code,b.sealerCode as sealerAutelId,b.languagecfgCode as saleContractLanguage,b.saleCfgCode as saleContractCfg"
				+ " from dt_productinfo a,dt_saleContract b where a.salecontractCode=b.code");
		sql.append(" and a.serialNo=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo);
	}
	
	
	public ProductInfo getByProCode(String code){
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.code=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, code);
	}
	
	
	public List<ProductInfo> queryProductInfo() {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
		String saleContractTbl = DBUtils.getTableName(SaleContract.class);
		
		StringBuffer sql = new StringBuffer("select pif.*,pt.name proTypeName,sc.name saleContractName from "  +  productInfoTbl + " pif ");
		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
		sql.append(" left join "+saleContractTbl + " sc on pif.saleContractCode = sc.code");
		sql.append(" order by pif.id asc ");
		
		return this.daoSupport.queryForList(sql.toString(), ProductInfo.class, new Object[]{});
	}

	public Page pageProductInfo(ProductInfo productInfo,int pageNo, int pageSize) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
		String saleContractTbl = DBUtils.getTableName(SaleContract.class);
		
		StringBuffer sql = new StringBuffer("select pif.*,pt.name proTypeName,sc.name saleContractName from "  +  productInfoTbl + " pif ");
		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
		sql.append(" left join "+saleContractTbl + " sc on pif.saleContractCode = sc.code");
		
		StringBuffer term = new StringBuffer();
		if(productInfo != null){
			//产品型号
			if(!StringUtil.isEmpty(productInfo.getProTypeCode())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				
				term.append(" pif.proTypeCode like '%"+productInfo.getProTypeCode().trim()+"%' ");
			}
			
			if(!StringUtil.isEmpty(productInfo.getSaleContractName())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" sc.name like '%" +productInfo.getSaleContractName()+"%'");
			}
			
			if(!StringUtil.isEmpty(productInfo.getSaleContractArea())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" sc.areaCfgCode = '"+productInfo.getSaleContractArea()+"'");
			}
			
			//产品序号号
			if(!StringUtil.isEmpty(productInfo.getSerialNo())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				
				term.append(" pif.serialNo like '%"+productInfo.getSerialNo().trim()+"%' ");
//				term.append(" pif.serialNo = '"+productInfo.getSerialNo().trim()+"' ");
			}
			
			//飞机序号号
			if(!StringUtil.isEmpty(productInfo.getAricraftSerialNumber())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" pif.aricraftSerialNumber like '%"+productInfo.getAricraftSerialNumber().trim()+"%' ");
			}
			//云台序号号
			if(!StringUtil.isEmpty(productInfo.getImuSerialNumber())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" pif.imuSerialNumber like '%"+productInfo.getImuSerialNumber().trim()+"%' ");
			}
			//遥控器序号号
			if(!StringUtil.isEmpty(productInfo.getRemoteControlSerialNumber())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" pif.remoteControlSerialNumber like '%"+productInfo.getRemoteControlSerialNumber().trim()+"%' ");
			}
			sql.append(term);
		}
		sql.append(" order by pif.id desc ");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,ProductInfo.class,new Object[]{});
		return page;
	}

	public List<ProductInfo> queryProductByUserCode(String userCode) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select b.*,c.languageCfgCode as saleContractLanguage,c.saleCfgCode as saleContractCfg  from  dt_customerproinfo a ,dt_productInfo b,dt_salecontract c ");
		sql.append("where a.proCode=b.code ");
		sql.append("and b.saleContractCode=c.code ");
		sql.append("and a.customerCode='"+userCode+"' ");
		sql.append("and c.proTypeCode='prt_DS708' ");
		sql.append("and c.areaCfgCode<>'acf_North_America'");
		
		return this.daoSupport.queryForList(sql.toString(), ProductInfo.class);
	}
	
	public ProductInfo getBySerialNoAndPwd(String serialNo,String password){
		StringBuffer sql = new StringBuffer("select a.*,b.name as proTypeName  from dt_productinfo a ,DT_ProductForSealer b where a.proTypeCode=b.code");
		sql.append(" and a.serialNo like ?  and regPwd=?");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo+ "%",password);
	}

	@Override
	public ProductInfo getAndVaildBySerialNo3(String serialNo) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select  p.code , "
				+ " (select distinct w.validDate from DT_MinSaleUnitSaleCfgDetail m ,DT_ProductSoftwareValidStatus w "
				+ " where m.minSaleUnitCode=w.minSaleUnitCode and m.saleCfgCode=sc.code and  w.proCode=p.code "
				+ " ) as validDate, "
				+ " p.id,p.serialNo serialNo,p.regTime ,p.proDate,p.saleContractCode,p.regPwd ,p.proTypeCode,p.noRegReason,p.regStatus,pt.name ,pt.picPath,sct.warrantyMonth as warrantymonth "
				+ " from   "
				+ " DT_ProductInfo p  "
				+ " left join DT_SaleContract sct on p.saleContractCode=sct.code  "
				+ " left join DT_SaleConfig sc on  sc.code=sct.saleCfgCode  "
				+ " left join DT_CustomerProInfo cpi on p.code=cpi.proCode  "
				+ " left join DT_ProductType pt on p.proTypeCode=pt.code "
				+ " where p.serialNo =");
		sql.append("'");
		sql.append(serialNo);
		sql.append("'");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class);
	}

	@Override
	public List<ProductInfo> queryProductUpgradeRecordList(String serialNo) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"select a.upgradeTime,b.serialNo,d.name softName from DT_ProductUpgradeRecord a,DT_ProductInfo b ,DT_SoftwareType d")
				.append(" where a.proCode = b.code and a.softwareTypeCode = d.code and b.serialNo='")
				.append(serialNo)
				.append("' order by a.upgradeTime Desc limit 0,20");
		return this.daoSupport.queryForList(sql.toString(), ProductInfo.class);
	}

	@Override
	public List<ProductInfo> queryProductRepairRecordList(String proCode) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"select a.repDate,a.repType,a.repContent from DT_ProductRepairRecord a")
				.append(" where a.proCode='").append(proCode)
				.append("' order by a.repDate desc");
		
		return this.daoSupport.queryForList(sql.toString(), ProductInfo.class);
	}

	@Override
	public ProductInfo shouwProductInfoBySerialNo(String serialNo) {
		StringBuffer sql = new StringBuffer();
		sql.append("select p.serialNo,t.name proName from DT_ProductInfo p left join DT_ProductForSealer t on p.proTypeCode=t.code where p.serialNo=");
		sql.append("'");
		sql.append(serialNo);
		sql.append("'");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class);
	}

	/**
	 * 20160428 A16043 hlh 
	 * 根据飞机序列号查询
	 * @param aricraftSerialNumber
	 * @return
	 */
	@Override
	public ProductInfo getByAricraftSerialNumber(String aricraftSerialNumber) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.AricraftSerialNumber=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, aricraftSerialNumber);
	}

	/**
	 * 20160428 A16043 hlh 
	 * 根据云台序列号查询
	 * @param imuSerialNumber
	 * @return
	 */
	@Override
	public ProductInfo getByImuSerialNumber(String imuSerialNumber) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.ImuSerialNumber=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, imuSerialNumber);
	}

	/**
	 * 20160428 A16043 hlh 
	 * 根据遥控器序列号查询
	 * @param remoteControlSerialNumber
	 * @return
	 */
	@Override
	public ProductInfo getByRemoteControlSerialNumber(
			String remoteControlSerialNumber) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.RemoteControlSerialNumber=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, remoteControlSerialNumber);
	}

	/**
	 * 20160428 A16043 hlh
	 * 根据电池序列号查询
	 * @param id
	 * @return
	 */
	@Override
	public ProductInfo getByBatterySerialNumber(String batterySerialNumber) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.batterySerialNumber=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, batterySerialNumber);
	}

	/**
	 * 20160428 A16043 hlh
	 * 根据云台MAC地址(路由地址)查询
	 * @param gimbalMac
	 * @return
	 */
	@Override
	public ProductInfo getByGimbalMac(String gimbalMac) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.GimbalMac=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, gimbalMac);
	}

	/**
	 * 20160428 A16043 hlh 
	 * 根据遥控器MAC地址(中继地址)查询
	 * @param remoteControlMac
	 * @return
	 */
	@Override
	public ProductInfo getByRemoteControlMac(String remoteControlMac) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.RemoteControlMac=? ");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, remoteControlMac);
	}
	
	/**
	 * 无人机退换货系统调用
	 */
	public List<ProductInfo> queryProInfo(ProductInfo productInfo) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
		String saleContractTbl = DBUtils.getTableName(SaleContract.class);
		
		StringBuffer sql = new StringBuffer("select pif.*,pt.name proTypeName,sc.name saleContractName from "  +  productInfoTbl + " pif ");
		sql.append(" left join "+productTypeTbl + " pt on pif.proTypeCode = pt.code ");
		sql.append(" left join "+saleContractTbl + " sc on pif.saleContractCode = sc.code");
		
		StringBuffer term = new StringBuffer();
		if(productInfo != null){
			//产品序号号
			if(!StringUtil.isEmpty(productInfo.getSerialNo())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				
				term.append(" pif.serialNo like '%"+productInfo.getSerialNo().trim()+"%' ");
			}
			//飞机序号号
			if(!StringUtil.isEmpty(productInfo.getAricraftSerialNumber())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" pif.aricraftSerialNumber like '%"+productInfo.getAricraftSerialNumber().trim()+"%' ");
			}
			//云台序号号
			if(!StringUtil.isEmpty(productInfo.getImuSerialNumber())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" pif.imuSerialNumber like '%"+productInfo.getImuSerialNumber().trim()+"%' ");
			}
			//遥控器序号号
			if(!StringUtil.isEmpty(productInfo.getRemoteControlSerialNumber())){
				if(term.length()>0){term.append(" and ");
				}else{term.append(" where ");}
				term.append(" pif.remoteControlSerialNumber like '%"+productInfo.getRemoteControlSerialNumber().trim()+"%' ");
			}
			sql.append(term);
		}
		sql.append(" order by pif.id desc ");
		
		return this.daoSupport.queryForList(sql.toString(), ProductInfo.class, new Object[]{});
	}


	@Override
	public ProductInfo getProInfoByProCodeAndCustomerCode(String proCode,
			String customerCode) {
		StringBuffer sql = new StringBuffer();
		sql.append("select t.* from DT_ProductInfo t ,DT_CustomerProInfo d,DT_CustomerInfo e ");
		sql.append("where t.code = d.proCode and e.code = d.customerCode ");
		sql.append("and t.code=");
		sql.append("'");
		sql.append(proCode);
		sql.append("'");
		sql.append(" and e.code=");
		sql.append("'");
		sql.append(customerCode);
		sql.append("'");
		return this.daoSupport.queryForObject(sql.toString(), ProductInfo.class);
	}
}
