package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.EmailLog;
import com.cheriscon.framework.database.Page;

public interface IEmailLogService {
	
	public void saveEmailLog(EmailLog log) throws Exception;
	
	public void updateEmailLog(EmailLog log) throws Exception;
	
	public List<EmailLog> listEmailLog() throws Exception;
	
	public  Page getEmailLogs(EmailLog emailLog, int pageNo,int pageSize) throws Exception;

}
