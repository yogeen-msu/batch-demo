package com.cheriscon.backstage.system.service;

import java.util.List;

import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.framework.database.Page;

/**
 * 产品信息Service
 * @author yinhb
 *
 */
public interface IProductInfoService {
	
	/**
	 * 增加
	 * @param productInfo
	 */
	public boolean saveProductInfo(ProductInfo productInfo);
	
	/**
	 * 修改
	 * @param productInfo
	 */
	public boolean updateProductInfo(ProductInfo productInfo);
	
	/**
	 * 删除
	 */
	public boolean delProductInfoById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public ProductInfo getById(int id);
	
	/**
	 * 根据产品序列号查询
	 * @param id
	 * @return
	 */
	public ProductInfo getBySerialNo(String serialNo);
	
	/**
	 * 查询出所有产品信息
	 * @return
	 */
	public List<ProductInfo> queryProductInfo();
	
	/**
	 * 翻页查询所有产品信息
	 * @param productInfo
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageProductInfo(ProductInfo productInfo,int pageNo, int pageSize);
	
	public List<ProductInfo> queryProductByUserCode(String userCode) throws Exception;
	
	/**
	 * 根据产品code查询
	 * @param id
	 * @return
	 */
	public ProductInfo getByProCode(String code);
	
	public ProductInfo getBySerialNo2(String serialNo);
	
	public ProductInfo getBySerialNo3(String serialNo);
	
	
	/**
	 * 根据产品序列号和密码查询产品信息
	 * @param serialNo password
	 * @return
	 */
	public ProductInfo getBySerialNoAndPwd(String serialNo,String password);
	public ProductInfo getAndVaildBySerialNo3(String serialNo);
	public List<ProductInfo> queryProductUpgradeRecordList(String serialNo);
	public List<ProductInfo> queryProductRepairRecordList(String proCode);
	public ProductInfo shouwProductInfoBySerialNo(String serialNo);

	
	/**
	 * 20160428 A16043 hlh 
	 * 根据飞机序列号查询
	 * @param aricraftSerialNumber
	 * @return
	 */
	public ProductInfo getByAricraftSerialNumber(String aricraftSerialNumber);
	/**
	 * 20160428 A16043 hlh 
	 * 根据云台序列号查询
	 * @param imuSerialNumber
	 * @return
	 */
	public ProductInfo getByImuSerialNumber(String imuSerialNumber);
	/**
	 * 20160428 A16043 hlh 
	 * 根据遥控器序列号查询
	 * @param remoteControlSerialNumber
	 * @return
	 */
	public ProductInfo getByRemoteControlSerialNumber(String remoteControlSerialNumber);
	/**
	 * 20160428 A16043 hlh
	 * 根据电池序列号查询
	 * @param id
	 * @return
	 */
	public ProductInfo getByBatterySerialNumber(String batterySerialNumber);
	/**
	 * 20160428 A16043 hlh
	 * 根据云台MAC地址(路由地址)查询
	 * @param gimbalMac
	 * @return
	 */
	public ProductInfo getByGimbalMac(String gimbalMac);
	/**
	 * 20160428 A16043 hlh 
	 * 根据遥控器MAC地址(中继地址)查询
	 * @param remoteControlMac
	 * @return
	 */
	public ProductInfo getByRemoteControlMac(String remoteControlMac);
	
	/**
	 * 20161111 A16043 hlh 
	 * 根据proCode 与 customerCode查询
	 * @param remoteControlMac
	 * @return
	 */
	public ProductInfo getProInfoByProCodeAndCustomerCode(String proCode,String customerCode);
	
	/**
	 * 无人机退换货系统调用
	 */
	public List<ProductInfo> queryProInfo(ProductInfo productInfo);
}
