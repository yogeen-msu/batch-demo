package com.cheriscon.backstage.system.service;

import com.cheriscon.common.model.ProductRepairRecord;
import com.cheriscon.framework.database.Page;

/**
 * 维修记录Service
 * @author yinhb
 *
 */
public interface IProductRepairRecordService {
	
	/**
	 * 增加
	 * @param productRepairRecord
	 */
	public boolean saveProductRepairRecord(ProductRepairRecord productRepairRecord);
	
	/**
	 * 修改
	 * @param productRepairRecord
	 */
	public boolean updateProductRepairRecord(ProductRepairRecord productRepairRecord);
	
	/**
	 * 删除
	 */
	public boolean delProductRepairRecordById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public ProductRepairRecord getById(int id);
	
	
	/**
	 * 翻页查询所有维修记录
	 * @param productRepairRecord
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageProductRepairRecord(ProductRepairRecord productRepairRecord,int pageNo, int pageSize);

}
