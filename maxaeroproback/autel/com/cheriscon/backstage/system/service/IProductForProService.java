package com.cheriscon.backstage.system.service;

import java.util.Map;

import com.cheriscon.common.model.ProductForPro;
import com.cheriscon.framework.database.Page;

public interface IProductForProService {

	public Page getProductForPros(ProductForPro productForPro, int pageNo,
			int pageSize) throws Exception;

	public Page getProductForProsForBand(ProductForPro productForPro, int pageNo,
			int pageSize) throws Exception;
	
	public void addProductForPro(ProductForPro productForPro) throws Exception;

	public void deleteProductForProById(int id) throws Exception;

	public void updateProductForPro(ProductForPro productForPro)
			throws Exception;
	
	public void updateProductForProByMap(Map<String,Object> fileds)
			throws Exception;

	public ProductForPro getProductForProByCode(String code) throws Exception;
	
	public ProductForPro getProductForProById(int id) throws Exception;
	
	public ProductForPro getProductForProBySerialNo(String serialNo) throws Exception;
}
