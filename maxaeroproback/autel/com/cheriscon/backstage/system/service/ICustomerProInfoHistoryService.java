package com.cheriscon.backstage.system.service;

import com.cheriscon.common.model.CustomerProInfoHistory;

public interface ICustomerProInfoHistoryService {
    public void add(CustomerProInfoHistory customerProInfoHistory) throws Exception;
    
    public void rebanding(Integer id,String updateTime,String updateUser) throws Exception;
}
