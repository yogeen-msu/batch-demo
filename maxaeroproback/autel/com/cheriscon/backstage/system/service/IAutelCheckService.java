package com.cheriscon.backstage.system.service;



import java.util.List;

import com.cheriscon.backstage.system.model.AutelCheckVo;
import com.cheriscon.backstage.trade.vo.OrderInfoVo;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.framework.database.Page;


public interface IAutelCheckService 
{	
	

	/**
	  * 获取今日订单数量，金额
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public  AutelCheckVo getTodayMoney() throws Exception;
	
	/**
	  * 获取订单数量，金额
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public  AutelCheckVo getTotalMoney() throws Exception;

	/**
	  * 获取每年中每个月订单数量，金额
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public List<AutelCheckVo> getMoneyByYear(String year) throws Exception;
	
	
	/**
	  * 统计充值卡使用情况
	  * @throws Exception
	  * @author chenqichuan
	  */
	public List<AutelCheckVo> getCardUseNum(int useFlag) throws Exception;
	
	
	/**
	  * 统计充值卡使用情况
	  * @throws Exception
	  * @author chenqichuan
	  */
	public int getCardIsUseNum(int useFlag) throws Exception;
	
	/**
	  * 统计产品情况
	  * @throws Exception
	  * @author chenqichuan
	  */
	public List<AutelCheckVo> getProductNum(AutelCheckVo autelCheck) throws Exception;
	
	/**
	  * 统计各状态产品数量（注册，未注册，解绑）
	  * @throws Exception
	  * @author chenqichuan
	  */
	public int getProductStatus(int status) throws Exception;
	
	/**
	  * 统计过期产品数量
	  * @throws Exception
	  * @author chenqichuan
	  */
	public int getProductValid() throws Exception;
	
	/**
	  * 按区域统计产品情况
	  * @throws Exception
	  * @author chenqichuan
	  */
	public List<AutelCheckVo> getProductNumByArea(AutelCheckVo autelCheck) throws Exception;
	/**
	  * 按语言统计产品情况
	  * @throws Exception
	  * @author chenqichuan
	  */
	public List<AutelCheckVo> getProductNumByLanguage(AutelCheckVo autelCheck) throws Exception;
	
	
	
	
}
