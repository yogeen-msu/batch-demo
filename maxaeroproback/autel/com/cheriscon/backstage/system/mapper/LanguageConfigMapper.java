package com.cheriscon.backstage.system.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.common.model.Language;

/**
 * 语言配置详情
 * @author yinhb
 */
public class LanguageConfigMapper  implements RowMapper {

	@Override
	public Language mapRow(ResultSet result, int arg1) throws SQLException {
		Language language = new Language();
		language.setId(result.getInt("lid"));
		language.setCode(result.getString("lcode"));
		language.setName(result.getString("lname"));
		return language;
	}
	
	
}
