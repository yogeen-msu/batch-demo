package com.cheriscon.backstage.system.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.common.model.AreaInfo;

/**
 * 语言配置详情
 * @author yinhb
 */
public class AreaConfigMapper  implements RowMapper {

	@Override
	public AreaInfo mapRow(ResultSet result, int arg1) throws SQLException {
		AreaInfo areaInfo = new AreaInfo();
		areaInfo.setId(result.getInt("aid"));
		areaInfo.setCode(result.getString("acode"));
		areaInfo.setName(result.getString("aname"));
		return areaInfo;
	}
	
	
}
