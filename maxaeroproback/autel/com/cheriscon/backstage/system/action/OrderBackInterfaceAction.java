package com.cheriscon.backstage.system.action;

import java.io.IOException;

import javax.annotation.Resource;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.trade.service.IOrderCancelLogService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.utils.JSONHelpUtils;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.StringUtil;

@ParentPackage("json-default")
@Namespace("/front/user")
public class OrderBackInterfaceAction extends WWAction {

	/**
	 * 客户在网站上在线续费后，可以在一定时间内取消付款，因此本接口用来 查询当前传入的序列号是否有取消付款的记录 作者：陈齐川 2014-08-29
	 */
	private static final long serialVersionUID = 140913433640079162L;
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private IOrderCancelLogService orderCancelLogService;

	private String jsonData;

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	@Action(value = "CheckProductBackOrder")
	public void CheckProductBackOrder() {
		String serialNo = getRequest().getParameter("serialNo"); // 产品序列号
		String regPwd = getRequest().getParameter("regPwd"); // 产品注册密码
		if (StringUtil.isEmpty(serialNo)) {
			jsonData = "0001";
		} else if (StringUtil.isEmpty(regPwd)) {
			jsonData = "0002";
		} else {
			ProductInfo info = productInfoService.getBySerialNoAndPwd(serialNo,
					regPwd);
			if (info == null) {
				jsonData = "0003";
			}else{
			try {
				int num = orderCancelLogService.getOrderCancelByProCode(info
						.getCode());
				if (num == 0) {
					jsonData = "0000";
				} else {
					jsonData = "0004";
				}
				
			} catch (Exception e) {
			}
			}
		}
		getResponse().setContentType("text/xml;charset=UTF-8");
		getResponse().setCharacterEncoding("UTF-8");
		try {
			getResponse().getWriter().write("<result>" + jsonData + "</result>");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
