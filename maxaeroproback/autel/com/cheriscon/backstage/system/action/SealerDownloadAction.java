package com.cheriscon.backstage.system.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.ISealerDownloadService;
import com.cheriscon.common.model.SealerDownload;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/sealerDownload_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/sealerDownload_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/sealerDownload_edit.jsp")
})
public class SealerDownloadAction extends WWAction {
	private SealerDownload sealerDownload;
	private int id;
	@Resource
	private ISealerDownloadService sealerDownloadService;
	
	public String list(){
		this.webpage = sealerDownloadService.pageSealerDownload(sealerDownload, this.getPage(), this.getPageSize());
		return "list";
	}
	
	
	public String add(){
		return "add";
	}
	
	@SuppressWarnings("unchecked")
	public String save(){
		try {
			sealerDownloadService.saveSealerDownload(sealerDownload);
			this.msgs.add(getText("common.js.addSuccess"));
		} catch (Exception e) {
			this.msgs.add(getText("common.js.addFail"));
		}
		this.urls.put(getText("syssetting.sealerman.list"), "sealer-download!list.do");
		return MESSAGE;
	}
	
	public String edit(){
		this.sealerDownload = sealerDownloadService.getSealerDownload(id);
		return "edit";
	}
	
	@SuppressWarnings("unchecked")
	public String update(){
		try {
			sealerDownloadService.updateSealerDownload(sealerDownload);
			this.msgs.add(getText("common.js.modSuccess"));
		} catch (Exception e) {
			this.msgs.add(getText("common.js.modFail"));
		}
		this.urls.put(getText("syssetting.sealerman.list"), "sealer-download!list.do");
		return MESSAGE;
	}
	
	@SuppressWarnings("unchecked")
	public String delete(){
		try {
			sealerDownloadService.deleteSealerDownload(id);
			this.msgs.add(getText("common.js.del"));
		} catch (Exception e) {
			msgs.add(getText("common.js.delFail"));
		}
		this.urls.put(getText("syssetting.sealerman.list"), "sealer-download!list.do");
		return MESSAGE;
	}


	public SealerDownload getSealerDownload() {
		return sealerDownload;
	}
	public void setSealerDownload(SealerDownload sealerDownload) {
		this.sealerDownload = sealerDownload;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
