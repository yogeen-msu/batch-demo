package com.cheriscon.backstage.system.action;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.constant.SystemConstant;
import com.cheriscon.backstage.system.service.ISiteLanguageService;
import com.cheriscon.common.model.SiteLanguage;
import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.Theme;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.utils.UploadUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.FileUtil;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/sitelanguage_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/sitelanguage_add.jsp")
})
public class SiteLanguageAction extends WWAction{
	
	private int id;
	private SiteLanguage siteLanguage;
	private File lanPackage;						//语言文件
	private String lanPackageFileName;			//语言文件名称
	
	private List<SiteLanguage> languageList;
	
	@Resource
	private IThemeManager themeManager;
	@Resource
	private ISiteLanguageService siteLanguageService; 
	
	// 显示列表
	public String list() {
		languageList = siteLanguageService.listLanguage();
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	@SuppressWarnings("unchecked")
	public String addSave(){
		
		List<SiteLanguage> list = siteLanguageService.getByNewSiteLanguage(siteLanguage);
		if(list == null || list.size() == 0){
			if (lanPackage != null) {
//			if (FileUtil.isAllowUp(languageFileName)) {
				if (lanPackageFileName.toLowerCase().endsWith(".properties")) {
					String fileName = SystemConstant.SITE_LANGUAGE_RES+"_"+siteLanguage.getLanguageCode()+"_"+siteLanguage.getStateCode()+".properties"; 
					
					//获取当前使用的主题
					CopSite site  = CopContext.getContext().getCurrentSite();
					Theme theme = themeManager.getTheme( site.getThemeid());
					
					String filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/language/"+theme.getPath()+"/";
					String path  = CopSetting.FILE_STORE_PREFIX+ "/attachment/language/" +theme.getPath()+ "/"+fileName;
					filePath += fileName;
					
					if(!UploadUtil.fileExists(filePath)){
						FileUtil.createFile(lanPackage, filePath);
						siteLanguage.setAttUrl(path);
						siteLanguageService.saveLanguage(siteLanguage);
						this.msgs.add(getText("system.sitelanguage.sucess"));
						this.urls.put(getText("system.sitelanguage.list"), "site-language!list.do");
					}else{
						this.msgs.add(getText("site.language.hasexist"));
						this.urls.put(getText("common.btn.return"), "site-language!add.do");
					}
				} else {
					this.msgs.add(getText("site.language.noallow.uploadfile"));
					this.urls.put(getText("common.btn.return"), "site-language!add.do");
				}
			}else{
				this.msgs.add(getText("site.language.name.notempey"));
				this.urls.put(getText("common.btn.return"), "site-language!add.do");
			}
		}else{
			this.msgs.add(getText("site.language.addfail"));
			this.urls.put(getText("common.btn.return"), "site-language!add.do");
		}
		
		return MESSAGE;
	}
	
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			SiteLanguage siteLanguage = siteLanguageService.getById(id);
			String filePath = siteLanguage.getAttUrl().replaceAll(CopSetting.FILE_STORE_PREFIX,CopSetting.IMG_SERVER_PATH+CopContext.getContext().getContextPath());
			FileUtil.delete(filePath);
			siteLanguageService.delLanguageById(id);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}

		this.urls.put(getText("site.language.list"), "site-language!list.do");
		return MESSAGE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SiteLanguage getSiteLanguage() {
		return siteLanguage;
	}

	public void setSiteLanguage(SiteLanguage siteLanguage) {
		this.siteLanguage = siteLanguage;
	}

	public List<SiteLanguage> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<SiteLanguage> languageList) {
		this.languageList = languageList;
	}

	public File getLanPackage() {
		return lanPackage;
	}

	public void setLanPackage(File lanPackage) {
		this.lanPackage = lanPackage;
	}

	public String getLanPackageFileName() {
		return lanPackageFileName;
	}

	public void setLanPackageFileName(String lanPackageFileName) {
		this.lanPackageFileName = lanPackageFileName;
	}
	
	

}
