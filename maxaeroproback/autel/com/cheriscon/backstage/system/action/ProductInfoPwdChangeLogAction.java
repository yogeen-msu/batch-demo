package com.cheriscon.backstage.system.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.system.service.IProductInfoPwdChangeLogService;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/productInfoPwdChangeLog")
@Result(name="list", location="/autel/backstage/log/ProductInfoPwdChangeLog.jsp")
public class ProductInfoPwdChangeLogAction extends WWAction {

	@Resource
	private IProductInfoPwdChangeLogService productInfoPwdChangeLogService;
	
	private String productSN;
	
	public String list() {
		try {
			this.webpage = productInfoPwdChangeLogService
					.getProductInfoPwdChangeLogs(productSN,this.getPage(),this.getPageSize());
		} catch (Exception e) {
		}
		return "list";
	}


	public String getProductSN() {
		return productSN;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

}
