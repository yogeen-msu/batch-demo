package com.cheriscon.backstage.system.action;

import java.util.List;
import javax.annotation.Resource;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import com.cheriscon.backstage.system.service.ICountryCodeService;
import com.cheriscon.common.model.CountryCode;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/country_code_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/country_code_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/country_code_edit.jsp")
})
public class CountryCodeAction extends WWAction {
	
	@Resource
	private ICountryCodeService countryCodeService;
	
	private CountryCode countryCode;
	private Integer id;
	
	/**
	 * 显示列表
	 * @return
	 */
	public String list() {
		this.webpage = countryCodeService.pageCountryCode(countryCode, this.getPage(), this.getPageSize());
		return "list";
	}

	/**
	 * 新增
	 * @return
	 * @throws Exception
	 */
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 新增保存
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String save() {
		List<CountryCode> list = countryCodeService.getCountryCode(countryCode.getCountry());
		if (null == list || list.size() == 0) {
			countryCodeService.saveCountryCode(countryCode);
			this.msgs.add(getText("common.js.addSuccess"));
		} else {
			this.msgs.add(getText("syssetting.countrycode.hasexist"));
		}
		
		this.urls.put(getText("syssetting.countrycode.list"), "country-code!list.do");
		return this.MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 * @throws Exception
	 */
	public String edit() throws Exception {
		countryCode = countryCodeService.getCountryCode(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String update() {
		List<CountryCode> list = countryCodeService.getCountryCode(countryCode.getCountry());
		
		if (null == list || (null != list && list.get(0).getId().intValue() == countryCode.getId().intValue())) {
			countryCodeService.updateCountryCode(countryCode);
			this.msgs.add(getText("common.js.modSuccess"));
		} else {
			this.msgs.add(getText("syssetting.countrycode.hasexist"));
		}
		
		this.urls.put(getText("syssetting.countrycode.list"), "country-code!list.do");
		return this.MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String delete() {
		try {
			countryCodeService.deleteCountryCode(id);
			msgs.add(getText("common.js.del"));
		} catch (Exception e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}
		this.urls.put(getText("syssetting.countrycode.list"), "country-code!list.do");
		return this.MESSAGE;
	}
	
	public CountryCode getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(CountryCode countryCode) {
		this.countryCode = countryCode;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
