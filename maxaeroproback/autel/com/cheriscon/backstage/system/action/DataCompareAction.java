package com.cheriscon.backstage.system.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.IDataCompareService;
import com.cheriscon.common.model.DataCompare;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.StringUtil;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/dataCompare")
@Result(name = "list", location = "/autel/backstage/dataCompare/dataCompare.jsp")
public class DataCompareAction extends WWAction {

	@Resource
	private IDataCompareService dataCompareService;

	private List<DataCompare> oldDataSoftware;

	private List<DataCompare> newDataSoftware;

	private List<DataCompare> minSaleCodeCompare;

	private List<DataCompare> languageDataCompare;

	private String serialNo;

	public String list() {
		try {
			if (!StringUtil.isEmpty(serialNo)) {
				List<DataCompare> newLanguageDatas = dataCompareService
						.getNewLanguages(serialNo.trim());
				List<DataCompare> oldLanguageDatas = dataCompareService
						.getOldLanguages(serialNo.trim());
				
                //新老数据合并
				languageDataCompare =new ArrayList<DataCompare>();
				if (oldLanguageDatas.size() >= newLanguageDatas.size()) {
					languageDataCompare.addAll(oldLanguageDatas);
					for (int i = 0; i < newLanguageDatas.size(); i++) {
						languageDataCompare.get(i).setNewLanguageCode(
								newLanguageDatas.get(i).getNewLanguageCode());
					}
				} else {
					languageDataCompare.addAll(newLanguageDatas);
					for (int i = 0; i < oldLanguageDatas.size(); i++) {
						languageDataCompare.get(i).setOldLanguageCode(
								oldLanguageDatas.get(i).getOldLanguageCode());
					}
				}

				List<DataCompare> newMinSaleCodes = dataCompareService
						.getNewMinSaleCodes(serialNo.trim());
				List<DataCompare> oldMinSaleCodes = dataCompareService
						.getOldMinSaleCodes(serialNo.trim());
				oldDataSoftware = dataCompareService.getOldSoftwares(serialNo
						.trim());
				
				//新老数据合并
				minSaleCodeCompare=new ArrayList<DataCompare>();
				if (oldMinSaleCodes.size() >= newMinSaleCodes.size()) {
					minSaleCodeCompare.addAll(oldMinSaleCodes);
					for (int i = 0; i < newMinSaleCodes.size(); i++) {
						minSaleCodeCompare.get(i).setMinSaleCode(
								newMinSaleCodes.get(i).getMinSaleCode());
						minSaleCodeCompare.get(i).setNewName(newMinSaleCodes.get(i).getNewName());
					}
				} else {
					minSaleCodeCompare.addAll(newMinSaleCodes);
					for (int i = 0; i < oldMinSaleCodes.size(); i++) {
						minSaleCodeCompare.get(i).setMinSaleUnitCode(
								oldMinSaleCodes.get(i).getMinSaleUnitCode());
						minSaleCodeCompare.get(i).setOldName(oldMinSaleCodes.get(i).getOldName());
					}
				}

				// 取出优化后新数据
				List<DataCompare> newDataSoftwareList = dataCompareService
						.getNewtSoftwares(serialNo.trim());
				// 如果老数据和优化后新数据大小相等，顺序一样就不再进行比较
				if (oldDataSoftware.size() == newDataSoftwareList.size()) {
					newDataSoftware = newDataSoftwareList;
				} else {
					// 如果优化后新数据对比老数据没有值则显示临时空对象
					DataCompare temp = new DataCompare();
					// 与老数据比较后显示在前台数据
					newDataSoftware = new ArrayList<DataCompare>(
							oldDataSoftware.size());
					for (int i = 0; i < oldDataSoftware.size(); i++) {
						boolean flag = false;
						for (int j = 0; j < newDataSoftwareList.size(); j++) {
							if (oldDataSoftware.get(i).equals(
									newDataSoftwareList.get(j))) {
								newDataSoftware.add(newDataSoftwareList.get(j));
								newDataSoftwareList.remove(j);
								flag = true;
							}
						}
						if (!flag) {
							newDataSoftware.add(temp);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return "list";
	}


	public List<DataCompare> getOldDataSoftware() {
		return oldDataSoftware;
	}

	public void setOldDataSoftware(List<DataCompare> oldDataSoftware) {
		this.oldDataSoftware = oldDataSoftware;
	}

	public List<DataCompare> getNewDataSoftware() {
		return newDataSoftware;
	}

	public void setNewDataSoftware(List<DataCompare> newDataSoftware) {
		this.newDataSoftware = newDataSoftware;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public List<DataCompare> getMinSaleCodeCompare() {
		return minSaleCodeCompare;
	}

	public void setMinSaleCodeCompare(List<DataCompare> minSaleCodeCompare) {
		this.minSaleCodeCompare = minSaleCodeCompare;
	}

	public List<DataCompare> getLanguageDataCompare() {
		return languageDataCompare;
	}

	public void setLanguageDataCompare(List<DataCompare> languageDataCompare) {
		this.languageDataCompare = languageDataCompare;
	}

}
