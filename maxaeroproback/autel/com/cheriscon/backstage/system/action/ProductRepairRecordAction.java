package com.cheriscon.backstage.system.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.IProductRepairRecordService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductRepairRecord;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/productRepairRecord_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/productRepairRecord_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/productRepairRecord_edit.jsp"),
	@Result(name="productInfo_select_list", location="/autel/backstage/system/productInfo_select_list.jsp"),
})
public class ProductRepairRecordAction extends WWAction{
	
	private int id;
	private ProductInfo productInfo;
	private ProductRepairRecord productRepairRecord;
	private List<ProductForSealer> productTypeList;
	
	@Resource
	private IProductRepairRecordService productRepairRecordService; 
	@Resource
	private IProductTypeService productTypeService;
	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private IProductForSealerService productForSealerService;
	// 显示列表
	public String list() {
		try {
			//productTypeList = productTypeService.listAll();
			productTypeList = productForSealerService.getProductForSealerList();
			this.webpage = productRepairRecordService.pageProductRepairRecord(productRepairRecord,this.getPage(), this.getPageSize());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "list";
	}
	
	
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 获取所有产品类型
	 * @return
	 */
	public String selectProductInfo() {
		try {
			productTypeList = productForSealerService.getProductForSealerList();
			this.webpage = productInfoService.pageProductInfo(productInfo,this.getPage(), this.getPageSize());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "productInfo_select_list";
	}
	
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		
		productRepairRecordService.saveProductRepairRecord(productRepairRecord);
		this.msgs.add(getText("common.js.addSuccess"));
		this.urls.put(getText("product.repair.list"), "product-repair-record!list.do");
		
		return MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		productRepairRecord = productRepairRecordService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		
		productRepairRecordService.updateProductRepairRecord(productRepairRecord);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("product.repair.list"), "product-repair-record!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			productRepairRecordService.delProductRepairRecordById(id);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}

		this.urls.put(getText("product.repair.list"), "product-repair-record!list.do");
		return MESSAGE;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public ProductInfo getProductInfo() {
		return productInfo;
	}


	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}


	public ProductRepairRecord getProductRepairRecord() {
		return productRepairRecord;
	}


	public void setProductRepairRecord(ProductRepairRecord productRepairRecord) {
		this.productRepairRecord = productRepairRecord;
	}


	public List<ProductForSealer> getProductTypeList() {
		return productTypeList;
	}


	public void setProductTypeList(List<ProductForSealer> productTypeList) {
		this.productTypeList = productTypeList;
	}


}
