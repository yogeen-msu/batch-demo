package com.cheriscon.backstage.system.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.IAreaConfigService;
import com.cheriscon.backstage.system.service.IAreaInfoService;
import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.AreaInfo;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/area_config_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/area_config_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/area_config_edit.jsp")
})
public class AreaConfigAction extends WWAction{
	
	private int id;
	private AreaConfig areaConfig;
	
	private int [] ids;			//区域配置id
	
	@Resource
	private IAreaInfoService areaInfoService; 
	@Resource
	private IAreaConfigService areaConfigService; 
	
	// 显示列表
	public String list() {
		this.webpage = areaConfigService.pageAreaConfig(areaConfig,this.getPage(), this.getPageSize());
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String addSave(){
		
		List<AreaConfig> list = areaConfigService.getByNewAreaConfig(areaConfig);
		
		if(list == null || list.size() == 0){
			areaConfig.setAreaInfoList(loadAreaInfoById(ids));	//加载配置的区域
			areaConfigService.saveAreaConfig(areaConfig);
			this.msgs.add(getText("common.js.addSuccess"));
		}else{
			this.msgs.add(getText("syssetting.areaconfig.area.hasexist"));
		}
	
		this.urls.put(getText("syssetting.areaconfig.list"), "area-config!list.do");
		
		return this.MESSAGE;
	}
	

	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		areaConfig = areaConfigService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String editSave() throws Exception {
		
		List<AreaConfig> list = areaConfigService.getByNewAreaConfig(areaConfig);
		
		String code = areaConfig.getCode();
		
		if(list != null && list.size() > 0 && !list.get(0).getCode().equals(code)){
			this.msgs.add(getText("syssetting.areaconfig.area.hasexist"));
		}else{
			areaConfig.setAreaInfoList(loadAreaInfoById(ids));	//加载配置的区域
			areaConfigService.updateAreaConfig(areaConfig);
			this.msgs.add(getText("common.js.modSuccess"));
		}
		
		this.urls.put(getText("syssetting.areaconfig.list"), "area-config!list.do");
		return this.MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			areaConfigService.delAreaConfigById(id);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}

		this.urls.put(getText("syssetting.areaconfig.list"), "area-config!list.do");
		return MESSAGE;
	}
	
	private List<AreaInfo> loadAreaInfoById(int [] ids){
		List<AreaInfo> languageList = new ArrayList<AreaInfo>();
		if(ids != null && ids.length > 0){
			for (int i = 0; i < ids.length; i++) {
				languageList.add(areaInfoService.getById(ids[i]));
			}
		}
		return languageList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AreaConfig getAreaConfig() {
		return areaConfig;
	}

	public void setAreaConfig(AreaConfig areaConfig) {
		this.areaConfig = areaConfig;
	}

	public int[] getIds() {
		return ids;
	}

	public void setIds(int[] ids) {
		this.ids = ids;
	}

}
