package com.cheriscon.backstage.system.action;

import java.util.Date;
import javax.annotation.Resource;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.IProductResetLogService;
import com.cheriscon.backstage.util.CallProcedureUtil;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductResetLog;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;

/**
 * @remark	产品恢复出厂设置（即注册后重新注册）action
 * @author chenqichuan	
 * @date   2013-9-26
 */
@ParentPackage("json_default")
@Namespace("/autel/call")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class CallProcedureAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	private String jsonData;
	
	private String name;

	
	@Action(value = "call", results = { @Result(name = "call", location = "/autel/backstage/system/call_procedure.jsp") })	
	public String call() throws Exception{
		return "call";
	}
	@SuppressWarnings("unchecked")
	@Action(value = "refresh")
	public String refresh() throws Exception{
		try{
			CallProcedureUtil.call(name);
			msgs.add("操作成功");
		}catch(Exception e){
			msgs.add("操作失败，请联系管理员");
		}
		this.urls.put("继续操作", "call.do");
		return MESSAGE;
	}
	
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}	
