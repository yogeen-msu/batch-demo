package com.cheriscon.backstage.system.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.IProductResetLogService;
import com.cheriscon.backstage.system.service.IUpLoginIpInfoService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductResetLog;
import com.cheriscon.common.model.UpLoginIpInfo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;

/**
 * @remark	产品升级IP查询
 * @author chenqichuan	
 * @date   2014-02-17
 */
@ParentPackage("json_default")
@Namespace("/autel/product")
public class UpgradeLoginIpAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IAdminUserManager adminUserManager;
    @Resource
    private IUpLoginIpInfoService upLoginIpInfoService;
	
	private String jsonData;
	private UpLoginIpInfo log;
	
	private String code;
    
	private String ipStr;
	
	public String getIpStr() {
		return ipStr;
	}

	public void setIpStr(String ipStr) {
		this.ipStr = ipStr;
	}

	//获取产品重置列表
	public void list(){
		try{
			this.webpage = upLoginIpInfoService.pageUpLoginIpInfo(log,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "upgradeLogin", results = { @Result(name = "upgradeLogin", location = "/autel/backstage/system/product_upgrade_list.jsp") })
	public String upgradeLogin() throws Exception{
		if(log!=null){
			list();
		}
		return "upgradeLogin";
	}
	
	@Action(value = "getIpJson", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getIpJson() throws Exception{
		URLConnection webUrlRequest;
		String code="";
		JSONObject jsonObj2=new JSONObject();
		JSONObject jsonObj=new JSONObject();
		webUrlRequest = webUrlRequest("http://ip.taobao.com/service/getIpInfo.php?ip="+ipStr);
		String jsonString = jsonString(webUrlRequest.getInputStream());
		jsonObj = JSONObject.fromObject(jsonString);
		code=jsonObj.getString("code");
		 if(code.equals("0")){
			    jsonObj2=jsonObj.getJSONObject("data");
			    String ipCountry=jsonObj2.getString("country");
			    jsonData=ipCountry;
		 }else{
			 jsonData="Not Find";
		 }
		//this.jsonData= JSONArray.fromObject(jsonData).toString();
		return SUCCESS;
	}
	
	public  URLConnection webUrlRequest(String requestUrl) throws IOException {
		return new URL(requestUrl).openConnection();
	}
	
	public String jsonString(InputStream inputStream) throws IOException {
		// List<String> list = new ArrayList<String>();
		BufferedReader bufferReader = null;
		try {
			bufferReader = new BufferedReader(
					new InputStreamReader(inputStream));

			String readline;
			while ((readline = bufferReader.readLine()) != null) {
				if (readline.indexOf("code") != -1) {
					return readline.toString();
				}
			}
		} finally {
			
			bufferReader.close();
		}
		return null;

	}
	
	public UpLoginIpInfo getLog() {
		return log;
	}

	public void setLog(UpLoginIpInfo log) {
		this.log = log;
	}

	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}	
