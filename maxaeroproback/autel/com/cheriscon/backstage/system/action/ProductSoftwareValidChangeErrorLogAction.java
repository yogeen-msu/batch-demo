package com.cheriscon.backstage.system.action;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeErrorLogService;
import com.cheriscon.common.model.ProductSoftwareValidChangeErrorLog;
import com.cheriscon.common.model.ProductSoftwareValidChangeLog;
import com.cheriscon.framework.action.WWAction;

@ParentPackage("json_default")
@Namespace("/autel/productSoftwareValidChangeErrorLog")
public class ProductSoftwareValidChangeErrorLogAction extends WWAction {
	private ProductSoftwareValidChangeErrorLog log;
	private String proCode;
	@Resource
	private IProductSoftwareValidChangeErrorLogService errorLogService ;

	public ProductSoftwareValidChangeErrorLogAction() {
		// TODO Auto-generated constructor stub
	}
	
	@Action(value = "list", results = { @Result(name = SUCCESS, location = "/autel/backstage/log/productSoftwareValidChangeErrorLog.jsp") })
	public String list(){
		this.webpage = errorLogService
				.pageErrorLog(log, this.getPage(),
						this.getPageSize());
		return SUCCESS;
	}
	
	
	@SuppressWarnings("unchecked")
	@Action(value = "update")
	public String update(){
		try{
		boolean re = errorLogService.updateSoftwareValid(proCode);
		if(re){
			this.msgs.add("更新成功");
			this.urls.put(getText("common.btn.return"), "list.do");
		}else{
			this.msgs.add("更新失败");
			this.urls.put(getText("common.btn.return"), "list.do");
		}
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add("更新失败");
			this.urls.put(getText("common.btn.return"), "list.do");
		}
		return MESSAGE;
	}

	public ProductSoftwareValidChangeErrorLog getLog() {
		return log;
	}

	public void setLog(ProductSoftwareValidChangeErrorLog log) {
		this.log = log;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}


}
