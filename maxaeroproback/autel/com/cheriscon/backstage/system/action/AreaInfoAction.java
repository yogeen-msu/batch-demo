package com.cheriscon.backstage.system.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.IAreaInfoService;
import com.cheriscon.common.model.AreaInfo;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/areaInfo_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/areaInfo_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/areaInfo_edit.jsp"),
	@Result(name="select_list", location="/autel/backstage/system/areaInfo_select_list.jsp")
})
public class AreaInfoAction extends WWAction{
	
	private int id;
	private AreaInfo areaInfo;
	
	private List<AreaInfo> areaInfoList;
	
	@Resource
	private IAreaInfoService areaInfoService; 
	
	// 显示列表
	public String list() {
		this.webpage = areaInfoService.pageAreaInfo(this.getPage(), this.getPageSize());
		return "list";
	}
	
	// 显示列表
	public String selectlist() {
		areaInfoList = areaInfoService.queryNotSelectAreaInfo();
		return "select_list";
	}
	
	
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		
		List<AreaInfo> list = areaInfoService.getByNewAreaInfo(areaInfo);
		
		if(list == null || list.size() == 0){
			areaInfoService.saveAreaInfo(areaInfo);
			this.msgs.add(getText("common.js.addSuccess"));
		}else{
			this.msgs.add(getText("syssetting.areaconfig.areahasexist"));
		}
		
		this.urls.put(getText("syssetting.areaconfig.arealist"), "area-info!list.do");
		
		return MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		areaInfo = areaInfoService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		
		List<AreaInfo> list = areaInfoService.getByNewAreaInfo(areaInfo);
		
		if(list != null && list.size() > 0){
			this.msgs.add(getText("syssetting.areaconfig.areahasexist"));
		}else{
			areaInfoService.updateAreaInfo(areaInfo);
			this.msgs.add(getText("common.js.modSuccess"));
		}
		
		this.urls.put(getText("syssetting.areaconfig.arealist"), "area-info!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			areaInfoService.delAreaInfoById(id);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail"));
		}

		this.urls.put(getText("syssetting.areaconfig.arealist"), "area-info!list.do");
		return MESSAGE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AreaInfo getAreaInfo() {
		return areaInfo;
	}

	public void setAreaInfo(AreaInfo areaInfo) {
		this.areaInfo = areaInfo;
	}

	public List<AreaInfo> getAreaInfoList() {
		return areaInfoList;
	}

	public void setAreaInfoList(List<AreaInfo> areaInfoList) {
		this.areaInfoList = areaInfoList;
	}

}
