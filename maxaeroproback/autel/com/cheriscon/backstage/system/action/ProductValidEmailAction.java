package com.cheriscon.backstage.system.action;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.IProductResetLogService;
import com.cheriscon.backstage.system.service.IProductValidEmailService;
import com.cheriscon.backstage.util.CallProcedureUtil;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductResetLog;
import com.cheriscon.common.model.ProductValidEmail;
import com.cheriscon.common.model.ToCustomerCharge;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;

/**
 * @remark 产品到期邮件提醒功能
 * @author chenqichuan
 * @date 2013-10-23
 */

public class ProductValidEmailAction extends WWAction {

	private static final long serialVersionUID = -6977843643579742427L;
	@Resource
	private IToCustomerChargeService toCustomerChargeService;

	@Resource
	private ISmtpManager smtpManager;

	@Resource
	private EmailProducer emailProducer;

	@Resource
	private IEmailTemplateService emailTemplateService;
	@Resource
	private IToCustomerChargeService chargeService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private IProductValidEmailService productValidService;
	
	public void productValidTaskExec() {

	}

	private void loadDefaultContext() {
		CopContext context = new CopContext();
		CopSite site = new CopSite();
		site.setUserid(1);
		site.setId(1);
		site.setThemeid(1);
		context.setCurrentSite(site);
		CopContext.setContext(context);

		ISiteManager siteManager = SpringContextHolder.getBean("siteManager");
		site = siteManager.get("localhost");
		context.setCurrentSite(site);
		CopContext.setContext(context);
	}
	
	
	@SuppressWarnings("unchecked")
	public void sendEmailTask() {
		try {
			String date = DateUtil.toString(new Date(), "yyyy-MM-dd");
			System.out.println("==========开始执行邮件提醒============"+ DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			List<ToCustomerCharge> list = chargeService.queryValidForEmail();
			for (int i = 0; i < list.size(); i++) {
				ToCustomerCharge customerCharge=list.get(i);
				try {
					loadDefaultContext();
					EmailModel emailModel = new EmailModel();
					String email = customerCharge.getAutelId();

					String languageCode = customerCharge.getLanguageCode();
                    
					Language language = languageService.getByCode(languageCode);
					
					String htmlStr="";
					if(DateUtil.compareDate2(DateUtil.toDate(customerCharge.getValidDate(), "yyyy-MM-dd"), DateUtil.toDate(date, "yyyy-MM-dd"))){
						htmlStr=FreeMarkerPaser.getBundleValue("welcome.warn.msg",language.getLanguageCode(),language.getCountryCode());
						htmlStr=htmlStr.replace("{0}", customerCharge.getValidDate());
						htmlStr=htmlStr.replace("{1}", customerCharge.getProductName());
						htmlStr=htmlStr.replace("{2}", customerCharge.getSerialNo());
					}
					if(language != null)
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("product.valid.email.mailtitle",
								language.getLanguageCode(),language.getCountryCode()));
					}
					else 
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("product.valid.email.mailtitle"));
					}
					String userName="";
					if(customerCharge!=null && customerCharge.getCustomerName()!=null){
					  userName=customerCharge.getCustomerName();
					}
					emailModel.getData().put("autelId", email);
					emailModel.getData().put("userName", userName);
					emailModel.getData().put("htmlStr", htmlStr);
					emailModel.setTo(email);
					int	emailTemplateType = CTConsatnt.EMAIL_TEMPLATE_TYEP_PRODUCT_VALID;
					EmailTemplate template = emailTemplateService
							.getUseTemplateByTypeAndLanguage(emailTemplateType,
									languageCode);
					if(FrontConstant.getPropertValue("autelproweb.email.url").equals("http://pro.auteltech.com")){
						emailModel.setTemplate(template.getCode() + ".html"); // 此处一定需要这样写
						emailProducer.send(emailModel);
						// 记录发送日志
						saveLog(customerCharge.getProCode(),"1");
					}
				} catch (Exception e) {
					e.printStackTrace();
					saveLog(customerCharge.getProCode(),"0");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveLog(String proCode,String flag){
		String year = DateUtil.toString(new Date(), "yyyy");
		ProductValidEmail productEmail=new ProductValidEmail();
		productEmail.setCreateTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
		productEmail.setYear(year);
		productEmail.setProCode(proCode);
		productEmail.setFlag(flag);
		try {
			productValidService.saveProductValidEmail(productEmail);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
