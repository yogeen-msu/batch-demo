package com.cheriscon.backstage.system.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.ILanguageConfigService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.LanguageConfig;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/language_config_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/language_config_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/language_config_edit.jsp")
})
public class LanguageConfigAction extends WWAction{
	
	private int id;
	private LanguageConfig languageConfig;
	
	private int [] lanIds;			//配置语言id
	
	@Resource
	private ILanguageService languagService; 
	@Resource
	private ILanguageConfigService languageConfigService; 
	
	// 显示列表
	public String list() {
		this.webpage = languageConfigService.pageLanguageConfig(languageConfig,this.getPage(), this.getPageSize());
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String addSave(){
		
		List<LanguageConfig> list = languageConfigService.getByNewLanguageConfig(languageConfig);
		
		if(list == null || list.size() == 0){
			languageConfig.setLanguageList(loadLanguageById(lanIds));	//加载配置的语言
			languageConfigService.saveLanguageConfig(languageConfig);
			this.msgs.add(getText("common.js.addSuccess"));
		}else{
			this.msgs.add(getText("syssetting.languageconfig.addfile"));
		}
		
		
		this.urls.put(getText("syssetting.languageconfig.list"), "language-config!list.do");
		
		return this.MESSAGE;
	}
	

	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		languageConfig = languageConfigService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String editSave() throws Exception {
		
		List<LanguageConfig> list = languageConfigService.getByNewLanguageConfig(languageConfig);
		
		String code = languageConfig.getCode();
		if(list != null && list.size() > 0 && !list.get(0).getCode().equals(code)){
			this.msgs.add(getText("syssetting.languageconfig.updatefile"));
		}else{
			languageConfig.setLanguageList(loadLanguageById(lanIds));	//加载配置的语言
			languageConfigService.updateLanguageConfig(languageConfig);
			this.msgs.add(getText("common.js.modSuccess"));
		}
		
		
		this.urls.put(getText("syssetting.languageconfig.list"), "language-config!list.do");
		return this.MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			languageConfigService.delLanguageConfigById(id);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail"));
		}

		this.urls.put(getText("syssetting.languageconfig.list"), "language-config!list.do");
		return MESSAGE;
	}
	
	private List<Language> loadLanguageById(int [] ids){
		List<Language> languageList = new ArrayList<Language>();
		if(lanIds != null && lanIds.length > 0){
			for (int i = 0; i < ids.length; i++) {
				languageList.add(languagService.getById(ids[i]));
			}
		}
		return languageList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LanguageConfig getLanguageConfig() {
		return languageConfig;
	}

	public void setLanguageConfig(LanguageConfig languageConfig) {
		this.languageConfig = languageConfig;
	}

	public int[] getLanIds() {
		return lanIds;
	}

	public void setLanIds(int[] lanIds) {
		this.lanIds = lanIds;
	}

	
}
