package com.cheriscon.backstage.system.action;

import java.util.List;

import javax.annotation.Resource;

import com.cheriscon.backstage.system.service.ISealerDataAuthService;
import com.cheriscon.common.model.SealerDataAuth;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
public class SealerDataAuthAction extends WWAction {

	@Resource
	private ISealerDataAuthService sealerDataAuthService;

	private SealerDataAuth sealerDataAuth;

	private List<SealerDataAuth> sealerDataAuths;

	public String list() {

		try {
			if (sealerDataAuth != null) {

				sealerDataAuths = sealerDataAuthService
						.getTreeBySealerDataAuthName(sealerDataAuth.getName());

			} else {
				sealerDataAuths = sealerDataAuthService
						.getSealerDataAuthTree("0");
			}

		} catch (Exception e) {
		}
		return "list";
	}
	
	public String add() {

		try {
			sealerDataAuths = sealerDataAuthService.getSealerDataAuthTree("0");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "add";
	}
	
	public String saveAdd(){
		try{
		    sealerDataAuthService.addSealerDataAuth(sealerDataAuth);
			this.json="{result:1}";
			 
		}catch(Exception e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}
		return this.JSON_MESSAGE;
	}

	public String edit(){

		try {
			sealerDataAuths = sealerDataAuthService.getSealerDataAuthTree("0");
			sealerDataAuth = sealerDataAuthService.getSealerDataAuthByCode(sealerDataAuth.getCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "edit";
	}
	
	public String saveEdit(){
		try{
			sealerDataAuthService.editSealerDataAuth(sealerDataAuth);
			this.json="{result:1}";
			 
		}catch(Exception e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}
		return this.JSON_MESSAGE;
	}
	
	public String delete(){
		try{
			if(sealerDataAuthService.getSealerDataAuths(sealerDataAuth.getCode(), null).size() > 0)
			{
				this.json="{result:3}";
			}
			else
			{
				sealerDataAuthService.deleteSealerDataAuth(sealerDataAuth.getCode());
				this.json="{result:1}";
			}
		}catch(Exception e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}		
		return this.JSON_MESSAGE;
	}
 
	public SealerDataAuth getSealerDataAuth() {
		return sealerDataAuth;
	}

	public void setSealerDataAuth(SealerDataAuth sealerDataAuth) {
		this.sealerDataAuth = sealerDataAuth;
	}

	public List<SealerDataAuth> getSealerDataAuths() {
		return sealerDataAuths;
	}

	public void setSealerDataAuths(List<SealerDataAuth> sealerDataAuths) {
		this.sealerDataAuths = sealerDataAuths;
	}

}
