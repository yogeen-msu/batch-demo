package com.cheriscon.backstage.system.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.utils.JSONHelpUtils;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;

@SuppressWarnings("serial")
@ParentPackage("json-default")
@Namespace("/front/user")
public class SystemInterfaceAction extends WWAction{
	
	/**
	 * 用于系统平台部在线编程用户验证接口
	 * chenqichuan
	 * 2014-10-28
	 * ***/
	private String returnVal="false";
	private String userName="";   //姓名
	private String sex="";       //性别
	private String email="";    //电子邮件
	private String telphone=""; //电话
	private String autelid="";
	private static String REGISTER_URL="http://pro.auteltech.com/regCustomerInfo.html?operationType=1&m=1";
	private static String REGISTER_PRO_URL="http://pro.auteltech.com/regProduct.html?m=1";
	
	public String getReturnVal() {
		return returnVal;
	}
	public void setReturnVal(String returnVal) {
		this.returnVal = returnVal;
	}

	@Resource
	private ICustomerInfoService customerInfoService; 
	@Resource
	private IProductInfoService productInfoService;
	
	//根据AutelID和密码验证客户信息是否存在
	@Action(value = "loginByAutelIdAndPwd")
	public String loginByAutelIdAndPwd() {
		String autelId=getRequest().getParameter("autelId");
		String password=getRequest().getParameter("password");
		String url="";
		if(!StringUtil.isEmpty(autelId)){
			try {
				CustomerInfo customer=new CustomerInfo();
				customer=customerInfoService.getCustomerInfoByAutelId(autelId);
				if(customer==null){
					returnVal="-2";
					url=REGISTER_URL;
				}else{
					if(!customer.getUserPwd().equals(password)){
						returnVal="-1";
					}else{
						userName=customer.getName();
						email=customer.getAutelId();
						telphone=customer.getDaytimePhoneCC()+customer.getDaytimePhoneAC()+customer.getDaytimePhone();
						returnVal="0";
					}
						
					
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("returnVal", returnVal);
		map.put("url", url);
		map.put("autelId", autelId);
		map.put("userName", userName);
		map.put("email", email);
		map.put("telphone", telphone);
		
		JSONObject obj=JSONObject.fromObject(map);
		try {
			getResponse().getWriter().write(JSONHelpUtils.getJsonString4JavaPOJO(obj));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	//根据序列号和产品注册密码判断客户信息是否存在
	@Action(value = "loginBySerialAndPwd")
	public String loginBySerialAndPwd() {
		String serialNo=getRequest().getParameter("serialNo");
		String password=getRequest().getParameter("password");
		String url="";
		if(!StringUtil.isEmpty(serialNo)){
			try {
				ProductInfo product=productInfoService.getBySerialNoAndPwd(serialNo,password);
				if(product==null){
					returnVal="-1";
				}else{
					if(!product.getRegStatus().equals(FrontConstant.PRODUCT_REGSTATUS_YES)){
						returnVal="-2";
						url=REGISTER_PRO_URL;
					}else{
						CustomerInfo customer=new CustomerInfo();
						customer=customerInfoService.getCustBySerialNo(serialNo,password);
						if(customer==null){
							returnVal="-1";
						}else{
							autelid=customer.getAutelId();
							userName=customer.getName();
							email=customer.getAutelId();
							telphone=customer.getDaytimePhoneCC()+customer.getDaytimePhoneAC()+customer.getDaytimePhone();
							returnVal="0";
						}
					}
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("returnVal", returnVal);
		map.put("autelId", autelid);
		map.put("userName", userName);
		map.put("url", url);
		map.put("email", email);
		map.put("telphone", telphone);
		JSONObject obj=JSONObject.fromObject(map);
		try {
			getResponse().getWriter().write(JSONHelpUtils.getJsonString4JavaPOJO(obj));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	//判断AutelID是否存在
	@Action(value = "autelIdCheck")
	public String autelIdCheck() {
		String autelId=getRequest().getParameter("autelId");
		String url="";
		if(!StringUtil.isEmpty(autelId)){
			try {
				CustomerInfo customer=new CustomerInfo();
				customer=customerInfoService.getCustomerInfoByAutelId(autelId);
				
				if(customer==null){
					returnVal="-2";
					url=REGISTER_URL;
				}else{
					userName=customer.getName();
					email=customer.getAutelId();
					telphone=customer.getDaytimePhoneCC()+customer.getDaytimePhoneAC()+customer.getDaytimePhone();
					returnVal="0";
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("returnVal", returnVal);
		map.put("autelId", autelId);
		map.put("url", url);
		map.put("userName", userName);
		map.put("email", email);
		map.put("telphone", telphone);
		JSONObject obj=JSONObject.fromObject(map);
		/*getResponse().setContentType("text/json;charset=UTF-8");
		getResponse().setCharacterEncoding("UTF-8");*/
		try {
			getResponse().getWriter().write(JSONHelpUtils.getJsonString4JavaPOJO(obj));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
