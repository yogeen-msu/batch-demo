package com.cheriscon.backstage.system.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.stereotype.Controller;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.system.service.ISoftListService;
import com.cheriscon.common.model.SoftList;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.constant.FrontConstant;

@SuppressWarnings("serial")
@Controller
@ParentPackage("cop_default")
@Namespace("/autel/ProductSoftSearch")
@Results({
	@Result(name="list",location="/autel/backstage/productSoftSearch/productSoftSearch.jsp")
})
public class ProductSoftSearchAction extends WWAction {
	
	@Resource
	private ISoftListService softListService;
	@Resource
	private IAdminUserManager adminUserManager;
	
	private List<SoftList> record;
	private SoftList softList;

	//经销商根据产品序列号获得客户的升级软件20140212
	public String getUpgradeSoftList(){
		
		try {
			if(softList!=null){
				String languageCode=FrontConstant.getPropertValue("languageCode");
				record = softListService.getSoftList(softList, languageCode,"1");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "list";
	}

	public List<SoftList> getRecord() {
		return record;
	}

	public void setRecord(List<SoftList> record) {
		this.record = record;
	}

	public SoftList getSoftList() {
		return softList;
	}

	public void setSoftList(SoftList softList) {
		this.softList = softList;
	}
	
}
