package com.cheriscon.backstage.system.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.IMinSaleUnitSaleCfgDetailService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.system.service.IMaxiSysToProErrorLogService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductForProService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.MaxiSysToProErrorLog;
import com.cheriscon.common.model.MinSaleUnitSaleCfgDetail;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductForPro;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;

@ParentPackage("json-default")
@Namespace("/front/user")
public class MaxiSysToProInterfaceAction extends WWAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1911889414615605282L;
	@Resource
	private IAdminUserManager adminUserManager;
	@Resource
	private ICustomerInfoService customerInfoService; 
	@Resource
	private IProductInfoService productInfoService; 
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IProductSoftwareValidStatusService validService;
	@Resource
	private IProductContractLogService productContractService;
	@Resource
	private IMinSaleUnitSaleCfgDetailService minSaleUnitSaleCfgDetailService;
	@Resource
	private IProductForProService productForProService;
	@Resource
	private IMaxiSysToProErrorLogService errorLogService;
	
	private List<SaleContract> saleContracts;
	
	private String jsonData;
	public String getJsonData() {
		return jsonData;
	}
	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	
	/**
	 * 20160429 A16043 hlh
	 * 在原来诊断产品网站提供给生产管理系统调用上传产品的Action的基础上做进一步修改，以用于无人机产品的上传
	 * @return
	 */
	@Action(value = "InsertProductInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String InsertProductInfo() {		
		System.out.println("无人机发布产品======"+getRequest().getParameter("serialNo"));
		String serialNo=getRequest().getParameter("serialNo"); //产品序列号		
		String saleContractCode=getRequest().getParameter("saleContractCode"); //销售契约
		String aricraftSerialNumber=getRequest().getParameter("AricraftSerialNumber"); //飞机序列号
		String imuSerialNumber=getRequest().getParameter("ImuSerialNumber"); //云台序列号
		String remoteControlSerialNumber=getRequest().getParameter("RemoteControlSerialNumber"); //遥控器序列号
		String batterySerialNumber=getRequest().getParameter("batterySerialNumber"); //电池序列号
		String gimbalMac=getRequest().getParameter("GimbalMac"); //云台MAC地址(路由地址)
		String remoteControlMac=getRequest().getParameter("RemoteControlMac"); //遥控器MAC地址(中继地址)
		try{
			ProductInfo product = null ;
			
			//校验主序列号
			if(null!=serialNo && (!serialNo.trim().equals(""))){
				 product=productInfoService.getBySerialNo(serialNo);
				if(product!=null){
					jsonData = "E51|飞机主序列号已经存在，发布失败";
					return SUCCESS;
				}
			}else{
				jsonData = "E52|飞机主序列号为空，发布失败";
				return SUCCESS;
			}
			//校验飞机序列号
			if(null!=aricraftSerialNumber && (!aricraftSerialNumber.trim().equals(""))){
			    product=productInfoService.getByAricraftSerialNumber(aricraftSerialNumber);
				if(product!=null){
					jsonData = "E53|飞机序列号已经存在，发布失败";
					return SUCCESS;
				}
			}
			//校验云台序列号
			if(null!=imuSerialNumber && (!imuSerialNumber.trim().equals(""))){
			     product=productInfoService.getByImuSerialNumber(imuSerialNumber);
				if(product!=null){
					jsonData = "E54|云台序列号已经存在，发布失败";
					return SUCCESS;
				}
			}
			//校验遥控器序列号
			if(null!=remoteControlSerialNumber && (!remoteControlSerialNumber.trim().equals(""))){
				product=productInfoService.getByRemoteControlSerialNumber(remoteControlSerialNumber);
				if(product!=null){
					jsonData = "E55|遥控器序列号已经存在，发布失败";
					return SUCCESS;
				}
			}
			//校验序列号
			if(null!=batterySerialNumber && (!batterySerialNumber.trim().equals(""))){
				product=productInfoService.getByBatterySerialNumber(batterySerialNumber);
				if(product!=null){
					jsonData = "E56|电池序列号已经存在，发布失败";
					return SUCCESS;
				}
			}
			//校验云台MAC地址(路由地址)
			if(null!=gimbalMac && (!gimbalMac.trim().equals(""))){
				product=productInfoService.getByGimbalMac(gimbalMac);
				if(product!=null){
					jsonData = "E57|路由地址已经存在，发布失败";
					return SUCCESS;
				}
			}
			//校验遥控器MAC地址(中继地址)
			if(null!=remoteControlMac && (!remoteControlMac.trim().equals(""))){
				product=productInfoService.getByRemoteControlMac(remoteControlMac);
				if(product!=null){
					jsonData = "E58|中继地址已经存在，发布失败";
					return SUCCESS;
				}
			} 
			//校验销售契约
			SaleContract  sale= null;
			if(null!=saleContractCode && (!saleContractCode.trim().equals(""))){
				sale=saleContractService.getSaleContractByCode(saleContractCode);
				if(sale==null){
					jsonData = "E59|销售契约不存在，发布失败:"+saleContractCode;
					return SUCCESS;
				}
			}else{
				jsonData = "E60|销售契约为空，发布失败";
				return SUCCESS;
			} 
			
			product=new ProductInfo();
			product.setSerialNo(serialNo);
			product.setSaleContractCode(saleContractCode);
			product.setAricraftSerialNumber(aricraftSerialNumber);
			product.setImuSerialNumber(imuSerialNumber);
			product.setRemoteControlSerialNumber(remoteControlSerialNumber);
			product.setBatterySerialNumber(batterySerialNumber);
			product.setGimbalMac(gimbalMac);
			product.setRemoteControlMac(remoteControlMac);
			product.setProDate(DateUtil.toString(new Date(), "YYYY-MM-dd"));
			product.setProTypeCode(sale.getProTypeCode());
			productInfoService.saveProductInfo(product);
			jsonData = "S01|SUCCESS";
			return SUCCESS;
		}catch(Exception e){
			e.getMessage();
			jsonData = "E03|系统异常";
			logger.error(e.getMessage(), e);
		}
		
		return SUCCESS;
	}
	
	
	
	/***
	 * 返回jason格式，returnStr
	 * 0001 J2534号码为空
	 * 0002 出厂日期为空
	 * 0003 产品类型为空
	 * 0004 绑定日期不能为空
	 * 0005 J2534已经存在
	 * 0006 产品序列号不存在
	 * 0007 产品类型必须是MaxiSys或者MaxiSys Pro
	 * 0008 J2534已经绑定到其他序列号
	 * 0009 绑定失败
	 * OK 返回正常
	 * ***/
	
	@Action(value = "InsertJ2534", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String InsertJ2534() {
		String serialNo=getRequest().getParameter("serialNo"); //产品序列号
		String J2534=getRequest().getParameter("J2534"); //J2534序列号
		String proDate=getRequest().getParameter("proDate"); //出厂日期
		String type=getRequest().getParameter("type"); //产品类型
		String regDate=getRequest().getParameter("regDate"); //绑定日期
		ProductForPro pro=null ;
		if(StringUtil.isEmpty(J2534)){ 
			jsonData = "0001";
			return SUCCESS;
		}
		if(StringUtil.isEmpty(proDate)){
			jsonData = "0002";
			return SUCCESS;
		}
		if(StringUtil.isEmpty(type)){
			jsonData = "0003";
			return SUCCESS;
		}
		if(!StringUtil.isEmpty(serialNo)&&StringUtil.isEmpty(regDate)){
			jsonData = "0004";
			return SUCCESS;
		}
		try {
			pro = productForProService.getProductForProBySerialNo(J2534);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		if(StringUtil.isEmpty(serialNo)){  //如果未传序列号的话
			if(pro==null){ 
				try {
					    pro=new ProductForPro();
				    	pro.setSerialNo(J2534);
				    	pro.setProDate(proDate);
				    	pro.setType(Integer.parseInt(type));
				    	productForProService.addProductForPro(pro);
				    	jsonData = "OK";
						return SUCCESS;
				 } catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				jsonData = "0005";
				return SUCCESS;
			}
		}
		if(!StringUtil.isEmpty(serialNo) && !StringUtil.isEmpty(regDate)){
			ProductInfo info =productInfoService.getBySerialNo(serialNo);
			if(info==null){
				jsonData = "0006";
				return SUCCESS;
			}
			try {
				String MaxiSYS = this.getPropertValue("product.type.maxisys");
				String MaxiSYS_Pro =this.getPropertValue("product.type.maxisyspro");
				String MaxiSys_Mini=this.getPropertValue("product.type.maxisysmini");
				if(!MaxiSys_Mini.equals(info.getProTypeCode()) && !MaxiSYS.equals(info.getProTypeCode()) && !MaxiSYS_Pro.equals(info.getProTypeCode())){
					jsonData = "0007";
					return SUCCESS;
				}
				if(pro==null){
					pro=new ProductForPro();
					pro.setSerialNo(J2534);
			    	pro.setProDate(proDate);
			    	pro.setType(Integer.parseInt(type));
			    	productForProService.addProductForPro(pro);
				}else{
					if(pro.getRegStatus()==FrontConstant.PRODUCTFORPRO_STATUS_BOUND_YES){
						jsonData = "0008";
						return SUCCESS;
					}
				}	
				
				if(info.getProTypeCode().equals(MaxiSYS)){
			    	if(getNewSealer(info,MaxiSYS_Pro,pro.getType())){
			    		
			    		pro=productForProService.getProductForProByCode(pro.getCode());
						Map<String,Object> fileds=new HashMap<String,Object>();
						fileds.put("id", pro.getId());
						fileds.put("regStatus", FrontConstant.PRODUCTFORPRO_STATUS_BOUND_YES);
						fileds.put("regTime",regDate);
						fileds.put("reason", "生产管理系统绑定");
						fileds.put("proCode",info.getSerialNo());
						productForProService.updateProductForProByMap(fileds);
		                //更新productInfo ProTypeCode 为MaxiSYS Pro
						if (MaxiSYS.equals(info.getProTypeCode()) && pro.getType()==2) {
							info.setSaleContractCode(saleContracts.get(0).getCode());
							info.setProTypeCode(MaxiSYS_Pro);
							productInfoService.updateProductInfo(info);
						}
						jsonData = "OK";
						return SUCCESS;
					}else{
						jsonData = "0009";
						return SUCCESS;
					}
			 }else{
				    pro=productForProService.getProductForProByCode(pro.getCode());
					Map<String,Object> fileds=new HashMap<String,Object>();
					fileds.put("id", pro.getId());
					fileds.put("regStatus", FrontConstant.PRODUCTFORPRO_STATUS_BOUND_YES);
					fileds.put("regTime",regDate);
					fileds.put("reason", "生产管理系统绑定");
					fileds.put("proCode",info.getSerialNo());
					productForProService.updateProductForProByMap(fileds);
					jsonData = "OK";
					return SUCCESS;
			 }
				
			} catch (Exception e) {
				e.printStackTrace();
				jsonData = "0009";
				return SUCCESS;
			}
		}
		
		
		return null;
	}
	

	/***
	 * 返回jason格式，returnStr
	 * 0001 产品序列号不存在
	 * 0002 产品密码不匹配
	 * 0003 产品类型不匹配
	 * 0006 参数格式不正确
	 * 0009 系统异常
	 * true 返回正常
	 * ***/
	
	/*@Action(value = "CheckProductType", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })*/
	public String CheckProductType() {
		String returnStr="0009";
		String params=com.cheriscon.common.utils.DesModule.getInstance().decrypt(getRequest().getQueryString());
		if(params!=null && params.contains("serialNo") && params.contains("RegPwd")){
			String [] ars=params.split("&");
			String serialNo=ars[0].substring(ars[0].indexOf("=")+1);
			String password=ars[1].substring(ars[1].indexOf("=")+1);
			ProductInfo info =productInfoService.getBySerialNoAndPwd(serialNo,password);
			if(info==null){
				returnStr="0001";   //产品序列号不存在
			}
			/*else if(info!=null && !info.getRegPwd().equals(password)){
				returnStr="0002";  //产品序列号密码不正确
			}*/else{
				try {
					//判断产品类型是不是maxisys
					String MaxiSYS = this.getPropertValue("product.type.maxisys");
					if(!MaxiSYS.equals(info.getProTypeCode())){
						returnStr="0003";  //产品类型必须是MaxiSys
					}else{
						returnStr="true";
					}
				} catch (IOException e) {
					e.printStackTrace();
				} 
			}
		}else{
			returnStr="0006";  //参数不正确
		}
		
		Map<String,String> returnMap=new HashMap<String,String>();
		returnMap.put("returnValue", returnStr);
		jsonData = JSONArray.fromObject(returnMap).toString();
		return SUCCESS;
	}
	
	
	/***
	 * 返回jason格式，returnStr
	 * 0001 产品序列号不存在
	 * 0002 产品密码不匹配
	 * 0003 产品类型不匹配
	 * 0004 J2534 账号已经使用
	 * 0005 产品对应的经销商没有对应的MaxiSys Pro 配置
	 * 0006 参数格式不正确
	 * 0009 系统异常
	 * true 绑定成功
	 * ***/
	
	/*@Action(value = "MaxiSysToPro", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })*/
	public String MaxiSysToPro() {
		
		String params=com.cheriscon.common.utils.DesModule.getInstance().decrypt(getRequest().getQueryString());
		String returnStr="0009";
		Map<String,String> returnMap=new HashMap<String,String>();
		if(params!=null && params.contains("serialNo") && params.contains("RegPwd") && params.contains("J2534Code")){
			String [] ars=params.split("&");
			String serialNo=ars[0].substring(ars[0].indexOf("=")+1);
			String password=ars[1].substring(ars[1].indexOf("=")+1);
			String J2534Code=ars[2].substring(ars[2].indexOf("=")+1);
			ProductInfo info =productInfoService.getBySerialNoAndPwd(serialNo,password);
			if(info==null){
				returnStr="0001";   //产品序列号不存在
			}
			/*else if(info!=null && !info.getRegPwd().equals(password)){
				returnStr="0002";  //产品序列号密码不正确
			}*/
			else{ 
				try {
					String MaxiSYS = this.getPropertValue("product.type.maxisys"); //判断产品类型是不是maxisys
					String MaxiSYS_Pro =this.getPropertValue("product.type.maxisyspro");
					if (MaxiSYS.equals(info.getProTypeCode())){
						ProductForPro pro=productForProService.getProductForProBySerialNo(J2534Code);
						if(pro!=null){ //判断J2534盒子是否存在
							
							if(pro.getRegStatus()==0){
								if(getNewSealer(info,MaxiSYS_Pro,2)){
									Map<String,Object> fileds=new HashMap<String,Object>();
									fileds.put("id", pro.getId());
									fileds.put("regStatus", FrontConstant.PRODUCTFORPRO_STATUS_BOUND_YES);
									fileds.put("regTime", DateUtil.toString(new Date(),
											"yyyy-MM-dd HH:mm:ss"));
									fileds.put("reason", "系统平台部自动绑定");
									fileds.put("proCode",info.getSerialNo());
									productForProService.updateProductForProByMap(fileds);
					                //更新productInfo ProTypeCode 为MaxiSYS Pro
									if (MaxiSYS.equals(info.getProTypeCode())) {
										info.setSaleContractCode(saleContracts.get(0).getCode());
										info.setProTypeCode(MaxiSYS_Pro);
										productInfoService.updateProductInfo(info);
									}
									returnStr="true";
								}else{
									returnStr="0005";  //经销商对应的MaxiSys Pro销售契约不存在
								}
							}else{
								returnStr="0004"; //J2534已经绑定到其他账号
							}
						}
					}else{
						returnStr="0003";  //产品类型必须是MaxiSys
					}
					
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					returnStr="0009";
					e.printStackTrace();
				}
			}
			
		}else{
			returnStr="0006";  //参数不正确
		}
		if(!returnStr.equals("true")){
			MaxiSysToProErrorLog log=new MaxiSysToProErrorLog();
			log.setCreateDate(DateUtil.toString(new Date(), "YYYY-MM-dd HH:mm:ss"));
			log.setErrorCode(returnStr);
			log.setType("MaxiSysToPro");
			log.setDesUrl(getRequest().getQueryString());
			log.setUrl(params);
			errorLogService.insertLog(log);
		}
		
		
		returnMap.put("returnValue", returnStr);
		jsonData = JSONArray.fromObject(returnMap).toString();
		return SUCCESS;
		}
	/**
	 * 从配置文件获取SaleCfgCode
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public String getPropertValue(String key) throws IOException{
		InputStream in = FileUtil.getResourceAsStream("config.properties");
		Properties properties = new Properties();
		properties.load(in);
		return properties.getProperty(key);
	}
	
	/**
	 * 获得新的销售契约
	 * @param productInfo
	 * @throws Exception
	 */
	public boolean getNewSealer(ProductInfo productInfo,String MaxiSYS_Pro,int type) throws Exception{
		 
		if(productInfo.getProTypeCode().equals(MaxiSYS_Pro)){
			return true;
		}
		if(type!=2){  //如果不是J2534盒子，直接返回
			return true;
		}
		
		//获取对应销售契约信息
		ProductInfo productInfoSale= productInfoService.getBySerialNo3(productInfo.getSerialNo());
		SaleContract saleContract=new SaleContract();
		saleContract.setSealerCode(productInfoSale.getSealerAutelId());
		saleContract.setLanguageCfgCode(productInfoSale.getSaleContractLanguage());
		saleContract.setProTypeCode(MaxiSYS_Pro);
		String SaleCfgCode=getPropertValue(productInfoSale.getSaleContractCfg());
		saleContract.setSaleCfgCode(SaleCfgCode);
		saleContracts=saleContractService.getContrctBySealer(saleContract);
		if(SaleCfgCode==null||saleContracts==null||saleContracts.isEmpty()){
			return false;
		}else{
			//更新日志
			ProductContractChangeLog productContractChangeLog=new ProductContractChangeLog();
			productContractChangeLog.setSendEmailFlag("N");
			//獲得ip
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			productContractChangeLog.setOperatorIp(FrontConstant.getIpAddr(request));
			productContractChangeLog.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
/*			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());*/
			productContractChangeLog.setOperatorUser("chenqichuan");
			productContractChangeLog.setProductSN(productInfo.getSerialNo());
			productContractChangeLog.setOldContract(productInfo.getSaleContractCode());
			productContractChangeLog.setNewContract(saleContracts.get(0).getCode());
			productContractService.saveLog(productContractChangeLog);
			
			
			//更新有效期表
			List<ProductSoftwareValidStatus> valids=validService.getProductSoftwareValidStatusList(productInfo.getCode());
			String validDate=null;
			if(valids!=null&&valids.size()>0){
				validDate=valids.get(0).getValidDate();
			}else{
				return true;
			}
			//先清除原来数据
			validService.deleteByProCode(productInfo.getCode());
			
			List<MinSaleUnitSaleCfgDetail> minSaleUnitSaleCfgDetails= minSaleUnitSaleCfgDetailService
					.getMinSaleUnitSaleCfgListByCode(saleContracts.get(0).getSaleCfgCode());
			ProductSoftwareValidStatus productSoftwareValidStatus;
			//插入新数据
			for(int i=0;i<minSaleUnitSaleCfgDetails.size();i++){
			    productSoftwareValidStatus=new ProductSoftwareValidStatus();
			    productSoftwareValidStatus.setProCode(productInfo.getCode());
			    productSoftwareValidStatus.setValidDate(validDate);
			    productSoftwareValidStatus.setMinSaleUnitCode(minSaleUnitSaleCfgDetails.get(i).getMinSaleUnitCode());
			    validService.saveProductSoftwareValidStatus(productSoftwareValidStatus);
			}
		
		}
		return true;
		 
	}
	
	}
	
