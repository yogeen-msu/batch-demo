package com.cheriscon.backstage.system.action;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IToolDownloadService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.ToolDownload;
import com.cheriscon.common.utils.JsonHelper;
import com.cheriscon.cop.sdk.utils.UploadUtil;
import com.cheriscon.framework.action.WWAction;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/toolDownload_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/toolDownload_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/toolDownload_edit.jsp")
})
public class ToolDownloadAction extends WWAction{
	
	private String code;
	private ToolDownload toolDownload;
	
	private String languageCode;			//多语言的编码 多个用逗号分开
	private String languageName;			//多语言的名称 多个用逗号分开
	
	private String productTypeCode;			//适应的产品型号多个用逗号分开
	
	private List<Language>	languageList;
	private List<ProductForSealer> productTypeList;
	private List<ToolDownload> toolDownloadList;
	private List<Language>	editLanList;		//现有的语言列表
	private List<ProductType> editProductTypeList;		//适应的产品型号
	
	private File tool;				//下载工具
	private String toolFileName;	//下载工具名称
	
	@Resource
	private IToolDownloadService toolDownloadService; 
	@Resource
	private IProductTypeService productTypeService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private IProductForSealerService productForSealerService;
	
	// 显示列表
	@SuppressWarnings("unchecked")
	public String list() {
		try {
			//productTypeList = productTypeService.listAll();
			productTypeList=productForSealerService.getProductForSealerList();
			this.webpage = toolDownloadService.pageToolDownload(toolDownload,this.getPage(), this.getPageSize());
			List<ToolDownload> toolDownloadList = (List<ToolDownload>)this.webpage.getResult();
			
			for (ToolDownload tool : toolDownloadList) {
				
				String path = UploadUtil.replacePath(tool.getDownloadPath());
				tool.setDownloadPath(path);
				
				//解析JSON格式存储的语言
				String toolName = tool.getToolName();
				String names = "";
				if(StringUtils.isNotEmpty(toolName)){
					Map<String,String> nameMap = JsonHelper.fromJson(toolName,new TypeToken<Map<String,String>>(){}.getType());
					for (String lanCode : nameMap.keySet()) {
						names += nameMap.get(lanCode) + " , ";
					}
					
					if(names.length() > 0){
						names = names.substring(0, names.length()-2);
					}
				}
				tool.setToolName(names);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		//productTypeList = productTypeService.listAll();
		productTypeList=productForSealerService.getProductForSealerList();
		languageList = languageService.queryAllLanguage();
		return "add";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		
		Map<String,String> lanMap = new HashMap<String,String>();
		if(StringUtils.isEmpty(languageCode)){		//多语言
			this.msgs.add(getText("tool.download.language.notempey"));
			this.urls.put(getText("tool.download.add"), "tool-download!add.do");
			return MESSAGE;
		}else{
			String [] lanCodes = languageCode.split(",");
			String [] lanNames = languageName.split(",");
			
			//TODO 判断工具名称是否有重复
			
			for (int i = 0; i < lanCodes.length; i++) {
				lanMap.put(lanCodes[i].trim(),lanNames[i].trim());
			}
		}
//		
//		if (tool != null) {
//			String tempFileName = toolFileName.toLowerCase().trim();
//			if (tempFileName.endsWith(".zip") || tempFileName.endsWith(".rar")) {
//				String path = UploadUtil.upload(this.tool,this.toolFileName, "tools");
//				toolDownload.setDownloadPath(path);
//			} else {
//				this.msgs.add(getText("tool.download.notallow.uploadfile"));
//				this.urls.put(getText("tool.download.add"), "tool-download!add.do");
//				return MESSAGE;
//			}
//		}
		
		String toolName = JsonHelper.toJson(lanMap);
		toolDownload.setToolName(toolName);
		String [] proTypeCode = null;
		if(StringUtils.isNotEmpty(productTypeCode)){
		   proTypeCode = productTypeCode.split(",");
		   if(proTypeCode != null && proTypeCode.length > 0){
			   for (int i = 0; i < proTypeCode.length; i++) {
				   proTypeCode[i] = proTypeCode[i].trim();
			   }
			}
		}
		toolDownloadService.saveToolDownload(toolDownload,proTypeCode);
		
		this.msgs.add(getText("common.js.addSuccess"));
		this.urls.put(getText("tool.download.list"), "tool-download!list.do");
		
		
		return MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 */
	public String edit() throws Exception {
	//	productTypeList = productTypeService.listAll();
		productTypeList=productForSealerService.getProductForSealerList();
		languageList = languageService.queryAllLanguage();
		toolDownload = toolDownloadService.getByCode(code);
		
//		String path = UploadUtil.replacePath(toolDownload.getDownloadPath());
//		toolDownload.setDownloadPath(path);
		
		//解析JSON格式存储的语言
		String toolName = toolDownload.getToolName();
		if(StringUtils.isNotEmpty(toolName)){
			editLanList = new ArrayList<Language>();
			Map<String,String> nameMap = JsonHelper.fromJson(toolName,new TypeToken<Map<String,String>>(){}.getType());
			for (String lanCode : nameMap.keySet()) {
				Language lan = new Language();
				lan.setCode(lanCode);
				lan.setName(nameMap.get(lanCode));
				editLanList.add(lan);
			}
		}
		
		editProductTypeList = toolDownload.getProductTypeList();
		
		
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		
		Map<String,String> lanMap = new HashMap<String,String>();
		if(StringUtils.isEmpty(languageCode)){		//多语言
			this.msgs.add(getText("tool.download.language.notempey"));
			this.urls.put(getText("tool.download.add"), "tool-download!edit.do?code="+toolDownload.getCode());
			return MESSAGE;
		}else{
			String [] lanCodes = languageCode.split(",");
			String [] lanNames = languageName.split(",");
			
			//TODO 判断工具名称是否有重复
			for (int i = 0; i < lanCodes.length; i++) {
				lanMap.put(lanCodes[i].trim(),lanNames[i].trim());
			}
		}
/*		
		if (tool != null) {
			String tempFileName = toolFileName.toLowerCase().trim();
			if (tempFileName.endsWith(".zip") || tempFileName.endsWith(".rar")) {
				String path = UploadUtil.upload(this.tool,this.toolFileName, "tools");
				toolDownload.setDownloadPath(path);
			} else {
				this.msgs.add(getText("tool.download.notallow.uploadfile"));
				this.urls.put(getText("tool.download.add"), "tool-download!add.do");
				return MESSAGE;
			}
		}*/
		
		String toolName = JsonHelper.toJson(lanMap);
		toolDownload.setToolName(toolName);
		String [] proTypeCode = null;
		if(StringUtils.isNotEmpty(productTypeCode)){
		   proTypeCode = productTypeCode.split(",");
		   if(proTypeCode != null && proTypeCode.length > 0){
			   for (int i = 0; i < proTypeCode.length; i++) {
				   proTypeCode[i] = proTypeCode[i].trim();
			   }
			}
		}
		
		toolDownloadService.updateToolDownload(toolDownload,proTypeCode);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("tool.download.list"), "tool-download!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			toolDownloadService.delToolDownloadByCode(code);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}

		this.urls.put(getText("tool.download.list"), "tool-download!list.do");
		return MESSAGE;
	}

	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ToolDownload getToolDownload() {
		return toolDownload;
	}

	public void setToolDownload(ToolDownload toolDownload) {
		this.toolDownload = toolDownload;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}

	public List<ProductForSealer> getProductTypeList() {
		return productTypeList;
	}

	public void setProductTypeList(List<ProductForSealer> productTypeList) {
		this.productTypeList = productTypeList;
	}

	public List<ToolDownload> getToolDownloadList() {
		return toolDownloadList;
	}

	public void setToolDownloadList(List<ToolDownload> toolDownloadList) {
		this.toolDownloadList = toolDownloadList;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public List<Language> getEditLanList() {
		return editLanList;
	}

	public void setEditLanList(List<Language> editLanList) {
		this.editLanList = editLanList;
	}

	public List<ProductType> getEditProductTypeList() {
		return editProductTypeList;
	}

	public void setEditProductTypeList(List<ProductType> editProductTypeList) {
		this.editProductTypeList = editProductTypeList;
	}

	public File getTool() {
		return tool;
	}

	public void setTool(File tool) {
		this.tool = tool;
	}

	public String getToolFileName() {
		return toolFileName;
	}

	public void setToolFileName(String toolFileName) {
		this.toolFileName = toolFileName;
	}


	
}
