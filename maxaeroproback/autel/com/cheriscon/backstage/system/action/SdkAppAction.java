package com.cheriscon.backstage.system.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.member.service.ISdkAppDownloadService;
import com.cheriscon.backstage.member.service.ISdkAppService;
import com.cheriscon.backstage.member.vo.SdkAppDownloadVO;
import com.cheriscon.backstage.member.vo.SdkAppVO;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("json_default")
@Namespace("/core/admin")
@Results({
		@Result(name = "list_sdkApp", location = "/autel/backstage/system/sdkApp_list.jsp"),
		@Result(name = "list_sdkAppDownload", location = "/autel/backstage/system/sdkAppDownload_list.jsp"),
		@Result(name = "add_sdkAppDownload", location = "/autel/backstage/system/sdkAppDownload_add.jsp") })
public class SdkAppAction extends WWAction {

	/**
	 * sdk app参数对象
	 */
	private SdkAppVO sdkAppVO;

	/**
	 * sdk app download查询返回对象
	 */
	private SdkAppDownloadVO sdkAppDownloadVO;

	private boolean result;// 返回结果

	@Resource
	private ISdkAppService sdkAppService;

	@Resource
	private ISdkAppDownloadService sdkAppDownloadService;

	private String opeaType;// 操作类型

	/**
	 * 查詢sdk app集合
	 * 
	 * @return String
	 */
	@Action(value = "queryAppList", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "webpage" }) })
	public String queryAppList() {
		webpage = sdkAppService.querySdkAppList(sdkAppVO, this.getPage(),
				this.getPageSize());
		return "list_sdkApp";
	}

	/**
	 * 删除sdk app
	 * 
	 * @return String
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "deleteSdkApp", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result" }) })
	public String deleteSdkApp() {
		try {
			result = sdkAppService.deleteSdkAppList(sdkAppVO);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail"));
		}
		this.urls.put(getText("sdk.app.common.back"), "queryAppList.do");
		return MESSAGE;
	}

	/**
	 * 分页查詢sdk app download信息
	 * 
	 * @return list
	 */
	@Action(value = "querySdkAppDownloadsList", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "webpage" }) })
	public String querySdkAppDownloadsList() {
		webpage = sdkAppDownloadService.querySdkAppDownloadsList(
				this.getPageSize(), this.getPage());
		return "list_sdkAppDownload";
	}

	/**
	 * 跳转添加sdk app download信息页面
	 * 
	 * @return String
	 * @throws Exception
	 */
	@Action(value = "addAppDownloadsPage", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "" }) })
	public String addAppDownloadsPage() throws Exception {
		// 判断操作类型，1代表添加，2代表修改
		if ("2".equals(getOpeaType())) {
			SdkAppDownloadVO downloadVo = sdkAppDownloadService
					.querySdkAppDownloadsInfoById(sdkAppDownloadVO.getId());
			if (null != downloadVo) {
				setSdkAppDownloadVO(downloadVo);
			}
		}
		return "add_sdkAppDownload";
	}

	/**
	 * 添加sdk app download信息
	 * 
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "addSdkAppDownloads", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result" }) })
	public String addSdkAppDownloads() throws Exception {
		try {
			// 判断操作类型，1代表添加，2代表修改
			if ("1".equals(getOpeaType())) {
				result = sdkAppDownloadService
						.addSdkAppDownloads(sdkAppDownloadVO);
				msgs.add(getText("common.js.addSuccess"));
			}
			if ("2".equals(getOpeaType())) {
				result = sdkAppDownloadService
						.updateSdkAppDownloads(sdkAppDownloadVO);
				msgs.add(getText("common.js.modSuccess"));
			}
		} catch (RuntimeException e) {
			msgs.add(("1".equals(getOpeaType()) ? getText("common.js.addFail")
					: getText("common.js.modFail")));
		}
		this.urls.put(getText("sdk.app.common.back"),
				"querySdkAppDownloadsList.do");
		return MESSAGE;
	}

	/**
	 * 删除sdk app download信息
	 * 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "deleteSdkAppDownloads", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "result" }) })
	public String deleteSdkAppDownloads() throws Exception {
		try {
			result = sdkAppDownloadService
					.deleteSdkAppDownloads(sdkAppDownloadVO);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail"));
		}
		this.urls.put(getText("sdk.app.common.back"),
				"querySdkAppDownloadsList.do");
		return MESSAGE;
	}

	public SdkAppVO getSdkAppVO() {
		return sdkAppVO;
	}

	public void setSdkAppVO(SdkAppVO sdkAppVO) {
		this.sdkAppVO = sdkAppVO;
	}

	public SdkAppDownloadVO getSdkAppDownloadVO() {
		return sdkAppDownloadVO;
	}

	public void setSdkAppDownloadVO(SdkAppDownloadVO sdkAppDownloadVO) {
		this.sdkAppDownloadVO = sdkAppDownloadVO;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public ISdkAppService getSdkAppService() {
		return sdkAppService;
	}

	public void setSdkAppService(ISdkAppService sdkAppService) {
		this.sdkAppService = sdkAppService;
	}

	public ISdkAppDownloadService getSdkAppDownloadService() {
		return sdkAppDownloadService;
	}

	public void setSdkAppDownloadService(
			ISdkAppDownloadService sdkAppDownloadService) {
		this.sdkAppDownloadService = sdkAppDownloadService;
	}

	public String getOpeaType() {
		return opeaType;
	}

	public void setOpeaType(String opeaType) {
		this.opeaType = opeaType;
	}

}
