package com.cheriscon.backstage.system.action;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.JSONHelpUtils;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.front.user.service.ICustomerInfoService;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/cloud")
public class CloudInterfaceAction extends WWAction{
	

	@Resource
	private ICustomerInfoService customerInfoService; 
	
	@Action(value = "login")
	public void login() {
		String serialNo=getRequest().getParameter("serialNo");
		String password=getRequest().getParameter("password");
		if(null != serialNo){
			try {
				CustomerInfo customer=new CustomerInfo();
				if(password==null){
					customer=customerInfoService.getCustBySerialNo(serialNo);
				}else{
					customer=customerInfoService.getCustBySerialNo(serialNo,password);
				}
				JSONObject object = new JSONObject();
				JSONHelpUtils.getJsonString4JavaPOJO(customer);
				object.put("object", JSONHelpUtils.getJsonString4JavaPOJO(customer));
				getResponse().setContentType("text/json;charset=UTF-8");
				getResponse().setCharacterEncoding("UTF-8");
				getResponse().getWriter().write(JSONHelpUtils.getJsonString4JavaPOJO(customer));
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		
		}
	}
	
}
