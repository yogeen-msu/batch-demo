package com.cheriscon.backstage.system.action;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.system.service.ICustomerProInfoHistoryService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.IProductLockService;
import com.cheriscon.common.model.CustomerProInfoHistory;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/productLock")
@Results({
		@Result(name = "list", location = "/autel/backstage/productLock/productLock_list.jsp"),
		@Result(name = "add", location = "/autel/backstage/productLock/productLock_add.jsp"),
		@Result(name = "view", location = "/autel/backstage/productLock/productLock_view.jsp") })
public class ProductLockAction extends WWAction {

	@Resource
	private IProductInfoService productInofService;

	@Resource
	private IProductLockService productLockService;
	
	@Resource
	private IAdminUserManager adminUserManager;
	
	@Resource
	private ICustomerProInfoHistoryService customerProInfoHistoryServiceImpl;

	private ProductInfo productInfo;

	
	private String serialNo;
	private Integer id;
	
	public String list() {

		try {
			
			String serialNo=null;
			if(productInfo!=null){
				serialNo=productInfo.getSerialNo();
			}
			this.webpage = productLockService.getProductLocks(
					serialNo, this.getPage(),
					this.getPageSize());
			
		} catch (Exception e) {
		}
		return "list";
	}

	public String add() {
		return "add";
	}

	public String save() {
		
		ProductInfo pfo = productInofService.getBySerialNo(productInfo.getSerialNo());

		if(pfo==null){
			this.msgs.add(getText("memberman.testpackman.message8"));
			this.urls.put("返回", "javascript:history.go(-1);");
		}else if(pfo.getRegStatus().equals(1)){
			this.msgs.add("产品已经注册");
			this.urls.put("返回", "javascript:history.go(-1);");
		}else if(pfo.getRegStatus().equals(2)){
			this.msgs.add("产品已经解绑");
			this.urls.put("返回", "javascript:history.go(-1);");
		}else{
			try {
				AdminUser user = adminUserManager.getCurrentUser();
				user = adminUserManager.get(user.getUserid());
				productInfo.setCreateUser(user.getUsername());
				productLockService.addProductLock(productInfo);
				
				CustomerProInfoHistory customerProInfoHistory = new CustomerProInfoHistory();
				customerProInfoHistory.setProCode(productInfo.getCode());
				customerProInfoHistory.setCreateUser(user.getUsername());
				customerProInfoHistoryServiceImpl.add(customerProInfoHistory);
				
				this.msgs.add(getText("common.js.addSuccess"));
				this.urls.put(getText("product.info.list"), "product-lock!list.do");
			} catch (Exception e) {
			}
		}
			
		return MESSAGE;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String rebanding() throws Exception{
		try{
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			String  updateTime=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			customerProInfoHistoryServiceImpl.rebanding(id,updateTime,user.getUsername());
			this.msgs.add("操作成功");
			this.urls.put(getText("product.info.list"), "product-lock!list.do");
		}catch(Exception e){
			this.msgs.add("操作失败，请联系管理员");
			this.urls.put(getText("product.info.list"), "product-lock!list.do");
		}
		return MESSAGE;
	}
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String view() {
		productInfo = productInofService.getById(productInfo.getId());
		return "view";
	}


	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

}
