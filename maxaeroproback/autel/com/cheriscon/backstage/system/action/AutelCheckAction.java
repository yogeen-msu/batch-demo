package com.cheriscon.backstage.system.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.SealerInfoVo;
import com.cheriscon.backstage.system.model.AutelCheckVo;
import com.cheriscon.backstage.system.service.IAutelCheckService;
import com.cheriscon.backstage.trade.service.IOrderInfoVoService;
import com.cheriscon.backstage.trade.service.IReChargeCardInfoService;
import com.cheriscon.backstage.trade.vo.OrderInfoVo;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.utils.SolrUtils;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

/**
 * @remark 产品网站监控action
 * @author chenqichuan
 * @date 2014-6-25
 */
@ParentPackage("json_default")
@Namespace("/autel/check")
public class AutelCheckAction extends WWAction {

	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IAutelCheckService autelCheckService;
	@Resource
	private IProductForSealerService productForSealerService;
	@Resource
	private ISealerInfoService sealerInfoService;
	
	
	private String jsonData;
	private AutelCheckVo todayOrderInfo; // 今日订单
	private AutelCheckVo totalOrderInfo; // 总的订单
	private List<AutelCheckVo> listOrderInfo; // 按月查询每月订单数
	private List<AutelCheckVo> rechargeCardInfo; // 升级卡使用情况
	private List<AutelCheckVo> productInfo; // 产品使用情况
	private int cardUseNum; // 升级卡已经使用数量
	private int cardNotUseNum; // 未使用升级卡数量
	private String year; // 查询条件年份
	private AutelCheckVo autel;
	private List<ProductForSealer> productTypeList;
	private int totalProduct = 0;
	private int notRegProduct = 0;
	private int regProduct = 0;
	private int notBoundProduct = 0;
	private int expireProduct = 0;
	private Map<String, Integer> upgradeInfo;
	private SealerInfoVo sealerInfoVoSel;
	


	@Action(value = "toCheckPage", results = { @Result(name = "toCheckPage", location = "/autel/backstage/check/checkPage.jsp") })
	public String toCheckPage() throws Exception {

		productTypeList = productForSealerService.getProductForSealerList();

		/*
		 * productInfo=autelCheckService.getProductNum(autel); //产品使用情况
		 */notRegProduct = autelCheckService
				.getProductStatus(FrontConstant.PRODUCT_REGSTATUS_NO); // 未注册
		regProduct = autelCheckService
				.getProductStatus(FrontConstant.PRODUCT_REGSTATUS_YES); // 已注册
		notBoundProduct = autelCheckService
				.getProductStatus(FrontConstant.PRODUCT_REGSTATUS_UNBINDED); // 已过期
		expireProduct = autelCheckService.getProductValid();
		totalProduct = notRegProduct + regProduct + notBoundProduct;

		/*
		 * todayOrderInfo=autelCheckService.getTodayMoney(); //今日订单
		 * totalOrderInfo=autelCheckService.getTotalMoney(); //总的订单
		 * cardUseNum=autelCheckService
		 * .getCardIsUseNum(FrontConstant.CARD_SERIAL_IS_NO_USE); //升级卡已使用数量
		 * cardNotUseNum
		 * =autelCheckService.getCardIsUseNum(FrontConstant.CARD_SERIAL_IS_YES_USE
		 * ); //升级卡未使用数量
		 */
		return "toCheckPage";
	}

	// 统计所有产品情况
	@Action(value = "toProductTotalPage", results = { @Result(name = "toProductTotalPage", location = "/autel/backstage/check/product_total.jsp") })
	public String toProductTotalPage() throws Exception {
		productInfo = autelCheckService.getProductNum(autel); // 产品使用情况
		return "toProductTotalPage";
	}

	// 按区域统计产品情况
	@Action(value = "toProductAreaPage", results = { @Result(name = "toProductAreaPage", location = "/autel/backstage/check/product_area.jsp") })
	public String toProductAreaPage() throws Exception {
		productInfo = autelCheckService.getProductNumByArea(autel); // 产品使用情况
		return "toProductAreaPage";
	}

	// 按语言统计产品情况
	@Action(value = "toProductLanguagePage", results = { @Result(name = "toProductLanguagePage", location = "/autel/backstage/check/product_language.jsp") })
	public String toProductLanguagePage() throws Exception {
		productInfo = autelCheckService.getProductNumByLanguage(autel); // 产品使用情况
		return "toProductLanguagePage";
	}
	
	@Action(value = "salesInfo", results = { @Result(name = "salesInfo", location = "/autel/backstage/check/salesInfo.jsp") })
	public String salesInfo() throws Exception {
		
		return "salesInfo";
	}
	
	
	//查询经销商的产品升级区域
	@Action(value = "productUpgradeInfo", results = { @Result(name = "productUpgradeInfo", location = "/autel/backstage/check/productUpgradeInfo.jsp") })
	public String productUpgradeInfo() throws Exception {
		productTypeList = productForSealerService.getProductForSealerList();
		String qStr="";
		if(autel==null){
			autel=new AutelCheckVo();
		}
		
		if(!StringUtil.isEmpty(autel.getProType())){
			qStr="proTypeCode:"+autel.getProType();
		};
		if(!StringUtil.isEmpty(autel.getSealerCode())){
			if(StringUtil.isEmpty(qStr)){
				qStr+="sealerCode:"+autel.getSealerCode();
			}else{
				qStr+=" AND sealerCode:"+autel.getSealerCode();
			}
		};
		
		upgradeInfo=SolrUtils.queryByGroup(qStr, "country", 20, 1);
		return "productUpgradeInfo";
	}
	
	
	  //查询产品续费情况
	@Action(value = "productUpgradeType", results = { @Result(name = "productUpgradeType", location = "/autel/backstage/check/productUpgradeType.jsp") })
	public String productUpgradeType() throws Exception {
		productTypeList = productForSealerService.getProductForSealerList();
		String qStr="";
		if(autel==null){
			autel=new AutelCheckVo();
		}
		
		if(!StringUtil.isEmpty(autel.getProType())){
			qStr="proTypeCode2:"+autel.getProType();
		};
		if(!StringUtil.isEmpty(autel.getSealerCode())){
			if(StringUtil.isEmpty(qStr)){
				qStr+="sealerCode2:"+autel.getSealerCode();
			}else{
				qStr+=" AND sealerCode2:"+autel.getSealerCode();
			}
		};
		
		upgradeInfo=SolrUtils.queryByGroup2(qStr, "country2", 20, 1);
		return "productUpgradeType";
	}
	
	   //产品软件使用情况统计报表
		@Action(value = "productSoftware", results = { @Result(name = "productSoftware", location = "/autel/backstage/check/productSoftwareInfo.jsp") })
		public String productSoftware() throws Exception {
			productTypeList = productForSealerService.getProductForSealerList();
			String qStr="";
			if(autel==null){
				autel=new AutelCheckVo();
			}
			
			if(!StringUtil.isEmpty(autel.getProType())){
				qStr="productCode:"+autel.getProType();
			};
			/*if(!StringUtil.isEmpty(autel.getSealerCode())){
				if(StringUtil.isEmpty(qStr)){
					qStr+="sealerCode2:"+autel.getSealerCode();
				}else{
					qStr+=" AND sealerCode2:"+autel.getSealerCode();
				}
			};*/
			
			upgradeInfo=SolrUtils.queryByGroup3(qStr, "softName", 20, 1);
			return "productSoftware";
		}
	
	

	@Action(value = "toOrderPage", results = { @Result(name = "toOrderPage", location = "/autel/backstage/check/orderPage.jsp") })
	public String toOrderPage() throws Exception {
		if (StringUtil.isEmpty(year)) {
			year = DateUtil.toString(new Date(), "YYYY");
		}
		listOrderInfo = autelCheckService.getMoneyByYear(year);
		return "toOrderPage";
	}

	@Action(value = "toCardPage", results = { @Result(name = "toCardPage", location = "/autel/backstage/check/cardPage.jsp") })
	public String toCardPage() throws Exception {
		rechargeCardInfo = autelCheckService
				.getCardUseNum(FrontConstant.CARD_SERIAL_IS_NO_USE);
		return "toCardPage";
	}

	@Action(value = "toSelectSealerDlg", results = { @Result(name = SUCCESS, location = "/autel/backstage/check/sale_sealer_select.jsp") })
	public String toSelectSealerDlg() throws Exception{
		try{
			this.webpage = sealerInfoService.pageSealerInfoVoPage2(sealerInfoVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	
	public Map<String, Integer> getUpgradeInfo() {
		return upgradeInfo;
	}

	public void setUpgradeInfo(Map<String, Integer> upgradeInfo) {
		this.upgradeInfo = upgradeInfo;
	}

	public int getCardUseNum() {
		return cardUseNum;
	}

	public int getCardNotUseNum() {
		return cardNotUseNum;
	}

	public void setCardUseNum(int cardUseNum) {
		this.cardUseNum = cardUseNum;
	}

	public void setCardNotUseNum(int cardNotUseNum) {
		this.cardNotUseNum = cardNotUseNum;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public AutelCheckVo getTodayOrderInfo() {
		return todayOrderInfo;
	}

	public AutelCheckVo getTotalOrderInfo() {
		return totalOrderInfo;
	}

	public List<AutelCheckVo> getListOrderInfo() {
		return listOrderInfo;
	}

	public String getYear() {
		return year;
	}

	public void setTodayOrderInfo(AutelCheckVo todayOrderInfo) {
		this.todayOrderInfo = todayOrderInfo;
	}

	public void setTotalOrderInfo(AutelCheckVo totalOrderInfo) {
		this.totalOrderInfo = totalOrderInfo;
	}

	public void setListOrderInfo(List<AutelCheckVo> listOrderInfo) {
		this.listOrderInfo = listOrderInfo;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public List<AutelCheckVo> getRechargeCardInfo() {
		return rechargeCardInfo;
	}

	public void setRechargeCardInfo(List<AutelCheckVo> rechargeCardInfo) {
		this.rechargeCardInfo = rechargeCardInfo;
	}

	public List<ProductForSealer> getProductTypeList() {
		return productTypeList;
	}

	public void setProductTypeList(List<ProductForSealer> productTypeList) {
		this.productTypeList = productTypeList;
	}

	public List<AutelCheckVo> getProductInfo() {
		return productInfo;
	}

	public AutelCheckVo getAutel() {
		return autel;
	}

	public void setProductInfo(List<AutelCheckVo> productInfo) {
		this.productInfo = productInfo;
	}

	public void setAutel(AutelCheckVo autel) {
		this.autel = autel;
	}

	public int getTotalProduct() {
		return totalProduct;
	}

	public int getNotRegProduct() {
		return notRegProduct;
	}

	public int getRegProduct() {
		return regProduct;
	}

	public int getNotBoundProduct() {
		return notBoundProduct;
	}

	public int getExpireProduct() {
		return expireProduct;
	}

	public void setTotalProduct(int totalProduct) {
		this.totalProduct = totalProduct;
	}

	public void setNotRegProduct(int notRegProduct) {
		this.notRegProduct = notRegProduct;
	}

	public void setRegProduct(int regProduct) {
		this.regProduct = regProduct;
	}

	public void setNotBoundProduct(int notBoundProduct) {
		this.notBoundProduct = notBoundProduct;
	}

	public void setExpireProduct(int expireProduct) {
		this.expireProduct = expireProduct;
	}

	public SealerInfoVo getSealerInfoVoSel() {
		return sealerInfoVoSel;
	}

	public void setSealerInfoVoSel(SealerInfoVo sealerInfoVoSel) {
		this.sealerInfoVoSel = sealerInfoVoSel;
	}
}
