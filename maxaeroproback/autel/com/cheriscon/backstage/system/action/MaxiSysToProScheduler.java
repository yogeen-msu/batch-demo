package com.cheriscon.backstage.system.action;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.cheriscon.app.base.core.model.MultiSite;
import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.IMultiSiteManager;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductContractService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.UpWarningInfo;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

public class MaxiSysToProScheduler {
	@Resource
	private IProductContractLogService productContractLogService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ILanguageService languageService;
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IEmailTemplateService emailTemplateService; 
	
	private void loadDefaultContext() {
		CopContext context = new CopContext();
		CopSite site = new CopSite();
		site.setUserid(1);
		site.setId(1);
		site.setThemeid(1);
		context.setCurrentSite(site);
		CopContext.setContext(context);

		ISiteManager siteManager = SpringContextHolder.getBean("siteManager");
		site = siteManager.get("localhost");
		context.setCurrentSite(site);
		CopContext.setContext(context);
	}
	
    public void doMaxiSysToProEmail() {
	   
	   try {
		  List<ProductContractChangeLog> list=productContractLogService.queryLogForMaxiSys();
		  System.out.println("开始执行MaxiSys升级到Pro邮件提醒===="+DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss")+"====数量："+list.size());
		  if(null!=list){
			  for(int i=0;i<list.size();i++){
				  ProductContractChangeLog log=list.get(i);
				  loadDefaultContext();	
				  //1.查询用户
				  CustomerInfo info=customerInfoService.getCustBySerialNo2(log.getProductSN());
				  String userName="";
				  if(info!=null && info.getName()!=null){
					  userName=info.getName();
				   }
				  //2.发送邮件
				  Smtp smtp = smtpManager.getCurrentSmtp( );
					
				  EmailModel emailModel = new EmailModel();
				  emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				  emailModel.getData().put("password", smtp.getPassword());
				  emailModel.getData().put("host", smtp.getHost());
				  emailModel.getData().put("port", smtp.getPort());
				  emailModel.getData().put("userName",userName);
				  emailModel.getData().put("serialNo",log.getProductSN());
				  Language language = languageService.getByCode(info.getLanguageCode());
					
					if(language != null)
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("maxisys.to.pro.email.title",
								language.getLanguageCode(),language.getCountryCode()));
					}
					else 
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("maxisys.to.pro.email.title"));
					}
					
				   emailModel.setTo(info.getAutelId());
				   EmailTemplate template = emailTemplateService.getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_MAXISYS_TO_PRO,info.getLanguageCode());
					
					if(template != null)
					{
						emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
					}
					

					if(log.getSendEmailFlag().equals("N") && FrontConstant.getPropertValue("autelproweb.email.url").equals("http://pro.auteltech.com")){
						emailProducer.send(emailModel);
						//3.更新发送标示
						log.setSendEmailFlag("Y");
						productContractLogService.updateSendEmailFlag(log);
					}
					
			  }
		  }
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
  
}