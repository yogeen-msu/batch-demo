package com.cheriscon.backstage.system.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.IEmailLogService;
import com.cheriscon.common.model.EmailLog;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/emailLog")
@Results({
	@Result(name="list", location="/autel/backstage/log/emailLog.jsp"),
})
public class EmailLogAction extends WWAction {
	
	@Resource
	private IEmailLogService emailLogService;
	
	private EmailLog emailLog;
	
	public String list(){
    	try {
    		this.webpage=emailLogService.getEmailLogs(emailLog,this.getPage(), this.getPageSize());
		} catch (Exception e) {
		}
		return "list";
	}

	public EmailLog getEmailLog() {
		return emailLog;
	}

	public void setEmailLog(EmailLog emailLog) {
		this.emailLog = emailLog;
	}
	
}
