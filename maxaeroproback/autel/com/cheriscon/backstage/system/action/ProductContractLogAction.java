package com.cheriscon.backstage.system.action;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.system.service.IProductChangeNoLogService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.common.model.ProductChangeNoLog;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IProductDateService;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/ProductContractLog")
@Results({
		@Result(name = "list", location = "/autel/backstage/log/ProductContractLog.jsp"),
		@Result(name = "noLoglist", location = "/autel/backstage/log/ProductContractNoLog.jsp"),
		@Result(name = "addCopy", location = "/autel/backstage/log/ProductContractNoLog_addCopy.jsp") })
public class ProductContractLogAction extends WWAction {

	@Resource
	private IProductContractLogService productContractLogService;
	@Resource
	private IProductChangeNoLogService productChangeNoLogService;
	@Resource
	private IRegProductService regProductService;
	@Resource
	private IProductDateService productDateService;
	@Resource
	private ICustomerProductService customerProductService;
	@Resource
	private IAdminUserManager adminUserManager;

	private String oldProductSN; // 更换产品序列号-老序列号
	private String newProductSN; // 更换产品序列号-新序列号
	private String productSN; // 查询序列号

	private ProductChangeNoLog productChangeNoLog;
	
	private ProductContractChangeLog productContractChangeLog;

	public String list() {
		try {
			this.webpage = productContractLogService
					.getProductContractChangeLogs(productContractChangeLog,
							this.getPage(), this.getPageSize());
		} catch (Exception e) {
		}
		return "list";
	}

	public String noLoglist() {
		try {
			this.webpage = productChangeNoLogService
					.getProductContractChangeNoLogs(productChangeNoLog,
							this.getPage(), this.getPageSize());
		} catch (Exception e) {
		}
		return "noLoglist";
	}

	public String addCopy() {
		return "addCopy";
	}

	/**
	 * 需求描述：根据王菲和俄国客户的协议，他们可以自主进行PCB板的更换。更换后需要将老序列号的客户信息拷贝到新序列号内。
	 * 
	 * @return
	 */
	public String copyProduct() {

		if (newProductSN.equals(oldProductSN)) {
			this.msgs.add("新老序列号不能一样");
			this.urls.put("返回", "javascript:history.go(-1)");
			return MESSAGE;
		}
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		ProductInfo newProduct = regProductService
				.queryProductAndSealer(newProductSN); // 判断新产品是否正确
		if (null == newProduct) {
			this.msgs.add("新产品序列号错误，该产品不存在");
			this.urls.put("返回", "javascript:history.go(-1)");
		} else {

			if (newProduct.getRegStatus() == FrontConstant.CHARGE_CARE_IS_YES_REG) {
				this.msgs.add("该新产品已经注册，请更换新序列号");
				this.urls.put("返回", "javascript:history.go(-1)");
			} else {

				ProductInfo oldProduct = regProductService
						.queryProductAndSealer(oldProductSN);
				if (oldProduct == null
						|| oldProduct.getRegStatus() == FrontConstant.CHARGE_CARE_IS_NO_REG) {
					this.msgs.add("该老产品未注册，或不存在");
					this.urls.put("返回", "javascript:history.go(-1)");
				} else if (!newProduct.getProTypeCode().equals(
						oldProduct.getProTypeCode())) {
					this.msgs.add("产品类型错误");
					this.urls.put("返回", "javascript:history.go(-1)");
				} else if (!newProduct.getSealerAutelId().equals(
						oldProduct.getSealerAutelId())) {
					this.msgs.add("产品序列号不属于经销商");
					this.urls.put("返回", "javascript:history.go(-1)");
				} else {
					/**
					 * 开始执行更换操作 1.将产品的信息写入中间表，包括原用户code，有效期等
					 * ***/
					ProductChangeNoLog log = new ProductChangeNoLog();
					AdminUser user = adminUserManager.getCurrentUser();
					log.setCreateUser(user.getUsername());
					log.setCreateIp(request.getRemoteAddr());
					log.setCreateDate(DateUtil.toString(new Date(),
							FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));

					log.setOldProCode(oldProduct.getCode());
					log.setNewProCode(newProduct.getCode());
					log.setValidDate(oldProduct.getValidDate()); // 有效期
					log.setRegDate(oldProduct.getRegTime()); // 注册日期
					try {
						// 更换产品序列号-用户code
						String customerCode = customerProductService
								.getCustomerproinfo(oldProduct.getCode());
						log.setCustomerCode(customerCode);
						// 先将原来的关系记录日志后删除
						productChangeNoLogService.saveLog(log);
						// 绑定新的关系
						RegProductAcountInfoVO regProductInfoVO = new RegProductAcountInfoVO();
						regProductInfoVO.setProCode(newProduct.getCode());
						regProductInfoVO.setProSerialNo(newProduct
								.getSerialNo());
						regProductInfoVO.setProTypeCode(newProduct
								.getProTypeCode());
						boolean result = regProductService
								.addProductSoftwareValidStatus(customerCode,
										oldProduct.getValidDate(),
										oldProduct.getRegTime(),
										oldProduct.getProDate(),
										regProductInfoVO);

						ProductAssociatesDate proDate = productDateService
								.getProductDateInfo(newProduct.getCode());
						if (proDate == null) {
							proDate = new ProductAssociatesDate();
							proDate.setAssociatesDate(DateUtil.toString(
									new Date(), "yyyy-MM-dd"));
							proDate.setProCode(newProduct.getCode());
							result = productDateService
									.insertProductDate(proDate);
						} else {
							result = productDateService
									.updateProductDate(DateUtil.toString(
											new Date(), "yyyy-MM-dd"),
											newProduct.getCode());
						}
						if (result) {
							this.msgs.add("拷贝成功");
							this.urls.put("返回列表",
									"product-contract-log!noLoglist.do");
						} else {
							this.msgs.add("拷贝失败");
							this.urls.put("返回列表",
									"product-contract-log!noLoglist.do");
						}
					} catch (Exception e) {
					}
				}
			}

		}

		return MESSAGE;
	}

	public String getProductSN() {
		return productSN;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	public ProductChangeNoLog getProductChangeNoLog() {
		return productChangeNoLog;
	}

	public void setProductChangeNoLog(ProductChangeNoLog productChangeNoLog) {
		this.productChangeNoLog = productChangeNoLog;
	}

	public String getOldProductSN() {
		return oldProductSN;
	}

	public void setOldProductSN(String oldProductSN) {
		this.oldProductSN = oldProductSN;
	}

	public String getNewProductSN() {
		return newProductSN;
	}

	public void setNewProductSN(String newProductSN) {
		this.newProductSN = newProductSN;
	}

	public ProductContractChangeLog getProductContractChangeLog() {
		return productContractChangeLog;
	}

	public void setProductContractChangeLog(
			ProductContractChangeLog productContractChangeLog) {
		this.productContractChangeLog = productContractChangeLog;
	}

}
