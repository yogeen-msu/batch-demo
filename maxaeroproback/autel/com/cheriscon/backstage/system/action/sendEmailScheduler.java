package com.cheriscon.backstage.system.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.cheriscon.app.base.core.model.MultiSite;
import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.IMultiSiteManager;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.system.service.IEmailLogService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductContractService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailLog;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.UpWarningInfo;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

public class sendEmailScheduler {
	@Resource
	private IEmailLogService emailLogService;
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IEmailTemplateService emailTemplateService; 
	
	private void loadDefaultContext() {
		CopContext context = new CopContext();
		CopSite site = new CopSite();
		site.setUserid(1);
		site.setId(1);
		site.setThemeid(1);
		context.setCurrentSite(site);
		CopContext.setContext(context);

		ISiteManager siteManager = SpringContextHolder.getBean("siteManager");
		site = siteManager.get("localhost");
		context.setCurrentSite(site);
		CopContext.setContext(context);
	}
	
    public void reSendEmail() {

    	
    	try {
    		String requestUrl =FrontConstant.getPropertValue("autelproweb.email.url");
        	if(requestUrl.equals("http://pro.auteltech.com")){
        		List<EmailLog> list=emailLogService.listEmailLog();
        		System.out.println("开始发送未发送邮件===="+DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss")+"====数量："+list.size());
        		loadDefaultContext();	
			  if(null!=list){
				  for(int i=0;i<list.size();i++){
					  EmailLog log=new EmailLog();
					  try{
						  log=list.get(i);
						 
						  EmailModel emailModel = new EmailModel();
						  emailModel.setTitle(log.getTitle());
						  emailModel.setTo(log.getToUser());
						  emailModel.setContent(log.getContent());
						  emailModel.setLog(log);
						  emailProducer.send(emailModel);
					  }catch(Exception e){
					  }
				  }
			  }
        	}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	
    }
  
}