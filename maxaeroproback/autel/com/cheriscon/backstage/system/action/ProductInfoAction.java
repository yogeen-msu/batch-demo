package com.cheriscon.backstage.system.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/productInfo_list.jsp"),
	@Result(name="query", location="/autel/backstage/system/productInfo_query_list.jsp"),
	@Result(name="listForChinaSealer", location="/autel/backstage/system/productInfo_china_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/productInfo_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/productInfo_edit.jsp"),
	@Result(name="editForChina", location="/autel/backstage/system/productInfo_edit_china.jsp"),
	@Result(name="batchEdit", location="/autel/backstage/system/productInfo_batchEdit.jsp"),
	@Result(name="pdtype_select_list", location="/autel/backstage/product/pdtype_select_list.jsp"),
	@Result(name="sale_contract_select_list", location="/autel/backstage/market/sale_contract_select_list.jsp")
})
public class ProductInfoAction extends WWAction{
	
	private int id;
	private String proTypeCode;				//产品型号
	private String proTypeName;				//产品型号名称
	private ProductInfo productInfo;
	private SaleContract contract=new SaleContract() ;
	private ProductForSealer productForSealer=new ProductForSealer();
	
	public ProductForSealer getProductForSealer() {
		return productForSealer;
	}

	public void setProductForSealer(ProductForSealer productForSealer) {
		this.productForSealer = productForSealer;
	}

	public SaleContract getContract() {
		return contract;
	}

	public void setContract(SaleContract contract) {
		this.contract = contract;
	}

	private List<ProductInfo> productInfoList;
	private List<ProductForSealer> productTypeList;
	
	private int editSetp;
	private String batchSerialNo;	//批量修改产品序列号
	private String saleContractCode;	//销售契约编号
	private String operType; //操作类型
	


	@Resource
	private IProductInfoService productInfoService; 
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IProductTypeService productTypeService;
	@Resource
	private IProductForSealerService productForSealerService;
	@Resource
	private IAdminUserManager adminUserManager;
	@Resource
	private IProductContractLogService productContractService;
	// 显示列表
	public String list() {
		try {
			productTypeList = productForSealerService.getProductForSealerList();
			this.webpage = productInfoService.pageProductInfo(productInfo,this.getPage(), this.getPageSize());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "list";
	}
	
	// 显示列表
		public String query() {
			try {
				productTypeList = productForSealerService.getProductForSealerList();
				this.webpage = productInfoService.pageProductInfo(productInfo,this.getPage(), this.getPageSize());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return "query";
		}
	
		// 中国显示列表
	public String listForChinaSealer() {
			try {
				productTypeList = productForSealerService.getProductForSealerList();
				if(productInfo==null){
					productInfo=new ProductInfo();
				}
				productInfo.setSaleContractArea("acf_China");
				this.webpage = productInfoService.pageProductInfo(productInfo,this.getPage(), this.getPageSize());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return "listForChinaSealer";
	}	
	@SuppressWarnings("unchecked")
	public String editForChina(){
		if(operType.equals("1")){
			productInfo = productInfoService.getById(id);
			return "editForChina";
		}else{
			productInfoService.updateProductInfo(productInfo);
			this.msgs.add(getText("common.js.modSuccess"));
			this.urls.put(getText("product.info.list"), "product-info!listForChinaSealer.do");
			return MESSAGE;
		}
	}
		
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 获取所有产品型号
	 * @return
	 */
	public String selectProductType() {
		try {
		//	this.webpage = productTypeService.searchPage(null,getPage(),getPageSize());
//			productTypeList = productTypeService.listAll();
			this.webpage = productForSealerService.pageProductForSealer(productForSealer, getPage(),getPageSize());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "pdtype_select_list";
	}
	
	/**
	 * 获取所有销售契约
	 * @return
	 */
	public String selectsaleContract() {
		
		contract.setProTypeCode(proTypeCode);
		this.webpage = saleContractService.pageSaleContract(contract,this.getPage(),8);
		return "sale_contract_select_list";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		
		ProductInfo pif = productInfoService.getBySerialNo(productInfo.getSerialNo());
		if(pif != null){
			this.msgs.add(getText("product.info.sernohasexist"));
			this.urls.put(getText("product.info.repeartadd"), "product-info!add.do");
			this.urls.put(getText("product.info.list"), "product-info!list.do");
		}else{
			productInfoService.saveProductInfo(productInfo);
			this.msgs.add(getText("common.js.addSuccess"));
			this.urls.put(getText("product.info.list"), "product-info!list.do");
		}
		
		
		
		return MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		productInfo = productInfoService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		
		productInfoService.updateProductInfo(productInfo);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("product.info.list"), "product-info!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			productInfoService.delProductInfoById(id);
			msgs.add(getText("common.js.del"));
		} catch (Exception e) {
			msgs.add(getText("product.info.usingnotremove"));
		}

		this.urls.put(getText("product.info.list"), "product-info!list.do");
		return MESSAGE;
	}

	/**
	 * 批量修改销售契约
	 * @return
	 */
	public String batchEditSale() {
		return "batchEdit";
	}
	
	/**
	 * 批量修改销售契约保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String batchEditSave() {
		List<String> successList = new ArrayList<String>();		//修改成功的的产品序列号
		List<String> failList = new ArrayList<String>();		//修改失败的产品序列号
		
		List<String> hasRegList = new ArrayList<String>();		//已注册不用修改的的产品序列号
		List<String> noExitList = new ArrayList<String>();		//不存在的产品序列号
		Map<String,String> proTypeMap = new HashMap<String,String>();	//产品型号
		
		try {
			if(editSetp == 1){	//校验
				boolean isPass = true;
				if(StringUtils.isEmpty(batchSerialNo)){
					this.msgs.add(getText("product.info.serno.notempty"));
					this.urls.put(getText("product.info.goback"), "product-info!batchEditSale.do");
					this.urls.put(getText("product.info.list"), "product-info!list.do");
					return MESSAGE;
				}
				
				String [] seriaArr = batchSerialNo.trim().split(",");
				for (int i = 0; i < seriaArr.length; i++) {
					String serialNo = seriaArr[i].trim();
					ProductInfo pinfo = productInfoService.getBySerialNo(serialNo);
					if(pinfo == null){
						isPass = false;
						noExitList.add(serialNo);
						continue;
					}
					
					if(pinfo.getRegStatus() != FrontConstant.PRODUCT_REGSTATUS_NO){	//非未注册
						//不允许修改的
						isPass = false;
						hasRegList.add(serialNo);
						continue;
					}
					
					//型号名称 	对应的产品根据产品型号归类
					String typeSeriaNos= proTypeMap.get(pinfo.getProTypeCode());
					if(StringUtils.isEmpty(typeSeriaNos)){
						proTypeMap.put(pinfo.getProTypeCode().trim(),serialNo);
					}else{
						proTypeMap.put(pinfo.getProTypeCode().trim(),typeSeriaNos+","+serialNo);
					}
				}
				
				
				String tipMsg = getText("product.info.input.sernoerror");
				//存在不同类型的产品
				if(proTypeMap.keySet().size() > 1){
					isPass = false;
					tipMsg += ","+getText("product.info.hasdifferent.serno");
				}
				
				if(!isPass) this.msgs.add(tipMsg);
				
				int i = 1;
				if(noExitList.size() > 0){
					this.msgs.add(i + " ). "+getText("product.info.serno.error")+"： "+StringUtils.join(noExitList, ","));
					i++;
				}
				
				if(hasRegList.size() > 0){
					this.msgs.add(i + " ). "+getText("product.info.serno.hasbund")+"： "+StringUtils.join(hasRegList, ","));
					i++;
				}
				
				if(proTypeMap.keySet().size() > 1){
					int j = 1;
					for (String key : proTypeMap.keySet()) {
						ProductForSealer productType = productForSealerService.getProductForSealerByCode(key);
						this.msgs.add(i + " ). "+getText("product.producttypevo")+""+j+"  ("+productType.getName() + ")： "+proTypeMap.get(key));
						i++;
						j++;
					}
				}
				
				
				if(!isPass){	//校验没有通过
//					this.urls.put(getText("common.update.continue"), "product-info!batchEditSale.do");
					this.urls.put(getText("product.info.goback"), "javascript:history.go(-1);");
					this.urls.put(getText("product.info.list"), "product-info!list.do");
					return MESSAGE;
				}else{
					//ProductType productType = productTypeService.getProductTypeByCode(String.valueOf(proTypeMap.keySet().toArray()[0]));
					ProductForSealer productType=productForSealerService.getProductForSealerByCode(String.valueOf(proTypeMap.keySet().toArray()[0]));
					proTypeCode = productType.getCode();
					proTypeName = productType.getName();
					return "batchEdit";
				}
				
			}else if(editSetp == 2){

				SaleContract contract = saleContractService.getSaleContractByCode(saleContractCode);
				HttpServletRequest request=ThreadContextHolder.getHttpRequest();
				AdminUser user = adminUserManager.getCurrentUser();
				user = adminUserManager.get(user.getUserid());
				String ip=FrontConstant.getIpAddr(request);
				String [] seriaArr = batchSerialNo.trim().split(",");
				for (int i = 0; i < seriaArr.length; i++) {
					String serialNo = seriaArr[i].trim();
					ProductInfo pinfo = productInfoService.getBySerialNo(serialNo);
					
					ProductContractChangeLog log=new ProductContractChangeLog();
					log.setNewContract(saleContractCode);
					log.setOldContract(pinfo.getSaleContractCode());
					log.setOperatorIp(ip);
					log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
					log.setOperatorUser(user.getUsername());
					log.setProductSN(serialNo);
					//1.保存日志
					productContractService.saveLog(log);
					pinfo.setProTypeCode(contract.getProTypeCode());
					pinfo.setSaleContractCode(contract.getCode());
					pinfo.setSaleContractName(contract.getName());
					try {
						productInfoService.updateProductInfo(pinfo);
						successList.add(serialNo);
					} catch (Exception e) {
						failList.add(serialNo);
					}
				}
				if(successList.size() == seriaArr.length){
					this.msgs.add(getText("salescontract.batchedit.fail"));
				}else{
					this.msgs.add(getText("common.js.modSuccess")+":"+StringUtils.join(successList, ","));
					this.msgs.add(getText("common.js.modFail")+":"+StringUtils.join(failList, ","));
				}
				this.urls.put(getText("common.update.continue"), "product-info!batchEditSale.do");
				this.urls.put(getText("product.info.list"), "product-info!list.do");
				return MESSAGE;
			}
			return MESSAGE;
		} catch (Exception e) {
			this.msgs.add(getText("salescontract.configdata.error"));
			this.urls.put(getText("common.update.continue"), "product-info!batchEditSale.do");
			this.urls.put(getText("product.info.list"), "product-info!list.do");
			return MESSAGE;
		}
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public List<ProductInfo> getProductInfoList() {
		return productInfoList;
	}

	public void setProductInfoList(List<ProductInfo> productInfoList) {
		this.productInfoList = productInfoList;
	}

	public List<ProductForSealer> getProductTypeList() {
		return productTypeList;
	}

	public void setProductTypeList(List<ProductForSealer> productTypeList) {
		this.productTypeList = productTypeList;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getBatchSerialNo() {
		return batchSerialNo;
	}

	public void setBatchSerialNo(String batchSerialNo) {
		this.batchSerialNo = batchSerialNo;
	}

	public String getSaleContractCode() {
		return saleContractCode;
	}

	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}

	public int getEditSetp() {
		return editSetp;
	}

	public void setEditSetp(int editSetp) {
		this.editSetp = editSetp;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}
	
}
