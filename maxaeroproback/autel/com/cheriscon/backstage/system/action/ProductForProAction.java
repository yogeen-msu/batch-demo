package com.cheriscon.backstage.system.action;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.app.base.core.model.AuthAction;
import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.IMinSaleUnitSaleCfgDetailService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductForProService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.MinSaleUnitSaleCfgDetail;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductForPro;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.front.constant.FrontConstant;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/autel/productForPro")
@Results({
		@Result(name = "list", location = "/autel/backstage/productForPro/productForPro_list.jsp"),
		@Result(name = "listForBand", location = "/autel/backstage/productForPro/productForPro_list_band.jsp"),
		@Result(name = "add", location = "/autel/backstage/productForPro/productForPro_add.jsp"),
		@Result(name = "edit", location = "/autel/backstage/productForPro/productForPro_edit.jsp"), })
public class ProductForProAction extends WWAction {

	@Resource
	private IProductForProService productForProService;

	@Resource
	private IProductInfoService productInfoService;
	
	@Resource
	private ISaleContractService saleContractService;
	
	@Resource
	private IAdminUserManager adminUserManager;
	
	@Resource
	private IMinSaleUnitSaleCfgDetailService minSaleUnitSaleCfgDetailService;
	
	@Resource
	private IProductSoftwareValidStatusService validService;
	
	@Resource
	private IProductContractLogService productContractService;
	
	private ProductForPro productForPro;
	
	private List<SaleContract> saleContracts;
	
	 private String adminFlag="N";   //是否是管理员
	

	public String list() {
		try {
			
			AdminUser user = adminUserManager.getCurrentUser();
			List<AuthAction> list=user.getAuthList();   //网站资料录入  //
			for(int i=0;i<list.size();i++){
				AuthAction a=list.get(i);
				if(a.getName().equals("管理员")){
					adminFlag="Y";
				}
					
			}
			this.webpage = productForProService.getProductForPros(
					productForPro, this.getPage(), this.getPageSize());
		} catch (Exception e) {
		}
		return "list";
	}

	public String listForBand() {
			try {
				if(productForPro!=null){
					this.webpage = productForProService.getProductForProsForBand(
							productForPro, this.getPage(), this.getPageSize());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return "listForBand";
	}
	
	
	
	public String add() {
		return "add";
	}

	public String addSave() {
		try {
			if (productForProService.getProductForProBySerialNo(productForPro
					.getSerialNo()) != null) {
				this.msgs.add("添加失败，该序列号已经存在");
				this.urls.put("重新添加", "javascript:history.go(-1)");
			} else {
				productForProService.addProductForPro(productForPro);
				this.msgs.add("添加成功");
				this.urls.put("返回列表", "product-for-pro!list.do");
			}
		} catch (Exception e) {
		}
		return MESSAGE;
	}

	public String edit() {
		try {
			productForPro = productForProService
					.getProductForProById(productForPro.getId());
		} catch (Exception e) {
		}
		return "edit";
	}

	public String editSave() {
		try {
//			productForProService.updateProductForPro(productForPro);
			Map<String,Object> fileds=new HashMap<String,Object>();
			fileds.put("id", productForPro.getId());
			fileds.put("regPwd", productForPro.getRegPwd());
			fileds.put("type", productForPro.getType());
			fileds.put("proDate", productForPro.getProDate());
			productForProService.updateProductForProByMap(fileds);
			this.msgs.add("更新成功");
			this.urls.put("返回列表", "product-for-pro!list.do");
		} catch (Exception e) {
		}
		return MESSAGE;
	}

	public String delete() {
		try {
			productForProService.deleteProductForProById(productForPro.getId());
			this.msgs.add("刪除成功");
			this.urls.put("返回列表", "product-for-pro!list.do");
		} catch (Exception e) {
		}
		return MESSAGE;
	}

	public String addBoundSave() {
		try {
			
			ProductInfo productInfo = productInfoService
					.getBySerialNo(productForPro.getProCode());
			String MaxiSYS = getPropertValue("product.type.maxisys");//"pts201308291344300924";
			String MaxiSYS_Pro = getPropertValue("product.type.maxisyspro");//"pts201310242155050166";
			String MaxiSys_Mini=this.getPropertValue("product.type.maxisysmini");
			String MS908 = getPropertValue("product.type.ms908");//"pts201507220521190972";
			String MS905 = getPropertValue("product.type.ms905");//"pts201507220521580410";
			
			if (productInfo == null) {
				this.json = "{result:0,message:'产品序列号错误，该产品不存在'}"; 

			} else if (MaxiSYS.equals(productInfo.getProTypeCode())
					|| MaxiSYS_Pro.equals(productInfo.getProTypeCode())
					|| MaxiSys_Mini.equals(productInfo.getProTypeCode()) 
					|| MS908.equals(productInfo.getProTypeCode())
					|| MS905.equals(productInfo.getProTypeCode()) ){
				
				if( MaxiSYS_Pro.equals(productInfo.getProTypeCode())
					|| MaxiSys_Mini.equals(productInfo.getProTypeCode())
					|| MS905.equals(productInfo.getProTypeCode()) ){
					Map<String,Object> fileds=new HashMap<String,Object>();
					fileds.put("id", productForPro.getId());
					fileds.put("regStatus", FrontConstant.PRODUCTFORPRO_STATUS_BOUND_YES);
					fileds.put("regTime", DateUtil.toString(new Date(),
							"yyyy-MM-dd HH:mm:ss"));
					fileds.put("reason", productForPro.getReason());
					fileds.put("proCode",productForPro.getProCode());
					productForProService.updateProductForProByMap(fileds);
					this.json = "{result:1}";  
				}
				if(MaxiSYS.equals(productInfo.getProTypeCode())
				   || MS908.equals(productInfo.getProTypeCode()) ){
					if(getNewSealer(productInfo,MaxiSYS_Pro,productForPro.getType())){
						
						Map<String,Object> fileds=new HashMap<String,Object>();
						fileds.put("id", productForPro.getId());
						fileds.put("regStatus", FrontConstant.PRODUCTFORPRO_STATUS_BOUND_YES);
						fileds.put("regTime", DateUtil.toString(new Date(),
								"yyyy-MM-dd HH:mm:ss"));
						fileds.put("reason", productForPro.getReason());
						fileds.put("proCode",productForPro.getProCode());
						productForProService.updateProductForProByMap(fileds);
		                //更新productInfo ProTypeCode 为MaxiSYS Pro
//						if (MaxiSYS.equals(productInfo.getProTypeCode()) && productForPro.getType()==2) {
//						与原项目比对更改 by 20151112
						if ((MaxiSYS.equals(productInfo.getProTypeCode())
							     || MS908.equals(productInfo.getProTypeCode())) && productForPro.getType()==2) {
							productInfo.setSaleContractCode(saleContracts.get(0).getCode());
							productInfo.setProTypeCode(MaxiSYS_Pro);
							productInfoService.updateProductInfo(productInfo);
						}
						this.json = "{result:1}";
					}
				}
				
				

			} else {
				this.json = "{result:0,message:'绑定的产品类型必须是MaxiSYS或 MaxiSYS Pro'}";
			}

		} catch (Exception e) {
		}
		return this.JSON_MESSAGE;
	}
	
	/**
	 * 获得新的销售契约
	 * @param productInfo
	 * @throws Exception
	 */
	public boolean getNewSealer(ProductInfo productInfo,String MaxiSYS_Pro,int type) throws Exception{
		 
		if(productInfo.getProTypeCode().equals(MaxiSYS_Pro)){
			return true;
		}
		if(type!=2){ //如果不是J2534盒子，直接返回
			return true;
		}
		//获取对应销售契约信息
		ProductInfo productInfoSale= productInfoService.getBySerialNo3(productForPro.getProCode());
		SaleContract saleContract=new SaleContract();
		saleContract.setSealerCode(productInfoSale.getSealerAutelId());
		saleContract.setLanguageCfgCode(productInfoSale.getSaleContractLanguage());
		saleContract.setProTypeCode(MaxiSYS_Pro);
		String SaleCfgCode=getPropertValue(productInfoSale.getSaleContractCfg());
		saleContract.setSaleCfgCode(SaleCfgCode);
		saleContracts=saleContractService.getContrctBySealer(saleContract);
		if(SaleCfgCode==null||saleContracts==null||saleContracts.isEmpty()){
			this.json = "{result:0,message:'绑定失败,销售契约为空，请联系管理员'}";
			return false;
		}else{
			//更新日志
			ProductContractChangeLog productContractChangeLog=new ProductContractChangeLog();
			productContractChangeLog.setSendEmailFlag("N");
			//獲得ip
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			productContractChangeLog.setOperatorIp(FrontConstant.getIpAddr(request));
			productContractChangeLog.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
/*			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());*/
			productContractChangeLog.setOperatorUser(adminUserManager.getCurrentUser().getUsername());
			productContractChangeLog.setProductSN(productInfo.getSerialNo());
			productContractChangeLog.setOldContract(productInfo.getSaleContractCode());
			productContractChangeLog.setNewContract(saleContracts.get(0).getCode());
			productContractService.saveLog(productContractChangeLog);
			
			
			//更新有效期表
			List<ProductSoftwareValidStatus> valids=validService.getProductSoftwareValidStatusList(productInfo.getCode());
			String validDate=null;
			if(valids!=null&&valids.size()>0){
				validDate=valids.get(0).getValidDate();
			}else{
				return true;
			}
			//先清除原来数据
			validService.deleteByProCode(productInfo.getCode());
			
			List<MinSaleUnitSaleCfgDetail> minSaleUnitSaleCfgDetails= minSaleUnitSaleCfgDetailService
					.getMinSaleUnitSaleCfgListByCode(saleContracts.get(0).getSaleCfgCode());
			ProductSoftwareValidStatus productSoftwareValidStatus;
			//插入新数据
			for(int i=0;i<minSaleUnitSaleCfgDetails.size();i++){
			    productSoftwareValidStatus=new ProductSoftwareValidStatus();
			    productSoftwareValidStatus.setProCode(productInfo.getCode());
			    productSoftwareValidStatus.setValidDate(validDate);
			    productSoftwareValidStatus.setMinSaleUnitCode(minSaleUnitSaleCfgDetails.get(i).getMinSaleUnitCode());
			    validService.saveProductSoftwareValidStatus(productSoftwareValidStatus);
			}
		
		}
		return true;
		 
	}
	
	/**
	 * 从配置文件获取SaleCfgCode
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public String getPropertValue(String key) throws IOException{
		InputStream in = FileUtil.getResourceAsStream("config.properties");
		Properties properties = new Properties();
		properties.load(in);
		return properties.getProperty(key);
	}
	
	public ProductForPro getProductForPro() {
		return productForPro;
	}

	public void setProductForPro(ProductForPro productForPro) {
		this.productForPro = productForPro;
	}

	public String getAdminFlag() {
		return adminFlag;
	}

	public void setAdminFlag(String adminFlag) {
		this.adminFlag = adminFlag;
	}

}
