package com.cheriscon.backstage.system.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.system.service.IPhoneMessageCfgService;
import com.cheriscon.common.model.PhoneMessageCfg;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/phoneMessageCfg_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/phoneMessageCfg_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/phoneMessageCfg_edit.jsp")
})
public class PhoneMessageCfgAction extends WWAction{
	
	private int id;
	private PhoneMessageCfg phoneMessageCfg;
	private List<PhoneMessageCfg> phoneMessageCfgList;
	
	@Resource
	private IPhoneMessageCfgService phoneMessageCfgService; 
	
	
	// 显示列表
	public String list() {
		phoneMessageCfgList = phoneMessageCfgService.listPhoneMessageCfg();
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		phoneMessageCfgService.savePhoneMessageCfg(phoneMessageCfg);
		this.msgs.add(getText("common.js.addSuccess"));
		this.urls.put(getText("syssetting.otherconfig.listsmsconfig"), "phone-message-cfg!list.do");
		
		return MESSAGE;
	}
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		phoneMessageCfg = phoneMessageCfgService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		phoneMessageCfgService.updatePhoneMessageCfg(phoneMessageCfg);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("syssetting.otherconfig.listsmsconfig"), "phone-message-cfg!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			int result = phoneMessageCfgService.delPhoneMessageCfgById(id);
			if(CTConsatnt.CT_IS_USE == result){
				msgs.add(getText("syssetting.otherconfig.usingnotremove"));
			}else if(CTConsatnt.CT_SUCCESS  == result){
				msgs.add(getText("common.js.del"));
			}
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}
		this.urls.put(getText("syssetting.otherconfig.listsmsconfig"), "phone-message-cfg!list.do");
		return MESSAGE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PhoneMessageCfg getPhoneMessageCfg() {
		return phoneMessageCfg;
	}

	public void setPhoneMessageCfg(PhoneMessageCfg phoneMessageCfg) {
		this.phoneMessageCfg = phoneMessageCfg;
	}

	public List<PhoneMessageCfg> getPhoneMessageCfgList() {
		return phoneMessageCfgList;
	}

	public void setPhoneMessageCfgList(List<PhoneMessageCfg> phoneMessageCfgList) {
		this.phoneMessageCfgList = phoneMessageCfgList;
	}

}
