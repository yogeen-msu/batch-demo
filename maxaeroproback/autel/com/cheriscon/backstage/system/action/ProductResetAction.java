package com.cheriscon.backstage.system.action;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.IProductResetLogService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductResetLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;

/**
 * @remark	产品恢复出厂设置（即注册后重新注册）action
 * @author chenqichuan	
 * @date   2013-9-26
 */
@ParentPackage("json_default")
@Namespace("/autel/product")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class ProductResetAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IAdminUserManager adminUserManager;
	@Resource
	private IProductResetLogService productResetService;
	@Resource
	private IProductInfoService productInfoService; 
	@Resource
	private ICustomerProductService customerProductService;
    @Resource
	private IProductSoftwareValidStatusService productSoftwareValidStatusService;
    
	private String jsonData;
	private ProductResetLog log;
	private String code;


	//获取产品重置列表
	public void list(){
		try{
			this.webpage = productResetService.pageProductReset(log,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "productResetList", results = { @Result(name = "productResetList", location = "/autel/backstage/system/productReset_list.jsp") })
	public String productResetList() throws Exception{
		list();
		return "productResetList";
	}

	@Action(value = "addProductReset", results = { @Result(name = "addProductReset", location = "/autel/backstage/system/productReset_add.jsp") })
	public String addProductReset() throws Exception{
		return "addProductReset";
	}
	@Action(value = "viewProductReset", results = { @Result(name = "viewProductReset", location = "/autel/backstage/system/productReset_view.jsp") })	
	public String viewProductReset() throws Exception{
		log=productResetService.getProductReset(code);
		return "viewProductReset";
	}
	
	@Action(value = "saveProductReset")
	public String saveProductReset() throws Exception{
		try{  
			ProductInfo info=productInfoService.getBySerialNo(log.getSerialNo());
			if(info==null){
				this.msgs.add(getText("memberman.testpackman.message8"));
				this.urls.put(getText("product.info.list"), "productResetList.do");
			}else if(info.getRegStatus().equals(0)){
				this.msgs.add("产品没有注册，不能重置");
				this.urls.put("返回", "javascript:history.go(-1);");
			}else{
				AdminUser user = adminUserManager.getCurrentUser();
				user = adminUserManager.get(user.getUserid());
				log.setResetUser(user.getUsername());
				log.setResetUserName(user.getUsername());
				String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
				log.setResetDate(dateStr);
				log.setProCode(info.getCode());
				log.setRegDate(info.getRegTime());
				
				log.setOldUserCode(customerProductService.getCustomerproinfo(info.getCode()));
				List<ProductSoftwareValidStatus> productSoftwareValidStatus=productSoftwareValidStatusService.getProductSoftwareValidStatusList(info.getCode());
				if(productSoftwareValidStatus!=null&& !productSoftwareValidStatus.isEmpty()){
					log.setValidDate(productSoftwareValidStatus.get(0).getValidDate());
				}
				    	
			    productResetService.saveLog(log);
				this.msgs.add(getText("common.js.addSuccess"));
				this.urls.put(getText("product.info.list"), "productResetList.do");
			}
		}catch(Exception e){
			this.msgs.add(getText("common.js.addFail"));
			this.urls.put(getText("product.info.list"), "productResetList.do");
		}
		
		return MESSAGE;
	}
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public ProductResetLog getLog() {
		return log;
	}

	public void setLog(ProductResetLog log) {
		this.log = log;
	}
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}	
