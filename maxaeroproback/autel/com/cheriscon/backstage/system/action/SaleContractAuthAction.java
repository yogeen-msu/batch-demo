package com.cheriscon.backstage.system.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.system.service.ISaleContractAuthService;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SaleContractAuth;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("json_default")
@Namespace("/autel/saleContractAuth")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class SaleContractAuthAction extends WWAction{

 
	private Integer id;
	private SaleContractAuth saleContractAuth;
	private List<AdminUser> adminUser;
	
	@Resource
	private ISaleContractAuthService saleContractAuthService;
	@Resource
	private IAdminUserManager adminUserManager;
	@Resource
	private ISaleContractService saleContractService;

	
	@Action(value = "listAddSaleContractAuth", results = { @Result(name = SUCCESS, location = "/autel/backstage/system/saleContractAuth_list.jsp") })
	public String listAddSaleContractAuth() throws Exception{
		try{
		this.webpage=saleContractAuthService.getSaleContractAuthPage(saleContractAuth, this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	//跳转到新增权限的界面
	@Action(value = "toAddSaleContractAuth", results = { @Result(name = SUCCESS, location = "/autel/backstage/system/saleContractAuth_add.jsp") })
	public String toAddSaleContractAuth() throws Exception{
		adminUser=adminUserManager.list();
		return SUCCESS;
	}
	//新增保存
	@SuppressWarnings("unchecked")
	@Action(value = "saveSaleContractAuth")
	public String saveSaleContractAuth(){
		try {
			
			int num=saleContractAuthService.getSaleContractAuth(saleContractAuth);
			if(num>0){
				this.msgs.add("新增失败：该管理员已经增加了该契约的修改权限");
				this.urls.put("返回列表","listAddSaleContractAuth.do");
			}else{
				SaleContract contract=saleContractService.getSaleContractByCode(saleContractAuth.getSaleContractCode());
				if(contract==null){
					this.msgs.add("新增失败：销售契约编码不存在");
					this.urls.put("返回列表","listAddSaleContractAuth.do");
				}else{
				
					saleContractAuthService.addSaleContractAuth(saleContractAuth);
					this.msgs.add("新增成功");
					this.urls.put("返回列表","listAddSaleContractAuth.do");
				}
			}
	
		} catch (Exception e) {
			this.msgs.add("新增失败，请联系管理员");
			this.urls.put("返回列表","listAddSaleContractAuth.do");
			e.printStackTrace();
		}
		return MESSAGE;
	}
	
	//删除权限
	@SuppressWarnings("unchecked")
	@Action(value = "delSaleContractAuth")
	public String delSaleContractAuth(){
		try {
			saleContractAuthService.delSaleContractAuthById(id);
			this.msgs.add("删除成功");
			this.urls.put("返回列表","listAddSaleContractAuth.do");
		} catch (Exception e) {
			this.msgs.add("删除失败，请联系管理员");
			this.urls.put("返回列表","listAddSaleContractAuth.do");
			e.printStackTrace();
		}
		return MESSAGE;
	}
	
	
	public Integer getId() {
		return id;
	}

	public SaleContractAuth getSaleContractAuth() {
		return saleContractAuth;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setSaleContractAuth(SaleContractAuth saleContractAuth) {
		this.saleContractAuth = saleContractAuth;
	}

	public List<AdminUser> getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(List<AdminUser> adminUser) {
		this.adminUser = adminUser;
	}
	
	
	

}
