package com.cheriscon.backstage.system.action;

import java.util.List;
import javax.annotation.Resource;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.ICountryCodeService;
import com.cheriscon.backstage.system.service.IStateProvinceService;
import com.cheriscon.common.model.CountryCode;
import com.cheriscon.common.model.StateProvince;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/state_province_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/state_province_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/state_province_edit.jsp")
})
public class StateProvinceAction extends WWAction {
	
	@Resource
	private IStateProvinceService stateProvinceService;
	@Resource
	private ICountryCodeService countryCodeService;
	
	private List<CountryCode> countrys;
	private StateProvince stateProvince;
	private Integer id;
	
	/**
	 * 显示列表
	 * @return
	 */
	public String list() {
		this.webpage = stateProvinceService.pageStateProvince(stateProvince, this.getPage(), this.getPageSize());
		return "list";
	}

	/**
	 * 新增
	 * @return
	 * @throws Exception
	 */
	public String add() throws Exception {
		this.countrys = countryCodeService.getCountryCodeList();
		return "add";
	}
	
	/**
	 * 新增保存
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String save() {
		List<StateProvince> list = stateProvinceService.getStateProvince(stateProvince.getState());
		if (null == list || list.size() == 0) {
			stateProvinceService.saveStateProvince(stateProvince);
			this.msgs.add(getText("common.js.addSuccess"));
		} else {
			this.msgs.add(getText("syssetting.province.hasexist"));
		}
		
		this.urls.put(getText("syssetting.province.list"), "state-province!list.do");
		return this.MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 * @throws Exception
	 */
	public String edit() throws Exception {
		stateProvince = stateProvinceService.getStateProvince(id);
		this.countrys = countryCodeService.getCountryCodeList();
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String update() {
		List<StateProvince> list = stateProvinceService.getStateProvince(stateProvince.getState());
		
		if (null == list || (null != list && list.get(0).getId().intValue() == stateProvince.getId().intValue())) {
			stateProvinceService.updateStateProvince(stateProvince);
			this.msgs.add(getText("common.js.modSuccess"));
		} else {
			this.msgs.add(getText("syssetting.province.hasexist"));
		}
		
		this.urls.put(getText("syssetting.province.list"), "state-province!list.do");
		return this.MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String delete() {
		try {
			stateProvinceService.deleteStateProvince(id);
			msgs.add(getText("common.js.del"));
		} catch (Exception e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}
		this.urls.put(getText("syssetting.province.list"), "state-province!list.do");
		return this.MESSAGE;
	}
	
	public StateProvince getStateProvince() {
		return stateProvince;
	}
	public void setStateProvince(StateProvince stateProvince) {
		this.stateProvince = stateProvince;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<CountryCode> getCountrys() {
		return countrys;
	}
	public void setCountrys(List<CountryCode> countrys) {
		this.countrys = countrys;
	}
	
}
