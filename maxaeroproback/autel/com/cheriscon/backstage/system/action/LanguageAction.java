package com.cheriscon.backstage.system.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/language_list.jsp"),
	@Result(name="add", location="/autel/backstage/system/language_add.jsp"),
	@Result(name="edit", location="/autel/backstage/system/language_edit.jsp"),
	@Result(name="select_list", location="/autel/backstage/system/language_select_list.jsp")
})
public class LanguageAction extends WWAction{
	
	private int id;
	private int isShow;
	private Language language;

	private List<Language> languageList;

	@Resource
	private ILanguageService languageService;

	// 显示列表
	public String list() {
		this.webpage = languageService.pageLanguage(this.getPage(), this.getPageSize());
		return "list";
	}

	// 显示列表
	public String selectlist() {
		languageList = languageService.queryNotSelectLanguage();
		return "select_list";
	}

	// 增加
	public String add() throws Exception {
		return "add";
	}

	/**
	 * 增加保存
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave() {
		List<Language> list = languageService.getByNewLanguage(language);

		if (list == null || list.size() == 0) {
			languageService.saveLanguage(language);
			this.msgs.add(getText("common.js.addSuccess"));
		} else {
			this.msgs.add(getText("syssetting.config.baselanguage.addfail"));
		}
		
		this.urls.put(getText("syssetting.config.baselanguagelist"), "language!list.do");
		
		return MESSAGE;
	}

	/**
	 * 修改
	 * 
	 * @return
	 */
	public String edit() {
		language = languageService.getById(id);
		return "edit";
	}

	/**
	 * 修改保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {

		List<Language> list = languageService.getByNewLanguage(language);

		boolean isExit = false;
		if (list != null && list.size() > 0) {
			Language exLan = list.get(0);
			if (language.getId() != exLan.getId()) {
				isExit = true;
			}
		}

		if (isExit) {
			this.msgs.add(getText("syssetting.config.baselanguage.updatefail"));
		} else {
			languageService.updateLanguage(language);
			this.msgs.add(getText("common.js.modSuccess"));
		}
		
		this.urls.put(getText("syssetting.config.baselanguagelist"), "language!list.do");
		return MESSAGE;
	}

	/**
	 * 删除
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			languageService.delLanguageById(id);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			// msgs.add("基本语言删除失败:" + e.getMessage());
			msgs.add(getText("common.js.delFail"));
		}

		this.urls.put(getText("syssetting.config.baselanguagelist"), "language!list.do");
		return MESSAGE;
	}

	/**
	 * 修改语言显示状态
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editIsShowState() throws Exception {
		try {
			languageService.updateIsShowState(id, isShow);
			msgs.add(getText("common.js.modSuccess"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.modFail"));
		}

		this.urls.put(getText("syssetting.config.baselanguagelist"), "language!list.do");
		return MESSAGE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}

	public int getIsShow() {
		return isShow;
	}

	public void setIsShow(int isShow) {
		this.isShow = isShow;
	}

}
