package com.cheriscon.backstage.system.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeLogService;
import com.cheriscon.common.model.ProductSoftwareValidChangeLog;
import com.cheriscon.framework.action.WWAction;


@SuppressWarnings("serial")
@Action
@ParentPackage("cop_default")
@Namespace("/autel/productSoftwareValidChangeLog")
@Result(name="list",location="/autel/backstage/log/productSoftwareValidChangeLog.jsp" )
public class ProductSoftwareValidChangeLogAction extends WWAction {

	private ProductSoftwareValidChangeLog log;
	@Resource
	private IProductSoftwareValidChangeLogService productSoftwareValidChangeLogServiceImpl;

	public String list() {
		this.webpage = productSoftwareValidChangeLogServiceImpl
				.pageProductSoftwareValidStatus(log, this.getPage(),
						this.getPageSize());
		return "list";
	}

	public void setLog(ProductSoftwareValidChangeLog log) {
		this.log = log;
	}

	public ProductSoftwareValidChangeLog getLog() {
		return log;
	}
	 
}
