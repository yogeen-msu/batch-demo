package com.cheriscon.backstage.system.action;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.cheriscon.app.base.core.model.MultiSite;
import com.cheriscon.app.base.core.service.IMultiSiteManager;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.CustomerComplaintInfoVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductContractService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.UpWarningInfo;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

public class ProductContractScheduler {

	
	
	@Resource
	private IProductContractService productContractService; 
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IProductInfoService productInfoService; 
	@Resource
	private IProductSoftwareValidStatusService validService;
	@Resource
	private IProductContractLogService productContractLogService;
	
	
    public void doContractWarning() {
	   
	   try {
		  List<UpWarningInfo> list=productContractService.queryList();
		  System.out.println("开始执行产品序列号销售区域预警处理===="+DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss")+"====数量："+list.size());
		  if(null!=list){
			  for(int i=0;i<list.size();i++){
				  UpWarningInfo u=list.get(i);
				  //1.根据当前销售契约编号查询基本配置
				  SaleContract con=saleContractService.getSaleContractByCode(u.getSaleCode());
				  
				  //2.根据当前序列号的销售配置查询美国销售中是否有对应的配置
				  SaleContract temp=new SaleContract();
				  temp.setAreaCfgCode("acf_North_America");  //美国区域
				  temp.setLanguageCfgCode(con.getLanguageCfgCode()); //产品语言
				  temp.setSaleCfgCode("scf_DS708_Basic"); //产品销售配置
				  temp.setSealerCode("sei201305101150570536");  //美国经销商编码
				  temp.setProTypeCode(con.getProTypeCode()); //升级卡类型
				  
				  List<SaleContract> tempList=saleContractService.getContrctList(temp);
				  if(null!=tempList && tempList.size()!=0){
					  SaleContract sale=tempList.get(0);
					  //3.更新当前序列号的销售契约
					  ProductInfo pinfo = productInfoService.getBySerialNo(u.getProductSn());
					  ProductContractChangeLog log=new ProductContractChangeLog();
					  log.setNewContract(sale.getCode());
					  log.setOldContract(pinfo.getSaleContractCode());
					  log.setOldMinSaleUnit("");
					  log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
					  log.setOperatorUser("系统自动运行");
					  log.setProductSN(u.getProductSn());
					  //.保存日志
					  productContractLogService.saveLog(log);
					
					  
					  
						
					  pinfo.setSaleContractCode(sale.getCode());
					  productInfoService.updateProductInfo(pinfo);
					  
					 //删除多余的最小销售单位有效期
					  validService.deleteMoreMinCode(pinfo.getCode());
					  //4.更新预警消息状态
					  productContractService.updateFlag(u.getWarningId());
					  
				  }
				  
			  }
		  }
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
  
}