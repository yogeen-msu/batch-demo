package com.cheriscon.backstage.system.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.member.service.IDataCompareService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;


/**
 * @remark	批量修改产品的销售契约
 * @author chenqichuan	
 * @date   2013-10-21
 */
@ParentPackage("json_default")
@Namespace("/autel/product")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class UpdateProductContractAction extends WWAction{
	
	private static final long serialVersionUID = -6977843643579742427L;

	@Resource
	private IAdminUserManager adminUserManager;
	@Resource
	private IProductInfoService productInfoService; 
	@Resource
	private IProductForSealerService productForSealerService;
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IProductSoftwareValidStatusService validService;
	@Resource
	private ISaleConfigService saleConfigService;
	@Resource
	private IProductContractLogService productContractService;
	@Resource
	private IDataCompareService dataCompareService;
	
	private String jsonData;
	private String code;
	private int editSetp;
	private String batchSerialNo;	//批量修改产品序列号
	private String saleContractCode;	//销售契约编号
	private String proTypeCode;				//产品型号
	private String proTypeName;				//产品型号名称
	private SaleContract contract=new SaleContract() ;
	private List<ProductForSealer> productTypeList;

	public List<ProductForSealer> getProductTypeList() {
		return productTypeList;
	}
	public void setProductTypeList(List<ProductForSealer> productTypeList) {
		this.productTypeList = productTypeList;
	}
	public SaleContract getContract() {
		return contract;
	}
	public void setContract(SaleContract contract) {
		this.contract = contract;
	}
	public String getProTypeCode() {
		return proTypeCode;
	}
	public String getProTypeName() {
		return proTypeName;
	}
	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}
	public String getBatchSerialNo() {
		return batchSerialNo;
	}
	public String getSaleContractCode() {
		return saleContractCode;
	}
	public void setBatchSerialNo(String batchSerialNo) {
		this.batchSerialNo = batchSerialNo;
	}
	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}
	public int getEditSetp() {
		return editSetp;
	}
	public void setEditSetp(int editSetp) {
		this.editSetp = editSetp;
	}

    //跳转到批量修改页面
	@Action(value = "editSaleContract", results = { @Result(name = "editSaleContract", location = "/autel/backstage/system/productInfo_edit_contract.jsp") })
	public String editSaleContract() throws Exception{
		editSetp=0;
		return "editSaleContract";
	}
	
	//跳转到批量修改页面20150616。海外售后专门用此接口来修改产品语言
		@Action(value = "editSaleContractForLanguage", results = { @Result(name = "editSaleContractForLanguage", location = "/autel/backstage/system/productInfo_edit_language.jsp") })
		public String editSaleContractForLanguage() throws Exception{
			editSetp=0;
			return "editSaleContractForLanguage";
		}
	
	
	/**
	 * 获取所有销售契约
	 * @return
	 */
	@Action(value = "selectsaleContract", results = { @Result(name = "selectsaleContract", location = "/autel/backstage/market/sale_contract_select_list2.jsp") })
	public String selectsaleContract() {
		
		try {
			productTypeList = productForSealerService.getProductForSealerList();
		//	contract.setProTypeCode(proTypeCode);
			this.webpage = saleContractService.pageSaleContract(contract,this.getPage(),8);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "selectsaleContract";
	}
	
	//修改产品契约
	@SuppressWarnings("unchecked")
	@Action(value = "saveEditContract", results = { @Result(name = "editSaleContract", location = "/autel/backstage/system/productInfo_edit_contract.jsp") })
	public String saveEditContract(){
		
		List<String> successList = new ArrayList<String>();		//修改成功的的产品序列号
		List<String> failList = new ArrayList<String>();		//修改失败的产品序列号
		
		List<String> hasRegList = new ArrayList<String>();		//已注册不用修改的的产品序列号
		List<String> noExitList = new ArrayList<String>();		//不存在的产品序列号
		Map<String,String> proTypeMap = new HashMap<String,String>();	//产品型号
		Map<String,String> contractMap = new HashMap<String,String>();	//销售契约
		try {
		if(editSetp == 1){	//校验输入的产品序列号
			boolean isPass = true;
			if(StringUtils.isEmpty(batchSerialNo)){
				this.msgs.add(getText("product.info.serno.notempty"));
				this.urls.put(getText("product.info.goback"), "editSaleContract.do");
				return MESSAGE;
			}
			String [] seriaArr = batchSerialNo.trim().split(",");
			for (int i = 0; i < seriaArr.length; i++) {
				String serialNo = seriaArr[i].trim();
				ProductInfo pinfo = productInfoService.getBySerialNo(serialNo);
				
				if(pinfo == null){
					isPass = false;
					noExitList.add(serialNo);
					continue;
				}else{
					proTypeCode=pinfo.getProTypeCode();
				}
				/*if(pinfo.getRegStatus() != FrontConstant.PRODUCT_REGSTATUS_YES){	//未注册的产品序列号不允许在这里修改
					isPass = false;
					hasRegList.add(serialNo);
					continue;
				}*/
				//型号名称 	对应的产品根据产品型号归类
				/*String typeSeriaNos= proTypeMap.get(pinfo.getProTypeCode());
				if(StringUtils.isEmpty(typeSeriaNos)){
					proTypeMap.put(pinfo.getProTypeCode().trim(),serialNo);
				}else{
					proTypeMap.put(pinfo.getProTypeCode().trim(),typeSeriaNos+","+serialNo);
				}*/
				
				//销售契约 	对应的销售契约根据产品型号归类
				/*String typeContracts= proTypeMap.get(pinfo.getSaleContractCode());
				if(StringUtils.isEmpty(typeContracts)){
					contractMap.put(pinfo.getSaleContractCode().trim(),serialNo);
				}else{
					contractMap.put(pinfo.getSaleContractCode().trim(),typeSeriaNos+","+serialNo);
				}*/
				
			}
			String tipMsg = getText("product.info.input.sernoerror");
			//存在不同类型的产品
			/*if(proTypeMap.keySet().size() > 1){
				isPass = false;
				tipMsg += ","+getText("product.info.hasdifferent.serno");
			}*/
			//存在不同类型的销售契约
			/*if(contractMap.keySet().size() > 1){
				isPass = false;
				tipMsg += ",存在不同类型的销售契约";
			}*/
			
			if(!isPass) this.msgs.add(tipMsg);
			int i = 1;
			if(noExitList.size() > 0){
				this.msgs.add(i + " ). "+getText("product.info.serno.error")+"： "+StringUtils.join(noExitList, ","));
				i++;
			}
			
			/*if(hasRegList.size() > 0){
				this.msgs.add(i + " ). 产品序列号未注册： "+StringUtils.join(hasRegList, ","));
				i++;
			}
			*/
			/*if(proTypeMap.keySet().size() > 1){
				int j = 1;
				for (String key : proTypeMap.keySet()) {
					ProductForSealer productType=productForSealerService.getProductForSealerByCode(key);
					this.msgs.add(i + " ). "+getText("product.producttypevo")+""+j+"  ("+productType.getName() + ")： "+proTypeMap.get(key));
					i++;
					j++;
				}
			}*/
			
			/*if(contractMap.keySet().size() > 1){
				int j = 1;
				for (String key : contractMap.keySet()) {
					SaleContract contract=saleContractService.getSaleContractByCode(key);
					this.msgs.add(i + " ). "+getText("marketman.salecontractman.sale.contract")+""+j+"  ("+contract.getName() + ")： "+contractMap.get(key));
					i++;
					j++;
				}
			}*/
			
			
			if(!isPass){	//校验没有通过
				this.urls.put(getText("product.info.goback"), "javascript:history.go(-1);");
				return MESSAGE;
			}else{
			/*	ProductForSealer productType=productForSealerService.getProductForSealerByCode(proTypeCode);
				proTypeCode = productType.getCode();
				proTypeName = productType.getName();*/
				return "editSaleContract";
			}
		}
		if(editSetp == 2){	//修改销售契约
			SaleContract contract = saleContractService.getSaleContractByCode(saleContractCode);
			if(contract==null){
				this.msgs.add(getText("选择的销售契约错误"));
				this.urls.put(getText("product.info.goback"), "javascript:history.go(-1);");
				this.urls.put(getText("common.update.continue"), "editSaleContract.do");
				return MESSAGE;
			}
			String [] seriaArr = batchSerialNo.trim().split(",");
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			String ip=FrontConstant.getIpAddr(request);
			String validDate="";
			for (int i = 0; i < seriaArr.length; i++) {
				String serialNo = seriaArr[i].trim();
				ProductInfo pinfo = productInfoService.getBySerialNo(serialNo);
				//在这里进行判断，产品是否注册，
				if(pinfo.getRegStatus() != FrontConstant.PRODUCT_REGSTATUS_YES){
					ProductContractChangeLog log=new ProductContractChangeLog();
					log.setNewContract(saleContractCode);
					log.setOldContract(pinfo.getSaleContractCode());
					log.setOperatorIp(ip);
					log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
					log.setOperatorUser(user.getUsername());
					log.setProductSN(serialNo);
					//1.保存日志
					productContractService.saveLog(log);
					pinfo.setProTypeCode(contract.getProTypeCode());
					pinfo.setSaleContractCode(contract.getCode());
					pinfo.setSaleContractName(contract.getName());
					try {
						productInfoService.updateProductInfo(pinfo);
						successList.add(serialNo);
					} catch (Exception e) {
						failList.add(serialNo);
					}
				}else{
					List<ProductSoftwareValidStatus> list=validService.getProductSoftwareValidStatusList(pinfo.getCode());
					for(int j=0;j<list.size();j++){
						ProductSoftwareValidStatus valid=list.get(j);
						validDate=valid.getValidDate();
						ProductContractChangeLog log=new ProductContractChangeLog();
						log.setNewContract(saleContractCode);
						log.setOldContract(pinfo.getSaleContractCode());
						log.setOldMinSaleUnit(valid.getMinSaleUnitCode());
						log.setOperatorIp(ip);
						log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
						log.setOperatorUser(user.getUsername());
						log.setProductSN(serialNo);
						//1.保存日志
						productContractService.saveLog(log);
						//2.删除有效期
						validService.delProductSoftwareValidStatusById(valid.getId());
					}
					List<MinSaleUnit> listUnit=saleConfigService.queryMinSaleUnits(contract.getSaleCfgCode());
					if(listUnit==null || listUnit.size()==0){
						this.msgs.add(getText("选择的销售契约标准销售配置没有最小销售单位"));
						this.urls.put(getText("product.info.goback"), "javascript:history.go(-1);");
						this.urls.put(getText("common.update.continue"), "editSaleContract.do");
						return MESSAGE;
					}
					//更新销售配置
					pinfo.setProTypeCode(contract.getProTypeCode());
					pinfo.setSaleContractCode(saleContractCode);
					productInfoService.updateProductInfo(pinfo);
					
					//4.插入新的标准销售配置
					for(int k=0;k<listUnit.size();k++){
						MinSaleUnit unit=listUnit.get(k);
						ProductSoftwareValidStatus valid=new ProductSoftwareValidStatus();
						valid.setMinSaleUnitCode(unit.getCode());
						valid.setProCode(pinfo.getCode());
						valid.setValidDate(validDate);
						validService.saveProductSoftwareValidStatus(valid);
					}
					successList.add(serialNo);
			}
			}
			
			if(successList.size() == seriaArr.length){
				this.msgs.add(getText("salescontract.batchedit.fail"));
			}
			this.urls.put(getText("common.update.continue"), "editSaleContract.do");
			return MESSAGE;
			
		}
		}catch(Exception e){
			this.msgs.add(getText("salescontract.configdata.error"));
			this.urls.put(getText("common.update.continue"), "editSaleContract.do");
			return MESSAGE;
		}
		
		
		return MESSAGE;
	}
	
	
	//修改产品契约
		@SuppressWarnings("unchecked")
		@Action(value = "saveEditContractForLanguage", results = { @Result(name = "editSaleContractForLanguage", location = "/autel/backstage/system/productInfo_edit_language.jsp") })
		public String saveEditContractForLanguage(){
			
			List<String> successList = new ArrayList<String>();		//修改成功的的产品序列号
			List<String> failList = new ArrayList<String>();		//修改失败的产品序列号
			
			List<String> hasRegList = new ArrayList<String>();		//已注册不用修改的的产品序列号
			List<String> noExitList = new ArrayList<String>();		//不存在的产品序列号
			Map<String,String> proTypeMap = new HashMap<String,String>();	//产品型号
			Map<String,String> contractMap = new HashMap<String,String>();	//销售契约
			try {
			if(editSetp == 1){	//校验输入的产品序列号
				boolean isPass = true;
				if(StringUtils.isEmpty(batchSerialNo)){
					this.msgs.add(getText("product.info.serno.notempty"));
					this.urls.put(getText("product.info.goback"), "editSaleContractForLanguage.do");
					return MESSAGE;
				}
				String [] seriaArr = batchSerialNo.trim().split(",");
				for (int i = 0; i < seriaArr.length; i++) {
					String serialNo = seriaArr[i].trim();
					ProductInfo pinfo = productInfoService.getBySerialNo(serialNo);
					
					if(pinfo == null){
						isPass = false;
						noExitList.add(serialNo);
						continue;
					}else{
						proTypeCode=pinfo.getProTypeCode();
					}
					
					String typeSeriaNos= proTypeMap.get(pinfo.getProTypeCode());
					if(StringUtils.isEmpty(typeSeriaNos)){
						proTypeMap.put(pinfo.getProTypeCode().trim(),serialNo);
					}else{
						proTypeMap.put(pinfo.getProTypeCode().trim(),typeSeriaNos+","+serialNo);
					}
				}
				String tipMsg = getText("product.info.input.sernoerror");
				//存在不同类型的产品
				if(proTypeMap.keySet().size() > 1){
					isPass = false;
					tipMsg += ","+getText("product.info.hasdifferent.serno");
				}
				//存在不同类型的销售契约
				/*if(contractMap.keySet().size() > 1){
					isPass = false;
					tipMsg += ",存在不同类型的销售契约";
				}*/
				
				if(!isPass) this.msgs.add(tipMsg);
				int i = 1;
				if(noExitList.size() > 0){
					this.msgs.add(i + " ). "+getText("product.info.serno.error")+"： "+StringUtils.join(noExitList, ","));
					i++;
				}
				
				
				if(!isPass){	//校验没有通过
					this.urls.put(getText("product.info.goback"), "javascript:history.go(-1);");
					return MESSAGE;
				}else{
					return "editSaleContractForLanguage";
				}
			}
			if(editSetp == 2){	//修改销售契约
				SaleContract contract = saleContractService.getSaleContractByCode(saleContractCode);
				if(contract==null){
					this.msgs.add(getText("选择的销售契约错误"));
					this.urls.put(getText("product.info.goback"), "javascript:history.go(-1);");
					this.urls.put(getText("common.update.continue"), "editSaleContractForLanguage.do");
					return MESSAGE;
				}
				String [] seriaArr = batchSerialNo.trim().split(",");
				HttpServletRequest request=ThreadContextHolder.getHttpRequest();
				AdminUser user = adminUserManager.getCurrentUser();
				user = adminUserManager.get(user.getUserid());
				String ip=FrontConstant.getIpAddr(request);
				for (int i = 0; i < seriaArr.length; i++) {
					String serialNo = seriaArr[i].trim();
					ProductInfo pinfo = productInfoService.getBySerialNo(serialNo);
					if(contract.getProTypeCode().equals(pinfo.getProTypeCode())){
						ProductContractChangeLog log=new ProductContractChangeLog();
						log.setNewContract(saleContractCode);
						log.setOldContract(pinfo.getSaleContractCode());
						log.setOperatorIp(ip);
						log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
						log.setOperatorUser(user.getUsername());
						log.setProductSN(serialNo);
						//1.保存日志
						productContractService.saveLog(log);
						pinfo.setProTypeCode(contract.getProTypeCode());
						pinfo.setSaleContractCode(contract.getCode());
						pinfo.setSaleContractName(contract.getName());
						try {
							productInfoService.updateProductInfo(pinfo);
							dataCompareService.insertProductMinSaleSearch(serialNo);
							successList.add(serialNo);
						} catch (Exception e) {
							failList.add(serialNo);
						}
					}else{
						failList.add(serialNo);
					}
				}
				
				if(successList.size() == seriaArr.length){
					this.msgs.add(getText("salescontract.batchedit.fail"));
				}
				if(failList.size()>0){
					for(int j=0;j<failList.size();j++){
						this.msgs.add("序列号："+failList.get(j)+"添加失败，请检查序列号产品类型和销售契约的产品类型是否一致");
					}
					this.urls.put(getText("product.info.goback"), "javascript:history.go(-1);");
					return MESSAGE;
				}
				this.urls.put(getText("common.update.continue"), "editSaleContractForLanguage.do");
				return MESSAGE;
				
			}
			}catch(Exception e){
				this.msgs.add(getText("salescontract.configdata.error"));
				this.urls.put(getText("common.update.continue"), "editSaleContractForLanguage.do");
				return MESSAGE;
			}
			
			
			return MESSAGE;
		}
		
	
	
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}	
