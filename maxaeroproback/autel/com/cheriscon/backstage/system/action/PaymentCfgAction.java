package com.cheriscon.backstage.system.action;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.system.service.IPaymentCfgService;
import com.cheriscon.common.model.PaymentCfg;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.plugin.IPlugin;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/system/payment_list.jsp"),
	@Result(name="input", location="/autel/backstage/system/payment_input.jsp")
})
public class PaymentCfgAction extends WWAction{
	
	private List<PaymentCfg> paymentCfglist;
	private List<IPlugin> pluginList;
	private Integer paymentId;
	private String pluginId;
	private Integer[] id; //删除用
	private String name; 
	private String type;
	private String biref;
	
	@Resource
	private IPaymentCfgService paymentCfgService;
	
	/**
	 * 列表
	 * @return
	 */
	public String list(){
		paymentCfglist  = this.paymentCfgService.list();
		return "list";
	}
	
	/**
	 * 到添加页 
	 * @return
	 */
	public String add(){	
		this.pluginList = this.paymentCfgService.listAvailablePlugins();
		return INPUT;
	}
	
	public String getPluginHtml(){
		try{
			this.json = this.paymentCfgService.getPluginInstallHtml(pluginId, paymentId);
		}catch(RuntimeException e){
			this.json = e.getMessage();
		}
		return JSON_MESSAGE;
	}
	
	/**
	 * 到修改页面
	 * @return
	 */
	public String edit(){
		this.pluginList = this.paymentCfgService.listAvailablePlugins();
		PaymentCfg cfg  = this.paymentCfgService.get(paymentId);
		this.name= cfg.getName();
		this.type= cfg.getType();
		this.biref= cfg.getBiref();
		return INPUT;
	}
	
	
	/**
	 * 保存添加
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String saveAdd(){
		try{
			HttpServletRequest  request = this.getRequest();
			Enumeration<String> names = request.getParameterNames();
			Map<String,String> params = new HashMap<String, String>();
			while(names.hasMoreElements()){
				String name= names.nextElement();
				if("name".equals(name)) continue;
				if("type".equals(name)) continue;
				if("biref".equals(name)) continue;
				if("paymentId".equals(name)) continue;
				if("submit".equals(name)) continue;
				String value  = request.getParameter(name);
				params.put(name, value);
			}
			
			this.paymentCfgService.add(name, type, biref, params);
			this.msgs.add(getText("common.js.addSuccess"));
			this.urls.put(getText("pay.paytype.list"), "payment-cfg!list.do");
		}catch(RuntimeException e){
			this.msgs.add(e.getMessage());
			this.urls.put(getText("pay.paytype.list"), "payment-cfg!add.do");
		} 		
		return MESSAGE;
	}
	
	
	public String save(){
		
		if(paymentId==null || "".equals(paymentId)){
			return this.saveAdd();
		}else{
			return this.saveEdit();
		}
		
	}
	
	
	/**
	 * 保存修改 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String saveEdit(){
		try{
			HttpServletRequest  request = this.getRequest();
			Enumeration<String> names = request.getParameterNames();
			Map<String,String> params = new HashMap<String, String>();
			while(names.hasMoreElements()){
				String name= names.nextElement();
				
				if("name".equals(name)) continue;
				if("type".equals(name)) continue;
				if("biref".equals(name)) continue;
				if("paymentId".equals(name)) continue;
				if("submit".equals(name)) continue;
				String value  = request.getParameter(name);
				params.put(name, value);
			}
			this.paymentCfgService.edit(paymentId,name,type, biref, params);
			this.msgs.add(getText("common.js.modSuccess"));
			this.urls.put(getText("pay.paytype.list"), "payment-cfg!list.do");
		}catch(RuntimeException e){
			this.msgs.add(e.getMessage());
		} 		
		return MESSAGE;
	}
	
	
	/**
	 * 删除
	 * @return
	 */
	public String delete(){
		try{
			this.paymentCfgService.delete(id);
			this.json="{result:0,message:'"+getText("common.js.del")+"'}";
		}catch(RuntimeException e){
			this.json="{result:1,message:'"+e.getMessage()+"'}";
		}
		return JSON_MESSAGE;
	}

	public List<PaymentCfg> getPaymentCfglist() {
		return paymentCfglist;
	}

	public void setPaymentCfglist(List<PaymentCfg> paymentCfglist) {
		this.paymentCfglist = paymentCfglist;
	}

	public List<IPlugin> getPluginList() {
		return pluginList;
	}

	public void setPluginList(List<IPlugin> pluginList) {
		this.pluginList = pluginList;
	}

	public Integer getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}

	public String getPluginId() {
		return pluginId;
	}

	public void setPluginId(String pluginId) {
		this.pluginId = pluginId;
	}

	public Integer[] getId() {
		return id;
	}

	public void setId(Integer[] id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBiref() {
		return biref;
	}

	public void setBiref(String biref) {
		this.biref = biref;
	}
	
	
	
}
