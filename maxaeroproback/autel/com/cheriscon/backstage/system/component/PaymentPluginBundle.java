package com.cheriscon.backstage.system.component;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cheriscon.framework.plugin.AutoRegisterPluginsBundle;
import com.cheriscon.framework.plugin.IPlugin;

@Component
public class PaymentPluginBundle extends AutoRegisterPluginsBundle {

	public String getName() {
		return "支付插件桩";
	}

	public List<IPlugin> getPluginList() {
		return this.getPlugins();
	}

}
