package com.cheriscon.backstage.system.component.plugin.paypal;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.system.component.AbstractPaymentPlugin;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;

/**
 * Paypal支付插件 
 */
@Component
public class PaypalPlugin extends AbstractPaymentPlugin/*  implements IPaymentEvent*/ {

	@Override
	public String getId() {
		return "paypalPlugin";
	}

	@Override
	public String getName() {
		return FreeMarkerPaser.getBundleValue("syssetting.payset.paypalinterface");
	}



}
