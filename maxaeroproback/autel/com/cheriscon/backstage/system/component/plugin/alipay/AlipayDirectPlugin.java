package com.cheriscon.backstage.system.component.plugin.alipay;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.system.component.AbstractPaymentPlugin;

/**
 * 支付宝即时到账插件 
 */
@Component
public class AlipayDirectPlugin extends AbstractPaymentPlugin/*  implements IPaymentEvent*/ {

	@Override
	public String getId() {
		return "alipayDirectPlugin";
	}

	@Override
	public String getName() {
		return "支付宝即时到帐接口";
	}



}
