package com.cheriscon.backstage.system.component.plugin.rate;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.system.component.AbstractPaymentPlugin;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;

/**
 * Paypal支付插件 
 */
@Component
public class RatePlugin extends AbstractPaymentPlugin/*  implements IPaymentEvent*/ {

	@Override
	public String getId() {
		return "ratePlugin";
	}

	@Override
	public String getName() {
		String name = FreeMarkerPaser.getBundleValue("syssetting.payset.rateconfig");
		return name;
//		return "汇率配置";
	}



}
