/**
*/ 
package com.cheriscon.backstage.main.action;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.ICustomerComplaintInfoVoService;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;

/**
 * @ClassName: IndexItemAction
 * @Description: 后台首页Action
 * @author pengdongan
 * @date 2013-03-12
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/main")
public class IndexItemAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4882565829771043191L;

	@Resource
	private ICustomerComplaintInfoVoService customerComplaintInfoVoService;

	private IAdminUserManager adminUserManager;
	
	private AdminUser user;
	
	private Map complaintCount;
	
	@Action(value = "indexItemCustomerComplaint", results = { @Result(name = SUCCESS, location="/autel/backstage/index/index_complaint.jsp") })
	public String indexItemCustomerComplaint() throws Exception{
		complaintCount = customerComplaintInfoVoService.complaintCountByUser(user);
		return SUCCESS;
	}

	
	public AdminUser getUser() {
		return user;
	}

	public void setUser(AdminUser user) {
		this.user = user;
	}


	public Map getComplaintCount() {
		return complaintCount;
	}


	public void setComplaintCount(Map complaintCount) {
		this.complaintCount = complaintCount;
	}
}
