/**
*/ 
package com.cheriscon.backstage.main.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.spring.SpringContextHolder;

/**
 * @ClassName: AdminAction
 * @Description: 后台首页构建
 * @author shaohu
 * @date 2013-1-9 下午05:49:17
 * 
 */
@ParentPackage("json-default")
@Namespace("/autel/main")
public class AdminAction extends WWAction{

	private static final long serialVersionUID = -7898581432038909708L;

	private IAdminUserManager adminUserManager;
	
	private AdminUser user;
	
	public AdminUser getUser() {
		return user;
	}

	public void setUser(AdminUser user) {
		this.user = user;
	}
	
	@Action(value = "welcome", results = { @Result(name = SUCCESS, location="/autel/backstage/main/welcome.jsp") })
	public String welcome() throws Exception{
		return SUCCESS;
	}

	@Action(value = "top", results = { @Result(name = SUCCESS, location="/autel/backstage/main/top.jsp") })
	public String top() throws Exception{
		adminUserManager  = SpringContextHolder.getBean("adminUserManager");
		user  =adminUserManager.getCurrentUser();
		user = adminUserManager.get(user.getUserid());
		return SUCCESS;
	}
	
	@Action(value = "content", results = { @Result(name = SUCCESS, location="/autel/backstage/main/content.jsp") })
	public String content() throws Exception{
		return SUCCESS;
	}
	
	@Action(value = "left", results = { @Result(name = SUCCESS, location="/autel/backstage/main/left.jsp") })
	public String left() throws Exception{
		adminUserManager  = SpringContextHolder.getBean("adminUserManager");
		user  =adminUserManager.getCurrentUser();
		user = adminUserManager.get(user.getUserid());
		return SUCCESS;
	}
	
	@Action(value = "right", results = { @Result(name = SUCCESS, location="/autel/backstage/main/right.jsp") })
	public String right() throws Exception{
		return SUCCESS;
	}
}
