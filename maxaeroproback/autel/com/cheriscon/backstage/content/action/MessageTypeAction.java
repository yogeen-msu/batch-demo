package com.cheriscon.backstage.content.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.content.service.IMessageTypeService;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/content/messageType_list.jsp"),
	@Result(name="add", location="/autel/backstage/content/messageType_add.jsp"),
	@Result(name="edit", location="/autel/backstage/content/messageType_edit.jsp")
})
public class MessageTypeAction extends WWAction{
	
	private int id;
	private MessageType messageType;
	
	private List<MessageType> messageTypeList;
	
	@Resource
	private IMessageTypeService messageTypeService; 
	
	// 显示列表
	public String list() {
		this.messageTypeList = messageTypeService.queryMessageType();
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		return "add";
	}
	
	/**
	 * 增加保存 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		
		messageTypeService.saveMessageType(messageType);
		this.msgs.add(getText("common.js.addSuccess"));
		this.urls.put(getText("cententman.messageman.listclassify"), "message-type!list.do");
		
		return MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		messageType = messageTypeService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		
		messageTypeService.updateMessageType(messageType);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("cententman.messageman.listclassify"), "message-type!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			messageTypeService.delMessageTypeById(id);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}

		this.urls.put(getText("cententman.messageman.listclassify"), "message-type!list.do");
		return MESSAGE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public List<MessageType> getMessageTypeList() {
		return messageTypeList;
	}

	public void setMessageTypeList(List<MessageType> messageTypeList) {
		this.messageTypeList = messageTypeList;
	}


}
