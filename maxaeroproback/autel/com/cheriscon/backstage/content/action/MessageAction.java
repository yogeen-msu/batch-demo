package com.cheriscon.backstage.content.action;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.content.service.IMessageTypeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.Message;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
		@Result(name = "list", location = "/autel/backstage/content/message_list.jsp"),
		@Result(name = "add", location = "/autel/backstage/content/message_add.jsp"),
		@Result(name = "edit", location = "/autel/backstage/content/message_edit.jsp"),
		@Result(name = "statistic", location = "/autel/backstage/content/statistic.jsp")})
public class MessageAction extends WWAction {

	private int id;
	private int status;
	private Message message;
	private List<Language> languageList;				//语言
	private List<MessageType> messageTypeList;			//消息分类
	
	private String msgIds; // 多个消息ID
	
	private List<Map> statisticList;				//消息统计信息

	@Resource
	private ILanguageService languagService;
	@Resource
	private IMessageService messageService;
	@Resource
	private IMessageTypeService messageTypeService;

	// 显示列表
	public String list() {
		languageList = languagService.queryLanguage();
		messageTypeList = messageTypeService.queryMessageType();
		this.webpage = messageService.pageMessage(message, this.getPage(),this.getPageSize());
		return "list";
	}

	// 增加
	public String add() throws Exception {
		languageList = languagService.queryLanguage();
		messageTypeList = messageTypeService.queryMessageType();
		return "add";
	}

	/**
	 * 增加保存
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String addSave() {

		messageService.saveMessage(message);
		this.msgs.add(getText("common.js.addSuccess"));
		this.urls.put(getText("cententman.messageman.allmsg"), "message!list.do");
		return this.MESSAGE;
	}

	/**
	 * 修改
	 * 
	 * @return
	 */
	public String edit() {
		languageList = languagService.queryLanguage();
		messageTypeList = messageTypeService.queryMessageType();
		message = messageService.getById(id);
		return "edit";
	}

	/**
	 * 修改保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public String editSave() throws Exception {
		messageService.updateMessage(message);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("cententman.messageman.allmsg"), "message!list.do");
		return this.MESSAGE;
	}
	
	/**
	 * 修改状态
	 * @return
	 * @throws Exception
	 */
	public String editStatus() throws Exception {
		try{
			if(!StringUtils.isEmpty(msgIds)){
				messageService.updateStatus(msgIds, status);
			}
			this.json="{result:1}";
		}catch(RuntimeException e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}
		return JSON_MESSAGE;
	}
	
	/**
	 * 修改弹出显示状态
	 * @return
	 * @throws Exception
	 */
	public String editDialogStatus() throws Exception {
		try{
			messageService.editDialogStatus(id, status);
			this.json="{result:1}";
		}catch(RuntimeException e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}
		return JSON_MESSAGE;
	}
	
	
	/**
	 * 删除
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			messageService.delMessageById(id);
			msgs.add(getText("common.js.del"));
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}

		this.urls.put(getText("cententman.messageman.allmsg"), "message!list.do");
		return MESSAGE;
	}
	
	public String statistics(){
		statisticList =	messageService.queryMesageCount();
		return "statistic";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}

	public List<MessageType> getMessageTypeList() {
		return messageTypeList;
	}

	public void setMessageTypeList(List<MessageType> messageTypeList) {
		this.messageTypeList = messageTypeList;
	}

	public String getMsgIds() {
		return msgIds;
	}

	public void setMsgIds(String msgIds) {
		this.msgIds = msgIds;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Map> getStatisticList() {
		return statisticList;
	}

	public void setStatisticList(List<Map> statisticList) {
		this.statisticList = statisticList;
	}
	
	
}
