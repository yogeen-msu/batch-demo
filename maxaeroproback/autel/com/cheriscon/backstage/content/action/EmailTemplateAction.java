package com.cheriscon.backstage.content.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/content/emailTemplate_list.jsp"),
	@Result(name="add", location="/autel/backstage/content/emailTemplate_add.jsp"),
	@Result(name="edit", location="/autel/backstage/content/emailTemplate_edit.jsp")
})
public class EmailTemplateAction extends WWAction{
	
	private int id;
	private int status;			//状态
	private EmailTemplate emailTemplate;
	private List<Language> languageList;
	
	@Resource
	private IEmailTemplateService emailTemplateService; 
	@Resource
	private ILanguageService languageService;
//	@Resource
//	private EmailProducer emailProducer;
	
	/**
	 * 邮件发送测试
	 */
//	@SuppressWarnings("unchecked")
	public void sendMailTest(){
//		EmailModel emailModel = new EmailModel();
//		emailModel.getData().put("username","12313");		//username 为freemarker模版中的参数
//		
//		emailModel.setTitle("您好，这是autel测试邮件!");
//		emailModel.setTo("251911078@qq.com");
//		EmailTemplate template = emailTemplateService.getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REGIST,"lag201301070146010554");
//		emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
//		emailProducer.send(emailModel);
	}
	
	// 显示列表
	public String list() {
		this.webpage = emailTemplateService.pageEmailTemplate(emailTemplate,this.getPage(), this.getPageSize());
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		languageList = languageService.queryLanguage();
		return "add";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		emailTemplateService.saveEmailTemplate(emailTemplate);
		this.msgs.add(getText("common.js.addSuccess"));
		this.urls.put(getText("cententman.messageman.emailmodellist"), "email-template!list.do");
		
		return MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		languageList = languageService.queryLanguage();
		emailTemplate = emailTemplateService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		emailTemplateService.updateEmailTemplate(emailTemplate);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("cententman.messageman.emailmodellist"), "email-template!list.do");
		return MESSAGE;
	}
	
	/**
	 * 修改状态
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editStatus() throws Exception {
		emailTemplateService.updateStatus(id, status);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("cententman.messageman.emailmodellist"), "email-template!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			int result = emailTemplateService.delEmailTemplateById(id);
			if(CTConsatnt.CT_IS_USE == result){
				msgs.add(getText("cententman.messageman.email.usingnotremove"));
			}else if(CTConsatnt.CT_SUCCESS  == result){
				msgs.add(getText("common.js.del"));
			}
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}

		this.urls.put(getText("cententman.messageman.emailmodellist"), "email-template!list.do");
		return MESSAGE;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public EmailTemplate getEmailTemplate() {
		return emailTemplate;
	}

	public void setEmailTemplate(EmailTemplate emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}
	

}
