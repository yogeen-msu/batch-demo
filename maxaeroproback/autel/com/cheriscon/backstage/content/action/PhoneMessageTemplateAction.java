package com.cheriscon.backstage.content.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IPhoneMessageTemplateService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.PhoneMessageTemplate;
import com.cheriscon.framework.action.WWAction;

@SuppressWarnings("serial")
@ParentPackage("cop_default")
@Namespace("/core/admin")
@Results({
	@Result(name="list", location="/autel/backstage/content/phoneMessageTemplate_list.jsp"),
	@Result(name="add", location="/autel/backstage/content/phoneMessageTemplate_add.jsp"),
	@Result(name="edit", location="/autel/backstage/content/phoneMessageTemplate_edit.jsp")
})
public class PhoneMessageTemplateAction extends WWAction{
	
	private int id;
	private int status;			//状态
	private PhoneMessageTemplate phoneMessageTemplate;
	private List<Language> languageList;
	
	@Resource
	private IPhoneMessageTemplateService phoneMessageTemplateService; 
	@Resource
	private ILanguageService languageService;
	
	
	// 显示列表
	public String list() {
		this.webpage = phoneMessageTemplateService.pagePhoneMessageTemplate(phoneMessageTemplate,this.getPage(), this.getPageSize());
		return "list";
	}
	
	// 增加
	public String add() throws Exception {
		languageList = languageService.queryLanguage();
		return "add";
	}
	
	/**
	 * 增加保存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String addSave(){
		phoneMessageTemplateService.savePhoneMessageTemplate(phoneMessageTemplate);
		this.msgs.add(getText("common.js.addSuccess"));
		this.urls.put(getText("cententman.messageman.listsmsmodel"), "phone-message-template!list.do");
		
		return MESSAGE;
	}
	
	/**
	 * 修改
	 * @return
	 */
	public String edit() {
		languageList = languageService.queryLanguage();
		phoneMessageTemplate = phoneMessageTemplateService.getById(id);
		return "edit";
	}
	
	/**
	 * 修改保存
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editSave() throws Exception {
		phoneMessageTemplateService.updatePhoneMessageTemplate(phoneMessageTemplate);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("cententman.messageman.listsmsmodel"), "phone-message-template!list.do");
		return MESSAGE;
	}
	
	/**
	 * 修改状态
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String editStatus() throws Exception {
		phoneMessageTemplateService.updateStatus(id, status);
		this.msgs.add(getText("common.js.modSuccess"));
		this.urls.put(getText("cententman.messageman.listsmsmodel"), "phone-message-template!list.do");
		return MESSAGE;
	}
	
	/**
	 * 删除
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String delete() throws Exception {
		try {
			int result = phoneMessageTemplateService.delPhoneMessageTemplateById(id);
			if(CTConsatnt.CT_IS_USE == result){
				msgs.add(getText("cententman.messageman.useingnotremove"));
			}else if(CTConsatnt.CT_SUCCESS  == result){
				msgs.add(getText("common.js.del"));
			}
		} catch (RuntimeException e) {
			msgs.add(getText("common.js.delFail")+":" + e.getMessage());
		}

		this.urls.put(getText("cententman.messageman.listsmsmodel"), "phone-message-template!list.do");
		return MESSAGE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public PhoneMessageTemplate getPhoneMessageTemplate() {
		return phoneMessageTemplate;
	}

	public void setPhoneMessageTemplate(PhoneMessageTemplate phoneMessageTemplate) {
		this.phoneMessageTemplate = phoneMessageTemplate;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}

	

}
