package com.cheriscon.backstage.content.component;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.app.cms.core.model.DataField;
import com.cheriscon.app.cms.core.model.DataModel;
import com.cheriscon.app.cms.core.service.IDataFieldManager;
import com.cheriscon.app.cms.core.service.IDataModelManager;
import com.cheriscon.backstage.content.component.plugin.LanguageSelectPlugin;
import com.cheriscon.framework.component.IComponent;
import com.cheriscon.framework.database.IDaoSupport;

 /**
  *CMS扩展组件
  */
@Component
public class CmsExtendComponent implements IComponent {
	
	@Resource
	private IDataModelManager dataModelManager;
	@Resource
	private IDataFieldManager dataFieldManager ;
	@Resource
	private IDaoSupport daoSupport;
	
	
	@Override
	public void install() {
		try{
	        installModelField();
		}catch(RuntimeException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void unInstall() {
		try{
			deleteModelField();
		}catch(RuntimeException e){
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void installModelField(){
		  List<DataModel> modelList = dataModelManager.list();
		  for (DataModel dataModel : modelList) {
			DataField dataField = new DataField();
			dataField.setChina_name("语言");
			dataField.setEnglish_name("language_code");
			dataField.setShow_form(LanguageSelectPlugin.PLUGIN_ID);
			dataField.setIs_show(0);
			dataField.setIs_validate(1);
			dataField.setModel_id(dataModel.getModel_id());

			dataFieldManager.add(dataField);
		  }
	}
	
	public void deleteModelField(){
		StringBuffer sql = new StringBuffer();
		sql.append("delete from es_data_field where english_name='language_code'");
		daoSupport.execute(sql.toString(),new Object[]{});
	}
	
	
}
