package com.cheriscon.backstage.content.component.plugin;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.core.model.DataField;
import com.cheriscon.app.cms.core.plugin.AbstractFieldPlugin;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.IDaoSupport;

@Component
public class LanguageSelectPlugin extends AbstractFieldPlugin{
	
	public static final String PLUGIN_ID = "language_select";
	
	@Resource
	private ILanguageService languageService;

	@Resource
	private IDaoSupport daoSupport;
	
	public String onDisplay(DataField field, Object value) {
		StringBuffer html = new StringBuffer();
		/*html.append("<select name=\"");
		html.append(field.getEnglish_name());
		html.append("\"");
		html.append(this.wrappValidHtml(field)) ;
		html.append(">");
		
		List<Language> languageList = languageService.queryLanguage();
		if(languageList != null){
			for (Language language : languageList) {
				html.append("<option ");
				html.append(" value=\"");
				html.append(language.getCode());
				html.append("\"");
				if(language.getCode().equals(value)){
					html.append(" selected=\"true\"");
				}
				html.append(" >");	
				html.append(language.getName());
				html.append("</option>");
			}
		}
		
		html.append("</select>");*/
		
		return html.toString();
	}

	
	public Object onShow(DataField field, Object value) {
		/*if (value != null) {
			String languageCode = value.toString();
			Language language = languageService.getByCode(languageCode);
			return language.getName();
		} else*/
			return "";
	}
	
	public void onSave(Map article, DataField field) {
		HttpServletRequest request  = ThreadContextHolder.getHttpRequest();
		String languageCode  = request.getParameter(field.getEnglish_name());
		article.put(field.getEnglish_name(),languageCode);		
	}
	
	
	public String getId() {
		return PLUGIN_ID;
	}

	
	public String getName() {
		return "语言选择下接框";
	}

	
	public int getHaveSelectValue() {
		return 1;
	}


}
