package com.cheriscon.backstage.content.service;

import java.util.List;
import java.util.Map;

import com.cheriscon.common.model.Message;
import com.cheriscon.framework.database.Page;

public interface IMessageService {
	
	/**
	 * 增加
	 * @param message
	 */
	public boolean saveMessage(Message message);
	
	/**
	 * 修改
	 * @param message
	 */
	public boolean updateMessage(Message message);
	
	/**
	 * 批量修改多个状态
	 * @param ids
	 * @param status
	 * @return
	 */
	public boolean updateStatus(String ids,int status);

	/**
	 * 修改弹出显示状态
	 * @param id
	 * @param status
	 * @return
	 */
	public boolean editDialogStatus(int id,int isDialog);
	
	/**
	 * 删除
	 */
	public boolean delMessageById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Message getById(int id);
	
	/**
	 * 根据消息code查询消息信息对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Message getMessageByCode(String code) throws Exception;
	
	/**
	 * 翻页查询所有软件语言
	 * @param message
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageMessage(Message message, int pageNo, int pageSize);
	
	/**
	 * 查询消息统计信息
	 * @return
	 */
	public List<Map> queryMesageCount();
	
	
	/**
	 * 查询消息分类查询对应消息条数
	 * @return
	 * @throws Exception
	 */
	public int queryMessageCount(String userCode,String msgTypeCode, String languageCode,
			int userType, int isDialog,Integer timeRange,String createTime) throws Exception;
	
	

	
	/**
	 * 查询我的消息记录
	 * @param message 用户类型
	 * @param userCode 用户code
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	public Page queryMyMessageList(final Message message,final String userCode,
									int pageNo, int pageSize)throws Exception;
	
	public List<Message> queryUserMessage(int userType, String languageCode,String userCode)
												throws Exception;
	
	
	public List<Message> queryUserMessageNew(int userType, String languageCode,String userCode)
			throws Exception;
  
	//按照功能软件查询是否有对应的接口
	public List<Message> getMessageBySoftCodeAndVersion(String softwareCode) throws Exception;
	
	//按照功能软件删除消息
	public int delMessageBySoftCodeAndVersion(String softwareCode) throws Exception;
	
	
	/**
	 * 查询未读消息数
	 * @return
	 * @throws Exception
	 */
	public int queryMessageCountNotRead(String userCode,String msgTypeCode, String languageCode,
			int userType, int isDialog,Integer timeRange,String createTime) throws Exception;
	
	/**
	 * 查询未读消息数
	 * @return
	 * @throws Exception
	 */
	public int queryMessageCountNotReadNew(String userCode,String msgTypeCode, String languageCode,
			int userType, int isDialog,Integer timeRange,String createTime) throws Exception;
	
	/**
	 * 系统平台查询升级消息接口
	 * @return
	 * @throws Exception
	 */
	public List<Message> queryUpdateMessage(String startId) throws Exception;
	
	/**
	 * 系统平台查询系统消息接口
	 * @return
	 * @throws Exception
	 */
	public List<Message> querySystemMessage(String startId) throws Exception;
	
}
