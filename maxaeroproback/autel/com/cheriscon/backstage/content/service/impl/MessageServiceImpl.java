package com.cheriscon.backstage.content.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.content.service.IUserMessageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.Message;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.common.model.UserMessage;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.UploadUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.ResourceBundleModel;

@Service
public class MessageServiceImpl extends BaseSupport<Message> implements IMessageService {
	@Resource
	private IUserMessageService userMessageService;

	@Transactional
	public boolean saveMessage(Message message) {

		String content = message.getContent();
		if (content != null) {
			//替换静态服务器域名为本地标识串(fs:)
			content = content.replaceAll(CopSetting.IMG_SERVER_DOMAIN + CopContext.getContext().getContextPath() + "/attachment/",
					CopSetting.FILE_STORE_PREFIX + "/attachment/");
		}
		content = HtmlUtils.htmlUnescape(content);
		message.setContent(content);
		//语言配置
		String tableName = DBUtils.getTableName(Message.class);
		message.setCreatTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		message.setCode(DBUtils.generateCode(DBConstant.MESSAGE_CODE));
		this.daoSupport.insert(tableName, message);
		return true;
	}

	@Transactional
	public boolean updateMessage(Message message) {
		String tableName = DBUtils.getTableName(Message.class);
		Message oldMessage = getById(message.getId());
		oldMessage.setLanguageCode(message.getLanguageCode());
		oldMessage.setMsgTypeCode(message.getMsgTypeCode());
		oldMessage.setTitle(message.getTitle());
		oldMessage.setSort(message.getSort());
		oldMessage.setCreator(message.getCreator());

		String content = message.getContent();
		if (content != null) {
			//替换静态服务器域名为本地标识串(fs:)
			content = content.replaceAll(CopSetting.IMG_SERVER_DOMAIN + CopContext.getContext().getContextPath() + "/attachment/",
					CopSetting.FILE_STORE_PREFIX + "/attachment/");
		}
		content = HtmlUtils.htmlUnescape(content);
		oldMessage.setContent(content);
		oldMessage.setUserType(message.getUserType());

		this.daoSupport.update(tableName, oldMessage, "id=" + message.getId());
		return true;
	}

	@Transactional
	public boolean updateStatus(String ids, int status) {
		String tableName = DBUtils.getTableName(Message.class);
		StringBuffer update = new StringBuffer();
		update.append("update ").append(tableName);
		update.append(" set status=" + status + " ");
		update.append(" where id in (" + ids + ") ");
		this.daoSupport.execute(update.toString(), new Object[] {});
		return true;
	}

	@Transactional
	public boolean editDialogStatus(int id, int isDialog) {
		String tableName = DBUtils.getTableName(Message.class);
		StringBuffer update = new StringBuffer();
		update.append("update ").append(tableName);
		update.append(" set isDialog=" + isDialog);
		update.append(" where id=?");
		this.daoSupport.execute(update.toString(), id);
		return true;
	}

	@Transactional
	public boolean delMessageById(int id) {
		String tableName = DBUtils.getTableName(Message.class);
		StringBuffer sql = new StringBuffer("delete from " + tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);
		return true;
	}

	public Message getById(int id) {
		String tableName = DBUtils.getTableName(Message.class);
		String messageTypeTB = DBUtils.getTableName(MessageType.class);
		String languageTB = DBUtils.getTableName(Language.class);
		StringBuffer sql = new StringBuffer("select ");
		sql.append(" msg.*,mst.name as msgTypeName,lan.name as languageName ");
		sql.append(" from " + tableName + " msg ");
		sql.append(" inner join " + messageTypeTB + " mst on mst.code = msg.msgTypeCode ");
		sql.append(" inner join " + languageTB + " lan on lan.code = msg.languageCode ");
		sql.append(" where msg.id=? ");
		Message message = this.daoSupport.queryForObject(sql.toString(), Message.class, id);

		String content = message.getContent();
		if (content != null) {
			content = UploadUtil.replacePath(content.toString());
		}
		message.setContent(content);
		return message;
	}

	public Page pageMessage(Message message, int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(Message.class);
		String messageTypeTB = DBUtils.getTableName(MessageType.class);
		String languageTB = DBUtils.getTableName(Language.class);
		StringBuffer sql = new StringBuffer("select ");
		sql.append(" msg.*,mst.name as msgTypeName,lan.name as languageName ");
		sql.append(" from " + tableName + " msg ");
		sql.append(" inner join " + messageTypeTB + " mst on mst.code = msg.msgTypeCode ");
		sql.append(" inner join " + languageTB + " lan on lan.code = msg.languageCode ");
		sql.append(" where 1=1 ");

		if (message != null) {
			if (!StringUtils.isEmpty(message.getTitle())) { //标题模糊查询
				sql.append(" and msg.title like '%").append(message.getTitle()).append("%' ");
			}

			if (!StringUtils.isEmpty(message.getMsgTypeCode())) { //消息分类
				sql.append(" and msg.msgTypeCode = '").append(message.getMsgTypeCode()).append("' ");
			}

			if (!StringUtils.isEmpty(message.getLanguageCode())) { //语言
				sql.append(" and msg.languageCode = '").append(message.getLanguageCode()).append("' ");
			}

			if (message.getUserType() != null && message.getUserType() > -1) { //展示对象
				sql.append(" and msg.userType = ").append(message.getUserType());
			}

			if (message.getStatus() != null && message.getStatus() > -1) { //发布状态
				sql.append(" and msg.status = ").append(message.getStatus());
			}

			if (message.getIsDialog() != null && message.getIsDialog() > -1) { //是否弹出
				sql.append(" and msg.isDialog = ").append(message.getIsDialog());
			}

			if (message.getTimeRange() != null) {
				if (message.getTimeRange() == FrontConstant.LATELY_THREE_MONTH)//查询最近3个月消息
				{
					sql.append(" and msg.creatTime >= '").append(message.getCreatTime()).append("'");
				} else if (message.getTimeRange() == FrontConstant.BEFORE_THREE_MONTH)//查询3个月前消息
				{
					sql.append(" and msg.creatTime < '").append(message.getCreatTime()).append("'");
				}
			}

		}
		sql.append(" order by msg.sort asc,msg.id asc ");

		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, Message.class, new Object[] {});
		return page;
	}

	public List<Map> queryMesageCount() {
		String mesTable = DBUtils.getTableName(Message.class);
		String mestTable = DBUtils.getTableName(MessageType.class);
		StringBuffer sql = new StringBuffer();
		sql.append("select mst.name as name,count(mst.name) as count from ");
		sql.append(mesTable).append(" mes inner join ").append(mestTable);
		sql.append("  mst on mes.msgTypeCode = mst.code group by name ");
		List<Map> dataMap = this.daoSupport.queryForList(sql.toString(), new Object[] {});
		
		for (Map map : dataMap) {
			map.put("name", this.getLocalName(map.get("name").toString()));
		}
		
		return dataMap;
	}

	@Override
	public int queryMessageCount(String userCode,String msgTypeCode, String languageCode, int userType, int isDialog, Integer timeRange, String createTime)
			throws Exception {
		StringBuffer sql = new StringBuffer("select count(a.code)");
		sql.append(" from  DT_Message a inner join DT_MessageType b "); 
		sql.append(" on  a.msgTypeCode = b.code  and a.languageCode='"+languageCode+"' ");  
		sql.append(" and a.userType='"+userType+"'  "); 
		if(!StringUtil.isEmpty(msgTypeCode)){
			sql.append(" and a.msgTypeCode='").append(msgTypeCode).append("'");
		}
		if (timeRange != null) {
			if (timeRange == FrontConstant.LATELY_THREE_MONTH)//查询最近3个月消息
			{
				sql.append(" and a.creatTime >= '").append(createTime).append("'");
			} else if (timeRange == FrontConstant.BEFORE_THREE_MONTH)//查询3个月前消息
			{
				sql.append(" and a.creatTime < '").append(createTime).append("'");
			}
		}
		sql.append(" and a.status =1 "); 
//		查询软件更新类型时，才加入以下sql ，以下sql数量太大影响查询速度
		if("mst201304251043350375".equals(msgTypeCode)){
			sql.append(" left join "); 
			sql.append(" (Select distinct f.name,   d.softwareTypeCode,m.name as minName    "); 
			sql.append(" from dt_customerinfo a "); 
			sql.append(" inner join dt_customerproinfo b on a.code=b.customerCode"); 
			sql.append(" inner join DT_ProductSoftwareValidStatus c on b.proCode=c.proCode "); 
			sql.append(" inner join DT_MinSaleUnitSoftwareDetail d on c.minSaleUnitCode=d.minSaleUnitCode"); 
			sql.append(" inner join dt_productinfo e on e.code=b.proCode"); 
			sql.append(" inner join DT_ProductForSealer f on f.code=e.proTypeCode "); 
			sql.append(" left outer join dt_product_software_config g on   g.`code`=d.softwareTypeCode  "); 
			sql.append(" left outer join dt_product_software_config_memo m on m.softConfigCode=g.parentCode and m.languageCode='"+languageCode+"'  "); 
			sql.append(" where 1=1 " );
			sql.append(" and a.code='"+userCode+"'");
			
			sql.append(" ) soft on a.softwareTypeCode=soft.softwareTypeCode "); 
			sql.append(" where "); 
			sql.append(" a.softwareTypeCode is null or soft.softwareTypeCode is not null "); 
		}
		
		return this.daoSupport.queryForInt(sql.toString());
	}

	
	@Override
	public int queryMessageCountNotRead(String userCode,String msgTypeCode, String languageCode, int userType, int isDialog, Integer timeRange, String createTime)
			throws Exception {
		StringBuffer sql = new StringBuffer("select count(a.code)");
		sql.append(" from  DT_Message a inner join DT_MessageType b "); 
		sql.append(" on  a.msgTypeCode = b.code  and a.languageCode='"+languageCode+"' ");  
		sql.append(" and a.userType='"+userType+"'  "); 
		if(!StringUtil.isEmpty(msgTypeCode)){
			sql.append(" and a.msgTypeCode='").append(msgTypeCode).append("'");
		}
		if (timeRange != null) {
			if (timeRange == FrontConstant.LATELY_THREE_MONTH)//查询最近3个月消息
			{
				sql.append(" and a.creatTime >= '").append(createTime).append("'");
			} else if (timeRange == FrontConstant.BEFORE_THREE_MONTH)//查询3个月前消息
			{
				sql.append(" and a.creatTime < '").append(createTime).append("'");
			}
		}
		sql.append(" and a.status =1 "); 
		sql.append(" and a.code not in(select messageCode from dt_user_message where userCode='"+userCode+"')");
		sql.append(" left join ");
		
		sql.append(" (Select distinct f.name,   d.softwareTypeCode,m.name as minName    "); 
		sql.append(" from dt_customerinfo a "); 
		sql.append(" inner join dt_customerproinfo b on a.code=b.customerCode"); 
		sql.append(" inner join DT_ProductSoftwareValidStatus c on b.proCode=c.proCode "); 
		sql.append(" inner join DT_MinSaleUnitSoftwareDetail d on c.minSaleUnitCode=d.minSaleUnitCode"); 
		sql.append(" inner join dt_productinfo e on e.code=b.proCode"); 
		sql.append(" inner join DT_ProductForSealer f on f.code=e.proTypeCode "); 
		sql.append(" left outer join dt_product_software_config g on   g.`code`=d.softwareTypeCode  "); 
		sql.append(" left outer join dt_product_software_config_memo m on m.softConfigCode=g.parentCode and m.languageCode='"+languageCode+"'  "); 
		sql.append(" where 1=1 " );
		sql.append(" and a.code='"+userCode+"'");
		
		sql.append(" ) soft on a.softwareTypeCode=soft.softwareTypeCode "); 
		sql.append(" where "); 
		sql.append(" a.softwareTypeCode is null or soft.softwareTypeCode is not null "); 
		
		return this.daoSupport.queryForInt(sql.toString());
	}
	
	@Override
	public int queryMessageCountNotReadNew(String userCode,String msgTypeCode, String languageCode, int userType, int isDialog, Integer timeRange, String createTime)
			throws Exception {
		StringBuffer sql = new StringBuffer("select count(a.code)");
		sql.append(" from DT_Message a where  a.status =1 and a.isDialog=1 and a.softwareTypeCode is null ");
//		if (userType == 2) { 
			sql.append(" and a.userType=" + userType);
			sql.append(" and a.languageCode= '" + languageCode + "'");
//		}
		sql.append(" and not exists(select messageCode from DT_User_Message m where m.userType="+userType+" and m.userCode='"+userCode+"' and a.code=m.messageCode)");
		
		
		return this.daoSupport.queryForInt(sql.toString());
	}
	
	@Override
	public Page queryMyMessageList(final Message message, final String userCode, int pageNo, int pageSize) throws Exception {
//		对非软性更新类型的消息减少 查询时关联的表
		final boolean isSoftUpdate = "mst201304251043350375".equals(message.getMsgTypeCode());
		StringBuffer sql = new StringBuffer("select ");
		if(isSoftUpdate){
			sql.append("a.code,soft.name as productTypeName,soft.minName as minProductName,");
		}else{
			sql.append("a.code,");
		}
		sql.append(" a.title as title,a.creatTime,b.name as  msgTypeName "); 
		sql.append(" from  DT_Message a inner join DT_MessageType b "); 
		sql.append(" on  a.msgTypeCode = b.code  and a.languageCode='"+message.getLanguageCode()+"' ");  
		sql.append(" and a.userType='"+message.getUserType()+"'  "); 
		if(!StringUtil.isEmpty(message.getMsgTypeCode())){
			sql.append(" and a.msgTypeCode='").append(message.getMsgTypeCode()).append("'");
		}
		if (message.getTimeRange() != null) {
			if (message.getTimeRange() == FrontConstant.LATELY_THREE_MONTH)//查询最近3个月消息
			{
				sql.append(" and a.creatTime >= '").append(message.getCreatTime()).append("'");
			} else if (message.getTimeRange() == FrontConstant.BEFORE_THREE_MONTH)//查询3个月前消息
			{
				sql.append(" and a.creatTime < '").append(message.getCreatTime()).append("'");
			}
		}
		sql.append(" and a.status =1 "); 
		if(isSoftUpdate){
			sql.append(" left join "); 
			
			sql.append(" (Select distinct f.name,   d.softwareTypeCode,m.name as minName    "); 
			sql.append(" from dt_customerinfo a "); 
			sql.append(" inner join dt_customerproinfo b on a.code=b.customerCode"); 
			sql.append(" inner join DT_ProductSoftwareValidStatus c on b.proCode=c.proCode "); 
			sql.append(" inner join DT_MinSaleUnitSoftwareDetail d on c.minSaleUnitCode=d.minSaleUnitCode"); 
			sql.append(" inner join dt_productinfo e on e.code=b.proCode"); 
			sql.append(" inner join DT_ProductForSealer f on f.code=e.proTypeCode "); 
			sql.append(" left outer join dt_product_software_config g on   g.`code`=d.softwareTypeCode  "); 
			sql.append(" left outer join dt_product_software_config_memo m on m.softConfigCode=g.parentCode and m.languageCode='"+message.getLanguageCode()+"'  "); 
			sql.append(" where 1=1 " );
			sql.append(" and a.code='"+userCode+"'");
			sql.append(" ) soft on a.softwareTypeCode=soft.softwareTypeCode "); 
			
			sql.append(" where "); 
			sql.append(" a.softwareTypeCode is null or soft.softwareTypeCode is not null "); 
		}
		
		sql.append(" order by a.creatTime desc");

		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, new RowMapper() {
			@Override
			public Message mapRow(ResultSet rs, int arg1) throws SQLException {
				Message newMessage = new Message();
				newMessage.setCode(rs.getString("code"));
				/*newMessage.setContent(rs.getString("content"));*/
				newMessage.setCreatTime(rs.getString("creatTime"));
//				newMessage.setMsgTypeName(FreeMarkerPaser.getBundleValue(rs.getString("msgTypeName")));
				newMessage.setMsgTypeName(rs.getString("msgTypeName"));
				String title="";
				if(isSoftUpdate){
					if(!StringUtil.isEmpty(rs.getString("productTypeName"))){
						title=rs.getString("productTypeName");
					}if(!StringUtil.isEmpty(rs.getString("minProductName"))){
						title=title+" "+rs.getString("minProductName");
					}
				}

				if(!StringUtil.isEmpty(rs.getString("title"))){
					title=title+" "+rs.getString("title");
				}
				
				newMessage.setTitle(title.trim());
				try {

					List<UserMessage> userMessageList = userMessageService.queryUserMessageList(message.getUserType(), userCode, rs.getString("code"));

					if (userMessageList.size() > 0) {
						newMessage.setMessageIsRead(FrontConstant.USER_MESSAGE_IS_YES_READ);//标记该用户消息已读
					} else {
						newMessage.setMessageIsRead(FrontConstant.USER_MESSAGE_IS_NO_READ);//标记该用户消息未读
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				return newMessage;
			}
		});

	}

	@Override
	public Message getMessageByCode(String code) throws Exception {
		String sql = "select * from DT_Message where code='" + code + "'";
		Message message = this.daoSupport.queryForObject(sql, Message.class);
		String content = message.getContent();
		if (content != null) {
			content = UploadUtil.replacePath(content.toString());
		}
		message.setContent(content);
		return message;
	}

	/**
	 * 国际化键值转成实际名称 
	 * @param menuList
	 */

	public String getLocalName(String key) {
		String localName = null;
		//localName = FreeMarkerPaser.getBundleValue(key);

		try {
			Locale locale = Locale.SIMPLIFIED_CHINESE;
			ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("backstage",locale);
			ResourceBundleModel rsbm = new ResourceBundleModel(RESOURCE_BUNDLE, new BeansWrapper());
			localName = rsbm.get(key).toString();			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		if (localName == null) localName = key;
		return localName;
	}

	
	public List<Message> queryUserMessageNew(int userType, String languageCode,String userCode)
			throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append(" select a.code,a.title as title from DT_Message a where  a.status =1 and a.isDialog=1 and a.softwareTypeCode is null ");
		sql.append(" and a.userType= " + userType);
		sql.append(" and not exists(select messageCode from DT_User_Message m where m.userType="+userType+" and m.userCode='"+userCode+"' and a.code=m.messageCode)");
		List<Message> temp=this.daoSupport.queryForList(sql.toString(), Message.class);
		return temp;
	}
	
	@Override
	public List<Message> queryUserMessage(int userType, String languageCode,
			String userCode) throws Exception 
	{
		StringBuffer sql = new StringBuffer("select ");
		sql.append("a.code,soft.name as productTypeName,soft.minName as minProductName,");
		sql.append(" a.title as title "); 
		sql.append(" from  DT_Message a inner join DT_MessageType b "); 
		sql.append(" on  a.msgTypeCode = b.code  and a.languageCode='"+languageCode+"' ");  
		sql.append(" and a.userType="+userType+"  "); 
		sql.append(" and a.status =1 and a.isDialog=1"); 
		sql.append(" and not exists(select messageCode from DT_User_Message m where m.userType="+userType+" and m.userCode='"+userCode+"' and a.code=m.messageCode)");
		sql.append(" left join "); 
		
		sql.append(" (Select distinct f.name,   d.softwareTypeCode,m.name as minName    "); 
		sql.append(" from dt_customerinfo a "); 
		sql.append(" inner join dt_customerproinfo b on a.code=b.customerCode"); 
		sql.append(" inner join DT_ProductSoftwareValidStatus c on b.proCode=c.proCode "); 
		sql.append(" inner join DT_MinSaleUnitSoftwareDetail d on c.minSaleUnitCode=d.minSaleUnitCode"); 
		sql.append(" inner join dt_productinfo e on e.code=b.proCode"); 
		sql.append(" inner join DT_ProductForSealer f on f.code=e.proTypeCode "); 
		sql.append(" left outer join dt_product_software_config g on   g.`code`=d.softwareTypeCode  "); 
		sql.append(" left outer join dt_product_software_config_memo m on m.softConfigCode=g.parentCode and m.languageCode='"+languageCode+"'  "); 
		sql.append(" where 1=1 " );
		sql.append(" and a.code='"+userCode+"'");
		
		sql.append(" ) soft on a.softwareTypeCode=soft.softwareTypeCode "); 
		sql.append(" where "); 
		sql.append(" a.softwareTypeCode is null or soft.softwareTypeCode is not null "); 
		
		sql.append(" order by a.creatTime desc");
		sql.append(" limit 0,4");
		/*StringBuffer sql = new StringBuffer();
		sql.append("select top 4 a.code,");
		sql.append(" soft.name as productTypeName, soft.minName as minProductName,a.title as title");
		sql.append(" from DT_Message a left join ");
		sql.append(" (Select distinct f.name,  d.softwareTypeCode,g.updateName as minName from dt_customerinfo a ,dt_customerproinfo b,");
		sql.append(" DT_ProductSoftwareValidStatus c ,DT_MinSaleUnitSoftwareDetail d ,dt_productinfo e,DT_ProductForSealer f,DT_MinSaleUnitMemo g");
		sql.append(" where a.code=b.customerCode");
		sql.append(" and b.proCode=c.proCode");
		sql.append(" and c.minSaleUnitCode=d.minSaleUnitCode");
		sql.append(" and b.proCode=e.code");
		sql.append(" and e.proTypeCode=f.code");
		sql.append(" and d.minSaleUnitCode=g.minSaleUnitCode");
		sql.append(" and g.languageCode='"+languageCode+"'");
		sql.append(" and a.code='"+userCode+"') soft on a.softwareTypeCode=soft.softwareTypeCode");
		sql.append(" where a.userType="+userType+"");
		sql.append(" and  a.languageCode='"+languageCode+"' ");
		sql.append(" and not exists(select messageCode from DT_User_Message m where m.userType="+userType+" and m.userCode='"+userCode+"' and a.code=m.messageCode)");
		sql.append(" and a.status=1 and a.isDialog=1");
		sql.append(" and soft.softwareTypeCode is not null or a.softwareTypeCode is  null");
		sql.append(" order by a.creatTime desc");*/
		
		List<Message> temp=this.daoSupport.queryForList(sql.toString(), Message.class);
		List<Message> list=new ArrayList<Message>();
		for(int i=0;i<temp.size();i++){
			Message message=temp.get(i);
			String title="";
			if(!StringUtil.isEmpty(message.getProductTypeName())){
				title=message.getProductTypeName();
			}
			if(!StringUtil.isEmpty(message.getMinProductName())){
				title=title+" "+message.getMinProductName();
			}
			if(!StringUtil.isEmpty(message.getTitle())){
				title=title+" "+message.getTitle();
			}
			message.setTitle(title.trim());
			list.add(message);
		}
		
		/*sql.append("select top 4 * from DT_Message m where m.languageCode=");
		sql.append("'");
		sql.append(languageCode);
		sql.append("'");
		sql.append("and m.userType=");
		sql.append("'");
		sql.append(userType);
		sql.append("'");
		sql.append("and not exists(select messageCode from DT_User_Message where userType=");
		sql.append("'");
		sql.append(userType);
		sql.append("'");
		sql.append("and userCode=");
		sql.append("'");
		sql.append(userCode);
		sql.append("'");
		sql.append(" and m.code=messageCode) and status=1 and isDialog=1");*/
		return list;
	}

	public List<Message> getMessageBySoftCodeAndVersion(String softwareCode) throws Exception{
		
		String sql = "select * from DT_Message where softwareTypeCode=?";
		List<Message> message = this.daoSupport.queryForList(sql, Message.class,softwareCode);
		
		return message;
	}
	public int delMessageBySoftCodeAndVersion(String softwareCode) throws Exception{
		try{
			String sql = "delete from DT_Message where softwareTypeCode=?";
			this.daoSupport.execute(sql, softwareCode);
			return 0;
		}catch(Exception e){
			return 1;
		}
	}
	
	public List<Message> queryUpdateMessage(String startId) throws Exception{
		
		StringBuffer sql=new StringBuffer();
		sql.append("select a.title,a.creator,a.creatTime,a.content,a.softwareTypeCode,a.versionCode,b.languageCode from dt_message a,dt_language b where a.languageCode=b.`code` "); 
		sql.append(" and userType=1 and softwareTypeCode is not null");
		sql.append(" and id>?");
		
		return this.daoSupport.queryForList(sql.toString(), Message.class, startId);
	}
	public List<Message> querySystemMessage(String startId) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select a.title,a.creator,a.creatTime,a.content,a.softwareTypeCode,a.versionCode,b.languageCode from dt_message a,dt_language b where a.languageCode=b.`code` "); 
		sql.append(" and userType=1 and softwareTypeCode is null");
		sql.append(" and id>?");
		return this.daoSupport.queryForList(sql.toString(), Message.class, startId);
	}
	
}
