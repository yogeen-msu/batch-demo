package com.cheriscon.backstage.content.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.content.service.IMessageTypeService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MessageType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.constant.FrontConstant;

@Service
public class MessageTypeServiceImpl extends BaseSupport<MessageType> implements IMessageTypeService {

	@Transactional
	public boolean saveMessageType(MessageType MessageType) {
		String tableName = DBUtils.getTableName(MessageType.class);
		MessageType.setCode(DBUtils.generateCode(DBConstant.MESSAGETYPE_CODE));
		this.daoSupport.insert(tableName, MessageType);
		return true;
	}

	@Transactional
	public boolean updateMessageType(MessageType MessageType) {
		String tableName = DBUtils.getTableName(MessageType.class);
		this.daoSupport.update(tableName, MessageType, "id=" + MessageType.getId());
		return true;
	}

	@Transactional
	public boolean delMessageTypeById(int id) {
		String tableName = DBUtils.getTableName(MessageType.class);
		StringBuffer sql = new StringBuffer("delete from " + tableName);
		sql.append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);
		return true;
	}

	public MessageType getById(int id) {
		String tableName = DBUtils.getTableName(MessageType.class);
		String sql = "select * from " + tableName + " where id=?";
		MessageType messageType = this.daoSupport.queryForObject(sql, MessageType.class, id);
//		paserName(messageType);
		return messageType;
	}

	public List<MessageType> queryMessageType() {
		String tableName = DBUtils.getTableName(MessageType.class);
		String sql = "select mst.* from " + tableName + " mst order by id asc";
		List<MessageType> messageTypeList = this.daoSupport.queryForList(sql, MessageType.class, new Object[] {});
//		paserLocalName(messageTypeList);
		return messageTypeList;
	}

	@Override
	public List<MessageType> queryMessageType(int userType, Integer timeRange, String languageCode, String creatTime) throws Exception {
		StringBuffer sql = new StringBuffer("select distinct b.code,b.name");
		sql.append(" from DT_Message a,DT_MessageType b where a.msgTypeCode = b.code ");

		if (!StringUtils.isEmpty(languageCode))//语言
		{
			sql.append(" and a.languageCode = '").append(languageCode).append("' ");
		}

		if (!StringUtils.isEmpty(String.valueOf(userType))) {
			sql.append(" and a.userType = ").append(userType);
		}

		if (timeRange != null) {
			if (timeRange == FrontConstant.LATELY_THREE_MONTH)//查询最近3个月消息
			{
				sql.append(" and a.creatTime >= '").append(creatTime).append("'");
			} else if (timeRange == FrontConstant.BEFORE_THREE_MONTH)//查询3个月前消息
			{
				sql.append(" and a.creatTime < '").append(creatTime).append("'");
			}
		}
		return this.daoSupport.queryForList(sql.toString(), new RowMapper() {

			@Override
			public MessageType mapRow(ResultSet rs, int arg1) throws SQLException {
				MessageType messageType = new MessageType();
				messageType.setCode(rs.getString("code"));
				messageType.setName(rs.getString("name"));
//				paserName(messageType);
				return messageType;
			}

		});

	}

	/**
	 * 国际化键值转成实际名称 
	 * @param menuList
	 */
	public void paserLocalName(List<MessageType> messageTypeList) {
		if (messageTypeList == null || messageTypeList.size() == 0)
			return;
		for (MessageType menu : messageTypeList) {
			paserName(menu);
		}
	}

	public void paserName(MessageType messageType) {
		if (messageType == null || StringUtils.isEmpty(messageType.getName()))
			return;

		String key = messageType.getName();
		String name = FreeMarkerPaser.getBundleValue(key);
		if (StringUtils.isNotEmpty(name)) {
			messageType.setName(name);
		}
	}

}
