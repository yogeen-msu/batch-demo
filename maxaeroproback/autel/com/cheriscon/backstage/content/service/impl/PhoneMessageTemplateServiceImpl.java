package com.cheriscon.backstage.content.service.impl;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IPhoneMessageTemplateService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.PhoneMessageTemplate;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;

@Service
public class PhoneMessageTemplateServiceImpl extends BaseSupport<PhoneMessageTemplate> implements IPhoneMessageTemplateService {
	
	@Transactional
	public boolean savePhoneMessageTemplate(PhoneMessageTemplate phoneMessageTemplate) {
		String tableName = DBUtils.getTableName(PhoneMessageTemplate.class);
		phoneMessageTemplate.setStatus(CTConsatnt.CT_STATUS_NOT_USE);
		this.daoSupport.insert(tableName, phoneMessageTemplate);
		return true;
	}

	@Transactional
	public boolean updatePhoneMessageTemplate(PhoneMessageTemplate phoneMessageTemplate) {
		String tableName = DBUtils.getTableName(PhoneMessageTemplate.class);
		PhoneMessageTemplate template = getById(phoneMessageTemplate.getId());
		template.setTitle(phoneMessageTemplate.getTitle());
		template.setLanguageCode(phoneMessageTemplate.getLanguageCode());
		template.setType(phoneMessageTemplate.getType());
		template.setContent(phoneMessageTemplate.getContent());
		this.daoSupport.update(tableName,phoneMessageTemplate,"id="+phoneMessageTemplate.getId());
		return true;
	}
	
	@Transactional
	public boolean updateStatus(int id,int status){
		String tableName = DBUtils.getTableName(PhoneMessageTemplate.class);
		PhoneMessageTemplate template = getById(id);
		
		//如果是启用新的模版，则停止当前使用的模版
		if(status == CTConsatnt.CT_STATUS_USE){
			//查出当前类型，语言环境下的正在使用模版
			PhoneMessageTemplate useTemplate =getUseTemplateByTypeAndLanguage(template.getType(),template.getLanguageCode());
			if(useTemplate != null){
				useTemplate.setStatus(CTConsatnt.CT_STATUS_NOT_USE);
				this.daoSupport.update(tableName,useTemplate,"id="+useTemplate.getId());	//停止当前模版
			}
		}
		
		if(status == CTConsatnt.CT_STATUS_USE){
			template.setUseTime(DateUtil.toString(new Date(),"yyyy-MM-dd"));
		}
		template.setStatus(status);
		this.daoSupport.update(tableName,template,"id="+id);	//停止当前模版
		
		
		return true;
	}

	@Transactional
	public int delPhoneMessageTemplateById(int id) {
		String tableName = DBUtils.getTableName(PhoneMessageTemplate.class);
		PhoneMessageTemplate template = getById(id);
		if(template.getStatus() == CTConsatnt.CT_STATUS_USE){	//下在使用不能删除
			return CTConsatnt.CT_IS_USE;
		}
		
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		
		this.daoSupport.execute(sql.toString(),id);
		return CTConsatnt.CT_SUCCESS;
	}
	
	public PhoneMessageTemplate getById(int id){
		String tableName = DBUtils.getTableName(PhoneMessageTemplate.class);
		String sql = "select * from " + tableName + " where id=?";
		return this.daoSupport.queryForObject(sql, PhoneMessageTemplate.class, id);
	}
	
	public PhoneMessageTemplate getUseTemplateByTypeAndLanguage(int type,String languageCode){
		PhoneMessageTemplate template = null;
		try {
			String tableName = DBUtils.getTableName(PhoneMessageTemplate.class);
			StringBuffer sql = new StringBuffer();
			sql.append("select tb.* from "  + tableName +" tb where 1=1 ");
			sql.append(" and tb.languageCode='"+languageCode+"' ");
			sql.append(" and tb.type="+type+" ");
			sql.append(" and tb.status="+CTConsatnt.CT_STATUS_USE+" ");			//正在使用
			template = this.daoSupport.queryForObject(sql.toString(), PhoneMessageTemplate.class,new Object[]{});
		} catch (Exception e) {
		}
		return template;
	}
	
	public Page pagePhoneMessageTemplate(PhoneMessageTemplate phoneMessageTemplate ,int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(PhoneMessageTemplate.class);
		String languageTB = DBUtils.getTableName(Language.class);
		StringBuffer sql = new StringBuffer();
		sql.append("select tb.*,lan.name as languageName  from "  + tableName +" tb ");
		sql.append(" inner join " + languageTB + " lan  on lan.code=tb.languageCode ");
		sql.append(" where 1=1 ");
		if(phoneMessageTemplate != null ){
			if(!StringUtils.isEmpty(phoneMessageTemplate.getLanguageCode())){
				sql.append(" and tb.languageCode='"+phoneMessageTemplate.getLanguageCode()+"' ");
			}
			
			if(!StringUtils.isEmpty(phoneMessageTemplate.getTitle())){
				sql.append(" and tb.title='"+phoneMessageTemplate.getTitle()+"' ");
			}
			
			if(phoneMessageTemplate.getType() != null && phoneMessageTemplate.getType() > 0){
				sql.append(" and tb.type="+phoneMessageTemplate.getType()+" ");
			}
		}
		
		sql.append(" order by tb.status desc ,tb.id asc ");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,PhoneMessageTemplate.class,new Object[]{});
		return page;
	}

}
