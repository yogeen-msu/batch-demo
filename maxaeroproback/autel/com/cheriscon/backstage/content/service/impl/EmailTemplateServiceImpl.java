package com.cheriscon.backstage.content.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;

@Service
public class EmailTemplateServiceImpl extends BaseSupport<EmailTemplate> implements IEmailTemplateService {
	
	@Transactional
	public boolean saveEmailTemplate(EmailTemplate emailTemplate) {
		String tableName = DBUtils.getTableName(EmailTemplate.class);
		emailTemplate.setStatus(CTConsatnt.CT_STATUS_NOT_USE);
		emailTemplate.setCode(DBConstant.EMAILTEMPLATE_CODE);
		this.daoSupport.insert(tableName, emailTemplate);
		return true;
	}

	@Transactional
	public boolean updateEmailTemplate(EmailTemplate emailTemplate) {
		String tableName = DBUtils.getTableName(EmailTemplate.class);
		EmailTemplate template = getById(emailTemplate.getId());
		template.setTitle(emailTemplate.getTitle());
		template.setLanguageCode(emailTemplate.getLanguageCode());
		template.setType(emailTemplate.getType());
		template.setContent(emailTemplate.getContent());
		this.daoSupport.update(tableName,emailTemplate,"id="+emailTemplate.getId());
		
		//删除现有的模板
//		String filePath =  CopSetting.cop_PATH + CopSetting.THEMES_STORAGE_PATH+"/"+template.getCode()+".html";
//		FileUtil.delete(filePath);
		
		return true;
	}
	
	@Transactional
	public boolean updateStatus(int id,int status){
		String tableName = DBUtils.getTableName(EmailTemplate.class);
		EmailTemplate template = getById(id);
		
		//如果是启用新的模版，则停止当前使用的模版
		if(status == CTConsatnt.CT_STATUS_USE){
			//查出当前类型，语言环境下的正在使用模版
			EmailTemplate useTemplate =getUseTemplateByTypeAndLanguage(template.getType(),template.getLanguageCode());
			if(useTemplate != null){
				useTemplate.setStatus(CTConsatnt.CT_STATUS_NOT_USE);
				this.daoSupport.update(tableName,useTemplate,"id="+useTemplate.getId());	//停止当前模版
			}
		}
		
		if(status == CTConsatnt.CT_STATUS_USE){
			template.setUseTime(DateUtil.toString(new Date(),"yyyy-MM-dd"));
		}
		template.setStatus(status);
		this.daoSupport.update(tableName,template,"id="+id);	//停止当前模版
		
		
		return true;
	}

	@Transactional
	public int delEmailTemplateById(int id) {
		String tableName = DBUtils.getTableName(EmailTemplate.class);
		EmailTemplate template = getById(id);
		if(template.getStatus() == CTConsatnt.CT_STATUS_USE){	//下在使用不能删除
			return CTConsatnt.CT_IS_USE;
		}
		
		StringBuffer sql = new StringBuffer("delete from "+ tableName);
		sql.append(" where id=?");
		
		//删除现有的模板
//		String filePath =  CopSetting.cop_PATH + CopSetting.THEMES_STORAGE_PATH+"/"+template.getCode()+".html";
//		FileUtil.delete(filePath);
		
		
		this.daoSupport.execute(sql.toString(),id);
		return CTConsatnt.CT_SUCCESS;
	}
	
	public EmailTemplate getById(int id){
		String tableName = DBUtils.getTableName(EmailTemplate.class);
		String sql = "select * from " + tableName + " where id=?";
		return this.daoSupport.queryForObject(sql, EmailTemplate.class, id);
	}
	
	public EmailTemplate getUseTemplateByTypeAndLanguage(int type,String languageCode){
		EmailTemplate template = null;
		try {
			String tableName = DBUtils.getTableName(EmailTemplate.class);
			StringBuffer sql = new StringBuffer();
			sql.append("select tb.* from "  + tableName +" tb where 1=1 ");
			sql.append(" and tb.languageCode='"+languageCode+"' ");
			sql.append(" and tb.type="+type+" ");
			sql.append(" and tb.status="+CTConsatnt.CT_STATUS_USE+" ");			//正在使用
			template = this.daoSupport.queryForObject(sql.toString(), EmailTemplate.class,new Object[]{});
			if(template != null){
				String filePath =  CopSetting.cop_PATH + CopSetting.THEMES_STORAGE_PATH+"/"+template.getCode()+".html";
	//			if(!FileUtil.exist(filePath)){
					FileOutputStream outputStream = new FileOutputStream(new File(filePath));
					outputStream.write(template.getContent().getBytes("utf-8"));
					outputStream.close();
	//			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return template;
	}
	
	public Page pageEmailTemplate(EmailTemplate emailTemplate ,int pageNo, int pageSize) {
		String tableName = DBUtils.getTableName(EmailTemplate.class);
		String languageTB = DBUtils.getTableName(Language.class);
		StringBuffer sql = new StringBuffer();
		sql.append("select tb.*,lan.name as languageName  from "  + tableName +" tb ");
		sql.append(" inner join " + languageTB + " lan  on lan.code=tb.languageCode ");
		sql.append(" where 1=1 ");
		if(emailTemplate != null ){
			if(!StringUtils.isEmpty(emailTemplate.getLanguageCode())){
				sql.append(" and tb.languageCode='"+emailTemplate.getLanguageCode()+"' ");
			}
			
			if(!StringUtils.isEmpty(emailTemplate.getTitle())){
				sql.append(" and tb.title='"+emailTemplate.getTitle()+"' ");
			}
			
			if(emailTemplate.getType() != null && emailTemplate.getType() > 0){
				sql.append(" and tb.type="+emailTemplate.getType()+" ");
			}
		}
		
		sql.append(" order by tb.type ,tb.status asc ");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize,EmailTemplate.class,new Object[]{});
		return page;
	}

}
