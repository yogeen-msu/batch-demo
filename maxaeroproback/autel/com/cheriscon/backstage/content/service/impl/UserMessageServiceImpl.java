package com.cheriscon.backstage.content.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.content.service.IUserMessageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.UserMessage;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.constant.FrontConstant;

/**
 * 用户消息业务逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-3-5
 */
@Service
public class UserMessageServiceImpl extends BaseSupport<UserMessage> 
		implements IUserMessageService
{

	@Override
	public void saveMessage(UserMessage userMessage) throws Exception
	{
		userMessage.setCode(DBUtils.generateCode(DBConstant.USER_MESSAGE_CODE));
		this.daoSupport.insert("DT_User_Message", userMessage);
	}

	@Override
	public int queryUserMessageIsReadCount(int userType,String languageCode, String userCode,int isDialog)
			throws Exception 
	{
		StringBuffer sql = new StringBuffer();
		
		if(userType == FrontConstant.CUSTOMER_USER_TYPE)
		{
			sql.append("select count(*) from DT_CustomerInfo A,DT_Message B,DT_User_Message D ")
			   .append(" where A.code = D.userCode and B.code = D.messageCode and B.status=1  ")
			   .append(" and B.userType=1 and D.messageIsRead = 1 and B.languageCode=? and A.code=? ");
		}
		else if(userType == FrontConstant.DISTRIBUTOR_USER_TYPE)
		{
			sql.append("select count(*) from DT_SealerInfo A,DT_Message B,DT_User_Message D ")
			   .append(" where A.code = D.userCode and B.code = D.messageCode and B.status = 1  ")
			   .append(" and B.userType = 2 and D.messageIsRead = 1 and B.languageCode = ? and A.code=? ");
		}
		
		//为-1时查询所有消息
		if( isDialog > -1)
		{		
			sql.append(" and B.isDialog = ").append(isDialog);
		}
		
		return this.daoSupport.queryForInt(sql.toString(),languageCode,userCode);
	}

	@Override
	public List<UserMessage> queryUserMessageList(int userType,
			String userCode, String messageCode) throws Exception 
	{
		StringBuffer querySql = new StringBuffer();
		
		querySql.append("select * from DT_User_Message ");
		querySql.append(" where userType = ? ").append(" and userCode =? ")
				.append(" and messageCode =? ").append(" and messageIsRead = 1 ");
		return this.daoSupport.queryForList(querySql.toString(), UserMessage.class, userType,userCode,messageCode);
	}

	@Override
	public int queryUserMessageCount(int userType, String languageCode,String userCode)
			throws Exception 
	{
		
		StringBuffer sql = new StringBuffer();
		sql.append("select COUNT(*) from DT_Message m where m.languageCode=");
		sql.append("'");
		sql.append(languageCode);
		sql.append("'");
		sql.append("and m.userType=");
		sql.append("'");
		sql.append(userType);
		sql.append("'");
		sql.append("and not exists(select messageCode from DT_User_Message where userType=");
		sql.append("'");
		sql.append(userType);
		sql.append("'");
		sql.append("and userCode=");
		sql.append("'");
		sql.append(userCode);
		sql.append("'");
		sql.append(" and m.code=messageCode) and status=1");
		return this.daoSupport.queryForInt(sql.toString());
	} 

}
