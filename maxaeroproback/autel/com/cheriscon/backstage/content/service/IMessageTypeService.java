package com.cheriscon.backstage.content.service;

import java.util.List;

import com.cheriscon.common.model.MessageType;

public interface IMessageTypeService {
	
	/**
	 * 增加
	 * @param messageType
	 */
	public boolean saveMessageType(MessageType messageType);
	
	/**
	 * 修改
	 * @param messageType
	 */
	public boolean updateMessageType(MessageType messageType);
	
	/**
	 * 删除
	 */
	public boolean delMessageTypeById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public MessageType getById(int id);
	
	/**
	 * 查询出所有安全问题
	 * @return
	 */
	public List<MessageType> queryMessageType();
	
	/**
	 * 
	 * @param userType
	 * @param timeRange
	 * @param languageCode
	 * @param creatTime
	 * @return
	 * @throws Exception
	 */
	public List<MessageType> queryMessageType(int userType,Integer timeRange,String languageCode,String creatTime)
			throws Exception;
	


}
