package com.cheriscon.backstage.content.service;

import com.cheriscon.common.model.PhoneMessageTemplate;
import com.cheriscon.framework.database.Page;

public interface IPhoneMessageTemplateService {
	
	/**
	 * 增加
	 * @param phoneMessageTemplate
	 */
	public boolean savePhoneMessageTemplate(PhoneMessageTemplate phoneMessageTemplate);
	
	/**
	 * 修改
	 * @param phoneMessageTemplate
	 */
	public boolean updatePhoneMessageTemplate(PhoneMessageTemplate phoneMessageTemplate);
	
	/**
	 * 修改使用状态
	 * @param id
	 * @param status	当前状态
	 * @return
	 */
	public boolean updateStatus(int id,int status);
	
	/**
	 * 删除
	 * @param id
	 * @return  操作结果   0：成功   1：失败   2：正在使用
	 */
	public int delPhoneMessageTemplateById(int id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public PhoneMessageTemplate getById(int id);
	
	/**
	 * 根据短信类型和语言种类获取
	 * @param languageCode
	 * @param type
	 * @return
	 */
	public PhoneMessageTemplate getUseTemplateByTypeAndLanguage(int type,String languageCode);
	
	/**
	 * 翻页查询所有短信模版
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pagePhoneMessageTemplate(PhoneMessageTemplate phoneMessageTemplate ,int pageNo, int pageSize);

}
