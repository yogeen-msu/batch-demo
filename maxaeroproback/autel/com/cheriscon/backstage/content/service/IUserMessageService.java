package com.cheriscon.backstage.content.service;

import java.util.List;

import com.cheriscon.common.model.UserMessage;

/**
 * 用户消息业务逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-3-5
 */
public interface IUserMessageService 
{
	/**
	 * 增加用户消息
	 * @param userMessage
	 */
	public void saveMessage(UserMessage userMessage)throws Exception;
	
	/**
	 * 根据用户类型、语言code、用户code查询该用户已读取的消息条数
	 * @param userType 用户类型
	 * @param languageCode 语言code
	 * @param userCode 用户code
	 * @param isDialog 是否弹出 0 ：未弹出 1：弹出
	 * @return
	 * @throws Exception
	 */
	public int queryUserMessageIsReadCount(int userType,String languageCode,String userCode,int isDialog)throws Exception;

	/**
	 * 根据用户类型、用户编码、消息code查询用户消息记录
	 * @param userType
	 * @param userCode
	 * @param messageCode
	 * @return
	 * @throws Exception
	 */
	public List<UserMessage> queryUserMessageList(int userType ,String userCode,String messageCode)throws Exception;
	
	/**
	 * 查询用户信息记录
	 * @param userType
	 * @param languageCode
	 * @return
	 * @throws Exception
	 */
	public int queryUserMessageCount(int userType,String languageCode,String userCode)throws Exception;
	



}
