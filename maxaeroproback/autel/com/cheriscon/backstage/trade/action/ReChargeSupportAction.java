package com.cheriscon.backstage.trade.action;



import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.member.service.IReChargeCardSupportService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;

import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.ReChargeCardSupport;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	充值卡支持
 * @author chenqichuan
 * @date   2013-10-15
 */
@ParentPackage("json_default")
@Namespace("/autel/trade")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class ReChargeSupportAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6693265756340979658L;

	@Resource
	private IReChargeCardSupportService supportService;
	
	//处理参数
	private ReChargeCardSupport cardSupport;
	
	
	public ReChargeCardSupport getCardSupport() {
		return cardSupport;
	}

	public void setCardSupport(ReChargeCardSupport cardSupport) {
		this.cardSupport = cardSupport;
	}

	//获取充值卡类型列表
	public void list(){
		try{
			this.webpage = supportService.pageReChargeCardSupport(cardSupport,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listrechargesupport", results = { @Result(name = "listrechargesupport", location = "/autel/backstage/trade/rechargecard_support_list.jsp") })
	public String listrechargesupport() throws Exception{
		list();
		return "listrechargesupport";
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "updateReChargeSupport")
	public String updateReChargeSupport() throws Exception{
		try{
			supportService.updateCardSupportStatus(cardSupport.getCode());
			this.msgs.add(getText("common.js.modSuccess"));
			this.urls.put(getText("common.js.returnlist"),"listrechargesupport.do");
		}catch(Exception e){
			this.msgs.add(getText("common.js.modFail"));
			this.urls.put(getText("common.js.returnlist"),"listrechargesupport.do");
		}
		return MESSAGE;
	}


}	
