package com.cheriscon.backstage.trade.action;



import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.jsoup.helper.StringUtil;

import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;

import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.ExcelUtils;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	充值卡信息action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/autel/trade")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class ReChargeCardInfoAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6693265756340979658L;

	@Resource
	private IReChargeCardInfoService reChargeCardInfoService;

	@Resource
	private IReChargeCardTypeService reChargeCardTypeService;
	
	@Resource
	private ISealerInfoService sealerInfoService;
	
	
	private ReChargeCardInfo reChargeCardInfo;
	
	private List<ReChargeCardInfo> reChargeCardInfos;
	
	private List<ReChargeCardType> reChargeCardTypes;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private ReChargeCardInfo reChargeCardInfoSel;
	private ReChargeCardInfo reChargeCardInfoAdd;
	private ReChargeCardInfo reChargeCardInfoEdit;
	
	private String jsonData;
	
	private File reChargeCardFile;
	
	private int result;
	
	private String cardInfoStr;
	private int sheetNum; 
	private String remark;
	
	
	//获取充值卡信息列表
	public void list(){
		try{
			this.webpage = reChargeCardInfoService.pageReChargeCardInfoPage(reChargeCardInfoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listrechargecardinfo", results = { @Result(name = "listrechargecardinfo", location = "/autel/backstage/trade/rechargecard_info_list.jsp") })
	public String listReChargeCardInfo() throws Exception{
		list();
		return "listrechargecardinfo";
	}

	@Action(value = "createrechargecardinfo", results = { @Result(name = "createrechargecardinfo", location = "/autel/backstage/trade/rechargecard_info_create.jsp") })
	public String createReChargeCardInfo() throws Exception{
		this.reChargeCardTypes = reChargeCardTypeService.listAll();
		return "createrechargecardinfo";
	}

	@Action(value = "toCheckReChargeCard",results = { @Result(name = "toCheckReChargeCard", location = "/autel/backstage/trade/rechargecard_info_check.jsp") })
	public String toCheckReChargeCard() throws Exception{
		
		return "toCheckReChargeCard";
	}
	
	
	//将升级卡信息和excel进行比对
	
	@Action(value = "checkReChargeCard")
	public String checkReChargeCard() throws Exception{
		
		String [] cards=cardInfoStr.split("|");
		String card=null;
		ExcelUtils excelUtils = new ExcelUtils(reChargeCardFile, sheetNum);  //读取文件的第一个sheet
		List<List<String>> resultList = excelUtils.readSimpleExcel(0,3);
		List<String> tempList=null;
		WritableWorkbook rwb = null;
	    Cell cell = null;
		
		
		for(int i=0;i<cards.length;i++){
			card=cards[i];
			String checkFlag="N";
			for(int j=0;j<resultList.size();j++){
				tempList=resultList.get(j);
				if(card.equals(tempList.get(0))){
					if(StringUtils.isBlank(tempList.get(3))){
						checkFlag="Y";
					}else{
						checkFlag="H";
					}
				}
			}
			if(checkFlag.equals("N")){     //号码不存在
				this.msgs.add(card+":不存在,请检查");
			}
			if(checkFlag.equals("H")){ //已经出货
				this.msgs.add(card+":已经出库,请检查");
			}
		}
		
		return MESSAGE;
	}
	
	
	
	
	
	/**
	 * 将充值卡信息导入数据库
	 * @return
	 * @throws Exception
	 */
@SuppressWarnings("unchecked")
	//	@Action(value = "savecreaterechargecard", results = { @Result(name = "savecreaterechargecard", location = "/autel/backstage/member/iframe_result.jsp")})
	@Action(value = "savecreaterechargecard")
	public String saveCreateReChargeCard() throws Exception{
		this.result = -1;
		List<Map> maps = new ArrayList<Map>();
		
		SealerInfo sealer=sealerInfoService.getSealerByAutelId(reChargeCardInfoAdd.getSealerAutelId());
		
		if(sealer==null){
			this.msgs.add("经销商编号不存在");
			this.urls.put("返回重新填写", "javascript:history.go(-1);");
			
			return MESSAGE;
		}
		
		
		if(reChargeCardFile != null)
		{
			try{
				maps = reChargeCardInfoService.saveCreateReChargeCard(reChargeCardInfoAdd.getSealerAutelId(),reChargeCardInfoAdd.getReChargeCardTypeCode(), reChargeCardFile);
				this.result = 0;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		//return "savecreaterechargecard";
		for (Map map : maps) {
			if(Integer.parseInt(map.get("type").toString())==1)
			{
				if(Integer.parseInt(map.get("megCode").toString())==2)
				{
					this.msgs.add(getText("trademan.rechargecard.message33",new String[] {"["+map.get("serialNo").toString()+"]"}));
				}
				else if(Integer.parseInt(map.get("megCode").toString())==3)
				{
					this.msgs.add(getText("trademan.rechargecard.message34",new String[] {"["+map.get("serialNo").toString()+"]"}));
				}
				else if(Integer.parseInt(map.get("megCode").toString())==4)
				{
					this.msgs.add(getText("trademan.rechargecard.message35",new String[] {"["+map.get("serialNo").toString()+"]"}));
				}
			}
			else if(Integer.parseInt(map.get("type").toString())==2)
			{
				if(Integer.parseInt(map.get("megCode").toString())==1)
				{
					this.msgs.add(getText("trademan.rechargecard.message32"));
				}
			}
			else if(Integer.parseInt(map.get("type").toString())==3)
			{
				this.msgs.add(getText("trademan.rechargecard.message31",new String[] {map.get("num").toString()}));
			}
		}
		
		
		if(result!=0)
		{
			this.msgs.add(getText("trademan.rechargecard.message25"));
		}

		this.urls.put(getText("trademan.rechargecard.message20"),"listrechargecardinfo.do");
		
		return MESSAGE;
	}

//	@Action(value = "activerechargecardinfo", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "activerechargecardinfo")
	public String activeReChargeCardInfo() throws Exception{
		int result = -1;
		try{
			reChargeCardInfoService.modiActiveReChargeCardInfos("'"+code+"'",2);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("trademan.rechargecard.message26"));
		}
		else
		{
			this.msgs.add(getText("trademan.rechargecard.message27"));
		}

		this.urls.put(getText("trademan.rechargecard.message20"),"listrechargecardinfo.do");
		
		return MESSAGE;
	}

//	@Action(value = "activerechargecardinfos", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "activerechargecardinfos")
	public String activeReChargeCardInfos() throws Exception{
		int result = -1;
		try{
			selectedcodes = selectedcodes.replace("‘", "'");
			reChargeCardInfoService.modiActiveReChargeCardInfos(selectedcodes,2);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("trademan.rechargecard.message26"));
		}
		else
		{
			this.msgs.add(getText("trademan.rechargecard.message27"));
		}

		this.urls.put(getText("trademan.rechargecard.message20"),"listrechargecardinfo.do");
		
		return MESSAGE;
	}

//	@Action(value = "delrechargecardinfo", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delrechargecardinfo")
	public String delReChargeCardInfo() throws Exception{
		int result = -1;
		try{
			reChargeCardInfoService.delReChargeCardInfo(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("trademan.rechargecard.message28"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.rechargecardtype.message26"));
		}
		else
		{
			this.msgs.add(getText("trademan.rechargecard.message29"));
		}

		this.urls.put(getText("trademan.rechargecard.message20"),"listrechargecardinfo.do");
		
		return MESSAGE;
	}

//	@Action(value = "delrechargecardinfos", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delrechargecardinfos")
	public String delReChargeCardInfos() throws Exception{
		int result = -1;
		try{
			selectedcodes = selectedcodes.replace("‘", "'");
			reChargeCardInfoService.delReChargeCardInfos(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("trademan.rechargecard.message28"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.rechargecardtype.message26"));
		}
		else
		{
			this.msgs.add(getText("trademan.rechargecard.message29"));
		}

		this.urls.put(getText("trademan.rechargecard.message20"),"listrechargecardinfo.do");
		
		return MESSAGE;
	}
	
	
	public ReChargeCardInfo getReChargeCardInfo() {
		return reChargeCardInfo;
	}

	public void setReChargeCardInfo(ReChargeCardInfo reChargeCardInfo) {
		this.reChargeCardInfo = reChargeCardInfo;
	}

	public List<ReChargeCardInfo> getReChargeCardInfos() {
		return reChargeCardInfos;
	}

	public void setReChargeCardInfos(List<ReChargeCardInfo> reChargeCardInfos) {
		this.reChargeCardInfos = reChargeCardInfos;
	}

	public List<ReChargeCardType> getReChargeCardTypes() {
		return reChargeCardTypes;
	}

	public void setReChargeCardTypes(List<ReChargeCardType> reChargeCardTypes) {
		this.reChargeCardTypes = reChargeCardTypes;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public ReChargeCardInfo getReChargeCardInfoSel() {
		return reChargeCardInfoSel;
	}

	public void setReChargeCardInfoSel(ReChargeCardInfo reChargeCardInfoSel) {
		this.reChargeCardInfoSel = reChargeCardInfoSel;
	}

	public ReChargeCardInfo getReChargeCardInfoAdd() {
		return reChargeCardInfoAdd;
	}

	public void setReChargeCardInfoAdd(ReChargeCardInfo reChargeCardInfoAdd) {
		this.reChargeCardInfoAdd = reChargeCardInfoAdd;
	}

	public ReChargeCardInfo getReChargeCardInfoEdit() {
		return reChargeCardInfoEdit;
	}

	public void setReChargeCardInfoEdit(ReChargeCardInfo reChargeCardInfoEdit) {
		this.reChargeCardInfoEdit = reChargeCardInfoEdit;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public File getReChargeCardFile() {
		return reChargeCardFile;
	}

	public void setReChargeCardFile(File reChargeCardFile) {
		this.reChargeCardFile = reChargeCardFile;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getCardInfoStr() {
		return cardInfoStr;
	}

	public void setCardInfoStr(String cardInfoStr) {
		this.cardInfoStr = cardInfoStr;
	}

	public int getSheetNum() {
		return sheetNum;
	}

	public void setSheetNum(int sheetNum) {
		this.sheetNum = sheetNum;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}	
