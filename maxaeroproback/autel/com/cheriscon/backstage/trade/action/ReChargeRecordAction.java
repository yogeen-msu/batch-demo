package com.cheriscon.backstage.trade.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.trade.service.IReChargeRecordService;

import com.cheriscon.common.model.ReChargeRecord;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.framework.action.WWAction;

/**
 * @remark	充值卡使用日志action
 * @author pengdongan
 * @date   2013-01-23
 */
@Namespace("/autel/trade")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class ReChargeRecordAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1848260468144422869L;

	@Resource
	private IReChargeRecordService reChargeRecordService;

	@Resource
	private IProductTypeService productTypeService;
	
	private ReChargeRecord reChargeRecord;
	
	private List<ReChargeRecord> reChargeRecords;
	
	private List<ProductType> productTypes;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private ReChargeRecord reChargeRecordSel;

	
	
	//获取充值卡使用日志列表
	public void list(){
		try{
			this.productTypes = productTypeService.listAll();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			this.webpage = reChargeRecordService.pageReChargeRecordPage(reChargeRecordSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listrechargerecord", results = { @Result(name = "listrechargerecord", location = "/autel/backstage/trade/recharge_record_list.jsp") })
	public String listReChargeRecord() throws Exception{
		list();
		return "listrechargerecord";
	}
	
	
	public ReChargeRecord getReChargeRecord() {
		return reChargeRecord;
	}

	public void setReChargeRecord(ReChargeRecord reChargeRecord) {
		this.reChargeRecord = reChargeRecord;
	}

	public List<ReChargeRecord> getReChargeRecords() {
		return reChargeRecords;
	}

	public void setReChargeRecords(List<ReChargeRecord> reChargeRecords) {
		this.reChargeRecords = reChargeRecords;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public ReChargeRecord getReChargeRecordSel() {
		return reChargeRecordSel;
	}

	public void setReChargeRecordSel(ReChargeRecord reChargeRecordSel) {
		this.reChargeRecordSel = reChargeRecordSel;
	}

	public List<ProductType> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductType> productTypes) {
		this.productTypes = productTypes;
	}

}	
