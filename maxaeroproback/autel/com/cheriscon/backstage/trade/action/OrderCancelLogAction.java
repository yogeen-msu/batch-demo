package com.cheriscon.backstage.trade.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.trade.service.IOrderCancelLogService;
import com.cheriscon.backstage.trade.service.IOrderInfoVoService;
import com.cheriscon.backstage.trade.vo.OrderInfoVo;

import com.cheriscon.common.model.OrderCancelLog;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * @remark	付款日志action
 * @author pengdongan
 * @date   2013-01-23
 */
@ParentPackage("json_default")
@Namespace("/autel/trade")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class OrderCancelLogAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 149426994573825423L;

	/**
	 * 
	 */

	@Resource
	private IOrderInfoService orderInfoService;
	
	@Resource
	private IOrderCancelLogService orderCancelLogService;
	
	private OrderCancelLog orderCancelLog;
	
	
	//分页显示订单取消日志
	@Action(value = "listordercancel", results = { @Result(name = "listordercancel", location = "/autel/backstage/trade/order_cancel_list.jsp") })
	public String listOrderCancel() throws Exception{
		try{
			if(orderCancelLog==null){
				orderCancelLog=new OrderCancelLog();
			}
			this.webpage = orderCancelLogService.pageOrderCancelLogPage(orderCancelLog,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
		return "listordercancel";
	}
	
	//跳转到新增订单取消日志
	@Action(value = "addOrderCancelLog", results = { @Result(name = "addOrderCancelLog", location = "/autel/backstage/trade/order_cancel_add.jsp") })
	public String addOrderCancelLog() throws Exception{
		return "addOrderCancelLog" ;
	}
	
	//保存订单取消付款日志
	@SuppressWarnings("unchecked")
	@Action(value = "saveOrderCancelLog")
	public String saveOrderCancelLog() throws Exception{
		OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderCancelLog.getOrderCode());
		if(orderInfo==null){
			this.msgs.add("订单号输入错误");
			this.urls.put("返回重新填写", "javascript:history.go(-1);");
			return MESSAGE;
		}
		if(!orderInfo.getOrderPayState().equals(FrontConstant.ORDER_PAY_STATE_YES)){
			this.msgs.add("只能对状态为已支付的订单进行取消付款操作");
			this.urls.put("返回重新填写！", "javascript:history.go(-1);");
			return MESSAGE;
		}
		orderCancelLogService.saveOrderCancelLog(orderInfo,orderCancelLog);	
		this.msgs.add("操作成功");
		this.urls.put("返回列表", "listordercancel.do");
		return MESSAGE;
	}
	
	   //已经追回款项，恢复客户以前状态
		@SuppressWarnings("unchecked")
		@Action(value = "cancelOrderCancelLog")
		public String cancelOrderCancelLog() throws Exception{
			try{
				orderCancelLogService.updateOrderCancelLog(orderCancelLog.getId());	
				this.msgs.add("操作成功");
				this.urls.put("返回列表", "listordercancel.do");
			}catch(Exception e){
				this.msgs.add("系统错误,请联系管理员");
				this.urls.put("返回！", "javascript:history.go(-1);");
			}
			return MESSAGE;
		}
	

	public OrderCancelLog getOrderCancelLog() {
		return orderCancelLog;
	}

	public void setOrderCancelLog(OrderCancelLog orderCancelLog) {
		this.orderCancelLog = orderCancelLog;
	}

}	
