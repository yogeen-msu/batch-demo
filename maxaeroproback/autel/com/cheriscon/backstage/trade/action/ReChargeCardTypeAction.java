package com.cheriscon.backstage.trade.action;



import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.IAreaConfigService;
import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;

import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.database.DBRuntimeException;

/**
 * @remark	充值卡类型action
 * @author pengdongan
 * @date   2013-01-17
 */
@ParentPackage("json_default")
@Namespace("/autel/trade")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class ReChargeCardTypeAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6693265756340979658L;

	@Resource
	private IReChargeCardTypeService reChargeCardTypeService;

	@Resource
	private IProductTypeService productTypeService;

	@Resource
	private ISaleContractService saleContractService;
	
	@Resource
	private IProductForSealerService productForSealerService;
	
	@Resource
	private IAreaConfigService areaConfigService;
	
	private ReChargeCardType reChargeCardType;
	
	private List<ReChargeCardType> reChargeCardTypes;
	
	private List<ProductForSealer> productTypes;
	
	private List<SaleContract> saleContracts;
	
	private List<AreaConfig> areaConfigs;
	
	public List<AreaConfig> getAreaConfigs() {
		return areaConfigs;
	}

	public void setAreaConfigs(List<AreaConfig> areaConfigs) {
		this.areaConfigs = areaConfigs;
	}

	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private ReChargeCardType reChargeCardTypeSel;
	private ReChargeCardType reChargeCardTypeAdd;
	private ReChargeCardType reChargeCardTypeEdit;
	private String proTypeCode;
	


	private String jsonData;
	
	
	//获取充值卡类型列表
	public void list(){
		try{
			this.webpage = reChargeCardTypeService.pageReChargeCardTypePage(reChargeCardTypeSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listrechargecardtype", results = { @Result(name = "listrechargecardtype", location = "/autel/backstage/trade/rechargecard_type_list.jsp") })
	public String listReChargeCardType() throws Exception{
		list();
		return "listrechargecardtype";
	}

	@Action(value = "addrechargecardtype", results = { @Result(name = "addrechargecardtype", location = "/autel/backstage/trade/rechargecard_type_add.jsp") })
	public String addReChargeCardType() throws Exception{
		//this.productTypes = productTypeService.listAll();
		this.productTypes=productForSealerService.getProductForSealerList();
		this.saleContracts = saleContractService.listAll();
		//区域配置
		this.areaConfigs = this.areaConfigService.listAll();
		return "addrechargecardtype";
	}

//	@Action(value = "insertrechargecardtype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "insertrechargecardtype")
	public String insertReChargeCardType() throws Exception{
		int result = -1;
		try{
			if(StringUtils.isBlank(reChargeCardTypeAdd.getPrice()))
				reChargeCardTypeAdd.setPrice("0");
			reChargeCardTypeService.insertReChargeCardType(reChargeCardTypeAdd);
			result = 0;
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.rechargecardtype.message21"));
		}
		else
		{
			this.msgs.add(getText("memberman.rechargecardtype.message22"));
		}

		this.urls.put(getText("memberman.rechargecardtype.message20"),"listrechargecardtype.do");
		
		return MESSAGE;
	}

	@Action(value = "editrechargecardtype", results = { @Result(name = "editrechargecardtype", location = "/autel/backstage/trade/rechargecard_type_edit.jsp") })
	public String editReChargeCardType() throws Exception{
		try{
			this.reChargeCardTypeEdit = reChargeCardTypeService.getReChargeCardTypeByCode(code);
			//this.productTypes = productTypeService.listAll();
			this.productTypes=productForSealerService.getProductForSealerList();
			this.saleContracts = saleContractService.listAll();
			//区域配置
			this.areaConfigs = this.areaConfigService.listAll();
		}catch(Exception e){
			e.printStackTrace();
		}
		return "editrechargecardtype";
	}

//	@Action(value = "updaterechargecardtype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "updaterechargecardtype")
	public String updateReChargeCardType() throws Exception{
		int result = -1;
		
		try{
			reChargeCardType = reChargeCardTypeService.getReChargeCardTypeByCode(reChargeCardTypeEdit.getCode());

			if(reChargeCardType != null)
			{
				reChargeCardType.setName(reChargeCardTypeEdit.getName());
				
				if(StringUtils.isNotBlank(reChargeCardTypeEdit.getPrice()))
					reChargeCardType.setPrice(reChargeCardTypeEdit.getPrice());
				else
					reChargeCardType.setPrice("0");
				
				reChargeCardType.setProTypeCode(reChargeCardTypeEdit.getProTypeCode());
				reChargeCardType.setSaleCfgCode(reChargeCardTypeEdit.getSaleCfgCode());
				reChargeCardType.setAreaCfgCode(reChargeCardTypeEdit.getAreaCfgCode());
				reChargeCardType.setSaleContractCode(reChargeCardTypeEdit.getSaleContractCode());
				
				reChargeCardTypeService.updateReChargeCardType(reChargeCardType);
				result = 0;
			}
			else
			{
				result = 1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.rechargecardtype.message23"));
		}
		else
		{
			this.msgs.add(getText("memberman.rechargecardtype.message24"));
		}

		this.urls.put(getText("memberman.rechargecardtype.message20"),"listrechargecardtype.do");
		
		return MESSAGE;
	}

//	@Action(value = "delrechargecardtype", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delrechargecardtype")
	public String delReChargeCardType() throws Exception{
		int result = -1;
		try{
			reChargeCardTypeService.delReChargeCardType(code);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.rechargecardtype.message25"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.rechargecardtype.message26"));
		}
		else
		{
			this.msgs.add(getText("memberman.rechargecardtype.message27"));
		}

		this.urls.put(getText("memberman.rechargecardtype.message20"),"listrechargecardtype.do");
		
		return MESSAGE;
	}

//	@Action(value = "delrechargecardtypes", results = { @Result(name = SUCCESS, type = "json", params = {
//			"root", "jsonData" }) })
	@Action(value = "delrechargecardtypes")
	public String delReChargeCardTypes() throws Exception{
		int result = -1;
		try{
			reChargeCardTypeService.delReChargeCardTypes(selectedcodes);
			result = 0;
		}
		catch(DBRuntimeException e){
			result = 11;
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
//		this.jsonData = JSONArray.fromObject(result).toString();
//		
//		return SUCCESS;
		
		if(result == 0)
		{
			this.msgs.add(getText("memberman.rechargecardtype.message25"));
		}
		else if(result == 11)
		{
			this.msgs.add(getText("memberman.rechargecardtype.message26"));
		}
		else
		{
			this.msgs.add(getText("memberman.rechargecardtype.message27"));
		}

		this.urls.put(getText("memberman.rechargecardtype.message20"),"listrechargecardtype.do");
		
		return MESSAGE;
	}
	
	
	@Action(value = "getContractByProCode", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getContractByProCode() throws Exception {
		List<SaleContract> list=saleContractService.getContrctByProCode(proTypeCode);
		jsonData = JSONArray.fromObject(list).toString();
		return SUCCESS;
	}
	
	
	public ReChargeCardType getReChargeCardType() {
		return reChargeCardType;
	}

	public void setReChargeCardType(ReChargeCardType reChargeCardType) {
		this.reChargeCardType = reChargeCardType;
	}

	public List<ReChargeCardType> getReChargeCardTypes() {
		return reChargeCardTypes;
	}

	public void setReChargeCardTypes(List<ReChargeCardType> reChargeCardTypes) {
		this.reChargeCardTypes = reChargeCardTypes;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public ReChargeCardType getReChargeCardTypeSel() {
		return reChargeCardTypeSel;
	}

	public void setReChargeCardTypeSel(ReChargeCardType reChargeCardTypeSel) {
		this.reChargeCardTypeSel = reChargeCardTypeSel;
	}

	public ReChargeCardType getReChargeCardTypeAdd() {
		return reChargeCardTypeAdd;
	}

	public void setReChargeCardTypeAdd(ReChargeCardType reChargeCardTypeAdd) {
		this.reChargeCardTypeAdd = reChargeCardTypeAdd;
	}

	public ReChargeCardType getReChargeCardTypeEdit() {
		return reChargeCardTypeEdit;
	}

	public void setReChargeCardTypeEdit(ReChargeCardType reChargeCardTypeEdit) {
		this.reChargeCardTypeEdit = reChargeCardTypeEdit;
	}

	public List<ProductForSealer> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductForSealer> productTypes) {
		this.productTypes = productTypes;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public List<SaleContract> getSaleContracts() {
		return saleContracts;
	}

	public void setSaleContracts(List<SaleContract> saleContracts) {
		this.saleContracts = saleContracts;
	}
	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
}	
