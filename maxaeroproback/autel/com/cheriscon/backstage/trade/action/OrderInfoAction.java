package com.cheriscon.backstage.trade.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.member.vo.CustomerSoftwareVo;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.trade.service.IOrderInfoVoService;
import com.cheriscon.backstage.trade.vo.OrderInfoVo;

import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * @remark	付款日志action
 * @author pengdongan
 * @date   2013-01-23
 */
@Namespace("/autel/trade")
@ExceptionMappings( { @ExceptionMapping(exception = "java.lang.RuntimeException", result = "error") })
public class OrderInfoAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1848260468144422869L;

	@Resource
	private IOrderInfoVoService orderInfoVoService;

	@Resource
	private IProductTypeService productTypeService;

	@Resource
	private ILanguageService languageService;
	
	private OrderInfo orderInfo;
	
	private OrderInfoVo orderInfoVo;
	
	private List<OrderInfoVo> orderInfoVos;
	
	private List<ProductType> productTypes;
	
	//查询、修改、删除参数
	private String code;
	private String selectedcodes;
	private OrderInfoVo orderInfoVoSel;
	private OrderInfo orderInfoAdd;
	private OrderInfo orderInfoEdit;
	
	
	//获取付款日志列表
	public void list(){
		try{
			this.productTypes = productTypeService.listAll();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			this.webpage = orderInfoVoService.pageOrderInfoVoPage(orderInfoVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "listorderinfo", results = { @Result(name = "listorderinfo", location = "/autel/backstage/trade/order_info_list.jsp") })
	public String listOrderInfo() throws Exception{
		list();
		return "listorderinfo";
	}
	
	/**
	 * 订单详情列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showorderinfo", results = { @Result(name = "showorderinfo", location = "/autel/backstage/trade/order_detail_list.jsp") })
	public String showOrderInfo() throws Exception{
		
		try{
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			orderInfoVo = orderInfoVo != null ? orderInfoVo : new OrderInfoVo();
			orderInfoVo.setAdminLanguageCode(languageService.getByLocale(request.getLocale()).getCode());
			orderInfoVo.setCode(code);
			this.orderInfoVos = orderInfoVoService.listOrderDetail(orderInfoVo);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "showorderinfo";
	}
	
	
	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public OrderInfoVo getOrderInfoVo() {
		return orderInfoVo;
	}

	public void setOrderInfoVo(OrderInfoVo orderInfoVo) {
		this.orderInfoVo = orderInfoVo;
	}

	public List<OrderInfoVo> getOrderInfoVos() {
		return orderInfoVos;
	}

	public void setOrderInfoVos(List<OrderInfoVo> orderInfoVos) {
		this.orderInfoVos = orderInfoVos;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSelectedcodes() {
		return selectedcodes;
	}

	public void setSelectedcodes(String selectedcodes) {
		this.selectedcodes = selectedcodes;
	}

	public OrderInfoVo getOrderInfoVoSel() {
		return orderInfoVoSel;
	}

	public void setOrderInfoVoSel(OrderInfoVo orderInfoVoSel) {
		this.orderInfoVoSel = orderInfoVoSel;
	}

	public OrderInfo getOrderInfoAdd() {
		return orderInfoAdd;
	}

	public void setOrderInfoAdd(OrderInfo orderInfoAdd) {
		this.orderInfoAdd = orderInfoAdd;
	}

	public OrderInfo getOrderInfoEdit() {
		return orderInfoEdit;
	}

	public void setOrderInfoEdit(OrderInfo orderInfoEdit) {
		this.orderInfoEdit = orderInfoEdit;
	}

	public List<ProductType> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductType> productTypes) {
		this.productTypes = productTypes;
	}

}	
