package com.cheriscon.backstage.trade.service;

import java.util.List;

import com.cheriscon.backstage.trade.vo.RechargeRecordVo;

public interface IRechargeRecordVoService {
	
	public List<RechargeRecordVo> listReChargeRecordPage(RechargeRecordVo reChargeRecord) throws Exception;

}
