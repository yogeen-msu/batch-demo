package com.cheriscon.backstage.trade.service;


import java.util.List;

import com.cheriscon.backstage.trade.vo.OrderInfoVo;
import com.cheriscon.framework.database.Page;

/**
 * 付款日志业务逻辑实现接口类
 * @author pengdongan
 * @date 2013-01-23
 */
public interface IOrderInfoVoService 
{

	/**
	  * 付款日志分页显示方法
	  * @param OrderInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageOrderInfoVoPage(OrderInfoVo orderInfoVo, int pageNo,
			int pageSize) throws Exception;


	/**
	  * 导出订单信息列表
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public List<OrderInfoVo> listOrderToExcel(OrderInfoVo orderInfoVo) throws Exception;

	/**
	  * 获取订单详情列表
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public List<OrderInfoVo> listOrderDetail(OrderInfoVo orderInfoVo) throws Exception;
	

}
