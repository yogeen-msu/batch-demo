package com.cheriscon.backstage.trade.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.framework.database.Page;

/**
 * 充值卡信息业务逻辑接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-9
 */
public interface IReChargeCardInfoService 
{
	/**
	 * 根据充值卡账号、充值密码查询充值卡信息对象
	 * @param cardSerialNo
	 * @return
	 * @throws Exception
	 */
	public ReChargeCardInfo getReChargeCardInfoByCardNoAndPwd(String cardSerialNo,String reChargePwd)throws Exception;

	/**
	 * 更新充值卡信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateReChargeCardInfo(ReChargeCardInfo reChargeCardInfo) throws Exception;


	
	/**
	 * 添加充值卡信息
	 * @param reChargeCardInfo
	 * 充值卡类型
	 * @throws Exception
	 * @author pengdongan
	 */
	public int insertReChargeCardInfo(ReChargeCardInfo reChargeCardInfo) throws Exception;
	
	/**
	 * 保存excel中充值卡信息
	 * @param reChargeCardInfo
	 * 充值卡类型
	 * @throws Exception
	 * @author pengdongan
	 */
	public List<Map> saveCreateReChargeCard(String SealerAutelId,String reChargeCardTypeCode,File reChargeCardFile) throws Exception;
	
	/**
	 * 根据编码查询充值卡信息对象
	 * @param code	充值卡信息code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public ReChargeCardInfo getReChargeCardInfoByCode(String code) throws Exception;
	

	/**
	 * 根据序列号查询充值卡信息对象
	 * @param serialNo
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public ReChargeCardInfo getReChargeCardInfoBySerialNo(String serialNo) throws Exception;

	/**
	 * 批量修改充值卡激活状态
	 * @param codes
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public int modiActiveReChargeCardInfos(String codes,int isActive) throws Exception;
	
	/**
	 * 根据编码删除充值卡信息对象
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public int delReChargeCardInfo(String code) throws Exception;
	
	/**
	 * 根据codes删除多个充值卡信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public int delReChargeCardInfos(String codes) throws Exception;

	/**
	  * 充值卡信息分页显示方法
	  * @param ReChargeCardInfo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  * @author pengdongan
	  */
	public Page pageReChargeCardInfoPage(ReChargeCardInfo reChargeCardInfo, int pageNo,
			int pageSize) throws Exception;

}
