package com.cheriscon.backstage.trade.service;



import java.util.List;

import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.framework.database.Page;

/**
 * 充值卡类型业务逻辑实现接口类
 * @author pengdongan
 * @date 2013-01-24
 */
public interface IReChargeCardTypeService 
{	
	/**
	 * 查询所有充值卡类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<ReChargeCardType> listAll() throws Exception;
	
	/**
	 * 添加充值卡类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertReChargeCardType(ReChargeCardType reChargeCardType) throws Exception;
	
	/**
	 * 更新充值卡类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateReChargeCardType(ReChargeCardType reChargeCardType) throws Exception;
	
	/**
	 * 根据编码查询充值卡类型对象
	 * @param code	充值卡类型code
	 * @return
	 * @throws Exception
	 */
	public ReChargeCardType getReChargeCardTypeByCode(String code) throws Exception;
	
	/**
	 * 根据编码删除充值卡类型对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delReChargeCardType(String code) throws Exception;
	
	/**
	 * 根据删除多个充值卡类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delReChargeCardTypes(String codes) throws Exception;

	/**
	  * 充值卡类型分页显示方法
	  * @param ReChargeCardType
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageReChargeCardTypePage(ReChargeCardType reChargeCardType, int pageNo,
			int pageSize) throws Exception;
	

}
