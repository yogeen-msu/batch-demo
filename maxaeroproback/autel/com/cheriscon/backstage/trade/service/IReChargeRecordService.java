package com.cheriscon.backstage.trade.service;

import java.util.List;

import com.cheriscon.common.model.ReChargeRecord;
import com.cheriscon.framework.database.Page;

/**
 * 充值使用记录业务逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-9
 */
public interface IReChargeRecordService 
{
	/**
	 * 增加充值卡记录
	 * @param reChargeRecord
	 * @throws Exception
	 */
	public void addReChargeRecord(ReChargeRecord reChargeRecord) throws Exception;
	
	/**
	  * 充值卡使用记录分页显示方法
	  * @param ReChargeRecord
	  * @param pageNo
	  * @param pageSize
	  * @throws Exception
	  * @author pengdongan
	  */
	public Page pageReChargeRecordPage(ReChargeRecord reChargeRecord, int pageNo,
			int pageSize) throws Exception;
	

	
	
}
