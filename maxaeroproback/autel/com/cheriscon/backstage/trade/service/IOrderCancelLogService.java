package com.cheriscon.backstage.trade.service;

import com.cheriscon.common.model.OrderCancelLog;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.framework.database.Page;

/**
 * 付款取消业务日志
 * @author chenqichuan
 * @date 2014-08-27
 */
public interface IOrderCancelLogService 
{

	/**
	  * 付款取消分页显示方法
	  * @param OrderCancelLog
	  * @param pageNo
	  * @param pageSize
	  * @return Page
	  * @throws Exception
	  */
	public Page pageOrderCancelLogPage(OrderCancelLog orderCancelLog, int pageNo,
			int pageSize) throws Exception;


	/**
	  * 保存付款取消日志
	  * @param OrderCancelLog
	  * @return
	  * @throws Exception
	  */
	public void saveOrderCancelLog(OrderInfo orderInfo,OrderCancelLog orderCancelLog) throws Exception;
	
	/**
	  * 通过id获得取消日志
	  * @param id
	  * @return OrderCancelLog
	  * @throws Exception
	  */
	public OrderCancelLog getOrderCancelLogById(int id) throws Exception;;

	/**
	  * 通过id更新付款取消日志
	  * @param id
	  * @return 
	  * @throws Exception
	  */
	public void updateOrderCancelLog(int id) throws Exception;
	
	
	/**
	  * 通过产品编码查询是否存在退款记录
	  * @param proCode
	  * @return int
	  * @throws Exception
	  */
	public int getOrderCancelByProCode(String proCode) throws Exception;
}
