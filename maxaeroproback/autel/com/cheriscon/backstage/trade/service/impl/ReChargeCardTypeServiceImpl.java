package com.cheriscon.backstage.trade.service.impl;


import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 充值卡类型业务逻辑实现类
 * @author pengdongan
 * @date 2013-01-24
 */
@Service
public class ReChargeCardTypeServiceImpl extends BaseSupport<ReChargeCardType> implements IReChargeCardTypeService
{
	/**
	 * 查询所有充值卡类型对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ReChargeCardType> listAll() throws Exception {
		String sql = "select * from DT_ReChargeCardType order by id desc";
		
		return this.daoSupport.queryForList(sql, ReChargeCardType.class);
	}
	
	
	/**
	 * 添加充值卡类型
	 * @param reChargeCardType
	 * 充值卡类型
	 * @throws Exception
	 */
	@Transactional
	public int insertReChargeCardType(ReChargeCardType reChargeCardType) throws Exception {
		reChargeCardType.setCode(DBUtils.generateCode(DBConstant.RECHARGECARD_TYPE_CODE));
		this.daoSupport.insert("DT_ReChargeCardType", reChargeCardType);
		return 0;
	}
	
	/**
	 * 更新充值卡类型
	 * @param reChargeCardType
	 * 充值卡类型
	 * @throws Exception
	 */
	@Transactional
	public int updateReChargeCardType(ReChargeCardType reChargeCardType) throws Exception {
		this.daoSupport.update("DT_ReChargeCardType", reChargeCardType, "code='"+reChargeCardType.getCode()+"'");
		return 0;
	}

	public ReChargeCardType getReChargeCardTypeByCode(String code) throws Exception {
		String sql = "select * from DT_ReChargeCardType where code = ?";
		
		return (ReChargeCardType) this.daoSupport.queryForObject(sql, ReChargeCardType.class, code);
	}

	@Transactional
	public int delReChargeCardType(String code) throws Exception {
		String sql = "delete from DT_ReChargeCardType where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delReChargeCardTypes(String codes) throws Exception {
		String sql = "delete from DT_ReChargeCardType where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}



	/**
	  * 充值卡类型分页显示方法
	  * @param ReChargeCardType
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageReChargeCardTypePage(ReChargeCardType reChargeCardType, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select a.*,b.name proTypeName,c.name saleContractName");
		sql.append(" from DT_ReChargeCardType a");
		sql.append(" left join DT_ProductForSealer b on a.proTypeCode=b.code");
		sql.append(" left join DT_SaleContract c on a.saleContractCode=c.code");
		sql.append(" where 1=1");
		
		if (null != reChargeCardType) {
			if (StringUtils.isNotBlank(reChargeCardType.getName())) {
				sql.append(" and a.name like '%");
				sql.append(reChargeCardType.getName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeCardType.getProTypeName())) {
				sql.append(" and b.name like '%");
				sql.append(reChargeCardType.getProTypeName().trim());
				sql.append("%'");
			}
		}

		sql.append(" order by a.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, ReChargeCardType.class);
		return page;
	}

}
