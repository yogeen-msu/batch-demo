package com.cheriscon.backstage.trade.service.impl;


import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.trade.service.IOrderInfoVoService;
import com.cheriscon.backstage.trade.vo.OrderInfoVo;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 付款日志业务逻辑实现类
 * @author pengdongan
 * @date 2013-01-23
 */
@Service
public class OrderInfoVoServiceImpl extends BaseSupport<OrderInfoVo> implements IOrderInfoVoService
{
	/**
	  * 付款日志分页显示方法
	  * @param OrderInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageOrderInfoVoPage(OrderInfoVo orderInfoVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		
		sql.append("select * from (");

		sql.append("select distinct a.*,p.trade_no as payNumber ,b.name userName,b.autelId");
		sql.append(" from DT_OrderInfo a");
		sql.append(" left join DT_CustomerInfo b on a.userCode=b.code");
		sql.append(" left join DT_OrderMinSaleUnitDetail c on a.code=c.orderCode");
		sql.append(" left join DT_ProductInfo d on c.productSerialNo=d.serialNo");
		sql.append(" left join DT_PayLog p on a.code=p.out_trade_no");
		sql.append(" where 1=1");
		
		if (null != orderInfoVo) {
			if (StringUtils.isNotBlank(orderInfoVo.getCode())) {
				sql.append(" and a.code like '%");
				sql.append(orderInfoVo.getCode().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(orderInfoVo.getProTypeCode())) {
				sql.append(" and d.proTypeCode = '");
				sql.append(orderInfoVo.getProTypeCode().trim());
				sql.append("'");
			}
			if (orderInfoVo.getOrderState() != null) {
				sql.append(" and a.orderState = ");
				sql.append(orderInfoVo.getOrderState().toString());
			}
			if (orderInfoVo.getOrderPayState() != null) {
				sql.append(" and a.orderPayState = ");
				sql.append(orderInfoVo.getOrderPayState().toString());
			}
			if (orderInfoVo.getRecConfirmState() != null) {
				sql.append(" and a.recConfirmState = ");
				sql.append(orderInfoVo.getRecConfirmState().toString());
			}
			if (orderInfoVo.getProcessState() != null) {
				sql.append(" and a.processState = ");
				sql.append(orderInfoVo.getProcessState().toString());
			}
			if (StringUtils.isNotBlank(orderInfoVo.getBeginDate())) {
				sql.append(" and a.orderDate >= '");
				sql.append(orderInfoVo.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(orderInfoVo.getEndDate())) {
				sql.append(" and a.orderDate < DATE_ADD('");
				sql.append(orderInfoVo.getEndDate().trim());
				sql.append("', INTERVAL 1 DAY)");
			}
			
			if(StringUtils.isNotBlank(orderInfoVo.getPayBeginDate())){
				sql.append(" and a.payDate >= '");
				sql.append(orderInfoVo.getPayBeginDate().trim());
				sql.append("'");
			}
			if(StringUtils.isNotBlank(orderInfoVo.getPayEndDate())){
				sql.append(" and a.payDate < DATE_ADD('");
				sql.append(orderInfoVo.getPayEndDate().trim());
				sql.append("', INTERVAL 1 DAY)");
			}
			if(StringUtils.isNotBlank(orderInfoVo.getPayNumber())){
				sql.append(" and p.trade_no like '%");
				sql.append(orderInfoVo.getPayNumber().trim());
				sql.append("%'");
			}
			if(StringUtils.isNotBlank(orderInfoVo.getAutelId())){
				sql.append(" and b.autelId like '%");
				sql.append(orderInfoVo.getAutelId().trim());
				sql.append("%'");
			}
		}
		
		sql.append(") h");

		sql.append(" order by h.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, OrderInfoVo.class);
		return page;
	}


	/**
	  * 导出订单信息列表
	  * @param code
	  * @return
	  * @throws Exception
	  */
	@Override
	public List<OrderInfoVo> listOrderToExcel(OrderInfoVo orderInfoVo) throws Exception{
		StringBuffer sql = new StringBuffer();
		
		sql.append("select * from (");

		sql.append("select distinct a.*,p.trade_no as payNumber,b.name userName,b.autelId,e.name proTypeName,c.productSerialNo productSerialNo,c.minSaleUnitPrice ");
		sql.append(" from DT_OrderInfo a");
		sql.append(" left join DT_CustomerInfo b on a.userCode=b.code");
		sql.append(" left join DT_OrderMinSaleUnitDetail c on a.code=c.orderCode");
		sql.append(" left join DT_ProductInfo d on c.productSerialNo=d.serialNo");
		sql.append(" left join DT_ProductForSealer e on d.proTypeCode=e.code");
		sql.append(" left join DT_PayLog p on a.code=p.out_trade_no and p.result=1");
		sql.append(" where 1=1");
		
		if (null != orderInfoVo) {
			if (StringUtils.isNotBlank(orderInfoVo.getCode())) {
				sql.append(" and a.code like '%");
				sql.append(orderInfoVo.getCode().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(orderInfoVo.getProTypeCode())) {
				sql.append(" and d.proTypeCode = '");
				sql.append(orderInfoVo.getProTypeCode().trim());
				sql.append("'");
			}
			if (orderInfoVo.getOrderState() != null) {
				sql.append(" and a.orderState = ");
				sql.append(orderInfoVo.getOrderState().toString());
			}
			if (orderInfoVo.getOrderPayState() != null) {
				sql.append(" and a.orderPayState = ");
				sql.append(orderInfoVo.getOrderPayState().toString());
			}
			if (orderInfoVo.getRecConfirmState() != null) {
				sql.append(" and a.recConfirmState = ");
				sql.append(orderInfoVo.getRecConfirmState().toString());
			}
			if (orderInfoVo.getProcessState() != null) {
				sql.append(" and a.processState = ");
				sql.append(orderInfoVo.getProcessState().toString());
			}
			if (StringUtils.isNotBlank(orderInfoVo.getBeginDate())) {
				sql.append(" and a.orderDate >= '");
				sql.append(orderInfoVo.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(orderInfoVo.getEndDate())) {
				sql.append(" and a.orderDate < DATE_ADD('");
				sql.append(orderInfoVo.getEndDate().trim());
				sql.append("', INTERVAL 1 DAY)");
			}
			
			if(StringUtils.isNotBlank(orderInfoVo.getPayBeginDate())){
				sql.append(" and a.payDate >= '");
				sql.append(orderInfoVo.getPayBeginDate().trim());
				sql.append("'");
			}
			if(StringUtils.isNotBlank(orderInfoVo.getPayEndDate())){
				sql.append(" and a.payDate < DATE_ADD('");
				sql.append(orderInfoVo.getPayEndDate().trim());
				sql.append("', INTERVAL 1 DAY)");
			}
		}
		
		sql.append(") h");

		sql.append(" order by h.id desc");
		
		return this.daoSupport.queryForList(sql.toString(), OrderInfoVo.class);
	}


	/**
	  * 获取订单详情列表
	  * @param code
	  * @return
	  * @throws Exception
	  */
	@Override
	public List<OrderInfoVo> listOrderDetail(OrderInfoVo orderInfoVo) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select distinct a.*,b.name userName,b.autelId,d.proTypeCode proTypeCode,e.name proTypeName,g.name areaCfgName,f.name saleContractName,c.year,");
		sql.append("case c.isSoft when 1 then f.name when 0 then i.name else '' end minSaleUnitName,c.consumeType,c.minSaleUnitPrice,c.productSerialNo,");
		sql.append("case c.isSoft when 1 then j.validDate when 0 then l.validDate else '' end validDate,h.name sealerName,p.trade_no as payNumber");
		sql.append(" from DT_OrderInfo a");
		sql.append(" left join DT_CustomerInfo b on a.userCode=b.code");
		sql.append(" left join DT_PayLog p on a.code=p.out_trade_no");
		sql.append(" left join DT_OrderMinSaleUnitDetail c on a.code=c.orderCode");
		sql.append(" left join DT_ProductInfo d on c.productSerialNo=d.serialNo");
		sql.append(" left join DT_ProductForSealer e on d.proTypeCode=e.code");
		sql.append(" left join DT_SaleContract f on d.saleContractCode=f.code");
		sql.append(" left join DT_AreaConfig g on f.areaCfgCode=g.code");
		sql.append(" left join DT_SealerInfo h on f.sealerCode=h.code");
		sql.append(" left join DT_MinSaleUnitMemo i on c.minSaleUnitCode=i.minSaleUnitcode and i.languageCode =? and c.isSoft=1");
		sql.append(" left join DT_ProductSoftwareValidStatus j on c.minSaleUnitCode=j.minSaleUnitcode and d.code=j.proCode and c.isSoft=1");
		sql.append(" left join DT_MinSaleUnitSaleCfgDetail k on c.minSaleUnitCode=k.saleCfgcode and c.isSoft=0");
		sql.append(" left join DT_ProductSoftwareValidStatus l on k.minSaleUnitCode=l.minSaleUnitCode and d.code=l.proCode and c.isSoft=0");
		sql.append(" where a.code = ?");		

		
//		if (StringUtils.isNotBlank(orderInfoVo.getAdminLanguageCode())) {
//			sql.append(" and i.languageCode = '");
//			sql.append(orderInfoVo.getAdminLanguageCode().trim());
//			sql.append("'");
//		}
		
		return this.daoSupport.queryForList(sql.toString(), OrderInfoVo.class, orderInfoVo.getAdminLanguageCode().trim(), orderInfoVo.getCode());
	}
	
	
}
