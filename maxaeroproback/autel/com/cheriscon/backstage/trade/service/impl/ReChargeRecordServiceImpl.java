package com.cheriscon.backstage.trade.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.trade.service.IReChargeRecordService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ReChargeRecord;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 充值使用记录业务逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-9
 */
@Service
public class ReChargeRecordServiceImpl extends BaseSupport<ReChargeRecord> 
		implements IReChargeRecordService
{

	@Transactional
	public void addReChargeRecord(ReChargeRecord reChargeRecord)
			throws Exception 
	{
		String code = DBUtils.generateCode(DBConstant.RCR_INFO_CODE);
		reChargeRecord.setCode(code);
		
		this.daoSupport.insert("DT_ReChargeRecord", reChargeRecord);
		
	}
	
	public List<ReChargeRecord> listReChargeRecordPage(ReChargeRecord reChargeRecord) throws Exception{
		StringBuffer sql=new StringBuffer();

		
		sql.append("select a.proSerialNo,a.rechargeTime,a.memberAutelId,d.name as  proTypeName,");
		sql.append("e.serialNo as reChargeCardSerialNo,e.sealerAutelId as  sealerAutelId");
		sql.append(" from DT_ReChargeRecord a");
		sql.append(" left join DT_CustomerInfo b on a.memberCode=b.code and a.memberType=1");
		sql.append(" left join DT_ProductInfo c on a.proCode=c.code");
		sql.append(" left join DT_ProductForSealer d on c.proTypeCode=d.code");
		sql.append(" left join DT_ReChargeCardInfo e on a.reChargeCardCode=e.code");
		sql.append(" where 1=1");
		
		if (null != reChargeRecord) {
			if (StringUtils.isNotBlank(reChargeRecord.getReChargeCardSerialNo())) {
				sql.append(" and e.serialNo like '%");
				sql.append(reChargeRecord.getReChargeCardSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeRecord.getProTypeName())) {
				sql.append(" and d.name like '%");
				sql.append(reChargeRecord.getProTypeName().trim());
				sql.append("%'");
			}
			if (reChargeRecord.getMemberType() != null) {
				sql.append(" and a.memberType = ");
				sql.append(reChargeRecord.getMemberType().toString());
			}
			if (StringUtils.isNotBlank(reChargeRecord.getMemberAutelId())) {
				sql.append(" and a.memberAutelId like '%");
				sql.append(reChargeRecord.getMemberAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeRecord.getProSerialNo())) {
				sql.append(" and c.serialNo like '%");
				sql.append(reChargeRecord.getProSerialNo().trim());
				sql.append("%'");
			}
			if(StringUtils.isNotBlank(reChargeRecord.getStartDate())){
				sql.append(" and a.rechargeTime >= '");
				sql.append(reChargeRecord.getStartDate());
				sql.append("'");
			}if(StringUtils.isNotBlank(reChargeRecord.getEndDate())){
				sql.append(" and a.rechargeTime <= '");
				sql.append(reChargeRecord.getEndDate());
				sql.append("'");
			}if(StringUtils.isNotBlank(reChargeRecord.getStartDate())){
				sql.append(" and a.rechargeTime >= '");
				sql.append(reChargeRecord.getStartDate());
				sql.append("'");
			}if(StringUtils.isNotBlank(reChargeRecord.getEndDate())){
				sql.append(" and a.rechargeTime <= '");
				sql.append(reChargeRecord.getEndDate());
				sql.append("'");
			}
			
		}


		sql.append(" order by a.id desc");
		return this.daoSupport.queryForList(sql.toString(), ReChargeRecord.class);
	}
	
	
	/**
	  * 充值卡使用记录分页显示方法
	  * @param ReChargeRecord
	  * @param pageNo
	  * @param pageSize
	  * @throws Exception
	  * @author pengdongan
	  */ 
	@Override
	public Page pageReChargeRecordPage(ReChargeRecord reChargeRecord, int pageNo,
			int pageSize) throws Exception {
		
		StringBuffer sql = new StringBuffer();

		sql.append("select * from (");
		
		sql.append("select a.*,d.name proTypeName,d.code proTypeCode,");
		sql.append("case a.memberType when 1 then b.name when 2 then i.name else '' end memberName,");
		sql.append("e.serialNo reChargeCardSerialNo,f.name reChargeCardTypeName,g.name saleCfgName,h.name areaCfgName");
		sql.append(" from DT_ReChargeRecord a");
		sql.append(" left join DT_CustomerInfo b on a.memberCode=b.code and a.memberType=1");
		sql.append(" left join DT_ProductInfo c on a.proCode=c.code");
		sql.append(" left join DT_ProductForSealer d on c.proTypeCode=d.code");
		sql.append(" left join DT_ReChargeCardInfo e on a.reChargeCardCode=e.code");
		sql.append(" left join DT_ReChargeCardType f on e.reChargeCardTypeCode=f.code");
		sql.append(" left join DT_SaleContract j on f.saleContractCode=j.code");
		sql.append(" left join DT_SaleConfig g on j.saleCfgCode=g.code");
		sql.append(" left join DT_AreaConfig h on j.areaCfgCode=h.code");
		sql.append(" left join DT_SealerInfo i on a.memberCode=i.code and a.memberType=2");
		sql.append(" where 1=1");
		
		if (null != reChargeRecord) {
			if (StringUtils.isNotBlank(reChargeRecord.getReChargeCardSerialNo())) {
				sql.append(" and e.serialNo like '%");
				sql.append(reChargeRecord.getReChargeCardSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeRecord.getProTypeName())) {
				sql.append(" and d.name like '%");
				sql.append(reChargeRecord.getProTypeName().trim());
				sql.append("%'");
			}
			if (reChargeRecord.getMemberType() != null) {
				sql.append(" and a.memberType = ");
				sql.append(reChargeRecord.getMemberType().toString());
			}
			if (StringUtils.isNotBlank(reChargeRecord.getMemberAutelId())) {
				sql.append(" and a.memberAutelId like '%");
				sql.append(reChargeRecord.getMemberAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeRecord.getProSerialNo())) {
				sql.append(" and c.serialNo like '%");
				sql.append(reChargeRecord.getProSerialNo().trim());
				sql.append("%'");
			}if(StringUtils.isNotBlank(reChargeRecord.getStartDate())){
				sql.append(" and a.rechargeTime >= '");
				sql.append(reChargeRecord.getStartDate());
				sql.append("'");
			}if(StringUtils.isNotBlank(reChargeRecord.getEndDate())){
				sql.append(" and a.rechargeTime <= '");
				sql.append(reChargeRecord.getEndDate());
				sql.append("'");
			}
		}

		sql.append(") m");

		sql.append(" order by m.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, ReChargeRecord.class);
		return page;
	}

}
