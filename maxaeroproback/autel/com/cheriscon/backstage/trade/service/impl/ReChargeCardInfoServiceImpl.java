package com.cheriscon.backstage.trade.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.trade.service.IReChargeCardInfoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.ExcelUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;

/**
 * 充值卡信息业务逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-9
 */
@Service
public class ReChargeCardInfoServiceImpl extends BaseSupport<ReChargeCardInfo> 
	implements IReChargeCardInfoService
{

	@Override
	public ReChargeCardInfo getReChargeCardInfoByCardNoAndPwd(String cardSerialNo,String reChargePwd)
			throws Exception 
	{
		String sql = "select * from DT_ReChargeCardInfo where  reChargePwd=? "; //修改为只需要密码验证
		
		return this.daoSupport.queryForObject(sql, ReChargeCardInfo.class,reChargePwd);
		
		
	}

	@Transactional
	public int updateReChargeCardInfo(ReChargeCardInfo reChargeCardInfo)
			throws Exception 
	{
		this.daoSupport.update("DT_ReChargeCardInfo", reChargeCardInfo, "serialNo='"+reChargeCardInfo.getSerialNo()+"'");
		return 0;
	}


	
	/**
	 * 添加充值卡信息
	 * @param reChargeCardInfo
	 * 充值卡类型
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public int insertReChargeCardInfo(ReChargeCardInfo reChargeCardInfo) throws Exception {
		reChargeCardInfo.setCode(DBUtils.generateCode(DBConstant.RECHARGECARD_INFO_CODE));
		this.daoSupport.insert("DT_ReChargeCardInfo", reChargeCardInfo);
		return 0;
	}
	
	/**
	 * 保存excel中充值卡信息
	 * @param reChargeCardInfo
	 * 充值卡类型
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public List<Map> saveCreateReChargeCard(String SealerAutelId,String reChargeCardTypeCode,File reChargeCardFile) throws Exception {
		
		List<Map> maps = new ArrayList<Map>();
		Map map;
		int result=0;
		int num=0;
		
		if (reChargeCardFile != null) {
			ReChargeCardInfo reChargeCardInfo = new ReChargeCardInfo();
			reChargeCardInfo.setReChargeCardTypeCode(reChargeCardTypeCode);
			reChargeCardInfo.setIsUse(1);
			reChargeCardInfo.setIsActive(2);
			reChargeCardInfo.setSealerAutelId(SealerAutelId);
			
			ExcelUtils excelUtils = new ExcelUtils(reChargeCardFile, 0);
			List<List<String>> resultList = excelUtils.readSimpleExcel(0,2);
			
			String dateCheck = "^[0-9]{2,4}-[0-9]{1,2}-[0-9]{1,2}$";
			Pattern dateRegex = Pattern.compile(dateCheck);
			
			for (List<String> list : resultList) {
				result = 0;
				if (StringUtils.isNotBlank(list.get(0)))
				{
					if (StringUtils.isBlank(list.get(1)))
					{
						map = new HashMap();
						map.put("type", 1);
						map.put("serialNo", list.get(0));
						map.put("megCode", 2);
						maps.add(map);
						result = 2;
					}
					if (!dateRegex.matcher(list.get(2)).matches())
					{
						map = new HashMap();
						map.put("type", 1);
						map.put("serialNo", list.get(0));
						map.put("megCode", 3);
						maps.add(map);
						result = 3;
					}
					if(this.getReChargeCardInfoBySerialNo(list.get(0)) != null)
					{
						map = new HashMap();
						map.put("type", 1);
						map.put("serialNo", list.get(0));
						map.put("megCode", 4);
						maps.add(map);
						result = 4;
					}
					
					if (result == 0)
					{
						reChargeCardInfo.setSerialNo(list.get(0));
						reChargeCardInfo.setReChargePwd(list.get(1));
						reChargeCardInfo.setValidDate(DateUtil.toString(DateUtil.toDate(list.get(2), "yy-MM-dd"),"yyyy-MM-dd"));
						
						if(this.getReChargeCardInfoBySerialNo(reChargeCardInfo.getSerialNo()) == null)
						{
							this.insertReChargeCardInfo(reChargeCardInfo);
							num++;
						}
					}
				}
				else
				{
					map = new HashMap();
					map.put("type", 2);
					map.put("megCode", 1);
					maps.add(map);
					result = 1;
				}
			}
		}
		map = new HashMap();
		map.put("type", 3);
		map.put("num", num);
		maps.add(map);
		
		return maps;
	}
	

	/**
	 * 根据编码查询充值卡信息对象
	 * @param code	充值卡信息code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Override
	public ReChargeCardInfo getReChargeCardInfoByCode(String code) throws Exception {
		String sql = "select * from DT_ReChargeCardInfo where code = ?";
		
		return (ReChargeCardInfo) this.daoSupport.queryForObject(sql, ReChargeCardInfo.class, code);
	}
	

	/**
	 * 根据序列号查询充值卡信息对象
	 * @param serialNo
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Override
	public ReChargeCardInfo getReChargeCardInfoBySerialNo(String serialNo) throws Exception {
		String sql = "select * from DT_ReChargeCardInfo where serialNo = ?";
		
		return (ReChargeCardInfo) this.daoSupport.queryForObject(sql, ReChargeCardInfo.class, serialNo);
	}

	/**
	 * 批量修改充值卡激活状态
	 * @param codes
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public int modiActiveReChargeCardInfos(String codes,int isActive) throws Exception {
		String sql = "update DT_ReChargeCardInfo set isActive="+isActive+" where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}

	/**
	 * 根据编码删除充值卡信息对象
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public int delReChargeCardInfo(String code) throws Exception {
		String sql = "delete from DT_ReChargeCardInfo where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	/**
	 * 根据codes删除多个充值卡信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public int delReChargeCardInfos(String codes) throws Exception {
		String sql = "delete from DT_ReChargeCardInfo where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}



	/**
	  * 充值卡信息分页显示方法
	  * @param ReChargeCardInfo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  * @author pengdongan
	  */ 
	@Override
	public Page pageReChargeCardInfoPage(ReChargeCardInfo reChargeCardInfo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		
		sql.append("select * from (");

		sql.append("select a.*,b.name reChargeCardTypeName,case when validDate > DATE_SUB(NOW(),INTERVAL 1 DAY) then 1 else 2 end isOverdue");
		sql.append(" from DT_ReChargeCardInfo a");
		sql.append(" left join DT_ReChargeCardType b on a.reChargeCardTypeCode=b.code");
		sql.append(" where 1=1");
		
		if (null != reChargeCardInfo) {
			if (StringUtils.isNotBlank(reChargeCardInfo.getSerialNo())) {
				sql.append(" and a.serialNo like '%");
				sql.append(reChargeCardInfo.getSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeCardInfo.getReChargeCardTypeName())) {
				sql.append(" and b.name like '%");
				sql.append(reChargeCardInfo.getReChargeCardTypeName().trim());
				sql.append("%'");
			}
			if (reChargeCardInfo.getIsActive() != null) {
				sql.append(" and a.isActive = ");
				sql.append(reChargeCardInfo.getIsActive().toString());
			}
			if (reChargeCardInfo.getIsUse() != null) {
				sql.append(" and a.isUse = ");
				sql.append(reChargeCardInfo.getIsUse().toString());
			}
			if (StringUtils.isNotBlank(reChargeCardInfo.getBeginDate())) {
				sql.append(" and a.validDate >= '");
				sql.append(reChargeCardInfo.getBeginDate().trim());
				sql.append("'");
			}
			if (StringUtils.isNotBlank(reChargeCardInfo.getEndDate())) {
				sql.append(" and a.validDate < cast('");
				sql.append(reChargeCardInfo.getEndDate().trim());
				sql.append("' as datetime)+1");
			}
		}
		
		sql.append(") m");

		sql.append(" order by m.id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, ReChargeCardInfo.class);
		return page;
	}

}
