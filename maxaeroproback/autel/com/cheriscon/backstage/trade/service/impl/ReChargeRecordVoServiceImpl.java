package com.cheriscon.backstage.trade.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.trade.service.IReChargeRecordService;
import com.cheriscon.backstage.trade.service.IRechargeRecordVoService;
import com.cheriscon.backstage.trade.vo.RechargeRecordVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.ReChargeRecord;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * 充值使用记录业务逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-9
 */
@Service
public class ReChargeRecordVoServiceImpl extends BaseSupport<RechargeRecordVo> 
		implements IRechargeRecordVoService
{


	public List<RechargeRecordVo> listReChargeRecordPage(RechargeRecordVo reChargeRecord) throws Exception{
		StringBuffer sql=new StringBuffer();

		
		sql.append("select a.proSerialNo,SUBSTR(reChargeTime FROM 1 FOR 10) as rechargeTime,a.memberAutelId,d.name as  proTypeName,");
		sql.append("e.serialNo as reChargeCardSerialNo,e.sealerAutelId as  sealerAutelId");
		sql.append(" from DT_ReChargeRecord a");
		sql.append(" left join DT_CustomerInfo b on a.memberCode=b.code and a.memberType=1");
		sql.append(" left join DT_ProductInfo c on a.proCode=c.code");
		sql.append(" left join DT_ProductForSealer d on c.proTypeCode=d.code");
		sql.append(" left join DT_ReChargeCardInfo e on a.reChargeCardCode=e.code");
		sql.append(" where 1=1 ");
		
		if (null != reChargeRecord) {
			/*if (StringUtils.isNotBlank(reChargeRecord.getReChargeCardSerialNo())) {
				sql.append(" and e.serialNo like '%");
				sql.append(reChargeRecord.getReChargeCardSerialNo().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeRecord.getProTypeName())) {
				sql.append(" and d.name like '%");
				sql.append(reChargeRecord.getProTypeName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeRecord.getMemberAutelId())) {
				sql.append(" and a.memberAutelId like '%");
				sql.append(reChargeRecord.getMemberAutelId().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(reChargeRecord.getProSerialNo())) {
				sql.append(" and c.serialNo like '%");
				sql.append(reChargeRecord.getProSerialNo().trim());
				sql.append("%'");
			}*/
			if(StringUtils.isNotBlank(reChargeRecord.getStartDate())){
				sql.append(" and a.rechargeTime >= '");
				sql.append(reChargeRecord.getStartDate());
				sql.append("'");
			}if(StringUtils.isNotBlank(reChargeRecord.getEndDate())){
				sql.append(" and a.rechargeTime <= '");
				sql.append(reChargeRecord.getEndDate());
				sql.append("'");
			}
			
		}


		sql.append(" order by a.id desc");
		return this.daoSupport.queryForList(sql.toString(), RechargeRecordVo.class);
	}
	


}
