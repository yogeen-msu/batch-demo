package com.cheriscon.backstage.trade.service.impl;


import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeLogService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.trade.service.IOrderCancelLogService;
import com.cheriscon.common.constant.BackageConstant;
import com.cheriscon.common.model.OrderCancelLog;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderMinSaleUnitDetail;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidChangeLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

@Service
public class OrderCancelLogServiceImpl extends BaseSupport implements IOrderCancelLogService {

	@Resource
	private IAdminUserManager adminUserManager;
	@Resource
	private IProductSoftwareValidStatusService productSoftwareValidStatusService; 
	@Resource
	private IProductSoftwareValidChangeLogService productSoftwareValidChangeLogService;
	@Resource
	private IOrderInfoService orderInfoService;
	@Resource
	private IProductInfoService productInfoService; 
	public OrderCancelLogServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Page pageOrderCancelLogPage(OrderCancelLog orderCancelLog,
			int pageNo, int pageSize) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("select a.*,b.autelId as custAutelId,c.serialNo as proSreialNo from dt_ordercancellog a ");
		sql.append(" left outer join dt_customerinfo b on a.custCode=b.`code`");
		sql.append(" left outer join dt_productinfo c on a.proCode=c.`code` ");
		sql.append(" where 1=1 ");
		//根据客户ID查询
		if(!StringUtil.isEmpty(orderCancelLog.getCustAutelId())){
			sql.append(" and b.autelId like '%").append(orderCancelLog.getCustAutelId().trim()).append("%'");
		}
		//根据订单号查询
		if(!StringUtil.isEmpty(orderCancelLog.getOrderCode())){
			sql.append(" and a.orderCode like '%").append(orderCancelLog.getOrderCode().trim()).append("%'");
		}
		//根据产品序列号查询
		if(!StringUtil.isEmpty(orderCancelLog.getProSreialNo())){
			sql.append(" and c.serialNo like '%").append(orderCancelLog.getProSreialNo().trim()).append("%'");
		}
		
		sql.append(" order by a.id desc");
		
		
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}

	@Override
	@Transactional
	public void saveOrderCancelLog(OrderInfo orderInfo,OrderCancelLog orderCancelLog) throws Exception{
		try{
		//需要三步操作 1.更新订单状态 ,2.更新产品有效期，3.记录操作日志
		//1.更新订单状态
		String updateOrder="update dt_orderInfo set orderPayState=? where code=?";
		this.daoSupport.execute(updateOrder, new Object[]{FrontConstant.ORDER_PAY_STATE_BACK,orderCancelLog.getOrderCode()});
		//2.更新软件有效期，由于一个订单里可能存在多个购买产品，因此需要先根据订单号查出产品序列号
		String queryProductSN="select * from DT_OrderMinSaleUnitDetail where orderCode=?";
		List<OrderMinSaleUnitDetail> listProductSN=this.daoSupport.queryForList(queryProductSN,OrderMinSaleUnitDetail.class,orderCancelLog.getOrderCode());
		if(listProductSN!= null && listProductSN.size()>0){
			for(int j=0;j<listProductSN.size();j++){
				OrderMinSaleUnitDetail orderDetail=listProductSN.get(j);
				HttpServletRequest request=ThreadContextHolder.getHttpRequest();
				List<ProductSoftwareValidStatus> list=productSoftwareValidStatusService.getProductSoftwareValidList(orderDetail.getProductSerialNo());
				String validDate=DateUtil.toString(new Date(), "yyyy-MM-dd");
				if(list==null || list.size()==0){
					
				}else{
					for(int i=0 ;i<list.size();i++){
						ProductSoftwareValidStatus valid=list.get(i);
						
						ProductSoftwareValidChangeLog log=new ProductSoftwareValidChangeLog();
						AdminUser user = adminUserManager.getCurrentUser();
						user = adminUserManager.get(user.getUserid());
						String ip=FrontConstant.getIpAddr(request);
						log.setProductSN(orderDetail.getProductSerialNo());
						log.setMinSaleUnit(valid.getMinSaleUnitCode());
						log.setNewDate(validDate);
						log.setOldDate(valid.getValidDate());
						log.setOperatorIp(ip);
						log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
						log.setOperatorUser(user.getUsername());
						log.setOperType(BackageConstant.PRODUCT_VALID_CHANGE_BACK_MONEY);
						productSoftwareValidChangeLogService.saveProductSoftwareValidStatus(log);
						
						valid.setValidDate(validDate);
						productSoftwareValidStatusService.updateValidDate(valid);
					}
				}
				//3.添加日志
				ProductInfo productInfo=productInfoService.getBySerialNo(orderDetail.getProductSerialNo());
				
				String operDate=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
				AdminUser user = adminUserManager.getCurrentUser();
				user = adminUserManager.get(user.getUserid());
				String operUser=user.getUsername();
				orderCancelLog.setProSreialNo(orderDetail.getProductSerialNo());
				orderCancelLog.setOperUser(operUser);
				orderCancelLog.setOperDate(operDate);
				orderCancelLog.setProCode(productInfo.getCode());
				orderCancelLog.setCustCode(orderInfo.getUserCode());
				orderCancelLog.setStatus(1);
				this.daoSupport.insert("dt_ordercancellog", orderCancelLog);
		}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public OrderCancelLog getOrderCancelLogById(int id) throws Exception {
		
		String querySql="select * from dt_ordercancellog where id=?";
		return (OrderCancelLog) this.daoSupport.queryForObject(querySql, OrderCancelLog.class, id);
	}

	public List<OrderCancelLog> getOrderCancelLogByOrderCode(String orderCode) throws Exception{
		String sql="select * from DT_OrderCancelLog where orderCode=?";
		return this.daoSupport.queryForList(sql, OrderCancelLog.class, orderCode);
		
	}
	
	
	@Override
	@Transactional
	public void updateOrderCancelLog(int id) throws Exception {
		OrderCancelLog orderCancelLog=this.getOrderCancelLogById(id);
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		String lastOperDate=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		AdminUser user = adminUserManager.getCurrentUser();
		user = adminUserManager.get(user.getUserid());
		String lastOperUser=user.getUsername();
		//1.修改订单状态为3(已回款)
		String updateOrder="update dt_orderInfo set orderPayState=? where code=?";
		this.daoSupport.execute(updateOrder, new Object[]{FrontConstant.ORDER_PAY_STATE_REBACK,orderCancelLog.getOrderCode()});
		//2.更新有效期
		List<OrderCancelLog> listOrderCancel= this.getOrderCancelLogByOrderCode(orderCancelLog.getOrderCode());
		if(listOrderCancel != null && listOrderCancel.size()>0){
			for(int i=0;i<listOrderCancel.size();i++){
				OrderCancelLog order=listOrderCancel.get(i);
				ProductInfo productInfo=productInfoService.getByProCode(order.getProCode());
				String validDate="";
				String queryValid="select * from dt_ProductSoftwareValidChangeLog where productSn=? and operType=? order by id desc limit 0,1 ";
				
				ProductSoftwareValidChangeLog changeLog=(ProductSoftwareValidChangeLog) this.daoSupport.queryForObject(queryValid,ProductSoftwareValidChangeLog.class,productInfo.getSerialNo(),BackageConstant.PRODUCT_VALID_CHANGE_BACK_MONEY);
				if(changeLog!=null && changeLog.getNewDate()!=null){
					validDate= changeLog.getOldDate();
				}
				
				List<ProductSoftwareValidStatus> list=productSoftwareValidStatusService.getProductSoftwareValidList(productInfo.getSerialNo());
				if(list==null || list.size()==0){
					
				}else{
					for(int j=0 ;j<list.size();j++){
						ProductSoftwareValidStatus valid=list.get(j);
						
						ProductSoftwareValidChangeLog log=new ProductSoftwareValidChangeLog();
						
						String ip=FrontConstant.getIpAddr(request);
						log.setProductSN(productInfo.getSerialNo());
						log.setMinSaleUnit(valid.getMinSaleUnitCode());
						log.setNewDate(validDate);
						log.setOldDate(valid.getValidDate());
						log.setOperatorIp(ip);
						log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
						log.setOperatorUser(user.getUsername());
						log.setOperType(BackageConstant.PRODUCT_VALID_CHANGE_RE_BACK);
						productSoftwareValidChangeLogService.saveProductSoftwareValidStatus(log);
						
						valid.setValidDate(validDate);
						productSoftwareValidStatusService.updateValidDate(valid);
					}
				}
				
			}
		
		}
		
		//3.更新日志表状态
		String updateSql="update dt_ordercancellog set lastOperUser=?,lastOperDate=?,status=? where orderCode=?";
		this.daoSupport.execute(updateSql, new Object[]{lastOperUser,lastOperDate,0,orderCancelLog.getOrderCode()});
		
	}

	public int getOrderCancelByProCode(String proCode) throws Exception{
		String sql="select count(1) from dt_ordercancellog where proCode=? and status=?";
		return this.daoSupport.queryForInt(sql, new Object[]{proCode,1});
	}
	
}
