package com.cheriscon.backstage.trade.vo;



/**
 * 
* @Description: 付款信息业务对象
* @author pengdongan
* @date 2013-01-23
*
 */
public class OrderInfoVo implements java.io.Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4231539158738948208L;
	
	private Integer id;						//ID
	private String code;					//订单code
	private String userCode;				//用户code
	private String orderMoney;				//订单金额
	private String orderDate;				//下单日期
	private Integer orderState;				//订单状态
	private Integer orderPayState;			//订单支付状态（0：未支付 1：已支付）
	private Integer recConfirmState;		//收款确认状态（0：未确认 1：已确认）
	private Integer processState;			//处理状态（0：未处理 1：已处理）
	private String discountMoney;			//优惠金额
	private String bankNumber;				//银行流水号
	private String userName;				//付款用户名称
	private String autelId;					//付款用户ID
	private String payDate;					//支付日期
	private String recConfirmDate;			//确认收款日期
	private String processDate;				//处理日期
	private String proTypeCode;				//产品型号code
	private String proTypeName;				//产品型号名称
	private String areaCfgName;				//区域配置名称
	private String minSaleUnitName;			//最小销售单位名称
	private Integer consumeType;			//消费类型
	private String minSaleUnitPrice;		//最小销售单位价格
	private String validDate;				//最小销售单位有效期
	private String sealerName;				//经销商名称
	private String saleContractName;		//销售契约名称
	private String year;					//续租或购买年份
	
	private String adminLanguageCode;	//管理员首选语言code
	
	private String productSerialNo;
	
	private String beginDate;	
	private String endDate;
	private String payNumber; //paypal流水号
	private String payBeginDate; //支付开始日期
	private String payEndDate; //支付结束日期

	public String getPayBeginDate() {
		return payBeginDate;
	}


	public String getPayEndDate() {
		return payEndDate;
	}

	public void setPayBeginDate(String payBeginDate) {
		this.payBeginDate = payBeginDate;
	}



	public void setPayEndDate(String payEndDate) {
		this.payEndDate = payEndDate;
	}

	public String getPayNumber() {
		return payNumber;
	}

	public void setPayNumber(String payNumber) {
		this.payNumber = payNumber;
	}

	public OrderInfoVo() {
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(String orderMoney) {
		this.orderMoney = orderMoney;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getOrderState() {
		return orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	public Integer getOrderPayState() {
		return orderPayState;
	}

	public void setOrderPayState(Integer orderPayState) {
		this.orderPayState = orderPayState;
	}

	public Integer getRecConfirmState() {
		return recConfirmState;
	}

	public void setRecConfirmState(Integer recConfirmState) {
		this.recConfirmState = recConfirmState;
	}

	public String getDiscountMoney() {
		return discountMoney;
	}

	public void setDiscountMoney(String discountMoney) {
		this.discountMoney = discountMoney;
	}

	public String getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(String bankNumber) {
		this.bankNumber = bankNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	public String getAreaCfgName() {
		return areaCfgName;
	}


	public void setAreaCfgName(String areaCfgName) {
		this.areaCfgName = areaCfgName;
	}


	public String getMinSaleUnitName() {
		return minSaleUnitName;
	}



	public void setMinSaleUnitName(String minSaleUnitName) {
		this.minSaleUnitName = minSaleUnitName;
	}


	public Integer getConsumeType() {
		return consumeType;
	}

	public void setConsumeType(Integer consumeType) {
		this.consumeType = consumeType;
	}

	public String getMinSaleUnitPrice() {
		return minSaleUnitPrice;
	}


	public void setMinSaleUnitPrice(String minSaleUnitPrice) {
		this.minSaleUnitPrice = minSaleUnitPrice;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	
	public String getSealerName() {
		return sealerName;
	}
	
	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}

	public String getAdminLanguageCode() {
		return adminLanguageCode;
	}


	public void setAdminLanguageCode(String adminLanguageCode) {
		this.adminLanguageCode = adminLanguageCode;
	}


	public String getProductSerialNo() {
		return productSerialNo;
	}


	public void setProductSerialNo(String productSerialNo) {
		this.productSerialNo = productSerialNo;
	}


	public String getSaleContractName() {
		return saleContractName;
	}


	public void setSaleContractName(String saleContractName) {
		this.saleContractName = saleContractName;
	}





	public Integer getProcessState() {
		return processState;
	}





	public void setProcessState(Integer processState) {
		this.processState = processState;
	}





	public String getBeginDate() {
		return beginDate;
	}





	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}





	public String getEndDate() {
		return endDate;
	}





	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}





	public String getPayDate() {
		return payDate;
	}





	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}





	public String getRecConfirmDate() {
		return recConfirmDate;
	}





	public void setRecConfirmDate(String recConfirmDate) {
		this.recConfirmDate = recConfirmDate;
	}





	public String getProcessDate() {
		return processDate;
	}





	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}





	public String getYear() {
		return year;
	}





	public void setYear(String year) {
		this.year = year;
	}





	public String getAutelId() {
		return autelId;
	}





	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

}