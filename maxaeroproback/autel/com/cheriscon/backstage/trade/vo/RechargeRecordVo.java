package com.cheriscon.backstage.trade.vo;

public class RechargeRecordVo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4206480336230385859L;

	private String code;					//code
	private String proCode;					//产品code
	private String reChargeTime;			//充值时间
	private String memberAutelId;		     //用户autelId
	private String proSerialNo;				//产品序列号
	private String proTypeName;				//产品型号名称
	private String reChargeCardSerialNo;	//充值卡序列号
	private String reChargeCardTypeName;	//充值卡类型名称
    private String startDate;   //统计开始时间
    private String endDate;    //统计结束时间
	private String sealerAutelId; //升级卡所属经销商
	public String getCode() {
		return code;
	}
	public String getProCode() {
		return proCode;
	}
	public String getReChargeTime() {
		return reChargeTime;
	}
	public String getMemberAutelId() {
		return memberAutelId;
	}
	public String getProSerialNo() {
		return proSerialNo;
	}
	public String getProTypeName() {
		return proTypeName;
	}
	public String getReChargeCardSerialNo() {
		return reChargeCardSerialNo;
	}
	public String getReChargeCardTypeName() {
		return reChargeCardTypeName;
	}
	public String getStartDate() {
		return startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public String getSealerAutelId() {
		return sealerAutelId;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public void setReChargeTime(String reChargeTime) {
		this.reChargeTime = reChargeTime;
	}
	public void setMemberAutelId(String memberAutelId) {
		this.memberAutelId = memberAutelId;
	}
	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}
	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}
	public void setReChargeCardSerialNo(String reChargeCardSerialNo) {
		this.reChargeCardSerialNo = reChargeCardSerialNo;
	}
	public void setReChargeCardTypeName(String reChargeCardTypeName) {
		this.reChargeCardTypeName = reChargeCardTypeName;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}
	
	
}
