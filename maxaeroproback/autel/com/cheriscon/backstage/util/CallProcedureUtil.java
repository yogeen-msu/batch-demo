package com.cheriscon.backstage.util;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.cheriscon.framework.util.FileUtil;

public class CallProcedureUtil {

	/**
	 * @param args
	 */
	
	public static void call(String name,String productSN){
		long time1=System.currentTimeMillis();
		System.out.println("-------  start 调用存储过程:"+name+"====="+ System.currentTimeMillis()); 
		
		Properties prop = new Properties(); 
		InputStream in=null;
		in =  FileUtil.getResourceAsStream("config/jdbc.properties"); 
	    Connection conn = null;  
	    CallableStatement callStmt = null; 
		 try { 
	            prop.load(in); 
	            Class.forName(prop.getProperty("jdbc.driverClassName"));  
	            conn = DriverManager.getConnection(prop.getProperty("jdbc.url"), prop.getProperty("jdbc.username"), prop.getProperty("jdbc.password"));  
	            callStmt = conn.prepareCall("{call "+name+"(?)}");
	            callStmt.setString(1, productSN);
	            callStmt.execute();
		 }catch(Exception e){
			 e.printStackTrace(System.out); 
		 }finally{
			 if (null != callStmt) {  
	               try {
					callStmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}  
	           }  
	           if (null != conn) {  
	               try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}  
	           }  
		 }
		 long time2=System.currentTimeMillis();
		 System.out.println("-------  end 调用存储过程:"+ System.currentTimeMillis()); 
		 System.out.println("-------  调用存储过程耗时:"+ (time2-time1)); 
	}
	
	public static void call(String name){
		long time1=System.currentTimeMillis();
		System.out.println("-------  start 调用存储过程:"+name+"====="+ System.currentTimeMillis()); 
		
		Properties prop = new Properties(); 
		InputStream in=null;
		in =  FileUtil.getResourceAsStream("config/jdbc.properties"); 
	    Connection conn = null;  
	    CallableStatement callStmt = null; 
		 try { 
	            prop.load(in); 
	            Class.forName(prop.getProperty("jdbc.driverClassName"));  
	            conn = DriverManager.getConnection(prop.getProperty("jdbc.url"), prop.getProperty("jdbc.username"), prop.getProperty("jdbc.password"));  
	            callStmt = conn.prepareCall("{call "+name+"()}");  
	            callStmt.execute();
		 }catch(Exception e){
			 e.printStackTrace(System.out); 
		 }finally{
			 if (null != callStmt) {  
	               try {
					callStmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}  
	           }  
	           if (null != conn) {  
	               try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}  
	           }  
		 }
		 long time2=System.currentTimeMillis();
		 System.out.println("-------  end 调用存储过程:"+ System.currentTimeMillis()); 
		 System.out.println("-------  调用存储过程耗时:"+ (time2-time1)); 
	}
	
	
	
	public static void main(String[] args) {
		CallProcedureUtil.call("pro_InitProductMinsaleSearch");
	}

}
