/**
*/ 
package com.cheriscon.backstage.market.action;

/**
* @ClassName: StringSameCount
* @Description: TODO
* @author shaohu
* @date 2013-3-31 下午04:17:28
* 
*/
import java.util.HashMap;


public class StringSameCount {
    private HashMap map;
    private int counter;   //用于统计 map中的value
    
    StringSameCount()
    {
        map=new HashMap<String,Integer>();
    }
    
    /**
     * 用于在hashmap中插入字符串
     * @param string
     */
    public void hashInsert(String string){
        if (map.containsKey(string)){
            counter=(Integer)map.get(string);
            map.put(string,++counter);
        }
        else{
            map.put(string, 1);
        }
    }
    
    public HashMap getHashMap(){
        return map;
    }
}
