/**
 */
package com.cheriscon.backstage.market.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.market.service.IMinSaleUnitPriceService;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitPrice;
import com.cheriscon.framework.action.WWAction;

/**
 * @ClassName: MinSaleUnitPriceAction
 * @Description: 最小销售单位价格管理控制类
 * @author shaohu
 * @date 2013-1-16 下午04:52:38
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/market")
public class MinSaleUnitPriceAction extends WWAction {

	private static final long serialVersionUID = 1541696846154680065L;

	@Resource
	private IMinSaleUnitPriceService priceService;

	private MinSaleUnit minSaleUnit;

	private List<MinSaleUnitPrice> prices;

	private List<MinSaleUnitPrice> delPrices;

	public IMinSaleUnitPriceService getPriceService() {
		return priceService;
	}

	public void setPriceService(IMinSaleUnitPriceService priceService) {
		this.priceService = priceService;
	}

	public MinSaleUnit getMinSaleUnit() {
		return minSaleUnit;
	}

	public void setMinSaleUnit(MinSaleUnit minSaleUnit) {
		this.minSaleUnit = minSaleUnit;
	}

	public List<MinSaleUnitPrice> getPrices() {
		return prices;
	}

	public void setPrices(List<MinSaleUnitPrice> prices) {
		this.prices = prices;
	}

	public List<MinSaleUnitPrice> getDelPrices() {
		return delPrices;
	}

	public void setDelPrices(List<MinSaleUnitPrice> delPrices) {
		this.delPrices = delPrices;
	}

	@SuppressWarnings("unchecked")
	@Action(value = "updateMinSaleUnitPrice")
	public String updateMinSaleUnitPrice() throws Exception {
		try {
			this.priceService.batchUpdateMinSaleUnitPrice(prices,delPrices);
			this.msgs.add(getText("market.mainsale.message4"));
//			this.urls.put(
//					getText("market.mainsale.message5"),
//					"toMinSaleUnitPrice.do?minSaleUnit.code="
//							+ minSaleUnit.getCode());
			this.urls.put(getText("common.btn.return"),"toMinSaleUnitPage.do");
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(
					getText("common.btn.return"),
					"toMinSaleUnitPrice.do?minSaleUnit.code="
							+ minSaleUnit.getCode());
//			this.urls.put(getText("common.btn.return"),"toMinSaleUnitPage.do");
		}
		return MESSAGE;
	}
}
