/**
*/ 
package com.cheriscon.backstage.market.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.market.service.IMinSalesUnitMemoService;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.framework.action.WWAction;

/**
 * @ClassName: MinSaleUnitMemoAction
 * @Description: 最小销售单位说明控制类
 * @author shaohu
 * @date 2013-1-16 下午01:51:00
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/market")
public class MinSaleUnitMemoAction extends WWAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7993812658978439597L;

	@Resource
	private IMinSalesUnitMemoService minSalesUnitMemoService;
	
	private MinSaleUnit minSaleUnit;
	
	private List<MinSaleUnitMemo> minSaleUnitMemos;
	
	/**
	 * 需删除的最小销售单位说明
	 */
	private List<MinSaleUnitMemo> delMinSaleUnitMemos;
	
	public MinSaleUnit getMinSaleUnit() {
		return minSaleUnit;
	}

	public void setMinSaleUnit(MinSaleUnit minSaleUnit) {
		this.minSaleUnit = minSaleUnit;
	}

	public List<MinSaleUnitMemo> getMinSaleUnitMemos() {
		return minSaleUnitMemos;
	}

	public void setMinSaleUnitMemos(List<MinSaleUnitMemo> minSaleUnitMemos) {
		this.minSaleUnitMemos = minSaleUnitMemos;
	}
	
	public List<MinSaleUnitMemo> getDelMinSaleUnitMemos() {
		return delMinSaleUnitMemos;
	}

	public void setDelMinSaleUnitMemos(List<MinSaleUnitMemo> delMinSaleUnitMemos) {
		this.delMinSaleUnitMemos = delMinSaleUnitMemos;
	}

	/**
	 * 
	* @Title: updateMinSaleUnitMemo
	* @Description: 修改最小销售单位说明数据
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "updateMinSaleUnitMemo")
	public String updateMinSaleUnitMemo() throws Exception {
		try {
			minSalesUnitMemoService.updateMinSaleUnitMemo(minSaleUnitMemos,
					delMinSaleUnitMemos);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("common.btn.return"),"toMinSaleUnitPage.do");
//			this.urls.put(getText("market.mainsale.message5"),"toMinSaleUnitMemo.do?minSaleUnit.code="+minSaleUnit.getCode());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"toMinSaleUnitMemo.do?minSaleUnit.code="+minSaleUnit.getCode());
//			this.urls.put(getText("common.btn.return"),"toMinSaleUnitPage.do");
		}
		return MESSAGE;
	}

}
