/**
*/ 
package com.cheriscon.backstage.market.action;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.AuthAction;
import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.service.ISaleConfigMemoService;
import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.backstage.market.vo.MinSaleUnitConfigVo;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.market.vo.SaleConfigSearch;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.MinSaleUnitSaleCfgDetail;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.common.model.SaleConfigMemo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * @ClassName: SaleConfigAction
 * @Description: 销售配置控制类
 * @author shaohu
 * @date 2013-1-17 下午01:58:15
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/market")
public class SaleConfigAction extends WWAction {

	private static final long serialVersionUID = 5696331645305458444L;
	
	
	@Resource
	private IProductTypeService productTypeService;
	
	@Resource
	private ISaleConfigService configService;
	
	@Resource
	private ILanguageService languageService;
	
	@Resource
	private ISaleConfigMemoService saleConfigMemoService;
	
	@Resource
	private IProductForSealerService productForSealerService;
	
	@Resource
	private IAdminUserManager adminUserManager;
	
	private SaleConfigSearch configSearch;
	
	private ProductType productType;
	
	private ProductForSealer productTypeForSealer;
	

	//private List<ProductType> productTypes;
	private List<ProductForSealer> productTypes;
	
	private List<SaleConfigMemo> saleConfigMemos;
	
	private List<Language> languages;
	
	private List<MinSaleUnitConfigVo> configVos;
	
	private List<MinSaleUnitConfigVo> minSaleUnitConfigVos;;
	
	private SaleConfig saleConfig;
	
	private List<MinSaleUnitSaleCfgDetail> details;
	
	private List<MinSaleUnitSaleCfgDetail> delDetails;
	
	private String areas ="";
	
	private String adminFlag="N";   //是否是管理员
	
	public String getAdminFlag() {
		return adminFlag;
	}

	public void setAdminFlag(String adminFlag) {
		this.adminFlag = adminFlag;
	}

	public SaleConfigSearch getConfigSearch() {
		return configSearch;
	}

	public void setConfigSearch(SaleConfigSearch configSearch) {
		this.configSearch = configSearch;
	}

	public List<ProductForSealer> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductForSealer> productTypes) {
		this.productTypes = productTypes;
	}

	public List<MinSaleUnitConfigVo> getConfigVos() {
		return configVos;
	}

	public void setConfigVos(List<MinSaleUnitConfigVo> configVos) {
		this.configVos = configVos;
	}

	public SaleConfig getSaleConfig() {
		return saleConfig;
	}

	public void setSaleConfig(SaleConfig saleConfig) {
		this.saleConfig = saleConfig;
	}

	public List<MinSaleUnitSaleCfgDetail> getDetails() {
		return details;
	}

	public void setDetails(List<MinSaleUnitSaleCfgDetail> details) {
		this.details = details;
	}

	public List<MinSaleUnitConfigVo> getMinSaleUnitConfigVos() {
		return minSaleUnitConfigVos;
	}

	public void setMinSaleUnitConfigVos(
			List<MinSaleUnitConfigVo> minSaleUnitConfigVos) {
		this.minSaleUnitConfigVos = minSaleUnitConfigVos;
	}

	public String getAreas() {
		return areas;
	}

	public void setAreas(String areas) {
		this.areas = areas;
	}

	public List<MinSaleUnitSaleCfgDetail> getDelDetails() {
		return delDetails;
	}

	public void setDelDetails(List<MinSaleUnitSaleCfgDetail> delDetails) {
		this.delDetails = delDetails;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public List<SaleConfigMemo> getSaleConfigMemos() {
		return saleConfigMemos;
	}

	public void setSaleConfigMemos(List<SaleConfigMemo> saleConfigMemos) {
		this.saleConfigMemos = saleConfigMemos;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}
	public ProductForSealer getProductTypeForSealer() {
		return productTypeForSealer;
	}

	public void setProductTypeForSealer(ProductForSealer productTypeForSealer) {
		this.productTypeForSealer = productTypeForSealer;
	}

	/**
	 * 
	* @Title: toSaleConfig
	* @Description: 跳转到销售配置列表页
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toSaleConfig", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleConfig_list.jsp") })
	public String toSaleConfig() throws Exception{
		if(null == configSearch){
			configSearch = new SaleConfigSearch();
		}
		AdminUser user = adminUserManager.getCurrentUser();
		List<AuthAction> list=user.getAuthList();   //网站资料录入  //
		for(int i=0;i<list.size();i++){
			AuthAction a=list.get(i);
			if(a.getName().equals("管理员")){
				adminFlag="Y";
			}
				
		}
		//this.productTypes = productTypeService.listAll();
		this.productTypes=productForSealerService.getProductForSealerList();
		this.webpage = configService.pageSaleConfig(configSearch, this.getPage(), this.getPageSize());
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: toAddSaleConfig
	* @Description: 跳转到添加页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toAddSaleConfig", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleConfig_add.jsp") })
	public String toAddSaleConfig() throws Exception{
	//	this.productTypes = this.productTypeService.listAll();
		this.productTypes=productForSealerService.getProductForSealerList();
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		this.configVos = this.configService.getMinSaleUnitConfigList(language.getCode());
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: addSaleConfig
	* @Description: 添加销售配置
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "addSaleConfig")
	public String addSaleConfig() throws Exception{
		try{
			this.configService.addSaleConfig(saleConfig, details);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("请点击添加销售配置说明"),"toSaleConfigMemo.do?saleConfig.code="+saleConfig.getCode());
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"toAddSaleConfig.do");
		}
		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: delSaleConfig
	* @Description: 删除销售配置
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "delSaleConfig", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleConfig_list.jsp") })
	public String delSaleConfig() throws Exception{
		try{
			this.configService.delSaleConfigByCode(saleConfig.getCode().trim());
			this.msgs.add(getText("common.js.del"));
			this.urls.put(getText("market.mainsale.message5"),"toSaleConfig.do");
		}catch(Exception e){
			this.msgs.add(getText("common.js.nonDel"));
			this.urls.put(getText("market.mainsale.message5"),"toSaleConfig.do");
		}
		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: toSaleConfigMemo
	* @Description: 跳到说明管理页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toSaleConfigMemo", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleConfigMemo_list.jsp") })
	public String toSaleConfigMemo() throws Exception{
		this.saleConfig = this.configService.getSaleConfigByCode(this.saleConfig.getCode().trim());
	//	this.productType = this.productTypeService.getProductTypeByCode(this.saleConfig.getProTypeCode());
	    this.productTypeForSealer=this.productForSealerService.getProductForSealerByCode(this.saleConfig.getProTypeCode());
		this.saleConfigMemos = this.saleConfigMemoService.getSaleConfigMemoListByUnitCode(this.saleConfig.getCode());
		this.languages = this.saleConfigMemoService.getAvailableLanguage(this.saleConfig.getCode());
		return SUCCESS;
	}
	
	
	/**
	 * 
	* @Title: toAddSaleConfig
	* @Description: 跳转到添加页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toViewSaleConfig", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleConfig_view.jsp") })
	public String toViewSaleConfig() throws Exception{
		//this.productTypes = this.productTypeService.listAll();
		this.productTypes=productForSealerService.getProductForSealerList();
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		this.saleConfig = this.configService.getSaleConfigByCode(saleConfig.getCode().trim());
		//this.configVos = this.configService.getAvailableMinSaleUnitConfigList(saleConfig.getCode().trim(),language.getCode(),this.saleConfig.getProTypeCode());
		this.configVos = this.configService.getMinSaleUnitConfigList(language.getCode());
		this.minSaleUnitConfigVos = this.configService.getSaleConfigUnit(saleConfig.getCode().trim(), language.getCode());
		this.findAreas();
		//		if(null != minSaleUnitConfigVos && minSaleUnitConfigVos.size() > 0){
//			//区域配置信息
//			this.areas = this.minSaleUnitConfigVos.get(0).getAreaCfgCode();
//		}
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: toAddSaleConfig
	* @Description: 跳转到添加页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toUpdateSaleConfig", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleConfig_update.jsp") })
	public String toUpdateSaleConfig() throws Exception{
		//this.productTypes = this.productTypeService.listAll();
		this.productTypes=productForSealerService.getProductForSealerList();
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		this.saleConfig = this.configService.getSaleConfigByCode(saleConfig.getCode().trim());
		//this.configVos = this.configService.getAvailableMinSaleUnitConfigList(saleConfig.getCode().trim(),language.getCode(),this.saleConfig.getProTypeCode());
		this.configVos = this.configService.getMinSaleUnitConfigList(language.getCode());
		this.minSaleUnitConfigVos = this.configService.getSaleConfigUnit(saleConfig.getCode().trim(), language.getCode());
		this.findAreas();
		//		if(null != minSaleUnitConfigVos && minSaleUnitConfigVos.size() > 0){
//			//区域配置信息
//			this.areas = this.minSaleUnitConfigVos.get(0).getAreaCfgCode();
//		}
		return SUCCESS;
	}
	

	/**
	 * 
	* @Title: showSaleConfig
	* @Description: 销售契约详情页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "showSaleConfig", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleConfig_show.jsp") })
	public String showSaleConfig() throws Exception{
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		this.saleConfig = this.configService.getSaleConfigDetailByCode(saleConfig.getCode().trim());
		this.minSaleUnitConfigVos = this.configService.getSaleConfigUnit(saleConfig.getCode().trim(), language.getCode());
		this.saleConfigMemos = this.saleConfigMemoService.getSaleConfigMemoListByUnitCode(saleConfig.getCode().trim());
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "updateSaleConfig")
	public String updateSaleConfig() throws Exception{
		try{
			this.configService.updateSaleConfig(saleConfig, details,delDetails);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("market.mainsale.message5"),"toSaleConfig.do");
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"updateSaleConfig.do?saleConfig.code="+saleConfig.getCode().trim());
		}
		return MESSAGE;
	}
	
	@SuppressWarnings("rawtypes")
	public void findAreas(){
		
		StringSameCount temp = new StringSameCount();
		
//		Map<String,String> map = new HashMap<String,String>();
		if(null != minSaleUnitConfigVos && minSaleUnitConfigVos.size() > 0){
			for(MinSaleUnitConfigVo v : minSaleUnitConfigVos){
//				map.put(v.getAreaCfgCode(), v.getAreaCfgCode());
				String t = v.getAreaCfgCode();
				String [] arr  = t.trim().split(",");
				int len = arr.length;
				for(int i = 0 ; i <len ; i++ ){
					if(StringUtils.isNotBlank(arr[i])){
						temp.hashInsert(arr[i]);
					}
				}
			}
			
		}
		
		HashMap map = temp.getHashMap();
		if(map.size()>0){
		Iterator it = map.keySet().iterator();

		Map<Integer, String> tmap = new HashMap<Integer, String>();

		while (it.hasNext()) {
			String value = (String) it.next();
			Integer count = Integer.parseInt(String.valueOf(map.get(value)).trim());
			if (tmap.containsKey(count)) {
				tmap.put(count, tmap.get(count) + " " + value);
			} else {
				tmap.put(count, value);
			}
		}
		
		int t [] = new int[tmap.size()];
		
		Set<Integer> keys = tmap.keySet();
		int v = 0;
		for(Integer e : keys){
			t[v] = e;
		}
		int max=maxElement(t);
		
		this.areas = tmap.get(max).toString().trim().replaceAll(" ", ",");
		}
//		
//		Set<String> keys = map.keySet();
//		for(String key : keys){
//			
//		}
		
	}
	
	public static int maxElement(int a[]) {
		if(a.length == 1){
			return a[0];
		}
		int i;
		int temp = a[0];
		int max = 0;
		for (i = 1; i < a.length; i++) {
			if (temp > a[i]) {
				max = temp;
			} else {
				max = a[i];
			}
			temp = max;
		}
		return max;
	}
}
