/**
*/ 
package com.cheriscon.backstage.market.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.market.service.IMinSaleUnitSoftwareDetailService;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitSoftwareDetail;
import com.cheriscon.framework.action.WWAction;

/**
 * @ClassName: MinSaleUnitSoftwareDetailAction
 * @Description: 最小销售单位软件管理控制类
 * @author shaohu
 * @date 2013-1-16 下午02:43:52
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/market")
public class MinSaleUnitSoftwareDetailAction extends WWAction{

	private static final long serialVersionUID = -5947726041316245409L;
	
	@Resource
	private IMinSaleUnitSoftwareDetailService detailService;
	
	private MinSaleUnit minSaleUnit;
	
	private List<MinSaleUnitSoftwareDetail> details;
	
	private List<MinSaleUnitSoftwareDetail> delDetails;
	
	public MinSaleUnit getMinSaleUnit() {
		return minSaleUnit;
	}

	public void setMinSaleUnit(MinSaleUnit minSaleUnit) {
		this.minSaleUnit = minSaleUnit;
	}

	public List<MinSaleUnitSoftwareDetail> getDetails() {
		return details;
	}

	public void setDetails(List<MinSaleUnitSoftwareDetail> details) {
		this.details = details;
	}

	public List<MinSaleUnitSoftwareDetail> getDelDetails() {
		return delDetails;
	}

	public void setDelDetails(List<MinSaleUnitSoftwareDetail> delDetails) {
		this.delDetails = delDetails;
	}

	@SuppressWarnings("unchecked")
	@Action(value = "updateMinSaleUnitSoftwareDetail")
	public String updateMinSaleUnitSoftwareDetail() throws Exception {
		try {
			this.detailService.batchDetail(details, delDetails);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("common.btn.return"),"toMinSaleUnitPage.do");
//			this.urls.put(getText("market.mainsale.message5"),"toMinSaleUnitSoftwareDetail.do?minSaleUnit.code="+minSaleUnit.getCode());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"toMinSaleUnitSoftwareDetail.do?minSaleUnit.code="+minSaleUnit.getCode());
//			this.urls.put(getText("common.btn.return"),"toMinSaleUnitPage.do");
		}
		return MESSAGE;
	}

}
