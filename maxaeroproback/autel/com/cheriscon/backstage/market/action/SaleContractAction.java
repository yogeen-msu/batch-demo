/**
*/ 
package com.cheriscon.backstage.market.action;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.AuthAction;
import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.market.vo.SaleConfigSearch;
import com.cheriscon.backstage.market.vo.SaleConfigVo;
import com.cheriscon.backstage.market.vo.SaleContractVo;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.service.ISealerTypeService;
import com.cheriscon.backstage.member.vo.SealerInfoVo;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.vo.ProductTypeVo;
import com.cheriscon.backstage.system.service.IAreaConfigService;
import com.cheriscon.backstage.system.service.ILanguageConfigService;
import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.LanguageConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.SealerType;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;

/**
 * @ClassName: SaleContractAction
 * @Description: 销售契约控制类
 * @author shaohu
 * @date 2013-1-17 上午10:27:56
 *
 */
@ParentPackage("json_default")
@Namespace("/autel/market")
public class SaleContractAction extends WWAction{

	private static final long serialVersionUID = -3681725000694946060L;
	
	@Resource
	private ISaleContractService saleContractService;
	
	@Resource
	private IProductTypeService productTypeService;
	
	@Resource
	private ISaleConfigService saleConfigService;
	
	@Resource
	private ILanguageConfigService languageConfigService;
	
	@Resource
	private IAreaConfigService areaConfigService;
	
	@Resource
	private ISealerTypeService sealerTypeService;;
	
	@Resource
	private ISealerInfoService sealerInfoService;
	
	@Resource
	private ISaleConfigService configService;
	@Resource
	private IAdminUserManager adminUserManager;
	
	@Resource
	private IProductForSealerService productForSealerService;
	
	private SaleConfigSearch configSearch;
	
	private SaleContract saleContract;
	
	private List<ProductForSealer> productTypes;
	
	private List<SaleConfigVo> saleConfigs;
	
	private List<LanguageConfig> languageConfigs;
	
	private List<AreaConfig> areaConfigs;
	
	private SaleContractVo saleContractvo;
	
	private List<SealerInfo> sealerInfos;
	
	private List<SealerType> sealerTypes;
	
//	private ProductTypeVo productTypeVo;
	private ProductForSealer productTypeVo;
	
	private SealerInfoVo sealerInfoVoSel;
	
    private String adminFlag="N";   //是否是管理员
    
    private String oldSealerAutelId;
    private String newSealerAutelId;
	
	

	public String getOldSealerAutelId() {
		return oldSealerAutelId;
	}

	public String getNewSealerAutelId() {
		return newSealerAutelId;
	}

	public void setOldSealerAutelId(String oldSealerAutelId) {
		this.oldSealerAutelId = oldSealerAutelId;
	}

	public void setNewSealerAutelId(String newSealerAutelId) {
		this.newSealerAutelId = newSealerAutelId;
	}

	public String getAdminFlag() {
		return adminFlag;
	}

	public void setAdminFlag(String adminFlag) {
		this.adminFlag = adminFlag;
	}

	public SaleContractVo getSaleContractvo() {
		return saleContractvo;
	}

	public void setSaleContractvo(SaleContractVo saleContractvo) {
		this.saleContractvo = saleContractvo;
	}

	public List<ProductForSealer> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductForSealer> productTypes) {
		this.productTypes = productTypes;
	}

	public List<SaleConfigVo> getSaleConfigs() {
		return saleConfigs;
	}

	public void setSaleConfigs(List<SaleConfigVo> saleConfigs) {
		this.saleConfigs = saleConfigs;
	}

	public List<LanguageConfig> getLanguageConfigs() {
		return languageConfigs;
	}

	public void setLanguageConfigs(List<LanguageConfig> languageConfigs) {
		this.languageConfigs = languageConfigs;
	}

	public List<AreaConfig> getAreaConfigs() {
		return areaConfigs;
	}

	public void setAreaConfigs(List<AreaConfig> areaConfigs) {
		this.areaConfigs = areaConfigs;
	}

	public List<SealerInfo> getSealerInfos() {
		return sealerInfos;
	}

	public void setSealerInfos(List<SealerInfo> sealerInfos) {
		this.sealerInfos = sealerInfos;
	}

	public List<SealerType> getSealerTypes() {
		return sealerTypes;
	}
	/**
	 * @return the productTypeVo
	 */
	public ProductForSealer getProductTypeVo() {
		return productTypeVo;
	}

	/**
	 * @param productTypeVo the productTypeVo to set
	 */
	public void setProductTypeVo(ProductForSealer productTypeVo) {
		this.productTypeVo = productTypeVo;
	}

	public void setSealerTypes(List<SealerType> sealerTypes) {
		this.sealerTypes = sealerTypes;
	}

	public SaleContract getSaleContract() {
		return saleContract;
	}

	public void setSaleContract(SaleContract saleContract) {
		this.saleContract = saleContract;
	}

	/**
	 * @return the configSearch
	 */
	public SaleConfigSearch getConfigSearch() {
		return configSearch;
	}

	/**
	 * @param configSearch the configSearch to set
	 */
	public void setConfigSearch(SaleConfigSearch configSearch) {
		this.configSearch = configSearch;
	}
	
	/**
	 * @return the sealerInfoVoSel
	 */
	public SealerInfoVo getSealerInfoVoSel() {
		return sealerInfoVoSel;
	}

	/**
	 * @param sealerInfoVoSel the sealerInfoVoSel to set
	 */
	public void setSealerInfoVoSel(SealerInfoVo sealerInfoVoSel) {
		this.sealerInfoVoSel = sealerInfoVoSel;
	}

	
	
	@Action(value = "toCopySalContract", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleContract_copy.jsp") })
	public String toCopySalContract() throws Exception{
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "saveCopySaleContract")
	public String saveCopySaleContract() throws Exception{
		try{
			SealerInfo oldSealer=sealerInfoService.getSealerByAutelId(oldSealerAutelId);
			if(oldSealer==null){
				this.msgs.add("老的经销商不存在");
				this.urls.put(getText("common.btn.return"),"javascript:history.go(-1);");
				return MESSAGE;
			}
			SealerInfo newSealer=sealerInfoService.getSealerByAutelId(newSealerAutelId);
			if(newSealer==null){
				this.msgs.add("新的经销商不存在");
				this.urls.put(getText("common.btn.return"),"javascript:history.go(-1);");
				return MESSAGE;
			}
			
			List<SaleContract> listOld=saleContractService.getContrctBySealerAutelId(oldSealerAutelId);
			int copyNum=0;
			int notCopyNum=0;
			
			for(int i=0 ;i<listOld.size();i++){
				SaleContract old=listOld.get(i);
				old.setSealerCode(newSealer.getCode());
				List<SaleContract> listNew=saleContractService.getSaleContract(old);
				if(listNew== null || listNew.size()==0){
					saleContract=new SaleContract();
					saleContract.setAreaCfgCode(old.getAreaCfgCode());
					saleContract.setContractDate(old.getContractDate());
					saleContract.setExpirationTime(old.getExpirationTime());
					saleContract.setLanguageCfgCode(old.getLanguageCfgCode());
					saleContract.setMaxSaleCfgCode(old.getMaxSaleCfgCode());
					saleContract.setName(old.getName().replace(oldSealerAutelId, newSealerAutelId));
					saleContract.setProTypeCode(old.getProTypeCode());
					saleContract.setReChargePrice(old.getReChargePrice());
					saleContract.setSaleCfgCode(old.getSaleCfgCode());
					saleContract.setSealerCode(newSealer.getCode());
					saleContract.setSealerTypeCode(old.getSealerTypeCode());
					saleContract.setUpMonth(old.getUpMonth());
					saleContract.setWarrantyMonth(old.getWarrantyMonth());
					
					/*新增添加人，添加人IP，添加时间*/
					AdminUser user = adminUserManager.getCurrentUser();
					user = adminUserManager.get(user.getUserid());
					saleContract.setCreateUser(user.getUsername());
					saleContract.setCreateTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
					saleContract.setCreateIP(FrontConstant.getIpAddr(getRequest()));
					this.saleContractService.addSaleContract(saleContract);
					copyNum++;
				}else{
					notCopyNum++;
				}
				
				
				
			}
		
			
			this.msgs.add("已成功复制"+copyNum+"条销售契约，已经存在"+notCopyNum+"条满足条件的契约");
			this.urls.put(getText("market.mainsale.message5"),"toSaleContractList.do");
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"javascript:history.go(-1);");
		}
		return MESSAGE;
	}
	
	
	/**
	 * 
	* @Title: toSaleContractList
	* @Description: 跳转到销售契约列表页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toSaleContractList", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleContract_list.jsp") })
	public String toSaleContractList() throws Exception{
		try{
			
			if( null == saleContractvo){
				saleContractvo = new SaleContractVo();
			}
			
			AdminUser user = adminUserManager.getCurrentUser();
			List<AuthAction> list=user.getAuthList();   //网站资料录入  //
			for(int i=0;i<list.size();i++){
				AuthAction a=list.get(i);
				if(a.getName().equals("管理员")){
					adminFlag="Y";
				}
					
			}
			
			//this.productTypes = productTypeService.listAll();
			this.productTypes=productForSealerService.getProductForSealerList();
			this.webpage = this.saleContractService.pageSaleContractDetail(saleContractvo, this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "toSaleContractListForUpdate", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleContract_list2.jsp") })
	public String toSaleContractListForUpdate() throws Exception{
		if( null == saleContractvo){
			saleContractvo = new SaleContractVo();
		}
		AdminUser user = adminUserManager.getCurrentUser();
		this.productTypes=productForSealerService.getProductForSealerList();
		this.webpage = this.saleContractService.pageSaleContractDetail2(saleContractvo,user.getUserid(), this.getPage(), this.getPageSize());
		return SUCCESS;
	}
	
	
	
	@Action(value = "toSearchPtDlg", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/sale_product_selet.jsp") })
	public String toSearchPtDlg() throws Exception{
		try {
			//this.webpage = productTypeService.searchPage(productTypeVo, this.getPage(), this.getPageSize());
			this.webpage =productForSealerService.pageProductForSealer(productTypeVo, this.getPage(), this.getPageSize());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	
	@Action(value = "toSelectSaleConfig", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/sale_config_select.jsp") })
	public String toSelectSaleConfig() throws Exception{
		try{
			
			if(null == configSearch){
				configSearch = new SaleConfigSearch();
			}
			this.webpage = configService.pageConfig(configSearch, this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value = "toSelectMaxSaleConfig", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/sale_maxconfig_select.jsp") })
	public String toSelectMaxSaleConfig() throws Exception{
		if(null == configSearch){
			configSearch = new SaleConfigSearch();
		}
		this.webpage = configService.pageConfig(configSearch, this.getPage(), this.getPageSize());
		return SUCCESS;
	}
	
	@Action(value = "toSelectSealerDlg", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/sale_sealer_select.jsp") })
	public String toSelectSealerDlg() throws Exception{
		try{
			this.webpage = sealerInfoService.pageSealerInfoVoPage2(sealerInfoVoSel,this.getPage(), this.getPageSize());
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	
	/**
	 * 
	* @Title: toAddSalContract
	* @Description: 跳转到添加销售契约页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toAddSalContract", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleContract_add.jsp") })
	public String toAddSalContract() throws Exception{
		//经销商类型
		this.sealerTypes = this.sealerTypeService.querySealerType();
		//经销商
		this.sealerInfos = this.sealerInfoService.querySealerInfo();
		//产品型号
		//this.productTypes = this.productTypeService.listAll();
		this.productTypes=productForSealerService.getProductForSealerList();
		//销售配置
		this.saleConfigs = this.saleConfigService.listAll();
		//语言配置
		this.languageConfigs = this.languageConfigService.listAll();
		//区域配置
		this.areaConfigs = this.areaConfigService.listAll();
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: addSalContract
	* @Description: 添加销售契约
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "addSaleContract")
	public String addSaleContract() throws Exception{
		try{
			
			boolean bool = this.saleContractService.checkRepeat(saleContract);
			if(bool){
				this.msgs.add(getText("marketman.salecontractman.warn"));
				this.urls.put(getText("common.btn.return"),"toSaleContractList.do");
				return MESSAGE;
			}
			/*新增添加人，添加人IP，添加时间*/
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			saleContract.setCreateUser(user.getUsername());
			saleContract.setCreateTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			saleContract.setCreateIP(FrontConstant.getIpAddr(getRequest()));
			
			
			this.saleContractService.addSaleContract(saleContract);
			this.msgs.add(getText("market.mainsale.message3"));
			this.urls.put(getText("market.mainsale.message5"),"toSaleContractList.do");
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"toAddSalContract.do");
		}
		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: delSaleContract
	* @Description: 删除销售契约
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "delSaleContract", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleContract_list.jsp") })
	public String delSaleContract() throws Exception{
		try{
			this.saleContractService.delSaleContract(saleContract);
			this.msgs.add(getText("common.js.del"));
			this.urls.put(getText("market.mainsale.message5"),"toSaleContractList.do");
		}catch(Exception e){
//			e.printStackTrace();
			this.msgs.add(getText("common.js.nonDel"));
			this.urls.put(getText("market.mainsale.message5"),"toSaleContractList.do");
		}
		return MESSAGE;
//		this.saleContractService.delSaleContract(saleContract);
//		if( null == saleContractvo){
//			saleContractvo = new SaleContractVo();
//		}
//		this.webpage = this.saleContractService.pageSaleContractDetail(saleContractvo, this.getPage(), this.getPageSize());
//		return SUCCESS;
	}
	

	/**
	 * 
	* @Title: toAddSalContract
	* @Description: 跳转到修改销售契约页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toUpdateSaleContract", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleContract_update.jsp") })
	public String toUpdateSaleContract() throws Exception{
		this.saleContract = this.saleContractService.getSaleContractByCode(saleContract.getCode().trim());
		//经销商类型
		this.sealerTypes = this.sealerTypeService.querySealerType();
		//经销商
		this.sealerInfos = this.sealerInfoService.querySealerInfo();
		//产品型号
		//this.productTypes = this.productTypeService.listAll();
		this.productTypes=productForSealerService.getProductForSealerList();
		//销售配置
		this.saleConfigs = this.saleConfigService.listAll();
		//语言配置
		this.languageConfigs = this.languageConfigService.listAll();
		//区域配置
		this.areaConfigs = this.areaConfigService.listAll();
		return SUCCESS;
	}
	
	
	/**
	 * 
	* @Title: toAddSalContract
	* @Description: 跳转到修改销售契约页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toViewSaleContract", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleContract_view.jsp") })
	public String toViewSaleContract() throws Exception{
		this.saleContract = this.saleContractService.getSaleContractByCode(saleContract.getCode().trim());
		//经销商类型
		this.sealerTypes = this.sealerTypeService.querySealerType();
		//经销商
		this.sealerInfos = this.sealerInfoService.querySealerInfo();
		//产品型号
		//this.productTypes = this.productTypeService.listAll();
		this.productTypes=productForSealerService.getProductForSealerList();
		//销售配置
		this.saleConfigs = this.saleConfigService.listAll();
		//语言配置
		this.languageConfigs = this.languageConfigService.listAll();
		//区域配置
		this.areaConfigs = this.areaConfigService.listAll();
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: addSalContract
	* @Description: 修改销售契约
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "updateSaleContract")
	public String updateSaleContract() throws Exception{
		try{
			
			boolean bool = this.saleContractService.checkRepeat(saleContract);
			if(bool){
				this.msgs.add(getText("marketman.salecontractman.warn"));
				this.urls.put(getText("common.btn.return"),"toUpdateSaleContract.do?saleContract.code="+saleContract.getCode());
				return MESSAGE;
			}
			/*新增最后修改人，最后修改时间*/
			AdminUser user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			saleContract.setUpdateLastUser(user.getUsername());
			saleContract.setUpdateTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			this.saleContractService.updateSaleContract(saleContract);
			this.msgs.add(getText("market.mainsale.message4"));
			/*this.urls.put(getText("market.mainsale.message5"),"toSaleContractList.do");*/
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			/*this.urls.put(getText("common.btn.return"),"toUpdateSaleContract.do?saleContract.code="+saleContract.getCode());*/
		}
		return MESSAGE;
	}
	

	/**
	 * 
	* @Title: showSaleContract
	* @Description: 销售契约详情页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "showSaleContract", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/saleContract_show.jsp") })
	public String showSaleContract() throws Exception{
		this.saleContractvo = this.saleContractService.getSaleContractDetail(saleContract.getCode().trim());
		return SUCCESS;
	}
}
