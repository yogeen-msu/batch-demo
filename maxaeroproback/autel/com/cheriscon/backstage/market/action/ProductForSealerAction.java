/**
 */
package com.cheriscon.backstage.market.action;


import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.market.vo.ProductForSealerConfig;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.vo.ProductTypeSerialPicVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.JSONHelpUtils;
import com.cheriscon.framework.action.WWAction;


/**
 * @ClassName: ProductForSealerAction
 * @Description: 外部产品管理
 * @author chenqichuan
 * @date 2013-8-24 下午02:38:03
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/sealserProduct")
public class ProductForSealerAction extends WWAction {

	private static final long serialVersionUID = -8182781630555532764L;
	private List<ProductType> productTypes;
	private String jsonData;
	private String jsonStr;
	
 
	private ProductForSealer product;
    private String code;
    private List<ProductTypeSerialPicVo> productTypeSerialPicVos;
    private List<Language> language;
    private List<ProductForSealerConfig> config;

	@Resource
	private IProductTypeService productTypeService;
	@Resource
	private IProductForSealerService productForSealerService;
	@Resource
	private ILanguageService  languageService;
	
	
	//进入列表页面
	@Action(value = "list", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/productForSealer_list.jsp") })
	public String list() throws Exception {
		this.productTypes = productTypeService.listAll();	
		this.webpage = productForSealerService.pageProductForSealer(product, this.getPage(), this.getPageSize());
		return SUCCESS;
	}
	
	@Action(value = "getLanguageJson", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getLanguageJson() throws Exception {
		jsonData = productForSealerService.getLanguageJson("");
		return SUCCESS;
	}
	
	//进入新增页面
	@Action(value = "add", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/productForSealer_add.jsp") })
	public String add() throws Exception {
		this.productTypes = productTypeService.listAll();		
		return SUCCESS;
	}
		
	//保存外部产品
	@Action(value = "save")
	public String save() throws Exception {
		try{
			String code = DBUtils.generateCode(DBConstant.PRODUCT_FOR_SEALER_CODE);
			product.setCode(code);
			this.productForSealerService.saveProductForSealer(product);
			this.msgs.add(getText("syssetting.toolman.urimapping.savesuccess"));
			this.urls.put(getText("market.mainsale.message5"),"list.do");
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"list.do");
		}
		return MESSAGE;
	}	
	
	//进入修改产品页面
	@Action(value = "edit", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/productForSealer_edit.jsp") })
	public String edit() throws Exception {
		this.productTypes = productTypeService.listAll();	
		product=productForSealerService.getProductForSealerByCode(code);
		config=productForSealerService.getProductForSealerCfgList(code);
		language=languageService.queryLanguage();
		productTypeSerialPicVos = productForSealerService
				.getProductTypeSerialList(product.getCode());
		
		return SUCCESS;
	}	
	
	//修改外部产品
	@Action(value = "update")
	public String update() throws Exception {
		try{
			this.productForSealerService.updateProductForSealer(product);
			this.msgs.add(getText("common.js.modSuccess"));
			this.urls.put(getText("market.mainsale.message5"),"list.do");
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"list.do");
		}
		return MESSAGE;
	}
	
	//删除外部产品
	@Action(value = "delete")
	public String delete() throws Exception {
		try{
			this.productForSealerService.delProductForSealer(code);
			this.msgs.add(getText("common.js.del"));
			this.urls.put(getText("market.mainsale.message5"),"list.do");
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("product.info.usingnotremove"));
			this.urls.put(getText("common.btn.return"),"list.do");
		}
		return MESSAGE;
	}
	public List<ProductType> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductType> productTypes) {
		this.productTypes = productTypes;
	}
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}
	public ProductForSealer getProduct() {
		return product;
	}

	public void setProduct(ProductForSealer product) {
		this.product = product;
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<ProductTypeSerialPicVo> getProductTypeSerialPicVos() {
		return productTypeSerialPicVos;
	}

	public void setProductTypeSerialPicVos(
			List<ProductTypeSerialPicVo> productTypeSerialPicVos) {
		this.productTypeSerialPicVos = productTypeSerialPicVos;
	}
	

	public List<Language> getLanguage() {
		return language;
	}

	public void setLanguage(List<Language> language) {
		this.language = language;
	}

	public List<ProductForSealerConfig> getConfig() {
		return config;
	}

	public void setConfig(List<ProductForSealerConfig> config) {
		this.config = config;
	}
}
