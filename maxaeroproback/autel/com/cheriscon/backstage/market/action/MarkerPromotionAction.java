/**
 */
package com.cheriscon.backstage.market.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.backstage.market.service.IMarkerPromotionAreaRuleService;
import com.cheriscon.backstage.market.service.IMarkerPromotionBaseInfoService;
import com.cheriscon.backstage.market.service.IMarkerPromotionDiscountRuleService;
import com.cheriscon.backstage.market.service.IMarkerPromotionRuleService;
import com.cheriscon.backstage.market.service.IMarkerPromotionSaleConfigService;
import com.cheriscon.backstage.market.service.IMarkerPromotionSoftwareRuleService;
import com.cheriscon.backstage.market.service.IMarkerPromotionTimesInfoService;
import com.cheriscon.backstage.market.service.IMinSaleUnitService;
import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.backstage.market.vo.MarkerPromotionSearch;
import com.cheriscon.backstage.market.vo.MinSaleUnitVo;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.IAreaConfigService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.MarkerPromotionAreaRule;
import com.cheriscon.common.model.MarkerPromotionBaseInfo;
import com.cheriscon.common.model.MarkerPromotionDiscountRule;
import com.cheriscon.common.model.MarkerPromotionRule;
import com.cheriscon.common.model.MarkerPromotionSaleConfig;
import com.cheriscon.common.model.MarkerPromotionSoftwareRule;
import com.cheriscon.common.model.MarkerPromotionTimesInfo;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * @ClassName: MarkerPromotionAction
 * @Description: 营销活动控制类
 * @author shaohu
 * @date 2013-1-19 下午02:38:03
 * 
 */
@ParentPackage("json_default")
@Namespace("/core/admin")
public class MarkerPromotionAction extends WWAction {

	private static final long serialVersionUID = -8182781630555532764L;

	private IAdminUserManager adminUserManager;

	private AdminUser user;

	@Resource
	private IMarkerPromotionBaseInfoService baseInfoService;

	@Resource
	private IMarkerPromotionRuleService ruleService;

	@Resource
	private IMarkerPromotionDiscountRuleService discountRuleService;

	@Resource
	private IMarkerPromotionAreaRuleService areaRuleService;

	@Resource
	private IMarkerPromotionSoftwareRuleService softwareRuleService;

	@Resource
	private IMarkerPromotionTimesInfoService timesInfoService;

	@Resource
	private IMinSaleUnitService minSaleUnitService;

	@Resource
	private IAreaConfigService areaConfigService;

	@Resource
	private ISaleConfigService saleConfigService;

	@Resource
	private IProductTypeService productTypeService;

	@Resource
	private IMarkerPromotionSaleConfigService promotionSaleConfigService;
	
	@Resource
	private ILanguageService languageService;

	private List<ProductType> productTypes;

	private List<MinSaleUnitVo> minSaleUnitVos;

	private List<AreaConfig> areaConfigs;

	/**
	 * 基础信息
	 */
	private MarkerPromotionBaseInfo baseInfo;

	/**
	 * 区域配置
	 */
	private List<MarkerPromotionAreaRule> areaRules;

	private List<MarkerPromotionAreaRule> delAreaRules;

	private MarkerPromotionSearch search;

	/**
	 * 规则
	 */
	private MarkerPromotionRule rule;

	/**
	 * 规则条件
	 */
	private List<MarkerPromotionDiscountRule> discountRules;

	private List<MarkerPromotionDiscountRule> delDiscountRules;

	/**
	 * 参与软件
	 */
	private List<MarkerPromotionSoftwareRule> softwareRules;

	private List<MarkerPromotionSoftwareRule> delSoftwareRules;

	/**
	 * 时间段
	 */
	private List<MarkerPromotionTimesInfo> timesInfos;

	private List<MarkerPromotionTimesInfo> delTimesInfos;

	/**
	 * 活动销售配置
	 */
	private List<MarkerPromotionSaleConfig> promotionSaleConfigs;

	private List<MarkerPromotionSaleConfig> delPromotionSaleConfigs;

	/**
	 * 销售配置集合
	 */
	private List<SaleConfig> saleConfigs;

	private String promotionCodes;

	private Integer display = 0;

	/**
	 * @return the display
	 */
	public Integer getDisplay() {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(Integer display) {
		this.display = display;
	}

	public List<ProductType> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductType> productTypes) {
		this.productTypes = productTypes;
	}

	public List<MinSaleUnitVo> getMinSaleUnitVos() {
		return minSaleUnitVos;
	}

	public void setMinSaleUnitVos(List<MinSaleUnitVo> minSaleUnitVos) {
		this.minSaleUnitVos = minSaleUnitVos;
	}

	public List<AreaConfig> getAreaConfigs() {
		return areaConfigs;
	}

	public void setAreaConfigs(List<AreaConfig> areaConfigs) {
		this.areaConfigs = areaConfigs;
	}

	public MarkerPromotionBaseInfo getBaseInfo() {
		return baseInfo;
	}

	public void setBaseInfo(MarkerPromotionBaseInfo baseInfo) {
		this.baseInfo = baseInfo;
	}

	public MarkerPromotionRule getRule() {
		return rule;
	}

	public void setRule(MarkerPromotionRule rule) {
		this.rule = rule;
	}

	public List<MarkerPromotionAreaRule> getAreaRules() {
		return areaRules;
	}

	public void setAreaRules(List<MarkerPromotionAreaRule> areaRules) {
		this.areaRules = areaRules;
	}

	public List<MarkerPromotionDiscountRule> getDiscountRules() {
		return discountRules;
	}

	public void setDiscountRules(List<MarkerPromotionDiscountRule> discountRules) {
		this.discountRules = discountRules;
	}

	public List<MarkerPromotionSoftwareRule> getSoftwareRules() {
		return softwareRules;
	}

	public void setSoftwareRules(List<MarkerPromotionSoftwareRule> softwareRules) {
		this.softwareRules = softwareRules;
	}

	public List<MarkerPromotionTimesInfo> getTimesInfos() {
		return timesInfos;
	}

	public void setTimesInfos(List<MarkerPromotionTimesInfo> timesInfos) {
		this.timesInfos = timesInfos;
	}

	public MarkerPromotionSearch getSearch() {
		return search;
	}

	public void setSearch(MarkerPromotionSearch search) {
		this.search = search;
	}

	public List<MarkerPromotionAreaRule> getDelAreaRules() {
		return delAreaRules;
	}

	public void setDelAreaRules(List<MarkerPromotionAreaRule> delAreaRules) {
		this.delAreaRules = delAreaRules;
	}

	public List<MarkerPromotionDiscountRule> getDelDiscountRules() {
		return delDiscountRules;
	}

	public void setDelDiscountRules(List<MarkerPromotionDiscountRule> delDiscountRules) {
		this.delDiscountRules = delDiscountRules;
	}

	public List<MarkerPromotionSoftwareRule> getDelSoftwareRules() {
		return delSoftwareRules;
	}

	public void setDelSoftwareRules(List<MarkerPromotionSoftwareRule> delSoftwareRules) {
		this.delSoftwareRules = delSoftwareRules;
	}

	public List<MarkerPromotionTimesInfo> getDelTimesInfos() {
		return delTimesInfos;
	}

	public void setDelTimesInfos(List<MarkerPromotionTimesInfo> delTimesInfos) {
		this.delTimesInfos = delTimesInfos;
	}

	public List<MarkerPromotionSaleConfig> getPromotionSaleConfigs() {
		return promotionSaleConfigs;
	}

	public void setPromotionSaleConfigs(List<MarkerPromotionSaleConfig> promotionSaleConfigs) {
		this.promotionSaleConfigs = promotionSaleConfigs;
	}

	public List<MarkerPromotionSaleConfig> getDelPromotionSaleConfigs() {
		return delPromotionSaleConfigs;
	}

	public void setDelPromotionSaleConfigs(List<MarkerPromotionSaleConfig> delPromotionSaleConfigs) {
		this.delPromotionSaleConfigs = delPromotionSaleConfigs;
	}

	public List<SaleConfig> getSaleConfigs() {
		return saleConfigs;
	}

	public void setSaleConfigs(List<SaleConfig> saleConfigs) {
		this.saleConfigs = saleConfigs;
	}

	public String getPromotionCodes() {
		return promotionCodes;
	}

	public void setPromotionCodes(String promotionCodes) {
		this.promotionCodes = promotionCodes;
	}

	/**
	 * 
	 * @Title: toMarkerPromotion
	 * @Description: 跳转到营销活动限时购买列表页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toMarkerPromotion", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/markerPromotion_list.jsp") })
	public String toMarkerPromotion() throws Exception {
		if (null == search) {
			search = new MarkerPromotionSearch();
		}
		this.webpage = this.baseInfoService.pageMarkerPromotion(search, this.getPage(), this.getPageSize());
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: toAddEmption
	 * @Description: 添加限时抢购页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toAddEmption", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/emption_add.jsp") })
	public String toAddEmption() throws Exception {
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		// 产品型号集合
	//	this.productTypes = this.productTypeService.listAll();
		// 最小销售单位list
		this.minSaleUnitVos = this.minSaleUnitService.listAll(language.getCode());
		// 区域配置list
		this.areaConfigs = this.areaConfigService.listAll();
		// 销售配置list
		this.saleConfigs = this.saleConfigService.querySaleConfig();
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: addPromotion
	 * @Description: 添加活动
	 * @param
	 * @return String
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "addPromotion")
	public String addPromotion() throws Exception {
		try {
			adminUserManager = SpringContextHolder.getBean("adminUserManager");
			user = adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			// 创建人
			baseInfo.setCreator(user.getUsername());
			this.baseInfoService.addPromotion(baseInfo, rule, discountRules, timesInfos, softwareRules, areaRules, promotionSaleConfigs);
			this.msgs.add(getText("market.mainsale.message3"));
			this.urls.put(getText("market.mainsale.message5"), "toMarkerPromotion.do?search.promotionType=" + search.getPromotionType());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toAddEmption.do?search.promotionType=" + search.getPromotionType());
		}
		return MESSAGE;
	}

	/**
	 * 
	 * @Title: toUpdateEmption
	 * @Description: 跳转到限时抢购更新页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toUpdateEmption", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/emption_update.jsp") })
	public String toUpdateEmption() throws Exception {
		this.baseInfo = this.baseInfoService.getMarkerPromotionBaseInfoByCode(this.baseInfo.getCode().trim());
		this.rule = this.ruleService.getMarkerPromotionRuleByPromotionCode(this.baseInfo.getCode());
		this.softwareRules = this.softwareRuleService.getSoftwareListByPromotionCode(this.baseInfo.getCode());
		this.timesInfos = this.timesInfoService.getTimesInfoListByPromotionCode(this.baseInfo.getCode());
		this.areaRules = this.areaRuleService.getPromotionAreaByPromotionCode(this.baseInfo.getCode());

		this.promotionSaleConfigs = this.promotionSaleConfigService.getSaleConfigByPromotionCode(this.baseInfo.getCode());

		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		minSaleUnitVos = this.softwareRuleService.getAdvMinSaleUnit(this.baseInfo.getCode(), language.getCode());
		areaConfigs = this.areaRuleService.getAdvAreaConfig(this.baseInfo.getCode());
		this.saleConfigs = this.promotionSaleConfigService.getAdvSaleConfig(this.baseInfo.getCode());
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	@Action(value = "updateEmption")
	public String updateEmption() throws Exception {
		try {
			this.baseInfoService.updatePromotion(baseInfo, rule, discountRules, delDiscountRules, timesInfos, delTimesInfos, softwareRules,
					delSoftwareRules, areaRules, delAreaRules, promotionSaleConfigs, delPromotionSaleConfigs);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("market.mainsale.message5"), "toMarkerPromotion.do?search.promotionType=" + search.getPromotionType());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toUpdateEmption.do?search.promotionType=" + search.getPromotionType());
		}
		return MESSAGE;
	}

	/**
	 * 
	 * @Title: toAddEmption
	 * @Description: 添加套餐页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toAddPackage", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/emptionPackage_add.jsp") })
	public String toAddPackage() throws Exception {
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		// 产品型号集合
		// productTypes = this.productTypeService.listAll();
		// 最小销售单位list
		minSaleUnitVos = minSaleUnitService.listAll(language.getCode());
		// 区域配置list
		areaConfigs = areaConfigService.listAll();

		// 销售配置list
		this.saleConfigs = this.saleConfigService.querySaleConfig();

		return SUCCESS;
	}

	/**
	 * 
	 * @Title: toUpdateEmption
	 * @Description: 跳转到套餐优惠更新页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toUpdatePackage", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/emptionPackage_update.jsp") })
	public String toUpdatePackage() throws Exception {
		this.baseInfo = this.baseInfoService.getMarkerPromotionBaseInfoByCode(this.baseInfo.getCode().trim());
		this.rule = this.ruleService.getMarkerPromotionRuleByPromotionCode(this.baseInfo.getCode());
		this.softwareRules = this.softwareRuleService.getSoftwareListByPromotionCode(this.baseInfo.getCode());
		this.timesInfos = this.timesInfoService.getTimesInfoListByPromotionCode(this.baseInfo.getCode());
		this.areaRules = this.areaRuleService.getPromotionAreaByPromotionCode(this.baseInfo.getCode());
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		minSaleUnitVos = this.softwareRuleService.getAdvMinSaleUnit(this.baseInfo.getCode(), language.getCode());
		areaConfigs = this.areaRuleService.getAdvAreaConfig(this.baseInfo.getCode());

		this.promotionSaleConfigs = this.promotionSaleConfigService.getSaleConfigByPromotionCode(this.baseInfo.getCode());

		this.saleConfigs = this.promotionSaleConfigService.getAdvSaleConfig(this.baseInfo.getCode());
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: toAddEmption
	 * @Description: 添加越早越便宜页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toAddCleap", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/emptionCleap_add.jsp") })
	public String toAddCleap() throws Exception {
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		// 产品型号集合
		// productTypes = this.productTypeService.listAll();
		// 最小销售单位list
		minSaleUnitVos = minSaleUnitService.listAll(language.getCode());
		// 区域配置list
		areaConfigs = areaConfigService.listAll();

		// 销售配置list
		this.saleConfigs = this.saleConfigService.querySaleConfig();
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: toUpdateCleap
	 * @Description: 跳转到越早越便宜更新页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toUpdateCleap", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/emptionCleap_update.jsp") })
	public String toUpdateCleap() throws Exception {
		this.baseInfo = this.baseInfoService.getMarkerPromotionBaseInfoByCode(this.baseInfo.getCode().trim());
		this.rule = this.ruleService.getMarkerPromotionRuleByPromotionCode(this.baseInfo.getCode());
		this.discountRules = this.discountRuleService.getDiscountRuleListByRuleCode(this.rule.getCode());
		this.softwareRules = this.softwareRuleService.getSoftwareListByPromotionCode(this.baseInfo.getCode());
		this.timesInfos = this.timesInfoService.getTimesInfoListByPromotionCode(this.baseInfo.getCode());
		this.areaRules = this.areaRuleService.getPromotionAreaByPromotionCode(this.baseInfo.getCode());
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		minSaleUnitVos = this.softwareRuleService.getAdvMinSaleUnit(this.baseInfo.getCode(), language.getCode());
		areaConfigs = this.areaRuleService.getAdvAreaConfig(this.baseInfo.getCode());

		this.promotionSaleConfigs = this.promotionSaleConfigService.getSaleConfigByPromotionCode(this.baseInfo.getCode());

		this.saleConfigs = this.promotionSaleConfigService.getAdvSaleConfig(this.baseInfo.getCode());
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: toAddExpire
	 * @Description: 添加过期续租页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toAddExpire", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/emptionExpire_add.jsp") })
	public String toAddExpire() throws Exception {
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		// 产品型号集合
		// productTypes = this.productTypeService.listAll();
		// 最小销售单位list
		minSaleUnitVos = minSaleUnitService.listAll(language.getCode());
		// 区域配置list
		areaConfigs = areaConfigService.listAll();

		// 销售配置list
		this.saleConfigs = this.saleConfigService.querySaleConfig();
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: toUpdateExpire
	 * @Description: 跳转到过期续租更新页面
	 * @param
	 * @return String
	 * @throws
	 */
	@Action(value = "toUpdateExpire", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/emptionExpire_update.jsp") })
	public String toUpdateExpire() throws Exception {
		this.baseInfo = this.baseInfoService.getMarkerPromotionBaseInfoByCode(this.baseInfo.getCode().trim());
		this.rule = this.ruleService.getMarkerPromotionRuleByPromotionCode(this.baseInfo.getCode());
		this.discountRules = this.discountRuleService.getDiscountRuleListByRuleCode(this.rule.getCode());
		this.softwareRules = this.softwareRuleService.getSoftwareListByPromotionCode(this.baseInfo.getCode());
		this.timesInfos = this.timesInfoService.getTimesInfoListByPromotionCode(this.baseInfo.getCode());
		this.areaRules = this.areaRuleService.getPromotionAreaByPromotionCode(this.baseInfo.getCode());
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		minSaleUnitVos = this.softwareRuleService.getAdvMinSaleUnit(this.baseInfo.getCode(), language.getCode());
		areaConfigs = this.areaRuleService.getAdvAreaConfig(this.baseInfo.getCode());
		this.promotionSaleConfigs = this.promotionSaleConfigService.getSaleConfigByPromotionCode(this.baseInfo.getCode());

		this.saleConfigs = this.promotionSaleConfigService.getAdvSaleConfig(this.baseInfo.getCode());
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: delPromotion
	 * @Description: 删除营销活动
	 * @param
	 * @return String
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "delPromotion")
	public String delPromotion() throws Exception {
		try {
			this.baseInfoService.delPromotion(baseInfo.getCode().trim());
			this.msgs.add(getText("market.promotion.delSuccess"));
			this.urls.put(getText("market.mainsale.message5"), "toMarkerPromotion.do?search.promotionType=" + search.getPromotionType());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toMarkerPromotion.do?search.promotionType=" + search.getPromotionType());
		}
		return MESSAGE;
	}

	/**
	 * 
	 * @Title: stopPromotion
	 * @Description: 启用营销活动
	 * @param
	 * @return String
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "startPromotion")
	public String startPromotion() throws Exception {
		try {
			boolean bool = this.baseInfoService.checkPromotion(promotionCodes);
			if (!bool) {
				//表示活动时间冲突
				this.msgs.add(getText("promotion.check.message1"));
				this.msgs.add(getText("promotion.check.message"));
			} else {

				this.baseInfoService.startPromotion(promotionCodes);
				this.msgs.add(getText("market.mainsale.message4"));
			}

			this.urls.put(getText("market.mainsale.message5"), "toMarkerPromotion.do?search.promotionType=" + search.getPromotionType());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toMarkerPromotion.do?search.promotionType=" + search.getPromotionType());
		}
		return MESSAGE;
	}

	/**
	 * 
	 * @Title: stopPromotion
	 * @Description: 终止营销活动
	 * @param
	 * @return String
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "stopPromotion")
	public String stopPromotion() throws Exception {
		try {
			this.baseInfoService.stopPromotion(promotionCodes);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("market.mainsale.message5"), "toMarkerPromotion.do?search.promotionType=" + search.getPromotionType());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"), "toMarkerPromotion.do?search.promotionType=" + search.getPromotionType());
		}
		return MESSAGE;
	}
}
