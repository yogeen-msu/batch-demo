/**
 */
package com.cheriscon.backstage.market.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.market.service.IMinSaleUnitPriceService;
import com.cheriscon.backstage.market.service.IMinSaleUnitService;
import com.cheriscon.backstage.market.service.IMinSaleUnitSoftwareDetailService;
import com.cheriscon.backstage.market.service.IMinSalesUnitMemoService;
import com.cheriscon.backstage.market.vo.MinSaleUnitSearch;
import com.cheriscon.backstage.product.service.IProductSoftwareConfigService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.util.CallProcedureUtil;
import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.model.MinSaleUnitPrice;
import com.cheriscon.common.model.MinSaleUnitSoftwareDetail;
import com.cheriscon.common.model.ProductSoftwareConfig;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.utils.JSONHelpUtils;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * @ClassName: MinSaleUnitAction
 * @Description: 最小销售单位管理action
 * @author shaohu
 * @date 2013-1-15 上午10:12:33
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/market")
public class MinSaleUnitAction extends WWAction {
	
	private static final long serialVersionUID = 7720131698096965737L;

	@Resource
	private IMinSaleUnitService minSaleUnitService;
	
	@Resource
	private IProductTypeService productTypeService;
	
	@Resource
	private IMinSalesUnitMemoService minSalesUnitMemoService;
	
	@Resource
	private IMinSaleUnitSoftwareDetailService detailService;
	
	@Resource
	private IMinSaleUnitPriceService priceService;
	
	@Resource
	private ILanguageService languageService;
	@Resource
	private IProductSoftwareConfigService productSoftwareConfigService;
	
	private MinSaleUnitSearch minSaleUnitSearch;
	
	private ProductType productType;
	
	private List<ProductType> productTypes;
	
	private MinSaleUnit minSaleUnit;
	
	private MinSaleUnitMemo minSaleUnitMemo;
	
	private List<MinSaleUnitMemo> minSaleUnitMemos;
	
	private List<Language> languages;
	
	private List<SoftwareType> softwareTypes;
	
	private List<MinSaleUnitSoftwareDetail> softwareDetails;
	
	private List<AreaConfig> areaConfigs;
	
	private List<MinSaleUnitPrice> prices;
	
	private String excludecodes;
	
	private String selprosoftconfigcodes;

	private SoftwareType softwareType;
	
	private List<ProductSoftwareConfig> productSoftwareConfigList;
	
	public SoftwareType getSoftwareType() {
		return softwareType;
	}

	public void setSoftwareType(SoftwareType softwareType) {
		this.softwareType = softwareType;
	}

	public String getExcludecodes() {
		return excludecodes;
	}

	public void setExcludecodes(String excludecodes) {
		this.excludecodes = excludecodes;
	}

	public String getSelprosoftconfigcodes() {
		return selprosoftconfigcodes;
	}

	public void setSelprosoftconfigcodes(String selprosoftconfigcodes) {
		this.selprosoftconfigcodes = selprosoftconfigcodes;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public MinSaleUnitSearch getMinSaleUnitSearch() {
		return minSaleUnitSearch;
	}

	public void setMinSaleUnitSearch(MinSaleUnitSearch minSaleUnitSearch) {
		this.minSaleUnitSearch = minSaleUnitSearch;
	}

	public List<ProductType> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(List<ProductType> productTypes) {
		this.productTypes = productTypes;
	}

	public MinSaleUnit getMinSaleUnit() {
		return minSaleUnit;
	}

	public void setMinSaleUnit(MinSaleUnit minSaleUnit) {
		this.minSaleUnit = minSaleUnit;
	}

	public MinSaleUnitMemo getMinSaleUnitMemo() {
		return minSaleUnitMemo;
	}

	public void setMinSaleUnitMemo(MinSaleUnitMemo minSaleUnitMemo) {
		this.minSaleUnitMemo = minSaleUnitMemo;
	}

	public List<MinSaleUnitMemo> getMinSaleUnitMemos() {
		return minSaleUnitMemos;
	}

	public void setMinSaleUnitMemos(List<MinSaleUnitMemo> minSaleUnitMemos) {
		this.minSaleUnitMemos = minSaleUnitMemos;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public List<SoftwareType> getSoftwareTypes() {
		return softwareTypes;
	}

	public void setSoftwareTypes(List<SoftwareType> softwareTypes) {
		this.softwareTypes = softwareTypes;
	}

	public List<MinSaleUnitSoftwareDetail> getSoftwareDetails() {
		return softwareDetails;
	}

	public void setSoftwareDetails(List<MinSaleUnitSoftwareDetail> softwareDetails) {
		this.softwareDetails = softwareDetails;
	}

	public List<AreaConfig> getAreaConfigs() {
		return areaConfigs;
	}

	public void setAreaConfigs(List<AreaConfig> areaConfigs) {
		this.areaConfigs = areaConfigs;
	}

	public List<MinSaleUnitPrice> getPrices() {
		return prices;
	}

	public void setPrices(List<MinSaleUnitPrice> prices) {
		this.prices = prices;
	}

	@Action(value = "refreshMin")
	public String refreshMin() throws Exception{
		try{
			CallProcedureUtil.call("pro_InitProductMinsaleSearch");
			msgs.add("操作成功");
		}catch(Exception e){
			msgs.add("操作失败，请联系管理员");
		}
		this.urls.put("返回", "toMinSaleUnitPage.do");
		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: toMinSaleUnitPage
	* @Description: 跳转到最小销售单位列表页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toMinSaleUnitPage", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minSaleUnit_list.jsp") })
	public String toMinSaleUnitPage() throws Exception {
		if(null == minSaleUnitSearch){
			minSaleUnitSearch= new MinSaleUnitSearch();
		}
		HttpServletRequest request =ThreadContextHolder.getHttpRequest();
		Language language = languageService.getByLocale(request.getLocale());
		
		this.minSaleUnitSearch.setLanguageCode(language.getCode());
		this.productTypes = productTypeService.listAll();
		this.webpage = minSaleUnitService.pageMinSaleUnit(minSaleUnitSearch, this.getPage(), this.getPageSize());
		return SUCCESS;
	}
	
//	@Action(value = "delMinSaleUnit", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minSaleUnit_list.jsp") })
	@SuppressWarnings("unchecked")
	@Action(value = "delMinSaleUnit")
	public String delMinSaleUnit() throws Exception{
		
		try{
			this.minSaleUnitService.delMinSaleUnit(minSaleUnitSearch.getCode().trim());
			//设置默认查询的语言编码
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			Language language = languageService.getByLocale(request.getLocale());
			this.minSaleUnitSearch.setLanguageCode(language.getCode());
			this.productTypes = productTypeService.listAll();
			this.webpage = minSaleUnitService.pageMinSaleUnit(minSaleUnitSearch, this.getPage(), this.getPageSize());
			this.msgs.add(getText("common.js.del"));
			this.urls.put(getText("market.mainsale.message5"),"toMinSaleUnitPage.do");
		}catch(Exception e){
//			e.printStackTrace();
			this.msgs.add(getText("common.js.nonDel"));
			this.urls.put(getText("market.mainsale.message5"),"toMinSaleUnitPage.do");
		}
		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: toAddMinSaleUnit
	* @Description: 跳转到添加最小销售单位
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toAddMinSaleUnit", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minSaleUnit_add.jsp") })
	public String toAddMinSaleUnit() throws Exception{
		this.productTypes = productTypeService.listAll();
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value = "addMinSaleUnit")
	public String addMinSaleUnit() throws Exception{
		try{
			if(StringUtils.isBlank(minSaleUnit.getProTypeCode().trim())){
				this.msgs.add(getText("market.mainsale.message1"));
				this.urls.put(getText("market.mainsale.message2"),"toAddMinSaleUnit.do");
			}else if(StringUtils.isBlank(minSaleUnitMemo.getName().trim())){
				this.msgs.add(getText("market.mainsale.message8"));
				this.urls.put(getText("market.mainsale.message2"),"toAddMinSaleUnit.do");
			}else{
				if(StringUtils.isNotBlank(minSaleUnit.getUpCode())){
					try{
						MinSaleUnit mUnit= this.minSaleUnitService.getMinSaleUnitByCode(minSaleUnit.getUpCode());
						if(null == mUnit){
							this.msgs.add(getText("market.mainsale.message9"));
							this.urls.put(getText("market.mainsale.message2"),"toAddMinSaleUnit.do");
							return MESSAGE;
						}
					}catch (Exception e) {
						this.msgs.add(getText("market.mainsale.message9"));
						this.urls.put(getText("market.mainsale.message2"),"toAddMinSaleUnit.do");
						return MESSAGE;
					}
				}
				//登录用户语言环境
				HttpServletRequest request =ThreadContextHolder.getHttpRequest();
				Language language = languageService.getByLocale(request.getLocale());
				this.minSaleUnitMemo.setLanguageCode(language.getCode());
				this.minSaleUnitService.addMinSaleUnit(minSaleUnit, minSaleUnitMemo);
				this.msgs.add(getText("market.mainsale.message3"));
				this.urls.put(getText("market.mainsale.message5"),"toMinSaleUnitPage.do");
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"toAddMinSaleUnit.do");
		}
		return MESSAGE;
	}

	/**
	 * 
	* @Title: toUpdateMinSaleUnit
	* @Description: 跳转到修改最小销售单位名称页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toUpdateMinSaleUnit", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minSaleUnit_update.jsp") })
	public String toUpdateMinSaleUnit() throws Exception{
		String name = new String(this.minSaleUnit.getName().getBytes("ISO8859-1"),"UTF-8");
		this.minSaleUnit = this.minSaleUnitService.getMinSaleUnitByCode(minSaleUnit.getCode());
		this.minSaleUnit.setName(name);
		this.productTypes = productTypeService.listAll();
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: updateMinSaleUnit
	* @Description: 修改最小销售单位管理
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "updateMinSaleUnit")
	public String updateMinSaleUnit() throws Exception{
		try{
			if(StringUtils.isBlank(minSaleUnit.getProTypeCode().trim())){
				this.msgs.add(getText("market.mainsale.message1"));
				this.urls.put(getText("market.mainsale.message2"),"toUpdateMinSaleUnit.do?minSaleUnit.code='"+minSaleUnit.getCode()+"'");
			}else if(StringUtils.isBlank(minSaleUnitMemo.getName().trim())){
				this.msgs.add(getText("market.mainsale.message3"));
				this.urls.put(getText("market.mainsale.message2"),"toUpdateMinSaleUnit.do?minSaleUnit.code='"+minSaleUnit.getCode()+"'");
			}else{
				if(StringUtils.isNotBlank(minSaleUnit.getUpCode())){
					try{
						MinSaleUnit mUnit= this.minSaleUnitService.getMinSaleUnitByCode(minSaleUnit.getUpCode());
						if(null == mUnit){
							this.msgs.add(getText("market.mainsale.message9"));
							this.urls.put(getText("market.mainsale.message11"),"toUpdateMinSaleUnit.do?minSaleUnit.code="+minSaleUnit.getCode()+"&minSaleUnit.name="+minSaleUnitMemo.getName());
							return MESSAGE;
						}
					}catch (Exception e) {
						this.msgs.add(getText("market.mainsale.message9"));
						this.urls.put(getText("market.mainsale.message11"),"toUpdateMinSaleUnit.do?minSaleUnit.code="+minSaleUnit.getCode()+"&minSaleUnit.name="+minSaleUnitMemo.getName());
						return MESSAGE;
					}
				}
				//登录用户语言环境
				HttpServletRequest request =ThreadContextHolder.getHttpRequest();
				Language language = languageService.getByLocale(request.getLocale());
				this.minSaleUnitMemo.setLanguageCode(language.getCode());
				this.minSaleUnitService.updateMinSaleUnit(minSaleUnit, minSaleUnitMemo);
				this.msgs.add(getText("market.mainsale.message4"));
				this.urls.put(getText("market.mainsale.message5"),"toMinSaleUnitPage.do");
			}
		}catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"toUpdateMinSaleUnit.do?minSaleUnit.code='"+minSaleUnit.getCode()+"'");
		}
		return MESSAGE;
	}
	
	/**
	 * 
	* @Title: toMinSaleUnitMemo
	* @Description: 跳到说明管理页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toMinSaleUnitMemo", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minSaleUnitMemo_list.jsp") })
	public String toMinSaleUnitMemo() throws Exception{
		String name = new String(this.minSaleUnit.getName().getBytes("ISO8859-1"),"UTF-8");
		this.minSaleUnit = this.minSaleUnitService.getMinSaleUnitByCode(this.minSaleUnit.getCode().trim());
		this.minSaleUnit.setName(name);
		this.productType = this.productTypeService.getProductTypeByCode(this.minSaleUnit.getProTypeCode());
		this.minSaleUnitMemos = this.minSalesUnitMemoService.getMinSaleUnitMemoListByUnitCode(this.minSaleUnit.getCode());
		this.languages = this.minSalesUnitMemoService.getAvailableLanguage(this.minSaleUnit.getCode());
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: toMinSaleUnitSoftwareDetail
	* @Description: 跳转到最小销售单位软件管理页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toMinSaleUnitSoftwareDetail", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minSaleUnitSoftwareDetail_list_new.jsp") })
	public String toMinSaleUnitSoftwareDetail() throws Exception{
		String name = new String(this.minSaleUnit.getName().getBytes("ISO8859-1"),"UTF-8");
		this.minSaleUnit = this.minSaleUnitService.getMinSaleUnitByCode(this.minSaleUnit.getCode().trim());
		this.minSaleUnit.setName(name);
		this.productType = this.productTypeService.getProductTypeByCode(this.minSaleUnit.getProTypeCode());
		this.softwareDetails = this.detailService.getDetailByUnitCode(this.minSaleUnit.getCode());
		this.softwareTypes = this.detailService.getAvailableSoftwareType(this.minSaleUnit.getCode() , this.minSaleUnit.getProTypeCode());
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: toMinSaleUnitPrice
	* @Description: 跳转到价格管理页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toMinSaleUnitPrice", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minSaleUnitPrice_list.jsp") })
	public String toMinSaleUnitPrice() throws Exception{
		String name = new String(this.minSaleUnit.getName().getBytes("ISO8859-1"),"UTF-8");
		this.minSaleUnit = this.minSaleUnitService.getMinSaleUnitByCode(this.minSaleUnit.getCode().trim());
		this.minSaleUnit.setName(name);
		this.productType = this.productTypeService.getProductTypeByCode(this.minSaleUnit.getProTypeCode());
		this.prices = this.priceService.getMinSaleUnitPriceListByUnitCode(this.minSaleUnit.getCode());
		this.areaConfigs = this.priceService.getAvailableAreaConfig(this.minSaleUnit.getCode());
		return SUCCESS;
	}
	
	/**
	 * 
	* @Title: showMinSaleUnit
	* @Description: 最小销售单位详情页面
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "showMinSaleUnit", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minSaleUnit_show.jsp") })
	public String showMinSaleUnit() throws Exception{
		this.minSaleUnit = this.minSaleUnitService.getMinSaleUnitByCode(this.minSaleUnit.getCode().trim());
		this.productType = this.productTypeService.getProductTypeByCode(this.minSaleUnit.getProTypeCode());
		this.minSaleUnitMemos = this.minSalesUnitMemoService.getMinSaleUnitMemoListByUnitCode(this.minSaleUnit.getCode());
		this.softwareDetails = this.detailService.getDetailByUnitCode(this.minSaleUnit.getCode());
		this.prices = this.priceService.getMinSaleUnitPriceListByUnitCode(this.minSaleUnit.getCode());
		return SUCCESS;
	}

	
	/**
	 * 
	* @Title: toSearchSoftDlg
	* @Description: 查询软件列表（添加时软件时用）
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toSearchSoftDlg", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minsaleunit_software_selet.jsp") })
	public String toSearchSoftDlg() throws Exception{
		try {
			excludecodes = excludecodes.replace("‘", "'");
			this.minSaleUnit = this.minSaleUnitService.getMinSaleUnitByCode(this.minSaleUnit.getCode().trim());
			this.softwareTypes = this.detailService.getSelSoftwareType(minSaleUnit, softwareType, excludecodes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}

	
	/**
	 * 
	* @Title: toSearchSoftDlg
	* @Description: 查询软件列表（添加时软件时用）
	* @param    
	* @return String    
	* @throws
	 */
	@Action(value = "toSearchSoftDlgNew", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minsaleunit_softwareselect.jsp") })
	public String toSearchSoftDlgNew() throws Exception{
		try {
			if(StringUtils.isNotBlank(selprosoftconfigcodes))
			{
				selprosoftconfigcodes = selprosoftconfigcodes.replace("‘", "'");
				excludecodes = excludecodes.replace("‘", "'");
				this.minSaleUnit = this.minSaleUnitService.getMinSaleUnitByCode(this.minSaleUnit.getCode().trim());
				this.softwareTypes = this.detailService.getSelSoftwareTypeNew(softwareType, excludecodes, selprosoftconfigcodes);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(value = "toSearchMinSale", results = { @Result(name = SUCCESS, location = "/autel/backstage/market/minsaleunit_select.jsp") })
	public String toSearchMinSale() throws Exception{
		try {
			if(StringUtils.isNotBlank(selprosoftconfigcodes))
			{
				selprosoftconfigcodes = selprosoftconfigcodes.replace("‘", "'");
				excludecodes = excludecodes.replace("‘", "'");
				this.minSaleUnit = this.minSaleUnitService.getMinSaleUnitByCode(this.minSaleUnit.getCode().trim());
				this.softwareTypes = this.detailService.getSelSoftwareTypeNew(softwareType, excludecodes, selprosoftconfigcodes);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(value = "jsonTree")
	public String jsonTree(){
		try {
			List<ProductSoftwareConfig> productSoftwareConfigParentList;
			HttpServletRequest request =ThreadContextHolder.getHttpRequest();
			Language language = languageService.getByLocale(request.getLocale());
			productSoftwareConfigList = productSoftwareConfigService.getProductMinSaleTree(language.getCode());
			
			productSoftwareConfigParentList = productSoftwareConfigList;
			
			
			List<Object> list = new ArrayList<Object>();
			list.add(productSoftwareConfigParentList);
			this.json = JSONHelpUtils.getJsonString4JavaPOJOList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.JSON_MESSAGE;
	}
	
}
