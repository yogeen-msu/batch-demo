/**
*/ 
package com.cheriscon.backstage.market.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.backstage.market.service.ISaleConfigMemoService;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.common.model.SaleConfigMemo;
import com.cheriscon.framework.action.WWAction;

/**
 * @ClassName: SaleConfigMemoAction
 * @Description: 销售配置说明控制类
 * @author shaohu
 * @date 2013-1-16 下午01:51:00
 * 
 */
@ParentPackage("json_default")
@Namespace("/autel/market")
public class SaleConfigMemoAction extends WWAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7993812658978439597L;

	@Resource
	private ISaleConfigMemoService saleConfigMemoService;
	
	private SaleConfig saleConfig;
	
	private List<SaleConfigMemo> saleConfigMemos;
	
	/**
	 * 需删除的销售配置说明
	 */
	private List<SaleConfigMemo> delSaleConfigMemos;
	
	public SaleConfig getSaleConfig() {
		return saleConfig;
	}

	public void setSaleConfig(SaleConfig saleConfig) {
		this.saleConfig = saleConfig;
	}

	public List<SaleConfigMemo> getSaleConfigMemos() {
		return saleConfigMemos;
	}

	public void setSaleConfigMemos(List<SaleConfigMemo> saleConfigMemos) {
		this.saleConfigMemos = saleConfigMemos;
	}
	
	public List<SaleConfigMemo> getDelSaleConfigMemos() {
		return delSaleConfigMemos;
	}

	public void setDelSaleConfigMemos(List<SaleConfigMemo> delSaleConfigMemos) {
		this.delSaleConfigMemos = delSaleConfigMemos;
	}

	/**
	 * 
	* @Title: updateSaleConfigMemo
	* @Description: 修改销售配置说明数据
	* @param    
	* @return String    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Action(value = "updateSaleConfigMemo")
	public String updateSaleConfigMemo() throws Exception {
		try {
			saleConfigMemoService.updateSaleConfigMemo(saleConfigMemos,
					delSaleConfigMemos);
			this.msgs.add(getText("market.mainsale.message4"));
			this.urls.put(getText("common.btn.return"),"toSaleConfig.do");
//			this.urls.put(getText("market.mainsale.message5"),"toSaleConfigMemo.do?saleConfig.code="+saleConfig.getCode());
		} catch (Exception e) {
			e.printStackTrace();
			this.msgs.add(getText("common.system.error"));
			this.urls.put(getText("common.btn.return"),"toSaleConfigMemo.do?saleConfig.code="+saleConfig.getCode());
//			this.urls.put(getText("common.btn.return"),"toSaleConfig.do");
		}
		return MESSAGE;
	}

}
