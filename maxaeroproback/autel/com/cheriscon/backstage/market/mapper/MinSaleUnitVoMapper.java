/**
*/ 
package com.cheriscon.backstage.market.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.backstage.market.vo.MinSaleUnitVo;

/**
 * @ClassName: MinSaleUnitVoMapper
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-15 下午02:44:16
 * 
 */
public class MinSaleUnitVoMapper implements RowMapper{

	/**
	  * <p>Title: mapRow</p>
	  * <p>Description: </p>
	  * @param arg0
	  * @param arg1
	  * @return
	  * @throws SQLException
	  */ 
	@Override
	public Object mapRow(ResultSet rs, int arg1) throws SQLException {
		MinSaleUnitVo v = new MinSaleUnitVo();
		v.setId(rs.getInt("id"));
		v.setCode(rs.getString("code"));
		v.setName(rs.getString("name"));
		v.setProTypeCode(rs.getString("proTypeCode"));
		v.setProductTypeName(rs.getString("productTypeName"));
		v.setPicPath(rs.getString("picPath"));
		return v;
	}

}
