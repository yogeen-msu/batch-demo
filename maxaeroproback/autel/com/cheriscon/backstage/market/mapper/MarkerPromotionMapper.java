/**
 */
package com.cheriscon.backstage.market.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.backstage.market.vo.MarkerPromotionVo;

/**
 * @ClassName: MarkerPromotionMapper
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-23 上午10:14:56
 * 
 */
public class MarkerPromotionMapper implements RowMapper {

	/**
	 * <p>
	 * Title: mapRow
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param arg0
	 * @param arg1
	 * @return
	 * @throws SQLException
	 */
	@Override
	public Object mapRow(ResultSet rs, int arg1) throws SQLException {
		MarkerPromotionVo v = new MarkerPromotionVo();
		v.setId(rs.getInt("id"));
		v.setPromotionName(rs.getString("promotionName"));
		v.setCode(rs.getString("code"));
		v.setAllArea(rs.getInt("allArea"));
		v.setAllSaleUnit(rs.getInt("allSaleUnit"));
		v.setCreatetime(rs.getString("createtime"));
		v.setCreator(rs.getString("creator"));
		v.setPromotionType(rs.getInt("promotionType"));
		v.setRsultTime(rs.getString("rsultTime"));
		v.setStatus(rs.getInt("status"));
		if (StringUtils.isNotBlank(v.getRsultTime())) {
			if (StringUtils.indexOf("br", v.getRsultTime()) != -1) {
				v.setRsultTime(v.getRsultTime().trim().replaceAll("&lt;", "<").replaceAll("&gt;", ">"));
			}
		}
		return v;
	}

}
