/**
*/ 
package com.cheriscon.backstage.market.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.backstage.market.vo.MinSaleUnitConfigVo;

/**
 * @ClassName: MinSaleUnitConfigVoMapper
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-17 下午04:46:04
 * 
 */
public class MinSaleUnitConfigVoMapper implements RowMapper{
	/**
	  * <p>Title: mapRow</p>
	  * <p>Description: </p>
	  * @param arg0
	  * @param arg1
	  * @return
	  * @throws SQLException
	  */ 
	@Override
	public Object mapRow(ResultSet rs, int arg1) throws SQLException {
		MinSaleUnitConfigVo v = new MinSaleUnitConfigVo();
		v.setId(rs.getInt("id"));
		v.setCode(rs.getString("code"));
		v.setName(rs.getString("name"));
		v.setProTypeCode(rs.getString("proTypeCode"));
		v.setAreaCfgCode(rs.getString("areaCfgCode"));
		return v;
	}
}
