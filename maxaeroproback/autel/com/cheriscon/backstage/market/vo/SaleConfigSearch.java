/**
*/ 
package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

/**
 * @ClassName: SaleConfigSearch
 * @Description: 销售配置查询条件表
 * @author shaohu
 * @date 2013-1-17 下午02:15:31
 * 
 */
public class SaleConfigSearch implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2482343041516007572L;

	/**
	 * 销售配置名称
	 */
	private String configName;

	/**
	 * 产品型号Code
	 */
	private String proTypeCode;

	public SaleConfigSearch() {
		super();
	}

	public SaleConfigSearch(String configName, String proTypeCode) {
		super();
		this.configName = configName;
		this.proTypeCode = proTypeCode;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
	
	
}
