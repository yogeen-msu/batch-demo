/**
 */
package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName: MarkerPromotion
 * @Description: 营销活动search
 * @author shaohu
 * @date 2013-1-22 下午05:57:02
 * 
 */
public class MarkerPromotionSearch implements Serializable {

	private static final long serialVersionUID = -7655633177577184690L;

	/**
	 * 活动名称
	 */
	private String promotionName;

	/**
	 * 活动类型
	 */
	private Integer promotionType;

	private Integer status;

	/**
	 * 活动开始时间
	 */
	private String startTime;

	/**
	 * 活动结束时间
	 */
	private String endTime;

	public MarkerPromotionSearch() {
		super();
	}

	public MarkerPromotionSearch(String promotionName, Integer promotionType,
			Integer status, String startTime, String endTime) {
		super();
		this.promotionName = promotionName;
		this.promotionType = promotionType;
		this.status = status;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStartTime() {
		if (StringUtils.isNotBlank(startTime)) {
			return startTime + " 00:00:00";
		}
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		if (StringUtils.isNotBlank(endTime)) {
			return endTime + " 00:00:00";
		}
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

}
