/**
 */
package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

/**
 * @ClassName: MarkerPromotionVo
 * @Description: 营销活动vo
 * @author shaohu
 * @date 2013-1-22 下午06:07:07
 * 
 */
public class MarkerPromotionVo implements Serializable {

	private static final long serialVersionUID = 4779601473944628496L;
	private Integer id;
	
	/**
	 * 活动名称
	 */
	private String promotionName;
	/**
	 * 活动编码
	 */
	private String code;

	/**
	 * 创建人
	 */
	private String createUser;

	/**
	 * 状态
	 */
	private Integer status;

	/**
	 * 活动类型
	 */
	private Integer promotionType;
	private String creator;
	private String createtime;
	private Integer allArea;
	private Integer allSaleUnit;
	private String rsultTime;

	public MarkerPromotionVo() {
		super();
	}

	public MarkerPromotionVo(Integer id, String promotionName, String code,
			String createUser, Integer status, Integer promotionType,
			String creator, String createtime, Integer allArea,
			Integer allSaleUnit, String rsultTime) {
		super();
		this.id = id;
		this.promotionName = promotionName;
		this.code = code;
		this.createUser = createUser;
		this.status = status;
		this.promotionType = promotionType;
		this.creator = creator;
		this.createtime = createtime;
		this.allArea = allArea;
		this.allSaleUnit = allSaleUnit;
		this.rsultTime = rsultTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public Integer getAllArea() {
		return allArea;
	}

	public void setAllArea(Integer allArea) {
		this.allArea = allArea;
	}

	public Integer getAllSaleUnit() {
		return allSaleUnit;
	}

	public void setAllSaleUnit(Integer allSaleUnit) {
		this.allSaleUnit = allSaleUnit;
	}

	public String getRsultTime() {
		return rsultTime;
	}

	public void setRsultTime(String rsultTime) {
		this.rsultTime = rsultTime;
	}

	
}
