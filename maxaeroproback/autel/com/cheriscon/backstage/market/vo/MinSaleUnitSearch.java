/**
*/ 
package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

/**
 * @ClassName: MinSaleUnitSearch
 * @Description: 最小销售单位search实体
 * @author shaohu
 * @date 2013-1-15 上午10:43:16
 * 
 */
public class MinSaleUnitSearch implements Serializable {

	private static final long serialVersionUID = 4965274599212826108L;
	
	/**
	 * 最小销售单位code
	 */
	private String code;
	
	/**
	 * 产品型号编码
	 */
	private String productTypeCode ;
	
	/**
	 * 最小销售单位名称
	 */
	private String name;
	
	/**
	 * 语言编码
	 */
	private String languageCode;

	public MinSaleUnitSearch() {
		super();
	}

	public MinSaleUnitSearch(String code, String productTypeCode, String name,
			String languageCode) {
		super();
		this.code = code;
		this.productTypeCode = productTypeCode;
		this.name = name;
		this.languageCode = languageCode;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	

}
