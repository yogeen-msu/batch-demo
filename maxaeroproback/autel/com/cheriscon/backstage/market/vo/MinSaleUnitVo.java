/**
*/ 
package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

/**
 * @ClassName: MinSaleUnitVo
 * @Description: 最小销售单位
 * @author shaohu
 * @date 2013-1-15 下午02:34:25
 * 
 */
public class MinSaleUnitVo implements Serializable {
	
	private static final long serialVersionUID = 2221332092880751500L;
	
	private Integer id;
	/**
	 * 最小销售单位编码
	 */
	private String code;
	/**
	 * 产品型号编码
	 */
	private String proTypeCode;
	/**
	 * 最小销售单位名称
	 */
	private String name;
	/**
	 * 产品型号名称
	 */
	private String productTypeName;
	
	
	private String picPath;
	
	public MinSaleUnitVo() {
		super();
	}

	public MinSaleUnitVo(Integer id, String code, String proTypeCode,
			String name, String productTypeName, String picPath) {
		super();
		this.id = id;
		this.code = code;
		this.proTypeCode = proTypeCode;
		this.name = name;
		this.productTypeName = productTypeName;
		this.picPath = picPath;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	
}
