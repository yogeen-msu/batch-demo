/**
*/ 
package com.cheriscon.backstage.market.vo;

/**
 * @ClassName: MarkerPromotionTime
 * @Description: TODO
 * @author shaohu
 * @date 2013-1-22 下午06:12:35
 * 
 */
public class MarkerPromotionTime {
	
	/**
	 * 开始时间
	 */
	private String startTime;
	
	/**
	 * 结束时间
	 */
	private String endTime;

	public MarkerPromotionTime() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MarkerPromotionTime(String startTime, String endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
}
