package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;

public class ProductForSealerConfig implements Serializable {

	/**
	 * 外部产品与内部产品对应关系
	 */
	private static final long serialVersionUID = -216937761966209164L;

    private Integer id ;
    private String productTypeCode;
    private String productForSealerCode;
    private String productTypeName;
    
    @NotDbField
	public String getProductTypeName() {
		return productTypeName;
	}
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}
	public Integer getId() {
		return id;
	}
	public String getProductTypeCode() {
		return productTypeCode;
	}
	public String getProductForSealerCode() {
		return productForSealerCode;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}
	public void setProductForSealerCode(String productForSealerCode) {
		this.productForSealerCode = productForSealerCode;
	}
}
