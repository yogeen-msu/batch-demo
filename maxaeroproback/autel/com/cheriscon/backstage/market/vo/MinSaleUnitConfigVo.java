/**
 */
package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

/**
 * @ClassName: MinSaleUnitConfigVo
 * @Description: 销售配置所需要最小单位业务类
 * @author shaohu
 * @date 2013-1-17 下午04:41:32
 * 
 */
public class MinSaleUnitConfigVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5121312338870033999L;
	private Integer id;
	private String code;
	private String proTypeCode;
	private String name;
	private String areaCfgCode = "";

	public MinSaleUnitConfigVo() {
		super();
	}

	public MinSaleUnitConfigVo(Integer id, String code, String proTypeCode,
			String name, String areaCfgCode) {
		super();
		this.id = id;
		this.code = code;
		this.proTypeCode = proTypeCode;
		this.name = name;
		this.areaCfgCode = areaCfgCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

}
