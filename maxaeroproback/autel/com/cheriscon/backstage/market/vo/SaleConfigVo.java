/**
 */
package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

/**
 * @ClassName: SaleConfigVo
 * @Description: 销售配置业务vo
 * @author shaohu
 * @date 2013-1-18 下午03:35:49
 * 
 */
public class SaleConfigVo implements Serializable {

	private static final long serialVersionUID = -4908073389490915028L;
	private Integer id;
	private String code;
	private String name;
	private String areaCfgCode;

	public SaleConfigVo() {
		super();
	}

	public SaleConfigVo(Integer id, String code, String name, String areaCfgCode) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.areaCfgCode = areaCfgCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

}
