package com.cheriscon.backstage.market.vo;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;

public class ProductForSealer implements Serializable {

	/**
	 * 外部产品
	 */
	private static final long serialVersionUID = -2450319490483605004L;

    private Integer id;
    private String code;          //编码
    private String name;          //产品名称
    private String picPath;       //产品图片
    private String serialPath;    //序列号图片
    
    private String proTypeName;   //对应的内部产品名称
   
    private String proTypeCodeStr; //内部产品编码字符串
    
    @NotDbField
	public String getProTypeCodeStr() {
		return proTypeCodeStr;
	}
	public void setProTypeCodeStr(String proTypeCodeStr) {
		this.proTypeCodeStr = proTypeCodeStr;
	}
	public String getProTypeName() {
		return proTypeName;
	}
	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}
	public Integer getId() {
		return id;
	}
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getPicPath() {
		return picPath;
	}
	
	public String getSerialPath() {
		return serialPath;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public void setSerialPath(String serialPath) {
		this.serialPath = serialPath;
	}

}
