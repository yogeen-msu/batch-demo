/**
 */
package com.cheriscon.backstage.market.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.mapper.MarkerPromotionMapper;
import com.cheriscon.backstage.market.service.IMarkerPromotionAreaRuleService;
import com.cheriscon.backstage.market.service.IMarkerPromotionBaseInfoService;
import com.cheriscon.backstage.market.service.IMarkerPromotionDiscountRuleService;
import com.cheriscon.backstage.market.service.IMarkerPromotionRuleService;
import com.cheriscon.backstage.market.service.IMarkerPromotionSaleConfigService;
import com.cheriscon.backstage.market.service.IMarkerPromotionSoftwareRuleService;
import com.cheriscon.backstage.market.service.IMarkerPromotionTimesInfoService;
import com.cheriscon.backstage.market.vo.MarkerPromotionSearch;
import com.cheriscon.backstage.market.vo.MarkerPromotionVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MarkerPromotionAreaRule;
import com.cheriscon.common.model.MarkerPromotionBaseInfo;
import com.cheriscon.common.model.MarkerPromotionDiscountRule;
import com.cheriscon.common.model.MarkerPromotionRule;
import com.cheriscon.common.model.MarkerPromotionSaleConfig;
import com.cheriscon.common.model.MarkerPromotionSoftwareRule;
import com.cheriscon.common.model.MarkerPromotionTimesInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: MarkerPromotionBaseInfoServiceImpl
 * @Description: 营销活动接口实现类
 * @author shaohu
 * @date 2013-1-22 下午02:58:52
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MarkerPromotionBaseInfoServiceImpl extends BaseSupport implements
		IMarkerPromotionBaseInfoService {

	private final static String TABLE_NAME = "DT_MarketPromotionBaseInfo";

	@Resource
	private IMarkerPromotionRuleService ruleService;

	@Resource
	private IMarkerPromotionDiscountRuleService discountRuleService;

	@Resource
	private IMarkerPromotionSoftwareRuleService softwareRuleService;
	
	@Resource
	private IMarkerPromotionSaleConfigService saleConfigService;

	@Resource
	private IMarkerPromotionTimesInfoService timesInfoService;

	@Resource
	private IMarkerPromotionAreaRuleService areaRuleService;

	/**
	 * 
	 * <p>
	 * Title: getMarkerPromotionBaseInfoByCode
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@Override
	public MarkerPromotionBaseInfo getMarkerPromotionBaseInfoByCode(String code)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where code=?");
		return (MarkerPromotionBaseInfo) this.daoSupport.queryForObject(
				sql.toString(), MarkerPromotionBaseInfo.class, code);
	}

	/**
	 * <p>
	 * Title: pageMarkerPromotion
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param promotionVo
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	@Override
	public Page pageMarkerPromotion(MarkerPromotionSearch search, int pageNo,
			int pageSize) throws Exception {

//		System.out.println("================ size:"+this.listAll().size());
		
		
		
		
		StringBuffer conditionSql = new StringBuffer();
		if (null != search) {
			if (null != search.getPromotionType()) {
				conditionSql.append(" and mpb.promotionType = ").append(
						search.getPromotionType());
			}
			if (StringUtils.isNotBlank(search.getPromotionName())) {
				conditionSql.append(" and mpb.promotionName like '%")
						.append(search.getPromotionName().trim()).append("%'");
			}
			if (null != search.getStatus()) {
				conditionSql.append(" and mpb.status = ").append(
						search.getStatus());
			}
			if (StringUtils.isNotBlank(search.getStartTime())) {
				conditionSql.append(" and mpt.startTime >= '")
						.append(search.getStartTime()).append("'");
			}
			if (StringUtils.isNotBlank(search.getEndTime())) {
				conditionSql.append(" and mpt.endTime <= '")
						.append(search.getEndTime()).append("'");
				;
			}
		}

		StringBuffer sql=new StringBuffer();
		sql.append(" select * from ");
		sql.append(" ( "); 
		sql.append(" select ta.id, "); 
		sql.append(" ta.code, "); 
		sql.append(" ta.promotionName, "); 
		sql.append(" ta.promotionType, "); 
		sql.append(" ta.status, "); 
		sql.append(" ta.creator, "); 
		sql.append(" ta.createtime , "); 
		sql.append(" ta.allArea , "); 
		sql.append(" ta.allSaleUnit,  "); 
		sql.append(" td.rsultTime rsultTime  "); 
		sql.append(" from ( select mpt.startTime ,mpt.endTime,mpb.* from DT_MarketPromotionTimesInfo mpt left join DT_MarketPromotionBaseInfo mpb on mpt.marketPromotionCode = mpb.code where 1=1 ");  
		sql.append(conditionSql);
		sql.append(" ) as ta  "); 
		sql.append(" , "); 
		sql.append(" ( "); 
		sql.append(" SELECT group_concat(rsultTime separator '') rsultTime,tb.code "); 
		sql.append(" FROM (select CONCAT( mpt.startTime,' ', mpt.endTime,'|') rsultTime,mpt.startTime ,mpt.endTime,mpb.* from DT_MarketPromotionTimesInfo mpt ");
		sql.append(" left join DT_MarketPromotionBaseInfo mpb on ");
		sql.append(" mpt.marketPromotionCode = mpb.code ");
		sql.append(" where 1=1"); 
		sql.append(conditionSql);
		sql.append(" ) as tb "); 
		sql.append(" group by tb.code "); 
		sql.append(" ) td  where td.code=ta.code"); 
		sql.append("  GROUP BY ta.code  ");  
	/*	sql.append(" rsultTime , "); 
		sql.append(" ta.id, "); 
		sql.append(" ta.promotionName, "); 
		sql.append(" ta.promotionType, "); 
		sql.append(" ta.status, "); 
		sql.append(" ta.creator, "); 
		sql.append(" ta.createtime , "); 
		sql.append(" ta.allArea , "); 
		sql.append(" ta.allSaleUnit  "); */
		sql.append(" ) "); 
		sql.append(" tc "); 
		sql.append("  order by tc.id desc  "); 
		
//		StringBuffer sql = new StringBuffer();
//		sql.append("select * from (");
//		sql.append("select ta.id,ta.code,ta.promotionName,ta.promotionType,ta.status,ta.creator,ta.createtime ,ta.allArea ,ta.allSaleUnit, SUBSTRING(rsultTime, 1, len(rsultTime)-1) rsultTime from ("
//				+ conditionSql.toString()
//				+ ") as ta cross APPLY (SELECT startTime + ' '+ endTime + '<br>'  FROM ("
//				+ conditionSql.toString()
//				+ ") as tb WHERE ta.code = tb.code  FOR XML PATH('')) D (rsultTime) GROUP BY ta.code , rsultTime , ta.id,ta.promotionName,ta.promotionType,ta.status,ta.creator,ta.createtime ,ta.allArea ,ta.allSaleUnit ");
//		sql.append(") tc order by tc.id desc ");*/
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, new MarkerPromotionMapper());
		return page;
	}

	/**
	 * <p>
	 * Title: addPromotion
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param baseInfo
	 * @param rule
	 * @param discountRules
	 * @param timesInfos
	 * @param softwareRules
	 * @throws Exception
	 */
	@Override
	@Transactional
	public void addPromotion(MarkerPromotionBaseInfo baseInfo,
			MarkerPromotionRule rule,
			List<MarkerPromotionDiscountRule> discountRules,
			List<MarkerPromotionTimesInfo> timesInfos,
			List<MarkerPromotionSoftwareRule> softwareRules,
			List<MarkerPromotionAreaRule> areaRules,
			List<MarkerPromotionSaleConfig> markerPromotionSaleConfigs) throws Exception {

		String code = DBUtils.generateCode(DBConstant.PROMOTION_BASE);

		if (null != baseInfo) {
			// 添加营销活动基础信息
			baseInfo.setCode(code);
			baseInfo.setCreatetime(DateUtil.toString(new Date(),
					"yyyyMMddHHmmssSSSS"));
			this.daoSupport.insert(TABLE_NAME, baseInfo);

			if (null != rule) {
				// 添加营销活动规则
				rule.setMarketPromotionCode(code);
				String ruleCode = ruleService.addMarkerPromotionRule(rule);
				if (null != discountRules && discountRules.size() > 0) {
					// 添加营销活动规则条件
					this.discountRuleService
							.batchAddMarkerPromotionDiscountRule(ruleCode,
									discountRules);
				}
			}

			// 添加营销活动时间段信息
			if (null != timesInfos && timesInfos.size() > 0) {
				this.timesInfoService.batchAddMarkerPromotionTimesInfo(code,
						timesInfos);
			}

			// 添加最小销售单位信息
			if (null != softwareRules && softwareRules.size() > 0) {
				this.softwareRuleService.batchAddMarkerPromotionSoftwareRule(
						code, softwareRules);
			}

			//添加销售配置
			if (null != markerPromotionSaleConfigs
					&& markerPromotionSaleConfigs.size() > 0) {
				this.saleConfigService.batchAddMarkerPromotionSaleConfig(code,
						markerPromotionSaleConfigs);
			}
			
			// 添加区域信息
			if (null != areaRules && areaRules.size() > 0) {
				this.areaRuleService.batchAddMarkerPromotionAreaRule(code,
						areaRules);
			}
			
			//所有最小销售单位
			if(baseInfo.getAllSaleUnit().intValue() == 1){
				this.softwareRuleService.addMarkerPromotionSoftwareRuleAll(code);
			}
			
			//所有区域
			if(baseInfo.getAllArea().intValue() == 1){
				this.areaRuleService.addMarkerPromotionAreaRuleAll(code);
			}
		}

	}

	/**
	 * <p>
	 * Title: updatePromotion
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param baseInfo
	 * @param rule
	 * @param discountRules
	 * @param delDiscountRules
	 * @param timesInfos
	 * @param delTimesInfos
	 * @param softwareRules
	 * @param delSoftwareRules
	 * @param areaRules
	 * @param delAreaRules
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void updatePromotion(MarkerPromotionBaseInfo baseInfo,
			MarkerPromotionRule rule,
			List<MarkerPromotionDiscountRule> discountRules,
			List<MarkerPromotionDiscountRule> delDiscountRules,
			List<MarkerPromotionTimesInfo> timesInfos,
			List<MarkerPromotionTimesInfo> delTimesInfos,
			List<MarkerPromotionSoftwareRule> softwareRules,
			List<MarkerPromotionSoftwareRule> delSoftwareRules,
			List<MarkerPromotionAreaRule> areaRules,
			List<MarkerPromotionAreaRule> delAreaRules,
			List<MarkerPromotionSaleConfig> markerPromotionSaleConfigs,
			List<MarkerPromotionSaleConfig> delMarkerPromotionSaleConfigs) throws Exception {
		// 修改基础信息
		this.daoSupport.update(TABLE_NAME, baseInfo,
				" code='" + baseInfo.getCode() + "'");

		String code = baseInfo.getCode();

		if (null != rule) {
			// 修改营销活动规则
			ruleService.updateMarkerPromotionRule(rule);
			if (null != discountRules && discountRules.size() > 0) {
				// 添加营销活动规则条件
				this.discountRuleService.batchAddMarkerPromotionDiscountRule(
						rule.getCode(), discountRules);
			}
			// 删除营销活动规则条件
			if (null != delDiscountRules && delDiscountRules.size() > 0) {
				for (MarkerPromotionDiscountRule discountRule : delDiscountRules) {
					this.discountRuleService
							.delMarkerPromotionDiscountRuleById(discountRule
									.getId());
				}
			}
		}

		// 添加营销活动时间段信息
		if (null != timesInfos && timesInfos.size() > 0) {
			this.timesInfoService.batchAddMarkerPromotionTimesInfo(code,
					timesInfos);
		}
		// 删除营销活动时间段信息
		if (null != delTimesInfos && delTimesInfos.size() > 0) {
			for (MarkerPromotionTimesInfo info : delTimesInfos) {
				this.timesInfoService.delPromotionTimesInfoById(info.getId());
			}
		}

		if (baseInfo.getAllSaleUnit() != DBConstant.ALL_SALE_UNIT) {
			//删除标识所有销售单位
			this.softwareRuleService.delPromotionMinsaleUnitByPromotionCodeALL(code);
			// 添加最小销售单位信息
			if (null != softwareRules && softwareRules.size() > 0) {
				this.softwareRuleService.batchAddMarkerPromotionSoftwareRule(
						code, softwareRules);
			}
			// 删除最小销售单位信息
			if (null != delSoftwareRules && delSoftwareRules.size() > 0) {
				for (MarkerPromotionSoftwareRule softwareRule : delSoftwareRules) {
					this.softwareRuleService
							.delPromotionMinsaleUnitById(softwareRule.getId());
				}
			}
		} else {
			this.softwareRuleService
					.delPromotionMinsaleUnitByPromotionCode(code);
			//添加标识所有销售单位
			this.softwareRuleService.addMarkerPromotionSoftwareRuleAll(code);
		}
		
		//添加营销活动销售配置
		if (null != markerPromotionSaleConfigs
				&& markerPromotionSaleConfigs.size() > 0) {
			this.saleConfigService.batchAddMarkerPromotionSaleConfig(code,
					markerPromotionSaleConfigs);
		}
		
		// 删除营销活动销售配置
		if (null != delMarkerPromotionSaleConfigs
				&& delMarkerPromotionSaleConfigs.size() > 0) {
			for (MarkerPromotionSaleConfig config : delMarkerPromotionSaleConfigs) {
				this.saleConfigService.delPromotionSaleConfigById(config
						.getId());
			}
		}

		if (baseInfo.getAllArea() != DBConstant.ALL_AREA) {
			this.areaRuleService.delMarkerPromotionAreaRuleAll(code);
			// 添加区域信息
			if (null != areaRules && areaRules.size() > 0) {
				this.areaRuleService.batchAddMarkerPromotionAreaRule(code,
						areaRules);
			}
			// 删除区域信息
			if (null != delAreaRules && delAreaRules.size() > 0) {
				for (MarkerPromotionAreaRule areaRule : delAreaRules) {
					this.areaRuleService
							.delMarkerPromotionAreaRuleById(areaRule.getId());
				}
			}
		} else {
			this.areaRuleService.delMarkerPromotionAreaByPromotionCode(code);
			this.areaRuleService.addMarkerPromotionAreaRuleAll(code);
		}

	}

	/**
	 * <p>
	 * Title: delPromotion
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param promotionCode
	 * @throws Exception
	 */
	@Override
	@Transactional
	public void delPromotion(String promotionCode) throws Exception {

		MarkerPromotionRule rule = this.ruleService
				.getMarkerPromotionRuleByPromotionCode(promotionCode);
		if (null != rule) {
			this.discountRuleService
					.delMarkerPromotionDiscountRuleByRuleCode(rule.getCode());
		}
		this.ruleService.delRuleByPromotionCode(promotionCode);
		this.timesInfoService.delTimesInfoByPromotionCode(promotionCode);
		this.softwareRuleService
				.delPromotionMinsaleUnitByPromotionCode(promotionCode);
		this.areaRuleService
				.delMarkerPromotionAreaByPromotionCode(promotionCode);
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code=?");
		this.daoSupport.execute(sql.toString(), promotionCode);

	}

	/**
	 * <p>
	 * Title: startPromotion
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param promotionCodes
	 * @throws Exception
	 */
	@Override
	public void startPromotion(String promotionCodes) throws Exception {
		String[] codes = promotionCodes.split(",");
		StringBuffer code = new StringBuffer();
		for (String s : codes) {
			code.append("'").append(s.trim()).append("'").append(" ");
		}
		StringBuffer sql = new StringBuffer();
		sql.append("update ").append(TABLE_NAME).append(" set status=1")
				.append(" where code in (")
				.append(code.toString().trim().replaceAll(" ", ","))
				.append(")");
		this.daoSupport.execute(sql.toString());
	}

	/**
	 * <p>
	 * Title: stopPromotion
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param promotionCodes
	 * @throws Exception
	 */
	@Override
	public void stopPromotion(String promotionCodes) throws Exception {
		String[] codes = promotionCodes.split(",");
		StringBuffer code = new StringBuffer();
		for (String s : codes) {
			code.append("'").append(s.trim()).append("'").append(" ");
		}
		StringBuffer sql = new StringBuffer();
		sql.append("update ").append(TABLE_NAME).append(" set status = 2")
				.append(" where code in (")
				.append(code.toString().trim().replaceAll(" ", ","))
				.append(")");
		this.daoSupport.execute(sql.toString());

	}

	/**
	 * 
	* @Title: checkPromotoin
	* @Description: 检查活动时间是否有冲突
	* @param    
	* @return boolean   false 表示校验不同过  活动时间有交叉 
	* @throws
	 */
	public boolean checkPromotion(String promotionCodes){
		List<MarkerPromotionVo> pList = this.listAll();
		List<PromotionTime> list = new ArrayList<PromotionTime>();
		
		if(null != pList && pList.size() >0 ){
			for(MarkerPromotionVo v : pList){
				if(v.getStatus().intValue() == 1 && promotionCodes.indexOf(v.getCode())!= -1){
					continue;
				}
				if(v.getStatus().intValue() == 1 || promotionCodes.indexOf(v.getCode())!= -1){
					String rsultTime = v.getRsultTime();
					if(StringUtils.isNotBlank(rsultTime)){
						String [] times = rsultTime.trim().split("&lt;br&gt;");
						int len = times.length;
						for(int i = 0 ; i < len ; i ++){
							String pTime = times[i];
							if (StringUtils.isNotBlank(pTime)) {
								PromotionTime time = new PromotionTime();
								String[] mTime = pTime.split(" ");
								time.setStartTime(mTime[0] + " " + mTime[1]);
								time.setEndTime(mTime[2] + " " + mTime[3]);
								list.add(time);
							}
						}
					}
					
				}
			}
		}
		
		java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try{
			
			if (list.size() > 1) {
				int s = list.size();
				for (int i = 0; i < s; i++) {
					PromotionTime time = list.get(i);
					
					String fStartTime = time.getStartTime();
					String fEndTime = time.getEndTime();
					
					long sTime  = sdf.parse(fStartTime).getTime();
					long eTime = sdf.parse(fEndTime).getTime();
					for (int j = i + 1; j < s; j++) {
						PromotionTime pTime = list.get(j);
						String pStartTime = pTime.getStartTime();
						String pEndTime = pTime.getEndTime();
						
						long psTime  = sdf.parse(pStartTime).getTime();
						long peTime = sdf.parse(pEndTime).getTime();
						
						if(psTime > eTime || peTime < sTime){
							continue;
						}else{
							return false;
						}
						
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return true;
	}
	
	/**
	 * 
	* @Title: listAll
	* @Description: 排除过期续租之后的所有已启用活动
	* @param    
	* @return List<MarkerPromotionVo>    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	public List<MarkerPromotionVo> listAll(){
		StringBuffer conditionSql = new StringBuffer();
		conditionSql.append("select CONCAT(mpt.startTime,' ',mpt.endTime) btTime,mpb.* from DT_MarketPromotionTimesInfo mpt left join DT_MarketPromotionBaseInfo mpb on mpt.marketPromotionCode = mpb.code where mpb.promotionType != 4 ");
		StringBuffer sql = new StringBuffer();
		sql.append("select * from (");
		sql.append("select ta.id,ta.code,ta.promotionName,ta.promotionType,ta.status,ta.creator,ta.createtime ,ta.allArea ,ta.allSaleUnit, td.rsultTime from ("
				+ conditionSql.toString()
				+ ") as ta , (SELECT GROUP_CONCAT(btTime separator '<br>') rsultTime ,tb.code  FROM ("
				+ conditionSql.toString()
				+ ") as tb  group by tb.code  ) td where ta.code = td.code  GROUP BY ta.code , rsultTime , ta.id,ta.promotionName,ta.promotionType,ta.status,ta.creator,ta.createtime ,ta.allArea ,ta.allSaleUnit ");
		sql.append(") tc order by tc.id desc ");
		return this.daoSupport.queryForList(sql.toString(), new MarkerPromotionMapper());
	}
}
class PromotionTime{
	/**
	 * 活动开始时间
	 */
	private String startTime;
	/**
	 * 活动结束时间
	 */
	private String endTime;
	
	public PromotionTime() {
		super();
	}
	
	public PromotionTime(String startTime, String endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
}
