/**
*/ 
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.market.service.IMarkerPromotionDiscountRuleService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MarkerPromotionDiscountRule;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MarkerPromotionDiscountRuleServiceImpl
 * @Description: 营销活动规则条件实现类
 * @author shaohu
 * @date 2013-1-22 下午03:07:34
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MarkerPromotionDiscountRuleServiceImpl extends BaseSupport implements
		IMarkerPromotionDiscountRuleService {

	private final static String TABLE_NAME = "DT_MarketPromotionDiscountRule";
	
	/**
	 * 
	  * <p>Title: getDiscountRuleListByRuleCode</p>
	  * <p>Description: </p>
	  * @param ruleCode
	  * @return
	  * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MarkerPromotionDiscountRule> getDiscountRuleListByRuleCode(String ruleCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where marketPromotionRuleCode=?");
		return this.daoSupport.queryForList(sql.toString(), MarkerPromotionDiscountRule.class ,ruleCode);
	}
	
	/**
	  * <p>Title: addMarkerPromotionDiscountRule</p>
	  * <p>Description: </p>
	  * @param discountRule
	  * @throws Exception
	  */ 
	@Override
	public void addMarkerPromotionDiscountRule(
			MarkerPromotionDiscountRule discountRule) throws Exception {
		String code = DBUtils.generateCode(DBConstant.PROMOTION_DIS_RULE);
		discountRule.setCode(code);
		this.daoSupport.insert(TABLE_NAME, discountRule);
	}

	/**
	  * <p>Title: batchAddMarkerPromotionDiscountRule</p>
	  * <p>Description: </p>
	  * @param discountRules
	  * @throws Exception
	  */ 
	@Override
	public void batchAddMarkerPromotionDiscountRule(
			String ruleCode ,List<MarkerPromotionDiscountRule> discountRules) throws Exception {
		int i = 0;
		//添加营销活动规则条件
		for(MarkerPromotionDiscountRule discountRule : discountRules){
			discountRule.setMarketPromotionRuleCode(ruleCode);
			String code = DBUtils.generateCode(DBConstant.PROMOTION_DIS_RULE,String.valueOf(i));
			discountRule.setCode(code);
			this.daoSupport.insert(TABLE_NAME, discountRule);
			i++;
		}
		
	}

	/**
	  * <p>Title: delMarkerPromotionDiscountRuleById</p>
	  * <p>Description: </p>
	  * @param id
	  * @throws Exception
	  */ 
	@Override
	public void delMarkerPromotionDiscountRuleById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);
	}

	/**
	  * <p>Title: delMarkerPromotionDiscountRuleByRuleCode</p>
	  * <p>Description: </p>
	  * @param ruleCode
	  * @throws Exception
	  */ 
	@Override
	public void delMarkerPromotionDiscountRuleByRuleCode(String ruleCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where marketPromotionRuleCode=?");
		this.daoSupport.execute(sql.toString(), ruleCode);
		
	}

}
