/**
*/
package com.cheriscon.backstage.market.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.mapper.MinSaleUnitConfigVoMapper;
import com.cheriscon.backstage.market.service.IMinSaleUnitSaleCfgDetailService;
import com.cheriscon.backstage.market.service.ISaleConfigService;
import com.cheriscon.backstage.market.vo.MinSaleUnitConfigVo;
import com.cheriscon.backstage.market.vo.SaleConfigSearch;
import com.cheriscon.backstage.market.vo.SaleConfigVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitSaleCfgDetail;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: SaleConfigServiceImpl
 * @Description: 销售配置实现类
 * @author shaohu
 * @date 2013-1-17 下午01:59:14
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class SaleConfigServiceImpl extends BaseSupport implements ISaleConfigService {

	private final static String TABLE_NAME = "DT_SaleConfig";

	@Resource
	private IMinSaleUnitSaleCfgDetailService cfgDetailService;

	@SuppressWarnings("unchecked")
	public Page pageConfig(SaleConfigSearch configSearch, int pageNo,
			int pageSize) throws Exception {
		
		StringBuffer countSql = new StringBuffer();
		
		countSql.append("select * from DT_SaleConfig where 1=1");
		if(null != configSearch){
			if(StringUtils.isNotBlank(configSearch.getConfigName())){
				countSql.append(" and name like '%").append(configSearch.getConfigName().trim()).append("%'");
			}
			if(StringUtils.isNotBlank(configSearch.getProTypeCode())){
				countSql.append(" and proTypeCode='"+configSearch.getProTypeCode().trim()+"'");
			}

		}
		countSql.append(" order by name ");
		List<SaleConfigVo> configVos2 = this.daoSupport.queryForList(countSql.toString(), SaleConfigVo.class);
		return this.daoSupport.queryForPage(countSql.toString(), pageNo, pageSize, SaleConfigVo.class);
		
	}

	public List<SaleConfigVo> convertVo(List<SaleConfigVo> configVos) throws Exception {
		//code为键
		Map<String, SaleConfigVo> map = new HashMap<String, SaleConfigVo>();
		for (SaleConfigVo v : configVos) {
			String code = v.getCode().trim();
			if (map.containsKey(code)) {
				SaleConfigVo t = map.get(code);
				if (StringUtils.isNotBlank(v.getAreaCfgCode()) && StringUtils.isNotBlank(t.getAreaCfgCode())) {
					t.setAreaCfgCode(t.getAreaCfgCode() + "," + v.getAreaCfgCode());
				} else if (StringUtils.isNotBlank(t.getAreaCfgCode())) {
					t.setAreaCfgCode(t.getAreaCfgCode());
				} else if (StringUtils.isNotBlank(v.getAreaCfgCode())) {
					t.setAreaCfgCode(v.getAreaCfgCode());
				}
				map.put(code, t);
			} else {
				map.put(code, v);
			}
		}
	
	List<SaleConfigVo> result = new ArrayList<SaleConfigVo>();
	Set<String> keys = map.keySet();
	for(String key : keys){
		SaleConfigVo v = map.get(key);
		result.add(v);
	}
	
	return result;
	}
	
	
	
	/**
	  * <p>Title: pageSaleConfig</p>
	  * <p>Description: </p>
	  * @param configSearch
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	@SuppressWarnings("unchecked")
	@Override
	public Page pageSaleConfig(SaleConfigSearch configSearch, int pageNo, int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dsc.*,dpt.name as proTypeName from DT_SaleConfig dsc left join DT_ProductForSealer dpt on dsc.proTypeCode = dpt.code where 1=1 ");
		if (null != configSearch) {
			if (StringUtils.isNotBlank(configSearch.getConfigName())) {
				sql.append(" and dsc.name like '%").append(configSearch.getConfigName().trim()).append("%'");
			}

			if (StringUtils.isNotBlank(configSearch.getProTypeCode())) {
				sql.append(" and dpt.code = '").append(configSearch.getProTypeCode().trim()).append("'");
			}
		}
		sql.append(" order by dsc.id desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, SaleConfig.class);
	}

	@SuppressWarnings("unchecked")
	private List<MinSaleUnitConfigVo> getMinSaleUnit(String sql, String languageCode) throws Exception {
		//lag201301070146010554
		//		StringBuffer sql = new StringBuffer();
		//		sql.append("select dmu.* , dmm.name, dmp.areaCfgCode from DT_MinSaleUnit dmu left join DT_MinSaleUnitMemo dmm on dmu.code = dmm.minSaleUnitCode and dmm.languageCode=? left join DT_MinSaleUnitPrice dmp on dmu.code = dmp.minSaleUnitCode ");
		return this.daoSupport.queryForList(sql.toString(), new MinSaleUnitConfigVoMapper(), languageCode);
	}

	@Override
	public List<MinSaleUnitConfigVo> getMinSaleUnitConfigList(String languageCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dmu.* , dmm.name, dmp.areaCfgCode from DT_MinSaleUnit dmu left join DT_MinSaleUnitMemo dmm on dmu.code = dmm.minSaleUnitCode and dmm.languageCode=? left join DT_MinSaleUnitPrice dmp on dmu.code = dmp.minSaleUnitCode order by dmm.name");
		List<MinSaleUnitConfigVo> configVos = this.getMinSaleUnit(sql.toString(), languageCode);
		return this.bulidUnitConfigList(configVos);
	}

	private List<MinSaleUnitConfigVo> bulidUnitConfigList(List<MinSaleUnitConfigVo> configVos) throws Exception {
		if (null == configVos || configVos.size() == 0) {
			return null;
		}
		//code为键
		Map<String, MinSaleUnitConfigVo> map = new HashMap<String, MinSaleUnitConfigVo>();
		for (MinSaleUnitConfigVo v : configVos) {
			String code = v.getCode().trim();
			if (map.containsKey(code)) {
				MinSaleUnitConfigVo t = map.get(code);
				if (StringUtils.isNotBlank(v.getAreaCfgCode()) && StringUtils.isNotBlank(t.getAreaCfgCode())) {
					t.setAreaCfgCode(t.getAreaCfgCode() + "," + v.getAreaCfgCode());
				} else if (StringUtils.isNotBlank(t.getAreaCfgCode())) {
					t.setAreaCfgCode(t.getAreaCfgCode());
				} else if (StringUtils.isNotBlank(v.getAreaCfgCode())) {
					t.setAreaCfgCode(v.getAreaCfgCode());
				}
				map.put(code, t);
			} else {
				map.put(code, v);
			}
		}

		List<MinSaleUnitConfigVo> result = new ArrayList<MinSaleUnitConfigVo>();
		Set<String> keys = map.keySet();
		for (String key : keys) {
			MinSaleUnitConfigVo v = map.get(key);
			result.add(v);
		}

		return result;
	}

	/**
	 * 
	* @Title: getAvailableMinSaleUnitConfigList
	* @Description: 修改时候查询可用的最小销售单位数据
	* @param    
	* @return List<MinSaleUnitConfigVo>    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitConfigVo> getAvailableMinSaleUnitConfigList(String saleCode, String languageCode, String proTypeCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dmu.* , dmm.name, dmp.areaCfgCode from DT_MinSaleUnit dmu left join DT_MinSaleUnitMemo dmm on dmu.code = dmm.minSaleUnitCode and dmm.languageCode=? and dmu.proTypeCode = ? left join DT_MinSaleUnitPrice dmp on dmu.code = dmp.minSaleUnitCode where dmu.code not in(select dmc.minSaleUnitCode from DT_MinSaleUnitSaleCfgDetail dmc where  dmc.saleCfgCode = ?);");
		List<MinSaleUnitConfigVo> configVos = this.daoSupport.queryForList(sql.toString(), new MinSaleUnitConfigVoMapper(), languageCode,
				proTypeCode, saleCode);
		return this.bulidUnitConfigList(configVos);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitConfigVo> getSaleConfigUnit(String saleCode, String languageCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dsu.id ,dmu.code,dmu.proTypeCode , dmm.name, dmp.areaCfgCode from DT_SaleConfig dc left join DT_MinSaleUnitSaleCfgDetail dsu on dc.code = dsu.saleCfgCode left join DT_MinSaleUnit dmu  on dsu.minSaleUnitCode = dmu.code left join DT_MinSaleUnitMemo dmm on dmu.code = dmm.minSaleUnitCode  left join DT_MinSaleUnitPrice dmp on dmu.code = dmp.minSaleUnitCode where  dmm.languageCode=? and dc.code = ?  ");
		List<MinSaleUnitConfigVo> configVos = this.daoSupport.queryForList(sql.toString(), new MinSaleUnitConfigVoMapper(), languageCode, saleCode);

		return this.bulidUnitConfigList(configVos);
	}

	/**
	  * <p>Title: addSaleConfig</p>
	  * <p>Description: </p>
	  * @param saleConfig
	  * @param details
	  * @throws Exception
	  */
	@Override
	@Transactional
	public void addSaleConfig(SaleConfig saleConfig, List<MinSaleUnitSaleCfgDetail> details) throws Exception {
		String code = DBUtils.generateCode(DBConstant.SALE_CONFIG_CODE);
		saleConfig.setCode(code);
		this.daoSupport.insert(TABLE_NAME, saleConfig);
		if (null != details && details.size() > 0) {
			for (MinSaleUnitSaleCfgDetail detail : details) {
				detail.setSaleCfgCode(code);
				this.cfgDetailService.addMinSaleUnitSaleCfgDetail(detail);
			}
		}
	}

	/**
	  * <p>Title: delSaleConfigByCode</p>
	  * <p>Description: </p>
	  * @param code
	  * @throws Exception
	  */
	@Override
	@Transactional
	public void delSaleConfigByCode(String code) throws Exception {
		this.cfgDetailService.delMinSaleUnitSaleCfgDetailByCode(code);
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code=?");
		this.daoSupport.execute(sql.toString(), code);

	}

	/**
	  * <p>Title: getSaleConfigDetailByCode</p>
	  * <p>Description: 根据编码查询销售配置详情</p>
	  * @param code
	  * @return
	  * @throws Exception
	  */
	@Override
	public SaleConfig getSaleConfigDetailByCode(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		
		sql.append("select dsc.*,dpt.name as proTypeName from ");
		sql.append("DT_SaleConfig dsc left join DT_ProductForSealer dpt on dsc.proTypeCode = dpt.code where dsc.code = ? ");
		
		return (SaleConfig) this.daoSupport.queryForObject(sql.toString(), SaleConfig.class, code);
	}

	/**
	  * <p>Title: getSaleConfigByCode</p>
	  * <p>Description: 根据编码查询销售配置</p>
	  * @param code
	  * @return
	  * @throws Exception
	  */
	@Override
	public SaleConfig getSaleConfigByCode(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where code=?");
		return (SaleConfig) this.daoSupport.queryForObject(sql.toString(), SaleConfig.class, code);
	}

	/**
	  * <p>Title: updateSaleConfig</p>
	  * <p>Description: </p>
	  * @param saleConfig
	  * @param details
	  * @param delDetails
	  * @throws Exception
	  */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void updateSaleConfig(SaleConfig saleConfig, List<MinSaleUnitSaleCfgDetail> details, List<MinSaleUnitSaleCfgDetail> delDetails)
			throws Exception {
		this.daoSupport.update(TABLE_NAME, saleConfig, " code='" + saleConfig.getCode() + "'");
	    String sql="delete from DT_MinSaleUnitSaleCfgDetail where saleCfgCode=?";
	    this.daoSupport.execute(sql, saleConfig.getCode());
	    if (null != details && details.size() > 0) {
			for (MinSaleUnitSaleCfgDetail detail : details) {
				detail.setSaleCfgCode(saleConfig.getCode());
				this.cfgDetailService.addMinSaleUnitSaleCfgDetail(detail);
			}
		}
		
		/*	if (null != details && details.size() > 0) {
			for (MinSaleUnitSaleCfgDetail cfgDetail : details) {
				cfgDetail.setSaleCfgCode(saleConfig.getCode());
				this.cfgDetailService.addMinSaleUnitSaleCfgDetail(cfgDetail);
			}
		}
		if (null != delDetails && delDetails.size() > 0) {
			for (MinSaleUnitSaleCfgDetail cfgDetail : delDetails) {
				this.cfgDetailService.delMinSaleUnitSaleCfgDetailById(cfgDetail.getId());
			}
		}*/

	}

	/**
	  * <p>Title: listAll</p>
	  * <p>Description: </p>
	  * @return
	  * @throws Exception
	  */
	@SuppressWarnings("unchecked")
	@Override
	public List<SaleConfigVo> listAll() throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select distinct dc.id ,dc.code, dc.name as name,dmp.areaCfgCode from DT_SaleConfig dc left join DT_MinSaleUnitSaleCfgDetail dsu on dc.code = dsu.saleCfgCode left join DT_MinSaleUnit dmu  on dsu.minSaleUnitCode = dmu.code left join DT_MinSaleUnitPrice dmp on dmu.code = dmp.minSaleUnitCode order by dc.id desc");
		List<SaleConfigVo> configVos = this.daoSupport.queryForList(sql.toString(), SaleConfigVo.class);

		if (null == configVos || configVos.size() == 0) {
			return null;
		}
		//code为键
		Map<String, SaleConfigVo> map = new HashMap<String, SaleConfigVo>();
		for (SaleConfigVo v : configVos) {
			String code = v.getCode().trim();
			if (map.containsKey(code)) {
				SaleConfigVo t = map.get(code);
				if (StringUtils.isNotBlank(v.getAreaCfgCode()) && StringUtils.isNotBlank(t.getAreaCfgCode())) {
					t.setAreaCfgCode(t.getAreaCfgCode() + "," + v.getAreaCfgCode());
				} else if (StringUtils.isNotBlank(t.getAreaCfgCode())) {
					t.setAreaCfgCode(t.getAreaCfgCode());
				} else if (StringUtils.isNotBlank(v.getAreaCfgCode())) {
					t.setAreaCfgCode(v.getAreaCfgCode());
				}
				map.put(code, t);
			} else {
				map.put(code, v);
			}
		}

		List<SaleConfigVo> result = new ArrayList<SaleConfigVo>();
		Set<String> keys = map.keySet();
		for (String key : keys) {
			SaleConfigVo v = map.get(key);
			result.add(v);
		}

		return result;
	}

	/**
	 * 得到所有的销售配置信息
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SaleConfig> querySaleConfig() throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dsc.*,dpt.name as proTypeName from DT_SaleConfig dsc left join DT_ProductForSealer dpt on dsc.proTypeCode = dpt.code where 1=1 ");
		sql.append(" order by dsc.id desc");
		return this.daoSupport.queryForList(sql.toString(), SaleConfig.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnit> queryMinSaleUnits(String code) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("select m.* from DT_MinSaleUnitSaleCfgDetail d," +
				" DT_MinSaleUnit m where d.minSaleUnitCode=m.code " +
				"and d.saleCfgCode=?");
		
		return this.daoSupport.queryForList(sql.toString(),MinSaleUnit.class, code);
	}
	
	
}
