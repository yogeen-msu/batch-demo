/**
*/ 
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.service.IMinSaleUnitPriceService;
import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.MinSaleUnitPrice;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MinSaleUnitPriceServiceImpl
 * @Description: 最小销售单位价格业务实现类
 * @author shaohu
 * @date 2013-1-16 下午04:53:06
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MinSaleUnitPriceServiceImpl extends BaseSupport implements IMinSaleUnitPriceService {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitPrice> getMinSaleUnitPriceListByUnitCode(String unitCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select dp.*,dc.name as areaName from DT_MinSaleUnitPrice dp left join DT_AreaConfig dc on dp.areaCfgCode = dc.code where 1=1 and ");
		sql.append(" dp.minSaleUnitCode = ?");
		sql.append(" order by dp.id desc");
		return this.daoSupport.queryForList(sql.toString(), MinSaleUnitPrice.class, unitCode);
	}
	
	/**
	 * 
	* @Title: getAvailableAreaConfig
	* @Description: 获取最小销售单位下 可用的区域配置数据
	* @param  unitCode 最小销售单位编码  
	* @return List<AreaConfig>    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<AreaConfig> getAvailableAreaConfig(String unitCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_AreaConfig dc where not exists (select * from DT_MinSaleUnitPrice dp where dc.code = dp.areaCfgCode and dp.minSaleUnitCode = ? ) order by dc.id desc");
		return this.daoSupport.queryForList(sql.toString(), AreaConfig.class, unitCode);
	}

	/**
	  * <p>Title: batchUpdateMinSaleUnitPrice</p>
	  * <p>Description: 批量更新最小销售单位价格数据</p>
	  * @param prices
	  * @param prices2
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void batchUpdateMinSaleUnitPrice(List<MinSaleUnitPrice> prices,
			List<MinSaleUnitPrice> prices2) throws Exception {
		
		if(null != prices && prices.size() > 0){
			for(MinSaleUnitPrice price : prices){
				this.addMinSaleUnitPrice(price);
			}
		}
		
		if(null != prices2 && prices2.size() > 0){
			for(MinSaleUnitPrice price : prices2){
				this.delMinSaleUnitPriceById(price.getId());
			}
		}
	}
	
	/**
	 * 
	* @Title: addMinSaleUnitPrice
	* @Description: 添加最小销售单位数据
	* @param    
	* @return void    
	* @throws
	 */
	public void addMinSaleUnitPrice(MinSaleUnitPrice minSaleUnitPrice) throws Exception{
		this.daoSupport.insert("DT_MinSaleUnitPrice", minSaleUnitPrice);
	}
	
	/**
	 * 
	* @Title: delMinSaleUnitPriceById
	* @Description: 根据id删除最小销售单位价格
	* @param    
	* @return void    
	* @throws
	 */
	public void delMinSaleUnitPriceById(Integer id) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("delete from DT_MinSaleUnitPrice where id = ?");
		this.daoSupport.execute(sql.toString(), id);
	}

	/**
	  * <p>Title: deleteUnitPriceByUnitCode</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @throws Exception
	  */ 
	@Override
	public void deleteUnitPriceByUnitCode(String unitCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from DT_MinSaleUnitPrice where minSaleUnitCode = ?");
		this.daoSupport.execute(sql.toString(), unitCode);
		
	}
}
