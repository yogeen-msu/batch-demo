package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.market.vo.SaleContractVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

@SuppressWarnings("rawtypes")
@Service
public class SaleContractServiceImpl extends BaseSupport implements ISaleContractService {

	private final static String TABLE_NAME = "DT_SaleContract";

	@SuppressWarnings("unchecked")
	public Page pageSaleContract(SaleContract saleContract, int pageNo, int pageSize) {
		String saleContractTbl = DBUtils.getTableName(SaleContract.class);

		StringBuffer sql = new StringBuffer("select tbl.*,c.name as languageName,b.autelId as  sealerAutelId from " + saleContractTbl + " tbl ,DT_SealerInfo b,DT_LanguageConfig c where tbl.sealerCode=b.code and tbl.languageCfgCode=c.code ");
		//查询条件
		StringBuffer term = new StringBuffer();
		if (saleContract != null) {
			
			//契约名称
			if(StringUtils.isNotEmpty(saleContract.getName())){
				term.append(" and tbl.name like '%"+saleContract.getName().trim()+"%' ");
			}
			
			//产品型号
			if(StringUtils.isNotEmpty(saleContract.getProTypeCode())){
				term.append(" and tbl.proTypeCode ='"+saleContract.getProTypeCode().trim()+"' ");
			}
			
			sql.append(term);
			
		}
		sql.append(" order by tbl.id asc ");

		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, SaleContract.class, new Object[] {});
		return page;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page pageSaleContractDetail(SaleContractVo saleContractvo, int pageNo, int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dsc.*,dsi.name as sealerName,dst.name as sealerTypeName ,dpt.name as productTypeName , dac.name as areaName ,dlc.name as languageCfgName ,dsf.name as saleName ,dsf2.name as maxSaleName from ");
		sql.append("DT_SaleContract dsc left join DT_SealerInfo dsi on dsc.sealerCode = dsi.code ");
		sql.append("left join DT_SealerType dst on dsc.sealerTypeCode = dst.code ");
		sql.append("left join DT_ProductForSealer dpt on dsc.proTypeCode = dpt.code ");
		sql.append("left join DT_AreaConfig dac on dsc.areaCfgCode = dac.code ");
		sql.append("left join DT_LanguageConfig dlc on dsc.languageCfgCode = dlc.code ");
		sql.append("left join DT_SaleConfig dsf on dsc.saleCfgCode = dsf.code ");
		sql.append("left join DT_SaleConfig dsf2 on dsc.maxSaleCfgCode = dsf2.code where 1=1 ");
		if (null != saleContractvo) {
			if (StringUtils.isNotBlank(saleContractvo.getName())) {
				sql.append(" and dsc.name like '%").append(saleContractvo.getName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getSealerName())) {
				sql.append(" and dsi.name like '%").append(saleContractvo.getSealerName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getSealerTypeName())) {
				sql.append(" and dst.name like '%").append(saleContractvo.getSealerTypeName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getProTypeCode())) {
				sql.append(" and dpt.code = '").append(saleContractvo.getProTypeCode().trim()).append("'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getAreaName())) {
				sql.append(" and dac.name like '%").append(saleContractvo.getAreaName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getLanguageCfgName())) {
				sql.append(" and dlc.name like '%").append(saleContractvo.getLanguageCfgName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getSaleName())) {
				sql.append(" and dsf.name like '%").append(saleContractvo.getSaleName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getMaxSaleName())) {
				sql.append(" and dsf2.name like '%").append(saleContractvo.getMaxSaleName().trim()).append("%'");
			}
		}

		sql.append(" order by dsc.id desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, SaleContractVo.class);
		return page;
	}
	
	
	@Override
	public Page pageSaleContractDetail2(SaleContractVo saleContractvo,Integer userId, int pageNo, int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dsc.*,dsi.name as sealerName,dst.name as sealerTypeName ,dpt.name as productTypeName , dac.name as areaName ,dlc.name as languageCfgName ,dsf.name as saleName ,dsf2.name as maxSaleName from ");
		sql.append(" DT_SaleContract dsc left join DT_SealerInfo dsi on dsc.sealerCode = dsi.code ");
		sql.append(" left join DT_SealerType dst on dsc.sealerTypeCode = dst.code ");
		sql.append(" left join DT_ProductForSealer dpt on dsc.proTypeCode = dpt.code ");
		sql.append(" left join DT_AreaConfig dac on dsc.areaCfgCode = dac.code ");
		sql.append(" left join DT_LanguageConfig dlc on dsc.languageCfgCode = dlc.code ");
		sql.append(" left join DT_SaleConfig dsf on dsc.saleCfgCode = dsf.code ");
		sql.append(" inner join DT_SaleContractAuth dsa on dsa.saleContractCode = dsc.code  and dsa.userId="+userId);
		sql.append(" left join DT_SaleConfig dsf2 on dsc.maxSaleCfgCode = dsf2.code where 1=1 ");
		if (null != saleContractvo) {
			if (StringUtils.isNotBlank(saleContractvo.getName())) {
				sql.append(" and dsc.name like '%").append(saleContractvo.getName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getSealerName())) {
				sql.append(" and dsi.name like '%").append(saleContractvo.getSealerName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getSealerTypeName())) {
				sql.append(" and dst.name like '%").append(saleContractvo.getSealerTypeName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getProTypeCode())) {
				sql.append(" and dpt.code = '").append(saleContractvo.getProTypeCode().trim()).append("'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getAreaName())) {
				sql.append(" and dac.name like '%").append(saleContractvo.getAreaName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getLanguageCfgName())) {
				sql.append(" and dlc.name like '%").append(saleContractvo.getLanguageCfgName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getSaleName())) {
				sql.append(" and dsf.name like '%").append(saleContractvo.getSaleName().trim()).append("%'");
			}
			if (StringUtils.isNotBlank(saleContractvo.getMaxSaleName())) {
				sql.append(" and dsf2.name like '%").append(saleContractvo.getMaxSaleName().trim()).append("%'");
			}
		}

		sql.append(" order by dsc.id desc");
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, SaleContractVo.class);
		return page;
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public SaleContractVo getSaleContractDetail(String saleContractCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		
		sql.append("select dsc.*,dsi.name as sealerName,dst.name as sealerTypeName ,dpt.name as productTypeName , dac.name as areaName ,dlc.name as languageCfgName ,dsf.name as saleName ,dsf2.name as maxSaleName from ");
		sql.append("DT_SaleContract dsc left join DT_SealerInfo dsi on dsc.sealerCode = dsi.code ");
		sql.append("left join DT_SealerType dst on dsc.sealerTypeCode = dst.code ");
		sql.append("left join DT_ProductForSealer dpt on dsc.proTypeCode = dpt.code ");
		sql.append("left join DT_AreaConfig dac on dsc.areaCfgCode = dac.code ");
		sql.append("left join DT_LanguageConfig dlc on dsc.languageCfgCode = dlc.code ");
		sql.append("left join DT_SaleConfig dsf on dsc.saleCfgCode = dsf.code ");
		sql.append("left join DT_SaleConfig dsf2 on dsc.maxSaleCfgCode = dsf2.code where dsc.code = ? ");
		
		return (SaleContractVo) this.daoSupport.queryForObject(sql.toString(), SaleContractVo.class, saleContractCode);
	}

	/**
	  * <p>Title: listAll</p>
	  * <p>Description: </p>
	  * @return
	  * @throws Exception
	  */
	@SuppressWarnings("unchecked")
	@Override
	public List<SaleContract> listAll() throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME);
		return this.daoSupport.queryForList(sql.toString(), SaleContract.class);
	}

	/**
	  * <p>Title: getSaleContractByCode</p>
	  * <p>Description: </p>
	  * @param code
	  * @return
	  * @throws Exception
	  */
	@Override
	public SaleContract getSaleContractByCode(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where code=?");
		return (SaleContract) this.daoSupport.queryForObject(sql.toString(), SaleContract.class, code);
	}

	/**
	  * <p>Title: addSaleContract</p>
	  * <p>Description: </p>
	  * @param saleContract
	  * @throws Exception
	  */
	@Override
	@Transactional
	public void addSaleContract(SaleContract saleContract) throws Exception {
		String code = DBUtils.generateCode(DBConstant.SALE_CONTRACT_CODE);
		saleContract.setCode(code);
		this.daoSupport.insert(TABLE_NAME, saleContract);

	}

	/**
	  * <p>Title: delSaleContract</p>
	  * <p>Description: </p>
	  * @param saleContract
	  * @throws Exception
	  */
	@Override
	public void delSaleContract(SaleContract saleContract) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code=?");
		this.daoSupport.execute(sql.toString(), saleContract.getCode().trim());

	}

	/**
	  * <p>Title: delSaleContractByCode</p>
	  * <p>Description: </p>
	  * @param code
	  * @throws Exception
	  */
	@Override
	public void delSaleContractByCode(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code=?");
		this.daoSupport.execute(sql.toString(), code);

	}

	/**
	  * <p>Title: updateSaleContract</p>
	  * <p>Description: </p>
	  * @param saleContract
	  * @throws Exception
	  */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void updateSaleContract(SaleContract saleContract) throws Exception {
		this.daoSupport.update(TABLE_NAME, saleContract, " code='" + saleContract.getCode() + "'");
	}

	/**
	  * <p>Title: checkRepeat</p>
	  * <p>Description: 检查是否有相同的销售契约</p>
	  * @param saleContract
	  * @return  true 重复   false  不重复
	  * @throws Exception
	  */
	@Override
	public boolean checkRepeat(SaleContract saleContract) throws Exception {

		StringBuffer sql = new StringBuffer();

		//		if(StringUtils.isNotBlank(saleContract.getSealerCode())){
		//			sql.append(" and sealerCode = ? ");
		//		}
		//		
		//		if(StringUtils.isNotBlank(saleContract.getSealerTypeCode())){
		//			sql.append(" and sealerTypeCode = ? ");
		//		}
		//		
		//		if(StringUtils.isNotBlank(saleContract.getProTypeCode())){
		//			sql.append(" and proTypeCode = ? ");
		//		}
		//		
		//		if(StringUtils.isNotBlank(saleContract.getAreaCfgCode())){
		//			sql.append(" and areaCfgCode = ? ");
		//		}
		//		
		//		if(StringUtils.isNotBlank(saleContract.getLanguageCfgCode())){
		//			sql.append(" and languageCfgCode = ? ");
		//		}
		//		
		//		if(StringUtils.isNotBlank(saleContract.getSaleCfgCode())){
		//			sql.append(" and saleCfgCode = ? ");
		//		}
		//		
		//		if(StringUtils.isNotBlank(saleContract.getMaxSaleCfgCode())){
		//			sql.append(" and maxSaleCfgCode = ? ");

		//		}
		if (null != saleContract) {
			sql.append("select * from ").append(TABLE_NAME).append(" where name = ? ");
			if(StringUtils.isNotBlank(saleContract.getCode())){
				sql.append(" and code !=  '").append(saleContract.getCode().trim()).append("' ");
			}
			List list = this.daoSupport.queryForList(sql.toString(), SaleContract.class, saleContract.getName().trim());
			if (null != list && list.size() > 0) {
				return true;
			}
			
			sql.append(" and sealerCode = ? ").append(" and sealerTypeCode = ? ");
			sql.append(" and proTypeCode = ? ").append(" and areaCfgCode = ? ");
			sql.append(" and languageCfgCode = ? ").append(" and saleCfgCode = ? ");
			sql.append(" and maxSaleCfgCode = ?");

			list = this.daoSupport.queryForList(sql.toString(), SaleContract.class, saleContract.getName().trim(), saleContract.getSealerCode()
					.trim(), saleContract.getSealerTypeCode(), saleContract.getProTypeCode().trim(), saleContract.getAreaCfgCode().trim(),
					saleContract.getLanguageCfgCode().trim(),saleContract.getSaleCfgCode().trim(),saleContract.getMaxSaleCfgCode().trim());
			
			if (null != list && list.size() > 0) {
				return true;
			}
		}

		return false;
	}
	public  List<SaleContract> getContrctByProCode(String proCode) throws Exception{
		
		StringBuffer sql = new StringBuffer();
		sql.append("select code,name from ").append(TABLE_NAME).append(" where proTypeCode=?");
		return this.daoSupport.queryForList(sql.toString(), SaleContract.class,proCode);
	}
	public  List<SaleContract> getContrctList(SaleContract saleContract) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select code,name from ").append(TABLE_NAME)
		   .append(" where sealerCode='"+saleContract.getSealerCode()+"'")
		   .append(" and proTypeCode='"+saleContract.getProTypeCode()+"'")
		   .append(" and areaCfgCode='"+saleContract.getAreaCfgCode()+"'")
		   .append(" and languageCfgCode='"+saleContract.getLanguageCfgCode()+"'")
		   .append(" and saleCfgCode='"+saleContract.getSaleCfgCode()+"'");
		  
		return this.daoSupport.queryForList(sql.toString(), SaleContract.class);
	}
	
	public  List<SaleContract> getContrctBySealer(SaleContract saleContract) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME);
		sql.append(" where sealerCode='"+saleContract.getSealerCode()+"'");
		sql.append(" and proTypeCode='"+saleContract.getProTypeCode()+"'");
		if(!StringUtil.isBlank(saleContract.getAreaCfgCode())){
			sql.append(" and areaCfgCode='"+saleContract.getAreaCfgCode()+"'");
		}
		sql.append(" and languageCfgCode='"+saleContract.getLanguageCfgCode()+"'");
		if(!StringUtil.isBlank(saleContract.getSaleCfgCode())){
		   sql.append(" and saleCfgCode='"+saleContract.getSaleCfgCode()+"'");
		  }
		  
		return this.daoSupport.queryForList(sql.toString(), SaleContract.class);
	}
	
	public  List<SaleContract> getContrctBySealerAutelId(String sealerAutelId) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select a.* from  dt_salecontract a ,dt_sealerinfo b");
		sql.append(" where a.sealerCode = b.code");
		sql.append(" and b.autelId='").append(sealerAutelId).append("'");
		return this.daoSupport.queryForList(sql.toString(), SaleContract.class);
	}
	public List<SaleContract> getSaleContract(SaleContract saleContract) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME);
		sql.append(" where sealerCode='"+saleContract.getSealerCode()+"'");
		sql.append(" and proTypeCode='"+saleContract.getProTypeCode()+"'");
		sql.append(" and saleCfgCode='"+saleContract.getSaleCfgCode()+"'");
		sql.append(" and maxSaleCfgCode='"+saleContract.getMaxSaleCfgCode()+"'");
		sql.append(" and languageCfgCode='"+saleContract.getLanguageCfgCode()+"'");
		sql.append(" and upMonth='"+saleContract.getUpMonth()+"'");
		sql.append(" and warrantyMonth='"+saleContract.getWarrantyMonth()+"'");
		return this.daoSupport.queryForList(sql.toString(), SaleContract.class);
	}

	/**
	 * A16043	hlh
	 * 提供给无人机APP的接口，可根据传入的条件进行查询
	 */
	@Override
	public List<SaleContract> getSaleContractUAV(String contractName,
			String contractCode, String startTime, String endTime)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME);
		sql.append(" where 1=1");
		if(null !=contractName && (!contractName.trim().equals(""))){
			sql.append(" and name like '%"+contractName+"%'");
		}
		if(null !=contractCode && (!contractCode.trim().equals(""))){
			sql.append(" and code='"+contractCode+"'");
		}
		if(null !=startTime && (!startTime.trim().equals(""))){
			sql.append(" and ( createtime >='"+startTime+"' or updateTime >='"+startTime+"' )");
		}
		if(null !=endTime && (!endTime.trim().equals(""))){
			sql.append(" and ( createtime <='"+endTime+"' or updateTime <='"+endTime+"' )");
		}
		return this.daoSupport.queryForList(sql.toString(), SaleContract.class);
		
	}

	/**
	 * 根据时间获取改最后修改时间在此时间之后的所有销售契约 ,给SAP调用的接口函数
	 */
	@Override
	public List<SaleContract> getContrctByDate(String startTime)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME);
		sql.append(" where ( createtime >'"+startTime+"' and updateTime is null )");
		sql.append(" or (updateTime > '"+startTime+"')");
		return this.daoSupport.queryForList(sql.toString(), SaleContract.class);
	}
	
	
}
