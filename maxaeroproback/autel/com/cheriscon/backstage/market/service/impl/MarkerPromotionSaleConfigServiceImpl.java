/**
 */
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.service.IMarkerPromotionSaleConfigService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MarkerPromotionSaleConfig;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MarkerPromotionSaleConfigServiceImpl
 * @Description: 营销活动参与销售配置接口服务实现类
 * @author shaohu
 * @date 2013-3-10 下午05:43:22
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MarkerPromotionSaleConfigServiceImpl extends BaseSupport implements
		IMarkerPromotionSaleConfigService {

	private final static String TABLE_NAME = "DT_MarketPromotionSaleConfig";

	@Override
	@Transactional
	public void batchAddMarkerPromotionSaleConfig(String promotionCode,
			List<MarkerPromotionSaleConfig> markerPromotionSaleConfigs)
			throws Exception {
		int i = 0;
		for (MarkerPromotionSaleConfig config : markerPromotionSaleConfigs) {
			config.setMarketPromotionCode(promotionCode);
			String code = DBUtils.generateCode(
					DBConstant.PROMOTION_SALECONFIG_CODE, String.valueOf(i));
			config.setCode(code);
			this.daoSupport.insert(TABLE_NAME, config);
			i++;
		}
	}

	/**
	 * <p>
	 * Title: getSaleConfigByPromotionCode
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param promotionCode
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MarkerPromotionSaleConfig> getSaleConfigByPromotionCode(
			String promotionCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dp.*,dt.name as productTypeName from DT_MarketPromotionSaleConfig dp left join DT_ProductForSealer dt on dp.productTypeCode = dt.code where marketPromotionCode=?");
		return this.daoSupport.queryForList(sql.toString(),
				MarkerPromotionSaleConfig.class, promotionCode);
	}

	/**
	 * <p>
	 * Title: getAdvSaleConfig
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param promotionCode
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SaleConfig> getAdvSaleConfig(String promotionCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dsc.*,dpt.name as proTypeName from DT_SaleConfig dsc left join DT_ProductForSealer dpt on dsc.proTypeCode = dpt.code where dsc.code not in(select saleConfigCode from DT_MarketPromotionSaleConfig where marketPromotionCode=? ) order by dsc.id desc");
		return this.daoSupport.queryForList(sql.toString(), SaleConfig.class,
				promotionCode);
	}
	
	/**
	 * 
	* @Title: delPromotionSaleConfigById
	* @Description: 根据id删除营销活动配置
	* @param    
	* @return void    
	* @throws
	 */
	public void delPromotionSaleConfigById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);
		
	}
}
