/**
 */
package com.cheriscon.backstage.market.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.market.service.IMarkerPromotionRuleService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MarkerPromotionRule;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MarkerPromotionRuleServiceImpl
 * @Description: 营销活动规则实现类
 * @author shaohu
 * @date 2013-1-22 下午03:05:00
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MarkerPromotionRuleServiceImpl extends BaseSupport implements
		IMarkerPromotionRuleService {

	private final static String TABLE_NAME = "DT_MarketPromotionRule";

	/**
	 * 
	 * <p>
	 * Title: getMarkerPromotionRuleByPromotionCode
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param promotionCode
	 *            营销活动编码
	 * @return
	 * @throws Exception
	 */
	@Override
	public MarkerPromotionRule getMarkerPromotionRuleByPromotionCode(
			String promotionCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME)
				.append(" where marketPromotionCode=?");
		return (MarkerPromotionRule) this.daoSupport.queryForObject(
				sql.toString(), MarkerPromotionRule.class, promotionCode);
	}

	/**
	 * 
	 * @Title: addMarkerPromotionRule
	 * @Description: 添加营销活动规则
	 * @param
	 * @return void
	 * @throws
	 */
	@Override
	public String addMarkerPromotionRule(MarkerPromotionRule markerPromotionRule)
			throws Exception {
		String code = DBUtils.generateCode(DBConstant.PROMOTION_RULE);
		markerPromotionRule.setCode(code);
		this.daoSupport.insert(TABLE_NAME, markerPromotionRule);
		return code;
	}

	/**
	 * <p>
	 * Title: updateMarkerPromotionRule
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param markerPromotionRule
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateMarkerPromotionRule(
			MarkerPromotionRule markerPromotionRule) throws Exception {
		this.daoSupport.update(TABLE_NAME, markerPromotionRule, " code='"
				+ markerPromotionRule.getCode() + "'");
	}

	/**
	  * <p>Title: delRuleByPromotionCode</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @throws Exception
	  */ 
	@Override
	public void delRuleByPromotionCode(String promotionCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where marketPromotionCode=?");
		this.daoSupport.execute(sql.toString(), promotionCode);
		
	}
}
