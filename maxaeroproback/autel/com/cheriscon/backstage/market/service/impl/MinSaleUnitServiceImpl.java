/**
*/ 
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.mapper.MinSaleUnitVoMapper;
import com.cheriscon.backstage.market.service.IMinSaleUnitPriceService;
import com.cheriscon.backstage.market.service.IMinSaleUnitService;
import com.cheriscon.backstage.market.service.IMinSaleUnitSoftwareDetailService;
import com.cheriscon.backstage.market.service.IMinSalesUnitMemoService;
import com.cheriscon.backstage.market.vo.MinSaleUnitSearch;
import com.cheriscon.backstage.market.vo.MinSaleUnitVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: MinSaleUnitServiceImpl
 * @Description: 最小销售单位业务实现类
 * @author shaohu
 * @date 2013-1-14 下午06:17:24
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MinSaleUnitServiceImpl extends BaseSupport implements IMinSaleUnitService {

	private final static String TABLE_NAME="DT_MinSaleUnit";
	
	@Resource
	private IMinSalesUnitMemoService minSalesUnitMemoService;
	
	@Resource
	private IMinSaleUnitPriceService minSaleUnitPriceService;
	
	@Resource
	private IMinSaleUnitSoftwareDetailService softwareDetailService;
	
	/**
	  * <p>Title: pageMinSaleUnit</p>
	  * <p>Description: 根据条件分页查询最小销售单位数据</p>
	  * @param minSaleUnitSearch
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageMinSaleUnit(MinSaleUnitSearch minSaleUnitSearch,
			int pageNo, int pageSize) throws Exception {
		//语言编码lag201301070146010554
		StringBuffer sql = new StringBuffer();
		sql.append("select du.ID,du.code,du.proTypeCode,du.picPath,ds.name as name,dt.name as productTypeName from DT_MinSaleUnit du right join DT_MinSaleUnitMemo ds on du.code = ds.minSaleUnitCode left join DT_ProductType dt on du.proTypeCode = dt.code where ds.languageCode = ? ");
		if(null != minSaleUnitSearch){
			if(StringUtils.isNotBlank(minSaleUnitSearch.getProductTypeCode())){
				sql.append(" and du.proTypeCode='").append(minSaleUnitSearch.getProductTypeCode()).append("'");
			}
			if(StringUtils.isNotBlank(minSaleUnitSearch.getName())){
				sql.append(" and ds.name like ").append("'%").append(minSaleUnitSearch.getName()).append("%'");
			}
		}
		sql.append(" order by du.ID desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, new MinSaleUnitVoMapper(), minSaleUnitSearch.getLanguageCode());
	}
	
	/**
	 * 
	  * <p>Title: listAll</p>
	  * <p>Description: 查询所有的最小销售单位</p>
	  * @param languageCode	语言编码
	  * @return
	  * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitVo> listAll(String languageCode) throws Exception{
		//语言编码lag201301070146010554
		StringBuffer sql = new StringBuffer();
		sql.append("select du.ID,du.code,du.proTypeCode,du.picPath,ds.name as name,dt.name as productTypeName from DT_MinSaleUnit du right join DT_MinSaleUnitMemo ds on du.code = ds.minSaleUnitCode left join DT_ProductType dt on du.proTypeCode = dt.code where ds.languageCode = ? ");
		sql.append(" order by du.ID desc");
		return this.daoSupport.queryForList(sql.toString(), MinSaleUnitVo.class, languageCode);
	}
	
	/**
	 * 
	  * <p>Title: getMinSaleUnitByCode</p>
	  * <p>Description: 根据最小销售单位的编码 查询</p>
	  * @param code 最小销售单位编码
	  * @return
	  * @throws Exception
	 */
	@Override
	public MinSaleUnit getMinSaleUnitByCode(String code) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where code=?");
		return (MinSaleUnit) this.daoSupport.queryForObject(sql.toString(), MinSaleUnit.class, code);
	}
	
	/**
	  * <p>Title: addMinSaleUnit</p>
	  * <p>Description: 添加最小销售单位</p>
	  * @param minSaleUnit
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void addMinSaleUnit(MinSaleUnit minSaleUnit,MinSaleUnitMemo minSaleUnitMemo) throws Exception {
		String code = DBUtils.generateCode(DBConstant.MIN_SALE_UNIT_CODE);
		minSaleUnit.setCode(code);
		minSaleUnitMemo.setMinSaleUnitcode(code);
		this.daoSupport.insert(TABLE_NAME, minSaleUnit);
		minSaleUnitMemo.setMemo(minSaleUnitMemo.getName());
		this.minSalesUnitMemoService.addMinSaleUnitMemo(minSaleUnitMemo);
		
	}

	/**
	  * <p>Title: updateMinSaleUnit</p>
	  * <p>Description: 修改最小销售单位</p>
	  * @param minSaleUnit
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void updateMinSaleUnit(MinSaleUnit minSaleUnit,MinSaleUnitMemo minSaleUnitMemo) throws Exception {
		this.daoSupport.update(TABLE_NAME, minSaleUnit, " code="+"'"+minSaleUnit.getCode()+"'");
		this.minSalesUnitMemoService.updateMinSaleUnitMemoByLanguage(minSaleUnitMemo);
	}

	/**
	  * <p>Title: delMinSaleUnit</p>
	  * <p>Description:删除最小销售单位 </p>
	  * @param code
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void delMinSaleUnit(String code) throws Exception {
		
		//删除最小说明数据
		this.minSalesUnitMemoService.delMinSaleUnitMemoByMinSaleUnitCode(code);
		
		this.minSaleUnitPriceService.deleteUnitPriceByUnitCode(code);
		
		this.softwareDetailService.deleteSoftwareDeatilByUnitCode(code);
		
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code = ?");
		this.daoSupport.execute(sql.toString(), code);
	}

}
