/**
 */
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.market.service.IMarkerPromotionAreaRuleService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.MarkerPromotionAreaRule;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MarkerPromotionAreaRuleServiceImpl
 * @Description: 营销活动区域实现类
 * @author shaohu
 * @date 2013-1-22 下午03:44:34
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MarkerPromotionAreaRuleServiceImpl extends BaseSupport implements
		IMarkerPromotionAreaRuleService {

	private final static String TABLE_NAME = "DT_MarketPromotionAreaRule";

	/**
	 * 
	  * <p>Title: getPromotionAreaByPromotionCode</p>
	  * <p>Description: 根据营销活动编码查询营销活动区域信息</p>
	  * @param promotionCode	营销活动编码
	  * @return
	  * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MarkerPromotionAreaRule> getPromotionAreaByPromotionCode(
			String promotionCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dp.*,dc.name as areaName from DT_MarketPromotionAreaRule dp left join DT_AreaConfig dc on dp.areaCode = dc.code where marketPromotionCode=? ");
		//sql.append("select * from ").append(TABLE_NAME).append(" where marketPromotionCode=?");
		return this.daoSupport.queryForList(sql.toString(), MarkerPromotionAreaRule.class,promotionCode);
	}

	/**
	 * <p>
	 * Title: addMarkerPromotionAreaRule
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param areaRule
	 * @throws Exception
	 */
	@Override
	public void addMarkerPromotionAreaRule(MarkerPromotionAreaRule areaRule)
			throws Exception {
		String code = DBUtils.generateCode(DBConstant.PROMOTION_AREA_CODE);
		areaRule.setCode(code);
		this.daoSupport.insert(TABLE_NAME, areaRule);
	}

	/**
	 * <p>
	 * Title: batchAddMarkerPromotionAreaRule
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param promotionCode
	 * @param areaRules
	 * @throws Exception
	 */
	@Override
	public void batchAddMarkerPromotionAreaRule(String promotionCode,
			List<MarkerPromotionAreaRule> areaRules) throws Exception {
		int i = 0;
		for (MarkerPromotionAreaRule areaRule : areaRules) {
			areaRule.setMarketPromotionCode(promotionCode);
			String code = DBUtils.generateCode(DBConstant.PROMOTION_AREA_CODE,
					String.valueOf(i));
			areaRule.setCode(code);
			this.daoSupport.insert(TABLE_NAME, areaRule);
			i++;
		}

	}

	/**
	  * <p>Title: getAdvAreaConfig</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @return
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	public List<AreaConfig> getAdvAreaConfig(String promotionCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_AreaConfig da where da.code not in (select areaCode from DT_MarketPromotionAreaRule where marketPromotionCode = ?) ").append(" order by da.id asc ");
		return this.daoSupport.queryForList(sql.toString(), AreaConfig.class,promotionCode);
	}

	/**
	  * <p>Title: delMarkerPromotionAreaRuleById</p>
	  * <p>Description: </p>
	  * @param id
	  * @throws Exception
	  */ 
	@Override
	public void delMarkerPromotionAreaRuleById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);
	}

	/**
	  * <p>Title: delMarkerPromotionAreaByPromotionCode</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @throws Exception
	  */ 
	@Override
	public void delMarkerPromotionAreaByPromotionCode(String promotionCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where marketPromotionCode=?");
		this.daoSupport.execute(sql.toString(), promotionCode);
		
	}

	/**
	  * <p>Title: addMarkerPromotionAreaRuleAll</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @throws Exception
	  */ 
	@Override
	public void addMarkerPromotionAreaRuleAll(String promotionCode)
			throws Exception {
		String code = DBUtils.generateCode(DBConstant.PROMOTION_AREA_CODE);
		MarkerPromotionAreaRule areaRule = new MarkerPromotionAreaRule();
		areaRule.setCode(code);
		areaRule.setMarketPromotionCode(promotionCode);
		areaRule.setAreaCode(DBConstant.IS_ALL);
		this.daoSupport.insert(TABLE_NAME, areaRule);
	}

	/**
	  * <p>Title: delMarkerPromotionAreaRuleAll</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @throws Exception
	  */ 
	@Override
	public void delMarkerPromotionAreaRuleAll(String promotionCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME)
				.append(" where marketPromotionCode=? ")
				.append(" and areaCode = ?");
		this.daoSupport.execute(sql.toString(), promotionCode, DBConstant.IS_ALL);
	}

}
