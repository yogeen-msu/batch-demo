/**
*/ 
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.service.IMinSaleUnitSaleCfgDetailService;
import com.cheriscon.common.model.MinSaleUnitSaleCfgDetail;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MinSaleUnitSaleCfgDetailServiceImpl
 * @Description: 销售配置和最小销售单位中间表实现类
 * @author shaohu
 * @date 2013-1-17 下午02:01:01
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MinSaleUnitSaleCfgDetailServiceImpl extends BaseSupport implements IMinSaleUnitSaleCfgDetailService{
	
	private final static String TABLE_NAME = "DT_MinSaleUnitSaleCfgDetail";
	

	/**
	  * <p>Title: addMinSaleUnitSaleCfgDetail</p>
	  * <p>Description: </p>
	  * @param cfgDetail
	  * @throws Exception
	  */ 
	@Override
	public void addMinSaleUnitSaleCfgDetail(MinSaleUnitSaleCfgDetail cfgDetail)
			throws Exception {
		this.daoSupport.insert(TABLE_NAME, cfgDetail);
		
	}

	/**
	  * <p>Title: delMinSaleUnitSaleCfgDetailById</p>
	  * <p>Description: </p>
	  * @param id
	  * @throws Exception
	  */ 
	@Override
	public void delMinSaleUnitSaleCfgDetailById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);
	}

	/**
	  * <p>Title: delMinSaleUnitSaleCfgDetailByCode</p>
	  * <p>Description: </p>
	  * @param code
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void delMinSaleUnitSaleCfgDetailByCode(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where saleCfgCode=?");
		this.daoSupport.execute(sql.toString(), code);
		
	}

	/**
	  * <p>Title: getMinSaleUnitSaleCfgListByCode</p>
	  * <p>Description: </p>
	  * @param saleCode
	  * @return
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitSaleCfgDetail> getMinSaleUnitSaleCfgListByCode(String saleCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where saleCfgCode=?");
		return this.daoSupport.queryForList(sql.toString(), MinSaleUnitSaleCfgDetail.class,saleCode);
	}
	
	
}
