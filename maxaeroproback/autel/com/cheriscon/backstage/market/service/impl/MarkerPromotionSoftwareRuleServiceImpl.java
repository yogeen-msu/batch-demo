/**
*/ 
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.market.service.IMarkerPromotionSoftwareRuleService;
import com.cheriscon.backstage.market.vo.MinSaleUnitVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MarkerPromotionSoftwareRule;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MarkerPromotionSoftwareRuleServiceImpl
 * @Description: 营销活动参与最小销售单位实现类
 * @author shaohu
 * @date 2013-1-22 下午03:10:30
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MarkerPromotionSoftwareRuleServiceImpl extends BaseSupport implements
		IMarkerPromotionSoftwareRuleService {

	private final static String TABLE_NAME="DT_MarketPromotionSoftwareRule";
	
	
	
	/**
	 * 
	  * <p>Title: getSoftwareListByPromotionCode</p>
	  * <p>Description: </p>
	  * @param promotionCode	营销活动编码
	  * @return
	  * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MarkerPromotionSoftwareRule> getSoftwareListByPromotionCode(String promotionCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select dt.*,dp.name as productTypeName from  DT_MarketPromotionSoftwareRule dt left join DT_ProductType dp on dt.productTypeCode = dp.code where marketPromotionCode=? order by dt.id ");
		return this.daoSupport.queryForList(sql.toString(), MarkerPromotionSoftwareRule.class,promotionCode);
	}
	
	/**
	  * <p>Title: addMarkerPromotionSoftwareRule</p>
	  * <p>Description: </p>
	  * @param softwareRule	
	  * @param promotionCode 	活动编码
	  * @throws Exception
	  */ 
	@Override
	public void addMarkerPromotionSoftwareRule(String promotionCode,
			MarkerPromotionSoftwareRule softwareRule) throws Exception {
		String code = DBUtils.generateCode(DBConstant.PROMOTION_SOFTWARE_CODE);
		softwareRule.setCode(code);
		softwareRule.setMarketPromotionCode(promotionCode);
		this.daoSupport.insert(TABLE_NAME, softwareRule);
	}
	
	
	/**
	 * 
	* @Title: addMarkerPromotionSoftwareRuleAll
	* @Description: 添加标示所有最小销售单位有效的数据
	* @param    
	* @return void    
	* @throws
	 */
	@Override
	public void addMarkerPromotionSoftwareRuleAll(String promotionCode) throws Exception {
		String code = DBUtils.generateCode(DBConstant.PROMOTION_SOFTWARE_CODE);
		MarkerPromotionSoftwareRule softwareRule = new MarkerPromotionSoftwareRule();
		softwareRule.setCode(code);
		softwareRule.setMarketPromotionCode(promotionCode);
		softwareRule.setMinSaleUnitCode(DBConstant.IS_ALL);
		this.daoSupport.insert(TABLE_NAME, softwareRule);
	}

	/**
	 * 
	* @Title: delPromotionMinsaleUnitByPromotionCodeAndStatus
	* @Description: 删除标准所有的那条数据
	* @param    
	* @return void    
	* @throws
	 */
	@Override
	public void delPromotionMinsaleUnitByPromotionCodeALL(
			String promotionCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME)
				.append(" where marketPromotionCode=? ")
				.append(" and minSaleUnitCode = ?");
		this.daoSupport.execute(sql.toString(), promotionCode, DBConstant.IS_ALL);

	}
	
	/**
	  * <p>Title: batchAddMarkerPromotionSoftwareRule</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @param softwareRules
	  * @throws Exception
	  */ 
	@Override
	public void batchAddMarkerPromotionSoftwareRule(String promotionCode,
			List<MarkerPromotionSoftwareRule> softwareRules) throws Exception {
		int i = 0;
		for(MarkerPromotionSoftwareRule softwareRule : softwareRules){
			softwareRule.setMarketPromotionCode(promotionCode);
			String code = DBUtils.generateCode(DBConstant.PROMOTION_SOFTWARE_CODE,String.valueOf(i));
			softwareRule.setCode(code);
			this.daoSupport.insert(TABLE_NAME, softwareRule);
			i++;
		}
		
	}

	/**
	  * <p>Title: getAdvMinSaleUnit</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @return
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitVo> getAdvMinSaleUnit(String promotionCode,String languageCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select du.ID,du.code,du.proTypeCode,du.picPath,ds.name as name,dt.name as productTypeName from DT_MinSaleUnit du right join DT_MinSaleUnitMemo ds on du.code = ds.minSaleUnitCode left join DT_ProductType dt on du.proTypeCode = dt.code where ds.languageCode = ? and  du.code not in(select minSaleUnitCode from DT_MarketPromotionSoftwareRule where marketPromotionCode=? )");
		return this.daoSupport.queryForList(sql.toString(),MinSaleUnitVo.class, languageCode,promotionCode);
	}

	/**
	  * <p>Title: delPromotionMinsaleUnitById</p>
	  * <p>Description: </p>
	  * @param id
	  * @throws Exception
	  */ 
	@Override
	public void delPromotionMinsaleUnitById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);
		
	}

	/**
	  * <p>Title: delPromotionMinsaleUnitByPromotionCode</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @throws Exception
	  */ 
	@Override
	public void delPromotionMinsaleUnitByPromotionCode(String promotionCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where marketPromotionCode=?");
		this.daoSupport.execute(sql.toString(), promotionCode);
		
	}
	
	

}
