/**
*/ 
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cheriscon.backstage.market.service.ISaleContractTypeService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.SaleContractType;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: SaleContractTypeServiceImpl
 * @Description: 销售契约目录业务实现类
 * @author shaohu
 * @date 2013-1-17 上午09:11:38
 * 
 */
@Service
public class SaleContractTypeServiceImpl extends BaseSupport<SaleContractType> implements ISaleContractTypeService {

	private final static String TABLE_NAME = "DT_SaleContractType";
	
	/**
	  * <p>Title: pageSaleContractType</p>
	  * <p>Description: </p>
	  * @param saleContractType
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageSaleContractType(SaleContractType saleContractType,int pageNo,int pageSize)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where 1=1");
		if(null != saleContractType){
			if(StringUtils.isNotBlank(saleContractType.getName())){
				sql.append(" and name=").append("'%").append(saleContractType.getName().trim()).append("%'");
			}
		}
		sql.append(" order by id desc");
		return this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize);
	}

	/**
	  * <p>Title: listAll</p>
	  * <p>Description: </p>
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<SaleContractType> listAll() throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME);
		return this.daoSupport.queryForList(sql.toString(), SaleContractType.class);
	}

	/**
	  * <p>Title: getSaleContractTypeByCode</p>
	  * <p>Description: </p>
	  * @param code
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public SaleContractType getSaleContractTypeByCode(String code)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" code=?");
		return this.daoSupport.queryForObject(sql.toString(), SaleContractType.class, code);
	}

	/**
	  * <p>Title: addSaleContractType</p>
	  * <p>Description: </p>
	  * @param saleContractType
	  * @throws Exception
	  */ 
	@Override
	public void addSaleContractType(SaleContractType saleContractType)
			throws Exception {
		String code = DBUtils.generateCode(DBConstant.SALE_CONTRACT_TYPE_CODE);
		saleContractType.setCode(code);
		this.daoSupport.insert(TABLE_NAME, saleContractType);
	}

	/**
	  * <p>Title: delSaleContractType</p>
	  * <p>Description: </p>
	  * @param saleContractType
	  * @throws Exception
	  */ 
	@Override
	public void delSaleContractType(SaleContractType saleContractType)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code=?");
		this.daoSupport.execute(sql.toString(), saleContractType.getCode());
	}

	/**
	  * <p>Title: delSaleContractTypeByCode</p>
	  * <p>Description: 删除销售契约</p>
	  * @param code
	  * @throws Exception
	  */ 
	@Override
	public void delSaleContractTypeByCode(String code) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where code=?");
		this.daoSupport.execute(sql.toString(), code);
	}

}
