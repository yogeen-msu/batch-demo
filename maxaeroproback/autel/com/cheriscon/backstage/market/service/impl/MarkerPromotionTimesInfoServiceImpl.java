/**
*/ 
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.market.service.IMarkerPromotionTimesInfoService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.MarkerPromotionTimesInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MarkerPromotionTimesInfoServiceImpl
 * @Description: 营销活动时间段实现类
 * @author shaohu
 * @date 2013-1-22 下午03:08:55
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MarkerPromotionTimesInfoServiceImpl extends BaseSupport implements
		IMarkerPromotionTimesInfoService {

	private final static String TABLE_NAME = "DT_MarketPromotionTimesInfo";
	
	
	/**
	 * 
	  * <p>Title: getTimesInfoListByPromotionCode</p>
	  * <p>Description:根据营销活动编码查询营销活动时间段信息 </p>
	  * @param promotionCode	营销活动编码
	  * @return
	  * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MarkerPromotionTimesInfo> getTimesInfoListByPromotionCode(String promotionCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME).append(" where marketPromotionCode=?");
		return this.daoSupport.queryForList(sql.toString(), MarkerPromotionTimesInfo.class,promotionCode);
	}
	
	/**
	  * <p>Title: addMarkerPromotionTimesInfo</p>
	  * <p>Description: </p>
	  * @param timesInfo
	  * @throws Exception
	  */ 
	@Override
	public void addMarkerPromotionTimesInfo(MarkerPromotionTimesInfo timesInfo)
			throws Exception {
		String code = DBUtils.generateCode(DBConstant.PROMOTION_TIME_CODE);
		timesInfo.setCode(code);
		this.daoSupport.insert(TABLE_NAME, timesInfo);
		
	}

	/**
	  * <p>Title: batchAddMarkerPromotionTimesInfo</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @param timesInfos
	  * @throws Exception
	  */ 
	@Override
	public void batchAddMarkerPromotionTimesInfo(String promotionCode,
			List<MarkerPromotionTimesInfo> timesInfos) throws Exception {
		int i = 0;
		for(MarkerPromotionTimesInfo timesInfo : timesInfos){
			timesInfo.setMarketPromotionCode(promotionCode);
			String code = DBUtils.generateCode(DBConstant.PROMOTION_TIME_CODE,String.valueOf(i));
			timesInfo.setCode(code);
			this.daoSupport.insert(TABLE_NAME, timesInfo);
			i ++;
		}
		
	}

	/**
	  * <p>Title: delPromotionTimesInfoById</p>
	  * <p>Description: </p>
	  * @param id
	  * @throws Exception
	  */ 
	@Override
	public void delPromotionTimesInfoById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);
		
	}

	/**
	  * <p>Title: delTimesInfoByPromotionCode</p>
	  * <p>Description: </p>
	  * @param promotionCode
	  * @throws Exception
	  */ 
	@Override
	public void delTimesInfoByPromotionCode(String promotionCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME).append(" where marketPromotionCode=?");
		this.daoSupport.execute(sql.toString(), promotionCode);
		
	}

}
