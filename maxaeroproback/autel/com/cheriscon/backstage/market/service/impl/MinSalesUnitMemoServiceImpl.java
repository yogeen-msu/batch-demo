/**
 */
package com.cheriscon.backstage.market.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.service.IMinSalesUnitMemoService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MinSalesUnitMemoServiceImpl
 * @Description: 最小销售单位业务实现类
 * @author shaohu
 * @date 2013-1-15 下午03:15:35
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MinSalesUnitMemoServiceImpl extends BaseSupport implements
		IMinSalesUnitMemoService {

	private final static String TABLE_NAME = "DT_MinSaleUnitMemo";

	@Resource
	private ILanguageService languageService;
	
	/**
	 * 
	  * <p>Title: getMinSaleUnitMemoListByUnitCode</p>
	  * <p>Description: 查询最小单位说明数据 根据最小销售单位编码</p>
	  * @param uintCode 最小销售单位编码
	  * @return
	  * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitMemo> getMinSaleUnitMemoListByUnitCode(String uintCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select dm.*,dl.name languageName from ").append(TABLE_NAME).append(" dm left join dt_language dl on dm.languageCode = dl.code ").append(" where dm.minSaleUnitcode=?").append(" order by dm.id desc");
		return this.daoSupport.queryForList(sql.toString(), MinSaleUnitMemo.class, uintCode);
	}
	
	/**
	 * <p>
	 * Title: delMinSaleUnitMemoByMinSaleUnitCode
	 * </p>
	 * <p>
	 * Description: 根据最小销售单位编码 删除最小销售单位说明
	 * </p>
	 * 
	 * @param code
	 * @throws Exception
	 */
	@Override
	public void delMinSaleUnitMemoByMinSaleUnitCode(String code)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME)
				.append(" where minSaleUnitcode=?");
		this.daoSupport.execute(sql.toString(), code);

	}

	/**
	 * <p>
	 * Title: addMinSaleUnitMemo
	 * </p>
	 * <p>
	 * Description: 添加最小销售单位说明
	 * </p>
	 * 
	 * @param minSaleUnitMemo
	 * @throws Exception
	 */
	@Override
	public void addMinSaleUnitMemo(MinSaleUnitMemo minSaleUnitMemo)
			throws Exception {
		this.daoSupport.insert(TABLE_NAME, minSaleUnitMemo);
	}

	/**
	 * <p>
	 * Title: updateMinSaleUnitMemoByLanguage
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param minSaleUnitMemo
	 * @throws Exception
	 */
	@Override
	public void updateMinSaleUnitMemoByLanguage(MinSaleUnitMemo minSaleUnitMemo)
			throws Exception {
		
		StringBuffer sql = new StringBuffer();
		sql.append("update ").append(TABLE_NAME).append(" set name=?").append(" where minSaleUnitcode = ? and languageCode=?");
		this.daoSupport.execute(sql.toString(),minSaleUnitMemo.getName(), minSaleUnitMemo.getMinSaleUnitcode(),minSaleUnitMemo.getLanguageCode());

	}

	/**
	 * 
	 * @Title: getMinSaleUnitMemoByLanguageCodeAndUnitCode
	 * @Description: 根据最小销售单位编码和语言编码查询最小销售单位说明
	 * @param
	 * @return MinSaleUnitMemo
	 * @throws
	 */
	public MinSaleUnitMemo getMinSaleUnitMemoByLanguageCodeAndUnitCode(
			String unitCode, String languageCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME)
				.append(" where minSaleUnitcode=? ").append(" and languageCode=?");
		return (MinSaleUnitMemo) this.daoSupport.queryForObject(sql.toString(),
				MinSaleUnitMemo.class, unitCode.trim(), languageCode.trim());
	}

	/**
	  * <p>Title: getAvailableLanguage</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<Language> getAvailableLanguage(String unitCode)
			throws Exception {
		List<Language> list = new ArrayList<Language>();
		List<Language> languages = languageService.queryLanguage();
		if(null == languages || languages.size() == 0){
			return list;
		}
		List<MinSaleUnitMemo> memos = this.getMinSaleUnitMemoListByUnitCode(unitCode);
		if(null == memos || memos.size() == 0){
			return languages;
		}
		Map<String , MinSaleUnitMemo> map = this.convertListToMap(memos);
		for(Language language : languages){
			if(!map.containsKey(language.getCode()))
				list.add(language);
		}
		return list;
	}
	
	private Map<String , MinSaleUnitMemo> convertListToMap(List<MinSaleUnitMemo> memos) throws Exception{
		Map<String , MinSaleUnitMemo> map = new HashMap<String , MinSaleUnitMemo>();
		for(MinSaleUnitMemo memo : memos){
			map.put(memo.getLanguageCode(), memo);
		}
		return map;
	}

	/**
	  * <p>Title: updateMinSaleUnitMemo</p>
	  * <p>Description: </p>
	  * @param memos
	  * @param memos2
	  * @return void
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void updateMinSaleUnitMemo(
			List<MinSaleUnitMemo> memos, List<MinSaleUnitMemo> memos2)
			throws Exception {
		//添加
		if(null != memos && memos.size() > 0){
			for(MinSaleUnitMemo memo : memos){
				this.addMinSaleUnitMemo(memo);
			}
		}
		//删除
		if(null != memos2 && memos2.size() > 0){
			for(MinSaleUnitMemo memo : memos2){
				this.delMinSaleUnitMemoById(memo.getId());
			}
		}
	}

	/**
	 * 
	* @Title: delMinSaleUnitMemoById
	* @Description: 根据最小销售单位说明id,删除最小销售单位说明
	* @param 最小销售单位说明id   
	* @return void    
	* @throws
	 */
	public void delMinSaleUnitMemoById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME)
				.append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);

	}

	@Override
	public MinSaleUnitMemo querMinSaleUnitMemo(String code, String languageCode) {
		StringBuffer sql=new StringBuffer();
		sql.append("select * from DT_MinSaleUnitMemo where minSaleUnitCode=");
		sql.append("'");
		sql.append(code);
		sql.append("'");
		sql.append(" and languageCode=");
		sql.append("'");
		sql.append(languageCode);
		sql.append("'");
		return (MinSaleUnitMemo)this.daoSupport.queryForObject(sql.toString(), MinSaleUnitMemo.class, new Object[]{});
	}
}
