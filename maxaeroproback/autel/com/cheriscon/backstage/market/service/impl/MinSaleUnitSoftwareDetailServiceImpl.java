/**
 */
package com.cheriscon.backstage.market.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.service.IMinSaleUnitSoftwareDetailService;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitSoftwareDetail;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: MinSaleUnitSoftwareDetailServiceImpl
 * @Description: 最小销售单位软件管理业务实现类
 * @author shaohu
 * @date 2013-1-16 下午02:48:32
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class MinSaleUnitSoftwareDetailServiceImpl extends BaseSupport implements
		IMinSaleUnitSoftwareDetailService {

	/**
	 * <p>
	 * Title: getDetailByUnitCode
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitSoftwareDetail> getDetailByUnitCode(String code)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dd.*,dt.name as softwareTypeName,(select count(1) from DT_SoftwareVersion where softwareTypeCode=dt.code and releaseState=1 ) as releaseFlag from DT_MinSaleUnitSoftwareDetail dd left join DT_SoftwareType dt on dd.softwareTypeCode = dt.code where 1=1 ");
		sql.append(" and dd.minSaleUnitCode = ?");
		sql.append(" order by dd.id desc");
		return this.daoSupport.queryForList(sql.toString(),
				MinSaleUnitSoftwareDetail.class, code);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SoftwareType> getAvailableSoftwareType(String code,String productTypeCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_SoftwareType dt where not exists (select * from DT_MinSaleUnitSoftwareDetail ds where dt.code = ds.softwareTypeCode and ds.minSaleUnitCode =?) and proTypeCode=?  order by dt.name ");
		return this.daoSupport.queryForList(sql.toString(), SoftwareType.class,
				code,productTypeCode);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SoftwareType> getSelSoftwareType(MinSaleUnit minSaleUnit,SoftwareType softwareType,String excludecodes)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_SoftwareType dt");
		sql.append(" where proTypeCode='"+minSaleUnit.getProTypeCode()+"' ");
		
		if (StringUtils.isNotBlank(softwareType.getName())) {
			sql.append(" and name like '%");
			sql.append(softwareType.getName().trim());
			sql.append("%'");
		}
		
		if (StringUtils.isNotBlank(excludecodes)) {
			sql.append(" and code not in (");
			sql.append(excludecodes.trim());
			sql.append(")");
		}
		
		sql.append(" order by dt.name");
		return this.daoSupport.queryForList(sql.toString(), SoftwareType.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SoftwareType> getSelSoftwareTypeNew(SoftwareType softwareType,String excludecodes,String selprosoftconfigcodes)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select dt.* from DT_SoftwareType dt left join DT_PRODUCT_SOFTWARE_CONFIG dp on dt.code=dp.code");
		sql.append(" where 1=1 ");
		
		if (StringUtils.isNotBlank(softwareType.getName())) {
			sql.append(" and dt.name like '%");
			sql.append(softwareType.getName().trim());
			sql.append("%'");
		}
		
		if (StringUtils.isNotBlank(excludecodes)) {
			sql.append(" and dt.code not in (");
			sql.append(excludecodes.trim());
			sql.append(")");
		}
		
		if (StringUtils.isNotBlank(selprosoftconfigcodes)) {
			sql.append(" and dp.parentCode in (");
			sql.append(selprosoftconfigcodes.trim());
			sql.append(")");
		}
		
		sql.append(" order by dt.name ");
		return this.daoSupport.queryForList(sql.toString(), SoftwareType.class);
	}

	/**
	 * 
	 * @Title: addDeatil
	 * @Description: 添加最小销售单位软件数据
	 * @param detail
	 *            最小销售单位软件数据
	 * @return void
	 * @throws
	 */
	public void addDeatil(MinSaleUnitSoftwareDetail detail) throws Exception {
		this.daoSupport.insert("DT_MinSaleUnitSoftwareDetail", detail);
	}

	/**
	 * <p>
	 * Title: batchDetail
	 * </p>
	 * <p>
	 * Description: 批量更新最小销售单位软件数据
	 * </p>
	 * 
	 * @param details
	 * @param details2
	 * @throws Exception
	 */
	@Override
	@Transactional
	public void batchDetail(List<MinSaleUnitSoftwareDetail> details,
			List<MinSaleUnitSoftwareDetail> details2) throws Exception {
		if (null != details && details.size() > 0) {
			for(MinSaleUnitSoftwareDetail detail : details){
				this.addDeatil(detail);
			}
		}
		if (null != details2 && details2.size() > 0) {
			for(MinSaleUnitSoftwareDetail detail : details2){
				this.delDetailById(detail.getId());
			}
		}
	}

	/**
	 * 
	 * @Title: delDetailById
	 * @Description: 根据id 删除最小销售单位软件数据
	 * @param id
	 *            最小销售单位软件id
	 * @return void
	 * @throws
	 */
	private void delDetailById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from DT_MinSaleUnitSoftwareDetail where id=?");
		this.daoSupport.execute(sql.toString(), id);
	}

	/**
	  * <p>Title: deleteSoftwareDeatilByUnitCode</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @throws Exception
	  */ 
	@Override
	public void deleteSoftwareDeatilByUnitCode(String unitCode)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from DT_MinSaleUnitSoftwareDetail where minSaleUnitCode=?");
		this.daoSupport.execute(sql.toString(), unitCode);
	}

}
