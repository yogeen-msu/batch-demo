/**
 */
package com.cheriscon.backstage.market.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.service.ISaleConfigMemoService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.SaleConfigMemo;
import com.cheriscon.cop.sdk.database.BaseSupport;

/**
 * @ClassName: SaleConfigMemoServiceImpl
 * @Description: 销售配置业务实现类
 * @author shaohu
 * @date 2013-1-15 下午03:15:35
 * 
 */
@SuppressWarnings("rawtypes")
@Service
public class SaleConfigMemoServiceImpl extends BaseSupport implements
		ISaleConfigMemoService {

	private final static String TABLE_NAME = "DT_SaleConfigMemo";

	@Resource
	private ILanguageService languageService;
	
	/**
	 * 
	  * <p>Title: getSaleConfigMemoListByUnitCode</p>
	  * <p>Description: 查询最小单位说明数据 根据销售配置编码</p>
	  * @param uintCode 销售配置编码
	  * @return
	  * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SaleConfigMemo> getSaleConfigMemoListByUnitCode(String uintCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append("select dm.*,dl.name languageName from ").append(TABLE_NAME).append(" dm left join dt_language dl on dm.languageCode = dl.code ").append(" where dm.saleConfigcode=?").append(" order by dm.id desc");
		return this.daoSupport.queryForList(sql.toString(), SaleConfigMemo.class, uintCode);
	}
	
	/**
	 * <p>
	 * Title: delSaleConfigMemoBySaleConfigCode
	 * </p>
	 * <p>
	 * Description: 根据销售配置编码 删除销售配置说明
	 * </p>
	 * 
	 * @param code
	 * @throws Exception
	 */
	@Override
	public void delSaleConfigMemoBySaleConfigCode(String code)
			throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME)
				.append(" where saleConfigcode=?");
		this.daoSupport.execute(sql.toString(), code);

	}

	/**
	 * <p>
	 * Title: addSaleConfigMemo
	 * </p>
	 * <p>
	 * Description: 添加销售配置说明
	 * </p>
	 * 
	 * @param saleConfigMemo
	 * @throws Exception
	 */
	@Override
	public void addSaleConfigMemo(SaleConfigMemo saleConfigMemo)
			throws Exception {
		this.daoSupport.insert(TABLE_NAME, saleConfigMemo);
	}

	/**
	 * <p>
	 * Title: updateSaleConfigMemoByLanguage
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param saleConfigMemo
	 * @throws Exception
	 */
	@Override
	public void updateSaleConfigMemoByLanguage(SaleConfigMemo saleConfigMemo)
			throws Exception {
		
		StringBuffer sql = new StringBuffer();
		sql.append("update ").append(TABLE_NAME).append(" set name=?").append(" where saleConfigcode = ? and languageCode=?");
		this.daoSupport.execute(sql.toString(),saleConfigMemo.getName(), saleConfigMemo.getSaleConfigcode(),saleConfigMemo.getLanguageCode());

	}

	/**
	 * 
	 * @Title: getSaleConfigMemoByLanguageCodeAndUnitCode
	 * @Description: 根据销售配置编码和语言编码查询销售配置说明
	 * @param
	 * @return SaleConfigMemo
	 * @throws
	 */
	public SaleConfigMemo getSaleConfigMemoByLanguageCodeAndUnitCode(
			String unitCode, String languageCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from ").append(TABLE_NAME)
				.append(" where saleConfigcode=? ").append(" and languageCode=?");
		return (SaleConfigMemo) this.daoSupport.queryForObject(sql.toString(),
				SaleConfigMemo.class, unitCode.trim(), languageCode.trim());
	}

	/**
	  * <p>Title: getAvailableLanguage</p>
	  * <p>Description: </p>
	  * @param unitCode
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public List<Language> getAvailableLanguage(String unitCode)
			throws Exception {
		List<Language> list = new ArrayList<Language>();
		List<Language> languages = languageService.queryLanguage();
		if(null == languages || languages.size() == 0){
			return list;
		}
		List<SaleConfigMemo> memos = this.getSaleConfigMemoListByUnitCode(unitCode);
		if(null == memos || memos.size() == 0){
			return languages;
		}
		Map<String , SaleConfigMemo> map = this.convertListToMap(memos);
		for(Language language : languages){
			if(!map.containsKey(language.getCode()))
				list.add(language);
		}
		return list;
	}
	
	private Map<String , SaleConfigMemo> convertListToMap(List<SaleConfigMemo> memos) throws Exception{
		Map<String , SaleConfigMemo> map = new HashMap<String , SaleConfigMemo>();
		for(SaleConfigMemo memo : memos){
			map.put(memo.getLanguageCode(), memo);
		}
		return map;
	}

	/**
	  * <p>Title: updateSaleConfigMemo</p>
	  * <p>Description: </p>
	  * @param memos
	  * @param memos2
	  * @return void
	  * @throws Exception
	  */ 
	@Override
	@Transactional
	public void updateSaleConfigMemo(
			List<SaleConfigMemo> memos, List<SaleConfigMemo> memos2)
			throws Exception {
		//添加
		if(null != memos && memos.size() > 0){
			for(SaleConfigMemo memo : memos){
				this.addSaleConfigMemo(memo);
			}
		}
		//删除
		if(null != memos2 && memos2.size() > 0){
			for(SaleConfigMemo memo : memos2){
				this.delSaleConfigMemoById(memo.getId());
			}
		}
	}

	/**
	 * 
	* @Title: delSaleConfigMemoById
	* @Description: 根据销售配置说明id,删除销售配置说明
	* @param 销售配置说明id   
	* @return void    
	* @throws
	 */
	public void delSaleConfigMemoById(Integer id) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(TABLE_NAME)
				.append(" where id=?");
		this.daoSupport.execute(sql.toString(), id);

	}

	@Override
	public SaleConfigMemo querSaleConfigMemo(String code, String languageCode) {
		StringBuffer sql=new StringBuffer();
		sql.append("select * from DT_SaleConfigMemo where saleConfigCode=");
		sql.append("'");
		sql.append(code);
		sql.append("'");
		sql.append(" and languageCode=");
		sql.append("'");
		sql.append(languageCode);
		sql.append("'");
		return (SaleConfigMemo)this.daoSupport.queryForObject(sql.toString(), SaleConfigMemo.class, new Object[]{});
	}
}
