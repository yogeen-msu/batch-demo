/**
 */
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.backstage.market.vo.MinSaleUnitVo;
import com.cheriscon.common.model.MarkerPromotionSoftwareRule;

/**
 * @ClassName: IMarkerPromotionSoftwareRuleService
 * @Description: 营销活动参与最小销售单位接口类
 * @author shaohu
 * @date 2013-1-22 下午03:09:44
 * 
 */
public interface IMarkerPromotionSoftwareRuleService {

	/**
	 * 
	* @Title: getSoftwareListByPromotionCode
	* @Description: 根据营销活动编码查询营销活动参与软件信息
	* @param    
	* @return List<MarkerPromotionSoftwareRule>    
	* @throws
	 */
	public List<MarkerPromotionSoftwareRule> getSoftwareListByPromotionCode(String promotionCode) throws Exception;
	
	/**
	 * 
	 * @Title: addMarkerPromotionSoftwareRule
	 * @Description: 添加营销活动最小销售单位
	 * @param
	 * @return void
	 * @throws
	 */
	public void addMarkerPromotionSoftwareRule(String promotionCode,
			MarkerPromotionSoftwareRule softwareRule) throws Exception;
	
	/**
	 * 
	* @Title: addMarkerPromotionSoftwareRuleAll
	* @Description: 添加标识所有的数据 (all)
	* @param    
	* @return void    
	* @throws
	 */
	public void addMarkerPromotionSoftwareRuleAll(String promotionCode)
			throws Exception;

	/**
	 * 
	* @Title: delPromotionMinsaleUnitByPromotionCodeAndStatus
	* @Description: 删除标识所有的数据(all)
	* @param    
	* @return void    
	* @throws
	 */
	public void delPromotionMinsaleUnitByPromotionCodeALL(
			String promotionCode) throws Exception;

	/**
	 * 
	* @Title: batchAddMarkerPromotionSoftwareRule
	* @Description: 批量添加最小销售单位
	* @param    
	* @return void    
	* @throws
	 */
	public void batchAddMarkerPromotionSoftwareRule(String promotionCode,
			List<MarkerPromotionSoftwareRule> softwareRules) throws Exception;
	
	
	/**
	 * 
	* @Title: getAdvMinSaleUnit
	* @Description: 查询该活动的可用最小销售单位
	* @param    
	* @return List    
	* @throws
	 */
	public List<MinSaleUnitVo> getAdvMinSaleUnit(String promotionCode,String languageCode) throws Exception;
	
	/**
	 * 
	* @Title: delPromotionMinsaleUnitById
	* @Description: 根据id删除营销活动最小销售单位
	* @param    
	* @return void    
	* @throws
	 */
	public void delPromotionMinsaleUnitById(Integer id) throws Exception;
	
	/**
	 * 
	* @Title: delPromotionMinsaleUnitByPromotionCode
	* @Description: 根据营销活动编码删除营销活动最小销售单位
	* @param    
	* @return void    
	* @throws
	 */
	public void delPromotionMinsaleUnitByPromotionCode(String promotionCode) throws Exception;

}
