/**
*/ 
package com.cheriscon.backstage.market.service;

import com.cheriscon.common.model.MarkerPromotionRule;

/**
 * @ClassName: IMarkerPromotionRuleService
 * @Description: 营销活动规则接口类
 * @author shaohu
 * @date 2013-1-22 下午03:04:28
 * 
 */
public interface IMarkerPromotionRuleService {
	
	/**
	 * 
	* @Title: getMarkerPromotionRuleByPromotionCode
	* @Description:  根据营销活动编码查询营销活动规则
	* @param    promotionCode 营销活动编码
	* @return MarkerPromotionRule    
	* @throws
	 */
	public MarkerPromotionRule getMarkerPromotionRuleByPromotionCode(String promotionCode) throws Exception;

	/**
	 * 
	* @Title: addMarkerPromotionRule
	* @Description: 添加营销活动规则
	* @param    
	* @return String 规则编码    
	* @throws
	 */
	public String addMarkerPromotionRule(MarkerPromotionRule markerPromotionRule) throws Exception;
	
	/**
	 * 
	* @Title: updateMarkerPromotionRule
	* @Description: 修改营销活动规则信息
	* @param    
	* @return String    
	* @throws
	 */
	public void updateMarkerPromotionRule(MarkerPromotionRule markerPromotionRule) throws Exception;
	
	/**
	 * 
	* @Title: delRuleByPromotionCode
	* @Description: 根据营销活动编码删除营销活动规则
	* @param    
	* @return void    
	* @throws
	 */
	public void delRuleByPromotionCode(String promotionCode) throws Exception;
}
