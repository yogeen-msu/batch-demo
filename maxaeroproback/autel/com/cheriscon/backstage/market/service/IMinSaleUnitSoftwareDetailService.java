/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitSoftwareDetail;
import com.cheriscon.common.model.SoftwareType;

/**
 * @ClassName: IMinSaleUnitSoftwareDetailService
 * @Description: 最小销售单位软件管理业务接口类
 * @author shaohu
 * @date 2013-1-16 下午02:47:53
 * 
 */
public interface IMinSaleUnitSoftwareDetailService {

	/**
	 * 
	* @Title: getDetailByUnitCode
	* @Description: 根据最小销售单位编码查询 最小销售单位的关联软件
	* @param    最小销售单位编码
	* @return List<MinSaleUnitSoftwareDetail>    
	* @throws
	 */
	public List<MinSaleUnitSoftwareDetail> getDetailByUnitCode(String code) throws Exception;
	
	/**
	 * 
	* @Title: getAvailableSoftwareType
	* @Description: 根据最小销售单位编码查询可用的功能软件
	* @param    code 最小销售单位编码
	* @param productTypeCode 产品类型编码
	* @return List<SoftwareType>    
	* @throws
	 */
	public List<SoftwareType> getAvailableSoftwareType(String code,String productTypeCode) throws Exception;
	
	/**
	 * 
	* @Title: getSelSoftwareType
	* @Description: 根据条件查询可用的功能软件
	* @return List<SoftwareType>    
	* @throws
	 */
	public List<SoftwareType> getSelSoftwareType(MinSaleUnit minSaleUnit,SoftwareType softwareType,String excludecodes) throws Exception;
	
	/**
	 * 
	* @Title: getSelSoftwareType
	* @Description: 根据条件查询可用的功能软件
	* @return List<SoftwareType>    
	* @throws
	 */
	public List<SoftwareType> getSelSoftwareTypeNew(SoftwareType softwareType,String excludecodes,String selprosoftconfigcodes) throws Exception;
	
	/**
	 * 
	* @Title: batchDetail
	* @Description: 批量更新最小销售单位软件管理数据
	* @param  details 添加的数据
	* @param  details2  删除的数据
	* @return void    
	* @throws
	 */
	public void batchDetail(List<MinSaleUnitSoftwareDetail> details,List<MinSaleUnitSoftwareDetail> details2) throws Exception;
	
	/**
	 * 
	* @Title: deleteSoftwareDeatilByUnitCode
	* @Description: 根据最小销售单位编码删除 软件信息
	* @param    
	* @return void    
	* @throws
	 */
	public void deleteSoftwareDeatilByUnitCode(String unitCode) throws Exception;
}
