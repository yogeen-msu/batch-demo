package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.backstage.market.vo.SaleContractVo;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.framework.database.Page;

/**
 * 销售契约
 * @author yinhb
 *
 */
public interface ISaleContractService {
	/**
	 * 翻页查询所有契约
	 * @param productInfo
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageSaleContract(SaleContract saleContract,int pageNo, int pageSize);
	
	/**
	 * 
	* @Title: pageSaleContractDetail
	* @Description: 分页查询详细销售契约信息
	* @param    
	* @return Page    
	* @throws
	 */
	public Page pageSaleContractDetail(SaleContractVo saleContractvo,int pageNo, int pageSize) throws Exception;
	
	
	/**
	 * 
	* @Title: pageSaleContractDetail
	* @Description: 分页查询详细销售契约信息(方便分配权限的管理人员进行修改)
	* @param    
	* @return Page    
	* @throws
	 */
	public Page pageSaleContractDetail2(SaleContractVo saleContractvo,Integer userId,int pageNo, int pageSize) throws Exception;
	
	/**
	 * 
	* @Title: getSaleContractDetail
	* @Description: 获取销售契约详细信息
	* @param  saleContractCode
	* @return SaleContractVo    
	* @throws
	* @author pengdongan
	 */
	public SaleContractVo getSaleContractDetail(String saleContractCode) throws Exception;
	
	/**
	 * 
	* @Title: listAll
	* @Description: 查询销售契约所有数据
	* @param    
	* @return List<SaleContract>    
	* @throws
	 */
	public List<SaleContract> listAll() throws Exception;
	
	/**
	 * 
	* @Title: getSaleContractByCode
	* @Description: 根据销售目录编码查询销售契约数据
	* @param   code 编码 
	* @return SaleContract    
	* @throws
	 */
	public SaleContract getSaleContractByCode(String code) throws Exception;
	
	/**
	 * 
	* @Title: addSaleContractType
	* @Description: 添加销售契约目录
	* @param    saleContract 销售契约目录
	* @return void    
	* @throws
	 */
	public void addSaleContract(SaleContract saleContract) throws Exception;
	
	/**
	 * 
	* @Title: updateSaleContract
	* @Description: 修改销售契约目录
	* @param    saleContract 销售契约目录
	* @return void    
	* @throws
	 */
	public void updateSaleContract(SaleContract saleContract) throws Exception;
	
	
	/**
	 * 
	* @Title: delSaleContract
	* @Description: 删除销售契约
	* @param    saleContract 销售企业
	* @return void    
	* @throws
	 */
	public void delSaleContract(SaleContract saleContract) throws Exception;
	
	/**
	 * 
	* @Title: delSaleContractByCode
	* @Description: 删除销售契约
	* @param    code 销售企业编码
	* @return void    
	* @throws
	 */
	public void delSaleContractByCode(String code) throws Exception;
	
	/**
	 * 
	* @Title: checkRepeat
	* @Description: 检查销售契约重复
	* @param    
	* @return boolean    
	* @throws
	 */
	public boolean checkRepeat(SaleContract saleContract) throws Exception;
	
	/**
	 * 
	* @Title: getContrctByProCode
	* @Description: 根据产品型号获取销售契约
	* @param    
	* @return List<SaleContract>    
	* @throws
	 */
	public  List<SaleContract> getContrctByProCode(String proCode) throws Exception;
	
	/**
	 * 
	* @Title: getContrctList
	* @Description: 根据产品的销售契约来获取是否在美国该区域有对应的销售契约
	* @param    SaleContract
	* @return List<SaleContract>    
	* @throws
	 */
	public  List<SaleContract> getContrctList(SaleContract saleContract) throws Exception;
	

	public  List<SaleContract> getContrctBySealer(SaleContract saleContract) throws Exception;
	
	/**
	 * 
	* @Title: getContrctBySealerAutelId
	* @Description: 根据经销商编码查询经销商下的所有契约
	* @param    sealerAutelId
	* @return List<SaleContract>    
	* @throws
	 */
	public  List<SaleContract> getContrctBySealerAutelId(String sealerAutelId) throws Exception;
	
	
	public List<SaleContract> getSaleContract(SaleContract saleContract) throws Exception;
	/**
	 * A16043  hlh
	 * 给无人机的接口函数，查询指定的契约
	 */
	public List<SaleContract> getSaleContractUAV(String contractName,String contractCode,String startTime,String endTime) throws Exception;
	/**
    * @Title: getContrctByDate
	* @Description: 根据时间获取改最后修改时间在此时间之后的所有销售契约
	* @param    startTime
	* @return List<SaleContract>    
	* @throws
	 */
	public List<SaleContract> getContrctByDate(String startTime) throws Exception;
}
