/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.MarkerPromotionSaleConfig;
import com.cheriscon.common.model.SaleConfig;

/**
 * @ClassName: IMarkerPromotionSaleConfigService
 * @Description: 营销活动销售配置service接口类
 * @author shaohu
 * @date 2013-3-10 下午05:42:39
 * 
 */
public interface IMarkerPromotionSaleConfigService {
	
	/**
	 * 
	* @Title: batchAddMarkerPromotionSoftwareRule
	* @Description: 批量添加营销活动销售配置数据
	* @param promotionCode 营销活动编码   
	* @return void    
	* @throws
	 */
	public void batchAddMarkerPromotionSaleConfig(String promotionCode,
			List<MarkerPromotionSaleConfig> markerPromotionSaleConfigs) throws Exception;
	
	/**
	 * 
	* @Title: getSaleConfigByPromotionCode
	* @Description: 根据营销活动获取该活动销售配置
	* @param    promotionCode 营销活动编码
	* @return List<MarkerPromotionSaleConfig>    
	* @throws
	 */
	public List<MarkerPromotionSaleConfig> getSaleConfigByPromotionCode(String promotionCode) throws Exception;
	
	/**
	 * 
	* @Title: getAdvSaleConfig
	* @Description: 根据营销活动获取该活动的可用销售配置
	* @param    
	* @return List<SaleConfig>    
	* @throws
	 */
	public List<SaleConfig> getAdvSaleConfig(String promotionCode)  throws Exception;
	
	/**
	 * 
	* @Title: delPromotionSaleConfigById
	* @Description: 根据id删除营销活动配置
	* @param    
	* @return void    
	* @throws
	 */
	public void delPromotionSaleConfigById(Integer id) throws Exception;
}
