/**
 */
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.SaleConfigMemo;

/**
 * @ClassName: ISaleConfigMemoService
 * @Description: 销售配置业务实现接口类
 * @author shaohu
 * @date 2013-1-15 下午03:12:06
 * 
 */
public interface ISaleConfigMemoService {
	
	/**
	 * 
	* @Title: getSaleConfigMemoList
	* @Description: 查询最小单位说明数据 根据销售配置编码
	* @param   uintCode 销售配置编码
	* @return List<SaleConfigMemo>    
	* @throws
	 */
	public List<SaleConfigMemo> getSaleConfigMemoListByUnitCode(String uintCode) throws Exception;
	
	/**
	 * 
	 * @Title: delSaleConfigMemoBySaleConfigCode
	 * @Description: 根据销售配置编码 删除销售配置说明
	 * @param code
	 *            销售配置编码
	 * @return void
	 * @throws
	 */
	public void delSaleConfigMemoBySaleConfigCode(String code)
			throws Exception;

	/**
	 * 
	 * @Title: addSaleConfigMemo
	 * @Description: 添加销售配置说明
	 * @param saleConfigMemo
	 *            销售配置说明
	 * @return void
	 * @throws
	 */
	public void addSaleConfigMemo(SaleConfigMemo saleConfigMemo)
			throws Exception;

	/**
	 * 
	 * @Title: updateSaleConfigMemo
	 * @Description: 修改销售配置说明
	 * @param
	 * @return void
	 * @throws
	 */
	public void updateSaleConfigMemoByLanguage(SaleConfigMemo saleConfigMemo)
			throws Exception;
	
	/**
	 * 
	* @Title: getAvailableLanguage
	* @Description: 获取未被添加的语言
	* @param    unitCode 依据销售配置编码
	* @return List<Language>    
	* @throws
	 */
	public List<Language> getAvailableLanguage(String unitCode) throws Exception;
	
	/**
	 * 
	* @Title: updateSaleConfigMemo
	* @Description: 销售配置说明数据的添加和删除
	* @param    memos	需添加的销售配置说明数据
	* @param    memos2	需删除的销售配置说明数据
	* @return void    
	* @throws
	 */
	public void updateSaleConfigMemo(List<SaleConfigMemo> memos,List<SaleConfigMemo> memos2) throws Exception;

	/**
	 * 查询销售配置说明
	 * @param code
	 * @param languageCode
	 * @return
	 */
	public SaleConfigMemo querSaleConfigMemo(String code,String languageCode);
}
