/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.SaleContractType;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: ISaleContractTypeService
 * @Description: 销售契约目录业务接口类
 * @author shaohu
 * @date 2013-1-17 上午09:11:24
 * 
 */
public interface ISaleContractTypeService {
	
	/**
	 * 
	* @Title: pageSaleContractType
	* @Description: 分页查询销售契约目录
	* @param    
	* @return Page    
	* @throws
	 */
	public Page pageSaleContractType(SaleContractType saleContractType,int pageNo,int pageSize) throws Exception;
	
	/**
	 * 
	* @Title: listAll
	* @Description: 查询销售目录所有数据
	* @param    
	* @return List<SaleContractType>    
	* @throws
	 */
	public List<SaleContractType> listAll() throws Exception;
	
	/**
	 * 
	* @Title: getSaleContractTypeByCode
	* @Description: 根据销售目录编码查询销售契约数据
	* @param   code 编码 
	* @return SaleContractType    
	* @throws
	 */
	public SaleContractType getSaleContractTypeByCode(String code) throws Exception;
	
	/**
	 * 
	* @Title: addSaleContractType
	* @Description: 添加销售契约目录
	* @param    saleContractType 销售契约目录
	* @return void    
	* @throws
	 */
	public void addSaleContractType(SaleContractType saleContractType) throws Exception;
	
	
	/**
	 * 
	* @Title: delSaleContractType
	* @Description: 删除销售契约
	* @param    saleContractType 销售企业
	* @return void    
	* @throws
	 */
	public void delSaleContractType(SaleContractType saleContractType) throws Exception;
	
	/**
	 * 
	* @Title: delSaleContractTypeByCode
	* @Description: 删除销售契约
	* @param    code 销售企业编码
	* @return void    
	* @throws
	 */
	public void delSaleContractTypeByCode(String code) throws Exception;
	
	
}
