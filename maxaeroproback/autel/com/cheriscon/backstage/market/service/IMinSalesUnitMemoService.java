/**
 */
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.MinSaleUnitMemo;

/**
 * @ClassName: IMinSaleUnitMemoService
 * @Description: 最小销售单位业务实现接口类
 * @author shaohu
 * @date 2013-1-15 下午03:12:06
 * 
 */
public interface IMinSalesUnitMemoService {
	
	/**
	 * 
	* @Title: getMinSaleUnitMemoList
	* @Description: 查询最小单位说明数据 根据最小销售单位编码
	* @param   uintCode 最小销售单位编码
	* @return List<MinSaleUnitMemo>    
	* @throws
	 */
	public List<MinSaleUnitMemo> getMinSaleUnitMemoListByUnitCode(String uintCode) throws Exception;
	
	/**
	 * 
	 * @Title: delMinSaleUnitMemoByMinSaleUnitCode
	 * @Description: 根据最小销售单位编码 删除最小销售单位说明
	 * @param code
	 *            最小销售单位编码
	 * @return void
	 * @throws
	 */
	public void delMinSaleUnitMemoByMinSaleUnitCode(String code)
			throws Exception;

	/**
	 * 
	 * @Title: addMinSaleUnitMemo
	 * @Description: 添加最小销售单位说明
	 * @param minSaleUnitMemo
	 *            最小销售单位说明
	 * @return void
	 * @throws
	 */
	public void addMinSaleUnitMemo(MinSaleUnitMemo minSaleUnitMemo)
			throws Exception;

	/**
	 * 
	 * @Title: updateMinSaleUnitMemo
	 * @Description: 修改最小销售单位说明
	 * @param
	 * @return void
	 * @throws
	 */
	public void updateMinSaleUnitMemoByLanguage(MinSaleUnitMemo minSaleUnitMemo)
			throws Exception;
	
	/**
	 * 
	* @Title: getAvailableLanguage
	* @Description: 获取未被添加的语言
	* @param    unitCode 依据最小销售单位编码
	* @return List<Language>    
	* @throws
	 */
	public List<Language> getAvailableLanguage(String unitCode) throws Exception;
	
	/**
	 * 
	* @Title: updateMinSaleUnitMemo
	* @Description: 最小销售单位说明数据的添加和删除
	* @param    memos	需添加的最小销售单位说明数据
	* @param    memos2	需删除的最小销售单位说明数据
	* @return void    
	* @throws
	 */
	public void updateMinSaleUnitMemo(List<MinSaleUnitMemo> memos,List<MinSaleUnitMemo> memos2) throws Exception;

	/**
	 * 查询最小销售单位说明
	 * @param code
	 * @param languageCode
	 * @return
	 */
	public MinSaleUnitMemo querMinSaleUnitMemo(String code,String languageCode);
}
