/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.MinSaleUnitSaleCfgDetail;

/**
 * @ClassName: IMinSaleUnitSaleCfgDetailService
 * @Description: 销售配置和最小销售单位中间表接口类
 * @author shaohu
 * @date 2013-1-17 下午02:00:32
 * 
 */
public interface IMinSaleUnitSaleCfgDetailService {
	
	/**
	 * 
	* @Title: addMinSaleUnitSaleCfgDetail
	* @Description: 添加
	* @param   cfgDetail
	* @return void    
	* @throws
	 */
	public void addMinSaleUnitSaleCfgDetail(MinSaleUnitSaleCfgDetail cfgDetail) throws Exception;
	
	/**
	 * 
	* @Title: delMinSaleUnitSaleCfgDetailById
	* @Description: 根据id 删除
	* @param    
	* @return void    
	* @throws
	 */
	public void delMinSaleUnitSaleCfgDetailById(Integer id) throws Exception;
	
	/**
	 * 
	* @Title: delMinSaleUnitSaleCfgDetailByCode
	* @Description: 根据销售配置编码删除 关联的最小销售到单位信息
	* @param   code 销售配置编码 
	* @return void    
	* @throws
	 */
	public void delMinSaleUnitSaleCfgDetailByCode(String code) throws Exception;
	
	/**
	 * 
	* @Title: getMinSaleUnitSaleCfgListByCode
	* @Description: 根据销售配置code 查询最小最小销售单位
	* @param    
	* @return List<MinSaleUnitSaleCfgDetail>    
	* @throws
	 */
	public List<MinSaleUnitSaleCfgDetail> getMinSaleUnitSaleCfgListByCode(String saleCode) throws Exception;
	
}
