/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.backstage.market.vo.MinSaleUnitSearch;
import com.cheriscon.backstage.market.vo.MinSaleUnitVo;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: IMinSaleUnitService
 * @Description: 最小小时单位service类
 * @author shaohu
 * @date 2013-1-14 下午06:16:44
 * 
 */
public interface IMinSaleUnitService {
	
	/**
	 * 
	* @Title: pageMinSaleUnit
	* @Description: 根据条件分页查询最小销售单位数据
	* @param  minSaleUnitSearch 最小销售单位查询条件
	* @param  pageNo
	* @param  pageSize
	* @return Page    
	* @throws
	 */
	public Page pageMinSaleUnit(MinSaleUnitSearch minSaleUnitSearch,int pageNo ,int pageSize) throws Exception;
	
	/**
	 * 
	* @Title: listAll
	* @Description: 查询所有的最小销售单位
	* @param    languageCode 语言编码
	* @return List<MinSaleUnitVo>    
	* @throws
	 */
	public List<MinSaleUnitVo> listAll(String languageCode) throws Exception;
	
	/**
	 * 
	* @Title: getMinSaleUnitByCode
	* @Description: 根据最小销售单位的编码 查询
	* @param    
	* @return MinSaleUnit    
	* @throws
	 */
	public MinSaleUnit getMinSaleUnitByCode(String code) throws Exception;
	
	/**
	 * 
	* @Title: addMinSaleUnit
	* @Description: 添加最小销售单位
	* @param    minSaleUnit 最小销售单位实体
	* @param minSaleUnitMemo 最小销售单位说明
	* @return void    
	* @throws
	 */
	public void addMinSaleUnit(MinSaleUnit minSaleUnit,MinSaleUnitMemo minSaleUnitMemo) throws Exception;
	
	/**
	 * 
	* @Title: updateMinSaleUnit
	* @Description: 修改最小销售单位
	* @param    
	* @return void    
	* @throws
	 */
	public void updateMinSaleUnit(MinSaleUnit minSaleUnit,MinSaleUnitMemo minSaleUnitMemo) throws Exception;
	
	/**
	 * 
	* @Title: delMinSaleUnit
	* @Description: 删除最小销售单位
	* @param    code 最小小时单位编码
	* @return void    
	* @throws
	 */
	public void delMinSaleUnit(String code) throws Exception;
	
	
}
