/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.MinSaleUnitPrice;

/**
 * @ClassName: IMinSaleUnitPriceService
 * @Description: 最小销售单位价格业务接口类
 * @author shaohu
 * @date 2013-1-16 下午04:52:47
 * 
 */
public interface IMinSaleUnitPriceService {
	/**
	 * 
	* @Title: getMinSaleUnitPriceListByUnitCode
	* @Description: 根据最小销售单位编码 查询最小销售单位价格数据
	* @param  unitCode  最小销售单位编码  
	* @return List<MinSaleUnitPrice>    
	* @throws
	 */
	public List<MinSaleUnitPrice> getMinSaleUnitPriceListByUnitCode(String unitCode) throws Exception;
	
	/**
	 * 
	* @Title: getAvailableAreaConfig
	* @Description: 取最小销售单位下 可用的区域配置数据
	* @param  unitCode   最小销售单位编码  
	* @return List<AreaConfig>    
	* @throws
	 */
	public List<AreaConfig> getAvailableAreaConfig(String unitCode) throws Exception;
	
	/**
	 * 
	* @Title: batchUpdateMinSaleUnitPrice
	* @Description: 批量更新最小销售单位价格数据
	* @param    prices  添加的数据
	* @param    prices2	删除的数据
	* @return void    
	* @throws
	 */
	public void batchUpdateMinSaleUnitPrice(List<MinSaleUnitPrice> prices,List<MinSaleUnitPrice> prices2) throws Exception;
	
	/**
	 * 
	* @Title: deleteUnitPriceByUnitCode
	* @Description: 根据最小销售单位编码删除价格
	* @param    
	* @return void    
	* @throws
	 */
	public void deleteUnitPriceByUnitCode(String unitCode) throws Exception;

}
