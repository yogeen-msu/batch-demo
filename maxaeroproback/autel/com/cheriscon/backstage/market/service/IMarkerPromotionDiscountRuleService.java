/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.MarkerPromotionDiscountRule;

/**
 * @ClassName: IMarkerPromotionDiscountRuleService
 * @Description: 营销活动规则条件接口类
 * @author shaohu
 * @date 2013-1-22 下午03:06:18
 * 
 */
public interface IMarkerPromotionDiscountRuleService {
	
	/**
	 * 
	* @Title: getDiscountRuleListByPromotionCode
	* @Description: 根据营销规则编码查询营销规则条件信息
	* @param    ruleCode 营销规则编码
	* @return List<MarkerPromotionDiscountRule>    
	* @throws
	 */
	public List<MarkerPromotionDiscountRule> getDiscountRuleListByRuleCode(String ruleCode) throws Exception;
	
	/**
	 * 
	* @Title: addMarkerPromotionDiscountRule
	* @Description: 添加营销活动规则信息
	* @param    
	* @return void    
	* @throws
	 */
	public void addMarkerPromotionDiscountRule(MarkerPromotionDiscountRule discountRule) throws Exception;
	
	/**
	 * 
	* @Title: batchAddMarkerPromotionDiscountRule
	* @Description: 批量添加
	* @param    
	* @return void    
	* @throws
	 */
	public void batchAddMarkerPromotionDiscountRule(String ruleCode ,List<MarkerPromotionDiscountRule> discountRules) throws Exception;
	
	
	/**
	 * 
	* @Title: delMarkerPromotionDiscountRuleById
	* @Description: 依据id删除营销活动规则条件信息
	* @param    
	* @return void    
	* @throws
	 */
	public void delMarkerPromotionDiscountRuleById(Integer id) throws Exception;
	
	/**
	 * 
	* @Title: delMarkerPromotionDiscountRuleByRuleCode
	* @Description: 根据营销活动规则编码删除营销活动规则条件
	* @param    
	* @return void    
	* @throws
	 */
	public void delMarkerPromotionDiscountRuleByRuleCode(String ruleCode) throws Exception;
}
