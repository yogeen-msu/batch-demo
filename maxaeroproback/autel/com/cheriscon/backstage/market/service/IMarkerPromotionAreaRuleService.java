/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.AreaConfig;
import com.cheriscon.common.model.MarkerPromotionAreaRule;

/**
 * @ClassName: IMarkerPromotionAreaRuleService
 * @Description: 营销活动区域接口类
 * @author shaohu
 * @date 2013-1-22 下午03:44:04
 * 
 */
public interface IMarkerPromotionAreaRuleService {
	
	/**
	 * 
	* @Title: getPromotionAreaByPromotionCode
	* @Description: 依据营销活动编码查询营销活动区域信息
	* @param    
	* @return List<MarkerPromotionAreaRule>    
	* @throws
	 */
	public List<MarkerPromotionAreaRule> getPromotionAreaByPromotionCode(String promotionCode) throws Exception;

	/**
	 * 
	* @Title: addMarkerPromotionAreaRule
	* @Description: 添加营销活动区域
	* @param    
	* @return void    
	* @throws
	 */
	public void addMarkerPromotionAreaRule(MarkerPromotionAreaRule areaRule) throws Exception;
	
	
	/**
	 * 
	* @Title: addMarkerPromotionAreaRuleAll
	* @Description: 添加标识所有的区域
	* @param    
	* @return void    
	* @throws
	 */
	public void addMarkerPromotionAreaRuleAll(String promotionCode) throws Exception;
	
	/**
	 * 
	* @Title: delMarkerPromotionAreaRuleAll
	* @Description: 删除标识所有的区域
	* @param    
	* @return void    
	* @throws
	 */
	public void delMarkerPromotionAreaRuleAll(String promotionCode)  throws Exception;
	/**
	 * 
	* @Title: batchAddMarkerPromotionAreaRule
	* @Description: 批量添加活动区域信息
	* @param    
	* @return void    
	* @throws
	 */
	public void batchAddMarkerPromotionAreaRule(String promotionCode , List<MarkerPromotionAreaRule> areaRules) throws Exception;
	
	/**
	 * 
	* @Title: getAdvAreaConfig
	* @Description: 获取该活动下面可用区域
	* @param  promotionCode	营销活动编码  
	* @return List<AreaConfig>    
	* @throws
	 */
	public List<AreaConfig> getAdvAreaConfig(String promotionCode) throws Exception;
	
	/**
	 * 
	* @Title: delMarkerPromotionAreaRuleById
	* @Description: 根据id删除营销活动区域数据
	* @param    
	* @return void    
	* @throws
	 */
	public void delMarkerPromotionAreaRuleById(Integer id) throws Exception;
	
	/**
	 * 
	* @Title: delMarkerPromotionAreaByPromotionCode
	* @Description: 删除营销活动区域信息
	* @param    promotionCode 	营销活动编码
	* @return void    
	* @throws
	 */
	public void delMarkerPromotionAreaByPromotionCode(String promotionCode) throws Exception;
}
