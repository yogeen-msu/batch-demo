/**
 */
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.backstage.market.vo.MarkerPromotionSearch;
import com.cheriscon.common.model.MarkerPromotionAreaRule;
import com.cheriscon.common.model.MarkerPromotionBaseInfo;
import com.cheriscon.common.model.MarkerPromotionDiscountRule;
import com.cheriscon.common.model.MarkerPromotionRule;
import com.cheriscon.common.model.MarkerPromotionSaleConfig;
import com.cheriscon.common.model.MarkerPromotionSoftwareRule;
import com.cheriscon.common.model.MarkerPromotionTimesInfo;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: IMarkerPromotionBaseInfo
 * @Description: 营销活动接口类
 * @author shaohu
 * @date 2013-1-22 下午02:58:06
 * 
 */
public interface IMarkerPromotionBaseInfoService {
	
	/**
	 * 
	* @Title: getMarkerPromotionBaseInfoByCode
	* @Description: 根据营销活动编码查询营销活动基本信息
	* @param    code 营销活动编码
	* @return MarkerPromotionBaseInfo    
	* @throws
	 */
	public MarkerPromotionBaseInfo getMarkerPromotionBaseInfoByCode(String code) throws Exception;
	
	/**
	 * 
	* @Title: pageMarkerPromotion
	* @Description: 分页查找活动
	* @param    
	* @return Page    
	* @throws
	 */
	public Page pageMarkerPromotion(MarkerPromotionSearch promotionVo,int pageNo,int pageSize) throws Exception;
	

	/**
	 * 
	 * @Title: addPromotion
	 * @Description: 添加活动
	 * @param baseInfo
	 *            营销活动基础信息
	 * @param rule
	 *            营销活动规则信息
	 * @param discountRules
	 *            营销活动规则条件信息
	 * @param timesInfos
	 *            营销活动时间段信息
	 * @param softwareRules
	 *            最小销售单位数据
	 * @param areaRules
	 *            区域数据
	 * @return void
	 * @throws
	 */
	public void addPromotion(MarkerPromotionBaseInfo baseInfo,
			MarkerPromotionRule rule,
			List<MarkerPromotionDiscountRule> discountRules,
			List<MarkerPromotionTimesInfo> timesInfos,
			List<MarkerPromotionSoftwareRule> softwareRules,
			List<MarkerPromotionAreaRule> areaRules,
			List<MarkerPromotionSaleConfig> markerPromotionSaleConfigs) throws Exception;
	
	
	/**
	 * 
	* @Title: updatePromotion
	* @Description: 修改营销活动信息
	* @param    
	* @return void    
	* @throws
	 */
	public void updatePromotion(MarkerPromotionBaseInfo baseInfo,
			MarkerPromotionRule rule,
			List<MarkerPromotionDiscountRule> discountRules,
			List<MarkerPromotionDiscountRule> delDiscountRules,
			List<MarkerPromotionTimesInfo> timesInfos,
			List<MarkerPromotionTimesInfo> delTimesInfos,
			List<MarkerPromotionSoftwareRule> softwareRules,
			List<MarkerPromotionSoftwareRule> delSoftwareRules,
			List<MarkerPromotionAreaRule> areaRules,
			List<MarkerPromotionAreaRule> delAreaRules,
			List<MarkerPromotionSaleConfig> markerPromotionSaleConfigs,
			List<MarkerPromotionSaleConfig> delMarkerPromotionSaleConfigs) throws Exception;
	
	/**
	 * 
	* @Title: delPromotion
	* @Description: 删除营销活动
	* @param    
	* @return void    
	* @throws
	 */
	public void delPromotion(String promotionCode) throws Exception;
	
	/**
	 * 
	* @Title: startPromotion
	* @Description: 启用活动
	* @param    
	* @return void    
	* @throws
	 */
	public void startPromotion(String promotionCodes) throws Exception;
	
	/**
	 * 
	* @Title: stopPromotion
	* @Description: 终止活动
	* @param    
	* @return void    
	* @throws
	 */
	public void stopPromotion(String promotionCodes) throws Exception;
	
	/**
	 * 
	* @Title: checkPromotion
	* @Description: 校验活动启用时间是否冲突
	* @param    
	* @return boolean    false 表示时间冲突
	* @throws
	 */
	public boolean checkPromotion(String promotionCodes) ;
}
