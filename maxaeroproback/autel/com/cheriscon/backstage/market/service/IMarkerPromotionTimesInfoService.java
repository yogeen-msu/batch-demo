/**
*/ 
package com.cheriscon.backstage.market.service;

import java.util.List;

import com.cheriscon.common.model.MarkerPromotionTimesInfo;

/**
 * @ClassName: IMarkerPromotionTimesInfoService
 * @Description: 营销活动时间段接口类
 * @author shaohu
 * @date 2013-1-22 下午03:08:22
 * 
 */
public interface IMarkerPromotionTimesInfoService {
	
	/**
	 * 
	* @Title: getTimesInfoListByPromotionCode
	* @Description: 根据营销活动编码查询营销活动时间段信息
	* @param    promotionCode 营销活动编码
	* @return List<MarkerPromotionTimesInfo>    
	* @throws
	 */
	public List<MarkerPromotionTimesInfo> getTimesInfoListByPromotionCode(String promotionCode) throws Exception;
	
	/**
	 * 
	* @Title: addMarkerPromotionTimesInfo
	* @Description: 添加营销活动时间段信息
	* @param    
	* @return void    
	* @throws
	 */
	public void addMarkerPromotionTimesInfo(MarkerPromotionTimesInfo timesInfo) throws Exception;
	
	/**
	 * 
	* @Title: batchAddMarkerPromotionTimesInfo
	* @Description: 批量添加
	* @param    
	* @return void    
	* @throws
	 */
	public void batchAddMarkerPromotionTimesInfo(String promotionCode, List<MarkerPromotionTimesInfo> timesInfos) throws Exception;
	
	/**
	 * 
	* @Title: delPromotionTimesInfoById
	* @Description: 根据id 删除营销活动时间段信息
	* @param    
	* @return void    
	* @throws
	 */
	public void delPromotionTimesInfoById(Integer id) throws Exception;
	
	/**
	 * 
	* @Title: delTimesInfoByPromotionCode
	* @Description: 根据营销活动编码删除营销活动时间段信息
	* @param    
	* @return void    
	* @throws
	 */
	public void delTimesInfoByPromotionCode(String promotionCode) throws Exception;
	
}
