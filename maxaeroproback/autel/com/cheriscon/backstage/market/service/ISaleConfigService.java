/**
 */
package com.cheriscon.backstage.market.service;

import java.util.List;

import org.springframework.aop.ThrowsAdvice;

import com.cheriscon.backstage.market.vo.MinSaleUnitConfigVo;
import com.cheriscon.backstage.market.vo.SaleConfigSearch;
import com.cheriscon.backstage.market.vo.SaleConfigVo;
import com.cheriscon.common.model.MinSaleUnit;
import com.cheriscon.common.model.MinSaleUnitSaleCfgDetail;
import com.cheriscon.common.model.SaleConfig;
import com.cheriscon.framework.database.Page;

/**
 * @ClassName: ISaleConfigService
 * @Description: 销售配置接口类
 * @author shaohu
 * @date 2013-1-17 下午01:58:59
 * 
 */
public interface ISaleConfigService {

	public Page pageConfig(SaleConfigSearch configSearch, int pageNo,
			int pageSize) throws Exception ;
	
	/**
	 * 
	 * @Title: pageSaleConfig
	 * @Description: 分页查询销售配置
	 * @param
	 * @return Page
	 * @throws
	 */
	public Page pageSaleConfig(SaleConfigSearch configSearch, int pageNo,
			int pageSize) throws Exception;

	/**
	 * 
	 * @Title: getMinSaleUnitConfigList
	 * @Description: 查询最小销售单位
	 * @param
	 * @return List<MinSaleUnitConfigVo>
	 * @throws
	 */
	public List<MinSaleUnitConfigVo> getMinSaleUnitConfigList(
			String languageCode) throws Exception;

	/**
	 * 
	 * @Title: getSaleConfigUnit
	 * @Description: 根据销售配置编码 查询已经添加的最小销售单位数据
	 * @param
	 * @return List<MinSaleUnitConfigVo>
	 * @throws
	 */
	public List<MinSaleUnitConfigVo> getSaleConfigUnit(String saleCode,
			String languageCode) throws Exception;

	/**
	 * 
	 * @Title: addSaleConfig
	 * @Description: 添加销售配置
	 * @param
	 * @return void
	 * @throws
	 */
	public void addSaleConfig(SaleConfig saleConfig,
			List<MinSaleUnitSaleCfgDetail> details) throws Exception;

	/**
	 * 
	 * @Title: delSaleConfigByCode
	 * @Description: 删除销售配置信息
	 * @param
	 * @return void
	 * @throws
	 */
	public void delSaleConfigByCode(String code) throws Exception;

	/**
	 * 
	 * @Title: getAvailableMinSaleUnitConfigList
	 * @Description: 修改时候查询可用的最小销售单位数据
	 * @param
	 * @return List<MinSaleUnitConfigVo>
	 * @throws
	 */
	public List<MinSaleUnitConfigVo> getAvailableMinSaleUnitConfigList(
			String saleCode, String languageCode,String proTypeCode) throws Exception;

	/**
	 * 
	 * @Title: getSaleConfigByCode
	 * @Description: 根据销售配置编码查询销售配置
	 * @param
	 * @return SaleConfig
	 * @throws
	 */
	public SaleConfig getSaleConfigByCode(String code) throws Exception;

	/**
	 * 
	 * @Title: updateSaleConfig
	 * @Description: 修改销售配置信息和相关联的最小销售单位信息
	 * @param saleConfig
	 * @param details
	 * @param delDetails
	 * @return void
	 * @throws
	 */
	public void updateSaleConfig(SaleConfig saleConfig,
			List<MinSaleUnitSaleCfgDetail> details,
			List<MinSaleUnitSaleCfgDetail> delDetails) throws Exception;
	
	/**
	 * 
	* @Title: listAll
	* @Description: 查询销售配置list
	* @param    
	* @return List<SaleConfigVo>    
	* @throws
	 */
	public List<SaleConfigVo> listAll() throws Exception;
	
	/**
	 * 得到所有的销售配置信息
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public List<SaleConfig> querySaleConfig() throws Exception;
	
	/**
	 * 得到销售配置中所有的最小销售单位
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<MinSaleUnit> queryMinSaleUnits(String code)throws Exception;

	/**
	  * <p>Title: getSaleConfigDetailByCode</p>
	  * <p>Description: 根据编码查询销售配置详情</p>
	  * @param code
	  * @return
	  * @throws Exception
	  */
	public SaleConfig getSaleConfigDetailByCode(String code) throws Exception;
}
