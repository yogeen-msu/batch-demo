package com.cheriscon.common.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户信息Session 工具类
 * @author yangpinggui 2013-1-12
 *
 */
public class SessionUtil 
{
	/**
	 * 获取Session中登录用户信息（包括个人用户、经销商用户）
	 * @param req
	 * @return
	 */
	public static Object getLoginUserInfo(HttpServletRequest request) 
	{
		Object obj = request.getSession().getAttribute("userInfo");
		return obj;
	}
	
	/**
	 * 获取登录用户信息设置到Session中
	 * @param request
	 * @param object
	 */
	public static void setLoginUserInfo(HttpServletRequest request , Object object)
	{
		request.getSession().setAttribute("userInfo", object);
	}
	


	/**
	 *  将登录用户信息从session中删除
	 */
	public static void removeLoginUserInfo(HttpServletRequest request) 
	{
		request.getSession().removeAttribute("userInfo");
		request.getSession().invalidate();
	}
}
