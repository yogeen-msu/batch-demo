package com.cheriscon.common.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

/**
 * JSON的处理器
 * <p>Description: </p>
 * <p>Copyright: G'FIVE GROUP AND RELATED PARTIES. ALL RIGHTS RESERVED.</p>
 * @version  
 */
public class DateJsonValueProcessor implements JsonValueProcessor {
  
    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";   
    private DateFormat dateFormat;   
    
    /**  
     * 构造方法.  
     * @param datePattern 日期格式  
     */  
    public DateJsonValueProcessor(String datePattern) {   
          
        if( null == datePattern ) {
            this.dateFormat = new SimpleDateFormat(DEFAULT_DATE_PATTERN);
        } else {
            this.dateFormat = new SimpleDateFormat(datePattern);
        } 
        
    }   
 
    public Object processArrayValue(Object arg0, JsonConfig arg1) {
        return process(arg0);   
    }

    public Object processObjectValue(String arg0, Object arg1, JsonConfig arg2) {
        return process(arg1);   
    }
    
    private Object process(Object value) {   
        return this.dateFormat.format((Date) value);   
    }   


}
