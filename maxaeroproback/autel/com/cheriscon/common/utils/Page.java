package com.cheriscon.common.utils;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * solr 搜索用到的分页
 * @author A13045
 *
 * @param <T>
 */
public class Page<T> {

	private int pagesize;// 单页记录
	private int currentpage;// 当前页面
	private int countdate;// 总记录数
	private int countPage;// 总的页数
	private List<T> objects;// 返回的结果集
	private int currentPageSize;// 当前页数量

	/**
	 * 查询字段
	 */
	private T searchObj;

	public Page() {
		pagesize = 10;
		currentpage = 1;
		countdate = 0;
	}

	public List<T> getObjects() {
		return objects;
	}

	public void setObjects(List<T> objects) {
		this.objects = objects;
		currentPageSize = objects.size();
	}

	/**
	 * 当前页面
	 * 
	 * @return
	 */
	public int getCurrentpage() {
		return currentpage;
	}

	public void setCurrentpage(int currentpage) {
        
		if (currentpage < 1) {
			this.currentpage = 1;
		} else if(currentpage>countPage&&countPage>0){
			this.currentpage = countPage;
		}else{
			this.currentpage = currentpage;
		}
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	/**
	 * 总记录数
	 * 
	 * @return
	 */
	public int getCountdate() {
		return countdate;
	}

	public void setCountdate(int countdate) {
		this.countdate = countdate;
		getCountpage();
	}

	/**
	 * 由记录数设定有关的页面数。 总的页数
	 * 
	 * @return
	 */
	public int getCountpage() {
		int i = this.countdate / this.pagesize;
		if ((this.countdate % this.pagesize) != 0) {
			i += 1;
		}
		setCountPage(i);
		return i;
	}

	public int getLastPage() {
		return this.getCurrentpage() - 1;
	}

	public int getNextPage() {
		return this.getCurrentpage() + 1;
	}

	// 给ibatis用数据
	public int getCurrent() {
		return (this.currentpage - 1) * pagesize;
	}

	public int getNext() {
		return this.currentpage * pagesize;
	}

	/**
	 * 总的页数
	 * 
	 * @return
	 */
	public int getCountPage() {
		return countPage;
	}

	/**
	 * 总的页数
	 * 
	 * @param countPage
	 */
	public void setCountPage(int countPage) {
		this.countPage = countPage;
	}

	public T getSearchObj() {
		return searchObj;
	}

	public void setSearchObj(T searchObj) {
		this.searchObj = searchObj;
	}

	public int getCurrentPageSize() {
		return currentPageSize;
	}

	public void setCurrentPageSize(int currentPageSize) {
		this.currentPageSize = currentPageSize;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this,
				ToStringStyle.MULTI_LINE_STYLE, true, true);
	}
	
}
