package com.cheriscon.common.utils;

import java.util.Timer;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.cheriscon.backstage.member.action.CustomerComplaintTask;
import com.cheriscon.backstage.member.action.MyTask;

public class AutelListener implements ServletContextListener {
  
  private Timer timer = null;

  public void contextInitialized(ServletContextEvent event) {
    timer = new Timer(true);
    //设置任务计划，启动和间隔时间
    //timer.schedule(new MyTask(), 0, 10000);
    //设置任务计划，启动和间隔时间
    //timer.schedule(new ComplaintSendEmailTask(), 0, 20000);
  }

  public void contextDestroyed(ServletContextEvent event) {
    timer.cancel();
  }
  
}