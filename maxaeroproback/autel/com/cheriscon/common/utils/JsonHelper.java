package com.cheriscon.common.utils;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonHelper {

	private static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	public static <T> T fromJson(String json, Class<T> clazz) {
		return gson.fromJson(json, clazz);
	}
	
	public static <T> T fromJson(String json, Type type) {
		return gson.fromJson(json, type);
	}

	public static String toJson(Object obj) {
		return gson.toJson(obj);
	}
}
