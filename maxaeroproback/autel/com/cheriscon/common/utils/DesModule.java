package com.cheriscon.common.utils;

import javax.crypto.spec.*;
import javax.crypto.*;

/**
 * 密码特殊处理工具类
 * @author yangpinggui 2013-1-12
 *
 */

public class DesModule 
{

	private static DesModule desModule = null;
	
	private static String desKeyData = null;

	public static  DesModule getInstance()
	{
		if(null == desModule)
		{
			desModule = new DesModule();
			desKeyData = "8Fb5j7RAYj4Dw3s6bZ11hVynY9oyx0LHdTz3Xww8xmNsfzGd1qABCerSz0ZRXMA3";
		}
		
		return desModule;
	}
	
	/**
	 * 加密方法
	 * @param src 
	 * @return 
	 * @throws
	 */
	public  String encrypt(String src){
		
		if(desKeyData.length() > 7)
		{
			try 
			{
				DESKeySpec desKeySpec = new DESKeySpec(desKeyData.getBytes());
				SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
				SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
				Cipher cdes = Cipher.getInstance("DES");
				cdes.init(Cipher.ENCRYPT_MODE, secretKey);
				byte[] ct = null;
				ct = cdes.doFinal(src.getBytes());
				return Function.byteArrayToHexString(ct);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
		
		return null;
	}

	/**
	 * 解密方法
	 * @param src
	 * @return
	 */
	public String decrypt(String src){
		
		if(desKeyData.length() > 7)
		{
			try
			{
				DESKeySpec desKeySpec = new DESKeySpec(desKeyData.getBytes());
				SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
				SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
				Cipher cdes = Cipher.getInstance("DES");
				cdes.init(Cipher.DECRYPT_MODE, secretKey);
				byte[] ct = cdes.doFinal(Function.hexStringToByteArray(src));
				return new String(ct);
			}catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
		
		return null;
	}
   public static void main(String args[]){
	 String serialNo="serialNo=V09C40201233&RegPwd=425085&J2534Code=CFJW50491037" ;
	 String test=DesModule.getInstance().encrypt(serialNo);
	 System.out.println(test);
	 test=DesModule.getInstance().decrypt("DEB98ABD6528D0378784D1AB3044AAE8D7F4BFB9446D9F2D8B0F11426D7CBFB259893B44A5930C25D31701FE0C8A545D24E743F3844A802251262A03388AF7B5");
	 System.out.println(test);
	 
  }
}
