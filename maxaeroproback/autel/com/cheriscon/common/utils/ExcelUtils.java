package com.cheriscon.common.utils;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class ExcelUtils {
	
	private int maxNull = 30;	
	private Workbook workbook = null;
	private Sheet sheet = null;
	
	
	public ExcelUtils(InputStream inputStream,int sheet) {
		try {
			workbook = Workbook.getWorkbook(inputStream);
			getSheet(sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ExcelUtils(File file,int sheet) {
		try {
			workbook = Workbook.getWorkbook(file);
			getSheet(sheet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 读Excel 从第几列到第列
	 * 			默認第一行為標題行，將忽略
	 * @param fromColumn
	 * @param toColumn
	 */
	public List<List<String>> readSimpleExcel(int fromColumn,int toColumn){
		int nullRow = 0;
		List<List<String>> resultList = new ArrayList<List<String>>();
		Cell cell = null;
		
		int rowCount = sheet.getRows();
		for (int i = 1; i < rowCount; i++) {
			
			//已每一行第一个单元格不能为空判断该行是否有效
			if(StringUtils.isBlank(sheet.getCell(0, i).getContents())){
					nullRow++;
					continue;
			}
			
			//如果连续读取了规定的最大空格数，则不在继续读取
			if(nullRow == maxNull)  return resultList;
			
			
			List<String> itemList = new ArrayList<String>();
			for (int j = fromColumn; j <= toColumn; j++) {
				
				// 第一个是表示列的，第二才表示行
				cell = sheet.getCell(j, i);
				itemList.add(cell.getContents());
				
//				// 要根据单元格的类型分别做处理，否则格式化过的内容可能会不正确
//				if (cell.getType() == CellType.NUMBER) {
//					System.out.print(((NumberCell) cell).getValue());
//				}
			}
			resultList.add(itemList);
		}
		close();
		return resultList;
	}
	
	
	public static void writeSimpleExcel(File file,int rows, int columns){
		 WritableWorkbook book;
		try {
			book = Workbook.createWorkbook(file);
			WritableSheet sheet = book.createSheet("Sheet1", 0);
			Label newLabel=new Label(columns,rows,"111111");
			sheet.addCell(newLabel);
			book.write();
			book.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	//	close();
	}
	
	
	/**
	 * 读取的Excel第几页 
	 * 
	 * 		从0开始，0为第一页
	 * 
	 * @param i
	 */
	private void getSheet(int i){
		sheet = workbook.getSheet(i);
	}

	private void close(){
		workbook.close();	// 关闭它，否则会有内存泄露
	}
	
	public static void main(String args[]) {
		
		writeSimpleExcel(new File("e:/test.xlsx"),1,1);
	}
}
