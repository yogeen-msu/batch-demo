package com.cheriscon.common.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.FacetParams;
import org.apache.solr.common.params.ModifiableSolrParams;

import com.cheriscon.common.model.solr.model.CustomerInfo;



public class SolrUtils {

	public SolrUtils() {

	}

	private static SolrServer  server;
	private static Properties p;

	public static SolrServer  connectSolr() {
		if (null == server) {
			
			server = new HttpSolrServer("http://localhost/solr/core1");
			
			}
		
		return server;
	}

	public static void queryList(ModifiableSolrParams params){
		server=connectSolr();
		SolrQuery query = new SolrQuery(); 
		query.setQuery("productName:*MaxiDAS*");
		query.setFacet(true);
		query.setFields("country");
		
	    query.setStart(0);  
	    query.setRows(1000000);  
	        try {
				QueryResponse res = server.query(query);
				//SolrDocumentList docs = res.getResults();
				List<CustomerInfo> beans = res.getBeans(CustomerInfo.class);
				for(int i=0 ;i<beans.size();i++){
					System.out.println(beans.get(i).getSerialNo());
				}
			} catch (SolrServerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
	        //可以直接查询相应的bean对象，但是不是很常用  
	        //使用这种方式无法获取总数量  
		 
	}
	
	
	public static Map<String, Integer> queryByGroup(String qStr,String groupField,Integer pageSize,Integer pageNum){
		  Map<String, Integer> rmap = new LinkedHashMap<String, Integer>();
		  try {
		   SolrServer server = new HttpSolrServer("http://localhost/solr/collection1");;//getSolrServer() 方法就是返回一个CommonsHttpSolrServer
		   SolrQuery query = new SolrQuery();
		   if(qStr!=null&&qStr.length()>0)
		    query.setQuery(qStr);
		   else
		    query.setQuery("*:*");//如果没有查询语句，必须这么写，否则会报异常
		   query.setIncludeScore(false);//是否按每组数量高低排序
		   query.setFacet(true);//是否分组查询
		   query.setRows(0);//设置返回结果条数，如果你是分组查询，你就设置为0
		   query.addFacetField(groupField);//增加分组字段
		//   query.setFacetSort(true);//分组是否排序
		   query.setFacetLimit(pageSize);//限制每次返回结果数
	//	   query.setSortField(sortField,asc ? SolrQuery.ORDER.asc :SolrQuery.ORDER.desc );//分组排序字段
		   query.set(FacetParams.FACET_OFFSET,(pageNum-1)*pageSize);//当前结果起始位置
		   QueryResponse rsp = server.query( query );   
		   
		   List<Count> countList = rsp.getFacetField(groupField).getValues();
		   List<Count> returnList = new ArrayList<Count>();
		   if(pageNum*pageSize<countList.size())
		    returnList = countList.subList((pageNum-1)*pageSize,pageNum*pageSize);
		   else
		    returnList = countList.subList((pageNum-1)*pageSize,countList.size()-1);
		   
		   for (Count count : returnList) {
		    if(count.getCount()>0)
		     rmap.put(count.getName(), (int) count.getCount());
		   }
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  return rmap;
		 } 
	
	
	public static Map<String, Integer> queryByGroup2(String qStr,String groupField,Integer pageSize,Integer pageNum){
		  Map<String, Integer> rmap = new LinkedHashMap<String, Integer>();
		  try {
		   SolrServer server =new HttpSolrServer("http://localhost/solr/core1");;//getSolrServer() 方法就是返回一个CommonsHttpSolrServer
		   SolrQuery query = new SolrQuery();
		   if(qStr!=null&&qStr.length()>0)
		    query.setQuery(qStr);
		   else
		    query.setQuery("*:*");//如果没有查询语句，必须这么写，否则会报异常
		   query.setIncludeScore(false);//是否按每组数量高低排序
		   query.setFacet(true);//是否分组查询
		   query.setRows(0);//设置返回结果条数，如果你是分组查询，你就设置为0
		   query.addFacetField(groupField);//增加分组字段
		//   query.setFacetSort(true);//分组是否排序
		   query.setFacetLimit(pageSize);//限制每次返回结果数
	//	   query.setSortField(sortField,asc ? SolrQuery.ORDER.asc :SolrQuery.ORDER.desc );//分组排序字段
		   query.set(FacetParams.FACET_OFFSET,(pageNum-1)*pageSize);//当前结果起始位置
		   QueryResponse rsp = server.query( query );   
		   
		   List<Count> countList = rsp.getFacetField(groupField).getValues();
		   List<Count> returnList = new ArrayList<Count>();
		   if(pageNum*pageSize<countList.size())
		    returnList = countList.subList((pageNum-1)*pageSize,pageNum*pageSize);
		   else
		    returnList = countList.subList((pageNum-1)*pageSize,countList.size()-1);
		   
		   for (Count count : returnList) {
		    if(count.getCount()>0)
		     rmap.put(count.getName(), (int) count.getCount());
		   }
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  return rmap;
		 } 
	public static Map<String, Integer> queryByGroup3(String qStr,String groupField,Integer pageSize,Integer pageNum){
		  Map<String, Integer> rmap = new LinkedHashMap<String, Integer>();
		  try {
		   SolrServer server =new HttpSolrServer("http://localhost/solr/softwareInfo");//getSolrServer() 方法就是返回一个CommonsHttpSolrServer
		   SolrQuery query = new SolrQuery();
		   if(qStr!=null&&qStr.length()>0)
		    query.setQuery(qStr);
		   else
		    query.setQuery("*:*");//如果没有查询语句，必须这么写，否则会报异常
		   query.setIncludeScore(false);//是否按每组数量高低排序
		   query.setFacet(true);//是否分组查询
		   query.setRows(0);//设置返回结果条数，如果你是分组查询，你就设置为0
		   query.addFacetField(groupField);//增加分组字段
		//   query.setFacetSort(true);//分组是否排序
		   query.setFacetLimit(pageSize);//限制每次返回结果数
		   query.setFacetSort(FacetParams.FACET_SORT_COUNT);
	//	   query.setSortField(sortField,asc ? SolrQuery.ORDER.asc :SolrQuery.ORDER.desc );//分组排序字段
		   query.set(FacetParams.FACET_OFFSET,(pageNum-1)*pageSize);//当前结果起始位置
		   QueryResponse rsp = server.query( query );   
		   
		   List<Count> countList = rsp.getFacetField(groupField).getValues();
		   List<Count> returnList = new ArrayList<Count>();
		   if(pageNum*pageSize<countList.size())
		    returnList = countList.subList((pageNum-1)*pageSize,pageNum*pageSize);
		   else
		    returnList = countList.subList((pageNum-1)*pageSize,countList.size()-1);
		   
		   for (Count count : returnList) {
		    if(count.getCount()>0)
		     rmap.put(count.getName(), (int) count.getCount());
		   }
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  return rmap;
		 } 
	
	
	public static void main(String args[]){
		
		queryByGroup(null,"country",20,1);
	}
	
	
}
