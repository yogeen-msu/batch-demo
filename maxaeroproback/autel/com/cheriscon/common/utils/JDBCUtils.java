
package com.cheriscon.common.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;


/**
 * 
 * 类JDBCUtils.java的实现描述：TODO JDBC操作简单工具类
 * 
 * @author yinhongbiao 2011-4-19 下午05:37:40
 */
@Component
public class JDBCUtils {
	
	public static final String mysqlDriver="com.mysql.jdbc.Driver";
	private static final Logger logger = Logger.getLogger(JDBCUtils.class);


	/**
	 * 获取 数据库连接
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Connection getConnection(String jdbc_driver,String jdbc_url,String user_name,String user_password) throws Exception {
		try {
			Class.forName(jdbc_driver);
			Connection conn = DriverManager.getConnection(jdbc_url, user_name, user_password);
			return conn;

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

	/**
	 * 关闭数据库相关连接
	 * 
	 * @param connection
	 */
	public static void close(ResultSet rs, Statement st, Connection conn) {
		try {
			if (rs != null)
				rs.close();
			rs = null;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null)
					st.close();
				st = null;
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (conn != null)
						conn.close();
					conn = null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 关闭数据库相关连接
	 * 
	 * @param connection
	 */
	private void close(PreparedStatement pstmt, Connection conn) {
		try {
			if (pstmt != null)
				pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
}
