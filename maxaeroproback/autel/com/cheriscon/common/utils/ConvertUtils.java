package com.cheriscon.common.utils;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * 
 * @remark 转换工具
 *
 */
public class ConvertUtils {
	
	/**
	 * 
	 * @remark 字符串分割
	 * @param szSource
	 * @param token
	 * @return ArrayList<String>
	 * @author pengdongan
	 * @date 2013-01-08
	 */
	public static final ArrayList<String> SplitString(String szSource, String token) {
		ArrayList<String> arr = new ArrayList<String>();
		if (szSource == null || token == null)
			return arr;
		StringTokenizer st1 = new StringTokenizer(szSource, token);
		String d1[] = new String[st1.countTokens()];
		for (int x = 0; x < d1.length; x++)
			if (st1.hasMoreTokens())
				arr.add(st1.nextToken());
		return arr;
	}

}
