package com.cheriscon.common.utils;

/**
 * 密码特殊处理方法工具类
 * @author yangpinggui 2013-1-12
 *
 */
public class Function
{
	/**
	 * 字节数组转换成十六进制字符串
	 * @param data
	 * @return
	 */
	public static String byteArrayToHexString(byte[] data) {
		if (data == null) {
			return "";
		}
		StringBuffer sb = new StringBuffer();
		int tmp;
		for (int i = 0; i < data.length; i++) {
			tmp = data[i] >= 0 ? data[i] : 256 + data[i];
			if (tmp < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(tmp));
		}
		return sb.toString().toUpperCase();
	}

	/**
	 * 十六进制字符串转换成字节数组
	 * @param data
	 * @return
	 */
	public static byte[] hexStringToByteArray(String data) {
		if (data == null || data.length() == 0 || (data.length() % 2) != 0) {
			return null;
		}
		int len = data.length() / 2;
		byte[] result = new byte[len];
		String tmp;
		for (int i = 0; i < len; i++) {
			tmp = data.substring(i * 2, (i + 1) * 2);
			try {
				result[i] = (byte) Integer.parseInt(tmp, 16);
			}
			catch (Exception e) {
				result[i] = 0x00;
			}
		}
		return result;
	}
}
