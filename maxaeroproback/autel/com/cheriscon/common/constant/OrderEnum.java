/**
*/
package com.cheriscon.common.constant;

/**
* @ClassName: OrderEnum
* @Description: 订单各种状态
* @author shaohu
* @date 2013-3-26 下午10:27:02
* 
*/
public enum OrderEnum {
	ORDER_STATE("订单状态"),ORDER_PAY_STATE("订单支付状态"),REC_CONFIRM_STATE("收款确认状态"),PROCESS_STATE("处理状态");
	/**
	 * 名称
	 */
	private String name;

	private OrderEnum(String name) {
		this.name = name;
	}
	
	public static void main(String [] args){
		System.out.println(OrderEnum.ORDER_STATE);
	}
}
