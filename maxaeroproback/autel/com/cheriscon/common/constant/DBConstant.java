package com.cheriscon.common.constant;

import java.util.Random;

public class DBConstant {
	
	/** 数据库表前缀 */
	public static final String DB_PREFIX = "DT_";
	
	
	/** 软件语言前缀*/
	public static final String LANGUAGE_CODE = "lag";
	
	/** 产品重置*/
	public static final String PRODUCT_RESET_CODE = "prs";
	
	/** 产品更换*/
	public static final String PRODUCT_CHANGE_CODE = "pcl";
	
	/** 语言配置前缀*/
	public static final String LANGUAGE_CONFIG_CODE = "lcf";
	
	/** 区域前缀*/
	public static final String AREAINFO_CODE = "ain";
	
	/** 区域配置前缀*/
	public static final String AREAINFO_CONFIG_CODE = "acf";
	
	/** 工具下载*/
	public static final String TOOLDOWNLOAD_CODE = "tdl";
	
	/** 安全问题*/
	public static final String QUESTIONINFO_CODE = "qui";
	
	/** 用户信息*/
	public static final String CUSTOMERINFO_CODE = "cui";
	
	/** 用户更改账户信息*/
	public static final String CUSTOMERINFO_CHANGE_CODE = "cuc";
	
	/** 用户登录信息*/
	public static final String USERLOGININFO_CODE = "uli";
	
	/** 产品信息*/
	public static final String PRODUCTINFO_CODE = "pro";
	
	/** MAXIFLASH PRO 信息*/
	public static final String PRODUCTFORPRO_CODE = "pfp";
	
	/** 产品维修记录*/
	public static final String PRODUCTREPAIRRECORD_CODE = "prr";
	/** 邮件模版*/
	public static final String EMAILTEMPLATE_CODE = "etp";
	/** 消息*/
	public static final String MESSAGE_CODE = "mes";
	/** 消息分类*/
	public static final String MESSAGETYPE_CODE = "mst";
	/** 经销商类型*/
	public static final String SEALERTYPE_CODE = "set";
	/** 经销商信息*/
	public static final String SEALERINFO_CODE = "sei";
	/** 经销商信息*/
	public static final String SALESQUERY_CODE = "sqy";
	
	/** 续租错误日志*/
	public static final String RECHARGE_CARD_ERROR_LOG = "err";
	
	/** 续租错误支持*/
	public static final String RECHARGE_CARD_SUPPORT = "sup";
	
	/*--------------             最小销售单位 start   -------------------------*/
	/**
	 * 最小销售单位编码
	 */
	public final static String MIN_SALE_UNIT_CODE = "msu";
	
	/**
	 * 外部产品编码
	 */
	public final static String PRODUCT_FOR_SEALER_CODE = "pts";
	
	/**
	 * 内部产品语言名称编码
	 */
	public final static String PRODUCT_NAME_LANGUAGE_CODE = "ptn";
	
	/*--------------             最小销售单位 end   -------------------------*/
	
	
	/*--------------------------   产品管理  start-------------------*/
	/**
	 * 产品型号编码前缀
	 */
	public final static String PRODUCT_TYPE_CODE = "prt";
	/**
	 * 虚拟目录编码前缀
	 */
	public final static String PRODUCT_SOFTWARE_CONFIG_CODE = "psc";
	
	/**
	 * 软件类型编码前缀
	 */
	public final static String SOFTWARE_TYPE_CODE = "swt";
	
	/**
	 * 软件版本编码前缀
	 */
	public final static String SOFTWARE_VERSION_CODE = "swv";
	
	/**
	 * 软件基础语言包编码前缀
	 */
	public final static String SOFTWARE_PACK_CODE = "swp";
	
	/**
	 * bata产品编码前缀
	 */
	public final static String BATA_PRODUCT_CODE = "bpr";
	
	/**
	 * 软件语言包编码前缀
	 */
	public final static String LANGUAGE_PACK_CODE = "lpk";
	
	
	/**
	 * 软件语言包编码前缀
	 */
	public final static String SOFTWARE_CONFIG_MEMO_CODE = "smo";
	
	/*--------------------------   产品管理  end -------------------*/
	
	
	
	/*----------------------------    销售契约管理start     -------------------------------*/
	/**
	 * 销售契约目录
	 */
	public final static String SALE_CONTRACT_TYPE_CODE="sct";
	/**
	 * 销售契约
	 */
	public final static String SALE_CONTRACT_CODE ="sac";
	
	/*----------------------------    销售契约管理end     -------------------------------*/
	
	
	
	/*----------------------------    销售配置start     -------------------------------*/
	
	public final static String SALE_CONFIG_CODE = "scf";
	
	
	/*----------------------------    销售配置end     -------------------------------*/
	
	
	
	/*----------------------------    营销活动start     -------------------------------*/
	
	/**
	 * 是销售配置
	 */
	public final static Integer IS_SALE_CONFIG = 1;
	
	/**
	 * 非销售配置
	 */
	public final static Integer NON_SALE_CONFIG = 0;
	
	/**
	 * 营销活动基础信息表
	 */
	public final static String PROMOTION_BASE = "mpb";
	
	/**
	 * 营销活动规则表
	 */
	public final static String PROMOTION_RULE = "mpr";
	
	/**
	 * 营销活动规则条件表
	 */
	public final static String PROMOTION_DIS_RULE = "mpd";
	
	/**
	 * 营销活动参与软件
	 */
	public final static String PROMOTION_SOFTWARE_CODE = "mps";
	
	/**
	 * 营销活动参与销售配置
	 */
	public final static String PROMOTION_SALECONFIG_CODE = "mpc";
	
	/**
	 * 营销活动时间段
	 */
	public final static String PROMOTION_TIME_CODE = "mpt";
	
	/**
	 * 营销活动区域
	 */
	public final static String PROMOTION_AREA_CODE = "mpa";
	
	/**
	 * 所有销售单位
	 */
	public final static Integer ALL_SALE_UNIT = 1;
	
	/**
	 * 所有区域
	 */
	public final static Integer ALL_AREA = 1;
	
	public final static String IS_ALL = "all";
	
	/*----------------------------    营销活动end     -------------------------------*/
	
	
	/**产品软件有效状态信息前缀*/
	public static final String 	PRO_SOFTWARE_VALID_STATUS = "psv";
	
	/**
	 * 经销商销售信息记录编码前缀
	 */
	public static String SALE_INFO_CODE = "sai";
	
	/**
	 * 客诉来源编码前缀
	 */
	public static String CUC_SOURCE_CODE = "cus";
	
	/**
	 * 客诉类型编码前缀
	 */
	public static String CUC_TYPE_CODE = "cut";
	
	/**
	 * 在线客服类型编码前缀
	 */
	public static String ONL_TYPE_CODE = "onl";
	
	/**
	 * 客诉信息记录编码前缀
	 */
	public static String CUC_INFO_CODE = "cuc";
	
	/**
	 * 客诉回复信息记录编码前缀
	 */
	public static String REC_INFO_CODE = "rec";
	
	/**
	 * 充值使用记录编码前缀
	 */
	public static String RCR_INFO_CODE = "rcr";
	
	/**
	 * 充值卡类型编码前缀
	 */
	public static String RECHARGECARD_TYPE_CODE = "rct";
	
	/**
	 * 充值卡信息编码前缀
	 */
	public static String RECHARGECARD_INFO_CODE = "rco";
	
	
	
	/**
	 * 订单信息编码前缀
	 */
	public static String ORD_INFO_CODE = "ori";
	
	/**
	 * 订单修改日志编码前缀
	 */
	public static String ORD_LOG_CODE = "orl";
	
	/**
	 * 订单支付状态修改日志编码前缀
	 */
	public static String ORD_PAYSTATUS_LOG_CODE = "opl";
	
	
	/**
	 * 订单最小销售单位编码前缀
	 */
	public static String ORD_MINSALEUNIT_INFO_CODE = "omu";
	
	
	/**
	 * 用户消息中间表编码前缀
	 */
	public static String USER_MESSAGE_CODE = "umc";
	
	
	
	/*------------------------  测试包 start  --------------------------------*/
	
	/**
	 * 测试包信息编码前缀
	 */
	public static String TEST_PACK = "tpk";
	/**
	 * 测试包语言包编码前缀
	 */
	public static String TEST_PACK_LANGUAGE_PACK = "tpl";
	
	public static String SALES_QUERY_STATUS_OPEN="1";
	
	public static String SALES_QUERY_STATUS_CLOSE="2";
	
	
	/*------------------------  测试包 end  --------------------------------*/
	
	
	
		public static String getRandomString(int length) { 
		    String base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";   
		    Random random = new Random();   
		    StringBuffer sb = new StringBuffer();   
		    for (int i = 0; i < length; i++) {   
		        int number = random.nextInt(base.length());   
		        sb.append(base.charAt(number));   
		    }   
		    return sb.toString();   
		 } 
		
		public static String getRandomNum(int length) { 
		    String base = "0123456789";   
		    Random random = new Random();   
		    StringBuffer sb = new StringBuffer();   
		    for (int i = 0; i < length; i++) {   
		        int number = random.nextInt(base.length());   
		        sb.append(base.charAt(number));   
		    }     
		    return sb.toString();   
		 } 
		
		public static String getRandStr(){
			
			return getRandomString(2)+getRandomNum(4)+getRandomNum(4);
		}
	
	
}


