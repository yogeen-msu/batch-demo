/**
*/ 
package com.cheriscon.common.constant;

/**
 * @ClassName: MarketEnum
 * @Description: 营销活动类型枚举
 * @author shaohu
 * @date 2013-1-19 下午02:10:05
 * 
 */
public enum MarketEnum {
	EMPTION(1,"限时购买"),CLEAP(2,"越早越便宜"),SET_MEAL(3,"套餐优惠");
	
	/**
	 * 值
	 */
	private Integer value;
	/**
	 * 名称
	 */
	private String name;
	
	private MarketEnum(Integer value ,String name){
		this.value = value;
		this.name = name;
	}
	
	public Integer getValue(String name){
		MarketEnum[] marketEnums=MarketEnum.values();
        for(MarketEnum m:marketEnums){
        	if(name == m.name){
        		return m.value;
        	}
        }
        return null;
	}
	
	public String getName(Integer value){
		MarketEnum[] marketEnums=MarketEnum.values();
        for(MarketEnum m:marketEnums){
        	if(value == m.value){
        		return m.name;
        	}
        }
        return null;
	}
	
	public static void main(String [] args){
		System.out.println(MarketEnum.EMPTION.getName(1));
	}
}
