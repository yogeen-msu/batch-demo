package com.cheriscon.common.constant;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;


public class SolrConstant {
	
	private static Properties p=null;
	/**
	 * 单例加载配置属性文件
	 * @param key
	 * @return
	 */
	public static String getInstance(String key){
		if(p==null){
			p=new Properties();
			InputStream io=SolrConstant.class.getResourceAsStream("/config/constant.properties");
			
			try {
				Reader reader = new InputStreamReader(io, "UTF-8");
				p.load(reader);
			} catch (IOException e) {
				System.out.println(" SolrConstant Initialize the abnormal");
				e.printStackTrace();
			}
		}
		return p.getProperty(key);
		
	}
	
	
	public static String getSolrURL(String maker){
		if(p==null){
			p=new Properties();
			InputStream io=SolrConstant.class.getResourceAsStream("/config/constant.properties");
			
			try {
				Reader reader = new InputStreamReader(io, "UTF-8");
				p.load(reader);
			} catch (IOException e) {
				System.out.println(" SolrConstant Initialize the abnormal");
				e.printStackTrace();
			}
		}
		//System.out.println(p.getProperty("solrURL") + "/" + maker);
//		return p.getProperty("solrURL") + "/" + maker;
		return p.getProperty("solrURL") ;//http://search.maxifix.com:8080/solr/KiaOem/
	}

}
