package com.cheriscon.common.constant;

public class BackageConstant {

	/** 更改有效期-在线升级*/
	public static final String PRODUCT_VALID_CHANGE_ONLINE = "1";
	
	/** 更改有效期-升级卡续费*/
	public static final String PRODUCT_VALID_CHANGE_CARD_OWN = "2";
	
	/** 更改有效期-后台手动*/
	public static final String PRODUCT_VALID_CHANGE_SUPPORT = "3";
	
	/** 更改有效期-退款*/
	public static final String PRODUCT_VALID_CHANGE_BACK_MONEY = "4";
	
	/** 更改有效期-在线升级*/
	public static final String PRODUCT_VALID_CHANGE_RE_BACK = "5";
	
	/** 更改有效期-经销商升级卡续费*/
	public static final String PRODUCT_VALID_CHANGE_CARD_SEALER = "6";
	/**	以旧换新  **/
	public static final String PRODUCT_VALID_CHANGE_OLD_FOR_NEW = "7";

}
