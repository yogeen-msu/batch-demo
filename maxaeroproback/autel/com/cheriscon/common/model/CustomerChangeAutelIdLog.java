package com.cheriscon.common.model;

/**
 * 更改AutelID日志表
 * 
 * @author chenqichuan
 * 
 */
public class CustomerChangeAutelIdLog implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1941165948575011920L;
	
	private Integer id;
	private String oldCustomerCode;
	private String newCustomerCode;
	private String operatorUser;
	private String operatorDate;
	private String operatorIp;
	private String oldAutelId;
	private String newAutelId;
	private String productSn;
	private String reason;
	private String proCode;

	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getProductSn() {
		return productSn;
	}
	public void setProductSn(String productSn) {
		this.productSn = productSn;
	}
	public String getOldAutelId() {
		return oldAutelId;
	}
	public String getNewAutelId() {
		return newAutelId;
	}
	public void setOldAutelId(String oldAutelId) {
		this.oldAutelId = oldAutelId;
	}
	public void setNewAutelId(String newAutelId) {
		this.newAutelId = newAutelId;
	}
	public Integer getId() {
		return id;
	}
	public String getOldCustomerCode() {
		return oldCustomerCode;
	}
	public String getNewCustomerCode() {
		return newCustomerCode;
	}
	public String getOperatorUser() {
		return operatorUser;
	}
	public String getOperatorDate() {
		return operatorDate;
	}
	public String getOperatorIp() {
		return operatorIp;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setOldCustomerCode(String oldCustomerCode) {
		this.oldCustomerCode = oldCustomerCode;
	}
	public void setNewCustomerCode(String newCustomerCode) {
		this.newCustomerCode = newCustomerCode;
	}
	public void setOperatorUser(String operatorUser) {
		this.operatorUser = operatorUser;
	}
	public void setOperatorDate(String operatorDate) {
		this.operatorDate = operatorDate;
	}
	public void setOperatorIp(String operatorIp) {
		this.operatorIp = operatorIp;
	}
}