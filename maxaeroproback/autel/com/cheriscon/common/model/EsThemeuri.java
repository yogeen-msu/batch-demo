package com.cheriscon.common.model;



/**
 * AbstractEsThemeuri entity provides the base persistence definition of the EsThemeuri entity. @author MyEclipse Persistence Tools
 */

public abstract class EsThemeuri  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer themeid;
     private String uri;
     private String path;
     private Integer deleteflag;
     private String pagename;
     private Integer point;
     private Integer sitemaptype;
     private String keywords;
     private String description;
     private Short httpcache;


    // Constructors

    /** default constructor */
    public EsThemeuri() {
    }

    
    /** full constructor */
    public EsThemeuri(Integer themeid, String uri, String path, Integer deleteflag, String pagename, Integer point, Integer sitemaptype, String keywords, String description, Short httpcache) {
        this.themeid = themeid;
        this.uri = uri;
        this.path = path;
        this.deleteflag = deleteflag;
        this.pagename = pagename;
        this.point = point;
        this.sitemaptype = sitemaptype;
        this.keywords = keywords;
        this.description = description;
        this.httpcache = httpcache;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getThemeid() {
        return this.themeid;
    }
    
    public void setThemeid(Integer themeid) {
        this.themeid = themeid;
    }

    public String getUri() {
        return this.uri;
    }
    
    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPath() {
        return this.path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }

    public Integer getDeleteflag() {
        return this.deleteflag;
    }
    
    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getPagename() {
        return this.pagename;
    }
    
    public void setPagename(String pagename) {
        this.pagename = pagename;
    }

    public Integer getPoint() {
        return this.point;
    }
    
    public void setPoint(Integer point) {
        this.point = point;
    }

    public Integer getSitemaptype() {
        return this.sitemaptype;
    }
    
    public void setSitemaptype(Integer sitemaptype) {
        this.sitemaptype = sitemaptype;
    }

    public String getKeywords() {
        return this.keywords;
    }
    
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Short getHttpcache() {
        return this.httpcache;
    }
    
    public void setHttpcache(Short httpcache) {
        this.httpcache = httpcache;
    }
   








}