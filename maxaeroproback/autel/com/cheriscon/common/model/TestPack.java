package com.cheriscon.common.model;

/**
 * 
* @ClassName: TestPack
* @Description: 测试包信息
* @author pengdongan
* @date 2013-01-22
*
 */
public class TestPack implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6230985006737332801L;
	
	
	private Integer id;
	/**
	 * 软件类型Code
	 */
	private String softwareTypeCode;
	
	private String code;
	/**
	 * 版本名称
	 */
	private String name;
	/**
	 * 发布日期
	 */
	private String releaseDate;
	
	/**
	 * 基础包下载路径
	 */
	private String downloadPath;
	
	/**
	 * 产品code
	 */
	private String proCode;
	
	private String versionName;

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public TestPack() {
		super();
	}

	public TestPack(Integer id, String softwareTypeCode, String code,
			String name, String releaseDate, String downloadPath, String proCode) {
		super();
		this.id = id;
		this.softwareTypeCode = softwareTypeCode;
		this.code = code;
		this.name = name;
		this.releaseDate = releaseDate;
		this.downloadPath = downloadPath;
		this.proCode = proCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}

	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

}