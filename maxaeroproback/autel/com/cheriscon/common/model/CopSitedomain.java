package com.cheriscon.common.model;



/**
 * AbstractCopSitedomain entity provides the base persistence definition of the CopSitedomain entity. @author MyEclipse Persistence Tools
 */

public abstract class CopSitedomain  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String domain;
     private Integer domaintype;
     private Integer siteid;
     private Integer userid;
     private Integer status;


    // Constructors

    /** default constructor */
    public CopSitedomain() {
    }

    
    /** full constructor */
    public CopSitedomain(String domain, Integer domaintype, Integer siteid, Integer userid, Integer status) {
        this.domain = domain;
        this.domaintype = domaintype;
        this.siteid = siteid;
        this.userid = userid;
        this.status = status;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getDomain() {
        return this.domain;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Integer getDomaintype() {
        return this.domaintype;
    }
    
    public void setDomaintype(Integer domaintype) {
        this.domaintype = domaintype;
    }

    public Integer getSiteid() {
        return this.siteid;
    }
    
    public void setSiteid(Integer siteid) {
        this.siteid = siteid;
    }

    public Integer getUserid() {
        return this.userid;
    }
    
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }
   








}