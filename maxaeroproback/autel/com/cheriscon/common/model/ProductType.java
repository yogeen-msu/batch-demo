package com.cheriscon.common.model;

import java.io.Serializable;
import java.util.List;

import com.cheriscon.framework.database.NotDbField;

/**
 * @remark	产品型号实体
 * @date	2013-1-4
 * @table	DT_ProductType
 * @author sh
 *
 */
public class ProductType implements Serializable{

	private static final long serialVersionUID = 2594647792497816346L;

	//产品型号id
	private Integer id;
	//产品型号code
	private String code;
	//产品型号名称
	private String name;
	//产品序列号图片路径
	private String proSerialPicPath;
	//产品图片路径
	private String picPath;
	
	// 临时字段 区域名称
	private String productCode;
	
	// 临时字段 区域名称
	private String areaName;
	
	//临时字段签约日期
	private String contractDate;
	
	// 临时字段到期日期
	private String expirationTime ;
	/**
	 * 功能软件
	 */
	private List<SoftwareType> softwareTypes;
	
	public ProductType() {
		super();
	}

	public ProductType(Integer id, String code, String name,
			String proSerialPicPath) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.proSerialPicPath = proSerialPicPath;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProSerialPicPath() {
		return proSerialPicPath;
	}

	public void setProSerialPicPath(String proSerialPicPath) {
		this.proSerialPicPath = proSerialPicPath;
	}

	@NotDbField
	public List<SoftwareType> getSoftwareTypes() {
		return softwareTypes;
	}

	public void setSoftwareTypes(List<SoftwareType> softwareTypes) {
		this.softwareTypes = softwareTypes;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	
	
}
