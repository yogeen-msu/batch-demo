package com.cheriscon.common.model;



/**
 * AbstractDataCat entity provides the base persistence definition of the DataCat entity. @author MyEclipse Persistence Tools
 */

public abstract class DataCat  implements java.io.Serializable {


    // Fields    

     private Integer catId;
     private String name;
     private Integer parentId;
     private String catPath;
     private Integer catOrder;
     private Integer modelId;
     private Integer ifAudit;
     private String url;
     private String detailUrl;
     private Integer tositemap;


    // Constructors

    /** default constructor */
    public DataCat() {
    }

    
    /** full constructor */
    public DataCat(String name, Integer parentId, String catPath, Integer catOrder, Integer modelId, Integer ifAudit, String url, String detailUrl, Integer tositemap) {
        this.name = name;
        this.parentId = parentId;
        this.catPath = catPath;
        this.catOrder = catOrder;
        this.modelId = modelId;
        this.ifAudit = ifAudit;
        this.url = url;
        this.detailUrl = detailUrl;
        this.tositemap = tositemap;
    }

   
    // Property accessors

    public Integer getCatId() {
        return this.catId;
    }
    
    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return this.parentId;
    }
    
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCatPath() {
        return this.catPath;
    }
    
    public void setCatPath(String catPath) {
        this.catPath = catPath;
    }

    public Integer getCatOrder() {
        return this.catOrder;
    }
    
    public void setCatOrder(Integer catOrder) {
        this.catOrder = catOrder;
    }

    public Integer getModelId() {
        return this.modelId;
    }
    
    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getIfAudit() {
        return this.ifAudit;
    }
    
    public void setIfAudit(Integer ifAudit) {
        this.ifAudit = ifAudit;
    }

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getDetailUrl() {
        return this.detailUrl;
    }
    
    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public Integer getTositemap() {
        return this.tositemap;
    }
    
    public void setTositemap(Integer tositemap) {
        this.tositemap = tositemap;
    }
   








}