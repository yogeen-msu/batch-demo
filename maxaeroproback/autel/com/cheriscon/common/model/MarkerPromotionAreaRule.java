package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 
 * @ClassName: MarkerPromotionAreaRule
 * @Description: 营销活动参与区域条件
 * @author shaohu
 * @date 2013-1-19 下午01:57:16
 * 
 */
public class MarkerPromotionAreaRule implements java.io.Serializable {

	private static final long serialVersionUID = 48543547246544123L;
	private Integer id;
	/**
	 * 营销活动对象编码
	 */
	private String marketPromotionCode;
	private String code;
	private String areaCode;

	private String areaName;
	public MarkerPromotionAreaRule() {
		super();
	}

	public MarkerPromotionAreaRule(Integer id, String marketPromotionCode,
			String code, String areaCode) {
		super();
		this.id = id;
		this.marketPromotionCode = marketPromotionCode;
		this.code = code;
		this.areaCode = areaCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarketPromotionCode() {
		return marketPromotionCode;
	}

	public void setMarketPromotionCode(String marketPromotionCode) {
		this.marketPromotionCode = marketPromotionCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	@NotDbField
	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

}