package com.cheriscon.common.model;

/**
 * 支付日志
 * 
 * @author caozhiyong
 */
public class PayLog implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5958381238938231138L;
	
	private Integer id;
	/**
	 * 订单号
	 */
	private String out_trade_no;
	/**
	 * 支付交易号
	 */
	private String trade_no;
	/**
	 * 支付状态
	 */
	private String trade_status;
	/**
	 * 验证结果
	 */
	private Integer result;
	
	private String ip;  //请求IP
	
	private String createTime; //请求时间
	
	public String getIp() {
		return ip;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public PayLog() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getTrade_no() {
		return trade_no;
	}

	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}

	public String getTrade_status() {
		return trade_status;
	}

	public void setTrade_status(String trade_status) {
		this.trade_status = trade_status;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}


	
	
	
	

}