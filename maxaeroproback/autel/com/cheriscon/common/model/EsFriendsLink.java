package com.cheriscon.common.model;



/**
 * AbstractEsFriendsLink entity provides the base persistence definition of the EsFriendsLink entity. @author MyEclipse Persistence Tools
 */

public abstract class EsFriendsLink  implements java.io.Serializable {


    // Fields    

     private Integer linkId;
     private String name;
     private String url;
     private String logo;
     private Short sort;


    // Constructors

    /** default constructor */
    public EsFriendsLink() {
    }

    
    /** full constructor */
    public EsFriendsLink(String name, String url, String logo, Short sort) {
        this.name = name;
        this.url = url;
        this.logo = logo;
        this.sort = sort;
    }

   
    // Property accessors

    public Integer getLinkId() {
        return this.linkId;
    }
    
    public void setLinkId(Integer linkId) {
        this.linkId = linkId;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return this.logo;
    }
    
    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Short getSort() {
        return this.sort;
    }
    
    public void setSort(Short sort) {
        this.sort = sort;
    }
   








}