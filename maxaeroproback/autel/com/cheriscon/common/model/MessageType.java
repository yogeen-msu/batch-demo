package com.cheriscon.common.model;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 消息分类
 * 
 * @author yinhb
 * 
 */
public class MessageType implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = -5390463439686337918L;
	private Integer id;
	private String code;			//编号
	private String name;			//名称
	private Integer messageTypeCount;   //临时字段 查询每个消息分类对应的条数
	private Integer messageCount; 
	
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getMessageTypeCount() {
		return messageTypeCount;
	}
	public void setMessageTypeCount(Integer messageTypeCount) {
		this.messageTypeCount = messageTypeCount;
	}
	public Integer getMessageCount() {
		return messageCount;
	}
	public void setMessageCount(Integer messageCount) {
		this.messageCount = messageCount;
	}
	
	

	
}