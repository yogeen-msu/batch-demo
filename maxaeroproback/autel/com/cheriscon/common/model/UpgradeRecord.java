package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

public class UpgradeRecord implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 239360636561034434L;
	
	private Integer upgradeRecordId;
	
	private String productSN; //产品序列号
	
	private String softCode; //软件code
	
	private String upgradeVersion; //升级版本
	
	private String  opCode ; //操作类型
	
	private String regCode; //注册密码
	
	private Integer upgradeTimestamp; //升级日期UTC
	
	private String upgradeTime; //升级日期
	
	private String softName; //软件名称
	

	@NotDbField
	public String getSoftName() {
		return softName;
	}

	public void setSoftName(String softName) {
		this.softName = softName;
	}
	
	public String getUpgradeTime() {
		return upgradeTime;
	}

	public void setUpgradeTime(String upgradeTime) {
		this.upgradeTime = upgradeTime;
	}

	public Integer getUpgradeRecordId() {
		return upgradeRecordId;
	}

	public String getProductSN() {
		return productSN;
	}

	public String getSoftCode() {
		return softCode;
	}

	public String getUpgradeVersion() {
		return upgradeVersion;
	}

	public String getOpCode() {
		return opCode;
	}

	public String getRegCode() {
		return regCode;
	}

	public Integer getUpgradeTimestamp() {
		return upgradeTimestamp;
	}

	public void setUpgradeRecordId(Integer upgradeRecordId) {
		this.upgradeRecordId = upgradeRecordId;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	public void setSoftCode(String softCode) {
		this.softCode = softCode;
	}

	public void setUpgradeVersion(String upgradeVersion) {
		this.upgradeVersion = upgradeVersion;
	}

	public void setOpCode(String opCode) {
		this.opCode = opCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public void setUpgradeTimestamp(Integer upgradeTimestamp) {
		this.upgradeTimestamp = upgradeTimestamp;
	}

}
