package com.cheriscon.common.model;



/**
 * AbstractDataField entity provides the base persistence definition of the DataField entity. @author MyEclipse Persistence Tools
 */

public abstract class DataField  implements java.io.Serializable {


    // Fields    

     private Integer fieldId;
     private String chinaName;
     private String englishName;
     private Integer dataType;
     private String dataSize;
     private String showForm;
     private String showValue;
     private Long addTime;
     private Integer modelId;
     private String saveValue;
     private Integer isValidate;
     private Integer taxis;
     private Integer dictId;
     private Integer isShow;


    // Constructors

    /** default constructor */
    public DataField() {
    }

    
    /** full constructor */
    public DataField(String chinaName, String englishName, Integer dataType, String dataSize, String showForm, String showValue, Long addTime, Integer modelId, String saveValue, Integer isValidate, Integer taxis, Integer dictId, Integer isShow) {
        this.chinaName = chinaName;
        this.englishName = englishName;
        this.dataType = dataType;
        this.dataSize = dataSize;
        this.showForm = showForm;
        this.showValue = showValue;
        this.addTime = addTime;
        this.modelId = modelId;
        this.saveValue = saveValue;
        this.isValidate = isValidate;
        this.taxis = taxis;
        this.dictId = dictId;
        this.isShow = isShow;
    }

   
    // Property accessors

    public Integer getFieldId() {
        return this.fieldId;
    }
    
    public void setFieldId(Integer fieldId) {
        this.fieldId = fieldId;
    }

    public String getChinaName() {
        return this.chinaName;
    }
    
    public void setChinaName(String chinaName) {
        this.chinaName = chinaName;
    }

    public String getEnglishName() {
        return this.englishName;
    }
    
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public Integer getDataType() {
        return this.dataType;
    }
    
    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public String getDataSize() {
        return this.dataSize;
    }
    
    public void setDataSize(String dataSize) {
        this.dataSize = dataSize;
    }

    public String getShowForm() {
        return this.showForm;
    }
    
    public void setShowForm(String showForm) {
        this.showForm = showForm;
    }

    public String getShowValue() {
        return this.showValue;
    }
    
    public void setShowValue(String showValue) {
        this.showValue = showValue;
    }

    public Long getAddTime() {
        return this.addTime;
    }
    
    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public Integer getModelId() {
        return this.modelId;
    }
    
    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getSaveValue() {
        return this.saveValue;
    }
    
    public void setSaveValue(String saveValue) {
        this.saveValue = saveValue;
    }

    public Integer getIsValidate() {
        return this.isValidate;
    }
    
    public void setIsValidate(Integer isValidate) {
        this.isValidate = isValidate;
    }

    public Integer getTaxis() {
        return this.taxis;
    }
    
    public void setTaxis(Integer taxis) {
        this.taxis = taxis;
    }

    public Integer getDictId() {
        return this.dictId;
    }
    
    public void setDictId(Integer dictId) {
        this.dictId = dictId;
    }

    public Integer getIsShow() {
        return this.isShow;
    }
    
    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }
   








}