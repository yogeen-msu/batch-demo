package com.cheriscon.common.model;


/**
 * 序列号销售区域与升级区域预警信息
 * 
 * @author chenqichuan
 * 
 */
public class UpWarningInfo implements java.io.Serializable {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 761614704138614175L;
	
	private Integer warningId;  //自动生成 id
	private String productSn;  //产品序列号
	private String warningCode;  //警告代码（-1 代表多个区域登陆，-2 表示没有找到对应的大区）
	private String saleCode; //销售契约 编号
	private String saleArea; //销售契约区域
	private String curArea; //当前区域
	private String updateNum; //当前区域登陆次数
	private Integer flag; //处理标识(1表示未处理，0表示已处理)
	
	public Integer getWarningId() {
		return warningId;
	}
	public String getProductSn() {
		return productSn;
	}
	public String getWarningCode() {
		return warningCode;
	}
	public String getSaleCode() {
		return saleCode;
	}
	public String getSaleArea() {
		return saleArea;
	}
	public String getCurArea() {
		return curArea;
	}
	public String getUpdateNum() {
		return updateNum;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setWarningId(Integer warningId) {
		this.warningId = warningId;
	}
	public void setProductSn(String productSn) {
		this.productSn = productSn;
	}
	public void setWarningCode(String warningCode) {
		this.warningCode = warningCode;
	}
	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}
	public void setSaleArea(String saleArea) {
		this.saleArea = saleArea;
	}
	public void setCurArea(String curArea) {
		this.curArea = curArea;
	}
	public void setUpdateNum(String updateNum) {
		this.updateNum = updateNum;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	
}