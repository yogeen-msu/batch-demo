package com.cheriscon.common.model;

import java.util.HashSet;
import java.util.Set;


/**
 * AbstractSysUser entity provides the base persistence definition of the SysUser entity. @author MyEclipse Persistence Tools
 */

public abstract class SysUser  implements java.io.Serializable {


    // Fields    

     private Integer userid;
     private String username;
     private String password;
     private Integer state;
     private String realname;
     private String userno;
     private String remark;
     private Integer dateline;
     private Set userRoles = new HashSet(0);


    // Constructors

    /** default constructor */
    public SysUser() {
    }

    
    /** full constructor */
    public SysUser(String username, String password, Integer state, String realname, String userno, String remark, Integer dateline, Set userRoles) {
        this.username = username;
        this.password = password;
        this.state = state;
        this.realname = realname;
        this.userno = userno;
        this.remark = remark;
        this.dateline = dateline;
        this.userRoles = userRoles;
    }

   
    // Property accessors

    public Integer getUserid() {
        return this.userid;
    }
    
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getState() {
        return this.state;
    }
    
    public void setState(Integer state) {
        this.state = state;
    }

    public String getRealname() {
        return this.realname;
    }
    
    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getUserno() {
        return this.userno;
    }
    
    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDateline() {
        return this.dateline;
    }
    
    public void setDateline(Integer dateline) {
        this.dateline = dateline;
    }

    public Set getUserRoles() {
        return this.userRoles;
    }
    
    public void setUserRoles(Set userRoles) {
        this.userRoles = userRoles;
    }
   








}