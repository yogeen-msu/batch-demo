package com.cheriscon.common.model;

import com.cheriscon.framework.database.PrimaryKeyField;

public class ProductRemoveLog implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6914689810784467084L;
	
	private int id;  //自动增长id
		
	private String proCode; //产品code
	
	private String customerCode ;  //原绑定人
	
	private String operateUser;  //操作人
	
	private String operateDate;  //操作时间
	
	private String operateIP;  //操作IP
	@PrimaryKeyField
	public int getId() {
		return id;
	}

	public String getProCode() {
		return proCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public String getOperateUser() {
		return operateUser;
	}

	public String getOperateDate() {
		return operateDate;
	}

	public String getOperateIP() {
		return operateIP;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public void setOperateUser(String operateUser) {
		this.operateUser = operateUser;
	}

	public void setOperateDate(String operateDate) {
		this.operateDate = operateDate;
	}

	public void setOperateIP(String operateIP) {
		this.operateIP = operateIP;
	}
	
	
}
