package com.cheriscon.common.model;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 区域配置详情
 * 
 * @author yinhb
 */
public class AreaConfigDetail implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = 3724718799254227798L;

	private Integer id;
	private String areaCode; 		// 区域Code
	private String areaCfgCode; 	// 区域配置Code
	
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getAreaCfgCode() {
		return areaCfgCode;
	}
	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	
}