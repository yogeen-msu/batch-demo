package com.cheriscon.common.model;

public class DataLoggingContent {

	private String id;
	private String datalogId;
	private String msgType;
	private String content;
	private String msgForm;
	private String createTime;
	private String isSynced;
	public String getId() {
		return id;
	}
	public String getDatalogId() {
		return datalogId;
	}
	public String getMsgType() {
		return msgType;
	}
	public String getContent() {
		return content;
	}
	public String getMsgForm() {
		return msgForm;
	}
	public String getCreateTime() {
		return createTime;
	}
	public String getIsSynced() {
		return isSynced;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setDatalogId(String datalogId) {
		this.datalogId = datalogId;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setMsgForm(String msgForm) {
		this.msgForm = msgForm;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public void setIsSynced(String isSynced) {
		this.isSynced = isSynced;
	}
	
}
