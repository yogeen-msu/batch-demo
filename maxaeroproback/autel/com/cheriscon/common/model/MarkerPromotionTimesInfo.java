package com.cheriscon.common.model;

/**
 * 
* @ClassName: MarkerPromotionTimesInfo
* @Description: 营销活动时间段信息表
* @author shaohu
* @date 2013-1-19 下午01:56:39
*
 */
public class MarkerPromotionTimesInfo implements java.io.Serializable {

	private static final long serialVersionUID = -7423825783806429478L;
	private Integer id;
	/**
	 * 营销活动对象编码
	 */
	private String marketPromotionCode;
	private String code;
	private String startTime;
	private String endTime;

	public MarkerPromotionTimesInfo() {
		super();
	}

	public MarkerPromotionTimesInfo(Integer id, String marketPromotionCode,
			String code, String startTime, String endTime) {
		super();
		this.id = id;
		this.marketPromotionCode = marketPromotionCode;
		this.code = code;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarketPromotionCode() {
		return marketPromotionCode;
	}

	public void setMarketPromotionCode(String marketPromotionCode) {
		this.marketPromotionCode = marketPromotionCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}