package com.cheriscon.common.model;



/**
 * 客诉回复信息实体类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */

public  class ReCustomerComplaintInfo  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	 private static final long serialVersionUID = 3160759228246838849L;
	 private Integer id;
     private String code;
     private String customerComplaintCode;
     private String reContent;
     private String reDate;
     private String rePerson;
     private String attachment;
     private Integer complaintCount;
     
     private Integer complaintScore;    //客诉评价分数
     private Integer rePersonType;      //回复人类型
     private String showAttachment; //显示附件名称
     private Integer sourceType; //标记1为经产品网站回复的消息，2为平板回复的消息
    // Constructors

    public Integer getSourceType() {
		return sourceType;
	}


	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}


	/** default constructor */
    public ReCustomerComplaintInfo() {
    }

    
    /** full constructor */
    public ReCustomerComplaintInfo(String code,String customerComplaintCode, String reContent, String reDate, String rePerson, String attachment) {
        this.code = code;
        this.customerComplaintCode = customerComplaintCode;
        this.reContent = reContent;
        this.reDate = reDate;
        this.rePerson = rePerson;
        this.attachment = attachment;
    }

   
    // Property accessors

    public String getCustomerComplaintCode() {
		return customerComplaintCode;
	}


	public void setCustomerComplaintCode(String customerComplaintCode) {
		this.customerComplaintCode = customerComplaintCode;
	}


	public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }



    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getReContent() {
        return this.reContent;
    }
    
    public void setReContent(String reContent) {
        this.reContent = reContent;
    }

    public String getReDate() {
        return this.reDate;
    }
    
    public void setReDate(String reDate) {
        this.reDate = reDate;
    }

    public String getRePerson() {
        return this.rePerson;
    }
    
    public void setRePerson(String rePerson) {
        this.rePerson = rePerson;
    }

    public String getAttachment() {
        return this.attachment;
    }
    
    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }


	public Integer getComplaintCount() {
		return complaintCount;
	}


	public void setComplaintCount(Integer complaintCount) {
		this.complaintCount = complaintCount;
	}


	public Integer getComplaintScore() {
		return complaintScore;
	}


	public void setComplaintScore(Integer complaintScore) {
		this.complaintScore = complaintScore;
	}


	public Integer getRePersonType() {
		return rePersonType;
	}


	public void setRePersonType(Integer rePersonType) {
		this.rePersonType = rePersonType;
	}


	public String getShowAttachment() {
		return showAttachment;
	}


	public void setShowAttachment(String showAttachment) {
		this.showAttachment = showAttachment;
	}
    
    


}