package com.cheriscon.common.model;



/**
 * AbstractCopSite entity provides the base persistence definition of the CopSite entity. @author MyEclipse Persistence Tools
 */

public abstract class CopSite  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer userid;
     private String sitename;
     private String productid;
     private String descript;
     private String icofile;
     private String logofile;
     private Integer deleteflag;
     private String keywords;
     private String themepath;
     private Integer adminthemeid;
     private Integer themeid;
     private Integer point;
     private Long createtime;
     private Long lastlogin;
     private Long lastgetpoint;
     private Integer logincount;
     private String bkloginpicfile;
     private String bklogofile;
     private Long sumpoint;
     private Long sumaccess;
     private String title;
     private String username;
     private Integer usersex;
     private String usertel;
     private String usermobile;
     private String usertel1;
     private String useremail;
     private Integer state;
     private String qqlist;
     private String msnlist;
     private String wwlist;
     private String tellist;
     private String worktime;
     private Integer siteon;
     private String closereson;
     private String copyright;
     private String icp;
     private String address;
     private String zipcode;
     private Integer qq;
     private Integer msn;
     private Integer ww;
     private Integer tel;
     private Integer wt;
     private String linkman;
     private String linktel;
     private String email;
     private Short multiSite;
     private String relid;
     private Short sitestate;
     private Short isimported;
     private Integer imptype;


    // Constructors

    /** default constructor */
    public CopSite() {
    }

    
    /** full constructor */
    public CopSite(Integer userid, String sitename, String productid, String descript, String icofile, String logofile, Integer deleteflag, String keywords, String themepath, Integer adminthemeid, Integer themeid, Integer point, Long createtime, Long lastlogin, Long lastgetpoint, Integer logincount, String bkloginpicfile, String bklogofile, Long sumpoint, Long sumaccess, String title, String username, Integer usersex, String usertel, String usermobile, String usertel1, String useremail, Integer state, String qqlist, String msnlist, String wwlist, String tellist, String worktime, Integer siteon, String closereson, String copyright, String icp, String address, String zipcode, Integer qq, Integer msn, Integer ww, Integer tel, Integer wt, String linkman, String linktel, String email, Short multiSite, String relid, Short sitestate, Short isimported, Integer imptype) {
        this.userid = userid;
        this.sitename = sitename;
        this.productid = productid;
        this.descript = descript;
        this.icofile = icofile;
        this.logofile = logofile;
        this.deleteflag = deleteflag;
        this.keywords = keywords;
        this.themepath = themepath;
        this.adminthemeid = adminthemeid;
        this.themeid = themeid;
        this.point = point;
        this.createtime = createtime;
        this.lastlogin = lastlogin;
        this.lastgetpoint = lastgetpoint;
        this.logincount = logincount;
        this.bkloginpicfile = bkloginpicfile;
        this.bklogofile = bklogofile;
        this.sumpoint = sumpoint;
        this.sumaccess = sumaccess;
        this.title = title;
        this.username = username;
        this.usersex = usersex;
        this.usertel = usertel;
        this.usermobile = usermobile;
        this.usertel1 = usertel1;
        this.useremail = useremail;
        this.state = state;
        this.qqlist = qqlist;
        this.msnlist = msnlist;
        this.wwlist = wwlist;
        this.tellist = tellist;
        this.worktime = worktime;
        this.siteon = siteon;
        this.closereson = closereson;
        this.copyright = copyright;
        this.icp = icp;
        this.address = address;
        this.zipcode = zipcode;
        this.qq = qq;
        this.msn = msn;
        this.ww = ww;
        this.tel = tel;
        this.wt = wt;
        this.linkman = linkman;
        this.linktel = linktel;
        this.email = email;
        this.multiSite = multiSite;
        this.relid = relid;
        this.sitestate = sitestate;
        this.isimported = isimported;
        this.imptype = imptype;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return this.userid;
    }
    
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getSitename() {
        return this.sitename;
    }
    
    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getProductid() {
        return this.productid;
    }
    
    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getDescript() {
        return this.descript;
    }
    
    public void setDescript(String descript) {
        this.descript = descript;
    }

    public String getIcofile() {
        return this.icofile;
    }
    
    public void setIcofile(String icofile) {
        this.icofile = icofile;
    }

    public String getLogofile() {
        return this.logofile;
    }
    
    public void setLogofile(String logofile) {
        this.logofile = logofile;
    }

    public Integer getDeleteflag() {
        return this.deleteflag;
    }
    
    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getKeywords() {
        return this.keywords;
    }
    
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getThemepath() {
        return this.themepath;
    }
    
    public void setThemepath(String themepath) {
        this.themepath = themepath;
    }

    public Integer getAdminthemeid() {
        return this.adminthemeid;
    }
    
    public void setAdminthemeid(Integer adminthemeid) {
        this.adminthemeid = adminthemeid;
    }

    public Integer getThemeid() {
        return this.themeid;
    }
    
    public void setThemeid(Integer themeid) {
        this.themeid = themeid;
    }

    public Integer getPoint() {
        return this.point;
    }
    
    public void setPoint(Integer point) {
        this.point = point;
    }

    public Long getCreatetime() {
        return this.createtime;
    }
    
    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getLastlogin() {
        return this.lastlogin;
    }
    
    public void setLastlogin(Long lastlogin) {
        this.lastlogin = lastlogin;
    }

    public Long getLastgetpoint() {
        return this.lastgetpoint;
    }
    
    public void setLastgetpoint(Long lastgetpoint) {
        this.lastgetpoint = lastgetpoint;
    }

    public Integer getLogincount() {
        return this.logincount;
    }
    
    public void setLogincount(Integer logincount) {
        this.logincount = logincount;
    }

    public String getBkloginpicfile() {
        return this.bkloginpicfile;
    }
    
    public void setBkloginpicfile(String bkloginpicfile) {
        this.bkloginpicfile = bkloginpicfile;
    }

    public String getBklogofile() {
        return this.bklogofile;
    }
    
    public void setBklogofile(String bklogofile) {
        this.bklogofile = bklogofile;
    }

    public Long getSumpoint() {
        return this.sumpoint;
    }
    
    public void setSumpoint(Long sumpoint) {
        this.sumpoint = sumpoint;
    }

    public Long getSumaccess() {
        return this.sumaccess;
    }
    
    public void setSumaccess(Long sumaccess) {
        this.sumaccess = sumaccess;
    }

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUsersex() {
        return this.usersex;
    }
    
    public void setUsersex(Integer usersex) {
        this.usersex = usersex;
    }

    public String getUsertel() {
        return this.usertel;
    }
    
    public void setUsertel(String usertel) {
        this.usertel = usertel;
    }

    public String getUsermobile() {
        return this.usermobile;
    }
    
    public void setUsermobile(String usermobile) {
        this.usermobile = usermobile;
    }

    public String getUsertel1() {
        return this.usertel1;
    }
    
    public void setUsertel1(String usertel1) {
        this.usertel1 = usertel1;
    }

    public String getUseremail() {
        return this.useremail;
    }
    
    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public Integer getState() {
        return this.state;
    }
    
    public void setState(Integer state) {
        this.state = state;
    }

    public String getQqlist() {
        return this.qqlist;
    }
    
    public void setQqlist(String qqlist) {
        this.qqlist = qqlist;
    }

    public String getMsnlist() {
        return this.msnlist;
    }
    
    public void setMsnlist(String msnlist) {
        this.msnlist = msnlist;
    }

    public String getWwlist() {
        return this.wwlist;
    }
    
    public void setWwlist(String wwlist) {
        this.wwlist = wwlist;
    }

    public String getTellist() {
        return this.tellist;
    }
    
    public void setTellist(String tellist) {
        this.tellist = tellist;
    }

    public String getWorktime() {
        return this.worktime;
    }
    
    public void setWorktime(String worktime) {
        this.worktime = worktime;
    }

    public Integer getSiteon() {
        return this.siteon;
    }
    
    public void setSiteon(Integer siteon) {
        this.siteon = siteon;
    }

    public String getClosereson() {
        return this.closereson;
    }
    
    public void setClosereson(String closereson) {
        this.closereson = closereson;
    }

    public String getCopyright() {
        return this.copyright;
    }
    
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getIcp() {
        return this.icp;
    }
    
    public void setIcp(String icp) {
        this.icp = icp;
    }

    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return this.zipcode;
    }
    
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Integer getQq() {
        return this.qq;
    }
    
    public void setQq(Integer qq) {
        this.qq = qq;
    }

    public Integer getMsn() {
        return this.msn;
    }
    
    public void setMsn(Integer msn) {
        this.msn = msn;
    }

    public Integer getWw() {
        return this.ww;
    }
    
    public void setWw(Integer ww) {
        this.ww = ww;
    }

    public Integer getTel() {
        return this.tel;
    }
    
    public void setTel(Integer tel) {
        this.tel = tel;
    }

    public Integer getWt() {
        return this.wt;
    }
    
    public void setWt(Integer wt) {
        this.wt = wt;
    }

    public String getLinkman() {
        return this.linkman;
    }
    
    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getLinktel() {
        return this.linktel;
    }
    
    public void setLinktel(String linktel) {
        this.linktel = linktel;
    }

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public Short getMultiSite() {
        return this.multiSite;
    }
    
    public void setMultiSite(Short multiSite) {
        this.multiSite = multiSite;
    }

    public String getRelid() {
        return this.relid;
    }
    
    public void setRelid(String relid) {
        this.relid = relid;
    }

    public Short getSitestate() {
        return this.sitestate;
    }
    
    public void setSitestate(Short sitestate) {
        this.sitestate = sitestate;
    }

    public Short getIsimported() {
        return this.isimported;
    }
    
    public void setIsimported(Short isimported) {
        this.isimported = isimported;
    }

    public Integer getImptype() {
        return this.imptype;
    }
    
    public void setImptype(Integer imptype) {
        this.imptype = imptype;
    }
   








}