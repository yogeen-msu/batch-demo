package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 用户账号安全问题
 * 
 * @author yinhb
 */
public class QuestionInfo implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = -3260185041368499112L;
	
	private Integer id;
	private String code;
	private String questionDesc;


 	/**  非表字段  begin */
	private String adminLanguageCode;		//管理员首选语言code

 	/**  非表字段 end */

	@PrimaryKeyField
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	

 	/**  非表字段  begin */

	@NotDbField
	public String getQuestionDesc() {
		return this.questionDesc;
	}

	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}

	@NotDbField
	public String getAdminLanguageCode() {
		return adminLanguageCode;
	}

	public void setAdminLanguageCode(String adminLanguageCode) {
		this.adminLanguageCode = adminLanguageCode;
	}

 	/**  非表字段  end */

	
	

}