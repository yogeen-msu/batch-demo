package com.cheriscon.common.model;



/**
 * AbstractEsUserRole entity provides the base persistence definition of the EsUserRole entity. @author MyEclipse Persistence Tools
 */

public abstract class EsUserRole  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer userid;
     private Integer roleid;


    // Constructors

    /** default constructor */
    public EsUserRole() {
    }

    
    /** full constructor */
    public EsUserRole(Integer userid, Integer roleid) {
        this.userid = userid;
        this.roleid = roleid;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return this.userid;
    }
    
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getRoleid() {
        return this.roleid;
    }
    
    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }
   








}