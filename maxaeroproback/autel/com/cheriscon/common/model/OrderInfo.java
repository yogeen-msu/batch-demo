package com.cheriscon.common.model;


import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 订单表
 */
public class OrderInfo implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = 3753065662498855860L;
	
	private Integer id;
	private String code;
	private String userCode;				//用户code
//	private String orderNo;					//订单编号
	private String orderMoney;				//订单金额
	private String orderDate;				//下单日期
	private Integer orderState;				//订单状态
	private Integer orderPayState;			//订单支付状态（0：未支付 1：已支付）
	private Integer recConfirmState;		//收款确认状态（0：未确认 1：已确认）
	private String discountMoney;			//优惠金额
	private String bankNumber;				//银行流水号
	private Integer processState; //处理状态 1：已处理  0：未处理
	private String customerName; //客户名称
	private String payDate;				//支付日期
	private String recConfirmDate;				//确认收款日期
	private String processDate;				//处理日期
	
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}
	
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public String getRecConfirmDate() {
		return recConfirmDate;
	}
	public void setRecConfirmDate(String recConfirmDate) {
		this.recConfirmDate = recConfirmDate;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
//	public String getOrderNo() {
//		return orderNo;
//	}
//	public void setOrderNo(String orderNo) {
//		this.orderNo = orderNo;
//	}
	public String getOrderMoney() {
		return orderMoney;
	}
	public void setOrderMoney(String orderMoney) {
		this.orderMoney = orderMoney;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public Integer getOrderState() {
		return orderState;
	}
	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}
	public Integer getOrderPayState() {
		return orderPayState;
	}
	public void setOrderPayState(Integer orderPayState) {
		this.orderPayState = orderPayState;
	}
	public Integer getRecConfirmState() {
		return recConfirmState;
	}
	public void setRecConfirmState(Integer recConfirmState) {
		this.recConfirmState = recConfirmState;
	}
	public String getDiscountMoney() {
		return discountMoney;
	}
	public void setDiscountMoney(String discountMoney) {
		this.discountMoney = discountMoney;
	}
	public String getBankNumber() {
		return bankNumber;
	}
	public void setBankNumber(String bankNumber) {
		this.bankNumber = bankNumber;
	}
	
	@NotDbField
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getProcessState() {
		return processState;
	}
	public void setProcessState(Integer processState) {
		this.processState = processState;
	}

	public String getProcessDate() {
		return processDate;
	}

	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}
	

	
}