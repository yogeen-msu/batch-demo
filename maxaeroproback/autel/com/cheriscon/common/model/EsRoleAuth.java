package com.cheriscon.common.model;



/**
 * AbstractEsRoleAuth entity provides the base persistence definition of the EsRoleAuth entity. @author MyEclipse Persistence Tools
 */

public abstract class EsRoleAuth  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer roleid;
     private Integer authid;


    // Constructors

    /** default constructor */
    public EsRoleAuth() {
    }

    
    /** full constructor */
    public EsRoleAuth(Integer roleid, Integer authid) {
        this.roleid = roleid;
        this.authid = authid;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleid() {
        return this.roleid;
    }
    
    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public Integer getAuthid() {
        return this.authid;
    }
    
    public void setAuthid(Integer authid) {
        this.authid = authid;
    }
   








}