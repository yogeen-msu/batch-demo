package com.cheriscon.common.model;

import java.util.HashSet;
import java.util.Set;


/**
 * AbstractAuthority entity provides the base persistence definition of the Authority entity. @author MyEclipse Persistence Tools
 */

public abstract class Authority  implements java.io.Serializable {


    // Fields    

     private Integer actid;
     private String name;
     private String type;
     private String objvalue;
     private Set roleAuths = new HashSet(0);


    // Constructors

    /** default constructor */
    public Authority() {
    }

    
    /** full constructor */
    public Authority(String name, String type, String objvalue, Set roleAuths) {
        this.name = name;
        this.type = type;
        this.objvalue = objvalue;
        this.roleAuths = roleAuths;
    }

   
    // Property accessors

    public Integer getActid() {
        return this.actid;
    }
    
    public void setActid(Integer actid) {
        this.actid = actid;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    public String getObjvalue() {
        return this.objvalue;
    }
    
    public void setObjvalue(String objvalue) {
        this.objvalue = objvalue;
    }

    public Set getRoleAuths() {
        return this.roleAuths;
    }
    
    public void setRoleAuths(Set roleAuths) {
        this.roleAuths = roleAuths;
    }
   








}