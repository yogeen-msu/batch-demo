package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;



/**
 * 在线客服权限实体类
 * @author pengdongan
 * @version 创建时间：2013-06-05
 */

public  class OnlineAdminuserInfo  implements java.io.Serializable {


    // Fields 
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = -3695796013761265783L;
	
	private Integer id;
     private String onlineTypeCode;
     private String adminUserid;



  	/**  非表字段  begin */
  	
  	private String languageCode;		//语言code
  	private String languageName;		//语言名称
  	private String onlineTypeName;		//在线客服类型名称
    private String adminUserids;		//adminUserids
  	
  	/**  非表字段  end */
  	
    // Constructors

    /** default constructor */
    public OnlineAdminuserInfo() {
    }


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getOnlineTypeCode() {
		return onlineTypeCode;
	}


	public void setOnlineTypeCode(String onlineTypeCode) {
		this.onlineTypeCode = onlineTypeCode;
	}



	public String getAdminUserid() {
		return adminUserid;
	}


	public void setAdminUserid(String adminUserid) {
		this.adminUserid = adminUserid;
	}


	/**  非表字段  begin */
	@NotDbField
	public String getOnlineTypeName() {
		return onlineTypeName;
	}


	public void setOnlineTypeName(String onlineTypeName) {
		this.onlineTypeName = onlineTypeName;
	}


	@NotDbField
	public String getLanguageCode() {
		return languageCode;
	}


	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}


	@NotDbField
	public String getLanguageName() {
		return languageName;
	}


	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}


	@NotDbField
	public String getAdminUserids() {
		return adminUserids;
	}


	public void setAdminUserids(String adminUserids) {
		this.adminUserids = adminUserids;
	}
	
	/**  非表字段  end */


}