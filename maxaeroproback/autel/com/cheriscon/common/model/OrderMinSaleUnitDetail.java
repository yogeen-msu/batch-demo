package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 订单最小销售单位表
 * 
 * @author yinhb
 */
public class OrderMinSaleUnitDetail implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = -994461975930729236L;
	
	private Integer id;
	private String code;
	private String orderCode; 					// 订单code
	private String productSerialNo; 			// 产品序列号
	private String minSaleUnitCode; 			// 最小销售单位Code
	private Integer consumeType;				// 消费类型（0：购买 1：续费）
	private Double minSaleUnitPrice; 				// 最小销售单位价格
	
	private String saleName; //最小销售单位名称
	private Integer year;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public String getProductSerialNo() {
		return productSerialNo;
	}
	public void setProductSerialNo(String productSerialNo) {
		this.productSerialNo = productSerialNo;
	}
	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	public Integer getConsumeType() {
		return consumeType;
	}
	public void setConsumeType(Integer consumeType) {
		this.consumeType = consumeType;
	}

	public Double getMinSaleUnitPrice() {
		return minSaleUnitPrice;
	}
	public void setMinSaleUnitPrice(Double minSaleUnitPrice) {
		this.minSaleUnitPrice = minSaleUnitPrice;
	}
	@NotDbField
	public String getSaleName() {
		return saleName;
	}
	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	
}