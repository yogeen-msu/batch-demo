package com.cheriscon.common.model;

/**
 * B2B商城自动登录，注册州/省编码
 * @author 高亚雄
 * @date 2016/4/5
 *
 */
public class StateProvince implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String state;//州/省名称
	private String code;//州/省编码
	private Integer countryId;//国家编码id
	private String country;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
