package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * @remark	客户信息实体
 * @date	2013-01-10
 * @table	DT_CustomerInfo
 * @author pengdongan
 *
 */
public class CustomerInfo implements Serializable{

	private static final long serialVersionUID = -7049151125267907818L;
	
	private Integer id;					//客户信息ID
	private String code;				//客户信息code
	private String autelId;				//AutelId
	private String userPwd;				//用户密码
	private String firstName;			//第一个名字
	private String middleName;			//中间名字
	private String lastName;			//最后名字
	private String name;				//用户名
	private String sex;					//性别
	private String birthdate;			//出生日期
	private String daytimePhone;		//固定电话
	private String daytimePhoneCC;		//固定电话国家代码
	private String daytimePhoneAC;		//固定电话区域代码
	private String daytimePhoneExtCode;	//固定电话分机号
	private String mobilePhone;			//移动电话
	private String mobilePhoneCC;		//移动电话国家代码
	private String mobilePhoneAC;		//移动电话区域代码
	private String regTime;				//注册时间
	private String languageCode;		//语言code
	private String country;				//国家
	private String city;				//城市
	private String address;				//地址
	private String company;				//公司
	private String zipCode;				//邮编
	private String questionCode;		//安全问题code
	private String answer;				//安全问题答案
	private String comUsername;			//论坛名字
	private Integer actState;			//autelId激活状态
	private String actCode;				//激活码
	private Integer isAllowSendEmail;	//是否允许发邮件
	private String lastLoginTime;		//最后登录时间
	private String secondEmail;		//用户第二邮箱
	private String sendActiveTime;  //发送邮箱账号激活时间
	private String sendResetPasswordTime;//发送重置密码时间
	private Integer secondActState;//第二邮箱激活状态(0:未激活 1：已激活)
	private String sendSecondEmailActiveTime;//发送第二邮箱账号激活时间
	private Integer sourceType; //数据来源途径，0为产品网站注册用户，1为企业网站用户，2为MaxiDas用户


	private String serialNo;//产品序列号 冗余字段
	private String proRegTime;//产品注册时间冗余字段
	private String expTime;//产品过期时间冗余字段
	private String comName;//用户全名=第一个名字+中间名字+最后名字
	private String province; //州/省
	private String proDate; //产品出厂日期
	private String Warrantymonth; //硬件保修期
	private String warrantyDate;//硬件保修期过期日期
	
	private String telephone;
	private String freshdeskId;
	private String facebookId;
	
	public String getWarrantyDate() {
		return warrantyDate;
	}

	public void setWarrantyDate(String warrantyDate) {
		this.warrantyDate = warrantyDate;
	}

	public String getWarrantymonth() {
		return Warrantymonth;
	}

	public void setWarrantymonth(String warrantymonth) {
		Warrantymonth = warrantymonth;
	}

	public String getProDate() {
		return proDate;
	}

	public void setProDate(String proDate) {
		this.proDate = proDate;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public CustomerInfo() {
		super();
	}

	public CustomerInfo(Integer id, String code, String autelId, String userPwd, String firstName,String sex,String birthdate,
			String middleName, String lastName, String name,String daytimePhone, String daytimePhoneCC,String daytimePhoneAC,
			String daytimePhoneExtCode, String mobilePhone, String mobilePhoneCC, String mobilePhoneAC,
			String regTime, String languageCode, String country, String city, String address,
			String company, String zipCode, String questionCode, String answer, String comUsername,
			Integer actState, String actCode, Integer isAllowSendEmail,String lastLoginTime,String secondEmail,String province) {
		this.id = id;
		this.code = code;
		this.autelId = autelId;
		this.userPwd = userPwd;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.name = name;
		this.sex = sex;
		this.birthdate = birthdate;
		this.daytimePhone = daytimePhone;
		this.daytimePhoneCC = daytimePhoneCC;
		this.daytimePhoneAC = daytimePhoneAC;
		this.daytimePhoneExtCode = daytimePhoneExtCode;
		this.mobilePhone = mobilePhone;
		this.mobilePhoneCC = mobilePhoneCC;
		this.mobilePhoneAC = mobilePhoneAC;
		this.regTime = regTime;
		this.languageCode = languageCode;
		this.country = country;
		this.city = city;
		this.address = address;
		this.company = company;
		this.zipCode = zipCode;
		this.questionCode = questionCode;
		this.answer = answer;
		this.comUsername = comUsername;
		this.actState = actState;
		this.actCode = actCode;
		this.isAllowSendEmail = isAllowSendEmail;
		this.lastLoginTime = lastLoginTime;
		this.secondEmail = secondEmail;
		this.province=province;
	}

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getDaytimePhone() {
		return daytimePhone;
	}

	public void setDaytimePhone(String daytimePhone) {
		this.daytimePhone = daytimePhone;
	}

	public String getDaytimePhoneCC() {
		return daytimePhoneCC;
	}

	public void setDaytimePhoneCC(String daytimePhoneCC) {
		this.daytimePhoneCC = daytimePhoneCC;
	}

	public String getDaytimePhoneAC() {
		return daytimePhoneAC;
	}

	public void setDaytimePhoneAC(String daytimePhoneAC) {
		this.daytimePhoneAC = daytimePhoneAC;
	}

	public String getDaytimePhoneExtCode() {
		return daytimePhoneExtCode;
	}

	public void setDaytimePhoneExtCode(String daytimePhoneExtCode) {
		this.daytimePhoneExtCode = daytimePhoneExtCode;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMobilePhoneCC() {
		return mobilePhoneCC;
	}

	public void setMobilePhoneCC(String mobilePhoneCC) {
		this.mobilePhoneCC = mobilePhoneCC;
	}

	public String getMobilePhoneAC() {
		return mobilePhoneAC;
	}

	public void setMobilePhoneAC(String mobilePhoneAC) {
		this.mobilePhoneAC = mobilePhoneAC;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getComUsername() {
		return comUsername;
	}

	public void setComUsername(String comUsername) {
		this.comUsername = comUsername;
	}

	public Integer getActState() {
		return actState;
	}

	public void setActState(Integer actState) {
		this.actState = actState;
	}

	public String getActCode() {
		return actCode;
	}

	public void setActCode(String actCode) {
		this.actCode = actCode;
	}

	public Integer getIsAllowSendEmail() {
		return isAllowSendEmail;
	}

	public void setIsAllowSendEmail(Integer isAllowSendEmail) {
		this.isAllowSendEmail = isAllowSendEmail;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getSecondEmail() {
		return secondEmail;
	}

	public void setSecondEmail(String secondEmail) {
		this.secondEmail = secondEmail;
	}

	public String getSendActiveTime() {
		return sendActiveTime;
	}

	public void setSendActiveTime(String sendActiveTime) {
		this.sendActiveTime = sendActiveTime;
	}

	public String getSendResetPasswordTime() {
		return sendResetPasswordTime;
	}

	public void setSendResetPasswordTime(String sendResetPasswordTime) {
		this.sendResetPasswordTime = sendResetPasswordTime;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getProRegTime() {
		return proRegTime;
	}

	public void setProRegTime(String proRegTime) {
		this.proRegTime = proRegTime;
	}

	public String getExpTime() {
		return expTime;
	}

	public void setExpTime(String expTime) {
		this.expTime = expTime;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public Integer getSecondActState() {
		return secondActState;
	}

	public void setSecondActState(Integer secondActState) {
		this.secondActState = secondActState;
	}

	public String getSendSecondEmailActiveTime() {
		return sendSecondEmailActiveTime;
	}

	public void setSendSecondEmailActiveTime(String sendSecondEmailActiveTime) {
		this.sendSecondEmailActiveTime = sendSecondEmailActiveTime;
	}
	
	
	public Integer getSourceType() {
		return sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFreshdeskId() {
		return freshdeskId;
	}

	public void setFreshdeskId(String freshdeskId) {
		this.freshdeskId = freshdeskId;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
	
	
}
