package com.cheriscon.common.model;

/**
 * AbstractCustomerProInfo entity provides the base persistence definition of
 * the CustomerProInfo entity. @author MyEclipse Persistence Tools
 */

/**
 * 客户产品关联信息
 */
public class CustomerProInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String customerCode; // 客户code
	private String proCode; // 产品code

	// Constructors

	/** default constructor */
	public CustomerProInfo() {
	}

	public CustomerProInfo(Integer id, String customerCode, String proCode) {
		super();
		this.id = id;
		this.customerCode = customerCode;
		this.proCode = proCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}



}