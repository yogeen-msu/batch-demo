package com.cheriscon.common.model;



/**
 * AbstractProductUpgradeRecord entity provides the base persistence definition of the ProductUpgradeRecord entity. @author MyEclipse Persistence Tools
 */

public abstract class ProductUpgradeRecord  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private SoftwareType softwareType;
     private ProductInfo productInfo;
     private String code;
     private String versionCode;
     private String upgradeTime;


    // Constructors

    /** default constructor */
    public ProductUpgradeRecord() {
    }

    
    /** full constructor */
    public ProductUpgradeRecord(SoftwareType softwareType, ProductInfo productInfo, String code, String versionCode, String upgradeTime) {
        this.softwareType = softwareType;
        this.productInfo = productInfo;
        this.code = code;
        this.versionCode = versionCode;
        this.upgradeTime = upgradeTime;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public SoftwareType getSoftwareType() {
        return this.softwareType;
    }
    
    public void setSoftwareType(SoftwareType softwareType) {
        this.softwareType = softwareType;
    }

    public ProductInfo getProductInfo() {
        return this.productInfo;
    }
    
    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getVersionCode() {
        return this.versionCode;
    }
    
    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getUpgradeTime() {
        return this.upgradeTime;
    }
    
    public void setUpgradeTime(String upgradeTime) {
        this.upgradeTime = upgradeTime;
    }
   








}