package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 
* @ClassName: SoftwareVersion
* @Description: 软件版本信息
* @author shaohu
* @date 2013-1-10 下午06:05:32
*
 */
public class SoftwareVersion implements java.io.Serializable {

	private static final long serialVersionUID = -8699507123154002532L;
	private Integer id;
	/**
	 * 软件类型Code
	 */
	private String softwareTypeCode;
	private String code;
	/**
	 * 版本名称
	 */
	private String versionName;
	/**
	 * 发布日期
	 */
	private String releaseDate;
	/**
	 * 发布状态 0表示未发布  1表示发布
	 */
	private Integer releaseState;
	
	/**
	 * 版本创建时间
	 */
	private String createDate;

	/**
	 * 基础包编码
	 */
	private String softwarePackCode;

	/**
	 * 基础包路径
	 */
	private String softPackPath;
	
	public SoftwareVersion() {
		super();
	}

	public SoftwareVersion(Integer id, String softwareTypeCode, String code,
			String versionName, String releaseDate, Integer releaseState,
			String createDate) {
		super();
		this.id = id;
		this.softwareTypeCode = softwareTypeCode;
		this.code = code;
		this.versionName = versionName;
		this.releaseDate = releaseDate;
		this.releaseState = releaseState;
		this.createDate = createDate;
	}

	public String getCreateDate() {
		return createDate;
	}


	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}

	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Integer getReleaseState() {
		return releaseState;
	}

	public void setReleaseState(Integer releaseState) {
		this.releaseState = releaseState;
	}

	@NotDbField
	public String getSoftwarePackCode() {
		return softwarePackCode;
	}

	public void setSoftwarePackCode(String softwarePackCode) {
		this.softwarePackCode = softwarePackCode;
	}

	@NotDbField
	public String getSoftPackPath() {
		return softPackPath;
	}

	public void setSoftPackPath(String softPackPath) {
		this.softPackPath = softPackPath;
	}

}