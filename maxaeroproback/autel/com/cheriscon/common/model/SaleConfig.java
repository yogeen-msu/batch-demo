package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 
* @ClassName: SaleConfig
* @Description: 销售配置实体表
* @author shaohu
* @date 2013-1-17 下午01:44:30
*
 */
public class SaleConfig implements java.io.Serializable {

	private static final long serialVersionUID = -8591623879627533348L;
	private Integer id;
	
	/**
	 * 产品型号编码
	 */
	private String proTypeCode;
	private String code;
	private String name;
	private String picPath;
	
	/**
	 * 产品型号名称
	 */
	private String proTypeName;

	public SaleConfig() {
		super();
	}

	public SaleConfig(Integer id, String proTypeCode, String code, String name,
			String proTypeName) {
		super();
		this.id = id;
		this.proTypeCode = proTypeCode;
		this.code = code;
		this.name = name;
		this.proTypeName = proTypeName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	@NotDbField
	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

}