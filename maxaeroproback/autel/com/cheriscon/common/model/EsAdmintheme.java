package com.cheriscon.common.model;



/**
 * AbstractEsAdmintheme entity provides the base persistence definition of the EsAdmintheme entity. @author MyEclipse Persistence Tools
 */

public abstract class EsAdmintheme  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String appid;
     private Integer siteid;
     private String themename;
     private String path;
     private Integer userid;
     private String author;
     private String version;
     private Integer framemode;
     private Integer deleteflag;
     private String thumb;


    // Constructors

    /** default constructor */
    public EsAdmintheme() {
    }

    
    /** full constructor */
    public EsAdmintheme(String appid, Integer siteid, String themename, String path, Integer userid, String author, String version, Integer framemode, Integer deleteflag, String thumb) {
        this.appid = appid;
        this.siteid = siteid;
        this.themename = themename;
        this.path = path;
        this.userid = userid;
        this.author = author;
        this.version = version;
        this.framemode = framemode;
        this.deleteflag = deleteflag;
        this.thumb = thumb;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppid() {
        return this.appid;
    }
    
    public void setAppid(String appid) {
        this.appid = appid;
    }

    public Integer getSiteid() {
        return this.siteid;
    }
    
    public void setSiteid(Integer siteid) {
        this.siteid = siteid;
    }

    public String getThemename() {
        return this.themename;
    }
    
    public void setThemename(String themename) {
        this.themename = themename;
    }

    public String getPath() {
        return this.path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }

    public Integer getUserid() {
        return this.userid;
    }
    
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getVersion() {
        return this.version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getFramemode() {
        return this.framemode;
    }
    
    public void setFramemode(Integer framemode) {
        this.framemode = framemode;
    }

    public Integer getDeleteflag() {
        return this.deleteflag;
    }
    
    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getThumb() {
        return this.thumb;
    }
    
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
   








}