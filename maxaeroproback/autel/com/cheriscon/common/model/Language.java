package com.cheriscon.common.model;

import com.cheriscon.framework.database.PrimaryKeyField;

public class Language {
	
	private int id;							//id
	private String code;					//编码
	private String name;					//名称
	private String countryCode;				//国家地区代码
	private String languageCode;			//国家语言代码
	private Integer isShow;                 //前台是否显示
	private String toolName;//临时字段 工具名称
	
	
	@PrimaryKeyField
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getToolName() {
		return toolName;
	}
	public void setToolName(String toolName) {
		this.toolName = toolName;
	}
	public Integer getIsShow() {
		return isShow;
	}
	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}
	
	
	
	
	
}
