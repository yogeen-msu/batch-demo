package com.cheriscon.common.model;



/**
 * AbstractEsCarArticle entity provides the base persistence definition of the EsCarArticle entity. @author MyEclipse Persistence Tools
 */

public abstract class EsCarArticle  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer sort;
     private Long addTime;
     private Long hit;
     private Long ableTime;
     private Integer state;
     private Long userId;
     private Integer catId;
     private Integer isCommend;
     private String title;
     private String img;
     private String content;
     private Integer sysLock;
     private Long lastmodified;
     private String pageTitle;
     private String pageKeywords;
     private String pageDescription;
     private Integer siteCode;
     private String siteidlist;


    // Constructors

    /** default constructor */
    public EsCarArticle() {
    }

    
    /** full constructor */
    public EsCarArticle(Integer sort, Long addTime, Long hit, Long ableTime, Integer state, Long userId, Integer catId, Integer isCommend, String title, String img, String content, Integer sysLock, Long lastmodified, String pageTitle, String pageKeywords, String pageDescription, Integer siteCode, String siteidlist) {
        this.sort = sort;
        this.addTime = addTime;
        this.hit = hit;
        this.ableTime = ableTime;
        this.state = state;
        this.userId = userId;
        this.catId = catId;
        this.isCommend = isCommend;
        this.title = title;
        this.img = img;
        this.content = content;
        this.sysLock = sysLock;
        this.lastmodified = lastmodified;
        this.pageTitle = pageTitle;
        this.pageKeywords = pageKeywords;
        this.pageDescription = pageDescription;
        this.siteCode = siteCode;
        this.siteidlist = siteidlist;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSort() {
        return this.sort;
    }
    
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getAddTime() {
        return this.addTime;
    }
    
    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public Long getHit() {
        return this.hit;
    }
    
    public void setHit(Long hit) {
        this.hit = hit;
    }

    public Long getAbleTime() {
        return this.ableTime;
    }
    
    public void setAbleTime(Long ableTime) {
        this.ableTime = ableTime;
    }

    public Integer getState() {
        return this.state;
    }
    
    public void setState(Integer state) {
        this.state = state;
    }

    public Long getUserId() {
        return this.userId;
    }
    
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getCatId() {
        return this.catId;
    }
    
    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getIsCommend() {
        return this.isCommend;
    }
    
    public void setIsCommend(Integer isCommend) {
        this.isCommend = isCommend;
    }

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return this.img;
    }
    
    public void setImg(String img) {
        this.img = img;
    }

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSysLock() {
        return this.sysLock;
    }
    
    public void setSysLock(Integer sysLock) {
        this.sysLock = sysLock;
    }

    public Long getLastmodified() {
        return this.lastmodified;
    }
    
    public void setLastmodified(Long lastmodified) {
        this.lastmodified = lastmodified;
    }

    public String getPageTitle() {
        return this.pageTitle;
    }
    
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageKeywords() {
        return this.pageKeywords;
    }
    
    public void setPageKeywords(String pageKeywords) {
        this.pageKeywords = pageKeywords;
    }

    public String getPageDescription() {
        return this.pageDescription;
    }
    
    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }

    public Integer getSiteCode() {
        return this.siteCode;
    }
    
    public void setSiteCode(Integer siteCode) {
        this.siteCode = siteCode;
    }

    public String getSiteidlist() {
        return this.siteidlist;
    }
    
    public void setSiteidlist(String siteidlist) {
        this.siteidlist = siteidlist;
    }
   








}