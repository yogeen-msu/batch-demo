package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * @remark	在线客服类型名称实体
 * @date	2013-06-02
 * @table	DT_OnlineServiceTypeName
 * @author pengdongan
 *
 */
public class OnlineServiceTypeName implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7172700938779602704L;
	//在线客服类型名称id
	private Integer id;
	//在线客服类型code
	private String onlineTypeCode;
	//语言类型code
	private String languageCode;
	//在线客服类型名称
	private String name;
	
	

 	/**  非表字段  begin */
	
	private String languageName;					//语言类型名称

 	/**  非表字段  end */
	
	
	public OnlineServiceTypeName() {
		super();
	}

	public OnlineServiceTypeName(Integer id, String onlineTypeCode, String languageCode, String name) {
		this.id = id;
		this.onlineTypeCode = onlineTypeCode;
		this.languageCode = languageCode;
		this.name = name;
	}

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOnlineTypeCode() {
		return onlineTypeCode;
	}

	public void setOnlineTypeCode(String onlineTypeCode) {
		this.onlineTypeCode = onlineTypeCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

 	/**  非表字段  begin */

	@NotDbField
	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

 	/**  非表字段  end */
	
}
