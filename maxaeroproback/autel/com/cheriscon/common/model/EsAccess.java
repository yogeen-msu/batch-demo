package com.cheriscon.common.model;


/**
 * AbstractEsAccess entity provides the base persistence definition of the EsAccess entity. @author MyEclipse Persistence Tools
 */

public abstract class EsAccess  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String ip;
     private String url;
     private String page;
     private String area;
     private Integer accessTime;
     private Integer stayTime;
     private Integer point;
     private String membername;


    // Constructors

    /** default constructor */
    public EsAccess() {
    }

    
    /** full constructor */
    public EsAccess(String ip, String url, String page, String area, Integer accessTime, Integer stayTime, Integer point, String membername) {
        this.ip = ip;
        this.url = url;
        this.page = page;
        this.area = area;
        this.accessTime = accessTime;
        this.stayTime = stayTime;
        this.point = point;
        this.membername = membername;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getIp() {
        return this.ip;
    }
    
    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getPage() {
        return this.page;
    }
    
    public void setPage(String page) {
        this.page = page;
    }

    public String getArea() {
        return this.area;
    }
    
    public void setArea(String area) {
        this.area = area;
    }

    public Integer getAccessTime() {
        return this.accessTime;
    }
    
    public void setAccessTime(Integer accessTime) {
        this.accessTime = accessTime;
    }

    public Integer getStayTime() {
        return this.stayTime;
    }
    
    public void setStayTime(Integer stayTime) {
        this.stayTime = stayTime;
    }

    public Integer getPoint() {
        return this.point;
    }
    
    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getMembername() {
        return this.membername;
    }
    
    public void setMembername(String membername) {
        this.membername = membername;
    }
   








}