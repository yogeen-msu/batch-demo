package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;



/**
 * 充值卡记录信息实体类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */

public  class ReChargeRecord  implements java.io.Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1588333270557212633L;
	
	
	
	private Integer id;						//id
	private String code;					//code
	private String proCode;					//产品code
	private String reChargeCardCode;		//充值卡code
	private String reChargeTime;			//充值时间
	private String memberCode;		         //用户autelId
	private String memberAutelId;		     //用户autelId
	private Integer memberType;				//用户类型(1:客户  2：经销商)
	private String saleCfgCode;             //标准销售code
	private String proSerialNo;				//产品序列号
	private String ip; //请求IP
	private String operateUser; //操作人
	

	public String getOperateUser() {
		return operateUser;
	}


	public void setOperateUser(String operateUser) {
		this.operateUser = operateUser;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	/**  非充值卡记录信息表字段  begin */
	
	private String proTypeName;				//产品型号名称
	private String proTypeCode;				//产品型号code
	private String memberName;				//用户名称
	private String reChargeCardSerialNo;	//充值卡序列号
	private String reChargeCardTypeName;	//充值卡类型名称
	private String saleCfgName;				//销售配置名称
	private String areaCfgName;				//区域配置名称
    private String startDate;   //统计开始时间
    private String endDate;    //统计结束时间
	private String sealerAutelId; //升级卡所属经销商
	
	/**  非充值卡记录信息表字段  end */


	
	
	
    // Constructors

    /** default constructor */
    public ReChargeRecord()
    {
    }

    
    /** full constructor */
    public ReChargeRecord(String code,String proCode, String reChargeCardCode, String reChargeTime, Integer memberType) {
        this.code = code;
    	this.proCode = proCode;
        this.reChargeCardCode = reChargeCardCode;
        this.reChargeTime = reChargeTime;
        this.memberType = memberType;
    }


    @PrimaryKeyField
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getProCode() {
		return proCode;
	}


	public void setProCode(String proCode) {
		this.proCode = proCode;
	}


	public String getReChargeCardCode() {
		return reChargeCardCode;
	}


	public void setReChargeCardCode(String reChargeCardCode) {
		this.reChargeCardCode = reChargeCardCode;
	}


	public String getReChargeTime() {
		return reChargeTime;
	}


	public void setReChargeTime(String reChargeTime) {
		this.reChargeTime = reChargeTime;
	}

	


	public String getMemberCode() {
		return memberCode;
	}


	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}


	public String getMemberAutelId() {
		return memberAutelId;
	}


	public void setMemberAutelId(String memberAutelId) {
		this.memberAutelId = memberAutelId;
	}


	public String getSaleCfgCode() {
		return saleCfgCode;
	}


	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}


	public Integer getMemberType() {
		return memberType;
	}


	public void setMemberType(Integer memberType) {
		this.memberType = memberType;
	}
	
	public String getProSerialNo() {
		return proSerialNo;
	}


	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}




	/**  非充值卡记录信息表字段  begin */
	
	@NotDbField
	public String getProTypeName() {
		return proTypeName;
	}


	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}


	@NotDbField
	public String getProTypeCode() {
		return proTypeCode;
	}


	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}





	@NotDbField
	public String getMemberName() {
		return memberName;
	}


	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}


	@NotDbField
	public String getReChargeCardSerialNo() {
		return reChargeCardSerialNo;
	}


	public void setReChargeCardSerialNo(String reChargeCardSerialNo) {
		this.reChargeCardSerialNo = reChargeCardSerialNo;
	}


	@NotDbField
	public String getReChargeCardTypeName() {
		return reChargeCardTypeName;
	}


	public void setReChargeCardTypeName(String reChargeCardTypeName) {
		this.reChargeCardTypeName = reChargeCardTypeName;
	}


	@NotDbField
	public String getSaleCfgName() {
		return saleCfgName;
	}

	

	public void setSaleCfgName(String saleCfgName) {
		this.saleCfgName = saleCfgName;
	}


	@NotDbField
	public String getAreaCfgName() {
		return areaCfgName;
	}


	public void setAreaCfgName(String areaCfgName) {
		this.areaCfgName = areaCfgName;
	}

	@NotDbField
	public String getStartDate() {
		return startDate;
	}

	@NotDbField
	public String getEndDate() {
		return endDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@NotDbField
	public String getSealerAutelId() {
		return sealerAutelId;
	}


	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}
	
	

	/**  非充值卡记录信息表字段  end */

}