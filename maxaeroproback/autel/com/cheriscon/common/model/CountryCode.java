package com.cheriscon.common.model;

/**
 * B2B商城自动登录，注册国家编码
 * @author 高亚雄
 * @date 2016/4/5
 *
 */
public class CountryCode implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String country;//国家名称
	private String code;//国家编码
	private String state;//州/省名称
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
}
