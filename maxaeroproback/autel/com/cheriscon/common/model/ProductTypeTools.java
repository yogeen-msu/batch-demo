package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;


public class ProductTypeTools implements java.io.Serializable {

	private static final long serialVersionUID = -1317043778170524366L;
	
	private String toolsCode; 				// 下载工具编号
	private String productTypeCode; 		// 产品型号编号
	
	private String toolName;               //临时字段  工具名称
	private String productTypeName;        //临时字段  产品名称
	private String lastVersion;            //临时字段 最新版本
	private String releaseDate;            //临时字段 发布日期
	private String downloadPath;           //临时字段  下载路径
	private String languageTypeName;       //临时字段语言类型名称
	

	public String getToolsCode() {
		return toolsCode;
	}

	public void setToolsCode(String toolsCode) {
		this.toolsCode = toolsCode;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	@NotDbField
	public String getToolName() {
		return toolName;
	}

	public void setToolName(String toolName) {
		this.toolName = toolName;
	}

	@NotDbField
	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	@NotDbField
	public String getLastVersion() {
		return lastVersion;
	}

	public void setLastVersion(String lastVersion) {
		this.lastVersion = lastVersion;
	}

	@NotDbField
	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	@NotDbField
	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getLanguageTypeName() {
		return languageTypeName;
	}

	public void setLanguageTypeName(String languageTypeName) {
		this.languageTypeName = languageTypeName;
	}


	
	
	

}