package com.cheriscon.common.model;



/**
 * AbstractEsSmtp entity provides the base persistence definition of the EsSmtp entity. @author MyEclipse Persistence Tools
 */

public abstract class EsSmtp  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String host;
     private String username;
     private String password;
     private Long lastSendTime;
     private Integer sendCount;
     private Integer maxCount;
     private String mailFrom;


    // Constructors

    /** default constructor */
    public EsSmtp() {
    }

    
    /** full constructor */
    public EsSmtp(String host, String username, String password, Long lastSendTime, Integer sendCount, Integer maxCount, String mailFrom) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.lastSendTime = lastSendTime;
        this.sendCount = sendCount;
        this.maxCount = maxCount;
        this.mailFrom = mailFrom;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getHost() {
        return this.host;
    }
    
    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public Long getLastSendTime() {
        return this.lastSendTime;
    }
    
    public void setLastSendTime(Long lastSendTime) {
        this.lastSendTime = lastSendTime;
    }

    public Integer getSendCount() {
        return this.sendCount;
    }
    
    public void setSendCount(Integer sendCount) {
        this.sendCount = sendCount;
    }

    public Integer getMaxCount() {
        return this.maxCount;
    }
    
    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    public String getMailFrom() {
        return this.mailFrom;
    }
    
    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }
   








}