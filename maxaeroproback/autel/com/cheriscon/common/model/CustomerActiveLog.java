package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;



/**
 * 手动激活客户autelID实体类
 * @author chenqichuan
 * @version 创建时间：2013-09-25
 */
public class CustomerActiveLog implements Serializable
{
		/**
	 * 
	 */
	private static final long serialVersionUID = -483164166075503829L;
	
		private Integer id;				//ID
		private String customerCode;   //用户code
		private String activeUser;     //激活人
		private String activeTime;     //激活时间

		public Integer getId() {
			return id;
		}
		public String getCustomerCode() {
			return customerCode;
		}
		public String getActiveUser() {
			return activeUser;
		}
		public String getActiveTime() {
			return activeTime;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public void setCustomerCode(String customerCode) {
			this.customerCode = customerCode;
		}
		public void setActiveUser(String activeUser) {
			this.activeUser = activeUser;
		}
		public void setActiveTime(String activeTime) {
			this.activeTime = activeTime;
		}
}