package com.cheriscon.common.model;



/**
 * AbstractEsTheme entity provides the base persistence definition of the EsTheme entity. @author MyEclipse Persistence Tools
 */

public abstract class EsTheme  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String appid;
     private String themename;
     private String path;
     private String author;
     private String version;
     private Integer deleteflag;
     private String thumb;
     private Integer siteid;


    // Constructors

    /** default constructor */
    public EsTheme() {
    }

    
    /** full constructor */
    public EsTheme(String appid, String themename, String path, String author, String version, Integer deleteflag, String thumb, Integer siteid) {
        this.appid = appid;
        this.themename = themename;
        this.path = path;
        this.author = author;
        this.version = version;
        this.deleteflag = deleteflag;
        this.thumb = thumb;
        this.siteid = siteid;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppid() {
        return this.appid;
    }
    
    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getThemename() {
        return this.themename;
    }
    
    public void setThemename(String themename) {
        this.themename = themename;
    }

    public String getPath() {
        return this.path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }

    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getVersion() {
        return this.version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getDeleteflag() {
        return this.deleteflag;
    }
    
    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getThumb() {
        return this.thumb;
    }
    
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public Integer getSiteid() {
        return this.siteid;
    }
    
    public void setSiteid(Integer siteid) {
        this.siteid = siteid;
    }
   








}