package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

public class ReChargeCardErrorLog implements java.io.Serializable {

	/**
	 * 充值卡充值使用错误日志类
	 */
	private static final long serialVersionUID = 3542829465383782076L;
	
	private Integer id;
	
	private String code;   //编码
	
	private String customerCode; //用户code
	
	private String createTime;   //生成时间
	
	private Integer status;    //是否处理 0 未处理 1 已处理
	
	private String remark;    //错误说明
	
	private String ip;  //请求IP
	
	private String updateUser;  //处理人
	
	private String updateTime;  //处理时间
	
	private String autelId;    //autelID
	
	private String type;   // 1,表示需要记录错误次数，0 只是单纯的记日志

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public String getCreateTime() {
		return createTime;
	}

	public Integer getStatus() {
		return status;
	}

	public String getRemark() {
		return remark;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public String getUpdateTime() {
		return updateTime;
	}
    @NotDbField 
	public String getAutelId() {
		return autelId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}


}
