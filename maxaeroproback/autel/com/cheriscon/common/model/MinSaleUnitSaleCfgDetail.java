package com.cheriscon.common.model;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 
* @ClassName: MinSaleUnitSaleCfgDetail
* @Description: 销售配置和最小销售单位关联表
* @author shaohu
* @date 2013-1-17 下午01:57:20
*
 */
public class MinSaleUnitSaleCfgDetail implements java.io.Serializable {

	private static final long serialVersionUID = 228325897724138763L;

	private Integer id;
	/**
	 * 最小销售单位编码
	 */
	private String minSaleUnitCode;
	/**
	 * 销售配置编码
	 */
	private String saleCfgCode;

	public MinSaleUnitSaleCfgDetail() {
	}

	public MinSaleUnitSaleCfgDetail(Integer id, String minSaleUnitCode,
			String saleCfgCode) {
		super();
		this.id = id;
		this.minSaleUnitCode = minSaleUnitCode;
		this.saleCfgCode = saleCfgCode;
	}

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	public String getSaleCfgCode() {
		return saleCfgCode;
	}

	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}

}