package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;



/**
 * 订单修改日志实体类
 * @author pengdongan
 * @version 创建时间：2013-4-27
 */
public class OrderInfoLog implements Serializable
{
		/**
	 * 
	 */
	private static final long serialVersionUID = -483164166075503829L;
	
		private Integer id;				//ID
		private String code;			//销售信息code
		private String orderCode;		//订单编码
		private String updateMoney;		//修改后的价格
		private String orderMoney;		//原订单价格
		private String updateReason;	//修改原因
		private String updateDate;		//修改时间
		private String updatePerson;	//修改人

	 	/**  非表字段  begin */
	 	
	 	private String autelId;			//客户ID
	 	private String userName;		//客户名称
	 	private String beginDate;		//开始时间
	 	private String endDate;			//结束时间
	 	
	 	/**  非表字段  end */


	    // Constructors

	    /** default constructor */
	    public OrderInfoLog() {
	    }


		@PrimaryKeyField
	    public Integer getId() {
	        return this.id;
	    }
	    
	    public void setId(Integer id) {
	        this.id = id;
	    }


		public String getCode() {
			return code;
		}


		public void setCode(String code) {
			this.code = code;
		}


		public String getOrderCode() {
			return orderCode;
		}


		public void setOrderCode(String orderCode) {
			this.orderCode = orderCode;
		}


		public String getUpdateMoney() {
			return updateMoney;
		}


		public void setUpdateMoney(String updateMoney) {
			this.updateMoney = updateMoney;
		}


		public String getOrderMoney() {
			return orderMoney;
		}


		public void setOrderMoney(String orderMoney) {
			this.orderMoney = orderMoney;
		}


		public String getUpdateReason() {
			return updateReason;
		}


		public void setUpdateReason(String updateReason) {
			this.updateReason = updateReason;
		}


		public String getUpdateDate() {
			return updateDate;
		}


		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}


		public String getUpdatePerson() {
			return updatePerson;
		}


		public void setUpdatePerson(String updatePerson) {
			this.updatePerson = updatePerson;
		}

		
		
	 	/**  非表字段  begin */

		@NotDbField
		public String getAutelId() {
			return autelId;
		}


		public void setAutelId(String autelId) {
			this.autelId = autelId;
		}


		@NotDbField
		public String getUserName() {
			return userName;
		}


		public void setUserName(String userName) {
			this.userName = userName;
		}


		@NotDbField
		public String getBeginDate() {
			return beginDate;
		}


		public void setBeginDate(String beginDate) {
			this.beginDate = beginDate;
		}


		@NotDbField
		public String getEndDate() {
			return endDate;
		}


		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
		
	 	/**  非表字段  end */

}