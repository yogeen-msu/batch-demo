package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 记录订单支付后更新产品有效期失败的数据
 * @author AUTEL
 *
 */
public class ProductSoftwareValidChangeErrorLog implements Serializable{
	
	private Integer id;
	private String proCode; // 产品序列号
	private String minSaleUnit; // 修改的最小销售单位
	private String createTime; //修改时间
	private String remark; //备注
	private String isUpdate; //是否更新 1 已更新 0 未更新
	private String operatorUser; // 操作人
	private String operatorTime; // 操作时间
	private String operatorIp; // 操作IP
	
	private String productSN;
	private String minSaleUnitName;

	public ProductSoftwareValidChangeErrorLog() {
		// TODO Auto-generated constructor stub
	}

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public String getMinSaleUnit() {
		return minSaleUnit;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getCreateTime() {
		return createTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public void setMinSaleUnit(String minSaleUnit) {
		this.minSaleUnit = minSaleUnit;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(String isUpdate) {
		this.isUpdate = isUpdate;
	}

	@NotDbField
	public String getProductSN() {
		return productSN;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	@NotDbField
	public String getMinSaleUnitName() {
		return minSaleUnitName;
	}

	public void setMinSaleUnitName(String minSaleUnitName) {
		this.minSaleUnitName = minSaleUnitName;
	}

	public String getOperatorUser() {
		return operatorUser;
	}

	public String getOperatorTime() {
		return operatorTime;
	}

	public String getOperatorIp() {
		return operatorIp;
	}

	public void setOperatorUser(String operatorUser) {
		this.operatorUser = operatorUser;
	}

	public void setOperatorTime(String operatorTime) {
		this.operatorTime = operatorTime;
	}

	public void setOperatorIp(String operatorIp) {
		this.operatorIp = operatorIp;
	}

	
}
