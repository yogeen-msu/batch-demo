package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 销售配置说明信息
 * 
 * @author caozhiyong
 * 
 */
public class SaleConfigMemo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String saleConfigcode;
	private String languageCode;
	private String memo;
	private String name;
	private String memoDesc;
	
	public String getMemoDesc() {
		return memoDesc;
	}

	public void setMemoDesc(String memoDesc) {
		this.memoDesc = memoDesc;
	}

	private String languageName;

	public SaleConfigMemo() {
	}

	public SaleConfigMemo(Integer id, String saleConfigcode,
			String languageCode, String memo, String name,String memoDesc) {
		super();
		this.id = id;
		this.saleConfigcode = saleConfigcode;
		this.languageCode = languageCode;
		this.memo = memo;
		this.name = name;
		this.memoDesc=memoDesc;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSaleConfigcode() {
		return saleConfigcode;
	}

	public void setSaleConfigcode(String saleConfigcode) {
		this.saleConfigcode = saleConfigcode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotDbField
	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

}