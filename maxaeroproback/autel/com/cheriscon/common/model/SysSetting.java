package com.cheriscon.common.model;



/**
 * AbstractSysSetting entity provides the base persistence definition of the SysSetting entity. @author MyEclipse Persistence Tools
 */

public abstract class SysSetting  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String code;
     private String setValue;


    // Constructors

    /** default constructor */
    public SysSetting() {
    }

    
    /** full constructor */
    public SysSetting(String code, String setValue) {
        this.code = code;
        this.setValue = setValue;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getSetValue() {
        return this.setValue;
    }
    
    public void setSetValue(String setValue) {
        this.setValue = setValue;
    }
   








}