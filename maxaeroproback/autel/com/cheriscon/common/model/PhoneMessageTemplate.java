package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 手机短信内容模板
 * 
 * @author yinhb
 * 
 */
public class PhoneMessageTemplate implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = -3996279731108294494L;
	
	private Integer id;
	private String languageCode;				//语言环境
	private String languageName;				//语言名称
	private String title;						//标题
	private Integer type;						//模版类型
	private String content;						//模版内容
	private Integer status;						//状态   0:未启用   1：启用
	private String useTime;						//启用时间


	@PrimaryKeyField
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLanguageCode() {
		return this.languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUseTime() {
		return useTime;
	}

	public void setUseTime(String useTime) {
		this.useTime = useTime;
	}

	@NotDbField
	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

}