package com.cheriscon.common.model;

/**
 * 
 * @ClassName: MarkerPromotionRule
 * @Description: 营销活动规则表
 * @author shaohu
 * @date 2013-1-19 下午12:02:29
 * 
 */
public class MarkerPromotionRule implements java.io.Serializable {

	private static final long serialVersionUID = 8425105638676599887L;
	private Integer id;
	/**
	 * 营销活动对象表
	 */
	private String marketPromotionCode;

	private String code;
	/**
	 * 条件类型：0 购买 1 续费
	 */
	private Integer conditionType;
	/**
	 * 优惠折扣
	 */
	private Long discount;

	public MarkerPromotionRule() {
		super();
	}

	public MarkerPromotionRule(Integer id, String marketPromotionCode,
			String code, Integer conditionType, Long discount) {
		super();
		this.id = id;
		this.marketPromotionCode = marketPromotionCode;
		this.code = code;
		this.conditionType = conditionType;
		this.discount = discount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarketPromotionCode() {
		return marketPromotionCode;
	}

	public void setMarketPromotionCode(String marketPromotionCode) {
		this.marketPromotionCode = marketPromotionCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getConditionType() {
		return conditionType;
	}

	public void setConditionType(Integer conditionType) {
		this.conditionType = conditionType;
	}

	public Long getDiscount() {
		return discount;
	}

	public void setDiscount(Long discount) {
		this.discount = discount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}