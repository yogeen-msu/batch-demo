package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 
 * @ClassName: TestPackLanguagePack
 * @Description: 测试包语言包
 * @author pengdongan
 * @date 2013-01-22
 * 
 */
public class TestPackLanguagePack implements java.io.Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8097857173639089854L;
	
	
	private Integer id;
	/**
	 * 测试包编码
	 */
	private String testPackCode;
	
	/**
	 * 语言编码
	 */
	private String languageTypeCode;
	
	private String code;
	
	/**
	 * 下载路径
	 */
	private String downloadPath;
	
	/**
	 * 可测车型文档路径
	 */
	private String testFilePath;
	
	/**
	 * 语言包说明
	 */
	private String memo;
	
	
	

 	

 	/**  非测试包语言包信息表字段  begin */

	private String languageTypeName;		//语言类型名称

 	/**  非测试包语言包信息表字段  end */
	

	public TestPackLanguagePack() {
	}

	public TestPackLanguagePack(Integer id, String testPackCode,
			String languageTypeCode, String code, String downloadPath,
			String testFilePath, String memo) {
		super();
		this.id = id;
		this.testPackCode = testPackCode;
		this.languageTypeCode = languageTypeCode;
		this.code = code;
		this.downloadPath = downloadPath;
		this.testFilePath = testFilePath;
		this.memo = memo;

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTestPackCode() {
		return testPackCode;
	}

	public void setTestPackCode(String testPackCode) {
		this.testPackCode = testPackCode;
	}

	public String getLanguageTypeCode() {
		return languageTypeCode;
	}

	public void setLanguageTypeCode(String languageTypeCode) {
		this.languageTypeCode = languageTypeCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getTestFilePath() {
		return testFilePath;
	}

	public void setTestFilePath(String testFilePath) {
		this.testFilePath = testFilePath;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

 	

 	/**  非测试包语言包信息表字段  begin */
	@NotDbField
	public String getLanguageTypeName() {
		return languageTypeName;
	}

	public void setLanguageTypeName(String languageTypeName) {
		this.languageTypeName = languageTypeName;
	}

 	/**  非测试包语言包信息表字段  end */
	
}