package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * @remark	客诉来源实体
 * @date	2013-03-31
 * @table	DT_CustomerComplaintSource
 * @author pengdongan
 *
 */
public class CustomerComplaintSource implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3683395711370949321L;
	
	//客诉来源id
	private Integer id;
	//客诉来源code
	private String code;
	//客诉来源名称
	private String name;
	
	public CustomerComplaintSource() {
		super();
	}

	public CustomerComplaintSource(Integer id, String code, String name) {
		this.id = id;
		this.code = code;
		this.name = name;
	}

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
