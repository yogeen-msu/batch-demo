package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

public class ProductResetLog implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6914689810784467084L;
	
	private int id;  //自动增长id
	
	private String code; //编码
	
	private String proCode; //产品code
	
	private String resetReason; //设置原因
	
	private String resetUser;  //设置人编码
	
	private String resetDate;  //设置时间
	
	private String serialNo;  //产品序列号
	
	private String regDate; //产品注册日期
	
	private String resetUserName;  //设置人ID
	
	private String oldUserCode; //关联原注册账户
	
	private String validDate;  //记录原产品序列号的升级有效期
	
	public String getResetUserName() {
		return resetUserName;
	}

	public void setResetUserName(String resetUserName) {
		this.resetUserName = resetUserName;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	@NotDbField
	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	@PrimaryKeyField
	public int getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getProCode() {
		return proCode;
	}

	public String getResetReason() {
		return resetReason;
	}

	public String getResetUser() {
		return resetUser;
	}

	public String getResetDate() {
		return resetDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public void setResetReason(String resetReason) {
		this.resetReason = resetReason;
	}

	public void setResetUser(String resetUser) {
		this.resetUser = resetUser;
	}

	public void setResetDate(String resetDate) {
		this.resetDate = resetDate;
	}

	public String getOldUserCode() {
		return oldUserCode;
	}

	public void setOldUserCode(String oldUserCode) {
		this.oldUserCode = oldUserCode;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

}
