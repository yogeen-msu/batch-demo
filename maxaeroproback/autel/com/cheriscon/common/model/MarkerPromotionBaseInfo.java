package com.cheriscon.common.model;

/**
 * 
 * @ClassName: MarkerPromotionBaseInfo
 * @Description: 营销活动基本信息表
 * @author shaohu
 * @date 2013-1-19 上午11:56:27
 * 
 */
public class MarkerPromotionBaseInfo implements java.io.Serializable {

	private static final long serialVersionUID = -675693786464535791L;
	private Integer id;
	private String code;
	/**
	 * 活动名称
	 */
	private String promotionName;
	/**
	 * 活动类型 1：限时 2：越早越便宜 3：套餐
	 */
	private Integer promotionType;

	/**
	 * 活动状态 (0:未执行  1：启用 2：终止)
	 */
	private Integer status = 0;
	
	/**
	 * 是否所有销售单位(1 所有,0非所有)
	 */
	private Integer allSaleUnit = 0;
	
	/**
	 * 是否所有区域(1所有,0非所有)
	 */
	private Integer allArea = 0;
	
	/**
	 * 创建人
	 */
	private String creator;
	private String createtime;

	public MarkerPromotionBaseInfo() {
		super();
	}

	public MarkerPromotionBaseInfo(Integer id, String code,
			String promotionName, Integer promotionType, Integer status,
			Integer allSaleUnit, Integer allArea, String creator,
			String createtime) {
		super();
		this.id = id;
		this.code = code;
		this.promotionName = promotionName;
		this.promotionType = promotionType;
		this.status = status;
		this.allSaleUnit = allSaleUnit;
		this.allArea = allArea;
		this.creator = creator;
		this.createtime = createtime;
	}

	public Integer getAllSaleUnit() {
		return allSaleUnit;
	}

	public void setAllSaleUnit(Integer allSaleUnit) {
		this.allSaleUnit = allSaleUnit;
	}

	public Integer getAllArea() {
		return allArea;
	}

	public void setAllArea(Integer allArea) {
		this.allArea = allArea;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

}