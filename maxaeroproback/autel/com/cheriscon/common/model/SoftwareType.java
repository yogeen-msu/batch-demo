package com.cheriscon.common.model;

public class SoftwareType implements java.io.Serializable {

	private static final long serialVersionUID = 597362072987213632L;
	private Integer id;
	private String code;
	//软件名称
	private String name;
	//产品型号code
	private String proTypeCode;
	
	private String proTypeName;
	
	private String softType;  //软件类型(系统程序/固化程序/免费诊断程序/付费诊断程序)
	
	private String newCarSignPath; //新车标路径，（英文）
	
	public String getNewCarSignPath() {
		return newCarSignPath;
	}

	public void setNewCarSignPath(String newCarSignPath) {
		this.newCarSignPath = newCarSignPath;
	}

	public String getSoftType() {
		return softType;
	}

	public void setSoftType(String softType) {
		this.softType = softType;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	//车标图片路径 默认中文
	private String carSignPath;

	public SoftwareType() {
		super();
	}

	public SoftwareType(Integer id, String proTypeCode, String code,
			String name, String carSignPath) {
		super();
		this.id = id;
		this.proTypeCode = proTypeCode;
		this.code = code;
		this.name = name;
		this.carSignPath = carSignPath;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCarSignPath() {
		return carSignPath;
	}

	public void setCarSignPath(String carSignPath) {
		this.carSignPath = carSignPath;
	}

}