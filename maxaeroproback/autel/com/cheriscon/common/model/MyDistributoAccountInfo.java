package com.cheriscon.common.model;

import java.io.Serializable;

/**
 * 我的账户(经销商)实体类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public class MyDistributoAccountInfo implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7554349339155411285L;
	private String productName;
	private String languageName;
	private String saleArea;
	private String lastLoginTime;
	
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSaleArea() {
		return saleArea;
	}
	public void setSaleArea(String saleArea) {
		this.saleArea = saleArea;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	
	
}
