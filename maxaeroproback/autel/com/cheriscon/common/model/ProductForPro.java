package com.cheriscon.common.model;

import java.io.Serializable;

public class ProductForPro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1411194028109997367L;
	
	private Integer id;
	private String code;   
	private String serialNo;  //序列号
	private int type;   //类型 1 蓝牙盒子 2 J23534盒子
	private String proCode;  //关联的产品序列号
	private int regStatus;  //绑定状态 0 为未绑定 1 为绑定
	private String regPwd;   //注册密码
	private String regTime;  //绑定时间
	private String proDate;  //序列号出厂日期
	private String reason;   //操作原因
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public int getRegStatus() {
		return regStatus;
	}
	public void setRegStatus(int regStatus) {
		this.regStatus = regStatus;
	}
	public String getRegPwd() {
		return regPwd;
	}
	public void setRegPwd(String regPwd) {
		this.regPwd = regPwd;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getProDate() {
		return proDate;
	}
	public void setProDate(String proDate) {
		this.proDate = proDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
}
