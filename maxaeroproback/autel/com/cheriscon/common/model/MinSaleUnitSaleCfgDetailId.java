package com.cheriscon.common.model;



/**
 * AbstractMinSaleUnitSaleCfgDetailId entity provides the base persistence definition of the MinSaleUnitSaleCfgDetailId entity. @author MyEclipse Persistence Tools
 */

public abstract class MinSaleUnitSaleCfgDetailId  implements java.io.Serializable {


    // Fields    

     private MinSaleUnit minSaleUnit;
     private SaleConfig saleConfig;


    // Constructors

    /** default constructor */
    public MinSaleUnitSaleCfgDetailId() {
    }

    
    /** full constructor */
    public MinSaleUnitSaleCfgDetailId(MinSaleUnit minSaleUnit, SaleConfig saleConfig) {
        this.minSaleUnit = minSaleUnit;
        this.saleConfig = saleConfig;
    }

   
    // Property accessors

    public MinSaleUnit getMinSaleUnit() {
        return this.minSaleUnit;
    }
    
    public void setMinSaleUnit(MinSaleUnit minSaleUnit) {
        this.minSaleUnit = minSaleUnit;
    }

    public SaleConfig getSaleConfig() {
        return this.saleConfig;
    }
    
    public void setSaleConfig(SaleConfig saleConfig) {
        this.saleConfig = saleConfig;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof MinSaleUnitSaleCfgDetailId) ) return false;
		 MinSaleUnitSaleCfgDetailId castOther = ( MinSaleUnitSaleCfgDetailId ) other; 
         
		 return ( (this.getMinSaleUnit()==castOther.getMinSaleUnit()) || ( this.getMinSaleUnit()!=null && castOther.getMinSaleUnit()!=null && this.getMinSaleUnit().equals(castOther.getMinSaleUnit()) ) )
 && ( (this.getSaleConfig()==castOther.getSaleConfig()) || ( this.getSaleConfig()!=null && castOther.getSaleConfig()!=null && this.getSaleConfig().equals(castOther.getSaleConfig()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getMinSaleUnit() == null ? 0 : this.getMinSaleUnit().hashCode() );
         result = 37 * result + ( getSaleConfig() == null ? 0 : this.getSaleConfig().hashCode() );
         return result;
   }   





}