package com.cheriscon.common.model;



/**
 * AbstractEsGuestbook entity provides the base persistence definition of the EsGuestbook entity. @author MyEclipse Persistence Tools
 */

public abstract class EsGuestbook  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String title;
     private String content;
     private Integer parentid;
     private Long dateline;
     private Short issubject;
     private String username;
     private String email;
     private String qq;
     private String tel;
     private Short sex;
     private String ip;
     private String area;


    // Constructors

    /** default constructor */
    public EsGuestbook() {
    }

    
    /** full constructor */
    public EsGuestbook(String title, String content, Integer parentid, Long dateline, Short issubject, String username, String email, String qq, String tel, Short sex, String ip, String area) {
        this.title = title;
        this.content = content;
        this.parentid = parentid;
        this.dateline = dateline;
        this.issubject = issubject;
        this.username = username;
        this.email = email;
        this.qq = qq;
        this.tel = tel;
        this.sex = sex;
        this.ip = ip;
        this.area = area;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }

    public Integer getParentid() {
        return this.parentid;
    }
    
    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public Long getDateline() {
        return this.dateline;
    }
    
    public void setDateline(Long dateline) {
        this.dateline = dateline;
    }

    public Short getIssubject() {
        return this.issubject;
    }
    
    public void setIssubject(Short issubject) {
        this.issubject = issubject;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return this.qq;
    }
    
    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getTel() {
        return this.tel;
    }
    
    public void setTel(String tel) {
        this.tel = tel;
    }

    public Short getSex() {
        return this.sex;
    }
    
    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getIp() {
        return this.ip;
    }
    
    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getArea() {
        return this.area;
    }
    
    public void setArea(String area) {
        this.area = area;
    }
   








}