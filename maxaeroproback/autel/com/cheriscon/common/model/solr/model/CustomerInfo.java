package com.cheriscon.common.model.solr.model;

import org.apache.solr.client.solrj.beans.Field;

public class CustomerInfo {
	@Field
	private String Id;
	@Field
	private String serialNo;
	@Field
	private String productName;
	@Field
	private String country;
	public String getId() {
		return Id;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public String getProductName() {
		return productName;
	}
	public String getCountry() {
		return country;
	}
	public void setId(String id) {
		Id = id;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
