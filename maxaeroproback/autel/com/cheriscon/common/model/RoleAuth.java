package com.cheriscon.common.model;



/**
 * AbstractRoleAuth entity provides the base persistence definition of the RoleAuth entity. @author MyEclipse Persistence Tools
 */

public abstract class RoleAuth  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Authority authority;
     private Role role;
     private Integer authid;


    // Constructors

    /** default constructor */
    public RoleAuth() {
    }

    
    /** full constructor */
    public RoleAuth(Authority authority, Role role, Integer authid) {
        this.authority = authority;
        this.role = role;
        this.authid = authid;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Authority getAuthority() {
        return this.authority;
    }
    
    public void setAuthority(Authority authority) {
        this.authority = authority;
    }

    public Role getRole() {
        return this.role;
    }
    
    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getAuthid() {
        return this.authid;
    }
    
    public void setAuthid(Integer authid) {
        this.authid = authid;
    }
   








}