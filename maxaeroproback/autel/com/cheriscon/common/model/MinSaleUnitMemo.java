package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 最小销售单位说明信息
 * 
 * @author caozhiyong
 * 
 */
public class MinSaleUnitMemo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String minSaleUnitcode;
	private String languageCode;
	private String memo;
	private String memoDetail;
	private String name;
	private String updateName;
	
	private String languageName;

	public MinSaleUnitMemo() {
	}

	public MinSaleUnitMemo(Integer id, String minSaleUnitcode,
			String languageCode, String memo, String memoDetail, String name) {
		super();
		this.id = id;
		this.minSaleUnitcode = minSaleUnitcode;
		this.languageCode = languageCode;
		this.memo = memo;
		this.memoDetail = memoDetail;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMinSaleUnitcode() {
		return minSaleUnitcode;
	}

	public void setMinSaleUnitcode(String minSaleUnitcode) {
		this.minSaleUnitcode = minSaleUnitcode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMemoDetail() {
		return memoDetail;
	}

	public void setMemoDetail(String memoDetail) {
		this.memoDetail = memoDetail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotDbField
	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getUpdateName() {
		return updateName;
	}

	public void setUpdateName(String updateName) {
		this.updateName = updateName;
	}

}