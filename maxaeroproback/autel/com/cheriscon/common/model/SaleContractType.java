package com.cheriscon.common.model;

/**
 * 
 * @ClassName: SaleContractType
 * @Description: 销售契约目录实体类
 * @author shaohu
 * @date 2013-1-17 上午09:07:38
 * 
 */
public class SaleContractType implements java.io.Serializable {

	private static final long serialVersionUID = 6265021248609349145L;

	private Integer id;
	/**
	 * 编码
	 */
	private String code;
	/**
	 * 名称
	 */
	private String name;

	public SaleContractType() {
		super();
	}

	public SaleContractType(Integer id, String code, String name) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}