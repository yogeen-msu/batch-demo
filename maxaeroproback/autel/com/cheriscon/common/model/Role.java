package com.cheriscon.common.model;

import java.util.HashSet;
import java.util.Set;


/**
 * AbstractRole entity provides the base persistence definition of the Role entity. @author MyEclipse Persistence Tools
 */

public abstract class Role  implements java.io.Serializable {


    // Fields    

     private Integer roleid;
     private String rolename;
     private String rolememo;
     private Set userRoles = new HashSet(0);
     private Set roleAuths = new HashSet(0);


    // Constructors

    /** default constructor */
    public Role() {
    }

    
    /** full constructor */
    public Role(String rolename, String rolememo, Set userRoles, Set roleAuths) {
        this.rolename = rolename;
        this.rolememo = rolememo;
        this.userRoles = userRoles;
        this.roleAuths = roleAuths;
    }

   
    // Property accessors

    public Integer getRoleid() {
        return this.roleid;
    }
    
    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return this.rolename;
    }
    
    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getRolememo() {
        return this.rolememo;
    }
    
    public void setRolememo(String rolememo) {
        this.rolememo = rolememo;
    }

    public Set getUserRoles() {
        return this.userRoles;
    }
    
    public void setUserRoles(Set userRoles) {
        this.userRoles = userRoles;
    }

    public Set getRoleAuths() {
        return this.roleAuths;
    }
    
    public void setRoleAuths(Set roleAuths) {
        this.roleAuths = roleAuths;
    }
   








}