package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

public class CustomerChangeAutel {

	private Integer id;		
	
	private String code;	
	
	private String customerCode; //老用户code
	
	private String newAutelId; //新AutelID
	
	private int actState;   //更改autelID激活状态
	
	private int oldActState; //老autelID激活状态
	
	private String oldAutelId; //老autelID

	public int getActState() {
		return actState;
	}

	public void setActState(int actState) {
		this.actState = actState;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public String getNewAutelId() {
		return newAutelId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public void setNewAutelId(String newAutelId) {
		this.newAutelId = newAutelId;
	}
	
    @NotDbField
	public int getOldActState() {
		return oldActState;
	}

	public void setOldActState(int oldActState) {
		this.oldActState = oldActState;
	}
	
	@NotDbField
	public String getOldAutelId() {
		return oldAutelId;
	}

	public void setOldAutelId(String oldAutelId) {
		this.oldAutelId = oldAutelId;
	}

}
