package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * @remark	在线客服类型实体
 * @date	2013-06-02
 * @table	DT_OnlineServiceType
 * @author pengdongan
 *
 */
public class OnlineServiceType implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -645626366597181321L;
	//在线客服类型id
	private Integer id;
	//在线客服类型code
	private String code;


 	/**  非表字段  begin */
	
 	private String name;					//在线客服类型名称	
	private String adminLanguageCode;		//管理员首选语言code

 	/**  非表字段 end */
	
	public OnlineServiceType() {
		super();
	}

	public OnlineServiceType(Integer id, String code) {
		this.id = id;
		this.code = code;
	}

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	

 	/**  非表字段  begin */

	@NotDbField
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotDbField
	public String getAdminLanguageCode() {
		return adminLanguageCode;
	}

	public void setAdminLanguageCode(String adminLanguageCode) {
		this.adminLanguageCode = adminLanguageCode;
	}

 	/**  非表字段  end */
	
}
