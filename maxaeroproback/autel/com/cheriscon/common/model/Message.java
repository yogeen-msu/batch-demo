package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 站内消息
 * 
 * @author yinhb
 */
public class Message implements java.io.Serializable {

	private static final long serialVersionUID = 4257955131110023871L;

	private Integer id;
	private String code;			
	private String title;					//标题
	private String languageCode;			//语言Code
	private String languageName;			//语言名称
	private String msgTypeCode;				//消息类型Code
	private String msgTypeName;				//消息类型名称
	private Integer sort;					//排序
	private String creator;					//发布人
	private String creatTime;				//发布时间
	private Integer status;					//状态			0: 未发布   1:已发布
	private Integer isDialog;				//是否弹出		0: 未弹出    1:弹出
	private String content;
	private Integer userType;               //用户类型     1：个人用户 2：经销商
	private Integer timeRange;                  //时间范围 1： 最近三个月消息 2：三个月前消息
	private Integer messageIsRead;         //临时字段 用户消息是否已读
	private String softwareTypeCode; //软件类型Code
	private String  versionCode; //软件版本Code
	private String productTypeName; //产品名称
	private String minProductName; //小产品名称 类似TPMS
	
	@NotDbField
	public String getProductTypeName() {
		return productTypeName;
	}
	@NotDbField
	public String getMinProductName() {
		return minProductName;
	}
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}
	public void setMinProductName(String minProductName) {
		this.minProductName = minProductName;
	}
	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}
	public String getVersionCode() {
		return versionCode;
	}
	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
	public String getMsgTypeCode() {
		return msgTypeCode;
	}
	public void setMsgTypeCode(String msgTypeCode) {
		this.msgTypeCode = msgTypeCode;
	}
	
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIsDialog() {
		return isDialog;
	}
	public void setIsDialog(Integer isDialog) {
		this.isDialog = isDialog;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@NotDbField
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	
	@NotDbField
	public String getMsgTypeName() {
		return msgTypeName;
	}
	public void setMsgTypeName(String msgTypeName) {
		this.msgTypeName = msgTypeName;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public Integer getTimeRange() {
		return timeRange;
	}
	public void setTimeRange(Integer timeRange) {
		this.timeRange = timeRange;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getMessageIsRead() {
		return messageIsRead;
	}
	public void setMessageIsRead(Integer messageIsRead) {
		this.messageIsRead = messageIsRead;
	}
	
	
	
	
	
}