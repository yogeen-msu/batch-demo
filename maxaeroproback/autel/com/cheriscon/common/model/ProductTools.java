package com.cheriscon.common.model;


public class ProductTools implements java.io.Serializable 
{

	private static final long serialVersionUID = -1317043778170524366L;
	
	private String toolsCode; 				// 下载工具编号
	private String productInfoCode; 		// 产品信息编号
	
	private String toolName;               //临时字段  工具名称
	private String productTypeName;        //临时字段  产品名称
	private String lastVersion;            //临时字段 最新版本
	private String releaseDate;            //临时字段 发布日期
	private String downloadPath;           //临时字段  下载路径

	public String getToolsCode() {
		return toolsCode;
	}

	public void setToolsCode(String toolsCode) {
		this.toolsCode = toolsCode;
	}

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public String getToolName() {
		return toolName;
	}

	public void setToolName(String toolName) {
		this.toolName = toolName;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public String getLastVersion() {
		return lastVersion;
	}

	public void setLastVersion(String lastVersion) {
		this.lastVersion = lastVersion;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}
	
	

}