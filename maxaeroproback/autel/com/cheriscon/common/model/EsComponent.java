package com.cheriscon.common.model;



/**
 * AbstractEsComponent entity provides the base persistence definition of the EsComponent entity. @author MyEclipse Persistence Tools
 */

public abstract class EsComponent  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String componentid;
     private String name;
     private Short installState;
     private Short enableState;
     private String version;
     private String author;
     private String javashopVersion;
     private String description;
     private String errorMessage;


    // Constructors

    /** default constructor */
    public EsComponent() {
    }

    
    /** full constructor */
    public EsComponent(String componentid, String name, Short installState, Short enableState, String version, String author, String javashopVersion, String description, String errorMessage) {
        this.componentid = componentid;
        this.name = name;
        this.installState = installState;
        this.enableState = enableState;
        this.version = version;
        this.author = author;
        this.javashopVersion = javashopVersion;
        this.description = description;
        this.errorMessage = errorMessage;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getComponentid() {
        return this.componentid;
    }
    
    public void setComponentid(String componentid) {
        this.componentid = componentid;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public Short getInstallState() {
        return this.installState;
    }
    
    public void setInstallState(Short installState) {
        this.installState = installState;
    }

    public Short getEnableState() {
        return this.enableState;
    }
    
    public void setEnableState(Short enableState) {
        this.enableState = enableState;
    }

    public String getVersion() {
        return this.version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getJavashopVersion() {
        return this.javashopVersion;
    }
    
    public void setJavashopVersion(String javashopVersion) {
        this.javashopVersion = javashopVersion;
    }

    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
    
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
   








}