package com.cheriscon.common.model;

/**
 * 
 * @ClassName: LanguagePack
 * @Description: 软件语言包
 * @author shaohu
 * @date 2013-1-10 下午07:40:31
 * 
 */
public class LanguagePack implements java.io.Serializable {

	private static final long serialVersionUID = -3946905938091481871L;
	private Integer id;
	/**
	 * 软件版本编码
	 */
	private String softwareVersionCode;
	/**
	 * 语言编码
	 */
	private String languageTypeCode;
	private String code;
	
	/**
	 * 升级公告标题
	 */
	private String title;
	
	/**
	 * 下载路径
	 */
	private String downloadPath;
	/**
	 * 可测车型文档路径
	 */
	private String testPath;
	/**
	 * 语言包说明
	 */
	private String memo;
	
	/**
	 * 语言包编码
	 */
	private String languagePackCode;
	
	/**
	 * 语言名称
	 */
	private String languageName;

	public LanguagePack() {
	}

	public LanguagePack(Integer id, String softwareVersionCode,
			String languageTypeCode, String code, String downloadPath,
			String testPath, String memo,String title) {
		super();
		this.id = id;
		this.softwareVersionCode = softwareVersionCode;
		this.languageTypeCode = languageTypeCode;
		this.code = code;
		this.downloadPath = downloadPath;
		this.testPath = testPath;
		this.memo = memo;
		this.title =title;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSoftwareVersionCode() {
		return softwareVersionCode;
	}

	public void setSoftwareVersionCode(String softwareVersionCode) {
		this.softwareVersionCode = softwareVersionCode;
	}

	public String getLanguageTypeCode() {
		return languageTypeCode;
	}

	public void setLanguageTypeCode(String languageTypeCode) {
		this.languageTypeCode = languageTypeCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getTestPath() {
		return testPath;
	}

	public void setTestPath(String testPath) {
		this.testPath = testPath;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLanguagePackCode() {
		return languagePackCode;
	}

	public void setLanguagePackCode(String languagePackCode) {
		this.languagePackCode = languagePackCode;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

}