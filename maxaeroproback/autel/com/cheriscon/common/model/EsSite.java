package com.cheriscon.common.model;



/**
 * AbstractEsSite entity provides the base persistence definition of the EsSite entity. @author MyEclipse Persistence Tools
 */

public abstract class EsSite  implements java.io.Serializable {


    // Fields    

     private Integer siteid;
     private Integer parentid;
     private Integer code;
     private String name;
     private String domain;
     private Integer themeid;
     private Integer sitelevel;


    // Constructors

    /** default constructor */
    public EsSite() {
    }

    
    /** full constructor */
    public EsSite(Integer parentid, Integer code, String name, String domain, Integer themeid, Integer sitelevel) {
        this.parentid = parentid;
        this.code = code;
        this.name = name;
        this.domain = domain;
        this.themeid = themeid;
        this.sitelevel = sitelevel;
    }

   
    // Property accessors

    public Integer getSiteid() {
        return this.siteid;
    }
    
    public void setSiteid(Integer siteid) {
        this.siteid = siteid;
    }

    public Integer getParentid() {
        return this.parentid;
    }
    
    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public Integer getCode() {
        return this.code;
    }
    
    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return this.domain;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Integer getThemeid() {
        return this.themeid;
    }
    
    public void setThemeid(Integer themeid) {
        this.themeid = themeid;
    }

    public Integer getSitelevel() {
        return this.sitelevel;
    }
    
    public void setSitelevel(Integer sitelevel) {
        this.sitelevel = sitelevel;
    }
   








}