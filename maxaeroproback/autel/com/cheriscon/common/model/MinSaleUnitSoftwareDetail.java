package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 
 * @ClassName: MinSaleUnitSoftwareDetail
 * @Description: 最小销售单位功能软件关联表
 * @author shaohu
 * @date 2013-1-16 下午02:43:02
 * 
 */
public class MinSaleUnitSoftwareDetail implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;

	/**
	 * 功能软件编码
	 */
	private String softwareTypeCode;
	/**
	 * 最小销售单位编码
	 */
	private String minSaleUnitCode;

	/**
	 * 功能软件名称
	 */
	private String softwareTypeName;
	
	/**
	 * 是否发布
	 */
	private String releaseFlag ;

	public String getReleaseFlag() {
		return releaseFlag;
	}

	public void setReleaseFlag(String releaseFlag) {
		this.releaseFlag = releaseFlag;
	}

	public MinSaleUnitSoftwareDetail() {
	}

	public MinSaleUnitSoftwareDetail(Integer id, String softwareTypeCode,
			String minSaleUnitCode, String softwareTypeName,String releaseFlag) {
		super();
		this.id = id;
		this.softwareTypeCode = softwareTypeCode;
		this.minSaleUnitCode = minSaleUnitCode;
		this.softwareTypeName = softwareTypeName;
		this.releaseFlag=releaseFlag;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSoftwareTypeCode() {
		return softwareTypeCode;
	}

	public void setSoftwareTypeCode(String softwareTypeCode) {
		this.softwareTypeCode = softwareTypeCode;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	@NotDbField
	public String getSoftwareTypeName() {
		return softwareTypeName;
	}

	public void setSoftwareTypeName(String softwareTypeName) {
		this.softwareTypeName = softwareTypeName;
	}

}