package com.cheriscon.common.model;



/**
 * AbstractPaymentLog entity provides the base persistence definition of the PaymentLog entity. @author MyEclipse Persistence Tools
 */

public abstract class PaymentLog  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String code;
     private String orderCode;
     private Integer orderType;
     private String pruductType;
     private String softType;
     private String softLanguage;
     private String softArea;
     private String saleCode;
     private String payTime;
     private Double paymen;
     private Integer menmberCode;
     private Integer menberType;


    // Constructors

    /** default constructor */
    public PaymentLog() {
    }

    
    /** full constructor */
    public PaymentLog(String code, String orderCode, Integer orderType, String pruductType, String softType, String softLanguage, String softArea, String saleCode, String payTime, Double paymen, Integer menmberCode, Integer menberType) {
        this.code = code;
        this.orderCode = orderCode;
        this.orderType = orderType;
        this.pruductType = pruductType;
        this.softType = softType;
        this.softLanguage = softLanguage;
        this.softArea = softArea;
        this.saleCode = saleCode;
        this.payTime = payTime;
        this.paymen = paymen;
        this.menmberCode = menmberCode;
        this.menberType = menberType;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getOrderCode() {
        return this.orderCode;
    }
    
    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Integer getOrderType() {
        return this.orderType;
    }
    
    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getPruductType() {
        return this.pruductType;
    }
    
    public void setPruductType(String pruductType) {
        this.pruductType = pruductType;
    }

    public String getSoftType() {
        return this.softType;
    }
    
    public void setSoftType(String softType) {
        this.softType = softType;
    }

    public String getSoftLanguage() {
        return this.softLanguage;
    }
    
    public void setSoftLanguage(String softLanguage) {
        this.softLanguage = softLanguage;
    }

    public String getSoftArea() {
        return this.softArea;
    }
    
    public void setSoftArea(String softArea) {
        this.softArea = softArea;
    }

    public String getSaleCode() {
        return this.saleCode;
    }
    
    public void setSaleCode(String saleCode) {
        this.saleCode = saleCode;
    }

    public String getPayTime() {
        return this.payTime;
    }
    
    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public Double getPaymen() {
        return this.paymen;
    }
    
    public void setPaymen(Double paymen) {
        this.paymen = paymen;
    }

    public Integer getMenmberCode() {
        return this.menmberCode;
    }
    
    public void setMenmberCode(Integer menmberCode) {
        this.menmberCode = menmberCode;
    }

    public Integer getMenberType() {
        return this.menberType;
    }
    
    public void setMenberType(Integer menberType) {
        this.menberType = menberType;
    }
   








}