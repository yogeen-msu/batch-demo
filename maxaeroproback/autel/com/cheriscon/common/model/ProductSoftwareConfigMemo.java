package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

public class ProductSoftwareConfigMemo implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5161827852646905125L;
	private int id;
	private String code;
	private String languageCode;
	private String name;
	
	private String languageName;
	private String softConfigCode;

	public String getSoftConfigCode() {
		return softConfigCode;
	}
	public void setSoftConfigCode(String softConfigCode) {
		this.softConfigCode = softConfigCode;
	}

	public int getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public String getName() {
		return name;
	}
    @NotDbField
	public String getLanguageName() {
		return languageName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	

}
