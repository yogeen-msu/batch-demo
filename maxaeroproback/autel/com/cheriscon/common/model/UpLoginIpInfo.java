package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 产品升级IP类
 * 
 * @author chenqichuan
 */
public class UpLoginIpInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6440327295427194627L;
	
	private Integer ipInforId;
	private String countryId;
	private String productSn;
	private String ipStr;
	private String loginStampStr;
	private String ip;
	private String loginStamp;
	private String saleContractCode; 
	private String saleContract; 
	
	@NotDbField
	public String getSaleContractCode() {
		return saleContractCode;
	}
	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}
	
	@NotDbField
	public String getSaleContract() {
		return saleContract;
	}
	public void setSaleContract(String saleContract) {
		this.saleContract = saleContract;
	}
	public String getIp() {
		return ip;
	}
	public String getLoginStamp() {
		return loginStamp;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public void setLoginStamp(String loginStamp) {
		this.loginStamp = loginStamp;
	}
	public Integer getIpInforId() {
		return ipInforId;
	}
	public String getCountryId() {
		return countryId;
	}
	public String getProductSn() {
		return productSn;
	}
	public String getIpStr() {
		return ipStr;
	}
	public String getLoginStampStr() {
		return loginStampStr;
	}
	public void setIpInforId(Integer ipInforId) {
		this.ipInforId = ipInforId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public void setProductSn(String productSn) {
		this.productSn = productSn;
	}
	public void setIpStr(String ipStr) {
		this.ipStr = ipStr;
	}
	public void setLoginStampStr(String loginStampStr) {
		this.loginStampStr = loginStampStr;
	}
	

	
}