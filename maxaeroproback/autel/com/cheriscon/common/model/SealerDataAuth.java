package com.cheriscon.common.model;

import java.io.Serializable;
import java.util.List;

import com.cheriscon.framework.database.NotDbField;

public class SealerDataAuth implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4829755915018890614L;

	private String code;
	private String parentcode;
	private String name;

	private List<SealerDataAuth> children;

	private boolean hasChildren;

	private Integer level;

	@NotDbField
	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	@NotDbField
	public boolean getHasChildren() {
		hasChildren = this.children == null || this.children.isEmpty() ? false
				: true;
		return hasChildren;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getParentcode() {
		return parentcode;
	}

	public void setParentcode(String parentcode) {
		this.parentcode = parentcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotDbField
	public List<SealerDataAuth> getChildren() {
		return children;
	}

	public void setChildren(List<SealerDataAuth> children) {
		this.children = children;
	}

}
