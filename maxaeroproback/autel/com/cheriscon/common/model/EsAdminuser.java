package com.cheriscon.common.model;



/**
 * AbstractEsAdminuser entity provides the base persistence definition of the EsAdminuser entity. @author MyEclipse Persistence Tools
 */

public abstract class EsAdminuser  implements java.io.Serializable {


    // Fields    

     private Integer userid;
     private String username;
     private String password;
     private Integer state;
     private String realname;
     private String userno;
     private String userdept;
     private String remark;
     private Integer dateline;
     private Short founder;
     private Integer siteid;


    // Constructors

    /** default constructor */
    public EsAdminuser() {
    }

    
    /** full constructor */
    public EsAdminuser(String username, String password, Integer state, String realname, String userno, String userdept, String remark, Integer dateline, Short founder, Integer siteid) {
        this.username = username;
        this.password = password;
        this.state = state;
        this.realname = realname;
        this.userno = userno;
        this.userdept = userdept;
        this.remark = remark;
        this.dateline = dateline;
        this.founder = founder;
        this.siteid = siteid;
    }

   
    // Property accessors

    public Integer getUserid() {
        return this.userid;
    }
    
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getState() {
        return this.state;
    }
    
    public void setState(Integer state) {
        this.state = state;
    }

    public String getRealname() {
        return this.realname;
    }
    
    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getUserno() {
        return this.userno;
    }
    
    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getUserdept() {
        return this.userdept;
    }
    
    public void setUserdept(String userdept) {
        this.userdept = userdept;
    }

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDateline() {
        return this.dateline;
    }
    
    public void setDateline(Integer dateline) {
        this.dateline = dateline;
    }

    public Short getFounder() {
        return this.founder;
    }
    
    public void setFounder(Short founder) {
        this.founder = founder;
    }

    public Integer getSiteid() {
        return this.siteid;
    }
    
    public void setSiteid(Integer siteid) {
        this.siteid = siteid;
    }
   








}