package com.cheriscon.common.model;



/**
 * AbstractEsSiteMenu entity provides the base persistence definition of the EsSiteMenu entity. @author MyEclipse Persistence Tools
 */

public abstract class EsSiteMenu  implements java.io.Serializable {


    // Fields    

     private Integer menuid;
     private Integer parentid;
     private String name;
     private String url;
     private String target;
     private Integer sort;


    // Constructors

    /** default constructor */
    public EsSiteMenu() {
    }

    
    /** full constructor */
    public EsSiteMenu(Integer parentid, String name, String url, String target, Integer sort) {
        this.parentid = parentid;
        this.name = name;
        this.url = url;
        this.target = target;
        this.sort = sort;
    }

   
    // Property accessors

    public Integer getMenuid() {
        return this.menuid;
    }
    
    public void setMenuid(Integer menuid) {
        this.menuid = menuid;
    }

    public Integer getParentid() {
        return this.parentid;
    }
    
    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getTarget() {
        return this.target;
    }
    
    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getSort() {
        return this.sort;
    }
    
    public void setSort(Integer sort) {
        this.sort = sort;
    }
   








}