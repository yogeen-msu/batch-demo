package com.cheriscon.common.model;



/**
 * AbstractCopUserdetail entity provides the base persistence definition of the CopUserdetail entity. @author MyEclipse Persistence Tools
 */

public abstract class CopUserdetail  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer userid;
     private String bussinessscope;
     private String regaddress;
     private Long regdate;
     private Integer corpscope;
     private String corpdescript;


    // Constructors

    /** default constructor */
    public CopUserdetail() {
    }

    
    /** full constructor */
    public CopUserdetail(Integer userid, String bussinessscope, String regaddress, Long regdate, Integer corpscope, String corpdescript) {
        this.userid = userid;
        this.bussinessscope = bussinessscope;
        this.regaddress = regaddress;
        this.regdate = regdate;
        this.corpscope = corpscope;
        this.corpdescript = corpdescript;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return this.userid;
    }
    
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getBussinessscope() {
        return this.bussinessscope;
    }
    
    public void setBussinessscope(String bussinessscope) {
        this.bussinessscope = bussinessscope;
    }

    public String getRegaddress() {
        return this.regaddress;
    }
    
    public void setRegaddress(String regaddress) {
        this.regaddress = regaddress;
    }

    public Long getRegdate() {
        return this.regdate;
    }
    
    public void setRegdate(Long regdate) {
        this.regdate = regdate;
    }

    public Integer getCorpscope() {
        return this.corpscope;
    }
    
    public void setCorpscope(Integer corpscope) {
        this.corpscope = corpscope;
    }

    public String getCorpdescript() {
        return this.corpdescript;
    }
    
    public void setCorpdescript(String corpdescript) {
        this.corpdescript = corpdescript;
    }
   








}