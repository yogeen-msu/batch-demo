package com.cheriscon.common.model;

import java.io.Serializable;

public class BbsUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6648288021629162170L;

	private String code;
	private String username;
	private String email;
	private String register_time;
	private String register_ip;
	private String last_login_ip;
	private int upload_total;
	private int group_id;
	private int login_count;
	private int upload_size;
	private int is_admin;
	private int is_disabled;
	private int prohibit_post;
	private int grade_today;
	private int upload_today;
	private int point;
	private int avatar_type;
	private int topic_count;
	private int reply_count;
	private int prime_count;
	private int post_today;
	private int prestige;
	private int magic_packet_size;
	private int gender;
	private int isuse;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegister_time() {
		return register_time;
	}

	public void setRegister_time(String register_time) {
		this.register_time = register_time;
	}

	public String getRegister_ip() {
		return register_ip;
	}

	public void setRegister_ip(String register_ip) {
		this.register_ip = register_ip;
	}

	public String getLast_login_ip() {
		return last_login_ip;
	}

	public void setLast_login_ip(String last_login_ip) {
		this.last_login_ip = last_login_ip;
	}

	public int getUpload_total() {
		return upload_total;
	}

	public void setUpload_total(int upload_total) {
		this.upload_total = upload_total;
	}

	public int getGroup_id() {
		return group_id;
	}

	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

	public int getLogin_count() {
		return login_count;
	}

	public void setLogin_count(int login_count) {
		this.login_count = login_count;
	}

	public int getUpload_size() {
		return upload_size;
	}

	public void setUpload_size(int upload_size) {
		this.upload_size = upload_size;
	}

	public int getIs_admin() {
		return is_admin;
	}

	public void setIs_admin(int is_admin) {
		this.is_admin = is_admin;
	}

	public int getIs_disabled() {
		return is_disabled;
	}

	public void setIs_disabled(int is_disabled) {
		this.is_disabled = is_disabled;
	}

	public int getProhibit_post() {
		return prohibit_post;
	}

	public void setProhibit_post(int prohibit_post) {
		this.prohibit_post = prohibit_post;
	}

	public int getGrade_today() {
		return grade_today;
	}

	public void setGrade_today(int grade_today) {
		this.grade_today = grade_today;
	}

	public int getUpload_today() {
		return upload_today;
	}

	public void setUpload_today(int upload_today) {
		this.upload_today = upload_today;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getAvatar_type() {
		return avatar_type;
	}

	public void setAvatar_type(int avatar_type) {
		this.avatar_type = avatar_type;
	}

	public int getTopic_count() {
		return topic_count;
	}

	public void setTopic_count(int topic_count) {
		this.topic_count = topic_count;
	}

	public int getReply_count() {
		return reply_count;
	}

	public void setReply_count(int reply_count) {
		this.reply_count = reply_count;
	}

	public int getPrime_count() {
		return prime_count;
	}

	public void setPrime_count(int prime_count) {
		this.prime_count = prime_count;
	}

	public int getPost_today() {
		return post_today;
	}

	public void setPost_today(int post_today) {
		this.post_today = post_today;
	}

	public int getPrestige() {
		return prestige;
	}

	public void setPrestige(int prestige) {
		this.prestige = prestige;
	}

	public int getMagic_packet_size() {
		return magic_packet_size;
	}

	public void setMagic_packet_size(int magic_packet_size) {
		this.magic_packet_size = magic_packet_size;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getIsuse() {
		return isuse;
	}

	public void setIsuse(int isuse) {
		this.isuse = isuse;
	}

}
