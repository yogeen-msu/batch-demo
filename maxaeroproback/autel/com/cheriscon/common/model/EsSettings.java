package com.cheriscon.common.model;



/**
 * AbstractEsSettings entity provides the base persistence definition of the EsSettings entity. @author MyEclipse Persistence Tools
 */

public abstract class EsSettings  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String code;
     private String cfgValue;
     private String cfgGroup;


    // Constructors

    /** default constructor */
    public EsSettings() {
    }

    
    /** full constructor */
    public EsSettings(String code, String cfgValue, String cfgGroup) {
        this.code = code;
        this.cfgValue = cfgValue;
        this.cfgGroup = cfgGroup;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getCfgValue() {
        return this.cfgValue;
    }
    
    public void setCfgValue(String cfgValue) {
        this.cfgValue = cfgValue;
    }

    public String getCfgGroup() {
        return this.cfgGroup;
    }
    
    public void setCfgGroup(String cfgGroup) {
        this.cfgGroup = cfgGroup;
    }
   








}