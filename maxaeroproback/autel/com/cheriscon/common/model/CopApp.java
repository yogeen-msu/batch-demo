package com.cheriscon.common.model;



/**
 * AbstractCopApp entity provides the base persistence definition of the CopApp entity. @author MyEclipse Persistence Tools
 */

public abstract class CopApp  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String appid;
     private String appName;
     private String author;
     private String descript;
     private Integer deployment;
     private String path;
     private String authorizationcode;
     private String installuri;
     private Integer deleteflag;
     private String version;


    // Constructors

    /** default constructor */
    public CopApp() {
    }

    
    /** full constructor */
    public CopApp(String appid, String appName, String author, String descript, Integer deployment, String path, String authorizationcode, String installuri, Integer deleteflag, String version) {
        this.appid = appid;
        this.appName = appName;
        this.author = author;
        this.descript = descript;
        this.deployment = deployment;
        this.path = path;
        this.authorizationcode = authorizationcode;
        this.installuri = installuri;
        this.deleteflag = deleteflag;
        this.version = version;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppid() {
        return this.appid;
    }
    
    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppName() {
        return this.appName;
    }
    
    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescript() {
        return this.descript;
    }
    
    public void setDescript(String descript) {
        this.descript = descript;
    }

    public Integer getDeployment() {
        return this.deployment;
    }
    
    public void setDeployment(Integer deployment) {
        this.deployment = deployment;
    }

    public String getPath() {
        return this.path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }

    public String getAuthorizationcode() {
        return this.authorizationcode;
    }
    
    public void setAuthorizationcode(String authorizationcode) {
        this.authorizationcode = authorizationcode;
    }

    public String getInstalluri() {
        return this.installuri;
    }
    
    public void setInstalluri(String installuri) {
        this.installuri = installuri;
    }

    public Integer getDeleteflag() {
        return this.deleteflag;
    }
    
    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getVersion() {
        return this.version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }
   








}