package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * paypal信用卡支付 保存token
 * @author AUTEL
 *
 */
public class PayPalToken implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String orderNo;
	private String secureToken;
	private String secureTokenId;
	private String createTime;
	
	public PayPalToken(){}
	
	public PayPalToken(String orderNo,String secureToken,String secureTokenId){
		this.orderNo = orderNo;
		this.secureToken = secureToken;
		this.secureTokenId = secureTokenId;
	}
	
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getSecureToken() {
		return secureToken;
	}
	public void setSecureToken(String secureToken) {
		this.secureToken = secureToken;
	}
	public String getSecureTokenId() {
		return secureTokenId;
	}
	public void setSecureTokenId(String secureTokenId) {
		this.secureTokenId = secureTokenId;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	

}
