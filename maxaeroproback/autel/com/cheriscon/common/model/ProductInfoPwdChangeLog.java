package com.cheriscon.common.model;

/**
 * 产品信息表
 * 
 * @author yinhb
 * 
 */
public class ProductInfoPwdChangeLog implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1941165948575011920L;
	
	private Integer id;
	private String proCode;
	private String oldPwd;
	private String newPwd;
	private String operatorUser;
	private String operatorDate;
	public Integer getId() {
		return id;
	}
	public String getProCode() {
		return proCode;
	}
	public String getOldPwd() {
		return oldPwd;
	}
	public String getNewPwd() {
		return newPwd;
	}
	public String getOperatorUser() {
		return operatorUser;
	}
	public String getOperatorDate() {
		return operatorDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}
	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}
	public void setOperatorUser(String operatorUser) {
		this.operatorUser = operatorUser;
	}
	public void setOperatorDate(String operatorDate) {
		this.operatorDate = operatorDate;
	}


	
}