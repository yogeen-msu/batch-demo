package com.cheriscon.common.model;



/**
 * AbstractEsAdcolumn entity provides the base persistence definition of the EsAdcolumn entity. @author MyEclipse Persistence Tools
 */

public abstract class EsAdcolumn  implements java.io.Serializable {


    // Fields    

     private Integer acid;
     private String cname;
     private String width;
     private String height;
     private String description;
     private Long anumber;
     private Integer atype;
     private Long arule;
     private String disabled;


    // Constructors

    /** default constructor */
    public EsAdcolumn() {
    }

    
    /** full constructor */
    public EsAdcolumn(String cname, String width, String height, String description, Long anumber, Integer atype, Long arule, String disabled) {
        this.cname = cname;
        this.width = width;
        this.height = height;
        this.description = description;
        this.anumber = anumber;
        this.atype = atype;
        this.arule = arule;
        this.disabled = disabled;
    }

   
    // Property accessors

    public Integer getAcid() {
        return this.acid;
    }
    
    public void setAcid(Integer acid) {
        this.acid = acid;
    }

    public String getCname() {
        return this.cname;
    }
    
    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getWidth() {
        return this.width;
    }
    
    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return this.height;
    }
    
    public void setHeight(String height) {
        this.height = height;
    }

    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAnumber() {
        return this.anumber;
    }
    
    public void setAnumber(Long anumber) {
        this.anumber = anumber;
    }

    public Integer getAtype() {
        return this.atype;
    }
    
    public void setAtype(Integer atype) {
        this.atype = atype;
    }

    public Long getArule() {
        return this.arule;
    }
    
    public void setArule(Long arule) {
        this.arule = arule;
    }

    public String getDisabled() {
        return this.disabled;
    }
    
    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }
   








}