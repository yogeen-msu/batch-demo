package com.cheriscon.common.model;



/**
 * AbstractCopAsk entity provides the base persistence definition of the CopAsk entity. @author MyEclipse Persistence Tools
 */

public abstract class CopAsk  implements java.io.Serializable {


    // Fields    

     private Integer askid;
     private String title;
     private String content;
     private Integer dateline;
     private Short isreply;
     private Integer userid;
     private Integer siteid;
     private String domain;
     private String sitename;
     private String username;


    // Constructors

    /** default constructor */
    public CopAsk() {
    }

    
    /** full constructor */
    public CopAsk(String title, String content, Integer dateline, Short isreply, Integer userid, Integer siteid, String domain, String sitename, String username) {
        this.title = title;
        this.content = content;
        this.dateline = dateline;
        this.isreply = isreply;
        this.userid = userid;
        this.siteid = siteid;
        this.domain = domain;
        this.sitename = sitename;
        this.username = username;
    }

   
    // Property accessors

    public Integer getAskid() {
        return this.askid;
    }
    
    public void setAskid(Integer askid) {
        this.askid = askid;
    }

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }

    public Integer getDateline() {
        return this.dateline;
    }
    
    public void setDateline(Integer dateline) {
        this.dateline = dateline;
    }

    public Short getIsreply() {
        return this.isreply;
    }
    
    public void setIsreply(Short isreply) {
        this.isreply = isreply;
    }

    public Integer getUserid() {
        return this.userid;
    }
    
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getSiteid() {
        return this.siteid;
    }
    
    public void setSiteid(Integer siteid) {
        this.siteid = siteid;
    }

    public String getDomain() {
        return this.domain;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSitename() {
        return this.sitename;
    }
    
    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
   








}