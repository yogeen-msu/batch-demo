package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * @remark	客诉类型名称实体
 * @date	2013-03-31
 * @table	DT_CustomerComplaintTypeName
 * @author pengdongan
 *
 */
public class CustomerComplaintTypeName implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7172700938779602704L;
	//客诉类型名称id
	private Integer id;
	//客诉类型code
	private String complaintTypeCode;
	//语言类型code
	private String languageCode;
	//客诉类型名称
	private String name;
	
	

 	/**  非表字段  begin */
	
	private String languageName;					//语言类型名称

 	/**  非表字段  end */
	
	
	public CustomerComplaintTypeName() {
		super();
	}

	public CustomerComplaintTypeName(Integer id, String complaintTypeCode, String languageCode, String name) {
		this.id = id;
		this.complaintTypeCode = complaintTypeCode;
		this.languageCode = languageCode;
		this.name = name;
	}

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getComplaintTypeCode() {
		return complaintTypeCode;
	}

	public void setComplaintTypeCode(String complaintTypeCode) {
		this.complaintTypeCode = complaintTypeCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

 	/**  非表字段  begin */

	@NotDbField
	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

 	/**  非表字段  end */
	
}
