package com.cheriscon.common.model;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 语言配置详情
 * @author yinhb
 */
public class LanguageConfigDetail {
	
	private int id;									//id
	private String languageCode;					//语言Code
	private String languageCfgCode;					//语言配置Code
	
	
	@PrimaryKeyField
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getLanguageCfgCode() {
		return languageCfgCode;
	}
	public void setLanguageCfgCode(String languageCfgCode) {
		this.languageCfgCode = languageCfgCode;
	}
	
}
