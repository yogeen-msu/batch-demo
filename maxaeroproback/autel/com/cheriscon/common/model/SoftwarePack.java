package com.cheriscon.common.model;

/**
 * @ClassName: SoftwarePack
 * @Description: 软件基础包信息
 * @author shaohu
 * @date 2013-1-11 下午02:46:49
 * 
 */
public class SoftwarePack implements java.io.Serializable {

	private static final long serialVersionUID = 1165020995011829531L;

	private Integer id;
	/**
	 * 软件版本编码
	 */
	private String softwareVersionCode;

	/**
	 * 软件基础包编码
	 */
	private String code;

	/**
	 * 基础包相对路径
	 */
	private String downloadPath;

	public SoftwarePack() {
		super();
	}

	public SoftwarePack(Integer id, String softwareVersionCode, String code,
			String downloadPath) {
		super();
		this.id = id;
		this.softwareVersionCode = softwareVersionCode;
		this.code = code;
		this.downloadPath = downloadPath;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSoftwareVersionCode() {
		return softwareVersionCode;
	}

	public void setSoftwareVersionCode(String softwareVersionCode) {
		this.softwareVersionCode = softwareVersionCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

}