package com.cheriscon.common.model;


/**
 * AbstractEsDataModel entity provides the base persistence definition of the EsDataModel entity. @author MyEclipse Persistence Tools
 */

public abstract class EsDataModel  implements java.io.Serializable {


    // Fields    

     private Integer modelId;
     private String chinaName;
     private String englishName;
     private Long addTime;
     private String projectName;
     private String brief;
     private Integer ifAudit;


    // Constructors

    /** default constructor */
    public EsDataModel() {
    }

    
    /** full constructor */
    public EsDataModel(String chinaName, String englishName, Long addTime, String projectName, String brief, Integer ifAudit) {
        this.chinaName = chinaName;
        this.englishName = englishName;
        this.addTime = addTime;
        this.projectName = projectName;
        this.brief = brief;
        this.ifAudit = ifAudit;
    }

   
    // Property accessors

    public Integer getModelId() {
        return this.modelId;
    }
    
    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getChinaName() {
        return this.chinaName;
    }
    
    public void setChinaName(String chinaName) {
        this.chinaName = chinaName;
    }

    public String getEnglishName() {
        return this.englishName;
    }
    
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public Long getAddTime() {
        return this.addTime;
    }
    
    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public String getProjectName() {
        return this.projectName;
    }
    
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getBrief() {
        return this.brief;
    }
    
    public void setBrief(String brief) {
        this.brief = brief;
    }

    public Integer getIfAudit() {
        return this.ifAudit;
    }
    
    public void setIfAudit(Integer ifAudit) {
        this.ifAudit = ifAudit;
    }
   








}