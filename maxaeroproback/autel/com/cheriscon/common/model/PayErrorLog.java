package com.cheriscon.common.model;

/**
 * 支付失败日志
 * 
 * @author chenqichuan
 */
public class PayErrorLog implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5958381238938231138L;
	
	private Integer id;
	/**
	 * 订单号
	 */
	private String orderNo;

	private String errorDate;
	private String remark;
	private String ip;
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getId() {
		return id;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public String getErrorDate() {
		return errorDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public void setErrorDate(String errorDate) {
		this.errorDate = errorDate;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}