package com.cheriscon.common.model;

public class EmailLog implements java.io.Serializable {

	/**
	 * 记录邮件发送日志
	 */
	private static final long serialVersionUID = -1540635241150208897L;

	private Integer id; 
	
	private String fromUser; //发送人
	
	private String toUser; // 接收人
	
	private String title; //标题
	
	private String content; //邮件正文
	
	private String createDate; //生成日期
	
	private Integer countNum; //失败次数
	
	private String updateDate; //更新日期

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getId() {
		return id;
	}

	public String getFromUser() {
		return fromUser;
	}

	public String getToUser() {
		return toUser;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public String getCreateDate() {
		return createDate;
	}

	public Integer getCountNum() {
		return countNum;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public void setCountNum(Integer countNum) {
		this.countNum = countNum;
	}
	
	

}
