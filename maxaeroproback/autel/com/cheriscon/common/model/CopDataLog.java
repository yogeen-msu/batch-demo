package com.cheriscon.common.model;



/**
 * AbstractCopDataLog entity provides the base persistence definition of the CopDataLog entity. @author MyEclipse Persistence Tools
 */

public abstract class CopDataLog  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String content;
     private String url;
     private String pics;
     private String sitename;
     private String domain;
     private String logtype;
     private String optype;
     private Integer dateline;
     private Long userid;
     private Long siteid;


    // Constructors

    /** default constructor */
    public CopDataLog() {
    }

    
    /** full constructor */
    public CopDataLog(String content, String url, String pics, String sitename, String domain, String logtype, String optype, Integer dateline, Long userid, Long siteid) {
        this.content = content;
        this.url = url;
        this.pics = pics;
        this.sitename = sitename;
        this.domain = domain;
        this.logtype = logtype;
        this.optype = optype;
        this.dateline = dateline;
        this.userid = userid;
        this.siteid = siteid;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getPics() {
        return this.pics;
    }
    
    public void setPics(String pics) {
        this.pics = pics;
    }

    public String getSitename() {
        return this.sitename;
    }
    
    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getDomain() {
        return this.domain;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getLogtype() {
        return this.logtype;
    }
    
    public void setLogtype(String logtype) {
        this.logtype = logtype;
    }

    public String getOptype() {
        return this.optype;
    }
    
    public void setOptype(String optype) {
        this.optype = optype;
    }

    public Integer getDateline() {
        return this.dateline;
    }
    
    public void setDateline(Integer dateline) {
        this.dateline = dateline;
    }

    public Long getUserid() {
        return this.userid;
    }
    
    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Long getSiteid() {
        return this.siteid;
    }
    
    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }
   








}