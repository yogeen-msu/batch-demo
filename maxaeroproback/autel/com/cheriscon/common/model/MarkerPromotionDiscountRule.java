package com.cheriscon.common.model;

/**
 * 
 * @ClassName: MarkerPromotionDiscountRule
 * @Description: 营销活动规则条件表
 * @author shaohu
 * @date 2013-1-19 上午11:59:31
 * 
 */
public class MarkerPromotionDiscountRule implements java.io.Serializable {

	private static final long serialVersionUID = -1679891506419016237L;
	private Integer id;
	/**
	 * 活动规则对象Coding
	 */
	private String marketPromotionRuleCode;
	private String code;
	/**
	 * 开始月
	 */
	private Integer betweenMonth;
	
	/**
	 * 结束月
	 */
	private Integer endMonth;
	/**
	 * 对应条件描述
	 */
	private String conditionDesc;
	/**
	 * 优惠折扣
	 */
	private Long discount;

	public MarkerPromotionDiscountRule() {
		super();
	}

	public MarkerPromotionDiscountRule(Integer id,
			String marketPromotionRuleCode, String code, Integer betweenMonth,
			Integer endMonth, String conditionDesc, Long discount) {
		super();
		this.id = id;
		this.marketPromotionRuleCode = marketPromotionRuleCode;
		this.code = code;
		this.betweenMonth = betweenMonth;
		this.endMonth = endMonth;
		this.conditionDesc = conditionDesc;
		this.discount = discount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarketPromotionRuleCode() {
		return marketPromotionRuleCode;
	}

	public void setMarketPromotionRuleCode(String marketPromotionRuleCode) {
		this.marketPromotionRuleCode = marketPromotionRuleCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getBetweenMonth() {
		return betweenMonth;
	}

	public void setBetweenMonth(Integer betweenMonth) {
		this.betweenMonth = betweenMonth;
	}

	public Integer getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(Integer endMonth) {
		this.endMonth = endMonth;
	}

	public String getConditionDesc() {
		return conditionDesc;
	}

	public void setConditionDesc(String conditionDesc) {
		this.conditionDesc = conditionDesc;
	}

	public Long getDiscount() {
		return discount;
	}

	public void setDiscount(Long discount) {
		this.discount = discount;
	}

}