package com.cheriscon.common.model;



/**
 * AbstractCopUser entity provides the base persistence definition of the CopUser entity. @author MyEclipse Persistence Tools
 */

public abstract class CopUser  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String username;
     private String companyname;
     private String password;
     private String address;
     private String legalrepresentative;
     private String linkman;
     private String tel;
     private String mobile;
     private String email;
     private String logofile;
     private String licensefile;
     private Integer defaultsiteid;
     private Integer deleteflag;
     private Integer usertype;
     private Integer createtime;


    // Constructors

    /** default constructor */
    public CopUser() {
    }

    
    /** full constructor */
    public CopUser(String username, String companyname, String password, String address, String legalrepresentative, String linkman, String tel, String mobile, String email, String logofile, String licensefile, Integer defaultsiteid, Integer deleteflag, Integer usertype, Integer createtime) {
        this.username = username;
        this.companyname = companyname;
        this.password = password;
        this.address = address;
        this.legalrepresentative = legalrepresentative;
        this.linkman = linkman;
        this.tel = tel;
        this.mobile = mobile;
        this.email = email;
        this.logofile = logofile;
        this.licensefile = licensefile;
        this.defaultsiteid = defaultsiteid;
        this.deleteflag = deleteflag;
        this.usertype = usertype;
        this.createtime = createtime;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompanyname() {
        return this.companyname;
    }
    
    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    public String getLegalrepresentative() {
        return this.legalrepresentative;
    }
    
    public void setLegalrepresentative(String legalrepresentative) {
        this.legalrepresentative = legalrepresentative;
    }

    public String getLinkman() {
        return this.linkman;
    }
    
    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getTel() {
        return this.tel;
    }
    
    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMobile() {
        return this.mobile;
    }
    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogofile() {
        return this.logofile;
    }
    
    public void setLogofile(String logofile) {
        this.logofile = logofile;
    }

    public String getLicensefile() {
        return this.licensefile;
    }
    
    public void setLicensefile(String licensefile) {
        this.licensefile = licensefile;
    }

    public Integer getDefaultsiteid() {
        return this.defaultsiteid;
    }
    
    public void setDefaultsiteid(Integer defaultsiteid) {
        this.defaultsiteid = defaultsiteid;
    }

    public Integer getDeleteflag() {
        return this.deleteflag;
    }
    
    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Integer getUsertype() {
        return this.usertype;
    }
    
    public void setUsertype(Integer usertype) {
        this.usertype = usertype;
    }

    public Integer getCreatetime() {
        return this.createtime;
    }
    
    public void setCreatetime(Integer createtime) {
        this.createtime = createtime;
    }
   








}