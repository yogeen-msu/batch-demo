package com.cheriscon.common.model;



/**
 * AbstractCopReply entity provides the base persistence definition of the CopReply entity. @author MyEclipse Persistence Tools
 */

public abstract class CopReply  implements java.io.Serializable {


    // Fields    

     private Integer replyid;
     private Integer askid;
     private String content;
     private String username;
     private Long dateline;


    // Constructors

    /** default constructor */
    public CopReply() {
    }

    
    /** full constructor */
    public CopReply(Integer askid, String content, String username, Long dateline) {
        this.askid = askid;
        this.content = content;
        this.username = username;
        this.dateline = dateline;
    }

   
    // Property accessors

    public Integer getReplyid() {
        return this.replyid;
    }
    
    public void setReplyid(Integer replyid) {
        this.replyid = replyid;
    }

    public Integer getAskid() {
        return this.askid;
    }
    
    public void setAskid(Integer askid) {
        this.askid = askid;
    }

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public Long getDateline() {
        return this.dateline;
    }
    
    public void setDateline(Long dateline) {
        this.dateline = dateline;
    }
   








}