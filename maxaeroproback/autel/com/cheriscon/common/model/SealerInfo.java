package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;


/**
 * @remark	经销商信息实体
 * @date	2013-01-14
 * @table	DT_SealerInfo
 * @author pengdongan
 *
 */

public class SealerInfo  implements java.io.Serializable {

	private static final long serialVersionUID = -2271633075495848502L;
	
	// Fields
	
	private Integer id;					//客户信息ID
	private String code;				//客户信息code
	private String autelId;				//AutelId
	private String userPwd;				//用户密码
	private String firstName;			//第一个名字
	private String middleName;			//中间名字
	private String lastName;			//最后名字
	private String daytimePhone;		//固定电话
	private String daytimePhoneCC;		//固定电话国家代码
	private String daytimePhoneAC;		//固定电话区域代码
	private String daytimePhoneExtCode;	//固定电话分机号
	private String mobilePhone;			//移动电话
	private String mobilePhoneCC;		//移动电话国家代码
	private String mobilePhoneAC;		//移动电话区域代码
	private String regTime;				//注册时间
	private String languageCode;		//语言code
	private String country;				//国家
	private String city;				//城市
	private String address;				//地址
	private String company;				//公司
	private String zipCode;				//邮编
	private String email;				//论坛名字
	private Integer isAllowSendEmail;	//是否允许发邮件
	private String lastLoginTime;		//最后登录时间
	private String name;
	private String sealerName;   //经销商名称
	private String areaName;     //区域名称
	private String secondEmail; //第二邮箱
	private String auth; //经销商权限编码
	private Integer status; //经销商账号状态 1.正常，0为停用
	private String telephone;//电话号码
	private String province; //州/省
	private String stateCode;//州/省代码
	private String countryCode;//国家代码
	
	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	@NotDbField
	public String getAuth() {
		return auth;
	}


	public void setAuth(String auth) {
		this.auth = auth;
	}


	public SealerInfo() {
		super();
	}


	public SealerInfo(Integer id, String code, String autelId, String userPwd,
			String firstName, String middleName, String lastName,
			String daytimePhone, String daytimePhoneCC, String daytimePhoneAC,
			String daytimePhoneExtCode, String mobilePhone,
			String mobilePhoneCC, String mobilePhoneAC, String regTime,
			String languageCode, String country, String city, String address,
			String company, String zipCode, String email,String telephone,
			Integer isAllowSendEmail, String lastLoginTime, String name,String secondEmail,
			String province,String stateCode,String countryCode) {
		super();
		this.id = id;
		this.code = code;
		this.autelId = autelId;
		this.userPwd = userPwd;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.daytimePhone = daytimePhone;
		this.daytimePhoneCC = daytimePhoneCC;
		this.daytimePhoneAC = daytimePhoneAC;
		this.daytimePhoneExtCode = daytimePhoneExtCode;
		this.mobilePhone = mobilePhone;
		this.mobilePhoneCC = mobilePhoneCC;
		this.mobilePhoneAC = mobilePhoneAC;
		this.regTime = regTime;
		this.languageCode = languageCode;
		this.country = country;
		this.city = city;
		this.address = address;
		this.company = company;
		this.zipCode = zipCode;
		this.email = email;
		this.isAllowSendEmail = isAllowSendEmail;
		this.lastLoginTime = lastLoginTime;
		this.name = name;
		this.secondEmail = secondEmail;
		this.telephone = telephone;
		this.province = province;
		this.stateCode = stateCode;
		this.countryCode = countryCode;
	}


	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDaytimePhone() {
		return daytimePhone;
	}

	public void setDaytimePhone(String daytimePhone) {
		this.daytimePhone = daytimePhone;
	}

	public String getDaytimePhoneCC() {
		return daytimePhoneCC;
	}

	public void setDaytimePhoneCC(String daytimePhoneCC) {
		this.daytimePhoneCC = daytimePhoneCC;
	}

	public String getDaytimePhoneAC() {
		return daytimePhoneAC;
	}

	public void setDaytimePhoneAC(String daytimePhoneAC) {
		this.daytimePhoneAC = daytimePhoneAC;
	}

	public String getDaytimePhoneExtCode() {
		return daytimePhoneExtCode;
	}

	public void setDaytimePhoneExtCode(String daytimePhoneExtCode) {
		this.daytimePhoneExtCode = daytimePhoneExtCode;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMobilePhoneCC() {
		return mobilePhoneCC;
	}

	public void setMobilePhoneCC(String mobilePhoneCC) {
		this.mobilePhoneCC = mobilePhoneCC;
	}

	public String getMobilePhoneAC() {
		return mobilePhoneAC;
	}

	public void setMobilePhoneAC(String mobilePhoneAC) {
		this.mobilePhoneAC = mobilePhoneAC;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIsAllowSendEmail() {
		return isAllowSendEmail;
	}

	public void setIsAllowSendEmail(Integer isAllowSendEmail) {
		this.isAllowSendEmail = isAllowSendEmail;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSealerName() {
		return sealerName;
	}

	public void setSealerName(String sealerName) {
		this.sealerName = sealerName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getSecondEmail() {
		return secondEmail;
	}

	public void setSecondEmail(String secondEmail) {
		this.secondEmail = secondEmail;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}