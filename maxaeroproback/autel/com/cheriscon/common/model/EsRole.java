package com.cheriscon.common.model;



/**
 * AbstractEsRole entity provides the base persistence definition of the EsRole entity. @author MyEclipse Persistence Tools
 */

public abstract class EsRole  implements java.io.Serializable {


    // Fields    

     private Integer roleid;
     private String rolename;
     private String rolememo;


    // Constructors

    /** default constructor */
    public EsRole() {
    }

    
    /** full constructor */
    public EsRole(String rolename, String rolememo) {
        this.rolename = rolename;
        this.rolememo = rolememo;
    }

   
    // Property accessors

    public Integer getRoleid() {
        return this.roleid;
    }
    
    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return this.rolename;
    }
    
    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getRolememo() {
        return this.rolememo;
    }
    
    public void setRolememo(String rolememo) {
        this.rolememo = rolememo;
    }
   








}