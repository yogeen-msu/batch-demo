package com.cheriscon.common.model;



/**
 * AbstractUserRole entity provides the base persistence definition of the UserRole entity. @author MyEclipse Persistence Tools
 */

public abstract class UserRole  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private SysUser sysUser;
     private Role role;


    // Constructors

    /** default constructor */
    public UserRole() {
    }

    
    /** full constructor */
    public UserRole(SysUser sysUser, Role role) {
        this.sysUser = sysUser;
        this.role = role;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public Role getRole() {
        return this.role;
    }
    
    public void setRole(Role role) {
        this.role = role;
    }
   








}