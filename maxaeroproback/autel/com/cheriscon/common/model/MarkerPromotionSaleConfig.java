package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 
* @ClassName: MarkerPromotionSaleConfig
* @Description: 营销活动参与销售配置
* @author shaohu
* @date 2013-3-10
 */
public class MarkerPromotionSaleConfig implements java.io.Serializable {
	
	private static final long serialVersionUID = -8900434939687699845L;
	
	private Integer id;
	/**
	 * 营销活动编码
	 */
	private String marketPromotionCode;
	
	private String code;
	/**
	 * 产品型号编码
	 */
	private String productTypeCode;
	/**
	 *  销售配置编码
	 */
	private String saleConfigCode;
	/**
	 * 销售配置名称
	 */
	private String saleConfigName;
	
	/**
	 * 产品型号名称
	 */
	private String productTypeName;
	

	public MarkerPromotionSaleConfig() {
		super();
	}

	public MarkerPromotionSaleConfig(Integer id, String marketPromotionCode,
			String code, String productTypeCode, String saleConfigCode,
			String saleConfigName, String productTypeName) {
		super();
		this.id = id;
		this.marketPromotionCode = marketPromotionCode;
		this.code = code;
		this.productTypeCode = productTypeCode;
		this.saleConfigCode = saleConfigCode;
		this.saleConfigName = saleConfigName;
		this.productTypeName = productTypeName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarketPromotionCode() {
		return marketPromotionCode;
	}

	public void setMarketPromotionCode(String marketPromotionCode) {
		this.marketPromotionCode = marketPromotionCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getSaleConfigCode() {
		return saleConfigCode;
	}

	public void setSaleConfigCode(String saleConfigCode) {
		this.saleConfigCode = saleConfigCode;
	}

	public String getSaleConfigName() {
		return saleConfigName;
	}

	public void setSaleConfigName(String saleConfigName) {
		this.saleConfigName = saleConfigName;
	}

	@NotDbField
	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

}