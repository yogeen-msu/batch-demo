package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * @remark	经销商类型实体
 * @date	2013-01-10
 * @table	DT_SealerType
 * @author pengdongan
 *
 */
public class SealerType implements Serializable{

	private static final long serialVersionUID = -1349031974926150909L;
	
	//经销商类型id
	private Integer id;
	//经销商类型code
	private String code;
	//经销商类型名称
	private String name;
	
	public SealerType() {
		super();
	}

	public SealerType(Integer id, String code, String name) {
		this.id = id;
		this.code = code;
		this.name = name;
	}

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
