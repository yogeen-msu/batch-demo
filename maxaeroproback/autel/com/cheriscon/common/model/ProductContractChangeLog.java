package com.cheriscon.common.model;

/**
 * 产品销售契约修改
 * 
 * @author chenqichuan
 */
public class ProductContractChangeLog implements java.io.Serializable {

	private static final long serialVersionUID = -7913713183686945911L;

	private Integer id;
	private String productSN; // 产品序列号
	private String oldMinSaleUnit; // 修改的最小销售单位

	private String oldContract; // 更改前时间
	private String newContract; // 更新后时间
	private String operatorUser; // 操作人
	private String operatorTime; // 操作时间
	private String operatorIp; // 操作IP
	private String sendEmailFlag; //是否发送过邮件提醒 Y/N
	public String getSendEmailFlag() {
		return sendEmailFlag;
	}
	public void setSendEmailFlag(String sendEmailFlag) {
		this.sendEmailFlag = sendEmailFlag;
	}
	public Integer getId() {
		return id;
	}
	public String getProductSN() {
		return productSN;
	}
	public String getOldMinSaleUnit() {
		return oldMinSaleUnit;
	}
	public String getOldContract() {
		return oldContract;
	}
	public String getNewContract() {
		return newContract;
	}
	public String getOperatorUser() {
		return operatorUser;
	}
	public String getOperatorTime() {
		return operatorTime;
	}
	public String getOperatorIp() {
		return operatorIp;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}
	public void setOldMinSaleUnit(String oldMinSaleUnit) {
		this.oldMinSaleUnit = oldMinSaleUnit;
	}
	public void setOldContract(String oldContract) {
		this.oldContract = oldContract;
	}
	public void setNewContract(String newContract) {
		this.newContract = newContract;
	}
	public void setOperatorUser(String operatorUser) {
		this.operatorUser = operatorUser;
	}
	public void setOperatorTime(String operatorTime) {
		this.operatorTime = operatorTime;
	}
	public void setOperatorIp(String operatorIp) {
		this.operatorIp = operatorIp;
	}

	
}