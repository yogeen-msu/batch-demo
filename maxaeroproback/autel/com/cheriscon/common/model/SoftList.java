package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

public class SoftList implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 239360636561034434L;
	
	private Integer softListId;
	
	private String productSN; //产品序列号
	
	private String softCode; //软件code
	
	private String softVersion; //软件版本
	
	private String regCode; //注册密码
	
	private Integer syncTimestamp; //升级日期UTC
	
	private String syncTime; //升级日期
	
	private String lastOpCode; //
	
	private String softName ; //软件名称
	
	private String softSysVersion; //系统上软件版本

	@NotDbField
	public String getSoftSysVersion() {
		return softSysVersion;
	}

	public void setSoftSysVersion(String softSysVersion) {
		this.softSysVersion = softSysVersion;
	}

	@NotDbField
	public String getSoftName() {
		return softName;
	}

	public void setSoftName(String softName) {
		this.softName = softName;
	}

	public Integer getSoftListId() {
		return softListId;
	}

	public String getProductSN() {
		return productSN;
	}

	public String getSoftCode() {
		return softCode;
	}

	public String getSoftVersion() {
		return softVersion;
	}

	public String getRegCode() {
		return regCode;
	}

	public Integer getSyncTimestamp() {
		return syncTimestamp;
	}

	public String getSyncTime() {
		return syncTime;
	}

	public String getLastOpCode() {
		return lastOpCode;
	}

	public void setSoftListId(Integer softListId) {
		this.softListId = softListId;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	public void setSoftCode(String softCode) {
		this.softCode = softCode;
	}

	public void setSoftVersion(String softVersion) {
		this.softVersion = softVersion;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public void setSyncTimestamp(Integer syncTimestamp) {
		this.syncTimestamp = syncTimestamp;
	}

	public void setSyncTime(String syncTime) {
		this.syncTime = syncTime;
	}

	public void setLastOpCode(String lastOpCode) {
		this.lastOpCode = lastOpCode;
	}
	
  
	
}
