package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 
* @ClassName: MarkerPromotionSoftwareRule
* @Description: 营销活动参与软件条件
* @author shaohu
* @date 2013-1-19 下午01:54:31
*
 */
public class MarkerPromotionSoftwareRule implements java.io.Serializable {
	private static final long serialVersionUID = -2699377051461702744L;
	private Integer id;
	/**
	 * 营销活动编码
	 */
	private String marketPromotionCode;
	
	private String code;
	/**
	 * 产品型号编码
	 */
	private String productTypeCode;
	/**
	 * 最小销售单位编码   或 是 销售配置编码
	 */
	private String minSaleUnitCode;
	/**
	 * 最小销售单位名称
	 */
	private String minSaleUnitName;
	/**
	 * 原价
	 */
	private Long costPrice;
	
	/**
	 * 产品型号名称
	 */
	private String productTypeName;
	

	public MarkerPromotionSoftwareRule() {
		super();
	}

	public MarkerPromotionSoftwareRule(Integer id, String marketPromotionCode,
			String code, String productTypeCode, String minSaleUnitCode,
			String minSaleUnitName, Long costPrice, String productTypeName) {
		super();
		this.id = id;
		this.marketPromotionCode = marketPromotionCode;
		this.code = code;
		this.productTypeCode = productTypeCode;
		this.minSaleUnitCode = minSaleUnitCode;
		this.minSaleUnitName = minSaleUnitName;
		this.costPrice = costPrice;
		this.productTypeName = productTypeName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarketPromotionCode() {
		return marketPromotionCode;
	}

	public void setMarketPromotionCode(String marketPromotionCode) {
		this.marketPromotionCode = marketPromotionCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	public String getMinSaleUnitName() {
		return minSaleUnitName;
	}

	public void setMinSaleUnitName(String minSaleUnitName) {
		this.minSaleUnitName = minSaleUnitName;
	}

	public Long getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Long costPrice) {
		this.costPrice = costPrice;
	}

	@NotDbField
	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

}