package com.cheriscon.common.model;

/**
 * 产品软件有效修改日志
 * 
 * @author chenqichuan
 */
public class ProductSoftwareValidChangeLog implements java.io.Serializable {

	private static final long serialVersionUID = -7913713183686945911L;

	private Integer id;
	private String productSN; // 产品序列号
	private String minSaleUnit; // 修改的最小销售单位

	private String oldDate; // 更改前时间
	private String newDate; // 更新后时间
	private String operatorUser; // 操作人
	private String operatorTime; // 操作时间
	private String operatorIp; // 操作IP
	private String operType; //操作类型 1，在线续费，2.升级卡，3.后台手动，4.退款操作，5.追回款项，恢复升级

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	public Integer getId() {
		return id;
	}

	public String getProductSN() {
		return productSN;
	}

	public String getOldDate() {
		return oldDate;
	}

	public String getNewDate() {
		return newDate;
	}

	public String getOperatorUser() {
		return operatorUser;
	}

	public String getOperatorTime() {
		return operatorTime;
	}

	public String getOperatorIp() {
		return operatorIp;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	public void setOldDate(String oldDate) {
		this.oldDate = oldDate;
	}

	public void setNewDate(String newDate) {
		this.newDate = newDate;
	}

	public void setOperatorUser(String operatorUser) {
		this.operatorUser = operatorUser;
	}

	public void setOperatorTime(String operatorTime) {
		this.operatorTime = operatorTime;
	}

	public void setOperatorIp(String operatorIp) {
		this.operatorIp = operatorIp;
	}

	public String getMinSaleUnit() {
		return minSaleUnit;
	}

	public void setMinSaleUnit(String minSaleUnit) {
		this.minSaleUnit = minSaleUnit;
	}
}