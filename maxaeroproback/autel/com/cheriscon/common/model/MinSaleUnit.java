package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 最小销售单位
 * 
 * @author yinhb
 */
public class MinSaleUnit implements java.io.Serializable {

	/*********************************
	 * 些实体只是临时修改，只使用用  id,code,proTypeCode 具体业务可在修改
	 * 尹鸿彪
	 ********************************/

	private static final long serialVersionUID = 1741864904898100067L;
	
	private Integer id;
	private String code;
	private String proTypeCode;
	private String picPath;
	
	/**
	 * 修改最小销售单位时使用
	 */
	private String name;
	
	/**
	 * 可以升级的最小销售单位code
	 */
	private String upCode;
	
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getProTypeCode() {
		return proTypeCode;
	}
	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
	
	public String getPicPath() {
		return picPath;
	}
	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	@NotDbField
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the upCode
	 */
	public String getUpCode() {
		return upCode;
	}
	/**
	 * @param upCode the upCode to set
	 */
	public void setUpCode(String upCode) {
		this.upCode = upCode;
	}
	
}