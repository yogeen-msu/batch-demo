package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 产品软件有效状态信息
 * 
 * @author yinhb
 */
public class ProductSoftwareValidStatus implements java.io.Serializable {

	private static final long serialVersionUID = -7913713183686945911L;
	
	
	// Fields
	private Integer id;
	private String code;
	private String validDate; // 有效期
	private String proCode; // 产品code
	private String minSaleUnitCode; // 小销售单位Code
	
	private String cusAutelid;			//客户ID
	private String sealAutelid;			//经销商ID
	
	private MinSaleUnitMemo minSaleUnitMemo;			//最小销售单位说明信息
	private ProductInfo productInfo;					//产品信息
	private SaleContract saleContract;					//销售契约
	private AreaConfig areaConfig;						//区域配置
	private CustomerProInfo customerProInfo;			//客户产品

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	@NotDbField
	public MinSaleUnitMemo getMinSaleUnitMemo() {
		return minSaleUnitMemo;
	}

	public void setMinSaleUnitMemo(MinSaleUnitMemo minSaleUnitMemo) {
		this.minSaleUnitMemo = minSaleUnitMemo;
	}

	@NotDbField
	public AreaConfig getAreaConfig() {
		return areaConfig;
	}

	public void setAreaConfig(AreaConfig areaConfig) {
		this.areaConfig = areaConfig;
	}

	@NotDbField
	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	@NotDbField
	public CustomerProInfo getCustomerProInfo() {
		return customerProInfo;
	}

	public void setCustomerProInfo(CustomerProInfo customerProInfo) {
		this.customerProInfo = customerProInfo;
	}

	public SaleContract getSaleContract() {
		return saleContract;
	}

	public void setSaleContract(SaleContract saleContract) {
		this.saleContract = saleContract;
	}

	@NotDbField
	public String getCusAutelid() {
		return cusAutelid;
	}

	public void setCusAutelid(String cusAutelid) {
		this.cusAutelid = cusAutelid;
	}

	@NotDbField
	public String getSealAutelid() {
		return sealAutelid;
	}

	public void setSealAutelid(String sealAutelid) {
		this.sealAutelid = sealAutelid;
	}

	
	
	
	

}