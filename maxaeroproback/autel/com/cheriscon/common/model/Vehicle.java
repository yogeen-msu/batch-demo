package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

public class Vehicle implements java.io.Serializable {

	/**
	 * 车型选择列表
	 */
	private static final long serialVersionUID = 2442828765875527266L;
	
	private Integer id;
	
	private String coding;
	
	private String yearName;
	
	private String mark;
	
	private String model;
	
	private String submodel;
	
	private String engine;
	
	private String queryType; //查询类型

	@NotDbField
	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public Integer getId() {
		return id;
	}

	public String getCoding() {
		return coding;
	}

	public String getYearName() {
		return yearName;
	}

	public String getMark() {
		return mark;
	}

	public String getModel() {
		return model;
	}

	public String getSubmodel() {
		return submodel;
	}

	public String getEngine() {
		return engine;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCoding(String coding) {
		this.coding = coding;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setSubmodel(String submodel) {
		this.submodel = submodel;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}
	

}
