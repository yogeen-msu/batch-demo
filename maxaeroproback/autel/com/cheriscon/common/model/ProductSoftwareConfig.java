package com.cheriscon.common.model;

import java.util.List;

import com.cheriscon.backstage.market.vo.MinSaleUnitSearch;
import com.cheriscon.framework.database.NotDbField;

public class ProductSoftwareConfig implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4314919453849447479L;
	
	private Integer id;
	private String code;
	private String name;
	private String parentCode;	
	private Integer SoftwareFlag;  // 0:不是软件，1：是软件
	

	private List<ProductSoftwareConfig> children;
	private List<MinSaleUnitSearch> minList;
	
	@NotDbField
	public List<MinSaleUnitSearch> getMinList() {
		return minList;
	}
	public void setMinList(List<MinSaleUnitSearch> minList) {
		this.minList = minList;
	}

	private boolean hasChildren ;


	@NotDbField
	public boolean getHasChildren() {
		hasChildren = this.children==null|| this.children.isEmpty() ?false:true;
		return hasChildren;
	}
	
	private Integer level;
	
	@NotDbField
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public List<ProductSoftwareConfig> getChildren() {
		return children;
	}
	public void setChildren(List<ProductSoftwareConfig> children) {
		this.children = children;
	}

	public ProductSoftwareConfig() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public Integer getSoftwareFlag() {
		return SoftwareFlag;
	}

	public void setSoftwareFlag(Integer softwareFlag) {
		SoftwareFlag = softwareFlag;
	}

}