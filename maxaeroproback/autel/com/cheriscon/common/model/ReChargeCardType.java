package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * AbstractReChargeCardType entity provides the base persistence definition of the ReChargeCardType entity. @author MyEclipse Persistence Tools
 */

public class ReChargeCardType  implements java.io.Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4798785974952496481L;
	// Fields    

     private Integer id;				//id
     private String code;				//code
     private String name;				//充值卡类型名称
     private String price;				//价格
     private String proTypeCode;		//产品型号code
     private String saleContractCode;	//销售契约code
     private String month; //升级月
     
 	

 	public String getMonth() {
		return month;
	}


	public void setMonth(String month) {
		this.month = month;
	}


	/**  非充值卡类型表字段  begin */

    private String saleCfgCode;				//销售配置code
    private String areaCfgCode;				//区域配置code
 	private String proTypeName;				//产品型号名称
 	private String saleCfgName;				//销售配置名称
 	private String areaCfgName;				//区域配置名称
 	private String saleContractName;		//销售契约名称
 	
 	/**  非充值卡类型表字段  end */
 	


    // Constructors

    /** default constructor */
    public ReChargeCardType() {
    }

   
    // Property accessors

    @PrimaryKeyField
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return this.price;
    }
    
    public void setPrice(String price) {
        this.price = price;
    }

    public String getProTypeCode() {
        return this.proTypeCode;
    }
    
    public void setProTypeCode(String proTypeCode) {
        this.proTypeCode = proTypeCode;
    }

    public String getSaleContractCode() {
		return saleContractCode;
	}


	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}


	
	

 	/**  非充值卡类型表字段  begin */


	
	public String getAreaCfgCode() {
        return this.areaCfgCode;
    }
    
    public void setAreaCfgCode(String areaCfgCode) {
        this.areaCfgCode = areaCfgCode;
    }


	@NotDbField
	public String getSaleCfgCode() {
		return saleCfgCode;
	}


	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}

	@NotDbField
	public String getProTypeName() {
		return proTypeName;
	}


	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}


	@NotDbField
	public String getSaleCfgName() {
		return saleCfgName;
	}


	public void setSaleCfgName(String saleCfgName) {
		this.saleCfgName = saleCfgName;
	}


	@NotDbField
	public String getAreaCfgName() {
		return areaCfgName;
	}


	public void setAreaCfgName(String areaCfgName) {
		this.areaCfgName = areaCfgName;
	}


	@NotDbField
	public String getSaleContractName() {
		return saleContractName;
	}


	public void setSaleContractName(String saleContractName) {
		this.saleContractName = saleContractName;
	}
 	
 	/**  非充值卡类型表字段  end */
 	
 	

}