package com.cheriscon.common.model;

import java.io.Serializable;

public class MaxiSysToProErrorLog  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4760966520661083651L;
	private Integer id;
	private String desUrl;
	private String url;
	private String createDate;
	private String type;
	private String errorCode;
	
	
	
	
	public Integer getId() {
		return id;
	}




	public String getDesUrl() {
		return desUrl;
	}




	public String getUrl() {
		return url;
	}




	public String getCreateDate() {
		return createDate;
	}




	public String getType() {
		return type;
	}




	public String getErrorCode() {
		return errorCode;
	}




	public void setId(Integer id) {
		this.id = id;
	}




	public void setDesUrl(String desUrl) {
		this.desUrl = desUrl;
	}




	public void setUrl(String url) {
		this.url = url;
	}




	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}




	public void setType(String type) {
		this.type = type;
	}




	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}




	public MaxiSysToProErrorLog() {
		// TODO Auto-generated constructor stub
	}

}
