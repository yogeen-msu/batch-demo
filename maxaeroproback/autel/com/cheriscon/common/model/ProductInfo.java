package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 产品信息表
 * 
 * @author yinhb
 * 
 */
public class ProductInfo implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1941165948575011920L;
	
	private Integer id;
	private String code;
	private String serialNo;					//产品序列号
	private String saleContractCode;			//销售契约Code
	private String proDate;						//产品出厂日期
	private Integer regStatus;					//注册状态
	private String regTime;						//注册时间
	private String aricraftSerialNumber;		//飞机序列号
	private String imuSerialNumber;				//云台序列号
	private String remoteControlSerialNumber;	//遥控器序列号
	private String batterySerialNumber;			//电池序列号
	private String gimbalMac;					//云台MAC地址
	private String remoteControlMac;			//遥控器MAC地址
	private String externInfo1;
	private String externInfo2;
	private String externInfo3;
	private String externInfo4;
	private String externInfo5;
	private String externInfo6;
	private String proTypeCode;					//产品型号Code
	private String createUser;  				//操作人
	
	
	
	
	private String saleContractName;			//销售契约名称
	private String proTypeName;					//产品型号名称
	private String regPwd;						//产品注册密码
	private String noRegReason;					//解绑原因
	private String saleContractArea; //销售契约对应的区域
	private String saleContractLanguage; //销售契约语言
	private String saleContractCfg; //销售契约标准销售配置
	private String sealerAutelId; //所属经销商
	private String validDate; //有效期
	private String remark; //说明
	
	private String noRegDate;   //操作时间
	
	private String customerCode; //对应日志表customerCode
	
	private Integer warrantyMonth;
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@NotDbField
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	@NotDbField
	public String getValidDate() {
		return validDate;
	}
	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}
	@NotDbField
	public String getSealerAutelId() {
		return sealerAutelId;
	}
	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}
	// Constructors
	@NotDbField
	public String getSaleContractCfg() {
		return saleContractCfg;
	}
	public void setSaleContractCfg(String saleContractCfg) {
		this.saleContractCfg = saleContractCfg;
	}
	@NotDbField
	public String getSaleContractLanguage() {
		return saleContractLanguage;
	}
	public void setSaleContractLanguage(String saleContractLanguage) {
		this.saleContractLanguage = saleContractLanguage;
	}
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getSaleContractCode() {
		return saleContractCode;
	}
	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}
	public String getProTypeCode() {
		return proTypeCode;
	}
	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
	public String getRegPwd() {
		return regPwd;
	}
	public void setRegPwd(String regPwd) {
		this.regPwd = regPwd;
	}
	public String getProDate() {
		return proDate;
	}
	public void setProDate(String proDate) {
		this.proDate = proDate;
	}
	public Integer getRegStatus() {
		return regStatus;
	}
	public void setRegStatus(Integer regStatus) {
		this.regStatus = regStatus;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getNoRegReason() {
		return noRegReason;
	}
	public void setNoRegReason(String noRegReason) {
		this.noRegReason = noRegReason;
	}
	public String getExternInfo1() {
		return externInfo1;
	}
	public void setExternInfo1(String externInfo1) {
		this.externInfo1 = externInfo1;
	}
	public String getExternInfo2() {
		return externInfo2;
	}
	public void setExternInfo2(String externInfo2) {
		this.externInfo2 = externInfo2;
	}
	public String getExternInfo3() {
		return externInfo3;
	}
	public void setExternInfo3(String externInfo3) {
		this.externInfo3 = externInfo3;
	}
	public String getExternInfo4() {
		return externInfo4;
	}
	public void setExternInfo4(String externInfo4) {
		this.externInfo4 = externInfo4;
	}
	public String getExternInfo5() {
		return externInfo5;
	}
	public void setExternInfo5(String externInfo5) {
		this.externInfo5 = externInfo5;
	}
	
	@NotDbField
	public String getProTypeName() {
		return proTypeName;
	}
	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}
	
	@NotDbField
	public String getSaleContractName() {
		return saleContractName;
	}
	public void setSaleContractName(String saleContractName) {
		this.saleContractName = saleContractName;
	}

	@NotDbField
	public String getSaleContractArea() {
		return saleContractArea;
	}
	public void setSaleContractArea(String saleContractArea) {
		this.saleContractArea = saleContractArea;
	}
	public String getNoRegDate() {
		return noRegDate;
	}
	public void setNoRegDate(String noRegDate) {
		this.noRegDate = noRegDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Integer getWarrantyMonth() {
		return warrantyMonth;
	}
	public void setWarrantyMonth(Integer warrantyMonth) {
		this.warrantyMonth = warrantyMonth;
	}
	public String getAricraftSerialNumber() {
		return aricraftSerialNumber;
	}
	public void setAricraftSerialNumber(String aricraftSerialNumber) {
		this.aricraftSerialNumber = aricraftSerialNumber;
	}
	public String getImuSerialNumber() {
		return imuSerialNumber;
	}
	public void setImuSerialNumber(String imuSerialNumber) {
		this.imuSerialNumber = imuSerialNumber;
	}	
	public String getRemoteControlSerialNumber() {
		return remoteControlSerialNumber;
	}
	public void setRemoteControlSerialNumber(String remoteControlSerialNumber) {
		this.remoteControlSerialNumber = remoteControlSerialNumber;
	}
	public String getBatterySerialNumber() {
		return batterySerialNumber;
	}
	public void setBatterySerialNumber(String batterySerialNumber) {
		this.batterySerialNumber = batterySerialNumber;
	}
	public String getGimbalMac() {
		return gimbalMac;
	}
	public void setGimbalMac(String gimbalMac) {
		this.gimbalMac = gimbalMac;
	}
	public String getRemoteControlMac() {
		return remoteControlMac;
	}
	public void setRemoteControlMac(String remoteControlMac) {
		this.remoteControlMac = remoteControlMac;
	}
	public String getExternInfo6() {
		return externInfo6;
	}
	public void setExternInfo6(String externInfo6) {
		this.externInfo6 = externInfo6;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}