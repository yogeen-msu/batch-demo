package com.cheriscon.common.model;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 经销商销售信息实体类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public class UserLoginInfo implements java.io.Serializable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3676875577156133176L;
	
	private Integer id;
	
	private String code;
	
	/**
	 * 用户code
	 */
	private String userCode;
	
	/**
	 * 登录时间
	 */
	private String loginTime;
	
	/**
	 * 用户类型
	 */
	private Integer userType;
	
	/**
	 * 登录ip
	 */
	private String loginIp;

	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	
	

}
