package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

/**
 * 
* @ClassName: MinSaleUnitPrice
* @Description: 最小销售单位价格实体表
* @author shaohu
* @date 2013-1-16 下午04:51:45
*
 */
@SuppressWarnings("serial")
public class MinSaleUnitPrice implements java.io.Serializable {

	private Integer id;
	private String minSaleUnitCode;
	private String areaCfgCode;
	private String price;
	
	/**
	 * 区域名称
	 */
	private String areaName;

	public MinSaleUnitPrice() {
	}

	public MinSaleUnitPrice(Integer id, String minSaleUnitCode,
			String areaCfgCode, String price, String areaName) {
		super();
		this.id = id;
		this.minSaleUnitCode = minSaleUnitCode;
		this.areaCfgCode = areaCfgCode;
		this.price = price;
		this.areaName = areaName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@NotDbField
	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

}