package com.cheriscon.common.model;

import java.io.Serializable;

public class DataCompare implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2047409318214577230L;
	
	private String carId;   //編碼
	private String carName;  //名稱
	private String languageCode; //語言
	private String versionName;  //版本
	
    private String oldLanguageCode;  //原始語言
    private String newLanguageCode;   //優化后語言
    
    private String minSaleUnitCode;  //原始最小銷售單位
    private String minSaleCode;    //優化后銷售單位
    
    private String oldName; //原始最小銷售單位名称
    private String newName; //優化后銷售單位名称
    
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getOldLanguageCode() {
		return oldLanguageCode;
	}
	public void setOldLanguageCode(String oldLanguageCode) {
		this.oldLanguageCode = oldLanguageCode;
	}
	public String getNewLanguageCode() {
		return newLanguageCode;
	}
	public void setNewLanguageCode(String newLanguageCode) {
		this.newLanguageCode = newLanguageCode;
	}
	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	public String getMinSaleCode() {
		return minSaleCode;
	}
	public void setMinSaleCode(String minSaleCode) {
		this.minSaleCode = minSaleCode;
	}
	
	public String getOldName() {
		return oldName;
	}
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
	public String getNewName() {
		return newName;
	}
	public void setNewName(String newName) {
		this.newName = newName;
	}
	@Override
	public boolean equals(Object arg0) {
		if(this==arg0){
			return true;
		}else if(arg0 instanceof DataCompare){
			DataCompare dataCompare=(DataCompare)arg0;
			if(dataCompare.getCarId().equals(carId)&&
					dataCompare.getLanguageCode().equals(languageCode)&&
					dataCompare.getVersionName().equals(versionName)){
				return true;
			}
		}else{
			return false;
		}
		return super.equals(arg0);
	}
	
}
