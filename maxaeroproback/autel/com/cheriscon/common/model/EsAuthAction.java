package com.cheriscon.common.model;



/**
 * AbstractEsAuthAction entity provides the base persistence definition of the EsAuthAction entity. @author MyEclipse Persistence Tools
 */

public abstract class EsAuthAction  implements java.io.Serializable {


    // Fields    

     private Integer actid;
     private String name;
     private String type;
     private String objvalue;


    // Constructors

    /** default constructor */
    public EsAuthAction() {
    }

    
    /** full constructor */
    public EsAuthAction(String name, String type, String objvalue) {
        this.name = name;
        this.type = type;
        this.objvalue = objvalue;
    }

   
    // Property accessors

    public Integer getActid() {
        return this.actid;
    }
    
    public void setActid(Integer actid) {
        this.actid = actid;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    public String getObjvalue() {
        return this.objvalue;
    }
    
    public void setObjvalue(String objvalue) {
        this.objvalue = objvalue;
    }
   








}