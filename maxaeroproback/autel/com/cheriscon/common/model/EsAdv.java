package com.cheriscon.common.model;


/**
 * AbstractEsAdv entity provides the base persistence definition of the EsAdv entity. @author MyEclipse Persistence Tools
 */

public abstract class EsAdv  implements java.io.Serializable {


    // Fields    

     private Long aid;
     private Long acid;
     private Integer atype;
     private Long begintime;
     private Long endtime;
     private Integer isclose;
     private String attachment;
     private String atturl;
     private String url;
     private String aname;
     private Integer clickcount;
     private String linkman;
     private String company;
     private String contact;
     private String disabled;


    // Constructors

    /** default constructor */
    public EsAdv() {
    }

    
    /** full constructor */
    public EsAdv(Long acid, Integer atype, Long begintime, Long endtime, Integer isclose, String attachment, String atturl, String url, String aname, Integer clickcount, String linkman, String company, String contact, String disabled) {
        this.acid = acid;
        this.atype = atype;
        this.begintime = begintime;
        this.endtime = endtime;
        this.isclose = isclose;
        this.attachment = attachment;
        this.atturl = atturl;
        this.url = url;
        this.aname = aname;
        this.clickcount = clickcount;
        this.linkman = linkman;
        this.company = company;
        this.contact = contact;
        this.disabled = disabled;
    }

   
    // Property accessors

    public Long getAid() {
        return this.aid;
    }
    
    public void setAid(Long aid) {
        this.aid = aid;
    }

    public Long getAcid() {
        return this.acid;
    }
    
    public void setAcid(Long acid) {
        this.acid = acid;
    }

    public Integer getAtype() {
        return this.atype;
    }
    
    public void setAtype(Integer atype) {
        this.atype = atype;
    }

    public Long getBegintime() {
        return this.begintime;
    }
    
    public void setBegintime(Long begintime) {
        this.begintime = begintime;
    }

    public Long getEndtime() {
        return this.endtime;
    }
    
    public void setEndtime(Long endtime) {
        this.endtime = endtime;
    }

    public Integer getIsclose() {
        return this.isclose;
    }
    
    public void setIsclose(Integer isclose) {
        this.isclose = isclose;
    }

    public String getAttachment() {
        return this.attachment;
    }
    
    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAtturl() {
        return this.atturl;
    }
    
    public void setAtturl(String atturl) {
        this.atturl = atturl;
    }

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getAname() {
        return this.aname;
    }
    
    public void setAname(String aname) {
        this.aname = aname;
    }

    public Integer getClickcount() {
        return this.clickcount;
    }
    
    public void setClickcount(Integer clickcount) {
        this.clickcount = clickcount;
    }

    public String getLinkman() {
        return this.linkman;
    }
    
    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getCompany() {
        return this.company;
    }
    
    public void setCompany(String company) {
        this.company = company;
    }

    public String getContact() {
        return this.contact;
    }
    
    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDisabled() {
        return this.disabled;
    }
    
    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }
   








}