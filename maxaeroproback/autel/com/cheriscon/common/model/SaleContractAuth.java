package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;

public class SaleContractAuth implements java.io.Serializable{

	/**
	 * 为了将部分契约修改权限下发到后台管理人员处
	 */
	private static final long serialVersionUID = -8649176719386535055L;

	/*数据库字段*/
	private Integer id;
	private String saleContractCode;
	private Integer userId;
	
	/*非数据库字段*/
    private String SaleContractName;
    private String userName;
	public Integer getId() {
		return id;
	}
	public String getSaleContractCode() {
		return saleContractCode;
	}
	public Integer getUserId() {
		return userId;
	}
	@NotDbField
	public String getSaleContractName() {
		return SaleContractName;
	}
	@NotDbField
	public String getUserName() {
		return userName;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public void setSaleContractName(String saleContractName) {
		SaleContractName = saleContractName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
    

}
