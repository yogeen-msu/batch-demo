package com.cheriscon.common.model;



/**
 * AbstractEsRegions entity provides the base persistence definition of the EsRegions entity. @author MyEclipse Persistence Tools
 */

public abstract class EsRegions  implements java.io.Serializable {


    // Fields    

     private Integer regionId;
     private Integer PRegionId;
     private String regionPath;
     private Integer regionGrade;
     private String localName;
     private String zipcode;
     private String cod;


    // Constructors

    /** default constructor */
    public EsRegions() {
    }

	/** minimal constructor */
    public EsRegions(String localName) {
        this.localName = localName;
    }
    
    /** full constructor */
    public EsRegions(Integer PRegionId, String regionPath, Integer regionGrade, String localName, String zipcode, String cod) {
        this.PRegionId = PRegionId;
        this.regionPath = regionPath;
        this.regionGrade = regionGrade;
        this.localName = localName;
        this.zipcode = zipcode;
        this.cod = cod;
    }

   
    // Property accessors

    public Integer getRegionId() {
        return this.regionId;
    }
    
    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public Integer getPRegionId() {
        return this.PRegionId;
    }
    
    public void setPRegionId(Integer PRegionId) {
        this.PRegionId = PRegionId;
    }

    public String getRegionPath() {
        return this.regionPath;
    }
    
    public void setRegionPath(String regionPath) {
        this.regionPath = regionPath;
    }

    public Integer getRegionGrade() {
        return this.regionGrade;
    }
    
    public void setRegionGrade(Integer regionGrade) {
        this.regionGrade = regionGrade;
    }

    public String getLocalName() {
        return this.localName;
    }
    
    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getZipcode() {
        return this.zipcode;
    }
    
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCod() {
        return this.cod;
    }
    
    public void setCod(String cod) {
        this.cod = cod;
    }
   








}