package com.cheriscon.common.model;



/**
 * AbstractEsMenu entity provides the base persistence definition of the EsMenu entity. @author MyEclipse Persistence Tools
 */

public abstract class EsMenu  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String appid;
     private Integer pid;
     private String title;
     private String url;
     private String target;
     private Integer sorder;
     private Integer menutype;
     private String datatype;
     private Integer selected;
     private Integer deleteflag;


    // Constructors

    /** default constructor */
    public EsMenu() {
    }

    
    /** full constructor */
    public EsMenu(String appid, Integer pid, String title, String url, String target, Integer sorder, Integer menutype, String datatype, Integer selected, Integer deleteflag) {
        this.appid = appid;
        this.pid = pid;
        this.title = title;
        this.url = url;
        this.target = target;
        this.sorder = sorder;
        this.menutype = menutype;
        this.datatype = datatype;
        this.selected = selected;
        this.deleteflag = deleteflag;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppid() {
        return this.appid;
    }
    
    public void setAppid(String appid) {
        this.appid = appid;
    }

    public Integer getPid() {
        return this.pid;
    }
    
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getTarget() {
        return this.target;
    }
    
    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getSorder() {
        return this.sorder;
    }
    
    public void setSorder(Integer sorder) {
        this.sorder = sorder;
    }

    public Integer getMenutype() {
        return this.menutype;
    }
    
    public void setMenutype(Integer menutype) {
        this.menutype = menutype;
    }

    public String getDatatype() {
        return this.datatype;
    }
    
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public Integer getSelected() {
        return this.selected;
    }
    
    public void setSelected(Integer selected) {
        this.selected = selected;
    }

    public Integer getDeleteflag() {
        return this.deleteflag;
    }
    
    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }
   








}