package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 订单取消日志
 * */
public class OrderCancelLog implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3959425256642591316L;
    private int id;
    private String proCode;
    private String custCode;
    private String orderCode;
    private String operUser;
    private String operDate;
    private String operReason;
    private int status;   //1为未处理，0为已处理
    private String lastOperUser;
    private String lastOperDate;
    
	/**非表字段start*/
    private String proSreialNo;
    private String custAutelId;
    /**非表字段end*/
    @PrimaryKeyField
	public int getId() {
		return id;
	}
	public String getProCode() {
		return proCode;
	}
	public String getCustCode() {
		return custCode;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public String getOperUser() {
		return operUser;
	}
	public String getOperDate() {
		return operDate;
	}
	public String getOperReason() {
		return operReason;
	}
	public int getStatus() {
		return status;
	}
	public String getLastOperUser() {
		return lastOperUser;
	}
	public String getLastOperDate() {
		return lastOperDate;
	}
	@NotDbField
	public String getProSreialNo() {
		return proSreialNo;
	}
	@NotDbField
	public String getCustAutelId() {
		return custAutelId;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public void setOperUser(String operUser) {
		this.operUser = operUser;
	}
	public void setOperDate(String operDate) {
		this.operDate = operDate;
	}
	public void setOperReason(String operReason) {
		this.operReason = operReason;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public void setLastOperUser(String lastOperUser) {
		this.lastOperUser = lastOperUser;
	}
	public void setLastOperDate(String lastOperDate) {
		this.lastOperDate = lastOperDate;
	}
	public void setProSreialNo(String proSreialNo) {
		this.proSreialNo = proSreialNo;
	}
	public void setCustAutelId(String custAutelId) {
		this.custAutelId = custAutelId;
	}

}
