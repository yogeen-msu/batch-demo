package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 产品维修记录
 * 
 * @author yinhb
 */
public class ProductRepairRecord implements java.io.Serializable {

	private static final long serialVersionUID = 3482335165602642575L;
	
	private Integer id;
	private String code;	
	private String proCode;					//产品编号
	private String repDate;					//维修日期
	private String repType;					//故障类型
	private String repContent;				//维修内容
	
	private String proTypeCode;				//产品型号
	private String proTypeName;				//产品型号名称
	private String proSerialNo;				//产品序列号
	private String proDate;					//出厂日期
	
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public String getRepDate() {
		return repDate;
	}
	public void setRepDate(String repDate) {
		this.repDate = repDate;
	}
	public String getRepType() {
		return repType;
	}
	public void setRepType(String repType) {
		this.repType = repType;
	}
	public String getRepContent() {
		return repContent;
	}
	public void setRepContent(String repContent) {
		this.repContent = repContent;
	}
	
	@NotDbField
	public String getProSerialNo() {
		return proSerialNo;
	}
	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}
	@NotDbField
	public String getProDate() {
		return proDate;
	}
	public void setProDate(String proDate) {
		this.proDate = proDate;
	}

	@NotDbField
	public String getProTypeName() {
		return proTypeName;
	}
	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}
	@NotDbField
	public String getProTypeCode() {
		return proTypeCode;
	}
	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}
	
	
	
	

}