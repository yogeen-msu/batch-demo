package com.cheriscon.common.model;

import com.cheriscon.framework.database.NotDbField;



/**
 * 客诉权限实体类
 * @author pengdongan
 * @version 创建时间：2013-03-23
 */

public  class ComplaintAdminuserInfo  implements java.io.Serializable {


    // Fields 
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = -3695796013761265783L;
	
	private Integer id;
    private String complaintTypeCode;
    private String adminUserid;
    private Integer emailId;
	private String emailName;


  	/**  非表字段  begin */
  	
  	private String languageCode;		//语言code
  	private String languageName;		//语言名称
  	private String complaintTypeName;	//客诉类型名称
  	
  	/**  非表字段  end */
  	
    // Constructors

    /** default constructor */
    public ComplaintAdminuserInfo() {
    }
    public Integer getEmailId() {
  		return emailId;
  	}

      @NotDbField
  	public String getEmailName() {
  		return emailName;
  	}


  	public void setEmailId(Integer emailId) {
  		this.emailId = emailId;
  	}


  	public void setEmailName(String emailName) {
  		this.emailName = emailName;
  	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getComplaintTypeCode() {
		return complaintTypeCode;
	}


	public void setComplaintTypeCode(String complaintTypeCode) {
		this.complaintTypeCode = complaintTypeCode;
	}



	public String getAdminUserid() {
		return adminUserid;
	}


	public void setAdminUserid(String adminUserid) {
		this.adminUserid = adminUserid;
	}


	/**  非表字段  begin */
	@NotDbField
	public String getComplaintTypeName() {
		return complaintTypeName;
	}


	public void setComplaintTypeName(String complaintTypeName) {
		this.complaintTypeName = complaintTypeName;
	}


	@NotDbField
	public String getLanguageCode() {
		return languageCode;
	}


	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}


	@NotDbField
	public String getLanguageName() {
		return languageName;
	}


	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	
	/**  非表字段  end */


}