package com.cheriscon.common.model;

import java.io.Serializable;

import com.cheriscon.framework.database.PrimaryKeyField;

public class CustomerProInfoHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1919647332208821419L;

	private Integer id;
	private String proCode;
	private String customerCode;
	private String createDate;
	private String createUser;
	private String updateDate;
	private String updateUser;
	
	
	@PrimaryKeyField
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
