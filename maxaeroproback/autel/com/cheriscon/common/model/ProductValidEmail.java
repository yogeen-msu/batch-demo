package com.cheriscon.common.model;

import java.io.Serializable;

public class ProductValidEmail implements Serializable{
	
	/**
	 * 产品到期邮件提醒记录
	 */
	private static final long serialVersionUID = -5463425836363281572L;
	
	private Integer  id;
	private String proCode;
	private String year;
	private String createTime;
	private String flag ; //邮件是否发送成功，1成功，0失败
	
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public Integer getId() {
		return id;
	}
	public String getProCode() {
		return proCode;
	}
	public String getYear() {
		return year;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
