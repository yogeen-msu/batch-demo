package com.cheriscon.common.model;

import java.util.List;

import com.cheriscon.framework.database.NotDbField;
import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 工具下载
 * @author yinhb
 */
public class ToolDownload implements java.io.Serializable {

	private static final long serialVersionUID = 32793800319651211L;
	
	
	// Fields
	private Integer id;
	private String code;						
	private String toolName;					//工具名称
	private String lastVersion;					//发布版本
	private String releaseDate;					//发布日期
	private String downloadPath;				//下载路径
	
	
	///////////////查询用参数//////////////////////
	private String productTypeCode;				//产品型号
	private String startDate;					//开始日期
	private String endDate;						//结束日期
	private List<ProductType> productTypeList;	//工具适用的产品型号

	@PrimaryKeyField
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getToolName() {
		return this.toolName;
	}

	public void setToolName(String toolName) {
		this.toolName = toolName;
	}

	public String getLastVersion() {
		return this.lastVersion;
	}

	public void setLastVersion(String lastVersion) {
		this.lastVersion = lastVersion;
	}

	public String getReleaseDate() {
		return this.releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDownloadPath() {
		return this.downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	@NotDbField
	public List<ProductType> getProductTypeList() {
		return productTypeList;
	}

	public void setProductTypeList(List<ProductType> productTypeList) {
		this.productTypeList = productTypeList;
	}

	@NotDbField
	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	@NotDbField
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@NotDbField
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	
	
	
}