package com.cheriscon.common.model;

import com.cheriscon.framework.database.PrimaryKeyField;

/**
 * 手机短信配置
 * 
 * @author yinhb
 */
public class PhoneMessageCfg implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7361631939037046056L;
	
	// Fields
	private Integer id;
	private String name;
	private String config;					//配置
	private String biref;					//描述
	private Integer status;					//状态   0:未启用   1：启用
	private String useTime;					//使用时间

	// Property accessors

	@PrimaryKeyField
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConfig() {
		return this.config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getBiref() {
		return this.biref;
	}

	public void setBiref(String biref) {
		this.biref = biref;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUseTime() {
		return useTime;
	}

	public void setUseTime(String useTime) {
		this.useTime = useTime;
	}
	
	
	

}