package com.cheriscon.front.user.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.BbsUser;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.JDBCUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.user.service.ICustomerInfoService;

/**
 * @remark 用户信息业务逻辑实现类
 * @author yangpinggui
 * @date 2013-01-07
 * 
 */
@Service
public class CustomerInfoServiceImpl extends BaseSupport<CustomerInfo>
		implements ICustomerInfoService {
	public CustomerInfoServiceImpl(){
		Properties pro = new Properties();
		InputStream inStream;
		try {
			inStream = CustomerInfoServiceImpl.class.getResourceAsStream("/config/jdbc.properties");
			pro.load(inStream);
			inStream.close();
			driverClass = pro.getProperty("bbs.jdbc.driverClassName");
			jdbcUrl = pro.getProperty("bbs.jdbc.url");
			user = pro.getProperty("bbs.jdbc.username");
			password = pro.getProperty("bbs.jdbc.password");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
//	@Resource(name="bbsDataSource")
//	private DataSource bbsDataSource;
	
	private String driverClass;
	private String jdbcUrl;
	private String user;
	private String password;

	
	@Transactional
	public boolean addCustomer(CustomerInfo customerInfo) throws Exception {
		customerInfo.setCode(DBUtils.generateCode(DBConstant.CUSTOMERINFO_CODE));
		this.daoSupport.insert("DT_CustomerInfo", customerInfo);
//		if (customerInfo.getFacebookId() == null) {
//			addBbsUser(customerInfo);
//		}
		return true;
	}

	protected void addBbsUser(CustomerInfo customerInfo) {

		BbsUser bbsUser = new BbsUser();
		bbsUser.setCode(customerInfo.getCode());
		bbsUser.setGroup_id(1);
		bbsUser.setUsername(customerInfo.getComUsername());
		bbsUser.setEmail(customerInfo.getAutelId());
		bbsUser.setRegister_time(customerInfo.getRegTime());
		bbsUser.setRegister_ip("");
		bbsUser.setLast_login_ip("");
		bbsUser.setLogin_count(0);
		bbsUser.setUpload_total(0);
		bbsUser.setUpload_size(0);
		bbsUser.setIs_admin(0);
		bbsUser.setIs_disabled(0);
		bbsUser.setProhibit_post(0);
		bbsUser.setPrestige(0);
		bbsUser.setIsuse(1);
		bbsUser.setUpload_today(0);
		bbsUser.setAvatar_type(0);
		bbsUser.setTopic_count(0);
		bbsUser.setReply_count(0);
		bbsUser.setPost_today(0);
		bbsUser.setMagic_packet_size(0);
		Map<String, Object> bbsum = new LinkedHashMap<String, Object>();
		bbsum.put("code", customerInfo.getCode());
		bbsum.put("Group_id", 1);
		bbsum.put("Username", customerInfo.getComUsername());
		bbsum.put("Email", customerInfo.getAutelId());
		bbsum.put("Register_time", customerInfo.getRegTime());
		bbsum.put("Register_ip", "");
		bbsum.put("Last_login_ip", "");
		bbsum.put("Login_count", 0);
		bbsum.put("Upload_total", 0);
		bbsum.put("Upload_size", 0);
		bbsum.put("Is_admin", 0);
		bbsum.put("Is_disabled", 0);
		bbsum.put("Prohibit_post", 0);
		bbsum.put("Prestige", 0);
		bbsum.put("Isuse", 0);
		bbsum.put("Upload_today", 0);
		bbsum.put("Avatar_type", 0);
		bbsum.put("Topic_count", 0);
		bbsum.put("Reply_count", 0);
		bbsum.put("Post_today", 0);
		bbsum.put("Magic_packet_size", 0);
		bbsum.put("GRADE_TODAY", 0);
		bbsum.put("POINT", 0);
		bbsum.put("PRIME_COUNT", 0);
		bbsum.put("gender", 0);
		
		this.daoSupport.insert("tbl_bbs_user", bbsUser);
//		this.daoSupport.insert("tbl_bbs_user", bbsUser);
		try {
			StringBuilder fields = new StringBuilder();
			StringBuilder values = new StringBuilder();
			Connection conn =JDBCUtils.getConnection(driverClass, jdbcUrl, user, password);
			PreparedStatement pstmt = null;
			conn.setAutoCommit(false); // 设置手动提交事务
			for(String key :bbsum.keySet()){
				fields.append(",`"+key+"`");
				values.append(",?");
			}
			String sql = "insert tbl_bbs_user ("+fields.deleteCharAt(0).toString()+") values ("+values.deleteCharAt(0).toString()+")";
			pstmt = conn.prepareStatement(sql); // 创建PreparedStatement对象
			int i = 1;
			// 赋值
			for(String key :bbsum.keySet()){
				pstmt.setObject(i,bbsum.get(key)) ;	
				i++;
			}
			pstmt.execute(); // 执行操作
			conn.commit(); // 提交事务;
			JDBCUtils.close(null, pstmt, conn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Transactional
	public boolean updateCustomer(CustomerInfo customerInfo) throws Exception {

		this.daoSupport.update("DT_CustomerInfo", customerInfo, "id='"
				+ customerInfo.getId() + "'");
		return true;
	}

	@Transactional
	public boolean updateCustomerByCode(CustomerInfo customerInfo)
			throws Exception {

		this.daoSupport.update("DT_CustomerInfo", customerInfo, "code='"
				+ customerInfo.getCode() + "'");
		return true;
	}

	@Override
	public void updateCustomer(String code, String lastLoginTime)
			throws Exception {
		String sql = "update DT_CustomerInfo set lastLoginTime=? where code= ?";
		this.daoSupport.execute(sql, lastLoginTime, code);
	}

	@Override
	public void updateCustomerPwd(String code, String password)
			throws Exception {
		String sql = "update DT_CustomerInfo set userPwd=? where code= ?";
		this.daoSupport.execute(sql, password, code);
	}

	@Override
	public CustomerInfo getCustomerInfoByAutelId(String autelId)
			throws Exception {
		String sql = "select * from DT_CustomerInfo where autelId='" + autelId
				+ "'";
		List<CustomerInfo> list = this.daoSupport.queryForList(sql,
				CustomerInfo.class);
		if (null == list || list.size() == 0) {
			return null;
		} else {
			return list.get(0);
		}

	}

	@Override
	public CustomerInfo getCustomerInfoByAutelIdAndPassword(String autelId,
			String password) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_CustomerInfo where autelId='")
				.append(autelId).append("' and userPwd='").append(password)
				.append("'");
		return this.daoSupport.queryForObject(sql.toString(),
				CustomerInfo.class);
	}

	@Override
	public CustomerInfo getCustomerInfoByAutelIdAndActCode(String autelId,
			String actCode) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_CustomerInfo where autelId='")
				.append(autelId).append("' and actCode='").append(actCode)
				.append("'");
		return this.daoSupport.queryForObject(sql.toString(),
				CustomerInfo.class);
	}

	public CustomerInfo getCustomerInfoBySecondEmailAndActCode(
			String secondEmail, String actCode) {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_CustomerInfo where secondEmail='")
				.append(secondEmail).append("' and actCode='").append(actCode)
				.append("'");

		List<CustomerInfo> list = this.daoSupport.queryForList(sql.toString(),
				CustomerInfo.class);
		if (list == null || list.size() == 0) {
			return null;
		} else {
			return list.get(0);
		}

	}

	/**
	 * 根据code查找用户信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Override
	public CustomerInfo getCustomerByCode(String code) throws Exception {
		String sql = "select * from DT_CustomerInfo where code = ?";

		return (CustomerInfo) this.daoSupport.queryForObject(sql,
				CustomerInfo.class, code);
	}

	/**
	 * 根据code删除用户信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public int delCustomer(String code) throws Exception {
		String sql = "delete from DT_CustomerInfo where code= ?";
		this.daoSupport.execute(sql, code);
		return 0;
	}

	/**
	 * 根据codes批量删除用户信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public int delCustomers(String codes) throws Exception {
		String sql = "delete from DT_CustomerInfo where code in(" + codes + ")";
		this.daoSupport.execute(sql);
		return 0;
	}

	@Override
	public CustomerInfo getCustomerInfoBySerialNo(String serialNo)
			throws Exception {
		StringBuffer querySql = new StringBuffer();
		querySql.append(
				"select a.serialNo,a.regTime,b.autelId,b.code,b.firstName,b.middleName,b.lastName,b.name,b.country,")
				.append(" b.address,b.city,b.company,b.zipCode,f.expirationTime from DT_ProductInfo a,")
				.append(" DT_CustomerInfo b,DT_CustomerProInfo d,DT_SaleContract f ")
				.append(" where a.code = d.proCode and b.code = d.customerCode ")
				.append(" and a.saleContractCode = f.code and a.serialNo='")
				.append(serialNo).append("'");

		return this.daoSupport.queryForObject(querySql.toString(),
				new ParameterizedBeanPropertyRowMapper<CustomerInfo>() {

					@Override
					public CustomerInfo mapRow(ResultSet rs, int arg1)
							throws SQLException {
						CustomerInfo customerInfo = new CustomerInfo();
						customerInfo.setSerialNo(rs.getString("serialNo"));
						customerInfo.setProRegTime(rs.getString("regTime"));
						customerInfo.setAutelId(rs.getString("autelId"));
						customerInfo.setCode(rs.getString("code"));
						customerInfo.setComName(rs.getString("firstName")
								+ rs.getString("middleName")
								+ rs.getString("lastName"));
						customerInfo.setCountry(rs.getString("country"));
						customerInfo.setName(rs.getString("name"));
						customerInfo.setAddress(rs.getString("address"));
						customerInfo.setCity(rs.getString("city"));
						customerInfo.setCompany(rs.getString("company"));
						customerInfo.setZipCode(rs.getString("zipCode"));
						customerInfo.setExpTime(rs.getString("expirationTime"));
						return customerInfo;
					}
				});
	}

	@Override
	public CustomerInfo getCustomerAutelIdBySerialNo(String serialNo)
			throws Exception {
		StringBuffer querySql = new StringBuffer();
		querySql.append("select b.autelId,b.code from DT_ProductInfo a,")
				.append(" DT_CustomerInfo b,DT_CustomerProInfo d ")
				.append(" where a.code = d.proCode and b.code = d.customerCode ")
				.append(" and  a.serialNo='").append(serialNo).append("'");

		return this.daoSupport.queryForObject(querySql.toString(),
				new ParameterizedBeanPropertyRowMapper<CustomerInfo>() {

					@Override
					public CustomerInfo mapRow(ResultSet rs, int arg1)
							throws SQLException {
						CustomerInfo customerInfo = new CustomerInfo();
						customerInfo.setAutelId(rs.getString("autelId"));
						customerInfo.setCode(rs.getString("code"));
						return customerInfo;
					}
				});
	}

	@Override
	public CustomerInfo findCustomerInfoByAutelId(String secondEmail,
			String firstName, String lastName) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_CustomerInfo where secondEmail='")
				.append(secondEmail).append("' and firstName='")
				.append(firstName).append("' and lastName='").append(lastName)
				.append("'");
		return this.daoSupport.queryForObject(sql.toString(),
				CustomerInfo.class);
	}

	@Override
	public List<CustomerInfo> getCustomerInfoListBySecondEmail(
			String secondEmail) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_CustomerInfo where secondEmail='")
				.append(secondEmail).append("'");
		return this.daoSupport.queryForList(sql.toString(), CustomerInfo.class);
	}

	@Override
	public int getCustomerInfoNumByAutelId(String autelId) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select count(id) from DT_CustomerInfo where autelId=?");

		return this.daoSupport.queryForInt(sql.toString(), autelId);
	}

	public List<CustomerInfo> updatePassWordList() throws Exception {
		String sql = "select id,code,userPwd from  DT_CustomerInfo where sourceType=22 and id>294194";
		return this.daoSupport.queryForList(sql.toString(), CustomerInfo.class);
	}

	public CustomerInfo getCustBySerialNo(String serialNo) throws Exception {
		String sql = "select a.* from DT_CustomerInfo a,DT_customerProInfo b ,DT_productInfo c where a.code=b.customerCode and b.procode=c.code and c.serialNo like ? limit 0,1";
		return this.daoSupport.queryForObject(sql, CustomerInfo.class, serialNo
				+ "%");
	}

	public CustomerInfo getCustBySerialNo2(String serialNo) throws Exception {
		String sql = "select a.* from DT_CustomerInfo a,DT_customerProInfo b ,DT_productInfo c where a.code=b.customerCode and b.procode=c.code and c.serialNo = ? limit 0,1";
		return this.daoSupport
				.queryForObject(sql, CustomerInfo.class, serialNo);
	}

	public CustomerInfo getCustBySerialNo3(String serialNo) throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select a.*,c.proDate as proDate,c.regTime as proRegTime,d.warrantyMonth as  warrantyMonth,");
		sql.append(" (select distinct validdate from DT_ProductSoftwareValidStatus where proCode=c.code ) as expTime");
		sql.append(" from DT_CustomerInfo a,DT_customerProInfo b ,DT_productInfo c ,DT_SaleContract d");
		sql.append(" where ");
		sql.append(" a.code=b.customerCode  ");
		sql.append(" and b.procode=c.code  ");
		sql.append(" and c.saleContractCode=d.code ");
		sql.append(" and c.serialNo=? limit 0,1");
		return this.daoSupport.queryForObject(sql.toString(),
				CustomerInfo.class, serialNo);
	}

	public CustomerInfo getCustBySerialNo(String serialNo, String password)
			throws Exception {
		String sql = "select a.* from DT_CustomerInfo a,DT_customerProInfo b ,DT_productInfo c where a.code=b.customerCode and b.procode=c.code and c.serialNo like ? and c.regPwd=?";
		return this.daoSupport.queryForObject(sql, CustomerInfo.class, serialNo
				+ "%", password);
	}
	public CustomerInfo getCustBySerialNo1(String serialNo, String password)
	throws Exception {
		StringBuffer querySql = new StringBuffer();
		querySql.append("select a.serialNo,a.regTime,b.autelId,b.code,b.firstName,b.middleName,b.lastName,b.name,b.country,")
		         .append(" b.address,b.city,b.company,b.zipCode,b.languageCode,f.expirationTime expTime, b.mobilePhone,b.mobilePhoneCc,b.mobilePhoneAc from DT_ProductInfo a,")
		         .append(" DT_CustomerInfo b,DT_CustomerProInfo d,DT_SaleContract f ")
		         .append(" where a.code = d.proCode and b.code = d.customerCode ")
		         .append(" and a.saleContractCode = f.code and a.serialNo ='").append(serialNo).append("'")
		         .append(" and a.regPwd='").append(password).append("'");		
		
		return this.daoSupport.queryForObject(querySql.toString(), CustomerInfo.class);
	}

	@Override
	public void updateBbsUsername(String code, String username)
			throws Exception {
		String sql = "update tbl_bbs_user set username=? where code= ?";
		this.daoSupport.execute(sql, username, code);
		Connection conn =JDBCUtils.getConnection(driverClass, jdbcUrl, user, password);
		PreparedStatement pstmt = null;
		conn.setAutoCommit(false); // 设置手动提交事务
		pstmt = conn.prepareStatement(sql); // 创建PreparedStatement对象
		// 赋值
		pstmt.setObject(1,username) ;	
		pstmt.setObject(2,code) ;	
		pstmt.execute(); // 执行操作
		conn.commit(); // 提交事务;
		JDBCUtils.close(null, pstmt, conn);
	}

	@Override
	public List<CustomerInfo> listCustomerInfoByAutelId(String autelId) {
		String sql = "select * from DT_CustomerInfo where autelId like '%" + autelId
		+ "%'";
		List<CustomerInfo> list = this.daoSupport.queryForList(sql,
		CustomerInfo.class);
		return list;
	}

	@Override
	public List<CustomerInfo> listCustomerInfoByAutelName(String name) {
		String sql = "select * from DT_CustomerInfo where name like '%" + name
		+ "%'";
		List<CustomerInfo> list = this.daoSupport.queryForList(sql,
		CustomerInfo.class);
		return list;
	}

	public CustomerInfo getByFacebookId(String id) {
		String sql = "select * from dt_customerinfo where facebookId=" + id;
		return this.daoSupport.queryForObject(sql.toString(), CustomerInfo.class);
	}

}
