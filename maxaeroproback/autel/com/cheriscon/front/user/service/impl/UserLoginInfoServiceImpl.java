package com.cheriscon.front.user.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.UserLoginInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.user.service.IUserLoginInfoService;


/**
 * @remark 用户登录信息service实现
 * @author yangpinggui
 * @date 2013-01-07
 * 
 */
@Service
public class UserLoginInfoServiceImpl extends BaseSupport<UserLoginInfo> implements IUserLoginInfoService
{

	@Transactional
	public boolean addUserLoginInfo(UserLoginInfo userLoginInfo)
			throws Exception
	{
		userLoginInfo.setCode(DBUtils.generateCode(DBConstant.USERLOGININFO_CODE));
		this.daoSupport.insert("DT_UserLoginInfo", userLoginInfo);
		return true;
	}

	@Override
	public List<UserLoginInfo> queryUserLoginInfoByCode(String usercode) {
		// 根据用户code查询用户登录信息
		StringBuffer sql = new StringBuffer();
		sql.append("select  * from dt_userLogininfo where userCode=");
		sql.append("'");
		sql.append(usercode);
		sql.append("'");
		sql.append(" order by loginTime desc limit 0,2");

		return this.daoSupport.queryForList(sql.toString(),
				UserLoginInfo.class, new Object[]{});
	}
}
