package com.cheriscon.front.user.service;

import java.util.List;

import com.cheriscon.common.model.CustomerInfo;


/**
 * 用户信息业务逻辑接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-11
 */
public interface ICustomerInfoService 
{
	
	/**
	 * 添加客户信息
	 * @param customerInfo
	 * @throws Exception
	 */
	public boolean addCustomer(CustomerInfo customerInfo) throws Exception ;
	
	
	/**
	 * 更新用户信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public boolean updateCustomer(CustomerInfo customerInfo) throws Exception;
	
	
	/**
	 * 更新用户信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public boolean updateCustomerByCode(CustomerInfo customerInfo) throws Exception;
	
	/**
	 * 更新用户信息对象
	 * @param code
	 * param lastLoginTime
	 * @return
	 * @throws Exception
	 */
	public void updateCustomer(String code,String lastLoginTime) throws Exception;
	
	/**
	 * 更新用户信息对象
	 * @param code
	 * param lastLoginTime
	 * @return
	 * @throws Exception
	 */
	public void updateCustomerPwd(String code,String password) throws Exception;
	
	
	/**
	 * 根据用户账号查询用户信息对象
	 * @param autelId
	 * @return
	 */
	public CustomerInfo getCustomerInfoByAutelId(String autelId)throws Exception;
	
	/**
	 * 根据用户账号查询用户数量
	 * @param autelId
	 * @return
	 */
	public int getCustomerInfoNumByAutelId(String autelId)throws Exception;
	
	/**
	 * 根据用户输入账号、密码查询用户信息对象
	 * @param autelId
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public CustomerInfo getCustomerInfoByAutelIdAndPassword(String autelId,String password)
			throws Exception;
	
	/**
	 * 根据用户输入账号、激活码查询用户信息对象
	 * @param autelId
	 * @param actCode
	 * @return
	 * @throws Exception
	 */
	public CustomerInfo getCustomerInfoByAutelIdAndActCode(String autelId,String actCode) 
			throws Exception;
	
	/**
	 * 根据用户输入账号、激活码查询用户信息对象
	 * @param secondEmail
	 * @param actCode
	 * @return
	 * @throws Exception
	 */
	public CustomerInfo getCustomerInfoBySecondEmailAndActCode(String secondEmail,String actCode) 
			throws Exception;


	/**
	 * 根据code查找用户信息
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public CustomerInfo getCustomerByCode(String code) throws Exception;

	/**
	 * 根据code删除用户信息
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public int delCustomer(String code) throws Exception;

	/**
	 * 根据codes批量删除用户信息
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public int delCustomers(String codes) throws Exception;
	
	/**
	 * 根据产品序列号查询该产品用户使用信息（包括用户基本信息、设备信息）
	 * @param serialNo
	 * @return
	 * @throws Exception
	 */
	public CustomerInfo getCustomerInfoBySerialNo(String serialNo)throws Exception;
	
	/**
	 * 根据产品序列号查询该产品使用用户的账号
	 * @param serialNo
	 * @return
	 * @throws Exception
	 */
	public CustomerInfo getCustomerAutelIdBySerialNo(String serialNo)throws Exception;
	
	/**
	 * 根据用户第二邮箱、firstName、lastName找回用户账号
	 * @param secondEmail
	 * @param firstName
	 * @param lastName
	 * @return
	 */
	public CustomerInfo findCustomerInfoByAutelId(String secondEmail,String firstName,String lastName)throws Exception;
	
	/**
	 * 根据用户第二邮箱查找用户信息集合
	 * @param secondEmail
	 * @return
	 */
	public List<CustomerInfo> getCustomerInfoListBySecondEmail(String secondEmail)throws Exception;
	
	/**
	 * 后台批量加密更新用户密码
	 * @param secondEmail
	 * @return
	 */
	public List<CustomerInfo> updatePassWordList( )throws Exception;
	
	public CustomerInfo getCustBySerialNo(String serialNo) throws Exception;
	
	public CustomerInfo getCustBySerialNo2(String serialNo) throws Exception;
	
	public CustomerInfo getCustBySerialNo3(String serialNo) throws Exception;
	
	public CustomerInfo getCustBySerialNo(String serialNo,String password) throws Exception;
	public CustomerInfo getCustBySerialNo1(String serialNo, String password) throws Exception;
	
	/**
	 * 更新论坛名称
	 * @param code
	 * @return
	 * @throws Exception
	 * @author pengdongan
	 */
	public void updateBbsUsername(String code, String username) throws Exception;
	
	public List<CustomerInfo> listCustomerInfoByAutelId(String autelId);
	
	public List<CustomerInfo> listCustomerInfoByAutelName(String name);
	
	public CustomerInfo getByFacebookId(String id);
	
}
