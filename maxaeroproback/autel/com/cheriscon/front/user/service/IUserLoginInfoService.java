package com.cheriscon.front.user.service;

import java.util.List;

import com.cheriscon.common.model.UserLoginInfo;

/**
 * 用户登录信息业务逻辑接口类
 * 
 * @author yangpinggui
 * @version 创建时间：2013-1-23
 */
public interface IUserLoginInfoService {
	/**
	 * 增加用户登录信息
	 * 
	 * @param userLoginInfo
	 * @throws Exception
	 */
	public boolean addUserLoginInfo(UserLoginInfo userLoginInfo)
			throws Exception;

	/**
	 * 查询用户登录信息
	 * 
	 * @param usercode
	 *            用户code
	 * @return
	 */
	public List<UserLoginInfo> queryUserLoginInfoByCode(String usercode)
			throws Exception;
}
