package com.cheriscon.front.user.service;

import java.util.List;

import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.common.model.CustomerInfo;

public interface IChangeAutelService {

	/**
	 * 添加客户更改AutelID信息
	 * @param customerInfo
	 * @throws Exception
	 */
	public boolean addCustomerChangeAutel(CustomerChangeAutel changeAutel) throws Exception ;
	
	/**
	 * 根据CustomerCode 查询更改信息
	 * @param customerInfo
	 * @throws Exception
	 */
	public  CustomerChangeAutel getCustomerChangeAutel(String customerCode) throws Exception ;
	
	/**
	 * 老用户更换autelID，根据用户账号查询用户数量
	 * @param autelId
	 * @return
	 */
	public int getCustomerInfoNumByIdAndSrouce(String autelId)throws Exception;
	
	/**
	 * 更新用户信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public boolean updateCustomerChange(CustomerChangeAutel changeAutel) throws Exception;

}
