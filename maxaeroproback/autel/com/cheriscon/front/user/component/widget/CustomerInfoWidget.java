package com.cheriscon.front.user.component.widget;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.service.IQuestionDescService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.QuestionDesc;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.UserLoginInfo;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.IChangeAutelService;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.user.service.IUserLoginInfoService;
import com.cheriscon.front.user.util.CookieUtils;
import com.cheriscon.front.user.util.RequestUtils;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;

/**
 * 用户信息控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-10
 */
@Component("customerInfoWidget")
public class CustomerInfoWidget extends RequestParamWidget
{

	public static final String AUTH_KEY_MEMBER = "auth_key_member";
	public static final String IP = "ip";
	public static final String LOGINSTATUS = "loginStatus";
	public static final String LOGINOUT = "loginout";
	public static final String USERTYPE = "userType";
	
	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private ICustomerInfoService customerInfoService;
	
	@Resource
	private ILanguageService languageService;
	
	@Resource
	private IRegProductService regProductService;
	
	@Resource
	private IQuestionDescService questionDescService;
	
	@Resource
	private ISystemErrorLogService systemErrorService;
	
	@Resource
	private ISealerInfoService sealerInfoService;
	
	@Resource
	private IChangeAutelService changeAutelService;
	
	@Resource
	private ISaleContractService saleContractService;
	
	@Resource
	private ISmtpManager smtpManager;

	@Resource
	private EmailProducer emailProducer;
	
	@Resource
	private IEmailTemplateService emailTemplateService; 
	@Resource
	private IProductSoftwareValidStatusService validService;
	@Resource
	private IUserLoginInfoService userLoginInfoService;
	
	@Resource
	private IMyAccountService service;   
	
	@Resource
	private IProductContractLogService productContractService;
	
	@Resource
	private IProductInfoService productInfoService;
	
	@Override
	protected void display(Map<String, String> params)
	{
		
		String operationType = params.get("operationType");
		String loginToType = params.get("loginToType");
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		HttpServletResponse response = ThreadContextHolder.getHttpResponse();
		this.putData("loginToType", loginToType);
		if(!StringUtil.isEmpty(loginToType)&&"0".equals(loginToType)){
			loginOut(request);
		}
		try
		{
			
			if(!StringUtil.isEmpty(operationType))
			{
				
					if(Integer.parseInt(operationType) == 1)//进入用户注册页面
					{
						goRegCtstomerPage(1,request);
					}
					else if(Integer.parseInt(operationType) == 2)//用户注册提交操作
					{
						regCustonerInfo(request);
					}
					else if(Integer.parseInt(operationType) == 3)//进入用户登录页面
					{
						clearCookie(request);
					}
					else if(Integer.parseInt(operationType) == 4)//用户登录提交操作
					{
						loginCustomerInfo(request);
					}
					else if(Integer.parseInt(operationType) == 5)//进入个人信息修改页面
					{
						goUpdateCustomerInfo(request);
					}
					else if(Integer.parseInt(operationType) == 7)//进入个人修改密码页面
					{
						this.putData("id",((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getId());
						this.putData("autelId",((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getAutelId());
					}
					else if(Integer.parseInt(operationType) == 9)//激活个人用户
					{
						activeCustomerInfo(params,request);
					}
					else if(Integer.parseInt(operationType) == 10)//进入找回密码页面
					{
						this.putData("autelId", params.get("autelId"));
					}
					else if(Integer.parseInt(operationType) == 11)//找回密码操作
					{
						this.putData("autelId", params.get("autelId"));
					}
					else if(Integer.parseInt(operationType) == 12)//进入找回密码选择方式页面（通过发送邮件或是回答问题）
					{
						
					}
					else if(Integer.parseInt(operationType) == 13)//直接发送邮件找回密码处理操作
					{
						//需要发送邮件处理
						selectQuestionForgotPasswordReslut(params,request);
					}
					else if(Integer.parseInt(operationType) == 14)//进入回答问题找回密码页面
					{
						selectQuestionForgotPassword(params);
					}
					else if(Integer.parseInt(operationType) == 15)//回答问题找回密码处理操作
					{
						selectQuestionForgotPasswordReslut(params,request);
					}
					else if(Integer.parseInt(operationType) == 16)//进入重置个人密码页面
					{
						this.putData("autelId", params.get("autelId"));
						this.putData("actCode",params.get("actCode"));
					}
					else if(Integer.parseInt(operationType) == 17)//个人密码重置操作
					{
						reSetPassword(params,request);
					}
					else if(Integer.parseInt(operationType) == 18)//用户退出
					{
						loginOut(request);
					}
					else if(Integer.parseInt(operationType) == 19)//第二邮箱找回用户账号操作
					{
						findAutelId(params,request);
					}
					else if(Integer.parseInt(operationType) == 20)//第二邮箱找回用户账号激活链接操作
					{
						activeSecondEmail(params, request);
					}
					else if(Integer.parseInt(operationType) == 21)//进入更换autelID页面
					{
						this.putData("autelId", params.get("autelId"));
					}
					else if(Integer.parseInt(operationType) == 22)//更换autelID
					{
						changeAutelID(params, request);
					}
					else if(Integer.parseInt(operationType) == 23)//更换autelID成功页面
					{
						CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						CookieUtils.cancleCookie1(request, response, IP, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						CookieUtils.cancleCookie1(request, response, LOGINSTATUS, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						CookieUtils.cancleCookie1(request, response, USERTYPE, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						CookieUtils.cancleCookie1(request, response, "language", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						CookieUtils.cancleCookie1(request, response, "autelId", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						CookieUtils.cancleCookie1(request, response, "custCountry", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						CookieUtils.cancleCookie1(request, response, "tokenKey" , FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						CookieUtils.addCookie1(request, response, LOGINOUT , "1", 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
						request.getSession().removeAttribute("language");
						SessionUtil.removeLoginUserInfo(request);
					}
					else if(Integer.parseInt(operationType) == 24)//更改autelID激活操作
					{
						activeCustomerInfoNew(params,request);
					}
					
					else if(Integer.parseInt(operationType) == 25)//激活客户账户，同时跳转到修改密码界面
					{
						activeCustomerInfoNew2(params,request);
					}
					else if(Integer.parseInt(operationType) == 26)//重新发送激活邮件
					{
						ResendEmail(params,request);
					}
					else if(Integer.parseInt(operationType) == 27)//选择国家
					{
						
					}
		}else{
			boolean flag = judgeIsLogin(request, response);
			boolean changeAutelFlag=false;
			if(flag){
				String userType =getUserTypeFromCookie(request);
				if (SessionUtil.getLoginUserInfo(request)!=null) {
					Object obj=SessionUtil.getLoginUserInfo(request);
					if(obj.getClass().equals(CustomerInfo.class)){
					CustomerInfo customerInfo =(CustomerInfo)SessionUtil.getLoginUserInfo(request);
					//SessionUtil.setLoginUserInfo(request, customerInfo);
					//判断用户是否已经注册产品
					int proCount=service.queryProductCount(customerInfo.getCode());
					request.getSession().setAttribute("proCount", proCount);
					//判断当前用户是否是合法的email账户
					String checkemail = "^([a-z0-9A-Z]+[-_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
					Pattern pattern = Pattern.compile(checkemail);
					Matcher matcher = pattern.matcher(customerInfo.getAutelId());
				//	changeAutelFlag=matcher.matches();
					changeAutelFlag=true;
					request.getSession().setAttribute("updateFlag", changeAutelFlag);
					String requestUrl="myAccount.html?userType="+userType;
					if(changeAutelFlag!=true){
						requestUrl="updateAutelID.html";
					}else{
						if (proCount>0) {
							requestUrl="myAccount.html?userType="+userType;
						}else{
							requestUrl="regProduct.html";
						}
					}
					this.putData("customer", customerInfo);
					response.sendRedirect(requestUrl);
					}else{
						String requestUrl="myDistributorAccount-1-1.html?userType="+userType;
						SealerInfo sealerInfo =(SealerInfo)SessionUtil.getLoginUserInfo(request);
						this.putData("sealerInfo", sealerInfo);
						response.sendRedirect(requestUrl);
					}
					
				}
			}
		}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
//		this.putData("loginToType", 1);
	}
	
	/**
	 * 退出系统处理
	 * @param request
	 */
	protected void loginOut(HttpServletRequest request)
	{
		
		String userType = request.getParameter("userType");
		
		if(!StringUtil.isEmpty(userType))
		{
			if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)
			{
				CustomerInfo customerInfo = (CustomerInfo) SessionUtil.getLoginUserInfo(request);
				
				if(customerInfo == null)
				{
					return;
				}
				
				try 
				{
					customerInfoService.updateCustomer(customerInfo.getCode(),DateUtil.toString(new Date(), 
											FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else if(Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)
			{
				SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
				
				if(sealerInfo == null)
				{
					return;
				}
				
				try 
				{
					sealerInfoService.updateSealer(sealerInfo.getCode(), DateUtil.toString(new Date(), 
										FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		
		//除去cookie
		HttpServletResponse response  = ThreadContextHolder.getHttpResponse();
		CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, IP, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, LOGINSTATUS, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, USERTYPE, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, "language", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, "custCountry", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, "tokenKey" , FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.addCookie1(request, response, LOGINOUT , "1", 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		
		//除去企业网站cookie
		/*CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, "p.auteltech.com");
		CookieUtils.cancleCookie1(request, response, "custCountry", "p.auteltech.com");
		CookieUtils.cancleCookie1(request, response, "tokenKey" , "p.auteltech.com");*/
		
		request.getSession().removeAttribute("language");
		SessionUtil.removeLoginUserInfo(request);
		
	}
	
	
	protected void clearCookie(HttpServletRequest request){
		       //除去cookie
				HttpServletResponse response  = ThreadContextHolder.getHttpResponse();
				CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, IP, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, LOGINSTATUS, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, USERTYPE, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "language", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "custCountry", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "tokenKey" , FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.addCookie1(request, response, LOGINOUT , "1", 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				request.getSession().removeAttribute("language");
				request.getSession().removeAttribute("userInfo");
				//除去企业网站cookie
/*				CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, "p.auteltech.com");
				CookieUtils.cancleCookie1(request, response, "custCountry", "p.auteltech.com");
				CookieUtils.cancleCookie1(request, response, "tokenKey" , "p.auteltech.com");*/
	}
	
	/**
	 * 进入注册用户页面
	 */
	protected void goRegCtstomerPage(int operationType,HttpServletRequest request)
	{
		setDataList(operationType,request,null);
	}
	
	protected void setDataList(int operationType,HttpServletRequest request,String autelId)
	{
		Language language = null;
		
		if(operationType == 1)
		{
			Language languageInfo =(Language)request.getSession().getAttribute("languageInfo");
			Locale local=null;
			if(languageInfo!=null){
				local=new Locale(languageInfo.getLanguageCode(),languageInfo.getCountryCode());
			}else{
				local=CopContext.getDefaultLocal();
			}
			language = languageService.getByLocale(local);
			this.putData("language",language.getLanguageCode());
		}
		else 
		{
			try 
			{
				CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
				
				if(customerInfo !=null)
				{
					language = languageService.getByCode(customerInfo.getLanguageCode());
				}
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
		}
		
		
		List<QuestionDesc> questionInfoList = new ArrayList<QuestionDesc>();
		
		if(language != null)
		{
			try 
			{
				questionInfoList = questionDescService.listQuestionDescByLanguageCode(language.getCode());
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		this.putData("languageList", languageService.queryLanguage());
		this.putData("questionInfoList",questionInfoList);
		/*this.putData("languageInfo",language);*/
	}
	/**
	 * 注册用户操作
	 */
	@SuppressWarnings("unchecked")
	protected void regCustonerInfo(HttpServletRequest request) throws Exception
	{
		String result="0";
		String checkEmail="^([a-z0-9A-Z]+[-_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
		Pattern pattern = Pattern.compile(checkEmail);
		Matcher matcher = pattern.matcher(request.getParameter("autelId"));
		String autelID=request.getParameter("autelId");
//		if(!matcher.matches()){
//			result="1";
//		}
		try {
			CustomerInfo temp = customerInfoService.getCustomerInfoByAutelId(autelID);
			if(temp!=null){
				result="2";
			}
		} catch (Exception e1) {
			result="9";
			e1.printStackTrace();
		}
		
		if(result=="0"){
			CustomerInfo customerInfo = new CustomerInfo();
			customerInfo.setAutelId(autelID);
			customerInfo.setFirstName(request.getParameter("firstName"));
			//customerInfo.setMiddleName(request.getParameter("middleName"));
			customerInfo.setLastName(request.getParameter("lastName"));
			customerInfo.setSecondEmail(request.getParameter("secondEmail"));
			customerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(request.getParameter("userPwd"),null));
			//customerInfo.setPhone(request.getParameter("phone"));
			customerInfo.setRegTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
			customerInfo.setLanguageCode(request.getParameter("languageCode"));
			customerInfo.setAddress(request.getParameter("address"));
			customerInfo.setCountry(request.getParameter("country"));
			customerInfo.setCompany(request.getParameter("company"));
			customerInfo.setZipCode(request.getParameter("zipCode"));
//			customerInfo.setQuestionCode(request.getParameter("questionCode"));
//			customerInfo.setAnswer(request.getParameter("answer"));
			customerInfo.setComUsername(request.getParameter("comUsername"));
			customerInfo.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
			customerInfo.setSecondActState(FrontConstant.USER_STATE_NOT_ACTIVE);
			customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
			customerInfo.setLastLoginTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
			customerInfo.setCity(request.getParameter("city"));
			customerInfo.setProvince(request.getParameter("province"));
			
			//customerInfo.setName(request.getParameter("firstName")+request.getParameter("middleName")+request.getParameter("lastName"));
			customerInfo.setName(request.getParameter("firstName")+" "+request.getParameter("lastName"));
			customerInfo.setSourceType(FrontConstant.CUSTOMER_SOURCE_TYPE_PRO);
			if(!StringUtil.isEmpty(request.getParameter("isAllowSendEmail")))
			{
				customerInfo.setIsAllowSendEmail(Integer.parseInt(request.getParameter("isAllowSendEmail")));
			}
	
			//设置激活时间点
			customerInfo.setSendActiveTime(DateUtil.toString(new Date(), 
											  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
			
			//默认给个第二邮箱激活时间点
			customerInfo.setSendSecondEmailActiveTime(DateUtil.toString(new Date(), 
											  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
			
			try{
				customerInfoService.addCustomer(customerInfo);
			}catch(Exception ex){
				result="9";
				ex.printStackTrace();
				String errorMsg="";
				StackTraceElement[] st = ex.getStackTrace();
				for (StackTraceElement stackTraceElement : st) {
				String exclass = stackTraceElement.getClassName();
				String method = stackTraceElement.getMethodName();
				
				if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
					
					errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
							+ method + "时在第" + stackTraceElement.getLineNumber()
							+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
					System.out.println(errorMsg);
					systemErrorService.saveLog("用户注册提交保存信息",errorMsg);
				}
			}
			}
				try
				{
					Smtp smtp = smtpManager.getCurrentSmtp( );
					
					EmailModel emailModel = new EmailModel();
					emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
					emailModel.getData().put("password", smtp.getPassword());
					emailModel.getData().put("host", smtp.getHost());
					emailModel.getData().put("port", smtp.getPort());
					
					String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
					String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
									 + customerInfo.getAutelId() + "&actCode="+customerInfo.getActCode()+"&operationType=9";
					
					String userName="";
					if(customerInfo!=null && customerInfo.getName()!=null){
					  userName=customerInfo.getName();
					}
					
					emailModel.getData().put("autelId", customerInfo.getAutelId());
					emailModel.getData().put("userName",userName);
					emailModel.getData().put("activeUrl", activeUrl);
					
					Language language = languageService.getByCode(customerInfo.getLanguageCode());
					
					if(language != null)
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
								language.getLanguageCode(),language.getCountryCode()));
					}
					else 
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
					}
					
					emailModel.setTo(customerInfo.getAutelId());
					EmailTemplate template = emailTemplateService.
							getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REGIST,customerInfo.getLanguageCode());
					
					if(template != null)
					{
						emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
					}
					
					emailProducer.send(emailModel);
					String webSite = "http://www."+customerInfo.getAutelId().substring(customerInfo.getAutelId().indexOf("@") + 1, customerInfo.getAutelId().length());
					this.putData("webSite",webSite);
			} 
			catch (Exception ex2) 
			{
				result="9";
				ex2.printStackTrace();
				String errorMsg2="";
				StackTraceElement[] st2 = ex2.getStackTrace();
				for (StackTraceElement stackTraceElement : st2) {
				String exclass = stackTraceElement.getClassName();
				String method = stackTraceElement.getMethodName();
				
				if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
					
					errorMsg2=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
							+ method + "时在第" + stackTraceElement.getLineNumber()
							+ "行代码处发生异常!异常类型:" + ex2.getClass().getName();
					System.out.println(errorMsg2);
					systemErrorService.saveLog("用户注册提交发送邮件",errorMsg2);
				}
				}
				
			}
			}
		this.putData("result",result);
	}
	
	/**
	 * 登录提交操作
	 * @param request
	 */
	protected void loginCustomerInfo(HttpServletRequest request) throws Exception
	{
		String userType ="0";
		String autelId = request.getParameter("autelId");
		String password = request.getParameter("password");
		String loginToType = request.getParameter("loginToType");
		
		String serverAddress = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
		
		//password = DesModule.getInstance().encrypt(password);
		password = Md5PwdEncoder.getInstance().encodePassword(password, null);
		String userCode="";
		String requestUrl = "";
		Language language = null;
		
		HttpServletResponse response  = ThreadContextHolder.getHttpResponse();
		CustomerInfo customerInfo=null;
		SealerInfo sealerInfo=null;
		try {
			customerInfo = customerInfoService.getCustomerInfoByAutelIdAndPassword(autelId, password);
			sealerInfo = sealerInfoService.getSealerInfoByAutelIdAndPassword(autelId, password);
		    if(customerInfo==null && sealerInfo!=null){
		    	userType="2" ;
		    }
		    if(customerInfo!=null && sealerInfo==null){
		    	userType="1" ;
		    }
		} catch (Exception ex) {
			ex.printStackTrace();
			userType=null;
			requestUrl="common_error.html";
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
				System.out.println(errorMsg);
			}
			}
			systemErrorService.saveLog("用户登录提交",errorMsg);
		}
		boolean flag=false;
		int proCount=0;
		if(!StringUtil.isEmpty(userType));
		{
			if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)
			{       try{
				    
				
					userCode = customerInfo.getCode();
					language = languageService.getByCode(customerInfo.getLanguageCode());
					
					
					SessionUtil.setLoginUserInfo(request, customerInfo);
					this.putData("customer", customerInfo);
					//用户注册产品数量
					proCount=service.queryProductCount(customerInfo.getCode());
					request.getSession().setAttribute("proCount", proCount);
					
					//获取到用户信息，将该用户ID和ip放入cookie中，在support端验证该信息实现单点登录
					String ip = RequestUtils.getIpAddr(request);
					String country=customerInfo.getCountry()==null?"":customerInfo.getCountry();
					request.getSession().setAttribute(AUTH_KEY_MEMBER, customerInfo.getAutelId());
					CookieUtils.addCookie1(request, response, AUTH_KEY_MEMBER, URLEncoder.encode(customerInfo.getAutelId(),"utf-8"), 3600,FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, LOGINSTATUS , "1", 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, IP , ip, 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, "language" , language.getLanguageCode()+"_"+language.getCountryCode(), 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, USERTYPE , userType, 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, "custCountry" , URLEncoder.encode(country, "utf-8"), 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, "tokenKey" , customerInfo.getUserPwd(), 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.cancleCookie1(request, response, LOGINOUT, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					
					//企业网站cookie
/*					CookieUtils.addCookie1(request, response, AUTH_KEY_MEMBER, URLEncoder.encode(customerInfo.getAutelId(),"utf-8"), 3600, "p.auteltech.com");
					CookieUtils.addCookie1(request, response, "custCountry", URLEncoder.encode(country, "utf-8"), 3600, "p.auteltech.com");
					CookieUtils.addCookie1(request, response, "tokenKey" , customerInfo.getUserPwd(), 3600, "p.auteltech.com");*/
					//putAutelIdtoCms(request,customerInfo.getAutelId());
					//判断当前用户是否是合法的email账户
					String checkemail = "^([a-z0-9A-Z]+[-_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
					Pattern pattern = Pattern.compile(checkemail);
					//Matcher matcher = pattern.matcher(customerInfo.getAutelId());
					//flag=matcher.matches();
					flag=true;
				if(!StringUtil.isEmpty(loginToType)&&"0".equals(loginToType)){
					requestUrl= serverAddress+"/autelsupport/";
				}else if(!StringUtil.isEmpty(loginToType)&&"3".equals(loginToType)){
					requestUrl= serverAddress+"/autelsupport/";
				}else{
					if(flag!=true){
						requestUrl="updateAutelID.html";
					}else{
						if (proCount>0) {
							requestUrl="myAccount.html?userType="+userType;
						}else{
							requestUrl="regProduct.html";
						}
					}
					
				}
				
				
				// 从登陆IP判断是否是南美区域，如果是，并且DS708区域不是南美区，则调整销售契约
				     URLConnection webUrlRequest;
					 String code="";
					 JSONObject jsonObj2=new JSONObject();
					 JSONObject jsonObj=new JSONObject();
					try{
						webUrlRequest = webUrlRequest("http://ip.taobao.com/service/getIpInfo.php?ip="+ip);
						webUrlRequest.setConnectTimeout(3000);
						webUrlRequest.setReadTimeout(3000);
						String jsonString = jsonString(webUrlRequest.getInputStream());
						jsonObj = JSONObject.fromObject(jsonString);
						code=jsonObj.getString("code");
					}catch(Exception ex){
						ex.printStackTrace();
						String errorMsg="";
						StackTraceElement[] st = ex.getStackTrace();
						for (StackTraceElement stackTraceElement : st) {
						String exclass = stackTraceElement.getClassName();
						String method = stackTraceElement.getMethodName();
						
						if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
							
							errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
									+ method + "时在第" + stackTraceElement.getLineNumber()
									+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
							System.out.println(errorMsg);
							systemErrorService.saveLog("用户登录提交验证Ip地址",errorMsg);
						}
						}
						
					}
				try {
				    if(code.equals("0")){
					    jsonObj2=jsonObj.getJSONObject("data");
					    String ipCountry=jsonObj2.getString("country");
						if(ipCountry.equals("加拿大") || ipCountry.equals("美国")){
						   List<ProductInfo> list=productInfoService.queryProductByUserCode(userCode);
						   if(null!=list && list.size()>0){
							   for(int i=0;i<list.size();i++){
								    ProductInfo temp=list.get(i);
								      SaleContract sale=new SaleContract();
								      sale.setAreaCfgCode("acf_North_America");  //美国区域
								      sale.setLanguageCfgCode(temp.getSaleContractLanguage()); //产品语言
								      sale.setSaleCfgCode("scf_DS708_Basic"); //产品销售配置
								      sale.setSealerCode("sei201305101150570536");  //美国经销商编码
								      sale.setProTypeCode("prt_DS708"); //升级卡类型
									  List<SaleContract> tempList=saleContractService.getContrctList(sale);
									  if(tempList!=null && tempList.size()!=0){
										  SaleContract changeContract=tempList.get(0);
										  
											ProductContractChangeLog log=new ProductContractChangeLog();
											log.setNewContract(changeContract.getCode());
											log.setOldContract(temp.getSaleContractCode());
											log.setOperatorIp(ip);
											log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
											log.setOperatorUser("登陆区域为南美自动更换契约");
											log.setProductSN(temp.getSerialNo());
											//1.保存日志
											productContractService.saveLog(log);
											//更新产品销售契约
											temp.setSaleContractCode(changeContract.getCode());
											productInfoService.updateProductInfo(temp); 
											//删除多余的最小销售单位有效期
											validService.deleteMoreMinCode(temp.getCode());
											
									  }
							   }
						   }
						}
				    }
				} catch (IOException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
					String errorMsg="";
					StackTraceElement[] st = ex.getStackTrace();
					for (StackTraceElement stackTraceElement : st) {
					String exclass = stackTraceElement.getClassName();
					String method = stackTraceElement.getMethodName();
					
					if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
						
						errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
								+ method + "时在第" + stackTraceElement.getLineNumber()
								+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
						System.out.println(errorMsg);
						systemErrorService.saveLog("用户登录提交",errorMsg);
					}
					}
					
				}
				
				
			}catch(Exception ex){
				System.out.println(ex.getMessage());
				userType=null;
				requestUrl="common_error.html";
				ex.printStackTrace();
				String errorMsg="";
				StackTraceElement[] st = ex.getStackTrace();
				for (StackTraceElement stackTraceElement : st) {
				String exclass = stackTraceElement.getClassName();
				String method = stackTraceElement.getMethodName();
				
				if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
					
					errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
							+ method + "时在第" + stackTraceElement.getLineNumber()
							+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
					System.out.println(errorMsg);
					systemErrorService.saveLog("用户登录提交",errorMsg);
				}
				}
				
			}
			}
			else if(Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)
			{
				try 
				{
				 	userCode = sealerInfo.getCode();
				 	language = languageService.getByCode(sealerInfo.getLanguageCode());
				 	SessionUtil.setLoginUserInfo(request, sealerInfo);
				 	
					//经销商cookie
					String ip = RequestUtils.getIpAddr(request);
					String country=sealerInfo.getCountry()==null?"":sealerInfo.getCountry();
					CookieUtils.addCookie1(request, response, AUTH_KEY_MEMBER, sealerInfo.getAutelId(), 3600,FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, LOGINSTATUS , "1", 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, IP , ip, 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, "language" , language.getLanguageCode()+"_"+language.getCountryCode(), 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, USERTYPE , userType, 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, "custCountry" ,  URLEncoder.encode(country, "utf-8"), null, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.addCookie1(request, response, "tokenKey" , sealerInfo.getUserPwd(), null, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
					CookieUtils.cancleCookie1(request, response, LOGINOUT, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				 	
					//企业网站cookie
/*					CookieUtils.addCookie1(request, response, AUTH_KEY_MEMBER, sealerInfo.getAutelId(), 3600, "p.auteltech.com");
					CookieUtils.addCookie1(request, response, "custCountry", country, null, "p.auteltech.com");
					CookieUtils.addCookie1(request, response, "tokenKey" , sealerInfo.getUserPwd(), null, "p.auteltech.com");*/
					
				 	this.putData("sealerInfo",sealerInfo);
				} 
				catch (Exception ex) 
				{
					ex.printStackTrace();
					String errorMsg="";
					StackTraceElement[] st = ex.getStackTrace();
					for (StackTraceElement stackTraceElement : st) {
					String exclass = stackTraceElement.getClassName();
					String method = stackTraceElement.getMethodName();
					
					if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
						
						errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
								+ method + "时在第" + stackTraceElement.getLineNumber()
								+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
						System.out.println(errorMsg);
					}
					}
					systemErrorService.saveLog("用户登录提交",errorMsg);
				}
				
				requestUrl="myDistributorAccount-1-1.html?userType="+userType;
			}
			request.getSession().setAttribute("updateFlag", flag);
			request.getSession().setAttribute("languageInfo",language);
			
			request.getSession().setAttribute(FrontConstant.SESSION_USER_TYPE_FLAG, userType);
			
			
		
			if(userType!=null){
			
				UserLoginInfo userLoginInfo = new UserLoginInfo();
				userLoginInfo.setUserCode(userCode);
				userLoginInfo.setLoginIp(request.getRemoteAddr());
				userLoginInfo.setLoginTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				userLoginInfo.setUserType(Integer.parseInt(userType));
				
				try 
				{
					userLoginInfoService.addUserLoginInfo(userLoginInfo);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			
			
			String forwardUrl = (String) request.getSession().getAttribute("forwardUrl");
			
			/**
			 * 针对URL特殊处理、记住上次url
			 */
			if(proCount>0 && flag==true && !StringUtil.isEmpty(forwardUrl))
			{
				requestUrl = forwardUrl;
			}
			
			try 
			{
				response.sendRedirect(requestUrl);
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		
	}
	
	
	public  URLConnection webUrlRequest(String requestUrl) throws IOException {
		return new URL(requestUrl).openConnection();
	}
	
	public String jsonString(InputStream inputStream) throws IOException {
		// List<String> list = new ArrayList<String>();
		BufferedReader bufferReader = null;
		try {
			bufferReader = new BufferedReader(
					new InputStreamReader(inputStream));

			String readline;
			while ((readline = bufferReader.readLine()) != null) {
				if (readline.indexOf("code") != -1) {
					return readline.toString();
				}
			}
		} finally {
			inputStream.close();
			bufferReader.close();
		}
		return null;

	}

	
	/**
	 * 进入修改个人信息页面
	 * @param request
	 */
	protected void goUpdateCustomerInfo(HttpServletRequest request)
	{
		String autelId = ((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getAutelId();
		try 
		{
			CustomerInfo info=customerInfoService.
					getCustomerInfoByAutelId(autelId);
			if(info==null){
				System.out.println("==manageCustomerInfoPage=="+DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss")+"===autelId==="+autelId);
				loginOut(request);
			}else{
				setDataList(2,request,autelId);
				
				this.putData("customerInfo",info );
			}
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
				System.out.println(errorMsg);
			}
			}
		}
	}


	/**
	 * 重置密码密码操作
	 * @param request
	 */
	protected void reSetPassword(Map<String, String> params,HttpServletRequest request)
	{
		String password = request.getParameter("userPwd");
		try 
		{
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(params.get("autelId"));
			
			if(customerInfo != null)
			{
				//更新actCode
				customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				customerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(password,null));
				customerInfoService.updateCustomer(customerInfo);
			}
		}
		catch (Exception e1) 
		{
			e1.printStackTrace();
		}
		
		
	}
	
	/**
	 * 激活用户
	 * @param params
	 */
	protected void activeCustomerInfo(Map<String, String> params,HttpServletRequest request)
	{
		String autelId = params.get("autelId");
		String actCode = params.get("actCode");
		try 
		{
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			String activeResult = "0";
			
			if(customerInfo != null)
			{
				//判断用户是否激活
				if(customerInfo.getActState() == 1)
				{
					activeResult = "3";
					this.putData("activeResult", activeResult);
					return;
				}
				
				if(!StringUtil.isEmpty(customerInfo.getSendActiveTime()))
				{
					//获取邮箱账户激活时设置的时间
					Date sendActiveTime =  DateUtil.toDate(customerInfo.getSendActiveTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
					
					//获取点击邮箱账户激活链接时的时间
	                Date currentSendActiveTime = DateUtil.toDate(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS),
	                												FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
					
	                //获取时间差
				    long timeDifference = (currentSendActiveTime.getTime() - sendActiveTime.getTime())/1000;

				    //如果12小时内账户未激活、不能进行账户激活操作
	                if(timeDifference > 10800)
	                {
	                	activeResult = "1";
	                }
	                else
	                {
	    				//更新激活状态
	    				customerInfo.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
	    				
	    				//更新actCode
	    				customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
	    				
	    				customerInfoService.updateCustomer(customerInfo);
	    				activeResult = "2";
	                }
				}
				
			}
			this.putData("autelId", autelId);
			this.putData("activeResult", activeResult);
		} 
		catch (Exception e) 
		{
			this.putData("activeResult", "4");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 激活更改AutelID用户
	 * @param params
	 */
	protected void activeCustomerInfoNew(Map<String, String> params,HttpServletRequest request)
	{
		String autelId = params.get("autelId");
		try 
		{
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			
			String result = "0";
			
			if(customerInfo != null)
			{
				CustomerChangeAutel change=changeAutelService.getCustomerChangeAutel(customerInfo.getCode());
				
				if(change==null){
					result = "0";
					this.putData("activeResult", result);
					return;
				}
				
				//判断用户是否激活
				if(change.getActState() == 1)
				{
					result = "3";
					this.putData("activeResult", result);
					return;
				}
				
				
				
			
				if(!StringUtil.isEmpty(customerInfo.getSendActiveTime()))
				{
					//获取邮箱账户激活时设置的时间
					Date sendActiveTime =  DateUtil.toDate(customerInfo.getSendActiveTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
					//获取点击邮箱账户激活链接时的时间
	                Date currentSendActiveTime = DateUtil.toDate(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS),
	                												FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
	                //获取时间差
				    long timeDifference = (currentSendActiveTime.getTime() - sendActiveTime.getTime())/1000;

				    //如果3小时内账户未激活、不能进行账户激活操作
	                if(timeDifference > 10800)
	                {
	                	result = "1";
	                }
	                else
	                {
	                	change.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
	    				changeAutelService.updateCustomerChange(change);
	    				autelId=change.getNewAutelId();
	    				CustomerInfo delCust=customerInfoService.getCustomerInfoByAutelId(autelId);
	    				if(delCust!=null){
	    					customerInfoService.delCustomer(delCust.getCode());
	    				}
	    				//更新actCode
	    				customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
	    				customerInfo.setAutelId(change.getNewAutelId());
	    				customerInfo.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
	    				customerInfoService.updateCustomer(customerInfo);
	    				result = "2";
	                }
				}
				
			}else{
				result="0";
			}
			this.putData("autelId", autelId);
			this.putData("activeResult", result);
		} 
		catch (Exception e) 
		{
			this.putData("activeResult", "0");
			e.printStackTrace();
		}
	}
	
	protected void ResendEmail(Map<String, String> params,HttpServletRequest request){
		
		String autelId = params.get("autelId");
		String result = "0";
		CustomerInfo customerInfo=null;
		try {
			customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			if(customerInfo!=null){
			
			customerInfo.setSendActiveTime(DateUtil.toString(new Date(), 
					  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
			customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
			customerInfoService.updateCustomer(customerInfo);
			
			Smtp smtp = smtpManager.getCurrentSmtp( );
			EmailModel emailModel = new EmailModel();
			emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
			emailModel.getData().put("password", smtp.getPassword());
			emailModel.getData().put("host", smtp.getHost());
			emailModel.getData().put("port", smtp.getPort());
			
			String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
			String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
							 + customerInfo.getAutelId() + "&actCode="+customerInfo.getActCode()+"&operationType=9";
			
			String userName="";
			if(customerInfo!=null && customerInfo.getName()!=null){
			  userName=customerInfo.getName();
			}
			
			emailModel.getData().put("autelId", customerInfo.getAutelId());
			emailModel.getData().put("userName",userName);
			emailModel.getData().put("activeUrl", activeUrl);
			
			Language language = languageService.getByCode(customerInfo.getLanguageCode());
			
			if(language != null)
			{
				emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
						language.getLanguageCode(),language.getCountryCode()));
			}
			else 
			{
				emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
			}
			
			emailModel.setTo(customerInfo.getAutelId());
			EmailTemplate template = emailTemplateService.
					getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REGIST,customerInfo.getLanguageCode());
			
			if(template != null)
			{
				emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
			}
			
			emailProducer.send(emailModel);
			String webSite = "http://www."+customerInfo.getAutelId().substring(customerInfo.getAutelId().indexOf("@") + 1, customerInfo.getAutelId().length());
			this.putData("webSite",webSite);
			result = "2";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.putData("autelId", autelId);
		this.putData("result", result);
	}
	
	
	protected void activeCustomerInfoNew2(Map<String, String> params,HttpServletRequest request)
	{
		String autelId = params.get("autelId");
		String result = "0";
		try 
		{
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);
			if(customerInfo != null)
			{
				CustomerChangeAutel change=changeAutelService.getCustomerChangeAutel(customerInfo.getCode());
				
				if(change==null){
					result = "0";
					this.putData("result", result);
					return;
				}
				
				//判断用户是否激活
				if(change.getActState() == 1)
				{
					result = "3";
					this.putData("result", result);
					return;
				}
				
				
				
			
				if(!StringUtil.isEmpty(customerInfo.getSendActiveTime()))
				{
					//获取邮箱账户激活时设置的时间
					Date sendActiveTime =  DateUtil.toDate(customerInfo.getSendActiveTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
					//获取点击邮箱账户激活链接时的时间
	                Date currentSendActiveTime = DateUtil.toDate(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS),
	                												FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
	                //获取时间差
				    long timeDifference = (currentSendActiveTime.getTime() - sendActiveTime.getTime())/1000;

				    //如果3小时内账户未激活、不能进行账户激活操作
	                if(timeDifference > 10800)
	                {
	                	result = "1";
	                }
	                else
	                {
	                	change.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
	    				changeAutelService.updateCustomerChange(change);
	    				autelId=change.getNewAutelId();
	    				CustomerInfo delCust=customerInfoService.getCustomerInfoByAutelId(autelId);
	    				if(delCust!=null){
	    					customerInfoService.delCustomer(delCust.getCode());
	    				}
	    				//更新actCode
	    				//customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
	    				customerInfo.setAutelId(change.getNewAutelId());
	    				customerInfo.setActState(FrontConstant.USER_STATE_YES_ACTIVE);
	    				customerInfoService.updateCustomer(customerInfo);
	    				result = "2";
	                }
				}
				
			}else{
				result="5";
			}
			
		} 
		catch (Exception e) 
		{   
			result="0";
			e.printStackTrace();
		}
		this.putData("autelId", autelId);
		this.putData("result", result);
	}
	
	
	/**
	 * 找回密码方式选择页面操作（选择回答问题）
	 * @param params
	 */
	protected void selectQuestionForgotPassword(Map<String, String> params)
	{
		String autelId = params.get("autelId");
		
		try 
		{
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(autelId);

			if(customerInfo != null)
			{
				String questionDesc="";
				QuestionDesc desc=questionDescService.getQuestionDesc(customerInfo.getQuestionCode(),
						customerInfo.getLanguageCode());
				if(desc!=null && desc.getQuestionDesc()!=null){
					questionDesc=desc.getQuestionDesc();
				}
				this.putData("autelId",autelId);
				this.putData("questionDesc", questionDesc);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 找回密码方式选择回答问题操作（需要给用户发送邮件）
	 * @param params
	 */
	@SuppressWarnings("unchecked")
	protected void selectQuestionForgotPasswordReslut(Map<String, String> params,HttpServletRequest request)
	{
		String autelId =  params.get("autelId");
		
		try 
		{
			CustomerInfo customer = customerInfoService.getCustomerInfoByAutelId(autelId);
			
			if(customer != null)
			{
				customer.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				//发送密码找回邮件需设置发送重置密码时间
				customer.setSendResetPasswordTime(DateUtil.toString(new Date(), 
												  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				
				customerInfoService.updateCustomer(customer);
				
				Smtp smtp = smtpManager.getCurrentSmtp( );
				
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("autelId", autelId);
				
				String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
				
				String resetPasswordUrl = requestUrl+ "/resetPassword.html?autelId="
				 						+ autelId + "&actCode="+customer.getActCode()+"&operationType=16";
				
				String userName="";
				if(customer!=null && customer.getName()!=null){
				  userName=customer.getName();
				}
				
				emailModel.getData().put("resetPasswordUrl", resetPasswordUrl);
				emailModel.getData().put("userName", userName);
				
				//emailModel.getData().put("userPwd", DesModule.getInstance().decrypt(customer.getUserPwd()));
				emailModel.getData().put("userPwd", Md5PwdEncoder.getInstance().encodePassword(customer.getUserPwd(), null));
				
				Language language = languageService.getByCode(customer.getLanguageCode());
				
				if(language != null)
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.forgotpassword.mailtitle",
							language.getLanguageCode(),language.getCountryCode()));
				}
				else 
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.forgotpassword.mailtitle"));
				}
				
				emailModel.setTo(autelId);
				EmailTemplate template = emailTemplateService.
						getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_PWD,customer.getLanguageCode());
				
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
			}
			
		
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 根据用户输入第二邮箱找回用户autelId
	 * @param params
	 */
	
	protected void findAutelId(Map<String, String> params,HttpServletRequest request)
	{
		String findResult="3";
		String productSN=request.getParameter("productSN");;
		try {
			CustomerInfo info=customerInfoService.getCustomerInfoBySerialNo(productSN);
			if(info==null){
				findResult="1";
			}else{
				if(StringUtil.isEmpty(info.getAutelId())){
					findResult="2";
				}else{
					findResult=info.getAutelId();
				}
			}
		} catch (Exception e) {
			findResult="3";
			e.printStackTrace();
		}
		this.putData("findResult",findResult);
		
		/*String secondEmail =  params.get("secondEmail");
		
		try 
		{
			List<CustomerInfo> customerInfoList = customerInfoService.getCustomerInfoListBySecondEmail(secondEmail);
			
			if(customerInfoList.size() > 0)
			{
				CustomerInfo customerInfo = customerInfoList.get(0);
				
				Smtp smtp = smtpManager.getCurrentSmtp( );
				
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("secondEmail", secondEmail);
				emailModel.getData().put("autelId", customerInfo.getAutelId());
				
				Language language = languageService.getByCode(customerInfo.getLanguageCode());
				
				if(language != null)
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.forgotauterlid.mailtitle",
							language.getLanguageCode(),language.getCountryCode()));
				}
				else 
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.forgotauterlid.mailtitle"));
				}
				
				emailModel.setTo(secondEmail);
				EmailTemplate template = emailTemplateService.
													getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_AUTELID_FIND,
																					customerInfo.getLanguageCode());
				
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
			}
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}*/
		
	}
	
	/**
	 * 激活第二邮箱
	 * @param params
	 */
	protected void activeSecondEmail(Map<String, String> params,HttpServletRequest request)
	{
		String secondEmail = params.get("secondEmail");
		String actCode = params.get("actCode");
		String result = "0";
		try 
		{
			
			CustomerInfo customerInfo = customerInfoService.getCustomerInfoBySecondEmailAndActCode(secondEmail, actCode);
			
			
			
			if(customerInfo != null)
			{
				//判断第二邮箱是否已激活
				if(customerInfo.getSecondActState() == FrontConstant.USER_STATE_YES_ACTIVE)
				{
					result = "3";
					this.putData("result", result);
					return;
				}
				
				//获取第二邮箱账户激活时设置的时间
				Date sendSecondEmailActiveTime =  DateUtil.toDate(customerInfo.getSendSecondEmailActiveTime(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
				
				//获取点击第二邮箱账户激活链接时的时间
                Date currentSendSecondEmailActiveTime = DateUtil.toDate(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS),
                												FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);
                //获取时间差
			    long timeDifference = (currentSendSecondEmailActiveTime.getTime() - sendSecondEmailActiveTime.getTime())/1000;

			    //如果12小时内账户未激活、不能进行账户激活操作
                if(timeDifference > 10800)
                {
                	result = "1";
                }
                else
                {
                	//更新激活状态
    				customerInfo.setSecondActState(FrontConstant.USER_STATE_YES_ACTIVE);
    				
    				//更新actCode
    				customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
    				
    				customerInfoService.updateCustomer(customerInfo);
    				result = "2";
                }
			}else{
				result = "0";
			}

			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			result = "0";
		}
		this.putData("result", result);
	}
	
	
	private boolean judgeIsLogin(HttpServletRequest request,HttpServletResponse response){
		
		String uid = null;
		String ip = "";
		String loginStatus = "";
		
		Cookie []cookies = request.getCookies();
		if(null != cookies)
		{
			for (Cookie cookie : cookies) {
				if(AUTH_KEY_MEMBER.equals(cookie.getName())){
					uid = cookie.getValue();
				}	
				if("loginStatus".equals(cookie.getName())){
					loginStatus = cookie.getValue();
				}	
				if("ip".equals(cookie.getName())){
					ip = cookie.getValue();
				}
			}
		}
		if(null != uid && ip.equals(RequestUtils.getIpAddr(request))){
			return true;
		} else{
			return false;
		}
	}
	
	private String getUserTypeFromCookie(HttpServletRequest request){
		String userType=null;
		Cookie []cookies = request.getCookies();
		if(null != cookies)
		{
			for (Cookie cookie : cookies) {
				if(USERTYPE.equals(cookie.getName())){
					userType = cookie.getValue();
					break;
				}	
			}
		}
		return userType;
	}
	private String getAutelIdFromCookie(HttpServletRequest request){
		String AutelId=null;
		Cookie []cookies = request.getCookies();
		if(null != cookies)
		{
			for (Cookie cookie : cookies) {
				if(AUTH_KEY_MEMBER.equals(cookie.getName())){
					AutelId = cookie.getValue();
					break;
				}	
			}
		}
		return AutelId;
	}
	
	
	@Override
	protected void config(Map<String, String> params)
	{
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		//String str="http://localhost:8080/autelproweb/login.html?forward=customerComplaintInfo-1-1.html%3FuserType%3D2%26operationType%3D1";
		String str = "http://localhost:8080/autelproweb/login.html";
		String [] strArray = str.split("?");
		
		System.out.println(strArray.length);

	}
	
	public void changeAutelID(Map<String, String> params,HttpServletRequest request){
		String oldId=params.get("autelId");
		String productSN=params.get("productSN");
		String productPwd=params.get("productPwd");
		String newAutelId=params.get("newAutelId");
		String regResult="success";
		
		try {
			int cust=changeAutelService.getCustomerInfoNumByIdAndSrouce(newAutelId);
			ProductInfo info = regProductService.queryProduct(productSN);
			if(info==null){
				regResult="notFound";
			}else if(cust!=0){
				regResult="custExist";
			}else{
				if(!info.getRegPwd().equals(productPwd)){
					regResult="passwordError";
				}else{
					 int num=regProductService.checkProduct(oldId,productSN);
					  if(num==0){
						  regResult="productNotForYou";
					  }else{
						  CustomerInfo updateCust=customerInfoService.getCustomerInfoByAutelId(oldId);
						  updateCust.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
						  updateCust.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
						  updateCust.setSendActiveTime(DateUtil.toString(new Date(), 
								  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
						  updateCust.setSendResetPasswordTime(DateUtil.toString(new Date(), 
								  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
						  customerInfoService.updateCustomer(updateCust);
						  
						  CustomerChangeAutel change=changeAutelService.getCustomerChangeAutel(updateCust.getCode());
						  if(change==null){
							  change=new CustomerChangeAutel();
						  }
						  change.setCustomerCode(updateCust.getCode());
						  change.setNewAutelId(newAutelId);
						  change.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
						  if(change.getId()==null){
								changeAutelService.addCustomerChangeAutel(change);
							}else{
								changeAutelService.updateCustomerChange(change);
						    }
						    Smtp smtp = smtpManager.getCurrentSmtp( );
							
							EmailModel emailModel = new EmailModel();
							emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
							emailModel.getData().put("password", smtp.getPassword());
							emailModel.getData().put("host", smtp.getHost());
							emailModel.getData().put("port", smtp.getPort());
							
							String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
							String activeUrl = requestUrl+ "/activeAndresetPwd.html?autelId="
											 + updateCust.getAutelId() + "&actCode="+updateCust.getActCode()+"&operationType=25";
							
							String userName="";
							if(updateCust!=null && updateCust.getName()!=null){
							  userName=updateCust.getName();
							}
							
							emailModel.getData().put("autelId", updateCust.getAutelId());
							emailModel.getData().put("userName", userName);
							emailModel.getData().put("password",  Md5PwdEncoder.getInstance().encodePassword(updateCust.getUserPwd(),null));
							emailModel.getData().put("activeUrl", activeUrl);
							
							Language language = languageService.getByCode(updateCust.getLanguageCode());
							
							if(language != null)
							{
								emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
										language.getLanguageCode(),language.getCountryCode()));
							}
							else 
							{
								emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
							}
							
							emailModel.setTo(newAutelId);
							EmailTemplate template = emailTemplateService.
									getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_AUTELID_CHANGE_ACTIVE,updateCust.getLanguageCode());
							
							if(template != null)
							{
								emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
							}
							
							emailProducer.send(emailModel);
						
						  
					  }
				}
			}
		} catch (Exception e) {
			this.putData("regResult", "");
			e.printStackTrace();
		}
		
		this.putData("regResult", regResult);
	} 
	
	private void putAutelIdtoCms(HttpServletRequest request ,String autelId){
		HttpClient client = new HttpClient();   
		String server = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort() + "/autelcms";
		client.getHostConfiguration().setHost(request.getServerName(),request.getServerPort(),"http");   
		NameValuePair[] values = new NameValuePair[1]; 
		values[0] = new NameValuePair("autelId",autelId);  
		HttpMethod method = getPostMethod(server + "/addAutelKey.jhtml",values); 
		try {
			client.executeMethod(method);
			String response = method.getResponseBodyAsString();
			method.releaseConnection();
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static HttpMethod getPostMethod(String uri,NameValuePair[] parametersBody) {   
		PostMethod post = new PostMethod(uri);   
		post.setRequestBody(parametersBody);   
		return post;   
	}
	
}
