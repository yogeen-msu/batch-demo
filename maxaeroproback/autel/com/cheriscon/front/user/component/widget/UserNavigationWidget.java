package com.cheriscon.front.user.component.widget;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsMinUnitVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;
/**
 * 用户导航
 * @date:2013-4-19
 * @author caozhiyong
 *
 */
@Component("userNavigationWidget")
public class UserNavigationWidget extends AbstractWidget {

	@Resource 
	private IShoppingCartService service;
	
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		
		if (params.get("isRenew")!=null) {
			int softtype=Integer.parseInt(params.get("isRenew"));
			this.putData("isRenew",softtype);
		}
		
		this.putData("navigationPage", params.get("navigationPage"));
		this.putData("proCount",request.getSession().getAttribute("proCount"));
		this.putData("updateFlag",request.getSession().getAttribute("updateFlag"));
		Object object=(Object)SessionUtil.getLoginUserInfo(request);
		
		if ( null != object && object.getClass().equals(CustomerInfo.class)) {
			CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
			
			if (customerInfo!=null) {
				String usercode=customerInfo.getCode();//用户code
				String languageCode=customerInfo.getLanguageCode();//用户首选语言code
				
				List<ShopingCartsProListVO> proListVOs=service.queryShoppingCartByCustomerCode(usercode,languageCode);
				int items=0;
				for (int i = 0; i < proListVOs.size(); i++) {
					for (int j = 0; j < proListVOs.get(i).getMinUnitVOs().size(); j++) {
						
						ShopingCartsMinUnitVO minUnitVO=proListVOs.get(i).getMinUnitVOs().get(j);
						
						MarketPromotionDiscountInfoVO info;
						if (minUnitVO.getIsSoft() == 1) {
							// 查询最小销售单位是否可参与活动
							info = minSaleUnitDetailService.queryDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
											minUnitVO.getType(),
											minUnitVO.getValidDate());
						} else {
							// 查询销售配置是否可参与活动
							info = minSaleUnitDetailService.querySaleCfgDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
											minUnitVO.getType(),
											minUnitVO.getValidDate()); 
						}
						String discountPrice = minUnitVO.getCostPrice();
						minUnitVO.setCostPrice(Float.valueOf(discountPrice).toString());
						if (info != null) {
							// 折后价
							discountPrice = String.valueOf((Float.parseFloat(minUnitVO.getCostPrice())*1000 * Float.parseFloat(String.valueOf(info.getDiscount()))) / 10/1000);
						}
						minUnitVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
						if(minUnitVO.getValidDate() != null){
							Date date = null;
							if(minUnitVO.getValidDate().indexOf("/")>-1){
								 date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy/MM/dd");
								}
								else{
								 date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy-MM-dd");	
								}
							Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
							//当前时间大于有效期时间(已经过期)
							if(currentDate.after(date)){
								minUnitVO.setIsValid(1);
							}
						}
						
						items++;
						proListVOs.get(i).getMinUnitVOs().set(j, minUnitVO);
					}
				}
			
				this.putData("shoppingCart",proListVOs);
				this.putData("items",items);
			}
		
		}
		
	}

	@Override
	protected void config(Map<String, String> params) {
		
	}

}
