package com.cheriscon.front.user.component.widget;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.SaleInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
/**
 * 
 * @date:2013-07-15
 * @author 
 *
 */
@Component("updateAutelWidget")
public class UpdateAutelWidget extends AbstractWidget {

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		Object object=(Object)SessionUtil.getLoginUserInfo(request);
		String autelId="";
		if (object!=null && object.getClass().equals(CustomerInfo.class)) {
			autelId=((CustomerInfo)object).getAutelId();
		}if(object!=null && object.getClass().equals(SaleInfo.class)){
			autelId=((SealerInfo)object).getAutelId();
		}
		this.putData("autelId",autelId);
	}

	@Override
	protected void config(Map<String, String> params) {
		
	}

}
