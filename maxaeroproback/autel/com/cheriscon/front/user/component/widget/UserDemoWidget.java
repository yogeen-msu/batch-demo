package com.cheriscon.front.user.component.widget;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cheriscon.cop.sdk.widget.AbstractWidget;
/**
 * @date:2013-1-4
 * @author sh
 *
 */
@Component("userDemoWidget")
@Scope("prototype")
public class UserDemoWidget extends AbstractWidget {

	@Override
	protected void display(Map<String, String> params) {
		this.putData("data", "2013-1-4");
		System.out.println("userDemoWidget: display +++++++++++++++");
	}

	@Override
	protected void config(Map<String, String> params) {
		
	}

}
