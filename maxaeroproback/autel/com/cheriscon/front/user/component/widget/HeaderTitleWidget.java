package com.cheriscon.front.user.component.widget;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;

/**
 * 
 * @author yangpinggui
 * @version 创建时间：2013-1-10
 */
@Component("headerTitleWidget")
public class HeaderTitleWidget extends RequestParamWidget
{


	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		
		String userType = (String) request.getSession().getAttribute(FrontConstant.SESSION_USER_TYPE_FLAG);
		String autelId = null;
		String country = null;
		
		if(!StringUtil.isEmpty(userType))
		{
			if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)
			{
				CustomerInfo customerInfo = ((CustomerInfo) (SessionUtil.getLoginUserInfo(request)));
				
				if(customerInfo != null)
				{
					autelId = customerInfo.getAutelId();
					country = customerInfo.getCountry();
				}
			}
			else if(Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)
			{
				SealerInfo sealerInfo = ((SealerInfo) (SessionUtil.getLoginUserInfo(request)));
				
				if(sealerInfo != null)
				{
					autelId = sealerInfo.getAutelId();
					country = sealerInfo.getCountry();
				}
			}
		}
		Language language = (Language) CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
		Locale locale = null;
		if (language != null) {
			locale = new Locale(language.getLanguageCode(), language.getCountryCode());
		} else {
			locale = CopContext.getDefaultLocal();
		}
        this.putData("locale",locale);
		
		this.putData("userType", userType);
		this.putData("autelId", autelId);
		this.putData("country",country);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
