package com.cheriscon.front.user.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.content.service.IUserMessageService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.service.IReChargeCardErrorLogService;
import com.cheriscon.backstage.member.service.IReChargeCardSupportService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;
import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.ReChargeCardErrorLog;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.model.ReChargeCardSupport;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.UserMessage;
import com.cheriscon.common.utils.DesModule;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.IChangeAutelService;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.user.util.CookieUtils;
import com.cheriscon.front.user.vo.ReChargeCardVo;
import com.cheriscon.front.user.vo.ReCustomerComplaintInfoVO;
import com.cheriscon.front.user.vo.UserInfoVo;
import com.cheriscon.front.user.vo.UserMessageVo;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;
import com.google.gson.annotations.Until;

/**
 * 用户信息ajax校验处理类
 * 
 * @author yangpinggui 2013-1-16
 * 
 */
@ParentPackage("json-default")
@Namespace("/front/user")
public class UserInfoAction extends WWAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5259338156776303638L;

	public static final String AUTH_KEY_MEMBER = "auth_key_member";
	public static final String IP = "ip";
	public static final String LOGINSTATUS = "loginStatus";
	public static final String LOGINOUT = "loginout";
	public static final String USERTYPE = "userType";
	
	
	private String jsonData;
	private UserInfoVo userInfoVo;
	private CustomerComplaintInfo customerComplaintInfo;
	private ReCustomerComplaintInfoVO reCustomerComplaintInfoVO;
	private String downToolPath;
	private ReChargeCardVo reChargeCardVo;
	private UserMessageVo userMessageVo;
	private String type="";
	private String oldId="";
	private String newAutelId="";
	private ReChargeCardSupport support;
	private CustomerInfo customerInfoEdit;

	public CustomerInfo getCustomerInfoEdit() {
		return customerInfoEdit;
	}

	public void setCustomerInfoEdit(CustomerInfo customerInfoEdit) {
		this.customerInfoEdit = customerInfoEdit;
	}

	public ReChargeCardSupport getSupport() {
		return support;
	}

	public void setSupport(ReChargeCardSupport support) {
		this.support = support;
	}

	public String getOldId() {
		return oldId;
	}

	public String getNewAutelId() {
		return newAutelId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	public void setNewAutelId(String newAutelId) {
		this.newAutelId = newAutelId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Resource
	private ICustomerInfoService customerInfoService;

	@Resource
	private ISealerInfoService sealerInfoService;

	@Resource
	private IReChargeCardTypeService reChargeCardTypeService;

	@Resource
	private IReChargeCardInfoService reChargeCardInfoService;

	@Resource
	private IProductSoftwareValidStatusService productSoftwareValidStatusService;

	@Resource
	private IMessageService messageService;

	@Resource
	private IUserMessageService userMessageService;

	@Resource
	private IReCustomerComplaintInfoService reCustomerComplaintInfoService;

	@Resource
	private IToCustomerChargeService toCustomerChargeService;

	@Resource
	private ISmtpManager smtpManager;

	@Resource
	private EmailProducer emailProducer;

	@Resource
	private IEmailTemplateService emailTemplateService;

	@Resource
	private ILanguageService languageService;
	
	@Resource
	private IChangeAutelService changeService;
	
	@Resource
	private IReChargeCardErrorLogService logService;
	
	@Resource
	private IProductInfoService productInfoService;
	
	@Resource
	private ISaleContractService saleContractService;
	
	@Resource
	private IReChargeCardSupportService supportService;
	
	@Resource
	private IMyAccountService myAccountService;

	@Resource
	private IRegProductService regProductService;
	
	@Resource
	private IProductContractLogService productContractService;
	
	@Resource
	private ISystemErrorLogService systemErrorService;
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public UserInfoVo getUserInfoVo() {
		return userInfoVo;
	}

	public void setUserInfoVo(UserInfoVo userInfoVo) {
		this.userInfoVo = userInfoVo;
	}

	public CustomerComplaintInfo getCustomerComplaintInfo() {
		return customerComplaintInfo;
	}

	public void setCustomerComplaintInfo(
			CustomerComplaintInfo customerComplaintInfo) {
		this.customerComplaintInfo = customerComplaintInfo;
	}

	public ReCustomerComplaintInfoVO getReCustomerComplaintInfoVO() {
		return reCustomerComplaintInfoVO;
	}

	public void setReCustomerComplaintInfoVO(
			ReCustomerComplaintInfoVO reCustomerComplaintInfoVO) {
		this.reCustomerComplaintInfoVO = reCustomerComplaintInfoVO;
	}

	public String getDownToolPath() {
		return downToolPath;
	}

	public void setDownToolPath(String downToolPath) {
		this.downToolPath = downToolPath;
	}

	public ReChargeCardVo getReChargeCardVo() {
		return reChargeCardVo;
	}

	public void setReChargeCardVo(ReChargeCardVo reChargeCardVo) {
		this.reChargeCardVo = reChargeCardVo;
	}

	public UserMessageVo getUserMessageVo() {
		return userMessageVo;
	}

	public void setUserMessageVo(UserMessageVo userMessageVo) {
		this.userMessageVo = userMessageVo;
	}

	/**
	 * 检查登录用户（包括个人用户、经销商用户） checkUserLogin
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "checkUserLogin", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkUserLogin() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		String autelId = userInfoVo.getAutelId();
		String userPwd = userInfoVo.getUserPwd();
		String imageCode = userInfoVo.getImageCode();
		try{
			String code = (String) ThreadContextHolder.getSessionContext()
					.getAttribute(FrontConstant.SESSION_IMAGE_CODE_NAME);
	
			//userPwd = DesModule.getInstance().encrypt(userPwd);
			userPwd = Md5PwdEncoder.getInstance().encodePassword(userPwd, null);
			
			if(code!=null){
				if (!imageCode.toUpperCase().equals(code.toUpperCase())) {
					return returnImageCodeJsonData(map);
				}
			}else{
				return returnImageCodeJsonData(map);
			}
			CustomerInfo customer=customerInfoService.getCustomerInfoByAutelId(autelId);
			SealerInfo sealer=sealerInfoService.getSealerByAutelId(autelId);
			if(customer==null && sealer == null){
				return returnAutelIdJsonData(map);
			}else if(customer != null && sealer == null){
				CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelIdAndPassword(autelId, userPwd);
				if (customerInfo == null) {
					return returnUserPwdJsonData(map);
				} else if (customerInfo.getActState() == FrontConstant.USER_STATE_NOT_ACTIVE) {
					return returnActCodeJsonData(map);
				}
				map.put("userType", "1");
			}else if(customer == null && sealer != null){
				if (sealerInfoService.getSealerInfoByAutelIdAndPassword(autelId,
						userPwd) == null) {
					return returnUserPwdJsonData(map);
				}
				map.put("userType", "2");
			}else{
				map.put("userType", "3");
			}
          
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception ex){
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.front.user.action.UserInfoAction".equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName()+"--autelId="+autelId+""+"-->userPwd="+userPwd+"-->imageCode="+imageCode;
				System.out.println(errorMsg);
				systemErrorService.saveLog("登录验证用户名密码验证码",errorMsg);
			}
			}
			return returnSysErrorJsonData(map);
		}
		return SUCCESS;
	}

	/**
	 * 检查用户账号找回 checkFindAutelId
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "checkFindAutelId", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkFindAutelId() throws Exception {
		try{
			Map<String, String> map = new HashMap<String, String>();
	       
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			String proSerialNo=request.getParameter("proSerialNo");
			String pwd=request.getParameter("proPwd");
			ProductInfo info = regProductService.queryProduct(proSerialNo);
			String result="0";
			if(info!=null){
				int count=myAccountService.queryCustomerByProCode(info.getCode());
				if(count==0){
					result="3";
				 }
			}else{
				result="1";
			}
			map.put("result", result);
//			String secondEmail = userInfoVo.getSecondEmail();
//			String firstName = userInfoVo.getFirstName();
//			String lastName = userInfoVo.getLastName();
//	
//			CustomerInfo customerInfo = customerInfoService
//					.findCustomerInfoByAutelId(secondEmail, firstName, lastName);
//	
//			if (customerInfo == null) {
//				map.put("result", "1");
//			} else {
//				// 判断第二邮箱是否激活
//				if (customerInfo.getSecondActState() == FrontConstant.USER_STATE_NOT_ACTIVE) {
//					map.put("result", "2");
//				} else {
//					map.put("result", "3");
//				}
//	
//			}
	
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return SUCCESS;
	}

	/**
	 * 根据用户密码校验返回结果处理
	 * 
	 * @param map
	 * @return
	 */
	private String returnUserPwdJsonData(Map<String, String> map) {
		map.put("userPwd", "false");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 根据用户密码校验返回结果处理
	 * 
	 * @param map
	 * @return
	 */
	private String returnSysErrorJsonData(Map<String, String> map) {
		map.put("systemError", "false");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	/**
	 * 根据用户账号（邮箱）校验返回结果处理
	 * 
	 * @param map
	 * @return
	 */
	private String returnAutelIdJsonData(Map<String, String> map) {
		map.put("autelId", "false");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 根据验证码校验返回结果处理
	 * 
	 * @param map
	 * @return
	 */
	private String returnImageCodeJsonData(Map<String, String> map) {
		map.put("imageCode", "false");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 根据用户账号、密码查询该用户激活状态校验返回结果处理
	 * 
	 * @param map
	 * @return
	 */
	private String returnActCodeJsonData(Map<String, String> map) {
		map.put("actCode", "false");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 检查用户账号是否存在、及验证码输入是否正确（用户登录） checkUserLogin
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "regCustomerCheck", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String regCustomerCheck() throws Exception {
		
		Map<String, String> map = new HashMap<String, String>();
		try{
			
			if(null != userInfoVo && null !=userInfoVo.getAutelId()){
				String autelId = userInfoVo.getAutelId();
		
				String code = (String) ThreadContextHolder.getSessionContext()
						.getAttribute(FrontConstant.SESSION_IMAGE_CODE_NAME);
				if(code!=null && userInfoVo.getImageCode()!=null){
					if (!userInfoVo.getImageCode().toUpperCase().equals(code.toUpperCase())) {
						return returnImageCodeJsonData(map);
					}
				}else{
					return returnImageCodeJsonData(map);
				}
		
				if (customerInfoService.getCustomerInfoNumByAutelId(autelId) < 1) {
					map.put("autelId", "false");
				} else {
					map.put("autelId", "true");
				}
			}else{
				map.put("autelId", "false1");
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			String errorMsg="";
			map.put("autelId", "SystemError");
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
				String exclass = stackTraceElement.getClassName();
				String method = stackTraceElement.getMethodName();
				
				if("com.cheriscon.front.user.action.UserInfoAction".equals(exclass)){
					
					errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
							+ method + "时在第" + stackTraceElement.getLineNumber()
							+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
					System.out.println(errorMsg);
					systemErrorService.saveLog("用户注册检查用户名",errorMsg);
				}
			}
			
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 检查用户账号是否存在、及验证码输入是否正确（用户登录） checkUserLogin
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "checkAutelIDIsUse", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkAutelIDIsUse() throws Exception {
		try{
			Map<String, String> map = new HashMap<String, String>();
			
			if(null != userInfoVo && null !=userInfoVo.getAutelId()){
				String autelId = userInfoVo.getAutelId();
				if (customerInfoService.getCustomerInfoNumByAutelId(autelId) < 1) {
					map.put("autelId", "false");
				} else {
					map.put("autelId", "true");
				}
			}else{
				map.put("autelId", "false1");
			}
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception ex){
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.front.user.action.UserInfoAction".equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
				System.out.println(errorMsg);
				systemErrorService.saveLog("注册检查用户名",errorMsg);
			}
			}
		
		}
		return SUCCESS;
	}
	

	/**
	 * 检查用户账号是否存在、及验证码输入是否正确（老用户更换Autel ID） checkUserLogin
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "checkAutelIDIsUseNew", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkAutelIDIsUseNew() throws Exception {
		try{
			Map<String, String> map = new HashMap<String, String>();
	
			String autelId = userInfoVo.getAutelId();
	
			if (changeService.getCustomerInfoNumByIdAndSrouce(autelId) < 1) {
				map.put("autelId", "false");
			} else {
				map.put("autelId", "true");
			}
	
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return SUCCESS;
	}

	/**
	 * 检查用户账号是否存在（密码找回） checkAutelIdIsExist
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "checkAutelIdIsExist", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkAutelIdIsExist() throws Exception {
		try{
			Map<String, String> map = new HashMap<String, String>();
	
			String autelId = userInfoVo.getAutelId();
	
			if (customerInfoService.getCustomerInfoByAutelId(autelId) == null) {
				map.put("autelId", "false");
			} else {
				map.put("autelId", "true");
			}
	
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return SUCCESS;
	}

	/**
	 * 获取用户信息
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "getCustomerInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getCustomerInfo(){
		try{
			Map<String, CustomerInfo> map = new HashMap<String, CustomerInfo>();
	
			String autelId = userInfoVo.getAutelId();
			map.put("customerInfo",
					customerInfoService.getCustomerInfoByAutelId(autelId));
	
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();

		}
		return SUCCESS;
	}

	/**
	 * 修改密码操作
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "updateCustomerPassword", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustomerPassword() throws Exception {
		try{
			Map<String, String> map = new HashMap<String, String>();
			String password = userInfoVo.getUserPwd();
			String oldPassword = userInfoVo.getOldPassword();
	
			// 检查旧密码是否正确
			if (customerInfoService.getCustomerInfoByAutelIdAndPassword(
					userInfoVo.getAutelId(),
					Md5PwdEncoder.getInstance().encodePassword(oldPassword,null)) == null) {
				map.put("oldPassword", "false");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
	
			try {
				CustomerInfo customerInfo = new CustomerInfo();
				customerInfo.setId(userInfoVo.getId());
				customerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(password,null));
				customerInfoService.updateCustomer(customerInfo);
				map.put("result", "true");
			} catch (Exception e) {
				map.put("result", "false");
				e.printStackTrace();
			}
	
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return SUCCESS;
	}

	
	@Action(value = "updateCustPwdForSealer", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustPwdForSealer() throws Exception {
		try{
			Map<String, String> map = new HashMap<String, String>();
			String password = userInfoVo.getUserPwd();
			
			try {
				CustomerInfo customerInfo = customerInfoService.getCustomerByCode(userInfoVo.getCode());
				customerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(password,null));
				customerInfoService.updateCustomerByCode(customerInfo);
				map.put("result", "true");
			} catch (Exception e) {
				map.put("result", "false");
				e.printStackTrace();
			}
	
			jsonData = JSONArray.fromObject(map).toString();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return SUCCESS;
	}

	
	/**
	 * 用户个人信息修改New
	 * 
	 * @param request
	 * @author pengdongan
	 */
	@Action(value = "updateCustomerInfoNew", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustomerInfoNew() {
		Map<String, String> map = new HashMap<String, String>();

		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(customerInfoEdit.getCode());

			if (customerInfo != null) {
				
				//修改论坛名字
				if (!StringUtil.isEmpty(customerInfoEdit.getComUsername())) {
					customerInfoService.updateBbsUsername(customerInfoEdit.getCode(), customerInfoEdit.getComUsername());
				}

				
				//获得新的autelId值
				String autelIdNew = customerInfoEdit.getAutelId();

				//获得新的secondEmail值
				String secondEmailNew = customerInfoEdit.getSecondEmail();
				
				if (!StringUtil.isEmpty(autelIdNew)) {
					// 判断第一邮箱是否重复
					if(!customerInfo.getAutelId().equals(autelIdNew)){
						if (changeService.getCustomerInfoNumByIdAndSrouce(autelIdNew) != 0) {
							map.put("result", "3");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}else{
							updateAutelIdNew(customerInfo.getAutelId(),autelIdNew);
						}
					}
	
				}

				if (!StringUtil.isEmpty(secondEmailNew)) {
					// 判断第二邮箱是否重复
					if(customerInfo.getSecondEmail()==null){
						if (customerInfoService.getCustomerInfoListBySecondEmail(secondEmailNew).size() > 0) {
							map.put("result", "3");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}
					}else if(!customerInfo.getSecondEmail().equals(secondEmailNew)){
						if (customerInfoService.getCustomerInfoListBySecondEmail(secondEmailNew).size() > 0) {
							map.put("result", "3");
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}
					}
	
				}

				if(!StringUtil.isEmpty(customerInfoEdit.getUserPwd())){
					customerInfoEdit.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(customerInfoEdit.getUserPwd(),null));
				}
				
				customerInfoEdit.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));

				if(!StringUtil.isEmpty(customerInfoEdit.getFirstName())){
					customerInfoEdit.setName(customerInfoEdit.getFirstName()
//								+ customerInfoEdit.getMiddleName()
							    +" " //应东升（美国）要求，页面显示的姓与名 之间要用  空格 隔开，故 用 空格 隔开 firstName 与 lastName
								+ customerInfoEdit.getLastName());
				}
				
				//autelId值不在原表中直接修改，要先激活
				customerInfoEdit.setAutelId(customerInfo.getAutelId());
				
				//更新用户信息表
				customerInfoService.updateCustomerByCode(customerInfoEdit);

				//填充customerInfoEdit对象 用于发邮件的相关值
				if (StringUtil.isEmpty(customerInfoEdit.getLanguageCode())){
					customerInfoEdit.setLanguageCode(customerInfo.getLanguageCode());
				}
				
				if (StringUtil.isEmpty(customerInfoEdit.getName())){
					customerInfoEdit.setName(customerInfo.getName());
				}
				
				
				//发邮件////////////////////////////
				
				if (!StringUtil.isEmpty(autelIdNew)) {
					// 设置个人信息时只有当第一邮箱变动时才发送邮件
					if (!customerInfo.getAutelId().equals(autelIdNew)) {
						sendAutelIDEmailForChange(customerInfoEdit,autelIdNew);
					}
				}

				if (!StringUtil.isEmpty(secondEmailNew)) {
					// 设置个人信息时只有只有当第一次输入第二邮箱以及第二邮箱变动时才发送邮件
					if (customerInfo.getSecondEmail()!=null) {
						if (!customerInfo.getSecondEmail().equals(secondEmailNew)) {
							sendSecondEmail(customerInfoEdit);
						}
					}else{
						sendSecondEmail(customerInfoEdit);
					}
				}

				map.put("result", "1");
			}
		} catch (Exception e) {
			map.put("result", "2");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;

	}

	/**
	 * 用户个人信息修改
	 * 
	 * @param request
	 */
	@Action(value = "updateCustomerInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustomerInfo() {
		Map<String, String> map = new HashMap<String, String>();

		try {
			

			CustomerInfo customerInfo = customerInfoService
					.getCustomerInfoByAutelId(userInfoVo.getAutelId());
			
			if (!StringUtil.isEmpty(userInfoVo.getSecondEmail())) {

				if(customerInfo.getSecondEmail()==null){
					// 判断第二邮箱是否重复
					if (customerInfoService.getCustomerInfoListBySecondEmail(userInfoVo.getSecondEmail()).size() > 0) {
						map.put("result", "3");
						jsonData = JSONArray.fromObject(map).toString();
						return SUCCESS;
					}
					
				}else if(!customerInfo.getSecondEmail().equals(userInfoVo.getSecondEmail())){
					
					if (customerInfoService.getCustomerInfoListBySecondEmail(userInfoVo.getSecondEmail()).size() > 0) {
						map.put("result", "3");
						jsonData = JSONArray.fromObject(map).toString();
						return SUCCESS;
					}
				}

			}
			
			String initSecondEmail = customerInfo.getSecondEmail();

			if (customerInfo != null) {
				if(!StringUtil.isEmpty(userInfoVo.getUserPwd())){
					customerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(userInfoVo.getUserPwd(),null));
				}
				customerInfo.setFirstName(userInfoVo.getFirstName());
				customerInfo.setMiddleName(userInfoVo.getMiddleName());
				customerInfo.setLastName(userInfoVo.getLastName());
				customerInfo.setComUsername(userInfoVo.getComUsername());
				customerInfo.setLanguageCode(userInfoVo.getLanguageCode());
				customerInfo.setAddress(userInfoVo.getAddress());
				customerInfo.setCountry(userInfoVo.getCountry());
				customerInfo.setCompany(userInfoVo.getCompany());
				customerInfo.setZipCode(userInfoVo.getZipCode());
				customerInfo.setCity(userInfoVo.getCity());
				customerInfo.setQuestionCode(userInfoVo.getQuestionCode());
				customerInfo.setAnswer(userInfoVo.getAnswer());
				customerInfo.setMobilePhone(userInfoVo.getMobilePhone());
				customerInfo.setMobilePhoneAC(userInfoVo.getMobilePhoneAC());
				customerInfo.setMobilePhoneCC(userInfoVo.getMobilePhoneCC());
				customerInfo.setDaytimePhone(userInfoVo.getDaytimePhone());
				customerInfo.setDaytimePhoneAC(userInfoVo.getDaytimePhoneAC());
				customerInfo.setDaytimePhoneCC(userInfoVo.getDaytimePhoneCC());
				customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				customerInfo.setIsAllowSendEmail(userInfoVo
						.getIsAllowSendEmail());
				customerInfo
						.setName(userInfoVo.getFirstName()
								+ userInfoVo.getMiddleName()
								+ userInfoVo.getLastName());
				customerInfo.setSecondEmail(userInfoVo.getSecondEmail());
				customerInfoService.updateCustomer(customerInfo);

				if (!StringUtil.isEmpty(userInfoVo.getSecondEmail())) {
					// 设置个人信息时只有只有当第一次输入第二邮箱以及第二邮箱变动时才发送邮件
					if (initSecondEmail!=null) {
						if (!initSecondEmail.equals(userInfoVo.getSecondEmail())) {
							sendSecondEmail(customerInfo);
						}
					}else{
						sendSecondEmail(customerInfo);
					}
					

				}

				map.put("result", "1");
			}
		} catch (Exception e) {
			map.put("result", "2");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;

	}
	



	/**
	 * 修改AutelId时，关联表DT_CustomerChangeAutel信息维护
	 * 
	 * @param request
	 */
	public void updateAutelIdNew(String oldAutelId,String newAutelId) throws Exception{
		
		CustomerInfo updateCust=customerInfoService.getCustomerInfoByAutelId(oldAutelId);
		
		//修改关联表DT_CustomerChangeAutel信息
		CustomerChangeAutel change=changeService.getCustomerChangeAutel(updateCust.getCode());
		if(change==null){
			change=new CustomerChangeAutel();
		}
		change.setCustomerCode(updateCust.getCode());
		change.setNewAutelId(newAutelId);
		change.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
		if(change.getId()==null){
			changeService.addCustomerChangeAutel(change);
		}else{
			changeService.updateCustomerChange(change);
	    }
	}

	@SuppressWarnings("unchecked")
	/**
	 * 根据个人信息设置、如第一邮箱不为空需给第一邮箱发送激活链接
	 */
	private void sendAutelIDEmailForChange(CustomerInfo cust,String autelIdNew) {
		// 发送激活第一邮箱邮件		
		Smtp smtp = smtpManager.getCurrentSmtp();
			
		EmailModel emailModel = new EmailModel();
		emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
		emailModel.getData().put("password", smtp.getPassword());
		emailModel.getData().put("host", smtp.getHost());
		emailModel.getData().put("port", smtp.getPort());
			
		HttpServletRequest request= ThreadContextHolder.getHttpRequest();
		
		String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");
		String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
							 + cust.getAutelId() + "&actCode="+cust.getActCode()+"&operationType=24";
		
		String userName="";
		if(cust!=null && cust.getName()!=null){
		  userName=cust.getName();
		}
		
		emailModel.getData().put("autelId", autelIdNew);
		emailModel.getData().put("userName", userName);
		emailModel.getData().put("activeUrl", activeUrl);
		Language language = languageService.getByCode(cust.getLanguageCode());
			
		if(language != null)
		{
			emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
						language.getLanguageCode(),language.getCountryCode()));
		}
		else 
		{
			emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
		}
			
		emailModel.setTo(autelIdNew);
		EmailTemplate template = emailTemplateService.
					getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_AUTELID_UPDATE_ACTIVE,cust.getLanguageCode());
			
		if(template != null)
		{
			emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
		}
		
		emailProducer.send(emailModel);

		try {
			CustomerInfo customerInfo = customerInfoService.getCustomerByCode(cust.getCode());

			if (customerInfo != null) {
				// 每次发送第一邮箱激活时需重新设置第一邮箱激活状态以及激活时间时间点
				//customerInfo.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
				customerInfo.setSendActiveTime(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				customerInfo.setSendResetPasswordTime(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				customerInfoService.updateCustomer(customerInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	/**
	 * 根据个人信息设置、如第二邮箱不为空需给第二邮箱发送激活链接
	 */
	private void sendSecondEmail(CustomerInfo cust) {
		// 发送激活第二邮箱邮件
		Smtp smtp = smtpManager.getCurrentSmtp();

		EmailModel emailModel = new EmailModel();
		emailModel.getData().put("username", smtp.getUsername());// username
																	// 为freemarker模版中的参数
		emailModel.getData().put("password", smtp.getPassword());
		emailModel.getData().put("host", smtp.getHost());

		String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
		String activeUrl = requestUrl
				+ "/goActiveSecondEmail.html?secondEmail="
				+ cust.getSecondEmail() + "&actCode="
				+ cust.getActCode() + "&operationType=20";

		String userName="";
		if(cust!=null && cust.getName()!=null){
		  userName=cust.getName();
		}
		emailModel.getData().put("secondEmail", cust.getSecondEmail());
		emailModel.getData().put("activeUrl", activeUrl);
		emailModel.getData().put("autelId", cust.getAutelId());
		emailModel.getData().put("userName", userName);
		Language language = languageService.getByCode(cust
				.getLanguageCode());

		if (language != null) {
			emailModel.setTitle(FreeMarkerPaser.getBundleValue(
					"accountinformation.activesecondemail.mailtitle",
					language.getLanguageCode(), language.getCountryCode()));
		} else {
			emailModel
					.setTitle(FreeMarkerPaser
							.getBundleValue("accountinformation.activesecondemail.mailtitle"));
		}

		emailModel.setTo(cust.getSecondEmail());
		EmailTemplate template = emailTemplateService
				.getUseTemplateByTypeAndLanguage(
						CTConsatnt.EMAIL_TEMPLATE_TYEP_AUTELID_SECONDEMAIL_ACTIVE,
						cust.getLanguageCode());

		if (template != null) {
			emailModel.setTemplate(template.getCode() + ".html"); // 此处一定需要这样写
		}

		emailProducer.send(emailModel);

		try {
			List<CustomerInfo> customerInfoList = customerInfoService
					.getCustomerInfoListBySecondEmail(cust
							.getSecondEmail());

			if (customerInfoList.size() > 0) {
				// 每次发送第二邮箱激活时需重新设置第二邮箱激活状态以及激活时间时间点
				CustomerInfo customerInfo = customerInfoList.get(0);
				customerInfo
						.setSecondActState(FrontConstant.USER_STATE_NOT_ACTIVE);
				customerInfo.setSendSecondEmailActiveTime(DateUtil.toString(
						new Date(),
						FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
				customerInfoService.updateCustomer(customerInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 经销商个人信息修改
	 * 
	 * @param request
	 */
	@Action(value = "updateSealerInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateSealerInfo() {
		Map<String, String> map = new HashMap<String, String>();

		try {

			SealerInfo sealerInfo = sealerInfoService
					.getSealerByCode(userInfoVo.getCode());

			if (sealerInfo == null) {
				map.put("result", "false");

			} else {
				/*sealerInfo.setFirstName(userInfoVo.getFirstName());
				sealerInfo.setMiddleName(userInfoVo.getMiddleName());
				sealerInfo.setLastName(userInfoVo.getLastName());*/
				sealerInfo.setLanguageCode(userInfoVo.getLanguageCode());
				sealerInfo.setAddress(userInfoVo.getAddress());
				sealerInfo.setCountry(userInfoVo.getCountry());
				sealerInfo.setCompany(userInfoVo.getCompany());
				sealerInfo.setZipCode(userInfoVo.getZipCode());
				sealerInfo.setEmail(userInfoVo.getEmail());
				sealerInfo.setCity(userInfoVo.getCity());
				sealerInfo.setMobilePhone(userInfoVo.getMobilePhone());
				sealerInfo.setMobilePhoneAC(userInfoVo.getMobilePhoneAC());
				sealerInfo.setMobilePhoneCC(userInfoVo.getMobilePhoneCC());
				sealerInfo.setDaytimePhone(userInfoVo.getDaytimePhone());
				sealerInfo.setDaytimePhoneAC(userInfoVo.getDaytimePhoneAC());
				sealerInfo.setDaytimePhoneCC(userInfoVo.getDaytimePhoneCC());
				sealerInfo
						.setIsAllowSendEmail(userInfoVo.getIsAllowSendEmail());
				sealerInfo.setSecondEmail(userInfoVo.getSecondEmail());
				sealerInfoService.updateSealer(sealerInfo);
				map.put("result", "true");
			}

		} catch (Exception e) {
			map.put("result", "false");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();

		return SUCCESS;
	}

	/**
	 * 经销商密码修改
	 * 
	 * @param request
	 * @throws Exception
	 */
	@Action(value = "updateSealerPassword", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateSealerPassword() throws Exception{
		Map<String, String> map = new HashMap<String, String>();
		String password = userInfoVo.getUserPwd();
		String oldPassword = userInfoVo.getOldPassword();

		// 检查旧密码是否正确
		try{
			if (sealerInfoService.getSealerInfoByAutelIdAndPassword(
					userInfoVo.getAutelId(),
					Md5PwdEncoder.getInstance().encodePassword(oldPassword,null)) == null) {
				map.put("oldPassword", "false");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
		}catch(Exception e1){
			e1.printStackTrace();
			throw e1;
		}

		try {
			SealerInfo sealerInfo = new SealerInfo();
			sealerInfo.setCode(userInfoVo.getCode());
			sealerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(password,null));
			sealerInfoService.updateSealer(sealerInfo);
			map.put("result", "true");
		} catch (Exception e) {
			map.put("result", "false");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();

		return SUCCESS;

	}

	/**
	 * 下载工具
	 * 
	 * @param request
	 */
	@Action(value = "downLoadTools", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String downLoadTools() {
		Map<String, String> map = new HashMap<String, String>();

		HttpServletResponse response = ThreadContextHolder.getHttpResponse();

		// path是指欲下载的文件的路径。
		File file = new File(downToolPath);

		if (!file.exists()) {
			map.put("fileResult", "false");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}

		// 取得文件名。
		String filename = file.getName();

		try {
			// 以流的形式下载文件。
			InputStream fis = new BufferedInputStream(new FileInputStream(
					downToolPath));
			byte[] buffer;
			buffer = new byte[fis.available()];
			fis.read(buffer);
			// fis.close();

			// 清空response
			response.reset();

			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename="
					+ new String(filename.getBytes()));
			response.addHeader("Content-Length", "" + file.length());
			OutputStream toClient = new BufferedOutputStream(
					response.getOutputStream());
			response.setContentType("application/octet-stream");
			toClient.write(buffer);
			toClient.flush();
			// toClient.close();
			// map.put("result", "true");
		} catch (FileNotFoundException e) {
			map.put("result", "false");
			e.printStackTrace();
		} catch (IOException e) {
			map.put("result", "false");
			e.printStackTrace();
		}

		// jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 客户升级卡操作
	 * 
	 * @param request
	 * @throws Exception
	 */
	
	@SuppressWarnings("unused")
	@Action(value = "reChargeCardOperation", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String reChargeCardOperation() throws Exception{
		Map<String, String> map = new HashMap<String, String>();
		String reChargeCardSerialNo = reChargeCardVo.getReChargeCardSerialNo();
		String reChargePwd = reChargeCardVo.getReChargeCardPassword()==null?"":reChargeCardVo.getReChargeCardPassword().trim().toUpperCase();
		ReChargeCardInfo reChargeCardInfo = null;

		String code = (String) ThreadContextHolder.getSessionContext().getAttribute(FrontConstant.SESSION_IMAGE_CODE_NAME);
		String imgcode=ThreadContextHolder.getHttpRequest().getParameter("imgcode");
		
		String proType=this.getPropertValue("product.type.maxidas");
		
		
		if(code!=null && imgcode!=null){
			if(!code.toUpperCase().equals(imgcode.toUpperCase())){
				map.put("result", "1"); //验证码输入错误
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
		}else{
			map.put("result", "1"); //验证码输入错误
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;
		}
		try {
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);
			if(customerInfo==null){
				map.put("result", "11"); // 登录失效
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
			
			int num=logService.getErrorLogCount(customerInfo.getCode());
			if(num>=5){
				map.put("result", "8"); // 今日充值错误次数超过五次
				//this.saveErrorLog("输入的续租卡密码不存在","1");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}
            
			String md5Pwd=Md5PwdEncoder.getInstance().encodePassword(reChargePwd,null);
			reChargeCardInfo = reChargeCardInfoService.getReChargeCardInfoByCardNoAndPwd(reChargeCardSerialNo,md5Pwd);
			
			if (reChargeCardInfo == null)
			{
				map.put("result", "2"); // 充值卡不存在
				this.saveErrorLog("输入的续租卡密码不存在","1",customerInfo.getCode());
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			} else if (reChargeCardInfo.getIsActive() == FrontConstant.CHARGE_CARE_IS_NO_ACTIVE)// 校验充值卡是否已激活
			{
				map.put("result","3"); //充值卡没有激活
				this.saveErrorLog("输入的续租卡没有激活","1",customerInfo.getCode());
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			} else if (reChargeCardInfo.getIsUse() == FrontConstant.CARD_SERIAL_IS_YES_USE)// 校验充值卡是否已使用
			{
				map.put("result","4"); //续租卡已经使用
				this.saveErrorLog("输入的续租卡已经被使用","1",customerInfo.getCode());
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}else{
				//1.根据产品序列号获得产品信息，包括产品类型和销售契约编号
				ProductInfo info=productInfoService.getBySerialNo(reChargeCardVo.getProSerialNo());
				//2.根据销售契约编号获取销售契约
				SaleContract proContract=saleContractService.getSaleContractByCode(info.getSaleContractCode());
				//3.根据充值卡类型code查询充值卡类型信息,获取续租卡的销售契约契约和地区
				ReChargeCardType reChargeCardType = reChargeCardTypeService.getReChargeCardTypeByCode(reChargeCardInfo.getReChargeCardTypeCode());
				//4.根据卡类型获取卡类型的销售契约
				SaleContract cardContract=saleContractService.getSaleContractByCode(reChargeCardType.getSaleContractCode());
				if(reChargeCardType==null){
					map.put("result","9"); //续租卡类型不存在
					this.saveErrorLog("续租卡类型不存在","0",customerInfo.getCode());
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}
//				
				else if(!proContract.getProTypeCode().equals(cardContract.getProTypeCode())){
					map.put("result","6"); //充值卡类型和产品型号不匹配
					this.saveErrorLog("充值卡类型和产品型号不匹配","0",customerInfo.getCode());
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}else if(info.getProTypeCode().equals(proType) &&  cardContract.getAreaCfgCode().equals("acf_North_America") && !proContract.getAreaCfgCode().equals("acf_North_America")){
					if(proContract.getAreaCfgCode().equals("acf_AutelDefault")){ 
						//如果产品的销售契约地区是“AutelDefault”，则修改它的销售契约为美国
						SaleContract temp=new SaleContract();
						temp.setAreaCfgCode(cardContract.getAreaCfgCode());  //美国区域
						temp.setLanguageCfgCode(proContract.getLanguageCfgCode()); //产品语言
						temp.setSaleCfgCode(proContract.getSaleCfgCode()); //产品销售配置
						temp.setSealerCode(cardContract.getSealerCode());  //美国经销商
						temp.setProTypeCode(cardContract.getProTypeCode()); //升级卡类型
						
						List<SaleContract> list=saleContractService.getContrctList(temp);
						if(list== null || list.size()==0){
							map.put("result","2"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
							this.saveErrorLog("AutelDefault对应的销售配置在美国区域找不到匹配","0",customerInfo.getCode());
							jsonData = JSONArray.fromObject(map).toString();
							return SUCCESS;
						}else{
							SaleContract con=list.get(0);
							
							String ip=FrontConstant.getIpAddr(request);
							ProductContractChangeLog log=new ProductContractChangeLog();
							log.setNewContract(con.getCode());
							log.setOldContract(info.getSaleContractCode());
							log.setOldMinSaleUnit("使用升级卡更换契约");
							log.setOperatorIp(ip);
							log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
							log.setOperatorUser(customerInfo.getCode());
							log.setProductSN(info.getSerialNo());
							//1.保存日志
							productContractService.saveLog(log);
							
							info.setSaleContractCode(con.getCode());
							productInfoService.updateProductInfo(info);  //更新该产品的销售契约
							
						
							
							
							// 充值
							boolean resultResult = toCustomerChargeService.toCharge(
									reChargeCardInfo, reChargeCardType, customerInfo,
									reChargeCardVo.getProSerialNo());

							if (resultResult == true) {
								map.put("result", "0");
							} else {
								map.put("result", "9");
							}
						}
						
						
					}else{
						map.put("result","5"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
						this.saveErrorLog("美国卡不能在非美国区使用","0",customerInfo.getCode());
						jsonData = JSONArray.fromObject(map).toString();
						return SUCCESS;
					}
				}else if(!cardContract.getAreaCfgCode().equals("acf_North_America") && proContract.getAreaCfgCode().equals("acf_North_America")){
					map.put("result","7"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
					this.saveErrorLog("非美国卡不能在美国区使用","0",customerInfo.getCode());
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}
				else{
				//	CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(reChargeCardVo.getMemberName());
					// 充值
					boolean resultResult = toCustomerChargeService.toCharge(
							reChargeCardInfo, reChargeCardType, customerInfo,
							reChargeCardVo.getProSerialNo());

					if (resultResult == true) {
						map.put("result", "0");
					} else {
						map.put("result", "9");
					}
				}
			} 

		} catch (Exception ex) {
			map.put("result", "9");
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
			
			if("com.cheriscon.front.user.action.UserInfoAction".equals(exclass)){
				
				errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
						+ method + "时在第" + stackTraceElement.getLineNumber()
						+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
				System.out.println(errorMsg);
				systemErrorService.saveLog("升级卡",errorMsg);
			}
			}
			
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	
	/**
	 * 经销商给客户使用升级卡
	 * 
	 * @param request
	 * @throws Exception
	 */
	@Action(value = "reChargeCardForUser", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String reChargeCardForUser() {
		Map<String, String> map = new HashMap<String, String>();
		String reChargeCardSerialNo = reChargeCardVo.getReChargeCardSerialNo();
		String reChargePwd = reChargeCardVo.getReChargeCardPassword();
		ReChargeCardInfo reChargeCardInfo = null;

		try {
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			SealerInfo sealer=(SealerInfo)SessionUtil.getLoginUserInfo(request);
			int num=logService.getErrorLogCount(sealer.getCode());
			if(num>=5){
				map.put("result", "8"); // 今日充值错误次数超过五次
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}

			String md5Pwd=Md5PwdEncoder.getInstance().encodePassword(reChargePwd,null);
			reChargeCardInfo = reChargeCardInfoService.getReChargeCardInfoByCardNoAndPwd(reChargeCardSerialNo,md5Pwd);
			
			if (reChargeCardInfo == null)
			{
				map.put("result", "2"); // 充值卡不存在
				this.saveErrorLog("输入的续租卡密码不存在","1",sealer.getCode());
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			} else if (reChargeCardInfo.getIsActive() == FrontConstant.CHARGE_CARE_IS_NO_ACTIVE)// 校验充值卡是否已激活
			{
				map.put("result","3"); //充值卡没有激活
				this.saveErrorLog("输入的续租卡没有激活","1",sealer.getCode());
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			} else if (reChargeCardInfo.getIsUse() == FrontConstant.CARD_SERIAL_IS_YES_USE)// 校验充值卡是否已使用
			{
				map.put("result","4"); //续租卡已经使用
				this.saveErrorLog("输入的续租卡已经被使用","1",sealer.getCode());
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}else{
				//1.根据产品序列号获得产品信息，包括产品类型和销售契约编号
				ProductInfo info=productInfoService.getBySerialNo(reChargeCardVo.getProSerialNo());
				//2.根据销售契约编号获取销售契约
				SaleContract proContract=saleContractService.getSaleContractByCode(info.getSaleContractCode());
				//3.根据充值卡类型code查询充值卡类型信息,获取续租卡的销售契约契约和地区
				ReChargeCardType reChargeCardType = reChargeCardTypeService.getReChargeCardTypeByCode(reChargeCardInfo.getReChargeCardTypeCode());
				//4.根据卡类型获取卡类型的销售契约
				SaleContract cardContract=saleContractService.getSaleContractByCode(reChargeCardType.getSaleContractCode());
				if(reChargeCardType==null){
					map.put("result","9"); //续租卡类型不存在
					this.saveErrorLog("续租卡类型不存在","0",sealer.getCode());
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}else if(cardContract.getAreaCfgCode().equals("acf_North_America") && !proContract.getAreaCfgCode().equals("acf_North_America")){
					map.put("result","5"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
					this.saveErrorLog("美国卡不能在非美国区使用","0",sealer.getCode());
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}else if(!cardContract.getAreaCfgCode().equals("acf_North_America") && proContract.getAreaCfgCode().equals("acf_North_America")){
					map.put("result","7"); //销售契约不在同一区域,这里的销售契约编号是写死的南美区
					this.saveErrorLog("非美国卡不能在美国区使用","0",sealer.getCode());
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}
				else if(!proContract.getProTypeCode().equals(cardContract.getProTypeCode())){
					map.put("result","6"); //充值卡类型和产品型号不匹配
					this.saveErrorLog("充值卡类型和产品型号不匹配","0",sealer.getCode());
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}else{
					CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(reChargeCardVo.getMemberName());
					// 充值
					boolean resultResult = toCustomerChargeService.toCharge(
							reChargeCardInfo, reChargeCardType, customerInfo,
							reChargeCardVo.getProSerialNo());

					if (resultResult == true) {
						map.put("result", "0");
					} else {
						map.put("result", "9");
					}
				}
			} 

		} catch (Exception e) {
			map.put("result", "9");
			e.printStackTrace();
		}
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	
	/**
	 * 续租后的时间处理
	 * 
	 * @param valiDate
	 *            该产品的有效期时间
	 * @param reChargeDate
	 *            续租时间年限 （默认1年）
	 * @return
	 */
	public String getReChargeDate(String valiDate, int reChargeDate) {
		String date = "";

		String year = valiDate.split(FrontConstant.DATE_CHARACTER_SPLIT_STRING)[0];
		String month = valiDate
				.split(FrontConstant.DATE_CHARACTER_SPLIT_STRING)[1];
		String other = valiDate
				.split(FrontConstant.DATE_CHARACTER_SPLIT_STRING)[2];

		date = Integer.parseInt(year) + reChargeDate
				+ FrontConstant.DATE_CHARACTER_SPLIT_STRING + month
				+ FrontConstant.DATE_CHARACTER_SPLIT_STRING + other;

		return date;
	}

	/**
	 * 查询弹出信息条数
	 * 
	 * @param request
	 */
	@Action(value = "queryDialogMessageCount", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String queryDialogMessageCount() {
		Map<String, String> map = new HashMap<String, String>();

		String languageCode = "";
		String userCode = "";

		if (userInfoVo.getUserType() == FrontConstant.CUSTOMER_USER_TYPE) {
			CustomerInfo customerInfo = (CustomerInfo) SessionUtil
					.getLoginUserInfo(getRequest());
			languageCode = customerInfo.getLanguageCode();
			userCode = customerInfo.getCode();
		} else if (userInfoVo.getUserType() == FrontConstant.DISTRIBUTOR_USER_TYPE) {
			SealerInfo sealerInfo = (SealerInfo) SessionUtil
					.getLoginUserInfo(getRequest());
			languageCode = sealerInfo.getLanguageCode();
			userCode = sealerInfo.getCode();
		}

		try {
			/*// 获取各类型用户弹出消息条数
			int messageCount = messageService.queryMessageCount(userCode,null,
					languageCode, userInfoVo.getUserType(),
					FrontConstant.USER_MESSAGE_IS_YES_DIALOG, null, null);

			// 获取每个用户已读取的弹出消息条数
			int userMessgaeIsDialogReadCount = userMessageService
					.queryUserMessageIsReadCount(userInfoVo.getUserType(),
							languageCode, userCode,
							FrontConstant.USER_MESSAGE_IS_YES_DIALOG);*/

			// 获取每个用户未读取的弹出消息条数
			int userMessageIsNoDialogReadCount = messageService.queryMessageCountNotReadNew(userCode, null, languageCode, userInfoVo.getUserType(),FrontConstant.USER_MESSAGE_IS_YES_DIALOG, null, null);

			map.put("userMessageIsNoDialogReadCount",
					String.valueOf(userMessageIsNoDialogReadCount));
			jsonData = JSONArray.fromObject(map).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	/**
	 * 增加入库记录用户已读消息
	 * 
	 * @param request
	 */
	@Action(value = "addUserMessageIsRead", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String addUserMessageIsRead() {
		Map<String, String> map = new HashMap<String, String>();
		UserMessage userMessage = new UserMessage();
		userMessage.setUserType(userMessageVo.getUserType());
		userMessage.setMessageCode(userMessageVo.getMessageCode());
		userMessage.setUserCode(userMessageVo.getUserCode());
		userMessage.setMessageIsRead(FrontConstant.USER_MESSAGE_IS_YES_READ);
		try {
			userMessageService.saveMessage(userMessage);
			map.put("result", "true");
		} catch (Exception e) {
			map.put("result", "false");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 重置密码检查
	 * 
	 * @param request
	 */
	@Action(value = "resetPasswordCheck", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String resetPasswordCheck() {
		Map<String, String> map = new HashMap<String, String>();

		try {
             
			CustomerInfo customerInfo = customerInfoService
					.getCustomerInfoByAutelIdAndActCode(
							userInfoVo.getAutelId(), userInfoVo.getActCode());
			if (customerInfo != null) {
				if (customerInfo.getActState() == FrontConstant.USER_STATE_NOT_ACTIVE) {
					map.put("result", "1");
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				}
				if(type!="reset" && !type.equals("reset")){
				if (StringUtil.isEmpty(customerInfo.getSendResetPasswordTime())) {
					map.put("result", "3");
					jsonData = JSONArray.fromObject(map).toString();
					return SUCCESS;
				} else {
					// 获取密码重置时设置的时间
					Date sendResetPasswordTime = DateUtil.toDate(
							customerInfo.getSendResetPasswordTime(),
							FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);

					// 获取点击密码重置链接时的时间
					Date currentResetPasswordTime = DateUtil
							.toDate(DateUtil
									.toString(
											new Date(),
											FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS),
									FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS);

					// 获取时间差
					long timeDifference = (currentResetPasswordTime.getTime() - sendResetPasswordTime
							.getTime()) / 1000;

					// 如果12小时内未重置密码、不能进行重置密码操作
					
					if (timeDifference > 43200) {
						map.put("result", "3");
						jsonData = JSONArray.fromObject(map).toString();
						return SUCCESS;
					}
					}
					
				}
			} else {
				map.put("result", "2");
				jsonData = JSONArray.fromObject(map).toString();
				return SUCCESS;
			}

		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "5");
			jsonData = JSONArray.fromObject(map).toString();
			return SUCCESS;

		}

		map.put("result", "4");
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}

	/**
	 * 对系统客服回复进行评分
	 * 
	 * @param request
	 */
	@Action(value = "updateCustomerComplaintScore", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateCustomerComplaintScore() {
		Map<String, String> map = new HashMap<String, String>();
		ReCustomerComplaintInfo reCustomerComplaintInfo = new ReCustomerComplaintInfo();

		try {
			reCustomerComplaintInfo.setId(reCustomerComplaintInfoVO.getId());
			reCustomerComplaintInfo.setComplaintScore(reCustomerComplaintInfoVO
					.getComplaintScore());
			reCustomerComplaintInfoService
					.updateReCustomerComplaintInfo(reCustomerComplaintInfo);
			map.put("result", "true");

		} catch (Exception e) {
			map.put("result", "false");
			e.printStackTrace();
		}

		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	//修改autelId
	@Action(value = "updateAutelId", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateAutelId(){
		Map<String, String> map = new HashMap<String, String>();
		String result="0";
		try {
			CustomerInfo updateCust=customerInfoService.getCustomerInfoByAutelId(oldId);
			int cust=changeService.getCustomerInfoNumByIdAndSrouce(newAutelId);
			if(updateCust!=null){
				if(cust!=0){
					result="1" ; //输入的autelID已经存在
				}else{
					updateCust.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
				//	updateCust.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
					updateCust.setSendActiveTime(DateUtil.toString(new Date(), 
							  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
					customerInfoService.updateCustomer(updateCust);
					
					 CustomerChangeAutel changeAutel=changeService.getCustomerChangeAutel(updateCust.getCode());
					  if(changeAutel==null){
						  changeAutel=new CustomerChangeAutel();
					  }
					changeAutel.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
					changeAutel.setCustomerCode(updateCust.getCode());
					changeAutel.setNewAutelId(newAutelId);
					
					if(changeAutel.getId()==null){
					changeService.addCustomerChangeAutel(changeAutel);
					}else{
					changeService.updateCustomerChange(changeAutel);
					}
					
					Smtp smtp = smtpManager.getCurrentSmtp( );
						
					EmailModel emailModel = new EmailModel();
					emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
					emailModel.getData().put("password", smtp.getPassword());
					emailModel.getData().put("host", smtp.getHost());
					emailModel.getData().put("port", smtp.getPort());
						
					HttpServletRequest request= ThreadContextHolder.getHttpRequest();
					
					String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");
					String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
										 + updateCust.getAutelId() + "&actCode="+updateCust.getActCode()+"&operationType=24";
					
					String userName="";
					if(updateCust!=null && updateCust.getName()!=null){
					  userName=updateCust.getName();
					}
					
					emailModel.getData().put("autelId", updateCust.getAutelId());
					emailModel.getData().put("userName", userName);
					emailModel.getData().put("activeUrl", activeUrl);
					Language language = languageService.getByCode(updateCust.getLanguageCode());
						
					if(language != null)
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
									language.getLanguageCode(),language.getCountryCode()));
					}
					else 
					{
						emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
					}
						
					emailModel.setTo(newAutelId);
					EmailTemplate template = emailTemplateService.
								getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_AUTELID_UPDATE_ACTIVE,updateCust.getLanguageCode());
						
					if(template != null)
					{
						emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
					}
					emailProducer.send(emailModel);
				}
			}
		} catch (Exception e) {
			result="2";
		}
		map.put("result", result);
		jsonData = JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	@Action(value = "clearUser", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "addResult" }) })
	public String clearUser(){
		HttpServletResponse response  = ThreadContextHolder.getHttpResponse();
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, IP, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, LOGINSTATUS, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, USERTYPE, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, "custCountry", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.cancleCookie1(request, response, "tokenKey" , FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		CookieUtils.addCookie1(request, response, LOGINOUT , "1", 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
		
		//除去企业网站cookie
		/*CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, "p.auteltech.com");
		CookieUtils.cancleCookie1(request, response, "custCountry", "p.auteltech.com");
		CookieUtils.cancleCookie1(request, response, "tokenKey" , "p.auteltech.com");*/
		
		SessionUtil.removeLoginUserInfo(request);
		return SUCCESS;
	}
	
	@Action(value = "checkUserSession", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkUserSession(){
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String autelId=request.getParameter("autelKey");
		String tokenKey=request.getParameter("tokenKey");
		String type=request.getParameter("type")==null?"":request.getParameter("type"); //用来区分来源系统 ，s代表Support
		Object obj=request.getSession().getAttribute("userInfo");
		Map<String, String> map = new HashMap<String, String>();
		String result="2";
		try {
			if(type.equals("s") && obj==null){
				HttpServletResponse response  = ThreadContextHolder.getHttpResponse();
				CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, IP, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, LOGINSTATUS, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, USERTYPE, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "autelId", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "custCountry", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "tokenKey" , FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.addCookie1(request, response, LOGINOUT , "1", 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "language", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				//除去企业网站cookie
			/*	CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, "p.auteltech.com");
				CookieUtils.cancleCookie1(request, response, "custCountry", "p.auteltech.com");
				CookieUtils.cancleCookie1(request, response, "tokenKey" , "p.auteltech.com");*/
				
				result="3";
			}else{
			String userType=CookieUtils.getCookie(request, "userType")==null?"":CookieUtils.getCookie(request, "userType").getValue();
			String tempToken=null;
			if(userType.equals("1")){
				CustomerInfo updateCust=customerInfoService.getCustomerInfoByAutelId(autelId);
				if(updateCust!=null){
					tempToken=updateCust.getUserPwd();
				}
			
			}else{
				SealerInfo sealerInfo = sealerInfoService.getSealerByAutelId(autelId);
				if(sealerInfo!=null){
					tempToken=sealerInfo.getUserPwd();
				}
			}
			
			if(null!=tempToken && tokenKey.equals(tempToken)){
				result="1";
			}else{
				HttpServletResponse response  = ThreadContextHolder.getHttpResponse();
				CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, IP, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, LOGINSTATUS, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, USERTYPE, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "custCountry", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "tokenKey" , FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.addCookie1(request, response, LOGINOUT , "1", 3600, FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				CookieUtils.cancleCookie1(request, response, "language", FreeMarkerPaser.getBundleValue("autelproweb.cookie.domain"));
				//除去企业网站cookie
				/*CookieUtils.cancleCookie1(request, response, AUTH_KEY_MEMBER, "p.auteltech.com");
				CookieUtils.cancleCookie1(request, response, "custCountry", "p.auteltech.com");
				CookieUtils.cancleCookie1(request, response, "tokenKey" , "p.auteltech.com");*/
				request.getSession().removeAttribute("userInfo");
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("checkResult", result);
		jsonData=JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	public void saveErrorLog(String remark,String type,String userCode){
		ReChargeCardErrorLog log=new ReChargeCardErrorLog();
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		log.setCreateTime(date);
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		String ip=FrontConstant.getIpAddr(request);
		log.setCustomerCode(userCode);
		log.setIp(ip);
		log.setStatus(0);
		log.setType(type);
		log.setRemark(remark);
		try {
			logService.saveErrorLog(log);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Action(value = "cardSupport", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String cardSupport() throws Exception{
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		Object obj=SessionUtil.getLoginUserInfo(request);
		if(obj.getClass().equals(CustomerInfo.class)){
			support.setCustomerCode(((CustomerInfo)obj).getCode());
			support.setAutelId(((CustomerInfo)obj).getAutelId());
		}
		if(obj.getClass().equals(SealerInfo.class)){
			support.setCustomerCode(((SealerInfo)obj).getCode());
			support.setAutelId(((SealerInfo)obj).getAutelId());
		}
		String ip=FrontConstant.getIpAddr(request);
		support.setIp(ip);
		support.setCreateTime(date);
		support.setStatus(0);
		Map<String, String> map = new HashMap<String, String>();
		try{
			supportService.saveCardSupport(support);
			map.put("result", "1");
		}catch(Exception e){
			map.put("result", "0");
		}
		jsonData=JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	
	@Action(value = "renewProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String renewProduct() throws Exception{
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);
		try{
		List<MyProduct> list=myAccountService.queryRenewByCode(customerInfo.getCode());
		Map<String, String> map = new HashMap<String, String>();
		String returnStr="";
		if(list!=null && list.size()>0){
			returnStr="<span style='font-size:18px'>"+FreeMarkerPaser.getBundleValue("welcome.warn")+"</span></br>";
		for(int i=0;i<list.size();i++){
			MyProduct product=list.get(i);
			if(product.getOverdueFlag().equals("1")){
				String temp=FreeMarkerPaser.getBundleValue("welcome.warn.msgs");
				temp=temp.replace("{0}", product.getValidDate());
				temp=temp.replace("{1}", product.getProTypeName());
				temp=temp.replace("{2}", product.getProSerialNo());
				returnStr+=temp+"</br>";
			}else{
				String temp=FreeMarkerPaser.getBundleValue("welcome.warn.msg");
				temp=temp.replace("{0}", product.getValidDate());
				temp=temp.replace("{1}", product.getProTypeName());
				temp=temp.replace("{2}", product.getProSerialNo());
				returnStr+=temp+"</br>";
			}
		}
		returnStr+="</br>"+FreeMarkerPaser.getBundleValue("welcome.warn.msg2")+"</br>";
//		returnStr+=FreeMarkerPaser.getBundleValue("welcome.warn.msg3")+"</br>";
		returnStr+="<a  class='font16red' style='color:#08c;' href='"+FreeMarkerPaser.getBundleValue("welcome.warn.msg3.url")+"'>"+FreeMarkerPaser.getBundleValue("welcome.warn.msg3")+"</a></br>";
//		returnStr+=FreeMarkerPaser.getBundleValue("welcome.warn.msg4")+"</br>";
		returnStr+="<a  class='font16red' style='color:#08c;' href='"+FreeMarkerPaser.getBundleValue("welcome.warn.msg4.url")+"'>"+FreeMarkerPaser.getBundleValue("welcome.warn.msg4")+"</a></br>";
		}
		map.put("result", returnStr);
		jsonData=JSONArray.fromObject(map).toString();
		}catch(Exception e){
		}
		return SUCCESS;
	}
	
	public String getPropertValue(String key) throws IOException{
		InputStream in = FileUtil.getResourceAsStream("config.properties");
		Properties properties = new Properties();
		properties.load(in);
		return properties.getProperty(key);
	}
	
	@Action(value = "getProductCardType", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getProductCardType() throws Exception{
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		String serialNo=request.getParameter("serialNo");
		ProductInfo info=productInfoService.getBySerialNo(serialNo);
		Map<String, String> map = new HashMap<String, String>();
		if(info!=null){
			
			String ds708_code=this.getPropertValue("product.type.maxidas");
			String usa_area_code=this.getPropertValue("saleconfig.area.usa");
			
			
			SaleContract proContract=saleContractService.getSaleContractByCode(info.getSaleContractCode());
			if(info.getProTypeCode().equals(ds708_code) && proContract.getAreaCfgCode().equals(usa_area_code)){
				map.put("result","1"); //区域为美国并且产品为708才提示使用美国卡
			}else{
				map.put("result","0");
			}
		}else{
			map.put("result","9");
		}
		jsonData=JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
	@Action(value = "getLanguage", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String getLanguage() throws Exception{
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		Map<String, String> map = new HashMap<String, String>();
		Language language = (Language)request.getSession().getAttribute("languageInfo");
		Locale locale = CopContext.getDefaultLocal();
		if (language != null) {
			locale =new Locale(language.getLanguageCode(),language.getCountryCode());
		}
		
		map.put("language", locale.getLanguage());
		jsonData=JSONArray.fromObject(map).toString();
		return SUCCESS;
	}
}
