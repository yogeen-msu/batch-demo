package com.cheriscon.front.user.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.content.service.IUserMessageService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.service.IReChargeCardErrorLogService;
import com.cheriscon.backstage.member.service.IReChargeCardSupportService;
import com.cheriscon.backstage.member.service.ISalesQueryInfoService;
import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.service.IVehicleService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductContractLogService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.IProductResetLogService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.backstage.trade.service.IReChargeCardInfoService;
import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;
import com.cheriscon.backstage.util.CallProcedureUtil;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerChangeAutel;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.CustomerProRemark;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductContractChangeLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductResetLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.ReChargeCardErrorLog;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.model.ReChargeCardSupport;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SalesQueryInfo;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.model.UserMessage;
import com.cheriscon.common.model.Vehicle;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.DesModule;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.IChangeAutelService;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.user.util.CookieUtils;
import com.cheriscon.front.user.vo.ReChargeCardVo;
import com.cheriscon.front.user.vo.ReCustomerComplaintInfoVO;
import com.cheriscon.front.user.vo.UserInfoVo;
import com.cheriscon.front.user.vo.UserMessageVo;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthDetail;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;
import com.cheriscon.front.usercenter.distributor.service.ICustomerProRemarkService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;
import com.google.gson.annotations.Until;

/**
 * 用户信息ajax校验处理类
 * 
 * @author chenqichuan 2014-02-19
 * 
 */
@ParentPackage("json-default")
@Namespace("/front/sealer")
public class SealerAction extends WWAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5565395991392721643L;

	@Resource
	private IProductInfoService productInfoService;
	@Resource
	private ISaleContractService saleContractService;
	@Resource
	private IProductContractLogService productContractLogService;
	@Resource
	private IProductResetLogService productResetLogSerivice;
	@Resource
	private IVehicleService vehicleService;
	@Resource
	private ISealerInfoService sealerInfoService;
	@Resource
	private ISealerDataAuthDeailsService sealerDataAuthDeailsService;
	@Resource
	private ISalesQueryInfoService salesQueryInfoService;
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ICustomerProRemarkService customerProRemarkService;
	@Resource
	private ISealerAuthService sealerAuthService;
	
	private String jsonData; //返回json
	private String proCode; //产品Code
	private String newLanguage; //产品新语言
	private Vehicle vehicle =new Vehicle();
	private SealerInfo sealer;
	private SalesQueryInfo salesQueryInfo;
	private String serialNo;
	private CustomerProRemark customerProRemark;
	private SealerAuthVO sealerAuth;
	private String sealerAuthDesc;

	public SealerAuthVO getSealerAuth() {
		return sealerAuth;
	}
	public String getSealerAuthDesc() {
		return sealerAuthDesc;
	}
	public void setSealerAuth(SealerAuthVO sealerAuth) {
		this.sealerAuth = sealerAuth;
	}
	public void setSealerAuthDesc(String sealerAuthDesc) {
		this.sealerAuthDesc = sealerAuthDesc;
	}
	public CustomerProRemark getCustomerProRemark() {
		return customerProRemark;
	}
	public void setCustomerProRemark(CustomerProRemark customerProRemark) {
		this.customerProRemark = customerProRemark;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public SalesQueryInfo getSalesQueryInfo() {
		return salesQueryInfo;
	}
	public void setSalesQueryInfo(SalesQueryInfo salesQueryInfo) {
		this.salesQueryInfo = salesQueryInfo;
	}
	public SealerInfo getSealer() {
		return sealer;
	}
	public void setSealer(SealerInfo sealer) {
		this.sealer = sealer;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle){
		this.vehicle=vehicle;
	}
	
	public String getProCode() {
		return proCode;
	}

	public String getNewLanguage() {
		return newLanguage;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public void setNewLanguage(String newLanguage) {
		this.newLanguage = newLanguage;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	//经销商更换语言检查
	@Action(value = "checkChangeLanguage", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkChangeLanguage() throws Exception {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
		if(sealerInfo == null)
		{
			jsonData="NoSession";
			return SUCCESS;
		}
		ProductInfo proInfo =productInfoService.getByProCode(proCode);
		if(proInfo==null){ //产品不存在
			jsonData="ProductNotFind";
			return SUCCESS;
		}
		if(proInfo.getRegStatus()==1 || proInfo.getRegStatus().equals(1)){
			//时间计算，注册超过三天就不能在取消
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(proInfo.getRegTime());
			Calendar cal= Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MONTH, 1);
			String date1=new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			String currentDate=DateUtil.toString(new Date(), "yyyy-MM-dd");
			if(DateUtil.toDate(date1, "yyyy-MM-dd").getTime()<DateUtil.toDate(currentDate,"yyyy-MM-dd").getTime())
			{
				jsonData="TimeOut";
				return SUCCESS;
			}
		}
		SaleContract oldContract=saleContractService.getSaleContractByCode(proInfo.getSaleContractCode());
		if(oldContract==null){ //旧契约不存在
			jsonData="OldContractNotFind";
			return SUCCESS;
		}
		List<ProductContractChangeLog>  listLog=productContractLogService.queryBySerialNo(proInfo.getSerialNo());
		if(null!=listLog&&listLog.size()!=0){
			jsonData="HadChanged"; 
			return SUCCESS;
		}
		jsonData="Success";
		return SUCCESS;
	}
	
	//经销商检查是否满足取消注册
	@Action(value = "checkCancelProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String checkCancelProduct() throws Exception {
		jsonData=checkProduct();
		return SUCCESS;
	}
	
	//经销商更换客户产品语言
	@Action(value = "CancelProduct", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String CancelProduct() throws Exception {
		jsonData=checkProduct();
		if(jsonData.equals("SUCCESS")){
			try{
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			ProductInfo proInfo =productInfoService.getByProCode(proCode);
			ProductResetLog log=new ProductResetLog();
			log.setResetUser(sealerInfo.getCode());
			log.setResetUserName(sealerInfo.getAutelId());
			String dateStr = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			log.setResetDate(dateStr);
			log.setProCode(proInfo.getCode());
			log.setRegDate(proInfo.getRegTime());
			productResetLogSerivice.saveLog(log);
			}catch(Exception e){
				jsonData="Fail";
			}
		}
		return SUCCESS;
	}
	
	
	
	
	public String checkProduct(){
		String returnMsg="SUCCESS";
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
		if(sealerInfo == null)
		{
			returnMsg="NoSession";
			return SUCCESS;
		}
		
		ProductInfo proInfo =productInfoService.getByProCode(proCode);
		if(proInfo==null){
			returnMsg="PrductNotFind";
		}else{
			if(proInfo.getRegStatus()!=1){
				returnMsg="PrductNotRegister";
			}else{
				Date date=null;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(proInfo.getRegTime());
					Calendar cal= Calendar.getInstance();
					cal.setTime(date);
					cal.add(Calendar.DATE, 7);
					String currentDate=DateUtil.toString(new Date(), "yyyy-MM-dd");
					String date1=new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
					if(DateUtil.toDate(date1, "yyyy-MM-dd").getTime()<=DateUtil.toDate(currentDate,"yyyy-MM-dd").getTime())
					{
						returnMsg="TimeOut";
					}else{
					
					List<ProductResetLog> listLog=productResetLogSerivice.getProductResetByProCode(proCode);
					if(null != listLog && listLog.size()!=0){
						returnMsg="HadReset";
					}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
		
		return returnMsg;
	}
	
	//经销商更换客户产品语言
	@Action(value = "changeLanguage", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String changeLanguage() throws Exception {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
		if(sealerInfo == null)
		{
			jsonData="NoSession";
			return SUCCESS;
		}
		ProductInfo proInfo =productInfoService.getByProCode(proCode);
		if(proInfo==null){ //产品不存在
			jsonData="ProductNotFind";
			return SUCCESS;
		}
		if(proInfo.getRegStatus()==1 || proInfo.getRegStatus().equals(1)){
			//时间计算，注册超过三天就不能在取消
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(proInfo.getRegTime());
			Calendar cal= Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.MONTH, 1);
			String date1=new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			String currentDate=DateUtil.toString(new Date(), "yyyy-MM-dd");
			if(DateUtil.toDate(date1, "yyyy-MM-dd").getTime()<=DateUtil.toDate(currentDate,"yyyy-MM-dd").getTime())
			{
				jsonData="TimeOut";
				return SUCCESS;
			}
		}
		
		SaleContract oldContract=saleContractService.getSaleContractByCode(proInfo.getSaleContractCode());
		if(oldContract==null){ //旧契约不存在
			jsonData="OldContractNotFind";
			return SUCCESS;
		}
		
		try{
		SaleContract temp=new SaleContract();
		temp.setSealerCode(oldContract.getSealerCode());
		temp.setLanguageCfgCode(newLanguage);
		temp.setProTypeCode(proInfo.getProTypeCode());
		List<SaleContract> list=saleContractService.getContrctBySealer(temp);
		SaleContract newContract=null;
		if(null==list || list.size()==0){ //新契约不存在
			jsonData="NewContractNotFind";
			return SUCCESS;
		}else{
			newContract=list.get(0);
		}
		
		List<ProductContractChangeLog>  listLog=productContractLogService.queryBySerialNo(proInfo.getSerialNo());
		if(null==listLog || listLog.size()==0){
			ProductContractChangeLog log=new ProductContractChangeLog();
			 log.setNewContract(newContract.getCode());
			 log.setOldContract(proInfo.getSaleContractCode());
			 log.setOldMinSaleUnit("");
			 log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			 log.setOperatorUser(sealerInfo.getCode());
			 log.setProductSN(proInfo.getSerialNo());
			  //.保存日志
			 productContractLogService.saveLog(log);
			 //更新产品契约
			 proInfo.setSaleContractCode(newContract.getCode());
			 productInfoService.updateProductInfo(proInfo);
			 //刷新存储过程
			 CallProcedureUtil.call("pro_InitProductMinsaleSearchBySn",proInfo.getSerialNo());
			 jsonData="Success";
		}else{
			 jsonData="HadChanged"; 
		}
		
		 
		}catch(Exception e){
			jsonData="Fail";
		}
		
		
		return  SUCCESS ;
	}

	//客诉获取车型列表
	@Action(value = "getVehicleJson", results = { @Result(name = SUCCESS, type = "json", params = {
				"root", "jsonData" }) })
	public String getVehicleJson() throws Exception{
			
		List<Vehicle> listJson=vehicleService.getVehicleList(vehicle);
		jsonData=JSONArray.fromObject(listJson).toString();	
		return SUCCESS;
		}
	
	@Action(value = "updateSealerAuthInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
	public String updateSealerAuthInfo() throws Exception{
		SealerInfo info =sealerInfoService.getSealerByAutelId(sealerAuth.getAutelId());
		if(info==null){
			jsonData="FAIL";
		}
		try{
			sealerAuthService.saveAuthDetail(info.getCode(), sealerAuthDesc);
			jsonData="SUCCESS";
		}catch(Exception e){
			jsonData="FAIL";
		}
		return SUCCESS;
	}
	
	
	@Action(value = "addSealerInfo", results = { @Result(name = SUCCESS, type = "json", params = {
					"root", "jsonData" }) })
	public String addSealerInfo() throws Exception{
			try{
				jsonData="SUCCESS";
				SealerInfo temp=sealerInfoService.getSealerByAutelId(sealer.getAutelId());
				if(temp==null){
					//插入经销商信息表
					HttpServletRequest request=ThreadContextHolder.getHttpRequest();
					SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
					sealer.setAddress(sealerInfo.getAddress());
					sealer.setAreaName(sealerInfo.getAreaName());
					sealer.setCity(sealerInfo.getCity());
					sealer.setCompany(sealerInfo.getCompany());
					sealer.setCountry(sealerInfo.getCountry());
					sealer.setLanguageCode(sealerInfo.getLanguageCode());
					sealer.setRegTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
					sealer.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(sealer.getUserPwd(),null));
					sealer.setIsAllowSendEmail(sealerInfo.getIsAllowSendEmail());
					sealer.setLastLoginTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
					sealer.setCode(DBUtils.generateCode(DBConstant.SEALERINFO_CODE));
					sealer.setStatus(1);
					sealerInfoService.insertSealer2(sealer);
					//插入左边导航权限
					sealerInfoService.insertSealerAuth("queryCustomer", sealer.getCode());
					sealerInfoService.insertSealerAuth("sealerComplaint", sealer.getCode());
					sealerInfoService.insertSealerAuth("updateSealer", sealer.getCode());
					sealerInfoService.insertSealerAuth("salesList", sealer.getCode());
					sealerInfoService.insertSealerAuth("rgamanagement", sealer.getCode());
					
					//插入数据角色权限
					
					SealerDataAuthDeails auth=new SealerDataAuthDeails();
					if(sealerInfo.getAutelId().equals("C-0063")){
						auth.setAuthCode("0063,0001");
					}
					if(sealerInfo.getAutelId().equals("C-0759")){
						auth.setAuthCode("0064,0001");
					}
					
					auth.setSealerCode(sealer.getAutelId());
					sealerDataAuthDeailsService.insertAuth(auth);
				}else{
					jsonData="EXIST";
				}
			}catch(Exception e){
				   jsonData="FAIL";
			}
			
			return SUCCESS;
		}
	@Action(value = "updateSealerStatus", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
    public String updateSealerStatus() throws Exception{
		jsonData="SUCCESS";
		try{
			SealerInfo temp=sealerInfoService.getSealerByAutelId(sealer.getAutelId());
			temp.setStatus(0);
			sealerInfoService.updateSealer(temp);
		}catch(Exception e){
			jsonData="FAIL";
		}
		return SUCCESS;
	}
	
	@Action(value = "addSalesQueryInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
    public String addSalesQueryInfo() throws Exception{
		jsonData="SUCCESS";
		try{
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			salesQueryInfo.setCustomerName(salesQueryInfo.getFirstName()+" "+salesQueryInfo.getLastName());
			salesQueryInfo.setCreateDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			salesQueryInfo.setCreateUser(sealerInfo.getAutelId());
			salesQueryInfoService.saveSalesQuery(salesQueryInfo);
		}catch(Exception e){
			jsonData="FAIL";
		}
		return SUCCESS;
	}
	
	
	@Action(value = "addCustomerProRemark", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
    public String addCustomerProRemark() throws Exception{
		jsonData="SUCCESS";  //TODO
		try{
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			customerProRemark.setCreateDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
			customerProRemark.setCreateUser(sealerInfo.getAutelId());
			customerProRemarkService.addCustomerProRemark(customerProRemark);
		}catch(Exception e){
			jsonData="FAIL";
		}
		return SUCCESS;
	}
	
	
	@Action(value = "updateSalesQueryInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "jsonData" }) })
    public String updateSalesQueryInfo() throws Exception{
		jsonData="SUCCESS";
		try{
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			
			if(null != salesQueryInfo && salesQueryInfo.getStatus().equals("2")){
				salesQueryInfo.setCloseDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
				salesQueryInfo.setCloseUser(sealerInfo.getAutelId());
			}
			salesQueryInfo.setCustomerName(salesQueryInfo.getFirstName()+" "+salesQueryInfo.getLastName());
			salesQueryInfoService.updateSalesQuery(salesQueryInfo);
		}catch(Exception e){
			jsonData="FAIL";
		}
		return SUCCESS;
	}
	
	//客诉获取车型列表
	@Action(value = "getCustomerInfoBySerialNo", results = { @Result(name = SUCCESS, type = "json", params = {
				"root", "jsonData" }) })
	public String getCustomerInfoBySerialNo() throws Exception{
			
		CustomerInfo customerInfo=customerInfoService.getCustBySerialNo3(serialNo);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date proDate = sdf.parse(customerInfo.getProDate());
		Date period =DateUtil.toDate(customerInfo.getProRegTime(), "yyyy-MM-dd");
		
		Date expDate=DateUtil.toDate(customerInfo.getExpTime(), "yyyy-MM-dd");
		customerInfo.setExpTime(new SimpleDateFormat("MM/dd/yyyy").format(expDate));
		customerInfo.setRegTime(new SimpleDateFormat("MM/dd/yyyy").format(period));
		
		int Warrantymonth=customerInfo.getWarrantymonth()==null?0:Integer.parseInt(customerInfo.getWarrantymonth());
		
		if(DateUtil.compareDate(DateUtil.toDate(customerInfo.getProDate(), "yyyy-MM-dd"), DateUtil.toDate(customerInfo.getProRegTime(), "yyyy-MM-dd"))){
			period.setMonth(period.getMonth()+Warrantymonth);
		}else{
			period=DateUtil.toDate(customerInfo.getProDate(), "yyyy-MM-dd");
			period.setMonth(period.getMonth()+Warrantymonth+3);
		}
		customerInfo.setWarrantyDate(new SimpleDateFormat("MM/dd/yyyy").format(period));
		customerInfo.setProDate(new SimpleDateFormat("MM/dd/yyyy").format(proDate));
		
		
		jsonData=JSONArray.fromObject(customerInfo).toString();	
		return SUCCESS;
		}
	
	
}
