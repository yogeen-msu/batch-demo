package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.member.service.IRGAInfoService;
import com.cheriscon.backstage.member.service.ISalesQueryInfoService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.RGAInfo;
import com.cheriscon.common.model.SalesQueryInfo;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

/**
 * 经销商RGA支持实现类
 * @author chenqichuan
 * @version 创建时间：2014-04-08
 */
@Component("rgaManagementWidget")
public class RGAManagementWidget extends RequestParamWidget
{
	
	@Resource
	private IRGAInfoService rgaService;
	@Resource
	private IProductForSealerService productForSealerService;
	private RGAInfo info;
	
	public RGAInfo getInfo() {
		return info;
	}
	public void setInfo(RGAInfo info) {
		this.info = info;
	}


	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String operType=params.get("operationType");
		if(operType.equals("1")){ //分页查询列表
			rgaQueryPage(params,request);
		}
		if(operType.equals("2")){
			List<ProductForSealer> productTypeList;
			try {
				productTypeList = productForSealerService.getProductForSealer();
				this.putData("productTypeList", productTypeList);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		if(operType.equals("3")){  //编辑
			String code=params.get("code");
			try {
				List<ProductForSealer> productTypeList=productForSealerService.getProductForSealer();
				this.putData("productTypeList", productTypeList);
				info=rgaService.getRGAInfoByCode(code);
				this.putData("info", info);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(operType.equals("4")){  //保存
			saveRGA(params,request);
		}
		if(operType.equals("5")){  //修改后保存
			updateRGA(params,request);
		}if(operType.equals("6")){  //客户查看RGA信息
			String code=params.get("code");
			try {
				info=rgaService.getRGAInfoByCode(code);
				this.putData("info", info);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	protected void saveRGA(Map<String, String> params,HttpServletRequest request)
	{
		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
		info.setAddress(params.get("address"));
		info.setCableFlag(params.get("cableFlag"));
		info.setConnectorFlag(params.get("connectorFlag"));
		info.setCompany(params.get("company"));
		info.setCustomerName(params.get("customerName"));
		info.setEmail(params.get("email"));
		info.setOtherPart(params.get("otherPart"));
		info.setProTypeCode(params.get("proTypeCode"));
		info.setReason(params.get("reason"));
		info.setReceiveDate(params.get("receiveDate"));
		info.setReturnNum(params.get("returnNum"));
		info.setSerialNo(params.get("serialNo"));
		info.setSolution(params.get("solution"));
		info.setReturnNum(params.get("returnNum"));
		info.setTelphone(params.get("telphone"));
		info.setTrackingNum(params.get("trackingNum"));
		info.setReturnDate(params.get("returnDate"));
		info.setUpdateStartDate(params.get("updateStartDate"));
		info.setUpdateEndDate(params.get("updateEndDate"));
		info.setRgaDate(params.get("rgaDate")); //20140804新增
		
		String status=params.get("status");
		info.setStatus(params.get("status"));
		if(null != status && status.equals("0")){
			info.setCloseDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
		}
		
		info.setProStartDate(params.get("proStartDate"));
		info.setProEndDate(params.get("proEndDate"));
		
		info.setCreateDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
		info.setCreateUser(sealerInfo.getAutelId());
		try {
			rgaService.saveRGAInfo(info);
			this.showJson("1");
		} catch (Exception e) {
			e.printStackTrace();
			this.showJson("0");
		}
	}
	
	protected void updateRGA(Map<String, String> params,HttpServletRequest request)
	{
		try {
			String code=params.get("code");
			RGAInfo updateRGA= rgaService.getRGAInfoByCode(code);
			if(updateRGA==null){
				this.showJson("0");
			}else{
			
				updateRGA.setAddress(params.get("address"));
				updateRGA.setCableFlag(params.get("cableFlag"));
				updateRGA.setConnectorFlag(params.get("connectorFlag"));
				updateRGA.setCompany(params.get("company"));
				updateRGA.setCustomerName(params.get("customerName"));
				updateRGA.setEmail(params.get("email"));
				updateRGA.setOtherPart(params.get("otherPart"));
				updateRGA.setProTypeCode(params.get("proTypeCode"));
				updateRGA.setReason(params.get("reason"));
				updateRGA.setReceiveDate(params.get("receiveDate"));
				updateRGA.setReturnNum(params.get("returnNum"));
				updateRGA.setSerialNo(params.get("serialNo"));
				updateRGA.setSolution(params.get("solution"));
				updateRGA.setReturnNum(params.get("returnNum"));
				updateRGA.setTelphone(params.get("telphone"));
				updateRGA.setTrackingNum(params.get("trackingNum"));
				updateRGA.setReturnDate(params.get("returnDate"));
				updateRGA.setStatus(params.get("status"));
				updateRGA.setProStartDate(params.get("proStartDate"));
				updateRGA.setProEndDate(params.get("proEndDate"));
				updateRGA.setUpdateStartDate(params.get("updateStartDate"));
				updateRGA.setUpdateEndDate(params.get("updateEndDate"));
				updateRGA.setRgaDate(params.get("rgaDate"));
				
				String status=params.get("status");
				if(null != status && status.equals("0")){
					updateRGA.setCloseDate(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
				}
				
				
				updateRGA.setProStartDate(params.get("proStartDate"));
				updateRGA.setProEndDate(params.get("proEndDate"));
				rgaService.updateRGAInfo(updateRGA);
			this.showJson("1");
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.showJson("0");
		}
	}
	
	protected void showJson(String json){
		this.disableCustomPage();
		this.setPageFolder("/commons/");
		this.freeMarkerPaser.setPageName("json");
		this.putData("json", json);
		 
	}

	protected void rgaQueryPage(Map<String, String> params,HttpServletRequest request)
	{
		String pageSize = params.get("pageSize");
		String pageNoStr = params.get("pageno");
		String proTypeCode=params.get("proTypeCode");
		String code=params.get("code");
		String serialNo=params.get("serialNo");
		
		info=new RGAInfo();
		
		
		if(!StringUtil.isEmpty(proTypeCode)){
			info.setProTypeCode(proTypeCode);
		}
		if(!StringUtil.isEmpty(serialNo)){
			info.setSerialNo(serialNo);
		}
		if(!StringUtil.isEmpty(code)){
			info.setCode(code);
		}
		
		
		this.putData("proTypeCode", proTypeCode);
		this.putData("status", code);
		this.putData("serialNo", serialNo);
		
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		SealerInfo sealerInfo = (SealerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		try {
			Page dataPage =	rgaService.pageRGAQuery(sealerInfo.getAuth(),info, pageNo, Integer.parseInt(pageSize));
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
			String page_html=pagerHtmlBuilder.buildPageHtml();
			long totalPageCount = dataPage.getTotalPageCount();
			long totalCount = dataPage.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			this.putData("rgaInfo", dataPage.getResult());
			List<ProductForSealer> productTypeList=productForSealerService.getProductForSealer();
			this.putData("productTypeList", productTypeList);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
