package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.member.service.ISalesQueryInfoService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.SalesQueryInfo;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

/**
 * 经销商售前支持实现类
 * @author chenqichuan
 * @version 创建时间：2014-04-07
 */
@Component("salesQueryWidget")
public class SalesQueryWidget extends RequestParamWidget
{
	
	@Resource
	private ISalesQueryInfoService salesQueryInfoService;
	@Resource
	private IProductForSealerService productForSealerService;
	
	private SalesQueryInfo salesQueryInfo;
	
	public SalesQueryInfo getSalesQueryInfo() {
		return salesQueryInfo;
	}

	public void setSalesQueryInfo(SalesQueryInfo salesQueryInfo) {
		this.salesQueryInfo = salesQueryInfo;
	}


	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String operType=params.get("operationType");
		if(operType.equals("1")){ //分页查询列表
			salesQueryPage(params,request);
		}
		if(operType.equals("2")){
			List<ProductForSealer> productTypeList;
			try {
				productTypeList = productForSealerService.getProductForSealer();
				this.putData("productTypeList", productTypeList);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		if(operType.equals("3")){  //编辑
			String code=params.get("code");
			try {
				List<ProductForSealer> productTypeList=productForSealerService.getProductForSealer();
				this.putData("productTypeList", productTypeList);
				salesQueryInfo=salesQueryInfoService.getSalesQueryInfoByCode(code);
				this.putData("salesQueryInfo", salesQueryInfo);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(operType.equals("4")){ //关闭
			
			String code=params.get("code");
			try {
				salesQueryInfo=salesQueryInfoService.getSalesQueryInfoByCode(code);
				this.putData("salesQueryInfo", salesQueryInfo);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	

	protected void salesQueryPage(Map<String, String> params,HttpServletRequest request)
	{
		String pageSize = params.get("pageSize");
		String pageNoStr = params.get("pageno");
		String proTypeCode=params.get("proTypeCode");
		String status=params.get("status");
		String customerName=params.get("customerName");
		
		salesQueryInfo=new SalesQueryInfo();
		
		
		if(!StringUtil.isEmpty(proTypeCode)){
			salesQueryInfo.setProTypeCode(proTypeCode);
		}
		if(!StringUtil.isEmpty(customerName)){
			salesQueryInfo.setCustomerName(customerName);
		}
		if(!StringUtil.isEmpty(status)){
			salesQueryInfo.setStatus(status);
		}
		
		
		this.putData("proTypeCode", proTypeCode);
		this.putData("status", status);
		this.putData("customerName", customerName);
		
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		SealerInfo sealerInfo = (SealerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		try {
			Page dataPage =	salesQueryInfoService.pageSalesQuery(sealerInfo.getAuth(),salesQueryInfo, pageNo, Integer.parseInt(pageSize));
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
			String page_html=pagerHtmlBuilder.buildPageHtml();
			long totalPageCount = dataPage.getTotalPageCount();
			long totalCount = dataPage.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			this.putData("salesQueryInfo", dataPage.getResult());
			List<ProductForSealer> productTypeList=productForSealerService.getProductForSealer();
			this.putData("productTypeList", productTypeList);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
