package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * 我的账户(经销商)控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Component("welcomeWidget")
public class WelcomeWidget extends RequestParamWidget
{
	
	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		
		SealerInfo sealerInfo = (SealerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		
		this.putData("lastLoginTime", sealerInfo.getLastLoginTime());
		this.putData("autelId", sealerInfo.getAutelId());
		this.putData("userName", sealerInfo.getName());
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
