package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.common.model.SaleInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.service.ISaleInfoService;

/**
 * 经销商销售信息控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Component("saleInfoWidget")
public class SaleInfoWidget extends RequestParamWidget
{

	
	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private ISaleInfoService saleInfoService;
	


	@Override
	protected void display(Map<String, String> params) 
	{
		
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
		
		if(sealerInfo == null)
		{
			return;
		}
		
		String saleAutelId = sealerInfo.getAutelId();
		String pageSize = params.get("pageSize");
		String pageNoStr = params.get("pageno");
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		String productName = request.getParameter("productName");
		
		try
		{
			Page dataPage  = saleInfoService.querySaleInfoPage(saleAutelId,productName, 
										pageNo, Integer.parseInt(pageSize));
			
			//统计销货数量
			SaleInfo saleInfo = saleInfoService.queryTotalValue(saleAutelId,productName);
			
			
			
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
			String page_html=pagerHtmlBuilder.buildPageHtml();
			long totalPageCount = dataPage.getTotalPageCount();
			long totalCount = dataPage.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			this.putData("saleInfo", saleInfo); 
			this.putData("saleInoList", dataPage.getResult());
			this.putData("productName",productName);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
