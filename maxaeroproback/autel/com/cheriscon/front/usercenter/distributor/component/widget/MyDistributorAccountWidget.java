package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.content.service.IMessageService;
import com.cheriscon.backstage.content.service.IUserMessageService;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.ISaleInfoService;

/**
 * 我的账户(经销商)控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Component("myDistributorAccountWidget")
public class MyDistributorAccountWidget extends RequestParamWidget
{

	
	@Resource
	private ISaleInfoService saleInfoService;
	
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;
	
	@Resource 
	private IUserMessageService userMessageService;
	
	@Resource
	private IProductTypeService productTypeService ; 
	
	@Resource
	private IMessageService messageService;
	
	@Override
	protected void display(Map<String, String> params)
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
		
		if(sealerInfo == null)
		{
			return;
		}
		

		try 
		{
			this.putData("saleInfoCount", saleInfoService.querySaleInfoCount(sealerInfo.getAutelId()));
			this.putData("customerComplaintCount", customerComplaintInfoService.queryCustomerComplaintCount(
													FrontConstant.DISTRIBUTOR_USER_TYPE, sealerInfo.getAutelId()));
			this.putData("userMessageCount", userMessageService.queryUserMessageCount(
													FrontConstant.DISTRIBUTOR_USER_TYPE, sealerInfo.getLanguageCode(),sealerInfo.getCode()));
			this.putData("messageList",messageService.queryUserMessage(FrontConstant.DISTRIBUTOR_USER_TYPE, sealerInfo.getLanguageCode(),sealerInfo.getCode()));
			
			this.putData("saleInfoCount", saleInfoService.querySaleInfoCount(sealerInfo.getAutelId()));
			this.putData("productTypeCount",productTypeService.queryProductTypeCount(sealerInfo.getCode()));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		
		
	}

	@Override
	protected void config(Map<String, String> params)
	{
		// TODO Auto-generated method stub
		
	}

}
