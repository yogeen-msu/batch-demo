package com.cheriscon.front.usercenter.distributor.component.widget;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;

/**
 * 经销商权限管理控制器实现类
 * @author yangpinggui
 * @version 创建时间：2014-02-27
 */
@Component("sealerAuthWidget")
public class SealerAuthWidget extends RequestParamWidget
{
	
	@Resource
	private ISealerDataAuthDeailsService sealerDataAuthDeailsService;
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;
	@Resource
	private ISealerAuthService sealerAuthService;
	@Resource
	private ISealerInfoService sealerInfoService;
	
	private List<SealerDataAuthDeails> sealerDataList;
	
	public List<SealerDataAuthDeails> getSealerDataList() {
		return sealerDataList;
	}
	public void setSealerDataList(List<SealerDataAuthDeails> sealerDataList) {
		this.sealerDataList = sealerDataList;
	}
	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		SealerInfo sealerInfo = (SealerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		
		String operType=params.get("operationType");
		if(operType.equals("1")){
			String pageSize = params.get("pageSize");
			String pageNoStr = params.get("pageno");
			Integer[] ids = this.parseId();
			Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
			String sealerCode=params.get("sealerCode");
			
			try {
				Page dataPage =	sealerDataAuthDeailsService.getAuthSealer(sealerCode,sealerInfo.getAuth(), sealerInfo.getAutelId(), pageNo, Integer.parseInt(pageSize));
				StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
				String page_html=pagerHtmlBuilder.buildPageHtml();
				long totalPageCount = dataPage.getTotalPageCount();
				long totalCount = dataPage.getTotalCount();
				this.putData("pager", page_html); 
				this.putData("pagesize", pageSize);
				this.putData("pageno", pageNo);
				this.putData("totalcount", totalCount);
				this.putData("totalpagecount",totalPageCount);
				this.putData("authList", dataPage.getResult());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(operType.equals("2")){
			
		}
		if(operType.equals("3")){ //查看客诉列表
			queryCustomerComplaintInfoPage(params,request);
		}
		if(operType.equals("4")){ //查看客户已有的权限，列出该经销商账号有的所有权限
			querySealerAuth(params,request);
		}
		
	}
	
	protected void querySealerAuth(Map<String, String> params,HttpServletRequest request){
		String autelId=request.getParameter("autelId");
		
		try {
			SealerInfo sealerInfo = sealerInfoService.getSealerByAutelId(autelId);
			SealerInfo sealerInfo2 = (SealerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
			if(sealerInfo!=null){
				List<SealerAuthVO> listAuth=sealerAuthService.list(sealerInfo.getCode());
				List<SealerAuthVO> listAuth2=sealerAuthService.list(sealerInfo2.getCode());
				List<SealerAuthVO> newList=new ArrayList<SealerAuthVO>();
				for(int i=0;i<listAuth2.size();i++){
					SealerAuthVO temp=listAuth2.get(i);
					
					for(int j=0;j<listAuth.size();j++){
						SealerAuthVO temp2=listAuth.get(j);
						if(temp2.getName().equals(temp.getName())){
							temp.setCheckedFlag("Y");
							break;
						}else{
							temp.setCheckedFlag("N");
						}
					}
					newList.add(temp);
				}
				
				this.putData("autelId",autelId);
				this.putData("newList",newList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

	protected void queryCustomerComplaintInfoPage(Map<String, String> params,HttpServletRequest request)
	{
		String pageSize = params.get("pageSize");
		String userType = "2";
		String timeRange = "1";
		String complaintState = request.getParameter("complaintState");
		String code=request.getParameter("sealerCode");
		if (complaintState == null || complaintState.equals("")) 
		{
			complaintState = String.valueOf(FrontConstant.QUERY_ALL);
		}
		SealerInfo sealerInfo = (SealerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		String pageNoStr = params.get("pageno");
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		
		try
		{
			Page dataPage  = customerComplaintInfoService.queryCustomerComplaintInfoPage3(Integer.parseInt(userType),Integer.parseInt(timeRange),Integer.parseInt(complaintState), code, 
									pageNo, Integer.parseInt(pageSize));
			
			sealerDataList=sealerDataAuthDeailsService.getAuthSealerList(sealerInfo.getAuth(), sealerInfo.getAutelId());
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
			String page_html=pagerHtmlBuilder.buildPageHtml();
			long totalPageCount = dataPage.getTotalPageCount();
			long totalCount = dataPage.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			this.putData("customerComplaintInfoList", dataPage.getResult());
			this.putData("userType", userType);
			this.putData("timeRange",timeRange);
			this.putData("sealerCode",code);
			this.putData("sealerDataList",sealerDataList);
			this.putData("complaintState",complaintState);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
