package com.cheriscon.front.usercenter.distributor.component.widget;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadFile;

import org.springframework.stereotype.Component;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.member.service.IComplaintAdminuserInfoService;
import com.cheriscon.backstage.member.service.ICustomerComplaintTypeNameService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.ComplaintAdminuserInfo;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.MultipartRequestWrapper;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;

/**
 * 客诉信息控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Component("salerComplaintInfoWidget")
public class salerComplaintInfoWidget extends RequestParamWidget
{
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;
	
	@Resource
	private IReCustomerComplaintInfoService reCustomerComplaintInfoService;
	
	@Resource
	private ICustomerInfoService customerInfoService;
	
	@Resource
	private ISealerInfoService sealerInfoService;
	
	@Resource
	private ICustomerComplaintTypeNameService customerComplaintTypeNameService;
	
	@Resource
	private IComplaintAdminuserInfoService complaintAdminuserInfoService;
	
	@Resource
	private ILanguageService languageService;
	
	@Resource
	private ISmtpManager smtpManager;

	@Resource
	private EmailProducer emailProducer;
	
	@Resource
	private IEmailTemplateService emailTemplateService; 
	
	/**
	 * 客诉添加或回复成功
	 */
	private static final String CUSTOMERCOMPLAINT_SUCCESS = "4";
	
	/**
	 * 客诉添加或回复失败
	 */
	private static final String CUSTOMERCOMPLAINT_FAIL = "3";
	
	/**
	 * 客诉添加或回复上传文件大小不能超过4 M
	 * 
	 */
	private static final String CUSTOMERCOMPLAINT_FILE_SIZE = "2";


	/**
	 * 客诉添加或回复上传文件格式的限制
	 * 
	 */
	private static final String CUSTOMERCOMPLAINT_FILE_TYPE = "1";
	
	public boolean cacheAble()
	{
		return false;
	}

	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		
		String operationType = params.get("operationType");
		
		if (operationType == null) 
		{
			return;
		}

		if (Integer.parseInt(operationType) == 1)// 查询我的客诉记录信息
		{
			queryCustomerComplaintInfoPage(params, request);
		} 
		else if (Integer.parseInt(operationType) == 2)// 进入添加客诉页面
		{
			try 
			{
				goAddCustomerComplaintPage(request);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		} 
		else if (Integer.parseInt(operationType) == 3)// 查看客诉与回复页面
		{
			queryCustomerAndComplaintDetail(request);
		} 
		else if (Integer.parseInt(operationType) == 4)// 客诉内容回复
		{
			replyCustomerComplaint(params,request);
		} 
		else if (Integer.parseInt(operationType) == 5)// 查看客诉详情
		{
			queryCustomerComplaintInfoDetail(request);
		} 
		else if (Integer.parseInt(operationType) == 6)// 添加客诉
		{
			addCustomerComplaintInfo(params,request);
		}
		else if (Integer.parseInt(operationType) == 7) //客诉分类
		{
			this.putData("userType",request.getParameter("userType"));
			this.putData("complaintType",params.get("complaintType"));
			
			String languageCode = null;
			
			if (!StringUtil.isEmpty(request.getParameter("userType"))) 
			{
				if (Integer.parseInt(request.getParameter("userType")) == FrontConstant.CUSTOMER_USER_TYPE) 
				{
					languageCode = ((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getLanguageCode();
				}
				else if (Integer.parseInt(request.getParameter("userType")) == FrontConstant.DISTRIBUTOR_USER_TYPE) 
				{
					languageCode = ((SealerInfo) SessionUtil.getLoginUserInfo(request)).getLanguageCode();
				}
				
				try 
				{
					this.putData("customerComplaintTypeList",customerComplaintTypeNameService.
																listComplaintTypeNameByLanguageCode(languageCode));
					
					String complaintTypeCode="";
					
					for(int i = 0 ; i < customerComplaintTypeNameService.
									listComplaintTypeNameByLanguageCode(languageCode).size();i++)
					{
						complaintTypeCode =customerComplaintTypeNameService.
								listComplaintTypeNameByLanguageCode(languageCode).get(i).getComplaintTypeCode()+","+ complaintTypeCode  ;
					}
					
					this.putData("complaintTypeCode",complaintTypeCode);
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			
		}
		else if(Integer.parseInt(operationType) == 8)//查看客诉表格详情
		{
			queryCustomerComplaintTableDetail(request);
		}
		else if(Integer.parseInt(operationType) == 9)//更新客诉状态
		{
			updateCustomerComplaintInfoState(request);
		}
		//获取系统时间
		Calendar calendar=Calendar.getInstance();
		String time=new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
		
		this.putData("cusCompaintdate",time);
	}
	
	/**
	 * 添加客诉
	 * @param params
	 */
	@SuppressWarnings("unchecked")
	private void addCustomerComplaintInfo(Map<String, String> params,HttpServletRequest request)
	{
		
		try 
		{
		
			String filePath = fileUpLoad(params);
			
			//上传文件大小超过4M或上传文件失败
			//上传文件大小超过4M或上传文件失败
			if(filePath.equals(CUSTOMERCOMPLAINT_FILE_TYPE)
					|| filePath.equals(CUSTOMERCOMPLAINT_FILE_SIZE)
					|| filePath.equals(CUSTOMERCOMPLAINT_FAIL))
			{
				this.showJson(filePath);
			}
			else
			{
				CustomerComplaintInfo  newCustomerComplaintInfo  = new CustomerComplaintInfo();
				newCustomerComplaintInfo.setComplaintTypeCode(params.get("complaintTypeCode"));
				newCustomerComplaintInfo.setComplaintTitle(params.get("complaintTitle"));
				newCustomerComplaintInfo.setComplaintPerson(params.get("complaintPerson"));
				newCustomerComplaintInfo.setCompany(params.get("company"));
				newCustomerComplaintInfo.setComplaintDate(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
				newCustomerComplaintInfo.setPhone(params.get("phone"));
				newCustomerComplaintInfo.setEmail(params.get("email"));
				newCustomerComplaintInfo.setVehicleSystem(params.get("vehicleSystem"));
				newCustomerComplaintInfo.setVehicleType(params.get("vehicleType"));
				newCustomerComplaintInfo.setDiagnosticConnector(params.get("diagnosticConnector"));
				newCustomerComplaintInfo.setCarYear(params.get("carYear"));
				newCustomerComplaintInfo.setCarnumber(params.get("carnumber"));
				newCustomerComplaintInfo.setEngineType(params.get("engineType"));
				newCustomerComplaintInfo.setSystem(params.get("system"));
				newCustomerComplaintInfo.setOther(params.get("other"));
				newCustomerComplaintInfo.setProductName(params.get("productName"));
				newCustomerComplaintInfo.setProductNo(params.get("productNo"));
				newCustomerComplaintInfo.setSoftwareName(params.get("softwareName"));
				newCustomerComplaintInfo.setSoftwareEdition(params.get("softwareEdition"));
				newCustomerComplaintInfo.setTestPath(params.get("testPath"));
				newCustomerComplaintInfo.setComplaintContent(params.get("complaintContent"));
				newCustomerComplaintInfo.setEmergencyLevel(Integer.parseInt(params.get("emergencyLevel")));
				newCustomerComplaintInfo.setComplaintState(FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE);
				newCustomerComplaintInfo.setComplaintEffective(FrontConstant.COMPLAINT_YES_STATE);
				newCustomerComplaintInfo.setUserType(Integer.parseInt(params.get("userType")));
				newCustomerComplaintInfo.setUserCode(params.get("userCode"));
				
				//没有上传附件处理
				if(filePath.equals(CUSTOMERCOMPLAINT_FILE_TYPE)||filePath.equals(CUSTOMERCOMPLAINT_FILE_SIZE) ||
						filePath.equals(CUSTOMERCOMPLAINT_FAIL)||filePath.equals(CUSTOMERCOMPLAINT_SUCCESS))
				{
					filePath = null;
				}
				
				newCustomerComplaintInfo.setAttachment(filePath);
				
				ComplaintAdminuserInfo complaintAdminuserInfo = complaintAdminuserInfoService.getComplaintAdminuserInfoBycomplaintTypeCode(params.get("complaintTypeCode"));
				if(complaintAdminuserInfo != null)
					newCustomerComplaintInfo.setAcceptor(complaintAdminuserInfo.getAdminUserid());
					
				customerComplaintInfoService.addCustomerComplaintInfo(newCustomerComplaintInfo);
				
				String autelId = "";
				String languageCode = "";
				String userName="";
				if(!StringUtil.isEmpty(params.get("userType")))
				{
					if(Integer.parseInt(params.get("userType")) == FrontConstant.CUSTOMER_USER_TYPE)
					{
						CustomerInfo customerInfo = ((CustomerInfo) SessionUtil.getLoginUserInfo(request));
						autelId = customerInfo.getAutelId();
						languageCode = customerInfo.getLanguageCode();
						if(customerInfo.getName()!=null){
							userName=customerInfo.getName();
						}
					}
					else if(Integer.parseInt(params.get("userType")) == FrontConstant.DISTRIBUTOR_USER_TYPE)
					{
						SealerInfo sealerInfo = ((SealerInfo) SessionUtil.getLoginUserInfo(request));
						autelId = sealerInfo.getEmail();
						languageCode = sealerInfo.getLanguageCode();
						if(sealerInfo.getName()!=null){
							userName=sealerInfo.getName();
						}
					}
				}
				
				Language lanuage=languageService.getByCode(languageCode);
				String status=FrontConstant.getComplaintStatus(lanuage.getCountryCode(), FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE);
				
				
				Smtp smtp = smtpManager.getCurrentSmtp( );
				
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
			
				emailModel.getData().put("autelId", autelId);
				emailModel.getData().put("userName", userName);
				emailModel.getData().put("ticketId", newCustomerComplaintInfo.getCode());
				emailModel.getData().put("subject", newCustomerComplaintInfo.getComplaintTitle());
				
				String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");;
				String url = requestUrl+ "/queryCustomerComplaintDetail.html?operationType=3&userType="
								 + Integer.parseInt(params.get("userType")) + "&code="+newCustomerComplaintInfo.getCode();
				emailModel.getData().put("url", url);
				
				emailModel.getData().put("status", status);
				
				emailModel.setTitle(newCustomerComplaintInfo.getComplaintTitle());
				emailModel.setTo(newCustomerComplaintInfo.getEmail());
				
				EmailTemplate template = emailTemplateService.
											getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_ADD_COMPLAINT_SUCCESS,
																			languageCode);
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				emailProducer.send(emailModel);
				
				this.showJson(CUSTOMERCOMPLAINT_SUCCESS);
			}
		} 
		//{complaintType=异常处理, username=it@auteltech.net, ticketId=cuc201305051525460206, host=smtp.ym.163.com, subject=AAAABB, autelId=527690766@qq.com, password=leisure, url=http://localhost:8086/autelproweb/queryCustomerComplaintDetail.html?operationType=3&userType=1&code=cuc201305051525460206}
		catch (Exception e) 
		{
			this.showJson(CUSTOMERCOMPLAINT_FAIL);
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 文件上传处理
	 * @param params
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String fileUpLoad(Map<String, String> params) 
	{
		
		//先上传图片
		String faceField = "uploadFile";
		String subFolder = "complaintinfo";
		HttpServletRequest requestUpload = ThreadContextHolder.getHttpRequest();
		
		if (MultipartFormDataRequest.isMultipartFormData(requestUpload))
		{
			try
			{
				
				String encoding = CopSetting.ENCODING;
				
				if(StringUtil.isEmpty(encoding))
				{
					encoding = "UTF-8";
				}
				
				MultipartFormDataRequest mrequest  = new MultipartFormDataRequest(requestUpload,null,1000*1024*1024,MultipartFormDataRequest.COSPARSER,encoding);
				Enumeration<String> enumer = mrequest.getParameterNames();
				while (enumer.hasMoreElements()) 
				{
					String key = enumer.nextElement();
					String value = mrequest.getParameter(key);
					params.put(key, value);
				}
				
				requestUpload = new MultipartRequestWrapper(requestUpload,mrequest);
				ThreadContextHolder.setHttpRequest(requestUpload);
				
				Hashtable files = mrequest.getFiles();
				UploadFile file = (UploadFile) files.get(faceField);
				
				if(file.getInpuStream() != null)
				{
					String fileFileName = file.getFileName();
					
					
				
					//判断文件类型
					String allowTYpe = "doc,docx,xls,xlsx,txt,rar,zip,gif,png,jpg,pdf,bmp,jpeg";
					if (!fileFileName.trim().equals("") && fileFileName.length() > 0) 
					{
						String ex = fileFileName.substring(fileFileName.lastIndexOf(".") + 1, fileFileName.length());
						if(allowTYpe.toString().indexOf(ex.toLowerCase()) < 0)
						{
							return CUSTOMERCOMPLAINT_FILE_TYPE;
						}
					}
					
					//判断文件大小
					if(file.getFileSize() > 4000 * 1024)
					{
						return CUSTOMERCOMPLAINT_FILE_SIZE;
					}
					
					String fileName = null;
					String filePath = ""; 
			 
					String ext = FileUtil.getFileExt(fileFileName);
					
					fileName = DateUtil.toString(new Date(), "yyyyMMddHHmmss") + StringUtil.getRandStr(4) + "." + ext;
					
					filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/";
					
					if(subFolder!=null)
					{
						filePath+=subFolder +"/";
					}
					
					String path  = "/attachment/" +(subFolder==null?"":subFolder)+ "/"+fileName;
				 
					filePath += fileName;
					FileUtil.createFile(file.getInpuStream(), filePath);
					return path;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				return CUSTOMERCOMPLAINT_FAIL;
			}
		}
		
		return CUSTOMERCOMPLAINT_SUCCESS;

	}
	

	
	/**
	 * 查询我的客诉信息记录
	 * @param params
	 * @param request
	 */
	protected void queryCustomerComplaintInfoPage(Map<String, String> params,HttpServletRequest request)
	{
		String pageSize = params.get("pageSize");
		String userType = params.get("userType");
		String timeRange = request.getParameter("timeRange");
		String complaintState = request.getParameter("complaintState");
		String code="";
		
		if(userType == null)
		{
			return;
		}
		String authCode="";
		//根据用户类型获取对应用户信息
		if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)//个人用户
		{
			CustomerInfo customerInfo =  (CustomerInfo) SessionUtil.getLoginUserInfo(request);
			code = customerInfo.getCode();
		}
		else if(Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)//经销商用户
		{
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			code = sealerInfo.getAutelId();
			authCode=sealerInfo.getAuth();
		}
		
		
		//默认查询最近3个月
		if (timeRange == null || timeRange.equals("")) 
		{
			timeRange = String.valueOf(FrontConstant.LATELY_THREE_MONTH);
		}
		
		
		if (complaintState == null || complaintState.equals("")) 
		{
			complaintState = String.valueOf(FrontConstant.QUERY_ALL);
		}

		
		String pageNoStr = params.get("pageno");
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		
		try
		{
			Page dataPage  = customerComplaintInfoService.queryCustomerComplaintInfoPage2(authCode,Integer.parseInt(userType),Integer.parseInt(timeRange),Integer.parseInt(complaintState), code, 
									pageNo, Integer.parseInt(pageSize));
			
			//List<CustomerComplaintInfo> complaintStateList = customerComplaintInfoService.queryCustomerComplaintStateList(Integer.parseInt(userType),Integer.parseInt(complaintState),code);
			
			
		/*	int openComplaintState = 0;    // 客诉状态-打开
			int guaqiComplaintState = 0;    //客诉状态-挂起
			int waitComplaintState = 0;    //客诉状态-等待
			int closeComplaintState = 0;    // 客诉状态-关闭
			
			if(complaintStateList.size() > 0)
			{
				for(int i = 0 ; i < complaintStateList.size() ; i++)
				{
					CustomerComplaintInfo customerComplaintInfo = (CustomerComplaintInfo) complaintStateList.get(i);
					
					if(customerComplaintInfo.getComplaintState() == FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE)
					{
						openComplaintState = customerComplaintInfo.getComplaintStateCount();
					}
					else if(customerComplaintInfo.getComplaintState() == FrontConstant.CUSTOMER_COMPLAINT_GUAQI_STATE)
					{
						guaqiComplaintState = customerComplaintInfo.getComplaintStateCount();
					}
					else if(customerComplaintInfo.getComplaintState() == FrontConstant.CUSTOMER_COMPLAINT_WAIT_STATE)
					{
						waitComplaintState = customerComplaintInfo.getComplaintStateCount();
					}
					else if(customerComplaintInfo.getComplaintState() == FrontConstant.CUSTOMER_COMPLAINT_CLOSE_STATE)
					{
						closeComplaintState = customerComplaintInfo.getComplaintStateCount();
					}
				}
			}*/
			
	
			
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, dataPage.getTotalCount(), Integer.valueOf(pageSize));
			String page_html=pagerHtmlBuilder.buildPageHtml();
			long totalPageCount = dataPage.getTotalPageCount();
			long totalCount = dataPage.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
			this.putData("customerComplaintInfoList", dataPage.getResult());
			this.putData("userType", userType);
/*			this.putData("openComplaintState", openComplaintState);
			this.putData("guaqiComplaintState", guaqiComplaintState);
			this.putData("waitComplaintState", waitComplaintState);
			this.putData("closeComplaintState", closeComplaintState);
			this.putData("complaintStateListSize", complaintStateList.size());*/
			this.putData("timeRange",timeRange);
			this.putData("complaintState",complaintState);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 进入添加客诉页面
	 * @param request
	 * @throws Exception 
	 */
	protected void goAddCustomerComplaintPage(HttpServletRequest request) throws Exception
	{
		String userType = request.getParameter("userType");
		String userCode = "";
		
		/**
		 * 投诉人
		 */
		String userName = "";
		
		/**
		 * 单位
		 */
		String company = "";
		
		/**
		 * 电话
		 */
		String phone="";
		
	
		String daytimePhoneCC="";
		String daytimePhoneAC="";
		String daytimePhone="";
		
		/**
		 * 邮箱
		 */
		String email = "";
		
		if(!StringUtil.isEmpty(userType))
		{
			if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)
			{
				userCode = ((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getCode();
				
				CustomerInfo customerInfo = customerInfoService.getCustomerByCode(userCode);
				company = customerInfo.getCompany();
				userName = customerInfo.getAutelId();
				
				if(StringUtil.isEmpty(customerInfo.getDaytimePhoneCC()))
				{
					daytimePhoneCC="";
				}
				
				if(StringUtil.isEmpty(customerInfo.getDaytimePhoneAC()))
				{
					daytimePhoneAC="";
				}
				
				if(StringUtil.isEmpty(customerInfo.getDaytimePhone()))
				{
					daytimePhone="";
				}
				
				email = customerInfo.getAutelId();
			}
			else if(Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)
			{
				userCode = ((SealerInfo) SessionUtil.getLoginUserInfo(request)).getCode();
				SealerInfo sealerInfo = sealerInfoService.getSealerByCode(userCode);
				company = sealerInfo.getCompany();
				userName = sealerInfo.getAutelId();
				
				if(StringUtil.isEmpty(sealerInfo.getDaytimePhoneCC()))
				{
					daytimePhoneCC="";
				}
				
				if(StringUtil.isEmpty(sealerInfo.getDaytimePhoneAC()))
				{
					daytimePhoneAC="";
				}
				
				if(StringUtil.isEmpty(sealerInfo.getDaytimePhone()))
				{
					daytimePhone="";
				}
				email = sealerInfo.getEmail();
			}
		}
	
		this.putData("userType",request.getParameter("userType"));
		this.putData("userCode", userCode);
		this.putData("company", company);
		this.putData("userName", userName);
		
		phone = daytimePhoneCC+daytimePhoneAC+daytimePhone;
		
		this.putData("phone", phone);
		this.putData("email", email);
		this.putData("complaintType",request.getParameter("complaintType"));
	}
	
	/**
	 * 查看客诉表格详情
	 * @param request
	 */
	protected void queryCustomerComplaintTableDetail(HttpServletRequest request)
	{
		String code = request.getParameter("code");

		
		try 
		{
			CustomerComplaintInfo customerComplaintInfo = customerComplaintInfoService.
																getCustomerComplaintInfoByCode(code);
			
			if(customerComplaintInfo == null)
			{
				return;
			}

			this.putData("customerComplaintInfo",customerComplaintInfo);
			this.putData("userType",request.getParameter("userType"));
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 查看客诉与回复
	 * @param request
	 */
	protected void queryCustomerAndComplaintDetail(HttpServletRequest request)
	{
		String code = request.getParameter("code");
		String autelId = "";
		String userType = request.getParameter("userType");
		
		if(!StringUtil.isEmpty(userType))
		{
			if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)
			{
				autelId = ((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getAutelId();
			}
			else if(Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)
			{
				autelId = ((SealerInfo) SessionUtil.getLoginUserInfo(request)).getAutelId();
			}
		}
		
		try 
		{
			CustomerComplaintInfo customerComplaintInfo = customerComplaintInfoService.
																getCustomerComplaintInfoByCode(code);
	
			
			
			this.putData("autelId", autelId);
			this.putData("userType", userType);
			this.putData("customerComplaintInfo",customerComplaintInfo);
			this.putData("reCustomerComplaintInfoList", reCustomerComplaintInfoService.
														queryReCustomerComplaintInfoByCode(code));
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 进入客诉详情页面进行客诉回复操作
	 * @param request
	 */
	protected void replyCustomerComplaint(Map<String, String> params,HttpServletRequest request)
	{
		
		String filePath = fileUpLoad(params);
		
		//上传文件大小超过4M或上传文件失败
		if(filePath.equals(CUSTOMERCOMPLAINT_FILE_TYPE)
				|| filePath.equals(CUSTOMERCOMPLAINT_FILE_SIZE)|| filePath.equals(CUSTOMERCOMPLAINT_FAIL))
		{
			this.showJson(filePath);
		}
		else
		{
			ReCustomerComplaintInfo reCustomerComplaintInfo = new ReCustomerComplaintInfo();
			reCustomerComplaintInfo.setCustomerComplaintCode(params.get("cusComCode"));
			reCustomerComplaintInfo.setRePersonType(Integer.parseInt(params.get("rePersonType")));
			reCustomerComplaintInfo.setReContent(params.get("reContent"));
			reCustomerComplaintInfo.setReDate(DateUtil.toString(new Date(),FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
			
			//没有上传附件处理
			if(filePath.equals(CUSTOMERCOMPLAINT_FILE_TYPE)||filePath.equals(CUSTOMERCOMPLAINT_FILE_SIZE) ||
					filePath.equals(CUSTOMERCOMPLAINT_FAIL)||filePath.equals(CUSTOMERCOMPLAINT_SUCCESS))
			{
				filePath = null;
			}
			
			reCustomerComplaintInfo.setAttachment(filePath);
		
			
			String userType = params.get("userType");
			
			if(userType != null)
			{
				if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)
				{
					reCustomerComplaintInfo.setRePerson(((CustomerInfo)SessionUtil.getLoginUserInfo(request)).getAutelId());
				}
				else if (Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)
				{
					reCustomerComplaintInfo.setRePerson(((SealerInfo)SessionUtil.getLoginUserInfo(request)).getAutelId());
				}
			}
			
			CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();
			customerComplaintInfo.setCode(params.get("cusComCode"));
			customerComplaintInfo.setComplaintState(FrontConstant.CUSTOMER_COMPLAINT_OPEN_STATE);	
			try
			{
				customerComplaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
				reCustomerComplaintInfoService.addReCustomerComplaintInfo(reCustomerComplaintInfo);
				this.showJson(CUSTOMERCOMPLAINT_SUCCESS);
			} 
			catch (Exception e)
			{
				this.showJson(CUSTOMERCOMPLAINT_FAIL);
				e.printStackTrace();
			}
			
		}
		
	}
	
	/**
	 * 更新客诉状态
	 * @param request
	 */
	protected void updateCustomerComplaintInfoState(HttpServletRequest request)
	{
		CustomerComplaintInfo customerComplaintInfo = new CustomerComplaintInfo();
		customerComplaintInfo.setCode(request.getParameter("cusComCode"));
		customerComplaintInfo.setComplaintState(Integer.parseInt(request.getParameter("complaintState")));
		//customerComplaintInfo.setComplaintEffective(Integer.parseInt(request.getParameter("complaintEffective")));
		
		try
		{
			customerComplaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
			this.showJson("true");
		} 
		catch (Exception e)
		{
			this.showJson("false");
			e.printStackTrace();
		}
	}
	
	/**
	 * 查看客诉详情
	 * @param request
	 */
	protected void queryCustomerComplaintInfoDetail(HttpServletRequest request)
	{
		String userType = request.getParameter("userType");
		String code = "";

		
		//根据用户类型获取对应用户信息
		if(Integer.parseInt(userType) == FrontConstant.CUSTOMER_USER_TYPE)//个人用户
		{
			CustomerInfo customerInfo =  (CustomerInfo) SessionUtil.getLoginUserInfo(request);
			code = customerInfo.getCode();

		}
		else if(Integer.parseInt(userType) == FrontConstant.DISTRIBUTOR_USER_TYPE)//经销商用户
		{
			SealerInfo sealerInfo = (SealerInfo) SessionUtil.getLoginUserInfo(request);
			code = sealerInfo.getCode();

		}
		
		try 
		{
			this.putData("customerComplaintInfo",customerComplaintInfoService.getCustomerComplaintInfoByCode(code));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		
	}
	
	


	
}
