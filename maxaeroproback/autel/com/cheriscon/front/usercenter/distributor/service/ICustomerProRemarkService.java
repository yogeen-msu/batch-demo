package com.cheriscon.front.usercenter.distributor.service;

import java.util.List;

import com.cheriscon.common.model.CustomerProRemark;

/**
 * 经销商备注续租信息业务类
 * @author chenqichuan
 * @version 
 */
public interface ICustomerProRemarkService 
{
	public void addCustomerProRemark(CustomerProRemark customerProRemark) 
			throws Exception;
	
	public void updateCustomerProRemark(CustomerProRemark customerProRemark) 
			throws Exception;
	
	public CustomerProRemark getCustomerProRemarkByProCode(String code);
	
	public CustomerProRemark getCustomerProRemarkByAutelId(String autelId);
	
	public CustomerProRemark getCustomerProRemarkById(String id);
	
	public List<CustomerProRemark> getCustomerRemarkListByProCode(String code);
}
