package com.cheriscon.front.usercenter.distributor.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.Message;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.service.IMyMessageInfoService;

/**
 * 我的消息（包含个人用户、经销商用户）逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Service
public class MyMessageInfoServiceImpl extends BaseSupport<Message> 
	implements IMyMessageInfoService
{

	@Override
	public Page queryMyDistributorAccountPage(String userCode, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql= new StringBuffer();
		
		sql.append("select * from DT_Message ");
		
		if(userCode != null && !StringUtil.isEmpty(userCode))
		{
			sql.append(" where userCode ='").append(userCode).append("'");
		}
		
		sql.append("order by creatTime desc");
		
		return this.daoSupport.queryForPage(sql.toString(), pageNo, 
					pageSize, Message.class);
		
	}

}
