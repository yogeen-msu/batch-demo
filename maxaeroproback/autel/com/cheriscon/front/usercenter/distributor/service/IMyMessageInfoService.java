package com.cheriscon.front.usercenter.distributor.service;

import com.cheriscon.framework.database.Page;

/**
 * 我的消息（包含个人用户、经销商用户）逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public interface IMyMessageInfoService 
{
	/**
	 * 根据用户code查询我的信息记录（用户code包含个人用户、经销商用户）
	 * @param userCode 用户code
	 * @param pageNo 页
	 * @param pageSize 大小
	 * @return
	 */
	public Page queryMyDistributorAccountPage(String userCode, int pageNo, int pageSize)throws Exception;
}
