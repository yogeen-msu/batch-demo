package com.cheriscon.front.usercenter.distributor.service;


import java.io.File;

import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.vo.SaleInfoVo;
import com.cheriscon.common.model.SaleInfo;
import com.cheriscon.framework.database.Page;

/**
 * 经销商销售信息业务逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public interface ISaleInfoService 
{
	/**
	 * 根据经销商账号查询经销商的销售信息记录
	 * @param saleAutelId 经销商id
	 * @param productName 产品名称
	 * @param pageNo 页
	 * @param pageSize 大小
	 * @return
	 */
	public Page querySaleInfoPage(String saleAutelId,String productName, int pageNo, int pageSize)
			throws Exception;
	
	/**
	 * 根据经销商code、统计销货数量、销货金额、销退数量、销退金额、销货净量、销货净额数据
	 * @param saleCode
	 * @param totalName
	 * @return
	 */
	public SaleInfo queryTotalValue(String saleAutelId,String productName)throws Exception;
	
	/**
	 * 增加经销商销售信息
	 * @param saleInfo
	 * @throws Exception
	 */
	public void addSaleInfo(SaleInfo saleInfo) throws Exception;

	

	
	/**
	 * 添加经销商销售信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int insertSaleInfo(SaleInfo saleInfo) throws Exception;
	
	/**
	 * 更新经销商销售信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int updateSaleInfo(SaleInfo saleInfo) throws Exception;
	
	/**
	 * 根据编码查询经销商销售信息对象
	 * @param code	经销商销售信息code
	 * @return
	 * @throws Exception
	 */
	public SaleInfo getSaleInfoByCode(String code) throws Exception;
	
	/**
	 * 根据编码删除经销商销售信息对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int delSaleInfo(String code) throws Exception;
	
	/**
	 * 根据删除多个经销商销售信息对象
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public int delSaleInfos(String codes) throws Exception;

	/**
	  * 经销商销售信息分页显示方法
	  * @param SaleInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */
	public Page pageSaleInfoVoPage(SaleInfoVo saleInfoVo, int pageNo,
			int pageSize) throws Exception;
	
	/**
	 * 查询经销商的销售信息记录
	 * @param saleAutelId
	 * @return
	 * @throws Exception
	 */
	public int querySaleInfoCount(String saleAutelId)throws Exception;
	
	/**
	 * 保存excel中经销商销售信息
	 * @throws Exception
	 * @author pengdongan
	 */
	public int saveCreateSaleInfo(File saleInfoFile) throws Exception;
	

}
