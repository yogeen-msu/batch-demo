package com.cheriscon.front.usercenter.distributor.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.common.model.ReCustomerComplaintInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.service.ICustomerComplaintInfoService;
import com.cheriscon.front.usercenter.distributor.service.IReCustomerComplaintInfoService;

/**
 * 客诉回复业务逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Service
public class ReCustomerComplaintInfoServiceImpl extends BaseSupport<ReCustomerComplaintInfo>
	implements IReCustomerComplaintInfoService
{
	@Resource
	private ICustomerComplaintInfoService customerComplaintInfoService;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void replayCustomerComplaintInfo(CustomerComplaintInfo customerComplaintInfo,
			ReCustomerComplaintInfo reCustomerComplaintInfo) throws Exception {
		this.customerComplaintInfoService.updateCustomerComplaintInfo(customerComplaintInfo);
		String code = DBUtils.generateCode(DBConstant.REC_INFO_CODE);
		reCustomerComplaintInfo.setCode(code);
//		reCustomerComplaintInfo.setSourceType(1); //产品网站添加的客诉
		this.daoSupport.insert("DT_ReCustomerComplaintInfo", reCustomerComplaintInfo);
		
	}
	
	@Transactional
	public void addReCustomerComplaintInfo(
			ReCustomerComplaintInfo reCustomerComplaintInfo) throws Exception 
	{
		String code = DBUtils.generateCode(DBConstant.REC_INFO_CODE);
		reCustomerComplaintInfo.setCode(code);
		reCustomerComplaintInfo.setSourceType(1); //产品网站添加的客诉
		this.daoSupport.insert("DT_ReCustomerComplaintInfo", reCustomerComplaintInfo);
	}

	@Override
	public List<ReCustomerComplaintInfo> queryReCustomerComplaintInfoByCode(String cusCode)  throws Exception {
		StringBuffer sql = new StringBuffer();
		sql.append("select id,reContent,reDate,rePerson,rePersonType,complaintScore,attachment,sourceType from DT_ReCustomerComplaintInfo where customerComplaintCode='").append(cusCode).append("' order by reDate Desc");
		
		return this.daoSupport.queryForList(sql.toString(),  new RowMapper()
		{

			int complaintCount = 0;
			public ReCustomerComplaintInfo mapRow(ResultSet rs, int arg1) throws SQLException 
			{
				ReCustomerComplaintInfo reCustomerComplaintInfo = new ReCustomerComplaintInfo();
				reCustomerComplaintInfo.setId(rs.getInt("id"));
				reCustomerComplaintInfo.setReContent(rs.getString("reContent"));
				reCustomerComplaintInfo.setReDate(rs.getString("reDate"));
				reCustomerComplaintInfo.setRePerson(rs.getString("rePerson"));
				reCustomerComplaintInfo.setRePersonType(rs.getInt("rePersonType"));
				reCustomerComplaintInfo.setComplaintScore(rs.getInt("complaintScore"));
				complaintCount++;
				reCustomerComplaintInfo.setComplaintCount(complaintCount);
				reCustomerComplaintInfo.setAttachment(rs.getString("attachment"));
				reCustomerComplaintInfo.setSourceType(rs.getInt("sourceType"));
				
				if(!StringUtil.isEmpty(rs.getString("attachment")))
				{
					reCustomerComplaintInfo.setShowAttachment(rs.getString("attachment").substring(rs.getString("attachment").
							  lastIndexOf("/")+1, rs.getString("attachment").length()));
				}
				else
				{
					reCustomerComplaintInfo.setShowAttachment(rs.getString("attachment"));
				}
			
				return reCustomerComplaintInfo;
				
			}});
	}

	/**
	  * 客诉回复信息分页显示方法
	  * @param customerComplaintCode
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@Override
	public Page pageReCustomerComplaintInfoPage(String customerComplaintCode, int pageNo, int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select * from DT_ReCustomerComplaintInfo");
		sql.append(" where customerComplaintCode=?");
		sql.append(" order by id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, ReCustomerComplaintInfo.class, customerComplaintCode);
		return page;
	}

	@Override
	public void updateReCustomerComplaintInfo(
			ReCustomerComplaintInfo reCustomerComplaintInfo) throws Exception 
	{
		this.daoSupport.update("DT_ReCustomerComplaintInfo", reCustomerComplaintInfo, 
							   "id='"+reCustomerComplaintInfo.getId()+"'");
	}

	@Override
	public ReCustomerComplaintInfo getReCustomerComplaintInfoByCode(String code)
			throws Exception 
	{
		return this.daoSupport.queryForObject(" select * from DT_ReCustomerComplaintInfo " +
											 " where code='"+code+"'", ReCustomerComplaintInfo.class);
	}


}
