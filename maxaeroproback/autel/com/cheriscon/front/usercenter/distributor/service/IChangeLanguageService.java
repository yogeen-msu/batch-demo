package com.cheriscon.front.usercenter.distributor.service;

import com.cheriscon.framework.database.Page;

public interface IChangeLanguageService {
	
	/**
	 * 查询经销商MaxiSys产品更换语言列表
	 * @param sealerCode 经销商code
	 * @param serialNo 产品序列号
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	public Page queryToCustomerChangePage(String sealerCode, String serialNo,
			String proTypeCode,int pageNo, int pageSize) throws Exception;
	
	
	/**
	 * 查询经销商查询自己所卖产品密码等信息
	 * @param sealerCode 经销商code
	 * @param serialNo 产品序列号
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	public Page querySealerProductPage(String sealerCode, String serialNo,
			String proTypeCode,int pageNo, int pageSize) throws Exception;

}
