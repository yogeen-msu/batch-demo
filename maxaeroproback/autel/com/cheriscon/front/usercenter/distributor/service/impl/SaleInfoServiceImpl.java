package com.cheriscon.front.usercenter.distributor.service.impl;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.member.vo.SaleInfoVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.SaleInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.DesModule;
import com.cheriscon.common.utils.ExcelUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.distributor.service.ISaleInfoService;

/**
 * 经销商销售信息业务逻辑实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@SuppressWarnings("rawtypes")
@Service
public class SaleInfoServiceImpl extends BaseSupport implements ISaleInfoService
{

	@Resource
	private ISealerInfoService sealerInfoService;

	@Override
	public Page querySaleInfoPage(String saleAutelId,String productName, int pageNo, int pageSize) 
	{
		StringBuffer querySql = new StringBuffer();
		
		querySql.append("select a.proName, a.proExpand,a.company, a.salesNum,a.salesMoney,a.pinBackNum,a.pinBackMoney,a.netWeight,a.netSales ")
				.append(" from DT_SaleInfo a left join DT_SealerInfo b on a.sealerName = b.autelId ")
				.append(" where 1=1 ");
		
		if(!StringUtil.isEmpty(saleAutelId))
		{
			querySql.append(" and a.sealerName = '").append(saleAutelId).append("'");
		}
		if(!StringUtil.isEmpty(productName))
		{
			querySql.append(" and a.proName like '%").append(productName).append("%'");
		}
		
		querySql.append(" order by a.id");
		return this.daoSupport.queryForPage(querySql.toString(), pageNo, pageSize,
				new RowMapper()
		{

			@Override
			public SaleInfo mapRow(ResultSet rs, int arg1) throws SQLException 
			{
				SaleInfo saleInfo = new SaleInfo();
				saleInfo.setProName(rs.getString("proName"));
				saleInfo.setProExpand(rs.getString("proExpand"));
				saleInfo.setCompany(rs.getString("company"));
				saleInfo.setSalesNum(rs.getInt("salesNum"));
				saleInfo.setSalesMoney(rs.getLong("salesMoney"));
				saleInfo.setPinBackNum(rs.getInt("pinBackNum"));
				saleInfo.setPinBackMoney(rs.getLong("pinBackMoney"));
				saleInfo.setNetWeight(rs.getInt("netWeight"));
				saleInfo.setNetSales(rs.getLong("netSales"));
				return saleInfo;
			}
		});
	}

	@Override
	public SaleInfo queryTotalValue(String saleAutelId,String productName) throws Exception 
	{
		StringBuffer sql = new StringBuffer();
		sql.append("select sum(salesNum) as salesNum,sum(salesMoney) as salesMoney,sum(pinBackNum)")
		   .append(" as pinBackNum,sum(pinBackMoney) as pinBackMoney,sum(netWeight) as netWeight,")
		   .append(" sum(netSales) as netSales  from  DT_SaleInfo a ")
		   .append(" left join DT_SealerInfo b on a.sealerName = b.autelId ") 
		   .append(" where a.sealerName = '")
		   .append(saleAutelId).append("'");
		
		if(!StringUtil.isEmpty(productName))
		{
			sql.append(" and a.proName like  '%").append(productName).append("%'");
		}
		
		return  (SaleInfo) this.daoSupport.queryForObject(sql.toString(),
				new ParameterizedRowMapper<SaleInfo>()
		{

			public SaleInfo mapRow(ResultSet rs, int arg1) throws SQLException 
			{
				SaleInfo saleInfo = new SaleInfo();
				saleInfo.setSalesNum(rs.getInt("salesNum"));
				saleInfo.setSalesMoney(rs.getLong("salesMoney"));
				saleInfo.setPinBackNum(rs.getInt("pinBackNum"));
				saleInfo.setPinBackMoney(rs.getLong("pinBackMoney"));
				saleInfo.setNetWeight(rs.getInt("netWeight"));
				saleInfo.setNetSales(rs.getLong("netSales"));
				return saleInfo;
			}
		});
	
	}

	@Override
	public void addSaleInfo(SaleInfo saleInfo) throws Exception
	{
		/*String code = DBUtils.generateCode(DBConstant.SALE_INFO_CODE);
		StringBuffer sql = new StringBuffer();
		sql.append("insert into DT_SaleInfo(code,proName,proExpand,company,salesNum,salesMoney,pinBackNum,pinBackMoney,netWeight,netSales,sealerName) ");
		sql.append(" values('").append(code).append("','").append(saleInfo.getProName()).append("','").append(saleInfo.getProExpand())
		   .append("','").append(saleInfo.getCompany()).append("',").append(saleInfo.getSalesNum()).append(",").append(saleInfo.getSalesMoney())
		   .append(",").append(saleInfo.getPinBackNum()).append(",").append(saleInfo.getPinBackMoney()).append(",").append(saleInfo.getNetWeight())
		   .append(",").append(saleInfo.getNetSales()).append(",'").append(saleInfo.getSealerName()).append("'");
		this.daoSupport.execute(sql.toString());*/
		
	}

	
	/**
	 * 添加经销商销售信息
	 * @param saleInfo
	 * 经销商销售信息
	 * @throws Exception
	 */
	@Transactional
	public int insertSaleInfo(SaleInfo saleInfo) throws Exception {
		saleInfo.setCode(DBUtils.generateCode(DBConstant.SALE_INFO_CODE));
		this.daoSupport.insert("DT_SaleInfo", saleInfo);
		return 0;
	}
	
	/**
	 * 更新经销商销售信息
	 * @param saleInfo
	 * 经销商销售信息
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public int updateSaleInfo(SaleInfo saleInfo) throws Exception {
		this.daoSupport.update("DT_SaleInfo", saleInfo, "code='"+saleInfo.getCode()+"'");
		return 0;
	}

	public SaleInfo getSaleInfoByCode(String code) throws Exception {
		String sql = "select * from DT_SaleInfo where code = ?";
		
		return (SaleInfo) this.daoSupport.queryForObject(sql, SaleInfo.class, code);
	}

	@Transactional
	public int delSaleInfo(String code) throws Exception {
		String sql = "delete from DT_SaleInfo where code= ?";
		this.daoSupport.execute(sql,code);
		return 0;
	}

	@Transactional
	public int delSaleInfos(String codes) throws Exception {
		String sql = "delete from DT_SaleInfo where code in("+codes+")";
		this.daoSupport.execute(sql);
		return 0;
	}



	/**
	  * 经销商销售信息分页显示方法
	  * @param SaleInfoVo
	  * @param pageNo
	  * @param pageSize
	  * @return
	  * @throws Exception
	  */ 
	@SuppressWarnings("unchecked")
	@Override
	public Page pageSaleInfoVoPage(SaleInfoVo saleInfoVo, int pageNo,
			int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();

		sql.append("select *");
		sql.append(" from DT_SaleInfo");
		sql.append(" where 1=1");
		
		if (null != saleInfoVo) {
			if (StringUtils.isNotBlank(saleInfoVo.getSealerName())) {
				sql.append(" and sealerName like '%");
				sql.append(saleInfoVo.getSealerName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(saleInfoVo.getProName())) {
				sql.append(" and proName like '%");
				sql.append(saleInfoVo.getProName().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(saleInfoVo.getProExpand())) {
				sql.append(" and proExpand like '%");
				sql.append(saleInfoVo.getProExpand().trim());
				sql.append("%'");
			}
			if (StringUtils.isNotBlank(saleInfoVo.getCompany())) {
				sql.append(" and company like '%");
				sql.append(saleInfoVo.getCompany().trim());
				sql.append("%'");
			}
		}

		sql.append(" order by id desc");
		
		Page page = this.daoSupport.queryForPage(sql.toString(), pageNo,
				pageSize, SaleInfoVo.class);
		return page;
	}

	@Override
	public int querySaleInfoCount(String saleAutelId) throws Exception 
	{
		StringBuffer sql = new StringBuffer();
		sql.append("select count(*) from DT_SaleInfo where sealerName='").append(saleAutelId).append("'");
		
		return this.daoSupport.queryForInt(sql.toString());
	}
	
	/**
	 * 保存excel中经销商销售信息
	 * @throws Exception
	 * @author pengdongan
	 */
	@Transactional
	public int saveCreateSaleInfo(File saleInfoFile) throws Exception {

		if (saleInfoFile != null) {
			SaleInfo saleInfo = new SaleInfo();
			
			ExcelUtils excelUtils = new ExcelUtils(saleInfoFile, 0);
			List<List<String>> resultList = excelUtils.readSimpleExcel(0,7);
			
			String intCheck = "^\\d+$";
			Pattern intRegex = Pattern.compile(intCheck);
			
			String moneyCheck = "^\\d+([.]\\d{1,3})?$";
			Pattern moneyRegex = Pattern.compile(moneyCheck);
			
			for (List<String> list : resultList) {
				
				if (sealerInfoService.getSealerByAutelId(list.get(0)) == null) return 1;
				if (StringUtils.isBlank(list.get(1))) return 2;
				if (StringUtils.isBlank(list.get(2))) return 3;
				if (StringUtils.isBlank(list.get(3))) return 4;
				if (!intRegex.matcher(list.get(4)).matches()) return 5;
				if (!intRegex.matcher(list.get(5)).matches()) return 6;
				if (StringUtils.isNotBlank(list.get(6)) && !intRegex.matcher(list.get(5)).matches()) return 7;
				if (StringUtils.isNotBlank(list.get(7)) && !intRegex.matcher(list.get(5)).matches()) return 8;
			}
			
			for (List<String> list : resultList) {
				saleInfo.setSealerName(list.get(0));
				saleInfo.setProName(list.get(1));
				saleInfo.setProExpand(list.get(2));
				saleInfo.setCompany(list.get(3));
				saleInfo.setSalesNum(Integer.parseInt(list.get(4)));
				saleInfo.setSalesMoney(Long.parseLong(list.get(5)));
				saleInfo.setPinBackNum(Integer.parseInt(StringUtils.isBlank(list.get(6))?"0":list.get(6)));
				saleInfo.setPinBackMoney(Long.parseLong(StringUtils.isBlank(list.get(7))?"0":list.get(7)));
				saleInfo.setNetWeight(saleInfo.getSalesNum()-saleInfo.getPinBackNum());
				saleInfo.setNetSales(saleInfo.getSalesMoney()-saleInfo.getPinBackMoney());
				
				this.insertSaleInfo(saleInfo);
			}
		}
		
		return 0;
	}

}
