package com.cheriscon.front.usercenter.distributor.service;

import java.util.List;

import com.cheriscon.common.model.CustomerComplaintInfo;
import com.cheriscon.framework.database.Page;


/**
 * 客诉信息(1:个人用户 2：经销商用户)业务逻辑实现接口类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
public interface ICustomerComplaintInfoService 
{
	/**
	 * 根据用户类型、用户code查询客诉信息记录
	 * @param userType 用户类型
	 * @param userCode 用户code
	 * @return
	 */
	public  Page queryCustomerComplaintInfoPage(int userType,int timeRange,int complaintState,String userCode, int pageNo, int pageSize) throws Exception;
	
	/**
	 * 
	 * @param userType 用户类型
	 * @param userCode 用户code
	 * @return
	 */
	public  Page queryCustomerComplaintInfoPage3(int userType,int timeRange,int complaintState,String userCode, int pageNo, int pageSize) throws Exception;
	
	
	/**
	 * 根据用户类型、用户code查询客诉信息记录
	 * @param userType 用户类型
	 * @param userCode 用户code
	 * @return
	 */
	public  Page queryCustomerComplaintInfoPage2(String authCode, int userType,int timeRange,int complaintState,String userCode, int pageNo, int pageSize) throws Exception;
	
	
	/**
	 * 新增客诉信息记录
	 * @param customerComplaintInfo
	 * @throws Exception
	 */
	public void addCustomerComplaintInfo(CustomerComplaintInfo customerComplaintInfo)throws Exception;
	
	
	/**
	 * 根据编码查找客诉信息对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public CustomerComplaintInfo getCustomerComplaintInfoByCode(String code) throws Exception;
	
	/**
	 * 修改客诉信息记录
	 * @param customerComplaintInfo
	 * @throws Exception
	 */
	public void updateCustomerComplaintInfo(CustomerComplaintInfo customerComplaintInfo) throws Exception;
	

	/**
	 * 根据用户类型、用户code查询对应的客诉状态条数
	 * @param userType 用户类型
	 * @param userCode 用户code

	 * @return
	 * @throws Exception
	 */
	public List<CustomerComplaintInfo> queryCustomerComplaintStateList(int userType,int complaintState,String userCode)
			throws Exception;
	/**
	 * 分配客服
	 * @param code,acceptor
	 * @throws Exception
	 */
	public int allotAcceptor(String code,String acceptor) throws Exception;
	
	/**
	 * 查询我的客诉记录条数
	 * @param userType
	 * @param userCode
	 * @return
	 * @throws Exception
	 */
	public int queryCustomerComplaintCount(int userType , String userCode) throws Exception;
	
	/**
	 * 根据时间类型、用户code查询客诉信息记录(平板端)
	 * @param timeRange 时间类型
	 * @param userCode 用户code
	 * @return
	 */
	public  List<CustomerComplaintInfo> queryCustomerComplaintInfoList(int timeRange,String userCode) throws Exception;
	
	public List<CustomerComplaintInfo> queryCustomerComplaintInfoList(int timeRange, 
			String userCode, Integer complaintState) throws Exception;
}
