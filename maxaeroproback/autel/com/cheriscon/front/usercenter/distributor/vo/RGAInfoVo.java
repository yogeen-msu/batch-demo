package com.cheriscon.front.usercenter.distributor.vo;

public class RGAInfoVo {

	private Integer id;
	private String code;
	private String createUser; // 创建者
	private String createDate; // 创建日期
	private String customerName; // 客户名称
	private String address; // 地址
	private String email; // 邮件
	private String telphone; // 电话
	private String company; // 公司
	private String proTypeCode; // 产品类型
	private String serialNo; // 序列号
	private String reason; // 返修原因

	private String receiveDate; // 签收日期
	private String trackingNum; // 运单号
	private String cableFlag; // 是否含连接线
	private String connectorFlag; // 是否含连接头
	private String otherPart; // 是否含其它部件
	private String solution; // 处理方案

	private String returnDate; // 送返日期
	private String returnNum; // 运单号
	private String status;

	private String createUserName; // 创建者姓名
	private String proStartDate; // 硬件保修开始日期
	private String proEndDate; // 硬件保修结束日期
	private String closeDate; // 关闭日期
	private String updateStartDate; // 产品更新开始日期
	private String updateEndDate;// 产品更新结束日期
	private Integer dayNum;// 从创建日期到当前日期相差多少天
	private String rgaDate; // RGA生成日期 20140804美国分公司要求加的

	
	private String auth; // 经销商权限编码
	private Integer pageNo;
	private Integer pageSize;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getTrackingNum() {
		return trackingNum;
	}

	public void setTrackingNum(String trackingNum) {
		this.trackingNum = trackingNum;
	}

	public String getCableFlag() {
		return cableFlag;
	}

	public void setCableFlag(String cableFlag) {
		this.cableFlag = cableFlag;
	}

	public String getConnectorFlag() {
		return connectorFlag;
	}

	public void setConnectorFlag(String connectorFlag) {
		this.connectorFlag = connectorFlag;
	}

	public String getOtherPart() {
		return otherPart;
	}

	public void setOtherPart(String otherPart) {
		this.otherPart = otherPart;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getReturnNum() {
		return returnNum;
	}

	public void setReturnNum(String returnNum) {
		this.returnNum = returnNum;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getProStartDate() {
		return proStartDate;
	}

	public void setProStartDate(String proStartDate) {
		this.proStartDate = proStartDate;
	}

	public String getProEndDate() {
		return proEndDate;
	}

	public void setProEndDate(String proEndDate) {
		this.proEndDate = proEndDate;
	}

	public String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}

	public String getUpdateStartDate() {
		return updateStartDate;
	}

	public void setUpdateStartDate(String updateStartDate) {
		this.updateStartDate = updateStartDate;
	}

	public String getUpdateEndDate() {
		return updateEndDate;
	}

	public void setUpdateEndDate(String updateEndDate) {
		this.updateEndDate = updateEndDate;
	}

	public Integer getDayNum() {
		return dayNum;
	}

	public void setDayNum(Integer dayNum) {
		this.dayNum = dayNum;
	}

	public String getRgaDate() {
		return rgaDate;
	}

	public void setRgaDate(String rgaDate) {
		this.rgaDate = rgaDate;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

}
