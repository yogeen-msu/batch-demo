package com.cheriscon.front.usercenter.distributor.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class HangUpVo implements Serializable {

	
	private String code;
	private Integer complaintState;
	private String hangUpReason;
	private String autelId;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getComplaintState() {
		return complaintState;
	}

	public void setComplaintState(Integer complaintState) {
		this.complaintState = complaintState;
	}

	public String getHangUpReason() {
		return hangUpReason;
	}

	public void setHangUpReason(String hangUpReason) {
		this.hangUpReason = hangUpReason;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

}
