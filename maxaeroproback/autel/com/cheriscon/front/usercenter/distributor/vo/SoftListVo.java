package com.cheriscon.front.usercenter.distributor.vo;


public class SoftListVo {

	private String languageCode;
	private String queryType;

	private Integer softListId;
	
	private String productSN; //产品序列号
	
	private String softCode; //软件code
	
	private String softVersion; //软件版本
	
	private String regCode; //注册密码
	
	private Integer syncTimestamp; //升级日期UTC
	
	private String syncTime; //升级日期
	
	private String lastOpCode; //
	
	private String softName ; //软件名称
	
	private String softSysVersion; //系统上软件版本
	
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public Integer getSoftListId() {
		return softListId;
	}

	public void setSoftListId(Integer softListId) {
		this.softListId = softListId;
	}

	public String getProductSN() {
		return productSN;
	}

	public void setProductSN(String productSN) {
		this.productSN = productSN;
	}

	public String getSoftCode() {
		return softCode;
	}

	public void setSoftCode(String softCode) {
		this.softCode = softCode;
	}

	public String getSoftVersion() {
		return softVersion;
	}

	public void setSoftVersion(String softVersion) {
		this.softVersion = softVersion;
	}

	public String getRegCode() {
		return regCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public Integer getSyncTimestamp() {
		return syncTimestamp;
	}

	public void setSyncTimestamp(Integer syncTimestamp) {
		this.syncTimestamp = syncTimestamp;
	}

	public String getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(String syncTime) {
		this.syncTime = syncTime;
	}

	public String getLastOpCode() {
		return lastOpCode;
	}

	public void setLastOpCode(String lastOpCode) {
		this.lastOpCode = lastOpCode;
	}

	public String getSoftName() {
		return softName;
	}

	public void setSoftName(String softName) {
		this.softName = softName;
	}

	public String getSoftSysVersion() {
		return softSysVersion;
	}

	public void setSoftSysVersion(String softSysVersion) {
		this.softSysVersion = softSysVersion;
	}

}
