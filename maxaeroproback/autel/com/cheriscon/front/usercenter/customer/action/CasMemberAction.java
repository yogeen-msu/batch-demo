package com.cheriscon.front.usercenter.customer.action;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.Md5PwdEncoder;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;

@ParentPackage("json-default")
@Namespace("/casmember")
public class CasMemberAction extends WWAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2011100890385268592L;
	
	@Resource
	private ICustomerInfoService customerInfoService;
	@Resource
	private ISystemErrorLogService systemErrorService;
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private EmailProducer emailProducer;
	@Resource
	private IEmailTemplateService emailTemplateService; 
	@Resource
	private ILanguageService languageService;
	
//	private CasMember casMember;
	private CustomerInfo customerInfo;

	
	@SuppressWarnings({ "unchecked", "unused" })
	@Action(value = "add")
	public String add() throws Exception {
		
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
//		CustomerInfo customerInfo = new CustomerInfo();
//		
		customerInfo.setRegTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
		customerInfo.setActState(FrontConstant.USER_STATE_NOT_ACTIVE);
		customerInfo.setSecondActState(FrontConstant.USER_STATE_NOT_ACTIVE);
		customerInfo.setActCode(StringUtils.remove(UUID.randomUUID().toString(), '-'));
		customerInfo.setLastLoginTime(DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
//		
		customerInfo.setUserPwd(Md5PwdEncoder.getInstance().encodePassword(customerInfo.getUserPwd(), null));
		//customerInfo.setName(customerInfo.getFirstName() + customerInfo.getMiddleName() + customerInfo.getLastName());
		customerInfo.setName(customerInfo.getFirstName() + " " + customerInfo.getLastName());
		customerInfo.setSourceType(FrontConstant.CUSTOMER_SOURCE_TYPE_PRO);
		if (!StringUtil.isEmpty(customerInfo.getIsAllowSendEmail().toString())) {
			customerInfo.setIsAllowSendEmail(customerInfo.getIsAllowSendEmail());
		}

		//设置激活时间点
		customerInfo.setSendActiveTime(DateUtil.toString(new Date(), 
										  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
		
		//默认给个第二邮箱激活时间点
		customerInfo.setSendSecondEmailActiveTime(DateUtil.toString(new Date(), 
										  FrontConstant.TIMEFORMAT_YYYY_MM_DD_HH_MM_SS));
		
		try {
			customerInfoService.addCustomer(customerInfo);
		} catch (Exception ex) {
			ex.printStackTrace();
			String errorMsg="";
			StackTraceElement[] st = ex.getStackTrace();
			for (StackTraceElement stackTraceElement : st) {
			String exclass = stackTraceElement.getClassName();
			String method = stackTraceElement.getMethodName();
				if("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)){
					
					errorMsg=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
							+ method + "时在第" + stackTraceElement.getLineNumber()
							+ "行代码处发生异常!异常类型:" + ex.getClass().getName();
					System.out.println(errorMsg);
					systemErrorService.saveLog("用户注册提交保存信息",errorMsg);
				}
			}
		}
		try {
			Smtp smtp = smtpManager.getCurrentSmtp();
			
			EmailModel emailModel = new EmailModel();
			emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
			emailModel.getData().put("password", smtp.getPassword());
			emailModel.getData().put("host", smtp.getHost());
			emailModel.getData().put("port", smtp.getPort());
			
			String requestUrl = FreeMarkerPaser.getBundleValue("autelproweb.url");;
			String activeUrl = requestUrl+ "/activeCustomerInfo.html?autelId="
					+ customerInfo.getAutelId() + "&actCode="+customerInfo.getActCode()+"&operationType=9";
			
			String userName = "";
			if (customerInfo != null && customerInfo.getName() != null) {
				userName=customerInfo.getName();
			}
			
			emailModel.getData().put("autelId", customerInfo.getAutelId());
			emailModel.getData().put("userName",userName);
			emailModel.getData().put("activeUrl", activeUrl);
			
			Language language = languageService.getByCode(customerInfo.getLanguageCode());
			
			if (language != null) {
				emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle",
						language.getLanguageCode(),language.getCountryCode()));
			} else {
				emailModel.setTitle(FreeMarkerPaser.getBundleValue("accountinformation.reguser.mailtitle"));
			}
			
			emailModel.setTo(customerInfo.getAutelId());
			
			
			EmailTemplate template = emailTemplateService.
					getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_REGIST,customerInfo.getLanguageCode());
			
			if (template != null) {
				emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
			}
			
			emailProducer.send(emailModel);
			//				String webSite = "http://www."+customerInfo.getAutelId().substring(customerInfo.getAutelId().indexOf("@") + 1, customerInfo.getAutelId().length());
		} catch (Exception ex2) {
			ex2.printStackTrace();
			String errorMsg2="";
			StackTraceElement[] st2 = ex2.getStackTrace();
			for (StackTraceElement stackTraceElement : st2) {
				String exclass = stackTraceElement.getClassName();
				String method = stackTraceElement.getMethodName();
				if ("com.cheriscon.front.user.component.widget.CustomerInfoWidget".equals(exclass)) {
					errorMsg2=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss") + ":" + "[类:" + exclass + "]调用"
							+ method + "时在第" + stackTraceElement.getLineNumber()
							+ "行代码处发生异常!异常类型:" + ex2.getClass().getName();
					System.out.println(errorMsg2);
					systemErrorService.saveLog("用户注册提交发送邮件",errorMsg2);
				}
			}
			
		}
		return SUCCESS;
	}

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

//	public CasMember getCasMember() {
//		return casMember;
//	}
//
//	public void setCasMember(CasMember casMember) {
//		this.casMember = casMember;
//	}

}
