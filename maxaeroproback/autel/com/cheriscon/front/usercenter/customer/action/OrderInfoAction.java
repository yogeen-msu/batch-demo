package com.cheriscon.front.usercenter.customer.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.aspectj.weaver.ast.Var;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ShoppingCart;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.OneOrderInfoVo;
import com.cheriscon.front.usercenter.customer.vo.OrderInfoVO;

/**
 * 订单模块处理类
 * @author caozhiyong
 * @version 2013-1-16
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class OrderInfoAction extends WWAction{

	private static final long serialVersionUID = 1L;
	
	@Resource
	private IOrderInfoService service;
	
	private String minSaleUnitList;
	
	private String allmoney;//总金额
	
	private String discountallmoney;//优惠金额
	
	private OrderInfoVO orderInfoVO=new OrderInfoVO();
	
	private String orderInfoCode;//订单编号
	
	private String subResult="false";//订单结果
	
	private String orderResult;
	
	@Resource 
	private IShoppingCartService shoppingCartService;
	
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	
	@Resource
	private ISmtpManager smtpManager;
	@Resource
	private ILanguageService languageService;
	@Resource
	private IEmailTemplateService emailTemplateService; 
	@Resource
	private EmailProducer emailProducer;
	/**
	 * 生成订单
	 * @return
	 */
	@Action(value = "submintOrderInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "orderResult"}) })
	public String submintOrderInfo(){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		String usercode=customerInfo.getCode();//用户code
		
		//String[] minSaleUnits=minSaleUnitList.split(",");
		Map<String,String> dataMap = new HashMap<String, String>();
		BigDecimal orderAllMoney=new BigDecimal(0.00);
		
		List<OneOrderInfoVo> infoVos=new ArrayList<OneOrderInfoVo>();
		List<ShoppingCart> shoppingCarts=shoppingCartService.querShoppingCarts(usercode);
		if (shoppingCarts.size()>0) {
			for (int i = 0; i < shoppingCarts.size(); i++) {
				OneOrderInfoVo info=new OneOrderInfoVo();
				ShoppingCart cart= shoppingCarts.get(i);
				if (cart.getIsSoft() == 1) {
					MinSaleUnitDetailVO detailVO=minSaleUnitDetailService.findMinSaleUnitDetailInfo(cart.getMinSaleUnitCode(), customerInfo.getLanguageCode(), cart.getProSerial());
					
					String discountPrice="0";
					//查询最小销售单位是否可参与活动
					MarketPromotionDiscountInfoVO  discountInfoVO=null;
					if (cart.getType() ==0) {
						info.setYear(1);//续租年限
						discountInfoVO=minSaleUnitDetailService.queryDiscount(customerInfo, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_BUY,detailVO.getDate());
						discountPrice=detailVO.getPrice();
						if (discountInfoVO!=null) {
							//折后价
							discountPrice=String.valueOf((Float.parseFloat(detailVO.getPrice())*1000 * Float.parseFloat(String.valueOf( discountInfoVO.getDiscount())))/10/1000);
						}
						info.setPrice(discountPrice);//原价
					}else {
						info.setYear(Integer.valueOf(cart.getYear()));//续租年限
						//查询最小销售单位是否可参与活动
						discountInfoVO=minSaleUnitDetailService.queryDiscount(customerInfo, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,detailVO.getDate());
						discountPrice=detailVO.getPrice();
						if (discountInfoVO!=null) {
							//折后价
							discountPrice=String.valueOf((Float.parseFloat(detailVO.getPrice())*1000 * Float.parseFloat(String.valueOf( discountInfoVO.getDiscount())))/10/1000);
						}
						info.setPrice(discountPrice);//原价
					}
					
					BigDecimal total=new BigDecimal(0.00);
					BigDecimal num1=new BigDecimal(info.getYear());
					num1.setScale(2, BigDecimal.ROUND_HALF_UP);
					
					BigDecimal num2=new BigDecimal(discountPrice);
					num2.setScale(2, BigDecimal.ROUND_HALF_UP);
					total=num1.multiply(num2);
					orderAllMoney=orderAllMoney.add(total);
				}else {
					
					CfgSoftRentInfoVO rentInfoVO=minSaleUnitDetailService.findSaleConfigDetailInfo(cart.getMinSaleUnitCode(), customerInfo.getLanguageCode(),cart.getProSerial());
					
					MarketPromotionDiscountInfoVO info2=minSaleUnitDetailService.querySaleCfgDiscount(customerInfo, rentInfoVO.getSaleCfgCode(), rentInfoVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,rentInfoVO.getDate());
					String discountPrice=rentInfoVO.getPrice();
					rentInfoVO.setPrice(Float.valueOf(discountPrice).toString());
					if (info2!=null) {
						//折后价
						discountPrice=String.valueOf((Float.parseFloat(rentInfoVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info2.getDiscount())))/10/1000);
					}
					info.setPrice(discountPrice);//原价
					
					BigDecimal total=new BigDecimal(0.00);
					BigDecimal num1=new BigDecimal(cart.getYear());
					num1.setScale(2, BigDecimal.ROUND_HALF_UP);
					
					BigDecimal num2=new BigDecimal(discountPrice);
					num2.setScale(2, BigDecimal.ROUND_HALF_UP);
					total=num1.multiply(num2);
					orderAllMoney=orderAllMoney.add(total);
					info.setYear(Integer.valueOf(cart.getYear()));//续租年限
				}
				
				
				info.setMinSaleUnitCode(cart.getMinSaleUnitCode());//最小销售单位code
				info.setSerial(cart.getProSerial());//序列号
				info.setType(cart.getType());
				//String picPath=CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+minSaleUnit[5];
				info.setPicPath(cart.getPicPath());//产品图片路径
				info.setIsSoft(Integer.valueOf(cart.getIsSoft()));
				infoVos.add(info);
			}
			
			orderInfoCode=DBConstant.getRandStr();
			orderInfoVO.setAllMoney(orderAllMoney.toString());
			orderInfoVO.setCustomerCode(usercode);
			orderInfoVO.setDiscountAllMoney("0");
			orderInfoVO.setOrderInfoVos(infoVos);
			
			orderInfoVO.setOrderDate(DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss"));//下单时间
			orderInfoVO.setOrderCode(orderInfoCode);//订单编码
			
			boolean submitResult=service.submitOrderInfo(orderInfoVO);
			if(submitResult){
				Smtp smtp = smtpManager.getCurrentSmtp( );
				EmailModel emailModel = new EmailModel();
				emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
				emailModel.getData().put("password", smtp.getPassword());
				emailModel.getData().put("host", smtp.getHost());
				emailModel.getData().put("port", smtp.getPort());
				
				String userName="";
				if(customerInfo!=null && customerInfo.getName()!=null){
				  userName=customerInfo.getName();
				}
				String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");
				String orderUrl=requestUrl+"/OnlinePay.html?orderInfoCode="+orderInfoVO.getOrderCode();
				
				emailModel.getData().put("autelId", customerInfo.getAutelId());
				emailModel.getData().put("userName", userName);
				emailModel.getData().put("orderCode", orderInfoVO.getOrderCode()); //订单编号
				emailModel.getData().put("orderDate", orderInfoVO.getOrderDate()); //订单时间
				emailModel.getData().put("orderMoney", orderInfoVO.getAllMoney()); //订单金额
				emailModel.getData().put("orderUrl", orderUrl); //订单金额
				
				Language language = languageService.getByCode(customerInfo.getLanguageCode());
				if(language != null)
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("cententman.messageman.order.confirm.mailtitle",
							language.getLanguageCode(),language.getCountryCode()));
				}
				else 
				{
					emailModel.setTitle(FreeMarkerPaser.getBundleValue("cententman.messageman.order.confirm.mailtitle"));
				}
				emailModel.getData().put("orderStatus", FreeMarkerPaser.getBundleValue("order.nopay",
						language.getLanguageCode(),language.getCountryCode())); //订单状态
				
				emailModel.setTo(customerInfo.getAutelId());
				EmailTemplate template = emailTemplateService.
						getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_ORDER_CONFIRM,customerInfo.getLanguageCode());
				
				if(template != null)
				{
					emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
				}
				
				emailProducer.send(emailModel);
			}
			//将结果转成json数据格式传输
			subResult=String.valueOf(submitResult);
			dataMap.put("orderInfoCode", orderInfoCode);
			dataMap.put("allmoney", orderAllMoney.toString());
		}
		
		dataMap.put("subResult", subResult);
		orderResult=JSONArray.fromObject(dataMap).toString();
		return "success";
		
	}

	/**
	 * 取消订单
	 * @return
	 */
	@Action(value = "removeOrderInfo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "orderResult"}) })
	public String removeOrderInfo(){
		boolean removeResult=service.removeOrder(orderInfoCode);
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("result", String.valueOf(removeResult));
		orderResult=JSONArray.fromObject(dataMap).toString();
		return "success"; 
	}
	
	public String getMinSaleUnitList() {
		return minSaleUnitList;
	}

	public void setMinSaleUnitList(String minSaleUnitList) {
		this.minSaleUnitList = minSaleUnitList;
	}

	public String getAllmoney() {
		return allmoney;
	}

	public void setAllmoney(String allmoney) {
		this.allmoney = allmoney;
	}

	public String getDiscountallmoney() {
		return discountallmoney;
	}

	public void setDiscountallmoney(String discountallmoney) {
		this.discountallmoney = discountallmoney;
	}

	public IOrderInfoService getService() {
		return service;
	}

	public void setService(IOrderInfoService service) {
		this.service = service;
	}

	public OrderInfoVO getOrderInfoVO() {
		return orderInfoVO;
	}

	public void setOrderInfoVO(OrderInfoVO orderInfoVO) {
		this.orderInfoVO = orderInfoVO;
	}

	public String getOrderInfoCode() {
		return orderInfoCode;
	}

	public void setOrderInfoCode(String orderInfoCode) {
		this.orderInfoCode = orderInfoCode;
	}

	public String getSubResult() {
		return subResult;
	}

	public void setSubResult(String subResult) {
		this.subResult = subResult;
	}

	public String getOrderResult() {
		return orderResult;
	}

	public void setOrderResult(String orderResult) {
		this.orderResult = orderResult;
	}
	

}
