package com.cheriscon.front.usercenter.customer.action;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.backstage.trade.service.IReChargeCardTypeService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ReChargeCardInfo;
import com.cheriscon.common.model.ReChargeCardType;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.distributor.service.IToCustomerChargeService;

/**
 * 使用充值卡充值
 * @author CaoZhiyong
 * @version 2013-1-29
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class ChargeAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Resource 
	private IToCustomerChargeService service;
	
	private String account;//充值卡账号
	
	private String pwd;//充值卡密码
	
	private String proSerial;//產品序列号
	
	@Resource
	private IReChargeCardTypeService reChargeCardTypeService;
	
	/**
	 * 充值结果(0充值卡账号或密码错误,1充值卡已使用,2充值成功,3充值失败,4客户不可使用该充值卡,5未激活,6已过期)
	 */
	private String chargeResult = "";
	
	@Action(value = "toCharge", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "chargeResult" }) })
	public String toCharge() {
		
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo = (CustomerInfo) SessionUtil
				.getLoginUserInfo(request);// 从session中获取当前登录的用户信息
		ReChargeCardInfo info = service.queryChargeCartInfo(account, pwd);
		ReChargeCardType reChargeCardType=null;
		
		if (info == null) {
			chargeResult = "0";// 充值卡账号或密码错误
			
		} else if (info.getIsUse() == FrontConstant.CARD_SERIAL_IS_YES_USE) {
			chargeResult = "1";// 已使用
			
		} else if (info.getIsActive() == FrontConstant.CHARGE_CARE_IS_NO_ACTIVE) {
			chargeResult = "5";// 未激活
		
		} else if (DateUtil.toDate(info.getValidDate(),
				FrontConstant.TIMEFORMAT_YYYY_MM_DD).getTime()< 
				DateUtil.toDate( DateUtil.toString(new Date(), FrontConstant.TIMEFORMAT_YYYY_MM_DD),FrontConstant.TIMEFORMAT_YYYY_MM_DD).getTime()) {
			chargeResult = "6";// 已过期
			
		} else {
			try {
				//根据充值卡类型code查询充值卡类型信息
				reChargeCardType = reChargeCardTypeService.
														getReChargeCardTypeByCode(info.getReChargeCardTypeCode());
				
				//充值
				boolean result = service.toCharge(info,reChargeCardType,customerInfo,proSerial);
				
				
				if (result) {
					chargeResult = "2";// 充值成功
					
				} else {
					chargeResult = "4";// 客户不可使用该充值卡
					
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				chargeResult = "3";// 充值失败
				
			}
		}
	
		Map<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("chargeResult", chargeResult);
		chargeResult = JSONArray.fromObject(dataMap).toString();
		return "success";
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getChargeResult() {
		return chargeResult;
	}

	public void setChargeResult(String chargeResult) {
		this.chargeResult = chargeResult;
	}

	public IReChargeCardTypeService getReChargeCardTypeService() {
		return reChargeCardTypeService;
	}

	public void setReChargeCardTypeService(
			IReChargeCardTypeService reChargeCardTypeService) {
		this.reChargeCardTypeService = reChargeCardTypeService;
	}

	public IToCustomerChargeService getService() {
		return service;
	}

	public void setService(IToCustomerChargeService service) {
		this.service = service;
	}

	public String getProSerial() {
		return proSerial;
	}

	public void setProSerial(String proSerial) {
		this.proSerial = proSerial;
	}

	
}
