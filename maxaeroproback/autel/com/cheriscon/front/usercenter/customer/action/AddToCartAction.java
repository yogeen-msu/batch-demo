package com.cheriscon.front.usercenter.customer.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsMinUnitVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;

/**
 * 软件批量加入购物车
 * @author caozhiyong
 * @version 2013-6-14
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class AddToCartAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int type;
	private int isSoft;
	private String minSaleUnitCode;
	private String serialNo;
	private String cfgPrice;
	private String validate;
	private String areaCfgCode;
	private int  year;
	private String picPath;
	private String addResult;//结果
	
	@Resource 
	private IShoppingCartService service;
	
	@Resource
	private IMinSaleUnitDetailService detailService;
	
	@Action(value = "addtocart", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "addResult" }) })
	public String addToCart(){
		Map<String,String> map = new HashMap<String, String>();
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		
		CfgSoftRentInfoVO rentInfoVO=null;
		MinSaleUnitDetailVO detailVO=null;
		String isSuccess="false";
		if (isSoft == 0) {
			rentInfoVO=detailService.findSaleConfigDetailInfo(minSaleUnitCode, customerInfo.getLanguageCode(),serialNo);
			
			MarketPromotionDiscountInfoVO info=detailService.querySaleCfgDiscount(customerInfo, rentInfoVO.getSaleCfgCode(), rentInfoVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,rentInfoVO.getDate());
			String discountPrice=rentInfoVO.getPrice();
			rentInfoVO.setPrice(Float.valueOf(discountPrice).toString());
			if (info!=null) {
				//折后价
				discountPrice=String.valueOf((Float.parseFloat(rentInfoVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
			}
			
			isSuccess=service.addShoppingCartIsRent(customerInfo.getCode(), "", serialNo, "", minSaleUnitCode, areaCfgCode, type, year,picPath,isSoft,rentInfoVO.getPrice(),rentInfoVO.getDate());	
		}else {
			detailVO=detailService.findMinSaleUnitDetailInfo(minSaleUnitCode, customerInfo.getLanguageCode(),serialNo);
			//查询最小销售单位是否可参与活动
			MarketPromotionDiscountInfoVO  info=null;
			if (type == 1) {
				info=detailService.queryDiscount(customerInfo, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,detailVO.getDate());
			}else {
				info=detailService.queryDiscount(customerInfo, detailVO.getMinSaleUnitCode(), detailVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_BUY,detailVO.getDate());
			}
					
			String discountPrice=detailVO.getPrice();
			detailVO.setPrice(Float.valueOf(discountPrice).toString());
			if (info!=null) {
				//折后价
				discountPrice=String.valueOf((Float.parseFloat(detailVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
			}
			isSuccess=service.addShoppingCartIsRent(customerInfo.getCode(), "", serialNo, "", minSaleUnitCode, areaCfgCode, type, year,picPath,isSoft,discountPrice,detailVO.getDate());	
		}
		
		map.put("addResult", String.valueOf(isSuccess));
		addResult=JSONArray.fromObject(map).toString();
		return "success";
	}

	
	@Action(value = "addtocart2", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "addResult" }) })
	public String addToCart2(){
		Map<String,String> map = new HashMap<String, String>();
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		
		CfgSoftRentInfoVO rentInfoVO=null;
		String isSuccess="false";
		if (isSoft == 0) {
			rentInfoVO=detailService.findSaleConfigDetailInfo(minSaleUnitCode, customerInfo.getLanguageCode(),serialNo);
			
			MarketPromotionDiscountInfoVO info=detailService.querySaleCfgDiscount(customerInfo, rentInfoVO.getSaleCfgCode(), rentInfoVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,rentInfoVO.getDate());
			String discountPrice=rentInfoVO.getPrice();
			rentInfoVO.setPrice(Float.valueOf(discountPrice).toString());
			if (info!=null) {
				//折后价
				discountPrice=String.valueOf((Float.parseFloat(rentInfoVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
			}
			picPath=rentInfoVO.getCfgpicpath();
			areaCfgCode=rentInfoVO.getAreaCfgCode();
			isSuccess=service.addShoppingCartIsRent(customerInfo.getCode(), "", serialNo, "", minSaleUnitCode, areaCfgCode, type, year,picPath,isSoft,rentInfoVO.getPrice(),rentInfoVO.getDate());	
		}
		map.put("addResult", String.valueOf(isSuccess));
		addResult=JSONArray.fromObject(map).toString();
		return "success";
	}
	
	
	@Action(value = "menuCart", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "addResult" }) })
	public String menuCart(){
		Map<String,List<ShopingCartsProListVO>> map = new HashMap<String, List<ShopingCartsProListVO>>();
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		
		if (customerInfo!=null) {
			String usercode=customerInfo.getCode();//用户code
			String languageCode=customerInfo.getLanguageCode();//用户首选语言code
			
			List<ShopingCartsProListVO> proListVOs=service.queryShoppingCartByCustomerCode(usercode,languageCode);
			//int items=0;
			for (int i = 0; i < proListVOs.size(); i++) {
				for (int j = 0; j < proListVOs.get(i).getMinUnitVOs().size(); j++) {
					
					ShopingCartsMinUnitVO minUnitVO=proListVOs.get(i).getMinUnitVOs().get(j);
					
					MarketPromotionDiscountInfoVO info;
					if (minUnitVO.getIsSoft() == 1) {
						// 查询最小销售单位是否可参与活动
						info = detailService.queryDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
										minUnitVO.getType(),
										minUnitVO.getValidDate());
					} else {
						// 查询销售配置是否可参与活动
						info = detailService.querySaleCfgDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
										minUnitVO.getType(),
										minUnitVO.getValidDate()); 
					}
					String discountPrice = minUnitVO.getCostPrice();
					minUnitVO.setCostPrice(Float.valueOf(discountPrice).toString());
					if (info != null) {
						// 折后价
						discountPrice = String.valueOf((Float.parseFloat(minUnitVO.getCostPrice())*1000 * Float.parseFloat(String.valueOf(info.getDiscount()))) / 10/1000);
					}
					minUnitVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
					
					
					//items++;
					proListVOs.get(i).getMinUnitVOs().set(j, minUnitVO);
				}
			}
			
			map.put("shoppingCart", proListVOs);
		}
		
		addResult=JSONArray.fromObject(map).toString();
		return "success";
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getIsSoft() {
		return isSoft;
	}

	public void setIsSoft(int isSoft) {
		this.isSoft = isSoft;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}

	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getCfgPrice() {
		return cfgPrice;
	}

	public void setCfgPrice(String cfgPrice) {
		this.cfgPrice = cfgPrice;
	}

	public String getValidate() {
		return validate;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getAddResult() {
		return addResult;
	}

	public void setAddResult(String addResult) {
		this.addResult = addResult;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	
}
