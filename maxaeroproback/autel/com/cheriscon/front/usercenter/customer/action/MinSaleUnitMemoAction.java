package com.cheriscon.front.usercenter.customer.action;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;

/**
 * 最小销售单位说明
 * @author caozhiyong
 * @version 2013-4-7
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class MinSaleUnitMemoAction extends WWAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Resource
	private IMinSaleUnitDetailService memoService;
	private String code;
	private String memo="";
	
	@Action(value = "memo", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "memo" }) })
	public String minSaleUnitMemo(){
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		
		memo=memoService.findMinSaleUnitMemo(code,customerInfo.getLanguageCode());
		
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("memo", memo);
		
		memo=JSONArray.fromObject(dataMap).toString();
		
		return "success";
	}

	public IMinSaleUnitDetailService getMemoService() {
		return memoService;
	}

	public void setMemoService(IMinSaleUnitDetailService memoService) {
		this.memoService = memoService;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	
	
}
