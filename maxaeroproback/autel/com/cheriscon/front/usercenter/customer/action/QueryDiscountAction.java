package com.cheriscon.front.usercenter.customer.action;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;

/**
 * 查询最小销售单位折扣
 * @author caozhiyong
 * @version 2013-1-28
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/customer")
public class QueryDiscountAction extends WWAction{

	private static final long serialVersionUID = 1L;
	
	@Resource
	private IMinSaleUnitDetailService service;
	private String areaCfgCode;//区域配置code
	private String minSaleUnitCode;//最小销售单位code
	private String discount="";//优惠活动
	private Integer type;//消费类型
	@Action(value= "queryMinSaleUnitDiscount",results={@Result(name=SUCCESS,type="json",params={"root","discount"}) })
	public String queryMinSaleUnitDiscount(){
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		MarketPromotionDiscountInfoVO info=service.queryDiscount(customerInfo, minSaleUnitCode, areaCfgCode, type,"");
		if (info!=null) {
			Map<String,String> dataMap = new HashMap<String, String>();
			dataMap.put("result", "true");
			dataMap.put("endTime", info.getEndTime());
			dataMap.put("startTime", info.getStartTime());
			dataMap.put("promotionName", info.getPromotionName());
			dataMap.put("discount",String.valueOf(info.getDiscount()));
			dataMap.put("promotionType", info.getPromotionType().toString());
			discount=JSONArray.fromObject(dataMap).toString();
		}else {
			Map<String,String> dataMap = new HashMap<String, String>();
			dataMap.put("result", "false");
			
			discount=JSONArray.fromObject(dataMap).toString();
			System.out.println(discount);
		}
		return "success";
	}
	public String getAreaCfgCode() {
		return areaCfgCode;
	}
	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}
	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public IMinSaleUnitDetailService getService() {
		return service;
	}
	public void setService(IMinSaleUnitDetailService service) {
		this.service = service;
	}
	
}
