package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 最小销售单位详细信息
 * @author caozhiyong
 * @version 2013-1-10
 */
public class MinSaleUnitDetailVO implements Serializable{
	
	/**
	 * 序列号
	 */
	private static final long serialVersionUID = 1L;
	
	private String price;//价格
	private String date;//到期时间
	private String minSaleUnitName;//软件名称（最小销售单位名称）
	private String minSaleUnitCode;//最小销售单位code
	private String areaCfgCode;//区域配置code
	private String languageCode;//语言code
	private String memo;//最小销售单位说明
	private String discountPrice;//折后价格
	private String picpath;//最小销售单位图片
	private String memoDetail;//最小销售单位详细说明
	private String upCode;
	private int rentYears=1;
	
	private int isValid=0;//是否过期（0，未过期，1，过期）
	
	public MinSaleUnitDetailVO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public MinSaleUnitDetailVO(String price, String date,
			String minSaleUnitName, String minSaleUnitCode, String areaCfgCode,
			String languageCode, String memo, String discountPrice,
			int rentYears, int isValid) {
		super();
		this.price = price;
		this.date = date;
		this.minSaleUnitName = minSaleUnitName;
		this.minSaleUnitCode = minSaleUnitCode;
		this.areaCfgCode = areaCfgCode;
		this.languageCode = languageCode;
		this.memo = memo;
		this.discountPrice = discountPrice;
		this.rentYears = rentYears;
		this.isValid = isValid;
	}


	public String getDiscountPrice() {
		return discountPrice;
	}


	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getMinSaleUnitName() {
		return minSaleUnitName;
	}


	public void setMinSaleUnitName(String minSaleUnitName) {
		this.minSaleUnitName = minSaleUnitName;
	}

	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}


	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}


	public String getAreaCfgCode() {
		return areaCfgCode;
	}


	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}


	public String getLanguageCode() {
		return languageCode;
	}


	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}


	public String getMemo() {
		return memo;
	}


	public void setMemo(String memo) {
		this.memo = memo;
	}


	public int getRentYears() {
		return rentYears;
	}


	public void setRentYears(int rentYears) {
		this.rentYears = rentYears;
	}


	public int getIsValid() {
		return isValid;
	}


	public void setIsValid(int isValid) {
		this.isValid = isValid;
	}


	public String getPicpath() {
		return picpath;
	}


	public void setPicpath(String picpath) {
		this.picpath = picpath;
	}


	public String getMemoDetail() {
		return memoDetail;
	}


	public void setMemoDetail(String memoDetail) {
		this.memoDetail = memoDetail;
	}


	public String getUpCode() {
		return upCode;
	}


	public void setUpCode(String upCode) {
		this.upCode = upCode;
	}
	
	
	
}
