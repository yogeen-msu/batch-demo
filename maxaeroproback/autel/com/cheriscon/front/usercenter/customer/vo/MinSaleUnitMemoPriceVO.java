package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

import com.cheriscon.common.model.MinSaleUnitMemo;

/**
 * 最小销售单位与出厂配置价格信息
 * @author caozhiyong
 * @version 2013-1-10
 */
public class MinSaleUnitMemoPriceVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String price; //出厂价格
	
	private MinSaleUnitMemo minSaleUnitMemo=new MinSaleUnitMemo(); //最小销售单位说明

	private String validDate;//产品软件有效期
	
	private String areaCfgCode;//区域配置code
	
	private String saleCfgCode;//销售配置code
	
	private String saleCfgName;//销售配置名称
	
	private String cfgpicpath;
	private String cfgMemo;
	
	public MinSaleUnitMemoPriceVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public MinSaleUnitMemoPriceVO(String price,
			MinSaleUnitMemo minSaleUnitMemo, String validDate,
			String areaCfgCode, String saleCfgCode, String saleCfgName) {
		super();
		this.price = price;
		this.minSaleUnitMemo = minSaleUnitMemo;
		this.validDate = validDate;
		this.areaCfgCode = areaCfgCode;
		this.saleCfgCode = saleCfgCode;
		this.saleCfgName = saleCfgName;
	}



	public String getSaleCfgName() {
		return saleCfgName;
	}



	public void setSaleCfgName(String saleCfgName) {
		this.saleCfgName = saleCfgName;
	}



	public String getSaleCfgCode() {
		return saleCfgCode;
	}

	public void setSaleCfgCode(String saleCfgCode) {
		this.saleCfgCode = saleCfgCode;
	}

	public String getAreaCfgCode() {
		return areaCfgCode;
	}

	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}

	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public MinSaleUnitMemo getMinSaleUnitMemo() {
		return minSaleUnitMemo;
	}


	public void setMinSaleUnitMemo(MinSaleUnitMemo minSaleUnitMemo) {
		this.minSaleUnitMemo = minSaleUnitMemo;
	}


	public String getValidDate() {
		return validDate;
	}


	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}



	public String getCfgpicpath() {
		return cfgpicpath;
	}



	public void setCfgpicpath(String cfgpicpath) {
		this.cfgpicpath = cfgpicpath;
	}



	public String getCfgMemo() {
		return cfgMemo;
	}



	public void setCfgMemo(String cfgMemo) {
		this.cfgMemo = cfgMemo;
	}



	

	

}
