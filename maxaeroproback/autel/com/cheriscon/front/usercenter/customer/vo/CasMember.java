package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;
import java.util.Date;


public class CasMember implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5265948740456548734L;
	
	private String id;
	// 登陆名称
	private String loginName;
	// 登陆密码
	private String loginPassword;
	/** 邮箱 **/
	private String email;
	/** 名 **/
	private String memberName;
	/** 姓 **/
	private String lastName;

	// 上次登陆时间
	private String lastTimeOut;
	// 登陆次数
	private Integer lastCount = 0;
	// 删除状态 1 未删除 2 已删除
	private Short delFlag;
	// 注册ip
	private String registerIp;
	// 注册时间
	private String registerDate;
	// 头像
	private String portrait;
	// 1为管理员 其他非管理员
	private Integer isAdmin;
	// 锁定状态 1未锁定 0 已锁定
	private Short state;
	// 激活验证码
	private String authCode;
	// 验证码生成时间
	private Date authCodeTime;
	// 默认语言
	private String defaultLanguage;
	// 默认国家
	private String defaultCountry;

	private Date lockTime;
	private int errorNum;
	private Date errorDate;

	/**
	 * autelproback customerinfo
	 */
	private String firstName; // 第一个名字
	private String middleName; // 中间名字
	private String name; // 用户名
	private String daytimePhone; // 固定电话
	private String daytimePhoneCC; // 固定电话国家代码
	private String daytimePhoneAC; // 固定电话区域代码
	private String daytimePhoneExtCode; // 固定电话分机号
	private String mobilePhone; // 移动电话
	private String mobilePhoneCC; // 移动电话国家代码
	private String mobilePhoneAC; // 移动电话区域代码
	private String regTime; // 注册时间
	private String languageCode; // 语言code
	private String country; // 国家
	private String city; // 城市
	private String address; // 地址
	private String company; // 公司
	private String zipCode; // 邮编
	private String questionCode; // 安全问题code
	private String answer; // 安全问题答案
	private String comUsername; // 论坛名字
	private Integer actState; // autelId激活状态
	private String actCode; // 激活码
	private Integer isAllowSendEmail; // 是否允许发邮件
	private String lastLoginTime; // 最后登录时间
	private String secondEmail; // 用户第二邮箱
	private String sendActiveTime; // 发送邮箱账号激活时间
	private String sendResetPasswordTime;// 发送重置密码时间
	private Integer secondActState;// 第二邮箱激活状态(0:未激活 1：已激活)
	private String sendSecondEmailActiveTime;// 发送第二邮箱账号激活时间
	private Integer sourceType; // 数据来源途径，0为产品网站注册用户，1为企业网站用户，2为MaxiDas用户

	private String serialNo;// 产品序列号 冗余字段
	private String proRegTime;// 产品注册时间冗余字段
	private String expTime;// 产品过期时间冗余字段
	private String comName;// 用户全名=第一个名字+中间名字+最后名字
	private String province; // 州/省
	private String proDate; // 产品出厂日期
	private String Warrantymonth; // 硬件保修期
	private String warrantyDate;// 硬件保修期过期日期

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastTimeOut() {
		return lastTimeOut;
	}

	public void setLastTimeOut(String lastTimeOut) {
		this.lastTimeOut = lastTimeOut;
	}

	public Integer getLastCount() {
		return lastCount;
	}

	public void setLastCount(Integer lastCount) {
		this.lastCount = lastCount;
	}

	public Short getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Short delFlag) {
		this.delFlag = delFlag;
	}

	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}

	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public Integer getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Integer isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Short getState() {
		return state;
	}

	public void setState(Short state) {
		this.state = state;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public Date getAuthCodeTime() {
		return authCodeTime;
	}

	public void setAuthCodeTime(Date authCodeTime) {
		this.authCodeTime = authCodeTime;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public String getDefaultCountry() {
		return defaultCountry;
	}

	public void setDefaultCountry(String defaultCountry) {
		this.defaultCountry = defaultCountry;
	}

	public Date getLockTime() {
		return lockTime;
	}

	public void setLockTime(Date lockTime) {
		this.lockTime = lockTime;
	}

	public int getErrorNum() {
		return errorNum;
	}

	public void setErrorNum(int errorNum) {
		this.errorNum = errorNum;
	}

	public Date getErrorDate() {
		return errorDate;
	}

	public void setErrorDate(Date errorDate) {
		this.errorDate = errorDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDaytimePhone() {
		return daytimePhone;
	}

	public void setDaytimePhone(String daytimePhone) {
		this.daytimePhone = daytimePhone;
	}

	public String getDaytimePhoneCC() {
		return daytimePhoneCC;
	}

	public void setDaytimePhoneCC(String daytimePhoneCC) {
		this.daytimePhoneCC = daytimePhoneCC;
	}

	public String getDaytimePhoneAC() {
		return daytimePhoneAC;
	}

	public void setDaytimePhoneAC(String daytimePhoneAC) {
		this.daytimePhoneAC = daytimePhoneAC;
	}

	public String getDaytimePhoneExtCode() {
		return daytimePhoneExtCode;
	}

	public void setDaytimePhoneExtCode(String daytimePhoneExtCode) {
		this.daytimePhoneExtCode = daytimePhoneExtCode;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMobilePhoneCC() {
		return mobilePhoneCC;
	}

	public void setMobilePhoneCC(String mobilePhoneCC) {
		this.mobilePhoneCC = mobilePhoneCC;
	}

	public String getMobilePhoneAC() {
		return mobilePhoneAC;
	}

	public void setMobilePhoneAC(String mobilePhoneAC) {
		this.mobilePhoneAC = mobilePhoneAC;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getComUsername() {
		return comUsername;
	}

	public void setComUsername(String comUsername) {
		this.comUsername = comUsername;
	}

	public Integer getActState() {
		return actState;
	}

	public void setActState(Integer actState) {
		this.actState = actState;
	}

	public String getActCode() {
		return actCode;
	}

	public void setActCode(String actCode) {
		this.actCode = actCode;
	}

	public Integer getIsAllowSendEmail() {
		return isAllowSendEmail;
	}

	public void setIsAllowSendEmail(Integer isAllowSendEmail) {
		this.isAllowSendEmail = isAllowSendEmail;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getSecondEmail() {
		return secondEmail;
	}

	public void setSecondEmail(String secondEmail) {
		this.secondEmail = secondEmail;
	}

	public String getSendActiveTime() {
		return sendActiveTime;
	}

	public void setSendActiveTime(String sendActiveTime) {
		this.sendActiveTime = sendActiveTime;
	}

	public String getSendResetPasswordTime() {
		return sendResetPasswordTime;
	}

	public void setSendResetPasswordTime(String sendResetPasswordTime) {
		this.sendResetPasswordTime = sendResetPasswordTime;
	}

	public Integer getSecondActState() {
		return secondActState;
	}

	public void setSecondActState(Integer secondActState) {
		this.secondActState = secondActState;
	}

	public String getSendSecondEmailActiveTime() {
		return sendSecondEmailActiveTime;
	}

	public void setSendSecondEmailActiveTime(String sendSecondEmailActiveTime) {
		this.sendSecondEmailActiveTime = sendSecondEmailActiveTime;
	}

	public Integer getSourceType() {
		return sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getProRegTime() {
		return proRegTime;
	}

	public void setProRegTime(String proRegTime) {
		this.proRegTime = proRegTime;
	}

	public String getExpTime() {
		return expTime;
	}

	public void setExpTime(String expTime) {
		this.expTime = expTime;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getProDate() {
		return proDate;
	}

	public void setProDate(String proDate) {
		this.proDate = proDate;
	}

	public String getWarrantymonth() {
		return Warrantymonth;
	}

	public void setWarrantymonth(String warrantymonth) {
		Warrantymonth = warrantymonth;
	}

	public String getWarrantyDate() {
		return warrantyDate;
	}

	public void setWarrantyDate(String warrantyDate) {
		this.warrantyDate = warrantyDate;
	}

}
