package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 我的订单信息
 * 
 * @author caozhiyong
 * @version 2013-1-22
 */
public class MyOrderInfoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 订单金额
	 */
	private String orderMoney;
	/**
	 * 下单时间
	 */
	private String orderDate;
	/**
	 * 订单状态
	 */
	private Integer orderState;
	/**
	 * 订单支付状态
	 */
	private Integer orderPayState;
	/**
	 * 优惠金额
	 */
	private String discountMoney;
	
	/**
	 * 订单编号
	 */
	private String orderNo;

	/**
	 * 订单最小销售单位集合
	 */
	private List<OrderMinSaleUnitInfoVO> orderMinSaleUnitInfos=new ArrayList<OrderMinSaleUnitInfoVO>();
	
	public MyOrderInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MyOrderInfoVO(String orderMoney, String orderDate,
			Integer orderState, Integer orderPayState, String discountMoney,
			String orderNo, List<OrderMinSaleUnitInfoVO> orderMinSaleUnitInfos) {
		super();
		this.orderMoney = orderMoney;
		this.orderDate = orderDate;
		this.orderState = orderState;
		this.orderPayState = orderPayState;
		this.discountMoney = discountMoney;
		this.orderNo = orderNo;
		this.orderMinSaleUnitInfos = orderMinSaleUnitInfos;
	}

	public String getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(String orderMoney) {
		this.orderMoney = orderMoney;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getOrderState() {
		return orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	public Integer getOrderPayState() {
		return orderPayState;
	}

	public void setOrderPayState(Integer orderPayState) {
		this.orderPayState = orderPayState;
	}

	public String getDiscountMoney() {
		return discountMoney;
	}

	public void setDiscountMoney(String discountMoney) {
		this.discountMoney = discountMoney;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public List<OrderMinSaleUnitInfoVO> getOrderMinSaleUnitInfos() {
		return orderMinSaleUnitInfos;
	}

	public void setOrderMinSaleUnitInfos(
			List<OrderMinSaleUnitInfoVO> orderMinSaleUnitInfos) {
		this.orderMinSaleUnitInfos = orderMinSaleUnitInfos;
	}
	
}
