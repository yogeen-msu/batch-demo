package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 功能软件信息
 * @author caozhiyong
 * @version 2013-4-9
 */
public class MinSaleUnitSoftwareVO  implements Serializable{

	private static final long serialVersionUID = 1L;

	private String name;//软件名称
	
	private String versionNo;//软件版本号
	
	/**
	 * 发布日期(精确到日期)
	 */
	private String releaseDate;
	
	private String versionMemo;//版本说明
	
	private String versionCode;

	/**
	 * 发布日期(精确到时分秒)
	 */
	private String releaseDate2;
	
	public MinSaleUnitSoftwareVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public MinSaleUnitSoftwareVO(String name, String versionNo,
			String releaseDate, String versionMemo, String versionCode,
			String releaseDate2) {
		super();
		this.name = name;
		this.versionNo = versionNo;
		this.releaseDate = releaseDate;
		this.versionMemo = versionMemo;
		this.versionCode = versionCode;
		this.releaseDate2 = releaseDate2;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getVersionMemo() {
		return versionMemo;
	}

	public void setVersionMemo(String versionMemo) {
		this.versionMemo = versionMemo;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}



	public String getReleaseDate2() {
		return releaseDate2;
	}



	public void setReleaseDate2(String releaseDate2) {
		this.releaseDate2 = releaseDate2;
	}
	
	
}
