package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 订单信息
 * @author caozhiyong
 * @version 2013-1-17
 */
public class OrderInfoVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private String allMoney;//订单金额
	private String discountAllMoney;//优惠总金额
	private String customerCode;//用户code
	private List<OneOrderInfoVo> orderInfoVos=new ArrayList<OneOrderInfoVo>();//订单产品信息集合

	private String orderCode;//订单编号
	private String orderDate;
	
	public OrderInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public OrderInfoVO(String allMoney, String discountAllMoney,
			String customerCode, List<OneOrderInfoVo> orderInfoVos,
			String orderCode, String orderDate) {
		super();
		this.allMoney = allMoney;
		this.discountAllMoney = discountAllMoney;
		this.customerCode = customerCode;
		this.orderInfoVos = orderInfoVos;
		this.orderCode = orderCode;
		this.orderDate = orderDate;
	}


	public String getAllMoney() {
		return allMoney;
	}

	public void setAllMoney(String allMoney) {
		this.allMoney = allMoney;
	}

	public String getDiscountAllMoney() {
		return discountAllMoney;
	}

	public void setDiscountAllMoney(String discountAllMoney) {
		this.discountAllMoney = discountAllMoney;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public List<OneOrderInfoVo> getOrderInfoVos() {
		return orderInfoVos;
	}

	public void setOrderInfoVos(List<OneOrderInfoVo> orderInfoVos) {
		this.orderInfoVos = orderInfoVos;
	}

	
	public String getOrderCode() {
		return orderCode;
	}


	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}


	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	
	
	
	
}
