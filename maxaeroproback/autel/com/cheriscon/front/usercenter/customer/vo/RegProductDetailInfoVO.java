package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 注册产品详细信息
 * @author caozhiyong
 * @version 2013-4-2
 */
public class RegProductDetailInfoVO  implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String code;
	private String serialNo;					//产品序列号
	private String saleContractCode;			//销售契约Code
	private String saleContractName;			//销售契约名称
	private String proTypeCode;					//产品型号Code
	private String proTypeName;					//产品型号名称
	private String regPwd;						//产品注册密码
	private String proDate;						//产品出厂日期
	private Integer regStatus;					//注册状态
	private String regTime;						//注册时间
	private String noRegReason;					//解绑原因
	
	private String sealerCode;
	private String autelId;
	
	
	public RegProductDetailInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegProductDetailInfoVO(Integer id, String code, String serialNo,
			String saleContractCode, String saleContractName,
			String proTypeCode, String proTypeName, String regPwd,
			String proDate, Integer regStatus, String regTime,
			String noRegReason, String sealerCode, String autelId) {
		super();
		this.id = id;
		this.code = code;
		this.serialNo = serialNo;
		this.saleContractCode = saleContractCode;
		this.saleContractName = saleContractName;
		this.proTypeCode = proTypeCode;
		this.proTypeName = proTypeName;
		this.regPwd = regPwd;
		this.proDate = proDate;
		this.regStatus = regStatus;
		this.regTime = regTime;
		this.noRegReason = noRegReason;
		this.sealerCode = sealerCode;
		this.autelId = autelId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getSaleContractCode() {
		return saleContractCode;
	}

	public void setSaleContractCode(String saleContractCode) {
		this.saleContractCode = saleContractCode;
	}

	public String getSaleContractName() {
		return saleContractName;
	}

	public void setSaleContractName(String saleContractName) {
		this.saleContractName = saleContractName;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	public String getRegPwd() {
		return regPwd;
	}

	public void setRegPwd(String regPwd) {
		this.regPwd = regPwd;
	}

	public String getProDate() {
		return proDate;
	}

	public void setProDate(String proDate) {
		this.proDate = proDate;
	}

	public Integer getRegStatus() {
		return regStatus;
	}

	public void setRegStatus(Integer regStatus) {
		this.regStatus = regStatus;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public String getNoRegReason() {
		return noRegReason;
	}

	public void setNoRegReason(String noRegReason) {
		this.noRegReason = noRegReason;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}
	
	
	
	
	
}
