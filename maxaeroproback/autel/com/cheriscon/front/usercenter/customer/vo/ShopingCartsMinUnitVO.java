package com.cheriscon.front.usercenter.customer.vo;

import java.io.Serializable;

/**
 * 购物车产品对应的最小销售单位集合
 * @author caozhiyong
 * @version 2013-3-14
 */
public class ShopingCartsMinUnitVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String customerCode;//客户code
	private String minSaleUnitCode;//最小销售单位code
	private String areaCfgCode;//区域配置code
	private Integer year;//续租年限
	private Integer type;//消费类型（0、购买，1、续租）
	private String name;//最小销售单位名称
	private String costPrice;//原价
	private String discountPrice;//折后价
	private String sparePrice="0";//节省
	private String validDate;//有效期
	private Integer isSoft;
	private int isValid=0;//是否过期（0，未过期，1，过期）
	
	public ShopingCartsMinUnitVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	



	public ShopingCartsMinUnitVO(String customerCode, String minSaleUnitCode,
			String areaCfgCode, Integer year, Integer type, String name,
			String costPrice, String discountPrice, String sparePrice,
			String validDate, Integer isSoft, int isValid) {
		super();
		this.customerCode = customerCode;
		this.minSaleUnitCode = minSaleUnitCode;
		this.areaCfgCode = areaCfgCode;
		this.year = year;
		this.type = type;
		this.name = name;
		this.costPrice = costPrice;
		this.discountPrice = discountPrice;
		this.sparePrice = sparePrice;
		this.validDate = validDate;
		this.isSoft = isSoft;
		this.isValid = isValid;
	}





	public Integer getIsSoft() {
		return isSoft;
	}

	public void setIsSoft(Integer isSoft) {
		this.isSoft = isSoft;
	}

	public String getValidDate() {
		return validDate;
	}

	public void setValidDate(String validDate) {
		this.validDate = validDate;
	}

	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getMinSaleUnitCode() {
		return minSaleUnitCode;
	}
	public void setMinSaleUnitCode(String minSaleUnitCode) {
		this.minSaleUnitCode = minSaleUnitCode;
	}
	public String getAreaCfgCode() {
		return areaCfgCode;
	}
	public void setAreaCfgCode(String areaCfgCode) {
		this.areaCfgCode = areaCfgCode;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}
	public String getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}
	public String getSparePrice() {
		return sparePrice;
	}
	public void setSparePrice(String sparePrice) {
		this.sparePrice = sparePrice;
	}





	public int getIsValid() {
		return isValid;
	}





	public void setIsValid(int isValid) {
		this.isValid = isValid;
	}
	
	
}
