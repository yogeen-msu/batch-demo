package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IBuySoftRentService;
import com.cheriscon.front.usercenter.customer.service.ICfgSoftRentService;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.BuySoftInfoVO;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;

/**
 * 产品型号
 * @author caozhiyong
 * @version 2013-1-10
 */
@Component("softRent")
public class SoftRentWidget extends RequestParamWidget {

	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private ICfgSoftRentService cfgSoftRentService;
	@Resource 
	private IBuySoftRentService buySoftRentService;
	
	@Resource
	private IMinSaleUnitDetailService service;
	
	private CfgSoftRentInfoVO rentInfoVO = new CfgSoftRentInfoVO();
	
	private BuySoftInfoVO buySoftRentInfoVO = new BuySoftInfoVO();
	@Resource 
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
		String serial  = params.get("serial");//序列号
		String proName = params.get("proName");//产品名称
		String saleContractCode = params.get("saleContractCode");//销售契约code
		String proCode = params.get("proCode");//产品code
		String picPath = params.get("picPath");//产品图片
		
		String pagesize=params.get("pagesize");
		String pageNoStr = params.get("pageno");
		Integer[] ids = this.parseId();
		Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
		if (pageNo == 1) {
			pagesize="5";
			cfgSoftRent(customerInfo,serial, proName, saleContractCode, proCode,picPath);
		}
		
		buySoftRentInfoVO.setProName(proName);
		buySoftRentInfoVO.setProSerial(serial);
		buySoftRentInfoVO.setProCode(proCode);
		buySoftRentInfoVO.setPicPath(picPath);
		buySoftRent(customerInfo,saleContractCode, proCode,pageNo,Integer.parseInt(pagesize));
	}

	/**
	 * 自购软件信息
	 * @param serial
	 * @param proName
	 * @param saleContractCode
	 * @param proCode
	 */
	@SuppressWarnings("unchecked")
	private void buySoftRent(CustomerInfo customerInfo,String saleContractCode, String proCode,int pageNo, int pageSize) {
		
		Page page=buySoftRentService.queryBuySoftRent(saleContractCode, proCode,pageNo,pageSize,customerInfo.getLanguageCode());

		List<MinSaleUnitDetailVO> detailVOs =(List<MinSaleUnitDetailVO>)page.getResult();
		buySoftRentInfoVO.getMinSaleUnitDetailVOs().clear();
		for (int i = 0; i < detailVOs.size(); i++) {
			MinSaleUnitDetailVO minSaleUnit=detailVOs.get(i);
			
			//查询最小销售单位是否可参与活动
			MarketPromotionDiscountInfoVO  info=minSaleUnitDetailService.queryDiscount(customerInfo, minSaleUnit.getMinSaleUnitCode(), minSaleUnit.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT, minSaleUnit.getDate());
			String discountPrice=minSaleUnit.getPrice();
			minSaleUnit.setPrice(Float.valueOf(discountPrice).toString());
			if (info!=null) {
				//折后价
				discountPrice=String.valueOf((Float.parseFloat(minSaleUnit.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
			}
			minSaleUnit.setDiscountPrice(Float.valueOf(discountPrice).toString());
			
			Date date = DateUtil.toDate(minSaleUnit.getDate().trim(), "MM/dd/yyyy");
			Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "MM/dd/yyyy"), "MM/dd/yyyy");
			//当前时间大于有效期时间(已经过期)
			if(currentDate.after(date)){
				minSaleUnit.setIsValid(1);
			}
			
			buySoftRentInfoVO.getMinSaleUnitDetailVOs().add(minSaleUnit);
		}
		
		StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, page.getTotalCount(), Integer.valueOf(pageSize));
		String page_html=pagerHtmlBuilder.buildPageHtml();
		long totalPageCount =page.getTotalPageCount();
		long totalCount = page.getTotalCount();
		
		/**
		 * 如果"A"最小销售单位可以升级到"B"最小销售单位，那么将"A"最小销售单位隐藏
		 */
		for (int i = 0; i < buySoftRentInfoVO.getMinSaleUnitDetailVOs().size(); i++) {
			MinSaleUnitDetailVO minSaleUnit=buySoftRentInfoVO.getMinSaleUnitDetailVOs().get(i);
			if (!( minSaleUnit.getUpCode()==null ||minSaleUnit.getUpCode().equals(""))) {
				for (int j = 0; j < buySoftRentInfoVO.getMinSaleUnitDetailVOs().size(); j++) {
					MinSaleUnitDetailVO minSaleUnit2=buySoftRentInfoVO.getMinSaleUnitDetailVOs().get(j);
					if (minSaleUnit.getUpCode().equals(minSaleUnit2.getMinSaleUnitCode())) {
						buySoftRentInfoVO.getMinSaleUnitDetailVOs().remove(i);
					}
				}
			}
		}
		
		this.putData("buySoftRentInfoVO",buySoftRentInfoVO);
		
		this.putData("minSaleUnitDetailVOs",buySoftRentInfoVO.getMinSaleUnitDetailVOs());
		
		this.putData("pager", page_html); 
		this.putData("pagesize", pageSize);
		this.putData("pageno", pageNo);
		this.putData("totalcount", totalCount);
		this.putData("totalpagecount",totalPageCount);
	}

	/**
	 * 出厂配置软件信息
	 * @param serial
	 * @param proName
	 * @param saleContractCode
	 * @param proCode
	 */
	private void cfgSoftRent(CustomerInfo customerInfo,String serial, String proName,
			String saleContractCode, String proCode,String picPath) {
			
		//查询产品出厂配置软件信息
		rentInfoVO=cfgSoftRentService.querySoftRent(saleContractCode,proCode,customerInfo);
		rentInfoVO.setProSerial(serial);
		rentInfoVO.setProName(proName);
		rentInfoVO.setProCode(proCode);
		rentInfoVO.setPicPath(picPath);
		
		MarketPromotionDiscountInfoVO info=service.querySaleCfgDiscount(customerInfo, rentInfoVO.getSaleCfgCode(), rentInfoVO.getAreaCfgCode(), FrontConstant.USER_IS_SOFT_RENT,rentInfoVO.getDate());
		String discountPrice=rentInfoVO.getPrice();
		rentInfoVO.setPrice(Float.valueOf(discountPrice).toString());
		if (info!=null) {
			//折后价
			discountPrice=String.valueOf((Float.parseFloat(rentInfoVO.getPrice())*1000 * Float.parseFloat(String.valueOf( info.getDiscount())))/10/1000);
		}
		Date date = DateUtil.toDate(rentInfoVO.getDate().trim(), "yyyy/MM/dd");
		Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
		//当前时间大于有效期时间(已经过期)
		if(currentDate.after(date)){
			rentInfoVO.setIsValid(1);
		}
		
		rentInfoVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
		
		this.putData("cfgSoftRentInfoVO",rentInfoVO);
		this.putData("minSaleUnitsMemos",rentInfoVO.getMinSaleUnitsMemo());
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

	public CfgSoftRentInfoVO getRentInfoVO() {
		return rentInfoVO;
	}

	public void setRentInfoVO(CfgSoftRentInfoVO rentInfoVO) {
		this.rentInfoVO = rentInfoVO;
	}

	public BuySoftInfoVO getBuySoftRentInfoVO() {
		return buySoftRentInfoVO;
	}

	public void setBuySoftRentInfoVO(BuySoftInfoVO buySoftRentInfoVO) {
		this.buySoftRentInfoVO = buySoftRentInfoVO;
	}
	

}
