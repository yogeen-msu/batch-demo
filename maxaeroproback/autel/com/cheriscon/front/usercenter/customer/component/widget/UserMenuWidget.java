package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.member.service.ISealerAuthService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsMinUnitVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;
import com.cheriscon.front.usercenter.distributor.model.SealerAuthVO;


@Component("userMenuWidget")
public class UserMenuWidget extends RequestParamWidget 
{
	@Resource 
	private IShoppingCartService service;
	
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	
	@Resource private ISealerAuthService sealerAuthService;

	@Override
	protected void display(Map<String, String> params)
	{
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		
		this.putData("userType",request.getParameter("userType"));
		this.putData("menuItem",params.get("menuItem"));
		this.putData("proCount",request.getSession().getAttribute("proCount"));
		this.putData("updateFlag",request.getSession().getAttribute("updateFlag"));
		HttpSession session = CopContext.getHttpRequest().getSession();
		

		
		Object object=(Object)SessionUtil.getLoginUserInfo(request);
		
		//TODO 经销商左边导航权限
		List<SealerAuthVO> leftSealerAuth=null; 
		if(object!=null){
			if(object.getClass().equals(SealerInfo.class)){
				leftSealerAuth=sealerAuthService.list(((SealerInfo)object).getCode());
			}
		}
		session.setAttribute("leftMenu", leftSealerAuth);
		this.putData("leftMenu",leftSealerAuth);
		
		
		if (object.getClass().equals(CustomerInfo.class)) {
			CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
			String usercode=customerInfo.getCode();//用户code
			String languageCode=customerInfo.getLanguageCode();//用户首选语言code
			
			List<ShopingCartsProListVO> proListVOs=service.queryShoppingCartByCustomerCode(usercode,languageCode);
			int items=0;
			for (int i = 0; i < proListVOs.size(); i++) {
				for (int j = 0; j < proListVOs.get(i).getMinUnitVOs().size(); j++) {
					
					ShopingCartsMinUnitVO minUnitVO=proListVOs.get(i).getMinUnitVOs().get(j);
					
					MarketPromotionDiscountInfoVO info;
					if (minUnitVO.getIsSoft() == 1) {
						// 查询最小销售单位是否可参与活动
						info = minSaleUnitDetailService.queryDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
										minUnitVO.getType(),
										minUnitVO.getValidDate());
					} else {
						// 查询销售配置是否可参与活动
						info = minSaleUnitDetailService.querySaleCfgDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
										minUnitVO.getType(),
										minUnitVO.getValidDate()); 
					}
					String discountPrice = minUnitVO.getCostPrice();
					minUnitVO.setCostPrice(Float.valueOf(discountPrice).toString());
					if (info != null) {
						// 折后价
						discountPrice = String.valueOf((Float.parseFloat(minUnitVO.getCostPrice())*1000 * Float.parseFloat(String.valueOf(info.getDiscount()))) / 10/1000);
					}
					minUnitVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
					if(minUnitVO.getValidDate() != null){
						Date date = null;
						if(minUnitVO.getValidDate().indexOf("/")>-1){
							 date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy/MM/dd");
							}
							else{
							 date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy-MM-dd");	
							}
						Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
						//当前时间大于有效期时间(已经过期)
						if(currentDate.after(date)){
							minUnitVO.setIsValid(1);
						}
					}
					
					items++;
					proListVOs.get(i).getMinUnitVOs().set(j, minUnitVO);
				}
			}
			this.putData("shoppingCart",proListVOs);
			this.putData("items",items);
		}
		
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}
}
