package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;

/**
 * 个人客户会员-我的产品
 * @date:2013-1-5
 * @author caozhiyong 
 */
@Component("myProduct")
public class MyProductWidget extends RequestParamWidget {

	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private IMyAccountService myAccountService;

	@Resource
	private ICustomerProductService customerProductService;
	
	private List<String> proSerialNos;
	private List<MyProduct> myProducts;
	private Page page = null;

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		String usercode=customerInfo.getCode();//用户code
		String pageSize = params.get("pageSize")==null?"2":params.get("pageSize");
		String remark="";
		try {
			remark = params.get("remark")==null?"":new String(params.get("remark").getBytes( "iso-8859-1" ), "UTF-8" );
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		this.putData("remark",remark);
		
		try {
			int productCount=myAccountService.queryProductCount(usercode);
			this.putData("productCount", productCount);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String myaccountpageno =params.get("myaccountpageno");
		
		if(myaccountpageno==null || myaccountpageno.equals("")){
			String pageNoStr = params.get("pageno");

			Integer[] ids = this.parseId();
			Integer pageNo = StringUtil.isEmpty(pageNoStr)? ids[0] : Integer.valueOf(pageNoStr);
			try {
				// 根据客户Code查询客户的产品信息
				page = myAccountService.queryProducts(remark,usercode,
						Integer.valueOf(pageNo), Integer.valueOf(pageSize));
				 String showFlag="no";
				 String areaCode=FrontConstant.getProperValue("saleconfig.area.usa");
				 int num=customerProductService.queryProductArea(areaCode,customerInfo.getCode());
				 if(num>0){
					 showFlag="yes";
				 }
				 this.putData("showFlag", showFlag);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
			StaticPagerHtmlBuilder pagerHtmlBuilder=new StaticPagerHtmlBuilder(pageNo, page.getTotalCount(), Integer.valueOf(pageSize));
			String page_html=pagerHtmlBuilder.buildPageHtml();
			long totalPageCount =page.getTotalPageCount();
			long totalCount = page.getTotalCount();
			this.putData("pager", page_html); 
			this.putData("pagesize", pageSize);
			this.putData("pageno", pageNo);
			this.putData("totalcount", totalCount);
			this.putData("totalpagecount",totalPageCount);
		}else {
			try {
				// 根据客户Code查询客户的产品信息
				page = myAccountService.queryProducts(remark,usercode,
						Integer.valueOf(myaccountpageno), Integer.valueOf(pageSize));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.putData("myProducts", page.getResult());
		
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

	public List<String> getProSerialNos() {
		return proSerialNos;
	}

	public void setProSerialNos(List<String> proSerialNos) {
		this.proSerialNos = proSerialNos;
	}

	public List<MyProduct> getMyProducts() {
		return myProducts;
	}

	public void setMyProducts(List<MyProduct> myProducts) {
		this.myProducts = myProducts;
	}

}
