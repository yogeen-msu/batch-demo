package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.order.service.IOrderInfoBackService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.common.model.PayLog;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.customer.service.IPayService;

/**
 * 续租卡续租
 * @author chenqichuan
 * @version 2013-10-8
 */
@Component("payErrorWidget")
public class PayErrorWidget extends RequestParamWidget{


	@Resource 
	private IPayService payService ;
	@Resource
	private IOrderInfoBackService orderInfoBackService;
	
	
	//记录操作失败日志
	   public void insertErrorLog(String orderNo,String remark){
		   PayErrorLog errorLog =new PayErrorLog();
		   String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		   HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		   String ip=FrontConstant.getIpAddr(request);
		   errorLog.setIp(ip);
		   errorLog.setErrorDate(date);
		   errorLog.setRemark(remark);
		   errorLog.setOrderNo(orderNo);
		   orderInfoBackService.payErrorLog(errorLog);
		}

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String orderNo=request.getSession().getAttribute("orderNo").toString();
		System.out.println(request.getParameter("RESPMSG"));
		System.out.println(request.getParameter("FPS_PREXMLDATA"));
		
		String PPREF=params.get("PPREF")==null?null:params.get("PPREF");
		
		PayLog payLog=new PayLog();
	    payLog.setOut_trade_no(orderNo);
	    
	    payLog.setResult(FrontConstant.PAY_FALSE);
		
	    String ip=FrontConstant.getIpAddr(request);
	    String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
	    payLog.setIp(ip);
	    payLog.setCreateTime(date);
	    
	    payLog.setTrade_no(PPREF);
	    payLog.setTrade_status("信用卡支付失败");
	    
	    try {
			payService.insertPayLog(payLog);
		} catch (Exception e) {
			insertErrorLog(orderNo,"信用卡失败返回-响应失败：插入支付日志错误");
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}


	
}
