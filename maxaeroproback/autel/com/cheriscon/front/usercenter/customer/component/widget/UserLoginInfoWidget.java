package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.UserLoginInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.user.service.IUserLoginInfoService;

/**
 * 用户登录信息
 * @author caozhiyong
 * @version 创建时间：2013-1-7
 */
//@Component("userLoginInfo")
@Component
public class UserLoginInfoWidget extends AbstractWidget {

	@Resource
	private IUserLoginInfoService userLoginInfoService;

	@Override
	protected void display(Map<String, String> params) {

		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
	
		//用户登录信息
		UserLoginInfo userLoginInfo=null;
		try {
			List<UserLoginInfo> infos=userLoginInfoService.queryUserLoginInfoByCode(customerInfo.getCode());
			if (infos.size()>1) {
				userLoginInfo = infos.get(1);//获得用户上次登录信息
				userLoginInfo.setLoginTime(DateUtil.toString(DateUtil.toDate(userLoginInfo.getLoginTime(),"yyyy-MM-dd hh:mm:ss"),"MM/dd/yyyy hh:mm:ss"));
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.putData("userLoginInfo", userLoginInfo);
		this.putData("userName", customerInfo.getAutelId());//用户AutelID
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

}
