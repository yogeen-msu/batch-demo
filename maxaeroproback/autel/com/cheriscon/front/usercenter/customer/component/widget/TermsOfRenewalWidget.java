package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.system.component.plugin.rate.RatePlugin;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IPaymentCfgService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.PaymentCfg;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * 支付
 * @author caozhiyong
 * @version 2013-1-17
 */
@Component("termsOfRenewalWidget")
public class TermsOfRenewalWidget extends RequestParamWidget{

	@Override
	protected void display(Map<String, String> params) {
		
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

	
}
