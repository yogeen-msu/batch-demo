package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsItemType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentInfoType;

import com.cheriscon.backstage.order.service.IOrderInfoBackService;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.common.model.PayLog;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.cheriscon.front.usercenter.customer.service.IPayService;
import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.sdk.exceptions.OAuthException;

/**
 * @author caozhiyong
 * @version 2013-4-3
 */
@Component("doExpressCheckoutWidget")
public class DoExpressCheckoutWidget extends AbstractWidget{

	@Resource 
	private IPayService payService ;
	@Resource
	private IOrderInfoService orderInfoService;
	
	@Resource
	private IOrderInfoBackService orderInfoBackService;
	
	@Transactional
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		HttpServletResponse response = ThreadContextHolder.getHttpResponse();
		
		HttpSession session = request.getSession();
		session.setAttribute("url", request.getRequestURI());
		session.setAttribute(
				"relatedUrl",
				"<ul><li><a href='EC/SetExpressCheckout'>SetExpressCheckout</a></li><li>" +
				"<a href='EC/GetExpressCheckout'>GetExpressCheckout</a></li><li>" +
				"<a href='EC/DoExpressCheckout'>DoExpressCheckout</a></li></ul>");
		response.setContentType("text/html");
	    String orderNo=session.getAttribute("orderNo").toString();
		try {
			PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(this
					.getClass().getResourceAsStream("/sdk_config.properties"));
			DoExpressCheckoutPaymentRequestType doCheckoutPaymentRequestType = new DoExpressCheckoutPaymentRequestType();
			DoExpressCheckoutPaymentRequestDetailsType details = new DoExpressCheckoutPaymentRequestDetailsType();
			details.setToken(request.getParameter("token"));
			details.setPayerID(request.getParameter("payerID"));
			details.setPaymentAction(PaymentActionCodeType
					.fromValue("Sale"));
			double itemTotalAmt = 0.00;
			double orderTotalAmt = 0.00;
			
	    
			OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderNo); //根据订单编号获取价格
			
			
			String amt = request.getParameter("amt");
			String quantity = request.getParameter("itemQuantity");
			amt = orderInfo.getOrderMoney();
			quantity = "1";
			itemTotalAmt = Double.parseDouble(amt)
					* Double.parseDouble(quantity);
			orderTotalAmt += itemTotalAmt;
			PaymentDetailsType paymentDetails = new PaymentDetailsType();
			BasicAmountType orderTotal = new BasicAmountType();
			orderTotal.setValue(Double.toString(orderTotalAmt));
			orderTotal.setCurrencyID(CurrencyCodeType.fromValue("USD"));
			paymentDetails.setOrderTotal(orderTotal);

			BasicAmountType itemTotal = new BasicAmountType();
			itemTotal.setValue(Double.toString(itemTotalAmt));

			itemTotal.setCurrencyID(CurrencyCodeType.fromValue("USD"));
			paymentDetails.setItemTotal(itemTotal);

			List<PaymentDetailsItemType> paymentItems = new ArrayList<PaymentDetailsItemType>();
			PaymentDetailsItemType paymentItem = new PaymentDetailsItemType();
			
		
			paymentItem.setName(orderNo);
			paymentItem.setQuantity(Integer.parseInt("1"));
			BasicAmountType amount = new BasicAmountType();
			amount.setValue(orderInfo.getOrderMoney());
			amount.setCurrencyID(CurrencyCodeType.fromValue("USD"));
			paymentItem.setAmount(amount);
			paymentItems.add(paymentItem);
			paymentDetails.setPaymentDetailsItem(paymentItems);
			paymentDetails.setNotifyURL(request.getParameter("notifyURL"));
			
			List<PaymentDetailsType> payDetailType = new ArrayList<PaymentDetailsType>();
			payDetailType.add(paymentDetails);
			details.setPaymentDetails(payDetailType);

			doCheckoutPaymentRequestType
					.setDoExpressCheckoutPaymentRequestDetails(details);
			DoExpressCheckoutPaymentReq doExpressCheckoutPaymentReq = new DoExpressCheckoutPaymentReq();
			doExpressCheckoutPaymentReq
					.setDoExpressCheckoutPaymentRequest(doCheckoutPaymentRequestType);

			DoExpressCheckoutPaymentResponseType doCheckoutPaymentResponseType = service
					.doExpressCheckoutPayment(doExpressCheckoutPaymentReq);
			response.setContentType("text/html");
			String TransactionID="";
			
			if (doCheckoutPaymentResponseType != null) {
				session.setAttribute("lastReq", service.getLastRequest());
				session.setAttribute("lastResp", service.getLastResponse());
				boolean flag=false;
				if (doCheckoutPaymentResponseType.getAck().toString()
						.equalsIgnoreCase("SUCCESS")) {
					flag=true;
					Map<Object, Object> map = new LinkedHashMap<Object, Object>();
					map.put("Ack", doCheckoutPaymentResponseType.getAck());
					Iterator<PaymentInfoType> iterator = doCheckoutPaymentResponseType
							.getDoExpressCheckoutPaymentResponseDetails()
							.getPaymentInfo().iterator();
					int index = 1;
					while (iterator.hasNext()) {
						PaymentInfoType result = (PaymentInfoType) iterator
								.next();
						map.put("Transaction ID" + index,
								result.getTransactionID());
						System.out.println("==============="+result.getTransactionID().toString());
						TransactionID=result.getTransactionID().toString();
						index++;
					}
					map.put("Billing Agreement ID",
							doCheckoutPaymentResponseType
									.getDoExpressCheckoutPaymentResponseDetails()
									.getBillingAgreementID());
					session.setAttribute("map", map);
					this.putData("orderNo",orderNo);
					try {
					    orderInfoService.updateOrderPayState(orderNo); //更新状态
						orderInfoBackService.processState(orderNo); //更新有效期
						this.putData("flag", "success");
					} catch (Exception e) {
						e.printStackTrace();
						insertErrorLog(orderNo,"响应失败：更新支付状态或者有效期失败");
					}  
					
					//response.sendRedirect(ServletActionContext.getServletContext().getContextPath()+"/Response.jsp");
				} else {

//					session.setAttribute("Error",
//							doCheckoutPaymentResponseType.getErrors());
					insertErrorLog(orderNo,"支付出现问题");
					this.putData("flag", "false");
					//response.sendRedirect(ServletActionContext.getServletContext().getContextPath()+"/Error.jsp");
				}
				
				
				PayLog payLog=new PayLog();
			    payLog.setOut_trade_no(orderNo);
			    if (flag) {
			    	payLog.setResult(FrontConstant.PAY_TRUE);
				}else {
					payLog.setResult(FrontConstant.PAY_FALSE);
				}
			    String ip=FrontConstant.getIpAddr(request);
			    String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			    payLog.setIp(ip);
			    payLog.setCreateTime(date);
			    
			    payLog.setTrade_no(TransactionID);
			    payLog.setTrade_status(doCheckoutPaymentResponseType.getAck().toString());
			    
			    try {
					payService.insertPayLog(payLog);
				} catch (Exception e) {
					insertErrorLog(orderNo,"paypal响应失败：插入支付日志错误");
					e.printStackTrace();
				}
				
			}
		} catch (SSLConfigurationException e) {
			insertErrorLog(orderNo,"响应失败：SSLConfigurationException");
			e.printStackTrace();
		} catch (InvalidCredentialException e) {
			insertErrorLog(orderNo,"响应失败：InvalidCredentialException");
			e.printStackTrace();
		} catch (HttpErrorException e) {
			insertErrorLog(orderNo,"响应失败：HttpErrorException");
			e.printStackTrace();
		} catch (InvalidResponseDataException e) {
			insertErrorLog(orderNo,"响应失败：InvalidResponseDataException");
			e.printStackTrace();
		} catch (ClientActionRequiredException e) {
			insertErrorLog(orderNo,"响应失败：ClientActionRequiredException");
			e.printStackTrace();
		} catch (MissingCredentialException e) {
			insertErrorLog(orderNo,"响应失败：MissingCredentialException");
			e.printStackTrace();
		} catch (OAuthException e) {
			insertErrorLog(orderNo,"响应失败：OAuthException");
			e.printStackTrace();
		} catch (IOException e) {
			insertErrorLog(orderNo,"响应失败：IOException");
			e.printStackTrace();
		} catch (InterruptedException e) {
			insertErrorLog(orderNo,"响应失败：InterruptedException");
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			insertErrorLog(orderNo,"响应失败：ParserConfigurationException");
			e.printStackTrace();
		} catch (SAXException e) {
			insertErrorLog(orderNo,"响应失败：SAXException");
			e.printStackTrace();
		}
	}
	
	//记录操作失败日志
   public void insertErrorLog(String orderNo,String remark){
	   PayErrorLog errorLog =new PayErrorLog();
	   String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
	   HttpServletRequest request=ThreadContextHolder.getHttpRequest();
	   String ip=FrontConstant.getIpAddr(request);
	   errorLog.setIp(ip);
	   errorLog.setErrorDate(date);
	   errorLog.setRemark(remark);
	   errorLog.setOrderNo(orderNo);
	   orderInfoBackService.payErrorLog(errorLog);
	}
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

	public IPayService getPayService() {
		return payService;
	}

	public void setPayService(IPayService payService) {
		this.payService = payService;
	}

	public IOrderInfoService getOrderInfoService() {
		return orderInfoService;
	}

	public void setOrderInfoService(IOrderInfoService orderInfoService) {
		this.orderInfoService = orderInfoService;
	}

	
	
}
