package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;

/**
 * 续租卡续租
 * @author chenqichuan
 * @version 2013-10-8
 */
@Component("renewByCardWidget")
public class RenewByCardWidget extends RequestParamWidget{

	@Resource
	private IMyAccountService myAccountService;
	@Resource
	private ICustomerProductService customerProductService;
	
	private List<MyProduct> listProduct;
	public List<MyProduct> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<MyProduct> listProduct) {
		this.listProduct = listProduct;
	}

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);
		Locale locale = null;
		try {
			 
			 listProduct=myAccountService.queryProductByCode(customerInfo.getCode());
			 Language language = (Language) CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
			 
			 if (language != null) {
				locale = new Locale(language.getLanguageCode(), language.getCountryCode());
			 } else {
				locale = CopContext.getDefaultLocal();
			 }
		     
			 
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.putData("listProduct", listProduct);
		this.putData("locale",locale);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}


	
}
