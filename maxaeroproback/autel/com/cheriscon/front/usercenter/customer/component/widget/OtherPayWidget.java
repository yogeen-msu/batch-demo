package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IPaymentCfgService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;

/**
 * 
 * @author caozhiyong
 * @version 2013-4-2
 */
@Component("otherPayWidget")
public class OtherPayWidget extends RequestParamWidget{

	@Resource
	private ILanguageService service;
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		String languageCode=customerInfo.getLanguageCode();//语言code
		Language language=service.getByCode(languageCode);
		this.putData("country",language.getCountryCode());
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

	

	
}
