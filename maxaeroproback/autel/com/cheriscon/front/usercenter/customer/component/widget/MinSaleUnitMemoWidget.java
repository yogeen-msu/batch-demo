package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;

/**
 * 最小销售单位功能描述
 * @author caozhiyong
 * @version 2013-1-12
 */
@Component("minSaleUnitMemoWidget")
public class MinSaleUnitMemoWidget extends AbstractWidget{

	@Resource
	private IMinSaleUnitDetailService memoService;
	@Override
	protected void display(Map<String, String> params) {
		String minSaleUnitCode=params.get("code");//最小销售单位code
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		
		String memo=memoService.findMinSaleUnitMemo(minSaleUnitCode,customerInfo.getLanguageCode());
		
		this.putData("memo",memo);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
