package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import net.sf.json.JSONArray;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.Header;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import paypal.payflow.AuthorizationTransaction;
import paypal.payflow.Context;
import paypal.payflow.Currency;
import paypal.payflow.Invoice;
import paypal.payflow.PayflowConnectionData;
import paypal.payflow.PayflowUtility;
import paypal.payflow.Response;
import paypal.payflow.SDKProperties;
import paypal.payflow.SaleTransaction;
import paypal.payflow.TransactionResponse;
import paypal.payflow.UserInfo;

import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;

import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.sdk.exceptions.OAuthException;

/**
 * @author caozhiyong
 * @version 2013-4-3
 */
@Component("creditPayWidget")
public class CreditPayWidget extends AbstractWidget{

	@Resource
	private IOrderInfoService orderInfoService;
	@Override
	protected void display(Map<String, String> params) {
		
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		String orderNo=request.getParameter("orderNo");
        SDKProperties.setHostAddress("payflowpro.paypal.com");
        SDKProperties.setHostPort(443);
        SDKProperties.setTimeOut(45);

        UserInfo user = new UserInfo("autelusinc", "autelusinc", "PayPal", "0116Autelus*123");
        PayflowConnectionData connection = new PayflowConnectionData();
        Invoice inv = new Invoice();

    	OrderInfo orderInfo=orderInfoService.queryOrderInfoByCode(orderNo); //根据订单编号获取价格
		String money=orderInfo.getOrderMoney();
        Currency amt = new Currency(new Double(money), "USD");
        inv.setAmt(amt);
        inv.setPoNum(orderNo);
        inv.setInvNum(orderNo);
        HttpSession session = request.getSession();
        session.setAttribute("orderNo", orderNo);

      //  AuthorizationTransaction trans = new AuthorizationTransaction(user, connection, inv, null, PayflowUtility.getRequestId());
        SaleTransaction  trans = new SaleTransaction (user, connection, inv, null, PayflowUtility.getRequestId());
        trans.setCreateSecureToken("Y");
        trans.setSecureTokenId(PayflowUtility.getRequestId());

        Response resp = trans.submitTransaction();

        if (resp != null) {
            TransactionResponse trxnResponse = resp.getTransactionResponse();

            if (trxnResponse != null) {
            	session.setAttribute("SECURETOKEN", trxnResponse.getSecureToken());
            	session.setAttribute("SECURETOKENID", trxnResponse.getSecureTokenId());
				this.putData("SECURETOKEN", trxnResponse.getSecureToken());
				this.putData("SECURETOKENID", trxnResponse.getSecureTokenId());
        }
	}
	}
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
