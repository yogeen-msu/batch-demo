package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.order.service.IOrderInfoBackService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.PayErrorLog;
import com.cheriscon.common.model.PayLog;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.cheriscon.front.usercenter.customer.service.IPayService;

/**
 * 续租卡续租
 * @author chenqichuan
 * @version 2013-10-8
 */
@Component("paySuccessWidget")
public class PaySuccessWidget extends RequestParamWidget{

	@Resource
	private IMyAccountService myAccountService;
	@Resource
	private IOrderInfoService orderInfoService;
	
	@Resource
	private IOrderInfoBackService orderInfoBackService;
	@Resource 
	private IPayService payService ;

	@Override
	protected  void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String orderNo=(String)request.getSession().getAttribute("orderNo");
//		String orderNo="IX05750987";
		String SECURETOKEN=(String)request.getSession().getAttribute("SECURETOKEN");
		String SECURETOKENID=(String)request.getSession().getAttribute("SECURETOKENID");
		this.putData("orderNo",orderNo);
		String RESULT = params.get("RESULT");
		String reSECURETOKEN= params.get("SECURETOKEN");
		String reSECURETOKENID=params.get("SECURETOKENID");
		String PPREF=request.getParameter("PPREF")==null?"":request.getParameter("PPREF");
//		String PPREF="testp";
		System.out.println("======paypal====="+request.getParameter("PPREF"));
		try {
			if(RESULT.equals("0") && SECURETOKEN.equals(reSECURETOKEN) && SECURETOKENID.equals(reSECURETOKENID))
			{
			
			   /* OrderInfo order= orderInfoService.queryOrderInfoByCode(orderNo);*/
				OrderInfo order= orderInfoService.queryOrderInfoByCode(orderNo);
//				int f = payService.getPayLogNumByTradeNum(PPREF);
//			    if(payService.getPayLogNumByTradeNum(PPREF)<=1){
//				    
//				    orderInfoService.updateOrderPayState(orderNo); //更新状态
//					orderInfoBackService.processState(orderNo); //更新有效期
//			    }
//	   		       根据oder支付状态来防止重复提交
				if(FrontConstant.ORDER_PAY_STATE_YES!=order.getOrderPayState().intValue()){
					orderInfoService.updateOrderPayState(orderNo); //更新状态
					orderInfoBackService.processState(orderNo); //更新有效期
				}
				this.putData("result","Y");
			}else{
				this.putData("result","N");
			}
		} catch (Exception e) {
			e.printStackTrace();
			insertErrorLog(orderNo,"信用卡成功返回-响应失败：修改订单状态失败");
			this.putData("result","N");
		}
//			记录日志
	    try {
	    	PayLog payLog=new PayLog();
		    payLog.setOut_trade_no(orderNo);
		    
		    payLog.setResult(FrontConstant.PAY_TRUE);
			
		    String ip=FrontConstant.getIpAddr(request);
		    String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		    payLog.setIp(ip);
		    payLog.setCreateTime(date);
		    
		    payLog.setTrade_no(PPREF);
		    payLog.setTrade_status("信用卡支付成功");
			payService.insertPayLog(payLog);
		} catch (Exception e) {
			insertErrorLog(orderNo,"信用卡成功返回-响应失败：插入支付日志错误");
			e.printStackTrace();
		}
		
	}
	//记录操作失败日志
	   public void insertErrorLog(String orderNo,String remark){
		   PayErrorLog errorLog =new PayErrorLog();
		   String date = DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
		   HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		   String ip=FrontConstant.getIpAddr(request);
		   errorLog.setIp(ip);
		   errorLog.setErrorDate(date);
		   errorLog.setRemark(remark);
		   errorLog.setOrderNo(orderNo);
		   orderInfoBackService.payErrorLog(errorLog);
		}
	
	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}


	
}
