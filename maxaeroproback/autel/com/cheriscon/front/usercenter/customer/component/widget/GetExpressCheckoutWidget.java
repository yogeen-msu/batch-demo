package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;

import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.sdk.exceptions.OAuthException;

/**
 * @author caozhiyong
 * @version 2013-4-3
 */
@Component("getExpressCheckoutWidget")
public class GetExpressCheckoutWidget extends AbstractWidget{

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		HttpServletResponse response = ThreadContextHolder.getHttpResponse();
		
		HttpSession session = request.getSession();
		session.setAttribute("url", request.getRequestURI());
		session.setAttribute(
				"relatedUrl",
				"<ul><li><a href='EC/SetExpressCheckout'>SetExpressCheckout</a></li><li>" +
				"<a href='EC/GetExpressCheckout'>GetExpressCheckout</a></li><li>" +
				"<a href='EC/DoExpressCheckout'>DoExpressCheckout</a></li></ul>");
		response.setContentType("text/html");
		
		try {
			PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(this
					.getClass().getResourceAsStream("/sdk_config.properties"));
			
			GetExpressCheckoutDetailsReq req = new GetExpressCheckoutDetailsReq();
			GetExpressCheckoutDetailsRequestType reqType = new GetExpressCheckoutDetailsRequestType(
					request.getParameter("token"));
			req.setGetExpressCheckoutDetailsRequest(reqType);
			GetExpressCheckoutDetailsResponseType resp = service
					.getExpressCheckoutDetails(req);
			if (resp != null) {
				session.setAttribute("lastReq", service.getLastRequest());
				session.setAttribute("lastResp", service.getLastResponse());
				if (resp.getAck().toString().equalsIgnoreCase("SUCCESS")) {
					Map<Object, Object> map = new LinkedHashMap<Object, Object>();
					map.put("Ack", resp.getAck());
					map.put("Token", resp
							.getGetExpressCheckoutDetailsResponseDetails()
							.getToken());
					map.put("Payer ID", resp
							.getGetExpressCheckoutDetailsResponseDetails()
							.getPayerInfo().getPayerID());
					session.setAttribute("map", map);
					
					StringBuffer sbHtml = new StringBuffer();
					 
					Iterator iterator = map.entrySet().iterator();
					while (iterator.hasNext()) {
						Map.Entry mapEntry = (Map.Entry) iterator.next();
						sbHtml.append("<tr><td>");
						sbHtml.append(mapEntry.getKey());
						
						sbHtml.append("</td><td>:</td><td><div id=");
						sbHtml.append("\"");
						sbHtml.append(mapEntry.getKey());
						sbHtml.append("\">");
						sbHtml.append(mapEntry.getValue());
						sbHtml.append("</div></td></tr>");
					}
				
					this.putData("sbHtml",sbHtml);
					this.putData("lastReq",service.getLastRequest());
					this.putData("lastResp",service.getLastResponse());
					this.putData("relatedUrl",session.getAttribute("relatedUrl"));
					this.putData("token",resp
							.getGetExpressCheckoutDetailsResponseDetails()
							.getToken());
					this.putData("PayerID",resp
							.getGetExpressCheckoutDetailsResponseDetails()
							.getPayerInfo().getPayerID());
					
				} else {

					session.setAttribute("Error", resp.getErrors());
					response.sendRedirect(ServletActionContext.getServletContext().getContextPath()+"/Error.jsp");
				}
			}
		} catch (SSLConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidCredentialException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HttpErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidResponseDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientActionRequiredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MissingCredentialException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
