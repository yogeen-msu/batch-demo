package com.cheriscon.front.usercenter.customer.component.widget;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.vo.CustomerProductVo;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductToolsService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ProductTypeTools;
import com.cheriscon.common.utils.JsonHelper;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsMinUnitVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;
import com.google.gson.reflect.TypeToken;

/**
 * 工具下载控制器实现类
 * @author yangpinggui
 * @version 创建时间：2013-1-5
 */
@Component("productToolsWidget")
public class ProductToolsWidget extends RequestParamWidget
{
	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource 
	private IShoppingCartService service;
	
	@Resource
	private IMinSaleUnitDetailService minSaleUnitDetailService;
	
	@Resource
	private IProductToolsService productToolsService;
	
	@Resource
	private ICustomerProductService customerProductService;
	
	@Resource
	private ILanguageService languageService;
	
	@Override
	protected void display(Map<String, String> params) 
	{
		HttpServletResponse response = ThreadContextHolder.getHttpResponse();
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		try
		{
			String operationType = params.get("operationType");
			
			if(operationType == null)
			{
				return;
			}

			if(Integer.parseInt(operationType)  == 1)//查看工具下载信息列表
			{
				queryProductToolList(request);
			}
			else if(Integer.parseInt(operationType)  == 2)//工具下载操作
			{
				downToolFile(request,response);
			}
			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
       Object object=(Object)SessionUtil.getLoginUserInfo(request);
		
		if (object!=null && object.getClass().equals(CustomerInfo.class)) {
			CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
			if (customerInfo!=null) {
				String usercode=customerInfo.getCode();//用户code
				String languageCode=customerInfo.getLanguageCode();//用户首选语言code
		
		List<ShopingCartsProListVO> proListVOs=service.queryShoppingCartByCustomerCode(usercode,languageCode);
		int items=0;
		for (int i = 0; i < proListVOs.size(); i++) {
			for (int j = 0; j < proListVOs.get(i).getMinUnitVOs().size(); j++) {
				
				ShopingCartsMinUnitVO minUnitVO=proListVOs.get(i).getMinUnitVOs().get(j);
				
				MarketPromotionDiscountInfoVO info;
				if (minUnitVO.getIsSoft() == 1) {
					// 查询最小销售单位是否可参与活动
					info = minSaleUnitDetailService.queryDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
									minUnitVO.getType(),
									minUnitVO.getValidDate());
				} else {
					// 查询销售配置是否可参与活动
					info = minSaleUnitDetailService.querySaleCfgDiscount(customerInfo,minUnitVO.getMinSaleUnitCode(),minUnitVO.getAreaCfgCode(),
									minUnitVO.getType(),
									minUnitVO.getValidDate()); 
				}
				String discountPrice = minUnitVO.getCostPrice();
				minUnitVO.setCostPrice(Float.valueOf(discountPrice).toString());
				if (info != null) {
					// 折后价
					discountPrice = String.valueOf((Float.parseFloat(minUnitVO.getCostPrice())*1000 * Float.parseFloat(String.valueOf(info.getDiscount()))) / 10/1000);
				}
				minUnitVO.setDiscountPrice(Float.valueOf(discountPrice).toString());
				if(minUnitVO.getValidDate() != null){
					Date date=null;
					if(minUnitVO.getValidDate().indexOf("/")>-1){
					 date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy/MM/dd");
					}
					else{
					 date = DateUtil.toDate(minUnitVO.getValidDate().trim(), "yyyy-MM-dd");	
					}
					Date currentDate = DateUtil.toDate(DateUtil.toString(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
					//当前时间大于有效期时间(已经过期)
					if(currentDate.after(date)){
						minUnitVO.setIsValid(1);
					}
				}
				
				items++;
				proListVOs.get(i).getMinUnitVOs().set(j, minUnitVO);
			}
		}
			
		this.putData("shoppingCart",proListVOs);
		this.putData("items",items);
			}
		}
	}
	
	/**
	 * 工具下载列表信息处理
	 */
	protected void queryProductToolList(HttpServletRequest request)
	{
		
		try 
		{
			
			String languageCode = ((CustomerInfo) SessionUtil.getLoginUserInfo(request)).getLanguageCode();
			String languageName = languageService.getByCode(languageCode).getName();
			
			List<ProductTypeTools> list = productToolsService.queryProductToolList(languageCode);
			List<ProductTypeTools> productTypeToolsList = new ArrayList<ProductTypeTools>();
			
			ProductTypeTools productTypeTools = null;
			String toolName = "";
			String productTypeName = "";
			
			 Object object=(Object)SessionUtil.getLoginUserInfo(request);
			 
			 String filePath=FrontConstant.getProperValue("download.url.other") ;
			if(object.getClass().equals(CustomerInfo.class)){
				String code=((CustomerInfo)object).getCode();
				String areaCode=FrontConstant.getProperValue("saleconfig.area.china");
				int num=customerProductService.queryProductArea(areaCode,code);
				if(num>0){
				
				filePath=FrontConstant.getProperValue("download.url.china");
				
				}
			}
			
			
			
			
			if(list.size() > 0)
			{
				for(int i = 0 ; i < list.size() ; i++)
				{
				
					Map<String,String> nameMap = JsonHelper.fromJson(list.get(i).getToolName(),
							new TypeToken<Map<String,String>>(){}.getType());
					
					for (String lanCode : nameMap.keySet())
					{
						if(!languageCode.equals(lanCode))
						{
							continue;
						}
						
						if(!toolName.equals(nameMap.get(lanCode)))
						{
							productTypeTools = new ProductTypeTools();
							productTypeTools.setToolName(nameMap.get(lanCode));
							productTypeName = list.get(i).getProductTypeName();
							productTypeTools.setProductTypeName(productTypeName);
							productTypeTools.setLastVersion(list.get(i).getLastVersion());
							Date date=DateUtil.toDate(list.get(i).getReleaseDate(), "yyyy-MM-dd");
							
							
							productTypeTools.setReleaseDate(DateUtil.toString(date, "MM/dd/yyyy"));
							
							String testPath=list.get(i).getDownloadPath();
							if(testPath!=null){
								testPath=testPath.replace("\\", "/");
							}
							
							productTypeTools.setDownloadPath(filePath+testPath);
							productTypeTools.setLanguageTypeName(languageName);
							productTypeToolsList.add(productTypeTools);
						}
						else
						{
							productTypeName = productTypeName + "、" + list.get(i).getProductTypeName();
							productTypeTools.setProductTypeName(productTypeName);
						}
						
						toolName = nameMap.get(lanCode);
					}
				}
			}
			
			this.putData("productTypeToolsList", productTypeToolsList);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 工具下载操作
	 * @param request
	 * @param response
	 */
	protected void downToolFile(HttpServletRequest request , HttpServletResponse response)
	{
		String path= request.getParameter("downToolPath");
		if(path.indexOf("fs:")!=-1){
			path=path.replace("fs:", "");
		}
		
		String filePath = CopSetting.IMG_SERVER_PATH + path;
		
		//客诉查看客诉附件返回时调用
		this.putData("code", request.getParameter("code"));
		this.putData("userType", request.getParameter("userType"));
		this.putData("type", request.getParameter("type"));
		
		// path是指欲下载的文件的路径。
		File file = new File(filePath);
		
		if(!file.exists())
		{
			this.putData("result", "1");
			return;
		}
		
		// 取得文件名。
		String filename = file.getName();

		
		try 
		{
			// 以流的形式下载文件。
			InputStream fis = new BufferedInputStream(new FileInputStream(filePath));
			byte[] buffer;
			buffer = new byte[fis.available()];
			fis.read(buffer);
			fis.close();
			
			// 清空response
			response.reset();
			
			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes()));
			response.addHeader("Content-Length", "" + file.length());
			OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
			response.setContentType("application/octet-stream");
			toClient.write(buffer);
			toClient.flush();
			this.putData("result", "2");
			
		} 
		catch (FileNotFoundException e) 
		{
			this.putData("result", "3");
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			this.putData("result", "3");
			e.printStackTrace();
		}
	}
	
	
	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

}
