package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.market.service.IProductForSealerService;
import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.backstage.product.service.IProductTypeService;
import com.cheriscon.backstage.product.vo.ProductTypeSerialPicVo;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.ProductType;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * 产品型号
 * 
 * @author caozhiyong
 * @version 2013-1-8
 */
@Component("regProductType")
public class ProductTypeWidget extends AbstractWidget {

	@Resource
	private IProductTypeService productTypeService;

	//private List<ProductType> productTypes;
	private List<ProductForSealer> productTypes;
	
	@Resource
	private IProductForSealerService productForSealerService;

	@Override
	protected void display(Map<String, String> params) {
		try {
			// 查询所有产品型号
			productTypes = productForSealerService.getProductForSealer908();
		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		
		String languageCode=customerInfo.getLanguageCode();//语言code
		List<ProductForSealer> product=new ArrayList<ProductForSealer>();
		for (int i = 0; i < productTypes.size(); i++) {
			ProductForSealer type=productTypes.get(i);
			String proSerialPicPath=type.getSerialPath();
			if (proSerialPicPath != null) {
				if (!proSerialPicPath.equals("")) {
					JSONArray jsonArray = JSONArray
							.fromObject(proSerialPicPath);
					JSONObject jsonObject;
					ProductTypeSerialPicVo pojoValue;

					for (int j = 0; j < jsonArray.size(); j++) {
						jsonObject = jsonArray.getJSONObject(j);
						pojoValue = (ProductTypeSerialPicVo) JSONObject.toBean(
								jsonObject, ProductTypeSerialPicVo.class);
						if (pojoValue.getLanguageCode().equals(languageCode)) {
							proSerialPicPath = pojoValue.getPicPath();
							type.setSerialPath(proSerialPicPath);
							break;
						}
					}
				}

			}else{
				proSerialPicPath = "";
				type.setSerialPath(proSerialPicPath);
			}
			
		    product.add(type);
		}
		this.putData("productTypes", productTypes);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

}
