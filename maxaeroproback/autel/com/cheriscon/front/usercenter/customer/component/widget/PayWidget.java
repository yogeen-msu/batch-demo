package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.backstage.system.component.plugin.rate.RatePlugin;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IPaymentCfgService;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.PaymentCfg;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * 支付
 * @author caozhiyong
 * @version 2013-1-17
 */
@Component("payWidget")
public class PayWidget extends RequestParamWidget{
	@Resource
	private ILanguageService service;
	
	@Resource
	private IPaymentCfgService cfgService;
	@Resource
	private IOrderInfoService orderInfoService;
	
	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		
		String languageCode=customerInfo.getLanguageCode();//语言code
		
		Language language=service.getByCode(languageCode);
		
		String orderInfoCode=params.get("orderInfoCode");//订单编号
		
		String money=orderInfoService.queryOrderInfoByCode(orderInfoCode).getOrderMoney();
		Integer orderPayState=orderInfoService.queryOrderInfoByCode(orderInfoCode).getOrderPayState();
		
		
		/**
		 *从数据库读取金额汇率
		 */
		PaymentCfg paymentCfg=cfgService.getByPluginId(new RatePlugin().getId());
		String biref=paymentCfg.getConfig();
	
		Gson gson = new GsonBuilder().create();
		Map<String, String> map =gson.fromJson(biref, new TypeToken<Map<String, String>>() {}.getType());
		
		this.putData("orderPayState",orderPayState);
		this.putData("orderInfoCode",orderInfoCode);
		this.putData("money",Float.valueOf(money).toString());
		this.putData("country",language.getCountryCode());
		this.putData("biref",map.get("rate"));
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub
		
	}

	public ILanguageService getService() {
		return service;
	}

	public void setService(ILanguageService service) {
		this.service = service;
	}
	

	
}
