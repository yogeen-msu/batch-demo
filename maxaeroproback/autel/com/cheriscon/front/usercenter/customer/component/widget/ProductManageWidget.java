package com.cheriscon.front.usercenter.customer.component.widget;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.component.widget.RequestParamWidget;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.pager.StaticPagerHtmlBuilder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;

/**
 * 个人客户会员-我的产品
 * @date:2013-1-5
 * @author caozhiyong 
 */
@Component("productManageWidget")
public class ProductManageWidget extends RequestParamWidget {

	public boolean cacheAble()
	{
		return false;
	}
	
	@Resource
	private IMyAccountService myAccountService;

	private List<String> proSerialNos;
	private List<MyProduct> myProducts;
	private List<MyProduct> listProduct;
	public List<MyProduct> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<MyProduct> listProduct) {
		this.listProduct = listProduct;
	}

	@Override
	protected void display(Map<String, String> params) {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息		
		try {
			 listProduct=myAccountService.queryProductByCode(customerInfo.getCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.putData("listProduct", listProduct);
		this.putData("autelId",customerInfo.getAutelId());
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

	public List<String> getProSerialNos() {
		return proSerialNos;
	}

	public void setProSerialNos(List<String> proSerialNos) {
		this.proSerialNos = proSerialNos;
	}

	public List<MyProduct> getMyProducts() {
		return myProducts;
	}

	public void setMyProducts(List<MyProduct> myProducts) {
		this.myProducts = myProducts;
	}

}
