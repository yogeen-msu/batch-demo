package com.cheriscon.front.usercenter.customer.service;

import java.util.List;

import javax.swing.text.StyledEditorKit.BoldAction;

import com.cheriscon.common.model.ShoppingCart;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;
import com.cheriscon.front.usercenter.customer.vo.ShoppingCartVO;
import com.sun.java_cup.internal.runtime.virtual_parse_stack;
import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * 购物车业务逻辑实现接口
 * 
 * @author caozhiyong
 * @version 2013-1-15
 */
public interface IShoppingCartService {

	/**
	 * 将续租产品加入购物车
	 * @param usercode
	 * @param proName
	 * @param proSerial
	 * @param proCode
	 * @param minSaleUnitCode
	 * @param areaCfgCode
	 * @param type
	 * @param year
	 */
	public String addShoppingCartIsRent(String usercode, String proName,
			String proSerial, String proCode, String minSaleUnitCode,
			String areaCfgCode, int type, int year,String picPath,int isSoft,String cfgPrice,String validData);
	
	/**
	 * 根据用户code查询购物车数据
	 * @param customerCode
	 * @return
	 */
	public List<ShopingCartsProListVO> queryShoppingCartByCustomerCode(String customerCode,String languageCode);
	public List<ShoppingCartVO> queryShoppingCartVoByCustomerCode(String customerCode,String languageCode);
	
	/**
	 * 软件批量加入购物车
	 * @param infoVos
	 * @return
	 */
	public boolean softAddShoppingCarts(List<ShoppingCart> infoVos) throws Exception;
	
	/**
	 * 购物车软件数量
	 * @param customerCode
	 * @return
	 */
	public int queryShoppingCartsSoftNumber(String customerCode);
	
	/**
	 * 购物车信息
	 * @param customerCode
	 * @return
	 */
	public List<ShoppingCart> querShoppingCarts(String customerCode);
	
	/**
	 * 更改购物车续租年限
	 * @param userCode
	 * @param serialNo
	 * @param minSaleUnitCode
	 * @param year
	 * @return
	 */
	public void updateYear(String userCode,String serialNo,String minSaleUnitCode,String year) throws Exception;
}
