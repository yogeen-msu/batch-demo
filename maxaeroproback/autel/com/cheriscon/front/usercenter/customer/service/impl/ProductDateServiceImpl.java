package com.cheriscon.front.usercenter.customer.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.usercenter.customer.service.IProductDateService;

@Service
public class ProductDateServiceImpl extends BaseSupport<ProductAssociatesDate> implements IProductDateService {

	@Override
	public ProductAssociatesDate getProductDateInfo(String proCode) {
		String sql="select * from dt_ProductAssociatesDate where proCode='"+proCode+"'";
		return (ProductAssociatesDate) this.daoSupport.queryForObject(sql, ProductAssociatesDate.class);
	}

	public  boolean updateProductDate(String proDate,String proCode) {
		String sql="update dt_ProductAssociatesDate set associatesDate='"+proDate+"'  where proCode='"+proCode+"'";
		this.daoSupport.execute(sql, null);
		return true;
	}
	
	public boolean insertProductDate(ProductAssociatesDate proDate){
		this.daoSupport.insert("dt_ProductAssociatesDate", proDate);
		return true;
	}
	
}
