package com.cheriscon.front.usercenter.customer.service;

import com.cheriscon.common.model.PayPalToken;

public interface IPayPalTokenService {
	
	public boolean savePayPalToken(PayPalToken payPalToken);
	public PayPalToken getPayPalToken(String secureTokenId);

}
