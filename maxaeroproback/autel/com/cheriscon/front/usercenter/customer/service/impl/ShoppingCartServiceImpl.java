package com.cheriscon.front.usercenter.customer.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.omg.CORBA.OBJ_ADAPTER;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.ShoppingCart;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.mapper.ShoppingCartSaleCfgVOMapper;
import com.cheriscon.front.usercenter.customer.model.mapper.ShoppingCartVOMapper;
import com.cheriscon.front.usercenter.customer.service.IShoppingCartService;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsMinUnitVO;
import com.cheriscon.front.usercenter.customer.vo.ShopingCartsProListVO;
import com.cheriscon.front.usercenter.customer.vo.ShoppingCartVO;

import freemarker.cache.StrongCacheStorage;

/**
 * 购物车业务逻辑实现类
 * @author caozhiyong
 * @version 2013-1-15
 */
@SuppressWarnings("rawtypes")
@Service
public class ShoppingCartServiceImpl extends BaseSupport implements IShoppingCartService {

	@Override
	public String addShoppingCartIsRent(String usercode, String proName,
			String proSerial, String proCode, String minSaleUnitCode,
			String areaCfgCode, int type, int year,String picPath,int isSoft,String cfgPrice,String validData) {
		
		ShoppingCart shoppingCart=new ShoppingCart();
		shoppingCart.setAreaCfgCode(areaCfgCode);
		shoppingCart.setCustomerCode(usercode);
		shoppingCart.setMinSaleUnitCode(minSaleUnitCode);
		shoppingCart.setProCode(proCode);
		shoppingCart.setProName(proName);
		shoppingCart.setProSerial(proSerial);
		shoppingCart.setType(type);
		shoppingCart.setYear(year);
		shoppingCart.setPicPath(picPath);
		shoppingCart.setIsSoft(isSoft);
		shoppingCart.setCfgPrice(cfgPrice);
		shoppingCart.setValidData(validData);
		
		StringBuffer sql=new StringBuffer();
		sql.append("select * from DT_ShoppingCart " +
				"where customerCode='"+usercode+"'"+ 
				" and minSaleUnitCode='"+minSaleUnitCode+"'"+
				" and proCode='"+proCode+"'"+
				" and proSerial='"+proSerial+"'"+
				" and areaCfgCode=");
		sql.append("'");
		sql.append(areaCfgCode);
		sql.append("'");
		ShoppingCart data=(ShoppingCart)this.daoSupport.queryForObject(sql.toString(), ShoppingCart.class, new Object[]{});
		//判断该数据是否已存在，如果已存在则更新
		if(data!=null){
			return data.getProSerial()+data.getMinSaleUnitCode();
		}else {
			this.daoSupport.insert(DBUtils.getTableName(ShoppingCart.class), shoppingCart);
			return "true";
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ShopingCartsProListVO> queryShoppingCartByCustomerCode(String customerCode,String languageCode) {
		
		StringBuffer sqlBuffer=new StringBuffer();
		sqlBuffer.append("select s.*,m.name,p.price as costPrice,p.price as discountPrice " +
				",s.picPath as proPicPath from DT_ShoppingCart s left join DT_MinSaleUnitMemo m on " +
				"s.minSaleUnitCode=m.minSaleUnitCode left join DT_MinSaleUnitPrice p " +
				"on s.minSaleUnitCode=p.minSaleUnitCode and s.areaCfgCode=p.areaCfgCode " +
				" left join DT_ProductInfo pdi on s.proCode=pdi.code left join DT_ProductType pt on pdi.proTypeCode=pt.code where s.isSoft='1' and  s.customerCode=");
		sqlBuffer.append("'");
		sqlBuffer.append(customerCode);
		sqlBuffer.append("'");
		sqlBuffer.append("and m.languageCode=");
		sqlBuffer.append("'");
		sqlBuffer.append(languageCode);
		sqlBuffer.append("'");
		
		
		StringBuffer saleCfgsql=new StringBuffer();
		saleCfgsql.append("select s.*,sc.name ,s.picPath as proPicPath from dt_shoppingcart s left join DT_SaleConfig sc on s.minSaleUnitCode=sc.code " +
				"left join DT_ProductInfo pdi on s.proCode=pdi.code left join DT_ProductType pt on pdi.proTypeCode=pt.code  where s.customerCode=");
		saleCfgsql.append("'");
		saleCfgsql.append(customerCode);
		saleCfgsql.append("'");
		saleCfgsql.append(" and s.isSoft='0'");
		
		
		List<ShoppingCartVO> shoList=(List<ShoppingCartVO>)this.daoSupport.queryForList(sqlBuffer.toString(), new ShoppingCartVOMapper(), new Object[]{});
		List<ShoppingCartVO> cfgShoList=(List<ShoppingCartVO>)this.daoSupport.queryForList(saleCfgsql.toString(), new ShoppingCartSaleCfgVOMapper(), new Object[]{});
		
		
		List<ShoppingCartVO> list=new ArrayList<ShoppingCartVO>();
		for (int i = 0; i < shoList.size(); i++) {
			ShoppingCartVO vo=shoList.get(i);
			if (vo.getType()==FrontConstant.USER_IS_SOFT_RENT) {
				
				StringBuffer sql=new StringBuffer();
				sql.append("select s.* from dt_productsoftwarevalidstatus s LEFT join dt_customerproinfo cpi on cpi.proCode=s.proCode " +
						"left join DT_ProductInfo p on p.code=cpi.proCode where cpi.customerCode=");
				sql.append("'");
				sql.append(vo.getCustomerCode());
				sql.append("'");
				sql.append(" and s.minSaleUnitCode=");
				sql.append("'");
				sql.append(vo.getMinSaleUnitCode());
				sql.append("'");
				sql.append(" and p.serialNo=");
				sql.append("'");
				sql.append(vo.getProSerial());
				sql.append("'");
				
				ProductSoftwareValidStatus status=(ProductSoftwareValidStatus) this.daoSupport.queryForObject(sql.toString(), ProductSoftwareValidStatus.class, new Object[]{});
				if (status!=null) {
					vo.setValidDate(status.getValidDate());
				}
				
			}
			list.add(vo);
		}
		list.addAll(cfgShoList);
		
		return toCartsProListVOs(list);
	}
	public List<ShoppingCartVO> queryShoppingCartVoByCustomerCode(String customerCode,String languageCode){
		StringBuffer sqlBuffer=new StringBuffer();
		sqlBuffer.append("select s.*,m.name,p.price as costPrice,p.price as discountPrice " +
				",s.picPath as proPicPath from DT_ShoppingCart s left join DT_MinSaleUnitMemo m on " +
				"s.minSaleUnitCode=m.minSaleUnitCode left join DT_MinSaleUnitPrice p " +
				"on s.minSaleUnitCode=p.minSaleUnitCode and s.areaCfgCode=p.areaCfgCode " +
				" left join DT_ProductInfo pdi on s.proCode=pdi.code left join DT_ProductType pt on pdi.proTypeCode=pt.code where s.isSoft='1' and  s.customerCode=");
		sqlBuffer.append("'");
		sqlBuffer.append(customerCode);
		sqlBuffer.append("'");
		sqlBuffer.append("and m.languageCode=");
		sqlBuffer.append("'");
		sqlBuffer.append(languageCode);
		sqlBuffer.append("'");
		
		
		StringBuffer saleCfgsql=new StringBuffer();
		saleCfgsql.append("select s.*,sc.name ,s.picPath as proPicPath from dt_shoppingcart s left join DT_SaleConfig sc on s.minSaleUnitCode=sc.code " +
				"left join DT_ProductInfo pdi on s.proCode=pdi.code left join DT_ProductType pt on pdi.proTypeCode=pt.code  where s.customerCode=");
		saleCfgsql.append("'");
		saleCfgsql.append(customerCode);
		saleCfgsql.append("'");
		saleCfgsql.append(" and s.isSoft='0'");
		
		
		List<ShoppingCartVO> shoList=(List<ShoppingCartVO>)this.daoSupport.queryForList(sqlBuffer.toString(), new ShoppingCartVOMapper(), new Object[]{});
		List<ShoppingCartVO> cfgShoList=(List<ShoppingCartVO>)this.daoSupport.queryForList(saleCfgsql.toString(), new ShoppingCartSaleCfgVOMapper(), new Object[]{});
		
		
		List<ShoppingCartVO> list=new ArrayList<ShoppingCartVO>();
		for (int i = 0; i < shoList.size(); i++) {
			ShoppingCartVO vo=shoList.get(i);
			if (vo.getType()==FrontConstant.USER_IS_SOFT_RENT) {
				
				StringBuffer sql=new StringBuffer();
				sql.append("select s.* from dt_productsoftwarevalidstatus s LEFT join dt_customerproinfo cpi on cpi.proCode=s.proCode " +
						"left join DT_ProductInfo p on p.code=cpi.proCode where cpi.customerCode=");
				sql.append("'");
				sql.append(vo.getCustomerCode());
				sql.append("'");
				sql.append(" and s.minSaleUnitCode=");
				sql.append("'");
				sql.append(vo.getMinSaleUnitCode());
				sql.append("'");
				sql.append(" and p.serialNo=");
				sql.append("'");
				sql.append(vo.getProSerial());
				sql.append("'");
				
				ProductSoftwareValidStatus status=(ProductSoftwareValidStatus) this.daoSupport.queryForObject(sql.toString(), ProductSoftwareValidStatus.class, new Object[]{});
				if (status!=null) {
					vo.setValidDate(status.getValidDate());
				}
				
			}
			list.add(vo);
		}
		list.addAll(cfgShoList);
		return list;
	}
	/**
	 * 将查询出来的数据构造成购物车数据
	 * @param shoList
	 * @param 
	 * @return
	 */
	private List<ShopingCartsProListVO> toCartsProListVOs(List<ShoppingCartVO> shoList){
		List<ShopingCartsProListVO> list=new ArrayList<ShopingCartsProListVO>();
		a:
		for (int i = 0; i < shoList.size(); i++) {
			ShoppingCartVO vo=shoList.get(i);
			
			for (int j = 0; j < list.size(); j++) {
				ShopingCartsProListVO vListVO=list.get(j);
				if(vo.getProSerial().equals(vListVO.getProSerial())){
					continue a; 
				}
				
			}
			ShopingCartsProListVO proListVO=new ShopingCartsProListVO();
			proListVO.setId(vo.getId());
			proListVO.setProCode(vo.getProCode());
			proListVO.setProName(vo.getProName());
			proListVO.setProSerial(vo.getProSerial());
			proListVO.setPicPath(vo.getPicPath());
			for (int k = 0; k < shoList.size(); k++) {
				ShoppingCartVO vo2=shoList.get(k);
				
				ShopingCartsMinUnitVO minUnitVO=new ShopingCartsMinUnitVO();
				minUnitVO.setAreaCfgCode(vo2.getAreaCfgCode());
				minUnitVO.setCostPrice(vo2.getCostPrice());
				minUnitVO.setCustomerCode(vo2.getCustomerCode());
				minUnitVO.setDiscountPrice(vo2.getDiscountPrice());
				minUnitVO.setMinSaleUnitCode(vo2.getMinSaleUnitCode());
				minUnitVO.setName(vo2.getName());
				minUnitVO.setSparePrice(vo2.getSparePrice());
				minUnitVO.setType(vo2.getType());
				minUnitVO.setYear(vo2.getYear());
				minUnitVO.setIsSoft(vo2.getIsSoft());
				minUnitVO.setValidDate(vo2.getValidDate());
				if(vo.getProSerial().equals(vo2.getProSerial())){
					proListVO.getMinUnitVOs().add(minUnitVO);
					
				}   
			}
			list.add(proListVO);
		}
		return list;
	}
	
	@Transactional
	@Override
	public boolean softAddShoppingCarts(List<ShoppingCart> infoVos) {
		for (int i = 0; i < infoVos.size(); i++) {
			ShoppingCart shoppingCart=infoVos.get(i);
			StringBuffer sql=new StringBuffer();
			sql.append("select * from DT_ShoppingCart " +
					"where customerCode='"+shoppingCart.getCustomerCode()+"'"+ 
					" and minSaleUnitCode='"+shoppingCart.getMinSaleUnitCode()+"'"+
					" and proCode='"+shoppingCart.getProCode()+"'"+
					" and proSerial='"+shoppingCart.getProSerial()+"'"+
					" and areaCfgCode=");
			sql.append("'");
			sql.append(shoppingCart.getAreaCfgCode());
			sql.append("'");
			ShoppingCart data=(ShoppingCart)this.daoSupport.queryForObject(sql.toString(), ShoppingCart.class, new Object[]{});
			//判断该数据是否已存在，如果已存在则更新
			if(data!=null){
				//更新购物车数据
				StringBuffer sqlUpdate=new StringBuffer();
				sqlUpdate.append("UPDATE DT_ShoppingCart SET ");
				sqlUpdate.append("proSerial=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getProSerial());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("proName=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getProName());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("customerCode=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getCustomerCode());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("proCode=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getProCode());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("year=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getYear());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("minSaleUnitCode=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getMinSaleUnitCode());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("type=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getType());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("picPath=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getPicPath());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("isSoft=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getIsSoft());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("cfgPrice=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getCfgPrice());
				sqlUpdate.append("'");
				sqlUpdate.append(",");
				sqlUpdate.append("validData=");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getValidData());
				sqlUpdate.append("'");
				
				sqlUpdate.append(" where customerCode='"+shoppingCart.getCustomerCode()+"'"+ 
					" and minSaleUnitCode='"+shoppingCart.getMinSaleUnitCode()+"'"+
					" and proCode='"+shoppingCart.getProCode()+"'"+
					" and proSerial='"+shoppingCart.getProSerial()+"'"+
					" and areaCfgCode= ");
				sqlUpdate.append("'");
				sqlUpdate.append(shoppingCart.getAreaCfgCode());
				sqlUpdate.append("'");
				this.daoSupport.execute(sqlUpdate.toString(), new Object[]{});			
			}else {
				this.daoSupport.insert(DBUtils.getTableName(ShoppingCart.class), shoppingCart);
			}
		}
		return true;
	}

	@Override
	public int queryShoppingCartsSoftNumber(String customerCode) {
		String sql=" select COUNT(*) from DT_ShoppingCart where customerCode=?";
		return this.daoSupport.queryForInt(sql, customerCode);
	}

	@Override
	public void updateYear(String userCode, String serialNo,
			String minSaleUnitCode, String year) {
		StringBuffer sql=new StringBuffer();
		sql.append("update DT_ShoppingCart set year=");
		sql.append("'");sql.append(year);sql.append("'");
		sql.append(" where customerCode=");
		sql.append("'");sql.append(userCode);sql.append("'");
		sql.append(" and proSerial=");
		sql.append("'");sql.append(serialNo);sql.append("'");
		sql.append(" and minSaleUnitCode=");
		sql.append("'");sql.append(minSaleUnitCode);sql.append("'");
		this.daoSupport.execute(sql.toString(), new Object[]{});
		return;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ShoppingCart> querShoppingCarts(String customerCode) {
		String sql="select * from dt_shoppingcart where customerCode=?";
		return this.daoSupport.queryForList(sql, ShoppingCart.class, customerCode);
	}

}
