package com.cheriscon.front.usercenter.customer.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.LanguagePack;
import com.cheriscon.common.model.MarkerPromotionBaseInfo;
import com.cheriscon.common.model.MarkerPromotionDiscountRule;
import com.cheriscon.common.model.MarkerPromotionRule;
import com.cheriscon.common.model.MinSaleUnitMemo;
import com.cheriscon.common.model.MinSaleUnitSoftwareDetail;
import com.cheriscon.common.model.SoftwareType;
import com.cheriscon.common.model.SoftwareVersion;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.mapper.MinSaleUnitMemoPriceMapper;
import com.cheriscon.front.usercenter.customer.model.mapper.MinSaleUnitSoftwareDetailMapper;
import com.cheriscon.front.usercenter.customer.service.IMinSaleUnitDetailService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MarketPromotionDiscountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitMemoPriceVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitSoftwareVO;

/**
 * 最小销售单位功能业务逻辑实现类
 * 
 * @author caozhiyong
 * @version 2013-1-12
 */
@SuppressWarnings("rawtypes")
@Service
public class MinSaleUnitDetailServiceImpl extends BaseSupport implements
		IMinSaleUnitDetailService {

	@Override
	public String findMinSaleUnitMemo(String minSaleUnitCode,String languageCode) {
		String memo = "null";

		StringBuffer numSql = new StringBuffer();
		numSql.append("select * from DT_MinSaleUnitSoftwareDetail where minSaleUnitCode=");
		numSql.append("'");
		numSql.append(minSaleUnitCode);
		numSql.append("'");
		
		@SuppressWarnings("unchecked")
		// 查询最小销售单位里面包含的软件类型
		List<MinSaleUnitSoftwareDetail> softwareDetails = (List<MinSaleUnitSoftwareDetail>) this.daoSupport
				.queryForList(numSql.toString(),
						new MinSaleUnitSoftwareDetailMapper(), new Object[]{});
		// 判断最小销售单位是否只包含一个软件版本
		if (softwareDetails.size() > 1) {
			// 查询最小销售单位的说明
			StringBuffer sql = new StringBuffer();
			sql.append("select * from DT_MinSaleUnitMemo where minSaleUnitCode=");
			sql.append("'");
			sql.append(minSaleUnitCode);
			sql.append("'");
			sql.append("and languageCode=");
			sql.append("'");
			sql.append(languageCode);
			sql.append("'");
			MinSaleUnitMemo minSaleUnitMemo = (MinSaleUnitMemo) this.daoSupport
					.queryForObject(sql.toString(), MinSaleUnitMemo.class,
							new Object[]{});
			if (minSaleUnitMemo.getMemo()!= null) {
				memo = minSaleUnitMemo.getMemo().toString();
			}
		} else {
			// 查询最新发布的软件版本的语言包说明
			StringBuffer sql = new StringBuffer();
		
			sql.append("select l.memo from DT_MinSaleUnitSoftwareDetail m" +
					" left join DT_SoftwareVersion s on " +
					"s.softwareTypeCode=m.softwareTypeCode " +
					"left join DT_LanguagePack l on s.code=l.softwareVersionCode " +
					"where m.minSaleUnitCode=");
			sql.append("'");
			sql.append(minSaleUnitCode);
			sql.append("'");
			sql.append("and s.releaseState=0 order by s.releaseDate asc,l.memo desc limit 0,1");
			String softMemo = this.daoSupport.queryForString(sql.toString());
			if (softMemo != null) {
				memo = softMemo;
			}
		}
		return memo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public MarketPromotionDiscountInfoVO queryDiscount(CustomerInfo customerInfo,String minSaleUnitCode, String areaCfgCode,
			Integer type,String date) {
		
		//获取系统时间
		Calendar calendar=Calendar.getInstance();
		String time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
		
		//查询软件可以参与的活动列表
		StringBuffer s=new StringBuffer();
		s.append("select bi.* from DT_MarketPromotionSoftwareRule sr LEFT join DT_MarketPromotionBaseInfo bi on sr.marketPromotionCode=bi.code");
		s.append(" left join DT_MarketPromotionTimesInfo ti on bi.code=ti.marketPromotionCode");
		s.append(" LEFT join DT_MarketPromotionAreaRule ar on bi.code=ar.marketPromotionCode");
		s.append(" where (sr.minSaleUnitCode=");
		s.append("'");
		s.append(minSaleUnitCode);
		s.append("'");
		s.append(" or sr.minSaleUnitCode='all'");
		s.append(")");
		s.append(" and bi.status=1 and ti.startTime<=");
		s.append("'");
		s.append(time);
		s.append("'");
		s.append(" and ti.endTime>=");
		s.append("'");
		s.append(time);
		s.append("'");
		s.append(" and (ar.areaCode=");
		s.append("'");
		s.append(areaCfgCode);
		s.append("'");
		s.append(" or ar.areaCode='all'");
		s.append(")"); 
		s.append(" order by  bi.promotionType desc");
		List<MarkerPromotionBaseInfo> baseInfos=this.daoSupport.queryForList(s.toString(), MarkerPromotionBaseInfo.class, new Object[]{});
		
		if (baseInfos.size()<1) {
			return null;
		}
		
		return toQueryDiscountByPromotionCode(type, date, baseInfos);
		
		
	}

	/**
	 * 根据活动基本信息查询对应的活动折扣
	 * @param type
	 * @param date
	 * @param baseInfos
	 * @return
	 */
	private MarketPromotionDiscountInfoVO toQueryDiscountByPromotionCode(Integer type, String date,
			List<MarkerPromotionBaseInfo> baseInfos) {
		Map<String, Integer> map=compareDate(date);
		
		for (int i = 0; i < baseInfos.size(); i++) {
			MarkerPromotionBaseInfo baseInfo=baseInfos.get(i);
			MarketPromotionDiscountInfoVO info=new MarketPromotionDiscountInfoVO();
			
			if (baseInfo.getPromotionType()==FrontConstant.MARKET_PROMOTION_TYPE_IS_CHEAP) {//活动类型为越早越便宜
				if (type==1) {
					if(map.get("after")>0){//过期软件不参与此活动
						return null;
					}
					int num=map.get("before");//获取过期前相差月份
					if (num>0) {
						return queryDiscountRule(num, baseInfo, info);
					}
				}else {//购买不可参与此活动
					 return null;
				}
				
			}else if (baseInfo.getPromotionType()==FrontConstant.MARKET_PROMOTION_TYPE_IS_CONFINE) {//限时购买
				if (type==1) {
					if(map.get("after")>0){//过期不参与此活动
						return null;
					}
				}
				
				//根据活动code查询折扣信息
				StringBuffer sql=new StringBuffer();
				sql.append("select * from DT_MarketPromotionRule where marketPromotionCode=");
				sql.append("'");
				sql.append(baseInfo.getCode());
				sql.append("'");
				sql.append(" and conditionType=");
				sql.append("'");
				sql.append(type);
				sql.append("'");
				
				MarkerPromotionRule rule=(MarkerPromotionRule)this.daoSupport.queryForObject(sql.toString(), MarkerPromotionRule.class, new Object[]{});
				
				if (rule==null) {
					return null;
				}
				info.setDiscount(rule.getDiscount());
				info.setPromotionCode(baseInfo.getCode());
				info.setPromotionName(baseInfo.getPromotionName());
				info.setPromotionType(baseInfo.getPromotionType());
				
				return info;
				
			}else if(baseInfo.getPromotionType()==FrontConstant.MARKET_PROMOTION_TYPE_IS_SETMEAL){//套餐
				if (type==1) {
					if(map.get("after")>0){//过期不参与此活动
						return null;
					}
				}
				
			}else if(baseInfo.getPromotionType()==FrontConstant.MARKET_PROMOTION_TYPE_IS_PAST){//过期续租
				if (type==1) {
					if (map.get("after")>0) {//没过期不参与此活动
						return queryDiscountRule(map.get("after"), baseInfo, info);
					}
				}
			}
		}
		return null;
	}

	/**
	 * 查询规则条件
	 * @param month
	 * @param baseInfo
	 * @param info
	 * @return
	 */
	private MarketPromotionDiscountInfoVO queryDiscountRule(int month, MarkerPromotionBaseInfo baseInfo,
			MarketPromotionDiscountInfoVO info) {
		
		StringBuffer sql=new StringBuffer();
		sql.append("select * from  DT_MarketPromotionDiscountRule dr where exists");
		sql.append("(select code from DT_MarketPromotionRule r where marketPromotionCode=");
		sql.append("'");
		sql.append(baseInfo.getCode());
		sql.append("'");
		sql.append(" and dr.marketPromotionRuleCode=r.code )");
		sql.append(" and ");
		sql.append("'");
		sql.append(month);
		sql.append("'");
		sql.append(" between betweenMonth and endMonth");
		
		MarkerPromotionDiscountRule discountRule=(MarkerPromotionDiscountRule)this.daoSupport.queryForObject(sql.toString(), MarkerPromotionDiscountRule.class, new Object[]{});
		if (discountRule==null) {
			return null;
		}
		info.setDiscount(discountRule.getDiscount());
		info.setPromotionCode(baseInfo.getCode());
		info.setPromotionName(baseInfo.getPromotionName());
		info.setPromotionType(baseInfo.getPromotionType());
		
		return info;
	}

	/**
	 * 时间比较得出相差月份
	 * @param date1
	 * @return
	 */
	public static Map<String, Integer> compareDate(String date1) {
		
		Map<String, Integer> map=new HashMap<String, Integer>();
		
		int  before= 0;//过期前月份
		int  after= 0;//过期后月份
		
		String formatStyle = "yyyy/MM/dd";
		
		if(date1!=null){
			date1=date1.replace("-", "/");
		}
		
		Calendar c = Calendar.getInstance();   
		Date date = c.getTime();   
		SimpleDateFormat simple = new SimpleDateFormat("yyyy/MM/dd");   

		DateFormat df = new SimpleDateFormat(formatStyle);
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		
		Calendar c3 = Calendar.getInstance();
		Calendar c4 = Calendar.getInstance();
		try {
			if(date1!=null){
			c1.setTime(df.parse(date1));
			c2.setTime(df.parse(simple.format(date)));
			
			c3.setTime(df.parse(date1));
			c4.setTime(df.parse(simple.format(date)));
			}
		} catch (Exception e3) {
			System.out.println(e3.getMessage());
			System.out.println("wrong occured");
		}
		while (!c2.after(c1)) { // 循环对比，直到相等，n 就是所要的结果
			before++;
			c2.add(Calendar.MONTH, 1); // 比较月份，月份+1
		}
		while (!c3.after(c4)) { // 循环对比，直到相等，n 就是所要的结果
			after++;
			c3.add(Calendar.MONTH, 1); // 比较月份，月份+1
		}
		
		map.put("before", before);
		map.put("after", after);
		return map;
	}

	@Override
	public MarketPromotionDiscountInfoVO querySaleCfgDiscount(
			CustomerInfo customerInfo, String saleCfgCode,
			String areaCfgCode, Integer type, String date) {
		
		//获取系统时间
		Calendar calendar=Calendar.getInstance();
		String time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
		
		//查询销售配置可以参与的活动列表
		StringBuffer s=new StringBuffer();
		s.append("select bi.* from DT_MarketPromotionSaleConfig sc LEFT join DT_MarketPromotionBaseInfo bi on sc.marketPromotionCode=bi.code");
		s.append(" left join DT_MarketPromotionTimesInfo ti on bi.code=ti.marketPromotionCode");
		s.append(" LEFT join DT_MarketPromotionAreaRule ar on bi.code=ar.marketPromotionCode");
		s.append(" where (sc.saleconfigCode=");
		s.append("'");
		s.append(saleCfgCode);
		s.append("'");
		s.append(" or sc.saleconfigCode='all'");
		s.append(")");
		s.append(" and bi.status=1 and ti.startTime<=");
		s.append("'");
		s.append(time);
		s.append("'");
		s.append(" and ti.endTime>=");
		s.append("'");
		s.append(time);
		s.append("'");
		s.append(" and (ar.areaCode=");
		s.append("'");
		s.append(areaCfgCode);
		s.append("'");
		s.append("or ar.areaCode='all'");
		s.append(") order by  bi.promotionType desc");
		@SuppressWarnings("unchecked")
		List<MarkerPromotionBaseInfo> baseInfos=this.daoSupport.queryForList(s.toString(), MarkerPromotionBaseInfo.class, new Object[]{});
		
		if (baseInfos.size()<1) {
			return null;
		}
		
		return toQueryDiscountByPromotionCode(type, date, baseInfos);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MinSaleUnitSoftwareVO> querySoftWareInfo(String languageCode,String saleCfgCode,int type) {
		StringBuffer sql=new StringBuffer();
		if (type ==1) {//销售配置
			sql.append("select t.id as id,t.code as code,s.softwareName as name,t.proTypeCode as proTypeCode,t.carSignPath as carSignPath,t.softType as softType from DT_SoftwareType t left join DT_MinSaleUnitSoftwareDetail d on t.code=d.softwareTypeCode");
			sql.append(" inner join DT_SoftwareName s on t.code=s.softwareCode ");
			sql.append(" where s.languageCode='"+languageCode+"'");
			sql.append(" and exists(select minSaleUnitCode from DT_MinSaleUnitSaleCfgDetail m where saleCfgCode=");
			sql.append("'");
			sql.append(saleCfgCode);
			sql.append("'");
			sql.append(" and d.minSaleUnitCode= m.minSaleUnitCode)");
			sql.append(" and t.softType='1' "); //付费诊断程序
		}else{//软件
			sql.append(" select t.id as id,t.code as code,s.softwareName as name,t.proTypeCode as proTypeCode,t.carSignPath as carSignPath,t.softType as softType");
			sql.append(" from DT_MinSaleUnitSoftwareDetail d ");
			sql.append(" left join DT_softwareName s on d.softwareTypeCode=s.softwareCode");
		    sql.append(" left join DT_SoftwareType t on d.softwareTypeCode=t.code where d.minSaleUnitCode=");
			sql.append("'");
			sql.append(saleCfgCode);
			sql.append("'");
			sql.append(" and s.languageCode='"+languageCode+"'");
			sql.append(" and t.softType='1' "); //付费诊断程序
		}
		
		List<SoftwareType> softwareTypes =this.daoSupport.queryForList(sql.toString(), SoftwareType.class, new Object[]{});
		
		List<MinSaleUnitSoftwareVO> softwareVOs=new ArrayList<MinSaleUnitSoftwareVO>();
		
		for (int i = 0,j=softwareTypes.size(); i <j; i++) {
			SoftwareType  softwareType=softwareTypes.get(i);
			StringBuffer softVersionSql=new StringBuffer();
			 
			softVersionSql.append("select v.id,v.code,v.versionName,v.softwareTypeCode,v.releaseState,v.createDate,v.releaseDate from DT_SoftwareVersion v where v.softwareTypeCode=");
			softVersionSql.append("'");
			softVersionSql.append(softwareType.getCode());
			softVersionSql.append("'");
			softVersionSql.append(" and v.releaseState=");
			softVersionSql.append(FrontConstant.SOFTWARE_VERSION_RELEASE_IS_STATE);//发布状态已发布
			softVersionSql.append(" order by v.releaseDate desc limit 0,1");
			
			SoftwareVersion softwareVersion=(SoftwareVersion)this.daoSupport.queryForObject(softVersionSql.toString(), SoftwareVersion.class, new Object[]{});
			if(softwareVersion!=null){
				MinSaleUnitSoftwareVO vo=new MinSaleUnitSoftwareVO();
				vo.setName(softwareType.getName());

				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");

				try {
					Date strToDate = sdf.parse(softwareVersion.getReleaseDate());
					String releaseDate=new SimpleDateFormat("MM/dd/yyyy").format(strToDate);
					String releaseDate2=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(strToDate);
					vo.setReleaseDate(releaseDate);
					vo.setVersionNo(softwareVersion.getVersionName());
					vo.setVersionCode(softwareVersion.getCode());
					vo.setReleaseDate2(releaseDate2);
					softwareVOs.add(vo);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
		return softwareVOs;
		
	}

	@Override
	public LanguagePack querySoftWareVersionDetail(
			String softwareVersionCode,CustomerInfo customerInfo) {
		StringBuffer sql=new StringBuffer();
		sql.append("select * from  DT_LanguagePack where softwareVersionCode=");
		sql.append("'");
		sql.append(softwareVersionCode);
		sql.append("'");
		sql.append("and languageTypeCode=");
		sql.append("'");
		sql.append(customerInfo.getLanguageCode());
		sql.append("'");
		
		LanguagePack languagePack=(LanguagePack)this.daoSupport.queryForObject(sql.toString(), LanguagePack.class, new Object[]{});
		return languagePack;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CfgSoftRentInfoVO findSaleConfigDetailInfo(String saleCfgCode,
			String langugeCode,String serialNo) {
		StringBuffer sql = new StringBuffer();
		sql.append("select DT_SaleContract.saleCfgCode,DT_SaleContract.areaCfgCode,DT_SaleContract.reChargePrice,DT_MinSaleUnitMemo.*,DT_ProductSoftwareValidStatus.validDate "
				+ " ,a.name as saleConfigName,a.picpath as cfgpicpath,scm.memo as cfgMemo from (DT_MinSaleUnitSaleCfgDetail left join DT_SaleContract on "
				+ "DT_MinSaleUnitSaleCfgDetail.saleCfgCode=DT_SaleContract.saleCfgCode)"
				+ "left join DT_MinSaleUnitMemo on DT_MinSaleUnitSaleCfgDetail.minSaleUnitCode"
				+ "=DT_MinSaleUnitMemo.minSaleUnitCode left join DT_ProductSoftwareValidStatus "
				+ "on DT_ProductSoftwareValidStatus.minSaleUnitCode=DT_MinSaleUnitMemo.minSaleUnitCode left join DT_SaleConfig a on a.code=DT_SaleContract.saleCfgCode left join dt_productInfo on " +
				"dt_productInfo.code=DT_ProductSoftwareValidStatus.proCode and DT_SaleContract.code=dt_productInfo.saleContractCode " +
				" left join DT_SaleConfigMemo scm on scm.saleConfigCode=a.code where  1=1");
		sql.append(" and dt_productInfo.serialNo=");
		sql.append("'");
		sql.append(serialNo);
		sql.append("'");
		sql.append(" and DT_MinSaleUnitMemo.languageCode=");
		sql.append("'");
		sql.append(langugeCode);
		sql.append("'");
		sql.append(" and scm.languageCode=");
		sql.append("'");
		sql.append(langugeCode);
		sql.append("'");
		sql.append(" order by DT_MinSaleUnitMemo.id desc");

		List<MinSaleUnitMemoPriceVO> memoPriceVOs = (List<MinSaleUnitMemoPriceVO>) this.daoSupport
				.queryForList(sql.toString(), new MinSaleUnitMemoPriceMapper(),
						new Object[] {});
		CfgSoftRentInfoVO rentInfoVO = new CfgSoftRentInfoVO();
		
		if (memoPriceVOs.size() > 0) {
			rentInfoVO.setPrice(memoPriceVOs.get(0).getPrice());
			String time=memoPriceVOs.get(0).getValidDate();
			try {
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(time);
				rentInfoVO.setDate(DateUtil.toString(date, "yyyy/MM/dd"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			rentInfoVO.setSaleCfgCode(memoPriceVOs.get(0).getSaleCfgCode());
			rentInfoVO.setAreaCfgCode(memoPriceVOs.get(0).getAreaCfgCode());
			rentInfoVO.setSaleCfgName(memoPriceVOs.get(0).getSaleCfgName());
			rentInfoVO.setMemo(memoPriceVOs.get(0).getCfgMemo());
			rentInfoVO.setCfgpicpath(memoPriceVOs.get(0).getCfgpicpath());
		}
		
		for (int i = 0; i < memoPriceVOs.size(); i++) {
			rentInfoVO.getMinSaleUnitsMemo().add(
					memoPriceVOs.get(i).getMinSaleUnitMemo());
		}

		return rentInfoVO;
		
	}

	@Override
	public MinSaleUnitDetailVO findMinSaleUnitDetailInfo(
			String minSaleUnitCode, String langugeCode,String serialNo) {
		StringBuffer sql=new StringBuffer();
		sql.append("select sc.areaCfgCode,s.validDate as date,mp.price,mp.minSaleUnitCode,m.name as minSaleUnitName,m.memo " +
				",m.memoDetail,mu.picpath from DT_ProductInfo p left join DT_SaleContract sc on p.saleContractCode =sc.code left join DT_MinSaleUnitPrice mp " +
				"on sc.areaCfgCode=mp.areaCfgCode left join DT_MinSaleUnitMemo m on m.minSaleUnitCode=mp.minSaleUnitCode " +
				"left join DT_ProductSoftwareValidStatus s on p.code=s.proCode and s.minSaleUnitCode=mp.minSaleUnitCode " +
				"left join DT_MinSaleUnit mu on mu.code=m.minSaleUnitCode where p.serialNo=");
		sql.append("'");
		sql.append(serialNo);
		sql.append("'");
		sql.append(" and mp.minSaleUnitCode=");
		sql.append("'");
		sql.append(minSaleUnitCode);
		sql.append("'");
		sql.append(" and m.languageCode=");
		sql.append("'");
		sql.append(langugeCode);
		sql.append("'");
		
		
		return (MinSaleUnitDetailVO)daoSupport.queryForObject(sql.toString(), MinSaleUnitDetailVO.class, new Object[]{});
	}

	
	 public List<MinSaleUnitSoftwareVO> querySoftWareInfoByContract(String languageCode,String contractCode,int type){
		 StringBuffer sql=new StringBuffer();
			if (type ==1) {//销售配置
				sql.append("select t.id as id,t.code as code,s.softwareName as name,t.proTypeCode as proTypeCode,t.carSignPath as carSignPath,t.softType as softType from DT_SoftwareType t left join DT_MinSaleUnitSoftwareDetail d on t.code=d.softwareTypeCode");
				sql.append(" inner join DT_SoftwareName s on t.code=s.softwareCode ");
				sql.append(" where s.languageCode='"+languageCode+"'");
				sql.append(" and exists(select c.minSaleUnitCode from DT_SaleContract a ,DT_SaleConfig b ,DT_MinSaleUnitSaleCfgDetail c ");
				sql.append(" where a.saleCfgCode=b.code ");
				sql.append(" and b.code=c.saleCfgCode ");
				sql.append(" and a.code=");
				sql.append("'");
				sql.append(contractCode);
				sql.append("'");
				sql.append(" and c.minSaleUnitCode=d.minSaleUnitCode)");
				sql.append(" and t.softType='1' "); //付费诊断程序
			}else{//软件
				sql.append(" select t.id as id,t.code as code,s.softwareName as name,t.proTypeCode as proTypeCode,t.carSignPath as carSignPath,t.softType as softType");
				sql.append(" from DT_MinSaleUnitSoftwareDetail d ");
				sql.append(" left join DT_softwareName s on d.softwareTypeCode=s.softwareCode");
			    sql.append(" left join DT_SoftwareType t on d.softwareTypeCode=t.code where d.minSaleUnitCode=");
				sql.append("'");
				sql.append(contractCode);
				sql.append("'");
				sql.append(" and s.languageCode='"+languageCode+"'");
				sql.append(" and t.softType='1' "); //付费诊断程序
			}
			
			List<SoftwareType> softwareTypes =this.daoSupport.queryForList(sql.toString(), SoftwareType.class, new Object[]{});
			
			List<MinSaleUnitSoftwareVO> softwareVOs=new ArrayList<MinSaleUnitSoftwareVO>();
			
			for (int i = 0,j=softwareTypes.size(); i <j; i++) {
				SoftwareType  softwareType=softwareTypes.get(i);
				StringBuffer softVersionSql=new StringBuffer();
				 
				softVersionSql.append("select v.id,v.code,v.versionName,v.softwareTypeCode,v.releaseState,v.createDate,v.releaseDate from DT_SoftwareVersion v where v.softwareTypeCode=");
				softVersionSql.append("'");
				softVersionSql.append(softwareType.getCode());
				softVersionSql.append("'");
				softVersionSql.append("and v.releaseState=");
				softVersionSql.append(FrontConstant.SOFTWARE_VERSION_RELEASE_IS_STATE);//发布状态已发布
				softVersionSql.append(" order by v.releaseDate desc limit 0,1");
				
				SoftwareVersion softwareVersion=(SoftwareVersion)this.daoSupport.queryForObject(softVersionSql.toString(), SoftwareVersion.class, new Object[]{});
				if(softwareVersion!=null){
					MinSaleUnitSoftwareVO vo=new MinSaleUnitSoftwareVO();
					vo.setName(softwareType.getName());

					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");

					try {
						Date strToDate = sdf.parse(softwareVersion.getReleaseDate());
						String releaseDate=new SimpleDateFormat("MM/dd/yyyy").format(strToDate);
						String releaseDate2=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(strToDate);
						vo.setReleaseDate(releaseDate);
						vo.setVersionNo(softwareVersion.getVersionName());
						vo.setVersionCode(softwareVersion.getCode());
						vo.setReleaseDate2(releaseDate2);
						softwareVOs.add(vo);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
			}
			
			return softwareVOs;
			
	 }
	
}
