package com.cheriscon.front.usercenter.customer.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.usercenter.customer.service.IDeleteShoppingCartService;

/**
 * 删除我的购物车数据业务逻辑实现类
 * @author caozhiyong
 * @version 2013-1-15
 */
@SuppressWarnings("rawtypes")
@Service
public class DeleteShoppingCartServiceImpl extends BaseSupport implements
		IDeleteShoppingCartService {

	@Override
	public boolean delShoppingCart(String usercode,String proSerial,String minSaleUnitCode) {
		StringBuffer sql=new StringBuffer();
		sql.append("delete from DT_ShoppingCart where customerCode=");
		sql.append("'");
		sql.append(usercode);
		sql.append("'");
		sql.append(" and minSaleUnitCode=");
		sql.append("'");
		sql.append(minSaleUnitCode);
		sql.append("'");
		sql.append(" and proSerial=");
		sql.append("'");
		sql.append(proSerial);
		sql.append("'");
		try {
			this.daoSupport.execute(sql.toString(), new Object[]{});
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	/**
	 * 批量删除
	 */
	@Override
	@Transactional
	public boolean batchDelshoppingCart(String usercode,
			List<String> minSaleUnitCodes) {
		
		StringBuffer sql=new StringBuffer();
		sql.append("delete DT_ShoppingCart where");
		sql.append(" customerCode=");
		sql.append("'");
		sql.append(usercode);
		sql.append("'");
		
		sql.append(" and minSaleUnitCode in");
		sql.append("(");
		
		for (int i = 0,j=minSaleUnitCodes.size(); i < j; i++) {
			if(i==j-1){
				sql.append("'");
				sql.append(minSaleUnitCodes.get(i));
				sql.append("'");
			}else {
				sql.append("'");
				sql.append(minSaleUnitCodes.get(i));
				sql.append("'");
				sql.append(",");
			}
		}
		sql.append(")");
		try {
			this.daoSupport.execute(sql.toString(), new Object[]{});
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean delshoppingCart(String usercode, String minSaleUnitCode,
			String proCode) {
		StringBuffer sql=new StringBuffer();
		sql.append("delete DT_ShoppingCart where");
		sql.append(" customerCode=");
		sql.append("'");
		sql.append(usercode);
		sql.append("'");
		
		sql.append(" and minSaleUnitCode =");
		sql.append("'");
		sql.append(minSaleUnitCode);
		sql.append("'");
		
		sql.append(" and proCode =");
		sql.append("'");
		sql.append(proCode);
		sql.append("'");
		
		
		try {
			this.daoSupport.execute(sql.toString(), new Object[]{});
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
