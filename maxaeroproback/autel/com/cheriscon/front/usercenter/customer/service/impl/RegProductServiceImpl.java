package com.cheriscon.front.usercenter.customer.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.market.vo.ProductForSealer;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.CustomerProInfo;
import com.cheriscon.common.model.ProductAssociatesDate;
import com.cheriscon.common.model.ProductChangeNoLog;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductRemoveLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.mapper.RegProductInfoMapper;
import com.cheriscon.front.usercenter.customer.service.IProductDateService;
import com.cheriscon.front.usercenter.customer.service.IRegProductService;
import com.cheriscon.front.usercenter.customer.vo.ProductSoftwareValidStatusUpMonthVO;
import com.cheriscon.front.usercenter.customer.vo.ProductSoftwareValidStatusVO;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;
import com.cheriscon.front.usercenter.customer.vo.RegProductDetailInfoVO;

/**
 * 注册产品信息业务实现类
 * 
 * @author caozhiyong
 * @version 2013-1-8
 */
@Service
public class RegProductServiceImpl extends
		BaseSupport implements IRegProductService {

	@Resource
	private IProductDateService productDateService;
	
	@Override
	public List<ProductInfo> queryProductInfo(RegProductAcountInfoVO regProductInfoVO) {
		StringBuffer sql = new StringBuffer();
		sql.append("select DT_ProductInfo.* from DT_ProductInfo where ");

		sql.append(" DT_ProductInfo.proTypeCode =");// 产品类型code
		sql.append("'");
		sql.append(regProductInfoVO.getProTypeCode());
		sql.append("'");

		sql.append(" and DT_ProductInfo.serialNo  like ");// 产品序列号
		sql.append("'");
		sql.append(regProductInfoVO.getProSerialNo());
		sql.append("%'");

//		sql.append(" and DT_ProductInfo.regPwd =");// 产品注册密码
//		sql.append("'");
//		sql.append(regProductInfoVO.getProRegPwd());
//		sql.append("'");
//
//		sql.append(" and DT_SealerInfo.autelId =");// 经销商编号
//		sql.append("'");
//		sql.append(regProductInfoVO.getSealCode());
//		sql.append("'");

		sql.append(" order by DT_ProductInfo.id desc");

		
		return  this.daoSupport.queryForList(sql.toString(), ProductInfo.class, new Object[]{});
		 //this.daoSupport.queryForPage(sql.toString(), 1, 1, new Object[]{});
	}

	@Override
	public boolean regProductInfo(RegProductAcountInfoVO regProductInfoVO) {
		StringBuffer sql = new StringBuffer();
		sql.append("update DT_ProductInfo set regStatus=1,regTime=?");
		sql.append(" where regPwd=");// 产品注册密码
		sql.append("'");
		sql.append(regProductInfoVO.getProRegPwd());
		sql.append("'");

		sql.append(" and proTypeCode =");// 产品类型code
		sql.append("'");
		sql.append(regProductInfoVO.getProTypeCode());
		sql.append("'");

		sql.append(" and serialNo =");// 产品序列号
		sql.append("'");
		sql.append(regProductInfoVO.getProSerialNo());
		sql.append("'");

		try {
			this.daoSupport.execute(sql.toString(),
					DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}

	public boolean addProductSoftwareValidStatus(String usercode,String validDate,String regDate,
			RegProductAcountInfoVO regProductInfoVO){
		try {
			
			/**
			 * 修改产品注册状态以及添加产品注册时间
			 */
			if(StringUtil.isBlank(regDate)){
				regDate=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			}
			
			StringBuffer sql = new StringBuffer();
			sql.append("update DT_ProductInfo set regStatus=1,regTime='"+regDate+"'");
			sql.append(" where regPwd=");// 产品注册密码
			sql.append("'");
			sql.append(regProductInfoVO.getProRegPwd());
			sql.append("'");

			sql.append(" and proTypeCode =");// 产品类型code
			sql.append("'");
			sql.append(regProductInfoVO.getProTypeCode());
			sql.append("'");

			sql.append(" and serialNo =");// 产品序列号
			sql.append("'");
			sql.append(regProductInfoVO.getProSerialNo());
			sql.append("'");

			
			this.daoSupport.execute(sql.toString(),null);
			
			// 查询标准出厂配置对应的最小销售单位
			StringBuffer querySql = new StringBuffer();
			querySql.append("select DT_ProductInfo.code as proCode,DT_ProductInfo.regTime as validDate,DT_MinSaleUnitSaleCfgDetail.minSaleUnitCode" +
					",DT_SaleContract.upMonth from"
					+ " (DT_MinSaleUnitSaleCfgDetail left join DT_SaleContract on "
					+ "DT_MinSaleUnitSaleCfgDetail.saleCfgCode =DT_SaleContract.saleCfgCode)"
					+ "left join DT_ProductInfo on DT_SaleContract.code=DT_ProductInfo.saleContractCode "
					+ "where 1=1");
			querySql.append(" and DT_ProductInfo.serialNo=?");// 序列号

			List<ProductSoftwareValidStatusUpMonthVO> proSoftwareValidStatusUpMonthVOs = this.daoSupport
					.queryForList(querySql.toString(),
							ProductSoftwareValidStatusUpMonthVO.class, regProductInfoVO.getProSerialNo());

			// 用户产品关联类
			CustomerProInfo customerProInfo = new CustomerProInfo();
			customerProInfo.setCustomerCode(usercode);
			customerProInfo.setProCode(proSoftwareValidStatusUpMonthVOs.get(0)
					.getProCode());
			

			// 添加用户产品关联信息数据
			this.daoSupport.insert(DBUtils.getTableName(CustomerProInfo.class),
					customerProInfo);
			
			
			
			for (int i = 0, j = proSoftwareValidStatusUpMonthVOs.size(); i < j; i++) {
				ProductSoftwareValidStatusVO productSoftwareValidStatusVO = new ProductSoftwareValidStatusVO();

				productSoftwareValidStatusVO.setMinSaleUnitCode(proSoftwareValidStatusUpMonthVOs.get(i).getMinSaleUnitCode());
				productSoftwareValidStatusVO.setProCode(proSoftwareValidStatusUpMonthVOs.get(i).getProCode());
				productSoftwareValidStatusVO.setValidDate(validDate);
				productSoftwareValidStatusVO.setCode(DBUtils
						.generateCode(DBConstant.PRO_SOFTWARE_VALID_STATUS));
				// 把产品对应的标准出厂软件配置（最小销售单位）写到产品软件效状态表中
				this.daoSupport.insert(
						DBUtils.getTableName(ProductSoftwareValidStatus.class),
						productSoftwareValidStatusVO);
			}

			
				
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

		return true;
	}
	public boolean addProductSoftwareValidStatus(String usercode,String validDate,String regDate,
			String proDate,RegProductAcountInfoVO regProductInfoVO){
		try {
			
			/**
			 * 修改产品注册状态以及添加产品注册时间
			 */
			if(StringUtil.isBlank(regDate)){
				regDate=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			}
			
			StringBuffer sql = new StringBuffer();
			sql.append("update DT_ProductInfo set regStatus=1,regTime='"+regDate+"',proDate='"+proDate+"'");
			sql.append(" where proTypeCode =");// 产品类型code
			sql.append("'");
			sql.append(regProductInfoVO.getProTypeCode());
			sql.append("'");

			sql.append(" and serialNo =");// 产品序列号
			sql.append("'");
			sql.append(regProductInfoVO.getProSerialNo());
			sql.append("'");

			
			this.daoSupport.execute(sql.toString(),null);
			
			// 查询标准出厂配置对应的最小销售单位
			StringBuffer querySql = new StringBuffer();
			querySql.append("select DT_ProductInfo.code as proCode,DT_ProductInfo.regTime as validDate,DT_MinSaleUnitSaleCfgDetail.minSaleUnitCode" +
					",DT_SaleContract.upMonth from"
					+ " (DT_MinSaleUnitSaleCfgDetail left join DT_SaleContract on "
					+ "DT_MinSaleUnitSaleCfgDetail.saleCfgCode =DT_SaleContract.saleCfgCode)"
					+ "left join DT_ProductInfo on DT_SaleContract.code=DT_ProductInfo.saleContractCode "
					+ "where 1=1");
			querySql.append(" and DT_ProductInfo.serialNo=?");// 序列号

			List<ProductSoftwareValidStatusUpMonthVO> proSoftwareValidStatusUpMonthVOs = this.daoSupport
					.queryForList(querySql.toString(),
							ProductSoftwareValidStatusUpMonthVO.class, regProductInfoVO.getProSerialNo());

			// 用户产品关联类
			CustomerProInfo customerProInfo = new CustomerProInfo();
			customerProInfo.setCustomerCode(usercode);
			customerProInfo.setProCode(proSoftwareValidStatusUpMonthVOs.get(0)
					.getProCode());
			

			// 添加用户产品关联信息数据
			this.daoSupport.insert(DBUtils.getTableName(CustomerProInfo.class),
					customerProInfo);
			
			
			
			for (int i = 0, j = proSoftwareValidStatusUpMonthVOs.size(); i < j; i++) {
				ProductSoftwareValidStatusVO productSoftwareValidStatusVO = new ProductSoftwareValidStatusVO();

				productSoftwareValidStatusVO.setMinSaleUnitCode(proSoftwareValidStatusUpMonthVOs.get(i).getMinSaleUnitCode());
				productSoftwareValidStatusVO.setProCode(proSoftwareValidStatusUpMonthVOs.get(i).getProCode());
				productSoftwareValidStatusVO.setValidDate(validDate);
				productSoftwareValidStatusVO.setCode(DBUtils
						.generateCode(DBConstant.PRO_SOFTWARE_VALID_STATUS));
				// 把产品对应的标准出厂软件配置（最小销售单位）写到产品软件效状态表中
				this.daoSupport.insert(
						DBUtils.getTableName(ProductSoftwareValidStatus.class),
						productSoftwareValidStatusVO);
			}

			
				
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

		return true;
	}
	@SuppressWarnings("deprecation")
	@Transactional
	public boolean addProductSoftwareValidStatus(String usercode,
			RegProductAcountInfoVO regProductInfoVO) {
		try {
			
			//查询是否是更换过产品序列号的用户20140213
			StringBuffer queryValidSql=new StringBuffer();
			queryValidSql.append("select * from dt_ProductChangeNoLog a,dt_productinfo b where a.oldprocode=b.code and b.serialNo='"+regProductInfoVO.getProSerialNo()+"' order by  a.id desc limit 0,1");
			ProductChangeNoLog changeLog=(ProductChangeNoLog)this.daoSupport.queryForObject(queryValidSql.toString(), ProductChangeNoLog.class);
			String validDate="";
			if(null != changeLog && changeLog.getValidDate()!=null){
				validDate=changeLog.getValidDate();
			}
			
			/**
			 * 修改产品注册状态以及添加产品注册时间
			 */
			String regTime=DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss");
			if(null != changeLog && changeLog.getRegDate()!=null){
				regTime=changeLog.getRegDate();
			}
			
			
			StringBuffer sql = new StringBuffer();
			sql.append("update DT_ProductInfo set regStatus=1,regTime='"+regTime+"'");
//			sql.append(" where regPwd=");// 产品注册密码
//			sql.append("'");
//			sql.append(regProductInfoVO.getProRegPwd());
//			sql.append("'");
			sql.append(" where proTypeCode =");// 产品类型code
			sql.append("'");
			sql.append(regProductInfoVO.getProTypeCode());
			sql.append("'");
			sql.append(" and serialNo =");// 产品序列号
			sql.append("'");
			sql.append(regProductInfoVO.getProSerialNo());
			sql.append("'");
System.out.println("*****************");
System.out.println(sql);
System.out.println("*****************");
			this.daoSupport.execute(sql.toString(),null);
			
			
			// 查询标准出厂配置对应的最小销售单位
			StringBuffer querySql = new StringBuffer();
			querySql.append("select DT_ProductInfo.code as proCode,DT_ProductInfo.regTime as validDate,DT_MinSaleUnitSaleCfgDetail.minSaleUnitCode" +
					",DT_SaleContract.upMonth from"
					+ " (DT_MinSaleUnitSaleCfgDetail left join DT_SaleContract on "
					+ "DT_MinSaleUnitSaleCfgDetail.saleCfgCode =DT_SaleContract.saleCfgCode)"
					+ "left join DT_ProductInfo on DT_SaleContract.code=DT_ProductInfo.saleContractCode "
					+ "where 1=1");
			querySql.append(" and DT_ProductInfo.serialNo=?");// 序列号

			List<ProductSoftwareValidStatusUpMonthVO> proSoftwareValidStatusUpMonthVOs = this.daoSupport
					.queryForList(querySql.toString(),
							ProductSoftwareValidStatusUpMonthVO.class, regProductInfoVO.getProSerialNo());

			// 用户产品关联类
			CustomerProInfo customerProInfo = new CustomerProInfo();
			customerProInfo.setCustomerCode(usercode);
			customerProInfo.setProCode(proSoftwareValidStatusUpMonthVOs.get(0)
					.getProCode());
			

			// 添加用户产品关联信息数据
			this.daoSupport.insert(DBUtils.getTableName(CustomerProInfo.class),
					customerProInfo);
			
			
			
			
			
			for (int i = 0, j = proSoftwareValidStatusUpMonthVOs.size(); i < j; i++) {
				ProductSoftwareValidStatusVO productSoftwareValidStatusVO = new ProductSoftwareValidStatusVO();

				productSoftwareValidStatusVO.setMinSaleUnitCode(proSoftwareValidStatusUpMonthVOs.get(i).getMinSaleUnitCode());
				productSoftwareValidStatusVO.setProCode(proSoftwareValidStatusUpMonthVOs.get(i).getProCode());
				
				if(validDate.equals("")){
					Date date=new SimpleDateFormat("yyyy-MM-dd").parse(proSoftwareValidStatusUpMonthVOs.get(i).getValidDate());
					date.setMonth(date.getMonth()+proSoftwareValidStatusUpMonthVOs.get(i).getUpMonth());
					productSoftwareValidStatusVO.setValidDate(new SimpleDateFormat("yyyy-MM-dd").format(date));//有效期年份加1年
				}else{
					productSoftwareValidStatusVO.setValidDate(validDate);
				}
//				当proSoftwareValidStatusUpMonthVOs有多条数据时 生成的code因没有加随机码会导致重复
//				所有在末尾加上了i
				productSoftwareValidStatusVO.setCode(DBUtils
						.generateCode(DBConstant.PRO_SOFTWARE_VALID_STATUS));
				// 把产品对应的标准出厂软件配置（最小销售单位）写到产品软件效状态表中
				this.daoSupport.insert(
						DBUtils.getTableName(ProductSoftwareValidStatus.class),
						productSoftwareValidStatusVO);
			}

			
				
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

		return true;
	}

	
	public int checkProduct(String autelId,String proSerialNo){
		
		StringBuffer sql = new StringBuffer();
		sql.append("select count(1) from DT_CustomerInfo a,DT_CustomerProInfo b,DT_ProductInfo c");
		sql.append(" where a.code=b.customerCode and b.proCode=c.code and a.autelId='");
		sql.append(autelId.trim()+"'");
		sql.append(" and c.serialNo='");
		sql.append(proSerialNo.trim()+"'");
		return this.daoSupport.queryForInt(sql.toString(), new Object[]{});
	}
	
	public ProductInfo queryProduct(String proSerialNo){
		StringBuffer sql=new StringBuffer();
		sql.append("select d.* from DT_ProductInfo d where serialNo='"+proSerialNo+"'");
		return (ProductInfo)this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, new Object[]{});
	}
	
	public ProductInfo queryProductByCode(String code){
		StringBuffer sql=new StringBuffer();
		sql.append("select d.* from DT_ProductInfo d where code='"+code+"'");
		return (ProductInfo)this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, new Object[]{});
	}
	
	public ProductInfo queryProductAndSealer(String proSerialNo){
		StringBuffer sql = new StringBuffer();
		sql.append("select d.*,b.autelId as sealerAutelId ,");
		sql.append(" (select distinct validdate from DT_ProductSoftwareValidStatus where proCode=d.code) as validDate");
		sql.append(" from dt_productinfo d,DT_SaleContract s,DT_SealerInfo b");
		sql.append(" where d.saleContractCode=s.code");
		sql.append(" and s.sealerCode=b.code");
		sql.append(" and d.serialNo=?");
		return (ProductInfo)this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, new Object[]{proSerialNo});
	}
	
	public boolean removeProductInfo(String proCode){
		
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		Object obj=SessionUtil.getLoginUserInfo(request);
		String operateUser="";
		if(obj.getClass().equals(CustomerInfo.class)){
			operateUser=((CustomerInfo)obj).getCode();
		}
		if(obj.getClass().equals(SealerInfo.class)){
			operateUser=((SealerInfo)obj).getCode();
		}
		
		String ip=FrontConstant.getIpAddr(request);
		String querySql="select * from dt_customerProInfo where proCode=?";
		CustomerProInfo proInfo=(CustomerProInfo) this.daoSupport.queryForObject(querySql, CustomerProInfo.class, proCode);
		if(proInfo!=null){
			ProductRemoveLog log=new ProductRemoveLog();
			log.setCustomerCode(proInfo.getCustomerCode());
			log.setProCode(proInfo.getProCode());
			log.setOperateDate(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			log.setOperateIP(ip);
			log.setOperateUser(operateUser);
			this.daoSupport.insert("DT_ProductRemoveLog", log);
			this.daoSupport.execute("delete from dt_customerProInfo where proCode=?", proCode);
		}else{
			return false;
		}
		
		return true;
		
	}
	public boolean removeProductInfo(String proCode,String operateUser,String ip){
		String querySql="select * from dt_customerProInfo where proCode=?";
		CustomerProInfo proInfo=(CustomerProInfo) this.daoSupport.queryForObject(querySql, CustomerProInfo.class, proCode);
		if(proInfo!=null){
			ProductRemoveLog log=new ProductRemoveLog();
			log.setCustomerCode(proInfo.getCustomerCode());
			log.setProCode(proInfo.getProCode());
			log.setOperateDate(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			log.setOperateIP(ip);
			log.setOperateUser(operateUser);
			this.daoSupport.insert("DT_ProductRemoveLog", log);
			this.daoSupport.execute("delete from dt_customerProInfo where proCode=?", proCode);
		}else{
			return false;
		}
		
		return true;
	}
	
	public CustomerProInfo CustomerProductNum(String proCode){
		StringBuffer sql = new StringBuffer();
		sql.append("select * from DT_CustomerProInfo where proCode=?");
		return (CustomerProInfo)this.daoSupport.queryForObject(sql.toString(), CustomerProInfo.class,new Object[]{proCode});
	}
	
	public boolean insertCustProInfo(CustomerProInfo custProInfo){
		this.daoSupport.insert(DBUtils.getTableName(CustomerProInfo.class),
				custProInfo);
		return true;
	}
	
	public boolean changeCustProInfo(String userCode,RegProductAcountInfoVO regProductInfoVO){
		//1.先删除对应关系
		String delSql="delete from dt_customerProInfo where proCode='"+regProductInfoVO.getProCode()+"'";
		this.daoSupport.execute(delSql, null);
		
		// 2.用户产品关联类
		CustomerProInfo customerProInfo = new CustomerProInfo();
		customerProInfo.setCustomerCode(userCode);
		customerProInfo.setProCode(regProductInfoVO.getProCode());
		this.daoSupport.insert("DT_CustomerProInfo", customerProInfo);
		//更新关联日期
		ProductAssociatesDate proDate=productDateService.getProductDateInfo(regProductInfoVO.getProCode());
		if(proDate==null){
			proDate=new ProductAssociatesDate();
			proDate.setAssociatesDate(DateUtil.toString(new Date(), "yyyy-MM-dd"));
			proDate.setProCode(regProductInfoVO.getProCode());
			productDateService.insertProductDate(proDate);
		}else{
			productDateService.updateProductDate(DateUtil.toString(new Date(), "yyyy-MM-dd"), regProductInfoVO.getProCode());
		}
		
		return true;
	}
	
	@Override
	public ProductInfo queryProductBySerialNo(String serialNo){
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		String productTypeTbl = DBUtils.getTableName(ProductForSealer.class);
		
		StringBuffer sql = new StringBuffer();
		sql.append("select p.*,pt.name proTypeName from "+ productInfoTbl +" p");
		sql.append(" left join "+productTypeTbl + " pt on p.proTypeCode = pt.code ");
		sql.append(" where p.serialNo = ?");
		
		return (ProductInfo) this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, serialNo);
	}
	
	/**
	 * 20160428 A16043 hlh
	 * 通过飞机序列号查找对应的主序列号
	 */
	@Override
	public ProductInfo queryProductByAricraftSerialNumber(
			String aricraftSerialNumber) {
		String productInfoTbl = DBUtils.getTableName(ProductInfo.class);
		StringBuffer sql = new StringBuffer("select pif.* from "  +  productInfoTbl + " pif ");
		sql.append(" where pif.AricraftSerialNumber=? ");
		return (ProductInfo)this.daoSupport.queryForObject(sql.toString(), ProductInfo.class, aricraftSerialNumber);
	}

	@Override
	public String queryRegAutelIDByAricraftSerialNumber(String aricraftSerialNumber) {
		StringBuffer querySql = new StringBuffer();
		querySql.append("select t.* from dt_customerInfo t ,dt_customerProInfo d,dt_productInfo e ");
		querySql.append("where t.code = d.customerCode and d.proCode = e.code ");
		querySql.append("and e.AricraftSerialNumber =?");// 序列号
        
		CustomerInfo customerInfo = (CustomerInfo)this.daoSupport.queryForObject(querySql.toString(),CustomerInfo.class, new Object[]{aricraftSerialNumber});
		if(customerInfo != null){
			return customerInfo.getAutelId();
		}
		return null;
	}
}
