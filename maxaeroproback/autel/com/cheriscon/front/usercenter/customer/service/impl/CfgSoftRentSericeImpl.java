package com.cheriscon.front.usercenter.customer.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.front.usercenter.customer.model.mapper.MinSaleUnitMemoPriceMapper;
import com.cheriscon.front.usercenter.customer.service.ICfgSoftRentService;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitMemoPriceVO;

/**
 * 产品出厂配置软件接口实现类
 * 
 * @author caozhiyong
 * @version 2013-1-10
 */
@Service
public class CfgSoftRentSericeImpl extends BaseSupport<MinSaleUnitMemoPriceVO>
		implements ICfgSoftRentService {

	@Override
	public CfgSoftRentInfoVO querySoftRent(String saleContractCode,
			String proCode,CustomerInfo customerInfo) {

		StringBuffer sql = new StringBuffer();
		sql.append("select DT_SaleContract.saleCfgCode,DT_SaleContract.areaCfgCode,DT_SaleContract.reChargePrice,DT_MinSaleUnitMemo.*,DT_ProductSoftwareValidStatus.validDate "
				+ " ,sm.name as saleConfigName,a.picpath as cfgpicpath,sm.memo as cfgMemo from (DT_MinSaleUnitSaleCfgDetail left join DT_SaleContract on "
				+ "DT_MinSaleUnitSaleCfgDetail.saleCfgCode=DT_SaleContract.saleCfgCode)"
				+ "left join DT_MinSaleUnitMemo on DT_MinSaleUnitSaleCfgDetail.minSaleUnitCode"
				+ "=DT_MinSaleUnitMemo.minSaleUnitCode left join DT_ProductSoftwareValidStatus "
				+ "on DT_ProductSoftwareValidStatus.minSaleUnitCode=DT_MinSaleUnitMemo.minSaleUnitCode left join DT_SaleConfig a on a.code=DT_SaleContract.saleCfgCode " +
				"left join DT_SaleConfigMemo sm on sm.saleConfigCode= a.code where  1=1");
		sql.append(" and DT_SaleContract.code=");
		sql.append("'");
		sql.append(saleContractCode);
		sql.append("'");
		sql.append(" and DT_ProductSoftwareValidStatus.proCode=");
		sql.append("'");
		sql.append(proCode);
		sql.append("'");
		sql.append(" and DT_MinSaleUnitMemo.languageCode=");
		sql.append("'");
		sql.append(customerInfo.getLanguageCode());
		sql.append("'");
		sql.append(" and sm.languageCode=");
		sql.append("'");
		sql.append(customerInfo.getLanguageCode());
		sql.append("'");
		sql.append(" order by DT_MinSaleUnitMemo.id desc");

		List<MinSaleUnitMemoPriceVO> memoPriceVOs = (List<MinSaleUnitMemoPriceVO>) this.daoSupport
				.queryForList(sql.toString(), new MinSaleUnitMemoPriceMapper(),
						new Object[] {});
		CfgSoftRentInfoVO rentInfoVO = new CfgSoftRentInfoVO();
		
		if (memoPriceVOs.size() > 0) {
			rentInfoVO.setPrice(memoPriceVOs.get(0).getPrice());
			String time=memoPriceVOs.get(0).getValidDate();
			try {
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(time);
				rentInfoVO.setDate(DateUtil.toString(date, "MM/dd/yyyy"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			rentInfoVO.setSaleCfgCode(memoPriceVOs.get(0).getSaleCfgCode());
			rentInfoVO.setAreaCfgCode(memoPriceVOs.get(0).getAreaCfgCode());
			rentInfoVO.setSaleCfgName(memoPriceVOs.get(0).getSaleCfgName());
			rentInfoVO.setCfgpicpath(memoPriceVOs.get(0).getCfgpicpath());
			rentInfoVO.setMemo(memoPriceVOs.get(0).getCfgMemo());
		}
		
		for (int i = 0; i < memoPriceVOs.size(); i++) {
			rentInfoVO.getMinSaleUnitsMemo().add(
					memoPriceVOs.get(i).getMinSaleUnitMemo());
		}

		return rentInfoVO;
	}	
}
