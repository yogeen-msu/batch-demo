package com.cheriscon.front.usercenter.customer.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.PayLog;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.front.usercenter.customer.service.IPayService;

/**
 * 支付接口实现类
 * 
 * @author caozhiyong
 * @version 2013-1-10
 */
@Service
public class PayLogSericeImpl extends BaseSupport<PayLog>
		implements IPayService {

	@Override
	public void insertPayLog(PayLog payLog) throws Exception{
		this.daoSupport.insert("DT_payLog", payLog);
	}

	public int getPayLogNumByTradeNum(String tradeNum) throws Exception{
		String sql="select count(1) from DT_payLog where trade_no=?";
		return this.daoSupport.queryForInt(sql, tradeNum);
		
	}
}
