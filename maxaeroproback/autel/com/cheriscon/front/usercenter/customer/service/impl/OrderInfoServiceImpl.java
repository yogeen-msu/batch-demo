package com.cheriscon.front.usercenter.customer.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.backstage.order.vo.OrderInfoVo;
import com.cheriscon.common.constant.DBConstant;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderMinSaleUnitDetail;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.service.IOrderInfoService;
import com.cheriscon.front.usercenter.customer.vo.MyOrderPageData;
import com.cheriscon.front.usercenter.customer.vo.OneOrderInfoVo;
import com.cheriscon.front.usercenter.customer.vo.OrderInfoVO;
import com.cheriscon.front.usercenter.customer.vo.OrderProductPageData;

/**
 * 订单业务逻辑实现类
 * @author caozhiyong
 * @version 2013-1-17
 */
@SuppressWarnings("rawtypes")
@Service
public class OrderInfoServiceImpl extends BaseSupport implements IOrderInfoService {

	@Transactional
	@Override
	public boolean submitOrderInfo(OrderInfoVO orderInfoVO) {
		String code=orderInfoVO.getOrderCode();
		boolean flag=true;
		while(flag==true){
			StringBuffer sql=new StringBuffer("select count(id) from DT_OrderInfo where code='");
			sql.append(code).append("'");
			int num=this.daoSupport.queryForInt(sql.toString());
			if(num>0){
				code=DBConstant.getRandStr();
			}else{
				flag=false;
			}
		};
		orderInfoVO.setOrderCode(code);
		
		//插入数据到订单信息表
		StringBuffer sql=new StringBuffer();
		sql.append("insert DT_OrderInfo(code,userCode,orderMoney,orderDate,discountMoney,orderState,orderPayState,recConfirmState,processState)");
		sql.append("values");
		sql.append("(");sql.append("'");
		sql.append(orderInfoVO.getOrderCode());
		sql.append("'");sql.append(",");sql.append("'");
		sql.append(orderInfoVO.getCustomerCode());
		sql.append("'");sql.append(",");sql.append("'");
		sql.append(orderInfoVO.getAllMoney());
		sql.append("'");sql.append(",");sql.append("'");
		sql.append(orderInfoVO.getOrderDate());
		sql.append("'");sql.append(",");sql.append("'");
		sql.append(orderInfoVO.getDiscountAllMoney());
		sql.append("'");sql.append(",");sql.append("1");sql.append(",");
		sql.append("0");sql.append(",");sql.append("0");
		sql.append(",");sql.append("0");
		sql.append(")");
		
		try {
			this.daoSupport.execute(sql.toString(), new Object[]{});
			
			//插入数据到订单最小销售单位表
			for (int i = 0; i < orderInfoVO.getOrderInfoVos().size(); i++) {
				OneOrderInfoVo orderInfo=orderInfoVO.getOrderInfoVos().get(i);
				
				StringBuffer sqlToOrderMinSalUnit=new StringBuffer();
				sqlToOrderMinSalUnit.append("insert DT_OrderMinSaleUnitDetail(code,orderCode,productSerialNo,minSaleUnitCode,consumeType,minSaleUnitPrice,year,picPath,isSoft)");
				sqlToOrderMinSalUnit.append("values");
				sqlToOrderMinSalUnit.append("(");
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(DBUtils.generateCode(DBConstant.ORD_MINSALEUNIT_INFO_CODE));
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(",");
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(orderInfoVO.getOrderCode());
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(",");
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(orderInfo.getSerial());
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(",");
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(orderInfo.getMinSaleUnitCode());
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(",");
				sqlToOrderMinSalUnit.append(orderInfo.getType());
				sqlToOrderMinSalUnit.append(",");
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(Double.parseDouble(orderInfo.getPrice()));
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(",");
				sqlToOrderMinSalUnit.append(orderInfo.getYear());
				sqlToOrderMinSalUnit.append(",");
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(orderInfo.getPicPath());
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(",");
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(orderInfo.getIsSoft());
				sqlToOrderMinSalUnit.append("'");
				sqlToOrderMinSalUnit.append(")");
				
				//订单最小销售单位表新增数据
				this.daoSupport.execute(sqlToOrderMinSalUnit.toString(), new Object[]{});
				
				/**
				 *提交订单成功后将数据从购物车移除
				 */
				StringBuffer delShoppingCartSql=new StringBuffer();
				delShoppingCartSql.append("delete from DT_ShoppingCart where ");
				delShoppingCartSql.append("customerCode=");
				delShoppingCartSql.append("'");
				delShoppingCartSql.append(orderInfoVO.getCustomerCode());
				delShoppingCartSql.append("'");
				delShoppingCartSql.append(" and minSaleUnitCode=");
				delShoppingCartSql.append("'");
				delShoppingCartSql.append(orderInfo.getMinSaleUnitCode());
				delShoppingCartSql.append("'");				
				this.daoSupport.execute(delShoppingCartSql.toString(), new Object[]{});
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	
	@Override
	public boolean updatePayState(String orderCode) {
		StringBuffer sql=new StringBuffer();
		sql.append("update DT_OrderInfo set orderPayState=1 where code=");
		sql.append("'");
		sql.append(orderCode);
		sql.append("'");
		this.daoSupport.execute(sql.toString(), new Object[]{});
		return true;
	}

	@Override
	public boolean removeOrder(String orderNo) {
		String sql="update DT_OrderInfo set orderState=0 where code=?";
		try {
			this.daoSupport.execute(sql, orderNo);
		} catch (Exception e) {
			return false;
		}
		return true;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Page pageOrderInfo(int orderDate, int orderState,
			CustomerInfo customerInfo,int pageSize,int pageNo) throws Exception  {
		StringBuffer orderCountSql = new StringBuffer();
		
		//获取当前系统时间
		Calendar calendar=Calendar.getInstance();
		//月份减1
		calendar.add(Calendar.MONTH, -1);
		//转换时间格式
		String ly_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
		
		orderCountSql.append("select di.*,dt.name as customerName from DT_OrderInfo as di  left join DT_CustomerInfo as dt on di.userCode = dt.code where 1=1 ");
		orderCountSql.append("and di.userCode=");
		orderCountSql.append("'");
		orderCountSql.append(customerInfo.getCode());
		orderCountSql.append("'");
		
		StringBuffer pageSql = new StringBuffer();
		pageSql.append("select * from (").append("select * from ( select di.*,dt.name as customerName from DT_OrderInfo as di  left join DT_CustomerInfo as dt on di.userCode = dt.code where di.userCode=? ");
		
		if (orderState < 2) {
			orderCountSql.append(" and di.orderState = ").append(orderState);
			pageSql.append(" and di.orderState = ").append(orderState);
		}
		if (orderDate==0) {
			orderCountSql.append(" and di.orderDate >= '").append(ly_time).append("'");
			pageSql.append(" and di.orderDate >= '").append(ly_time).append("'");
		}else {
			orderCountSql.append(" and di.orderDate <= '").append(ly_time).append("'");
			pageSql.append(" and di.orderDate <= '").append(ly_time).append("'");
		}
		
		pageSql.append(" ) tb");
		
		pageSql.append(") ta").append(" inner join ").append("(select dom.isSoft,dom.picPath,pt.name as proName,dom.code as detailCode,dom.orderCode,dom.productSerialNo,dom.consumeType,dom.minSaleUnitPrice,dom.year,dmm.name as saleName " +
				" ,dom.minSaleUnitCode from DT_OrderMinSaleUnitDetail dom  join DT_MinSaleUnitMemo dmm on dom.minSaleUnitCode = dmm.minSaleUnitCode and dmm.languageCode=?" +
				" left join DT_ProductInfo pi on dom.productSerialNo=pi.serialNo left join DT_ProductForSealer pt on pt.code=pi.proTypeCode where dom.isSoft='1') tc on ta.code = tc.orderCode order by orderDate desc");
		
		List<OrderInfoVo> list = this.daoSupport.queryForList(orderCountSql.toString(), OrderInfoVo.class);
		
		List<OrderInfoVo> orderInfoVos = this.daoSupport.queryForList(pageSql.toString(), OrderInfoVo.class,customerInfo.getCode(),customerInfo.getLanguageCode());
		
		StringBuffer pageSql2 = new StringBuffer();
		pageSql2.append("select * from (").append("select * from ( select di.*,dt.name as customerName from DT_OrderInfo as di  left join DT_CustomerInfo as dt on di.userCode = dt.code where di.userCode=? ");
		
		if (orderState < 2) {
			pageSql2.append(" and di.orderState = ").append(orderState);
		}
		if (orderDate==0) {
			pageSql2.append(" and di.orderDate >= '").append(ly_time).append("'");
		}else {
			pageSql2.append(" and di.orderDate <= '").append(ly_time).append("'");
		}
		
		pageSql2.append(" ) tb");
		
		pageSql2.append(") ta")
				.append(" inner join ")
				.append("(select dom.isSoft,dom.picPath,pt.name as proName,dom.code as detailCode,dom.orderCode," +
						"dom.productSerialNo,dom.consumeType,dom.minSaleUnitPrice,dom.year,sc.name as saleName ," +
						"dom.minSaleUnitCode from dt_orderMinSaleUnitDetail dom LEFT join  dt_saleConfig sc on " +
						"dom.minSaleUnitCode=sc.code left join DT_ProductInfo pi on dom.productSerialNo=pi.serialNo " +
						"left join DT_ProductForSealer pt on pt.code=pi.proTypeCode where dom.isSoft='0') tc on ta.code = tc.orderCode order by orderDate desc");
		
		
		List<OrderInfoVo> ordercfgInfoVos = this.daoSupport.queryForList(pageSql2.toString(), OrderInfoVo.class,customerInfo.getCode());
		
		List<OrderInfoVo> orderList=new ArrayList<OrderInfoVo>();
		orderList.addAll(orderInfoVos);
		orderList.addAll(ordercfgInfoVos);
		
		List<MyOrderPageData> datas=this.convertPageData(orderList);
		List<MyOrderPageData> pageDatas=new ArrayList<MyOrderPageData>();
		for (int i = 0; i < datas.size(); i++) {
			if(i >= ((pageNo-1)*pageSize) && i<=(((pageNo-1)*pageSize+pageSize)-1)){
				pageDatas.add(datas.get(i));
			}
		}
		Page page =new Page(pageNo, datas.size(), pageSize, pageDatas);
		
		return page;
	}

	/**
	 * 
	* @Title: convertPageData
	* @Description: 将查询结果转换为可控显示的分页数据
	* @param    
	* @return List<MyOrderPageData>    
	* @throws
	 */
	@SuppressWarnings("unchecked")
	public List<MyOrderPageData> convertPageData(List<OrderInfoVo> orderInfoVos) throws Exception{
		
		List<MyOrderPageData> result = new ArrayList<MyOrderPageData>();
		
		Map orderMap = new HashMap();//订单
		Map detailMap = new HashMap();//订单产品
		Map minSaleUnitDetailMap = new HashMap();//订单产品最小销售单位
		
		for(OrderInfoVo v : orderInfoVos){
			String code = v.getCode();
			if(!orderMap.containsKey(code)){
				OrderInfo info = new OrderInfo();
				BeanUtils.copyProperties(v, info);//对象copy
				orderMap.put(code, info);
			}
			
			/**
			 *订单最小销售单位
			 */
			String orderMinSaleUnitCode = v.getDetailCode();
			
			if (!minSaleUnitDetailMap.containsKey(orderMinSaleUnitCode)) {
				OrderMinSaleUnitDetail detail = new OrderMinSaleUnitDetail();
				BeanUtils.copyProperties(v, detail);
				minSaleUnitDetailMap.put(orderMinSaleUnitCode, detail);
			}
			
			String proSerialNoOrderCode = v.getProductSerialNo()+"-"+v.getOrderCode();//序列号+订单code
			
			if (!detailMap.containsKey(proSerialNoOrderCode)) {
				OrderProductPageData data = new OrderProductPageData();
				data.setOrderCode(code);
				data.setProName(v.getProName());
				data.setProSerialNo(v.getProductSerialNo());
				data.setPicPath(v.getPicPath());
				detailMap.put(proSerialNoOrderCode, data);
			}
			
					
		}	
		Set keys = orderMap.keySet();
		for (Object key : keys) {
			MyOrderPageData myOrderPageData = new MyOrderPageData();
			OrderInfo orderInfo = (OrderInfo) orderMap.get(key);
			
			List<OrderProductPageData> orderProductPageDatas = new ArrayList<OrderProductPageData>();
			Set prokeys = detailMap.keySet();
			for (Object prokey : prokeys) {
				OrderProductPageData data=(OrderProductPageData)detailMap.get(prokey);
				
				List<OrderMinSaleUnitDetail> minSaleUnitlList = new ArrayList<OrderMinSaleUnitDetail>();
				Set minSaleUnitkeys = minSaleUnitDetailMap.keySet();
				for (Object minSaleUnitkey : minSaleUnitkeys) {
					OrderMinSaleUnitDetail minSaleUnit=(OrderMinSaleUnitDetail)minSaleUnitDetailMap.get(minSaleUnitkey);
					if(data.getOrderCode().equals(minSaleUnit.getOrderCode())&& data.getProSerialNo().equals(minSaleUnit.getProductSerialNo())){
						minSaleUnitlList.add(minSaleUnit);
					}
				}
				data.setMinSaleUnitDetails(minSaleUnitlList);
				
				if(data.getOrderCode().equals(orderInfo.getCode())){
					orderProductPageDatas.add(data);
				}
				
			}
			myOrderPageData.setOrderInfo(orderInfo);
			myOrderPageData.setDetails(orderProductPageDatas);
			result.add(myOrderPageData);
		}
		ComparatorUser comparator=new ComparatorUser();
		Collections.sort(result, comparator);
		
		List<MyOrderPageData> result2 = new ArrayList<MyOrderPageData>();
		
		for (int i = 0; i < result.size(); i++) {
			MyOrderPageData data=result.get(i);
			Date date=new SimpleDateFormat("yyyy-MM-dd").parse(data.getOrderInfo().getOrderDate());
			
			data.getOrderInfo().setOrderDate(new SimpleDateFormat("MM/dd/yyyy").format(date)); 
			result2.add(data);
		}
		return result2;
	}

	/**
	 * 集合排序
	 * 
	 * @author temp
	 * 
	 */
	public class ComparatorUser implements Comparator {

		public int compare(Object arg0, Object arg1) {
			MyOrderPageData order0 = (MyOrderPageData) arg0;
			MyOrderPageData order1 = (MyOrderPageData) arg1;

			// 按订单时间降序排序
			int flag = order1.getOrderInfo().getOrderDate()
					.compareTo(order0.getOrderInfo().getOrderDate());
			return flag;
		}
	}

	@Override
	public int queryOrderIsNotPay(String customerCode) {
		StringBuffer s=new StringBuffer();
		s.append("select count(*) from dt_orderinfo where userCode=");
		s.append("'");
		s.append(customerCode);
		s.append("'");
		s.append(" and orderPayState=0 and orderState=1");
		
		int num=this.daoSupport.queryForInt(s.toString(), new Object[]{});
		return num;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<MyOrderPageData> queryOrderDetailInfoByOrderCode(String orderCode,String languageCode) throws Exception {
		
		List<OrderInfoVo> orderInfoVos=new ArrayList<OrderInfoVo>();
		StringBuffer pageSql = new StringBuffer();
		pageSql.append("select * from (select * from ( select di.*,dt.name as customerName from DT_OrderInfo di left join " +
				"DT_CustomerInfo as dt on di.userCode = dt.code where  di.code=");
		pageSql.append("'");
		pageSql.append(orderCode);
		pageSql.append("'");
		pageSql.append(" ) tb  )ta inner join (select dom.picPath, pt.code as proCode,pt.name as proName,dom.code as  detailCode,dom.orderCode,dom.productSerialNo," +
				"dom.consumeType,dom.minSaleUnitPrice,dom.year, dmm.name as saleName,dmm.minSaleUnitCode as minSaleUnitCode " +
				"from DT_OrderMinSaleUnitDetail dom left join DT_MinSaleUnitMemo dmm on dom.minSaleUnitCode = dmm.minSaleUnitCode " +
				"and dmm.languageCode=");
		pageSql.append("'");
		pageSql.append(languageCode);
		pageSql.append("'");
		pageSql.append("left join DT_ProductInfo pi on dom.productSerialNo=pi.serialNo left join DT_ProductType pt " +
				"on pt.code=pi.proTypeCode where dom.isSoft='1') tc  on ta.code = tc.orderCode");
		
		orderInfoVos.addAll(this.daoSupport.queryForList(pageSql.toString(), OrderInfoVo.class,new Object[]{}));
		
		
		StringBuffer sql = new StringBuffer();
		sql.append("select * from (select * from ( select di.*,dt.name as customerName from DT_OrderInfo di left join " +
				"DT_CustomerInfo as dt on di.userCode = dt.code where  di.code=");
		sql.append("'");
		sql.append(orderCode);
		sql.append("'");
		sql.append(") tb  ) ta inner join (select dom.picPath,pt.code as proCode,pt.name as proName,dom.code as  detailCode,dom.orderCode," +
				"dom.productSerialNo,dom.consumeType,dom.minSaleUnitPrice,dom.year, sc.name as saleName,sc.code as minSaleUnitCode from " +
				"DT_OrderMinSaleUnitDetail dom left join dt_saleConfig sc on dom.minSaleUnitCode = sc.code left join DT_ProductInfo pi on " +
				"dom.productSerialNo=pi.serialNo left join DT_ProductType pt on pt.code=pi.proTypeCode  where dom.isSoft='0') tc  " +
				"on ta.code = tc.orderCode");
		
		orderInfoVos.addAll(this.daoSupport.queryForList(sql.toString(), OrderInfoVo.class,new Object[]{}));

		return this.convertPageData(orderInfoVos);
		
	}


	@Override
	public boolean updateOrderPayState(String orderCode) {
		//获取当前系统时间
		Calendar calendar=Calendar.getInstance();
		//转换时间格式
		String ly_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
		StringBuffer sql=new StringBuffer();
		sql.append("update dt_orderInfo set orderPayState=");
		sql.append("'");
		sql.append(FrontConstant.ORDER_PAY_STATE_YES);//修改订单支付状态为已支付
		sql.append("'");
		sql.append(",payDate=");
		sql.append("'");
		sql.append(ly_time);//订单支付时间
		sql.append("'");
		sql.append(" where code=");
		sql.append("'");
		sql.append(orderCode);//订单支付时间
		sql.append("'");
		
		this.daoSupport.execute(sql.toString(), new Object[]{});
		return true;
	}


	@Override
	public OrderInfo queryOrderInfoByCode(String code) {
		String sql="select * from dt_orderInfo where code=?";
		return (OrderInfo) this.daoSupport.queryForObject(sql, OrderInfo.class, code);
	}
	
	/**
	 * 修改订单信息
	 * @param orderInfo
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public int updateOrderInfo(OrderInfo orderInfo) throws Exception {
		this.daoSupport.update("DT_OrderInfo", orderInfo, "code='"+orderInfo.getCode()+"'");
		return 0;
	}


	@Override
	public OrderMinSaleUnitDetail queryOrderInfoByInfo(String serialNo, int consumeType,
			String minSaleUnitCode, String userCode) {
		StringBuffer sql=new StringBuffer();
		sql.append("select m.* from dt_orderinfo o,DT_OrderMinSaleUnitDetail m where " +
				"o.code=m.orderCode and o.orderPayState='1'");
		sql.append(" and m.consumeType=");
		sql.append("'");
		sql.append(consumeType);
		sql.append("'");
		sql.append(" and m.productSerialNo=");
		sql.append("'");
		sql.append(serialNo);
		sql.append("'");
		sql.append(" and m.minSaleUnitCode=");
		sql.append("'");
		sql.append(minSaleUnitCode);
		sql.append("'");
		sql.append(" and o.userCode=");
		sql.append("'");
		sql.append(userCode);
		sql.append("'");
		
		OrderMinSaleUnitDetail  orderMinSaleUnitDetail=(OrderMinSaleUnitDetail)this.daoSupport.queryForObject(sql.toString(), OrderMinSaleUnitDetail.class, new Object[]{});
		return orderMinSaleUnitDetail;
	}

	
}
