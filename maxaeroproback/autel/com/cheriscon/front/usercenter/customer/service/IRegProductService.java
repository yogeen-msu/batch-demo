package com.cheriscon.front.usercenter.customer.service;

import java.util.List;

import com.cheriscon.common.model.CustomerProInfo;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.vo.RegProductAcountInfoVO;

/**
 * 注册产品业务逻辑实现接口
 * @author caozhiyong
 * @version 2013-1-8
 */
public interface IRegProductService {

	/**
	 * 查询产品信息
	 * @param regProductInfoVO
	 * @return
	 */
	public List<ProductInfo> queryProductInfo(RegProductAcountInfoVO regProductInfoVO);
	
	/**
	 * 注册产品信息
	 * @param regProductInfoVO
	 * @return
	 */
	public boolean regProductInfo(RegProductAcountInfoVO regProductInfoVO);
	
	
	/**
	 *  注册成功后把产品对应的标准出厂软件配置（最小销售单位）写到产品软件效状态表中
	 * @param usercode
	 * @param regProductInfoVO
	 */
	public boolean addProductSoftwareValidStatus(String usercode,RegProductAcountInfoVO regProductInfoVO);
	
	
	/**
	 * 根据产品序列号和autelID判断是否对应
	 *  @param autelId
	 *  @param proSerialNo
	 */
	public int checkProduct(String autelId,String proSerialNo);
	
	/**
	 * 根据产品序列号和autelID判断是否对应
	 *  @param autelId
	 *  @param proSerialNo
	 */
	public ProductInfo queryProduct(String proSerialNo);
	
	//根据产品序列号获取产品信息和经销商20140213
	public ProductInfo queryProductAndSealer(String proSerialNo);
	
	/**
	 * 
	 * 
	 *  @param code
	 */
	public ProductInfo queryProductByCode(String code);
	
	/**
	 * 移除注册产品信息
	 * @param regProductInfoVO
	 * @return
	 */
	public boolean removeProductInfo(String proCode);
	public boolean removeProductInfo(String proCode,String operateUser,String ip);
	
	
	/**
	 * 查询产品是否已经关联用户
	 * @param proCode
	 * @return
	 */
	public CustomerProInfo CustomerProductNum(String proCode);
	
	/**
	 * 插入用户与产品关联信息表
	 * @param regProductInfoVO
	 * @return
	 */
	public boolean insertCustProInfo(CustomerProInfo custProInfo);
	
	
	public boolean changeCustProInfo(String userCode,RegProductAcountInfoVO regProductInfoVO);
	
	public boolean addProductSoftwareValidStatus(String usercode,String validDate,String regDate,
			RegProductAcountInfoVO regProductInfoVO);
	
	public boolean addProductSoftwareValidStatus(String usercode,String validDate,String regDate,
			String proDate,RegProductAcountInfoVO regProductInfoVO);
	/**
	 * 根据产品序列号查询产品
	 * @param serialNo
	 * @return
	 */
	public ProductInfo queryProductBySerialNo(String serialNo);
	/**
	 * 20160428 A16043 hlh  
	 * 通过飞机序列号查询对应的主序列号
	 *  @param aricraftSerialNumber
	 */
	public ProductInfo queryProductByAricraftSerialNumber(String aricraftSerialNumber);
	/**
	 * 20161208 A16043 hlh  
	 * 通过飞机序列号查询飞机的注册AutelID
	 *  @param aricraftSerialNumber
	 */
	public String queryRegAutelIDByAricraftSerialNumber(String aricraftSerialNumber);
}
