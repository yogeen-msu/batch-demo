package com.cheriscon.front.usercenter.customer.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.OrderInfo;
import com.cheriscon.common.model.OrderMinSaleUnitDetail;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.vo.MyOrderPageData;
import com.cheriscon.front.usercenter.customer.vo.OrderInfoVO;

/**
 * 订单业务逻辑实现接口
 * @author caozhiyong
 * @version 2013-1-17
 */
public interface IOrderInfoService {

	/**
	 * 提交订单
	 * @param orderInfoVO
	 * @return
	 */
	public boolean submitOrderInfo(OrderInfoVO orderInfoVO);
	
	/**
	 * 修改订单支付状态
	 * @param orderCode
	 * @return
	 */
	public boolean updatePayState(String orderCode);
	
	/**
	 * 分页查询订单数据
	 * @param orderDate
	 * @param orderState
	 * @param customerInfo
	 * @return
	 */
	public Page pageOrderInfo(int orderDate,int orderState,CustomerInfo customerInfo,int pageSize,int pageNo) throws Exception ;
	
	/**
	 * 取消订单
	 * @param orderNo
	 * @return
	 */
	public boolean removeOrder(String orderNo);
	
	/**
	 * 查询未支付的订单数量
	 */
	public int queryOrderIsNotPay(String customerCode);
	
	/**
	 * 查看订单详情
	 * @param orderCode
	 * @return
	 */
	public List<MyOrderPageData> queryOrderDetailInfoByOrderCode(String orderCode,String languageCode)throws Exception;
	
	/**
	 * 修改订单支付状态
	 * @param orderCode
	 * @return
	 */
	public boolean updateOrderPayState(String orderCode);
	
	/**
	 * 查询订单
	 * @param code
	 * @return
	 */
	public OrderInfo queryOrderInfoByCode(String code);
	
	/**
	 * 修改订单信息
	 * @param orderInfo
	 * @throws Exception
	 */
	public int updateOrderInfo(OrderInfo orderInfo) throws Exception;
	
	/**
	 * 查询订单信息
	 * @param serialNo
	 * @param consumeType
	 * @param minSaleUnitCode
	 * @param userCode
	 * @return
	 */
	public OrderMinSaleUnitDetail queryOrderInfoByInfo(String serialNo,int consumeType,String minSaleUnitCode,String userCode);
}
