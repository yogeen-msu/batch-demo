package com.cheriscon.front.usercenter.customer.service;

import com.cheriscon.front.user.vo.SystemErrorLog;


/**
 * 记录系统错误日志
 * @author chenqichuan
 * @version 2013-12-31
 */
public interface ISystemErrorLogService {
	
	/**
	 * 插入系统错误日志
	 * @param type 操作模块  errorMsg错误信息
	 */
	public void saveLog(String type,String errorMsg)throws Exception;
}
