package com.cheriscon.front.usercenter.customer.service.impl;

import org.springframework.stereotype.Service;

import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.model.mapper.BuyMinSaleUnitDetailMapper;
import com.cheriscon.front.usercenter.customer.service.IBuySoftService;

/**
 * 用户产品可购买软件业务逻辑实现类
 * @author caozhiyong
 * @version 2013-1-11
 */
@Service
public class BuySoftServiceImpl extends BaseSupport<Object> implements IBuySoftService {

	@Override
	public Page queryBuySoftForPage(String saleContractCode, String proCode,
			int pageNo, int pageSize,String languageCode) {
		StringBuffer sql=new StringBuffer();
		sql.append("select p.id,m.minSaleUnitCode,s.areaCfgCode,u.memo,u.name,u.languageCode,p.price" +
				",u.memoDetail,mu.picpath from DT_MinSaleUnitSaleCfgDetail m left join DT_SaleContract s " +
				"on m.saleCfgCode=s.maxSaleCfgCode left join DT_MinSaleUnitMemo u " +
				"on m.minSaleUnitCode=u.minSaleUnitCode left join DT_MinSaleUnitPrice p " +
				"on m.minSaleUnitCode=p.minSaleUnitCode and s.areaCfgCode=p.areaCfgCode " +
				"left join DT_MinSaleUnit mu on mu.code=u.minSaleUnitCode where m.minSaleUnitCode not in(select p.minSaleUnitCode from  DT_ProductSoftwareValidStatus p " +
				"where p.proCode=");
		sql.append("'");
		sql.append(proCode);
		sql.append("'");
		sql.append(")");
		sql.append(" and s.code=");
		sql.append("'");
		sql.append(saleContractCode);
		sql.append("'");
		sql.append("and u.languageCode=");
		sql.append("'");
		sql.append(languageCode);
		sql.append("'");
		sql.append("order by p.id desc");
		
		Page page=this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, new BuyMinSaleUnitDetailMapper(),  new Object[]{});
		
		return page;
	}

}
