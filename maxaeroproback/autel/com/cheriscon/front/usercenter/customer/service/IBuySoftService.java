package com.cheriscon.front.usercenter.customer.service;

import com.cheriscon.framework.database.Page;

/**
 * 用户产品可购买软件业务逻辑实现接口
 * @author caozhiyong
 * @version 2013-1-11
 */
public interface IBuySoftService {

	/**
	 * 分页查询用户产品可购买软件
	 * @param saleContractCode
	 * @param proCode
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page queryBuySoftForPage(String saleContractCode,String proCode,int pageNo, int pageSize,String languageCode);
}
