package com.cheriscon.front.usercenter.customer.service;

import com.cheriscon.framework.database.Page;

/**
 * 自购软件查询实现接口
 * @author caozhiyong
 * @version 2013-1-10
 */
public interface IBuySoftRentService {
	/**
	 * 查询用户产品自购软件
	 * @param saleContractCode
	 * @param proCode
	 * @return
	 */
	public Page queryBuySoftRent(String saleContractCode,String proCode,int pageNo, int pageSize,String languageCode);

	/**
	 * 查询用户已过期软件数量
	 * @param customerCode
	 * @return
	 */
	public int querySoftIsExpiring(String customerCode);
}
