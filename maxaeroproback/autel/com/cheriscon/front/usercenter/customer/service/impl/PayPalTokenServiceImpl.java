package com.cheriscon.front.usercenter.customer.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.PayPalToken;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.front.usercenter.customer.service.IPayPalTokenService;

@Service
public class PayPalTokenServiceImpl  extends BaseSupport<PayPalToken> implements IPayPalTokenService {

	@Override
	public boolean savePayPalToken(PayPalToken payPalToken) {
		payPalToken.setCreateTime(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		this.daoSupport.insert("DT_payPalToken", payPalToken);
		return true;
	}

	@Override
	public PayPalToken getPayPalToken(String secureTokenId) {
		String sql = "select * from DT_payPalToken where secureTokenId='"+secureTokenId+"'";
		return this.daoSupport.queryForObject(sql, PayPalToken.class);
	}

}
