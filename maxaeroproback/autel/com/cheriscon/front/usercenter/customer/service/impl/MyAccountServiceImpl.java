package com.cheriscon.front.usercenter.customer.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cheriscon.backstage.product.service.ISoftwareVersionService;
import com.cheriscon.backstage.system.service.ISoftListService;
import com.cheriscon.common.model.SoftList;
import com.cheriscon.common.model.SoftwareVersion;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.MyProduct;
import com.cheriscon.front.usercenter.customer.service.IMyAccountService;

/**
 * date 2013-1-5
 * 个人客户我的产品信息实现类
 * @author caozhiyong 
 */
@Service
public class MyAccountServiceImpl extends BaseSupport<MyProduct> implements
		IMyAccountService {

	@Resource
	private ISoftwareVersionService softwareVersionService;
	@Resource
	private ISoftListService softListService;
	
	
	@SuppressWarnings("deprecation")
	@Override
	public Page queryProducts(String remark,String usercode, int pageNo, int pageSize)
			throws Exception {
		StringBuffer sql = new StringBuffer();

		/**
		 * 根据usercode查询DT_CustomerProInfo，
		 * DT_CustomerProInfo和DT_ProductInfo与DT_ProductType 三表级联 
		 * 根据产品信息对应的销售契约查询销售配置的有效期
		 */		
		sql.append(" select  p.code as proCode, " + 
				   " (select code from dt_rgainfo where serialNo=p.serialNo and (date_format(closedate,'%Y-%m-%d')<date_format(DATE_ADD(NOW(),INTERVAL 20 DAY),'%Y-%m-%d') or closedate is null)  order by id desc limit 0,1) as rgaFlag,"+
                   " (select distinct w.validDate from DT_MinSaleUnitSaleCfgDetail m ,DT_ProductSoftwareValidStatus w "+  
                   " where m.minSaleUnitCode=w.minSaleUnitCode and m.saleCfgCode=sc.code and  w.proCode=p.code "+
                   " ) as validDate, "+
                   " p.id,p.serialNo as proSerialNo,p.aricraftSerialNumber as aricraftSerialNumber,p.externInfo6 as externInfo6,p.regTime as proRegTime,p.proDate,trim(p.saleContractCode) as saleContractCode,p.proTypeCode,p.regStatus,pt.name as proTypeName,pt.picPath,sct.warrantyMonth as warrantymonth "+
                   " from   "+
                   " DT_ProductInfo p  "+
                   " left join DT_SaleContract sct on p.saleContractCode=sct.code  "+
                   " left join DT_SaleConfig sc on  sc.code=sct.saleCfgCode  "+
                   " left join DT_CustomerProInfo cpi on p.code=cpi.proCode  "+
                   " inner join DT_ProductForSealer pt on p.proTypeCode=pt.code " +
                   " where cpi.customerCode =");
		sql.append("'");
		sql.append(usercode);
		sql.append("'");
		sql.append(" and p.regStatus=");
		sql.append("'");
		sql.append(FrontConstant.PRODUCT_REGSTATUS_YES);
		sql.append("'");
		if(!StringUtil.isEmpty(remark)){
			sql.append(" and (p.remark like '%").append(remark.trim()).append("%' or p.serialNo like '%").append(remark.trim()).append("%')");
		}
		
		sql.append(" order by regTime desc");
		
		List<MyProduct> myProducts=this.daoSupport.queryForList(sql.toString(), MyProduct.class,  new Object[]{});
		
		List<MyProduct> products=new ArrayList<MyProduct>();
		for (int i = 0; i < myProducts.size(); i++) {
			if(i >= ((pageNo-1)*pageSize) && i<=(((pageNo-1)*pageSize+pageSize)-1)){
				MyProduct myProduct=myProducts.get(i);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				
				try {
					
					//1.查询当前系统上所有的软件版本
					List<SoftwareVersion> listVersion =softwareVersionService.querySoftVersionByProCode(myProduct.getProCode());
					//2.查询客户机器上所有的软件版本
					List<SoftList> softList=softListService.getSoftListByProductSn(myProduct.getProSerialNo());
					//3.比较当前版本和历史版本是否相等
					String isUpgradeFlag=compareVersion(listVersion,softList);
					myProduct.setIsUpgradeFlag(isUpgradeFlag);
					Date regTime = sdf.parse(myProduct.getProRegTime());
					if (myProduct.getValidDate() != null) {
						Date validDate = sdf.parse(myProduct.getValidDate());
						myProduct.setValidDate(new SimpleDateFormat("MM/dd/yyyy").format(validDate));
					}
					Date proDate = sdf.parse(myProduct.getProDate());
					int Warrantymonth=Integer.parseInt(myProduct.getWarrantymonth());
					myProduct.setProRegTime(new SimpleDateFormat("MM/dd/yyyy").format(regTime));
					myProduct.setProDate(new SimpleDateFormat("MM/dd/yyyy").format(proDate));
				//	myProduct.setPeriod(new SimpleDateFormat("dd/MM/yyyy").format(period));
				     
					// 前台用户注册产品时，如果出厂日期在当前日期的三个月以内，按注册日期开始算保修期，如果在三个月以外，按出厂日期加三个月算保修期。
					// 硬件保修期=保修开始日期+契约的硬件保修期（月）
					Date period =DateUtil.toDate(myProduct.getProRegTime(), "MM/dd/yyyy");
//					int num=compareDate(myProduct.getProDate(),myProduct.getProRegTime());
//					if(num>0){
//						period.setMonth(period.getMonth()+Warrantymonth);
//					}else{
//						period=DateUtil.toDate(myProduct.getProDate(), "MM/dd/yyyy");
//						period.setMonth(period.getMonth()+Warrantymonth+3);
//					}
					if(DateUtil.compareDate(DateUtil.toDate(myProduct.getProDate(), "MM/dd/yyyy"), DateUtil.toDate(myProduct.getProRegTime(), "MM/dd/yyyy"))){
						period.setMonth(period.getMonth()+Warrantymonth);
					}else{
						period=DateUtil.toDate(myProduct.getProDate(), "MM/dd/yyyy");
						period.setMonth(period.getMonth()+Warrantymonth+3);
					}
					myProduct.setPeriod(new SimpleDateFormat("MM/dd/yyyy").format(period));
					
					//即将在三个月过期的产品，显示购买升级按钮，否则显示产品列表界面
					if (myProduct.getValidDate() != null) {
						Date renewDate =DateUtil.toDate(myProduct.getValidDate(), "MM/dd/yyyy");
						Date currenDate=DateUtil.toDate(new SimpleDateFormat("MM/dd/yyyy").format(new Date()),"MM/dd/yyyy");
						currenDate.setMonth(currenDate.getMonth()+3);
						
						
						if(DateUtil.compareDate2(renewDate, currenDate)){
							myProduct.setRenewButton("1");  //显示功能软件列表
						}else{
							myProduct.setRenewButton("0");  //显示续租
						}
					}
					
					
				
				} catch (ParseException e) {
					e.printStackTrace();
				}
				products.add(myProduct);
			}
		}
		
		Page page =new Page(pageNo, myProducts.size(), pageSize, products);

		return page;
	}

	
	//比较两个软件版本
	public String compareVersion(List<SoftwareVersion> versionList,List<SoftList> softList){
		String isUpgradeFlag="0";
		if(versionList.size()>softList.size()){
			isUpgradeFlag="1";
		}else{
			for(int i=0;i<versionList.size();i++){
				SoftwareVersion softVersion=versionList.get(i);
				for(int j=0;j<softList.size();j++){
					SoftList soft=softList.get(j);
					if(soft.getSoftCode().equals(softVersion.getCode()) &&
							!softVersion.getVersionName().equals(soft.getSoftVersion()))
					
					{
						isUpgradeFlag="1";
						break;
					}
				}
				
			}
			
			
		}
		
		return isUpgradeFlag ;
		
	}
	
	@Override
	public int queryProductCount(String userCode) throws Exception {
		StringBuffer sql=new StringBuffer();
		sql.append("select count(cp.id) from DT_CustomerProInfo cp left join DT_ProductInfo p on cp.proCode=p.code where cp.customerCode=");
		sql.append("'");
		sql.append(userCode);
		sql.append("'");
		sql.append("and p.regStatus!=");
		sql.append("'");
		sql.append(FrontConstant.PRODUCT_REGSTATUS_UNBINDED);
		sql.append("'");
		
		return this.daoSupport.queryForInt(sql.toString(), new Object[]{});
	}
	@Override
	public List<MyProduct> queryProductByCode(String userCode) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select p.code as proCode, p.serialNo as proSerialNo,pt.name  as proTypeName,date_format(pd.associatesDate,'%m/%d/%Y') as proRegTime  from DT_CustomerProInfo cp left join DT_ProductInfo p on cp.proCode=p.code " +
				   " left join DT_ProductForSealer pt on p.proTypeCode=pt.code " +
				   " left join DT_ProductAssociatesDate pd on p.code=pd.proCode " +
				   " where cp.customerCode=");
		sql.append("'");
		sql.append(userCode);
		sql.append("'");
		sql.append("and p.regStatus!=");
		sql.append("'");
		sql.append(FrontConstant.PRODUCT_REGSTATUS_UNBINDED);
		sql.append("'");
		
		List<MyProduct> list=this.daoSupport.queryForList(sql.toString(),MyProduct.class);
		return list;
	}
	
	/**
	 * 查询客户即将到期产品
	 * @param userCode
	 * @return
	 */
	public List<MyProduct> queryRenewByCode(String userCode) throws Exception{
		StringBuffer sql = new StringBuffer();
		sql.append(" select  p.code as proCode, "+
                   " (select distinct w.validDate from DT_MinSaleUnitSaleCfgDetail m ,DT_ProductSoftwareValidStatus w "+ 
                   " where m.minSaleUnitCode=w.minSaleUnitCode and m.saleCfgCode=sc.code and  w.proCode=p.code and DATE_ADD(w.validDate,INTERVAL -30 DAY)< NOW() "+
                   " ) as validDate, "+
                   " p.id,p.serialNo as proSerialNo,p.regTime as proRegTime,p.proDate,p.saleContractCode,p.regPwd as proRegPwd,p.proTypeCode,p.noRegReason,p.regStatus,pt.name as proTypeName,pt.picPath,sct.warrantyMonth as warrantymonth "+
                   " from   "+
                   " DT_ProductInfo p  "+
                   " left join DT_SaleContract sct on p.saleContractCode=sct.code  "+
                   " left join DT_SaleConfig sc on  sc.code=sct.saleCfgCode  "+
                   " left join DT_CustomerProInfo cpi on p.code=cpi.proCode  "+
                   " inner join DT_ProductForSealer pt on p.proTypeCode=pt.code " +
                   " where cpi.customerCode =");
		sql.append("'");
		sql.append(userCode);
		sql.append("'");
		sql.append(" and p.regStatus=");
		sql.append("'");
		sql.append(FrontConstant.PRODUCT_REGSTATUS_YES);
		sql.append("'");
		sql.append("order by regTime desc");
		
		List<MyProduct> list=this.daoSupport.queryForList(sql.toString(),MyProduct.class);
		List<MyProduct> products=new ArrayList<MyProduct>();
		String date = DateUtil.toString(new Date(), "yyyy-MM-dd");
		for (int i = 0; i < list.size(); i++) {
			MyProduct myProduct=list.get(i);
			if(myProduct.getValidDate()!=null){
				if(DateUtil.compareDate2(DateUtil.toDate(myProduct.getValidDate(), "yyyy-MM-dd"), DateUtil.toDate(date, "yyyy-MM-dd"))){
					myProduct.setOverdueFlag("0");
				}else{
					myProduct.setOverdueFlag("1");	
				}
				products.add(myProduct);
			}
		}
		
		return products;
	}
	
	public int queryCustomerByProCode(String proCode) throws Exception{
		StringBuffer sql=new StringBuffer();
		sql.append("select count(cp.id) from DT_CustomerProInfo cp where cp.proCode=?");
		return this.daoSupport.queryForInt(sql.toString(), proCode);
	}
	
	public int  compareDate(String s1,String s2){
		java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd");
		java.util.Calendar c1=java.util.Calendar.getInstance();
		java.util.Calendar c2=java.util.Calendar.getInstance();
		try {
			c1.setTime(df.parse(s1));
			c1.add(Calendar.MONTH, 3);
			c2.setTime(df.parse(s2));
			int result=c1.compareTo(c2);
			System.out.println(result);
			return  result;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
}
