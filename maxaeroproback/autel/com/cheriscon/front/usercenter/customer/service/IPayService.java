package com.cheriscon.front.usercenter.customer.service;

import com.cheriscon.common.model.PayLog;


/**
 * 支付模块实现接口
 * @author caozhiyong
 * @version 2013-3-27
 */
public interface IPayService {
	
	/**
	 * 插入支付日志
	 * @param payLog
	 */
	public void insertPayLog(PayLog payLog)throws Exception;
	
	
	/**
	 * 根据支付流水号，查询是否已经成功录入过系统，防止重复提交
	 * */
	
	public int getPayLogNumByTradeNum(String tradeNum) throws Exception;
	
}
