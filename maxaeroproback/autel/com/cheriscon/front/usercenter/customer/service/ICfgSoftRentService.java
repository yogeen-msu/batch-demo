package com.cheriscon.front.usercenter.customer.service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.front.usercenter.customer.vo.CfgSoftRentInfoVO;

/**
 * 出厂配置软件查询实现接口
 * @author caozhiyong
 * @version 2013-1-10
 */
public interface ICfgSoftRentService {

	/**
	 * 查询用户产品的出厂配置软件
	 * @param saleContractCode 销售契约code
	 * @param proCode 产品code
	 */
	public CfgSoftRentInfoVO querySoftRent(String saleContractCode,String proCode,CustomerInfo customerInfo);
	
}
