package com.cheriscon.front.usercenter.customer.service.impl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.vo.SystemErrorLog;
import com.cheriscon.front.usercenter.customer.service.ISystemErrorLogService;

@Service
public class SystemErrorLogServiceImpl extends BaseSupport<SystemErrorLog> implements ISystemErrorLogService {

	@Override
	public void saveLog(String type,String errorMsg) throws Exception {
		SystemErrorLog errorLog=new SystemErrorLog();
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		Object obj=SessionUtil.getLoginUserInfo(request);
		if(obj==null){
			errorLog.setUserCode("");
		}
		else 
			{if(obj.getClass().equals(CustomerInfo.class)){
				errorLog.setUserCode(((CustomerInfo)obj).getCode());
			}
			if(obj.getClass().equals(SealerInfo.class)){
				errorLog.setUserCode(((SealerInfo)obj).getCode());
			}
		}
		String ip=FrontConstant.getIpAddr(request);
		String createDate=DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss");
		errorLog.setOperateIp(ip);
		errorLog.setOperateTime(createDate);
		errorLog.setErrorMsg(errorMsg);
		errorLog.setType(type);
		this.daoSupport.insert("DT_SystemErrorLog", errorLog);
	}

}
