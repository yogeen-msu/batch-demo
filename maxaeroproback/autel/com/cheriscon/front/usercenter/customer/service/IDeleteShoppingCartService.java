package com.cheriscon.front.usercenter.customer.service;

import java.util.List;

/**
 * 删除我的购物车数据
 * @author caozhiyong
 * @version 2013-1-15
 */
public interface IDeleteShoppingCartService {

	/**
	 * 删除我的购物车数据
	 * @param usercode
	 * @param proSerial
	 * @param minSaleUnitCode
	 * @return
	 */
	public boolean delShoppingCart(String usercode,String proSerial,String minSaleUnitCode);
	
	/**
	 * 批量删除我的购物车数据
	 * @param usercode
	 * @param minSaleUnitCodes
	 * @return
	 */
	public boolean batchDelshoppingCart(String usercode,List<String> minSaleUnitCodes);
	
	/**
	 * 删除我的购物车数据
	 * @param usercode
	 * @param minSaleUnitCodes
	 * @return
	 */
	public boolean delshoppingCart(String usercode,String minSaleUnitCode,String proCode);
}
