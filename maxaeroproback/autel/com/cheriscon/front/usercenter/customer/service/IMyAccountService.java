package com.cheriscon.front.usercenter.customer.service;


import java.util.List;

import com.cheriscon.framework.database.Page;
import com.cheriscon.front.usercenter.customer.model.MyProduct;

/**
 * date:2013-1-5
 * 
 * @author caozhiyong
 * 
 */
public interface IMyAccountService {

	/**
	 * 
	 * @param usercode
	 *            客户code
	 * @param pagesize
	 * @return 客户所购买的所有产品信息
	 * @throws Exception
	 */
	
	public Page queryProducts(String remark,String usercode, int pageNo, int pageSize)
			throws Exception;
	
	/**
	 * 查询用户注册产品数量
	 * @param userCode
	 * @return
	 */
	public int queryProductCount(String userCode) throws Exception;
	
	/**
	 * 查询用户注册产品列表
	 * @param userCode
	 * @return
	 */
	public List<MyProduct> queryProductByCode(String userCode) throws Exception;
	
	
	/**
	 * 查询客户即将到期产品
	 * @param userCode
	 * @return
	 */
	public List<MyProduct> queryRenewByCode(String userCode) throws Exception;
	
	/**
	 * 根据产品code查询产品是否已经注册用户
	 * @param proCode
	 * @return
	 */
	public int queryCustomerByProCode(String proCode) throws Exception;
	
	
	
}
