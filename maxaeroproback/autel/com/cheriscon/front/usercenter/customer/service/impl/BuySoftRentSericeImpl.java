package com.cheriscon.front.usercenter.customer.service.impl;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.stereotype.Service;

import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.usercenter.customer.model.mapper.MinSaleUnitDetailMapper;
import com.cheriscon.front.usercenter.customer.service.IBuySoftRentService;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;

/**
 * 自购软件接口实现类
 * 
 * @author caozhiyong
 * @version 2013-1-10
 */
@Service
public class BuySoftRentSericeImpl extends BaseSupport<MinSaleUnitDetailVO>
		implements IBuySoftRentService {

	@Override
	public Page queryBuySoftRent(String saleContractCode,
			String proCode,int pageNo, int pageSize,String languageCode) {
		StringBuffer sql=new StringBuffer();
		sql.append("select s.*,m.price ,m.areaCfgCode ,u.languageCode,u.memo,u.name, u.memoDetail as memoDetail,mu.picpath,mu.upCode from " +
				"DT_ProductSoftwareValidStatus s left join " +
				"DT_MinSaleUnitPrice m on s.minSaleUnitCode=m.minSaleUnitCode " +
				"left join DT_SaleContract c on m.areaCfgCode =c.areaCfgCode " +
				"left join DT_MinSaleUnitMemo u on s.minSaleUnitCode=u.minSaleUnitCode");
		sql.append(" left join DT_MinSaleUnit mu on mu.code=u.minSaleUnitCode where s.proCode=");
		sql.append("'");
		sql.append(proCode);
		sql.append("'");
		sql.append(" and s.minSaleUnitCode not in (select DT_MinSaleUnitSaleCfgDetail.minSaleUnitCode from " +
				"DT_MinSaleUnitSaleCfgDetail left join DT_SaleContract on " +
				"DT_MinSaleUnitSaleCfgDetail.saleCfgCode = DT_SaleContract.saleCfgCode where " +
				"DT_SaleContract.code=");
		sql.append("'");
		sql.append(saleContractCode);
		sql.append("'");
		sql.append(")")	;	
		sql.append(" and c.code=");
		sql.append("'");
		sql.append(saleContractCode);
		sql.append("'");
		sql.append(" and u.languageCode=");
		sql.append("'");
		sql.append(languageCode);
		sql.append("'");
		sql.append("order by s.id desc");
		
		Page page=this.daoSupport.queryForPage(sql.toString(), pageNo, pageSize, new MinSaleUnitDetailMapper(),  new Object[]{});
		
		return page;
	}

	@Override
	public int querySoftIsExpiring(String customerCode) {
		Calendar calendar=Calendar.getInstance();
		String time=new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
		StringBuffer s=new StringBuffer();
		s.append("select count(1) from dt_productsoftwarevalidStatus s ");
		s.append("left join dt_customerProInfo p on s.proCode=p.proCode left join dt_productinfo pi on pi.code=p.proCode");
		s.append(" where s.validDate<");
		s.append("'");
		s.append(time);
		s.append("'");
		s.append(" and  p.customerCode=");
		s.append("'");
		s.append(customerCode);
		s.append("'");
		s.append(" and  pi.regStatus!=");
		s.append("'");
		s.append(FrontConstant.PRODUCT_REGSTATUS_UNBINDED);
		s.append("'");
		
		int num=this.daoSupport.queryForInt(s.toString(), new Object[]{});
		return num;
	}

}
