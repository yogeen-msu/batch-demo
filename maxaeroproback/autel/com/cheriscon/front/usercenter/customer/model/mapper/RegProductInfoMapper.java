package com.cheriscon.front.usercenter.customer.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.front.usercenter.customer.vo.RegProductDetailInfoVO;

/**
 * 注册产品详细信息
 * @author caozhiyong
 * 2013-4-2
 */
public class RegProductInfoMapper  implements RowMapper{

	@Override
	public RegProductDetailInfoVO mapRow(ResultSet arg0, int arg1) throws SQLException {
		RegProductDetailInfoVO infoVO=new RegProductDetailInfoVO();
		infoVO.setAutelId(arg0.getString("autelId"));
		infoVO.setCode(arg0.getString("code"));
		infoVO.setId(arg0.getInt("id"));
		infoVO.setNoRegReason(arg0.getString("noRegReason"));
		infoVO.setProDate(arg0.getString("proDate"));
		infoVO.setProTypeCode(arg0.getString("proTypeCode"));
		infoVO.setProTypeName(arg0.getString("proTypeName"));
		infoVO.setRegPwd(arg0.getString("regPwd"));
		infoVO.setRegStatus(arg0.getInt("regStatus"));
		infoVO.setRegTime(arg0.getString("regTime"));
		infoVO.setSaleContractCode(arg0.getString("saleContractCode"));
		infoVO.setSaleContractName(arg0.getString("saleContractName"));
		infoVO.setSealerCode(arg0.getString("sealerCode"));
		infoVO.setSerialNo(arg0.getString("serialNo"));
		return infoVO;
	}

}
