package com.cheriscon.front.usercenter.customer.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.front.usercenter.customer.vo.ShoppingCartVO;

/**
 * 购物车详细信息
 * @author caozhiyong
 * @version 2013-1-15
 */
public class ShoppingCartSaleCfgVOMapper implements RowMapper{

	@Override
	public ShoppingCartVO mapRow(ResultSet arg0, int arg1) throws SQLException {
		ShoppingCartVO shoppingCartVO=new ShoppingCartVO();
		shoppingCartVO.setId(arg0.getInt("id"));//id
		shoppingCartVO.setCustomerCode(arg0.getString("customerCode"));//客户code
		shoppingCartVO.setMinSaleUnitCode(arg0.getString("minSaleUnitCode"));//最小销售单位code
		shoppingCartVO.setAreaCfgCode(arg0.getString("areaCfgCode"));//区域配置code
		shoppingCartVO.setProName(arg0.getString("proName"));//产品名称
		shoppingCartVO.setProSerial(arg0.getString("proSerial"));//产品序列号
		shoppingCartVO.setProCode(arg0.getString("proCode"));//产品code
		shoppingCartVO.setYear(arg0.getInt("year"));//续租年限
		shoppingCartVO.setType(arg0.getInt("type"));//消费类型（0、购买，1、续租）
		shoppingCartVO.setName(arg0.getString("name")) ;//最小销售单位名称
		shoppingCartVO.setCostPrice(arg0.getString("cfgPrice"));//原价
		shoppingCartVO.setPicPath(arg0.getString("proPicPath"));
		shoppingCartVO.setValidDate(arg0.getString("validData"));
		shoppingCartVO.setIsSoft(0);
		
		shoppingCartVO.setDiscountPrice(arg0.getString("cfgPrice")) ;//折后价
		double costPrice=Double.parseDouble(shoppingCartVO.getCostPrice());
		double discountPrice=Double.parseDouble(shoppingCartVO.getDiscountPrice());
		double sparePrice=costPrice-discountPrice;
		shoppingCartVO.setSparePrice(String.valueOf(sparePrice));//节省
		
		
		return shoppingCartVO;
	}

}
