package com.cheriscon.front.usercenter.customer.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.front.usercenter.customer.model.MyProduct;

/**
 * 我的产品详情
 * @author caozhiyong
 */
public class MyProductMapper  implements RowMapper {

	@Override
	public MyProduct mapRow(ResultSet arg0, int arg1) throws SQLException {
		MyProduct myProduct=new MyProduct();
		myProduct.setProSerialNo(arg0.getString("serialNo"));
		myProduct.setProID(arg0.getInt("id"));
		myProduct.setSaleContractCode(arg0.getString("saleContractCode"));
		myProduct.setProRegPwd(arg0.getString("regPwd"));
		myProduct.setProDate(arg0.getString("proDate"));
		myProduct.setRegStatus(arg0.getInt("regStatus"));
		myProduct.setPicPath(arg0.getString("picPath"));
		myProduct.setNoRegReason(arg0.getString("noRegReason"));
		myProduct.setProTypeCode(arg0.getString("proTypeCode"));
		myProduct.setProTypeName(arg0.getString("name"));
		myProduct.setProCode(arg0.getString("code"));
		String regTime=arg0.getString("regTime");
		String validDate=arg0.getString("validDate");;
		try {
			Date date=new SimpleDateFormat("yyyy-MM-dd").parse(validDate);
			myProduct.setValidDate(DateUtil.toString(date, "yyyy/MM/dd"));
			
			Date date2=new SimpleDateFormat("yyyy-MM-dd").parse(regTime);
			myProduct.setProRegTime(DateUtil.toString(date2, "yyyy/MM/dd"));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return myProduct;
	}
	
	
}
