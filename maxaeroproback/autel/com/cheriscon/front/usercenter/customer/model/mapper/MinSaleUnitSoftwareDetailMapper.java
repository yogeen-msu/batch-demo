package com.cheriscon.front.usercenter.customer.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.common.model.MinSaleUnitSoftwareDetail;

/**
 * 最小销售单位与软件关联信息
 * @author caozhiyong
 * @version 2013-1-12
 */
public class MinSaleUnitSoftwareDetailMapper implements RowMapper {

	@Override
	public MinSaleUnitSoftwareDetail mapRow(ResultSet arg0, int arg1) throws SQLException {
		MinSaleUnitSoftwareDetail detail=new MinSaleUnitSoftwareDetail();
		detail.setId(arg0.getInt("id"));
		detail.setMinSaleUnitCode(arg0.getString("minSaleUnitCode"));
		detail.setSoftwareTypeCode(arg0.getString("softwareTypeCode"));
		return detail;
	}

}
