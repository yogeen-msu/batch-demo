package com.cheriscon.front.usercenter.customer.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitMemoPriceVO;

/**
 * 最小销售单位说明与产品出厂配置价格
 * @author caozhiyong
 * @version2013-1-10
 */
public class MinSaleUnitMemoPriceMapper implements RowMapper{

	@Override
	public MinSaleUnitMemoPriceVO mapRow(ResultSet arg0, int arg1) throws SQLException {
		MinSaleUnitMemoPriceVO memoPriceVO=new MinSaleUnitMemoPriceVO();
		memoPriceVO.setPrice(arg0.getString("reChargePrice"));
		memoPriceVO.getMinSaleUnitMemo().setId(arg0.getInt("id"));
		memoPriceVO.getMinSaleUnitMemo().setLanguageCode(arg0.getString("languageCode"));
		memoPriceVO.getMinSaleUnitMemo().setMemo(arg0.getString("memo"));
		memoPriceVO.getMinSaleUnitMemo().setMinSaleUnitcode(arg0.getString("minSaleUnitCode"));
		memoPriceVO.getMinSaleUnitMemo().setName(arg0.getString("name"));	
		memoPriceVO.setValidDate(arg0.getString("validDate"));
		memoPriceVO.setAreaCfgCode(arg0.getString("areaCfgCode"));
		memoPriceVO.setSaleCfgCode(arg0.getString("saleCfgCode"));
		memoPriceVO.setSaleCfgName(arg0.getString("saleConfigName"));
		memoPriceVO.setCfgMemo(arg0.getString("cfgMemo"));
		memoPriceVO.setCfgpicpath(arg0.getString("cfgpicpath"));
		return memoPriceVO;
	}

}
