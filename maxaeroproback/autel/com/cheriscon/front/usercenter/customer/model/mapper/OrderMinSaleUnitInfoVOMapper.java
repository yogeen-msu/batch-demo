package com.cheriscon.front.usercenter.customer.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.cheriscon.front.usercenter.customer.vo.OrderMinSaleUnitInfoVO;

/**
 * 我的订单最小销售单位信息详情
 * @author caozhiyong
 * @version 2013-1-22
 */
public class OrderMinSaleUnitInfoVOMapper  implements RowMapper{

	@Override
	public OrderMinSaleUnitInfoVO mapRow(ResultSet arg0, int arg1) throws SQLException {
		OrderMinSaleUnitInfoVO  orderInfo=new OrderMinSaleUnitInfoVO();
		orderInfo.setConsumeType(arg0.getInt("consumeType"));
		orderInfo.setDiscountMoney(arg0.getString("discountMoney"));
		orderInfo.setMinSaleUnitPrice(arg0.getString("minSaleUnitPrice"));
		orderInfo.setOrderDate(arg0.getString("orderDate"));
		orderInfo.setOrderMoney(arg0.getString("orderMoney"));
		orderInfo.setOrderPayState(arg0.getInt("orderPayState"));
		orderInfo.setOrderState(arg0.getInt("orderState"));
		orderInfo.setProductSerialNo(arg0.getString("productSerialNo"));
		orderInfo.setProName(arg0.getString("proName"));
		orderInfo.setSoftName(arg0.getString("softName"));
		orderInfo.setOrderNo(arg0.getString("code"));
		return orderInfo;
	}

}
