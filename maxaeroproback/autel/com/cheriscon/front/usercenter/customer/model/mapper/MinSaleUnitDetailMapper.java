package com.cheriscon.front.usercenter.customer.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.front.usercenter.customer.vo.MinSaleUnitDetailVO;

/**
 * 最小销售单位详细信息
 * 
 * @author caozhiyong
 * @version2013-1-10
 */
public class MinSaleUnitDetailMapper implements RowMapper {

	@Override
	public MinSaleUnitDetailVO mapRow(ResultSet arg0, int arg1)
			throws SQLException {
		MinSaleUnitDetailVO detailVO = new MinSaleUnitDetailVO();
		detailVO.setAreaCfgCode(arg0.getString("areaCfgCode"));
		
		String dateString=arg0.getString("validDate");
		try {
			if (dateString!=null) {
				Date date=new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
				
				detailVO.setDate(DateUtil.toString(date, "yyyy/MM/dd"));
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		detailVO.setLanguageCode(arg0.getString("languageCode"));
		detailVO.setMemo(arg0.getString("memo"));
		detailVO.setMinSaleUnitCode(arg0.getString("minSaleUnitCode"));
		detailVO.setMinSaleUnitName(arg0.getString("name"));
		detailVO.setPrice(arg0.getString("price"));
		detailVO.setPicpath(arg0.getString("picpath"));
		detailVO.setMemoDetail(arg0.getString("memoDetail"));
		detailVO.setUpCode(arg0.getString("upCode"));
		return detailVO;
	}

}
