package com.cheriscon.app.cms.core.action;

import java.util.List;

import javax.annotation.Resource;

import com.cheriscon.app.cms.core.model.DataCat;
import com.cheriscon.app.cms.core.service.ArticleCatRuntimeException;
import com.cheriscon.app.cms.core.service.IDataCatManager;
import com.cheriscon.app.cms.core.service.IDataModelManager;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

/**
 * 文章类别管理action
 * @author kingapex
 * 2010-7-5上午09:26:26
 */
public class DataCatAction extends WWAction {
	
	@Resource private ILanguageService languageService;
	
	private IDataCatManager dataCatManager;
	private IDataModelManager dataModelManager;
	private boolean isEdit;
	private List catList;
	private List modelList;
	private DataCat cat;
	private int cat_id;
	private int[] cat_ids; //分类id 数组,用于保存排序
	private int[] cat_sorts; //分类排序值
	
	private int parentId;
	
	private List<Language> languageList;
	
	public String list(){
		languageList = languageService.queryLanguage();
		this.catList = this.dataCatManager.listAllChildren(0);
		return "list";
	}
	
	/**
	 * 查询所有子分类
	 * @return
	 */
	public String listChild(){
		languageList = languageService.queryLanguage();
		this.catList = this.dataCatManager.listAllChildren(parentId);
		return "list";
	}
	
	

	//到添加页面
	public String add(){
		isEdit =false;
		languageList = languageService.queryLanguage();
		Language language = languageService.getByLocale(ThreadContextHolder.getHttpRequest().getLocale());
		String languageCode = null;
		if(language != null){
			languageCode = language.getCode();
		}
		catList = dataCatManager.listAllChildren(0,languageCode);
		modelList  = this.dataModelManager.list();
		return this.INPUT;
	}
	
	
	//保存编辑
	public String edit(){
		isEdit = true;
		cat = dataCatManager.get(cat_id);
		languageList = languageService.queryLanguage();
		String languageCode = cat.getLanguage_code();
		catList = dataCatManager.listAllChildren(0,languageCode);
		modelList  = this.dataModelManager.list();
		return this.INPUT;
	}
	
	
	//保存添加
	public String saveAdd(){
		try{
		   dataCatManager.add(cat);
		}catch(ArticleCatRuntimeException ex){
		   this.msgs.add("同级文章栏目不能同名");
		   if(parentId > 0){
			   this.urls.put(getText("content.cms.back"), "cat!listChild.do?parentId="+parentId);
		   }else{
			   this.urls.put(getText("content.cms.back"), "cat!list.do");
		   }
		   return this.MESSAGE; 
		}
		this.msgs.add(getText("content.cms.add"));
		 if(parentId > 0){
			   this.urls.put(getText("content.cms.back"), "cat!listChild.do?parentId="+parentId);
		   }else{
			   this.urls.put(getText("content.cms.back"), "cat!list.do");
		   }
		return this.MESSAGE; 
	}
 
	
	
	//保存修改 
	public String saveEdit(){
		try{
		   this.dataCatManager.edit(cat);
		}catch(ArticleCatRuntimeException ex){
			   this.msgs.add("同级文章栏目不能同名");
			   if(parentId > 0){
				   this.urls.put(getText("content.cms.back"), "cat!listChild.do?parentId="+parentId);
			   }else{
				   this.urls.put(getText("content.cms.back"), "cat!list.do");
			   }
			   return this.MESSAGE; 
		}
		this.msgs.add(getText("content.cms.update"));
		 if(parentId > 0){
			   this.urls.put(getText("content.cms.back"), "cat!listChild.do?parentId="+parentId);
		   }else{
			   this.urls.put(getText("content.cms.back"), "cat!list.do");
		   }
		return this.MESSAGE;
	}
	
	
	//删除
	public String delete(){
		
		int r  = this.dataCatManager.delete(cat_id);
		
		if(r==0){
			json= "{'result':1,'message':'"+getText("content.cms.delete")+"'}";
		}else if(r==1){
			json= "{'result':0,'message':'"+getText("content.cms.delete.error")+"'}";
		}		
				
		return this.JSON_MESSAGE;
	}
	
	
	public String saveSort(){
		try{
			this.dataCatManager.saveSort(cat_ids, cat_sorts);
			json= "{'result':1,'message':'"+getText("content.cms.save")+"'}";
		}catch(RuntimeException  e){
			this.logger.error(e.getMessage(),e);
			json= "{'result':0,'message':'"+getText("contents.cms.fail")+"'}";
		}
		return this.JSON_MESSAGE;
	}

	
	/**
	 * 用于异步显示分类树
	 * @return
	 */
	public String showCatTree(){
		catList = dataCatManager.listAllChildren(cat_id);
		return "cat_tree";
	}


	public IDataCatManager getDataCatManager() {
		return dataCatManager;
	}



	public void setDataCatManager(IDataCatManager dataCatManager) {
		this.dataCatManager = dataCatManager;
	}



	public IDataModelManager getDataModelManager() {
		return dataModelManager;
	}



	public void setDataModelManager(IDataModelManager dataModelManager) {
		this.dataModelManager = dataModelManager;
	}



	public List getCatList() {
		return catList;
	}



	public void setCatList(List catList) {
		this.catList = catList;
	}



	public DataCat getCat() {
		return cat;
	}



	public void setCat(DataCat cat) {
		this.cat = cat;
	}



	public int getCat_id() {
		return cat_id;
	}



	public void setCat_id(int catId) {
		cat_id = catId;
	}



	public int[] getCat_ids() {
		return cat_ids;
	}



	public void setCat_ids(int[] catIds) {
		cat_ids = catIds;
	}



	public int[] getCat_sorts() {
		return cat_sorts;
	}



	public void setCat_sorts(int[] catSorts) {
		cat_sorts = catSorts;
	}



	public List getModelList() {
		return modelList;
	}



	public void setModelList(List modelList) {
		this.modelList = modelList;
	}	
	
	public void setIsEdit(boolean isEdit){
		this.isEdit = isEdit;
	}
	
	public boolean getIsEdit(){
		return this.isEdit;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}
	
	

}
