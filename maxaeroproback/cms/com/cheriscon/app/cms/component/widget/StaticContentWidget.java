package com.cheriscon.app.cms.component.widget;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cheriscon.cop.sdk.widget.AbstractWidget;

/**
 * @author lzf 
 * 这个挂件啥也不干，是空的，主要用于显示如联系我们一类的东西
 */
@Component("staticContent")
@Scope("prototype")
public class StaticContentWidget extends AbstractWidget {

	protected void config(Map<String, String> params) {

	}

	protected void display(Map<String, String> params) {

	}

}
