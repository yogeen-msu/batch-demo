package com.cheriscon.app.cms.component.widget;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.core.service.IDataManager;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.RequestUtil;
import com.cheriscon.framework.util.StringUtil;

/**
 * 相关文章挂件
 * @author kingapex
 *
 */
@Component("relatedData")
@Scope("prototype")
public class RelatedDataWidget extends RequestParamWidget {
	protected void config(Map<String, String> params) {
		
	}

	protected void display(Map<String, String> params) {
		String fieldname = params.get("fieldname");
		String catidStr  = params.get("catid");
		Integer[] ids= this.parseId();
		Integer articleid = ids[0];
		Integer catid = ids[1];
		
		int relcatid  = Integer.valueOf( catidStr );
 
		List dataList = dataManager.listRelated(catid,relcatid, articleid, fieldname);
		this.putData("dataList", dataList);
		
	}

}
