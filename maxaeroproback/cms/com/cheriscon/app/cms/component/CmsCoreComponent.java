package com.cheriscon.app.cms.component;

import org.springframework.stereotype.Component;

import com.cheriscon.framework.component.IComponent;

/**
 * CMS核心组件
 * @author kingapex
 *2012-3-29下午2:29:57
 */
@Component
public class CmsCoreComponent implements IComponent {

	@Override
	public void install() {
		 

	}

	@Override
	public void unInstall() {
		 

	}

}
