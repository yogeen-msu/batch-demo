package com.cheriscon.app.cms.component.plugin;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.cheriscon.app.cms.core.model.DataField;
import com.cheriscon.app.cms.core.plugin.AbstractFieldPlugin;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.DateUtil;

/**
 * 日期控件字段插件
 * @author kingapex
 * 2010-7-7上午11:04:30
 */
@Component
public class DateFieldPlugin extends AbstractFieldPlugin {

	
	public int getHaveSelectValue() {
		
		return 0;
	}

	
	public String onDisplay(DataField field, Object value) {
		StringBuffer html = new StringBuffer();
		 
		html.append("<input type=\"text\" name=\"");
		html.append(field.getEnglish_name());
		html.append("\" readonly=\"true\"");
		if(value!=null){
			html.append("value=\"");
			html.append(value);
			html.append("\"");
		}
		html.append(" class=\"dateinput\" ");
		html.append(">");
	 
		return html.toString();
	}

	
	@Override
	public String getId() {
		
		return "dateinput";
	}

	@Override
	public String getName() {
		
		return "日期控件";
	}

	


}
