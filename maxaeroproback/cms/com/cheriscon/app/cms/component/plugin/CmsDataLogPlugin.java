package com.cheriscon.app.cms.component.plugin;

import java.util.Iterator;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.cheriscon.app.base.core.model.DataLog;
import com.cheriscon.app.cms.core.model.DataField;
import com.cheriscon.app.cms.core.plugin.IDataSaveEvent;
import com.cheriscon.cop.resource.IDataLogManager;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.plugin.AutoRegisterPlugin;

/**
 * cms数据日志记录插件
 * @author kingapex
 * 2010-10-19下午03:23:38
 */
@Component
public class CmsDataLogPlugin  extends AutoRegisterPlugin implements IDataSaveEvent{

	private IDataLogManager dataLogManager;
	
	/**
	 * 响应文章保存事件
	 * 取出data中所有的数据，形成文本为datalog的content<br>
	 * 其中以fs:开头的数据则认为是图片
	 */
	public void onSave(Map data) {
		
		Iterator iter = data.keySet().iterator();
		StringBuffer content = new StringBuffer();
		StringBuffer pics = new StringBuffer();
		while(iter.hasNext()){
			String key = (String)iter.next();
			Object v = data.get(key);
			if(!(v instanceof String)) continue;
			
			String value  = (String)v;

			if(value!=null){
				if(value.startsWith(CopSetting.FILE_STORE_PREFIX)){
					if(pics.length()!=0){
						pics.append(",");
					}
					pics.append(value+"|"+value);
				}else{
					content.append(key+":"+value+"<br>");
				}
			}
			
			
		}
 
		DataLog datalog  = new DataLog();
		datalog.setContent(content.toString());
		datalog.setPics(pics.toString());
		datalog.setLogtype("文章");
		if(data.get("id")== null ) 
			datalog.setOptype("添加");
		else
			datalog.setOptype("修改");
		this.dataLogManager.add(datalog);
	}
	
	
	public IDataLogManager getDataLogManager() {
		return dataLogManager;
	}
	
	public void setDataLogManager(IDataLogManager dataLogManager) {
		this.dataLogManager = dataLogManager;
	}


	
	
}
