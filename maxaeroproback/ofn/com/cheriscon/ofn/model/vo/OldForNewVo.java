package com.cheriscon.ofn.model.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class OldForNewVo implements Serializable {

	private String code;
	private String autelId; // 客户ID
	private String serialNo; // 产品序列号
	private String sealerAutelId; // 经销商autel ID
	private String sealerCode; // 经销商编号

	
	private String frontAutelId;
	private Integer pageNo;
	private Integer pageSize;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAutelId() {
		return autelId;
	}

	public void setAutelId(String autelId) {
		this.autelId = autelId;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getSealerAutelId() {
		return sealerAutelId;
	}

	public void setSealerAutelId(String sealerAutelId) {
		this.sealerAutelId = sealerAutelId;
	}

	public String getSealerCode() {
		return sealerCode;
	}

	public void setSealerCode(String sealerCode) {
		this.sealerCode = sealerCode;
	}

	public String getFrontAutelId() {
		return frontAutelId;
	}

	public void setFrontAutelId(String frontAutelId) {
		this.frontAutelId = frontAutelId;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

}
