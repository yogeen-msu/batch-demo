package com.cheriscon.ofn.front.action.widget;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.ofn.model.OldForNew;
import com.cheriscon.ofn.service.IOldForNewSerice;

@Component("oldForNewDetailWidget")
public class OldForNewDetailWidget extends AbstractWidget {

	@Resource
	private IOldForNewSerice oldForNewSerice;
	
	@Override
	protected void display(Map<String, String> params) {
		OldForNew oldForNew = oldForNewSerice.getByProSerialNo1(params.get("proSerialNo"));
		this.putData("oldForNew", oldForNew);
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

}
