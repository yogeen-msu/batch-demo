package com.cheriscon.ofn.front.action.widget;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.utils.DBUtils;
import com.cheriscon.cop.sdk.widget.AbstractWidget;

@Component("oldForNewWidget")
public class OldForNewWidget extends AbstractWidget {
	
	@Resource
	private IProductInfoService productInfoService;
	

	@Override
	protected void display(Map<String, String> params) {
		ProductInfo product =  productInfoService.getBySerialNo2(params.get("proSerialNo"));
		this.putData("productinfo", product);
		this.putData("code", DBUtils.generateCode("OFN"));
	}

	@Override
	protected void config(Map<String, String> params) {
		// TODO Auto-generated method stub

	}

}
