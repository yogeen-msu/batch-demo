package com.cheriscon.ofn.front.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.content.constant.CTConsatnt;
import com.cheriscon.backstage.content.service.IEmailTemplateService;
import com.cheriscon.backstage.market.service.ISaleContractService;
import com.cheriscon.backstage.member.service.ICustomerProductService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidChangeLogService;
import com.cheriscon.backstage.member.service.IProductSoftwareValidStatusService;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.backstage.system.service.IProductInfoService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.constant.BackageConstant;
import com.cheriscon.common.model.CustomerInfo;
import com.cheriscon.common.model.EmailTemplate;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductInfo;
import com.cheriscon.common.model.ProductSoftwareValidChangeLog;
import com.cheriscon.common.model.ProductSoftwareValidStatus;
import com.cheriscon.common.model.SaleContract;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.common.model.SealerInfo;
import com.cheriscon.common.utils.SessionUtil;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.jms.EmailModel;
import com.cheriscon.framework.jms.EmailProducer;
import com.cheriscon.front.constant.FrontConstant;
import com.cheriscon.front.user.service.ICustomerInfoService;
import com.cheriscon.ofn.model.OldForNew;
import com.cheriscon.ofn.service.IOldForNewSerice;


/**
 * 以旧换新活动
 * @author AUTEL
 *
 */
@ParentPackage("json-default")
@Namespace("/front/usercenter/oldForNew")
public class OldForNewAction extends WWAction{

	private OldForNew oldForNew ;
	private int id;
	private String code;
	private String proTypeCode;
	private String proSerialNo;
	private String regResult = "false";
	private String oserialNo;					//旧产品序列号
	private String proRegPwd;					//旧产品注册密码
	private String oproductTypeCode;			// 旧产品型号Code
	private Integer state;						// 状态  1.待处理 2验证通过  3 验证未通过 4已完成
	private Integer giftType;					// 赠品类型 1换产品 2 现金
	private String giftProTypeCode;				//赠品 商品分类
	private String mailAddress;					//邮寄地址
	private String logisticsCompany;    // 物流公司
	private String logisticsNo;       // 物流编号
	private String logisticsTime;    //物流时间
	private String consignee; //收货人
	private String phone ;  //联系电话
	private String receivedDate; //接货时间
	private String reasons; //验证未通过原因
	
	private String serialNos; 					//产品序列号
	
	private Date activeStartTimePre =  DateUtil.toDate("2015-08-01 00:00:00", "yyyy-MM-dd HH:mm:ss");//活动开始时间 2015-08-01
	private Date activeEndTimeNext = DateUtil.toDate("2015-11-01 00:00:00", "yyyy-MM-dd HH:mm:ss");//活动结束时间2015-10-31
	
	@Resource
	private IProductInfoService productInfoService;
	
	@Resource
	private ISaleContractService saleContractService;
	
	@Resource
	private IOldForNewSerice oldForNewSerice;
	
	@Resource
	private ICustomerProductService customerProductService;
	
	@Resource
	private IProductSoftwareValidStatusService productSoftwareValidStatusService; 
	
	@Resource
	private IProductSoftwareValidChangeLogService ProductSoftwareValidChangeLogService;
	@Resource
	private ISmtpManager smtpManager;

	@Resource
	private EmailProducer emailProducer;
	
	@Resource
	private IEmailTemplateService emailTemplateService; 
	
	@Resource
	private ILanguageService languageService;
	
	@Resource
	private ICustomerInfoService customerInfoService;
	
	@Resource
	private ISealerDataAuthDeailsService sealerDataAuthDeailsService;
	
	@Resource
	private ISealerInfoService sealerInfoService;
	
	/**
	 * 验证是否在活动范围内
	 * @return
	 */
	@Action(value = "check", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String check(){
//		验证是否在活动范围内
		regResult = isScope();
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("regResult", regResult);
		regResult=JSONArray.fromObject(dataMap).toString();
		return SUCCESS;
	}
	
	/**
	 * 添加以旧换新
	 * @return
	 */
	@Action(value = "add", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String add(){
//		验证是否在活动范围内
		regResult = isScope();
		if(regResult.equals("true")){
			oldForNew = new OldForNew();
			oldForNew.setGiftProTypeCode(giftProTypeCode);
			oldForNew.setGiftType(giftType);
			oldForNew.setMailAddress(mailAddress);
			oldForNew.setOproductTypeCode(oproductTypeCode);
			oldForNew.setOserialNo(oserialNo);
			oldForNew.setProductTypeCode(proTypeCode);
			oldForNew.setSerialNo(proSerialNo);
			oldForNew.setConsignee(consignee);
			oldForNew.setPhone(phone);
			if(StringUtils.isNotEmpty(code)){
				oldForNew.setCode(code);
			}
			oldForNew.setState(1);
			HttpServletRequest request=ThreadContextHolder.getHttpRequest();
	        CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
	        oldForNew.setAutelId(customerInfo.getAutelId());
			OldForNew coldFor = oldForNewSerice.getByProSerialNo(proSerialNo);
//			注册的产品已经参与了活动
			if(coldFor!=null){
				regResult = "prouserd";
			}else{
				try{
					boolean isDssp = false;
					ProductInfo product = null;
//					选择DS708作为旧产品的时候 需要验证序列号和注册密码 ，其他产品只有邮寄到公司，由技术人员验证
					if(oproductTypeCode.equals("MaxiDAS DS708")){
						 product = productInfoService.getBySerialNo(oserialNo);
						
						if(product==null){
							regResult = "pwderror";
	//					验证密码
						}else {
							SaleContract saleContract = saleContractService.getSaleContractByCode(product.getSaleContractCode());
//							 用区域判断不准 使用经销商来判断
							String area = saleContract.getSealerCode();
//							活动地区北美 经销商为C-0063
							if(!checkrea(area)){
								regResult = "oproAreaError";
//								判断旧产品注册状态 类型 密码
							}else if(product.getRegStatus() == FrontConstant.CHARGE_CARE_IS_YES_REG&&product.getRegPwd().equals(proRegPwd)&&
									product.getProTypeCode().equals("prt_DS708")){
								coldFor = oldForNewSerice.getByOProSerialNo(oserialNo);
		//						旧DS708已经 参与了活动
								if(coldFor!=null){
									regResult = "oprouserd";
								}else{
	//								oldForNewSerice.saveOldForNew(oldForNew);
									regResult = "true";
									isDssp=true;
								}
		//					密码不对
							}else{
								regResult = "pwderror";
							}
						}
					}else{
//						oldForNewSerice.saveOldForNew(oldForNew);
						regResult = "true";
					}
//					设置产品
					if(regResult.equals("true")){
						if(giftType==1){
							oldForNew.setGiftType(1);
							oldForNew.setGiftProTypeCode("TS501");
						}if(giftType==3){
							oldForNew.setGiftType(1);
							oldForNew.setGiftProTypeCode("MP408");
						}else{
							oldForNew.setGiftType(2);
							oldForNew.setGiftProTypeCode("300");
						}
						oldForNewSerice.saveOldForNew(oldForNew);
						try{
						String validDate = DateUtil.toString(new Date(), "yyyy-MM-dd");
						if(isDssp){
							List<ProductSoftwareValidStatus> list=productSoftwareValidStatusService.getProductSoftwareValidStatusList(oserialNo);
							if(list!=null && list.size() > 0) {
								for(int i=0 ;i<list.size();i++){
									ProductSoftwareValidStatus valid=list.get(i);
//									如果当前时间大于有效期就不改
									if(validDate.compareTo(valid.getValidDate())>0){
										continue;
									}
									ProductSoftwareValidChangeLog log=new ProductSoftwareValidChangeLog();
									String ip=FrontConstant.getIpAddr(request);
									log.setProductSN(oserialNo);
									log.setMinSaleUnit(valid.getMinSaleUnitCode());
									log.setNewDate(validDate);
									log.setOldDate(valid.getValidDate());
									log.setOperatorIp(ip);
									log.setOperatorTime(DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss"));
									log.setOperatorUser(customerInfo.getAutelId());
									log.setOperType(BackageConstant.PRODUCT_VALID_CHANGE_OLD_FOR_NEW);
									ProductSoftwareValidChangeLogService.saveProductSoftwareValidStatus(log);
									
									valid.setValidDate(validDate);
									productSoftwareValidStatusService.updateValidDate(valid);
									
								}
							}
						}
						}catch (Exception e) {
							e.printStackTrace();
						}
						
//						发送邮件
						try{
							Smtp smtp = smtpManager.getCurrentSmtp( );
							
							EmailModel emailModel = new EmailModel();
							emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
							emailModel.getData().put("password", smtp.getPassword());
							emailModel.getData().put("host", smtp.getHost());
							emailModel.getData().put("autelId", customerInfo.getAutelId());
							
							String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");
							String oldForNewUrl = requestUrl+ "/oldForNewDetail.html?proSerialNo="+proSerialNo;
							String userName="";
							if(customerInfo!=null && customerInfo.getName()!=null){
							  userName=customerInfo.getName();
							}
							
							emailModel.getData().put("oldForNewUrl", oldForNewUrl);
							emailModel.getData().put("userName", userName);
							emailModel.getData().put("oldForNew", oldForNew);
							Language language = languageService.getByCode(customerInfo.getLanguageCode());
							
							if(language != null)
							{
								emailModel.setTitle(FreeMarkerPaser.getBundleValue("ofn.mailtitle",
										language.getLanguageCode(),language.getCountryCode()));
							}
							else 
							{
								emailModel.setTitle(FreeMarkerPaser.getBundleValue("ofn.mailtitle"));
							}
							
							emailModel.setTo(customerInfo.getAutelId());
							EmailTemplate template = emailTemplateService.
									getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_OLD_FOR_NEW,customerInfo.getLanguageCode());
							
							if(template != null)
							{
								emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
							}
							emailProducer.send(emailModel);
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
				}catch (Exception e) {
				e.printStackTrace();
				regResult = "false";
			}
			}

		}
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("regResult", regResult);
		regResult=JSONArray.fromObject(dataMap).toString();
		return SUCCESS;
	}
	
	/**
	 * 验证序列号是否参与了以旧换新活动
	 * @return
	 */
	@Action(value = "checkOnAc", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String checkOnAc(){
		regResult = "";
		if(StringUtils.isNotEmpty(serialNos)){
			String [] serialNoArr = serialNos.split(",");
			List<OldForNew> oldForNews = oldForNewSerice.getByProSerialNos(serialNos);
			if(oldForNews!=null&&oldForNews.size()>0){
				for(OldForNew ofn :oldForNews){
					regResult += "," + ofn.getSerialNo();
				}
				regResult = regResult.substring(1);
			}else{
				regResult = "";
			}
		}
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("regResult", regResult);
		regResult=JSONArray.fromObject(dataMap).toString();
		return SUCCESS;
	}
	/**
	 * 经销商处理活动
	 * @return
	 */
	@Action(value = "dispose", results = { @Result(name = SUCCESS, type = "json", params = {
			"root", "regResult"}) })
	public String dispose(){
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
		try{
			SealerInfo sealerInfo=(SealerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
	        String sealerCode = sealerInfo.getCode();
	        if(sealerInfo.getAuth()!=null&&sealerInfo.getAuth().startsWith("0063")){
	        	sealerCode = "sei201305101150570536";
	        }else{
	        	String authCode = sealerInfo.getAuth();
	        	if(authCode!=null&&authCode.indexOf(",")>0){
	        		authCode = authCode.substring(0, authCode.indexOf(","));
	        	}
	        	SealerDataAuthDeails sealerDataAuthDeails = sealerDataAuthDeailsService.getDataAuth(authCode);
	        	if(sealerDataAuthDeails!=null){
	        		SealerInfo tsi = sealerInfoService.getSealerByAutelId(sealerDataAuthDeails.getSealerCode());
	        		sealerCode = tsi.getCode();
	        	}
	        }
			if(sealerInfo == null||!checkrea(sealerCode)){
	        	regResult = "notLogin";
	        }else{
				oldForNew = oldForNewSerice.get(id);
				if(oldForNew==null){
					regResult = "notFind";
				}else{
					if(state==null){
						state = 1;
					}
					oldForNew.setState(state);
					oldForNew.setLogisticsCompany(null);
					oldForNew.setLogisticsNo(null);
					oldForNew.setLogisticsTime(null);
					if(state==4){
						oldForNew.setLogisticsCompany(logisticsCompany);
						oldForNew.setLogisticsNo(logisticsNo);
						if(StringUtils.isEmpty(logisticsTime)){
							logisticsTime = DateUtil.toString(new Date(), "yyyy-MM-dd");
						}
						oldForNew.setLogisticsTime(logisticsTime);
					}else if(state==2){
						if(StringUtils.isEmpty(receivedDate)){
							receivedDate = DateUtil.toString(new Date(), "yyyy-MM-dd");
						}
						oldForNew.setReceivedDate(receivedDate);
					}else if(state==3){
						oldForNew.setReasons(reasons);
					}
					oldForNew.setGiftType(giftType);
					giftType=giftType==null?0:giftType;
					if(giftType==1){
						oldForNew.setGiftType(1);
						oldForNew.setGiftProTypeCode("TS501");
					}if(giftType==3){
						oldForNew.setGiftType(1);
						oldForNew.setGiftProTypeCode("MP408");
					}else{
						oldForNew.setGiftType(2);
						oldForNew.setGiftProTypeCode("300");
					}
					oldForNew.setMailAddress(mailAddress);
					oldForNew.setConsignee(consignee);
					oldForNew.setPhone(phone);
						oldForNewSerice.updateOldForNew(oldForNew);
						regResult = "true";
						if(state==4||state==3){
//							发送邮件
							try{
								Smtp smtp = smtpManager.getCurrentSmtp( );
								
								EmailModel emailModel = new EmailModel();
								emailModel.getData().put("username",smtp.getUsername());//username 为freemarker模版中的参数
								emailModel.getData().put("password", smtp.getPassword());
								emailModel.getData().put("host", smtp.getHost());
								emailModel.getData().put("autelId", oldForNew.getAutelId());
								
								CustomerInfo customerInfo = customerInfoService.getCustomerInfoByAutelId(oldForNew.getAutelId());
								String requestUrl =FreeMarkerPaser.getBundleValue("autelproweb.url");
								String oldForNewUrl = requestUrl+ "/oldForNewDetail.html?proSerialNo="+oldForNew.getSerialNo();
								String userName="";
								if(customerInfo!=null && customerInfo.getName()!=null){
								  userName=customerInfo.getName();
								}
								
								emailModel.getData().put("oldForNewUrl", oldForNewUrl);
								emailModel.getData().put("userName", userName);
								emailModel.getData().put("oldForNew", oldForNew);
								emailModel.getData().put("state", state);
								Language language = languageService.getByCode(customerInfo.getLanguageCode());
								
								if(language != null)
								{
									emailModel.setTitle(FreeMarkerPaser.getBundleValue("ofn.mailtitle",
											language.getLanguageCode(),language.getCountryCode()));
								}
								else 
								{
									emailModel.setTitle(FreeMarkerPaser.getBundleValue("ofn.mailtitle"));
								}
								
								emailModel.setTo(customerInfo.getAutelId());
								EmailTemplate template = emailTemplateService.
										getUseTemplateByTypeAndLanguage(CTConsatnt.EMAIL_TEMPLATE_TYEP_OLD_FOR_DISPATCH,customerInfo.getLanguageCode());
								
								if(template != null)
								{
									emailModel.setTemplate(template.getCode()+".html");		//此处一定需要这样写
								}
								
								emailProducer.send(emailModel);
							}catch (Exception e) {
								e.printStackTrace();
							}
						}
				}
	        }
		}catch (Exception e) {
			regResult = "error";
		}
		Map<String,String> dataMap = new HashMap<String, String>();
		dataMap.put("regResult", regResult);
		regResult=JSONArray.fromObject(dataMap).toString();
		
		return SUCCESS;
	}
	
	/**
	 * 验证是否在活动范围内
	 * @return
	 */
	private String isScope(){
		String scope = "false";
		HttpServletRequest request=ThreadContextHolder.getHttpRequest();
        CustomerInfo customerInfo=(CustomerInfo)SessionUtil.getLoginUserInfo(request);//从session中获取当前登录的用户信息	
        if(customerInfo == null){
        	return "notLogin";
        }
		Date now = new Date();
//		活动时间
		if(now.after(activeStartTimePre)&&now.before(activeEndTimeNext)){
//			活动参与产品MS908 MS908需要验证序列号
			if(StringUtils.isNotEmpty(proSerialNo)){
				ProductInfo product = productInfoService.getBySerialNo(proSerialNo);
				if(product==null){
						scope = "proNotFind";
//				判断是不是MS908
				}else if(product.getProTypeCode().equals("pts201308291344300924")){
					proTypeCode = product.getProTypeCode();
					try{
	//					判断产品注册状态
						if(product.getRegStatus() == FrontConstant.CHARGE_CARE_IS_YES_REG){
	//						判断产品注册时间
							Date regTime =  null;
							if(product.getRegTime().length()>10){
								regTime = DateUtil.toDate(product.getRegTime(), "yyyy-MM-dd HH:mm:ss");
							}else{
								regTime = DateUtil.toDate(product.getRegTime(), "yyyy-MM-dd");
							}
							if(regTime.compareTo(activeStartTimePre)>-1&&regTime.before(activeEndTimeNext)){
								SaleContract saleContract = saleContractService.getSaleContractByCode(product.getSaleContractCode());
//								 用区域判断不准 使用经销商来判断
								String area = saleContract.getSealerCode();
//								活动地区北美 经销商为C-0063
								if(checkrea(area)){
									String customerCode = customerProductService.getCustomerproinfo(product.getCode());
//									验证注册用户
									if(customerCode!=null&&customerCode.equals(customerInfo.getCode())){
										return "true";
									}else{
										scope = "customerError";
									}
									
								}else{
									scope = "areaerror";
								}
							}else{
								scope = "regTimeError";
							}
						}else{
							scope = "proNotReg";
						}
					}catch (Exception e) {
						e.printStackTrace();
						scope = "proerror";
					}
				}else{
					scope = "proerror";
				}
				
			}else{
				scope = "proerror";
			}
		}else{
			scope = "timeerror";
		}
		return scope;
	}
	
	private boolean checkrea(String sealerCode){
		String[] sealerCodes = new String[]{"sei201305101150570536","C-0001",
				"sei201305101150570664","C-0611","sei201404142005450087"};
		for(String code :sealerCodes){
			if(code.equals(sealerCode)){
				return true;
			}
		}
		return false;
	}

	public OldForNew getOldForNew() {
		return oldForNew;
	}

	public void setOldForNew(OldForNew oldForNew) {
		this.oldForNew = oldForNew;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProTypeCode() {
		return proTypeCode;
	}

	public void setProTypeCode(String proTypeCode) {
		this.proTypeCode = proTypeCode;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getRegResult() {
		return regResult;
	}

	public void setRegResult(String regResult) {
		this.regResult = regResult;
	}


	public String getOproductTypeCode() {
		return oproductTypeCode;
	}
	public void setOproductTypeCode(String oproductTypeCode) {
		this.oproductTypeCode = oproductTypeCode;
	}
	public String getProRegPwd() {
		return proRegPwd;
	}

	public void setProRegPwd(String proRegPwd) {
		this.proRegPwd = proRegPwd;
	}


	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getGiftType() {
		return giftType;
	}

	public void setGiftType(Integer giftType) {
		this.giftType = giftType;
	}

	public String getGiftProTypeCode() {
		return giftProTypeCode;
	}

	public void setGiftProTypeCode(String giftProTypeCode) {
		this.giftProTypeCode = giftProTypeCode;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public IProductInfoService getProductInfoService() {
		return productInfoService;
	}

	public void setProductInfoService(IProductInfoService productInfoService) {
		this.productInfoService = productInfoService;
	}

	public ISaleContractService getSaleContractService() {
		return saleContractService;
	}

	public void setSaleContractService(ISaleContractService saleContractService) {
		this.saleContractService = saleContractService;
	}

	public IOldForNewSerice getOldForNewSerice() {
		return oldForNewSerice;
	}

	public void setOldForNewSerice(IOldForNewSerice oldForNewSerice) {
		this.oldForNewSerice = oldForNewSerice;
	}
	public String getOserialNo() {
		return oserialNo;
	}
	public void setOserialNo(String oserialNo) {
		this.oserialNo = oserialNo;
	}

	public String getSerialNos() {
		return serialNos;
	}

	public void setSerialNos(String serialNos) {
		this.serialNos = serialNos;
	}

	public String getLogisticsCompany() {
		return logisticsCompany;
	}

	public void setLogisticsCompany(String logisticsCompany) {
		this.logisticsCompany = logisticsCompany;
	}

	public String getLogisticsNo() {
		return logisticsNo;
	}

	public void setLogisticsNo(String logisticsNo) {
		this.logisticsNo = logisticsNo;
	}

	public String getLogisticsTime() {
		return logisticsTime;
	}

	public void setLogisticsTime(String logisticsTime) {
		this.logisticsTime = logisticsTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}
	
	
}
