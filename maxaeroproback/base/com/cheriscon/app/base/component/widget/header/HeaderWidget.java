package com.cheriscon.app.base.component.widget.header;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.StringUtil;


/**
 * 网站头挂件
 * @author kingapex
 * 2010-6-22上午10:13:17
 */
@Component("header")
@Scope("prototype")
public class HeaderWidget extends AbstractWidget {

	
	protected void config(Map<String, String> params) {
		
	}

	
	protected void display(Map<String, String> params) {
		this.setPageName("header");
		CopSite site = CopContext.getContext().getCurrentSite();
		
		String ctx = ThreadContextHolder.getHttpRequest().getContextPath();
		this.putData("ctx", ctx);
		
		if(this.getData(HeaderConstants.title)==null)
			this.putData(HeaderConstants.title, StringUtil.isEmpty(site.getTitle())?site.getSitename():FreeMarkerPaser.getBundleValue(site.getTitle()));
		
		if(this.getData("keywords")==null)
			this.putData(HeaderConstants.keywords, site.getKeywords());
		
		if(this.getData("description")==null)
			this.putData(HeaderConstants.description, site.getDescript());
		
		
		this.putData("ico",  site.getIcofile());
		this.putData("logo", site.getLogofile());
	}


}
