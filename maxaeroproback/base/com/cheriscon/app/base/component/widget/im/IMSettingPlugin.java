package com.cheriscon.app.base.component.widget.im;

import org.springframework.stereotype.Component;

import com.cheriscon.app.base.core.plugin.setting.IOnSettingInputShow;
import com.cheriscon.framework.plugin.AutoRegisterPlugin;
import com.cheriscon.framework.plugin.page.JspPageTabs;

/**
 * 在线客服务设置插件
 * @author kingapex
 * 2010-9-15下午02:47:58
 */

public class IMSettingPlugin extends AutoRegisterPlugin  implements IOnSettingInputShow {

	public String getSettingGroupName() {
		
		return "im";
	}

	public String onShow() {
		
		return "setting";
	}

	
	public String getId() {
		
		return "imSettingPlugin";
	}

	
	public String getName() {
		
		return "imSettingPlugin";
	}

	
 

	@Override
	public String getTabName() {
	 
		return "在线客服";
	}

	@Override
	public int getOrder() {
		 
		return 0;
	}

}
