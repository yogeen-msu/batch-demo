package com.cheriscon.app.base.component.widget.regions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.util.StringUtil;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;


/**
 * 地区下拉框控件
 * @author kingapex
 *2012-3-11下午9:41:19
 */
public class RegionSelectDirective implements TemplateDirectiveModel {

	
	
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		
		Object provinceobj = params.get("province_id");
		Object cityObj = params.get("city_id");
		Object regionObj =  params.get("region_id");
		

		
		RegionsSelectWidget regionsSelect = SpringContextHolder.getBean("regionsSelect");
		Map<String,String> wparams = new HashMap<String, String>();
		String selectHtml = regionsSelect.process(wparams);
		
		if(provinceobj!=null && cityObj!=null && cityObj!=null ){
			
			String province_id = params.get("province_id").toString();
			String city_id = params.get("city_id").toString();
			String region_id = params.get("region_id").toString();
			if(
					!StringUtil.isEmpty(province_id)
				&&	!StringUtil.isEmpty(city_id)
				&&	!StringUtil.isEmpty(region_id)
			){
				selectHtml +="<script>$(function(){ RegionsSelect.load("+province_id+","+city_id+","+region_id+");  });</script>";
			}
		}
		
		
		
		env.getOut().write(selectHtml);
		
	}
	

}
