package com.cheriscon.app.base.component.widget;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.database.IDBRouter;

/**
 * sql挂件
 */
@Component("sql_list")
@Scope("prototype")
public class SqlListWidget extends AbstractWidget {
	private IDBRouter baseDBRouter;
	
	@Resource private ILanguageService languageService;
	
	
	@Override
	protected void config(Map<String, String> params) {
		
	}

	@Override
	protected void display(Map<String, String> params) {
		String sql  =params.get("sql");
		
		sql = filterSql(sql);
		
		String localParam = params.get("local_param");
		if(StringUtils.isNotEmpty(localParam)){	//本地语言不为空
			//增加语言环境查询参数
			Language languages = (Language)CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
			
			Locale locale = null;

			if (languages != null) {
				locale = new Locale(languages.getLanguageCode(),
						languages.getCountryCode());
			} else {
				locale = CopContext.getDefaultLocal();
			}
			
			Language language = languageService.getByLocale(locale);
			if(language != null){
				if(sql.indexOf("where") != -1){
					String [] tsql = sql.split("where");
					sql =tsql[0]+" where "+ (localParam +" = '"+language.getCode()+"'") + " and " + tsql[1];
				}else{
					String pattern = "order(\\s*)by";
					Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
					Matcher m = p.matcher(sql);
					if(m.find()) {
						String orderBy  = m.group(0);
						String [] tsql = sql.split(orderBy);
						sql = tsql[0]+" where "+ (localParam +" = '"+language.getCode()+"'") + " " + orderBy + " " + tsql[1];
					}else{
						sql += " where " + (localParam +" = '"+language.getCode()+"'");
					}
				}
			}
		}
		
		List list = this.daoSupport.queryForList(sql);
		this.putData("dataList", list);
		
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		this.putData("servlet_path",request.getServletPath());
		
	}
	
	private String filterSql(String sql ){
		 
		String pattern = "#(.*?)#";
 
		Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
		Matcher m = p.matcher(sql);
		while(m.find()) {
				String tb  = m.group(0);
				 
				String newTb  = tb.replaceAll("#", "");
				newTb = this.baseDBRouter.getTableName(newTb);
				sql = sql.replaceAll(tb, newTb);
		}		
		return sql;
	}
	
	public static void main(String[] args){
		String sql  ="select * from  #goods# g ,#goods_cat# where cat_id=1 order by 1";
		String pattern = "order(\\s*)by";
//		String pattern = "#(.*?)#";
// 
		Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
		Matcher m = p.matcher(sql);
		while(m.find()) {
				String tb  = m.group(0);
				System.out.println(tb);
//				String newTb  = tb.replaceAll("#", "");
//				sql = sql.replaceAll(tb, newTb);
		}		
		
//		if(sql.indexOf("where") != -1){
//			String [] tsql = sql.split("where");
//			System.out.println(tsql[0]);
//			System.out.println(tsql[1]);
//		}
	}

	public IDBRouter getBaseDBRouter() {
		return baseDBRouter;
	}

	public void setBaseDBRouter(IDBRouter baseDBRouter) {
		this.baseDBRouter = baseDBRouter;
	}
	
}
