package com.cheriscon.app.base.component.widget.counter;

import java.io.File;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.widget.AbstractWidget;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;

/**
 * 计数器挂件
 * @author kingapex
 * 2010-8-15下午07:00:35
 */
@Component("counter")
@Scope("prototype")
public class CounterWidget extends AbstractWidget {

	@Override
	protected void config(Map<String, String> params) {

	}

	@Override
	protected void display(Map<String, String> params) {
		String counterPath  =CopSetting.cop_PATH+ CopContext.getContext().getContextPath() +"/counter.txt";
		File file  = new File(counterPath);
		try{
			if(!file.exists()) file.createNewFile();
		}catch(Exception e){
			e.printStackTrace();
		}
		String count =  FileUtil.read(counterPath, "UTF-8");
		if(StringUtil.isEmpty(count)) count="0";
		count = count.replaceAll("\n", "");
		FileUtil.write(counterPath, ""+ (Integer.valueOf(count) +1)) ;
		this.putData("count", (Integer.valueOf(count) +1));
	}

}
