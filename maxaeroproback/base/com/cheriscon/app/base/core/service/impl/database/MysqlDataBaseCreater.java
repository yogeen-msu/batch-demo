package com.cheriscon.app.base.core.service.impl.database;

import org.springframework.jdbc.core.JdbcTemplate;

import com.cheriscon.app.base.core.service.IDataBaseCreater;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.database.ISqlFileExecutor;

/**
 *  mysql数据库创建
 * @author kingapex
 *
 */
public class MysqlDataBaseCreater implements IDataBaseCreater {
	private  ISqlFileExecutor sqlFileExecutor;
	
	public void create() {
		sqlFileExecutor.execute("file:com/cheriscon/cop/cop_mysql.sql");
		sqlFileExecutor.execute("file:com/cheriscon/app/shop/javashop_mysql.sql");
		sqlFileExecutor.execute("file:com/cheriscon/app/cms/cms_mysql.sql");
	}
	public ISqlFileExecutor getSqlFileExecutor() {
		return sqlFileExecutor;
	}
	public void setSqlFileExecutor(ISqlFileExecutor sqlFileExecutor) {
		this.sqlFileExecutor = sqlFileExecutor;
	}

}
