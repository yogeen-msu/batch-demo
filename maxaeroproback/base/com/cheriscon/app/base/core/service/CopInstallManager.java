package com.cheriscon.app.base.core.service;

import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.app.base.core.service.dbsolution.DBSolutionFactory;
import com.cheriscon.app.base.core.service.dbsolution.IDBSolution;
import com.cheriscon.app.base.core.service.solution.ISolutionInstaller;
import com.cheriscon.cop.resource.IAppManager;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.IUserManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.CopUser;
import com.cheriscon.cop.sdk.context.CopContext;

public class CopInstallManager {
	private JdbcTemplate jdbcTemplate;
	private IAppManager appManager;
	private ApplicationContext context;
	private IUserManager userManager;
	private ISiteManager siteManager;
	private ISolutionInstaller solutionInstaller;

	public void install(String username, String password, String domain,
			String productid) {
		CopSite site = new CopSite();
		site.setUserid(1);
		site.setId(1);
		CopContext context = new CopContext();
		context.setCurrentSite(site);
		CopContext.setContext(context);

		this.installUser(username, password, domain, productid);
	}

	/*
	 * 初始化cop 平台的用户
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void installUser(String username, String password, String domain,
			String productid) {
		DBSolutionFactory.dbImport("file:com/cheriscon/app/base/init.xml","");

		CopUser CopUser = new CopUser();
		CopUser.setAddress("在这里输入详细地址");
		CopUser.setUsername(username);
		CopUser.setCompanyname("单机版用户");
		CopUser.setLinkman("在这里输入联系人信息");
		CopUser.setTel("010-12345678");
		CopUser.setMobile("13888888888");
		CopUser.setEmail("youmail@domain.com");
		CopUser.setUsertype(1);
		CopUser.setPassword(password);
		Integer userid = userManager.createUser(CopUser);
		userManager.login(username, password);

		CopSite site = new CopSite();
		site.setSitename("javashop");
		site.setThemeid(1);
		site.setAdminthemeid(1);
		site.setSitename(productid + "新建站点");
		site.setUserid(userid);
		site.setUsername(CopUser.getUsername());
		site.setUsertel(CopUser.getTel());
		site.setUsermobile(CopUser.getMobile());
		site.setUseremail(CopUser.getEmail());

		Integer siteid = siteManager.add(site, domain);

		solutionInstaller.install(userid, siteid, productid);
		solutionInstaller.install(userid, siteid, "base");
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public IAppManager getAppManager() {
		return appManager;
	}

	public void setAppManager(IAppManager appManager) {
		this.appManager = appManager;
	}

	public ApplicationContext getContext() {
		return context;
	}

	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	public ISolutionInstaller getSolutionInstaller() {
		return solutionInstaller;
	}

	public void setSolutionInstaller(ISolutionInstaller solutionInstaller) {
		this.solutionInstaller = solutionInstaller;
	}

	public IUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(IUserManager userManager) {
		this.userManager = userManager;
	}

	public ISiteManager getSiteManager() {
		return siteManager;
	}

	public void setSiteManager(ISiteManager siteManager) {
		this.siteManager = siteManager;
	}

}
