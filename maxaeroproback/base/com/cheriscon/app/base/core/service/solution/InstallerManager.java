package com.cheriscon.app.base.core.service.solution;

import com.cheriscon.cop.resource.model.CopProduct;
import com.cheriscon.framework.database.IDaoSupport;
import com.cheriscon.framework.database.Page;

/**
 * 安装者管理
 * @author kingapex
 * 2010-6-24下午05:27:22
 */
public class InstallerManager {
	private IDaoSupport<CopProduct> daoSupport;
	
	public void add(Installer installer){
		String sql ="select count(0) from cop_installer where ip=?";
		int count = this.daoSupport.queryForInt(sql, installer.getIp());
		if(count==0)
			this.daoSupport.insert("cop_installer", installer);
	}
	
	public void add1(Installer installer){
		this.daoSupport.insert("cop_installer", installer);
	}
	
	public Page list(int pageNo,int pageSize){
		
		return this.daoSupport.queryForPage("select * from cop_installer order by installtime desc",pageNo, pageSize);
		
	}
	
	public IDaoSupport<CopProduct> getDaoSupport() {
		return daoSupport;
	}

	public void setDaoSupport(IDaoSupport<CopProduct> daoSupport) {
		this.daoSupport = daoSupport;
	}
	
}
