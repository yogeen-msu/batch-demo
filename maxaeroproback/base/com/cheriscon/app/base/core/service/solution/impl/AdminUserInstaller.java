package com.cheriscon.app.base.core.service.solution.impl;

import org.w3c.dom.Node;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.app.base.core.service.solution.IInstaller;
import com.cheriscon.cop.resource.IUserManager;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.CopSiteAdmin;
import com.cheriscon.cop.resource.model.CopUser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.database.IDaoSupport;

public class AdminUserInstaller implements IInstaller {

	private IUserManager userManager;
	private IAdminUserManager adminUserManager;
	private IDaoSupport daoSupport;

	public void install(String productId, Node fragment) {
		if ("base".equals(productId)) {
			CopUser user = this.userManager.getCurrentUser();
			CopSite site = CopContext.getContext().getCurrentSite();
			int userid = site.getUserid();
			int siteid = site.getId();
			if (user != null) { // 站点本身导入user会为空
				// this.adminUserManager.clean();
				// //为站点添加管理员，此管理员为创始人。
				AdminUser adminUser = new AdminUser();
				adminUser.setUsername(user.getUsername());
				adminUser.setPassword(user.getPassword());
				adminUser.setFounder(1);
				int adminUserId = this.adminUserManager.add(site.getUserid(),
						siteid, adminUser);
				// 创建管理员时的密码为双md5了，更新为md5码
				if (CopSetting.RUNMODE.equals("2")) {
					this.daoSupport.execute("update es_adminuser_" + userid
							+ "_" + siteid + " set password=? where userid=?",
							user.getPassword(), adminUserId);
				} else {
					this.daoSupport
							.execute(
									"update es_adminuser  set password=? where userid=?",
									user.getPassword(), adminUserId);
				}
			} else { // 如果是本地导入，adminuser表已经清空，重新插入当前用户
				AdminUser adminUser = this.adminUserManager.getCurrentUser();
				String tablename = "es_adminuser";
				if (CopSetting.RUNMODE.equals("2")) { // saas式时表名变更
					tablename = tablename + "_" + userid + "_" + siteid;
				}
				this.daoSupport.insert(tablename, adminUser);
				Integer adminuserid = adminUser.getUserid();

				// 创建管理员时的密码为双md5了，更新为md5码
				if (CopSetting.RUNMODE.equals("2")) {
					this.daoSupport.execute("update es_adminuser_" + userid
							+ "_" + siteid + " set password=? where userid=?",
							adminUser.getPassword(), adminuserid);
				} else {
					this.daoSupport
							.execute(
									"update es_adminuser  set password=? where userid=?",
									adminUser.getPassword(), userid);
				}
			}
			
			//添加订单管理员
			AdminUser orderUser = new AdminUser();
			orderUser.setUsername("order");
			orderUser.setPassword("order");
			orderUser.setState(1);
			orderUser.setRoleids(new int[]{2});
			adminUserManager.add(orderUser);
			
			//添加开发者
			AdminUser devUser = new AdminUser();
			devUser.setUsername("developer");
			devUser.setPassword("developer");
			devUser.setState(1);
			devUser.setRoleids(new int[]{3});
			adminUserManager.add(devUser);
			
			
		}
	}

	public IUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(IUserManager userManager) {
		this.userManager = userManager;
	}

	public IAdminUserManager getAdminUserManager() {
		return adminUserManager;
	}

	public void setAdminUserManager(IAdminUserManager adminUserManager) {
		this.adminUserManager = adminUserManager;
	}

	public IDaoSupport getDaoSupport() {
		return daoSupport;
	}

	public void setDaoSupport(IDaoSupport daoSupport) {
		this.daoSupport = daoSupport;
	}

}
