package com.cheriscon.app.base.core.service.auth.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;

public  class PermissionConfig {
	private static Map<String,Integer> authMap= new HashMap<String,Integer>();
	static{ 
 
		try{
			InputStream in  = FileUtil.getResourceAsStream("auth.properties");
			Properties props = new Properties();
			props.load(in);
				 
				
			int allocation = StringUtil.toInt(props.getProperty("auth.allocation"), true)  ;
			int depot_allocation = StringUtil.toInt(props.getProperty("auth.depot_allocation"), true)  ;		
			int depot_ship = StringUtil.toInt(props.getProperty("auth.depot_ship"), true)  ;
			int depot_admin = StringUtil.toInt(props.getProperty("auth.depot_admin"), true)  ;
			int finance = StringUtil.toInt(props.getProperty("auth.finance"), true)  ;
			int order = StringUtil.toInt(props.getProperty("auth.order"), true)  ;
			int goods = StringUtil.toInt(props.getProperty("auth.goods"), true)  ;
			int depot_supper = StringUtil.toInt(props.getProperty("auth.depot_supper"), true)  ;
			int customer_service = StringUtil.toInt(props.getProperty("auth.customer_service"), true)  ;
			int add_goods = StringUtil.toInt(props.getProperty("auth.add_goods"), true)  ;
			int goods_warn = StringUtil.toInt(props.getProperty("auth.goods_warn"), true)  ;
			int super_admin= StringUtil.toInt(props.getProperty("auth.super_admin"), true)  ;
	
			authMap.put("allocation",allocation); //配货下达权限
			authMap.put("depot_allocation", depot_allocation); //分店配货权限
			authMap.put("depot_ship", depot_ship); //分店发货权限 
			authMap.put("depot_admin", depot_admin); //分店库存管理权限
			authMap.put("finance", finance); //财务权限
			authMap.put("order", order); //订单管理权限
			authMap.put("goods", goods); //商品管理权限
			authMap.put("depot_supper", depot_supper); //总库管
			authMap.put("customer_service", customer_service); //客服权限
			authMap.put("add_goods", add_goods); //添加商品权限
			authMap.put("goods_warn", goods_warn); //设置商品报警数量
			authMap.put("super_admin", super_admin); //超级管理员
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static int getAuthId(String type){
		return authMap.get(type);
	}
	
}
