package com.cheriscon.app.base.core.service.impl;

import java.util.List;

import com.cheriscon.app.base.core.model.ShortMsg;
import com.cheriscon.app.base.core.plugin.shortmsg.ShortMsgPluginBundle;
import com.cheriscon.app.base.core.service.IShortMsgManager;
import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.cop.sdk.utils.DateUtil;

/**
 * 短消息管理
 * @author kingapex
 *
 */
public class ShortMsgManager extends BaseSupport<ShortMsg> implements IShortMsgManager {

	private ShortMsgPluginBundle shortMsgPluginBundle;

	@Override
	public List<ShortMsg> listNotReadMessage() {
		
		return shortMsgPluginBundle.getMessageList();
	}

	public ShortMsgPluginBundle getShortMsgPluginBundle() {
		return shortMsgPluginBundle;
	}

	public void setShortMsgPluginBundle(ShortMsgPluginBundle shortMsgPluginBundle) {
		this.shortMsgPluginBundle = shortMsgPluginBundle;
	}

	
	 

	
}
