package com.cheriscon.app.base.core.service.solution.impl;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.cheriscon.app.base.core.service.solution.ISetupLoader;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.util.FileUtil;

public class SetupLoaderImpl implements ISetupLoader {

	
	public Document load(String productId) {
		String xmlFile = CopSetting.PRODUCTS_STORAGE_PATH+"/"+productId +"/setup.xml"; 
		Document document = null;
		SAXReader saxReader = new SAXReader();
		try {
			if (FileUtil.exist(xmlFile)) {
				document = saxReader.read(new File(xmlFile));
			}

		} catch (DocumentException e) {
			System.out.println(e.getMessage());
		} 	
		return document;
		 
	}

}
