package com.cheriscon.app.base.core.service.impl.database;

import com.cheriscon.app.base.core.service.IDataBaseCreater;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.database.ISqlFileExecutor;


/**
 * mssql数据库创建者
 * @author kingapex
 *
 */
public class MssqlDataBaseCreater implements IDataBaseCreater {
	private  ISqlFileExecutor sqlFileExecutor;
	public void create() {
		if(CopSetting.RUNMODE.equals("2")){
			this.sqlFileExecutor.execute("USE [master];");
//			this.sqlFileExecutor.execute("GO;");
			this.sqlFileExecutor.execute("IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'cop')  DROP DATABASE [cop]");
			this.sqlFileExecutor.execute("CREATE DATABASE [cop]");
			this.sqlFileExecutor.execute("USE [cop]");
		}
			
		sqlFileExecutor.execute("file:com/cheriscon/cop/cop_mssql.sql");
		sqlFileExecutor.execute("file:com/cheriscon/app/shop/javashop_mssql.sql");
		sqlFileExecutor.execute("file:com/cheriscon/app/cms/cms_mssql.sql");		
	}
	public ISqlFileExecutor getSqlFileExecutor() {
		return sqlFileExecutor;
	}
	public void setSqlFileExecutor(ISqlFileExecutor sqlFileExecutor) {
		this.sqlFileExecutor = sqlFileExecutor;
	}
}
