package com.cheriscon.app.base.core.service.solution.impl;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cheriscon.app.base.core.service.solution.IInstaller;
import com.cheriscon.cop.resource.IBorderManager;
import com.cheriscon.cop.resource.model.Border;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.util.FileUtil;

/**
 * 挂件边框安装器
 * @author kingapex
 * 2010-1-25上午10:54:39
 */
public class BorderInstaller implements IInstaller {
	private IBorderManager borderManager;
 
	public void install(String productId,  Node fragment) {

		try {
		///	this.borderManager.clean();
			FileUtil.copyFolder(
					CopSetting.PRODUCTS_STORAGE_PATH + "/" + productId + "/borders/",
					CopSetting.cop_PATH +CopContext.getContext().getContextPath()+ "/borders/");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("安装borders出错...");
		}
		
		
		if (fragment.getNodeType() == Node.ELEMENT_NODE) {
			Element themeNode = (Element) fragment;
			 NodeList nodeList = themeNode.getChildNodes();
			 for( int i=0;i<nodeList.getLength();i++){
				 Node node  = nodeList.item(i);
				 if(node.getNodeType() ==  Node.ELEMENT_NODE){
					 Element el = (Element) node;
					 String id  = el.getAttribute("id");
					 String name  = el.getAttribute("name");
					 Border border = new Border();
					 border.setBorderid(id);
					 border.setBordername(name);
					 border.setThemepath(themeNode.getAttribute("id"));
					 borderManager.add(border);
				 }
			 }

		}
	}
	public void setBorderManager(IBorderManager borderManager) {
		this.borderManager = borderManager;
	}
	

}
