package com.cheriscon.app.base.core.service.dbsolution;

import java.sql.Connection;
import java.sql.SQLException;

import org.springframework.jdbc.core.JdbcTemplate;

import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.spring.SpringContextHolder;

/**
 * 数据解决方案工厂类
 * 
 * @author liuzy
 * 
 */
public class DBSolutionFactory {
	public static IDBSolution getDBSolution() {
		IDBSolution result = null;
		if (CopSetting.DBTYPE.equals("1")) {
			result = SpringContextHolder.getBean("mysqlSolution");
		} else if (CopSetting.DBTYPE.equals("2")) {
			result = SpringContextHolder.getBean("oracleSolution");
		} else if (CopSetting.DBTYPE.equals("3")) {
			result = SpringContextHolder.getBean("sqlserverSolution");
		} else
			throw new RuntimeException("未知的数据库类型");

		return result;
	}
	
	public static Connection getConnection(JdbcTemplate jdbcTemplate){
		if(jdbcTemplate==null)
			jdbcTemplate =  SpringContextHolder.getBean("jdbcTemplate");
		
		try {
			return jdbcTemplate.getDataSource().getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean dbImport(String xml, String prefix) {
		Connection conn = getConnection(null);
		IDBSolution dbsolution = getDBSolution();
		dbsolution.setPrefix(prefix);
		dbsolution.setConnection(conn);
		boolean result;
		if(CopSetting.RUNMODE.equals("1"))
			result = dbsolution.dbImport(xml);
		else{
			CopSite site = CopContext.getContext().getCurrentSite();
			Integer userid = site.getUserid();
			Integer siteid = site.getId();
			result = dbsolution.dbSaasImport(xml, userid, siteid);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return result;
	}
	
	public static String dbExport(String[] tables,boolean dataOnly,String prefix) {
		Connection conn = getConnection(null);
		IDBSolution dbsolution = getDBSolution();
		dbsolution.setPrefix(prefix);
		dbsolution.setConnection(conn);
		String result = "";
		if(CopSetting.RUNMODE.equals("1")){
			result = dbsolution.dbExport(tables,dataOnly);
		}else{
			CopSite site = CopContext.getContext().getCurrentSite();
			Integer userid = site.getUserid();
			Integer siteid = site.getId();
			result = dbsolution.dbSaasExport(tables, dataOnly, userid, siteid);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}
	
//	public static boolean dbSaasImport(String xml, int userid, int siteid, String prefix){
//		Connection conn = getConnection(null);
//		IDBSolution dbsolution = getDBSolution();
//		dbsolution.setPrefix(prefix);
//		dbsolution.setConnection(conn);
//		boolean result = dbsolution.dbSaasImport(xml, userid, siteid);
//		try {
//			conn.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return false;
//		}
//		return result;
//	}
}
