package com.cheriscon.app.base.core.service.solution.impl;

import org.apache.commons.beanutils.BeanUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cheriscon.app.base.core.service.solution.IInstaller;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;

public class SiteInstaller implements IInstaller {
	private ISiteManager siteManager;
	
	private boolean setProperty(CopSite site,String name,String value) {
		try {
			BeanUtils.setProperty(site, name, value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Override
	public void install(String productId, Node fragment) {
		CopSite site = CopContext.getContext().getCurrentSite();
		
		NodeList nodeList  = fragment.getChildNodes();
		for(int i=0,len=nodeList.getLength();i<len;i++){
			Node node = nodeList.item(i);
			if(node.getNodeType()==Node.ELEMENT_NODE) {
				Element element = (Element)node;
				String name = element.getAttribute("name");
				String value = element.getAttribute("value");
				setProperty(site,name,value);
			}
		}
		siteManager.edit(site);
	}

	public ISiteManager getSiteManager() {
		return siteManager;
	}

	public void setSiteManager(ISiteManager siteManager) {
		this.siteManager = siteManager;
	}

}
