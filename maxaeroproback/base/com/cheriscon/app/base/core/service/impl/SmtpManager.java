package com.cheriscon.app.base.core.service.impl;

import java.util.List;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.util.StringUtil;

/**
 * smtp管理
 * @author kingapex
 * @date 2011-11-1 下午12:10:30 
 * @version V1.0
 */
public class SmtpManager extends BaseSupport<Smtp> implements ISmtpManager {

	@Override
	public void add(Smtp smtp) {
		this.baseDaoSupport.insert("smtp", smtp);
	}

	@Override
	public void edit(Smtp smtp) {
		this.baseDaoSupport.update("smtp", smtp,"id="+smtp.getId());
	}

	@Override
	public void delete(Integer[] idAr) {
		
		if(idAr==null || idAr.length==0) return;
		String idstr = StringUtil.arrayToString(idAr, ",");
		
		this.baseDaoSupport.execute("delete from smtp where id in("+idstr+")");
		
	}

	@Override
	public List<Smtp> list() {
		
		return this.baseDaoSupport.queryForList("select * from smtp", Smtp.class);
	}

	@Override
	public void sendOneMail(Smtp currSmtp) {
		this.baseDaoSupport.update("smtp", currSmtp, "id="+currSmtp.getId());
	}

	public Smtp getCurrentSmtp(String support){
		String sql="select distinct s.* from es_smtp s,DT_ComplaintAdminuserInfo b where s.id=b.emailid and b.adminUserid=?";
		return this.baseDaoSupport.queryForObject(sql,Smtp.class,support);
	}
	

	/**
	 * 根据邮箱号查询账号信息
	 * @return
	 */
	public Smtp getSmtpByEmail(String email){
		String sql="select * from es_smtp s where username=?";
		return this.baseDaoSupport.queryForObject(sql,Smtp.class,email);
	}
	
	
	public Smtp get(int id){
		return this.baseDaoSupport.queryForObject("select * from smtp where id=?", Smtp.class, id);
	}

	@Override
	public Smtp getCurrentSmtp() {
		// TODO Auto-generated method stub
		return null;
	}
}
