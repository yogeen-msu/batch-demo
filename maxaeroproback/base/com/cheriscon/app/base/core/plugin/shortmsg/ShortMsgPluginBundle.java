package com.cheriscon.app.base.core.plugin.shortmsg;

import java.util.ArrayList;
import java.util.List;

import com.cheriscon.app.base.core.model.ShortMsg;
import com.cheriscon.framework.plugin.AutoRegisterPluginsBundle;
import com.cheriscon.framework.plugin.IPlugin;

/**
 * 短消息插件桩
 * @author kingapex
 *
 */
public class ShortMsgPluginBundle extends AutoRegisterPluginsBundle {

	@Override
	public String getName() {
		return "短消息插件桩";
	}
	
	public List<ShortMsg> getMessageList(){
		List<ShortMsg> msgList  = new ArrayList<ShortMsg>();
		List<IPlugin> plugins = this.getPlugins();
		
		if (plugins != null) {
			for (IPlugin plugin : plugins) {
				if(plugin instanceof IShortMessageEvent){
					IShortMessageEvent event = (IShortMessageEvent)plugin;
					List<ShortMsg> somemsgList = event.getMessage();
					msgList.addAll(somemsgList);
				}
			}
		}
		return msgList;
	}

}
