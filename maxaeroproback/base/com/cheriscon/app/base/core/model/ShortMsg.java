package com.cheriscon.app.base.core.model;


/**
 * 短消息 
 * @author kingapex
 *
 */
public class ShortMsg {
	private String title;
	private String url;
	private String target;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	 
}
