package com.cheriscon.app.base.core.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.Theme;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.sun.jmx.snmp.ThreadContext;

/**
 * 站点主题管理
 * 
 * @author lzf
 *         <p>
 *         2009-12-30 上午11:01:08
 *         </p>
 * @version 1.0
 */
public class SiteThemeAction extends WWAction {

	private List<Theme> listTheme;
	private Theme theme;
	private IThemeManager themeManager;
	private CopSite CopSite;
	private ISiteManager siteManager;
	private String previewpath;
	private String previewBasePath;
	private Integer themeid;

	
	public String execute() throws Exception {
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		String ctx = request.getContextPath();
		CopSite site  = CopContext.getContext().getCurrentSite();
		String contextPath = CopContext.getContext().getContextPath();
	 
		previewBasePath = ctx+contextPath+ "/themes/";


		theme = themeManager.getTheme( site.getThemeid());
		listTheme = themeManager.list();
		previewpath = previewBasePath + theme.getPath() + "/preview.png";
		return SUCCESS;
	}
	
	
	public String add(){
		
		return this.INPUT;
	}
	
	public String save(){
		this.msgs.add("模板创建成功");
		this.urls.put("模板列表", "siteTheme.do");
		this.themeManager.addBlank(theme);
		return this.MESSAGE;
	}
	
	public String change()throws Exception {
		siteManager.changeTheme( themeid);
		return this.execute(); 
	}

	public List<Theme> getListTheme() {
		return listTheme;
	}

	public void setListTheme(List<Theme> listTheme) {
		this.listTheme = listTheme;
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	public IThemeManager getThemeManager() {
		return themeManager;
	}

	public void setThemeManager(IThemeManager themeManager) {
		this.themeManager = themeManager;
	}

	public CopSite getCopSite() {
		return CopSite;
	}

	public void setCopSite(CopSite CopSite) {
		this.CopSite = CopSite;
	}

	public ISiteManager getSiteManager() {
		return siteManager;
	}

	public void setSiteManager(ISiteManager siteManager) {
		this.siteManager = siteManager;
	}

	public String getPreviewpath() {
		return previewpath;
	}

	public void setPreviewpath(String previewpath) {
		this.previewpath = previewpath;
	}

	public String getPreviewBasePath() {
		return previewBasePath;
	}

	public void setPreviewBasePath(String previewBasePath) {
		this.previewBasePath = previewBasePath;
	}

	public Integer getThemeid() {
		return themeid;
	}

	public void setThemeid(Integer themeid) {
		this.themeid = themeid;
	}
 

}
