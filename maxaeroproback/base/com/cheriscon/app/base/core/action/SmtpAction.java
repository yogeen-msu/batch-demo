package com.cheriscon.app.base.core.action;

import java.util.List;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.framework.action.WWAction;


/**
 * smtp管理 
 * @author kingapex
 * @date 2011-11-1 下午12:27:51 
 * @version V1.0
 */
public class SmtpAction extends WWAction {
	
	private ISmtpManager smtpManager;
	private Smtp smtp;
	private Integer[] idAr;
	private int id;
	private int isedit ;
	private List<Smtp> smtpList;
	
	public String add(){
		isedit=0;
		return this.INPUT;
	}

	public String edit(){
		isedit=1;
		this.smtp = this.smtpManager.get(id);
		return this.INPUT;
	}
	
	
	public String saveAdd(){
		
		try{
			this.smtpManager.add(smtp);
			this.msgs.add(getText("stmp.message.success"));
			this.urls.put(getText("stmp.message.list"), "smtp!list.do");
		}catch(RuntimeException e){
			this.logger.error(getText("stmp.message.error"), e);
			this.msgs.add(getText("stmp.message.error")+ e.getMessage());
		}
		
		return this.MESSAGE;
	}
	
	
	public String saveEdit(){
		try{
			this.smtpManager.edit(smtp);
			this.msgs.add(getText("stmp.message.success1"));
			this.urls.put(getText("stmp.message.list"), "smtp!list.do");
		}catch(RuntimeException e){
			this.logger.error(getText("stmp.message.error1"), e);
			this.msgs.add(getText("stmp.message.error1")+ e.getMessage());
		}
		
		return this.MESSAGE;
	}
	  
	
	public String list(){
		 smtpList = this.smtpManager.list();		
		return "list";
	}

	public String delete(){
		try{
			this.smtpManager.delete(idAr);
			this.json="{result:0,message:'"+getText("common.js.del")+"'}";
		}catch(RuntimeException e){
			this.logger.error(getText("stmp.message.error2"), e);
			this.showSuccessJson(getText("stmp.message.error2")+e.getMessage()+"]");
		} 
		return JSON_MESSAGE;
	}

	public ISmtpManager getSmtpManager() {
		return smtpManager;
	}

	public void setSmtpManager(ISmtpManager smtpManager) {
		this.smtpManager = smtpManager;
	}

	public Smtp getSmtp() {
		return smtp;
	}

	public void setSmtp(Smtp smtp) {
		this.smtp = smtp;
	}

	public Integer[] getIdAr() {
		return idAr;
	}

	public void setIdAr(Integer[] idAr) {
		this.idAr = idAr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIsedit() {
		return isedit;
	}

	public void setIsedit(int isedit) {
		this.isedit = isedit;
	}

	public List<Smtp> getSmtpList() {
		return smtpList;
	}

	public void setSmtpList(List<Smtp> smtpList) {
		this.smtpList = smtpList;
	}
	
}
