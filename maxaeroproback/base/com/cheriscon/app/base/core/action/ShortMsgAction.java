package com.cheriscon.app.base.core.action;

import java.util.List;

import net.sf.json.JSONArray;

import com.cheriscon.app.base.core.model.ShortMsg;
import com.cheriscon.app.base.core.service.IShortMsgManager;
import com.cheriscon.framework.action.WWAction;

/**
 * 短消息action
 * @author kingapex
 *
 */
public class ShortMsgAction extends WWAction {
	
	private IShortMsgManager shortMsgManager ;
	private List<ShortMsg> msgList;
	
	
	/**
	 * 读取所有未读消息
	 * @return
	 */
	public String listNew(){
		msgList = this.shortMsgManager.listNotReadMessage();
		this.json = JSONArray.fromObject(JSONArray.fromObject(msgList)).toString();
		return this.JSON_MESSAGE;
	}


	public IShortMsgManager getShortMsgManager() {
		return shortMsgManager;
	}




	public void setShortMsgManager(IShortMsgManager shortMsgManager) {
		this.shortMsgManager = shortMsgManager;
	}




	public List<ShortMsg> getMsgList() {
		return msgList;
	}




	public void setMsgList(List<ShortMsg> msgList) {
		this.msgList = msgList;
	}
	
	
	   
	
	
}
