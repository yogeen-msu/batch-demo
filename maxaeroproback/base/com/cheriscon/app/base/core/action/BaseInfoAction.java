package com.cheriscon.app.base.core.action;

import java.io.File;

import com.cheriscon.cop.resource.IUserManager;
import com.cheriscon.cop.resource.model.CopUser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.utils.UploadUtil;
import com.cheriscon.framework.action.WWAction;

/**
 * User基本信息维护
 * 
 * @author lzf
 *         <p>
 *         created_time 2009-12-4 上午10:37:58
 *         </p>
 * @version 1.0
 */
public class BaseInfoAction extends WWAction {

	private CopUser CopUser;
	private Integer userid;

	private IUserManager userManager;
	private File cologo;		
	public File getCologo() {
		return cologo;
	}

	public void setCologo(File cologo) {
		this.cologo = cologo;
	}

	public String getCologoFileName() {
		return cologoFileName;
	}

	public void setCologoFileName(String cologoFileName) {
		this.cologoFileName = cologoFileName;
	}

	private String cologoFileName;
	private File license;		//营业执照
	private String licenseFileName;

	public File getLicense() {
		return license;
	}

	public void setLicense(File license) {
		this.license = license;
	}

	public String getLicenseFileName() {
		return licenseFileName;
	}

	public void setLicenseFileName(String licenseFileName) {
		this.licenseFileName = licenseFileName;
	}
 

	public CopUser getCopUser() {
		return CopUser;
	}

	public void setCopUser(CopUser CopUser) {
		this.CopUser = CopUser;
	}

 
	
	public String execute() throws Exception {
		this.userid =CopContext.getContext().getCurrentSite().getUserid();
		CopUser = userManager.get(userid);
		return "input";
	}

	public String save() throws Exception {

		
		try {
			if (cologo != null) {
				String logoPath = UploadUtil.upload(cologo, cologoFileName,
						"user");
				CopUser.setLogofile(logoPath);

			}

			if (license != null) {

				String licensePath = UploadUtil.upload(license,
						licenseFileName, "user");
				CopUser.setLicensefile(licensePath);

			}

			this.userManager.edit(CopUser);
			this.msgs.add("修改成功");
			
		} catch (RuntimeException e) {
			this.msgs.add(e.getMessage());
		}
		this.urls.put("用户信息页面", "baseInfo.do");
		return "message";
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public IUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(IUserManager userManager) {
		this.userManager = userManager;
	}

	
}
