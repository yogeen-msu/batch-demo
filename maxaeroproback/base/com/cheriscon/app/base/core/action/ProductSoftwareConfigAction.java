package com.cheriscon.app.base.core.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.cheriscon.backstage.product.service.IProductSoftwareConfigMemoService;
import com.cheriscon.backstage.product.service.IProductSoftwareConfigService;
import com.cheriscon.backstage.system.service.ILanguageService;
import com.cheriscon.common.model.Language;
import com.cheriscon.common.model.ProductSoftwareConfig;
import com.cheriscon.common.model.ProductSoftwareConfigMemo;
import com.cheriscon.common.utils.JSONHelpUtils;
import com.cheriscon.cop.resource.IMenuManager;
import com.cheriscon.cop.resource.model.Menu;
import com.cheriscon.framework.action.WWAction;
/**
 * 虚拟目录管理
 * @author pengdongan
 * 2013-11-28
 */
public class ProductSoftwareConfigAction extends WWAction {
	private IMenuManager menuManager;
	
	@Resource
	private IProductSoftwareConfigService productSoftwareConfigService;
	@Resource
	private IProductSoftwareConfigMemoService configMemoService;
	@Resource
	private ILanguageService  languageService;
	
	private List<ProductSoftwareConfig> productSoftwareConfigList;
	private String parentCode;
	private String productSoftwareConfigCode;
	private ProductSoftwareConfig productSoftwareConfig;
	private List<ProductSoftwareConfigMemo> configMemo;
	private List<Language> language;
	private List<Menu> menuList;
	private Menu menu;
	private Integer parentid;
	private Integer id;
	private Integer[] menu_ids;
	private Integer[] menu_sorts;
	private String jsonStr;
	public String list(){
		try {
			if(productSoftwareConfig != null)
			{
				productSoftwareConfigList = productSoftwareConfigService.getTreeByProductName(productSoftwareConfig.getName());
			}
			else
			{
				productSoftwareConfig = new ProductSoftwareConfig();
				productSoftwareConfigList = productSoftwareConfigService.getProductSoftwareConfigTree("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "list";
	}
	
	public String jsonTree(){
		try {
			List<ProductSoftwareConfig> productSoftwareConfigParentList;
			
			productSoftwareConfig = productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfigCode);
			productSoftwareConfigList = productSoftwareConfigService.getProductSoftwareConfigTree(productSoftwareConfigCode);
			
			if(productSoftwareConfig != null)
			{
				productSoftwareConfigParentList = new ArrayList<ProductSoftwareConfig>();
				productSoftwareConfigParentList.add(productSoftwareConfig);
				productSoftwareConfig.setChildren(productSoftwareConfigList);
			}
			else
			{
				productSoftwareConfigParentList = productSoftwareConfigList;
			}
			
			List<Object> list = new ArrayList<Object>();
			list.add(productSoftwareConfigParentList);
			this.json = JSONHelpUtils.getJsonString4JavaPOJOList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.JSON_MESSAGE;
	}
	
	public String add(){
		try {
			productSoftwareConfigList = productSoftwareConfigService.getProductSoftwareConfigTree("0");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "add";
	}
	
	//跳转到维护消息推送界面
	public String addUpgrade(){
		try {
			productSoftwareConfig = productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfigCode);
			configMemo=configMemoService.getProductSoftwareConfigMemo(productSoftwareConfigCode);
			language=languageService.queryLanguage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "addUpgrade";
	}
	
	public String saveUpgradeMemo(){
		
		try {
			JSONArray array = JSONArray.fromObject(jsonStr);
			if(array!=null ){
				configMemoService.delProductSoftwareConfigMemo(productSoftwareConfigCode);
				for(int i=0;i<array.size();i++){
					ProductSoftwareConfigMemo memo=new ProductSoftwareConfigMemo();
					JSONObject object = JSONObject.fromObject(array.get(i));
					memo.setSoftConfigCode(productSoftwareConfigCode);
					memo.setLanguageCode(object.get("languageCode")==null?"":object.get("languageCode").toString());
					memo.setName(object.get("name")==null?"":object.get("name").toString());
					configMemoService.addProductSoftwareConfigMemo(memo);
					this.msgs.add("操作成功");
					this.urls.put("返回列表", "productSoftwareConfig!list.do");
				}
			}
		} catch (Exception e) {
			this.msgs.add("操作失败");
			this.urls.put("返回", "javascript:history.go(-1);");
		}
		
		
		return this.MESSAGE;
	}
	
	public String saveAdd(){
		try{
			if(productSoftwareConfigService.isSameName(productSoftwareConfig))
			{
				this.json="{result:2}";
			}
			else
			{
				productSoftwareConfigCode = productSoftwareConfigService.add(productSoftwareConfig);
				this.json="{result:1,code:\""+productSoftwareConfigCode+"\"}";
			}
		}catch(Exception e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}
		return this.JSON_MESSAGE;
	}
	
	
	public String edit(){

		try {
			productSoftwareConfigList  = productSoftwareConfigService.getProductSoftwareConfigTree("0");
			productSoftwareConfig = productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfigCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "edit";
	}
 
	
	public String saveEdit(){
		try{
			if(productSoftwareConfigService.listProductSoftwareConfigByParentCode(productSoftwareConfig.getCode()).size() > 0 && isEditParentCode(productSoftwareConfig))
			{
				this.json="{result:4}";
			}
			else if(productSoftwareConfigService.isSameName(productSoftwareConfig))
			{
				this.json="{result:2}";
			}
			else
			{
				productSoftwareConfigService.update(productSoftwareConfig);
				this.json="{result:1}";
			}
		}catch(Exception e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}
		return this.JSON_MESSAGE;
	}
	
	public String delete(){
		try{
			if(productSoftwareConfigService.listProductSoftwareConfigByParentCode(productSoftwareConfigCode).size() > 0)
			{
				this.json="{result:3}";
			}
			else
			{
				productSoftwareConfigService.delProductSoftwareConfig(productSoftwareConfigCode);
				this.json="{result:1}";
			}
		}catch(Exception e){
			this.logger.error(e.getMessage(), e);
			this.json="{result:0,message:'"+e.getMessage()+"'}";
		}		
		return this.JSON_MESSAGE;
	}
	
	
	private boolean isEditParentCode(ProductSoftwareConfig productSoftwareConfig) throws Exception{
		boolean result = true;
		
		if(productSoftwareConfig.getParentCode().equals(productSoftwareConfigService.getProductSoftwareConfigByCode(productSoftwareConfig.getCode()).getParentCode()))
		{
			result = false;
		}
		
		return result;
	}

	public IMenuManager getMenuManager() {
		return menuManager;
	}

	public void setMenuManager(IMenuManager menuManager) {
		this.menuManager = menuManager;
	}

	public List<Menu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public Integer[] getMenu_ids() {
		return menu_ids;
	}

	public void setMenu_ids(Integer[] menuIds) {
		menu_ids = menuIds;
	}

	public Integer[] getMenu_sorts() {
		return menu_sorts;
	}

	public void setMenu_sorts(Integer[] menuSorts) {
		menu_sorts = menuSorts;
	}

	public List<ProductSoftwareConfig> getProductSoftwareConfigList() {
		return productSoftwareConfigList;
	}

	public void setProductSoftwareConfigList(
			List<ProductSoftwareConfig> productSoftwareConfigList) {
		this.productSoftwareConfigList = productSoftwareConfigList;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getProductSoftwareConfigCode() {
		return productSoftwareConfigCode;
	}

	public void setProductSoftwareConfigCode(String productSoftwareConfigCode) {
		this.productSoftwareConfigCode = productSoftwareConfigCode;
	}

	public ProductSoftwareConfig getProductSoftwareConfig() {
		return productSoftwareConfig;
	}

	public void setProductSoftwareConfig(ProductSoftwareConfig productSoftwareConfig) {
		this.productSoftwareConfig = productSoftwareConfig;
	}

	public List<ProductSoftwareConfigMemo> getConfigMemo() {
		return configMemo;
	}

	public void setConfigMemo(List<ProductSoftwareConfigMemo> configMemo) {
		this.configMemo = configMemo;
	}

	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}

	public List<Language> getLanguage() {
		return language;
	}

	public void setLanguage(List<Language> language) {
		this.language = language;
	}
	
	
}
