package com.cheriscon.app.base.core.action;

import com.cheriscon.cop.resource.IUserDetailManager;
import com.cheriscon.cop.resource.model.CopUserDetail;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.action.WWAction;

/**
 * @author lzf
 * <p>created_time 2009-12-4 下午03:55:33</p>
 * @version 1.0
 */
public class UserDetailAction extends WWAction {
	
	private CopUserDetail CopUserDetail;
	private IUserDetailManager userDetailManager;
	private Integer userid;
	public String execute() throws Exception {
		this.userid =CopContext.getContext().getCurrentSite().getUserid();
		CopUserDetail =userDetailManager.get(userid);
		return "input";
	} 
	
	public String save() throws Exception {
		try{
		userDetailManager.edit(CopUserDetail);
		this.msgs.add("修改成功");
		} catch (RuntimeException e) {
			this.msgs.add(e.getMessage());

		}
		this.urls.put("用户信息页面", "userDetail.do");
 
		return this.MESSAGE;
	}

	public CopUserDetail getCopUserDetail() {
		return CopUserDetail;
	}

	public void setCopUserDetail(CopUserDetail CopUserDetail) {
		this.CopUserDetail = CopUserDetail;
	}

	public IUserDetailManager getUserDetailManager() {
		return userDetailManager;
	}

	public void setUserDetailManager(IUserDetailManager userDetailManager) {
		this.userDetailManager = userDetailManager;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	
	
}
