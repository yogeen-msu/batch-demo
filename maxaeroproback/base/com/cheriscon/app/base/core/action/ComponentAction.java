package com.cheriscon.app.base.core.action;

import java.util.List;

import com.cheriscon.framework.action.WWAction;
import com.cheriscon.framework.component.ComponentView;
import com.cheriscon.framework.component.IComponentManager;


/**
 * 组件管理action
 * @author kingapex
 *
 */
public class ComponentAction extends WWAction {
	
	private IComponentManager componentManager;
	private List<ComponentView> componentList;
	private String componentid;
	
	
	public String list(){
		componentList= this.componentManager.list();
		return "list";
	}
	
	
	
	
	/**
	 * 安装
	 * @return
	 */
	public String install(){
		try{
			this.componentManager.install(componentid);
			this.showSuccessJson("安装成功");
		}catch(RuntimeException e){
			this.logger.error("安装组件["+componentid+"]",e);
			this.showErrorJson(e.getMessage());
		}
		return this.JSON_MESSAGE;
	}
	
	
	
	
	/**
	 * 卸载
	 * @return
	 */
	public String unInstall(){
		try{
			this.componentManager.unInstall(componentid);
			this.showSuccessJson("卸载成功");
		}catch(RuntimeException e){
			this.logger.error("卸载组件["+componentid+"]",e);
			this.showErrorJson(e.getMessage());
		}
		return this.JSON_MESSAGE;
	}
	
	
	
	
	/**
	 * 启用
	 * @return
	 */
	public String start(){
		try{
			this.componentManager.start(componentid);
			this.showSuccessJson("启动成功");
		}catch(RuntimeException e){
			this.logger.error("启动组件["+componentid+"]",e);
			this.showErrorJson(e.getMessage());
		}
		return this.JSON_MESSAGE;
	}
	
	
	/**
	 * 停用
	 * @return
	 */
	public String stop(){
		try{
			this.componentManager.stop(componentid);
			this.showSuccessJson("停用成功");
		}catch(RuntimeException e){
			this.logger.error("停用组件["+componentid+"]",e);
			this.showErrorJson(e.getMessage());
		}
		return this.JSON_MESSAGE;
	}
	
	
	
	public IComponentManager getComponentManager() {
		return componentManager;
	}
	public void setComponentManager(IComponentManager componentManager) {
		this.componentManager = componentManager;
	}

	public List<ComponentView> getComponentList() {
		return componentList;
	}

	public void setComponentList(List<ComponentView> componentList) {
		this.componentList = componentList;
	}

	public String getComponentid() {
		return componentid;
	}

	public void setComponentid(String componentid) {
		this.componentid = componentid;
	}
	
	

}
