package com.cheriscon.app.base.core.action;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.statistics.LiveCacheStatistics;

import com.cheriscon.framework.action.WWAction;

public class CacheAction extends WWAction {

	 public String execute(){
		  CacheManager manager = CacheManager.getInstance();
		  Cache  cache = manager.getCache("widgetCache");	
		//  cache.setStatisticsEnabled(true);
		 LiveCacheStatistics statistis = cache.getLiveCacheStatistics();
		 boolean memory =statistis.isStatisticsEnabled();
	 
//		  System.out.println("memory:"+memory);
//		  System.out.println("getLocalHeapSizeInBytes："+statistis.getLocalHeapSizeInBytes());
//		  System.out.println("getLocalHeapSize："+statistis.getLocalHeapSize());
//		  
//		  System.out.println("getMemoryStoreSize:"+cache.getMemoryStoreSize());
//		  System.out.println("getOffHeapStoreSize:"+cache.getOffHeapStoreSize());
//		  System.out.println("getSize:"+cache.getSize());
//		  System.out.println("getMemoryStoreSize:"+cache.getMemoryStoreSize());
		  return "list"; 
	 } 

}
