package com.cheriscon.app.base.core.action;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import com.cheriscon.app.base.core.plugin.user.AdminUserPluginBundle;
import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.app.base.core.service.auth.IPermissionManager;
import com.cheriscon.app.base.core.service.auth.IRoleManager;
import com.cheriscon.backstage.member.service.ISealerInfoService;
import com.cheriscon.backstage.system.service.IAdminUserAutelService;
import com.cheriscon.common.model.ComplaintAdminuserInfo;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.action.WWAction;

/**
 * 站点管理员管理
 * @author kingapex
 * 2010-11-5下午04:28:22新增角色管理
 */
public class UserAdminAction extends WWAction {
	private AdminUserPluginBundle adminUserPluginBundle;
	private IAdminUserManager adminUserManager;
	private IRoleManager roleManager;
	private IPermissionManager permissionManager;
	private AdminUser adminUser;
	private Integer id;
	private List roleList;
	private List userRoles;
	private int[] roleids;
	private List userList;
	private String newPassword; //新密码
	private String updatePwd;//是否修改密码
	private int multiSite;
	private List<String> htmlList;
	private String jsonData;

	@Resource
	private IAdminUserAutelService adminUserAutelService;
	
	public String execute() {
//		userList= this.adminUserManager.list();
//		return SUCCESS;
		
		this.webpage = adminUserAutelService.pageAdminUserpage(adminUser, this.getPage(), this.getPageSize());
		return "autel";
	}

	public String add() throws Exception {
		
		multiSite =CopContext.getContext().getCurrentSite().getMulti_site();
		roleList = roleManager.list();
		this.htmlList = this.adminUserPluginBundle.getInputHtml(null);
		//return "add";
		
		return "addautel";
	}
	
	public String addSave() throws Exception {
		try{
			adminUser.setRoleids(roleids);
			adminUserManager.add(adminUser);
			this.msgs.add(getText("common.js.addSuccess"));
			this.urls.put(getText("common.js.returnlist"), "userAdmin.do");
		 } catch (RuntimeException e) {
			this.msgs.add(e.getMessage());
		}	
			return this.MESSAGE;
	}

	public String edit() throws Exception {
		multiSite =CopContext.getContext().getCurrentSite().getMulti_site();
		roleList = roleManager.list();
		this.userRoles =permissionManager.getUserRoles(id);
		adminUser = this.adminUserManager.get(id);
		this.htmlList = this.adminUserPluginBundle.getInputHtml(adminUser);
		//return "edit";
		
		return "editautel";
	}

	public String editSave() throws Exception {
		try {
			if(updatePwd!=null){
				adminUser.setPassword(newPassword);
			}
			adminUser.setRoleids(roleids);
			this.adminUserManager.edit(adminUser);
			this.msgs.add(getText("common.js.modSuccess"));
		} catch (RuntimeException e) {
			e.printStackTrace();
			this.logger.error(e,e.fillInStackTrace());
			this.msgs.add(getText("common.js.modFail")+":"+e.getMessage());
		}
		this.urls.put(getText("common.js.returnlist"), "userAdmin.do");

		return this.MESSAGE;
	}
	

	public String delete() throws Exception {
		try {
			this.adminUserManager.delete(id);
			this.msgs.add(getText("common.js.del"));
			this.urls.put(getText("common.js.returnlist"), "userAdmin.do");
		} catch (RuntimeException e) {
			this.msgs.add(getText("common.js.delFail")+":"+e.getMessage());
			this.urls.put(getText("common.js.returnlist"), "userAdmin.do");
		}

		return this.MESSAGE;
	}
	

	public String usernameExist() throws Exception {
		int result = -1;
		try {
			AdminUser adminUser2 = this.adminUserManager.getAdminUserByUsername(adminUser.getUsername());
			if(adminUser2 == null || adminUser2.getUserid().equals(adminUser.getUserid()))
			{
				result = 0;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.jsonData =  JSONArray.fromObject(result).toString();
		
		return "usernameExist";
	}

	public String editPassword() throws Exception {
		return "editPassword";
	}



	public IAdminUserManager getAdminUserManager() {
		return adminUserManager;
	}

	public void setAdminUserManager(IAdminUserManager adminUserManager) {
		this.adminUserManager = adminUserManager;
	}

	public IRoleManager getRoleManager() {
		return roleManager;
	}

	public void setRoleManager(IRoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public IPermissionManager getPermissionManager() {
		return permissionManager;
	}

	public void setPermissionManager(IPermissionManager permissionManager) {
		this.permissionManager = permissionManager;
	}

	public AdminUser getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(AdminUser adminUser) {
		this.adminUser = adminUser;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List getRoleList() {
		return roleList;
	}

	public void setRoleList(List roleList) {
		this.roleList = roleList;
	}

	public List getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List userRoles) {
		this.userRoles = userRoles;
	}

	public int[] getRoleids() {
		return roleids;
	}

	public void setRoleids(int[] roleids) {
		this.roleids = roleids;
	}

	public List getUserList() {
		return userList;
	}

	public void setUserList(List userList) {
		this.userList = userList;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getUpdatePwd() {
		return updatePwd;
	}

	public void setUpdatePwd(String updatePwd) {
		this.updatePwd = updatePwd;
	}

	public int getMultiSite() {
		return multiSite;
	}

	public void setMultiSite(int multiSite) {
		this.multiSite = multiSite;
	}

	public AdminUserPluginBundle getAdminUserPluginBundle() {
		return adminUserPluginBundle;
	}

	public void setAdminUserPluginBundle(AdminUserPluginBundle adminUserPluginBundle) {
		this.adminUserPluginBundle = adminUserPluginBundle;
	}

	public List<String> getHtmlList() {
		return htmlList;
	}

	public void setHtmlList(List<String> htmlList) {
		this.htmlList = htmlList;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
 
	
}
