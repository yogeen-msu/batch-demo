package com.cheriscon.cop.sdk.listener;

import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.component.IComponent;
import com.cheriscon.framework.component.IComponentManager;
import com.cheriscon.framework.component.context.WidgetContext;
import com.cheriscon.framework.context.spring.SpringContextHolder;

/**
 * copLinstener 负责初始化站点缓存
 * 只有saas版本有效
 * @author kingapex
 * 2010-7-18下午04:01:16
 */
public class CopContextLoaderListener implements ServletContextListener {

	
	public void contextDestroyed(ServletContextEvent event) {

	}
	
	public void contextInitialized(ServletContextEvent event) {
		
		if( CopSetting.INSTALL_LOCK.toUpperCase().equals("YES") && "1".equals(CopSetting.RUNMODE)){
			IComponentManager componentManager = SpringContextHolder.getBean("componentManager");
			componentManager.startComponents();
		}
		
		//saas式并且已经安装完成
		if("2".equals(CopSetting.RUNMODE) &&  CopSetting.INSTALL_LOCK.toUpperCase().equals("YES")  ){
			ISiteManager siteManager = SpringContextHolder.getBean("siteManager");
			siteManager.getDnsList();
		}
		
		
	}

}
