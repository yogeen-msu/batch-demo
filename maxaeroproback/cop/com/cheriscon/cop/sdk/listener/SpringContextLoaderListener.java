package com.cheriscon.cop.sdk.listener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.util.FileUtil;

public class SpringContextLoaderListener extends ContextLoaderListener {
	
	public SpringContextLoaderListener(){
		
		super();
	}
	 public void contextInitialized(ServletContextEvent event){
		 InputStream in  = FileUtil.getResourceAsStream("cop.properties");
			Properties props = new Properties();
			try {
				props.load(in);
				String servletPath = event.getServletContext().getRealPath("").replace("\\", "/");
//				覆盖目录路径设置
				props.put("path.imageserver",servletPath+"/statics");
				props.put("storage.products",servletPath+"/products");
				props.put("storage.app_data",servletPath+"/products/commons");
				props.put("storage.copServer",servletPath);
				props.put("path.context_path",event.getServletContext().getContextPath());
//				初始化常量设置
				CopSetting.init(props);
				in.close();
			} catch (IOException e) {
				if(in!=null){
					try {
						in.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				throw new RuntimeException(e);
			}
			super.contextInitialized(event);
	 }

}
