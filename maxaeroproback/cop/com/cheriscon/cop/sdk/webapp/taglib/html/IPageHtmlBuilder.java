package com.cheriscon.cop.sdk.webapp.taglib.html;

public interface IPageHtmlBuilder {
	public String buildPageHtml();
	public void setUrl(String url); 
}
