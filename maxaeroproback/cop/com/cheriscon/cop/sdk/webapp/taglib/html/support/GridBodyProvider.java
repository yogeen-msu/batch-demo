package com.cheriscon.cop.sdk.webapp.taglib.html.support;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.PageContext;

import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.webapp.bean.Grid;
import com.cheriscon.cop.sdk.webapp.taglib.IListTaglibParam;
import com.cheriscon.cop.sdk.webapp.taglib.IListTaglibProvider;
import com.cheriscon.framework.database.Page;

public class GridBodyProvider implements IListTaglibProvider {
	public List getData(IListTaglibParam _param, PageContext pageContext) {

		GridBodyParam param = (GridBodyParam) _param;
		String from = param.getFrom();

		Object obj = pageContext.getAttribute(from);
		if (obj == null){
			obj = pageContext.getRequest().getAttribute(from);
			if (obj == null){
				return new ArrayList();
			}
		}
		//	from	即可以是Page对象，也可以是Grid对象。
		Page page = null;
		List list = null;
		if(obj instanceof Page){
			page = (Page)obj;
			list  = (List) page.getResult();
		}
		else if(obj instanceof Grid ){
			page = ((Grid)obj).getWebpage();
			list  = (List) page.getResult();
		}else if(obj instanceof List){
			list = (List)obj;
		}
		
		String tempPage = pageContext.getRequest().getParameter("page");
		int pageNo = 1;
		if(tempPage!=null && !tempPage.toString().equals("")){
			pageNo = Integer.valueOf(tempPage.toString());
			pageNo = (pageNo > 0) ? pageNo : 1;
		}
		pageContext.setAttribute("pageNo",pageNo);
		
		int pageSize = Integer.parseInt(CopSetting.BACKEND_PAGESIZE);
		if(page != null){
			pageSize = page.getPageSize();
		}
		pageContext.setAttribute("pageSize",pageSize);
		
		return list;
	}
}
