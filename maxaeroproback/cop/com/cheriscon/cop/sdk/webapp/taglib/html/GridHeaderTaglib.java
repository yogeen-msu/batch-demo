package com.cheriscon.cop.sdk.webapp.taglib.html;

import com.cheriscon.cop.sdk.webapp.taglib.HtmlTaglib;

public class GridHeaderTaglib extends HtmlTaglib {
	
	
	protected String postStart() {
		return "<thead><tr>";
	}
	
	
	protected String postEnd() {
		return "</tr></thead>";
	}
}
