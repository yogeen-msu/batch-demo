package com.cheriscon.cop.sdk.webapp.taglib.html;

import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang3.StringUtils;

import com.cheriscon.cop.sdk.webapp.taglib.ListTaglibSupport;
import com.cheriscon.cop.sdk.webapp.taglib.html.support.GridBodyParam;
import com.cheriscon.cop.sdk.webapp.taglib.html.support.GridBodyProvider;

public class GridBodyTaglib extends ListTaglibSupport{
	private GridBodyProvider gridBodyProvider;
	
	public GridBodyTaglib() {
		gridBodyProvider = new GridBodyProvider();
	}
	
	protected void loadProvider() {
		
		Tag tag = this.getParent();
		
		if(tag!=null){
			GridTaglib gridTaglib =(GridTaglib)tag;
			GridBodyParam bodyparm = new GridBodyParam();
			bodyparm.setFrom( gridTaglib.getFrom() );
			this.param =  bodyparm;
			
			this.tagProvider = this.gridBodyProvider;
		} else {
			this.print("body tag must be included in grid tag");
		}
	}
	
	
	protected String postStart() {
		if(getIndex() %  2 ==0){
			return "<tr class='even' >";
		}else{
			return "<tr>";
		}
		
	}

	
	protected String postEnd() {
		return "</tr>";
	}

	
	protected String postStartOnce() {
		if(StringUtils.isEmpty(getId())){
			return "<tbody>";
		}else{
			return "<tbody id='"+getId()+"'>";
		}
		
	}
	
	
	protected String postEndOnce() {
		return "</tbody>";
	}
}
