package com.cheriscon.cop.sdk;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface CopServlet   {
 
	public void service(HttpServletRequest request, HttpServletResponse response)throws  IOException ;
 

}
