package com.cheriscon.cop.sdk.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cheriscon.app.base.core.model.MultiSite;
import com.cheriscon.app.base.core.service.IMultiSiteManager;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;

public class CopContextIniter {

	public static void init(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
		FreeMarkerPaser.set(new FreeMarkerPaser());
		CopContext.setHttpRequest(httpRequest);
		FreeMarkerPaser fmp = FreeMarkerPaser.getInstance();
		/**
		 * 将requst response及静态资源域名加入到上下文
		 */
		HttpSession session = httpRequest.getSession();
		ThreadContextHolder.getSessionContext().setSession(session);
		
		ThreadContextHolder.setHttpRequest(httpRequest);
		ThreadContextHolder.setHttpResponse(httpResponse);
		httpRequest.setAttribute("staticserver", CopSetting.IMG_SERVER_DOMAIN);
		httpRequest.setAttribute("imagePath", CopSetting.IMAGE_PATH);
		httpRequest.setAttribute("ext", CopSetting.EXTENSION);
		
		fmp.putData("languageInfo", httpRequest.getSession().getAttribute("languageInfo"));
		String servletPath = httpRequest.getServletPath();

		// System.out.println("uri : "+ RequestUtil.getRequestUrl(httpRequest));
		if (servletPath.startsWith("/statics"))
			return;

		if (servletPath.startsWith("/install")) {
			CopSite site = new CopSite();
			site.setUserid(1);
			site.setId(1);
			site.setThemeid(1);
			CopContext context = new CopContext();
			context.setCurrentSite(site);
			CopContext.setContext(context);
		} else {
			CopContext context = new CopContext();
			CopSite site = new CopSite();
			site.setUserid(1);
			site.setId(1);
			site.setThemeid(1);
			context.setCurrentSite(site);
			CopContext.setContext(context);

			ISiteManager siteManager = SpringContextHolder.getBean("siteManager");
			site = siteManager.get("localhost");

			context.setCurrentSite(site);
			if (site.getMulti_site() == 1) { //开启多站点功能
				String domain = httpRequest.getServerName();
				IMultiSiteManager multiSiteManager = SpringContextHolder.getBean("multiSiteManager");
				MultiSite multiSite = multiSiteManager.get(domain);
				context.setCurrentChildSite(multiSite);
			}

			CopContext.setContext(context);
			fmp.putData("site", site);
		}

		/**
		 * 设置freemarker的相关常量
		 */
		fmp.putData("ctx", httpRequest.getContextPath());
		fmp.putData("ext", CopSetting.EXTENSION);
		fmp.putData("staticserver", CopSetting.IMG_SERVER_DOMAIN);
		fmp.putData("imagePath", CopSetting.IMAGE_PATH);
	}

}
