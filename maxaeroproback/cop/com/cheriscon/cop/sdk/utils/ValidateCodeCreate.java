package com.cheriscon.cop.sdk.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 随机验证码生成类
 * @author yangpinggui 2013-1-19
 *
 */
public class ValidateCodeCreate extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6676599596799434249L;
	public String sgbRand = "";   

    /**
     * 初始化验证图片属性
     */
    public void init() throws ServletException
    {
    }
    
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
    	
    	try 
    	{
			HttpSession session = request.getSession();
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setContentType("image/jpeg");
			
			Color[] itemColor= {Color.RED,Color.BLACK,
					Color.GRAY,Color.CYAN,Color.ORANGE,
					Color.BLUE,Color.GREEN,Color.MAGENTA};
			
			Random random = new Random();
			
			int colorId = random.nextInt(itemColor.length); //颜色
			int codeNum=random.nextInt(4)+2;	//验证码个数
			String ranstr=getRandomString(codeNum);	//验证码
			
			session.setAttribute("validtorImageConstant", ranstr);
			
			
			char[] item=ranstr.toCharArray();
			
			ArrayList list=new ArrayList();
			
			for(int i=0;i<8;i++)
			{
				list.add(i);
			}
			
			ArrayList listCode=new ArrayList();
			int intCurrentPos = 0;	//随机产生的位置数
			int intMinPosition = 0;	//
			
			
			
			
			for(int i=0;i<codeNum;i++)
			{
				
				if(i == 0)
				{
					intCurrentPos = GetPosition(8 - codeNum);
				}
				else
				{
					intMinPosition = 8 - (codeNum - i);
					
					intCurrentPos = intCurrentPos + GetPosition(intMinPosition - intCurrentPos);
					
					intCurrentPos =  intCurrentPos > 8 ? 8 : intCurrentPos;
				}
				
				Code code=new Code();
				code.setNumber(intCurrentPos);
				code.setColor(itemColor[colorId]);
				code.setValue(item[i]);
				listCode.add(code);
				Integer intcu=new Integer(intCurrentPos);
				intCurrentPos++;
				list.remove(intcu);
			}
			
			
			
			do
			{
				if(list.size() == 0) 
				{
					break;
				}
				
				int mt=random.nextInt(itemColor.length);
				
				if(mt==colorId)
				{
					continue;
				}
				
				int rt=random.nextInt(list.size());
				Integer intcu=Integer.valueOf(list.get(rt).toString());
				Code code=new Code();
				code.setNumber(intcu);
				code.setColor(itemColor[mt]);
				code.setValue(getRandomString(1).toCharArray()[0]);
				listCode.add(code);
				list.remove(intcu);
			}
			while(list.size()>0);
			
			
			
			int imageWidth = 250;
			int imageHeight = 70;
			BufferedImage image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = image.createGraphics();
			g.fillRect(1, 1, imageWidth - 1, imageHeight - 1);
			g.setColor(new Color(70, 70, 70));
			g.drawRect(0, 0, imageWidth - 1, imageHeight - 1);
			g.setFont(new Font("\u5b8b\u4f53", Font.BOLD, 15));
			g.setColor(Color.BLACK); 
			String ds ="\u8bf7\u8f93\u5165\u9a8c\u8bc1\u7801\u4e2d\u6240\u6709";
			g.drawString(ds, 9, 19);
			String df="\u8272\u7684\u5b57\u7b26";
			g.drawString(df, 175, 19);
			g.dispose();
			Graphics2D s = image.createGraphics();
			s.setColor(itemColor[colorId]);
			s.fillRect(153,5,20,20);
			s.dispose();
	
			
			for(int k=0;k<8;k++)
			{
				Graphics2D m = image.createGraphics();
				Code code=(Code)listCode.get(k);
				m.setColor(code.getColor());
				//String randomString =getRandomString(6);
				m.setFont(new Font("Courier New", Font.BOLD, 22));
				String ms=String.valueOf(code.getValue());
				int num=code.getNumber();
				m.drawString(ms, num*22+40, 50);
				m.dispose();
			}
			
			ImageIO.write(image, "JPEG", response.getOutputStream());
			
		} 
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
    }
    
    public int GetPosition(int maxValue)
    {
    	Random rdm = new Random();
    	return rdm.nextInt(100001) % (maxValue + 1);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
    	doGet(req, resp);
    }
    
	/**
	 * 获取随机字符串
	 * 
	 * @param
	 */
	public final static String getRandomString(int length) 
	{
		StringBuffer buffer = new StringBuffer("23456789abcdefghjkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ");
		StringBuffer sb = new StringBuffer();
		Random r = new Random();
		int range = buffer.length();
		
		for (int i = 0; i < length; i++) 
		{
			sb.append(buffer.charAt(r.nextInt(range)));
		}
		
		return sb.toString();
	}
	
	/**
	 * 获得随机验证码
	 * @return
	 */
	/*private Color getRandomColor() 
	{
		Random random = new Random();
		int red = random.nextInt(100) + 100;
		int green = random.nextInt(120) + 100;
		int blue = random.nextInt(90) + 100;
		return new Color(red, green, blue);
	}*/

	class Code
	{
		private int number;
		private Color	color;
		private char value;
		
		public int getNumber() 
		{
			return number;
		}
		
		public void setNumber(int number) 
		{
			this.number = number;
		}
		
		public Color getColor() 
		{
			return color;
		}
		
		public void setColor(Color color) 
		{
			this.color = color;
		}
	
		public char getValue() 
		{
			return value;
		}
		
		public void setValue(char value)
		{
			this.value = value;
		}
	}

}
