package com.cheriscon.cop.sdk.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadException;
import javazoom.upload.UploadFile;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;

import com.cheriscon.cop.processor.MultipartRequestWrapper;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.image.IThumbnailCreator;
import com.cheriscon.framework.image.ThumbnailCreatorFactory;
import com.cheriscon.framework.util.DateUtil;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;

public class UploadUtil {
	
	private UploadUtil(){
	
	}
 
	public static String uploadUseRequest(String name,String subFolder,String allowext){
		if( name==null) throw new IllegalArgumentException("file or filename object is null");
		if(subFolder == null)throw new IllegalArgumentException("subFolder is null");
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		if (!MultipartFormDataRequest.isMultipartFormData(request)) throw new RuntimeException("request data is not MultipartFormData"); 
	
		try {
			String encoding =CopSetting.ENCODING;
			if(StringUtil.isEmpty(encoding)){
				encoding = "UTF-8";
			}
			
			MultipartFormDataRequest mrequest  = new MultipartFormDataRequest(request,null,1000*1024*1024,MultipartFormDataRequest.COSPARSER,encoding);
			request = new MultipartRequestWrapper(request,mrequest);
			ThreadContextHolder.setHttpRequest(request);
			
			Hashtable files = mrequest.getFiles();
			UploadFile file = (UploadFile) files.get(name);
			if(file.getInpuStream() == null) return null;
			
			String fileFileName = file.getFileName();
		
		 
			
			String fileName = null;
			String filePath = "";
	 
			String ext = FileUtil.getFileExt(fileFileName);
			
			if(!allowext.equals(ext)){
				throw new IllegalArgumentException("不被允许的上传文件类型");
			}
			
			fileName = DateUtil.toString(new Date(), "yyyyMMddHHmmss") + StringUtil.getRandStr(4) + "." + ext;
			
			filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/";
			if(subFolder!=null){
				filePath+=subFolder +"/";
			}
			
			String path  = CopSetting.FILE_STORE_PREFIX+ "/attachment/" +(subFolder==null?"":subFolder)+ "/"+fileName;
		 
			filePath += fileName;
			FileUtil.createFile(file.getInpuStream(), filePath);
		 
			return path;
			
			
		} catch (UploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	 
 
	
		
		return null;
	}
	
	/**
	 * 通过request上传文件 ，表单要声明  enctype="multipart/form-data" 
	 * @param name file控件的name
	 * @param subFolder 要上传的子文件夹
	 * @return
	 */
	public static String uploadUseRequest(String name,String subFolder){
		if( name==null) throw new IllegalArgumentException("file or filename object is null");
		if(subFolder == null)throw new IllegalArgumentException("subFolder is null");
		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
		if (!MultipartFormDataRequest.isMultipartFormData(request)) throw new RuntimeException("request data is not MultipartFormData"); 
	
		try {
			String encoding =CopSetting.ENCODING;
			if(StringUtil.isEmpty(encoding)){
				encoding = "UTF-8";
			}
			
			MultipartFormDataRequest mrequest  = new MultipartFormDataRequest(request,null,1000*1024*1024,MultipartFormDataRequest.COSPARSER,encoding);
			request = new MultipartRequestWrapper(request,mrequest);
			ThreadContextHolder.setHttpRequest(request);
			
			Hashtable files = mrequest.getFiles();
			UploadFile file = (UploadFile) files.get(name);
			if(file.getInpuStream() == null) return null;
			
			String fileFileName = file.getFileName();
		
			String fileName = null;
			String filePath = ""; 
	 
			String ext = FileUtil.getFileExt(fileFileName);
			fileName = DateUtil.toString(new Date(), "yyyyMMddHHmmss") + StringUtil.getRandStr(4) + "." + ext;
			
			filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/";
			if(subFolder!=null){
				filePath+=subFolder +"/";
			}
			
			String path  = CopSetting.FILE_STORE_PREFIX+ "/attachment/" +(subFolder==null?"":subFolder)+ "/"+fileName;
		 
			filePath += fileName;
			FileUtil.createFile(file.getInpuStream(), filePath);
		 
			return path;
			
			
		} catch (UploadException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	 
 
	
		
		return null;
	}
	
	/**
	 * 上传图片<br/>
	 * 图片会被上传至用户上下文的attacment文件夹的subFolder子文件夹中<br/>
	 * 
	 * @param file  图片file对象
	 * @param fileFileName 上传的图片原名
	 * @param subFolder  子文件夹名
	 * @return 
	 * @since 上传后的本地路径，如:fs:/attachment/goods/2001010101030.jpg<br/>
	 * 
	 */
	public static String upload(File file,String fileFileName,String subFolder ) {
		
		if(file==null || fileFileName==null) throw new IllegalArgumentException("file or filename object is null");
		if(subFolder == null)throw new IllegalArgumentException("subFolder is null");
		
		if(!FileUtil.isAllowUp(fileFileName)){
			throw new IllegalArgumentException("不被允许的上传文件类型");
		}
		
		String fileName = null;
		String filePath = "";
 
		String ext = FileUtil.getFileExt(fileFileName);
		String fName =fileFileName.substring(0,fileFileName.indexOf(".")); 
		fileName = fName+"_"+DateUtil.toString(new Date(), "yyyyMMddHHmmss") + StringUtil.getRandStr(2) + "." + ext;
		
		filePath =  CopSetting.IMG_SERVER_PATH +CopContext.getContext().getContextPath()+  "/attachment/";
		if(subFolder!=null){
			filePath+=subFolder +"/";
		}
		
		String path  = CopSetting.FILE_STORE_PREFIX+ "/attachment/" +(subFolder==null?"":subFolder)+ "/"+fileName;
	 
		filePath += fileName;
		FileUtil.createFile(file, filePath);
	 
		return path;
	}
	
	
	/**
	 *  发送文件至前台
	 *  
	 * @param file
	 * @param fileFileName
	 * @param subFolder
	 * @return
	 */
	public static String publish(File file, String fileFileName, String subFolder) {
		if(file == null || fileFileName == null) {
			throw new IllegalArgumentException("file or filename object is null");
		}
		if(subFolder == null) {
			throw new IllegalArgumentException("subFolder is null");
		}
		if(!FileUtil.isAllowUp(fileFileName)){
			throw new IllegalArgumentException("不被允许的上传文件类型");
		}
		
		String fileName = null;
		String filePath = "";
 
		String ext = FileUtil.getFileExt(fileFileName);
		String fName =fileFileName.substring(0, fileFileName.indexOf(".")); 
		fileName = fName + "_"+DateUtil.toString(new Date(), "yyyyMMddHHmmss") + StringUtil.getRandStr(2) + "." + ext;
		
		filePath = CopSetting.IMG_SERVER_PATH + CopContext.getContext().getContextPath() + "/attachment/";
		if (subFolder!=null) {
			filePath += subFolder + "/";
		}
		
		String path = CopSetting.FILE_STORE_PREFIX + "/attachment/" + (subFolder==null?"":subFolder) + "/" + fileName;
	 
		filePath += fileName;
		FileUtil.createFile(file, filePath);
		
		try{
			PostMethod httppost = new PostMethod(FreeMarkerPaser.getBundleValue(
					"autelpro.url") + "/uploadFileServlet.do");  
			
			httppost.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
			httppost.getParams().setContentCharset("UTF-8");
//	        httppost.addRequestHeader("Content-Type", "text/html;charset=UTF-8");
//	        httppost.setRequestHeader("Content-Type", "text/html;charset=UTF-8");
//			httppost.addRequestHeader("Content-Type", "multipart/form-data;charset=UTF-8");
			
			ReFilePartUtil reFilePart = new ReFilePartUtil("Filedata", new File(filePath));
//			FilePart f = new FilePart("Filedata", new File(filePath), null, "UTF-8");
//			StringPart np = new StringPart(fileName, fileName);
//			np.setCharSet("UTF-8");
			Part[] parts = { reFilePart };
			
			
	
			// 对于MIME类型的请求，httpclient建议全用MulitPartRequestEntity进行包装
			MultipartRequestEntity mre = new MultipartRequestEntity(parts,
					httppost.getParams());
			httppost.setRequestEntity(mre);
			HttpClient client = new HttpClient();
			client.getParams().setContentCharset("UTF-8");
			client.getHttpConnectionManager().getParams()
					.setConnectionTimeout(50000);// 设置连接时间
//			int status = 
			client.executeMethod(httppost);
//			if (status == HttpStatus.SC_OK) {
//				String res = httppost.getResponseBodyAsString();
//				JSONObject json = JSONObject.fromObject(res);
//			} 
		}catch (Exception e) {
		}
	 
		return path;
	}
	
	
	
	public static String replacePath(String path){
		
		if(StringUtil.isEmpty(path)) return path;
		//if(!path.startsWith(CopSetting.FILE_STORE_PREFIX)) return path;
		
		return     path.replaceAll(CopSetting.FILE_STORE_PREFIX, CopSetting.IMG_SERVER_DOMAIN+CopContext.getContext().getContextPath() );
	}
	
	/**
	 * 上传图片并生成缩略图
	 * 图片会被上传至用户上下文的attacment文件夹的subFolder子文件夹中<br/>
	 * 
	 * @param file  图片file对象
	 * @param fileFileName 上传的图片原名
	 * @param subFolder  子文件夹名
	 * @param width 缩略图的宽
	 * @param height 缩略图的高
	 * @return 上传后的图版全路径，如:http://static.cop.com/user/1/1/attachment/goods/2001010101030.jpg<br/>
	 * 返回值为大小为2的String数组，第一个元素为上传后的原图全路径，第二个为缩略图全路径
	 */
	public static String[] upload(File file,String fileFileName,String subFolder,int width,int height ){
		if(file==null || fileFileName==null) throw new IllegalArgumentException("file or filename object is null");
		if(subFolder == null)throw new IllegalArgumentException("subFolder is null");
		String fileName = null;
		String filePath = "";
		String [] path = new String[2];
		if(!FileUtil.isAllowUp(fileFileName)){
			throw new IllegalArgumentException("不被允许的上传文件类型");
		}
		String ext = FileUtil.getFileExt(fileFileName);
		fileName = DateUtil.toString(new Date(), "yyyyMMddHHmmss") + StringUtil.getRandStr(4) + "." + ext;
		
		filePath =  CopSetting.IMG_SERVER_PATH+ getContextFolder()+ "/attachment/";
		if(subFolder!=null){
			filePath+=subFolder +"/";
		}
		
		path[0]  = CopSetting.IMG_SERVER_DOMAIN+ getContextFolder()+ "/attachment/" +(subFolder==null?"":subFolder)+ "/"+fileName;
		filePath += fileName;
		FileUtil.createFile(file, filePath);
		String thumbName= getThumbPath(filePath,"_thumbnail");
		 
		IThumbnailCreator thumbnailCreator = ThumbnailCreatorFactory.getCreator( filePath, thumbName);
		thumbnailCreator.resize(width, height);	
		path[1] = getThumbPath(path[0], "_thumbnail");
		return path;
	}
	
	
	
	
	/**
	 * 删除某个上传的文件
	 * @param filePath 文件全路径如：http://static.cop.com/user/1/1/attachment/goods/2001010101030.jpg
	 */
	public static void deleteFile(String filePath){
		filePath =filePath.replaceAll(CopSetting.IMG_SERVER_DOMAIN, CopSetting.IMG_SERVER_PATH);
		FileUtil.delete(filePath);		
	}
	
	/**
	 * 文件是否存在
	 * @param filePath
	 * @return
	 */
	public static boolean fileExists(String filePath){
		filePath =filePath.replaceAll(CopSetting.IMG_SERVER_DOMAIN, CopSetting.IMG_SERVER_PATH);
		try {
			File file = new File(filePath);
			if (file.exists()){
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}
	
	
	
	/**
	 * 删除某个上传的文件
	 * @param filePath 文件全路径如：http://static.cop.com/user/1/1/attachment/goods/2001010101030.jpg
	 */
	public static void deleteFileAndThumb(String filePath){
		filePath =filePath.replaceAll(CopSetting.IMG_SERVER_DOMAIN, CopSetting.IMG_SERVER_PATH);
		FileUtil.delete(filePath);		
		FileUtil.delete(getThumbPath(filePath,"_thumbnail"));		
	}
	
	
	private static String getContextFolder(){
		 
		return CopContext.getContext().getContextPath();
	}
	
	/**
	 * 转换图片的名称
	 * @param filePath  如：http://static.cop.com/user/1/1/attachment/goods/2001010101030.jpg
	 * @param shortName 
	 * @return
	 */
	public static  String getThumbPath(String filePath, String shortName) {
		String pattern = "(.*)([\\.])(.*)";
		String thumbPath = "$1" + shortName + "$2$3";

		Pattern p = Pattern.compile(pattern, 2 | Pattern.DOTALL);
		Matcher m = p.matcher(filePath);
		if (m.find()) {
			String s = m.replaceAll(thumbPath);

			return s;
		}
		return null;
	}	
	
	
	public static void main(String[] args){
	 System.out.println(getThumbPath("http://static.cop.com/user/1/1/attachment/goods/2001010101030.jpg","_thumbnail"));	
	 System.out.println(getThumbPath("/user/1/1/attachment/goods/2001010101030.jpg","_thumbnail"));	
	}
	
}
