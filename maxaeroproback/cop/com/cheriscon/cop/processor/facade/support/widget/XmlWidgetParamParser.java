package com.cheriscon.cop.processor.facade.support.widget;

import java.util.Map;

import com.cheriscon.cop.processor.widget.IWidgetParamParser;
import com.cheriscon.cop.processor.widget.WidgetXmlUtil;
import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.Theme;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;

/**
 *  SAAS式的xml挂件参数解析器
 * @author kingapex
 * 2010-2-4下午04:05:18
 */
public class XmlWidgetParamParser implements IWidgetParamParser {

	 
	private IThemeManager themeManager;
	
	public Map<String, Map<String, Map<String,String>>> parse() {
		CopSite  site  =CopContext.getContext().getCurrentSite();
		Theme theme = themeManager.getTheme(site.getThemeid());
		String contextPath  = CopContext.getContext().getContextPath();
		String path =
		CopSetting.cop_PATH	
		+contextPath
		+ CopSetting.THEMES_STORAGE_PATH+
		"/" + theme.getPath()  + "/widgets.xml"; 
		return WidgetXmlUtil.parse(path);
	}
	public void setThemeManager(IThemeManager themeManager) {
		this.themeManager = themeManager;
	}
	
	
}
