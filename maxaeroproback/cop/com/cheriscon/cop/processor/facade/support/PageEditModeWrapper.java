package com.cheriscon.cop.processor.facade.support;

import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import com.cheriscon.cop.processor.IPagePaser;
import com.cheriscon.cop.processor.PageWrapper;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.utils.FreeMarkerUtil;
import com.cheriscon.cop.sdk.utils.HtmlUtil;
import com.sun.xml.messaging.saaj.util.ByteOutputStream;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * 前台页面编辑模式包装器
 * @author kingapex
 * 2010-2-9上午01:27:30
 */
public class PageEditModeWrapper extends PageWrapper {

	public PageEditModeWrapper(IPagePaser paser) {
		super(paser);
	}

	
	public String pase(String url) {
		String content  = super.pase(url);
		String script= this.getToolBarScript();
		String html = this.getToolBarHtml();
		
		content =wrapPageMain(content);
		content =HtmlUtil.appendTo(content, "head", script);
		content =HtmlUtil.insertTo(content, "body", html);
		//System.out.println(content);
		return content;
	}
	
	private String getToolBarScript(){
		String copFld= CopSetting.cop_PATH+"/cop/";
		try {
			Map<String, String> data = new HashMap<String, String>();
			data.put("staticserver", CopSetting.IMG_SERVER_DOMAIN) ;	
			data.put("ctx", CopSetting.CONTEXT_PATH) ;
			CopSite site = CopContext.getContext().getCurrentSite();
			data.put("userid",""+site.getUserid() ) ;
			data.put("siteid",""+site.getId() ) ;
			
			Configuration cfg = FreeMarkerUtil.getFolderCfg(copFld);
			Template temp = cfg.getTemplate("widget_tool_script.html");
			ByteOutputStream stream = new ByteOutputStream();
			Writer out = new OutputStreamWriter(stream);
			temp.process(data, out);
			out.flush();
			return stream.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		
	}
	
	private String getToolBarHtml(){
		//widget_toolbar.html
		String copFld= CopSetting.cop_PATH+"/cop/";
		try {
			Map<String, String> data = new HashMap<String, String>();
			data.put("staticserver", CopSetting.IMG_SERVER_DOMAIN) ;	
			data.put("ctx", CopSetting.CONTEXT_PATH) ;
			CopSite site = CopContext.getContext().getCurrentSite();
			data.put("userid",""+site.getUserid() ) ;
			data.put("siteid",""+site.getId() ) ;
			Configuration cfg = FreeMarkerUtil.getFolderCfg(copFld);
			Template temp = cfg.getTemplate("widget_toolbar.html");
			ByteOutputStream stream = new ByteOutputStream();
			Writer out = new OutputStreamWriter(stream);
			temp.process(data, out);
			out.flush();
			return stream.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private String wrapPageMain(String content){
		content = HtmlUtil.insertTo(content, "body", "<div id=\"pagemain\">");
		content =HtmlUtil.appendTo(content, "body", "</div>");
		return content;
	}

}
