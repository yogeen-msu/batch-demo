package com.cheriscon.cop.processor.facade.support.widget;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import com.cheriscon.cop.processor.widget.IWidgetParamParser;
import com.cheriscon.cop.processor.widget.IWidgetParamUpdater;
import com.cheriscon.cop.processor.widget.WidgetXmlUtil;
import com.cheriscon.cop.resource.IThemeManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.Theme;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;

/**
 * saas式挂件参数更新
 * @author kingapex
 * 2010-2-10上午11:26:21
 */
public class XmlWidgetParamUpdater implements IWidgetParamUpdater {
	private IThemeManager themeManager;
	
	
	public void update(String pageId,List<Map<String,String>> params) {
		CopSite  site  =CopContext.getContext().getCurrentSite();
		Theme theme = themeManager.getTheme(site.getThemeid());
		String contextPath  = CopContext.getContext().getContextPath();
		String path =
		CopSetting.cop_PATH	
		+contextPath
		+ CopSetting.THEMES_STORAGE_PATH+
		"/" + theme.getPath()  + "/widgets.xml"; 
			
		WidgetXmlUtil.save(path, pageId, params);		
	}
	
	public void setThemeManager(IThemeManager themeManager) {
		this.themeManager = themeManager;
	}
	

}
