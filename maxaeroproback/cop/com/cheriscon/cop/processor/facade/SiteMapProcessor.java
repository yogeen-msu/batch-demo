package com.cheriscon.cop.processor.facade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cheriscon.app.base.core.service.ISitemapManager;
import com.cheriscon.cop.processor.Processor;
import com.cheriscon.cop.processor.core.HttpHeaderConstants;
import com.cheriscon.cop.processor.core.Response;
import com.cheriscon.cop.processor.core.StringResponse;
import com.cheriscon.framework.context.spring.SpringContextHolder;

public class SiteMapProcessor implements Processor {

	public Response process(int mode, HttpServletResponse httpResponse,
			HttpServletRequest httpRequest) {
		ISitemapManager siteMapManager = SpringContextHolder.getBean("sitemapManager"); 
		String siteMapStr = siteMapManager.getsitemap();
		Response response = new StringResponse();
		response.setContent(siteMapStr);
		response.setContentType(HttpHeaderConstants.CONTEXT_TYPE_XML);
		return response;
	}

}
