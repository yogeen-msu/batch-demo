package com.cheriscon.cop.processor.core.freemarker;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cheriscon.common.model.Language;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.utils.CopUtil;
import com.cheriscon.cop.sdk.utils.FreeMarkerUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.sun.xml.messaging.saaj.util.ByteOutputStream;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.ResourceBundleModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * FreeMarker解析器
 * 
 * @author kingapex 2010-2-16下午03:42:40
 */
public   final class FreeMarkerPaser {

	private static final Log log = LogFactory.getLog(FreeMarkerPaser.class);
	private static ThreadLocal<FreeMarkerPaser> managerLocal = new ThreadLocal<FreeMarkerPaser>();
	
	public FreeMarkerPaser(){ 
		data = new HashMap<String, Object>();
		this.clazz =null;
		this.pageFolder =null;
	}
	
	public void setClz(Class clz){
		this.clazz = clz;
	}
	/**
	 * 获取当前线程的 fremarkManager
	 * @return
	 */
	public final static FreeMarkerPaser getInstance(){
		
		if( managerLocal.get()==null ){
			throw new RuntimeException("freemarker paser is null");
		}
		FreeMarkerPaser fmp =managerLocal.get();
		
		fmp.setPageFolder(null);
		fmp.setWrapPath(true);
		loadBundleResource(fmp);
		
		return fmp;
	}
	
	public final static FreeMarkerPaser getCurrInstance(){
		if( managerLocal.get()==null ){
			throw new RuntimeException("freemarker paser is null");
		}
		FreeMarkerPaser fmp =managerLocal.get();
		
		loadBundleResource(fmp);

		return fmp;
	}
	
	/**
	 * 加载国际资源
	 * @param fmp
	 */
	public final static void loadBundleResource(FreeMarkerPaser fmp)
	{	
//		Language language = null;
//		HttpServletRequest request = ThreadContextHolder.getHttpRequest();
//		if(null !=SessionUtil.getLoginUserInfo(request)){
		Language  language = (Language) fmp.getData("languageInfo");
//		}
		Locale locale = getLocale(language);
		ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("backstage",locale);
		ResourceBundleModel rsbm = new ResourceBundleModel(RESOURCE_BUNDLE, new BeansWrapper());
		fmp.putData("bundle", rsbm);
	}
	
	/**
	 * 前台语言处理，如果该用户未登陆那么获取浏览器设置语言、如果已登陆则获取该用户个人信息中设置的语言
	 */
	public static Locale getLocale(Language language)
	{
		Locale locale = null;
		
		if(language != null)
		{
			locale = new Locale(language.getLanguageCode(),language.getCountryCode());
		}
		else 
		{
			locale = CopContext.getDefaultLocal();
		}
		
		return locale;
	}
	
	public final static String getBundleValue(String... key) {
		try {
			Language language = null;
			if (key.length > 1) {
				language = new Language();
				language.setLanguageCode(key[1]);
				language.setCountryCode(key[2]);
			} else {
				language = (Language) CopContext.getHttpRequest().getSession().getAttribute("languageInfo");
			}
			Locale locale = getLocale(language);
			ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("backstage", locale);
			ResourceBundleModel rsbm = new ResourceBundleModel(RESOURCE_BUNDLE, new BeansWrapper());
			return rsbm.get(key[0]).toString();
		} catch (Exception e) {
		}
		return key[0];
	}
	
	public final static String getBackStageBundleValue(String... key) {
		try {
			Language language = null;
			Locale locale = getLocale(language);
			ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("backstage", locale);
			ResourceBundleModel rsbm = new ResourceBundleModel(RESOURCE_BUNDLE, new BeansWrapper());
			return rsbm.get(key[0]).toString();
		} catch (Exception e) {
		}
		return key[0];
	}
	public final static void set(FreeMarkerPaser fp){
		 managerLocal.set(fp);
	} 
	public final static void remove(){
		 managerLocal.remove();
	} 
	
	private Class clazz;
	public  FreeMarkerPaser(Class clz){
		this.clazz = clz;
		data = new HashMap<String, Object>();
	}

	public FreeMarkerPaser(Class clz,String folder){
		this.clazz = clz;
		this.pageFolder = folder;
		data = new HashMap<String, Object>();
		
	}
	/**
	 * 设置挂件模板的变量
	 * 
	 * @param key
	 * @param value
	 */
	public void putData(String key, Object value) {
		if (key != null && value != null)
			data.put(key, value);
	}
	
	public void putData(Map  map){
		if(map!=null)
			data.putAll(map);
	}
	
	public Object getData(String key){
		if(key==null) return null;
		
		return data.get(key);
	}
	
	private boolean wrapPath=true;
	
	public void setWrapPath(boolean wp){
		wrapPath =wp;
	}

	public String proessPageContent(){
		
		try {
			
			String name = this.clazz.getSimpleName();
			
			//CGLIB代理
			int pos= name.indexOf("$$EnhancerByCGLIB$$");
			if(pos>0 ){
				name= name.substring(0,pos);
			 
			}
			
			pageExt = pageExt == null ? ".html" : pageExt;		
			name = this.pageName == null ? name : pageName;
		 
			cfg = this.getCfg();
			cfg.setNumberFormat("0.##");
			Template temp = cfg.getTemplate(name + pageExt);
			ByteOutputStream stream = new ByteOutputStream();
			Writer out = new OutputStreamWriter(stream);
			temp.process(data, out);
			out.flush();
			String content = stream.toString();
			if(wrapPath){
//				System.out.println("before:---------------------------");
//				System.out.println(content);
				
				content = CopUtil.wrapjavascript(content, this.getResPath());
				content =  CopUtil.wrapcss(content, getResPath());
//				System.out.println("after:---------------------------");
//				System.out.println(content);
			}
			//content= StringUtil.compressHtml(content);
			return content;
		} catch (IOException e) {
			e.printStackTrace();
			return "<script>window.location='common_error.html';</script>";
		} catch (TemplateException e) {
			e.printStackTrace();
			return "<script>window.location='common_error.html';</script>";
		}

		//return "widget  processor error";
	}
		
	
	private static   Configuration  cfg;
	

	/*
	 * freemarker data model 通过putData方法设置模板中的值
	 */
	private Map<String, Object> data;

	/*
	 * 模板路径前缀 默认为"" 可以通过 {@link #setPathPrefix(String)} 设置
	 */
	private String pathPrefix;

	/*
	 * 模板文件的名字，默认为与插件类同名
	 */
	private String pageName;

	/*
	 * 模板页面的扩展名，默认为.html
	 */
	private String pageExt;
 
	
	
	/*
	 * 页面所在文件夹
	 * 默认为插件类所在文件夹
	 */
	private String pageFolder; 
	
 
	
	private Configuration getCfg(){
			
			if(cfg==null){
				 cfg = FreeMarkerUtil.getCfg();		
			}

			 
			pathPrefix = pathPrefix== null?"":pathPrefix;
			
			if(pageFolder==null) {//默认使用挂件所在文件夹
				//System.out.println(" folder null use "+ this.clazz.getName() );
				cfg.setClassForTemplateLoading(this.clazz, pathPrefix);
				}
			else{
				//System.out.println(" folder not null use "+ pageFolder);
			  cfg.setServletContextForTemplateLoading(ThreadContextHolder.getHttpRequest().getSession().getServletContext(), pageFolder);
			}
			cfg.setObjectWrapper(new DefaultObjectWrapper());	
			/*cfg.setCacheStorage(new freemarker.cache.MruCacheStorage(20, 250)) ;*/
			cfg.setDefaultEncoding("UTF-8");
			cfg.setLocale(java.util.Locale.CHINA);
			cfg.setEncoding(java.util.Locale.CHINA, "UTF-8");
		return cfg;
	}
	
	

	



	/**
	 * 设置模板路径前缀
	 * 
	 * @param path
	 */
	public void setPathPrefix(String path) {
		this.pathPrefix = path;
	}

	
	/**
	 * 设置模板文件的名称
	 * 
	 * @param pageName
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	/**
	 * 设置模板页面扩展名
	 * 
	 * @param pageExt
	 */
	public void setPageExt(String pageExt) {
		this.pageExt = pageExt;
	}

	public void setPageFolder(String pageFolder){
		this.pageFolder = pageFolder;
	}

	
	/**
	 * 获取资源根路径
	 * @return
	 */
	private String getResPath(){
		String ctx =CopSetting.CONTEXT_PATH;
		ctx = ctx.equals("/")?"":ctx;
		if( this.pageFolder ==null ){
			return ctx+"/resource/"+ this.clazz.getPackage().getName().replaceAll("\\.", "/")+"/";
		}else{
			return ctx+pageFolder+"/";
		}
		
	}

}
