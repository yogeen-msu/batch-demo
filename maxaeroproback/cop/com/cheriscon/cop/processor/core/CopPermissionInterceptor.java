package com.cheriscon.cop.processor.core;

import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.user.UserContext;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.context.webcontext.WebSessionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

/**
 * cop权限拦截器
 * @author kingapex
 * 2010-10-23下午11:55:03
 */
public class CopPermissionInterceptor implements Interceptor {

	public void destroy() {
	}

	public void init() {
	}

	public String intercept(ActionInvocation inv) throws Exception {
		
		Integer userid  = CopContext.getContext().getCurrentSite().getUserid();
		if(userid.intValue() !=1){
			return "error";
		}
		
		String result = inv.invoke();
		return result;
	}

}
