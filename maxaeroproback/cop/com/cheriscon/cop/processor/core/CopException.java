package com.cheriscon.cop.processor.core;

public class CopException extends RuntimeException {
	public CopException(){
		super();
	}
	
	public CopException(String message){
		super(message);
	}
}
