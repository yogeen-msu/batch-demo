package com.cheriscon.cop.processor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javazoom.upload.UploadException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import com.cheriscon.common.model.Language;
import com.cheriscon.cop.processor.core.Response;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.processor.httpcache.HttpCacheManager;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopContextIniter;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.context.SaasCopContextIniter;
import com.cheriscon.cop.sdk.utils.FreeMarkerUtil;
import com.cheriscon.cop.sdk.utils.JspUtil;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.front.constant.FrontConstant;
import com.sun.xml.messaging.saaj.util.ByteOutputStream;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * 独立版的filter
 * @author kingapex
 * @version 1.0
 * @created 12-十月-2009 10:30:23
 */
public class DispatcherFilter implements Filter {
	private static String pkey = "b7022c97a7d8c9fc47b2c14b8b375a9e";
	public void init(FilterConfig config) {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException { 
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String uri = httpRequest.getServletPath();
		
		String conString = httpRequest.getHeader("REFERER");//获取父url 
		try {
			Language language = (Language)httpRequest.getSession().getAttribute("languageInfo");
			if (language == null) {
				if( null!=conString && conString.contains("http://www.auteltech.com")){ //英文企业网站
					language=new Language();
					language.setCountryCode("US");
					language.setLanguageCode("en");
					httpRequest.getSession().setAttribute("languageInfo", language);
				}else if( null!=conString && conString.contains("http://www.autel.com")){  //英文企业网站
					language=new Language();
					language.setCountryCode("US");
					language.setLanguageCode("en");
					httpRequest.getSession().setAttribute("languageInfo", language);
				}else if( null!=conString && conString.contains("http://www.auteltech.com/de")){  //德语企业网站
					language=new Language();
					language.setCountryCode("DE");
					language.setLanguageCode("de");
					httpRequest.getSession().setAttribute("languageInfo", language);
				}else if( null!=conString && conString.contains("http://www.autel.eu")){  //德语企业网站
					language=new Language();
					language.setCountryCode("DE");
					language.setLanguageCode("de");
					httpRequest.getSession().setAttribute("languageInfo", language);
				}else if( null!=conString && conString.contains("http://www.auteltech.com/es")){  //西班牙企业网站
					language=new Language();
					language.setCountryCode("LA");
					language.setLanguageCode("es");
					httpRequest.getSession().setAttribute("languageInfo", language);
				}else if( null!=conString && conString.contains("http://www.autel.com/es")){  //西班牙语企业网站
					language=new Language();
					language.setCountryCode("LA");
					language.setLanguageCode("es");
					httpRequest.getSession().setAttribute("languageInfo", language);
				}else if( null!=conString && conString.contains("http://www.auteltech.cn")){  //中文企业网站
					language=new Language();
					language.setCountryCode("CN");
					language.setLanguageCode("zh");
					httpRequest.getSession().setAttribute("languageInfo", language);
				}
				else{
					CopContext.setDefaultLocal();
				}
			}else{
				if(StringUtil.isEmpty(language.getCode())&& null!=conString && conString.contains("http://www.auteltech.com")){  //英文企业网站
					language=new Language();
					language.setCountryCode("US");
					language.setLanguageCode("en");
					httpRequest.getSession().setAttribute("languageInfo", language);
					
				}
				
				if(StringUtil.isEmpty(language.getCode())&& null!=conString && conString.contains("http://www.autel.com")){  //英文企业网站
					language=new Language();
					language.setCountryCode("US");
					language.setLanguageCode("en");
					httpRequest.getSession().setAttribute("languageInfo", language);
					
				}
				if(StringUtil.isEmpty(language.getCode())&& null!=conString && conString.contains("http://www.autel.com/de")){  //德语企业网站
					language=new Language();
					language.setCountryCode("DE");
					language.setLanguageCode("de");
					httpRequest.getSession().setAttribute("languageInfo", language);
					
				}
				if(StringUtil.isEmpty(language.getCode())&& null!=conString && conString.contains("http://www.autel.eu")){  //德语企业网站
					language=new Language();
					language.setCountryCode("DE");
					language.setLanguageCode("de");
					httpRequest.getSession().setAttribute("languageInfo", language);
					
				}
				if(StringUtil.isEmpty(language.getCode())&& null!=conString && conString.contains("http://www.autel.com/es")){  //西班牙企业网站
					language=new Language();
					language.setCountryCode("LA");
					language.setLanguageCode("es");
					httpRequest.getSession().setAttribute("languageInfo", language);
					
				}
				if(StringUtil.isEmpty(language.getCode())&& null!=conString && conString.contains("http://www.auteltech.com/es")){  //西班牙企业网站
					language=new Language();
					language.setCountryCode("LA");
					language.setLanguageCode("es");
					httpRequest.getSession().setAttribute("languageInfo", language);
					
				}
				if(StringUtil.isEmpty(language.getCode())&& null!=conString && conString.contains("http://www.auteltech.cn")){  //中文企业网站
					language=new Language();
					language.setCountryCode("ZH");
					language.setLanguageCode("cn");
					httpRequest.getSession().setAttribute("languageInfo", language);
					
				}
				
				
				if(StringUtil.isEmpty(language.getCode())&& null!=conString && !conString.contains(FrontConstant.getCMS())){
					Locale locale=httpRequest.getLocale();
					language=new Language();
					language.setCountryCode(locale.getCountry());
					language.setLanguageCode(locale.getLanguage());
					httpRequest.getSession().setAttribute("languageInfo", language);
				}
			}
			
			
			
			
			httpRequest= this.wrapRequest(httpRequest, uri);
			if(uri.startsWith("/statics")) { 
				chain.doFilter(httpRequest, httpResponse);
				return; 
			}
			
			//js 页面 jstl 国际化处理处理文件 直接跳过
			if(uri.endsWith(".js_i18n.jsp")){
				chain.doFilter(request, response);
				return;
			}
			
			if(uri.endsWith("页头顶栏.html") || uri.endsWith("页头导航栏.html")){
				chain.doFilter(request, response);
				return;
			}
			
//			if(uri.endsWith("mvc")){
//				chain.doFilter(httpRequest, httpResponse);
//				return; 
//			}
			
//			if(!uri.startsWith("/install") && CopSetting.INSTALL_LOCK.toUpperCase().equals("NO")){
//				httpResponse.sendRedirect(httpRequest.getContextPath()+"/install");
//				return;
//			}
//			if(!uri.startsWith("/install.html") &&uri.startsWith("/install")
//			  && !uri.startsWith("/install/images")		
//			  && CopSetting.INSTALL_LOCK.toUpperCase().equals("YES")){
//				httpResponse.getWriter().write("如要重新安装，请先删除/install/install.lock文件，并重起web容器");
//				return ;
//			}
			
			if("2".equals(CopSetting.RUNMODE)){
				SaasCopContextIniter.init(httpRequest, httpResponse);
			}else{
				CopContextIniter.init(httpRequest, httpResponse);
			}
			if(uri.startsWith("/rest")){
//				String token = request.getParameter("_token");
				String sign = request.getParameter("_sign");
//				String sign1 = DigestUtils.md5Hex(token+pkey);
				String sign1 =  getSign(httpRequest.getParameterMap(),httpRequest.getMethod());
				if(sign1.equals(sign)){
					chain.doFilter(request, response);
				}else{
					httpResponse.getWriter().println("signerror");
				}
				return;
			}
			/*Processor loginprocessor = SpringContextHolder.getBean("autoLoginProcessor");
			if(loginprocessor!=null)
				loginprocessor.process(1, httpResponse, httpRequest);*/
			
			
			Processor processor = ProcessorFactory.newProcessorInstance(uri,httpRequest);
			
			if (processor == null) {
				chain.doFilter(request, response);
			} else {
				if (uri.equals("/") || uri.endsWith(".html")) {
					boolean result = HttpCacheManager.getIsCached(uri);
					if (result)
						return;
				}
				
				
				
		        if(("".equals(conString) || null==conString)){ 
		            //String servletPath = request.getServletPath();//当前请求url 
		            if(uri.contains("login.html") 
		            		|| uri.contains("common_error.html")
		            		|| uri.contains("goActiveSecondEmail.html")
		            		|| uri.contains("queryCustomerComplaintDetail.html")
		            		|| uri.contains("resetPassword.html")
		            		|| uri.contains("DoExpressCheckout.html")
		            		|| uri.contains("activeCustomerInfo.html")
		            		|| uri.contains("myAccount.html")
		            		|| uri.contains("regProduct.html")
		            		|| uri.contains("updateAutelID.html")
		            		|| uri.contains("iepngfix.htc")
		            		|| uri.contains("supportHeader.html")
		            		|| uri.contains("supportFooter.html")
		            		|| uri.contains("return_url.html")
		            		|| uri.contains("GetExpressCheckout.html")
		            		|| uri.contains("orderDetailInfo.html")
		            		|| uri.contains("OnlinePay.html")
		            		|| uri.contains("activeAndresetPwd.html")
		            		|| uri.contains("myOrders-1-1.html")
		            		|| uri.contains("pay_error.html")
		            		|| uri.contains("pay_success.html")
		            		|| uri.contains("payPalReturn.html")
		            		|| uri.contains("measurecar-1-1.html")
		            		|| uri.contains("myMessageInfo-1-1.html")
		            		|| uri.equals("/")){ 
		               
		            }else { 
		            	try {
		            		/*过滤后台连接，本地环境和生产环境路径不一样
		            		*本地：!uri.startsWith("/autel-cqc/") && !uri.startsWith("/autel")
		            		*生产：
                                        if (uri.startsWith("/admin/")) {
      										if (!uri.startsWith("/admin/themes/"))
        										processor = new BackgroundProcessor();
    									}
		            		*/
		            		if(!uri.startsWith("/autel-cqc") && !uri.startsWith("/autel")){
			            		httpRequest.getSession().invalidate();
			            		Cookie[] cookies=httpRequest.getCookies();
			            		if (null != cookies) {
				            		for(Cookie cookie: cookies){
					            		cookie.setMaxAge(0);
					            		cookie.setPath("/");
					            		httpResponse.addCookie(cookie);
				            		}
			            		}
			            		httpResponse.sendRedirect("login.html");
			            		return;
		            		}
						} catch (Exception e) {
							e.printStackTrace();
						} 
		            	
		            
		            } 
		            
		        } 
				

				
				Response copResponse = processor.process(0, httpResponse,httpRequest);

				InputStream in = copResponse.getInputStream();

				if (in != null) {

					byte[] inbytes = IOUtils.toByteArray(in);
					httpResponse.setContentType(copResponse.getContentType());
					httpResponse.setCharacterEncoding("UTF-8");
					httpResponse.setHeader("Content-Length", ""+ inbytes.length);

					OutputStream output = httpResponse.getOutputStream();
					IOUtils.write(inbytes, output);
				} else {
					chain.doFilter(request, response);
				}

			}
	
		} 	 
		catch (RuntimeException exception) {
			exception.printStackTrace();
			response.setContentType("text/html; charset=UTF-8");
			request.setAttribute("message", exception.getMessage());
			String content = JspUtil.getJspOutput("/commons/error.jsp",
					httpRequest, httpResponse);
			response.getWriter().print(content);
			// response.flushBuffer();
		} catch (UploadException e) {
			e.printStackTrace();
		}finally{
			
			ThreadContextHolder.remove();
			FreeMarkerPaser.remove();
			CopContext.remove();
		} 
	}

	public void destroy() {

	}
	
	private HttpServletRequest wrapRequest(HttpServletRequest request,String url) throws UploadException, IOException{
 
		
		List<String> safeUrlList = CopSetting.safeUrlList;
		for(String safeUrl:safeUrlList){
			if(safeUrl.equals(url)){
				return request;
			}
		}
		return new SafeHttpRequestWrapper(request);//包装安全request
	}
	
	public String get404Html(String url){
		String themeFld = CopSetting.cop_PATH+"/themes/" ;
		Map data  = new HashMap();
		data.put("url", url);
		Configuration cfg;
		try {
			cfg = FreeMarkerUtil.getFolderCfg(themeFld);
			Template temp = cfg.getTemplate("404.html");
			ByteOutputStream stream = new ByteOutputStream();
			
			Writer out = new OutputStreamWriter(stream);
			temp.process(data, out);
			
			out.flush();
			String html = stream.toString() ;
			return html;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

	}
	private  String getSign(Map<String,String[]> map,String method){
		ArrayList<String> list = new ArrayList<String>();
        for(Iterator iter = map.keySet().iterator(); iter.hasNext();){
//            if(entry.getValue()!=null&&entry.getValue().equals("")){
//                list.add(entry.getKey() + "=" + entry.getValue() + "&");
//            }
        	String name = (String) iter.next();
			String[] values = (String[]) map.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]: valueStr + values[i] + ",";
			}
			
			if(!name.equals("_sign")&&!valueStr.equals("")){
				if("get".equals(method.toLowerCase())){
					try {
						valueStr = new String(valueStr.getBytes("ISO-8859-1"),"UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
				list.add(name + "=" + valueStr + "&");
			}
        }
        int size = list.size();
        String [] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size; i ++) {
            sb.append(arrayToSort[i]);
        }
        String sign = sb.toString();
        sign =sign+pkey;
    	try {
			sign = DigestUtils.md5Hex(sign.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			sign = DigestUtils.md5Hex(sign);
		}
        return sign;
	}
}