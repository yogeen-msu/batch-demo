package com.cheriscon.cop.processor.backend;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cheriscon.cop.processor.FacadePage;
import com.cheriscon.cop.processor.Processor;
import com.cheriscon.cop.processor.backend.support.BackPageGetter;
import com.cheriscon.cop.processor.backend.support.MenuJsonGetter;
import com.cheriscon.cop.processor.core.LocalRequest;
import com.cheriscon.cop.processor.core.Request;
import com.cheriscon.cop.processor.core.Response;
import com.cheriscon.cop.processor.core.freemarker.FreeMarkerPaser;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.ConnectType;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.user.IUserService;
import com.cheriscon.cop.sdk.user.UserContext;
import com.cheriscon.cop.sdk.user.UserServiceFactory;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.context.webcontext.WebSessionContext;
import com.cheriscon.framework.util.HttpUtil;

/**
 * 后台页面解析器
 * @author kingapex
 * @version 1.0
 * @created 11-十月-2009 16:20:08
 */
public class BackgroundProcessor implements Processor {
	protected final Logger logger = Logger.getLogger(getClass());
	/**
	 * 
	 * @param httpResponse
	 * @param httpRequest
	 */
	public Response process(int model,HttpServletResponse httpResponse, HttpServletRequest httpRequest){
		String uri = httpRequest.getServletPath(); 
		
		
		 IUserService userService = UserServiceFactory.getUserService();
		 
		 if(!userService.isUserLoggedIn()){
		  if(!uri.startsWith("/autel-cqc/admin-login.login") && !uri.startsWith("/autel/cloud/login.do")
			 && !uri.startsWith("/autel-cqc/login.do") && !uri.startsWith("/autel-cqc/menu.do")
			 &&!uri.startsWith("/autel/cloud")){
				List<String> msgs = new ArrayList<String>();
			 	Map<String ,String> urls = new HashMap<String,String>();
			 	msgs.add(FreeMarkerPaser.getBundleValue("system.nologin.tip"));
			 	String ctx = httpRequest.getContextPath();
			 	String conString = httpRequest.getHeader("REFERER");//获取父url
			 	if(conString!=null){
			 	urls.put(FreeMarkerPaser.getBundleValue("system.nologin.tologin"), ctx+"/autel-cqc/admin-login.login");
			 	}
			 	httpRequest.setAttribute("msgs", msgs);
			 	httpRequest.setAttribute("urls", urls);
				httpRequest.setAttribute("target", "_top");
				Request request = new LocalRequest();
				Response response = request.execute("/admin/message.jsp", httpResponse,
						httpRequest);
				return response;
		  }
		 }
		 
//		//用户身份校验
//		 if( !uri.startsWith("/admin/login") && ! uri.startsWith("/admin/index.jsp") 
//			&& !uri.equals("/admin/") && !uri.equals("/admin") && !uri.startsWith("/autel/cloud/login.do") ){
//			// logger.debug(" 处理登录。。。");
//	
//			 if(!userService.isUserLoggedIn()){
//				 	List<String> msgs = new ArrayList<String>();
//				 	Map<String ,String> urls = new HashMap<String,String>();
////				 	msgs.add("您尚未登录或登录已经超时，请重新登录。");
//				 	msgs.add(FreeMarkerPaser.getBundleValue("system.nologin.tip"));
//				 	String ctx = httpRequest.getContextPath();
////				 	urls.put("点进这里进入登录页面", ctx+"/admin/");
//				 	urls.put(FreeMarkerPaser.getBundleValue("system.nologin.tologin"), ctx+"/admin/");
//				 	httpRequest.setAttribute("msgs", msgs);
//				 	httpRequest.setAttribute("urls", urls);
//					httpRequest.setAttribute("target", "_top");
////					httpRequest.setAttribute("message", "您尚未登录或登录已经超时，请重新登录。");
////					httpRequest.setAttribute("link", "index.jsp");
////					httpRequest.setAttribute("linkmessage", "点进这里进入登录页面");		
//					Request request = new LocalRequest();
//					Response response = request.execute("/admin/message.jsp", httpResponse,
//							httpRequest);
//					return response;
//			 }
//		 }
		
		Processor processor  =null;
		
		CopSite site  = CopContext.getContext().getCurrentSite();
 
		model = ConnectType.local;
		FacadePage page = new FacadePage(site);
 
		page.setUri(uri);
		
		if(uri.startsWith("/autel-cqc/menu.do")){
			processor = new MenuJsonGetter(page);
		}else if( uri.startsWith("/autel-cqc/login.do") ){
			processor = new LoginProcessor();
		}else if( uri.startsWith("/autel-cqc/logout.do") ) {
			processor = new LogoutProcessor();
		}else if(uri.startsWith("/autel-cqc/plugin")){
			processor = new AjaxPluginProcessor();
		}else if(uri.startsWith("/autel/cloud")){
			processor = new BackPageGetter(page); 
		}else if(uri.startsWith("/autel/cloud/MaxiSysToPro.do")){
			processor = new BackPageGetter(page); 
		}
		else{
			String conString = httpRequest.getHeader("REFERER");//获取父url
			if(null==conString || ""==conString || "".equals(conString)){
			    if(uri.startsWith("/autel-cqc/admin-login.login")
						|| uri.equals("/autel-cqc/main.jsp") 
						|| uri.equals("/autel/product/toSoftwareVersion.do") 
						|| uri.equals("/autel/product/toSoftwareType.do")
						|| uri.equals("/autel/product/search.do")
						|| uri.equals("/autel/product/search.do")
						|| uri.equals("/autel/member/updateCustomerPassword.do")
						|| uri.equals("/autel/market/toAddSalContract.do")
						|| uri.equals("/autel/market/toUpdateSaleContract.do")
						){
			    	processor = new BackPageGetter(page); 
			    	
			    }else{
			    	processor = new LogoutProcessor();
			    }
			}else{	
			processor = new BackPageGetter(page); //负责显示后台内容界面
			}
		}
		Response response = processor.process(model, httpResponse, httpRequest);		
		Locale locale = httpRequest.getLocale();
		String local =locale.getLanguage()+"-"+locale.getCountry();
		HttpUtil.updateCookie(httpRequest,httpResponse,"in18_lan",local.toLowerCase(),60000);
		return response;
	}
}