package com.cheriscon.cop.processor.backend;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cheriscon.cop.processor.Processor;
import com.cheriscon.cop.processor.core.RemoteRequest;
import com.cheriscon.cop.processor.core.Request;
import com.cheriscon.cop.processor.core.Response;
import com.cheriscon.cop.processor.core.StringResponse;
import com.cheriscon.cop.sdk.user.UserContext;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.context.webcontext.WebSessionContext;

public class LogoutProcessor implements Processor {

	
	public Response process(int mode, HttpServletResponse httpResponse,
			HttpServletRequest httpRequest) {
		
		WebSessionContext<UserContext> sessonContext = ThreadContextHolder.getSessionContext();			
		Response response= new StringResponse();
		sessonContext.removeAttribute(UserContext.CONTEXT_KEY);
		response.setContent("<script>location.href='index.jsp'</script>");
		return response;
	}
 
}
