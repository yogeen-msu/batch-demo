package com.cheriscon.cop.processor.backend;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.cop.processor.Processor;
import com.cheriscon.cop.processor.core.CopException;
import com.cheriscon.cop.processor.core.Response;
import com.cheriscon.cop.processor.core.StringResponse;
import com.cheriscon.cop.sdk.utils.ValidCodeServlet;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.context.webcontext.WebSessionContext;
import com.cheriscon.framework.util.EncryptionUtil;
import com.cheriscon.framework.util.HttpUtil;
import com.cheriscon.framework.util.LanguageUtil;
import com.cheriscon.framework.util.StringUtil;

/**
 * 用户登录处理器
 * @author kingapex
 * <p>2009-12-16 上午10:20:33</p>
 * @version 1.0
 */
public class LoginProcessor implements Processor {

	protected final Logger logger = Logger.getLogger(getClass());
	
	/**
	 * 进行用户登录操作<br/>
	 * 如果登录成功，则生成应用的domain json供SSO的javascript使用
	 */
	@SuppressWarnings("unchecked")
	
	public Response process(int mode,  HttpServletResponse httpResponse,
			HttpServletRequest httpRequest) {
		String type = httpRequest.getParameter("type");
		if(type==null || "".equals(type)){
			return this.userLogin(httpResponse, httpRequest);
		}else{
			return this.sysLogin(httpResponse, httpRequest);
		}
		
	
	}
	
	
	/**
	 * 系统跳转登录后台
	 * @param httpResponse
	 * @param httpRequest
	 * @return
	 */
	public Response sysLogin(HttpServletResponse httpResponse,
			HttpServletRequest httpRequest){
		Response response = new StringResponse();
		
		String endata =  httpRequest.getParameter("s");
		if(endata==null){ response.setContent(LanguageUtil.getBundleValue("login.illegaldata")); return response;}
		
		endata =  EncryptionUtil.authCode(endata, "DECODE");
		String[] ar = StringUtils.split(endata,",");
		//if(ar==null||ar.length!=3){ response.setContent("非法数据"); return response;}
		
		String username = ar [0];
		String password = ar [1];
		Long dateline = Long.valueOf(ar[2]);
		
//		if(new Date().getTime() - dateline>5000){
//			 response.setContent("已经过期"); return response;
//		}
		 
		try {
			
			/*
			 * 登录校验
			 */
			IAdminUserManager userManager =SpringContextHolder.getBean("adminUserManager");
			int result = userManager.loginBySys(username, password);
			if(result==1){
				response.setContent( "<script>location.href='main.jsp'</script>"+LanguageUtil.getBundleValue("login.forwardbackend"));
			}else{
				response.setContent("{'result':1,'message':"+LanguageUtil.getBundleValue("login.loinerror")+"}");
			}
			return response;
			
		} catch (CopException exception) {
			
			response.setContent("{'result':1,'message':'"+exception.getMessage()+"'}");
			return response;
		}	 
		
	}
	
	/**
	 * 用户手工登录后台
	 * @param httpResponse
	 * @param httpRequest
	 * @return
	 */
	public Response userLogin(HttpServletResponse httpResponse,
			HttpServletRequest httpRequest){
		
		try {
			
			String username = httpRequest.getParameter("username");
			if(StringUtils.isBlank(username)) throw new CopException(LanguageUtil.getBundleValue("login.usernameisempty"));	
			String password = httpRequest.getParameter("password");
			if(StringUtils.isBlank(password)) throw new CopException(LanguageUtil.getBundleValue("login.passwordisempty"));	
			String valid_code = httpRequest.getParameter("valid_code");
			
			/*
			 * 校验验证码
			 */
			if(valid_code==null) throw new CopException(LanguageUtil.getBundleValue("login.captcherror"));			
			WebSessionContext sessonContext = ThreadContextHolder.getSessionContext();			
			Object realCode = sessonContext.getAttribute(ValidCodeServlet.SESSION_VALID_CODE+"admin");
			
			if(!valid_code.equals(realCode)){
				throw new CopException(LanguageUtil.getBundleValue("login.captcherror"));
			}
			 
			/*
			 * 登录校验
			 */
			IAdminUserManager userManager =SpringContextHolder.getBean("adminUserManager");
			userManager.login(username, password);
 
			StringBuffer json = new StringBuffer();
			json.append("{'result':0}");
		 
			
			Response response = new StringResponse();
			response.setContent( json.toString() );
			
			String remember_login_name = httpRequest.getParameter("remember_login_name");
			if(!StringUtil.isEmpty(remember_login_name)){ //记住用户名
				HttpUtil.addCookie(httpResponse, "loginname", username, 365*24*60*60);
			}else{  //删除用户名
				HttpUtil.addCookie(httpResponse, "loginname", "", 0);
			}
			
			
			return response;
			 
		} catch (RuntimeException exception) {
//			exception.printStackTrace();
			this.logger.error(exception.getMessage(),exception.fillInStackTrace());
			Response response = new StringResponse();
			response.setContent("{'result':1,'message':'"+exception.getMessage()+"'}");
			return response;
		}	 
	}
 

}
