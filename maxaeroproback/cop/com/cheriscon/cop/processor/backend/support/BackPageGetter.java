package com.cheriscon.cop.processor.backend.support;

import javax.servlet.http.HttpServletRequest;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.cop.processor.AbstractFacadeProcessor;
import com.cheriscon.cop.processor.FacadePage;
import com.cheriscon.cop.processor.core.Response;
import com.cheriscon.cop.resource.IAdminThemeManager;
import com.cheriscon.cop.resource.model.AdminTheme;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.util.HttpUtil;

/**
 * 后台页面获取器
 * @author kingapex
 * 2010-9-13下午12:13:24
 */
public class BackPageGetter extends AbstractFacadeProcessor {
	
	private IAdminThemeManager adminThemeManager;
	private IAdminUserManager adminUserManager;
	public BackPageGetter(FacadePage page) {
		super(page);
	}

	
	protected Response process() {

		CopSite site = CopContext.getContext().getCurrentSite();//this.page.getSite();
		
		adminThemeManager = SpringContextHolder.getBean("adminThemeManager");
		adminUserManager  = SpringContextHolder.getBean("adminUserManager");
		//读取后台使用的模板
		AdminTheme theme = adminThemeManager.get( site.getAdminthemeid());
		String path ="default";
		if(theme!=null){
			path = theme.getPath();
		}
		StringBuffer context = new StringBuffer();

		// context.append(CopSetting.IMG_SERVER_DOMAIN);
		//当前用户的上下文
		//String contextPath  = CopContext.getContext().getContextPath();
//		context.append(contextPath);
		context.append(CopSetting.ADMINTHEMES_STORAGE_PATH);
		context.append("/");
		context.append(path);
		
		//StringBuffer  staticdomain = new StringBuffer();
		
		/***********************20100920修改：***************************/
//		//静态资源分离模式
//		if(CopSetting.RESOURCEMODE.equals("1")){
//			staticdomain.append(CopSetting.IMG_SERVER_DOMAIN);
//		}
		//静态资源合并模式
//		if(CopSetting.RESOURCEMODE.equals("2")){
			//if("/".equals(CopSetting.CONTEXT_PATH) )
			//	staticdomain.append("");
		//	else	
		//		staticdomain.append(CopSetting.CONTEXT_PATH);
//		}
		HttpServletRequest httpRequest  = ThreadContextHolder.getHttpRequest();
			String contextPath  =httpRequest.getContextPath();
			
		// 设置页面上变量的值
		httpRequest.setAttribute("context", contextPath + context.toString()); //静态资源上下文		
		httpRequest.setAttribute("title",site.getSitename()); //站点名称
		httpRequest.setAttribute("ico",site.getIcofile());    //ico文件
		httpRequest.setAttribute("logo", site.getLogofile()) ; //logo文件
		httpRequest.setAttribute("version", CopSetting.VERSION) ; //版本
		httpRequest.setAttribute("bkloginpicfile", site.getBkloginpicfile()); //后台登录logo
		httpRequest.setAttribute("bklogo", site.getBklogofile()==null?site.getLogofile():site.getBklogofile()); //后台主界面logo
		
		String uri = page.getUri();

		if (uri.startsWith("/autel-cqc/main")) { //后台首页
			AdminUser user  =adminUserManager.getCurrentUser();
			user = adminUserManager.get(user.getUserid());
			httpRequest.setAttribute("user", user);
			uri = context.toString() + "/main.jsp";
//			request  = new GetPointJsWrapper(page, request); //包装积分获取js
			request = new HelpDivWrapper(page, request);
		
		} else if (uri.equals("/autel-cqc/admin-login.login")) { //登录页面
			//读取记住的用户名
			String username  = HttpUtil.getCookieValue(httpRequest, "loginname");
			httpRequest.setAttribute("username", username);
			uri =  context.toString() + "/login.jsp";
		} else {
			
			if(CopSetting.EXTENSION.equals("action")){
				uri = uri.replace(".do", ".action");
			}
			
			String ajax = httpRequest.getParameter("ajax");
			if(!"yes".equals(ajax)){ //非异步包装后台内容界面
				request = new BackTemplateWrapper(page, request); 
				request = new HelpDivWrapper(page, request);
				
			}
			
		}

		Response response = request.execute(uri, httpResponse, httpRequest);
	 
		return response;

	}



	
}