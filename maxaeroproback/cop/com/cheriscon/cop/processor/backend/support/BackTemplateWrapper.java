package com.cheriscon.cop.processor.backend.support;

import com.cheriscon.cop.processor.AbstractFacadePagetParser;
import com.cheriscon.cop.processor.FacadePage;
import com.cheriscon.cop.processor.core.Request;
import com.cheriscon.cop.processor.core.Response;
import com.cheriscon.cop.sdk.utils.JspUtil;

/**
 * 后台页面包装器<br>
 * 用/admin/main_frame.jsp包装业务页面 <br>
 * @author kingapex
 * 2010-9-13下午12:17:04
 */
public class BackTemplateWrapper extends AbstractFacadePagetParser {
	
	public BackTemplateWrapper(FacadePage page, Request request) {
		super(page, request);
	}

	
	protected Response parse(Response response) {
		httpRequest.setAttribute("content", response.getContent());
		String content  = JspUtil.getJspOutput("/admin/main_frame.jsp", httpRequest, httpResponse);
		response.setContent(content);
		return response;
	}

}
