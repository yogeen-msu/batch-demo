package com.cheriscon.cop.resource.impl;

import com.cheriscon.cop.resource.IUserDetailManager;
import com.cheriscon.cop.resource.model.CopUserDetail;
import com.cheriscon.framework.database.IDaoSupport;
/**
 * 用户详细信息管理
 * @author kingapex
 *2010-5-10下午12:36:16
 */
public class UserDetailManagerImpl implements IUserDetailManager {
	private IDaoSupport<CopUserDetail> daoSupport;
	
	public void add(CopUserDetail CopUserDetail) {
		this.daoSupport.insert("cop_userdetail", CopUserDetail);
	}

	
	public void edit(CopUserDetail CopUserDetail) {
		this.daoSupport.update("cop_userdetail", CopUserDetail, " userid = " + CopUserDetail.getUserid());
	}

	
	public CopUserDetail get(Integer userid) {
		return this.daoSupport.queryForObject(	"select * from cop_userdetail where userid = ?",
				CopUserDetail.class, userid);
	}

	public IDaoSupport<CopUserDetail> getDaoSupport() {
		return daoSupport;
	}

	public void setDaoSupport(IDaoSupport<CopUserDetail> daoSupport) {
		this.daoSupport = daoSupport;
	}

	
}
