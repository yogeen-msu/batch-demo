package com.cheriscon.cop.resource.impl;

import java.util.List;

import com.cheriscon.cop.resource.IBorderManager;
import com.cheriscon.cop.resource.model.Border;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.IDBRouter;
import com.cheriscon.framework.database.IDaoSupport;

/**
 * saas式的边框管理 
 * @author kingapex
 * 2010-1-28下午05:41:09
 */
public class BorderManagerImpl extends BaseSupport<Border>  implements IBorderManager {
 
	public void clean() {
		this.baseDaoSupport.execute("truncate table border");
	}
	
	
	public void add(Border border) {
		this.baseDaoSupport.insert("border", border);
	}

	
	public void delete(Integer id) {
		this.baseDaoSupport.execute("delete from border where id=?", id);
	}

	
	public List<Border> list() {
		String sql  ="select * from  border";
		List<Border> list = this.baseDaoSupport.queryForList(sql, Border.class);
		return list;
	}

	
	public void update(Border border) {
		this.baseDaoSupport.update("border", border, "id="+border.getId());
	}

 
	
}
