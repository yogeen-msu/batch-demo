package com.cheriscon.cop.resource.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.app.base.core.service.auth.IAdminUserManager;
import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.IUserManager;
import com.cheriscon.cop.resource.model.CopUser;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.user.UserContext;
import com.cheriscon.framework.context.webcontext.ThreadContextHolder;
import com.cheriscon.framework.context.webcontext.WebSessionContext;
import com.cheriscon.framework.database.IDaoSupport;
import com.cheriscon.framework.database.Page;
import com.cheriscon.framework.util.StringUtil;

/**
 * 易族易站用户管理 
 * @author kingapex
 * 2010-11-8下午02:22:24
 */
public class UserManagerImpl implements IUserManager {
	private IDaoSupport daoSupport;
	private ISiteManager siteManager;
	private IAdminUserManager adminUserManager;
	protected final Logger logger = Logger.getLogger(getClass());
	
	public void changeDefaultSite(Integer userid, Integer managerid,
			Integer defaultsiteid) {
		UserUtil.validUser(userid);
		String sql  ="update cop_user set defaultsiteid=? where id=?";
		  daoSupport.execute(sql, defaultsiteid,userid);
	}

	/**
	 * 创建用户
	 * @param user 创建一个用户
	 * @return 此用户的用户id
	 * @throws RuntimeException 用户名已经存在
	 */ 
	@Transactional(propagation = Propagation.REQUIRED)
	public Integer createUser(CopUser user) {
		//检测用户是否已经存在
		String sql ="select count(0) from cop_user where username=?";
		int count = this.daoSupport.queryForInt(sql, user.getUsername());
		if(count>0) throw new RuntimeException("用户"+ user.getUsername()+"已存在");
		user.setPassword(StringUtil.md5(user.getPassword()));
		
		this.daoSupport.insert("cop_user", user);
		Integer userid = this.daoSupport.getLastId("cop_user");

		return userid;
	}

	
	
	/**
	 * 检测用户名是否存在
	 * @param username
	 * @return 存在返回true，不存在返回false
	 * @author kingapex
	 */
	private boolean checkUserName(String username){
		String sql ="select * from cop_user where username=?";
		List list  = this.daoSupport.queryForList(sql, username);
		if(list== null || list.isEmpty() || list.size()==0) 
			return false;
		else
			return true;
	}
	
	
	/**
	 * 平台用户登录
	 */
	public int login(String username, String password) {
		int result=0;
		try{
			CopUser user = this.get(username);
			if(user.getPassword().equals( StringUtil.md5(password) )){
				result =1;
				WebSessionContext<CopUser> sessonContext = ThreadContextHolder
				.getSessionContext();	
				sessonContext.setAttribute(IUserManager.USER_SESSION_KEY, user);

			}else{
				result =0;
			}
		}catch(RuntimeException e){
			this.logger.error(e.fillInStackTrace());
		}
	
		return result;
	}
 
 
	public int checkIsLogin() {
		WebSessionContext<CopUser> sessonContext = ThreadContextHolder
		.getSessionContext();	
		CopUser user = sessonContext.getAttribute(IUserManager.USER_SESSION_KEY);
		if(user!=null)
			return 1;
		else
			return 0;
	}
	
	public void logout() {
		
		WebSessionContext<UserContext> sessonContext = ThreadContextHolder.getSessionContext();		
		sessonContext.removeAttribute(IUserManager.USER_SESSION_KEY);
		
		ThreadContextHolder.getSessionContext().removeAttribute("userAdmin");	
		
	}
	
	
	/**
	 * 获取当前登录用户
	 * @return 当前登录的用户
	 * @throws RuntimeException 用户未登录抛出此异常
	 */
	public CopUser getCurrentUser() {
		WebSessionContext<CopUser> sessonContext = ThreadContextHolder.getSessionContext();	
		CopUser user = sessonContext.getAttribute(IUserManager.USER_SESSION_KEY);
		return user;
	}
	
	
	public CopUser get(Integer userid) {
		String sql ="select * from cop_user where deleteflag = 0 and id = ?";
		return (CopUser)this.daoSupport.queryForObject(sql, CopUser.class, userid);
	}
	
	

	public IDaoSupport<CopUser> getDaoSupport() {
		return daoSupport;
	}



	public void setDaoSupport(IDaoSupport<CopUser> daoSupport) {
		this.daoSupport = daoSupport;
	}



	public ISiteManager getSiteManager() {
		return siteManager;
	}



	public void setSiteManager(ISiteManager siteManager) {
		this.siteManager = siteManager;
	}



	public IAdminUserManager getAdminUserManager() {
		return adminUserManager;
	}



	public void setAdminUserManager(IAdminUserManager adminUserManager) {
		this.adminUserManager = adminUserManager;
	}


	
	public void edit(CopUser user) {
		this.daoSupport.update("cop_user", user, "id = "+user.getId());
	}




	public CopUser get(String username) {
		return  (CopUser)this.daoSupport.queryForObject("select * from cop_user where username=?", CopUser.class, username);
	}


	public Page list(String keyword, int pageNo, int pageSize) {
		String sql ="select u.*,d.regdate  regdate from cop_user u left join cop_userdetail d on  u.id= d.userid";
		if(!StringUtil.isEmpty(keyword)){
			sql+=" where  u.username like '%" + keyword +"%'";
		
		}
		sql+=" order by  d.regdate desc";
		return this.daoSupport.queryForPage(sql, pageNo, pageSize);
	}
	
	
	
	/**
	 * 删除某用户信息，会删除 此用户的所有站点 
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(Integer userid){
		
		//删除此用户的所有站点
		List<Map> siteList  = this.siteManager.list(userid);
		for(Map site: siteList){
			siteManager.delete((Integer)site.get("id"));
		}
		
		this.daoSupport.execute("delete from cop_userdetail where userid = ?", userid); //删除用户详细表
		this.daoSupport.execute("delete from cop_useradmin where userid = ?", userid); //删除管理员表
		this.daoSupport.execute("delete from cop_user where id = ?", userid); //删除用户信息
	}

}
