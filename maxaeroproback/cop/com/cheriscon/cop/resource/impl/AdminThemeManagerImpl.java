package com.cheriscon.cop.resource.impl;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cheriscon.cop.resource.IAdminThemeManager;
import com.cheriscon.cop.resource.model.AdminTheme;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.database.IDaoSupport;
import com.cheriscon.framework.util.FileUtil;

/**
 * 后台主题管理
 * @author kingapex
 *2010-5-9下午07:46:18
 */
public class AdminThemeManagerImpl extends BaseSupport<AdminTheme> implements IAdminThemeManager {

 
	@Transactional(propagation = Propagation.REQUIRED)
	public Integer add(AdminTheme theme,boolean isCommon) {
		 
		try {
			//this.copy(theme,isCommon);
			this.baseDaoSupport.insert("admintheme", theme);
			return this.baseDaoSupport.getLastId("admintheme");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("安装后台主题出错");
		}
		
	}
	
	public void clean() {
		this.baseDaoSupport.execute("truncate table admintheme");
	}
	
	
	private void copy(AdminTheme theme,boolean isCommon) throws Exception  {
	 
		CopSite site  = CopContext.getContext().getCurrentSite();

		//公用模板由common目录复制，非公用由产品目录复制
		String basePath =isCommon?CopSetting.APP_DATA_STORAGE_PATH:CopSetting.PRODUCTS_STORAGE_PATH+ "/" + theme.getProductId();
		basePath= basePath +"/adminthemes";
		
		
		String contextPath = CopContext.getContext().getContextPath();
		//复制图片至静态资源服务器
		String targetPath = CopSetting.IMG_SERVER_PATH   +contextPath + "/adminthemes/"+ theme.getPath() ;
		FileUtil.copyFolder(basePath + "/" + theme.getPath() + "/images",targetPath+ "/images");
		FileUtil.copyFile(basePath + "/" + theme.getPath() + "/preview.png",targetPath+ "/preview.png");
		FileUtil.copyFolder(basePath + "/" + theme.getPath() + "/css",targetPath+ "/css");
		FileUtil.copyFolder(basePath + "/" + theme.getPath() + "/js",targetPath+ "/js");
		
		
		FileUtil.copyFolder(basePath + "/" + theme.getPath() ,CopSetting.cop_PATH
				+contextPath
				+ "/adminthemes/" + theme.getPath());
		/*
		 * 只考jsp到cop应用服务器中
		
		IOFileFilter txtSuffixFilter = FileFilterUtils.suffixFileFilter(".jsp");
		IOFileFilter txtFiles = FileFilterUtils.andFileFilter(FileFileFilter.FILE, txtSuffixFilter);

		  
		FileUtils.copyDirectory(
		new File(basePath + "/" + theme.getPath() )
		, 
		
		new File(CopSetting.cop_PATH
		+ "/user/"
		+ userid
		+ "/"
		+ siteid
		+ "/adminthemes/" + theme.getPath())
		, 
		txtFiles
		);

		 */
	}
	
	
	
	public AdminTheme get(Integer themeid) {
		List<AdminTheme> list= this.baseDaoSupport.queryForList("select * from admintheme where id=?", AdminTheme.class, themeid);
		if(list==null || list.isEmpty()) return null;
		else 
		return list.get(0);
	}

	
	public List<AdminTheme> list() {
		
		return this.baseDaoSupport.queryForList("select * from admintheme ", AdminTheme.class);
	}

	public IDaoSupport<AdminTheme> getDaoSupport() {
		return daoSupport;
	}

	public void setDaoSupport(IDaoSupport<AdminTheme> daoSupport) {
		this.daoSupport = daoSupport;
	}
	



}
