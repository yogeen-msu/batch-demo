package com.cheriscon.cop.resource.impl;

import java.util.List;

import com.cheriscon.cop.resource.IDomainManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.CopSiteDomain;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.framework.database.IDaoSupport;

/**
 * 域名管理
 * 
 * @author kingapex 2010-5-9下午08:14:11
 */
public class DomainManagerImpl implements IDomainManager {

	private IDaoSupport<CopSiteDomain> daoSupport;

	
	
	public CopSiteDomain get(Integer id) {
		String sql = "select * from cop_sitedomain where id = ?";
		return daoSupport.queryForObject(sql, CopSiteDomain.class, id);
	}

	
	public List<CopSiteDomain> listUserDomain() {
		Integer userid = CopContext.getContext().getCurrentSite().getUserid();
		String sql = "select * from cop_sitedomain where userid=?";
		return this.daoSupport.queryForList(sql, CopSiteDomain.class, userid);
	}

	
	public List<CopSiteDomain> listSiteDomain() {
		CopSite site = CopContext.getContext().getCurrentSite();
		String sql = "select * from cop_sitedomain where userid=? and siteid =?";
		return this.daoSupport.queryForList(sql, CopSiteDomain.class, site
				.getUserid(), site.getId());
	}
	
	public List<CopSiteDomain> listSiteDomain(Integer siteid) {
		String sql = "select * from cop_sitedomain where  siteid =?";
		return this.daoSupport.queryForList(sql, CopSiteDomain.class, siteid);
	}
	
	public void edit(CopSiteDomain domain) {
		this.daoSupport.update("cop_sitedomain", domain, " id = "
				+ domain.getId());
	}

	public IDaoSupport<CopSiteDomain> getDaoSupport() {
		return daoSupport;
	}

	public void setDaoSupport(IDaoSupport<CopSiteDomain> daoSupport) {
		this.daoSupport = daoSupport;
	}

	
	public int getDomainCount(Integer siteid) {
		String sql = "select count(0)  from cop_sitedomain where  siteid =?";
		return this.daoSupport.queryForInt(sql, siteid);
	}




}
