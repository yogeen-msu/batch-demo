package com.cheriscon.cop.resource;

import java.util.List;

import com.cheriscon.cop.resource.model.CopApp;

/**
 * 应用管理 * 
 * @author lzf
 *         <p>
 *         created_time 2009-12-14 上午10:10:41
 *         </p>
 * @version 1.0
 */
public interface IAppManager {

	
	/**
	 * 添加一个应用
	 * @param app
	 */
	public void add(CopApp app);
	
	/**
	 * 获取所有应用列表
	 * @return
	 */
	public List<CopApp> list();
	
	
	/**
	 * 获取某个应用
	 * @param appid
	 * @return
	 */
	public CopApp get(String appid);

}
