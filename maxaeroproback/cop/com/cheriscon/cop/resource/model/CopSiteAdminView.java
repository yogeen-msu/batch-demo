package com.cheriscon.cop.resource.model;

/**
 * @author lzf
 *         <p>
 *         created_time 2009-12-9 下午02:01:24
 *         </p>
 * @version 1.0
 */
public class CopSiteAdminView extends CopSiteAdmin {
	private String sitename;

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
}
