package com.cheriscon.cop.resource;

import com.cheriscon.cop.resource.model.CopUserDetail;

/**
 * 用户详细管理接口
 * @author kingapex
 *2010-5-10下午12:33:51
 */
public interface IUserDetailManager {
	
	/**
	 * 读取详细
	 * @param userid
	 * @return
	 */
	public CopUserDetail get(Integer userid);

	
	/**
	 * 添加
	 * @param CopUserDetail
	 */
	public void add(CopUserDetail CopUserDetail);

	
	
	/**
	 * 修改
	 * @param CopUserDetail
	 */
	public void edit(CopUserDetail CopUserDetail);
}
