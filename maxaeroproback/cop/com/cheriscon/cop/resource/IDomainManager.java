package com.cheriscon.cop.resource;

import java.util.List;

import com.cheriscon.cop.resource.model.CopSiteDomain;

/**
 * 域名管理
 * @author kingapex
 *2010-5-9下午08:13:24
 */
public interface IDomainManager {
	
	/**
	 * 根据id获取域名
	 * @param id
	 * @return
	 */
	public CopSiteDomain get(Integer id);
	
	
	
	/**
	 * 修改域名
	 * @param domain
	 */
	public void edit(CopSiteDomain domain);
	
	
	
	
	/**
	 * 获取某用户的所有域名
	 * @param userid
	 * @return
	 */
	public List<CopSiteDomain> listUserDomain();
	
	
	
	/**
	 * 读取当前站点的所有域名
	 * @param userid
	 * @param siteid
	 * @return
	 */
	public List<CopSiteDomain> listSiteDomain();
	
	
	/**
	 * 读取某站点的所有域名
	 * @param userid
	 * @param siteid
	 * @return
	 */
	public List<CopSiteDomain> listSiteDomain(Integer siteid);
	
	
	
	/**
	 * 读取某个站点的域名个数
	 * @param siteid
	 * @return
	 */
	public int getDomainCount(Integer siteid);
	
}
