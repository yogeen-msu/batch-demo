package com.cheriscon.cop.resource.dto;

import com.cheriscon.cop.processor.core.CopException;
import com.cheriscon.cop.resource.model.AdminUser;
import com.cheriscon.cop.resource.model.CopUser;
import com.cheriscon.cop.resource.model.CopUserDetail;

/**
 * @author lzf
 *         <p>
 *         created_time 2009-12-10 下午05:53:44
 *         </p>
 * @version 1.0
 */
public class UserDTO {
	private CopUser user;
	private CopUserDetail userDetail;
	private AdminUser userAdmin;
	private SiteDTO siteDTO;
	private Integer siteid;
	
//	public UserDTO(){
//		user = new CopUser();
//		userDetail = new CopUserDetail();
//		userAdminList = new ArrayList<CopUserAdmin>();
//		siteList = new ArrayList<SiteDTO>();
//	}
	
	public void vaild(){
		
		if(this.userAdmin==null){
			throw new CopException("用户管理员不能为空！");
		}		
		if(this.userDetail==null){
			throw new CopException("用户详细信息不能为空！");
		}
		if(this.siteDTO==null){
			throw new CopException("用户站点不能为空！");
		}
		siteDTO.vaild();
	}
	
	public void setUserId(Integer userid){
		this.userDetail.setUserid(userid);
		userAdmin.setUserid(userid);
		siteDTO.setUserId(userid);
	}

	public CopUser getUser() {
		return user;
	}

	public void setUser(CopUser user) {
		this.user = user;
	}

	public CopUserDetail getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(CopUserDetail userDetail) {
		this.userDetail = userDetail;
	}

	public AdminUser getUserAdmin() {
		return userAdmin;
	}

	public void setUserAdmin(AdminUser userAdmin) {
		this.userAdmin = userAdmin;
	}

	public SiteDTO getSiteDTO() {
		return siteDTO;
	}

	public void setSiteDTO(SiteDTO siteDTO) {
		this.siteDTO = siteDTO;
	}

	public Integer getSiteid() {
		return siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteDTO.setSiteId(siteid);
		this.siteid = siteid;
	}
}
