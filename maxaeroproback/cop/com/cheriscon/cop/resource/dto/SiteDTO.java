package com.cheriscon.cop.resource.dto;

import com.cheriscon.cop.processor.core.CopException;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.resource.model.CopSiteAdmin;
import com.cheriscon.cop.resource.model.CopSiteDomain;

/**
 * @author lzf
 *         <p>
 *         created_time 2009-12-10 下午06:21:25
 *         </p>
 * @version 1.0
 */
public class SiteDTO {
	private CopSite site;
	private CopSiteDomain domain;
	private CopSiteAdmin siteAdmin;
	
//	public SiteDTO(){
//		site = new CopSite();
//		domainList = new ArrayList<CopSiteDomain>();
//		listSiteApp = new ArrayList<CopSiteApp>();
//		siteAdminList = new ArrayList<CopSiteAdmin>();
//	}
	
	public void vaild(){
		if(this.domain==null){
			throw new IllegalArgumentException("站点至少要有一个域名！");
		}		
		
		if(this.siteAdmin==null){
			throw new IllegalArgumentException("站点至少应该指定一位管理员！");
		}
	}
	
	public void setUserId(Integer userid){
		site.setUserid(userid);
		domain.setUserid(userid);
		siteAdmin.setUserid(userid);
	}
	
	public void setSiteId(Integer siteid){
		domain.setSiteid(siteid);
		siteAdmin.setSiteid(siteid);
	}
	
	public void setManagerid(Integer managerid){
		siteAdmin.setManagerid(managerid);
	}

	public CopSite getSite() {
		return site;
	}

	public void setSite(CopSite site) {
		this.site = site;
	}

	public CopSiteDomain getDomain() {
		return domain;
	}

	public void setDomain(CopSiteDomain domain) {
		this.domain = domain;
	}

	public CopSiteAdmin getSiteAdmin() {
		return siteAdmin;
	}

	public void setSiteAdmin(CopSiteAdmin siteAdmin) {
		this.siteAdmin = siteAdmin;
	}

}
