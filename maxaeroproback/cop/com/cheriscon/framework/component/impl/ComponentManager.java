package com.cheriscon.framework.component.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cheriscon.cop.resource.ISiteManager;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.database.BaseSupport;
import com.cheriscon.framework.component.ComponentView;
import com.cheriscon.framework.component.IComponent;
import com.cheriscon.framework.component.IComponentManager;
import com.cheriscon.framework.component.PluginView;
import com.cheriscon.framework.component.WidgetView;
import com.cheriscon.framework.component.context.ComponentContext;
import com.cheriscon.framework.component.context.WidgetContext;
import com.cheriscon.framework.context.spring.SpringContextHolder;
import com.cheriscon.framework.plugin.IPlugin;
import com.cheriscon.framework.plugin.IPluginBundle;
import com.cheriscon.framework.util.FileUtil;
import com.cheriscon.framework.util.StringUtil;
import com.cheriscon.framework.util.XMLUtil;
 
/**
 * 组件管理
 * @author kingapex
 *2012-5-15上午6:58:38
 */
public class ComponentManager extends BaseSupport implements IComponentManager {

	/**
	 * 先由组件上下文中加载组件列表
	 * 再由数据库中加载组件状态
	 */
	@Override
	public List<ComponentView> list() {
		 
		List<ComponentView> viewList  = new ArrayList<ComponentView>();
		List<ComponentView>  componentViews =  ComponentContext.getComponents();//加载所有声明的组件
		
		List<ComponentView> dbList = getDbList();
		
		if(componentViews!=null){
			for(ComponentView view:componentViews){
				
				ComponentView componentView =(ComponentView)view.clone();
				
				if(this.logger.isDebugEnabled()){
					this.logger.debug("load component["+ componentView.getName()+"] start ");
				}
				
				
				try {
					componentView.setInstall_state(0); //默认为未安装状态
					componentView.setEnable_state(0); //默认为未启用
					
					
			 
					this.loadComponentState(componentView, dbList); //由数据库加载组件信息
					
					
					if(this.logger.isDebugEnabled()){
						this.logger.debug("load component["+ componentView.getName()+"] end ");
					}
				} catch (Exception e) {
					 this.logger.error("加载组件["+componentView.getName()+"]出错",e);
					 componentView.setEnable_state(2);
					 componentView.setError_message(e.getMessage());
				}  
				viewList.add(componentView);
			}
		}
		
		return viewList;
	}

	/**
	 * 用数据库的组件列表加载组件上下文中的组件状态
	 * @param componentView
	 * @param dbList
	 */
	private void loadComponentState(ComponentView componentView ,List<ComponentView> dbList){
		for(ComponentView dbView:dbList){
			if( dbView.getComponentid().equals( componentView.getComponentid() ) ){
				
				
				
				if(this.logger.isDebugEnabled()){
					this.logger.debug("load component[" +componentView.getName()+ "]state->install state:"+dbView.getInstall_state()+",enable_state:"+dbView.getEnable_state());
				}
				
				 
				
				componentView.setInstall_state(dbView.getInstall_state());
				componentView.setEnable_state( dbView.getEnable_state());
				componentView.setId(dbView.getId());
				
				
				if(componentView.getInstall_state()!=2){
					
					if(dbView.getEnable_state() == 0){
					//	this.stop(dbView.getComponentid()   );
					}else if (dbView.getEnable_state() == 1){
					//	this.start(dbView.getComponentid());
					}else{
						componentView.setError_message(dbView.getError_message());
					}
				}
			}
		}
	}
	
	@Override
	public void install(String componentid) {
		if(this.logger.isDebugEnabled()){
			this.logger.debug("install component["+componentid+"]...");
		}
		
		try{
			ComponentView componentView = this.getComponentView(componentid);
			
			if( componentView!=null ){
				componentView.getComponent().install();
				
				if(!this.isInDb(componentView.getComponentid())){//数据库中还不存在，需要先插入一条
					ComponentView temp = (ComponentView)componentView.clone();
					temp.setInstall_state(1);
					
					this.baseDaoSupport.insert("component", temp);
				 
				}else{
					this.baseDaoSupport.execute("update component set install_state=1 where id=?", componentView.getId());
				}
			}
		}catch(RuntimeException e){
			this.logger.error("安装组件["+componentid+"]出错",e);
		}
		
	}
	
	
	private boolean isInDb(String componentid ){
		String sql  ="select count(0)  from component where componentid=?";
		return this.baseDaoSupport.queryForInt(sql, componentid)>0;
	}

	@Override
	public void unInstall(String componentid) {
		if(this.logger.isDebugEnabled()){
			this.logger.debug("install component["+componentid+"]...");
		}
		
		ComponentView componentView = this.getComponentView(componentid);
		if( componentView!=null ){
			componentView.getComponent().unInstall();
		 
			if(!this.isInDb(componentView.getComponentid())){
				ComponentView temp = (ComponentView)componentView.clone();
				temp.setInstall_state(0);
				this.baseDaoSupport.insert("component", temp);
			}else{
				this.baseDaoSupport.execute("update component set install_state=1 where id=?", componentView.getId());
			}
		}
		
	}
	
	private ComponentView getComponentView(String componentid){
		List<ComponentView> componentList = ComponentContext.getComponents();
		
		
		for( ComponentView componentView : componentList ){
			if ( componentView.getComponentid().equals(componentid) ){
				return componentView;
			}
		}
				
		return null;
	}
	
	/**
	 * 启用某个组件
	 * 将其插件及挂件启用
	 * @param componentid 组件标识id
	 */
	@Override
	public void start(String componentid) {
		
		if(this.logger.isDebugEnabled()){
			this.logger.debug("start component["+componentid+"]...");
		}
		
			ComponentView componentView= this.getComponentView(componentid);
				
				/**
				 * 启用相应插件
				 */
				List<PluginView> pluginList =  componentView.getPluginList();
				for(PluginView pluginView: pluginList){
					String pluginid = pluginView.getId();
					IPlugin plugin = SpringContextHolder.getBean(pluginid);
					
					List<String> bundleList  = pluginView.getBundleList();
					if(bundleList!=null){
						for(String bundleId:bundleList){
							IPluginBundle pluginBundle = SpringContextHolder.getBean(bundleId);
							pluginBundle.registerPlugin(plugin);
						}
					}
					
				 
				}
				
				
				/**
				 * 启用相应挂件
				 */
				List<WidgetView> widgetList  = componentView.getWidgetList();
				for(WidgetView widgetView:widgetList){
					String widgetid  = widgetView.getId();
					WidgetContext.putWidgetState(widgetid, true);
					 
				}
				
				
				/**
				 * 更新数据库的状态 
				 */
				String sql  ="update component set enable_state=1 where componentid=?";
				this.baseDaoSupport.execute(sql, componentid);
				
	 
				
	 
		
		
		if(this.logger.isDebugEnabled()){
			this.logger.debug("start component["+componentid+"] complete");
		}
		
		
	}

	
	/**
	 * 停用某个组件
	 * 将其插件及挂件停用
	 * @param componentid 组件标识id
	 */
	@Override
	public void stop(String componentid) {
		
		if(this.logger.isDebugEnabled()){
			this.logger.debug("stop component["+componentid+"]...");
		}
		
		
		ComponentView componentView= this.getComponentView(componentid);
				
				/**
				 * 
				 * 停用相应插件
				 */
				List<PluginView> pluginList =  componentView.getPluginList();
				for(PluginView pluginView: pluginList){
					String pluginid = pluginView.getId();
					IPlugin plugin = SpringContextHolder.getBean(pluginid);
					List<String> bundleList = pluginView.getBundleList();
					for(String bundleId:bundleList){
						IPluginBundle pluginBundle= SpringContextHolder.getBean(bundleId);
						pluginBundle.unRegisterPlugin(plugin);
					}
					
				}
				
				
				/**
				 * 停用相应挂件
				 */
				List<WidgetView> widgetList  = componentView.getWidgetList();
				for(WidgetView widgetView:widgetList){
					String widgetid  = widgetView.getId();
					WidgetContext.putWidgetState(widgetid, false);
				}
				
				/**
				 * 更新数据库的状态 
				 */
				String sql  ="update component set enable_state=0 where componentid=?";
				this.baseDaoSupport.execute(sql, componentid);
				
				
				
			 
	 
		if(this.logger.isDebugEnabled()){
			this.logger.debug("stop component["+componentid+"] complete");
		}
		
		
		
	}

	
	
	/**
	 * 启动站点组件
	 */
	@Override
	public void startComponents() {
		
		if(this.logger.isDebugEnabled()){
			this.logger.debug("start components start...");
		}
 
		List<ComponentView> dbList = getDbList();
		for(ComponentView componentView:dbList){
			if(componentView.getInstall_state()!=2){
				if (componentView.getEnable_state() == 1){
					this.start(componentView.getComponentid());
				} 
			}
		}

		
		if(this.logger.isDebugEnabled()){
			this.logger.debug("start components end!");
		}
		
	}
	

	@Override
	public void saasStartComponents() {
		
		CopSite site  = CopContext.getContext().getCurrentSite();
		int userid = site.getUserid();
		int siteid = site.getId();
		
		boolean isstart = ComponentContext.getSiteComponentState(userid, siteid);
		if(!isstart){
			this.startComponents();
			ComponentContext.siteComponentStart(userid, siteid);
		}
		
		
	}
 
	
	


	
	private List<ComponentView> getDbList(){
		String sql  ="select * from component ";
		return this.baseDaoSupport.queryForList(sql, ComponentView.class);
	}



	

}
