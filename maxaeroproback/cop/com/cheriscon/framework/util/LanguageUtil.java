package com.cheriscon.framework.util;

import java.util.Locale;
import java.util.ResourceBundle;

import com.cheriscon.cop.sdk.context.CopContext;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.ResourceBundleModel;

public class LanguageUtil {

	public final static String getBundleValue(String ... key){
		try 
		{
			Locale locale = CopContext.getDefaultLocal();
			ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("backstage",locale);
			ResourceBundleModel rsbm = new ResourceBundleModel(RESOURCE_BUNDLE, new BeansWrapper());
			return rsbm.get(key[0]).toString();
		} catch (Exception e) {
		}
		return key[0];
	}
}
