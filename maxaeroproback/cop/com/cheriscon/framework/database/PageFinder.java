package com.cheriscon.framework.database;


import java.io.Serializable;
import java.util.List;

/**
 * 分页对象. 包含当前页数据及分页信息
 * 
 */
@SuppressWarnings("serial")
public class PageFinder<T> implements Serializable {

//	private static int DEFAULT_PAGE_SIZE = 20;

	/**
	 * 每页的记录数
	 */
	private long pageSize = 3;

	/**
	 * 当前页中存放的数据
	 */
	private List<T> data;

	/**
	 * 总记录数
	 */
	private long rowCount;

	/**
	 * 页数
	 */
	private long pageCount;

	/**
	 * 跳转页数
	 */
	private long pageNo;

	/**
	 * 是否有上一页
	 */
	private boolean hasPrevious = false;

	/**
	 * 是否有下一页
	 */
	private boolean hasNext = false;
	
	public PageFinder(){
		
	}
	
	public PageFinder(long pageNo, long rowCount) {
		this.pageNo = pageNo;
		this.rowCount = rowCount;
		this.pageCount = getTotalPageCount();
		refresh();
	}

	/**
	 * 构造方法
	 */
	public PageFinder(long pageNo, long pageSize, long rowCount) {
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.rowCount = rowCount;
		this.pageCount = getTotalPageCount();
		refresh();
	}

	/**
	 * 
	 */
	public PageFinder(long pageNo, long pageSize, long rowCount, List<T> data) {
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.rowCount = rowCount;
		this.pageCount = getTotalPageCount();
		this.data = data;
		refresh();
	}

	/**
	 * 取总页数
	 */
	private final long getTotalPageCount() {
		if (rowCount % pageSize == 0)
			return rowCount / pageSize;
		else
			return rowCount / pageSize + 1;
	}

	/**
	 * 刷新当前分页对象数据
	 */
	private void refresh() {
		if (pageCount <= 1) {
			hasPrevious = false;
			hasNext = false;
		} else if (pageNo == 1) {
			hasPrevious = false;
			hasNext = true;
		} else if (pageNo == pageCount) {
			hasPrevious = true;
			hasNext = false;
		} else {
			hasPrevious = true;
			hasNext = true;
		}
	}

	/**
	 * 取每页数据数
	 */
	public long getPageSize() {
		return pageSize;
	}

	/**
	 * 取当前页中的记录.
	 */
	public Object getResult() {
		return data;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public long getRowCount() {
		return rowCount;
	}

	public void setRowCount(long rowCount) {
		this.rowCount = rowCount;
	}

	public long getPageCount() {
		if (rowCount % pageSize == 0){
			long c = (rowCount/pageSize);
			if(c==0){
				c=1;
			}
			return c;
		}else
			return rowCount / pageSize + 1;
//		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public long getPageNo() {
		return pageNo;
	}

	public void setPageNo(long pageNo) {
		this.pageNo = pageNo;
	}

	public boolean isHasPrevious() {
		return pageNo>1;
	}

	public void setHasPrevious(boolean hasPrevious) {
		this.hasPrevious = hasPrevious;
	}

	public boolean isHasNext() {
		return getPageCount()>pageNo;
	}

	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 获取跳转页第�?��数据在数据集的位�?
	 */
	public long getStartOfPage() {
		return (pageNo - 1) * pageSize;
	}
	public Integer[] getPages(){
		Integer[] pages = new Integer[2];
		Long pc = getPageCount();
		if(pc<8){
			pages[0]= 1;
			pages[1]= pc.intValue();
		}else if((pc-pageNo)<4){
			pages[0]= (pc.intValue()-6);
			pages[1]=pc.intValue();
		}else{
			pages[0]=pc.intValue()-3;
			pages[1]=pc.intValue()+3;
		}
		return pages;
	}
	
	public String[] getLast(){
		if(getPageCount()<10){
			return new String[]{};
		}
		if((getPageCount()-pageNo)<5){
			return new String[]{};
		}
		 return new String[]{null,getPageCount()+""};
	}
	
	public String[] getFirst(){
		if(getPageCount()<10){
			return new String[]{};
		}
		if(pageNo<6){
			return new String[]{};
		}
		 return new String[]{"1",null};
	}
	
}