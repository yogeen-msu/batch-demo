package com.cheriscon.framework.jms;

import java.util.HashMap;
import java.util.Map;

import com.cheriscon.common.model.EmailLog;
import com.cheriscon.cop.resource.model.CopSite;
import com.cheriscon.cop.sdk.context.CopContext;

public class EmailModel {

	private Map data = new HashMap();
	
	private String title = "";
	
	private String to = "";
	
	private String template = "";

	private String type;
	
	private String content;
	
	private EmailLog log;

	public EmailLog getLog() {
		return log;
	}

	public void setLog(EmailLog log) {
		this.log = log;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	private CopContext CopContext;
	
	
	public EmailModel() {
		CopContext =CopContext.getContext();
		/*siteid = site.getId();
		userid = site.getUserid();*/
		 
	}

	public EmailModel(Map data, String title, String to, String template,String type,String content,
			EmailLog log) {
		super();
		this.data = data;
		this.title = title;
		this.to = to;
		this.template = template;
		this.type=type;
		this.content=content;
		this.log=log;
	}

	public Map getData() {
		return data;
	}

	public void setData(Map data) {
		this.data = data;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}


	public CopContext getCopContext() {
		return CopContext;
	}

 
	
	
}
