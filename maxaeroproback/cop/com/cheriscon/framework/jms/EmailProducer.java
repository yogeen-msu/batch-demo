package com.cheriscon.framework.jms;

import javax.jms.Queue;

import org.springframework.jms.core.JmsTemplate;

public class EmailProducer {

	private JmsTemplate template;

	private Queue destination;

	public void setTemplate(JmsTemplate template) {
		this.template = template;
	}

	public void setDestination(Queue destination) {
		this.destination = destination;
	}

	public void send(EmailModel emailModel) {
		template.convertAndSend(this.destination, emailModel);
	}
	
}
