package com.cheriscon.framework.jms;

import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.springframework.jms.support.converter.MessageConverter;


public class EmailConverter implements MessageConverter {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.jms.support.converter.MessageConverter#toMessage(java.lang.Object,
	 *      javax.jms.Session)
	 */
	public Message toMessage(Object obj, Session session) throws JMSException {
		if (obj instanceof EmailModel) {
			ActiveMQObjectMessage objMsg = (ActiveMQObjectMessage) session.createObjectMessage();
			Map<String, EmailModel> map = new HashMap<String, EmailModel>();
			map.put("EmailModel", (EmailModel) obj);
			objMsg.setObjectProperty("Map", map);
			return objMsg;
		} else {
			throw new JMSException("Object:[" + obj + "] is not Member");
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.jms.support.converter.MessageConverter#fromMessage(javax.jms.Message)
	 */
	public Object fromMessage(Message msg) throws JMSException {
		if (msg instanceof ObjectMessage) {
			return ((Map) ((ObjectMessage) msg).getObjectProperty("Map")).get("EmailModel");
		} else {
			throw new JMSException("Msg:[" + msg + "] is not Map");
		}
	}
	
}
