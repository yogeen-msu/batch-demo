package com.cheriscon.framework.jms;

import java.io.File;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.util.HtmlUtils;

import com.cheriscon.app.base.core.model.Smtp;
import com.cheriscon.app.base.core.service.ISmtpManager;
import com.cheriscon.backstage.system.service.IEmailLogService;
import com.cheriscon.backstage.system.service.ISealerDataAuthDeailsService;
import com.cheriscon.common.model.EmailLog;
import com.cheriscon.common.model.SealerDataAuthDeails;
import com.cheriscon.cop.sdk.context.CopContext;
import com.cheriscon.cop.sdk.context.CopSetting;
import com.cheriscon.cop.sdk.utils.DateUtil;
import com.cheriscon.cop.sdk.utils.FreeMarkerUtil;
import com.sun.xml.messaging.saaj.util.ByteOutputStream;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class EmailConsumer {

	private JavaMailSender mailSender;
	private ISmtpManager smtpManager;

	@Resource
	private IEmailLogService emailLogService;
	@Resource
	private ISealerDataAuthDeailsService sealerDataAuthService;

	public String getSupport(String support) {

		try {
			SealerDataAuthDeails sealer = sealerDataAuthService
					.getDataAuthBySealerCode(support);
			if (sealer != null) {
				String authCode = sealer.getAuthCode();
				if (authCode.startsWith("0063")) {
					support = "C-0063";
				}
				if (authCode.startsWith("0064")) {
					support = "C-0759";
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return support;
	}

	public void sendEmail(EmailModel emailModel) {

		if (emailModel.getLog() != null) {
			sendEmail2(emailModel);
		} else {

			Smtp smtp = null;
			String html = "";
			try {
				if (emailModel.getType() != null) {
					smtp = smtpManager.getCurrentSmtp(getSupport(emailModel
							.getType()));
				} else {
					smtp = smtpManager.getCurrentSmtp();
				}
				CopContext.setContext(emailModel.getCopContext());
				Properties props = System.getProperties();
				if (smtp.getHost().indexOf("gmail") > 0) {
					props.setProperty("mail.smtp.socketFactory.class",
							"javax.net.ssl.SSLSocketFactory");
				}
				props.setProperty("mail.smtp.socketFactory.fallback", "false");
				props.setProperty("mail.smtp.socketFactory.port",
						smtp.getPort());
				props.setProperty("mail.smtp.timeout", "300000");
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.port", smtp.getPort());
				props.put("mail.smtp.host", smtp.getHost());

				JavaMailSenderImpl javaMailSender = (JavaMailSenderImpl) mailSender;
				javaMailSender.setUsername(smtp.getUsername());
				javaMailSender.setPassword(smtp.getPassword());

				javaMailSender.setJavaMailProperties(props);

				MimeMessage message = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(message, true,
						"UTF-8");

				helper.setSubject(emailModel.getTitle());
				helper.setTo(emailModel.getTo());
				helper.setFrom(smtp.getMail_from());
				Configuration cfg = FreeMarkerUtil.getCfg();
				String pageFolder = CopSetting.cop_PATH
						+ CopContext.getContext().getContextPath() + "/themes/";
				cfg.setDirectoryForTemplateLoading(new File(pageFolder));
				Template temp = cfg.getTemplate(emailModel.getTemplate());
				ByteOutputStream stream = new ByteOutputStream();
				Writer out = new OutputStreamWriter(stream);
				temp.process(emailModel.getData(), out);
				out.flush();
				html = stream.toString();
				html = HtmlUtils.htmlUnescape(html);
				helper.setText(html, true);
				javaMailSender.send(message);
				this.smtpManager.sendOneMail(smtp);
				this.saveEmailLog(emailModel, smtp, html, 0); // 记录发送日志
				CopContext.remove();
			} catch (Exception e) {
				this.saveEmailLog(emailModel, smtp, html, 1); // 记录发送日志
				System.out.println("邮件发送失败1-"
						+ DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss")
						+ ":" + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public void sendEmail2(EmailModel emailModel) {
		Smtp smtp = null;
		String html = "";
		EmailLog log = null;
		try {
			log = emailModel.getLog();
			smtp = smtpManager.getSmtpByEmail(log.getFromUser());
			CopContext.setContext(emailModel.getCopContext());
			Properties props = System.getProperties();
			props.setProperty("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory");
			props.setProperty("mail.smtp.socketFactory.fallback", "false");
			props.setProperty("mail.smtp.socketFactory.port", smtp.getPort());
			props.put("mail.smtp.auth", "true");

			JavaMailSenderImpl javaMailSender = (JavaMailSenderImpl) mailSender;
			javaMailSender.setHost(smtp.getHost());
			javaMailSender.setUsername(smtp.getUsername());
			javaMailSender.setPassword(smtp.getPassword());
			javaMailSender.setPort(Integer.parseInt(smtp.getPort()));

			if (smtp.getHost().indexOf("gmail") > 0) {
				javaMailSender.setJavaMailProperties(props);
			}

			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true,
					"UTF-8");

			helper.setSubject(emailModel.getTitle());
			helper.setTo(emailModel.getTo());

			helper.setFrom(smtp.getMail_from());
			html = emailModel.getContent();

			helper.setText(html, true);
			javaMailSender.send(message);

			this.smtpManager.sendOneMail(smtp);
			log.setCountNum(0);
			log.setUpdateDate(DateUtil.toString(new Date(),
					"yyyy-MM-dd hh:mm:ss"));
			emailLogService.updateEmailLog(log);
			; // 记录发送日志
			CopContext.remove();
		} catch (Exception e) {
			System.out.println("邮件发送失败2-"
					+ DateUtil.toString(new Date(), "yyyy-MM-dd hh:mm:ss")
					+ ":" + e.getMessage());
			log.setCountNum(log.getCountNum() + 1);
			log.setUpdateDate(DateUtil.toString(new Date(),
					"yyyy-MM-dd hh:mm:ss"));
			try {
				emailLogService.updateEmailLog(log);
			} catch (Exception e1) {
			}
		}
	}

	public JavaMailSender getMailSender() {
		return mailSender;
	}

	public void saveEmailLog(EmailModel emailModel, Smtp smtp, String content,
			Integer num) {

		try {
			EmailLog log = new EmailLog();
			log.setToUser(emailModel.getTo());
			log.setFromUser(smtp.getMail_from());
			log.setTitle(emailModel.getTitle());
			log.setContent(content);
			log.setCreateDate(DateUtil.toString(new Date(),
					"yyyy-MM-dd hh:mm:ss"));
			log.setCountNum(num);
			emailLogService.saveEmailLog(log);
		} catch (Exception e) {
			System.out.println("日志错误");
		}
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public ISmtpManager getSmtpManager() {
		return smtpManager;
	}

	public void setSmtpManager(ISmtpManager smtpManager) {
		this.smtpManager = smtpManager;
	}

	public IEmailLogService getEmailLogService() {
		return emailLogService;
	}

	public void setEmailLogService(IEmailLogService emailLogService) {
		this.emailLogService = emailLogService;
	}

}
